package com.vtm.adslib;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.vtm.adslib.template.TemplateView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AdsNativeHelper {
    private static final int NUMBER_OF_ADS = 5;
    private static final int NUMBER_RELOAD = 5;
//    private static final int TIME_RELOAD = 3 * 60 * 1000;//3 phut
    private static final int TIME_EXPIRE = 45 * 60 * 1000;//45 phut
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();
    private AdLoader adLoader;
    private TemplateView adContainer;
    private boolean isShowAds = false;
    private Activity context;
    private int reloadIndex = 0;
    private Handler handler = new Handler();
    private long startTime = 0;
    private long firstTimeLoad = 0;

    private AdsNativeHelper() {
    }

    public static AdsNativeHelper getInstance() {
        return AdsNativeHelper.SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final AdsNativeHelper INSTANCE = new AdsNativeHelper();
    }

    public void initAds(Activity context, String adUnitId) {
        this.context = context;
        startTime = System.currentTimeMillis();
        firstTimeLoad = System.currentTimeMillis();
        reloadIndex = 0;
        mNativeAds.clear();

        adLoader = new AdLoader.Builder(context, adUnitId)
                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        mNativeAds.add(unifiedNativeAd);
                        if (!adLoader.isLoading()) {
                            firstTimeLoad = System.currentTimeMillis();
                            Log.i("ADS", "Time start init ads native loaded: " + (System.currentTimeMillis()) + "|" + (System.currentTimeMillis() - startTime));
                            //Load xong ad neu chua show thi show len
                            if(!isShowAds)
                                addAdsView();

                            //Dat schedule 120s load lai 1 lan
//                            reloadAds();
                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // Handle the failure by logging, altering the UI, and so on.

                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        .setRequestMultipleImages(true)
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        loadAd();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            loadAd();
            handler.removeCallbacks(runnable);
        }
    };

//    private void reloadAds()
//    {
//        reloadIndex++;
//        firstTimeLoad = System.currentTimeMillis();
//        if(reloadIndex == NUMBER_RELOAD)
//        {
//            handler.removeCallbacks(runnable);
//            return;
//        }
//        handler.postDelayed(runnable, TIME_RELOAD);
//    }

    public void loadAd() {
        if (adLoader != null && !adLoader.isLoading()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
                    .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
                    .build();
            adLoader.loadAds(adRequest, NUMBER_OF_ADS);
        }
    }

    public boolean canShowAd() {
        return adLoader != null && mNativeAds.size() > 0 && !checkAdsExpire();
    }

    public void showAd(TemplateView adContainer) {
        if (AdsCommon.checkShowAds()) {
            this.adContainer = adContainer;
            if (canShowAd()) {
                addAdsView();
            } else {
                isShowAds = false;
                loadAd();
            }
        }
    }

    private void addAdsView() {
        if (adContainer == null /*|| adView == null*/ ||  mNativeAds.size() == 0) return;

        isShowAds = true;

        Random ran = new Random();
        int pos = ran.nextInt(mNativeAds.size());
        if (pos == mNativeAds.size())
            pos = mNativeAds.size() - 1;
        adContainer.setNativeAd(mNativeAds.get(pos));
        adContainer.setVisibility(View.VISIBLE);

//        if (adContainer.getChildCount() > 0) {
//            adContainer.removeAllViews();
//        }
//        if (adView.getParent() != null) {
//            ((ViewGroup) adView.getParent()).removeView(adView);
//        }
//        // Add the banner ad to the ad view.
//        adContainer.addView(adView);
//        adContainer.setVisibility(View.VISIBLE);
    }

    private boolean checkAdsExpire()
    {
        long currentTime = System.currentTimeMillis();
        if (firstTimeLoad > 0 && currentTime - firstTimeLoad > TIME_EXPIRE) {
            return true;
        }
        return false;
    }
}

package com.vtm.adslib;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AdsHelper {
    private Context context;
    private AdView adView;
    private boolean isLoaded = false;
    private boolean isReloaded = false;

    public AdsHelper(Context context)
    {
        this.context = context;
    }

    public void init(String adsId, AdSize type, final AdsBannerListener listener) {
        if (context == null || TextUtils.isEmpty(adsId))
            return;

        adView = new AdView(context);
        adView.setAdUnitId(adsId);
        adView.setAdSize(type);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                isLoaded = true;

                if(listener != null)
                    listener.onAdShow(adView);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                isLoaded = false;
                if (!isReloaded) {
                    isReloaded = true;
                    loadAd();
                }
            }
        });
        loadAd();
    }

    public void loadAd() {
        if (adView != null && !adView.isLoading()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
                    .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
                    .build();
            adView.loadAd(adRequest);
        }
    }

    private boolean canShowAd() {
        return adView != null && isLoaded;
    }

    public void showAd(ViewGroup adContainer) {
        if (AdsCommon.checkShowAds()) {
            if (canShowAd()) {
                isReloaded = false;
                addAdsView(adContainer);
            } else {
                loadAd();
            }
        }
    }

    private void addAdsView(ViewGroup adContainer) {
        if (adContainer == null || adView == null) return;

        if (adContainer.getChildCount() > 0) {
            adContainer.removeAllViews();
        }
        if (adView.getParent() != null) {
            ((ViewGroup) adView.getParent()).removeView(adView);
        }

        // Add the banner ad to the ad view.
        adContainer.addView(adView);
        adContainer.setVisibility(View.VISIBLE);
    }

    public void onResume() {
        if(adView != null)
            adView.resume();
    }

    public void onPause() {
        if(adView != null)
            adView.pause();
    }

    public void onDestroy() {
        if(adView != null)
            adView.destroy();
    }

    public interface AdsBannerListener {
        void onAdShow(AdView adView);
    }
}

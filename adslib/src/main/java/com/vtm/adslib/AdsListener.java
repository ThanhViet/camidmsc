package com.vtm.adslib;

public interface AdsListener {
    void onAdClosed();
    void onAdShow();
}

package com.vtm.adslib;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AdsBannerHelper {
    private AdView adView;
    private ViewGroup adContainer;
    private boolean isReloaded = false;
    private boolean isLoaded = false;
    private Context context;
    private long startTime = 0;

    private AdsBannerHelper() {
    }

    public static AdsBannerHelper getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final AdsBannerHelper INSTANCE = new AdsBannerHelper();
    }

    public void init(Context context, String adsId, AdSize type) {
        if (TextUtils.isEmpty(adsId))
            return;
        startTime = System.currentTimeMillis();
        this.context = context;
        adView = new AdView(context);
        adView.setAdUnitId(adsId);
        adView.setAdSize(type);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                isLoaded = true;
                addAdsView();

                Log.i("ADS", "Time start init ads banner loaded: " + (System.currentTimeMillis()) + "|" + (System.currentTimeMillis() - startTime));
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                isLoaded = false;
                if (!isReloaded) {
                    isReloaded = true;
                    loadAd();
                }

//                GaHelper.INSTANCE.logEvent(context, GaHelper.ADS_ERROR, new EventModel(
//                        "ADS_BANNER",
//                        "ADS_ERROR_CODE: " + i,
//                        GaHelper.ADS_ERROR));
            }
        });
        loadAd();
    }

    private void loadAd() {
        if (adView != null && !adView.isLoading()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
                    .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
                    .build();
            adView.loadAd(adRequest);

//            if (context != null) {
//                GaHelper.INSTANCE.logEvent(
//                        context,
//                        GaHelper.ADS_REQUEST,
//                        new EventModel("ADS_BANNER", "ADS_BANNER", GaHelper.ADS_REQUEST)
//                );
//            }
        }
    }

    private boolean canShowAd() {
        return adView != null && isLoaded;
    }

    public void showAd(ViewGroup adContainer, AdsListener listener) {
        if (AdsCommon.checkShowAds()) {
            this.adContainer = adContainer;
            if (canShowAd()) {
                isReloaded = false;
                addAdsView();

                if (listener != null)
                    listener.onAdShow();
            } else {
                loadAd();
            }
        }
    }

    private void addAdsView() {
        if (adContainer == null || adView == null) return;

        if (adContainer.getChildCount() > 0) {
            adContainer.removeAllViews();
        }
        if (adView.getParent() != null) {
            ((ViewGroup) adView.getParent()).removeView(adView);
        }

        // Add the banner ad to the ad view.
        adContainer.addView(adView);
        adContainer.setVisibility(View.VISIBLE);
    }

    public void onResume() {
        if(adView != null)
            adView.resume();
    }

    public void onPause() {
        if(adView != null)
            adView.pause();
    }

    public void onDestroy() {
        if(adView != null)
            adView.destroy();
    }
}

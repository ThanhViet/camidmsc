package com.vtm.adslib;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;


public class AdsInterstitialHelper {
    private InterstitialAd interstitialAd;
    private AdsListener adsListener;
    private boolean isReloaded = false;
    private long lastTimeShowInterstitialAd;
    private Context context;

    private AdsInterstitialHelper() {
    }

    public static AdsInterstitialHelper getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final AdsInterstitialHelper INSTANCE = new AdsInterstitialHelper();
    }
    long startTime;
    public void init(Context context, String adsId) {
        if (TextUtils.isEmpty(adsId))
            return;
        startTime = System.currentTimeMillis();
        this.context = context;
        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(adsId);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (adsListener != null)
                    adsListener.onAdClosed();
                loadInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if (!isReloaded) {
                    isReloaded = true;
                    loadInterstitialAd();
                }

//                GaHelper.INSTANCE.logEvent(context, GaHelper.ADS_ERROR, new EventModel(
//                        "ADS_FULLSCREEN",
//                        "ADS_ERROR_CODE: " + i,
//                        GaHelper.ADS_ERROR));
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.i("ADS", "Time start init ads full loaded: " + (System.currentTimeMillis()) + "|" + (System.currentTimeMillis() - startTime));
            }
        });
        loadInterstitialAd();
    }

    private void loadInterstitialAd() {
        if (interstitialAd != null && !interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
                    .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
                    .build();
            interstitialAd.loadAd(adRequest);

//            if (context != null) {
//                GaHelper.INSTANCE.logEvent(
//                        context,
//                        GaHelper.ADS_REQUEST,
//                        new EventModel("ADS_FULLSCREEN", "ADS_FULLSCREEN", GaHelper.ADS_REQUEST)
//                );
//            }
        }
    }

    private boolean canShowInterstitialAd() {
        return interstitialAd != null && interstitialAd.isLoaded();
    }

    public void showInterstitialAd(AdsListener adsListener) {
        if (AdsCommon.checkShowAds()) {
            if (canShowInterstitialAd()) {
                long currentTime = System.currentTimeMillis();
                //Check time sau bao lau thi hien lai
                if (currentTime - lastTimeShowInterstitialAd > getLimitTime()) {
                    lastTimeShowInterstitialAd = currentTime;
                    isReloaded = false;
                    this.adsListener = adsListener;
                    interstitialAd.show();

                    if (adsListener != null)
                        adsListener.onAdShow();
                } else {
                    adsListener.onAdClosed();
                }
            } else {
                loadInterstitialAd();
                adsListener.onAdClosed();
            }
        } else {
            adsListener.onAdClosed();
        }
    }

    private long getLimitTime() {
        return FirebaseRemoteConfig.getInstance().getLong(AdsCommon.KEY_FIREBASE.AD_FULL_INTERVAL);
    }
}

package com.vtm.adslib;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class AdsCommon {
    public static final class KEY_FIREBASE {
        public static final String ENABLE_ADS = "ENABLE_ADS";
        public static final String AD_REWARD_SHOW = "AD_REWARD_SHOW";
        public static final String AD_FULL_INTERVAL = "AD_FULL_INTERVAL";
        public static final String AD_APP_ID = "AD_APP_ID";
    }

    public static boolean checkShowAds() {
        // Co enable ads ko? ko phai nam trong whitelist vs ad fullscreen khac null
        if (isEnableAds() == 0)
            return false;
        return true;
    }

    private static long isEnableAds() {
        return FirebaseRemoteConfig.getInstance().getLong(AdsCommon.KEY_FIREBASE.ENABLE_ADS);
    }
}

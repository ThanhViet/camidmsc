package com.vtm.adslib;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class AdsRewardedHelper {
    private RewardedAd rewardedAd;
    private AdsRewardListener adsListener;
    private boolean isReloaded = false;
    private String adsId;
    private Activity context;

    private AdsRewardedHelper() {
    }

    public static AdsRewardedHelper getInstance() {
        return AdsRewardedHelper.SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final AdsRewardedHelper INSTANCE = new AdsRewardedHelper();
    }

    public void init(Activity context, String adsId) {
        if (context == null || TextUtils.isEmpty(adsId))
            return;

        this.context = context;
        this.adsId = adsId;
        loadAd();
    }

    RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
        @Override
        public void onRewardedAdLoaded() {
            // Ad successfully loaded.
            if (BuildConfig.DEBUG)
                Toast.makeText(context, "onRewardedAdLoaded!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onRewardedAdFailedToLoad(int errorCode) {
            // Ad failed to load.
            if (!isReloaded) {
                isReloaded = true;
                loadAd();
            }
        }
    };

    RewardedAdCallback adCallback = new RewardedAdCallback() {
        public void onRewardedAdOpened() {
            // Ad opened.
            if (adsListener != null)
                adsListener.onAdShow();
        }

        public void onRewardedAdClosed() {
            // Ad closed.
            if (adsListener != null)
                adsListener.onAdClosed();
            loadAd();
        }

        public void onUserEarnedReward(@NonNull RewardItem reward) {
            // User earned reward.
            if (adsListener != null)
                adsListener.onUserEarnedReward(reward);
        }

        public void onRewardedAdFailedToShow(int errorCode) {
            // Ad failed to display
            if (adsListener != null)
                adsListener.onAdClosed();
        }
    };

    private void loadAd() {
        if (context == null || TextUtils.isEmpty(adsId))
            return;
        rewardedAd = new RewardedAd(context, adsId);
        if (rewardedAd != null && !rewardedAd.isLoaded()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("891BEFAD795E7F1817C44E163E1E4634")
                    .addTestDevice("A99759E9DF92EA8DA850072342D81F1D")
                    .build();
            rewardedAd.loadAd(adRequest, adLoadCallback);
        }
    }

    public boolean canShowAd() {
        boolean isAdsShow = FirebaseRemoteConfig.getInstance().getLong(AdsCommon.KEY_FIREBASE.AD_REWARD_SHOW) == 1;
        return context != null && rewardedAd != null && rewardedAd.isLoaded() && isAdsShow;
    }

    public void showAd(AdsRewardListener adsListener) {
        if (context != null && AdsCommon.checkShowAds()) {
            if (canShowAd()) {
                isReloaded = false;
                this.adsListener = adsListener;
                rewardedAd.show(context, adCallback);
            } else {
                loadAd();
                adsListener.onAdClosed();
            }
        } else {
            adsListener.onAdClosed();
        }
    }

    public interface AdsRewardListener {
        void onAdClosed();

        void onAdShow();

        void onUserEarnedReward(@NonNull RewardItem reward);
    }
}

package com.stringee;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


public class Chart {
    private String startTime;
    private String endTime;
    private JsonArray values;

    private String mediaType;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public JsonArray getValues() {
        return values;
    }

    public void setValues(JsonArray values) {
        this.values = values;
    }

    public Chart(){
        values = new JsonArray();
    }

    public void addElement(JsonElement value){
        values.add(value);
    }

    public void addAll(JsonArray arr){
       values.addAll(arr);
    }


    public JsonObject toObject(){
        JsonObject obj = new JsonObject();
        try {
            obj.addProperty("startTime", this.startTime);
            obj.addProperty("endTime", this.endTime);
            obj.addProperty("values",   values.toString());
            obj.addProperty("mediaType", mediaType);
        }catch (Exception e){
            e.printStackTrace();
        }
        return obj;
    }
}

package com.stringee;

import org.webrtc.IceCandidate;

/**
 * Created by luannguyen on 9/27/16.
 */
public interface StringeeCallListener {

    public static enum StringeeConnectionState {
        NEW,
        CHECKING,
        CONNECTED,
        COMPLETED,
        FAILED,
        DISCONNECTED,
        CLOSED,
        ICE_REMOVE,
        RESTARTICE_CONNECTED,
        RESTARTICE_ONFAIL,
        LOG_CONNECTING;

        private StringeeConnectionState() {

        }
    }

    public void onSetSdpSuccess(boolean isSetLocalSDP,StringeeSessionDescription sdp);

    public void onCreateIceCandidate(StringeeIceCandidate iceCandidate);

    public void onChangeConnectionState(StringeeConnectionState state);

    public void onLocalStreamCreated(StringeeStream stream);

    public void onAddStream(StringeeStream StringeeStream);

    public void onIceCandidatesRemoved(IceCandidate[] iceCandidates);

}

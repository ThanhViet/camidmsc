package com.stringee;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SignalingStatisticElement {

    private String time;
    private String type;
    private String value;


    public SignalingStatisticElement(String type, String value) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        this.time = dateFormat.format(date);
        this.type = type;
        this.value = value;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public JsonObject toObject(){
        JsonObject obj = new JsonObject();
        try {
            obj.addProperty("time", this.time);
            obj.addProperty("type", this.type);
            obj.addProperty("value",this.value);

        }catch (Exception e){
            e.printStackTrace();
        }
        return obj;
    }



}

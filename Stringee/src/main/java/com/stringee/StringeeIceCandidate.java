package com.stringee;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by luannguyen on 9/27/16.
 */
public class StringeeIceCandidate {
    public final String sdpMid;
    public final int sdpMLineIndex;
    public final String sdp;

    public StringeeIceCandidate(String sdpMid, int sdpMLineIndex, String sdp) {
        this.sdpMid = sdpMid;
        this.sdpMLineIndex = sdpMLineIndex;
        this.sdp = sdp;
    }

    public String toString() {
        return this.sdpMid + ":" + this.sdpMLineIndex + ":" + this.sdp;
    }

    public static StringeeIceCandidate parseFromCallData(JSONObject js) {
        String sdpMid = js.optString("sdpMid");
        int sdpMLineIndex = js.optInt("sdpMLineIndex");
        String sdp2;
        sdp2 = js.optString("sdp");
        return new StringeeIceCandidate(sdpMid, sdpMLineIndex, sdp2);
    }


    public static boolean compare(StringeeIceCandidate ice1, StringeeIceCandidate ice2) {
        if (ice1 == null || ice2 == null) return false;
        String sdp1 = ice1.sdp;
        String sdpMid = ice1.sdpMid;
        if (TextUtils.isEmpty(sdp1) || TextUtils.isEmpty(sdpMid)) return false;
        return sdp1.equals(ice2.sdp) && sdpMid.equals(ice2.sdpMid) && ice1.sdpMLineIndex == ice2.sdpMLineIndex;
    }

    public String getSdpMid() {
        return sdpMid;
    }

    public int getSdpMLineIndex() {
        return sdpMLineIndex;
    }

    public String getSdp() {
        return sdp;
    }
}

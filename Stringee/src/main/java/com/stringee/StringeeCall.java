package com.stringee;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.audiofx.AcousticEchoCanceler;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RTCStats;
import org.webrtc.RTCStatsCollectorCallback;
import org.webrtc.RTCStatsReport;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SoftwareVideoDecoderFactory;
import org.webrtc.SoftwareVideoEncoderFactory;
import org.webrtc.StatsObserver;
import org.webrtc.StatsReport;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
//import org.webrtc.VideoRenderer;
import org.webrtc.VideoDecoderFactory;
import org.webrtc.VideoEncoderFactory;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;
import org.webrtc.audio.AudioDeviceModule;
import org.webrtc.audio.JavaAudioDeviceModule;
import org.webrtc.voiceengine.WebRtcAudioUtils;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static com.stringee.StringeeConstant.VIDEO_HD_MIN_HEIGHT;
import static com.stringee.StringeeConstant.VIDEO_HD_MIN_WIDTH;

/**
 * Created by luannguyen on 9/27/16.
 */
public class StringeeCall {

    private static final String VIDEO_CODEC_H264 = "H264";
    public static final boolean ENABLE_UPLOAD_CALL_REPORT = true;

    public static final String AUDIO_TRACK_ID = "ARDAMSa0";
    public static final String VIDEO_TRACK_ID = "ARDAMSv0";
    private static final String TAG = StringeeCall.class.getSimpleName();
    private PeerConnection peerConnection;
    private AudioSource audioSource;
    private AudioTrack localAudioTrack;
    private MediaConstraints sdpMediaConstraints;
    private static VideoSource videoSource;
    private static VideoCapturer videoCapturer;//to switch camera
    private static VideoTrack localVideoTrack;
    private Context mContext;
    public boolean callStarted = false;
    public boolean isInitVideoCapture;
    private boolean isCaptureStopped;

    LinkedList<StringeeIceCandidate> queueIceCandidate = new LinkedList<>();

    private LinkedList<StringeeIceServer> iceServers = new LinkedList<>();

    StringeeCallListener listener = null;
    boolean iceConnected = false;

    PeerConnectionFactory peerConnectionFactory;
    final ScheduledExecutorService executor;

    private SessionDescription localDescription;
    private boolean sdpSent;
    private EglBase.Context localContext;
    private EglBase.Context remoteContext;
    private EglBase eglBase;
    private String preferedAudioCodec;
    private String preferedVideoCodec;

    private CallConnectionListener connectionListener;

    private boolean notFoundFrontCam;

    public StringeeCall(StringeeCallListener listener) {
        notFoundFrontCam = false;
        executor = Executors.newSingleThreadScheduledExecutor();
        this.listener = listener;
    }

    public void setConnectionListener(CallConnectionListener listener) {
        this.connectionListener = listener;
    }

    public CallConnectionListener getConnectionListener() {
        return connectionListener;
    }


    /**
     * dainv
     * current: not use hardware acceleration and H264 codec because some device error with them.
     * can research after and use them later
     */
    public void init(final Context context) {
        Log.e("Stringee", "init begin");
        mContext = context;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Log.e("Stringee", "init begin execute");
                // Create peer connection factory.
                eglBase = EglBase.create();
                localContext = eglBase.getEglBaseContext();
                remoteContext = eglBase.getEglBaseContext();
//                Logging.enableLogToDebugOutput(Logging.Severity.LS_INFO);
                // create PeerConnectionFactory
                PeerConnectionFactory.initialize(PeerConnectionFactory.InitializationOptions
                        .builder(context.getApplicationContext())
                        .createInitializationOptions());
                PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
                VideoEncoderFactory videoEncoderFactory;
                VideoDecoderFactory videoDecoderFactory;
                // only use software codec video for samsung exynos 74: eg galaxy s6
//                if (Build.BRAND.equals("samsung") && Build.HARDWARE.contains("exynos74")) {
                videoEncoderFactory = new SoftwareVideoEncoderFactory();
                videoDecoderFactory = new SoftwareVideoDecoderFactory();
//                } else {
//                    videoEncoderFactory = new DefaultVideoEncoderFactory(localContext,
//                            true, false);
//                    videoDecoderFactory = new DefaultVideoDecoderFactory(localContext);
//                    boolean isSupportH264 = false;
//                    VideoCodecInfo[] videoEncoderSupports = videoEncoderFactory.getSupportedCodecs();
//                    VideoCodecInfo[] videoDecoderSupports = videoDecoderFactory.getSupportedCodecs();
//                    for (VideoCodecInfo encoderInfo : videoEncoderSupports) {
//                        if (encoderInfo.name.equals(VIDEO_CODEC_H264)) {
//                            isSupportH264 = true;
//                            break;
//                        }
//                    }
//                    // set prefer Video Codec if support H264
//                    if (isSupportH264) {
//                        setPreferedVideoCodec(VIDEO_CODEC_H264);
//                    }
//                }
                peerConnectionFactory = PeerConnectionFactory.builder()
                        .setOptions(options)
                        .setAudioDeviceModule(createAudioDeviceModule())
                        .setVideoEncoderFactory(videoEncoderFactory)
                        .setVideoDecoderFactory(videoDecoderFactory)
                        .createPeerConnectionFactory();
                if (ENABLE_UPLOAD_CALL_REPORT) {
                    webrtcInternalDumpObj = createDumpObject();
                    mapChart = new HashMap<>();
                }

                Log.e("Stringee", "init end execute");
            }
        });
    }

    private AudioDeviceModule createAudioDeviceModule() {
        JavaAudioDeviceModule.AudioRecordErrorCallback audioRecordErrorCallback = new JavaAudioDeviceModule.AudioRecordErrorCallback() {
            @Override
            public void onWebRtcAudioRecordInitError(String s) {

            }

            @Override
            public void onWebRtcAudioRecordStartError(JavaAudioDeviceModule.AudioRecordStartErrorCode audioRecordStartErrorCode, String s) {

            }

            @Override
            public void onWebRtcAudioRecordError(String s) {

            }
        };

        JavaAudioDeviceModule.AudioTrackErrorCallback audioTrackErrorCallback = new JavaAudioDeviceModule.AudioTrackErrorCallback() {
            @Override
            public void onWebRtcAudioTrackInitError(String s) {

            }

            @Override
            public void onWebRtcAudioTrackStartError(JavaAudioDeviceModule.AudioTrackStartErrorCode audioTrackStartErrorCode, String s) {

            }

            @Override
            public void onWebRtcAudioTrackError(String s) {

            }
        };
        return JavaAudioDeviceModule.builder(mContext)
                .setAudioRecordErrorCallback(audioRecordErrorCallback)
                .setAudioTrackErrorCallback(audioTrackErrorCallback)
                .setUseHardwareAcousticEchoCanceler(true)
                .setUseHardwareNoiseSuppressor(true)
                .createAudioDeviceModule();
    }

    StringeeCreateSdpObserver createSdpObserver;

    public void startCall(final boolean isCaller, final boolean isCallOut, final boolean hasVideo, final int quality,
                          final JSONObject sdpObject, final int iceTimeout, final String bundlePolicy,
                          final String rtcpMuxPolicy, final String iceTransportsType) {
//        Logging.enableLogToDebugOutput(Logging.Severity.LS_VERBOSE);
//        Toast.makeText(mContext, bundlePolicy + "|" + rtcpMuxPolicy + "|" + iceTransportsType, Toast.LENGTH_SHORT).show();
        Log.e("Stringee", "startCall begin");
        callStarted = true;

        createSdpObserver = new StringeeCreateSdpObserver(true, this);

        final PCObserver pcObserver = new PCObserver(this);
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Log.e("Stringee", "startCall begin execute");
                // Create peer connection constraints.
                MediaConstraints pcConstraints = new MediaConstraints();
                pcConstraints.optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));

                LinkedList<PeerConnection.IceServer> iceServers = createIceServers();
                PeerConnection.RTCConfiguration rtcConfig = new PeerConnection.RTCConfiguration(iceServers);
                /*if (isCallOut) {
                    rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXCOMPAT;
                } else {
                    rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.BALANCED;
                }*/
                //TODO old version
                /*rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.BALANCED;
                rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE;*/
                PeerConnection.BundlePolicy bPolicy = PeerConnection.BundlePolicy.BALANCED;
                if (!TextUtils.isEmpty(bundlePolicy)) {
//                    if ("MAXBUNDLE".equals(bundlePolicy))
//                        bPolicy = PeerConnection.BundlePolicy.MAXBUNDLE;
//                    if ("MAXCOMPAT".equals(bundlePolicy))
//                        bPolicy = PeerConnection.BundlePolicy.MAXCOMPAT;
                }


                if (isCallOut) {
                    rtcConfig.bundlePolicy = bPolicy;
                    //rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.NEGOTIATE;
                    PeerConnection.IceTransportsType iceTranType = PeerConnection.IceTransportsType.NOHOST;
                    if (!TextUtils.isEmpty(iceTransportsType)) {
                        if ("NONE".equals(iceTransportsType))
                            iceTranType = PeerConnection.IceTransportsType.NONE;
                        if ("RELAY".equals(iceTransportsType))
                            iceTranType = PeerConnection.IceTransportsType.RELAY;
                        if ("ALL".equals(iceTransportsType))
                            iceTranType = PeerConnection.IceTransportsType.ALL;
                    }
                    rtcConfig.iceTransportsType = iceTranType;
                    rtcConfig.pruneTurnPorts = true;
                } else {
                    rtcConfig.bundlePolicy = bPolicy;

                    PeerConnection.IceTransportsType iceTranType = PeerConnection.IceTransportsType.RELAY;
                    if (!TextUtils.isEmpty(iceTransportsType)) {
                        if ("NONE".equals(iceTransportsType))
                            iceTranType = PeerConnection.IceTransportsType.NONE;
                        if ("NOHOST".equals(iceTransportsType))
                            iceTranType = PeerConnection.IceTransportsType.NOHOST;
                        if ("ALL".equals(iceTransportsType))
                            iceTranType = PeerConnection.IceTransportsType.ALL;
                    }

                    rtcConfig.iceTransportsType = iceTranType;
                    //rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE;
                }

                if (iceTimeout != 0) {
                    rtcConfig.iceConnectionReceivingTimeout = iceTimeout;
                }

                PeerConnection.RtcpMuxPolicy rtcpMux = PeerConnection.RtcpMuxPolicy.REQUIRE;
                if (!TextUtils.isEmpty(rtcpMuxPolicy)) {
                    if ("NEGOTIATE".equals(rtcpMuxPolicy))
                        rtcpMux = PeerConnection.RtcpMuxPolicy.NEGOTIATE;
                }
                rtcConfig.rtcpMuxPolicy = rtcpMux;
                rtcConfig.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY;
//                rtcConfig.iceRegatherOnFailedNetworksInterval = Integer.valueOf(20000);
                //rtcConfig.iceRegatherIntervalRange = new PeerConnection.IntervalRange(5000, 30000);//TODO test
                //disable TCP
                rtcConfig.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED;
                Log.e("Stringee", "startCall begin createPeer");
                peerConnection = peerConnectionFactory.createPeerConnection(rtcConfig, pcObserver);
                Log.e("Stringee", "startCall end createPeer");
                MediaStream mediaStream = peerConnectionFactory.createLocalMediaStream("ARDAMS");

                Log.e("Stringee", "VIETTEL_DEBUG: 01");

                MediaConstraints audioConstraints = new MediaConstraints();
                if (AcousticEchoCanceler.isAvailable() && WebRtcAudioUtils.isAcousticEchoCancelerSupported()) {
                    WebRtcAudioUtils.setWebRtcBasedAcousticEchoCanceler(true);
                    WebRtcAudioUtils.useWebRtcBasedAcousticEchoCanceler();
                } else { //Step 2: Nếu thiết bị không hỗ trợ thì dùng của webrtc
                    audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair("googNoiseSuppression", "true"));
                    audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair("googEchoCancellation", "true"));
                    audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair("googHighpassFilter ", "true"));
                    audioConstraints.mandatory.add(new MediaConstraints.KeyValuePair("googTypingNoiseDetection ", "true"));
                }
                audioSource = peerConnectionFactory.createAudioSource(audioConstraints);
                Log.e("Stringee", "VIETTEL_DEBUG: 02");
                localAudioTrack = peerConnectionFactory.createAudioTrack(AUDIO_TRACK_ID, audioSource);
                Log.e("Stringee", "VIETTEL_DEBUG: 03");
                localAudioTrack.setEnabled(true);
                mediaStream.addTrack(localAudioTrack);
                Log.e("Stringee", "VIETTEL_DEBUG: 04");

                if (!isCallOut) {
                    int minWidth = StringeeConstant.VIDEO_NORMAL_MIN_WIDTH;
                    int minHeight = StringeeConstant.VIDEO_NORMAL_MIN_HEIGHT;
                    switch (quality) {
                        case StringeeConstant.VIDEO_QUALITY_NORMAL:
                            minWidth = StringeeConstant.VIDEO_NORMAL_MIN_WIDTH;
                            minHeight = StringeeConstant.VIDEO_NORMAL_MIN_HEIGHT;
                            break;
                        case StringeeConstant.VIDEO_QUALITY_HD:
                            minWidth = VIDEO_HD_MIN_WIDTH;
                            minHeight = VIDEO_HD_MIN_HEIGHT;
                            break;
                        case StringeeConstant.VIDEO_QUALITY_FULL_HD:
                            minWidth = StringeeConstant.VIDEO_FULL_HD_MIN_WIDTH;
                            minHeight = StringeeConstant.VIDEO_FULL_HD_MIN_HEIGHT;
                            break;
                    }
                    if (isCameraAvailable(mContext)) {
                        videoCapturer = createVideoCapturer();
                        Log.e("Stringee", "VIETTEL_DEBUG: 05");
                        videoSource = peerConnectionFactory.createVideoSource(videoCapturer.isScreencast());
                        SurfaceTextureHelper surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", localContext);
                        videoCapturer.initialize(surfaceTextureHelper, mContext.getApplicationContext(), videoSource.getCapturerObserver());
                        isInitVideoCapture = true;
                        videoCapturer.startCapture(minWidth, minHeight, 30);
                        localVideoTrack = peerConnectionFactory.createVideoTrack(VIDEO_TRACK_ID, videoSource);
                        localVideoTrack.setEnabled(hasVideo);
                        mediaStream.addTrack(localVideoTrack);
                        Log.e("Stringee", "VIETTEL_DEBUG: 06");
                    } else {
                        Log.e("Stringee", "VIETTEL_DEBUG: no camera");
                    }
                }

                peerConnection.addStream(mediaStream);
                Log.e("Stringee", "VIETTEL_DEBUG: 07");
                if (listener != null) {
                    StringeeStream stringeeStream = new StringeeStream(eglBase.getEglBaseContext());
                    stringeeStream.setLocal(true);
                    stringeeStream.setMediaStream(mediaStream);
                    listener.onLocalStreamCreated(stringeeStream);
                    Log.e("Stringee", "VIETTEL_DEBUG: 08");
                }

                if (isCaller) {
                    // Create SDP constraints.
                    sdpMediaConstraints = new MediaConstraints();
                    sdpMediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"));
                    if (!isCallOut) {
                        sdpMediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo",
                                "true"));
                    } else {
                        sdpMediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo",
                                "false"));
                    }
                    Log.e("Stringee", "VIETTEL_DEBUG: 09");
                    peerConnection.createOffer(createSdpObserver, sdpMediaConstraints);
                    addSignalingStatistics(new SignalingStatisticElement("createOffer", sdpMediaConstraints.toString()));
                    Log.e("Stringee", "VIETTEL_DEBUG: 10");
                } else {
                    try {
                        String sdpType = sdpObject.getString("type");
                        String sdp = sdpObject.getString("sdp");

                        StringeeSessionDescription.Type type = StringeeSessionDescription.Type.OFFER;
                        if (sdpType.equals("offer") || sdpType.equals("0")) {
                            type = StringeeSessionDescription.Type.OFFER;
                            Log.e("Stringee", "+++++++++++++++++++++++ sdp OFFER received");
                        } else if (sdpType.equals("answer") || sdpType.equals("2")) {
                            Log.e("Stringee", "+++++++++++++++++++++++ sdp ANSWER received");
                            type = StringeeSessionDescription.Type.ANSWER;
                        } else if (sdpType.equals("pranswer") || sdpType.equals("1")) {
                            type = StringeeSessionDescription.Type.PRANSWER;
                        }
                        StringeeSessionDescription sessionDescription = new StringeeSessionDescription(type, sdp);
                        setRemoteDescription(sessionDescription);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                Log.e("Stringee", "startCall end execute");
            }
        });
    }

    public void restartICE() {
        Log.i(TAG, "---------restartICE");
        executor.execute(new Runnable() {
            @Override
            public void run() {
                addSignalingStatistics(new SignalingStatisticElement("restartICE", ""));
                sdpMediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("IceRestart", "true"));
                peerConnection.createOffer(createSdpObserver, sdpMediaConstraints);
                addSignalingStatistics(new SignalingStatisticElement("createOffer", sdpMediaConstraints.toString()));
            }
        });
    }

    public void stopCall() {
        if (!callStarted) {
            return;
        }
        callStarted = false;
        /*if (ENABLE_UPLOAD_CALL_REPORT) {
            Log.d("webrtcdump", webrtcInternalDumpObj.toString());
            Log.d("CalculateMOS", calculateMOS(webrtcInternalDumpObj.toString()) + "");
        }*/
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (peerConnection != null) {
                    peerConnection.dispose();
                    peerConnection = null;
                }
                if (audioSource != null) {
                    audioSource.dispose();
                    audioSource = null;
                }

                if (videoCapturer != null) {
                    try {
                        videoCapturer.stopCapture();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    videoCapturer.dispose();
                    videoCapturer = null;
                    isInitVideoCapture = false;
                }

                if (videoSource != null) {
                    videoSource.dispose();
                    videoSource = null;
                }
                //Closing peer connection factory.
                if (peerConnectionFactory != null) {
                    peerConnectionFactory.dispose();
                    peerConnectionFactory = null;
                }
                if (eglBase != null) {
                    eglBase.release();
                    eglBase = null;
                }
                createSdpObserver = null;
                setSdpSent(false);
            }
        });
    }

    StringeeSessionDescription mSessionDescription;

    //TODO addlog
    public void setRemoteDescription(final StringeeSessionDescription sessionDescription) {
        mSessionDescription = sessionDescription;
        boolean setOffer = true;
        final SessionDescription.Type type;
        if (sessionDescription.type == StringeeSessionDescription.Type.OFFER) {
            type = SessionDescription.Type.OFFER;
            setOffer = true;
        } else if (sessionDescription.type == StringeeSessionDescription.Type.PRANSWER) {
            type = SessionDescription.Type.PRANSWER;
            setOffer = false;
        } else if (sessionDescription.type == StringeeSessionDescription.Type.ANSWER) {
            type = SessionDescription.Type.ANSWER;
            setOffer = false;
        } else {
            type = SessionDescription.Type.OFFER;
        }

        final StringeeSetSdpObserver setSdpObserver = new StringeeSetSdpObserver(false, setOffer, this);
        executor.execute(new Runnable() {
            @Override
            public void run() {

                SessionDescription webrtcSdp = new SessionDescription(type, sessionDescription.description);
                Log.e("Stringee", "...................................... setRemoteDescription");
                peerConnection.setRemoteDescription(setSdpObserver, webrtcSdp);

                addSignalingStatistics(new SignalingStatisticElement("setRemoteDescription", webrtcSdp.description));
            }
        });
    }


    //TODO addlog
    public void addIceCandidate(final StringeeIceCandidate iceCandidate, final boolean isCallout) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                //xem la relay hay host
                String[] sdpA = iceCandidate.sdp.split(" ");

                //final String[] lines = sdpDescription.split("\r\n");
                String transport = sdpA[2];
                String candType = sdpA[7];

                IceCandidate webrtcIceCandidate = new IceCandidate(iceCandidate.sdpMid, iceCandidate.sdpMLineIndex,
                        iceCandidate.sdp);
                if (isCallout) {
                    peerConnection.addIceCandidate(webrtcIceCandidate);
                } else {
                    //peerConnection.addIceCandidate(webrtcIceCandidate);
                    if (transport.equals("udp") && candType.equals("relay")) {
                        Log.e("Stringee", "add luon ice candidate RELAY +++++++++++++++++++++++++: " + iceCandidate
                                .sdp);
                        peerConnection.addIceCandidate(webrtcIceCandidate);
                    }/* else if (iceConnected) {
                        Log.e("Stringee", "add luon vi da connected +++++++++++++++++++++++++: " + iceCandidate.sdp);
                        peerConnection.addIceCandidate(webrtcIceCandidate);
                    } else {
                        Log.e("Stringee", "CHUA add ice candidate RELAY, cho iceconnected +++++++++++++++++++++++++:
                        " + iceCandidate.sdp);
                        queueIceCandidate.add(iceCandidate);
                    }*/
                }
                addSignalingStatistics(new SignalingStatisticElement("addIceCandidate", webrtcIceCandidate.toString()));
            }
        });
    }

    private LinkedList<PeerConnection.IceServer> createIceServers() {
        LinkedList<PeerConnection.IceServer> list = new LinkedList<>();

        for (StringeeIceServer ice : iceServers) {
            PeerConnection.IceServer iceServer1 = new PeerConnection.IceServer(ice.uri, ice.username, ice.password);
            list.add(iceServer1);
        }

        return list;
    }

    public ScheduledExecutorService getExecutor() {
        return executor;
    }

    public List<StringeeIceServer> getIceServers() {
        return iceServers;
    }

    public void setIceServers(LinkedList<StringeeIceServer> iceServers) {
        this.iceServers = iceServers;
    }

    public void setListener(StringeeCallListener listener) {
        this.listener = listener;
    }

    public boolean isCallStarted() {
        return callStarted;
    }

    public void setCallStarted(boolean callStarted) {
        this.callStarted = callStarted;
    }

    public void setMicrophoneMute(final boolean isMute) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (localAudioTrack != null) {
                    localAudioTrack.setEnabled(!isMute);
                }
            }
        });
    }

    public void getStats(final AudioStatsListener statsListener) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                //Log.d("DEBUG","Call_get_statistics: " + System.currentTimeMillis()/1000);
//                peerConnection.getStats(new RTCStatsCollectorCallback() {
//                    @Override
//                    public void onStatsDelivered(RTCStatsReport rtcStatsReport) {
//
//                    }
//                });

                // use for report to server
                peerConnection.getStats(new StatsObserver() {
                    @Override
                    public void onComplete(StatsReport[] reports) {
                        processReportStats(reports, statsListener);
                    }
                }, null);
                peerConnection.getStats(new RTCStatsCollectorCallback() {
                    @Override
                    public void onStatsDelivered(RTCStatsReport rtcStatsReport) {
                        Map<String, RTCStats> map = rtcStatsReport.getStatsMap();
                        RTCStats rtcStats = map.get("RTCTransport_audio_1");
                        Map<String, Object> members = rtcStats.getMembers();
                        StringeeStream.StringeeAudioStats audioStats = new StringeeStream.StringeeAudioStats();
                        BigInteger bytesSent = (BigInteger) members.get("bytesSent");
                        audioStats.audioBytesSent = bytesSent.intValue();
                        BigInteger bytesReceived = (BigInteger) members.get("bytesReceived");
                        audioStats.audioBytesReceived = bytesReceived.intValue();
                        audioStats.timeStamp = System.currentTimeMillis();
                        statsListener.onAudioStats(audioStats);
                    }
                });
            }
        });
    }

    private void processReportStats(StatsReport[] reports, AudioStatsListener statsListener) {
//        String audioReceived = "0";
//        String videoReceived = "0";
//        String audioSend = "0";
//        String videoSend = "0";
        for (StatsReport report : reports) {
            if (ENABLE_UPLOAD_CALL_REPORT) {
                createDumpReport(report);
                Log.d("createDumpReport", report.toString());
            }
//            if (report.type.equals("ssrc") && report.id.contains("ssrc") && report.id.contains("send")) {// send
//                Map<String, String> reportMap = getReportMap(report);
//                String audioInputLevel = reportMap.get("audioInputLevel");
//                String frameWidthSent = reportMap.get("googFrameWidthSent");
//                if (audioInputLevel != null) {
//                    audioSend = reportMap.get("bytesSent");
//                } else if (frameWidthSent != null) {
//                    videoSend = reportMap.get("bytesSent");
//                }
//            }//rec
//            if (report.type.equals("ssrc") && report.id.contains("ssrc") && report.id.contains("recv")) {
//                Map<String, String> reportMap = getReportMap(report);
//                String audioOutputLevel = reportMap.get("audioOutputLevel");
//                String frameWidthReceived = reportMap.get("googFrameWidthReceived");
//                if (audioOutputLevel != null) {
//                    audioReceived = reportMap.get("bytesReceived");
//                } else if (frameWidthReceived != null) {
//                    videoReceived = reportMap.get("bytesReceived");
//                }
//            }
        }
//        StringeeStream.StringeeAudioStats audioStats = new StringeeStream.StringeeAudioStats(audioSend, videoSend,
//                audioReceived, videoReceived);
//        audioStats.timeStamp = System.currentTimeMillis();
//        statsListener.onAudioStats(audioStats);
    }

    private Map<String, String> getReportMap(StatsReport report) {
        Map<String, String> reportMap = new HashMap<>();
        for (StatsReport.Value value : report.values) {
            reportMap.put(value.name, value.value);
        }
        return reportMap;
    }

    public void enableVideo(final boolean isEnable) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (localVideoTrack != null) {
                    localVideoTrack.setEnabled(isEnable);
                }
            }
        });
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(mContext);
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer = null;
        if (useCamera2()) {
            videoCapturer = createCameraCapturer(new Camera2Enumerator(mContext));
        } else {
            videoCapturer = createCameraCapturer(new Camera1Enumerator(true));
        }
        return videoCapturer;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Log.i(TAG, "Front facing camera not found");
        notFoundFrontCam = true;
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    public SessionDescription getLocalDescription() {
        return localDescription;
    }

    public void setLocalDescription(SessionDescription localDescription) {
        this.localDescription = localDescription;
    }

    public boolean isSdpSent() {
        return sdpSent;
    }

    public void setSdpSent(boolean sdpSent) {
        this.sdpSent = sdpSent;
    }

    public String getPreferedAudioCodec() {
        return preferedAudioCodec;
    }

    public void setPreferedAudioCodec(String preferedAudioCodec) {
        this.preferedAudioCodec = preferedAudioCodec;
    }

    public String getPreferedVideoCodec() {
        return preferedVideoCodec;
    }

    public void setPreferedVideoCodec(String preferedVideoCodec) {
        this.preferedVideoCodec = preferedVideoCodec;
    }

    public void switchCamera(CameraVideoCapturer.CameraSwitchHandler handler) {
        if (videoCapturer instanceof CameraVideoCapturer) {
            CameraVideoCapturer cameraVideoCapturer = (CameraVideoCapturer) videoCapturer;
            cameraVideoCapturer.switchCamera(handler);
        }
    }

    public void setLocalDescription(final SdpObserver observer, final SessionDescription sessionDescription) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                peerConnection.setLocalDescription(observer, sessionDescription);

                addSignalingStatistics(new SignalingStatisticElement("setLocalDescription", sessionDescription.toString()));
            }
        });
    }

    public void setRemoteDescription(final SdpObserver observer, final SessionDescription sessionDescription) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                peerConnection.setRemoteDescription(observer, sessionDescription);
            }
        });
    }

    public void createAnswer(final SdpObserver observer, final MediaConstraints mediaConstraints) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                peerConnection.createAnswer(observer, mediaConstraints);
                addSignalingStatistics(new SignalingStatisticElement("createAnswer", mediaConstraints.toString()));
            }
        });
    }

    public void renderStream(final StringeeStream stream, boolean zOverlay) {
        if (stream == null) {
            return;
        }
        MediaStream mediaStream = stream.getMediaStream();
        if (mediaStream != null && mediaStream.videoTracks.size() > 0) {
            SurfaceViewRenderer renderer = stream.getSurfaceViewRenderer(mContext.getApplicationContext());
            VideoTrack videoTrack = mediaStream.videoTracks.get(0);
            if (renderer != null) {
                videoTrack.removeSink(renderer);
//                renderer.release();
            }
//            else {
//                renderer = new SurfaceViewRenderer(mContext.getApplicationContext());
//                renderer.init(eglBase.getEglBaseContext(), null);
//                renderer.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
//            }
            if (!stream.isLocal()) {
                renderer.setMirror(false);
//                renderer.init(remoteContext, null);
                renderer.setEnableHardwareScaler(false /* enabled */);

            } else {
//                renderer.init(localContext, null);
                renderer.setEnableHardwareScaler(true /* enabled */);
                if (notFoundFrontCam)
                    renderer.setMirror(false);
                else
                    renderer.setMirror(true);
            }
            renderer.setZOrderMediaOverlay(zOverlay);
            videoTrack.addSink(renderer);
            stream.setRenderer(renderer);
        }
    }

    public boolean isCameraAvailable(Context context) {
        PackageManager pm = context.getPackageManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                return true;
            }
        } else {
            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                return true;
            }
        }
        return false;
    }


    public void removeIceCandidate(final StringeeIceCandidate[] iceCandidates, final boolean isCallout) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                //xem la relay hay host
                IceCandidate[] iceCandidates1 = new IceCandidate[iceCandidates.length];
                for (int i = 0; i < iceCandidates.length; i++) {
                    StringeeIceCandidate ice = iceCandidates[i];
                    IceCandidate webrtcIceCandidate = new IceCandidate(ice.sdpMid, ice.sdpMLineIndex,
                            ice.sdp);
                    iceCandidates1[i] = webrtcIceCandidate;
                }
                Log.i(TAG, "removeIceCandidate: " + iceCandidates1.length);
                peerConnection.removeIceCandidates(iceCandidates1);
            }
        });
    }

    /*
     *
     * HERE IS REGION FOR STATISTIC REPORT
     * */

    private JsonObject webrtcInternalDumpObj = null;
    private Map<String, Chart> mapChart = null;

    private String createDumpReport(StatsReport report) {
        try {
            //stats obj
            JsonObject peerConnectionsObj = (JsonObject) webrtcInternalDumpObj.get("PeerConnections");
            JsonObject pidObj = (JsonObject) peerConnectionsObj.get("pid");
            JsonObject statsObj = (JsonObject) pidObj.get("stats");
            List<Pair<String, Chart>> listKeyAndChartObj = createKeyAndChartObj(report);

            for (Pair<String, Chart> item : listKeyAndChartObj) {
                Chart c = mapChart.get(item.first); // You get chart object from key, and check if chart obj exist or not
                if (c == null) {  // if not exist -> create new one and put to map chart
                    c = item.second;
                    mapChart.put(item.first, c);
                } else {
                    c.addAll(item.second.getValues()); // Next, you add new value to object
                }
                c.setEndTime(item.second.getEndTime());
                statsObj.add(item.first, c.toObject()); // Final, you update statsObj
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return webrtcInternalDumpObj.toString();
    }

    private List<Pair<String, Chart>> createKeyAndChartObj(StatsReport report) {
        List<Pair<String, Chart>> pairList = new ArrayList<>();
        if (!report.id.isEmpty()) {
            Map<String, String> reportMap = getReportMap(report);
            Iterator it = reportMap.entrySet().iterator();

            String mediaType = null;
            if (report.id.contains("ssrc")) {
                String str = Arrays.toString(report.values);
                if (str.contains("mediaType: video")) {
                    mediaType = "video";
                } else if (str.contains("mediaType: audio")) {
                    mediaType = "audio";
                }
            }

            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String key = report.id + "-" + pair.getKey();
                Chart chartObj = new Chart();

                String str = convertTime((long) report.timestamp);
                chartObj.setStartTime(str);
                chartObj.setEndTime(str);

                JsonPrimitive element;
                if (isNumeric((String) pair.getValue())) {
                    if (isInteger(pair.getValue().toString())) {
                        element = new JsonPrimitive((Integer.parseInt(pair.getValue().toString())));
                    } else {
                        element = new JsonPrimitive(Double.parseDouble(pair.getValue().toString()));
                    }
                } else {
                    element = new JsonPrimitive((pair.getValue().toString()));
                }

                chartObj.addElement(element);
                chartObj.setMediaType(mediaType);
                pairList.add(new Pair<>(key, chartObj));
            }
        }
        return pairList;
    }

    private String convertTime(long time) {
        Timestamp timestamp = new Timestamp(time);
        Date date = new Date(timestamp.getTime());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        //String result = format.format(date);
        //System.out.println(result);
        return format.format(date);
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private JsonObject createDumpObject() {
        JsonObject webrtcInternalDumpObj = new JsonObject();
        try {
            // getUserMedia
            JsonArray getUserMediaArray = new JsonArray();
            JsonObject getUserMediaObj = new JsonObject();
            getUserMediaObj.addProperty("audio", "audio");
            getUserMediaObj.addProperty("origin", "origin");
            getUserMediaObj.addProperty("pid", "pid");
            getUserMediaObj.addProperty("rid", "rid");
            getUserMediaObj.addProperty("video", "video");
            getUserMediaArray.add(getUserMediaObj);
            webrtcInternalDumpObj.add("getUserMedia", getUserMediaArray);

            // PeerConnections
            JsonObject peerConnectionsObj = new JsonObject();
            JsonObject pidObj = new JsonObject();
            pidObj.addProperty("constraints", "constraints");
            pidObj.addProperty("rtcConfiguration", "rtcConfiguration");


            //stats obj
            JsonObject statsObj = new JsonObject();
            pidObj.add("stats", statsObj);
            pidObj.add("updateLog", new JsonArray());
            pidObj.addProperty("url", "https://appr.tc/");
            peerConnectionsObj.add("pid", pidObj);
            webrtcInternalDumpObj.add("PeerConnections", peerConnectionsObj);
            // UserAgent
            webrtcInternalDumpObj.addProperty("UserAgent", "UserAgent");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return webrtcInternalDumpObj;
    }

    public static double findAverageWithoutUsingStream(int[] array) {
        int sum = findSumWithoutUsingStream(array);
        return (double) sum / array.length;
    }

    public static int findSumWithoutUsingStream(int[] array) {
        int sum = 0;
        for (int value : array) {
            sum += value;
        }
        return sum;
    }


    private double average(String data, String key) {
        double average = -1.0;
        try {
            String array = parserData(data, key);
            String[] items = new String[]{};
            if (array != null) {
                items = array.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
            }

            int[] results = new int[items.length];
            for (int i = 0; i < items.length; i++) {
                try {
                    results[i] = Integer.parseInt(items[i]);
                } catch (NumberFormatException nfe) {
                    //NOTE: write something here if you need to recover from formatting errors
                }
                ;
            }

            average = findAverageWithoutUsingStream(results);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return average;
    }

    public int maxValue(int array[]) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
        return Collections.max(list);
    }

    private double calculatePacketLoss(String data, String keyPacketLoss, String keyTotalPacket) {
        double packetLossPercent = -1;

        try {
            String arrPacketLoss = parserData(data, keyPacketLoss);

            String[] items = arrPacketLoss.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
            int[] results = new int[items.length];
            for (int i = 0; i < items.length; i++) {
                try {
                    results[i] = Integer.parseInt(items[i]);
                } catch (NumberFormatException nfe) {
                    //NOTE: write something here if you need to recover from formatting errors
                }
                ;
            }

            int highestPacketLossReport = maxValue(results);

            String arrSendPacket = parserData(data, keyTotalPacket);
            String[] elements = arrSendPacket.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
            int[] resultSendPackets = new int[elements.length];
            for (int i = 0; i < elements.length; i++) {
                try {
                    resultSendPackets[i] = Integer.parseInt(elements[i]);
                } catch (NumberFormatException nfe) {
                    //NOTE: write something here if you need to recover from formatting errors
                }
                ;
            }

            int totalPacketSent = maxValue(resultSendPackets);
            packetLossPercent = ((double) highestPacketLossReport / totalPacketSent) * 100;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return packetLossPercent;
    }


    private double calculateMOS(String data) {
        double averageLatency;
        double jitterAverage;
        double packetLoss;
        double R;
        double MOS = -1.0;
        try {
            averageLatency = average(data, "_send-googRtt");
            jitterAverage = average(data, "_send-googJitterReceived");
            packetLoss = calculatePacketLoss(data, "_send-packetsLost", "_send-packetsSent");
            Log.d("CalculateMOS_PacketLost", packetLoss + "");

            if (averageLatency <= 0 || jitterAverage < 0 || packetLoss < 0) {
                return MOS;
            }

            double effectiveLatency = (averageLatency + jitterAverage * 2 + 10);
            if (effectiveLatency < 160) {
                R = 93.2 - (effectiveLatency / 40);
            } else {
                R = 93.2 - (effectiveLatency - 120) / 10;
            }
            R = R - (packetLoss * 2.5);
            MOS = (1 + (0.035) * R + (.000007) * R * (R - 60) * (100 - R));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return MOS;
    }


    public String parserData(String data, String key) {

        String result = null;
        try {
            /* STEP 1: READ DATA FILE */
//      File file = new File(PATH);
//      String data = FileUtils.readFileToString(file);

            /* STEP 2: PARSER JSON */
            JsonObject parent = new JsonParser().parse(data).getAsJsonObject();

            JsonObject peerJsonObj = find(parent, "PeerConnections");
            if (peerJsonObj != null) {
                JsonObject peerIdJsonObj = first(peerJsonObj);
                if (peerIdJsonObj != null) {
                    JsonObject statsJsonObj = find(peerIdJsonObj, "stats");
                    if (statsJsonObj != null) {

                        List<JsonObject> listChartObj = contains(statsJsonObj, key, true);

                        for (JsonObject item : listChartObj) {
                            String str = item.get("mediaType").getAsString();
                            if (item.get("mediaType").getAsString().equals("audio")) {
                                result = item.get("values").getAsString();
                                Log.d("parserData", key + ": " + result);
                                break;
                            }
                        }


//                        if (googActualEncBitrateJsonObject != null) {
//                            print(googActualEncBitrateJsonObject);
//                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private void print(JsonObject googActualEncBitrateJsonObject) {
        String startTime = googActualEncBitrateJsonObject.get("startTime").getAsString();
        String endTime = googActualEncBitrateJsonObject.get("endTime").getAsString();
        String values = googActualEncBitrateJsonObject.get("values").getAsString();
        System.out.println("*-googActualEncBitrate" + "|" + startTime + "|" + endTime + "|" + values);
    }

    private JsonObject find(JsonObject parent, String key) {
        for (java.util.Map.Entry<String, JsonElement> entry : parent.entrySet()) {
            if (entry.getValue().isJsonObject()) {
                if (key.equals(entry.getKey())) {
                    return entry.getValue().getAsJsonObject();
                }
            }
        }
        return null;
    }

    private JsonObject first(JsonObject parent) {
        for (java.util.Map.Entry<String, JsonElement> entry : parent.entrySet()) {
            if (entry.getValue().isJsonObject()) {
                return entry.getValue().getAsJsonObject();
            }
        }
        return null;
    }

    /*
     * isAudio = 0 -> get key for video
     * */
//    private List<Pair<String,JsonObject>> contains(JsonObject parent, String key, boolean isAudio) {
//        List<Pair<String,JsonObject>> result = new ArrayList<>();
//        for (java.util.Map.Entry<String, JsonElement> entry : parent.entrySet()) {
//            if (entry.getValue().isJsonObject()) {
//                if (entry.getKey().contains(key)) {
//
//
//
//
//                    result.add(new Pair<>(entry.getKey(), entry.getValue().getAsJsonObject()));
//                }
//            }
//        }
//        return result;
//    }

    private List<JsonObject> contains(JsonObject parent, String key, boolean isAudio) {
        List<JsonObject> result = new ArrayList<>();
        for (java.util.Map.Entry<String, JsonElement> entry : parent.entrySet()) {
            if (entry.getValue().isJsonObject()) {
                if (entry.getKey().contains(key)) {
                    result.add(entry.getValue().getAsJsonObject());
                }
            }
        }
        return result;
    }


    public void addSignalingStatistics(SignalingStatisticElement signalingStatisticElement) {
        // get updateLogs obj
        if (webrtcInternalDumpObj != null) {
            JsonObject peerConnectionsObj = (JsonObject) webrtcInternalDumpObj.get("PeerConnections");
            JsonObject pidObj = (JsonObject) peerConnectionsObj.get("pid");
            JsonArray updateLogJsonArray = (JsonArray) pidObj.get("updateLog");
            updateLogJsonArray.add(signalingStatisticElement.toObject());
        }
    }

    public JsonObject getWebrtcInternalDumpObj() {
        return webrtcInternalDumpObj;
    }

    public EglBase.Context getEglBaseContext() {
        return eglBase.getEglBaseContext();
    }


    /**
     * fix tạm thời khi không lắng nghe được event stop capture.
     */
    public void onActivityResume() {
        executor.execute(() -> {
            if (localVideoTrack != null) {
                if (videoCapturer != null && isInitVideoCapture) {
                    try {
                        videoCapturer.stopCapture();
                        videoCapturer.startCapture(VIDEO_HD_MIN_WIDTH, VIDEO_HD_MIN_HEIGHT, 30);
                        localVideoTrack.setEnabled(localVideoTrack.enabled());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
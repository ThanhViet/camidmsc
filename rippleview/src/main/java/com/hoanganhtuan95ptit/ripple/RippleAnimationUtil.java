package com.hoanganhtuan95ptit.ripple;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class RippleAnimationUtil {

    /**
     * ẩn view
     *
     * @param view     view
     * @param duration thời gian thực hiện animation
     * @param delay    thời gian delay animation
     * @param listener bộ lắng nghe animation
     */
    public static void fadeInView(final View view, long duration, long delay, final AnimationListener listener) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
            view.setAlpha(0.0F);
            view.animate().alpha(1.0F).setStartDelay(delay).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (listener != null)
                        listener.onAnimationEnd(view);
                }
            });
        }
    }

    /**
     * hiện view view
     *
     * @param view     view
     * @param duration thời gian thực hiện animation
     * @param delay    thời gian delay animation
     * @param listener bộ lắng nghe animation
     */
    public static void fadeOutView(final View view, long duration, long delay, final AnimationListener listener) {
        if (view != null)
            view.animate().alpha(0.0F).setStartDelay(delay).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.INVISIBLE);
                    if (listener != null)
                        listener.onAnimationEnd(view);
                }
            });
    }

    /**
     * trình lắng nghe kết thức animation
     */
    public interface AnimationListener {
        /**
         * kết thúc animation
         *
         * @param view view animation
         */
        void onAnimationEnd(View view);
    }
}

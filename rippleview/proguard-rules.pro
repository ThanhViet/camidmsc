-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontoptimize
-dontpreverify
-verbose

-keep public class * extends android.view.View {
    public *;
}

-keep public class com.hoanganhtuan95ptit.ripple.RippleLayout {
    public *;
}

-keep public interface com.hoanganhtuan95ptit.ripple.RippleLayout$OnRippleListener {
    public *;
}

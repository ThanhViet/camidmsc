package org.jivesoftware.smack.sasl;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.StringUtils;

/**
 * Created by thaodv on 5/10/2015.
 */
public class NonSASLAuthInfo extends IQ {
    private String token;
    private String domainFile;
    private String domainMessage;
    private String domainOnMedia;
    private String publicRSAKey;
    private String domainServiceKeeng;
    private String domainMedia2Keeng;
    private String domainImageKeeng;
    private int vipInfo = -1;
    private int cbnv = -1;
    private int call = -1;
    private int SSL = -1;
    private int smsIn = -1;

    private String domainImage;
    private String domainFileV1;
    private String domainImageV1;
    private String domainOnMediaV1;
    private String domainMochaVideo;
    private String domainKeengMusic;
    private String domainKeengMovies;
    private String domainNetnews;
    private String domainTiin;
    private String domainKeengMusicSearch;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDomainFile() {
        return domainFile;
    }

    public void setDomainFile(String domainFile) {
        this.domainFile = domainFile;
    }

    public String getDomainMessage() {
        return domainMessage;
    }

    public void setDomainMessage(String domainMessage) {
        this.domainMessage = domainMessage;
    }

    public String getDomainOnMedia() {
        return domainOnMedia;
    }

    public void setDomainOnMedia(String domainOnMedia) {
        this.domainOnMedia = domainOnMedia;
    }

    public String getPublicRSAKey() {
        return publicRSAKey;
    }

    public void setPublicRSAKey(String publicRSAKey) {
        this.publicRSAKey = publicRSAKey;
    }

    public String getDomainServiceKeeng() {
        return domainServiceKeeng;
    }

    public void setDomainServiceKeeng(String domainServiceKeeng) {
        this.domainServiceKeeng = domainServiceKeeng;
    }

    public String getDomainMedia2Keeng() {
        return domainMedia2Keeng;
    }

    public void setDomainMedia2Keeng(String domainMedia2Keeng) {
        this.domainMedia2Keeng = domainMedia2Keeng;
    }

    public String getDomainImageKeeng() {
        return domainImageKeeng;
    }

    public void setDomainImageKeeng(String domainImageKeeng) {
        this.domainImageKeeng = domainImageKeeng;
    }

    public void setDomainOnMediaV1(String domainOnMediaV1) {
        this.domainOnMediaV1 = domainOnMediaV1;
    }

    public int getVipInfo() {
        return vipInfo;
    }

    public void setVipInfo(int vipInfo) {
        this.vipInfo = vipInfo;
    }

    public int getCBNV() {
        return cbnv;
    }

    public void setCBNV(int cbnv) {
        this.cbnv = cbnv;
    }

    public int getCall() {
        return call;
    }

    public void setCall(int call) {
        this.call = call;
    }

    public int getSSL() {
        return SSL;
    }

    public void setSSL(int ssl) {
        this.SSL = ssl;
    }

    public int getSmsIn() {
        return smsIn;
    }

    public void setSmsIn(int smsIn) {
        this.smsIn = smsIn;
    }

    public void setDomainImage(String domainImage) {
        this.domainImage = domainImage;
    }

    public void setDomainFileV1(String domainFileV1) {
        this.domainFileV1 = domainFileV1;
    }

    public void setDomainImageV1(String domainImageV1) {
        this.domainImageV1 = domainImageV1;
    }

    public void setDomainMochaVideo(String domainMochaVideo) {
        this.domainMochaVideo = domainMochaVideo;
    }

    public void setDomainKeengMusic(String domainKeengMusic) {
        this.domainKeengMusic = domainKeengMusic;
    }

    public void setDomainKeengMovies(String domainKeengMovies) {
        this.domainKeengMovies = domainKeengMovies;
    }

    public void setDomainNetnews(String domainNetnews) {
        this.domainNetnews = domainNetnews;
    }

    public void setDomainTiin(String domainTiin) {
        this.domainTiin = domainTiin;
    }

    public void setDomainKeengMusicSearch(String domainKeengMusicSearch) {
        this.domainKeengMusicSearch = domainKeengMusicSearch;
    }

    @Override
    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<auth_info>");
        if (token != null) {
            buf.append("<token>").append(StringUtils.escapeForXML(token)).append("</token>");
        }
        if (domainFile != null) {
            buf.append("<domain_file>").append(StringUtils.escapeForXML(domainFile)).append("</domain_file>");
        }
        if (domainMessage != null) {
            buf.append("<domain_msg>").append(StringUtils.escapeForXML(domainMessage)).append("</domain_msg>");
        }
        if (domainOnMedia != null) {
            buf.append("<domain_on_media>").append(StringUtils.escapeForXML(domainOnMedia)).append("</domain_on_media>");
        }
        if (domainServiceKeeng != null) {
            buf.append("<kservice>").append(StringUtils.escapeForXML(domainServiceKeeng)).append("</kservice>");
        }
        if (domainMedia2Keeng != null) {
            buf.append("<kmedia>").append(StringUtils.escapeForXML(domainMedia2Keeng)).append("</kmedia>");
        }
        if (domainImageKeeng != null) {
            buf.append("<kimage>").append(StringUtils.escapeForXML(domainImageKeeng)).append("</kimage>");
        }

        if (domainImage != null)
            buf.append(" domain_img=\"").append(StringUtils.escapeForXML(domainImage)).append("\"");
        if (domainFileV1 != null)
            buf.append(" domain_file_v1=\"").append(StringUtils.escapeForXML(domainFileV1)).append("\"");
        if (domainImageV1 != null)
            buf.append(" domain_img_v1=\"").append(StringUtils.escapeForXML(domainImageV1)).append("\"");
        if (domainOnMediaV1 != null)
            buf.append(" domain_on_media_v1=\"").append(StringUtils.escapeForXML(domainOnMediaV1)).append("\"");
        if (domainMochaVideo != null)
            buf.append(" domain_mcvideo=\"").append(StringUtils.escapeForXML(domainMochaVideo)).append("\"");
        if (domainKeengMusic != null)
            buf.append(" domain_kmusic=\"").append(StringUtils.escapeForXML(domainKeengMusic)).append("\"");
        if (domainKeengMovies != null)
            buf.append(" domain_kmovies=\"").append(StringUtils.escapeForXML(domainKeengMovies)).append("\"");
        if (domainNetnews != null)
            buf.append(" domain_netnews=\"").append(StringUtils.escapeForXML(domainNetnews)).append("\"");
        if (domainTiin != null)
            buf.append(" domain_tiin=\"").append(StringUtils.escapeForXML(domainTiin)).append("\"");
        if (domainKeengMusicSearch != null)
            buf.append(" domain_kmusic_search=\"").append(StringUtils.escapeForXML(domainKeengMusicSearch)).append("\"");

        buf.append("<vip>").append(vipInfo).append("</vip>");
        buf.append("</auth_info>");
        return buf.toString();
    }
}

/**
 * $RCSfile$
 * $Revision: $
 * $Date: $
 * <p/>
 * Copyright 2003-2007 Jive Software.
 * <p/>
 * All rights reserved. Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jivesoftware.smack.sasl;

import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.sasl.RealmCallback;
import org.apache.harmony.javax.security.sasl.RealmChoiceCallback;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.Base64;
import org.jivesoftware.smack.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.measite.smack.Sasl;

/**
 * Base class for SASL mechanisms. Subclasses must implement these methods:
 * <ul>
 * <li>{@link #getName()} -- returns the common name of the SASL mechanism.</li>
 * </ul>
 * Subclasses will likely want to implement their own versions of these mthods:
 * <li>{@link #authenticate(String, String, String)} -- Initiate authentication
 * stanza using the deprecated method.</li> <li>
 * {@link #authenticate(String, String, CallbackHandler)} -- Initiate
 * authentication stanza using the CallbackHandler method.</li> <li>
 * {@link #challengeReceived(String)} -- Handle a challenge from the server.</li>
 * </ul>
 *
 * @author Jay Kline
 */
public abstract class SASLMechanism implements CallbackHandler {

    private SASLAuthentication saslAuthentication;
    protected SaslClient sc;
    protected String authenticationId;
    protected String password;
    protected String hostname;

    public SASLMechanism(SASLAuthentication saslAuthentication) {
        this.saslAuthentication = saslAuthentication;
    }

    /**
     * Builds and sends the <tt>auth</tt> stanza to the server. Note that this
     * method of authentication is not recommended, since it is very inflexable.
     * Use {@link #authenticate(String, String, CallbackHandler)} whenever
     * possible.
     *
     * @param username the username of the user being authenticated.
     * @param host     the hostname where the user account resides.
     * @param password the password for this account.
     * @throws IOException   If a network error occurs while authenticating.
     * @throws XMPPException If a protocol error occurs or the user is not authenticated.
     */
    public void authenticate(String username, String host, String password)
            throws IOException, XMPPException {
        // Since we were not provided with a CallbackHandler, we will use our
        // own with the given
        // information

        // Set the authenticationID as the username, since they must be the same
        // in this case.
        this.authenticationId = username;
        this.password = password;
        this.hostname = host;

        String[] mechanisms = {getName()};
        Map<String, String> props = new HashMap<String, String>();
        sc = Sasl.createSaslClient(mechanisms, username, "xmpp", host, props,
                this);
        authenticate();
    }

    /**
     * Builds and sends the <tt>auth</tt> stanza to the server. The callback
     * handler will handle any additional information, such as the
     * authentication ID or realm, if it is needed.
     *
     * @param username the username of the user being authenticated.
     * @param host     the hostname where the user account resides.
     * @param cbh      the CallbackHandler to obtain user information.
     * @throws IOException   If a network error occures while authenticating.
     * @throws XMPPException If a protocol error occurs or the user is not authenticated.
     */
    public void authenticate(String username, String host, CallbackHandler cbh)
            throws IOException, XMPPException {
        String[] mechanisms = {getName()};
        Map<String, String> props = new HashMap<String, String>();
        sc = Sasl.createSaslClient(mechanisms, username, "xmpp", host, props,
                cbh);
        authenticate();
    }

    protected void authenticate() throws IOException, XMPPException {
        String authenticationText = null;
        try {
            if (sc.hasInitialResponse()) {
                byte[] response = sc.evaluateChallenge(new byte[0]);
                authenticationText = Base64.encodeBytes(response,
                        Base64.DONT_BREAK_LINES);
            }
        } catch (SaslException e) {
            throw new XMPPException("SASL authentication failed", e);
        }

        // Send the authentication to the server
        getSASLAuthentication().send(
                new AuthMechanism(getName(), authenticationText));
    }

    /**
     * The server is challenging the SASL mechanism for the stanza he just sent.
     * Send a response to the server's challenge.
     *
     * @param challenge a base64 encoded string representing the challenge.
     * @throws IOException if an exception sending the response occurs.
     */
    public void challengeReceived(String challenge) throws IOException {
        byte response[];
        if (challenge != null) {
            response = sc.evaluateChallenge(Base64.decode(challenge));
        } else {
            response = sc.evaluateChallenge(new byte[0]);
        }

        Packet responseStanza;
        if (response == null) {
            responseStanza = new Response();
        } else {
            responseStanza = new Response(Base64.encodeBytes(response,
                    Base64.DONT_BREAK_LINES));
        }

        // Send the authentication to the server
        getSASLAuthentication().send(responseStanza);
    }

    /**
     * Returns the common name of the SASL mechanism. E.g.: PLAIN, DIGEST-MD5 or
     * GSSAPI.
     *
     * @return the common name of the SASL mechanism.
     */
    protected abstract String getName();

    protected SASLAuthentication getSASLAuthentication() {
        return saslAuthentication;
    }

    /**
     *
     */
    public void handle(Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            if (callbacks[i] instanceof NameCallback) {
                NameCallback ncb = (NameCallback) callbacks[i];
                ncb.setName(authenticationId);
            } else if (callbacks[i] instanceof PasswordCallback) {
                PasswordCallback pcb = (PasswordCallback) callbacks[i];
                pcb.setPassword(password.toCharArray());
            } else if (callbacks[i] instanceof RealmCallback) {
                RealmCallback rcb = (RealmCallback) callbacks[i];
                rcb.setText(hostname);
            } else if (callbacks[i] instanceof RealmChoiceCallback) {
                // unused
                // RealmChoiceCallback rccb = (RealmChoiceCallback)callbacks[i];
            } else {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }

    /**
     * Initiating SASL authentication by select a mechanism.
     */
    public class AuthMechanism extends Packet {
        final private String name;
        final private String authenticationText;

        public AuthMechanism(String name, String authenticationText) {
            if (name == null) {
                throw new NullPointerException(
                        "SASL mechanism name shouldn't be null.");
            }
            this.name = name;
            this.authenticationText = authenticationText;
        }

        public String toXML() {
            StringBuilder stanza = new StringBuilder();
            stanza.append("<auth mechanism=\"").append(name);
            stanza.append("\" xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (authenticationText != null
                    && authenticationText.trim().length() > 0) {
                stanza.append(authenticationText);
            }
            stanza.append("</auth>");
            return stanza.toString();
        }
    }

    /**
     * A SASL challenge stanza.
     */
    public static class Challenge extends Packet {
        final private String data;

        public Challenge(String data) {
            this.data = data;
        }

        public String toXML() {
            StringBuilder stanza = new StringBuilder();
            stanza.append("<challenge xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (data != null && data.trim().length() > 0) {
                stanza.append(data);
            }
            stanza.append("</challenge>");
            return stanza.toString();
        }
    }

    /**
     * A SASL response stanza.
     */
    public class Response extends Packet {
        final private String authenticationText;

        public Response() {
            authenticationText = null;
        }

        public Response(String authenticationText) {
            if (authenticationText == null
                    || authenticationText.trim().length() == 0) {
                this.authenticationText = null;
            } else {
                this.authenticationText = authenticationText;
            }
        }

        public String toXML() {
            StringBuilder stanza = new StringBuilder();
            stanza.append("<response xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (authenticationText != null) {
                stanza.append(authenticationText);
            }
            stanza.append("</response>");
            return stanza.toString();
        }
    }

    /**
     * A SASL success stanza.
     */
    public static class Success extends Packet {
        private String token;
        private String data;
        private String domainFile;
        private String domainMsg;
        private String domainOnMedia;

        private String domainImage;
        private String domainFileV1;
        private String domainImageV1;
        private String domainOnMediaV1;
        private String domainMochaVideo;
        private String domainKeengMusic;
        private String domainKeengMovies;
        private String domainNetnews;
        private String domainTiin;
        private String domainKeengMusicSearch;

        public void setDomainFile(String domainFile) {
            this.domainFile = domainFile;
        }

        public void setDomainMsg(String domainMsg) {
            this.domainMsg = domainMsg;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }

        public String getDomainFile() {
            return domainFile;
        }

        public String getDomainMsg() {
            return domainMsg;
        }

        public String getDomainOnMedia() {
            return domainOnMedia;
        }

        public Success() {

        }

        public Success(String data, String token, String domainFile, String domainMsg, String domainOnMedia) {
            this.data = data;
            this.token = token;
            this.domainFile = domainFile;
            this.domainMsg = domainMsg;
            this.domainOnMedia = domainOnMedia;
        }

        public void setDomainOnMedia(String domainOnMedia) {
            this.domainOnMedia = domainOnMedia;
        }

        public void setDomainImage(String domainImage) {
            this.domainImage = domainImage;
        }

        public void setDomainFileV1(String domainFileV1) {
            this.domainFileV1 = domainFileV1;
        }

        public void setDomainImageV1(String domainImageV1) {
            this.domainImageV1 = domainImageV1;
        }

        public void setDomainMochaVideo(String domainMochaVideo) {
            this.domainMochaVideo = domainMochaVideo;
        }

        public void setDomainKeengMusic(String domainKeengMusic) {
            this.domainKeengMusic = domainKeengMusic;
        }

        public void setDomainKeengMovies(String domainKeengMovies) {
            this.domainKeengMovies = domainKeengMovies;
        }

        public void setDomainNetnews(String domainNetnews) {
            this.domainNetnews = domainNetnews;
        }

        public void setDomainTiin(String domainTiin) {
            this.domainTiin = domainTiin;
        }

        public void setDomainKeengMusicSearch(String domainKeengMusicSearch) {
            this.domainKeengMusicSearch = domainKeengMusicSearch;
        }

        public void setDomainOnMediaV1(String domainOnMediaV1) {
            this.domainOnMediaV1 = domainOnMediaV1;
        }

        public String toXML() {
            StringBuilder stanza = new StringBuilder();
            stanza.append("<success xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\"");
            if (token != null)
                stanza.append(" token=\"").append(StringUtils.escapeForXML(token)).append("\"");
            if (domainFile != null)
                stanza.append(" domain_file=\"").append(StringUtils.escapeForXML(domainFile)).append("\"");
            if (domainMsg != null)
                stanza.append(" domain_msg=\"").append(StringUtils.escapeForXML(domainMsg)).append("\"");
            if (domainOnMedia != null)
                stanza.append(" domain_on_media=\"").append(StringUtils.escapeForXML(domainOnMedia)).append("\"");

            if (domainImage != null)
                stanza.append(" domain_img=\"").append(StringUtils.escapeForXML(domainImage)).append("\"");
            if (domainFileV1 != null)
                stanza.append(" domain_file_v1=\"").append(StringUtils.escapeForXML(domainFileV1)).append("\"");
            if (domainImageV1 != null)
                stanza.append(" domain_img_v1=\"").append(StringUtils.escapeForXML(domainImageV1)).append("\"");
            if (domainOnMediaV1 != null)
                stanza.append(" domain_on_media_v1=\"").append(StringUtils.escapeForXML(domainOnMediaV1)).append("\"");
            if (domainMochaVideo != null)
                stanza.append(" domain_mcvideo=\"").append(StringUtils.escapeForXML(domainMochaVideo)).append("\"");
            if (domainKeengMusic != null)
                stanza.append(" domain_kmusic=\"").append(StringUtils.escapeForXML(domainKeengMusic)).append("\"");
            if (domainKeengMovies != null)
                stanza.append(" domain_kmovies=\"").append(StringUtils.escapeForXML(domainKeengMovies)).append("\"");
            if (domainNetnews != null)
                stanza.append(" domain_netnews=\"").append(StringUtils.escapeForXML(domainNetnews)).append("\"");
            if (domainTiin != null)
                stanza.append(" domain_tiin=\"").append(StringUtils.escapeForXML(domainTiin)).append("\"");
            if (domainKeengMusicSearch != null)
                stanza.append(" domain_kmusic_search=\"").append(StringUtils.escapeForXML(domainKeengMusicSearch)).append("\"");

            stanza.append(">");
            if (data != null && data.trim().length() > 0)
                stanza.append(data);
            stanza.append("</success>");
            return stanza.toString();
        }
    }

    /**
     * A SASL failure stanza.
     */
    public static class Failure extends Packet {
        private final String condition;
        private boolean isLocked = false;
        private int lockedTime = 0;
        private int retries = -1;

        public Failure(String condition, boolean isLocked, int lockedTime, int retries) {
            this.condition = condition;
            this.isLocked = isLocked;
            this.lockedTime = lockedTime;
            this.setRetries(retries);
        }

        /**
         * Get the SASL related error condition.
         *
         * @return the SASL related error condition.
         */
        public String getCondition() {
            return condition;
        }

        public boolean isLocked() {
            return isLocked;
        }

        public int getLockedTime() {
            return lockedTime;
        }

        public int getRetries() {
            return retries;
        }

        public void setRetries(int retries) {
            this.retries = retries;
        }

        public String toXML() {
            StringBuilder stanza = new StringBuilder();
            stanza.append("<failure xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (condition != null && condition.trim().length() > 0) {
                stanza.append("<").append(condition).append("/>");
            }
            if (lockedTime > 0) {
                stanza.append("<locked time=\"" + lockedTime + "\"/>");
            }
            if (retries >= 0) {
                stanza.append("<retries>" + retries + "</retries>");
            }
            stanza.append("</failure>");
            return stanza.toString();
        }
    }
}

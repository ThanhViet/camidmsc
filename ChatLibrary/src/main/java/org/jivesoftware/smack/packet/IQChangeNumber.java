package org.jivesoftware.smack.packet;

import android.text.TextUtils;

import org.jivesoftware.smack.util.StringUtils;

/**
 * Created by thanhnt72 on 8/6/2018.
 */

public class IQChangeNumber extends IQ {

    private static final String NAME_SPACE = "mocha:iq:changenum";
    private int errorCode;
    private String newJid;
    private String currentJid;
    private String otp;


    public IQChangeNumber() {
    }

    @Override
    public String getChildElementXML() {
        return null;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int code) {
        this.errorCode = code;
    }

    public String getNewJid() {
        return newJid;
    }

    public void setNewJid(String newJid) {
        this.newJid = newJid;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getCurrentJid() {
        return currentJid;
    }

    public void setCurrentJid(String currentJid) {
        this.currentJid = currentJid;
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<iq");
        if (getPacketID() != null) {
            buf.append(" id=\"").append(getPacketID()).append("\"");
        }
        if (getTo() != null) {
            buf.append(" to=\"").append(StringUtils.escapeForXML(getTo()))
                    .append("\"");
        }
        if (getFrom() != null) {
            buf.append(" from=\"").append(StringUtils.escapeForXML(getFrom()))
                    .append("\"");
        }
        if (getType() != null) {
            buf.append(" type=\"").append(getType()).append("\"");
        }
        buf.append(">");
        // add event namespace
            buf.append("<query xmlns=\"").append(NAME_SPACE).append("\">");
        // add name of group
        if (errorCode!=0) {
            buf.append("<error>").append(errorCode).append("</error>");
        }
        if(!TextUtils.isEmpty(currentJid)){
            buf.append("<user>").append(currentJid).append("</user>");
        }
        if(!TextUtils.isEmpty(newJid)){
            buf.append("<newuser>").append(newJid).append("</newuser>");
        }
        if(!TextUtils.isEmpty(otp)){
            buf.append("<otp>").append(otp).append("</otp>");
        }
        buf.append("</query>");
        buf.append("</iq>");
        return buf.toString();
    }

}

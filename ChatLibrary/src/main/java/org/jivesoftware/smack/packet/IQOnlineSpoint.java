package org.jivesoftware.smack.packet;

import org.jivesoftware.smack.util.StringUtils;

public class IQOnlineSpoint extends IQ {

    public static final String NAME_SPACE = "mocha:iq:bonusspoint";
    private int errorCode;
    private int bonus;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int code) {
        this.errorCode = code;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public String getChildElementXML() {
        return null;
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<iq");
        if (getPacketID() != null) {
            buf.append(" id=\"").append(getPacketID()).append("\"");
        }
        if (getTo() != null) {
            buf.append(" to=\"").append(StringUtils.escapeForXML(getTo()))
                    .append("\"");
        }
        if (getFrom() != null) {
            buf.append(" from=\"").append(StringUtils.escapeForXML(getFrom()))
                    .append("\"");
        }
        if (getType() != null) {
            buf.append(" type=\"").append(getType()).append("\"");
        }
        buf.append(">");
        // add event namespace
        buf.append("<query xmlns=\"").append(NAME_SPACE).append("\">");
        // add name of group
        buf.append("</query>");
        buf.append("</iq>");
        return buf.toString();
    }
}

package org.jivesoftware.smack.packet;

import android.text.TextUtils;

import org.jivesoftware.smack.model.IceServer;
import org.jivesoftware.smack.util.StringUtils;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/14/2016.
 */

public class IQCall extends IQ {
    public static final String nameSpaceFree = "mocha:iq:initcall";
    public static final String nameSpaceCallOut = "mocha:iq:initcallout";
    public static final String nameSpaceCallViaFS = "mocha:iq:initcallv2";
    private String nameSpace;
    private String callSession = "";
    private String caller;
    private String callee;
    private String errorCode;
    private ArrayList<IceServer> iceServers;
    private String codecPrefs;
    private String codecVideoPrefs;
    private String virtual;
    private boolean isVideo = false;
    private int calloutGlobal;
    private int callout = 0;
    private String language;
    private String country;
    private String tOpr, fOpr;
    private int usingDesktop = -1;
    private String platform, revision, osVersion;


    private boolean enableRestartICE = false;
    private int iceTimeout;
    private long restartICEDelay;
    private long restartICEPeriod;
    private int restartICELoop;
    private long zeroBwEndCall;
    private long network2failedTime;
    private long timedis2recon;
    private long timeRestartBw;
    private long delayRestartOnFailed;

    private String bundlePolicy;
    private String rtcpMuxPolicy;
    private String iceTransportsType;

    private long fcallviafs;

    public IQCall(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getCallSession() {
        return callSession;
    }

    public void setCallSession(String callSession) {
        this.callSession = callSession;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getCallee() {
        return callee;
    }

    public void setCallee(String callee) {
        this.callee = callee;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ArrayList<IceServer> getIceServers() {
        return iceServers;
    }

    public void addIceServer(String user, String credential, String domain) {
        if (iceServers == null) {
            iceServers = new ArrayList<>();
        }
        IceServer iceServer = new IceServer(user, credential, domain);
        iceServers.add(iceServer);
    }

    public String getCodecPrefs() {
        return codecPrefs;
    }

    public void setCodecPrefs(String codecPrefs) {
        this.codecPrefs = codecPrefs;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public void setVirtual(String virtual) {
        this.virtual = virtual;
    }

    public boolean isEnableRestartICE() {
        return enableRestartICE;
    }

    public void setEnableRestartICE(boolean enableRestartICE) {
        this.enableRestartICE = enableRestartICE;
    }

    public int getIceTimeout() {
        return iceTimeout;
    }

    public void setIceTimeout(int iceTimeout) {
        this.iceTimeout = iceTimeout;
    }

    public long getRestartICEDelay() {
        return restartICEDelay;
    }

    public void setRestartICEDelay(long restartICEDelay) {
        this.restartICEDelay = restartICEDelay;
    }

    public long getRestartICEPeriod() {
        return restartICEPeriod;
    }

    public void setRestartICEPeriod(long restartICEPeriod) {
        this.restartICEPeriod = restartICEPeriod;
    }

    public int getRestartICELoop() {
        return restartICELoop;
    }

    public void setRestartICELoop(int restartICELoop) {
        this.restartICELoop = restartICELoop;
    }

    public long getZeroBwEndCall() {
        return zeroBwEndCall;
    }

    public void setZeroBwEndCall(long zeroBwEndCall) {
        this.zeroBwEndCall = zeroBwEndCall;
    }

    public long getNetwork2failedTime() {
        return network2failedTime;
    }

    public void setNetwork2failedTime(long network2failedTime) {
        this.network2failedTime = network2failedTime;
    }

    public long getTimedis2recon() {
        return timedis2recon;
    }

    public void setTimedis2recon(long timedis2recon) {
        this.timedis2recon = timedis2recon;
    }

    public long getTimeRestartBw() {
        return timeRestartBw;
    }

    public void setTimeRestartBw(long timeRestartBw) {
        this.timeRestartBw = timeRestartBw;
    }

    public long getDelayRestartOnFailed() {
        return delayRestartOnFailed;
    }

    public void setDelayRestartOnFailed(long delayRestartOnFailed) {
        this.delayRestartOnFailed = delayRestartOnFailed;
    }

    public String getBundlePolicy() {
        return bundlePolicy;
    }

    public void setBundlePolicy(String bundlePolicy) {
        this.bundlePolicy = bundlePolicy;
    }

    public String getRtcpMuxPolicy() {
        return rtcpMuxPolicy;
    }

    public void setRtcpMuxPolicy(String rtcpMuxPolicy) {
        this.rtcpMuxPolicy = rtcpMuxPolicy;
    }

    public String getIceTransportsType() {
        return iceTransportsType;
    }

    public void setIceTransportsType(String iceTransportsType) {
        this.iceTransportsType = iceTransportsType;
    }

    public long getFcallviafs() {
        return fcallviafs;
    }

    public void setFcallviafs(long fcallviafs) {
        this.fcallviafs = fcallviafs;
    }

    public int getCalloutGlobal() {
        return calloutGlobal;
    }

    public void setCalloutGlobal(int calloutGlobal) {
        this.calloutGlobal = calloutGlobal;
    }

    public int getCallout() {
        return callout;
    }

    public void setCallout(int callout) {
        this.callout = callout;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getToOpr() {
        return tOpr;
    }

    public void setToOpr(String tOpr) {
        this.tOpr = tOpr;
    }

    public String getFromOpr() {
        return fOpr;
    }

    public void setFromOpr(String fOpr) {
        this.fOpr = fOpr;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public int getUsingDesktop() {
        return usingDesktop;
    }

    public void setUsingDesktop(int usingDesktop) {
        this.usingDesktop = usingDesktop;
    }

    public String getCodecVideoPrefs() {
        return codecVideoPrefs;
    }

    public void setCodecVideoPrefs(String codecVideoPrefs) {
        this.codecVideoPrefs = codecVideoPrefs;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @Override
    public String getChildElementXML() {
        return null;
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<iq");
        if (getPacketID() != null) {
            buf.append(" id=\"").append(getPacketID()).append("\"");
        }
        if (getTo() != null) {
            buf.append(" to=\"").append(StringUtils.escapeForXML(getTo()))
                    .append("\"");
        }
        if (getFrom() != null) {
            buf.append(" from=\"").append(StringUtils.escapeForXML(getFrom()))
                    .append("\"");
        }
        if (getType() != null) {
            buf.append(" type=\"").append(getType()).append("\"");
        }
        buf.append(">");
        // add event namespace
        buf.append("<query xmlns=\"").append(nameSpace).append("\">");
        buf.append("<session>").append(callSession).append("</session>");
        if (!TextUtils.isEmpty(virtual)) {
            buf.append("<virtual>").append(virtual).append("</virtual>");
        }
        if (!TextUtils.isEmpty(caller)) {
            buf.append("<caller>").append(caller).append("</caller>");
        }
        if (!TextUtils.isEmpty(callee)) {
            buf.append("<callee>").append(callee).append("</callee>");
        }
        if (isVideo) {
            buf.append("<video_call/>");
        }
        if (iceServers != null && !iceServers.isEmpty()) {
            buf.append("<iceservers>");
            for (IceServer iceServer : iceServers) {
                buf.append(iceServer.toXml());
            }
            buf.append("</iceservers>");
        }
        if (!TextUtils.isEmpty(errorCode)) {
            buf.append("<error>").append(errorCode).append("</error>");
        }
        if (!TextUtils.isEmpty(codecPrefs)) {
            buf.append("<codecPrefs>").append(codecPrefs).append("</codecPrefs>");
        }
        if (calloutGlobal != -1) {
            buf.append("<callout_global>").append(calloutGlobal).append("</callout_global>");
        }
        if (callout != 0) {
            buf.append("<callout>").append(callout).append("</callout>");
        }
        if (!TextUtils.isEmpty(country)) {
            buf.append("<country>").append(country).append("</country>");
        }
        if (!TextUtils.isEmpty(language)) {
            buf.append("<language>").append(language).append("</language>");
        }
        if (!TextUtils.isEmpty(platform)) {
            buf.append("<platform>").append(platform).append("</platform>");
        }
        if (!TextUtils.isEmpty(revision)) {
            buf.append("<revision>").append(revision).append("</revision>");
        }
        if (!TextUtils.isEmpty(tOpr)) {
            buf.append("<t_opr>").append(tOpr).append("</t_opr>");
        }
        if (!TextUtils.isEmpty(fOpr)) {
            buf.append("<f_opr>").append(fOpr).append("</f_opr>");
        }
        if (usingDesktop != -1) {
            buf.append("<cdesktop>").append(usingDesktop).append("</cdesktop>");
        }
        if (!TextUtils.isEmpty(osVersion)) {
            buf.append("<os_version>").append(osVersion).append("</os_version>");
        }
        // end add query
        buf.append("</query>");
        buf.append("</iq>");
        return buf.toString();
    }

    public static boolean containsIQCall(String nameSpace) {
        return nameSpaceFree.equals(nameSpace) || nameSpaceCallOut.equals(nameSpace) || nameSpaceCallViaFS.equals(nameSpace);
    }
}

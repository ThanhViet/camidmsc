package com.viettel.util;

import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

public class PowerModeHelper {

    public static boolean isPowerSaveMode(Context context) {
        PowerManager powerManager = (PowerManager)
                context.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                && powerManager.isPowerSaveMode()) {
            return true;
        }
        return false;
    }
}

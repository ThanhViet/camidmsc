-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontoptimize
-dontpreverify
-verbose

-keep class com.hoanganhtuan95ptit.imageupload.** { public *; }
-keepclassmembers class com.hoanganhtuan95ptit.imageupload.** { public *; }
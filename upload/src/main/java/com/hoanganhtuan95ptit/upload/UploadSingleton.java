package com.hoanganhtuan95ptit.upload;

import java.util.HashMap;
import java.util.Map;

public class UploadSingleton {
    private static final UploadSingleton ourInstance = new UploadSingleton();

    public static UploadSingleton getInstance() {
        return ourInstance;
    }

    private Map<String, UploadProgress> uploadProgressMap;

    private UploadSingleton() {
        uploadProgressMap = new HashMap<>();
    }

    public void add(String key, UploadProgress listener) {
        uploadProgressMap.put(key, listener);
    }

    public void remove(String key) {
        uploadProgressMap.remove(key);
    }

    public UploadProgress get(String key) {
        return uploadProgressMap.get(key);
    }
}

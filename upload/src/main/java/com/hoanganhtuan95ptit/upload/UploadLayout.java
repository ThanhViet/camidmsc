package com.hoanganhtuan95ptit.upload;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.hoanganhtuan95ptit.upload.view.CircleProgress;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

public class UploadLayout extends RelativeLayout implements OnUploadProgressListener {

    private CircleProgress circleProgress;
    private ImageView imageView;
    private TextView tvError;

    private UploadProgress uploadProgress;
    private String url;

    public UploadLayout(Context context) {
        this(context, null);
    }

    public UploadLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UploadLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.layout_image_upload, this);

        circleProgress = findViewById(R.id.progress_upload);
        imageView = findViewById(R.id.iv_image);
        tvError = findViewById(R.id.tv_error);
    }

    public void bindData(String url) {
        this.url = url;
        Context context = getContext();
        if (context == null) return;
        Glide.with(getContext())
                .asBitmap()
                .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCornersTransformation((int) dp2px(context, 8), (int) dp2px(context, 1))))
                .transition(withCrossFade(500))
                .load(url)
                .into(imageView);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (url.startsWith("http")) {
            tvError.setVisibility(View.GONE);
            circleProgress.setVisibility(View.GONE);
        } else {
            uploadProgress = UploadSingleton.getInstance().get(url);
            if (uploadProgress == null) {
                if (url == null || url.isEmpty()) {
                    tvError.setVisibility(View.VISIBLE);
                    circleProgress.setVisibility(View.GONE);
                } else {
                    tvError.setVisibility(View.GONE);
                    circleProgress.setVisibility(View.GONE);
                }
            } else if (uploadProgress.getState() == UploadProgress.State.SUCCESS) {
                tvError.setVisibility(View.GONE);
                circleProgress.setVisibility(View.GONE);
            } else if (uploadProgress.getState() == UploadProgress.State.FAILURE) {
                tvError.setVisibility(View.VISIBLE);
                circleProgress.setVisibility(View.GONE);
            } else {
                uploadProgress.addOnUploadProgressListeners(this);
                circleProgress.setVisibility(View.VISIBLE);
                circleProgress.setProgress(uploadProgress.getProgress());
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (uploadProgress == null) return;
        uploadProgress.removeOnUploadProgressListeners(this);
        uploadProgress = null;
    }

    @Override
    public void onProgress(float progress) {
        if (uploadProgress == null || circleProgress == null) return;
        circleProgress.setProgress((int) progress);
    }

    @Override
    public void onStateChange(UploadProgress.State state) {
        if (state == UploadProgress.State.SUCCESS) {
            tvError.setVisibility(View.GONE);
            circleProgress.setVisibility(View.GONE);
        } else if (state == UploadProgress.State.FAILURE) {
            tvError.setVisibility(View.VISIBLE);
            circleProgress.setVisibility(View.GONE);
        }
    }

    public static float dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }
}

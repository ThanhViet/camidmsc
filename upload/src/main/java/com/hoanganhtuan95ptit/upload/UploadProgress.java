package com.hoanganhtuan95ptit.upload;

import java.util.concurrent.CopyOnWriteArrayList;

public class UploadProgress {
    private int mProgress = 20;
    private State mState = State.PROGRESS;

    private CopyOnWriteArrayList<OnUploadProgressListener> onUploadProgressListeners;

    public UploadProgress() {
        onUploadProgressListeners = new CopyOnWriteArrayList<>();
    }

    public int getProgress() {
        return mProgress;
    }

    public void setProgress(int progress) {
        if (progress < mProgress) return;
        mProgress = progress;
        for (OnUploadProgressListener onUploadProgressListener : onUploadProgressListeners) {
            if (onUploadProgressListener != null)
                onUploadProgressListener.onProgress(progress);
        }
    }

    public State getState() {
        return mState;
    }

    public void setState(State state) {
        mState = state;
        for (OnUploadProgressListener onUploadProgressListener : onUploadProgressListeners) {
            if (onUploadProgressListener != null)
                onUploadProgressListener.onStateChange(state);
        }
    }

    public void removeOnUploadProgressListeners(OnUploadProgressListener onUploadProgressListener) {
        onUploadProgressListeners.remove(onUploadProgressListener);
    }

    public void addOnUploadProgressListeners(OnUploadProgressListener onUploadProgressListener) {
        if (!onUploadProgressListeners.contains(onUploadProgressListener))
            onUploadProgressListeners.add(onUploadProgressListener);
    }

    public enum State {
        PROGRESS, SUCCESS, FAILURE
    }

}

package com.hoanganhtuan95ptit.upload;

public interface OnUploadProgressListener {

    void onProgress(float progress);

    void onStateChange(UploadProgress.State state);
}

package com.hoanganhtuan95ptit.upload.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

/**
 * Created by hoanganhtuan95ptit.
 */
public class CircleProgress extends View {

    private int progress = 0;

    private Paint strokePaint;
    private Paint solidPaint;
    private Paint progressPaint;
    private Paint textPaint;

    private int padding;
    private int textSize;

    private ObjectAnimator progressObjectAnimator;

    public CircleProgress(Context context) {
        this(context, null);
    }

    public CircleProgress(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        padding = (int) dp2px(context, 2);
        textSize = (int) dp2px(context, 11);
        initView();
    }

    private void initView() {
        solidPaint = new Paint();
        solidPaint.setAntiAlias(true);
        solidPaint.setStyle(Paint.Style.FILL);
        solidPaint.setColor(Color.parseColor("#000000"));
        solidPaint.setAlpha(50);

        strokePaint = new Paint();
        strokePaint.setAntiAlias(true);
        strokePaint.setStrokeWidth(padding);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setColor(Color.parseColor("#FFFFFFFF"));

        progressPaint = new Paint();
        progressPaint.setAntiAlias(true);
        progressPaint.setStrokeWidth(padding);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setStrokeCap(Paint.Cap.ROUND);
        progressPaint.setColor(Color.parseColor("#FF2D83CE"));

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(textSize);
        textPaint.setTypeface(Typeface.create(Typeface.SERIF, Typeface.NORMAL));
        textPaint.setColor(Color.parseColor("#FFFFFF"));
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    public void resetProgress(){
        setProgress(0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        int innerCircleRadius = getWidth() / 2 - padding;
        RectF rectF = new RectF(padding, padding, getWidth() - padding, getHeight() - padding);
        int sweepAngle = (int) ((float) progress / 100 * 360);
        canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, innerCircleRadius, solidPaint);
        canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, innerCircleRadius, strokePaint);
        canvas.drawArc(rectF, -90, sweepAngle, false, progressPaint);

        drawCenter(canvas, textPaint, progress + "%");
    }

    private Rect r = new Rect();

    private void drawCenter(Canvas canvas, Paint paint, String text) {
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

    public static float dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    // dainv
    public void setSmoothProgress(int progress, int duration) {
        if (progressObjectAnimator != null) {
            progressObjectAnimator.cancel();
        }
        progressObjectAnimator = ObjectAnimator.ofInt(this,"progress", this.progress, progress);
        progressObjectAnimator.setDuration(duration);
        progressObjectAnimator.setInterpolator(new DecelerateInterpolator());
        progressObjectAnimator.start();
    }

    public void setSmoothProgress(int progress) {
        setSmoothProgress(progress, 700);
    }
}
package com.ashokvarma.bottomnavigation;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

/**
 * CustomView base on FixedBottomNavigationTab class of ashokvarma (Tags: 2.0.3)
 *
 * Created by ThanhHV on 12/08/2020
 */
class MFixedBottomNavigationTab extends BottomNavigationTab {

    float labelScale;

    public MFixedBottomNavigationTab(Context context) {
        super(context);
    }

    public MFixedBottomNavigationTab(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MFixedBottomNavigationTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MFixedBottomNavigationTab(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    void init() {
        // [start] modify
        paddingTopActive = (int) getResources().getDimension(R.dimen.fixed_height_top_padding_inactive);
        // [end] modify
        paddingTopInActive = (int) getResources().getDimension(R.dimen.fixed_height_top_padding_inactive);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        // [start] modify
        View view = inflater.inflate(R.layout.m_fixed_bottom_navigation_item, this, true);
        // [end] modify
        containerView = view.findViewById(R.id.fixed_bottom_navigation_container);
        labelView = (TextView) view.findViewById(R.id.fixed_bottom_navigation_title);
        iconView = (ImageView) view.findViewById(R.id.fixed_bottom_navigation_icon);
        iconContainerView = (FrameLayout) view.findViewById(R.id.fixed_bottom_navigation_icon_container);
        badgeView = (BadgeTextView) view.findViewById(R.id.fixed_bottom_navigation_badge);
        iconDot = view.findViewById(R.id.fixed_bottom_navigation_dot);
        animation_view = view.findViewById(R.id.fixed_bottom_navigation_animation_view);

        labelScale = getResources().getDimension(R.dimen.fixed_label_inactive) / getResources().getDimension(R.dimen.fixed_label_active);

        super.init();
    }

    @Override
    public void select(boolean setActiveColor, int animationDuration) {
        // [start] modify
        // labelView.animate().scaleX(1).scaleY(1).setDuration(animationDuration).start();
        // [end] modify
//        labelView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.fixed_label_active));
        super.select(setActiveColor, animationDuration);
        iconDot.setVisibility(VISIBLE);
        labelView.setVisibility(GONE);
        if(lottieFile != null && !lottieFile.equals("")){
            iconView.setVisibility(GONE);
            animation_view.setVisibility(VISIBLE);
            animation_view.playAnimation();
            animation_view.addAnimatorListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    Log.d("CUONGND","onAnimationStart");
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    Log.d("CUONGND","onAnimationEnd");
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    Log.d("CUONGND","onAnimationCancel");
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    Log.d("CUONGND","onAnimationRepeat");
                }
            });
        }
    }

    @Override
    public void unSelect(boolean setActiveColor, int animationDuration) {
        // [start] modify
        // labelView.animate().scaleX(labelScale).scaleY(labelScale).setDuration(animationDuration).start();
        // [end] modify
//        labelView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.fixed_label_inactive));
        super.unSelect(setActiveColor, animationDuration);
        iconView.setVisibility(VISIBLE);
        iconDot.setVisibility(GONE);
        labelView.setVisibility(VISIBLE);
        animation_view.setVisibility(INVISIBLE);
        if (animation_view.isAnimating()) {
            animation_view.cancelAnimation();
        }
    }

    @Override
    protected void setNoTitleIconContainerParams(FrameLayout.LayoutParams layoutParams) {
        layoutParams.height = getContext().getResources().getDimensionPixelSize(R.dimen.fixed_no_title_icon_container_height);
        layoutParams.width = getContext().getResources().getDimensionPixelSize(R.dimen.fixed_no_title_icon_container_width);
    }

    @Override
    protected void setNoTitleIconParams(LayoutParams layoutParams) {
        layoutParams.height = getContext().getResources().getDimensionPixelSize(R.dimen.fixed_no_title_icon_height);
        layoutParams.width = getContext().getResources().getDimensionPixelSize(R.dimen.fixed_no_title_icon_width);
    }
}

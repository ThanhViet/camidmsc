/*
 * Created by admin on 2020/9/6
 */

package com.iknow.trimvideo.listener;

import android.view.View;

public interface OnChooseVideoListener {

    void onChooseVideo(View view, String filePath);
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2021/6/5
 *
 */

package com.iknow.trimvideo.task;

import android.os.AsyncTask;

import com.arthenica.mobileffmpeg.FFmpeg;
import com.iknow.trimvideo.listener.FFmpegExecuteResponseHandler;

public class AsyncFFmpegExecuteTask extends AsyncTask<String, Integer, Integer> {
    private FFmpegExecuteResponseHandler handler;
    private long timeout;
    private long startTime;

    static boolean isProcessCompleted(Process process) {
        try {
            if (process == null) return true;
            process.exitValue();
            return true;
        } catch (IllegalThreadStateException e) {
            // do nothing
        } catch (Exception e) {
            // do nothing
        }
        return false;
    }

    public void setHandler(FFmpegExecuteResponseHandler handler) {
        this.handler = handler;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    @Override
    protected void onPreExecute() {
        startTime = System.currentTimeMillis();
        if (handler != null) {
            handler.onStart();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (values != null && values.length > 0) {
            int progress = values[0];
            if (handler != null) {
                handler.onProgress(progress);
            }
        }
    }

    @Override
    protected void onPostExecute(Integer integer) {
        String output = FFmpeg.getLastCommandOutput();
        if (handler != null) {
            if (integer == 0) {
                handler.onSuccess(output);
            } else {
                handler.onFailure(output);
            }
            handler.onFinish();
        }
    }

    @Override
    protected Integer doInBackground(String... arguments) {
        return FFmpeg.execute(arguments);
    }

}

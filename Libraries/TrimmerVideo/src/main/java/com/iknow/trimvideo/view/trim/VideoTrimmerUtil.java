/*
 * Copyright (c) admin on 2020/8/30
 */

package com.iknow.trimvideo.view.trim;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.iknow.trimvideo.listener.FFmpegExecuteResponseHandler;
import com.iknow.trimvideo.listener.VideoTrimListener;
import com.iknow.trimvideo.task.AsyncFFmpegExecuteTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import iknow.android.utils.DeviceUtil;
import iknow.android.utils.UnitConverter;
import iknow.android.utils.callback.SingleCallback;
import iknow.android.utils.thread.BackgroundExecutor;

public class VideoTrimmerUtil {

    public static final long MIN_SHOOT_DURATION = 30000L;
    public static final int VIDEO_MAX_TIME = 7200;
    public static final long MAX_SHOOT_DURATION = VIDEO_MAX_TIME * 1000L;
    public static final int MAX_COUNT_RANGE = 5;
    public static final int RECYCLER_VIEW_PADDING = UnitConverter.dpToPx(35);
    private static final String TAG = VideoTrimmerUtil.class.getSimpleName();
    private static final int SCREEN_WIDTH_FULL = DeviceUtil.getDeviceWidth();
    public static final int VIDEO_FRAMES_WIDTH = SCREEN_WIDTH_FULL - RECYCLER_VIEW_PADDING * 2;
    public static final int THUMB_WIDTH = (SCREEN_WIDTH_FULL - RECYCLER_VIEW_PADDING * 2) / VIDEO_MAX_TIME;
    private static final int THUMB_HEIGHT = UnitConverter.dpToPx(50);

//    public static void trim(Context context, String inputFile, String outputFile, long startMs, long endMs, final VideoTrimListener callback) {
//        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
//        final String outputName = "trimmedVideo_" + timeStamp + ".mp4";
//        outputFile = outputFile + "/" + outputName;
//
//        String start = convertSecondsToTime(startMs / 1000);
//        String duration = convertSecondsToTime((endMs - startMs) / 1000);
//        //String start = String.valueOf(startMs);
//        //String duration = String.valueOf(endMs - startMs);
//
//        /** 裁剪视频ffmpeg指令说明：
//         * ffmpeg -ss START -t DURATION -i INPUT -codec copy -avoid_negative_ts 1 OUTPUT
//         -ss 开始时间，如： 00:00:20，表示从20秒开始；
//         -t 时长，如： 00:00:10，表示截取10秒长的视频；
//         -i 输入，后面是空格，紧跟着就是输入视频文件；
//         -codec copy -avoid_negative_ts 1 表示所要使用的视频和音频的编码格式，这里指定为copy表示原样拷贝；
//         INPUT，输入视频文件；
//         OUTPUT，输出视频文件
//         */
//        //TODO: Here are some instructions
//        //https://trac.ffmpeg.org/wiki/Seeking
//        //https://superuser.com/questions/138331/using-ffmpeg-to-cut-up-video
//        //ffmpeg -ss 30 -i input.wmv -c copy -t 10 output.wmv
//        String cmd = "-ss " + start + " -t " + duration + " -accurate_seek" + " -i " + inputFile + " -codec copy -avoid_negative_ts 1 " + outputFile;
//        //String cmd = "-ss " + start + " -i " + inputFile + " -ss " + start + " -t " + duration + " -vcodec copy " + outputFile;
//        //{"ffmpeg", "-ss", "" + startTime, "-y", "-i", inputFile, "-t", "" + induration, "-vcodec", "mpeg4", "-b:v", "2097152", "-b:a", "48000", "-ac", "2", "-ar", "22050", outputFile}
//        //String cmd = "-ss " + start + " -y " + "-i " + inputFile + " -t " + duration + " -vcodec " + "mpeg4 " + "-b:v " + "2097152 " + "-b:a " + "48000 " + "-ac " + "2 " + "-ar " + "22050 "+ outputFile;
//        String[] complexCommand = {"-ss", "" + startMs / 1000, "-y", "-i", inputFile, "-t", "" + (endMs - startMs) / 1000, "-vcodec", "mpeg4", "-b:v", "2097152", "-b:a", "48000", "-ac", "2", "-ar", "22050", outputFile};
//        String[] command = cmd.split(" ");
//        try {
//            final String finalOutputFile = outputFile;
//            FFmpeg.getInstance(context).execute(command, new FFmpegExecuteResponseHandler() {
//                @Override
//                public void onProgress(String message) {
//                    LogUtils.d(TAG, "trim onProgress: " + message);
//                }
//
//                @Override
//                public void onFailure(String message) {
//                    LogUtils.e(TAG, "trim onFailure: " + message);
//                    if (callback != null) callback.onTrimFailure(message);
//                }
//
//                @Override
//                public void onFinish() {
//                    LogUtils.d(TAG, "trim onFinish");
//                }
//
//                @Override
//                public void onSuccess(String s) {
//                    LogUtils.e(TAG, "trim onSuccess: " + s);
//                    if (callback != null) callback.onTrimSuccess(finalOutputFile, s);
//                }
//
//                @Override
//                public void onStart() {
//                    LogUtils.d(TAG, "trim onStart");
//                    if (callback != null) callback.onStartTrim();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//            if (callback != null) callback.onTrimFailure(e.getMessage());
//        }
//    }

    public static void trim(Context context, String inputFile, String outputFile, long startMs, long endMs, final VideoTrimListener callback) {
        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        final String outputName = "trimmedVideo_" + timeStamp + ".mp4";
        outputFile = outputFile + "/" + outputName;

        String start = convertSecondsToTime(startMs / 1000);
        String duration = convertSecondsToTime((endMs - startMs) / 1000);
        String cmd = "-ss " + start + " -t " + duration + " -accurate_seek" + " -i " + inputFile + " -codec copy -avoid_negative_ts 1 " + outputFile;
        String[] command = cmd.split(" ");
        try {
            final String finalOutputFile = outputFile;
            AsyncFFmpegExecuteTask task = new AsyncFFmpegExecuteTask();
            task.setTimeout(30000);
            task.setHandler(new FFmpegExecuteResponseHandler() {
                @Override
                public void onProgress(Integer message) {
                    LogUtils.d(TAG, "trim onProgress: " + message);
                }

                @Override
                public void onFailure(String message) {
                    LogUtils.e(TAG, "trim onFailure: " + message);
                    if (callback != null) callback.onTrimFailure(message);
                }

                @Override
                public void onFinish() {
                    LogUtils.d(TAG, "trim onFinish");
                }

                @Override
                public void onSuccess(String s) {
                    LogUtils.e(TAG, "trim onSuccess: " + s + "\nfinalOutputFile: " + finalOutputFile);
                    if (callback != null) callback.onTrimSuccess(finalOutputFile, s);
                }

                @Override
                public void onStart() {
                    LogUtils.d(TAG, "trim onStart");
                    if (callback != null) callback.onStartTrim();
                }
            });
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, command);
        } catch (Exception e) {
            e.printStackTrace();
            if (callback != null) callback.onTrimFailure(e.getMessage());
        }
    }

    public static void shootVideoThumbInBackground(final Context context, final Uri videoUri, final int totalThumbsCount, final long startPosition,
                                                   final long endPosition, final SingleCallback<Bitmap, Integer> callback) {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0L, "") {
            @Override
            public void execute() {
                try {
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(context, videoUri);
                    // Retrieve media data use microsecond
                    long interval = 0;
                    if (totalThumbsCount > 1) interval = (endPosition - startPosition) / (totalThumbsCount - 1);
                    for (long i = 0; i < totalThumbsCount; ++i) {
                        long frameTime = startPosition + interval * i;
                        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(frameTime * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                        if (bitmap == null) continue;
                        if (THUMB_WIDTH > 0 && THUMB_HEIGHT > 0) {
                            try {
                                bitmap = Bitmap.createScaledBitmap(bitmap, THUMB_WIDTH, THUMB_HEIGHT, false);
                            } catch (final Throwable t) {
                                t.printStackTrace();
                            }
                        }
                        callback.onSingleCallback(bitmap, (int) interval);
                    }
                    mediaMetadataRetriever.release();
                } catch (final Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }
        });
    }

    public static String getVideoFilePath(String url) {
        if (TextUtils.isEmpty(url) || url.length() < 5) return "";
        if (url.substring(0, 4).equalsIgnoreCase("http")) {

        } else {
            url = "file://" + url;
        }

        return url;
    }

    private static String convertSecondsToTime(long seconds) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (seconds <= 0) {
            return "00:00";
        } else {
            minute = (int) seconds / 60;
            if (minute < 60) {
                second = (int) seconds % 60;
                timeStr = "00:" + unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99) return "99:59:59";
                minute = minute % 60;
                second = (int) (seconds - hour * 3600 - minute * 60);
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }

    private static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10) {
            retStr = "0" + i;
        } else {
            retStr = "" + i;
        }
        return retStr;
    }
}

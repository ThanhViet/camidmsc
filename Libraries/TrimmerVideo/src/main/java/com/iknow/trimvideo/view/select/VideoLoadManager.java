/*
 * Copyright (c) admin on 2020/8/30
 */

package com.iknow.trimvideo.view.select;

import android.content.Context;

import iknow.android.utils.callback.SimpleCallback;

public class VideoLoadManager {

    private ILoader mLoader;

    public void setLoader(ILoader loader) {
        this.mLoader = loader;
    }

    public void load(final Context context, final SimpleCallback listener) {
        mLoader.load(context, listener);
    }
}

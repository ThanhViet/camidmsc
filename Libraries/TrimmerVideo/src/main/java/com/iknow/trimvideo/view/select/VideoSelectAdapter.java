/*
 * Copyright (c) admin on 2020/8/30
 */

package com.iknow.trimvideo.view.select;

import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.blankj.utilcode.util.FileUtils;
import com.bumptech.glide.Glide;
import com.iknow.android.R;
import com.iknow.trimvideo.listener.OnChooseVideoListener;
import com.library.listener.OnSingleClickListener;

import java.io.File;

import iknow.android.utils.DateUtil;

public class VideoSelectAdapter extends CursorAdapter {

    private int heightItem;
    //    private MediaMetadataRetriever mMetadataRetriever;
    private Context mContext;
    private OnChooseVideoListener listener;

    public VideoSelectAdapter(Context context, Cursor c) {
        super(context, c, true);
        this.mContext = context;
//        mMetadataRetriever = new MediaMetadataRetriever();
        heightItem = context.getResources().getDisplayMetrics().widthPixels / 3;
    }

    private static boolean checkDataValid(MediaMetadataRetriever metadataRetriever, final Cursor cursor) {
        final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        if (TextUtils.isEmpty(path) || !new File(path).exists()) {
            return false;
        }
        try {
            metadataRetriever.setDataSource(path);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        final String duration = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return !TextUtils.isEmpty(duration);
    }

    private static Uri getVideoUri(Cursor cursor) {
        String id = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
        return Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);
    }

    private static void setImage(ImageView imageView, Cursor cursor, int heightItem) {
        if (imageView == null || cursor == null) return;
        try {
            Glide.with(imageView.getContext())
                    .load(getVideoUri(cursor))
                    .centerCrop()
                    .placeholder(R.drawable.bg_image_placeholder_es)
                    .error(R.drawable.bg_image_error_es)
                    .override(heightItem)
                    .into(imageView);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListener(OnChooseVideoListener listener) {
        this.listener = listener;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_grid_video_select, null);
        VideoGridViewHolder holder = new VideoGridViewHolder();
        holder.itemView = itemView.findViewById(R.id.layout_root);
        holder.ivCover = itemView.findViewById(R.id.iv_cover);
        holder.tvDuration = itemView.findViewById(R.id.tv_duration);
        itemView.setTag(holder);
        return itemView;
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        final VideoGridViewHolder holder = (VideoGridViewHolder) view.getTag();
        final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        if (TextUtils.isEmpty(path)) {
            holder.tvDuration.setVisibility(View.GONE);
            holder.ivCover.setVisibility(View.INVISIBLE);
        } else {
            try {
//                mMetadataRetriever.setDataSource(path);
                final long duration = FileUtils.getDuration(path);
                if (duration <= 0) {
                    holder.tvDuration.setVisibility(View.GONE);
                } else {
                    holder.tvDuration.setVisibility(View.VISIBLE);
                    holder.tvDuration.setText(DateUtil.convertSecondsToTime(duration / 1000));
                }
            } catch (Exception e) {
                holder.tvDuration.setVisibility(View.GONE);
                e.printStackTrace();
            }
            holder.ivCover.setVisibility(View.VISIBLE);
            setImage(holder.ivCover, cursor, heightItem);
            holder.itemView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onChooseVideo(view, path);
                }
            });
        }
    }

    private static class VideoGridViewHolder {
        View itemView;
        AppCompatImageView ivCover;
        AppCompatTextView tvDuration;
    }
}

/*
 * Copyright (c) admin on 2020/8/30
 */

package com.iknow.trimvideo.listener;

public interface VideoTrimListener {
    void onStartTrim();

    void onCancelTrim();

    void onTrimSuccess(String filePath, String output);

    void onTrimFailure(String message);
}

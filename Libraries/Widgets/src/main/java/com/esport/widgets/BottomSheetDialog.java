/*
 * Copyright (c) admin created on 2020-09-30 09:52:24 PM
 */

package com.esport.widgets;

import android.content.Context;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import static com.google.android.material.bottomsheet.BottomSheetBehavior.from;

public class BottomSheetDialog extends com.google.android.material.bottomsheet.BottomSheetDialog {

    public BottomSheetDialog(@NonNull Context context) {
        super(context, R.style.style_bottom_sheet_dialog);
    }

    public BottomSheetDialog(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected BottomSheetDialog(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Nullable
    public BottomSheetBehavior getBottomSheetBehavior() {
        BottomSheetBehavior behavior = null;
        try {
            FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
            if (bottomSheet != null) behavior = from(bottomSheet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return behavior;
    }

}

/*
 * Created by admin on 2020/9/6
 */

package com.library.listener;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.view.View;

public abstract class OnSingleClickListener implements View.OnClickListener {

    private long lastTimeClick = 0L;

    public boolean isLockTime() {
        return true;
    }

    public abstract void onSingleClick(View view);

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View view) {
        if (isLockTime()) {
            long currentTime = SystemClock.uptimeMillis();
            long deltaTime = Math.abs(currentTime - lastTimeClick);
            lastTimeClick = currentTime;
            if (deltaTime < 800) return;
        }
        onSingleClick(view);
    }

}

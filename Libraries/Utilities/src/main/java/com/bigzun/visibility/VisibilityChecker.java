/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2020/12/23
 *
 */

package com.bigzun.visibility;

import android.graphics.Rect;
import android.os.SystemClock;
import android.view.View;

public class VisibilityChecker {
    /**
     * A rect to use for hit testing.
     * Create this once to avoid excess garbage collection
     */
    private final Rect mClipRect = new Rect();

    /**
     * Whether the visible time has elapsed
     * from the startPlayer time. Easily mocked for testing.
     */
    boolean hasRequiredTimeElapsed(
            final long startTimeMillis,
            final int minTimeViewed) {
        return SystemClock.uptimeMillis() - startTimeMillis >= minTimeViewed;
    }

    /**
     * Whether the view is at least certain % visible
     */
    public boolean isVisible(
            final View view,
            final int minPercentageViewed) {
        return !(view == null || view.getVisibility() != View.VISIBLE || view.getParent() == null) &&
                isViewVisible(view, minPercentageViewed);
    }

    public boolean isVisibleWithParent(
            final View rootView,
            final View view,
            final int minPercentageViewed) {
        if (view == null || view.getVisibility() != View.VISIBLE ||
                view.getParent() == null || rootView.getParent() == null) {
            return false;
        }
        return isViewVisible(view, minPercentageViewed);
    }

    private boolean isViewVisible(View view, int minPercentageViewed) {
        if (!view.getGlobalVisibleRect(mClipRect)) {
            /** Not visible */return false;
        }

        /** visible check - the cast is to avoid int overflow for large views. */
        final long visibleViewArea = (long) mClipRect.height() * mClipRect.width();
        final long totalViewArea = (long) view.getHeight() * view.getWidth();
        return totalViewArea > 0 &&
                100 * visibleViewArea >= minPercentageViewed * totalViewArea;
    }
}

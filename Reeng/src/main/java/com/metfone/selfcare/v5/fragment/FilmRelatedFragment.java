package com.metfone.selfcare.v5.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movie.adapter.RelateAdapter;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FilmRelatedFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private Unbinder unbinder;
    private RelateAdapter adapter;
    private boolean hasPadding = false;

    public static FilmRelatedFragment newInstance(boolean hasPadding) {
        Bundle bundle = new Bundle();
//        bundle.putSerializable(Constants.TabVideo.VIDEO, video);
//        bundle.putString(Constants.TabVideo.PLAYER_TAG, playerTag);
//        bundle.putBoolean(Constants.TabVideo.PLAYER_INIT, fromLoading);
        bundle.putBoolean("HAS_PADDING", hasPadding);
        FilmRelatedFragment fragment = new FilmRelatedFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setAdapter(RelateAdapter adapter) {
        this.adapter = adapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_related, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hasPadding = getArguments() != null && getArguments().getBoolean("HAS_PADDING");
        if (adapter != null) {
//            BaseAdapter.setupGridRecycler(getActivity(), recyclerView, null, adapter, 2, R.dimen.v5_spacing_normal, hasPadding);
            BaseAdapter.setupHorizontalRecyclerV5(getActivity(), recyclerView, null, adapter, false);
            if (!hasPadding) {
                int spacing = Utilities.dpToPixel(R.dimen.v5_spacing_normal, activity.getResources());
                recyclerView.setPaddingRelative(spacing, recyclerView.getPaddingTop(), spacing, spacing);
            }
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }
}

package com.metfone.selfcare.v5.utils;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by daipr on 4/17/2020
 */
public class LinearItemDecoration extends RecyclerView.ItemDecoration {

    private final int mSpaceHeight;
    private int numberHeader;

    public LinearItemDecoration(int mSpaceHeight) {
        this.mSpaceHeight = mSpaceHeight;
    }

    public LinearItemDecoration(int mSpaceHeight, int numberHeader) {
        this.mSpaceHeight = mSpaceHeight;
        this.numberHeader = numberHeader;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        if (outRect != null) {
            if(parent.getChildAdapterPosition(view) < numberHeader ){
                return;
            }
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = mSpaceHeight;
            }
            outRect.bottom = mSpaceHeight;
        }
    }
}
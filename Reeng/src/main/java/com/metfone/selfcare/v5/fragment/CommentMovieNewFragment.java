package com.metfone.selfcare.v5.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.common.FirebaseEventConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.databinding.CommentMovieNewFragmentBinding;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.ClickLikeMessage;
import com.metfone.selfcare.model.CommentMessage;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.Movie2;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;
import static com.metfone.selfcare.util.EnumUtils.COMMENT;
import static com.metfone.selfcare.util.EnumUtils.RANK_ID;

public class CommentMovieNewFragment extends Fragment implements ItemViewClickListener, View.OnClickListener {
    private final String TAG = "CommentMovieNewFragment";
    private static String TOTAL_COMMENTS_KEY = "comments";
    private final int SIZE_COMMENTS = 3;
    private String mLinkWap;
    private ApplicationController mApplication;
    private CommentMovieNewFragmentBinding mBinding;
    private BaseAdapter mCommentAdapter;
    private ArrayList<FeedAction> mListComment = new ArrayList<>();
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private AvatarBusiness avatarBusiness;
    private ContactBusiness contactBusiness;
    private com.metfone.selfcare.model.account.UserInfo userInfo = userInfoBusiness.getUser();
    int rankID;
    private Movie movie;
    private WSOnMedia rest;
    private ArrayList<String> datas = new ArrayList<>();
    private String msisdn;
    private int totalComments;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        avatarBusiness = ApplicationController.self().getAvatarBusiness();
        contactBusiness = ApplicationController.self().getContactBusiness();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mApplication = (ApplicationController) context.getApplicationContext();
        msisdn = mApplication.getReengAccountBusiness().getJidNumber();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = CommentMovieNewFragmentBinding.inflate(inflater, container, false);
        EventBus.getDefault().register(this);
        if (totalComments != 0) {
            mBinding.tvTotal.setText(totalComments + " " + getString(R.string.comments));
        }
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rest = new WSOnMedia(mApplication);
        initView();
        getData();
    }

    private void initView() {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integers.add(i);
        }
        mBinding.tvReadMore.setOnClickListener(this);
        mBinding.btnSendComment.setOnClickListener(this);
        mBinding.edtComment.setOnClickListener(this);
        mBinding.recyclerCommentNew.setLayoutManager(new LinearLayoutManager(getContext()));
        ViewHolderComment viewHolderComment = new ViewHolderComment(getActivity().getWindow().getDecorView().getRootView(), this);
        mCommentAdapter = new BaseAdapter(mListComment, getContext(), R.layout.item_comment_movie_new, viewHolderComment);
        mBinding.recyclerCommentNew.setAdapter(mCommentAdapter);
        // set avatar img cmt
//        Glide.with(getContext())
//                .load(userInfo.getAvatar())
//                .error(R.drawable.avatar_default_new)
//                .into(mBinding.imgAvatar);
        if (TextUtils.isEmpty(userInfo.getAvatar())) {
            getRankAccount();
        } else {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_member)
                    .error(R.drawable.ic_avatar_member)
                    .into(mBinding.imgAvatar);
        }
        // sự kiện thay đổi text trong edit text comment
        mBinding.edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty() && s.toString().length() > 1) {
                    mBinding.btnSendComment.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_comment_new_focus));
                    mBinding.btnSendComment.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                } else {
                    mBinding.btnSendComment.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_comment_new));
                    mBinding.btnSendComment.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onReceiveData(CommentMessage event) {
        mBinding.contrainsComment.setVisibility(View.VISIBLE);
        mBinding.edtComment.setText("");
        hideKeyboard();
        if (event.isCommit()) {
            if (isLogin() == EnumUtils.LoginVia.NOT) {
                userInfoBusiness.showLoginDialog((BaseSlidingFragmentActivity) getActivity(), R.string.info, R.string.content_popup_login);
            } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
                userInfoBusiness.showAddPhoneDialog((BaseSlidingFragmentActivity) getActivity(), R.string.info, R.string.content_popup_add_phone);
            } else {
                sendCommentStatus(event.getComment());
            }
        }
    }


    private void getData() {
        Log.e("duongnk", "getData: CommentMovieNewFragment");
        mLinkWap = getArguments().getString("linkWap");
        movie = (Movie) getArguments().getSerializable("movie");
        Log.e("duongnk", "mLinkWap : " + mLinkWap);
        rest.getDetailUrl(new OnMediaInterfaceListener.GetListFeedListener() {
            @Override
            public void onGetListFeedDone(RestAllFeedsModel restAllFeedsModel) {
                if (restAllFeedsModel != null && restAllFeedsModel.getData() != null && restAllFeedsModel.getData().size() > 0) {
//                    totalComments = (int) restAllFeedsModel.getData().get(0).getFeedContent().getCountComment();
//                    if (totalComments > 0) {
//                        mBinding.tvTotal.setText(totalComments + " " + getString(R.string.comments));
//                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, mLinkWap, true);
        rest.getListComment(mLinkWap, "", new OnMediaInterfaceListener.GetListComment() {
            @Override
            public void onGetListCommentSuccess(ArrayList<FeedAction> listComment, ArrayList<UserInfo> listUserLike, FeedAction feedAction, long timeServer) {
                Log.e("duongnk", "onGetListCommentSuccess: ");
                if (listComment == null || listComment.size() == 0) {
                    mBinding.recyclerCommentNew.setVisibility(View.GONE);
                    return;
                } else {
                    totalComments = listComment.size();
                    mBinding.tvTotal.setText(totalComments + " " + getString(R.string.comments));
                    mBinding.recyclerCommentNew.setVisibility(View.VISIBLE);
                    if (listComment.size() > SIZE_COMMENTS) {
                        mBinding.tvReadMore.setVisibility(View.VISIBLE);
                        mCommentAdapter.setItemCount(SIZE_COMMENTS);
                    } else {
                        mBinding.tvReadMore.setVisibility(View.GONE);
                    }
                    mListComment.addAll(listComment);
                    mCommentAdapter.updateAdapter(mListComment);
                    if (listComment.size() == 20) {
                        getAllComments(listComment);
                    }else{
                        mBinding.tvTotal.setText(totalComments + " " + getString(R.string.comments));
                    }
                    EventBus.getDefault().post(listComment);
                }

            }

            @Override
            public void onGetListCommentError(int code, String des) {
                Log.e("duongnk", "onGetListCommentError: ");
            }
        }, Integer.toHexString(System.identityHashCode(this)), false);

    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mBinding.tvReadMore.getId()) {
            if (mListComment != null && mListComment.size() > SIZE_COMMENTS) {
                mBinding.tvReadMore.setVisibility(View.GONE);
                mCommentAdapter.setItemCount(mListComment.size());
                mCommentAdapter.notifyDataSetChanged();
            }
        } else if (id == mBinding.btnSendComment.getId()) {
            ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_COMMENT);
            if (isLogin() == EnumUtils.LoginVia.NOT) {
                userInfoBusiness.showLoginDialog((BaseSlidingFragmentActivity) getActivity(), R.string.info, R.string.content_popup_login);
            } else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getJidNumber())) {
                userInfoBusiness.showAddPhoneDialog((BaseSlidingFragmentActivity) getActivity(), R.string.info, R.string.content_popup_add_phone);
            } else {
                sendCommentStatus(mBinding.edtComment.getText().toString());
            }
        } else if (id == mBinding.edtComment.getId()) {
            mBinding.contrainsComment.setVisibility(View.GONE);
            CommentBottomSheetDialogFragment bottomSheetFragment = new CommentBottomSheetDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString(COMMENT, mBinding.edtComment.getText().toString());
            bundle.putInt(RANK_ID, rankID);
            bottomSheetFragment.setArguments(bundle);
            bottomSheetFragment.show(getChildFragmentManager(), bottomSheetFragment.getTag());
            getChildFragmentManager().executePendingTransactions();
            bottomSheetFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    EventBus.getDefault().post(true);
                    hideKeyboard();
                }
            });
        }
    }

    public void hideKeyboard() {
        Handler handler = new Handler();
        handler.postDelayed(() -> InputMethodUtils.hideSoftKeyboard(getActivity()), 100);
    }

    private void showToast(int message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mApplication, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkSpamComment(String content) {
        if (datas.size() < 1) return false;
        for (String data : datas) {
            if (data.equals(content)) {
                return true;
            }
        }
        return false;
    }

    private void updateComments(FeedAction commentStatusFeed) {
        datas.add(commentStatusFeed.getComment());
        if (mListComment.size() > 0) {
            mListComment.add(0, commentStatusFeed);
        } else {
            mListComment.add(commentStatusFeed);
        }
        mCommentAdapter.notifyDataSetChanged();
        mBinding.edtComment.setText("");
        mBinding.tvTotal.setText(++totalComments + " " + getString(R.string.comments));
        mBinding.btnSendComment.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        if (mBinding.recyclerCommentNew.getVisibility() != View.VISIBLE) {
            mBinding.recyclerCommentNew.setVisibility(View.VISIBLE);
        }
    }

    private void sendCommentStatus(String messageContent) {
        if (messageContent.length() == 0 || TextUtils.isEmpty(messageContent.trim())) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(getActivity())) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }

        Log.i(TAG, "send text: " + mBinding.edtComment.getText().toString());
        if (checkSpamComment(messageContent)) {
            showToast(R.string.comment_onmedia_fail);
            return;
        }
        String idCmtLocal = EncryptUtil.encryptMD5(mApplication.getReengAccountBusiness().getJidNumber()
                + System.currentTimeMillis());

        FeedContent feedContent = null;
        try {
            feedContent = FeedContent.convertMovieToFeedContent(movie);
        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointerException", e);
        }
        if (feedContent == null) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        feedContent.setContentAction(FeedContent.CONTENT_ACTION);
        feedContent.setIdCmtLocal(idCmtLocal);
        FeedModelOnMedia.ActionFrom actionFrom = FeedModelOnMedia.ActionFrom.mochavideo;

        FeedAction commentStatusFeed = new FeedAction(msisdn, messageContent, System.currentTimeMillis(), System.currentTimeMillis());
        UserInfo userInfo = new UserInfo();
        userInfo.setName(userInfoBusiness.getUser().getFull_name());
        userInfo.setMsisdn(userInfoBusiness.getUser().getPhone_number());
        commentStatusFeed.setUserInfo(userInfo);
        commentStatusFeed.setIdCmtLocal(idCmtLocal);
        updateComments(commentStatusFeed);

        rest.logAppV6(feedContent.getUrl(), "", feedContent, FeedModelOnMedia.ActionLogApp.COMMENT,
                messageContent, "", "", actionFrom,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionComment: onresponse: " + response);
//                        mProgressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {

                                } else {
                                    showToast(R.string.comment_not_success);
                                }
                            } else {
                                showToast(R.string.comment_not_success);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            showToast(R.string.comment_not_success);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Response error" + volleyError.getMessage());
//                        mProgressBar.setVisibility(View.GONE);
                        showToast(R.string.comment_not_success);
                    }
                });
    }

    public class ViewHolderComment extends BaseViewHolder {

        public ViewHolderComment(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
            super(itemView, itemViewClickListener);
        }

        CircleImageView imgAvatarComment;
        AppCompatTextView tvUserName;
        AppCompatTextView tvContentComment;

        @Override
        public void initViewHolder(View v) {
            if (mData == null || mData.size() == 0) return;
            if (mData.get(0) instanceof FeedAction) {
                imgAvatarComment = v.findViewById(R.id.imgAvatarComment);
                tvUserName = v.findViewById(R.id.tvUserName);
                tvContentComment = v.findViewById(R.id.tvContentComment);
            }
        }

        @Override
        public void onBinViewHolder(List<?> obj, int pos, Context context) {
            if (mData.get(pos) instanceof FeedAction) {
                FeedAction feedAction = (FeedAction) mData.get(pos);
                Glide.with(context)
                        .load(feedAction.getUserInfo().getAvatar())
                        .error(R.drawable.avatar_default_new)
                        .into(imgAvatarComment);
                try {
                    tvUserName.setText(feedAction.getUserInfo().getNameUser());
                } catch (Exception e) {
                    tvUserName.setText(feedAction.getUserInfo().getName());
                }
                tvContentComment.setText(feedAction.getComment());
                UserInfo userInfo = feedAction.getUserInfo();
                if (userInfo == null) return;
                String mFriendJid = userInfo.getMsisdn();
                PhoneNumber mFriendPhone;
                if (userInfo.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL) {
                    if (mFriendJid.contains(userInfoBusiness.getUser().getPhone_number())) {
                        tvUserName.setText(userInfoBusiness.getUser().getFull_name());
                        Glide.with(getContext())
                                .load(ImageUtils.convertBase64ToBitmap(userInfoBusiness.getUser().getAvatar()))
                                .error(R.drawable.ic_avatar_member)
                                .into(imgAvatarComment);
                    } else {
                        mFriendPhone = contactBusiness.getPhoneNumberFromNumber(mFriendJid);
                        String mFriendAvatarUrl;
                        if (mFriendPhone != null) {// luu danh ba
                            if (!StringUtils.isEmpty(mFriendPhone.getName())) {
                                tvUserName.setText(mFriendPhone.getName());
                            }
                            mFriendAvatarUrl = avatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo.getMsisdn
                                    (), 38);
                        } else {
                            if (userInfo.isUserMocha()) {
                                if ("0".equals(userInfo.getAvatar())) {
                                    mFriendAvatarUrl = "";
                                } else {
                                    mFriendAvatarUrl = avatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo.getMsisdn
                                            (), 38);
                                }
                            } else {
                                mFriendAvatarUrl = userInfo.getAvatar();
                            }
                        }
                        avatarBusiness.setAvatarOnMedia(imgAvatarComment, null,
                                mFriendAvatarUrl, userInfo.getMsisdn(), null, 38);
                    }
                }
            }

        }

        @Override
        public void onItemViewClick(View v, int pos) {

        }
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount() {
        KhHomeClient homeKhClient = new KhHomeClient();
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                initRankInfo(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null) {
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                }
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                if (getActivity() != null) {
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), getString(R.string.service_error));
                }
            }
        });
    }

    private void initRankInfo(AccountRankDTO account) {
        if (getActivity() != null) {
            if (account != null) {
                rankID = account.rankId;
                EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
                if (myRank != null) {
                    Glide.with(this)
                            .load(myRank.drawable)
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(mBinding.imgAvatar);
                } else {
                    Glide.with(this)
                            .load(ContextCompat.getDrawable(getActivity(), R.drawable.ic_avatar_member))
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(mBinding.imgAvatar);
                }
            } else {
                mBinding.imgAvatar.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_avatar_member));
//            imgCrown.setVisibility(View.GONE);
            }
        }

    }

    private void getAllComments(ArrayList<FeedAction> comments) {
        String lastId = "";
        for (int i = comments.size() - 1; i >= 0; i--) {
            if (!comments.get(i).isReplyCmt()) {
                lastId = comments.get(i).getBase64RowId();
                break;
            }
        }
        rest.getListComment(mLinkWap, lastId, 1000, new OnMediaInterfaceListener.GetListComment() {
            @Override
            public void onGetListCommentSuccess(ArrayList<FeedAction> listComment, ArrayList<UserInfo> listUserLike, FeedAction feedAction, long timeServer) {
                if (listComment == null || listComment.size() >= 0) {
                    mListComment.addAll(listComment);
                    mCommentAdapter.notifyDataSetChanged();
                    if(totalComments == 0){
                        totalComments = listComment.size();
                    }
                }
                totalComments = mListComment.size();
                mBinding.tvTotal.setText(totalComments + " " + getString(R.string.comments));
            }

            @Override
            public void onGetListCommentError(int code, String des) {
            }
        }, Integer.toHexString(System.identityHashCode(this)), false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CommentMessage stickyEvent = EventBus.getDefault().getStickyEvent(CommentMessage.class);
// Better check that an event was actually posted before
        if (stickyEvent != null) {
            // "Consume" the sticky event
            EventBus.getDefault().removeStickyEvent(stickyEvent);
            // Now do something with it
        }
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.v5.home.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.reflect.TypeToken;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.video.event.TabVideoReselectedEvent;
import com.metfone.selfcare.module.video.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.video.fragment.VideoPagerFragment;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.dialog.BottomSheetDialog;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.tabvideo.activity.videoLibrary.TabVideoLibraryActivity;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.SubscribeChannelActivity;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelFragment;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.CHANNEL_FOLLOW;
import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.MY_CHANNEL;
import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.VIDEO_LIBRARY;

public class TabVideoFragmentV2 extends BaseHomeFragment implements ViewPager.OnPageChangeListener
        , TabLayout.OnTabSelectedListener, OnInternetChangedListener, OnChannelChangedDataListener {

    public static final String CONTENT_TYPE_MOVIE = "film";
    public static final String CONTENT_TYPE_SUBSCRIBE_CHANNEL = "following";
    public static final String CONTENT_TYPE_NEW = "new";
    public static final String CONTENT_TYPE_HOT = "hot";

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.loading_view_tab)
    LoadingView loadingView;
    @BindView(R.id.layout_action_bar)
    View viewActionBar;
    @BindView(R.id.iv_back)
    View btnBack;
    @BindView(R.id.tv_tab_name)
    View tvTabName;
    @BindView(R.id.iv_search)
    View btnSearch;
    @BindView(R.id.iv_more)
    View btnMore;
    private ArrayList<Category> data;
    private boolean isLoading;
    private VideoApi mVideoApi;
    private ChannelApi mChannelApi;
    private ListenerUtils mListenerUtils;
    private ApiCallbackV2<ArrayList<Object>> callbackCategory = new ApiCallbackV2<ArrayList<Object>>() {
        @Override
        public void onSuccess(String msg, ArrayList<Object> results) throws JSONException {
            if (data == null) data = new ArrayList<>();
            else data.clear();
            if (results != null && results.size() >= 2) {
                try {
                    ArrayList<AdsRegisterVip> adsRegisterList = (ArrayList<AdsRegisterVip>) results.remove(0);
                    ArrayList<Category> categoriesList = (ArrayList<Category>) results.remove(0);
                    data.addAll(categoriesList);
                    app.getReengAccountBusiness().setListAdsRegisterVip(adsRegisterList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (activity == null || activity.isFinishing()) return;
            activity.runOnUiThread(() -> {
                setupViewPager(data);
                if (Utilities.isEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedEmpty();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            });
        }

        @Override
        public void onError(String s) {
            if (Utilities.isEmpty(data)) {
                if (loadingView != null) loadingView.showLoadedError();
            } else {
                if (loadingView != null) loadingView.showLoadedSuccess();
            }
        }

        @Override
        public void onComplete() {
            isDataInitiated = true;
            isLoading = false;
        }
    };
    private Channel myChannel;
    private ApiCallbackV2<Channel> callbackChannel = new ApiCallbackV2<Channel>() {

        @Override
        public void onSuccess(String lastId, Channel channel) {
            SharedPrefs.getInstance().put(Constants.TabVideo.TIME_GET_CHANNEL_INFO, System.currentTimeMillis());
            SharedPrefs.getInstance().put(Constants.TabVideo.CACHE_MY_CHANNEL_INFO, channel);
            myChannel = channel;
        }

        @Override
        public void onError(String s) {

        }

        @Override
        public void onComplete() {
        }
    };
    private Utils utils;
    private BottomSheetDialog dialogMore;
    private ArrayList<ItemContextMenu> dialogMoreItems;
    private int indexSubTab;
    private int currentPosition = -1;

    public static TabVideoFragmentV2 newInstance(int indexSubTab) {
        Bundle args = new Bundle();
        TabVideoFragmentV2 fragment = new TabVideoFragmentV2();
        args.putInt(Constants.KEY_POSITION, indexSubTab);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_tab_video_v2;
    }

    @Override
    public void onCreateView() {
        Bundle bundle = getArguments();
        indexSubTab = bundle != null ? bundle.getInt(Constants.KEY_POSITION) : 0;
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
            }
        });
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity instanceof ModuleActivity) activity.finish();
                else if (activity != null) activity.onBackPressed();
            }
        });
        btnMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickMore();
            }
        });
        btnSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity == null) return;
                Utilities.openSearch(activity, Constants.TAB_VIDEO_HOME);
            }
        });
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_video);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        mVideoApi = app.getApplicationComponent().providerVideoApi();
        mChannelApi = app.getApplicationComponent().providerChannelApi();
        utils = app.getApplicationComponent().providesUtils();
        if (activity instanceof HomeActivity) {
            btnBack.setVisibility(View.GONE);
            Utilities.setMargins(tvTabName, Utilities.dpToPixel(R.dimen.v5_spacing_normal, res), tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
//            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), Utilities.dpToPixel(R.dimen.v5_margin_top_action_bar_home, res), viewActionBar.getRight(), viewActionBar.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
            new Handler().postDelayed(this::initWithCacheData, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        } else {
            btnBack.setVisibility(View.VISIBLE);
            Utilities.setMargins(tvTabName, 0, tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
            new Handler().postDelayed(this::initWithCacheData, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad())
            loadData(true);
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    private void initWithCacheData() {
        try {
            ArrayList<Category> categoriesList = SharedPrefs.getInstance().get(Constants.TabVideo.CACHE_CATEGORIES, new TypeToken<ArrayList<Category>>() {
            }.getType());
            if (Utilities.notEmpty(categoriesList)) {
                ArrayList<Object> items = new ArrayList<>();
                ArrayList<AdsRegisterVip> adsRegisterList = SharedPrefs.getInstance().get(Constants.TabVideo.CACHE_ADS_REGISTER_VIPS, new TypeToken<ArrayList<AdsRegisterVip>>() {
                }.getType());
                items.add(adsRegisterList);
                items.add(categoriesList);
                if (callbackCategory != null) callbackCategory.onSuccess("", items);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadData(Utilities.isEmpty(data));
        loadChannelInfo();
    }

    private void loadData(boolean showLoading) {
        if (isLoading || mVideoApi == null) return;
        isLoading = false;
        if (showLoading) {
            if (loadingView != null) loadingView.showLoading();
        }
        if (mVideoApi != null)
            mVideoApi.getCategoriesV2(callbackCategory);
    }

    private void setupViewPager(ArrayList<Category> data) {
        if (viewPager != null) {
            viewPager.setAdapter(createAdapterPage(data));
            viewPager.setOffscreenPageLimit(3);
            if (activity instanceof HomeActivity)
                viewPager.setCurrentItem(((HomeActivity) activity).getCurrentSubTabVideo());
            else
                viewPager.setCurrentItem(indexSubTab);
            viewPager.addOnPageChangeListener(this);
            if (tabLayout != null) {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.addOnTabSelectedListener(this);
            }
        }
        if (data.size() < 2) {
            if (appBarLayout != null) appBarLayout.setVisibility(View.GONE);
        } else {
            if (appBarLayout != null) appBarLayout.setVisibility(View.VISIBLE);
            if (data.size() < 4) {
                if (tabLayout != null) tabLayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                if (tabLayout != null) tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
        if (coordinatorLayout != null) {
            if (Utilities.isEmpty(data))
                coordinatorLayout.setVisibility(View.GONE);
            else
                coordinatorLayout.setVisibility(View.VISIBLE);
        }
    }

    private PagerAdapter createAdapterPage(ArrayList<Category> list) {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(activity);
        for (int i = 0; i < list.size(); i++) {
            Category category = list.get(i);
            String id = category.getId();
            if (CONTENT_TYPE_MOVIE.equals(category.getContentType())) {
                creator.add(FragmentPagerItem.of(category.getName(), MoviePagerFragment.class, MoviePagerFragment.arguments(i, viewPager.getCurrentItem(), id)));
            } else if (CONTENT_TYPE_SUBSCRIBE_CHANNEL.equals(category.getContentType())) {
                creator.add(FragmentPagerItem.of(category.getName(), SubscribeChannelFragment.class, SubscribeChannelFragment.arguments(i, viewPager.getCurrentItem(), id)));
            } else {
                if (Utilities.isEmpty(id)) continue;
                creator.add(FragmentPagerItem.of(category.getName(), VideoPagerFragment.class, VideoPagerFragment.arguments(i, viewPager.getCurrentItem(), id, category.getContentType())));
            }
        }
        return new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }

    private void loadChannelInfo() {
        if (!app.getReengAccountBusiness().isAnonymousLogin()) {
            long time;
            try {
                time = SharedPrefs.getInstance().get(Constants.TabVideo.TIME_GET_CHANNEL_INFO, Long.class, 0L);
            } catch (Exception e) {
                time = 0L;
            }
            myChannel = SharedPrefs.getInstance().get(Constants.TabVideo.CACHE_MY_CHANNEL_INFO, Channel.class);
            if (TimeHelper.checkTimeInDay(time) && myChannel != null && Utilities.notEmpty(myChannel.getId())) {
                return;
            }
            if (mChannelApi != null) {
                mChannelApi.getMyChannelInfoV2(callbackChannel);
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (appBarLayout != null) appBarLayout.setExpanded(true);
        if (currentPosition != position) {
            if (data != null && data.size() > position && position >= 0) {
                Category model = data.get(position);
                if (model != null) {
                }
            }
        }
        currentPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(activity) && Utilities.isEmpty(data)) {
            loadData(false);
        }
    }

    @Override
    public void onChannelCreate(Channel channel) {
        try {
            if (callbackChannel != null) callbackChannel.onSuccess("", channel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {

    }

    private void initDataDialogMore() {
        if (dialogMoreItems == null) dialogMoreItems = new ArrayList<>();
        else dialogMoreItems.clear();
        ItemContextMenu item;

        if (app.getReengAccountBusiness().isVietnam()) {
                if (myChannel != null && Utilities.notEmpty(myChannel.getId())) {
                    item = new ItemContextMenu(getString(R.string.myChannel) + " (" + myChannel.getName() + ")", R.drawable.ic_my_channel_option, null, Constants.MENU.MENU_MY_CHANNEL);
                    dialogMoreItems.add(item);
                } else {
                    item = new ItemContextMenu(getString(R.string.myChannel), R.drawable.ic_my_channel_option, null, Constants.MENU.MENU_MY_CHANNEL);
                    dialogMoreItems.add(item);
                }
                item = new ItemContextMenu(getString(R.string.post_video), R.drawable.ic_upload_option, null, Constants.MENU.MENU_POST_VIDEO);
                dialogMoreItems.add(item);
        }

        item = new ItemContextMenu(getString(R.string.videoLibrary), R.drawable.ic_folder_option, null, Constants.MENU.MENU_VIDEO_LIBRARY);
        dialogMoreItems.add(item);

        item = new ItemContextMenu(getString(R.string.title_subscribe_channel), R.drawable.ic_channel_following_option, null, Constants.MENU.MENU_CHANNEL_SUBSCRIBE);
        dialogMoreItems.add(item);

        if (app.getConfigBusiness().isEnableDiscovery()) {
            item = new ItemContextMenu(getString(R.string.discover), R.drawable.ic_close_option, null, Constants.MENU.MENU_DISCOVER);
            dialogMoreItems.add(item);
        }

//        if (app.getConfigBusiness().isEnableChannelUploadMoney()) {
//            item = new ItemContextMenu(getString(R.string.top_money_upload), R.drawable.ic_v5_top_money, null, Constants.MENU.MENU_TOP_MONEY_UPLOAD);
//            dialogMoreItems.add(item);
//        }

        item = new ItemContextMenu(getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        dialogMoreItems.add(item);
    }

    private void onClickMore() {
        if (dialogMore != null && dialogMore.isShowing()) {
            dialogMore.dismiss();
            dialogMore = null;
        }
        dialogMore = new BottomSheetDialog(activity);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        initDataDialogMore();
        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(dialogMoreItems);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu item = (ItemContextMenu) object;
                    switch (item.getActionTag()) {
                        case Constants.MENU.MENU_CHANNEL_SUBSCRIBE:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                DeepLinkHelper.getInstance().openSchemaLink(activity, CHANNEL_FOLLOW);
                            } else
                                SubscribeChannelActivity.start(activity, Constants.TAB_SUBSCRIBE_CHANNEL);
                            break;

                        case Constants.MENU.MENU_DISCOVER:
                            NavigateActivityHelper.navigateToOnMediaDetail(activity, "", Constants.ONMEDIA.DISCOVERY, 0, false);
                            break;

                        case Constants.MENU.MENU_MY_CHANNEL:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                DeepLinkHelper.getInstance().openSchemaLink(activity, MY_CHANNEL);
                            } else {
                                if (myChannel == null || Utilities.isEmpty(myChannel.getId())) {
                                    if (utils != null) utils.openCreateChannel(activity);
                                } else {
                                    if (utils != null) utils.openChannelInfo(activity, myChannel);
                                }
                            }
                            break;

                        case Constants.MENU.MENU_VIDEO_LIBRARY:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                DeepLinkHelper.getInstance().openSchemaLink(activity, VIDEO_LIBRARY);
                            } else
                                TabVideoLibraryActivity.start(activity);
                            break;

                        case Constants.MENU.MENU_TOP_MONEY_UPLOAD:
                            if (app.getConfigBusiness().isEnableChannelUploadMoney()) {
                                SubscribeChannelActivity.start(activity, Constants.TAB_TOP_MONEY_UPLOAD);
                            } else {
                                activity.showToast(R.string.e666_not_support_function);
                            }
                            break;
                        case Constants.MENU.MENU_POST_VIDEO:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                activity.showDialogLogin();
                            } else {
                                if (myChannel == null || Utilities.isEmpty(myChannel.getId())) {
                                    if (utils != null) utils.openCreateChannel(activity);
                                } else {
                                    DialogUtils.onClickUpload(activity);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                dialogMore.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialogMore.setContentView(sheetView);
        dialogMore.setOnShowListener(dialog -> {

        });
        dialogMore.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabVideoReselectedEvent event) {
        if (event.isReselectedHome()) {
            if (appBarLayout != null) {
                appBarLayout.setExpanded(true);
            }
            if (mListenerUtils != null && viewPager != null) {
                if (event.getIndexTabSelected() >= 0) {
                    viewPager.setCurrentItem(event.getIndexTabSelected(), true);
                }
                mListenerUtils.notifyTabReselected(Constants.TAB_VIDEO_HOME, viewPager.getCurrentItem());
            }
        }
    }

}

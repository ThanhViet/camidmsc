package com.metfone.selfcare.v5.login;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnEditProfileListener;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 11/14/2019.
 */

public class InfoNameFragment extends Fragment {

    private static final String TAG = InfoNameFragment.class.getSimpleName();
    EditText etUserName;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    Unbinder unbinder;
    @BindView(R.id.tilUserName)
    TextInputLayout tilUserName;
    @BindView(R.id.ivAvatar)
    RoundedImageView ivAvatar;


    private BaseSlidingFragmentActivity activity;
    private ApplicationController app;
    private Resources res;
    private OnEditProfileListener mListener;
    private int uploadOption;

    ReengAccount reengAccount;

    public static InfoNameFragment newInstance() {
        InfoNameFragment fragment = new InfoNameFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_name, container, false);
        unbinder = ButterKnife.bind(this, view);
        etUserName = tilUserName.getEditText();
        drawDetail();
        setViewListener();
        return view;
    }

    private void setViewListener() {
        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        app = (ApplicationController) activity.getApplication();
        res = activity.getResources();
        reengAccount = app.getReengAccountBusiness().getCurrentAccount();
        try {
            mListener = (OnEditProfileListener) context;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    private void drawDetail() {
        if (reengAccount == null) return;
        etUserName.setText(reengAccount.getName());
        if (!TextUtils.isEmpty(reengAccount.getName()))
            etUserName.setSelection(reengAccount.getName().length());

        app.getAvatarBusiness().setMyAvatar(ivAvatar, reengAccount);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ivAvatar, R.id.llChangeAvatar, R.id.llConnectFb, R.id.btnContinue, R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivAvatar:
                break;
            case R.id.llChangeAvatar:
                uploadOption = ImageProfileConstant.IMAGE_AVATAR;
                showPopupChangeAvatar();
                break;
            case R.id.llConnectFb:
                if (activity instanceof LoginActivity) {
                    ((LoginActivity) activity).getInfoFb();
                }
                break;
            case R.id.btnContinue:
                if (activity instanceof LoginActivity) {
                    ((LoginActivity) activity).setNameAndGoToDateOfBirthFragment(etUserName.getText().toString());
                }
                break;
            case R.id.ivBack:
                activity.onBackPressed();
                break;
        }
    }


    private void showPopupChangeAvatar() {
        /*ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu selectGallery = new ItemContextMenu(mParentActivity, mRes.getString(R.string
                .select_from_gallery),
                -1, null, Constants.MENU.SELECT_GALLERY);
        ItemContextMenu capture = new ItemContextMenu(mParentActivity, mRes.getString(R.string.capture_image),
                -1, null, Constants.MENU.CAPTURE_IMAGE);
        listItem.add(capture);
        listItem.add(selectGallery);
        PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(), null, listItem, this);*/

        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu openCamera = new ItemContextMenu(activity.getString(R.string.camera),
                R.drawable.ic_bottom_camera, null, Constants.MENU.CAPTURE_IMAGE);
        ItemContextMenu openGallery = new ItemContextMenu(activity.getString(R.string.select_from_gallery),
                R.drawable.ic_bottom_image, null, Constants.MENU.SELECT_GALLERY);
        listItem.add(openCamera);
        listItem.add(openGallery);
        new BottomSheetMenu(activity, true)
                .setListItem(listItem)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onItemClick(int itemId, Object entry) {
                        switch (itemId) {
                            case Constants.MENU.CAPTURE_IMAGE:
                                if (mListener != null)
                                    mListener.takeAPhoto(uploadOption);
                                break;
                            case Constants.MENU.SELECT_GALLERY:
                                if (mListener != null)
                                    mListener.openGallery(uploadOption);
                                break;
                        }
                    }
                }).show();
    }

    public void updateAvatar(String filePath) {
        reengAccount = app.getReengAccountBusiness().getCurrentAccount();
        if (reengAccount != null && ivAvatar != null) {
//            mImgAvatar.setBorderWidth(0);
//            mTvwAvatarDefault.setVisibility(View.GONE);
            app.getAvatarBusiness().setMyAvatar(ivAvatar, null, null, reengAccount,
                    filePath);

            if (activity instanceof LoginActivity) {
                ReengAccount tmp = ((LoginActivity) activity).getReengAccountTmp();
                if (tmp != null) {
                    if (!TextUtils.isEmpty(tmp.getName())) {
                        etUserName.setText(tmp.getName());
                        etUserName.setSelection(tmp.getName().length());
                    }
                }
            }
        }
    }

    private void setEnableButton(boolean enableButton) {
        btnContinue.setClickable(enableButton);
        if (enableButton) {
            btnContinue.setTextColor(ContextCompat.getColor(activity, R.color.v5_text_7));
            btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(activity, R.color.v5_main_color),
                    ContextCompat.getColor(activity, R.color.v5_main_color_press));
        } else {
            btnContinue.setTextColor(ContextCompat.getColor(activity, R.color.v5_text_3));
            btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(activity, R.color.v5_cancel),
                    ContextCompat.getColor(activity, R.color.v5_cancel));
        }
    }
}

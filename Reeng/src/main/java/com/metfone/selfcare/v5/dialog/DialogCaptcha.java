package com.metfone.selfcare.v5.dialog;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

public class DialogCaptcha extends Dialog implements View.OnClickListener {
    private BaseSlidingFragmentActivity activity;
    private DialogInterface listener;
    private AppCompatTextView tvTitle, tvMessage;
    private AppCompatButton btnLeft, btnRight;
    private EditText editTextInput;
    private AppCompatImageView ivCaptcha, ivRefreshCaptcha;
    private Bitmap bitmap;

    public DialogCaptcha(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogCaptcha setDialogListener(DialogInterface listener) {
        this.listener = listener;
        return this;
    }
    public DialogCaptcha setBitmapCaptcha(Bitmap bitmap) {
        this.bitmap = bitmap;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_captcha_spoint);
        findComponentViews();
        drawDetail();
        setListener();
    }

    private void drawDetail() {
        if(bitmap != null) {
            ivCaptcha.setImageBitmap(bitmap);
        }
    }

    private void findComponentViews() {
        tvTitle = findViewById(R.id.tvTitleDialog);
        tvMessage = findViewById(R.id.tvMessageDialog);

        btnLeft = findViewById(R.id.btnLeft);
        btnRight = findViewById(R.id.btnRight);
        editTextInput = findViewById(R.id.edt_input_money);
        ivCaptcha = findViewById(R.id.iv_captcha);
        ivRefreshCaptcha = findViewById(R.id.iv_load_captcha);
    }

    private void setListener() {
        btnLeft.setOnClickListener(this);
        btnRight.setOnClickListener(this);
        ivRefreshCaptcha.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft:
                if (listener != null) {
                    listener.onClickLeft();
                }
                break;
            case R.id.btnRight:
                if (listener != null) {
                    listener.onClickRight(editTextInput.getText().toString().trim());
                }
                break;
            case R.id.iv_load_captcha:
                if (listener != null) {
                    listener.onClickRefresh(ivCaptcha);
                    return;
                }
                break;
        }
        dismiss();
    }

    public interface DialogInterface {
        void onClickLeft();

        void onClickRight(String captcha);

        void onClickRefresh(AppCompatImageView ivRefresh);
    }
}

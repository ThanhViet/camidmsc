package com.metfone.selfcare.v5.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.v5.holder.BlockContactViewHolder;

import java.util.ArrayList;
import java.util.List;

public class SettingListAdapter extends BaseAdapter<BaseViewHolder, Object> {

    public static final int SCREEN_BLOCK_USER = 1;
    public static final int SCREEN_SAVED_MESSAGE = 2;
    private List<Object> items;
    private int screenType;
    private ItemListener itemListener;

    public SettingListAdapter(Activity activity, int screenType) {
        super(activity);
        this.screenType = screenType;
        items = new ArrayList<>();
    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder viewHolder = null;
//        if (screenType == SCREEN_BLOCK_USER) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_saved_message_v5, parent, false);
        viewHolder = new BlockContactViewHolder(view, screenType);
        ((BlockContactViewHolder) viewHolder).setType(screenType);
        ((BlockContactViewHolder) viewHolder).setItemListener(itemListener);

//        } else {

//        }
        return viewHolder;
    }

    public void setItemListener(ItemListener itemListener) {
        this.itemListener = itemListener;
    }



    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.setElement(items.get(position));
    }

    public <T> void addAll(List<T> listItem) {
        if (listItem == null || listItem.isEmpty()) {
            return;
        }
        items.addAll(listItem);
        notifyItemRangeInserted(items.size() - listItem.size(), listItem.size());
    }

    public <T> void swapList(List<T> listItem) {
        items.clear();
        if (listItem != null) {
            items.addAll(listItem);
        }
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface ItemListener {
        void removeItem(int position);

        void itemClick(int position);

        void avatarClickListener(int position);
    }


}

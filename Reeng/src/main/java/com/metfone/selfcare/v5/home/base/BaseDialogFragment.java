package com.metfone.selfcare.v5.home.base;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;

public abstract class BaseDialogFragment extends DialogFragment {

    protected AppCompatButton btnRight;
    protected AppCompatButton btnLeft;
    protected AppCompatTextView tvTitle;
    protected DialogListener selectListener;

    protected abstract int getResId();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.V5DialogRadioButton);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getResId(), container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getDialog().getWindow()
//                .getAttributes().windowAnimations = R.style.V5DialogWindowAnimation;
        initView();
    }

    public void setSelectListener(DialogListener selectListener) {
        this.selectListener = selectListener;
    }

    protected void initView(){
        btnLeft = getView().findViewById(R.id.btnLeft);
        btnRight = getView().findViewById(R.id.btnRight);
        tvTitle = getView().findViewById(R.id.txtTitle);
        if(btnLeft != null){
            btnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getDialog().cancel();
                    if( selectListener != null){
                        selectListener.dialogLeftClick();
                    }
                }
            });
        }
    }


    public interface DialogListener {
        void dialogRightClick(int value);
        void dialogLeftClick();
    }


}

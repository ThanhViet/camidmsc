package com.metfone.selfcare.v5.login;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 2/10/2020.
 */

public class SelectCountryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Region> listData;
    private BaseSlidingFragmentActivity activity;
    private RecyclerClickListener mRecyclerClickListener;

    public SelectCountryAdapter(ArrayList<Region> listData, BaseSlidingFragmentActivity activity, RecyclerClickListener mRecyclerClickListener) {
        this.listData = listData;
        this.activity = activity;
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    public void setListData(ArrayList<Region> listData) {
        this.listData = listData;
    }

    public ArrayList<Region> getListData() {
        return listData;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.holder_select_country, parent, false);
        CountryFlagHolder viewHolder = new CountryFlagHolder(view);
        if (mRecyclerClickListener != null)
            viewHolder.setRecyclerClickListener(mRecyclerClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((CountryFlagHolder) holder).setElement(listData.get(position));
        ((CountryFlagHolder) holder).setViewClick(position, listData.get(position));
    }

    @Override
    public int getItemCount() {
        if (listData == null) return 0;
        return listData.size();
    }

    public class CountryFlagHolder extends BaseViewHolder {

        private ImageView ivFlag, ivCheckDefault;
        private TextView tvCountry;

        private Region region;

        public CountryFlagHolder(View itemView) {
            super(itemView);
            ivFlag = itemView.findViewById(R.id.ivFlCountry);
            ivCheckDefault = itemView.findViewById(R.id.ivCheckDefault);
            tvCountry = itemView.findViewById(R.id.tvCountry);
        }

        @Override
        public void setElement(Object obj) {
            region = (Region) obj;

            String patch = "file:///android_asset/fl_country/" + region.getRegionCode().toLowerCase() + ".png";
            Glide.with(activity)
                    .load(Uri.parse(patch))
                    .apply(new RequestOptions()
                            .error(R.color.gray)
                            .fitCenter())
                    .into(ivFlag);

            String text = region.getRegionName()/* + " (+" + region.getCountryCode() + ")"*/;
            tvCountry.setText(text);

            if (region.isSelected())
                ivCheckDefault.setVisibility(View.VISIBLE);
            else
                ivCheckDefault.setVisibility(View.GONE);
        }
    }
}

package com.metfone.selfcare.v5.utils;

import androidx.transition.ChangeBounds;
import androidx.transition.Slide;
import androidx.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Created by dainv on 12/23/2019
 */
public final class AnimationUtil {

    private static final long ANIMATION_DURATION_DEFAULT = 300;

    public static void animationScale(ViewGroup viewGroup, final View view) {
        ScaleAnimation scaleUp = new ScaleAnimation(0.8f, 1.2f, 0.8f, 1.2f,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleUp.setDuration(400);
        scaleUp.setStartOffset(200);
        scaleUp.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleUp.setAnimationListener(new Animation.AnimationListener() {
                                         @Override
                                         public void onAnimationStart(Animation animation) {

                                         }

                                         @Override
                                         public void onAnimationEnd(Animation animation) {
                                             final ScaleAnimation scaleDown = new ScaleAnimation(1.2f, 1f, 1.2f, 1f,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                             scaleDown.setDuration(200);
                                             scaleDown.setInterpolator(new AccelerateDecelerateInterpolator());
                                             view.startAnimation(scaleDown);
                                         }

                                         @Override
                                         public void onAnimationRepeat(Animation animation) {

                                         }
                                     });
        view.startAnimation(scaleUp);
    }


    public static void slideDownShow(View view, ViewGroup rootView){
        Slide slide = new Slide();
        slide.addTarget(view);
        slide.setSlideEdge(Gravity.TOP);
        slide.setMode(Slide.MODE_IN);
        slide.setDuration(ANIMATION_DURATION_DEFAULT);
        TransitionManager.beginDelayedTransition(rootView, slide);
        view.setVisibility(View.VISIBLE);
    }

    public static void slideUpHide(View view, ViewGroup rootView){
        Slide slide = new Slide();
        slide.addTarget(view);
        slide.setSlideEdge(Gravity.TOP);
        slide.setMode(Slide.MODE_OUT);
        slide.setDuration(ANIMATION_DURATION_DEFAULT);
        TransitionManager.beginDelayedTransition(rootView, slide);
        view.setVisibility(View.GONE);
    }
}

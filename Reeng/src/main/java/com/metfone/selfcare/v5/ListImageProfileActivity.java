package com.metfone.selfcare.v5;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.feedback.FeedbackItemDecoration;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.v5.adapter.ImageViewerAdapter;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by daipr on 4/3/2020
 */
public class ListImageProfileActivity extends BaseSlidingFragmentActivity {

    private static final String LIST_IMAGE_KEY = "list_image_profile_key";
    private static final String TITLE_KEY = "title_key";
    private static final String JID_KEY = "jid_key";

    private ImageViewerAdapter mAdapter;
    @BindView(R.id.recyclerViewListImage)
    RecyclerView recyclerView;
    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView tvTitle;
    @BindView(R.id.icBackToolbar)
    AppCompatImageView icBack;
    private ImageProfileBusiness imageProfileBusiness;
    private ReengAccountBusiness reengAccountBusiness;
    private Unbinder unbinder;
    private EventOnMediaHelper eventOnMediaHelper;
    private ArrayList<ImageProfile> profileArrayList;

    String title = "", jid = "";


    public static void startActivity(BaseSlidingFragmentActivity activity, ArrayList<ImageProfile> imageProfiles, String title, String jidNumber) {
        Intent intent = new Intent(activity, ListImageProfileActivity.class);
        intent.putExtra(LIST_IMAGE_KEY, imageProfiles);
        intent.putExtra(TITLE_KEY, title);
        intent.putExtra(JID_KEY, jidNumber);
        activity.startActivity(intent);
    }

    public static void startActivity(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, ListImageProfileActivity.class);
        activity.startActivity(intent);
    }

    /*public static ListImageFragment newInstance() {
        Bundle args = new Bundle();
        ListImageFragment fragment = new ListImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ListImageFragment newInstance(ArrayList<ImageProfile> imageProfiles, String title, String jidNumber) {
        Bundle args = new Bundle();
        args.putSerializable(LIST_IMAGE_KEY, imageProfiles);
        args.putString(TITLE_KEY, title);
        args.putString(JID_KEY, jidNumber);
        ListImageFragment fragment = new ListImageFragment();
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageProfileBusiness = ApplicationController.self().getImageProfileBusiness();
        reengAccountBusiness = ApplicationController.self().getReengAccountBusiness();
        setContentView(R.layout.fragment_list_image);
        unbinder = ButterKnife.bind(this);


        if (savedInstanceState != null) {
            profileArrayList = (ArrayList<ImageProfile>) savedInstanceState.getSerializable(LIST_IMAGE_KEY);
            title = savedInstanceState.getString(TITLE_KEY);
            jid = savedInstanceState.getString(JID_KEY);
        } else if (getIntent() != null) {
            profileArrayList = (ArrayList<ImageProfile>) getIntent().getSerializableExtra(LIST_IMAGE_KEY);
            title = getIntent().getStringExtra(TITLE_KEY);
            jid = getIntent().getStringExtra(JID_KEY);
        }
        if (profileArrayList == null || profileArrayList.isEmpty()) {
            profileArrayList = imageProfileBusiness.getImageProfileList();
        }
        if (TextUtils.isEmpty(title)) {
            if (TextUtils.isEmpty(jid))
                tvTitle.setText(R.string.my_image);
            else
                tvTitle.setText(jid);
        } else {
            tvTitle.setText(title);
        }
        icBack.setOnClickListener(view -> onBackPressed());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new FeedbackItemDecoration(10, false, 3));
        mAdapter = new ImageViewerAdapter(profileArrayList, this, imageProfileBusiness, (TextUtils.isEmpty(title) && TextUtils.isEmpty(jid)));
        mAdapter.setImageClickListener(position -> {
            if (eventOnMediaHelper == null) {
                eventOnMediaHelper = new EventOnMediaHelper(this);
            }
            if (!TextUtils.isEmpty(title))
                eventOnMediaHelper.showImageDetail(title, jid, profileArrayList, position,
                        FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
            else
                eventOnMediaHelper.showImageDetail(reengAccountBusiness.getCurrentAccount().getName(), reengAccountBusiness.getJidNumber(),
                        null, position, FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });
        recyclerView.setAdapter(mAdapter);
    }


    /*@Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_image, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getArguments() != null) {
            if (getArguments().containsKey(TITLE_KEY)) {
                if (getArguments().getString(TITLE_KEY) != null) {
                    tvTitle.setText(getArguments().getString(TITLE_KEY));
                } else {
                    tvTitle.setText(getArguments().getString(JID_KEY));
                }
            } else {
                tvTitle.setText(R.string.my_image);
            }
        }
        icBack.setOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new FeedbackItemDecoration(10, false, 3));
        if (getArguments() != null && getArguments().containsKey(LIST_IMAGE_KEY)) {
            profileArrayList = (ArrayList<ImageProfile>) getArguments().getSerializable(LIST_IMAGE_KEY);
        } else {
            profileArrayList = imageProfileBusiness.getImageProfileList();
        }
        mAdapter = new ImageViewerAdapter(profileArrayList, getContext(), imageProfileBusiness);
        mAdapter.setImageClickListener(position -> {
            if (eventOnMediaHelper == null) {
                eventOnMediaHelper = new EventOnMediaHelper((BaseSlidingFragmentActivity) getActivity());
            }
            if (getArguments() != null && getArguments().containsKey(LIST_IMAGE_KEY)) {
                eventOnMediaHelper.showImageDetail(getArguments().getString(TITLE_KEY), getArguments().getString(JID_KEY), profileArrayList, position,
                        FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
                return;
            }
            eventOnMediaHelper.showImageDetail(reengAccountBusiness.getCurrentAccount().getName(), reengAccountBusiness.getJidNumber(),
                    null, position, FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });
        recyclerView.setAdapter(mAdapter);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.updateSize();
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        unbinder = null;
        super.onDestroy();
    }

    /*@Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        unbinder = null;
    }*/
}

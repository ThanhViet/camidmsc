package com.metfone.selfcare.v5.utils;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;

/**
 * Created by thanhnt72 on 11/6/2019.
 */

public class ToastUtils {

    private static final int MAX_LENGTH_SHORT_TOAST = 200;

    public static void showToast(Context context, String message, int duration, int resIdCustomImg, ToastType type) {
        if (TextUtils.isEmpty(message)) return;
        ApplicationController app = ApplicationController.self();
        if (app == null) return;
        Toast toast = new Toast(app.getApplicationContext());
        View layout = LayoutInflater.from(app.getApplicationContext()).inflate(R.layout.custom_toast_v2, null, false);
        TextView l1 = layout.findViewById(R.id.text);
        AppCompatImageView img = layout.findViewById(R.id.image);
        l1.setText(message);
        if (type == null)
            img.setVisibility(View.GONE);
        else
            switch (type) {
                case customImage:
                    img.setImageResource(resIdCustomImg);
                    break;
                case done:
                    img.setImageResource(R.drawable.ic_v5_done);
                    break;
                case error:
                    break;

            }
        toast.setView(layout);
        toast.setDuration(duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showToastSuccess(Context context, String message) {
        int duration;
        if (message != null && message.length() > MAX_LENGTH_SHORT_TOAST)
            duration = Toast.LENGTH_LONG;
        else
            duration = Toast.LENGTH_SHORT;
        showToast(context, message, duration, R.drawable.ic_tick_v5, ToastType.customImage);

    }

    public static void showToast(Context context, String message) {
        int duration;
        if (message != null && message.length() > MAX_LENGTH_SHORT_TOAST)
            duration = Toast.LENGTH_LONG;
        else
            duration = Toast.LENGTH_SHORT;
        showToast(context, message, duration, 0, null);

    }

    public enum ToastType {
        done, error, customImage
    }
}

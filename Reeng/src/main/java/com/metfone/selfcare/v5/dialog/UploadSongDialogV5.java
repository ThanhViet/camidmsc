package com.metfone.selfcare.v5.dialog;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.widget.MochaProgressBarV5;

/**
 * Created by dainv00 on 5/25/2020
 */
public class UploadSongDialogV5 extends BaseDialogFragment {

    private boolean isUploadCompleted;
    private View viewDivider;
    private MochaProgressBarV5 mochaProgressBarV5;
    private AppCompatTextView tvMessage;


    public static UploadSongDialogV5 newInstance() {
        Bundle args = new Bundle();
        UploadSongDialogV5 fragment = new UploadSongDialogV5();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogSettingWithPercent);
        setCancelable(false);
    }

    @Override
    protected void initView() {
        super.initView();
        tvMessage = getView().findViewById(R.id.tvMessage);
        tvMessage.setText(R.string.song_uploading_des);
        mochaProgressBarV5 = getView().findViewById(R.id.progressBar);
        viewDivider = getView().findViewById(R.id.lineVertical);
        viewDivider.setVisibility(View.GONE);
        btnLeft.setVisibility(View.GONE);
        btnRight.getLayoutParams().width = ConstraintLayout.LayoutParams.MATCH_PARENT;
        tvTitle.setText(getString(R.string.song_uploading, "0%"));
        btnRight.setOnClickListener(v -> {
            if (isUploadCompleted) {
                if (selectListener != null) {
                    selectListener.dialogRightClick(1);
                }

            } else {
                if (selectListener != null) {
                    selectListener.dialogRightClick(0); //cancel upload
                }
            }
            dismiss();
            getDialog().cancel();
        });
    }

    public void updateProgress(int progress) {
        if(progress == 100) {
            tvTitle.setText(getString(R.string.song_uploading,"99%"));
        }else {
            tvTitle.setText(getString(R.string.song_uploading, progress + "%"));
        }
        mochaProgressBarV5.setSmoothProgress(progress);
    }

    public void uploadCompleted() {
        isUploadCompleted = true;
        mochaProgressBarV5.setVisibility(View.GONE);
        tvTitle.setText(R.string.upload_song_completed);
        tvMessage.setText(R.string.upload_song_completed_des);
        getView().findViewById(R.id.lineVertical).setVisibility(View.VISIBLE);
        btnLeft.setVisibility(View.VISIBLE);
        viewDivider.setVisibility(View.VISIBLE);
        btnRight.getLayoutParams().width = 0;
        btnRight.setText(R.string.listen_together);
    }

    public void setError(String message){
        tvMessage.setText(message);
        mochaProgressBarV5.setProgress(0);
    }

    @Override
    protected int getResId() {
        return R.layout.dialog_upload_song_v5;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.v5.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.activity.SetUpProfileActivity;
import com.metfone.selfcare.activity.WebViewNewActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.listeners.VipInfoChangeListener;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.tab_home.adapter.TabHomeAdapter;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.HomeContact;
import com.metfone.selfcare.module.tab_home.model.HomeContent;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.network.TabHomeApi;
import com.metfone.selfcare.module.tab_home.utils.ConvertHelper;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class TabHomeFragmentV2 extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, TabHomeListener.OnAdapterClick, InitDataListener
        , FeedBusiness.FeedBusinessInterface, ProfileListener, ReengMessageListener
        , ContentConfigBusiness.OnConfigChangeListener, VipInfoChangeListener, OnClickMoreItemListener {

    @BindView(R.id.iv_menu)
    @Nullable
    AppCompatImageView btnMenu;
    @BindView(R.id.layout_search_view)
    @Nullable
    View viewSearch;
    @BindView(R.id.iv_notify)
    @Nullable
    AppCompatImageView ivNotify;
    @BindView(R.id.tv_hint_search)
    @Nullable
    TextView tvHintSearch;

    @BindView(R.id.tv_contact_avatar)
    TextView tvContactAvatar;
    @BindView(R.id.iv_profile_avatar)
    AppCompatImageView ivProfileAvatar;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;

    private boolean isLoading;
    private ListenerUtils mListenerUtils;
    private ArrayList<TabHomeModel> data;
    private TabHomeAdapter adapter;
    private TabHomeModel listBanners;
    private TabHomeModel listFeatures;
    private TabHomeModel listContacts;
    private Handler mHandler;
    private LinearLayoutManager layoutManager;
    private Runnable runnableLoadFeatures = () -> {
        Log.d(TAG, "runnableLoadFeatures");
        loadFeaturesData();
    };
    private ApiCallbackV2<TabHomeApi.TabHomeResponse> apiCallback = new ApiCallbackV2<TabHomeApi.TabHomeResponse>() {

        @Override
        public void onSuccess(String msg, TabHomeApi.TabHomeResponse result) throws JSONException {
            isDataInitiated = true;
            isLoading = false;
            if (data == null) data = new ArrayList<>();
            data.clear();
            //quick contact
            if (listContacts != null) data.add(listContacts);
            //banner
            ArrayList<Content> banners = result.getListBanners();
            if (Utilities.notEmpty(banners)) {
                int size = banners.size() - 1;
                for (int i = size; i >= 0; i--) {
                    if (banners.get(i) == null) banners.remove(i);
                }
            }
            if (Utilities.notEmpty(banners)) {
                listBanners = new TabHomeModel();
                listBanners.setType(TabHomeModel.TYPE_SLIDER_BANNER);
                listBanners.getList().addAll(banners);
                data.add(listBanners);
            } else {
                listBanners = null;
            }

            //feature
            if (listFeatures != null) data.add(listFeatures);
            //box content
            try {
                ArrayList<HomeContent> contents = result.getListContents();
                if (Utilities.notEmpty(contents)) {
                    for (HomeContent homeContent : contents) {
                        if (homeContent != null) {
                            homeContent.checkEmpty();
                            if (!homeContent.isEmpty()) {
                                TabHomeModel model = new TabHomeModel();
                                if (homeContent.isVideo()) {
                                    model.setType(TabHomeModel.TYPE_BOX_VIDEO);
                                    if (Utilities.notEmpty(homeContent.getListFlashHot())) {
                                        int count = 0;
                                        for (Content content : homeContent.getListFlashHot()) {
                                            if (content != null) {
                                                TabHomeModel largeItem = new TabHomeModel();
                                                largeItem.setType(TabHomeModel.TYPE_LARGE_VIDEO);
                                                largeItem.setObject(content);
                                                model.getList().add(largeItem);
                                                count++;
                                                if (count == 2) break;
                                            }
                                        }
                                    }
                                    if (Utilities.notEmpty(homeContent.getListPlayHot())) {
                                        TabHomeModel gridItem = new TabHomeModel();
                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_VIDEO);
                                        gridItem.getList().addAll(homeContent.getListPlayHot());
                                        model.getList().add(gridItem);
                                    }
//                                    if (Utilities.notEmpty(homeContent.getListChannelHot())) {
//                                        TabHomeModel gridItem = new TabHomeModel();
//                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_CHANNEL);
//                                        gridItem.getList().addAll(homeContent.getListChannelHot());
//                                        model.getList().add(gridItem);
//                                    }
                                } else if (homeContent.isMusic()) {
                                    model.setType(TabHomeModel.TYPE_BOX_MUSIC);
                                    if (Utilities.notEmpty(homeContent.getListFlashHot())) {
                                        int count = 0;
                                        for (Content content : homeContent.getListFlashHot()) {
                                            if (content != null) {
                                                TabHomeModel largeItem = new TabHomeModel();
                                                largeItem.setType(TabHomeModel.TYPE_LARGE_MUSIC);
                                                largeItem.setObject(content);
                                                model.getList().add(largeItem);
                                                count++;
                                                if (count == 2) break;
                                            }
                                        }
                                    }
                                    if (Utilities.notEmpty(homeContent.getListPlayHot())) {
                                        TabHomeModel gridItem = new TabHomeModel();
                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_MUSIC);
                                        gridItem.getList().addAll(homeContent.getListPlayHot());
                                        model.getList().add(gridItem);
                                    }
                                } else if (homeContent.isMovie()) {
                                    model.setType(TabHomeModel.TYPE_BOX_MOVIE);
                                    if (Utilities.notEmpty(homeContent.getListFlashHot())) {
                                        int count = 0;
                                        for (Content content : homeContent.getListFlashHot()) {
                                            if (content != null) {
                                                TabHomeModel largeItem = new TabHomeModel();
                                                largeItem.setType(TabHomeModel.TYPE_LARGE_MOVIE);
                                                largeItem.setObject(content);
                                                model.getList().add(largeItem);
                                                count++;
                                                if (count == 2) break;
                                            }
                                        }
                                    }
                                    if (Utilities.notEmpty(homeContent.getListPlayHot())) {
                                        TabHomeModel gridItem = new TabHomeModel();
                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_MOVIE);
                                        gridItem.getList().addAll(homeContent.getListPlayHot());
                                        model.getList().add(gridItem);
                                    }
                                } else if (homeContent.isNews()) {
                                    model.setType(TabHomeModel.TYPE_BOX_NEWS);
                                    if (Utilities.notEmpty(homeContent.getListFlashHot())) {
                                        int count = 0;
                                        for (Content content : homeContent.getListFlashHot()) {
                                            if (content != null) {
                                                TabHomeModel largeItem = new TabHomeModel();
                                                largeItem.setType(TabHomeModel.TYPE_LARGE_NEWS);
                                                largeItem.setObject(content);
                                                model.getList().add(largeItem);
                                                count++;
                                                if (count == 2) break;
                                            }
                                        }
                                    }
                                    if (Utilities.notEmpty(homeContent.getListPlayHot())) {
                                        TabHomeModel gridItem = new TabHomeModel();
                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_NEWS);
                                        gridItem.getList().addAll(homeContent.getListPlayHot());
                                        model.getList().add(gridItem);
                                    }
                                } else if (homeContent.isComic()) {
                                    model.setType(TabHomeModel.TYPE_BOX_COMIC);
                                    if (Utilities.notEmpty(homeContent.getListFlashHot())) {
                                        int count = 0;
                                        for (Content content : homeContent.getListFlashHot()) {
                                            if (content != null) {
                                                TabHomeModel largeItem = new TabHomeModel();
                                                largeItem.setType(TabHomeModel.TYPE_LARGE_COMIC);
                                                largeItem.setObject(content);
                                                model.getList().add(largeItem);
                                                count++;
                                                if (count == 2) break;
                                            }
                                        }
                                    }
                                    if (Utilities.notEmpty(homeContent.getListPlayHot())) {
                                        TabHomeModel gridItem = new TabHomeModel();
                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_COMIC);
                                        gridItem.getList().addAll(homeContent.getListPlayHot());
                                        model.getList().add(gridItem);
                                    }
                                } else if (homeContent.isTiin()) {
                                    model.setType(TabHomeModel.TYPE_BOX_TIIN);
                                    if (Utilities.notEmpty(homeContent.getListFlashHot())) {
                                        int count = 0;
                                        for (Content content : homeContent.getListFlashHot()) {
                                            if (content != null) {
                                                TabHomeModel largeItem = new TabHomeModel();
                                                largeItem.setType(TabHomeModel.TYPE_LARGE_TIIN);
                                                largeItem.setObject(content);
                                                model.getList().add(largeItem);
                                                count++;
                                                if (count == 2) break;
                                            }
                                        }
                                    }
                                    if (Utilities.notEmpty(homeContent.getListPlayHot())) {
                                        TabHomeModel gridItem = new TabHomeModel();
                                        gridItem.setType(TabHomeModel.TYPE_BOX_GRID_TIIN);
                                        gridItem.getList().addAll(homeContent.getListPlayHot());
                                        model.getList().add(gridItem);
                                    }
                                }
                                model.setTitle(homeContent.getTitle());
                                model.setId(homeContent.getId());
                                data.add(model);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }

            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            hideRefresh();
            if (Utilities.isEmpty(data)) showLoadedEmpty();
            else {
                showLoadedSuccess();
            }
        }

        @Override
        public void onError(String s) {
            isLoading = false;
            hideRefresh();
            if (Utilities.isEmpty(data)) {
                if (data == null) data = new ArrayList<>();
                data.clear();
                //banner
                listBanners = null;
                //quick contact
                if (listContacts != null) {
                    data.add(listContacts);
                }
                //feature
                if (listFeatures != null) {
                    data.add(listFeatures);
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                if (Utilities.isEmpty(data))
                    showLoadedError();
                else
                    showLoadedSuccess();
            }
        }

        @Override
        public void onComplete() {

        }
    };

    public static TabHomeFragmentV2 newInstance() {
        Bundle args = new Bundle();
        TabHomeFragmentV2 fragment = new TabHomeFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_tab_home_v2;
    }

    @Override
    public void onCreateView() {
        swipeRefreshLayout.setColorSchemeColors(getColor());
        swipeRefreshLayout.setOnRefreshListener(this);
        if (data == null) data = new ArrayList<>();
        else data.clear();
        //banner
        listBanners = null;
        //feature
        listFeatures = null;
        //contact
        listContacts = null;
        adapter = new TabHomeAdapter(activity);
        adapter.setItems(data);
        adapter.setListener(this);
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//            }
//        });
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayout.VERTICAL, false);
        BaseAdapter.setupVerticalRecyclerView(activity, recyclerView, layoutManager, adapter, true, 5);
        if (btnMenu != null) btnMenu.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity == null) return;
                // TODO: [START] Cambodia version
//                NavigateActivityHelper.navigateToSettingActivity(activity, -1);
                NavigateActivityHelper.navigateToSettingKhActivity(activity, -1);
                // TODO: [END] Cambodia version
            }
        });
        if (viewSearch != null) viewSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity == null) return;
                Utilities.openSearch(activity, Constants.TAB_HOME);
            }
        });
        if (ivNotify != null) ivNotify.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (app == null || activity == null) return;
                if (app.getFeedBusiness().getTotalNotify() != 0) {
                    app.getFeedBusiness().setTotalNotify(0);
                    app.getFeedBusiness().setNotifySeen("");
                    drawNotify();
                }
                app.trackingEvent(R.string.ga_category_onmedia, R.string.ga_onmedia_action_notify,
                        R.string.ga_onmedia_label_open_notify);
                app.cancelNotification(Constants.NOTIFICATION.NOTIFY_ONMEDIA);
                Intent intent = new Intent(activity, OnMediaActivityNew.class);
                intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.NOTIFICATION);
                startActivity(intent);
            }
        });
        if (ivProfileAvatar != null)
            ivProfileAvatar.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (app == null || activity == null) return;
                    if (app.getReengAccountBusiness().isAnonymousLogin()) {
                        activity.loginFromAnonymous();
                    } else {
                        Intent intent = new Intent(activity, SetUpProfileActivity.class);
                        activity.startActivity(intent);

                    }
                }
            });
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.bg_mocha);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        drawProfile();
        drawNotify();
        drawSetting();
        mHandler = new Handler();
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        ListenerHelper.getInstance().addConfigChangeListener(this);
        ListenerHelper.getInstance().addProfileChangeListener(this);
        app.getFeedBusiness().setFeedBusinessInterface(this);
        app.getMessageBusiness().addReengMessageListener(this);
        app.getReengAccountBusiness().addVipInfoChangeListener(this);
        TabHomeApi.TabHomeResponse tmp = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_DATA_TAB_HOME_NEW, TabHomeApi.TabHomeResponse.class);
        if (tmp != null && apiCallback != null) {
            try {
                apiCallback.onSuccess("", tmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (app.getReengAccountBusiness().isValidAccount()) {
            loadFeaturesData();
            if (!app.getReengAccountBusiness().isAnonymousLogin()) {
                loadNumberNotify();
                //loadContactsData();// đã load ở onResume
            }
        }
        new Handler().postDelayed(() -> loadData(Utilities.isEmpty(data)), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
    }

    @Override
    public void onDestroyView() {
        if (adapter != null) adapter.setListener(null);
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        ListenerHelper.getInstance().removeConfigChangeListener(this);
        ListenerHelper.getInstance().removeProfileChangeListener(this);
        if (app != null) {
            app.getFeedBusiness().setFeedBusinessInterface(null);
            app.getMessageBusiness().removeReengMessageListener(this);
            app.getReengAccountBusiness().removeVipInfoChangeListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad())
            loadData(Utilities.isEmpty(data));
        if (isVisibleToUser && isAdded() && app != null) {
            autoLoadData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (app != null && app.isDataReady()) {
            drawSetting();
            if (!app.getReengAccountBusiness().isAnonymousLogin()) {
                drawProfile();
                if (mHandler != null) mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadContactsData();
                    }
                });
            }
        }
    }

    private void loadNumberNotify() {
        if (app.getReengAccountBusiness().isAnonymousLogin()) return;
        Log.i(TAG, "loadNumberNotify");
        new WSOnMedia(app).getNumberNotify(new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "loadNumberNotify: " + s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                        int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                        if (code == HTTPCode.E200_OK) {
                            int number = jsonObject.optInt("number");
                            if (app != null) {
                                if (number != app.getFeedBusiness().getTotalNotify()) {
                                    app.getFeedBusiness().setTotalNotify(number);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                Log.e(TAG, e);
            }
        });
    }

    private synchronized void loadContactsData() {
        Log.i(TAG, "loadContactsData");
        if (Utilities.notEmpty(data)) {
            int size = data.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (data.get(i) != null && data.get(i).getType() == TabHomeModel.TYPE_QUICK_CONTACT) {
                    data.remove(i);
                }
            }
        }
        ArrayList<HomeContact> list = TabHomeUtils.getListQuickContacts();
        if (Utilities.notEmpty(list)) {
            listContacts = new TabHomeModel();
            if (res != null) listContacts.setTitle(res.getString(R.string.tab_home_quick_contact));
            listContacts.setType(TabHomeModel.TYPE_QUICK_CONTACT);
            listContacts.getList().addAll(list);
            if (data != null) data.add(0, listContacts);
        } else listContacts = null;
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        if (Utilities.notEmpty(data)) showLoadedSuccess();
    }

    private synchronized void loadFeaturesData() {
        Log.i(TAG, "loadFeaturesData");
        if (Utilities.notEmpty(data)) {
            int size = data.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (data.get(i) != null && data.get(i).getType() == TabHomeModel.TYPE_FEATURE) {
                    data.remove(i);
                }
            }
        }
        ArrayList<ItemMoreHome> list = new ArrayList<>();
        list.addAll(TabHomeUtils.getListNotActiveTabs(app));
        list.addAll(TabHomeUtils.getListFunctions());
        if (Utilities.notEmpty(list)) {
            listFeatures = new TabHomeModel();
            if (res != null) listFeatures.setTitle(res.getString(R.string.featured));
            listFeatures.setType(TabHomeModel.TYPE_FEATURE);
            listFeatures.getList().addAll(list);
            int index = 0;
            if (listContacts != null) index++;
            if (listBanners != null) index++;
            if (data != null) data.add(index, listFeatures);
        } else listFeatures = null;
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        if (Utilities.notEmpty(data)) showLoadedSuccess();
    }

    private void showLoading() {
        hideRefresh();
        if (loadingView != null) loadingView.showLoading();
    }

    private void showLoadedEmpty() {
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (loadingView != null) loadingView.showLoadedEmpty();
    }

    private void showLoadedSuccess() {
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
        if (loadingView != null) loadingView.showLoadedSuccess();
    }

    private void showLoadedError() {
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (loadingView != null) loadingView.showLoadedError();
    }

    private void loadData(boolean showLoading) {
        Log.i(TAG, "loadData isLoading: " + isLoading + ", showLoading: " + showLoading);
        if (isLoading) return;
        isLoading = true;
        if (showLoading) showLoading();
        new TabHomeApi().getHome(apiCallback);
    }

    @Override
    public void onRefresh() {
        if (isLoading) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        } else {
            if (mHandler != null) mHandler.post(new Runnable() {
                @Override
                public void run() {
                    loadFeaturesData();
                    loadContactsData();
                }
            });
        }
        loadData(false);
        loadNumberNotify();
        AdsManager.getInstance().reloadAdsNative();
    }

    @Override
    public void onInternetChanged() {
        Log.i(TAG, "onInternetChanged");
        if (NetworkHelper.isConnectInternet(activity) && canLazyLoad())
            loadData(Utilities.isEmpty(data));
    }

    @Override
    public void onDataReady() {
        Log.i(TAG, "onDataReady");
        if (app != null) app.getMessageBusiness().addReengMessageListener(this);
    }

    @Override
    public void onUpdateNotifyFeed(int totalNotify) {
        Log.i(TAG, "onUpdateNotifyFeed totalNotify: " + totalNotify);
        drawNotify();
    }

    @Override
    public void onClickTitleBoxContact() {
        if (activity instanceof HomeActivity) {
            ((HomeActivity) activity).openContacts();
        }
    }

    @Override
    public void onClickContact(ThreadMessage item) {
        NavigateActivityHelper.navigateToChatDetail(activity, item);
    }

    @Override
    public void onClickFeature(ItemMoreHome item, int position) {
        TabHomeUtils.openFeature(activity, item);

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onProfileChange() {
    }

    @Override
    public void onRequestFacebookChange(String fullName, String birthDay, int gender) {
    }

    @Override
    public void onAvatarChange(String avatarPath) {
        Log.i(TAG, "onAvatarChange avatarPath: " + avatarPath);
        drawProfile();
    }

    @Override
    public void onCoverChange(String coverPath) {
    }

    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
        Log.i(TAG, "notifyNewIncomingMessage");
        if (mHandler != null) mHandler.post(new Runnable() {
            @Override
            public void run() {
                loadContactsData();
            }
        });
    }

    @Override
    public void onRefreshMessage(int threadId) {
        Log.i(TAG, "onRefreshMessage");
        if (mHandler != null) mHandler.post(new Runnable() {
            @Override
            public void run() {
                loadContactsData();
            }
        });
    }

    @Override
    public void notifyNewOutgoingMessage() {
    }

    @Override
    public void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {
    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {
    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {
    }

    @Override
    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {
    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {
    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {
    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {
    }

    @Override
    public void onUpdateStateRoom() {
    }

    @Override
    public void onBannerDeepLinkUpdate(com.metfone.selfcare.database.model.ReengMessage message, ThreadMessage mCorrespondingThread) {
    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onConfigStickyBannerChanged() {

    }

    @Override
    public void onConfigTabChange() {

    }

    @Override
    public void onConfigBackgroundHeaderHomeChange() {
        Log.i(TAG, "onConfigBackgroundHeaderHomeChange");
//        if (activity != null && !activity.isFinishing()) {
//            activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    ImageBusiness.downloadAndSaveBackgroundHeaderHome();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (activity != null && !activity.isFinishing()) {
//                                if (listFeatures != null) {
//                                    listFeatures.setLastTime();
//                                    if (homeHeader != null) homeHeader.setFeature(listFeatures);
//                                    notifyHeaderProfile();
//                                }
//                            }
//                        }
//                    }, 2000);
//                }
//            });
//        }
    }

    @Override
    public void onChange() {
        Log.i(TAG, "onChange LoadFeatures");
        if (app != null && app.getReengAccountBusiness().isValidAccount()) {
            if (mHandler != null) {
                mHandler.removeCallbacks(runnableLoadFeatures);
                mHandler.post(runnableLoadFeatures);
            }
        }
    }

    @Override
    public void onMoreConfigChange() {
        Log.i(TAG, "onMoreConfigChange LoadFeatures");
        if (app != null && app.getReengAccountBusiness().isValidAccount()) {
            if (mHandler != null) {
                mHandler.removeCallbacks(runnableLoadFeatures);
                mHandler.post(runnableLoadFeatures);
            }
        }
    }

    @Override
    public void onAVNOChange() {
        //loadFeaturesData();
    }

    private void autoLoadData() {
        Log.d(TAG, "autoLoadData");
        if (app != null && app.isDataReady()) {
            long lastTime = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LAST_TIME_DATA_TAB_HOME, Long.class);
            if (Math.abs(System.currentTimeMillis() - lastTime) > 300000) {
                Log.d(TAG, "autoLoadData start");
                loadData(false);
                if (!app.getReengAccountBusiness().isAnonymousLogin()) {
                    if (mHandler != null) mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            loadNumberNotify();
                            loadContactsData();
                        }
                    });
                }
            }
        }
    }

    private void drawProfile() {
        if (app == null) return;
        if (app.getReengAccountBusiness().isAnonymousLogin()) {
            if (ivProfileAvatar != null) {
                ivProfileAvatar.setVisibility(View.VISIBLE);
                ivProfileAvatar.setImageResource(R.drawable.ic_avatar_default);
            }
            if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
        } else {
            ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
            if (account != null) {
                String lAvatar = account.getLastChangeAvatar();
                if (TextUtils.isEmpty(lAvatar)) {
                    if (ivProfileAvatar != null) {
                        ivProfileAvatar.setVisibility(View.VISIBLE);
                        ivProfileAvatar.setImageResource(R.drawable.ic_avatar_default);
                    }
                    if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
                } else {
                    app.getAvatarBusiness().setMyAvatar(ivProfileAvatar, tvContactAvatar
                            , null, account, null);
                }
            }
        }
        if (tvHintSearch != null) {
            String hint = app.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.HOME_SEACH_HINT);
            if (!TextUtils.isEmpty(hint) && !hint.equals("-")) {
                tvHintSearch.setText(hint);
            } else
                tvHintSearch.setText(R.string.home_hint_search);
        }
    }

    private void drawNotify() {
        if (app == null) return;
        if (ivNotify != null) {
            ivNotify.setVisibility(app.getReengAccountBusiness().isAnonymousLogin() ? View.GONE : View.VISIBLE);
            if (app.getFeedBusiness().getTotalNotify() == 0) {
                ivNotify.setImageResource(R.drawable.ic_noti_home);
            } else {
                ivNotify.setImageResource(R.drawable.ic_noti_home_new);
            }
        }
    }

    private void drawSetting() {
        if (app == null || btnMenu == null) return;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (app.getConfigBusiness().isUpdate()) {
                btnMenu.setImageResource(R.drawable.ic_setting_home_new);
            } else {
                btnMenu.setImageResource(R.drawable.ic_setting_home);
            }
        } else {
            btnMenu.setImageResource(R.drawable.ic_setting_home);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabHomeEvent event) {
        Log.i(TAG, "onMessageEvent event: " + event);
        if (event.isReselected()) {
            if (layoutManager != null && recyclerView != null) {
                recyclerView.stopScroll();
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            }
        }
        if (event.isUpdateNotify()) {
            if (app != null && !app.getReengAccountBusiness().isAnonymousLogin()) {
                loadNumberNotify();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEvent(final TabHomeEvent event) {
        Log.i(TAG, "onEvent event: " + event);
        if (event.isUpdateFeature()) {
            if (app != null && app.getReengAccountBusiness().isValidAccount()) {
                if (mHandler != null) mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadFeaturesData();
                    }
                });
            }
        }
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Override
    public void onClickTitleBox(TabHomeModel item, int position) {
        if (activity == null || item == null) return;
        switch (item.getType()) {
            case TabHomeModel.TYPE_QUICK_CONTACT:
                if (activity instanceof HomeActivity) {
                    ((HomeActivity) activity).openContacts();
                }
                break;
            case TabHomeModel.TYPE_BOX_VIDEO:
                TabHomeUtils.openTabVideo(activity);
                break;
            case TabHomeModel.TYPE_BOX_MUSIC:
                TabHomeUtils.openTabMusic(activity);
                break;
            case TabHomeModel.TYPE_BOX_MOVIE:
                TabHomeUtils.openTabMovie(activity);
                break;
            case TabHomeModel.TYPE_BOX_NEWS:
                TabHomeUtils.openTabNews(activity);
                break;
            case TabHomeModel.TYPE_BOX_COMIC:
                TabHomeUtils.openTabWap(activity, item.getId());
                break;
            case TabHomeModel.TYPE_BOX_TIIN:
                TabHomeUtils.openTabTiin(activity);
                break;
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickComicItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.COMIC), null);
            String url = ((Content) item).getUrl();
            if (Utilities.notEmpty(url)) {
                if (URLUtil.isNetworkUrl(url))
                    Utilities.gotoWebViewFullScreen(app, activity, url, WebViewNewActivity.ORIENTATION_AUTO, "", "", false);
                else
                    DeepLinkHelper.getInstance().openSchemaLink(activity, url);
            }
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.COMIC), null);
                String url = ((Content) model.getObject()).getUrl();
                if (Utilities.notEmpty(url)) {
                    if (URLUtil.isNetworkUrl(url))
                        Utilities.gotoWebViewFullScreen(app, activity, url, WebViewNewActivity.ORIENTATION_AUTO, "", "", false);
                    else
                        DeepLinkHelper.getInstance().openSchemaLink(activity, url);
                }
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreComicItem(Object item, int position) {
        if (item instanceof Content) {
            DialogUtils.showOptionComicItem(activity, item, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                DialogUtils.showOptionComicItem(activity, model.getObject(), this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.MOVIE), null);
            Movie movie = ConvertHelper.convert2Movie((Content) item);
            app.getApplicationComponent().providesUtils().openMovieDetail(activity, movie);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.MOVIE), null);
                Movie movie = ConvertHelper.convert2Movie((Content) model.getObject());
                app.getApplicationComponent().providesUtils().openMovieDetail(activity, movie);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (item instanceof Content) {
            Movie movie = ConvertHelper.convert2Movie((Content) item);
            DialogUtils.showOptionMovieItem(activity, movie, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Movie movie = ConvertHelper.convert2Movie((Content) model.getObject());
                DialogUtils.showOptionMovieItem(activity, movie, this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMusicItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.MUSIC), null);
            AllModel music = ConvertHelper.convert2Music((Content) item);
            TabHomeUtils.openMusicKeeng(activity, music);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.MUSIC), null);
                AllModel music = ConvertHelper.convert2Music((Content) model.getObject());
                TabHomeUtils.openMusicKeeng(activity, music);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreMusicItem(Object item, int position) {
        if (item instanceof Content) {
            AllModel music = ConvertHelper.convert2Music((Content) item);
            DialogUtils.showOptionMusicItem(activity, music, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                AllModel music = ConvertHelper.convert2Music((Content) model.getObject());
                DialogUtils.showOptionMusicItem(activity, music, this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickNewsItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.NEWS), null);
            NewsModel news = ConvertHelper.convert2News((Content) item);
            CommonUtils.readDetailNews(activity, news, false);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.NEWS), null);
                NewsModel news = ConvertHelper.convert2News((Content) model.getObject());
                CommonUtils.readDetailNews(activity, news, false);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreNewsItem(Object item, int position) {
        if (item instanceof Content) {
            NewsModel news = ConvertHelper.convert2News((Content) item);
            DialogUtils.showOptionNewsItem(activity, news, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                NewsModel news = ConvertHelper.convert2News((Content) model.getObject());
                DialogUtils.showOptionNewsItem(activity, news, this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickVideoItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.VIDEO), null);
            Content content = (Content) item;
            Video video = ConvertHelper.convert2Video(content);
            app.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.VIDEO), null);
                Content content = (Content) model.getObject();
                Video video = ConvertHelper.convert2Video(content);
                app.getApplicationComponent().providesUtils().openVideoDetail(activity, video);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreVideoItem(Object item, int position) {
        if (item instanceof Content) {
            Content content = (Content) item;
            Video video = ConvertHelper.convert2Video(content);
            DialogUtils.showOptionVideoItem(activity, video, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                Video video = ConvertHelper.convert2Video(content);
                DialogUtils.showOptionVideoItem(activity, video, this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickChannelVideoItem(Object item, int position) {
        if (item instanceof Content) {
            Content content = (Content) item;
            Video video = ConvertHelper.convert2Video(content);
            Channel channel = video.getChannel();
            if (channel != null)
                app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                Video video = ConvertHelper.convert2Video(content);
                Channel channel = video.getChannel();
                if (channel != null)
                    app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickChannelItem(Object item, int position) {
        if (item instanceof Content) {
            Content content = (Content) item;
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(content, LogApi.ContentType.VIDEO), null);
            Channel channel = ConvertHelper.convert2ChannelVideo(content);
            app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(content, LogApi.ContentType.VIDEO), null);
                Channel channel = ConvertHelper.convert2ChannelVideo(content);
                app.getApplicationComponent().providesUtils().openChannelInfo(activity, channel);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreChannelItem(Object item, int position) {
        if (item instanceof Content) {
            Content content = (Content) item;
            Channel channel = ConvertHelper.convert2ChannelVideo(content);
            DialogUtils.showOptionChannelItem(activity, channel, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                Content content = (Content) model.getObject();
                Channel channel = ConvertHelper.convert2ChannelVideo(content);
                DialogUtils.showOptionChannelItem(activity, channel, this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickSliderBannerItem(Object item, int position) {
        if (item instanceof Content) {
            Content model = (Content) item;
            app.getLogApi().logAction(LogApi.LogType.CLICK_BANNER, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(model, LogApi.ContentType.BANNER), null);
            String itemType = model.getItemType();
            if ("deeplink".equals(itemType)) {
                DeepLinkHelper.getInstance().openSchemaLink(activity, model.getUrl());
            } else {
                Utilities.processOpenLink(app, activity, model.getUrl());
            }
        } else if (item instanceof TabHomeModel) {
            TabHomeModel provisional = (TabHomeModel) item;
            if (provisional.getObject() instanceof Content) {
                Content model = (Content) provisional.getObject();
                app.getLogApi().logAction(LogApi.LogType.CLICK_BANNER, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log(model, LogApi.ContentType.BANNER), null);
                String itemType = model.getItemType();
                if ("deeplink".equals(itemType)) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, model.getUrl());
                } else {
                    Utilities.processOpenLink(app, activity, model.getUrl());
                }
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        ShareUtils.openShareMenu(activity, object);
                    }
                    break;
                case Constants.MENU.MENU_SAVE_VIDEO:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        if (object instanceof Video) {
                            VideoDataSource.getInstance((ApplicationController) activity.getApplication()).saveVideoFromMenu((Video) object);
                            activity.showToast(R.string.videoSavedToLibrary);
                        }
                    }
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        FeedModelOnMedia feed = null;
                        if (object instanceof Movie) {
                            feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                        } else if (object instanceof Video) {
                            feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia((Video) object);
                        } else if (object instanceof AllModel) {
                            feed = FeedModelOnMedia.convertMediaToFeedModelOnMedia((AllModel) object);
                        } else if (object instanceof NewsModel) {
                            feed = FeedModelOnMedia.convertNewsToFeedModelOnMedia((NewsModel) object);
                        } else if (object instanceof TiinModel) {
                            feed = FeedModelOnMedia.convertTiinToFeedModelOnMedia((TiinModel) object);
                        }
                        if (feed != null) {
                            new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                    , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                    , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                        @Override
                                        public void onError(String s) {

                                        }

                                        @Override
                                        public void onComplete() {

                                        }

                                        @Override
                                        public void onSuccess(String msg, String result) throws JSONException {
                                            if (activity != null)
                                                activity.showToast(R.string.add_favorite_success);
                                        }
                                    });
                        }
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                        activity.showDialogLogin();
                    } else {
                        if (object instanceof Video) {
                            VideoDataSource.getInstance((ApplicationController) activity.getApplication()).watchLaterVideoFromMenu((Video) object);
                            activity.showToast(R.string.add_later_success);
                        }
                        if (object instanceof Movie) {
                            new MovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                                @Override
                                public void onError(String s) {

                                }

                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onSuccess(String msg, String result) throws JSONException {
                                    if (activity != null)
                                        activity.showToast(R.string.add_later_success);
                                }
                            });
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    @Override
    public void onClickTiinItem(Object item, int position) {
        if (item instanceof Content) {
            app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) item, LogApi.ContentType.TIIN), null);
            TiinModel tiin = ConvertHelper.convert2Tiin((Content) item);
            CommonUtils.readDetailTiin(activity, tiin, false);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                app.getLogApi().logAction(LogApi.LogType.CLICK_CONTENT, LogApi.TabType.TAB_HOME, ConvertHelper.convert2Log((Content) model.getObject(), LogApi.ContentType.TIIN), null);
                TiinModel tiin = ConvertHelper.convert2Tiin((Content) model.getObject());
                CommonUtils.readDetailTiin(activity, tiin, false);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }

    @Override
    public void onClickMoreTiinItem(Object item, int position) {
        if (item instanceof Content) {
            TiinModel tiin = ConvertHelper.convert2Tiin((Content) item);
            DialogUtils.showOptionTiinItem(activity, tiin, this);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                TiinModel tiin = ConvertHelper.convert2Tiin((Content) model.getObject());
                DialogUtils.showOptionTiinItem(activity, tiin, this);
            }
        }

        AdsManager.getInstance().showAdsFullScreenHome();
    }
}

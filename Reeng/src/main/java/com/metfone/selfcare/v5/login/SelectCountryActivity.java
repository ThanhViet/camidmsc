package com.metfone.selfcare.v5.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by thanhnt72 on 2/10/2020.
 */

public class SelectCountryActivity extends BaseSlidingFragmentActivity {

    public static void startActivityForResult(BaseSlidingFragmentActivity activity, int index, int code) {
        Intent intent = new Intent(activity, SelectCountryActivity.class);
        intent.putExtra("index", index);
        activity.startActivityForResult(intent, code);
    }


    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.edit_search)
    EditText editSearch;
    @BindView(R.id.button_clear)
    ImageView buttonClear;
    @BindView(R.id.rvCountry)
    RecyclerView rvCountry;

    SelectCountryAdapter adapter;
    private ArrayList<Region> mRegions;
    private ApplicationController app;
    private String mCurrentRegionCode = "VN";// mac dinh vn
    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeLightStatusBar();
        setContentView(R.layout.activity_select_country);
        ButterKnife.bind(this);
        app = (ApplicationController) getApplication();


        int index = getIntent().getIntExtra("index", -1);

        rvCountry.setLayoutManager(new LinearLayoutManager(app));
        mRegions = app.getLoginBusiness().getAllRegions();
        if (index < 0)
            position = app.getLoginBusiness().getPositionRegion(mCurrentRegionCode);
        else position = index;
        mRegions.get(position).setSelected(true);

        adapter = new SelectCountryAdapter(mRegions, this, new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                /*mRegions.get(position).setSelected(false);
                position = pos;
                mRegions.get(position).setSelected(true);
                adapter.notifyDataSetChanged();*/
                if (adapter.getListData() == null || adapter.getListData().isEmpty()) return;
                int index = findIndexSearch(pos);
                final Intent data = new Intent();
                data.putExtra("data", index);
                setResult(Activity.RESULT_OK, data);
                finish();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        rvCountry.setAdapter(adapter);


        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.setListData(filterSearch(editSearch.getText().toString()));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private int findIndexSearch(int indexSearch) {
        if (!isSearching) return indexSearch;
        else {
            Region region = adapter.getListData().get(indexSearch);
            for (int i = 0; i < mRegions.size(); i++) {
                Region r = mRegions.get(i);
                if (r.getRegionName().equals(region.getRegionName()))
                    return i;

            }
            return 0;
        }
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        onBackPressed();
    }

    private boolean isSearching;

    private ArrayList<Region> filterSearch(String keyword) {
        if (TextUtils.isEmpty(keyword)) {
            isSearching = false;
            return mRegions;
        }
        ArrayList<Region> listSearch = new ArrayList<>();
        for (Region region : mRegions) {
            if (region.getRegionName().toLowerCase().contains(keyword.toLowerCase()) || region.getRegionCode().toLowerCase().contains(keyword.toLowerCase()))
                listSearch.add(region);
        }
        isSearching = true;
        return listSearch;
    }
}

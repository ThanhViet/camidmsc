package com.metfone.selfcare.v5.login;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 11/15/2019.
 */

public class DateOfBirthFragment extends Fragment {

    private static final String TAG = DateOfBirthFragment.class.getSimpleName();
    EditText etDOB;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    Unbinder unbinder;
    @BindView(R.id.tilDOB)
    TextInputLayout tilDOB;
    @BindView(R.id.tvGenderTitle)
    TextView tvGenderTitle;
    @BindView(R.id.ivBgMale)
    CircleImageView ivBgMale;
    @BindView(R.id.ivMale)
    ImageView ivMale;
    @BindView(R.id.tvGenderMale)
    AppCompatTextView tvGenderMale;
    @BindView(R.id.ivBgFemale)
    CircleImageView ivBgFemale;
    @BindView(R.id.ivFemale)
    ImageView ivFemale;
    @BindView(R.id.tvGenderFemale)
    AppCompatTextView tvGenderFemale;

    private BaseSlidingFragmentActivity activity;
    private ApplicationController app;
    private Resources res;

    ReengAccount reengAccount;

    public static DateOfBirthFragment newInstance() {
        DateOfBirthFragment fragment = new DateOfBirthFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_dob, container, false);
        unbinder = ButterKnife.bind(this, view);
        etDOB = tilDOB.getEditText();
        disableEditText(etDOB);
        drawDetail();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        app = (ApplicationController) activity.getApplication();
        res = activity.getResources();
        reengAccount = app.getReengAccountBusiness().getCurrentAccount();
    }

    private void drawDetail() {
        if (reengAccount == null) return;
        String birthdayStr = reengAccount.getBirthdayString();
        gender = reengAccount.getGender();

        if (activity instanceof LoginActivity) {
            ReengAccount tmp = ((LoginActivity) activity).getReengAccountTmp();
            if (tmp != null) {
                if (!TextUtils.isEmpty(tmp.getBirthdayString())) {
                    birthdayStr = tmp.getBirthdayString();
                }
                gender = tmp.getGender();
            }
        }

        if (!TextUtils.isEmpty(birthdayStr)) {
            etDOB.setText(TimeHelper.getStringBirthday(birthdayStr));
        }

        if (gender == Constants.CONTACT.GENDER_FEMALE) {
            selectedGender(ivBgFemale, ivFemale, tvGenderFemale);
            unselectedGender(ivBgMale, ivMale, tvGenderMale);
        } else {
            selectedGender(ivBgMale, ivMale, tvGenderMale);
            unselectedGender(ivBgFemale, ivFemale, tvGenderFemale);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    int gender = Constants.CONTACT.GENDER_FEMALE;

    @OnClick({R.id.ivBack, R.id.tilDOB, R.id.llGenderMale, R.id.llGenderFemale, R.id.btnContinue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tilDOB:
                showDialogTimePicker();
                break;
            case R.id.btnContinue:
                long birthday = TimeHelper.convertBirthdayToTime(etDOB.getText().toString());
                sendInfoToServerAndUpdateInfo(((LoginActivity) activity).getCurrentName(), birthday, gender);
                break;
            case R.id.ivBack:
                activity.onBackPressed();
                break;
            case R.id.llGenderMale:
                gender = Constants.CONTACT.GENDER_MALE;
                selectedGender(ivBgMale, ivMale, tvGenderMale);
                unselectedGender(ivBgFemale, ivFemale, tvGenderFemale);
                break;
            case R.id.llGenderFemale:
                gender = Constants.CONTACT.GENDER_FEMALE;
                selectedGender(ivBgFemale, ivFemale, tvGenderFemale);
                unselectedGender(ivBgMale, ivMale, tvGenderMale);
                break;
        }
    }

    public void setUserInfo(final ReengAccount account) {
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                activity.showToast(R.string.update_info_ok);
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "onError: " + errorCode);
                activity.showToast(R.string.update_infor_fail);
            }
        };
        ProfileRequestHelper.getInstance(app).setUserInfo(account, listener);
    }

    private void sendInfoToServerAndUpdateInfo(String name, long birthday, int gender) {
        String titleFail = activity.getString(R.string.error);
        if (!NetworkHelper.isConnectInternet(activity)) {
            activity.showError(activity.getString(R.string.error_internet_disconnect), titleFail);
            return;
        }
        activity.showLoadingDialog("", R.string.processing);
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        if (account != null) {
            account.setName(name);
            account.setGender(gender);
            account.setBirthday(String.valueOf(birthday));
            account.setBirthdayString(TimeHelper.formatTimeBirthdayString(birthday));
            setUserInfo(account);
        } else {
            activity.hideLoadingDialog();
        }

        if (((LoginActivity) activity).isNeedUploadAvatar() && !TextUtils.isEmpty(((LoginActivity) activity).getFileCropTmp())) {
            ((LoginActivity) activity).setNeedUploadAvatar(false);
            app.getReengAccountBusiness().processUploadAvatarTask(((LoginActivity) activity).getFileCropTmp());
        }

        if (app.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
            activity.finish();
            app.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
            ListenerHelper.getInstance().onLoginFromAnonymous();
        } else
            activity.goToHome();
    }

    private void showDialogTimePicker() {
        String dateChoseDefault;
        if (TextUtils.isEmpty(etDOB.getText().toString())) {
            dateChoseDefault = TimeHelper.formatTimeBirthday(TimeHelper.BIRTHDAY_DEFAULT_PICKER);
        } else
            dateChoseDefault = etDOB.getText().toString();

        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);

        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(activity, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                etDOB.setText(dateDesc);
//                Toast.makeText(MainActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
            }
        }).textConfirm("CONFIRM") //text of confirm button
                .textCancel("CANCEL") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(mYear - 100) //min year in loop
                .maxYear(mYear - 12) // max year in loop
                .dateChose(dateChoseDefault) // date chose when init popwindow
                .build();
        pickerPopWin.showPopWin(activity);
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

    private void selectedGender(ImageView ivBg, ImageView ivGender, TextView tvGender) {
        ivBg.setImageResource(R.color.v5_bg_gender_selected);
        ivGender.setColorFilter(ContextCompat.getColor(activity, R.color.white));
        tvGender.setTextColor(ContextCompat.getColor(activity, R.color.v5_text));
    }

    private void unselectedGender(ImageView ivBg, ImageView ivGender, TextView tvGender) {
        ivBg.setImageResource(R.color.v5_bg_gender_unselected);
        ivGender.setColorFilter(ContextCompat.getColor(activity, R.color.v5_gender_unselected));
        tvGender.setTextColor(ContextCompat.getColor(activity, R.color.v5_avatar_default));
    }

}

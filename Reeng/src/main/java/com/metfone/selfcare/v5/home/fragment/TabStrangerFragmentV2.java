package com.metfone.selfcare.v5.home.fragment;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;

import com.metfone.selfcare.R;

/**
 * Created by thanhnt72 on 11/6/2019.
 */

public class TabStrangerFragmentV2 extends BaseHomeFragment {
    public static TabStrangerFragmentV2 newInstance() {
        Bundle args = new Bundle();
        TabStrangerFragmentV2 fragment = new TabStrangerFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_home_v2;
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }
    @Override
    public int getColor() {
        return 0;
    }
}

package com.metfone.selfcare.v5.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.AspectImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by daipr on 4/3/2020
 */
public class ImageViewerAdapter extends RecyclerView.Adapter<ImageViewerAdapter.ImageViewerHolder> {

    private List<ImageProfile> listImage;
    private Context context; // application context

    private ImageClickListener imageClickListener;
    private ImageProfileBusiness imageProfileBusiness;
    private int oldSize;
    private boolean saveDb;

    public ImageViewerAdapter(List<ImageProfile> listImage, Context context, ImageProfileBusiness imageProfileBusiness, boolean saveDb) {
        this.listImage = listImage;
        this.saveDb = saveDb;
        this.imageProfileBusiness = imageProfileBusiness;
        if (listImage != null) {
            oldSize = listImage.size();
        } else {
            oldSize = 0;
        }
        this.context = context;
    }

    public void setListImage(List<ImageProfile> listImage) {
        if (listImage != null && this.listImage != null && this.listImage.size() != listImage.size()) {
            this.listImage = listImage;
            notifyDataSetChanged();
        }
    }

    public void updateSize() {
        if (listImage != null && oldSize != listImage.size()) {
            oldSize = listImage.size();
            notifyDataSetChanged();
        }
    }

    public void setImageClickListener(ImageClickListener imageClickListener) {
        this.imageClickListener = imageClickListener;
    }

    @NonNull
    @Override
    public ImageViewerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_viewer_simple_v5, parent, false);
        ImageViewerHolder holder = new ImageViewerHolder(convertView, imageProfileBusiness);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewerHolder holder, int position) {
        holder.setElement(listImage.get(position));
    }

    @Override
    public int getItemCount() {
        return listImage != null ? listImage.size() : 0;
    }

    public class ImageViewerHolder extends BaseViewHolder {
        private AspectImageView imageView;
        private ImageProfileBusiness mImageProfileBusiness;

        public ImageViewerHolder(View convertView, ImageProfileBusiness imageProfileBusiness) {
            super(convertView);
            this.mImageProfileBusiness = imageProfileBusiness;
            imageView = convertView.findViewById(R.id.img_image);
            imageView.setOnClickListener(view -> {
                if (imageClickListener != null) {
                    imageClickListener.imageClick(getAdapterPosition());
                }
            });
        }

        @Override
        public void setElement(Object obj) {
            if (obj != null && obj instanceof ImageProfile) {
                if (saveDb) {
                    mImageProfileBusiness.displayImageProfile(imageView, (ImageProfile) obj, true);
                } else {
                    mImageProfileBusiness.displayImageProfileFriend(imageView,
                            (ImageProfile) obj, false);
                }


            }

        }
    }

    public interface ImageClickListener {
        void imageClick(int position);
    }
}

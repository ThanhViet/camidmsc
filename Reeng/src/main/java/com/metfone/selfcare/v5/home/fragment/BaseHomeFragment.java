package com.metfone.selfcare.v5.home.fragment;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPFeedbackUsFragment;
import com.metfone.selfcare.util.Log;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 11/6/2019.
 */

public abstract class BaseHomeFragment extends Fragment {


    protected String TAG = getClass().getSimpleName();

    protected View viewParent;
    protected BaseSlidingFragmentActivity activity;
    protected ApplicationController app;
    protected Resources res;
    protected boolean isViewInitiated;

    protected boolean isVisibleToUser;
    protected boolean isDataInitiated;

    Unbinder unbinder;
    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        app = (ApplicationController) activity.getApplication();
        res = activity.getResources();
    }

    public abstract int getResIdView();

    public abstract void onCreateView();

    public abstract TabLayout getTabLayout();

    public abstract int getColor();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        long time = System.currentTimeMillis();
        viewParent = inflater.inflate(getResIdView(), container, false);
        unbinder = ButterKnife.bind(this, viewParent);
        mFragmentManager = getParentFragmentManager();
        isViewInitiated = false;

        initTablayout();
        onCreateView();
        Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - time));
        return viewParent;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewInitiated = true;
    }

    public void updateBadge(int index, String textBadge) {
        if (getTabLayout() == null) return;
        TabLayout.Tab tab = getTabLayout().getTabAt(index);
        if (tab != null) {
            View v = tab.getCustomView();
            if (v != null) {
                TextView tvBadge = v.findViewById(R.id.tvBadge);
                if (tvBadge != null) {
                    if (TextUtils.isEmpty(textBadge))
                        tvBadge.setVisibility(View.GONE);
                    else {
                        tvBadge.setVisibility(View.VISIBLE);
                        tvBadge.setText(textBadge);
                    }
                }

            }
        }
    }

    private void initTablayout() {
        if (getTabLayout() == null) return;
        for (int i = 0; i < getTabLayout().getTabCount(); i++) {

            TabLayout.Tab tab = getTabLayout().getTabAt(i);
            if (tab != null) {

                View vTabLayout = LayoutInflater.from(activity).inflate(R.layout.item_tablayout, null);
                tab.setCustomView(vTabLayout);

                TextView tabTextView = vTabLayout.findViewById(R.id.tvTitle);
                tabTextView.setText(tab.getText());

                TextView tabBadge = vTabLayout.findViewById(R.id.tvBadge);
                if (i == 0) {
                    tabTextView.setTextColor(getColor());
                    tabTextView.setTypeface(tabTextView.getTypeface(), Typeface.BOLD);
                    tabTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_selected));
                    tabBadge.setText("99+");
                } else
                    tabBadge.setText("8");
            }

        }
        getTabLayout().addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View v = tab.getCustomView();
                if (v != null) {
                    TextView tabTextView = v.findViewById(R.id.tvTitle);
                    if (tabTextView != null) {
                        onTextSelected(tabTextView);
                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View v = tab.getCustomView();
                if (v != null) {
                    TextView tabTextView = v.findViewById(R.id.tvTitle);
                    if (tabTextView != null) {
                        onTextUnSelected(tabTextView);
                    }

                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void onTextSelected(final TextView textViewSelected) {
        final float maxSize = getResources().getDimension(R.dimen.text_selected); // Size in pixels
        final float minSize = getResources().getDimension(R.dimen.text_unselected);
        long animationDuration = 100; // Animation duration in ms

        ValueAnimator animator = ValueAnimator.ofFloat(minSize, maxSize);
        animator.setDuration(animationDuration);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                textViewSelected.setTextSize(TypedValue.COMPLEX_UNIT_PX, animatedValue);
            }
        });
        animator.start();
        textViewSelected.setTextColor(getColor());
        textViewSelected.setTypeface(textViewSelected.getTypeface(), Typeface.BOLD);

    }

    private void onTextUnSelected(final TextView textViewUnselected) {
        final float maxSize = getResources().getDimension(R.dimen.text_selected); // Size in pixels
        final float minSize = getResources().getDimension(R.dimen.text_unselected);
        long animationDuration = 100; // Animation duration in ms

        ValueAnimator animator = ValueAnimator.ofFloat(maxSize, minSize);
        animator.setDuration(animationDuration);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                textViewUnselected.setTextSize(TypedValue.COMPLEX_UNIT_PX, animatedValue);
            }
        });
        animator.start();
        textViewUnselected.setTextColor(ContextCompat.getColor(activity, R.color.v5_tablayout_unselected));
        textViewUnselected.setTypeface(null, Typeface.NORMAL);
    }


    public boolean canLazyLoad() {
        return isVisibleToUser && isViewInitiated && !isDataInitiated;
    }
    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(containerViewId, fragment, fragmentTag);
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mFragmentTransaction.addToBackStack(fragmentTag);
        mFragmentTransaction.commitAllowingStateLoss();
    }
    public void popBackStackFragment() {
        mFragmentManager.popBackStack();
    }
}

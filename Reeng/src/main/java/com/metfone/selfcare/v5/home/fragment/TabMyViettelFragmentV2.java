package com.metfone.selfcare.v5.home.fragment;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;

import com.metfone.selfcare.R;

/**
 * Created by thanhnt72 on 11/8/2019.
 */

public class TabMyViettelFragmentV2 extends BaseHomeFragment {
    public static TabMyViettelFragmentV2 newInstance() {
        Bundle args = new Bundle();
        TabMyViettelFragmentV2 fragment = new TabMyViettelFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_home_v2;
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }
    @Override
    public int getColor() {
        return 0;
    }
}

package com.metfone.selfcare.v5.holder;

import androidx.appcompat.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.message.NoteMessageItem;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.adapter.SettingListAdapter;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.DecodeFormat.DEFAULT;

public class BlockContactViewHolder extends BaseViewHolder {

    @BindView(R.id.imgAvatar)
    RoundedImageView imgAvatar;
    @BindView(R.id.tvName)
    AppCompatTextView tvName;
    @BindView(R.id.tvAvatar)
    AppCompatTextView tvAvatar;
    @BindView(R.id.tvFirst)
    AppCompatTextView tvFirst;
    @BindView(R.id.tvSecond)
    AppCompatTextView tvSecond;
    @BindView(R.id.btnRemove)
    RoundTextView btnRemove;
    @BindView(R.id.viewRoot)
    View viewRoot;
    private ApplicationController mApplication;
    private SettingListAdapter.ItemListener itemListener;

    private RequestOptions displayGroupAvatarOptions;


    private int type;

    public BlockContactViewHolder(final View itemView, int type) {
        super(itemView);
        this.type = type;
        ButterKnife.bind(this, itemView);
        mApplication = ApplicationController.self();
        btnRemove.setOnClickListener(view -> {
            if (itemListener != null) {
                itemListener.removeItem(getAdapterPosition());
            }
        });
        viewRoot.setOnClickListener(view -> {
            if (itemListener != null) {
                itemListener.itemClick(getAdapterPosition());
            }
        });
        if (type == SettingListAdapter.SCREEN_SAVED_MESSAGE) {
            btnRemove.setText(R.string.unarchive);
            displayGroupAvatarOptions = new RequestOptions()
                    .placeholder(R.drawable.ic_avatar_default)
                    .error(R.drawable.ic_avatar_default)
                    .dontAnimate()
                    .dontTransform()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .format(DEFAULT)
                    .skipMemoryCache(false);
            viewRoot.setOnLongClickListener(view -> true);
            imgAvatar.setClickable(true);
            imgAvatar.setOnClickListener(view -> {
                if(itemListener != null){
                    itemListener.avatarClickListener(getAdapterPosition());
                }
            });
            tvSecond.setVisibility(View.VISIBLE);
        }

    }

    public void setType(int type) {
        this.type = type;
    }

    public void setItemListener(SettingListAdapter.ItemListener itemListener) {
        this.itemListener = itemListener;
    }

    @Override
    public void setElement(Object obj) {
        if (obj != null && obj instanceof PhoneNumber) {
            PhoneNumber phoneNumber = (PhoneNumber) obj;
            tvName.setText(phoneNumber.getName());
            tvFirst.setText(phoneNumber.getJidNumber());
            mApplication.getAvatarBusiness().setPhoneNumberAvatar(imgAvatar, tvAvatar
                    , phoneNumber, Utilities.dpToPixel(R.dimen.v5_avatar_normal_size, mApplication.getResources()));
        } else if (obj instanceof NoteMessageItem) {
            NoteMessageItem item = (NoteMessageItem) obj;
            if (TextUtils.isEmpty(item.getThread_avatar()) || (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT)) {
                imgAvatar.setImageResource(R.drawable.ic_avatar_default);
//                mApplication.getUniversalImageLoader().cancelDisplayTask(imgAvatar);
                Glide.with(mApplication).clear(imgAvatar);
            } else {
                if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    Glide.with(mApplication)
                            .load(item.getThread_avatar())
                            .apply(displayGroupAvatarOptions)
                            .into(imgAvatar);
//                    mApplication.getUniversalImageLoader().displayImage(item.getThread_avatar(), new ImageViewAwareTargetSize(imgAvatar), displayGroupAvatarOptions);

                } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
                    File fileGroupAvatar = new File(item.getThread_avatar());
                    if (fileGroupAvatar.exists()) {
                        Glide.with(mApplication)
                                .load("file://" + item.getThread_avatar())
                                .apply(displayGroupAvatarOptions)
                                .into(imgAvatar);

//                        mApplication.getUniversalImageLoader().displayImage("file://" + item.getThread_avatar(), imgAvatar, displayGroupAvatarOptions);
                    } else {
                        if (item.getThread_avatar().contains("http://")) {
                            Glide.with(mApplication)
                                    .load(item.getThread_avatar())
                                    .apply(displayGroupAvatarOptions)
                                    .into(imgAvatar);
//                            mApplication.getUniversalImageLoader().displayImage(item.getThread_avatar(), new ImageViewAwareTargetSize(imgAvatar), displayGroupAvatarOptions);
                        } else {
                            imgAvatar.setImageResource(R.drawable.ic_avatar_default);
//                            mApplication.getUniversalImageLoader().cancelDisplayTask(imgAvatar);
                            Glide.with(mApplication).clear(imgAvatar);
                        }
                    }

                } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room
                    mApplication.getAvatarBusiness().setNoteMessageAvatar(imgAvatar, item.getThread_avatar());


                } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {    // official
                    Glide.with(mApplication)
                            .load(item.getThread_avatar())
                            .apply(displayGroupAvatarOptions)
                            .into(imgAvatar);
//                    mApplication.getUniversalImageLoader().displayImage(item.getThread_avatar(), new ImageViewAwareTargetSize(imgAvatar), displayGroupAvatarOptions);

                }
            }

            tvName.setText(item.getThread_name());
            tvFirst.setText(TimeHelper.formatTimeInDay(item.getTimestamp()) + " " +TimeHelper.getDateOfMessage(item.getTimestamp()));
            tvSecond.setText(item.getContent());
        }
    }


}

package com.metfone.selfcare.v5.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.EpisodeAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FilmEpisodesFragment extends BaseFragment {

    @BindView(R.id.tab_total_episode)
    TextView tab_total_episode;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private Unbinder unbinder;

    private EpisodeAdapter episodeAdapter;

    public static FilmEpisodesFragment newInstance(Video video) {
        FilmEpisodesFragment fragment = new FilmEpisodesFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setEpisodeAdapter(EpisodeAdapter episodeAdapter) {
        this.episodeAdapter = episodeAdapter;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_episodes, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (episodeAdapter != null) {
            BaseAdapter.setupGridRecycler(getActivity(), recyclerView, null, episodeAdapter, 1, R.dimen.v5_spacing_normal, true);
            recyclerView.setAdapter(episodeAdapter);
            episodeAdapter.notifyDataSetChanged();
            recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
            recyclerView.setHasFixedSize(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getEpisodes(ArrayList<Movie> movies) {
        if(movies.size() > 0){
            tab_total_episode.setVisibility(View.VISIBLE);
            tab_total_episode.setText(movies.size() + " " + getString(R.string.episodes));
        }else{
            tab_total_episode.setVisibility(View.GONE);
        }
    }
}
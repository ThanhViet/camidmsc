package com.metfone.selfcare.v5.fragment;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.databinding.FragmentCommentBottomSheetBinding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.model.ClickLikeMessage;
import com.metfone.selfcare.model.CommentMessage;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;
import static com.metfone.selfcare.util.EnumUtils.COMMENT;
import static com.metfone.selfcare.util.EnumUtils.RANK_ID;

public class CommentBottomSheetDialogFragment extends BottomSheetDialogFragment implements IOnBackPressed {
    FragmentCommentBottomSheetBinding binding;
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private com.metfone.selfcare.model.account.UserInfo userInfo = userInfoBusiness.getUser();
    private final String TAG = "CommentBottomSheetDialogFragment";
    boolean isCommit;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CommentBottomSheetDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        binding = FragmentCommentBottomSheetBinding.inflate(inflater, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().setDimAmount(0.7f);
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Handler handler = new Handler();
        handler.postDelayed(this::detectKeyboard, 500);
        binding.edtComment.requestFocus();
        UIUtil.showKeyboard(getContext(), binding.edtComment);
        if (getArguments() != null && getArguments().getString(COMMENT) != null) {
            binding.edtComment.setText(getArguments().getString(COMMENT));
            binding.edtComment.setSelection(getArguments().getString(COMMENT).length());
            if (!TextUtils.isEmpty(getArguments().getString(COMMENT)) && getArguments().getString(COMMENT).length() > 1) {
                binding.btnSendComment.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_comment_new_focus));
                binding.btnSendComment.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
            }
        }
        if (TextUtils.isEmpty(userInfo.getAvatar())) {
            initRankInfo(getArguments().getInt(RANK_ID));
        } else {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_member)
                    .error(R.drawable.ic_avatar_member)
                    .into(binding.imgAvatar);
        }
        // s? ki?n thay ??i text trong edit text comment
        binding.edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty() && s.toString().length() > 1) {
                    binding.btnSendComment.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_comment_new_focus));
                    binding.btnSendComment.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                } else {
                    binding.btnSendComment.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_comment_new));
                    binding.btnSendComment.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.btnSendComment.setOnClickListener(view1 ->{
            hideKeyboard();
            dismiss();
            EventBus.getDefault().postSticky(new CommentMessage(binding.edtComment.getText().toString(), true));
        });
    }

    private void initRankInfo(int rankID) {
        if (getActivity() != null) {
            if (rankID > 1) {
                EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
                if (myRank != null) {
                    Glide.with(this)
                            .load(myRank.drawable)
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(binding.imgAvatar);
                } else {
                    Glide.with(this)
                            .load(ContextCompat.getDrawable(getActivity(), R.drawable.ic_avatar_member))
                            .centerCrop()
                            .placeholder(R.drawable.ic_avatar_member)
                            .error(R.drawable.ic_avatar_member)
                            .into(binding.imgAvatar);
                }
            } else {
                binding.imgAvatar.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_avatar_member));
//            imgCrown.setVisibility(View.GONE);
            }
        }
    }
    public void hideKeyboard() {
        Handler handler = new Handler();
        handler.postDelayed(() -> InputMethodUtils.hideSoftKeyboard(getActivity()), 1);
    }
    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        hideKeyboard();
        EventBus.getDefault().postSticky(new CommentMessage(binding.edtComment.getText().toString(), isCommit));
        EventBus.getDefault().post(true);
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        hideKeyboard();
        EventBus.getDefault().postSticky(new CommentMessage(binding.edtComment.getText().toString(), isCommit));
        EventBus.getDefault().post(true);
    }

    public void detectKeyboard(){
        binding.contrainsComment.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                binding.contrainsComment.getWindowVisibleDisplayFrame(r);
                int heightDiff = binding.contrainsComment.getRootView().getHeight() - (r.bottom - r.top);
                int screenHeight = binding.contrainsComment.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d(TAG, "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
//                    mFrameLayout.setVisibility(View.GONE);

                }
                else {
                    dismiss();
                    // keyboard is closed
                    android.util.Log.d("TAG", "onGlobalLayout:  keyboard is closed");
                }
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        dismiss();
        new Handler().postDelayed(() -> InputMethodUtils.hideSoftKeyboard(getActivity()), 100);
        EventBus.getDefault().post(true);
        return false;
    }
}
package com.metfone.selfcare.v5.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.DecelerateInterpolator;

import com.metfone.selfcare.R;

/**
 * Created by dainv00 on 22/11/2019
 */
public class ViewPIN extends AppCompatEditText {

    private int itemCount;
    private int colorPinBackground;
    private int colorPin;
    private int itemMargin;
    private int itemRadius;
    private boolean useAnimation;
    private Paint pinPaint;
    private Paint pinPaintBackground;
//    private ValueAnimator valueAnimator;
    private static final int DURATION_ANIMATION = 400;
    private boolean isDeleteAction;
    private float[] arrayRadius;

    public ViewPIN(Context context) {
        super(context);
    }

    public ViewPIN(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, R.attr.pinViewStyle);
    }

    public ViewPIN(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attributeSet, int defStyleAttr) {
        setCursorVisible(false);
        setBackground(null);
        TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.ViewPIN, defStyleAttr, 0);
        itemRadius = typedArray.getDimensionPixelSize(R.styleable.ViewPIN_item_radius, 30);
        itemMargin = typedArray.getDimensionPixelSize(R.styleable.ViewPIN_item_margin, 10);
        useAnimation = typedArray.getBoolean(R.styleable.ViewPIN_use_animation, true);
        itemCount = typedArray.getInt(R.styleable.ViewPIN_item_count, 4);
        colorPinBackground = typedArray.getColor(R.styleable.ViewPIN_color_pin_background, Color.LTGRAY);
        colorPin = typedArray.getColor(R.styleable.ViewPIN_color_pin, ContextCompat.getColor(getContext(), R.color.colorAccent));
        typedArray.recycle();
        arrayRadius = new float[itemCount];
        pinPaint = new Paint();
        pinPaint.setAntiAlias(true);
        pinPaint.setStyle(Paint.Style.FILL);
        pinPaint.setColor(colorPin);
        pinPaintBackground = new Paint();
        pinPaintBackground.setAntiAlias(true);
        pinPaintBackground.setStyle(Paint.Style.FILL);
        pinPaintBackground.setColor(colorPinBackground);
        setItemCount(itemCount);
        if (useAnimation) {
//            valueAnimator = valueAnimator.ofFloat(0.5f, 1f);
//            valueAnimator.setDuration(DURATION_ANIMATION);
//            valueAnimator.setInterpolator(new DecelerateInterpolator());
//            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                @Override
//                public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                    float scale = (float) valueAnimator.getAnimatedValue();
//                    lastCurrentRadiusPin = (int) (itemRadius * scale);
//                    postInvalidate();
//                }
//            });
        }
        isDeleteAction = false;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        drawPin(canvas);
        drawPinNotHaveText(canvas);
    }

    private void drawPinNotHaveText(Canvas canvas) {
        for (int i = length(); i < itemCount; i++) {
            int cx = i * itemMargin + itemRadius + itemRadius * 2 * i + getPaddingLeft();
            int cy = +getPaddingTop() + itemRadius;
            canvas.drawCircle(cx, cy, itemRadius, pinPaintBackground);
        }
    }

    private void drawPin(Canvas canvas) {
        for (int i = 0; i < length(); i++) {
            Log.e("debug", "index = "  + (i + 1) + " radius = " +  arrayRadius[i]);
            int cx = i * itemMargin + itemRadius + itemRadius * 2 * i + getPaddingLeft();
            int cy = +getPaddingTop() + itemRadius;
            float radius = i == length() - 1 && !isDeleteAction ? arrayRadius[i] : itemRadius;
            canvas.drawCircle(cx, cy, radius, pinPaint);
        }
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(itemCount)});
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureSize(widthMeasureSpec, heightMeasureSpec);
    }

    // define size of width or height
    private void measureSize(int widthMeasureSpec, int heightMeasureSpec) {
        int widthCaculate;
        int heightCaculate;
        int desireWidth = itemCount * itemRadius * 2 + (itemCount - 1) * itemMargin + getPaddingLeft() + getPaddingRight();
        int desireHeight = itemRadius * 2 + getPaddingTop() + getPaddingBottom() + 15; // tam thoi fix kieu nay :)) cho loi bi scroll
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpec = MeasureSpec.getSize(widthMeasureSpec);
        if (widthMode == MeasureSpec.EXACTLY) {
            widthCaculate = widthSpec;
        } else {
            widthCaculate = desireWidth;
            if (widthMode == MeasureSpec.AT_MOST) {
                widthCaculate = Math.min(desireWidth, widthSpec);
            }
        }
        if (widthCaculate < desireWidth) {
            Log.e("Pin view ", "width size pin view is not enough to show view");
        }
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSpec = MeasureSpec.getSize(heightMeasureSpec);
        if (heightMode == MeasureSpec.EXACTLY) {
            heightCaculate = heightSpec;
        } else {
            heightCaculate = desireHeight;
            if (widthMode == MeasureSpec.AT_MOST) {
                heightCaculate = Math.min(desireHeight, heightCaculate);
            }
        }
        if (heightCaculate < desireHeight) {
            Log.e("Pin view ", "height size pin view is not enough to show view");
        }

        setMeasuredDimension(widthCaculate, heightCaculate);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (lengthAfter < lengthBefore) {
            isDeleteAction = true;
        } else {
            isDeleteAction = false;
        }
        if (useAnimation) {
            if(length() > 0) {
                startAnimation(length() - 1);
            }
        }
    }

    private void startAnimation(final int position) {
       ValueAnimator valueAnimator = ValueAnimator.ofFloat(0.3f, 1f);
        valueAnimator.setDuration(DURATION_ANIMATION);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float scale = (float) valueAnimator.getAnimatedValue();
                arrayRadius[position] = (itemRadius * scale);
                if(length()> 0 &&  position == length() -1) {
                    postInvalidate();
                }
            }
        });
        valueAnimator.start();
    }

    public int getItemCount() {
        return itemCount;
    }

    public void reset() {
        setText(null);
    }
}

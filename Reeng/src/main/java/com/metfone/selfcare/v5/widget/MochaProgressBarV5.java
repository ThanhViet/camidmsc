package com.metfone.selfcare.v5.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ProgressBar;

public class MochaProgressBarV5 extends ProgressBar {

    private ValueAnimator valueAnimator;

    public MochaProgressBarV5(Context context) {
        super(context);
    }

    public MochaProgressBarV5(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MochaProgressBarV5(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setSmoothProgress(int progress) {
        if(valueAnimator != null){
            valueAnimator.end();
        }
        valueAnimator = valueAnimator.ofInt(getProgress(), progress);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        float ratio = ((float) Math.abs(progress - getProgress())) / 100;
        valueAnimator.setDuration((long) (ratio * 800));
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int valueUpdate = (int) valueAnimator.getAnimatedValue();
                setProgress(valueUpdate);
            }
        });
        valueAnimator.start();
    }

    public void resetProgress(){
        if(valueAnimator != null){
            valueAnimator.end();
            valueAnimator = null;
        }
        setProgress(0);
    }
}

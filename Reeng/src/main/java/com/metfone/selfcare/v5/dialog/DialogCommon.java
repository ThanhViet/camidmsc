package com.metfone.selfcare.v5.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.v5.mcinterface.DialogInterface;

/**
 * Created by thanhnt72 on 11/12/2019.
 */

public class DialogCommon extends Dialog implements View.OnClickListener {

    private static final String TAG = DialogCommon.class.getSimpleName();

    private BaseSlidingFragmentActivity activity;
    private Object object;

    private String title;
    private String msg;
    private String lbCancel;
    private String lbAction1, lbAction2;
    private boolean containHtml;
    private Spanned messageSpan;

    private DialogInterface listener;

    private TextView tvTitle;
    private TextView tvMessage;
    private TextView tvAction;
    private TextView tvTwoFirst;
    private TextView tvTwoSecond;
    private TextView tvThreeFirst;
    private TextView tvThreeSecond;
    private TextView tvThreeThird;

    private View vTwo;
    private View vThree;


    public DialogCommon(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogCommon setObject(Object obj) {
        this.object = obj;
        return this;
    }

    public DialogCommon setTitle(String title) {
        this.title = title;
        return this;
    }

    public DialogCommon setMessage(String msg) {
        this.msg = msg;
        return this;
    }

    public DialogCommon setLabelCancel(String lbCancel) {
        this.lbCancel = lbCancel;
        return this;
    }

    public void setMessageSpan(Spanned messageSpan) {
        this.messageSpan = messageSpan;
    }

    public DialogCommon setLabelAction1(String lbAction) {
        this.lbAction1 = lbAction;
        return this;
    }

    public DialogCommon setLabelAction2(String lbAction) {
        this.lbAction2 = lbAction;
        return this;
    }

    public DialogCommon setDialogListener(DialogInterface listener) {
        this.listener = listener;
        return this;
    }

    public DialogCommon setContainHtml(boolean containHtml) {
        this.containHtml = containHtml;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_common);
        findComponentViews();
        drawDetail();
        setListener();
    }


    private void drawDetail() {
        if (!TextUtils.isEmpty(title))
            tvTitle.setText(title);
        else
            tvTitle.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(msg) || messageSpan != null)
            if (containHtml)
                setTextViewHTML(tvMessage, msg);
            else
                if(messageSpan != null){
                    tvMessage.setText(messageSpan);
                }else {
                    tvMessage.setText(msg);
                }
        else
            tvMessage.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(lbAction1)) {
            if (!TextUtils.isEmpty(lbCancel)) {
                //nếu có label 2
                if (!TextUtils.isEmpty(lbAction2)) {
                    tvAction.setVisibility(View.GONE);
                    vTwo.setVisibility(View.GONE);
                    vThree.setVisibility(View.VISIBLE);
                    tvThreeFirst.setText(lbAction1);
                    tvThreeSecond.setText(lbAction2);
                    tvThreeThird.setText(lbCancel);
                } else {
                    tvAction.setVisibility(View.GONE);
                    vTwo.setVisibility(View.VISIBLE);
                    vThree.setVisibility(View.GONE);
                    tvTwoFirst.setText(lbAction1);
                    tvTwoSecond.setText(lbCancel);
                }
            }
            //nếu ko có cancel thì là dialog message, hiện 1 nút
            else {
                tvAction.setVisibility(View.VISIBLE);
                vTwo.setVisibility(View.GONE);
                vThree.setVisibility(View.GONE);
                tvAction.setText(lbAction1);
            }


        } else {
            tvAction.setVisibility(View.GONE);
            vTwo.setVisibility(View.GONE);
            vThree.setVisibility(View.GONE);
        }
    }

    public void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClickHyperLink(span.getURL());
                }
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void findComponentViews() {
        tvTitle = findViewById(R.id.tvTitleDialog);
        tvMessage = findViewById(R.id.tvMessageDialog);

        tvAction = findViewById(R.id.tvAction);
        tvTwoFirst = findViewById(R.id.tvTwoFirst);
        tvTwoSecond = findViewById(R.id.tvTwoSecond);
        tvThreeFirst = findViewById(R.id.tvFirst);
        tvThreeSecond = findViewById(R.id.tvSecond);
        tvThreeThird = findViewById(R.id.tvThird);
        vTwo = findViewById(R.id.clTwoAction);
        vThree = findViewById(R.id.llThreeAction);
    }

    private void setListener() {
        tvAction.setOnClickListener(this);
        tvTwoFirst.setOnClickListener(this);
        tvTwoSecond.setOnClickListener(this);
        tvThreeFirst.setOnClickListener(this);
        tvThreeSecond.setOnClickListener(this);
        tvThreeThird.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAction:
                if (listener != null) {
                    listener.onClickAction1();
                }
                break;
            case R.id.tvTwoFirst:
                if (listener != null) {
                    listener.onClickAction1();
                }
                break;
            case R.id.tvTwoSecond:
                if (listener != null) {
                    listener.onClickCancel();
                }
                break;
            case R.id.tvFirst:
                if (listener != null) {
                    listener.onClickAction1();
                }
                break;
            case R.id.tvSecond:
                if (listener != null) {
                    listener.onClickAction2();
                }
                break;
            case R.id.tvThird:
                if (listener != null) {
                    listener.onClickCancel();
                }
                break;
        }
        dismiss();
    }
}

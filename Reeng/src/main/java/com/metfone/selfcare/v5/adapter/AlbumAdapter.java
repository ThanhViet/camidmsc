package com.metfone.selfcare.v5.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.helper.images.ImageDirectory;
import com.metfone.selfcare.holder.BaseViewHolder;

import java.util.ArrayList;

/**
 * Created by DaiNV on 1/2/20.
 */
public class AlbumAdapter extends BaseAdapter<BaseViewHolder, ImageDirectory> {

    public AlbumAdapter(Activity activity, ArrayList<ImageDirectory> list) {
        super(activity, list);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }
}

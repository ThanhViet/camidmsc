package com.metfone.selfcare.v5.mcinterface;

/**
 * Created by thanhnt72 on 11/12/2019.
 */

public interface DialogInterface {

    void onClickAction1();

    void onClickAction2();

    void onClickHyperLink(String link);

    void onClickCancel();

}

package com.metfone.selfcare.v5.widget;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tabMovie.MovieKind;

public class LoadingViewV5 extends LinearLayout {

    ProgressBar progressLoading;
    TextView tvEmpty;
    TextView tvEmptyDesc;
    TextView tvEmptyRetry1;
    TextView tvEmptyRetry2;
    AppCompatImageView btnRetry;
    AppCompatImageView imgType;

    private String categoryId = "";

    public LoadingViewV5(Context context) {
        super(context);
        initView(context);
    }

    public LoadingViewV5(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LoadingViewV5(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public void initView(Context context) {
        if (!isInEditMode()) {
            ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.include_loading_view_v5, this, true);
            progressLoading = getRootView().findViewById(R.id.empty_progress);
            tvEmpty = getRootView().findViewById(R.id.tvEmptyTitle);
            tvEmptyDesc = getRootView().findViewById(R.id.tvEmptyDesc);
            tvEmptyRetry1 = getRootView().findViewById(R.id.empty_retry_text1);
            tvEmptyRetry2 = getRootView().findViewById(R.id.empty_retry_text2);
            btnRetry = getRootView().findViewById(R.id.empty_retry_button);
            imgType = getRootView().findViewById(R.id.imgType);
        }
    }

    public void setOnClickRetryListener(OnClickListener listener) {
        if (btnRetry != null) btnRetry.setOnClickListener(listener);
    }

    public void showLoading() {
        setVisibility(View.VISIBLE);
        if (progressLoading != null) progressLoading.setVisibility(View.VISIBLE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnRetry != null) btnRetry.setVisibility(View.GONE);
        if (imgType != null) imgType.setVisibility(View.GONE);
    }

    public void showLoadedEmpty() {
        setVisibility(View.VISIBLE);
        if (tvEmpty != null) {
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText(R.string.no_data);
        }
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnRetry != null) btnRetry.setVisibility(View.GONE);
        showEmptyTypeImage();
    }

    public void showLoadedEmpty(String message) {
        setVisibility(View.VISIBLE);
        if (tvEmpty != null) {
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText(message);
        }
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnRetry != null) btnRetry.setVisibility(View.GONE);
        showEmptyTypeImage();
    }

    public void showLoadedSuccess() {
        setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnRetry != null) btnRetry.setVisibility(View.GONE);
        if (imgType != null) imgType.setVisibility(View.GONE);
    }

    public void showLoadedError() {
        setVisibility(View.VISIBLE);
        if (btnRetry != null) btnRetry.setVisibility(View.VISIBLE);
        if (imgType != null) imgType.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (getContext() != null) {
            if (NetworkHelper.isConnectInternet(getContext())) {
                if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.VISIBLE);
                if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
            } else {
                if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
                if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.VISIBLE);
            }
        } else {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.VISIBLE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        }
    }

    public void showLoadedError(String message) {
        setVisibility(View.VISIBLE);
        if (btnRetry != null) btnRetry.setVisibility(View.VISIBLE);
        if (imgType != null) imgType.setVisibility(View.GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) {
            tvEmptyRetry1.setText(message);
            tvEmptyRetry1.setVisibility(View.VISIBLE);
        }
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
    }

    public void showLoadedError(String message, int drawableRes) {
        setVisibility(View.VISIBLE);
        if (btnRetry != null) {
            btnRetry.setVisibility(View.VISIBLE);
            if (getContext() != null)
                btnRetry.setImageDrawable(getContext().getResources().getDrawable(drawableRes));
        }
        imgType.setVisibility(GONE);
        if (progressLoading != null) progressLoading.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyDesc != null) tvEmptyDesc.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) {
            tvEmptyRetry1.setText(message);
            tvEmptyRetry1.setVisibility(View.VISIBLE);
        }
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public void showTypeImage(int drawableRes) {
        imgType.setVisibility(VISIBLE);
        imgType.setImageResource(drawableRes);
    }

    private void showEmptyTypeImage() {
        if (getContext() == null) return;
        switch (categoryId) {
            case MovieKind.CATEGORYID_GET_LATER:
                if (tvEmpty != null)
                    tvEmpty.setText(Html.fromHtml(getContext().getString(R.string.no_saved_film)));
                if (tvEmptyDesc != null) {
                    tvEmptyDesc.setVisibility(VISIBLE);
                    tvEmptyDesc.setText(Html.fromHtml(getContext().getString(R.string.no_saved_film_desc)));
                }
                showTypeImage(R.drawable.ic_empty_film_later);
                break;
            case MovieKind.CATEGORYID_GET_WATCHED:
                if (tvEmpty != null)
                    tvEmpty.setText(Html.fromHtml(getContext().getString(R.string.no_watching_film)));
                if (tvEmptyDesc != null) {
                    tvEmptyDesc.setVisibility(VISIBLE);
                    tvEmptyDesc.setText(Html.fromHtml(getContext().getString(R.string.no_watching_film_desc)));
                }
                showTypeImage(R.drawable.ic_empty_film);
                break;
            case MovieKind.CATEGORYID_GET_LIKED_ODD:
            case MovieKind.CATEGORYID_GET_LIKED_SERIES:
            case MovieKind.CATEGORYID_GET_LIKED:
                if (tvEmpty != null)
                    tvEmpty.setText(Html.fromHtml(getContext().getString(R.string.no_liked_film)));
                if (tvEmptyDesc != null) {
                    tvEmptyDesc.setVisibility(VISIBLE);
                    tvEmptyDesc.setText(Html.fromHtml(getContext().getString(R.string.no_liked_film_desc)));
                }
                showTypeImage(R.drawable.ic_empty_film_fav);
                break;
        }
    }
}

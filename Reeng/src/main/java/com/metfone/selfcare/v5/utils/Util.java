package com.metfone.selfcare.v5.utils;

import android.app.Dialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

public final class Util {

    public static void showDialogExplain(AppCompatActivity activity, String textExplain, float x, float y, View viewAnchor) {
        if (activity != null) {
            final Dialog dialog = new Dialog(activity, R.style.V5DialogExplainSettingStyle);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.getWindow().getAttributes().windowAnimations = R.style.V5DialogWindowAnimation;
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            View view = LayoutInflater.from(activity.getApplicationContext()).inflate(R.layout.dialog_setting_explain_v5, null, false);
            ConstraintLayout rootView = (ConstraintLayout) view;
            AppCompatTextView txtExplain = rootView.findViewById(R.id.txtExplain);
            AppCompatImageView imgDownArrow = rootView.findViewById(R.id.icDownArrow);
            txtExplain.setText(textExplain);
            ConstraintLayout.LayoutParams layoutIcon = (ConstraintLayout.LayoutParams) imgDownArrow.getLayoutParams();
            ConstraintLayout.LayoutParams layoutText = (ConstraintLayout.LayoutParams) txtExplain.getLayoutParams();
            // define position view
            if (y > 500) { // arrow down
                layoutText.topToBottom = -1;
                layoutText.bottomToTop = imgDownArrow.getId();
                imgDownArrow.setImageResource(R.drawable.ic_arrow_down_filled_triangle);
                layoutIcon.topMargin = (int) (y - Utilities.dpToPixel(R.dimen.dimen10dp, activity.getResources()));
                layoutIcon.leftMargin = (int) (x + viewAnchor.getPaddingLeft());
            } else { // arrow up
                layoutIcon.topMargin = (int) (y + viewAnchor.getHeight());
                layoutIcon.leftMargin = (int) (x + viewAnchor.getPaddingLeft());
            }

//            imgDownArrow.setLayoutParams(layoutParams);
//            view.invalidate();
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.setContentView(view);
            dialog.show();
        }
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.v5.home.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.reflect.TypeToken;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.LoginFromAnonymousListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.movie.event.TabMovieReselectedEvent;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.movienew.fragment.GenresFragment;
import com.metfone.selfcare.module.movienew.fragment.SearchMovieNewFragment;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.dialog.BottomSheetDialog;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.MOVIE_KEENG;

public class TabMovieFragmentV2 extends BaseHomeFragment implements ViewPager.OnPageChangeListener
        , TabLayout.OnTabSelectedListener, OnInternetChangedListener, LoginFromAnonymousListener {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.loading_view_tab)
    LoadingView loadingView;
    @BindView(R.id.layout_action_bar)
    View viewActionBar;
    @BindView(R.id.iv_back)
    View btnBack;
    @BindView(R.id.tv_tab_name)
    TextView tvTabName;
    @BindView(R.id.iv_search)
    View btnSearch;
    @BindView(R.id.iv_more)
    View btnMore;
    private ArrayList<SubtabInfo> data;
    private boolean isLoading;
    private ListenerUtils mListenerUtils;
    private ApiCallbackV2<ArrayList<SubtabInfo>> apiCallback = new ApiCallbackV2<ArrayList<SubtabInfo>>() {
        @Override
        public void onSuccess(String msg, ArrayList<SubtabInfo> result) throws JSONException {
            if (data == null) data = new ArrayList<>();
            else data.clear();
            if (Utilities.notEmpty(result)) {
                data.addAll(result);
            }
            setupViewPager(data);
            if (Utilities.isEmpty(data)) {
                if (loadingView != null) loadingView.showLoadedEmpty();
            } else {
                if (loadingView != null) loadingView.showLoadedSuccess();
            }
        }

        @Override
        public void onError(String s) {
            if (Utilities.isEmpty(data)) {
                if (loadingView != null) loadingView.showLoadedError();
            } else {
                if (loadingView != null) loadingView.showLoadedSuccess();
            }
        }

        @Override
        public void onComplete() {
            isDataInitiated = true;
            isLoading = false;
        }
    };
    private BottomSheetDialog dialogMore;
    private ArrayList<ItemContextMenu> dialogMoreItems;
    private int currentPosition = -1;

    public static TabMovieFragmentV2 newInstance() {
        Bundle args = new Bundle();
        TabMovieFragmentV2 fragment = new TabMovieFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_tab_movie_v2;
    }

    @Override
    public void onCreateView() {
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
            }
        });
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity instanceof ModuleActivity) activity.finish();
                else if (activity != null) activity.onBackPressed();
            }
        });
        btnMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickMore();
            }
        });
        btnSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
//                if (activity == null) return;
//                Utilities.openSearch(activity, Constants.TAB_MOVIE_HOME);
                Intent intent = new Intent(getActivity(), SearchMovieNewFragment.class);
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_movie);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
        ListenerHelper.getInstance().addLoginAnonymousListener(this);
        tvTabName.setText(app.getReengAccountBusiness().isCambodia() ? R.string.tab_5dmax : R.string.tab_movie);
        if (activity instanceof HomeActivity) {
            btnBack.setVisibility(View.GONE);
            Utilities.setMargins(tvTabName, Utilities.dpToPixel(R.dimen.v5_spacing_normal, res), tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
            new Handler().postDelayed(this::initWithCacheData, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        } else {
            btnBack.setVisibility(View.VISIBLE);
            Utilities.setMargins(tvTabName, 0, tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
            new Handler().postDelayed(this::initWithCacheData, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad())
            loadData(true);
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        ListenerHelper.getInstance().removeLoginAnonymousListener(this);
        super.onDestroyView();
    }

    private void initWithCacheData() {
        try {
            ArrayList<SubtabInfo> list = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_MOVIES_SUBTAB_HOME_LIST, new TypeToken<ArrayList<SubtabInfo>>() {
            }.getType());
            if (Utilities.notEmpty(list)) {
                if (apiCallback != null) apiCallback.onSuccess("", list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadData(Utilities.isEmpty(data));
    }

    private void loadData(boolean showLoading) {
        if (isLoading) return;
        isLoading = false;
        if (showLoading) {
            if (loadingView != null) loadingView.showLoading();
        }
        new MovieApi().getListSubtab(null, apiCallback);
    }

    private void setupViewPager(ArrayList<SubtabInfo> data) {
        if (viewPager != null) {
            viewPager.setAdapter(createAdapterPage(data));
            viewPager.setOffscreenPageLimit(3);
            viewPager.setCurrentItem(0);
            viewPager.addOnPageChangeListener(this);
            if (tabLayout != null) {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.addOnTabSelectedListener(this);
            }
        }
        if (data.size() < 2) {
            if (appBarLayout != null) appBarLayout.setVisibility(View.GONE);
        } else {
            if (appBarLayout != null) appBarLayout.setVisibility(View.VISIBLE);
            if (data.size() < 4) {
                if (tabLayout != null) tabLayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                if (tabLayout != null) tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
        if (coordinatorLayout != null) {
            if (Utilities.isEmpty(data))
                coordinatorLayout.setVisibility(View.GONE);
            else
                coordinatorLayout.setVisibility(View.VISIBLE);
        }
    }

    private PagerAdapter createAdapterPage(ArrayList<SubtabInfo> list) {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(activity);
        int size = list.size() - 1;
        for (int i = size; i >= 0; i--) {
            if (list.get(i) == null || TextUtils.isEmpty(list.get(i).getCategoryName()))
                list.remove(i);
        }
        Bundle bundle;
        for (int i = 0; i < list.size(); i++) {
            SubtabInfo item = list.get(i);
            item.setTabHome(i == 0);
            bundle = new Bundle();
            bundle.putInt(Constants.KEY_POSITION, i);
            bundle.putSerializable(Constants.KEY_DATA, item);
            if (item.isShowCategoryDetail())
                creator.add(FragmentPagerItem.of(item.getCategoryName(), GenresFragment.class, bundle));
            else
                creator.add(FragmentPagerItem.of(item.getCategoryName(), MoviePagerFragment.class, bundle));
        }
        return new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (appBarLayout != null) appBarLayout.setExpanded(true);
        if (currentPosition != position) {
            if (data != null && data.size() > position && position >= 0) {
                SubtabInfo model = data.get(position);
            }
        }
        currentPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(activity) && Utilities.isEmpty(data)) {
            loadData(false);
        }
    }

    private void initDataDialogMore() {
        if (dialogMoreItems == null) dialogMoreItems = new ArrayList<>();
        else dialogMoreItems.clear();
        ItemContextMenu item;
        if (app.getReengAccountBusiness().isVietnam()) {
            item = new ItemContextMenu(getString(R.string.film_liked), R.drawable.ic_movie_liked_option, null, Constants.MENU.MENU_MOVIE_LIKED);
            dialogMoreItems.add(item);

            item = new ItemContextMenu(getString(R.string.movie_watch_later), R.drawable.ic_movie_later_option, null, Constants.MENU.MENU_MOVIE_WATCH_LATER);
            dialogMoreItems.add(item);
        }

        item = new ItemContextMenu(getString(R.string.film_watched), R.drawable.ic_movie_watched_option, null, Constants.MENU.MENU_MOVIE_WATCHED);
        dialogMoreItems.add(item);

        if (app.getReengAccountBusiness().isVietnam()) {
            item = new ItemContextMenu(getString(R.string.open_app_keeng), R.drawable.ic_open_keeng_option, null, Constants.MENU.MENU_OPEN_KEENG_MOVIE);
            dialogMoreItems.add(item);
        }

        item = new ItemContextMenu(getString(R.string.btn_cancel_option), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        dialogMoreItems.add(item);
    }

    private void onClickMore() {
        if (dialogMore != null && dialogMore.isShowing()) {
            dialogMore.dismiss();
            dialogMore = null;
        }
        dialogMore = new BottomSheetDialog(activity);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_more_tab_movie, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        initDataDialogMore();
        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(dialogMoreItems);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu item = (ItemContextMenu) object;
                    switch (item.getActionTag()) {
                        case Constants.MENU.MENU_MOVIE_LIKED:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                DeepLinkHelper.getInstance().openSchemaLink(activity, MOVIE_KEENG);
                            } else {
                                SubtabInfo tabInfo = new SubtabInfo();
                                tabInfo.setType(MovieKind.TYPE_LOCAL);
                                tabInfo.setCategoryId(MovieKind.CATEGORYID_GET_LIKED);
                                tabInfo.setCategoryName(activity.getString(R.string.film_liked));
                                tabInfo.setSubtab(true);
                                NavigateActivityHelper.navigateToTabMoviesCategoryDetail(activity, tabInfo);
                            }
                            break;

                        case Constants.MENU.MENU_MOVIE_WATCHED:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                DeepLinkHelper.getInstance().openSchemaLink(activity, MOVIE_KEENG);
                            } else {
                                SubtabInfo tabInfo = new SubtabInfo();
                                tabInfo.setType(MovieKind.TYPE_LOCAL);
                                tabInfo.setCategoryId(MovieKind.CATEGORYID_GET_WATCHED);
                                tabInfo.setCategoryName(activity.getString(R.string.film_watched));
                                tabInfo.setSubtab(true);
                                NavigateActivityHelper.navigateToTabMoviesCategoryDetail(activity, tabInfo);
                            }
                            break;

                        case Constants.MENU.MENU_MOVIE_WATCH_LATER:
                            if (app.getReengAccountBusiness().isAnonymousLogin()) {
                                DeepLinkHelper.getInstance().openSchemaLink(activity, MOVIE_KEENG);
                            } else {
                                SubtabInfo tabInfo = new SubtabInfo();
                                tabInfo.setType(MovieKind.TYPE_LOCAL);
                                tabInfo.setCategoryId(MovieKind.CATEGORYID_GET_LATER);
                                tabInfo.setCategoryName(activity.getString(R.string.movie_watch_later));
                                tabInfo.setSubtab(true);
                                NavigateActivityHelper.navigateToTabMoviesCategoryDetail(activity, tabInfo);
                            }
                            break;

                        case Constants.MENU.MENU_OPEN_KEENG_MOVIE:
                            Utilities.openApp(activity, "com.vttm.keeng");
                            break;

                        default:
                            break;
                    }
                }
                dialogMore.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialogMore.setContentView(sheetView);
        dialogMore.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

            }
        });
        dialogMore.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabMovieReselectedEvent event) {
        Log.i(TAG, "onMessageEvent " + event);
        if (event.isReselectedHome()) {
            onInternetChanged();
            if (appBarLayout != null) {
                appBarLayout.setExpanded(true);
            }
            if (mListenerUtils != null && viewPager != null) {
                mListenerUtils.notifyTabReselected(Constants.TAB_MOVIE_HOME, viewPager.getCurrentItem());
            }
        }
    }

    @Override
    public void onLoginSuccess() {
        loadData(Utilities.isEmpty(data));
    }
}

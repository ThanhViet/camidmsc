package com.metfone.selfcare.v5.widget;

import android.content.res.Resources;
import android.view.View;

/**
 * Created by HaiKE on 10/13/17.
 */

public interface ColorUiInterface {
    public View getView();

    public void setTheme(Resources.Theme themeId);
}

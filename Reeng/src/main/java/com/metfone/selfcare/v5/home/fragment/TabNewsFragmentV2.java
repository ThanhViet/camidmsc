package com.metfone.selfcare.v5.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.netnews.ChildNews.fragment.ChildNewsFragment;
import com.metfone.selfcare.module.netnews.HomeNews.fragment.HomeNewsFragment;
import com.metfone.selfcare.module.netnews.activity.NetNewsActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.model.CategoryModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.response.CategoryResponse;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by thanhnt72 on 11/6/2019.
 */

public class TabNewsFragmentV2 extends BaseHomeFragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener, OnInternetChangedListener {
    @BindView(R.id.layout_action_bar)
    View viewActionBar;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.loading_view_tab)
    LoadingView loadingView;
    @BindView(R.id.iv_more)
    View ivMore;
    @BindView(R.id.iv_search)
    View viewSearch;
    @BindView(R.id.tv_tab_name)
    TextView tvTabName;
    private ArrayList<CategoryModel> dataCategory;
    private ViewPagerDetailAdapter adapter;
    private int currentPosition = -1;

    public static TabNewsFragmentV2 newInstance() {
        Bundle args = new Bundle();
        TabNewsFragmentV2 fragment = new TabNewsFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_tab_news_v2;
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_news);
    }

    @Override
    public void onCreateView() {
        Log.e("Duong", "---------onCreateView---------");
        if (viewSearch != null) viewSearch.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity == null) return;
                Utilities.openSearch(activity, Constants.TAB_NEWS_HOME);
            }
        });
        ivMore.setOnClickListener(v -> {
            if (activity == null) return;
            if (activity instanceof NetNewsActivity) {
                ((NetNewsActivity) activity).showFragment(CommonUtils.TAB_SETTING_CATEGORY, null, true);
            } else {
                //start NetNewsActivity
                Intent intent = new Intent(activity, NetNewsActivity.class);
                intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_SETTING_CATEGORY);
                activity.startActivity(intent);
            }
        });
        ivBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (activity instanceof ModuleActivity) activity.finish();
                else if (activity != null) activity.onBackPressed();
            }
        });
        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData(true);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("Duong", "---------onActivityCreated---------");
        if (activity instanceof ModuleActivity) {
            ivBack.setVisibility(View.VISIBLE);
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
            Utilities.setMargins(tvTabName, 0, tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
            new Handler().postDelayed(() -> loadData(true), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        } else {
            ivBack.setVisibility(View.GONE);
            Utilities.setMargins(tvTabName, Utilities.dpToPixel(R.dimen.v5_spacing_normal, getResources()), tvTabName.getTop(), tvTabName.getRight(), tvTabName.getBottom());
//            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), Utilities.dpToPixel(R.dimen.v5_margin_top_action_bar_home, res), viewActionBar.getRight(), viewActionBar.getBottom());
            Utilities.setMargins(viewActionBar, viewActionBar.getLeft(), 0, viewActionBar.getRight(), viewActionBar.getBottom());
            new Handler().postDelayed(() -> loadData(true), Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) {
            loadData(true);
        }
    }

    private void loadData(boolean flag) {
        if (flag) {
            if (loadingView != null) loadingView.showLoading();
        }
        if (activity == null) return;
        if (TiinUtilities.checkDate(activity)) { // goi api 1 lan 1 ngay va khi cai app lan dau
            long currentTime = System.currentTimeMillis();
            SharedPref.newInstance(activity).putLong("TIME_CATEGORY", currentTime);
            loadCategory();
        } else {
            CategoryResponse data = SharedPrefs.getInstance().get(SharedPrefs.LIST_CATEGORY_NEW_V5, CategoryResponse.class);

            if (data == null || (data.getData() == null || data.getData().size() == 0)) {
                loadCategory();
            } else {
                dataCategory = data.getData();
                setupViewPager(dataCategory);
                if (Utilities.isEmpty(dataCategory)) {
                    if (loadingView != null) loadingView.showLoadedEmpty();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
                isDataInitiated = true;
            }
        }

    }

    private void loadCategory() {
        new NetNewsApi(ApplicationController.self()).getNewsCategory(new HttpCallBack() {
            @Override
            public void onSuccess(String data) {
                if (dataCategory == null) dataCategory = new ArrayList<>();
                else dataCategory.clear();
                Gson gson = new Gson();
                CategoryResponse categoryResponse = gson.fromJson(data, CategoryResponse.class);
                if (categoryResponse != null) {
                    dataCategory = categoryResponse.getData();
                    setupViewPager(dataCategory);
                    SharedPrefs.getInstance().put(SharedPrefs.LIST_CATEGORY_NEW_V5, categoryResponse);
                } else {
                    if (activity == null) return;
                    dataCategory = CommonUtils.getListCategoryNews(activity);
                    setupViewPager(dataCategory);
                }
                if (Utilities.isEmpty(data)) {
                    if (loadingView != null) loadingView.showLoadedEmpty();
                } else {
                    if (loadingView != null) loadingView.showLoadedSuccess();
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                if (Utilities.isEmpty(dataCategory)) {
                    dataCategory = CommonUtils.getListCategoryNews(activity);
                    setupViewPager(dataCategory);
                }
                if (loadingView != null) loadingView.showLoadedSuccess();
            }

            @Override
            public void onCompleted() {
                super.onCompleted();
                isDataInitiated = true;
            }
        });
    }

    private void setupViewPager(ArrayList<CategoryModel> data) {
            if (isAdded() && data != null) {
            adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
            int size = data.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (data.get(i) == null || TextUtils.isEmpty(data.get(i).getName()))
                    data.remove(i);
            }
            //Tab home
            adapter.addFragment(HomeNewsFragment.newInstance(), getString(R.string.home_news));

            //Tab moi nhat
            Bundle bundleLatest = new Bundle();
            bundleLatest.putInt(CommonUtils.KEY_CATEGORY_ID, 0);
            adapter.addFragment(ChildNewsFragment.newInstance(bundleLatest), getString(R.string.latest_news));
            //tab chuyen muc
            Bundle bundle;
            for (int i = 0; i < data.size(); i++) {
                CategoryModel item = data.get(i);
                bundle = new Bundle();
                bundle.putInt(CommonUtils.KEY_CATEGORY_ID, item.getId());
                adapter.addFragment(ChildNewsFragment.newInstance(bundle), item.getName());
            }
            if (viewPager != null) {
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(1);
                viewPager.setCurrentItem(0);
                viewPager.addOnPageChangeListener(this);
                if (tabLayout != null) {
                    tabLayout.setupWithViewPager(viewPager);
                    tabLayout.addOnTabSelectedListener(this);
                }
            }
            if (data.size() < 2) {
//            if (appBarLayout != null) appBarLayout.setVisibility(View.GONE);
            } else {
                if (appBarLayout != null) appBarLayout.setVisibility(View.VISIBLE);
                if (data.size() < 4) {
                    if (tabLayout != null) tabLayout.setTabMode(TabLayout.MODE_FIXED);
                } else {
                    if (tabLayout != null) tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                }
            }
            if (coordinatorLayout != null) {
                if (Utilities.isEmpty(data))
                    coordinatorLayout.setVisibility(View.GONE);
                else
                    coordinatorLayout.setVisibility(View.VISIBLE);
            }
        }
    }

//    private PagerAdapter createAdapterPage(ArrayList<CategoryModel> list) {
//        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(activity);
//        int size = list.size() - 1;
//        for (int i = size; i >= 0; i--) {
//            if (list.get(i) == null || TextUtils.isEmpty(list.get(i).getName()))
//                list.remove(i);
//        }
//        //Tab home
//        creator.add(FragmentPagerItem.of(getString(R.string.home_news), HomeNewsFragment.class));
//
//        //Tab moi nhat
//        Bundle bundleLatest = new Bundle();
//        bundleLatest.putInt(CommonUtils.KEY_CATEGORY_ID, 0);
//        creator.add(FragmentPagerItem.of(getString(R.string.latest_news), ChildNewsFragment.class, bundleLatest));
//        //tab chuyen muc
//        Bundle bundle;
//        for (int i = 0; i < list.size(); i++) {
//            CategoryModel item = list.get(i);
//            bundle = new Bundle();
//            bundle.putInt(CommonUtils.KEY_CATEGORY_ID, item.getId());
//            creator.add(FragmentPagerItem.of(item.getName(), ChildNewsFragment.class, bundle));
//        }
//        adapter = new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
//        return adapter;
//    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (appBarLayout != null) appBarLayout.setExpanded(true);
        if (currentPosition != position) {
            String tabName = "";
            if (position == 0) {
                tabName = getString(R.string.home_news);
            } else if (position == 1) {
                tabName = getString(R.string.latest_news);
            } else {
                int pos = position - 2;
                if (dataCategory != null && dataCategory.size() > pos && pos >= 0) {
                    CategoryModel model = dataCategory.get(pos);
                    if (model != null) {
                        tabName = model.getName();
                    }
                }
            }
        }
        currentPosition = position;
        try {
            if (position == 1)//Tab moi nhat
            {
                //An news count di
                if (getCurrentAdapter() != null && getCurrentAdapter().getItem(0) instanceof HomeNewsFragment) {
                    HomeNewsFragment homeNewsFragment = (HomeNewsFragment) getCurrentAdapter().getItem(0);
                    if (homeNewsFragment != null) {
                        AppStateProvider.getInstance().setNewsCount(0);
                        homeNewsFragment.hideNewsCount();
                        Log.e("---Duong----", "Home");
                    }
                }

                if (getCurrentAdapter() != null && getCurrentAdapter().getItem(1) instanceof ChildNewsFragment) {
                    ChildNewsFragment childNewsFragment = (ChildNewsFragment) getCurrentAdapter().getItem(1);
                    if (childNewsFragment != null) {
                        childNewsFragment.updateLastNews();
                        Log.e("---Duong----", "Child");
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Log.e("--Duong---", "Tab:" + tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onInternetChanged() {
        Log.e("Duong", "change network tab new");
        if (NetworkHelper.isConnectInternet(activity) && Utilities.isEmpty(dataCategory)) {
            loadData(true);
        }
    }

    public void selectTab(final int position) {
        if (viewPager != null && tabLayout != null) {
            tabLayout.setScrollPosition(position, 0f, true);
            viewPager.setCurrentItem(position);
        }
    }

    public ViewPagerDetailAdapter getCurrentAdapter() {
        return adapter;
    }

    public void scrollToPosition(int position) {
        try {
            if (adapter != null && viewPager != null) {
                BaseFragment fragment = (BaseFragment) adapter.getItem(viewPager.getCurrentItem());
                if (fragment instanceof HomeNewsFragment) {
                    ((HomeNewsFragment) fragment).scrollToPosition(position);
                } else if (fragment instanceof ChildNewsFragment) {
                    ((ChildNewsFragment) fragment).scrollToPosition(position);
                }
            }
            if (appBarLayout != null) {
                appBarLayout.setExpanded(true);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabNewsEvent event) {
        if (event.isReselected()) {
            scrollToPosition(0);
        }
    }

    public static class TabNewsEvent {
        boolean reselected;

        public boolean isReselected() {
            return reselected;
        }

        public TabNewsEvent(boolean reselected) {
            this.reselected = reselected;
        }
    }
}

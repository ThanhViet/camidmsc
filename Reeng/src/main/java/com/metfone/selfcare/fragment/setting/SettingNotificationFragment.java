package com.metfone.selfcare.fragment.setting;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.Settings;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationManagerCompat;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.workmanager.SettingWorkManager;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.dialog.DialogRadioSelect;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.widget.SwitchButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;

/**
 * Created by toanvk2 on 12/9/2016.
 */
public class SettingNotificationFragment extends BaseSettingFragment implements View.OnClickListener, SwitchButton.OnCheckedChangeListener {
    private static final String TAG = SettingNotificationFragment.class.getSimpleName();
    private SettingBusiness mSettingBusiness;
    private Resources mRes;

    private ConstraintLayout mViewPreviewMessage, mViewQuickReply, mViewUnLockQuickReply,
            mViewNotifyNewUser, mViewOnRingtone, mViewVibrate, mViewSettingOnOffNotify;
    private SwitchButton mTogglePreviewMessage, mToggleQuickReply, mToggleUnLockQuickReply,
            mToggleNotifyNewUser, mToggleOnRingtone, mToggleVibrate, mToggleSettingNotify;

    private LinearLayoutCompat mLayoutSettingDetail;
    @BindView(R.id.txtStateNotification)
    AppCompatTextView tvNotificationOnOff;
    @BindView(R.id.tvNotificationDescription)
    AppCompatTextView tvNotificationDes;
    @BindView(R.id.tvTitleQuickReply)
    AppCompatTextView tvTitleQuickReply;
    @BindView(R.id.tvQuickReplyDes)
    AppCompatTextView tvQuickReplyDescription;
    @BindView(R.id.txtQuickReplyUnlock)
    AppCompatTextView tvQuickReplyUnlock;
    @BindView(R.id.txtQuickReplyUnlockDes)
    AppCompatTextView tvQuickReplyUnlockDes;
    @BindView(R.id.txtAllowPreviewMessage)
    AppCompatTextView tvAllowPreviewMessage;
    @BindView(R.id.txtAllowPreviewMessageDes)
    AppCompatTextView tvAllowPreviewMessageDes;
    @BindView(R.id.txtNotifyNewUser)
    AppCompatTextView tvNotifyNewUser;
    @BindView(R.id.txtNotifyNewUserDes)
    AppCompatTextView tvNofityNewUserDes;
    @BindView(R.id.txtSettingRingtone)
    AppCompatTextView tvSettingRingtone;
    @BindView(R.id.txtSettingRingtoneDes)
    AppCompatTextView tvSettingRingtoneDes;
    @BindView(R.id.txtSettingVibrate)
    AppCompatTextView tvSettingVibrate;
    @BindView(R.id.txtSettingVibrateDes)
    AppCompatTextView tvSettingVibrateDes;

    //    @BindView(R.id.txtTimeStateNotification)
//    protected AppCompatTextView tvNotificationTimeOff;
    private static final String SDF_IN_DAY = "HH:mm";
    private static final String SDF_IN_YEAR = "dd/MM";
    private static final String SDF_OTH_YEAR = "dd/MM/yyyy";
    private static final boolean SUPPORT_ON_OFF_NOTI = Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION;

    public static SettingNotificationFragment newInstance() {
        SettingNotificationFragment f = new SettingNotificationFragment();
        return f;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        mRes = mParentActivity.getResources();
        /*try {
            mListener = (OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }*/
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        SettingWorkManager.startWorkOnTimeNotiSetting(SettingBusiness.getInstance(mApplication).getTimeToStartSettingNoti());
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    private void findComponentViews(View rootView) {
//        initActionbar(inflater);
        mLayoutSettingDetail = rootView.findViewById(R.id.layout_setting_detail_notify);
        mViewSettingOnOffNotify = rootView.findViewById(R.id.setting_on_off_notify);
        mViewPreviewMessage = rootView.findViewById(R.id.setting_preview_msg);
        mViewQuickReply = rootView.findViewById(R.id.setting_quick_reply);
        mViewUnLockQuickReply = rootView.findViewById(R.id.setting_quick_reply_unlock);
        mViewNotifyNewUser = rootView.findViewById(R.id.setting_notify_new_user);
        mViewOnRingtone = rootView.findViewById(R.id.setting_ringtone_new_message);
        mViewVibrate = rootView.findViewById(R.id.setting_on_vibrate);
        mToggleSettingNotify = rootView.findViewById(R.id.setting_on_off_notify_toggle);
        mTogglePreviewMessage = rootView.findViewById(R.id.setting_preview_msg_toggle);
        mToggleQuickReply = rootView.findViewById(R.id.setting_quick_reply_toggle);
        mToggleUnLockQuickReply = rootView.findViewById(R.id.setting_quick_reply_unlock_toggle);
        mToggleNotifyNewUser = rootView.findViewById(R.id.setting_notify_new_user_toggle);
        mToggleOnRingtone = rootView.findViewById(R.id.setting_ringtone_new_message_toggle);
        mToggleVibrate = rootView.findViewById(R.id.setting_on_vibrate_toggle);
        drawDetail();
    }

    @Override
    public String getName() {
        return "Notification setting fragment";
    }

    @Override
    public void onResume() {
        super.onResume();
        drawStateSettingOnOffNotify();
        drawStatePreviewMessage();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_notification_setting_v5;
    }

    @Override
    protected void initView(View view) {
        findComponentViews(view);
        setTitle(R.string.notification);
        setViewListener();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_on_off_notify:
                mToggleSettingNotify.setChecked(!mToggleSettingNotify.isChecked());
                break;
            case R.id.setting_preview_msg:
                mTogglePreviewMessage.setChecked(!mTogglePreviewMessage.isChecked());
                break;
            case R.id.setting_quick_reply:
                mToggleQuickReply.setChecked(!mToggleQuickReply.isChecked());
                break;
            case R.id.setting_quick_reply_unlock:
                mToggleUnLockQuickReply.setChecked(!mToggleUnLockQuickReply.isChecked());
                break;
            case R.id.setting_notify_new_user:
                mToggleNotifyNewUser.setChecked(!mToggleNotifyNewUser.isChecked());
                break;
            case R.id.setting_ringtone_new_message:
                mToggleOnRingtone.setChecked(!mToggleOnRingtone.isChecked());
                break;
            case R.id.setting_on_vibrate:
                mToggleVibrate.setChecked(!mToggleVibrate.isChecked());
                break;
        }
    }


    private void setViewListener() {
        mViewSettingOnOffNotify.setOnClickListener(this);
//        mImgBack.setOnClickListener(this);
        mViewPreviewMessage.setOnClickListener(this);
        mViewQuickReply.setOnClickListener(this);
        mViewUnLockQuickReply.setOnClickListener(this);
        mViewNotifyNewUser.setOnClickListener(this);
        mViewOnRingtone.setOnClickListener(this);
        mViewVibrate.setOnClickListener(this);

//        mToggleSettingNotify.setClickable(false);
//        mTogglePreviewMessage.setOnClickListener(this);
//        mToggleUnLockQuickReply.setOnClickListener(this);

        mToggleSettingNotify.setOnCheckedChangeListener(this);
        mToggleVibrate.setOnCheckedChangeListener(this);
        mToggleOnRingtone.setOnCheckedChangeListener(this);
        mToggleQuickReply.setOnCheckedChangeListener(this);
        mToggleNotifyNewUser.setOnCheckedChangeListener(this);
        mTogglePreviewMessage.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
        switch (view.getId()) {
            case R.id.setting_on_off_notify_toggle:
                processSettingOnOffNotify();
                break;
            case R.id.setting_on_vibrate_toggle:
                processSettingVibrate();
                break;
            case R.id.setting_ringtone_new_message_toggle:
                processSettingOnRingtone();
                break;
            case R.id.setting_quick_reply_toggle:
                processSettingQuickReply();
                break;
            case R.id.setting_notify_new_user_toggle:
                processSettingNotifyNewUser();
                break;
            case R.id.setting_preview_msg_toggle:
                processSettingPreviewMessage();
                break;
        }

    }


//    @Override
//    public boolean onTouch(View view, MotionEvent motionEvent) {
//
//        return false;
//    }


    private void drawDetail() {
        mViewSettingOnOffNotify.setVisibility(SUPPORT_ON_OFF_NOTI ? View.VISIBLE : View.GONE);
        mToggleSettingNotify.setChecked(mSettingBusiness.getPrefSettingOnNoti());
        mTogglePreviewMessage.setChecked(mSettingBusiness.getPrefPreviewMsg());
        mToggleQuickReply.setChecked(mSettingBusiness.getPrefMsgInPopup());
//        mToggleUnLockQuickReply.setChecked(mSettingBusiness.getPrefEnableUnlock());
        mToggleNotifyNewUser.setChecked(mSettingBusiness.getPrefSettingNotifyNewUser());
        mToggleOnRingtone.setChecked(mSettingBusiness.getPrefRingtone());
        mToggleVibrate.setChecked(mSettingBusiness.getPrefVibrate());
        drawStateSettingOnOffNotify();
        drawStatePreviewMessage();
    }

    private void drawStatePreviewMessage() {
        if (mSettingBusiness.getPrefPreviewMsg()) {
            mViewQuickReply.setEnabled(true);
            mToggleQuickReply.setEnabled(true);
            tvTitleQuickReply.setEnabled(true);
            tvQuickReplyDescription.setEnabled(true);
            if (mSettingBusiness.getPrefMsgInPopup()) {
                mViewUnLockQuickReply.setEnabled(true);
                mToggleUnLockQuickReply.setEnabled(true);
                tvQuickReplyUnlock.setEnabled(true);
                tvQuickReplyUnlockDes.setEnabled(true);
            } else {
                mViewUnLockQuickReply.setEnabled(false);
                mToggleUnLockQuickReply.setEnabled(false);
                tvQuickReplyUnlock.setEnabled(false);
                tvQuickReplyUnlockDes.setEnabled(false);
            }
        } else {
            mViewQuickReply.setEnabled(false);
            mViewUnLockQuickReply.setEnabled(false);
            mToggleQuickReply.setEnabled(false);
            tvTitleQuickReply.setEnabled(false);
            tvQuickReplyDescription.setEnabled(false);
            mViewUnLockQuickReply.setEnabled(false);
            mToggleUnLockQuickReply.setEnabled(false);
            tvQuickReplyUnlock.setEnabled(false);
            tvQuickReplyUnlockDes.setEnabled(false);
        }
    }

    private void drawStateSettingOnOffNotify() {
        if (!SUPPORT_ON_OFF_NOTI) return;
        if (mSettingBusiness.getPrefSettingOnNoti() && NotificationManagerCompat.from(mApplication).areNotificationsEnabled()) {
//            mLayoutSettingDetail.setEnabled(true);
//            mToggleSettingNotify.setChecked(true);
            changeViewEnable(true);
            tvNotificationOnOff.setText(mRes.getString(R.string.setting_on_notify));
            tvNotificationDes.setVisibility(View.GONE);
        } else {
            changeViewEnable(false);
//            mLayoutSettingDetail.setEnabled(false);
            if (NotificationManagerCompat.from(mApplication).areNotificationsEnabled()) {
//                mToggleSettingNotify.setChecked(false);
                tvNotificationDes.setVisibility(View.VISIBLE);
                tvNotificationDes.setText(formatCommonTime(mSettingBusiness.getSettingUntilTime(), System.currentTimeMillis(), mRes));
                tvNotificationOnOff.setText(mRes.getString(R.string.setting_off_noti));
            } else {
//                mToggleSettingNotify.setChecked(false);
                tvNotificationOnOff.setText(mRes.getString(R.string.setting_off_noti));
            }
        }
    }

    private void changeViewEnable(boolean isEnable) {
        mViewPreviewMessage.setEnabled(isEnable);
        mViewQuickReply.setEnabled(isEnable);
        mViewUnLockQuickReply.setEnabled(isEnable);
        mViewNotifyNewUser.setEnabled(isEnable);
        mViewOnRingtone.setEnabled(isEnable);
        mViewVibrate.setEnabled(isEnable);
        mToggleVibrate.setEnabled(isEnable);
        mToggleOnRingtone.setEnabled(isEnable);
        mToggleQuickReply.setEnabled(isEnable);
        mToggleNotifyNewUser.setEnabled(isEnable);
        mTogglePreviewMessage.setEnabled(isEnable);
        tvTitleQuickReply.setEnabled(isEnable);
        tvQuickReplyDescription.setEnabled(isEnable);
        tvQuickReplyUnlock.setEnabled(isEnable);
        tvQuickReplyUnlockDes.setEnabled(isEnable);
        tvAllowPreviewMessage.setEnabled(isEnable);
        tvAllowPreviewMessageDes.setEnabled(isEnable);
        tvNotifyNewUser.setEnabled(isEnable);
        tvNofityNewUserDes.setEnabled(isEnable);
        tvSettingRingtone.setEnabled(isEnable);
        tvSettingRingtoneDes.setEnabled(isEnable);
        tvSettingVibrate.setEnabled(isEnable);
        tvSettingVibrateDes.setEnabled(isEnable);
    }


    private void processSettingOnOffNotify() {
        if (!SUPPORT_ON_OFF_NOTI) return;

        if (!NotificationManagerCompat.from(mApplication).areNotificationsEnabled()) {
            // go to settings
            try {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", mParentActivity.getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                return;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                if (mParentActivity != null) {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }

        boolean state = mToggleSettingNotify.isChecked();
        if (!state) {
            DialogRadioSelect dialogRadioSelect = DialogRadioSelect.newInstance(DialogRadioSelect.TURN_OFF_NOTIFICATION_TYPE
                    , -1, R.string.setting_off_notify, R.string.cancel_dialog_notifi);
            dialogRadioSelect.setSelectListener(new BaseDialogFragment.DialogListener() {
                @Override
                public void dialogRightClick(int value) {
                    if (value >= 0 && value < 4) {
                        mSettingBusiness.setPrefSettingOnOffNoti(false, value);
                        drawStateSettingOnOffNotify();
                        long time = mSettingBusiness.getPrefSettingOnOffNotiTime(value);
                        if (time > 0) {
                            SettingWorkManager.startWorkOnTimeNotiSetting(time);
                        }
                    }
                }

                @Override
                public void dialogLeftClick() {
                    mToggleSettingNotify.setOnCheckedChangeListener(null);
                    mToggleSettingNotify.setChecked(true);
                    mToggleSettingNotify.setOnCheckedChangeListener(SettingNotificationFragment.this);
                }
            });
            dialogRadioSelect.setCancelable(false);
            dialogRadioSelect.show(getChildFragmentManager(), DialogRadioSelect.TAG);
        } else {
//            mToggleSettingNotify.setChecked(true);
            mSettingBusiness.setPrefSettingOnOffNoti(true, -1);
            drawStateSettingOnOffNotify();
        }
    }

    private void processSettingOnNoti() {
        if (!SUPPORT_ON_OFF_NOTI) return;
        mToggleSettingNotify.setChecked(true);
        mSettingBusiness.setPrefSettingOnOffNoti(true, -1);
        drawStateSettingOnOffNotify();
    }

    private void processSettingPreviewMessage() {
        boolean state = mTogglePreviewMessage.isChecked();
        mSettingBusiness.setPrefPreviewMsg(state);
        drawStatePreviewMessage();
    }

    private void processSettingQuickReply() {
        boolean state = mToggleQuickReply.isChecked();
        mSettingBusiness.setPrefMsgInPopup(state);
        drawStatePreviewMessage();
    }

    private void processSettingUnLockQuickReply() {
        boolean state = mToggleUnLockQuickReply.isChecked();
        mSettingBusiness.setPrefEnableUnlock(state);
    }

    private void processSettingNotifyNewUser() {
        boolean state = mToggleNotifyNewUser.isChecked();
        mSettingBusiness.setPrefSettingNotifyNewUser(state);
    }

    private void processSettingOnRingtone() {
        boolean newState = mToggleOnRingtone.isChecked();
        mSettingBusiness.setPrefRingtone(newState);
        mApplication.deleteAllNotifyChannel();
    }

    private void processSettingVibrate() {
        boolean state = mToggleVibrate.isChecked();
        mSettingBusiness.setPrefVibrate(state);
        mApplication.deleteAllNotifyChannel();
    }
//
//    @Override
//    public void onSettingOffTimeSelected(int postion) {
//        if (postion >= 0 && postion < 4) {
//            mToggleSettingNotify.setChecked(false);
//            mSettingBusiness.setPrefSettingOnOffNoti(false, postion);
//            drawStateSettingOnOffNotify();
//            long time = mSettingBusiness.getPrefSettingOnOffNotiTime(postion);
//            if (time > 0) {
//                SettingWorkManager.startWorkOnTimeNotiSetting(time);
//            }
//        }
//    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSettingNotiEvent(SettingNotificationEvent event) {
        if (event.isOn()) {
            processSettingOnNoti();
        }
    }


    public static class SettingNotificationEvent {
        boolean isOn;

        public SettingNotificationEvent(boolean isOn) {
            this.isOn = isOn;
        }

        public boolean isOn() {
            return isOn;
        }

        public void setOn(boolean on) {
            isOn = on;
        }
    }

    public static String formatCommonTime(long mTime, long currentTime, Resources res) {
        SimpleDateFormat inDay = new SimpleDateFormat(SDF_IN_DAY);
        SimpleDateFormat inYear = new SimpleDateFormat(SDF_IN_YEAR);
        SimpleDateFormat othYear = new SimpleDateFormat(SDF_OTH_YEAR);
        Calendar currentCal = Calendar.getInstance();
        currentCal.setTimeInMillis(currentTime);

        Calendar desTime = Calendar.getInstance();
        desTime.setTimeInMillis(mTime);

        int currentDay = currentCal.get(Calendar.DAY_OF_YEAR);
        int desDay = desTime.get(Calendar.DAY_OF_YEAR);
        int currentMonth = currentCal.get(Calendar.MONTH);
        int desMonth = desTime.get(Calendar.MONTH);
        int currentYear = currentCal.get(Calendar.YEAR);
        int desYear = desTime.get(Calendar.YEAR);

        if (desYear == currentYear) {
            if (desMonth == currentMonth) {
                if (desDay == currentDay) {
                    return res.getString(R.string.setting_off_time_until_inday, inDay.format(mTime));
                } else {
                    int dayOfYear = desDay - currentDay;
                    if (dayOfYear == 1) {// ngay mai
                        return res.getString(R.string.setting_off_time_until_tomorrow, inDay.format(mTime));
                    } else {
                        return res.getString(R.string.setting_off_time_until_inday, inDay.format(mTime) + ", " + inYear.format(mTime));
                    }
                }
            } else {
                return res.getString(R.string.setting_off_time_until_inday, inDay.format(mTime) + ", " + inYear.format(mTime));
            }
        } else {
            return res.getString(R.string.setting_off_time_until_inday, inDay.format(mTime) + ", " + othYear.format(mTime));
        }
    }
}
package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by tungt on 2/1/2016.
 */
public class DataPackDetailFragment extends Fragment {
    private static final String TAG = DataPackDetailFragment.class.getSimpleName();
    private SettingActivity mParentActivity;
    private Resources mRes;
    private ApplicationController mApplication;
    private View mActionBarView;
    private WebView mWebView;
    private EllipsisTextView mTvwTitle;
    private ImageView mImgBack;
    private ImageView mImgOption;
    private TextView mTvNoConnection;
    private ProgressLoading mLoading;
    private Button mBtnUnVip;

    public static DataPackDetailFragment newInstance() {
        DataPackDetailFragment fragment = new DataPackDetailFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SettingActivity) activity;
        mRes = mParentActivity.getResources();
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pack_data_detail, container, false);
        findComponentViews(view, inflater);
        setListener();
        loadWebView();
        return view;
    }

    private void findComponentViews(View view, LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        mActionBarView = mParentActivity.getToolBarView();

        mTvwTitle = mActionBarView.findViewById(R.id.ab_title);
        mImgBack = mActionBarView.findViewById(R.id.ab_back_btn);
        mImgOption = mActionBarView.findViewById(R.id.ab_more_btn);
        mTvNoConnection = view.findViewById(R.id.text_detail);
        mLoading = view.findViewById(R.id.progressbar_loading);
        mWebView = view.findViewById(R.id.webview);
        mBtnUnVip = view.findViewById(R.id.button_un_vip);
        mImgOption.setVisibility(View.GONE);
        mTvwTitle.setText(mRes.getString(R.string.setting_data_pack_info));
        mWebView.getSettings().setJavaScriptEnabled(false);
    }

    private void setListener() {
        setBackListener();
        mWebView.setOnKeyListener((v, keyCode, event) -> {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            }
            return false;
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress < 20) {
                    newProgress = 20;
                }
                mLoading.setProgress(newProgress);
                if (newProgress >= 95) {
                    mLoading.setVisibility(View.INVISIBLE);
                } else {
                    mLoading.setVisibility(View.VISIBLE);
                }
            }
        });

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView web, String url) {
                Log.i(TAG, "onPageFinished url: " + url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                Log.i(TAG, "shouldOverrideUrlLoading url: " + url);
                return Utilities.processOnShouldOverrideUrlLoading(mParentActivity, webView, url);
            }
        });
        setButtonUnVipListener();
    }

    private void setButtonUnVipListener() {
        mBtnUnVip.setOnClickListener(view -> {
            if (NetworkHelper.isConnectInternet(mApplication)) {
                ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, mParentActivity,
                        mApplication.getConfigBusiness().getSubscriptionConfig().getUnVip(),
                        mApplication.getConfigBusiness().getSubscriptionConfig().getCmdCancel(), "chi_tiet_goi_cuoc", true);
            } else {
                mParentActivity.showToast(R.string.error_internet_disconnect);
            }
        });
    }

    private void loadWebView() {
        if (NetworkHelper.isConnectInternet(mApplication)) {
            String url = getUrlSubscriptionDetail();
            Log.i(TAG, "WebView.loadUrl: " + url);
            mWebView.loadUrl(url);
            if (mApplication.getReengAccountBusiness().isVip()) {
                mBtnUnVip.setVisibility(View.VISIBLE);
            } else {
                mBtnUnVip.setVisibility(View.GONE);
            }
        } else {
            mBtnUnVip.setVisibility(View.GONE);
            mLoading.setVisibility(View.GONE);
            mTvNoConnection.setVisibility(View.VISIBLE);
            //            mParentActivity.showToast(R.string.no_connectivity_check_again);
        }
    }

    private String getUrlSubscriptionDetail() {
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        StringBuilder sb = new StringBuilder();
        String msmidn = HttpHelper.EncoderUrl(accountBusiness.getJidNumber());
        String timestamp = String.valueOf(TimeHelper.getCurrentTime());
        String cmd = mApplication.getConfigBusiness().getSubscriptionConfig().getCmdCancel();
        sb.append(msmidn).append(cmd).append(accountBusiness.getToken()).append(timestamp);
        String security = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken()));
        String urlGetDataPackInfo = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_PACK_DATA_INFO);
        String url = String.format(urlGetDataPackInfo,
                timestamp,
                security,
                msmidn) + "&cmd=" + HttpHelper.EncoderUrl(cmd);
        Log.d(TAG, "getUrlSubscriptionDetail: " + url);
        return url;
    }

    private void setBackListener() {
        mImgBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }
}

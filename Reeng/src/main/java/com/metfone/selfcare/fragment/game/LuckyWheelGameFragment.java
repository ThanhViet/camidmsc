package com.metfone.selfcare.fragment.game;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelArrayAdapter;
import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.WheelSpin;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.risenumber.CountAnimationTextView;
import com.metfone.selfcare.util.Log;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thanh on 3/16/2015.
 */
public class LuckyWheelGameFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = LuckyWheelGameFragment.class.getSimpleName();
    private static final int NUMBER_WHEEL = 4;
    private static final int ITEM_COUNT = 15;
    private static final int ANGLE_SLOW_DOWN = 147;
    private static final int ANGLE_CIRCLE = 360;
    private float angleSlow = 1000000;
    private int itemSelect = 0;
    private View viewParent;
    private WheelView wheelView;
    private Button mBtnPlay;
    private TextView mTvwTerm;
    private List<String> mEntryPrize;
    private View mImgBudgetHelp;
    private ImageView mImgBtnWheel, mImgShake, mImgGiftSonTung;

    private ImageView mImgReloadBudgetValue;
    private CountAnimationTextView mCountAnimationTextView;

    private int resultFromServer = -1;
    private boolean needShowPopup = false;

    private ListGamesActivity mActivity;
    private ApplicationController mApplication;
    private LuckyWheelHelper mLuckyWheelHelper;
    private Resources mRes;
    private Handler mHanlder;
    private boolean noNeedShowDialogWheelEnd = true;
    private int currentLottPoint;
    private Animation shakeAnimation;

    public LuckyWheelGameFragment() {
    }

    public static LuckyWheelGameFragment newInstance() {
        LuckyWheelGameFragment fragment = new LuckyWheelGameFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
        mHanlder = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_lucky_wheel, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        setViewComponent(rootView);
        initWheelView();
        initShakeView();
        setViewListener();
        Glide.with(mApplication).load(R.drawable.bg_lucky_wheel_v2).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                wheelView.setWheelDrawable(resource);
            }
        });
        Glide.with(mApplication).load(R.drawable.ic_bg_vqmm).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                viewParent.setBackground(resource);
            }
        });
        mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LUCKEY_WHEEL_LAST_TIME_CLICK,
                System.currentTimeMillis()).apply();
        return rootView;
    }

    private void setToolbar(LayoutInflater inflater) {
        View abView = mActivity.getToolBarView();
        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_one_action, null));
        ImageView mAbOption = abView.findViewById(R.id.ab_more_btn);
        ImageView mBtnBack = abView.findViewById(R.id.ab_back_btn);
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mRes.getString(R.string.lucky_wheel_title));
        mAbOption.setImageResource(R.drawable.ic_action_accumulate);
        mBtnBack.setOnClickListener(this);
        mAbOption.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: itemSelect" + itemSelect);
        if (needShowPopup) {
            wheelClick();
            mHanlder.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (resultFromServer != -1) {
                        setAngleSlowDown(resultFromServer);
                    } else {
                        wheelView.setSlowSpeed(false);
                    }
                }
            }, 150);
        } else {
            drawBtnCenterWheel(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (wheelView.isWheelRotating()) {
            wheelView.setPosition(0);
        }
        wheelView.setWheelStop();
    }

    @Override
    public void onStop() {
        // VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG);
        super.onStop();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getData(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("needShowPopup", needShowPopup);
        outState.putBoolean("noNeedShowDialogWheelEnd", noNeedShowDialogWheelEnd);
        outState.putInt("resultFromServer", resultFromServer);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        mImgShake.clearAnimation();
        mImgReloadBudgetValue.clearAnimation();
        if (shakeAnimation != null) shakeAnimation.cancel();
        super.onDestroyView();
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            needShowPopup = savedInstanceState.getBoolean("needShowPopup");
            noNeedShowDialogWheelEnd = savedInstanceState.getBoolean("noNeedShowDialogWheelEnd");
            resultFromServer = savedInstanceState.getInt("resultFromServer");
        }
    }

    // khoi tao du lieu cho WheelView
    private void initWheelView() {

//        mPrize = Arrays.asList(getResources().getStringArray(R.array.item_prize));
        mEntryPrize = new ArrayList<>(ITEM_COUNT);
        for (int i = 0; i < ITEM_COUNT; i++) {
//            Map.Entry<Integer, String> entry = new AbstractMap.SimpleEntry<Integer, String>(i, "");
            mEntryPrize.add("");
        }
        wheelView.setAdapter(new WheelViewAdapter(mEntryPrize));
        wheelView.setPosition(0);
    }

    private void initShakeView() {
        if (mApplication.getConfigBusiness().isEnableSonTung83()) {
            mImgShake.setVisibility(View.GONE);
            mImgReloadBudgetValue.setVisibility(View.GONE);
            mImgGiftSonTung.setVisibility(View.VISIBLE);
        } else if (mApplication.getConfigBusiness().isShakeGame()) {
            mImgShake.setVisibility(View.VISIBLE);
            mImgGiftSonTung.setVisibility(View.GONE);
            mImgReloadBudgetValue.setVisibility(View.GONE);
            shakeAnimation = AnimationUtils.loadAnimation(mApplication, R.anim.shake_lw);
            mImgShake.startAnimation(shakeAnimation);
        } else {
            mImgGiftSonTung.setVisibility(View.GONE);
            mImgShake.setVisibility(View.GONE);
            mImgReloadBudgetValue.setVisibility(View.GONE);
        }
    }

    private void setViewComponent(View rootView) {
        viewParent = rootView.findViewById(R.id.viewParent);
        wheelView = rootView.findViewById(R.id.wheelview);
        mBtnPlay = rootView.findViewById(R.id.btn_wheel);
        mTvwTerm = rootView.findViewById(R.id.lucky_wheel_term);
        mImgShake = rootView.findViewById(R.id.btn_shake);
        mImgGiftSonTung = rootView.findViewById(R.id.btn_gift);
        wheelView.setEnabled(false);
        wheelView.setWheelEnable(false);
        mTvwTerm.setText(TextHelper.fromHtml(mRes.getString(R.string.lucky_wheel_term)));

        mImgReloadBudgetValue = rootView.findViewById(R.id.img_reload_budget_value);
        mCountAnimationTextView = rootView.findViewById(R.id.tvw_budget_value);
        //mCountAnimationTextView.setDecimalFormat(new DecimalFormat("###,###,###"));

        DecimalFormat df = new DecimalFormat("###,###,###");
        DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
        sym.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(sym);
        mCountAnimationTextView.setDecimalFormat(df);

        currentLottPoint = LuckyWheelHelper.getInstance(mApplication).getCurrentLottPoint();
        mImgReloadBudgetValue.clearAnimation();
        // set value
        mCountAnimationTextView.setAnimationDuration(1).countAnimation(0, currentLottPoint > 0L ? currentLottPoint : 0);
        mImgBudgetHelp = rootView.findViewById(R.id.img_budget_help);
        mImgBtnWheel = rootView.findViewById(R.id.img_btn_wheel);
        drawBtnCenterWheel(false);
        getLottPoint(false);
    }

    private void setViewListener() {
        setWheelViewListener();
        mBtnPlay.setOnClickListener(this);
        mTvwTerm.setOnClickListener(this);
        mImgReloadBudgetValue.setOnClickListener(this);
        mImgBtnWheel.setOnClickListener(this);
        mImgBudgetHelp.setOnClickListener(this);
        mImgShake.setOnClickListener(this);
        mImgGiftSonTung.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mActivity.onBackPressed();
                break;
            case R.id.ab_more_btn:
                mActivity.navigateToAccumulateFragment();
                break;
            case R.id.btn_wheel:
                enableClickWheel(false);
                checkAndRequestSpinWheel();
                break;
            case R.id.lucky_wheel_term:
                UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(mApplication, mActivity, mRes.getString(R
                        .string.lucky_wheel_term_url));
                break;
            case R.id.img_reload_budget_value:
                getLottPoint(true);
                break;
            case R.id.img_budget_help:
                UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(mApplication, mActivity, mRes.getString(R
                        .string.lucky_wheel_budget_help));
                break;
            case R.id.img_btn_wheel:
                enableClickWheel(false);
                checkAndRequestSpinWheel();
                break;
            case R.id.btn_shake:
                NavigateActivityHelper.navigateToShakeGame(mActivity);
                break;
            case R.id.btn_gift:
                NavigateActivityHelper.navigateToGift83Activity(mActivity);
                break;
        }
    }

    private void enableClickWheel(boolean enable) {
        mBtnPlay.setEnabled(enable);
        mImgBtnWheel.setEnabled(enable);
    }

    private void checkAndRequestSpinWheel() {
        mLuckyWheelHelper = LuckyWheelHelper.getInstance(mApplication);
        if (mLuckyWheelHelper.isExistMission()) {
            mLuckyWheelHelper.showPopupExistMission(mActivity);
            enableClickWheel(true);
        } else if (!NetworkHelper.isConnectInternet(mApplication)) {
            mLuckyWheelHelper.showPopupError(mActivity, mRes.getString(R.string.lucky_wheel_err_network));
            enableClickWheel(true);
        } else {
            needShowPopup = true;
            resultFromServer = -1;
            wheelClick();
            mLuckyWheelHelper.requestWheelSpin(new LuckyWheelHelper.WheelSpinRequestListener() {
                @Override
                public void onSuccess(WheelSpin wheelSpin, int lottPoint) {
                    noNeedShowDialogWheelEnd = false;
                    //resultFromServer = wheelSpin.getId();// id item quay được
                    resultFromServer = LuckyWheelHelper.getPositionWheelSpin(wheelSpin.getId());
                    setAngleSlowDown(resultFromServer);
                    long durration = 3000;
                    if (lottPoint < currentLottPoint) {
                        durration = 0;
                    }
                    mCountAnimationTextView.setAnimationDuration(durration).countAnimation(currentLottPoint, lottPoint);
                    currentLottPoint = lottPoint;
                }

                @Override
                public void onOutOfTurn() {
                    noNeedShowDialogWheelEnd = true;
                    needShowPopup = false;
                    mLuckyWheelHelper.showPopupOutOfTurn(mActivity);
                    enableClickWheel(true);
                    setSlowDownError();
                    //setAngleSlowDown(new Random().nextInt(15));
                }

                @Override
                public void onError(int code, String desc) {
                    noNeedShowDialogWheelEnd = true;
                    needShowPopup = false;
                    String msgError;
                    if (code == LuckyWheelHelper.PAUSE_GAME && !TextUtils.isEmpty(desc)) {
                        msgError = desc;
                    } else {
                        msgError = mRes.getString(R.string.request_send_error);
                    }
                    mLuckyWheelHelper.showPopupError(mActivity, msgError);
                    enableClickWheel(true);
                    setSlowDownError();
                }
            });
        }
    }

    private void setWheelViewListener() {
        wheelView.setOnWheelStartListener(new WheelView.OnWheelStartListener() {
            @Override
            public void onWheelStart() {
                drawBtnCenterWheel(true);
                //angleSlow = wheelView.getAngle() + ANGLE_CIRCLE * 100;
                angleSlow = calculatorAngSlow(0, 100);
                wheelView.setWheelEnable(false);
                wheelView.setSlowSpeed(false);
                Log.i(TAG, "setOnWheelStartListener: angleSlow" + angleSlow +
                        "---wheelView.getAngle: " + wheelView.getAngle());
            }
        });

        wheelView.setOnWheelAngleChangeListener(new WheelView.OnWheelAngleChangeListener() {
            @Override
            public void onWheelAngleChange(float angle) {
                if (angle >= angleSlow) {
                    wheelView.setSlowSpeed(true);
                }
            }
        });

        wheelView.setOnWheelStopListener(new WheelView.OnWheelStopListener() {
            @Override
            public void onWheelStop() {
                drawBtnCenterWheel(false);
                wheelView.setSlowSpeed(false);
                if (!noNeedShowDialogWheelEnd) {
                    needShowPopup = false;
                    noNeedShowDialogWheelEnd = true;
                    if (resultFromServer != -1)
                        mLuckyWheelHelper.showPopupLuckyWheel(mActivity);
                    else
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    enableClickWheel(true);
                }
                if (!mBtnPlay.isEnabled()) {
                    enableClickWheel(true);
                }
            }
        });
    }

    public void setAngleSlowDown(int result) {
        if (result < 0) {
            mApplication.trackingException("setAngleSlowDown error: " + result, false);
            result = 0;
        }
        itemSelect = result;
        angleSlow = calculatorAngSlow(itemSelect, NUMBER_WHEEL);
        Log.i(TAG, "setAngleSlowDown----itemSelect: " + itemSelect + "----angleSlow" + angleSlow);
    }

    private void setSlowDownError() {
        mHanlder.postDelayed(new Runnable() {
            @Override
            public void run() {
                angleSlow = calculatorAngSlow(2, NUMBER_WHEEL);
            }
        }, 1000);
    }

    private float calculatorAngSlow(int position, int numberSpin) {
        int tmp = (int) wheelView.getAngle() / ANGLE_CIRCLE;
        float angle = ANGLE_CIRCLE * numberSpin + (tmp + 1) * ANGLE_CIRCLE +
                (ITEM_COUNT - 1 - position) * ANGLE_CIRCLE / ITEM_COUNT - ANGLE_SLOW_DOWN - 7.5f;
        return angle;
    }

    private void wheelClick() {
        Log.i(TAG, "click play wheel");
        wheelView.setEnabled(true);
        wheelView.setWheelEnable(true);
        mHanlder.postDelayed(new Runnable() {
            @Override
            public void run() {
                int posX = 2 * mApplication.getWidthPixels() / 3;
                int posY = mApplication.getHeightPixels() / 3;
                int deltaX = mApplication.getWidthPixels() / 100;
                int deltaY = mApplication.getHeightPixels() / 100;
                MotionEvent motionEventDown = MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN,
                        posX, posY, 1.0f, 1.0f, 0, 1.0f, 1.0f, 9, 0x0);
                wheelView.dispatchTouchEvent(motionEventDown);
                MotionEvent motionEventMove = MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,
                        posX, posY, 1.0f, 1.0f, 0, 1.0f, 1.0f, 9, 0x0);
                wheelView.dispatchTouchEvent(motionEventMove);
                MotionEvent motionEventMove2 = MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,
                        posX + deltaX, posY + deltaY, 1.0f, 1.0f, 0, 1.0f, 1.0f, 9, 0x0);
                wheelView.dispatchTouchEvent(motionEventMove2);
                MotionEvent motionEventMove3 = MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,
                        posX + 2 * deltaX, posY + 2 * deltaY, 1.0f, 1.0f, 0, 1.0f, 1.0f, 9, 0x0);
                wheelView.dispatchTouchEvent(motionEventMove3);
                MotionEvent motionEventUp = MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,
                        posX + 2 * deltaX, posY + 2 * deltaY, 1.0f, 1.0f, 0, 1.0f, 1.0f, 9, 0x0);
                wheelView.dispatchTouchEvent(motionEventUp);
                wheelView.setEnabled(false);
                wheelView.setWheelEnable(false);
            }
        }, 50);
    }

    private void getLottPoint(final boolean showError) {
        Animation an = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation
                .RELATIVE_TO_SELF, 0.5f);
        an.setDuration(1000);
        an.setRepeatCount(-1);
        an.setRepeatMode(Animation.RESTART);
        an.setFillAfter(false);
        // mImgReloadBudgetValue.setAnimation(an);
        mImgReloadBudgetValue.startAnimation(an);
        LuckyWheelHelper.getInstance(mApplication).getValueBudgetLuckyWheel(new LuckyWheelHelper
                .GetValueBudgetListener() {
            @Override
            public void onGetValueBudgetSuccess(int lottPoint) {
                long durration = 3000;
                if (lottPoint < currentLottPoint) {
                    durration = 0;
                }
                mCountAnimationTextView.setAnimationDuration(durration).countAnimation(currentLottPoint, lottPoint);
                currentLottPoint = lottPoint;
                mImgReloadBudgetValue.clearAnimation();
            }

            @Override
            public void onGetValueError(int code, String desc) {
                mImgReloadBudgetValue.clearAnimation();
                if (showError) {
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        });
    }

    private void drawBtnCenterWheel(boolean isRunning) {
        if (isRunning) {
            ImageBusiness.setResource(mImgBtnWheel, R.drawable.ic_wheel_cursor_2_run);
            //mImgBtnWheel.setImageResource(R.drawable.ic_wheel_cursor_2_run);
        } else {
            ImageBusiness.setResource(mImgBtnWheel, R.drawable.ic_wheel_cursor_2);
            //mImgBtnWheel.setImageResource(R.drawable.ic_wheel_cursor_2);
        }
    }

    class WheelViewAdapter extends WheelArrayAdapter<String> {
        WheelViewAdapter(List<String> entries) {
            super(entries);
        }

        @Override
        public Drawable getDrawable(int position) {
            return null;
        }
    }
}
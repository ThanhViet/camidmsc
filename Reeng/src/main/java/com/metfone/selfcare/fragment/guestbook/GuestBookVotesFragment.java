package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.GuestBookVotesAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.BookVote;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.holder.guestbook.BookVoteHolder;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/25/2017.
 */
public class GuestBookVotesFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener,
        BookVoteHolder.OnBookVoteListener,
        View.OnClickListener {
    private static final String TAG = GuestBookVotesFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private GuestBookHelper mGuestBookHelper;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private TextView mAbTitle;
    private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private GuestBookVotesAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderFooterAdapter;
    //load more
    private View mFooterView;
    private LinearLayout mLoadmoreFooterView;
    private boolean isLoading = false;
    private boolean isNoLoadMore;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    private ArrayList<BookVote> bookVotes;
    private int tabType;
    private boolean needRequest = false;
    private int currentPage = 1;


    public static GuestBookVotesFragment newInstance(int type) {
        GuestBookVotesFragment fragment = new GuestBookVotesFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.GUEST_BOOK.VOTE_TAB_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public GuestBookVotesFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        needRequest = true;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        getDataAnDraw(savedInstanceState);
        setViewListener();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.GUEST_BOOK.VOTE_TAB_TYPE, tabType);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onRetryClick() {

    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (TextView) abView.findViewById(R.id.ab_title);
        mAbTitle.setText(mRes.getString(R.string.guest_book_vote_title));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbBack.setOnClickListener(this);
    }

    private void getDataAnDraw(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            tabType = savedInstanceState.getInt(Constants.GUEST_BOOK.VOTE_TAB_TYPE);
        } else if (getArguments() != null) {
            tabType = getArguments().getInt(Constants.GUEST_BOOK.VOTE_TAB_TYPE);
        }
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        if (bookVotes == null) {
            bookVotes = mGuestBookHelper.getListVotes(tabType);
        }
        if (bookVotes == null) {
            showProgressLoading();
        } else {
            drawDetail();
        }
        if (needRequest || bookVotes == null) {
            currentPage = 1;
            needRequest = false;
            mGuestBookHelper.requestListVotes(tabType, currentPage, getListVoteListener);
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLoadmoreFooterView = (LinearLayout) mFooterView.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }

    private void setViewListener() {
    }

    private void drawDetail() {
        hideEmptyView();
        setAdapter();
    }

    private void setAdapter() {
        if (mAdapter == null || mHeaderFooterAdapter == null || mRecyclerView.getAdapter() == null) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mAdapter = new GuestBookVotesAdapter(mApplication, bookVotes, this);
            mHeaderFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setAdapter(mHeaderFooterAdapter);
            mHeaderFooterAdapter.addFooterView(mFooterView);
            setItemListener();
        } else {
            mAdapter.setListItems(bookVotes);
            mHeaderFooterAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void viewDetail(BookVote bookVote) {
        getBookDetail(bookVote.getId());
    }

    private void getBookDetail(String booId) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestGetBookDetail(booId, new GuestBookHelper.GetBookDetailListener() {
            @Override
            public void onSuccess(Book bookDetail) {
                // book.setPages(bookDetail.getPages());
                mGuestBookHelper.setCurrentBookEditor(bookDetail);
                mListener.navigateToBookPreview();
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(int error) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.request_send_error);
            }
        });
    }

    private void setItemListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof BookVote) {
                    getBookDetail(((BookVote) object).getId());
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });

       /* mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadNextPage(View view) {
                super.onLoadNextPage(view);
                LoadingFooter.State state = RecyclerViewStateUtils.getFooterViewState(mRecyclerView);
                if (state == LoadingFooter.State.Loading) {
                    Log.d(TAG, "the state is Loading, just wait..");
                    return;
                }
                if (isNoLoadMore) {
                    Log.d(TAG, "no load more");
                    RecyclerViewStateUtils.setFooterViewState(mParentActivity, mRecyclerView, LoadingFooter.State.Normal, null);
                } else {
                    Log.d(TAG, "loading more");
                    RecyclerViewStateUtils.setFooterViewState(mParentActivity, mRecyclerView, LoadingFooter.State.Loading, null);
                    currentPage++;
                    mGuestBookHelper.requestListVotes(tabType, currentPage, getMoreListVoteListener);
                }
            }
        });*/
        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !bookVotes.isEmpty() && !isNoLoadMore) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }
            }
        };
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));
    }

    private void onLoadMore() {
        if (isLoading) {
            Log.i(TAG, "onLoadMore isLoading");
        } else {
            if (!isNoLoadMore) {
                mParentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoadmoreFooterView.setVisibility(View.VISIBLE);
                    }
                });
                Log.i(TAG, "onLoadMore " + currentPage);
                currentPage++;
                isLoading = true;
                mGuestBookHelper.requestListVotes(tabType, currentPage, getMoreListVoteListener);
            } else {
                Log.i(TAG, "onLoadMore nomorefeed");
            }
        }
    }

    private GuestBookHelper.GetListVoteListener getListVoteListener = new GuestBookHelper.GetListVoteListener() {
        @Override
        public void onSuccess(ArrayList<BookVote> votes) {
            currentPage = 1;
            isNoLoadMore = false;
            bookVotes = votes;
            drawDetail();
        }

        @Override
        public void onError(int error) {
            if (bookVotes == null) {
                showEmptyNote(mRes.getString(R.string.request_send_error));
            } else {
                hideEmptyView();
            }
        }
    };

    private GuestBookHelper.GetListVoteListener getMoreListVoteListener = new GuestBookHelper.GetListVoteListener() {
        @Override
        public void onSuccess(ArrayList<BookVote> votes) {
            isLoading = false;
            mLoadmoreFooterView.setVisibility(View.GONE);
            //RecyclerViewStateUtils.setFooterViewState(mParentActivity, mRecyclerView, LoadingFooter.State.Normal, null);
            if (votes.isEmpty()) {
                isNoLoadMore = true;
            }
            bookVotes.addAll(votes);
            drawDetail();
        }

        @Override
        public void onError(int error) {
            isLoading = false;
            mLoadmoreFooterView.setVisibility(View.GONE);
            //RecyclerViewStateUtils.setFooterViewState(mParentActivity, mRecyclerView, LoadingFooter.State.Normal, null);
            isNoLoadMore = true;
        }
    };

    public interface OnFragmentInteractionListener {
        void navigateToBookPreview();
    }
}
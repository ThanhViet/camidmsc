/*
package com.metfone.selfcare.fragment.contact;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.metfone.selfcare.activity.AlbumViewActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.adapter.onmedia.OMFeedAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.OfficalAccountOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.ProfileHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.FeedOnMediaListener;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.imageview.SquareImageView;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

*/
/**
 * Created by thanhnt72 on 4/26/2016.
 *//*

public class OfficialDetailFragment extends Fragment implements ClickListener.IconListener
        , OnMediaHolderListener, FeedOnMediaListener, TagMocha.OnClickTag {
    private final String TAG = OfficialDetailFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private ReengAccount mReengAccount;
    private ImageProfileBusiness mImageProfileBusiness;
    private ContactDetailActivity mParentActivity;
    private Resources mRes;

    private ClickListener.IconListener iconListener;
    //===onmedia
    private FeedBusiness mFeedBusiness;
    private OMFeedAdapter mFeedAdapter;
    private ArrayList<FeedModelOnMedia> listFeed = new ArrayList<>();
    private OnMediaInterfaceListener feedInterface;
    private boolean isLoading = false;

    private AvatarBusiness mAvatarBusiness;
    private Handler mHandler;
    private ArrayList<ImageProfile> mImageProfiles;
    private ImageProfile mImageCover;

    private WSOnMedia rest;
    private boolean noMoreFeed = false;

    private int gaCategoryId, gaActionId;
    private boolean enableClickTag = false;

    private OfficalAccountOnMedia mOfficial;
    private String officialId, officialName, avatarUrl;
    private int userType;
    private int page = 0;

    private EventOnMediaHelper eventOnMediaHelper;

    private RecyclerView mRecyclerViewFeed;
    private View mFooterView, mHeaderView;
    private LinearLayout mLoadmoreFooterView;

    private View mViewFakeToolbar, mViewFakeStatusBar, mViewBottomToolbar;
    private ImageView mImgBack, mImgOption;
    private TextView mTvwProfileNameToolbar;
    private Drawable mDrawableAb, mDrawableStatus;

    private CircleImageView mImgAvatar;
    private TextView mTvwAvatar;
    private AspectImageView mImgProfileAvatar;
    private EllipsisTextView mTvwProfileStatus;
    private TextView mTvwProfileName;
    private ImageView mImgSex;
    private TextView mTvwAge;
    private int widthBigAvatar, heightBigAvatar;

    private View mLayoutInfo;
    private TextView mTvwButtonProfile;
    private View mLayoutInfoAgePoint;
    private TextView mTvwPoint;
    //    private View mLayoutAction1, mLayoutAction2, mLayoutAction3;
    private View mViewPoint;

    private View mView3Image, mView2Image, mBtnShowAll;
    private SquareImageView[] mImageViewList;
    private int maxHeight = 500;
    private int maxHeightDefaut = 500;
    private View mLayoutActionProfile;

    private TextView mTvwAction;
    private View mLayoutMyAlbum;

    public static OfficialDetailFragment newInstance(String officialId, String officialName,
                                                     String avatarUrl, int userType) {
        OfficialDetailFragment fragment = new OfficialDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.ONMEDIA.OFFICIAL_ID, officialId);
        args.putString(Constants.ONMEDIA.OFFICIAL_NAME, officialName);
        args.putString(Constants.ONMEDIA.OFFICIAL_AVATAR, avatarUrl);
        args.putInt(Constants.ONMEDIA.OFFICIAL_USER_TYPE, userType);
        fragment.setArguments(args);
        return fragment;
    }

    public OfficialDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_official_profile;
        iconListener = this;
        eventOnMediaHelper = new EventOnMediaHelper(mParentActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        mImageViewList = new SquareImageView[5];
        getData();
        findComponentViews(rootView, inflater, container);
        setViewListeners();
        initViewFeed();
        drawPhoneNumberDetail();
        if (mApplication.getConfigBusiness().isEnableOnMedia()) {
            loadData();
        }
        return rootView;
    }

    private void initViewFeed() {
        mFeedAdapter = new OMFeedAdapter(mApplication, listFeed, this, this);
        mFeedAdapter.setActivity(mParentActivity);
        mRecyclerViewFeed.setLayoutManager(new LinearLayoutManager(mRecyclerViewFeed.getContext()));
        HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter = new
                HeaderAndFooterRecyclerViewAdapter(mFeedAdapter);
        mRecyclerViewFeed.setAdapter(mHeaderAndFooterRecyclerViewAdapter);
        mHeaderAndFooterRecyclerViewAdapter.addFooterView(mFooterView);
        mHeaderAndFooterRecyclerViewAdapter.addHeaderView(mHeaderView);
    }

    private void getData() {
        if (getArguments() != null) {
            officialId = getArguments().getString(Constants.ONMEDIA.OFFICIAL_ID);
            officialName = getArguments().getString(Constants.ONMEDIA.OFFICIAL_NAME);
            avatarUrl = getArguments().getString(Constants.ONMEDIA.OFFICIAL_AVATAR);
            userType = getArguments().getInt(Constants.ONMEDIA.OFFICIAL_USER_TYPE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (ContactDetailActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mAvatarBusiness = mApplication.getAvatarBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        mReengAccount = mAccountBusiness.getCurrentAccount();
        mImageProfileBusiness = mApplication.getImageProfileBusiness();
        mRes = mParentActivity.getResources();
        rest = new WSOnMedia(mApplication);
        try {
            feedInterface = (OnMediaInterfaceListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectionFeedInterface");
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        enableClickTag = true;
        ListenerHelper.getInstance().addNotifyFeedOnMediaListener(this);
        if (mHandler == null) {
            mHandler = new Handler();
        }
        if (mFeedAdapter != null && mFeedAdapter.getItemCount() != 0) {
            mFeedAdapter.notifyDataSetChanged();
            Log.i(TAG, "notify when resume");
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        mImgAvatar.setImageBitmap(null);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHandler = null;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            //========onmedia
            case Constants.MENU.MENU_SHARE_LINK:
                FeedModelOnMedia feed = (FeedModelOnMedia) entry;
                handleShareLink(feed);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share_now);
                break;
            case Constants.MENU.MENU_WRITE_STATUS:
                FeedModelOnMedia feedWrite = (FeedModelOnMedia) entry;
                handleWriteStatus(feedWrite);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_write_status);
                break;
            case Constants.MENU.COPY:
                FeedModelOnMedia feedCopy = (FeedModelOnMedia) entry;
//                if (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(feedCopy.getFeedContent().getItemSubType())) {
//                    TextHelper.copyToClipboard(mParentActivity, feedCopy.getFeedContent().getContentUrl());
//                } else {
//                    TextHelper.copyToClipboard(mParentActivity, feedCopy.getFeedContent().getUrl());
//                }
                TextHelper.copyToClipboard(mParentActivity, feedCopy.getFeedContent().getLink());
                mParentActivity.showToast(R.string.copy_to_clipboard);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy);
                break;
            case Constants.MENU.UNFOLLOW:
                FeedModelOnMedia feedUnfollow = (FeedModelOnMedia) entry;
                confirmUnfollow(feedUnfollow);
                break;

            case Constants.MENU.REPORT:
                FeedModelOnMedia feedReport = (FeedModelOnMedia) entry;
                confirmReport(feedReport);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_report);
                break;
            case Constants.MENU.POPUP_UNFOLLOW:
                FeedModelOnMedia feedPopupUnfollow = (FeedModelOnMedia) entry;
                handleUnfollow(feedPopupUnfollow);
                break;

            case Constants.MENU.POPUP_REPORT:
                FeedModelOnMedia feedPopupReport = (FeedModelOnMedia) entry;
                handleReport(feedPopupReport);
                break;

            case Constants.MENU.MENU_LIST_SHARE:
                FeedModelOnMedia feedListShare = (FeedModelOnMedia) entry;
                if (feedInterface != null) {
                    if (!TextUtils.isEmpty(feedListShare.getFeedContent().getUrl())) {
                        feedInterface.navigateToListShare(feedListShare.getFeedContent().getUrl());
                    }
                }
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_share);
                break;
            case Constants.MENU.POPUP_EXIT:
                FeedModelOnMedia item = (FeedModelOnMedia) entry;
                eventOnMediaHelper.handleClickPopupExitListenTogether(item, feedInterface);
                break;
            case Constants.MENU.MENU_COPY_TEXT:
                FeedModelOnMedia feedCopyText = (FeedModelOnMedia) entry;
                String textCopy = mFeedBusiness.getTextTagCopy(feedCopyText.getUserStatus(), feedCopyText.getListTag());
                TextHelper.copyToClipboard(mParentActivity, textCopy);
                mParentActivity.showToast(R.string.copy_to_clipboard);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy_text);
                break;
            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                FeedModelOnMedia feedShareFb = (FeedModelOnMedia) entry;
                OnMediaHelper.shareFacebookOnMedia(feedShareFb, mParentActivity);
                break;
            case Constants.MENU.SEND_MESSAGE:
                if (entry instanceof FeedModelOnMedia) {
                    ShareUtils.sendToFriend(mParentActivity, (FeedModelOnMedia) entry);
                }
                break;
            case Constants.MENU.MORE:
                if (entry instanceof FeedModelOnMedia) {
                    ShareUtils.shareWithIntent(mParentActivity, (FeedModelOnMedia) entry);
                }
                break;
            default:
                break;
        }
    }

    private void handleWriteStatus(final FeedModelOnMedia feedWrite) {
        eventOnMediaHelper.navigateToPostOnMedia(mParentActivity,
                feedWrite.getFeedContent(), "", feedWrite.getBase64RowId(), false,
                FeedModelOnMedia.ActionFrom.onmedia);
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mRecyclerViewFeed = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mHeaderView = inflater.inflate(R.layout.header_friend_profile, container, false);
        mLoadmoreFooterView = (LinearLayout) mFooterView.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);

        mViewFakeToolbar = rootView.findViewById(R.id.layout_ab_profile);
        mViewFakeStatusBar = rootView.findViewById(R.id.view_fake_status_bar);
        mViewBottomToolbar = rootView.findViewById(R.id.view_bottom_toolbar);
        mViewBottomToolbar.setVisibility(View.GONE);
        mDrawableAb = new ColorDrawable(ContextCompat.getColor(mApplication, R.color.white));
        mDrawableStatus = new ColorDrawable(ContextCompat.getColor(mApplication, R.color.status_bar));
        mDrawableAb.setAlpha(0);
        mDrawableStatus.setAlpha(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mViewFakeToolbar.setBackground(mDrawableAb);
            mViewFakeStatusBar.setBackground(mDrawableStatus);
        } else {
            mViewFakeToolbar.setBackgroundDrawable(mDrawableAb);
            mViewFakeStatusBar.setBackground(mDrawableStatus);
        }

        mImgBack = (ImageView) rootView.findViewById(R.id.ab_back_btn);
        mTvwProfileNameToolbar = (TextView) rootView.findViewById(R.id.ab_title);
        mImgOption = (ImageView) rootView.findViewById(R.id.ab_more_action_btn);
        mImgOption.setVisibility(View.GONE);

        mImgProfileAvatar = (AspectImageView) mHeaderView.findViewById(R.id.img_profile_avatar);
        mLayoutInfo = mHeaderView.findViewById(R.id.layout_info);
        mImgAvatar = (CircleImageView) mHeaderView.findViewById(R.id.img_profile_avatar_image);
        mTvwAvatar = (TextView) mHeaderView.findViewById(R.id.tvw_profile_avatar_text);
        mTvwButtonProfile = (TextView) mHeaderView.findViewById(R.id.tvw_button_profile);
        mTvwButtonProfile.setVisibility(View.INVISIBLE);
        mLayoutActionProfile = mHeaderView.findViewById(R.id.layout_action_profile);
        mLayoutActionProfile.setVisibility(View.GONE);
        mTvwProfileName = (TextView) mHeaderView.findViewById(R.id.tvw_profile_name);
        mLayoutInfoAgePoint = mHeaderView.findViewById(R.id.layout_info_age_point);
        mLayoutInfoAgePoint.setVisibility(View.GONE);
        mImgSex = (ImageView) mHeaderView.findViewById(R.id.img_profile_sex);
        mViewPoint = mHeaderView.findViewById(R.id.layout_info_point);
        mViewPoint.setVisibility(View.GONE);
        mTvwAge = (TextView) mHeaderView.findViewById(R.id.tvw_profile_age);
        mTvwPoint = (TextView) mHeaderView.findViewById(R.id.tvw_point_friend);
        mTvwProfileStatus = (EllipsisTextView) mHeaderView.findViewById(R.id.tvw_profile_status);
        Typeface face = Typeface.createFromAsset(mApplication.getAssets(), "fonts/utm_Helve.ttf");
        mTvwProfileStatus.setTypeface(face);
        */
/*mLayoutAction1 = mHeaderView.findViewById(R.id.layout_profile_action_1);
        mLayoutAction2 = mHeaderView.findViewById(R.id.layout_profile_action_2);
        mLayoutAction3 = mHeaderView.findViewById(R.id.layout_profile_action_3);*//*


        //image album
        View mImgUpload = mHeaderView.findViewById(R.id.btn_upload_image);
        mImgUpload.setVisibility(View.GONE);
        mBtnShowAll = mHeaderView.findViewById(R.id.tvw_view_all_album);
        mBtnShowAll.setVisibility(View.VISIBLE);

        mTvwAction = (TextView) mHeaderView.findViewById(R.id.tvw_title_action);
        mTvwAction.setVisibility(View.GONE);
        mLayoutMyAlbum = mHeaderView.findViewById(R.id.view_my_album);

        mView3Image = mHeaderView.findViewById(R.id.ll_image_view_small);
        mView2Image = mHeaderView.findViewById(R.id.ll_image_view_big);

        mImageViewList = new SquareImageView[5];
        mImageViewList[0] = (SquareImageView) mView3Image.findViewById(R.id.img_0);
        mImageViewList[1] = (SquareImageView) mView3Image.findViewById(R.id.img_1);
        mImageViewList[2] = (SquareImageView) mView3Image.findViewById(R.id.img_2);

        mImageViewList[3] = (SquareImageView) mView2Image.findViewById(R.id.img_3);
        mImageViewList[4] = (SquareImageView) mView2Image.findViewById(R.id.img_4);

        mView3Image.setVisibility(View.GONE);
        mView2Image.setVisibility(View.GONE);

        mBtnShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAlbumPreview();
            }
        });
        for (int i = 0; i < 5; i++) {
            mImageViewList[i].setVisibility(View.GONE);
        }

        DisplayMetrics displayMetrics = mApplication.getResources().getDisplayMetrics();
        int widthScreen = displayMetrics.widthPixels;
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.ratio_profile_avatar_friend, outValue, true);
        float ratio = outValue.getFloat();
        widthBigAvatar = widthScreen;
        maxHeight = Math.round((float) widthBigAvatar / ratio);
        maxHeightDefaut = maxHeight;
        if (widthBigAvatar > 800) {
            widthBigAvatar = 800;
        }
        heightBigAvatar = Math.round((float) widthBigAvatar / ratio);
        Log.i(TAG, "width avatar: " + widthBigAvatar + " height avatar: " + heightBigAvatar);
    }

    boolean colorWhite = true;

    private void setColorItemFakeToolbar(boolean isWhite) {
        if (isWhite) {
            if (!colorWhite) {
                mTvwProfileNameToolbar.setVisibility(View.INVISIBLE);
                mViewBottomToolbar.setVisibility(View.GONE);
                mImgBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));
                colorWhite = true;
            }
        } else {
            colorWhite = false;
            mTvwProfileNameToolbar.setVisibility(View.VISIBLE);
            mViewBottomToolbar.setVisibility(View.VISIBLE);
            mImgBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.bg_ab_icon));
        }
    }

    private void gotoAlbumPreview() {
        mParentActivity.trackingEvent(gaCategoryId, R.string.ga_action_interaction, R.string.ga_label_show_all_album);
        if (mImageProfiles == null) {
            mImageProfiles = new ArrayList<>();
        }
        Intent intent = new Intent(mApplication, AlbumViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, mImageProfiles);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME, officialName);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, officialId);
        */
/*intent.putExtra(AlbumViewActivity.PARAM_LIST_IMAGE, mImageProfiles);
        intent.putExtra(AlbumViewActivity.PARAM_HIRE_REMOVE_BUTTON, true);
        intent.putExtra(AlbumViewActivity.PARAM_THREAD_NAME, officialName);
        intent.putExtra(AlbumViewActivity.PARAM_CONTACT_TYPE, Constants.CONTACT_TYPE.OFFICIAL);*//*

        mParentActivity.startActivity(intent);
    }

    //===========onmedia
    private void drawPhoneNumberDetail() {
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mAvatarBusiness.setAvatarOnMediaWithListener(mImgAvatar, mTvwAvatar,
                avatarUrl, "", officialName, size, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (loadedImage == null) {
                            ProfileHelper.blurHeaderProfile(mApplication, "", mImgProfileAvatar);
                        } else {
                            ImageHelper.blurAvatar(mApplication, loadedImage, null, mImgProfileAvatar);
//                            ProfileHelper.blurHeaderProfileWithBitmap(mApplication, loadedImage, mImgProfileAvatar);
                        }
                        super.onLoadingComplete(imageUri, view, loadedImage);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        ProfileHelper.blurHeaderProfile(mApplication, "", mImgProfileAvatar);
                        super.onLoadingFailed(imageUri, view, failReason);
                    }
                });
        //neu avatar url empty thi set blur mac dinh
        if (TextUtils.isEmpty(avatarUrl)) {
            ProfileHelper.blurHeaderProfile(mApplication, "", mImgProfileAvatar);
        }
        mTvwProfileName.setText(officialName);
        mTvwProfileNameToolbar.setText(officialName);
        */
/*if (userType == UserInfo.USER_ONMEDIA_VERIFY) {
            mTvwName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checkbox_selected, 0);
        } else {
            mTvwName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }*//*

        if (null == mImageCover) {
//                getInfoContactFromNumber(mPhoneNumberNonContact);
        } else {
            setImageCover();
        }
        getInfoContactFromNumber();
        */
/*if (mTvwStatus.getVisibility() == View.VISIBLE)
            setBackgroundStatus();*//*

    }


    */
/*private void setBackgroundStatus() {
        if (mParentActivity != null && mTvwStatus != null)
            mTvwStatus.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = mTvwStatus.getLineCount();
                    Log.i(TAG, "lineCount=" + lineCount);
                    if (lineCount <= 1) {
                        mTvwStatus.setBackgroundResource(R.drawable.nic_quote);
                    } else if (lineCount > 1) {
                        mTvwStatus.setBackgroundResource(R.drawable.ic_quote);
                    }
                }
            });
    }*//*


    //======onmedia
    private void loadData() {
        if (mParentActivity != null) {
            if (!NetworkHelper.isConnectInternet(mParentActivity)) {
                mParentActivity.showToast(R.string.no_connectivity_check_again);
                mLoadmoreFooterView.setVisibility(View.GONE);
                return;
            }
            isLoading = true;
            page++;
            final long timeRequest = System.currentTimeMillis();
            Log.i(TAG, "getactivity: " + timeRequest);
            rest.getActivitiesOfficial(officialId, page, new OnMediaInterfaceListener.GetListFeedListener() {
                @Override
                public void onGetListFeedDone(RestAllFeedsModel restAllFeedsModel) {
                    Log.i(TAG, "onResponse: getActivitiesOfficial code " + restAllFeedsModel.getCode() +
                            " take: " + (System.currentTimeMillis() - timeRequest));
                    isLoading = false;
                    mLoadmoreFooterView.setVisibility(View.GONE);
                    if (restAllFeedsModel.getCode() == HTTPCode.E200_OK) {
                        mFeedBusiness.setDeltaTimeServer(
                                System.currentTimeMillis() - restAllFeedsModel.getCurrentTimeServer());
                        onLoadDataDone(restAllFeedsModel.getData());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    isLoading = false;
                    mLoadmoreFooterView.setVisibility(View.GONE);
                    Log.i(TAG, "error");
                }
            });
        }
    }

    private void onLoadDataDone(ArrayList<FeedModelOnMedia> result) {
        if (result != null && !result.isEmpty()) {
            noMoreFeed = false;
            result = mFeedBusiness.preProcessListFeedModelListTag(result);
            if (page == 1) {
                listFeed = result;
                mTvwAction.setVisibility(View.VISIBLE);
            } else {
                listFeed.addAll(result);
            }
            Log.i(TAG, "load data done: " + listFeed.size());
            mFeedAdapter.setListFeed(listFeed);
            mFeedAdapter.notifyDataSetChanged();
        } else {
            Log.i(TAG, "nomore feed");
            noMoreFeed = true;
        }
    }

    private String getLastRowId() {
        if (listFeed == null || listFeed.isEmpty()) {
            return "";
        } else {
            return listFeed.get(listFeed.size() - 1).getBase64RowId();
        }
    }

    private void onLoadMore() {
        if (isLoading) {
            Log.i(TAG, "loaddata onLoadMore isLoading");
        } else {
            if (!noMoreFeed) {
                mLoadmoreFooterView.setVisibility(View.VISIBLE);
                loadData();
            } else {
                Log.i(TAG, "loaddata onLoadMore nomorefeed");
            }
        }
    }

    private void setViewListeners() {
        setAvatarImageClickListener();
        setListviewListener();
        setAbBackListener();
        */
/*mBtnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClickBtnFollow();
            }
        });*//*

    }

    */
/*private void handleClickBtnFollow() {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            mLoadmoreFooterView.setVisibility(View.GONE);
            return;
        }
        mParentActivity.showLoadingDialog("", "");
        rest.followOfficial(officialId, mOfficial.isUserFollow() ? 0 : 1, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "resones: " + s);
                mParentActivity.hideLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                        int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                        if (code == HTTPCode.E200_OK) {
                            long delta = mOfficial.isUserFollow() ? -1 : 1;
                            long follower = mOfficial.getFollower();
                            Log.i(TAG, "delta: " + delta + " follower: " + follower);
                            mOfficial.setFollower(follower + delta);
                            mOfficial.setUserStateFollow(!mOfficial.isUserFollow());
                            Log.i(TAG, "official: " + mOfficial);
                            setStateButtonFollow();
                            setStateFollower();
                        } else {
                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
                } catch (Exception ex) {
                    Log.e(TAG, "Exception", ex);
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "volleyError: " + volleyError.toString());
                mParentActivity.showToast(R.string.e601_error_but_undefined);
                mParentActivity.hideLoadingDialog();
            }
        });
    }*//*


    */
/*private void setStateFollower() {
        String textFollow;
        if (mOfficial.getFollower() <= 0) {
            textFollow = mRes.getString(R.string.onmedia_nobody_follow);
        } else if (mOfficial.getFollower() == 1) {
            textFollow = String.format(mRes.getString(R.string.onmedia_follower), String.valueOf(1));
        } else {
            textFollow = String.format(mRes.getString(R.string.onmedia_followers),
                    Utilities.shortenLongNumber(mOfficial.getFollower()));
        }
        mTvwBirthday.setText(textFollow);
        mLayoutStatus.setVisibility(View.VISIBLE);
    }

    private void setStateButtonFollow() {
        //ko cho follow/unfollow nua
        mBtnFollow.setVisibility(View.GONE);
        mLayoutSendMessage.setVisibility(View.VISIBLE);
        if (mOfficial.isUserFollow()) {
            mBtnFollow.setText(mRes.getString(R.string.onmedia_unfollow));
        } else {
            mBtnFollow.setText(mRes.getString(R.string.onmedia_follow));
        }
    }*//*


    private void setImageClickListener() {
        for (int i = 0; i < 5; i++) {
            final int position = i;
            mImageViewList[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mImageProfileBusiness == null
                            || mImageProfiles == null
                            || mImageProfiles.size() == 0) {
                        Log.d(TAG, "NULL CMNR");
                        return;
                    }
                    int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfiles, position);
                    Log.i(TAG, "pos: " + pos);
                    eventOnMediaHelper.showImageDetail(officialName, officialId, mImageProfiles, pos,
                            FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
                }
            });
        }
    }

    private void setAbBackListener() {
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private int getAlphaforActionBar(int scrollY) {
        int minDist = 0, maxDist = maxHeight;
        if (scrollY > maxDist) {
            return 255;
        } else if (scrollY < minDist) {
            return Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR;
        } else {
            maxHeight = maxHeightDefaut;
            int alpha = (int) ((255.0 / maxDist) * scrollY);
            return (alpha < Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR) ? Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR :
                    alpha;
        }
    }

    private void setAvatarImageClickListener() {
        mImgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAvatarBusiness.displayFullAvatarOfficial(officialName, officialId, avatarUrl);
            }
        });
    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void setListviewListener() {

        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                checkAndSetAlphaActionBar();
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !listFeed.isEmpty
                            () && !noMoreFeed) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }

            }
        };
        mRecyclerViewFeed.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));

    }

    private void checkAndSetAlphaActionBar() {
        View c = mRecyclerViewFeed.getChildAt(0);
        if (c != null) {
            if (mHeaderView == c) {
//                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                if (layoutManager == null) return;
                int scrolly = -c.getTop() + layoutManager.findFirstVisibleItemPosition() * c.getHeight();
                int alpha = getAlphaforActionBar(scrolly);
                mDrawableAb.setAlpha(alpha);
                mDrawableStatus.setAlpha(alpha);
                if (alpha < Constants.SETTINGS.ALPHA_SHOW_ACTIONBAR) {
                    setColorItemFakeToolbar(true);
                } else {
                    setColorItemFakeToolbar(false);
                }
            }
        }
    }

    private void setImageCover() {
        */
/*if (null == mImageCover) {
            mImageCover = new ImageProfile();
            mImageCover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
        }
        mImageProfileBusiness.displayImageProfile(mImgCover, mImageCover, false);
        mImgCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                if (mImageCover == null || !mImageCover.hasCover()) {
                if (mImageCover == null) {
                    Log.d(TAG, "Image Cover NULL CMNR");
                    return;
                }
                ArrayList<ImageProfile> imageCover = new ArrayList<>();
                imageCover.add(mImageCover);
                eventOnMediaHelper.showImageDetail(officialName, officialId, imageCover, 0,
                        FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
                *//*
*/
/*mParentActivity.showImageDetail(officialName, officialId, true, imageCover,
                        Constants.CONTACT_TYPE.OFFICIAL, 0);*//*
*/
/*
            }
        });*//*

    }

    private void setListImageSmall() {
        //TODO disable chuc nang album
        if (mImageProfiles == null || mImageProfiles.isEmpty()) {
            mImageProfiles = new ArrayList<>();
            mLayoutMyAlbum.setVisibility(View.GONE);
        } else {
            mLayoutMyAlbum.setVisibility(View.VISIBLE);
            setImageClickListener();
            ProfileHelper.drawAlbum(mApplication, mImageProfiles, mImageViewList, mView3Image,
                    mView2Image, mBtnShowAll, false, true, null);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    */
/*public void onClickImageProfile(int position) {
        ArrayList<ImageProfile> imageList = new ArrayList<ImageProfile>();
        if (position >= 0) {
            imageList = mImageProfiles;
        } else {
            imageList.add(mImageCover);
            position = 0;
        }
        Intent intent = new Intent(mApplication, PreviewImageProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(PreviewImageProfileActivity.PARAM_LIST_IMAGE, imageList);
        intent.putExtra(PreviewImageProfileActivity.PARAM_CURRENT_IMAGE, position);
        intent.putExtra(PreviewImageProfileActivity.PARAM_AVATAR_COVER, true);
        intent.putExtra(PreviewImageProfileActivity.PARAM_CONTACT_TYPE, Constants.CONTACT_TYPE.OFFICIAL);
        intent.putExtra(PreviewImageProfileActivity.PARAM_THREAD_NAME, officialName);
        intent.putExtra(PreviewImageProfileActivity.PARAM_PHONE_NUMBER, officialId);
        mParentActivity.startActivity(intent);
    }*//*


    //============onmedia
    @Override
    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        final boolean isLiked = feed.getIsLike() == 1;
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        mFeedBusiness.addUrlActionLike(feed.getFeedContent().getUrl(), feed.getBase64RowId(), action, feed.getFeedContent());
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        if (feed.getFeedContent() == null) return;
        String urlKey = feed.getFeedContent().getUrl();
        if (TextUtils.isEmpty(urlKey)) return;
        for (int i = 0; i < listFeed.size(); i++) {
            if (listFeed.get(i).getFeedContent() != null && urlKey.equals(listFeed.get(i).getFeedContent().getUrl())) {
                int delta;
                if (isLike) {
                    delta = 1;
                    listFeed.get(i).setIsLike(1);
                } else {
                    delta = -1;
                    listFeed.get(i).setIsLike(0);
                }
                long countLike = listFeed.get(i).getFeedContent().getCountLike();
                listFeed.get(i).getFeedContent().setCountLike(countLike + delta);
            }
        }
        mFeedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickCommentFeed(FeedModelOnMedia feed) {
        displayCommentStatusFragment(feed);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_comment);
    }

    public void displayCommentStatusFragment(FeedModelOnMedia mItem) {
        if (feedInterface != null) {
            feedInterface.navigateToComment(mItem);
        }
    }

    private void addAllStatusOnProfile(String urlKey, int countCmt) {
        for (FeedModelOnMedia feed : listFeed) {
            if (feed.getFeedContent().getUrl().equals(urlKey)) {
                long countComment = feed.getFeedContent().getCountComment();
                feed.getFeedContent().setCountComment(countComment + countCmt);
            }
        }
    }

    @Override
    public void onClickShareFeed(FeedModelOnMedia feed) {
        eventOnMediaHelper.showPopupContextMenuShare(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share);
    }

    @Override
    public void onClickUser(UserInfo userInfo) {
        if (userInfo != null) {
            if (!userInfo.getMsisdn().equals(officialId)) {
                eventOnMediaHelper.processUserClick(userInfo);
            }
        }
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_avatar);
    }

    @Override
    public void onClickMediaItem(FeedModelOnMedia feed) {
        eventOnMediaHelper.
                handleClickMediaItem(feed, gaCategoryId, gaActionId, feedInterface, iconListener);
    }

    @Override
    public void onClickImageItem(FeedModelOnMedia feed, int positionImage) {
        eventOnMediaHelper.handleClickImage(feed, positionImage, Constants.ONMEDIA.PARAM_IMAGE
                .FEED_FROM_FRIEND_PROFILE);
        //TODO GA
    }

    @Override
    public void onClickMoreOption(FeedModelOnMedia feed) {
//        showPopupContextMenu(feed);
        eventOnMediaHelper.handleClickMoreOption(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_option_feed);
    }

    @Override
    public void onClickButtonTotal(View rowView, FeedModelOnMedia feed) {
        eventOnMediaHelper.handleShareTotal(rowView, feed, gaCategoryId, gaActionId);
    }

    @Override
    public void onLongClickStatus(FeedModelOnMedia feed) {
        if (feed != null) {
            eventOnMediaHelper.showPopupContextMenuLongClick(feed, this);
        }
    }

    @Override
    public void onDeepLinkClick(FeedModelOnMedia feed, String link) {
        eventOnMediaHelper.handleDeeplink(feed, link);
    }

    @Override
    public void onClickSuggestFriend(UserInfo userInfo) {

    }

    @Override
    public void openChannelInfo(FeedModelOnMedia feed) {
        Channel channelVideo = Channel.convertFromChannelOnMedia(feed.getFeedContent().getChannel());
        if (channelVideo == null) return;
        mApplication.getApplicationComponent().providesUtils().openChannelInfo(mParentActivity, channelVideo);
    }

    @Override
    public void openPlayStore(FeedModelOnMedia feed, String packageName) {
        NavigateActivityHelper.navigateToPlayStore(mParentActivity, packageName);
    }

    @Override
    public void onSubscribeChannel(FeedModelOnMedia feed, Channel channel) {

    }

    */
/*private void showPopupContextMenuLongClick(FeedModelOnMedia feed) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(mParentActivity, mRes.getString(R.string.onmedia_copy_text), -1,
                feed, Constants.MENU.MENU_COPY_TEXT);
        listMenu.add(copyItem);

        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(),
                    null, listMenu, this);
        }
    }*//*


    */
/*private void showPopupContextMenuShare(FeedModelOnMedia feed) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu shareNow = new ItemContextMenu(mApplication, mRes.getString(R.string.onmedia_share_now), -1,
                feed, Constants.MENU.MENU_SHARE_LINK);
        ItemContextMenu writeStatus = new ItemContextMenu(mApplication, mRes.getString(R.string.onmedia_write_status)
                , -1,
                feed, Constants.MENU.MENU_WRITE_STATUS);
        ItemContextMenu listShare = new ItemContextMenu(mApplication, mRes.getString(R.string
        .onmedia_title_user_share), -1,
                feed, Constants.MENU.MENU_LIST_SHARE);
        listMenu.add(shareNow);
        listMenu.add(writeStatus);
        listMenu.add(listShare);
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(),
                    null, listMenu, this);
        }
    }*//*


    public void handleShareLink(final FeedModelOnMedia feed) {
        if (feed.getIsShare() == 1) {
            mParentActivity.showToast(R.string.onmedia_already_shared);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        setShareFeed(feed, 1);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE, "",
                feed.getBase64RowId(), "", null,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionShare: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    mParentActivity.showToast(R.string.onmedia_share_success);
                                } else {
                                    setShareFeed(feed, -1);
                                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                                }
                            } else {
                                setShareFeed(feed, -1);
                                mParentActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            setShareFeed(feed, -1);
                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        setShareFeed(feed, -1);
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void setShareFeed(FeedModelOnMedia feed, int delta) {
        String urlKey = feed.getFeedContent().getUrl();
        for (int i = 0; i < listFeed.size(); i++) {
            if (listFeed.get(i).getFeedContent().getUrl().equals(urlKey)) {
                long countShare = listFeed.get(i).getFeedContent().getCountShare();
//        long countComment = feed.getFeedContent().getCountComment();
                listFeed.get(i).getFeedContent().setCountShare(countShare + delta);
//        feed.getFeedContent().setCountComment(countComment + delta);
                if (delta == 1) {
                    listFeed.get(i).setIsShare(1);
                } else {
                    listFeed.get(i).setIsShare(0);
                }
            }
        }
        mFeedAdapter.notifyDataSetChanged();
    }

    */
/*private void handleMediaItemClick(FeedModelOnMedia feed) {
        FeedContent feedContent = feed.getFeedContent();
        if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_VIDEO)) {
            if (!TextUtils.isEmpty(feedContent.getMediaUrl())) {
                if (mMusicBusiness.isExistListenMusic()) {
                    checkMusicTogether(feed);
                } else if (mApplication.getPlayMusicController().isPlaying()) {
                    mApplication.getPlayMusicController().closeMusic();
                    checkPlayer();
                    if (!TextUtils.isEmpty(feedContent.getMediaUrl())) {
                        if (feedInterface != null) {
                            feedInterface.navigateToVideoView(feed);
                        }
                    }
                } else {
                    if (!TextUtils.isEmpty(feedContent.getMediaUrl())) {
                        if (feedInterface != null) {
                            feedInterface.navigateToVideoView(feed);
                        }
                    }
                }
            } else {
                if (feedInterface != null) {
                    feedInterface.navigateToWebView(feed);
                }
            }
        } else {
            if (mMusicBusiness.isExistListenMusic()) {
                checkMusicTogether(feed);
            } else {
                if (mApplication.getPlayMusicController().isPlayFromFeed() && mFeedBusiness.getFeedPlayList() != null) {
                    FeedContent feedContentPlaying = mFeedBusiness.getFeedPlayList().getFeedContent();
                    if (feedContent.getUrl().equals(feedContentPlaying.getUrl())) {
                        if (feedInterface != null) {
                            feedInterface.navigateToAlbum(feed);
                        }
                    } else {
                        playSongAlbum(feed);
                    }
                } else {
                    playSongAlbum(feed);
                }
            }
        }
    }*//*


    */
/*private void playSongAlbum(FeedModelOnMedia feed) {
        mApplication.getPlayMusicController().pauseMusic();
        mMusicBusiness.resetSessionMusic();
        MediaModel mediaModel = feed.getFeedContent().getMediaMode();
        mApplication.getPlayMusicController().getPlayingListDetail(mediaModel);
        // log feed
        mApplication.getKeengProfileBusiness().logListenTogether(mediaModel);
        Log.i(TAG, "mediamode: mediaurl: " + mediaModel.getMedia_url() + " itemid: " + mediaModel.getId() + " url: "
        + mediaModel.getUrl());
        mApplication.getPlayMusicController().setIsPlayFromFeed(true);
        if (feedInterface != null) {
            feedInterface.navigateToAlbum(feed);
        }
    }*//*


    */
/*private void checkPlayer() {
        if (mApplication.getPlayMusicController() != null &&
                mApplication.getPlayMusicController().isPlaying() &&
                mApplication.getPlayMusicController().isPlayFromFeed()) {
            Log.i(TAG, "mMediaService.isPlaying()");
        } else {
            Log.i(TAG, "mMediaService not playing");
        }
    }*//*


    */
/*private void handleClickPopupExitListenTogether(FeedModelOnMedia item) {
        FeedContent feedContent = item.getFeedContent();
        if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_VIDEO)) {
            mMusicBusiness.closeMusic();
            mApplication.getPlayMusicController().pauseMusic();
//                    checkPlayer();
            if (!TextUtils.isEmpty(feedContent.getMediaUrl())) {
                if (feedInterface != null) {
                    feedInterface.navigateToVideoView(item);
                }
            }
        } else {
            mMusicBusiness.closeMusic();
            mApplication.getPlayMusicController().pauseMusic();
            mApplication.getPlayMusicController().getPlayingListDetail(feedContent.getMediaMode());
            mApplication.getPlayMusicController().setIsPlayFromFeed(true);
            if (feedInterface != null) {
                feedInterface.navigateToAlbum(item);
            }
            // log feed
            mApplication.getKeengProfileBusiness().logListenTogether(feedContent.getMediaMode());
        }
    }*//*


    */
/*private void checkMusicTogether(FeedModelOnMedia feed) {
//        FeedContent mediaItem = feed.getFeedContent();
        String currentNumberInRoom = mMusicBusiness.getCurrentNumberFriend();
        MessageBusiness mMessageBusiness = mApplication.getMessageBusiness();
        if (!TextUtils.isEmpty(currentNumberInRoom)) {
            String currentName = mMessageBusiness.getFriendName(currentNumberInRoom);
            PopupHelper.getInstance(mParentActivity).showDialogConfirm(mParentActivity,
                    "", String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                            currentName),
                    mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                    this, feed, Constants.MENU.POPUP_EXIT);
        } else if (mMusicBusiness.isWaitingStrangerMusic()) {// cung nghe voi nguoi la
            PopupHelper.getInstance(mParentActivity).showDialogConfirm(mParentActivity,
                    "", String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                            mRes.getString(R.string.stranger)),
                    mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                    this, feed, Constants.MENU.POPUP_EXIT);
        } else {
            handleClickPopupExitListenTogether(feed);
        }
    }*//*


    */
/*private void showPopupContextMenu(FeedModelOnMedia feed) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<ItemContextMenu>();
        *//*
*/
/*ItemContextMenu reportItem = new ItemContextMenu(mApplication, mRes.getString(R.string
        * .onmedia_setting_report), -1,
                feed, Constants.MENU.REPORT);
        listMenu.add(reportItem);*//*
*/
/*
        ItemContextMenu copyItem = new ItemContextMenu(mApplication, mRes.getString(R.string.web_pop_copylink), -1,
                feed, Constants.MENU.COPY);
        listMenu.add(copyItem);
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(),
                    null, listMenu, this);
        }
    }*//*


    private void confirmUnfollow(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.onmedia_setting_unfollow_text1);
        String msg = mRes.getString(R.string.onmedia_message_unfollow);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_UNFOLLOW);
    }

    private void confirmReport(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.onmedia_setting_report);
        String msg = mRes.getString(R.string.onmedia_message_report);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_REPORT);
    }

    private void handleUnfollow(FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.unFollowFriend(feed.getUserInfo().getMsisdn(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                        int code = jsonObject.optInt(Constants.HTTP.REST_CODE, 0);
                        if (code == HTTPCode.E200_OK) {
                            mParentActivity.showToast(R.string.onmedia_unfollow_success);
                        } else {
                            throw new Exception();
                        }
                    } else {
                        throw new Exception();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    private void handleReport(FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.reportViolation(feed, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                        int code = jsonObject.optInt(Constants.HTTP.REST_CODE, 0);
                        if (code == HTTPCode.E200_OK) {
                            mParentActivity.showToast(R.string.onmedia_action_success);
                        } else {
                            throw new Exception();
                        }
                    } else {
                        throw new Exception();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    @Override
    public void notifyFeedOnMedia(boolean needNotify, boolean needToSetSelection) {
        Log.i(TAG, "notifyFeedOnMedia: " + needNotify + " selection: " + needToSetSelection);
        notifyNewFeed(needToSetSelection);
    }

    @Override
    public void onPostFeed(FeedModelOnMedia feed) {
        mFeedBusiness.getListFeed().add(0, feed);
        mFeedBusiness.getListPost().add(feed);
        mFeedBusiness.getListFeedProfile().add(0, feed);
        mFeedAdapter.notifyDataSetChanged();
    }

    public void notifyNewFeed(final boolean needToSetSelection) {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "notify nowwwwwwwwwwwwww");
                FeedModelOnMedia feedProcess = mFeedBusiness.getFeedProfileProcess();
                if (feedProcess == null || feedProcess.getFeedContent() == null ||
                        TextUtils.isEmpty(feedProcess.getFeedContent().getUrl())) {
                    return;
                }
                for (int i = 0; i < listFeed.size(); i++) {
                    FeedModelOnMedia feedTmp = listFeed.get(i);
                    if (feedTmp == null || feedTmp.getFeedContent() == null ||
                            TextUtils.isEmpty(feedTmp.getFeedContent().getUrl())) {
                        break;
                    }
                    if (feedTmp.getFeedContent().getUrl().equals(feedProcess.getFeedContent().getUrl())) {
                        feedTmp.getFeedContent().setCountShare(feedProcess.getFeedContent().getCountShare());
                        feedTmp.getFeedContent().setCountComment((feedProcess.getFeedContent().getCountComment()));
                        feedTmp.getFeedContent().setCountLike(feedProcess.getFeedContent().getCountLike());
                        feedTmp.setIsLike(feedProcess.getIsLike());
                    }
                }
                mFeedAdapter.notifyDataSetChanged();
                if (needToSetSelection) {
                    mRecyclerViewFeed.scrollToPosition(1);
                }
            }
        });

    }

    @Override
    public void OnClickUser(String msisdn, String name) {
        if (enableClickTag) {
            //ko hieu sao cai clickableSpan bi nhay vao 2 lan lien @@
            enableClickTag = false;
            Log.i(TAG, "msisdn: " + msisdn);
            if (!msisdn.equals(officialId)) {
                UserInfo userInfo = new UserInfo();
                userInfo.setMsisdn(msisdn);
                userInfo.setName(name);
                userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                userInfo.setStateMocha(1);
                eventOnMediaHelper.processUserClick(userInfo);
            }
        } else {
            enableClickTag = true;
        }
    }

    public void getInfoContactFromNumber() {
        final long timeRequestgetInfo = System.currentTimeMillis();
        Log.d(TAG, "getInfoContactFromNumber: " + timeRequestgetInfo);
        rest.getProfileOfficial(officialId, officialName, avatarUrl,
                new ContactRequestHelper.onResponseOfficialOnMediaInfoListener() {
                    @Override
                    public void onResponse(OfficalAccountOnMedia official) {
                        if (official != null) {
                            mOfficial = official;
                            Log.i(TAG, "official: " + mOfficial + " take: " + (System.currentTimeMillis() -
                                    timeRequestgetInfo));
                            mImageCover = mOfficial.getImageCover();
                            if (null == mImageProfiles) {
                                mImageProfiles = new ArrayList<>();
                            } else {
                                mImageProfiles.clear();
                            }
                            mImageProfiles.addAll(mOfficial.getAlbums());
                            setImageCover();
                            setListImageSmall();
                            String status = mOfficial.getStatus();
                            if (TextUtils.isEmpty(status)) {
                                mTvwProfileStatus.setVisibility(View.GONE);
                            } else {
                                mTvwProfileStatus.setVisibility(View.VISIBLE);
                                mTvwProfileStatus.setEmoticonStatus(mParentActivity, status, status.hashCode(), status);
                            }
                            */
/*setStateFollower();
                            setStateButtonFollow();
                            setButtonSendMsg();*//*

                        } else {
                            setImageCover();
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        setImageCover();
                        Log.e(TAG, "Error getInfoContactFromNumber: " + errorCode);
                    }
                });
    }

    */
/*private void setButtonSendMsg() {
        if (mOfficial.getOfChatInfo() != null) {
            mBtnSendMessage.setVisibility(View.VISIBLE);
            mBtnSendMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "click: " + mOfficial.getOfChatInfo());
                    ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateOfficerThread(
                            mOfficial.getOfChatInfo().getOfChatId(),
                            mOfficial.getOfChatInfo().getOfChatName(),
                            mOfficial.getOfChatInfo().getOfChatAvatar(),
                            OfficerAccountConstant.ONMEDIA_TYPE_NONE);//TODO comment
                    mParentActivity.navigateToThreadDetail(threadMessage);
                }
            });
        }
    }*//*

}*/

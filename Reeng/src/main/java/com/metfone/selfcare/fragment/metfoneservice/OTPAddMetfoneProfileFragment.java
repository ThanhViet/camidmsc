package com.metfone.selfcare.fragment.metfoneservice;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.icu.util.CurrencyAmount;
import android.os.Bundle;
import android.os.Handler;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.esport.service.CamIDAPIService;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.EnterAddPhoneNumberActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.activity.MetFoneServiceActivity;
import com.metfone.selfcare.activity.OTPChangePhoneActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.CheckPhoneInfo;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.Formatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static androidx.constraintlayout.motion.utils.Oscillator.TAG;
import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;

public class OTPAddMetfoneProfileFragment extends Fragment implements View.OnClickListener, ClickListener.IconListener, MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String PHONE_NUMBER_PARAM = "phoneNumber";
    private static final String SERVICE_ID_PARAM = "serviceId";
    private static final String CODE_CHECK_PHONE_PARAM = "codeCheckPhone";
    private static final int TIME_DEFAULT = 60000;
    private static boolean handlerflag = false;
    private  static final int REQ_USER_CONSENT = 6;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.etOtp_verify)
    PinEntryEditText etOtp;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    @Nullable
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.rll_otp_add_metfone)
    RelativeLayout mRllOtpAddMetfone;
    UserInfo currentUser;
    UserInfoBusiness userInfoBusiness;
    int remain = 0;
    private MetFoneServiceActivity metFoneServiceActivity;
    private String password = "";
    private Handler mHandler;
    private long countDownTimer;
    private ClickListener.IconListener mClickHandler;
    private String phoneNumber = "";
    private int checkPhoneCode;
    private ApplicationController mApplication;
    private Resources mRes;
    private boolean isSaveInstanceState = false;
    private String mCodeCheckPhone;
    private int serviceId = 1;
    private BaseMPConfirmDialog baseMPConfirmDialog;
    private MySMSBroadcastReceiver mySMSBroadcastReceiver;
    BaseMPSuccessDialog baseMPSuccessDialog;

    public static OTPAddMetfoneProfileFragment newInstance() {
        Bundle args = new Bundle();
        OTPAddMetfoneProfileFragment fragment = new OTPAddMetfoneProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        metFoneServiceActivity = (MetFoneServiceActivity) context;
        serviceId = metFoneServiceActivity.getServiceId();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        handlerflag = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frgament_otp_profile, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getData();
        setViewListeners();
        mApplication = (ApplicationController) requireActivity().getApplicationContext();
        mRes = getResources();
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        metFoneServiceActivity.registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
        mySMSBroadcastReceiver.initOTPListener(this);
        tvEnterNumberDes.setText(fomatString());
        mApplication.registerSmsOTPObserver();
        mCodeCheckPhone = getArguments().getString(CODE_CHECK_PHONE_PARAM);
        userInfoBusiness = new UserInfoBusiness(requireContext());
        currentUser = userInfoBusiness.getUser();
        setupUI(mRllOtpAddMetfone);
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.WHITE);
        metFoneServiceActivity.setColorStatusBar(R.color.color_rlContainer);
        if (getActivity() != null){
            setSystemBarTheme((getActivity()),true);
        }
    }

    private void getData() {
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(EnumUtils.OBJECT_KEY);
            checkPhoneCode = getArguments().getInt(EnumUtils.OTP_KEY);
        }
    }

    @Optional
    @OnClick({R.id.ivBack, R.id.tvResendOtp})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvResendOtp:
                showDialogConfirmResendCode();
                break;
            case R.id.ivBack:
                metFoneServiceActivity.onBackPressed();
                break;
        }
    }

    private void startCountDown(int time) {
        tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    setResendPassListener();
                } else if(remain > 0){
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                }else{remain=0;}
                String countDownString = getString(R.string.mc_resend_otp);
                tvResendOtp.setText(countDownString);
                tvTimeResendOtp.setText(remain +" "+ getString(R.string.text_seconds));
            }
        }, 1000);
    }

    private void setResendPassListener() {
        tvResendOtp.setText(getString(R.string.mc_resend_otp));
        tvResendOtp.setTextColor(ContextCompat.getColor(requireContext(), R.color.resend));
        tvResendOtp.setOnClickListener(view -> {
            showDialogConfirmResendCode();
        });
    }
    private String fomatString(){
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        String mPhoneNumber = phoneNumber.substring(0,3)+" "+ phoneNumber.substring(3);
        fmt.format(getString(R.string.text_otp_has_been_sent_to), mPhoneNumber);
        return sbuf.toString();
    }

    private void showDialogConfirmResendCode() {
        String labelOK = getString(R.string.ok);
        String labelCancel = getString(R.string.cancel);
        String title = getString(R.string.title_confirm_resend_code);
        String msg = String.format(getString(R.string.msg_confirm_resend_code), phoneNumber.substring(0,3)+" "+phoneNumber.substring(3));
        PopupHelper.getInstance().showDialogConfirm((BaseSlidingFragmentActivity) requireActivity(), title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    private void setPasswordEditText(String pass) {
        etOtp.setText(pass);
        Selection.setSelection(etOtp.getText(), etOtp.getText().length());
    }
    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void generateOtpByServer() {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.generateOtp(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                startSmsUserConsent();
                Log.d("TAG", "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(),etOtp);
                    }
                }, 200);
            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Log.d("TAG", "onError: " + error.getMessage());
            }
        });

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mClickHandler = this;
       etOtp.requestFocus();
       UIUtil.showKeyboard(getContext(),etOtp);
        if (!handlerflag && remain > 0) {
            startCountDown(remain * 1000);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        handlerflag = false;
        InputMethodUtils.hideSoftKeyboard(etOtp, getContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        metFoneServiceActivity.unregisterReceiver(mySMSBroadcastReceiver);
    }

    //XU ly phan sms otp
    @Override
    public void onOTPReceived(Intent intent) {
        startActivityForResult(intent, REQ_USER_CONSENT);

    }

    @Override
    public void onOTPTimeOut() {
    }


    private void addService(String otp) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.addServiceProfile(phoneNumber, otp, serviceId, new ApiCallback<GetUserResponse>() {
            @Override
            public void onResponse(Response<GetUserResponse> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(requireContext());
                        userInfoBusiness.setUser(response.body().getData().getUser());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (checkPhoneCode == 42){
                            baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.img_dialog_1, R.string.success_combining,
                                    String.format(getString(R.string.m_p_dialog_combine_phone_content_success), phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3)));
                        }else {
                            baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.img_dialog_1, R.string.successfully,
                                    String.format(getString(R.string.m_p_dialog_success_add_service), phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3)));
                        }

                        baseMPSuccessDialog.show();
                        getChildFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(dialogInterface -> {
                                if (getActivity() != null) {
                                    NavigateActivityHelper.navigateToSetUpProfile(getActivity());
                                    getActivity().finish();
                                }
                            });
                        }
                        //error otp invalid
                    }else if("1".equals(response.body().getCode())){
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
                        baseMPSuccessDialog.show();
                    } else if (response.body().getCode().equals("61")){
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry_full_service), getString(R.string.content_error_full_service_dialog), false);
                        baseMPSuccessDialog.show();
                        getChildFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    metFoneServiceActivity.onBackPressed();
                                }
                            });
                        }
                    }
                    //case camid socia
                    else if (response.body().getCode().equals("49")){
                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), String.format(getString(R.string.content_error_social_combine), phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3)), false);
                    if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
                        baseMPSuccessDialog.setBgWhite(true);
                    }
                    baseMPSuccessDialog.show();
                    getChildFragmentManager().executePendingTransactions();
                    if (baseMPSuccessDialog != null) {
                        baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                metFoneServiceActivity.onBackPressed();
                            }
                        });
                    }

                    //21,24,41,43,45,64: Call api add login và hiển thị popup success
                } else {
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
                        baseMPSuccessDialog.show();
                        getChildFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    if (getActivity() != null) {
                                        metFoneServiceActivity.onBackPressed();
                                    }
                                }
                            });
                        }
                    }

                } else {
                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), getString(R.string.content_add_service_fail), false);
                    baseMPSuccessDialog.show();
                    getChildFragmentManager().executePendingTransactions();
                    if (baseMPSuccessDialog != null) {
                        baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                if (getActivity() != null) {
                                    metFoneServiceActivity.onBackPressed();
                                }
                            }
                        });
                    }
                }

            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                com.metfone.selfcare.util.Log.e("TAG", "Exception", error);
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), error.getMessage(), false);
                baseMPSuccessDialog.show();
                getChildFragmentManager().executePendingTransactions();
                if (baseMPSuccessDialog != null) {
                    baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (getActivity() != null) {
                                metFoneServiceActivity.onBackPressed();
                            }
                        }
                    });
                }
            }
        });

//            startCountDown();

    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        generateOtpByServer();

        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
                addService(str.toString());
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == RESULT_OK && data != null) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (message != null) {
                    getOtpFromMessage(message);
                }

            }

        }
    }
    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                Log.d(TAG,"Success");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,"Fail");

            }
        });
    }
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            etOtp.setText(matcher.group(0));
        }
    }

}
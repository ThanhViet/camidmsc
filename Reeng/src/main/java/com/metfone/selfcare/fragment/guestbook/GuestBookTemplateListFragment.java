package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.TemplateAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Template;
import com.metfone.selfcare.database.model.guestbook.TemplateDetail;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.ui.recyclerview.headerfooter.StrangerGridLayoutManager;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookTemplateListFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener,
        GuestBookHelper.GetDataListener,
        View.OnClickListener {
    private static final String TAG = GuestBookMainFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private GuestBookHelper mGuestBookHelper;
    private TextView mAbTitle;
    private OnFragmentInteractionListener mListener;
    private View mViewHeader;
    private TextView mTvwHeader;
    private RecyclerView mRecyclerView;
    private TemplateAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private StrangerGridLayoutManager mGridLayoutManager;
    private boolean needRequest;
    private Book newBook;

    public GuestBookTemplateListFragment() {

    }

    public static GuestBookTemplateListFragment newInstance() {
        GuestBookTemplateListFragment fragment = new GuestBookTemplateListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        needRequest = true;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        drawDetail();
        setViewListener();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        mGuestBookHelper.cancelRequest(GuestBookHelper.TAG_GET_TEMPLATES);
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onRetryClick() {

    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (TextView) abView.findViewById(R.id.ab_title);
        mAbTitle.setText(mRes.getString(R.string.guest_book_title));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbBack.setOnClickListener(this);
    }

    private void getData(Bundle savedInstanceState) {
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewHeader = inflater.inflate(R.layout.header_guest_book_label, container, false);
        mTvwHeader = (TextView) mViewHeader.findViewById(R.id.guest_book_header_label);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }

    @Override
    public void onSuccess() {
        hideEmptyView();
        mTvwHeader.setText(mRes.getString(R.string.guest_book_header_listtemp));
        setAdapter();
    }

    @Override
    public void onError(int error) {
        showEmptyNote(mRes.getString(R.string.request_send_error));
    }

    private void drawDetail() {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_NON);
        if (mGuestBookHelper.getTemplates() == null) {
            showProgressLoading();
        } else {
            hideEmptyView();
            mTvwHeader.setText(mRes.getString(R.string.guest_book_header_listtemp));
            setAdapter();
        }
        if (needRequest || mGuestBookHelper.getTemplates() == null) {
            needRequest = false;
            mGuestBookHelper.requestTemplates(this);
        }
    }

    private void setViewListener() {

    }

    private void setAdapter() {
        if (mAdapter == null || mHeaderAndFooterAdapter == null || mRecyclerView.getAdapter() == null) {
            mGridLayoutManager = new StrangerGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            mGridLayoutManager.setAutoMeasureEnabled(false);
            mRecyclerView.setLayoutManager(mGridLayoutManager);
            GuestBookHelper mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
            mAdapter = new TemplateAdapter(mApplication);
            mAdapter.setListTemplates(mGuestBookHelper.getTemplates());
            mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
            RecyclerViewUtils.setHeaderView(mRecyclerView, mViewHeader);
            setItemListViewListener();
        } else {
            mAdapter.setListTemplates(mGuestBookHelper.getTemplates());
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setItemListViewListener() {
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                handleCreateNewBook((Template) object);
                //mListener.navigateToTemplateDetail((Template) object);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        // long click
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void handleCreateNewBook(Template template) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestTemplateDetail(template.getId(), new GuestBookHelper.GetTemplateDetailListener() {
            @Override
            public void onSuccess(TemplateDetail templateDetail) {
                newBook = mGuestBookHelper.createNewBook(templateDetail);
                mGuestBookHelper.requestSaveNewBook(newBook, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mGuestBookHelper.setNeedLoadMyBooks(true);
                        mListener.navigateToBookDetail(newBook, true);
                    }

                    @Override
                    public void onError(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                    }
                });
            }

            @Override
            public void onError(int error) {
                hideEmptyView();
                mParentActivity.showToast(R.string.request_send_error);
            }
        });
    }

    public interface OnFragmentInteractionListener {
        void navigateToTemplateDetail(Template template);

        void navigateToBookDetail(Book book, boolean isNew);
    }
}

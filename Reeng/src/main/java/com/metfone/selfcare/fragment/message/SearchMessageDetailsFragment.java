package com.metfone.selfcare.fragment.message;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.PreviewImageActivity;
import com.metfone.selfcare.adapter.message.SearchMessageDetailsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.ReplyMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.NoteMessageHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TagHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.helper.message.SpamRoomManager;
import com.metfone.selfcare.helper.video.Video;
import com.metfone.selfcare.helper.voicemail.AudioVoicemailPlayerImpl2;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.ui.CusRelativeLayout;
import com.metfone.selfcare.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by huongnd38 on 17/10/2018.
 */
public class SearchMessageDetailsFragment extends Fragment implements SmartTextClickListener, TagMocha.OnClickTag, MessageInteractionListener, ClickListener.IconListener, View.OnClickListener {
    private static final String TAG = SearchMessageDetailsFragment.class.getSimpleName();
    private CusRelativeLayout rootView;
    private ListView mLvwContent;
    private ThreadMessage mThreadMessage;
    private SearchMessageDetailsAdapter mSearchAdapter;
    private ArrayList<Integer> mAllMessagesArrayList;
    private CopyOnWriteArrayList<ReengMessage> mMessageArrayList = new CopyOnWriteArrayList<>();
    private MessageBusiness mMessageBusiness;
    private ApplicationController mApplication;
    private int mThreadId, mThreadType;
    private int gaCategoryId;
    private String userNumber = "", friendPhoneNumber = "";
    private Handler mHandler;
    private SharedPreferences mPref;
    private ImageView mImgBackground;
    private TextView mTvDescription;
    private String searchTag = "";
    private String languageCode;
    private BaseSlidingFragmentActivity mActivity;
    private SearchMessageResultsFragment.OnFragmentInteractionListener mListener;
    private AudioVoicemailPlayerImpl2 audioVoicemailPlayerImpl2;
    private ContactBusiness mContactBusiness;
    private ReengAccountBusiness mReengAccountBusiness;
    private boolean isShowInfo, isOnClick;
    private float mDownX, mDownY;
    private final float SCROLL_THRESHOLD = 10;
    private ReengMessage mCurrentReengMessage;

    private static final int NEAREST_MESSAGES_NUM_DEFAULT = 15;
    private int cState = -1;

    public static SearchMessageDetailsFragment newInstance(int threadId, int threadType, String searchTag, ReengMessage message, ArrayList<Integer> list) {
        SearchMessageDetailsFragment fragment = new SearchMessageDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(ThreadMessageConstant.THREAD_ID, threadId);
        args.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadType);
        fragment.setArguments(args);
        fragment.searchTag = searchTag;
        fragment.mCurrentReengMessage = message;
        fragment.mAllMessagesArrayList = list;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        //show off check box delete
        mActivity = (BaseSlidingFragmentActivity) getActivity();
        mApplication = (ApplicationController) getContext().getApplicationContext();
        audioVoicemailPlayerImpl2 = new AudioVoicemailPlayerImpl2(mApplication);
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        mReengAccountBusiness = mApplication.getReengAccountBusiness();
        mAvatarBusiness = mApplication.getAvatarBusiness();
        if (getArguments() != null) {
            mThreadId = getArguments().getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = getArguments().getInt(ThreadMessageConstant.THREAD_IS_GROUP);
        }
        gaCategoryId = R.string.ga_category_chat_screen;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() == null) return;
        if (mPref == null) {
            mPref = getActivity().getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        mApplication = (ApplicationController) getContext().getApplicationContext();
        String deviceLanguage = mApplication.getReengAccountBusiness().getDeviceLanguage();
        languageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
        try {
            mListener = (SearchMessageResultsFragment.OnFragmentInteractionListener) getActivity();
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (CusRelativeLayout) inflater.inflate(R.layout.fragment_search_message_details, container, false);
        mLvwContent = rootView.findViewById(R.id.person_chat_detail_content);
        mImgBackground = rootView.findViewById(R.id.layout_background);
        mTvDescription = rootView.findViewById(R.id.tv_message_description);
        View searchMessageDescription = rootView.findViewById(R.id.message_detail_description_layout);

        if (searchMessageDescription != null) {
            searchMessageDescription.setVisibility(View.VISIBLE);
        }
        setUp();
        return rootView;
    }

    public void getFriendPhoneNumber() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mThreadMessage != null) {
            friendPhoneNumber = mThreadMessage.getSoloNumber();
        }
    }

    public void setUp() {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        getFriendPhoneNumber();

        if (mThreadMessage == null) return;
        setViewListeners();
        int index = findNearestMessagesFromOne(mCurrentReengMessage);
        setDescriptionClick();
        mSearchAdapter = new SearchMessageDetailsAdapter(mActivity, mThreadType, this, this, this, index);
        mSearchAdapter.setMessages(mMessageArrayList);
        mSearchAdapter.setThreadMessage(mThreadMessage);
        mLvwContent.setAdapter(mSearchAdapter);
        if (index >= 0) {
            mLvwContent.setSelection(index >= 4 ? (index - 4) : 0);
        }
        setViewHeaderProfile();
        updateBackground();
    }

    private int findCurrentMessageIndex(ReengMessage reengMessage) {
        if (reengMessage == null || mMessageArrayList == null || mMessageArrayList.size() == 0)
            return -1;
        return mMessageArrayList.indexOf(reengMessage);
    }

    private int findNearestMessagesFromOne(ReengMessage message) {
        if (message == null || mAllMessagesArrayList == null || mAllMessagesArrayList.size() == 0)
            return -1;

        int index = mAllMessagesArrayList.indexOf(message.getId());

        if (index == -1) return -1;
        int start = index >= NEAREST_MESSAGES_NUM_DEFAULT ? index - NEAREST_MESSAGES_NUM_DEFAULT : 0;
        int end = index + NEAREST_MESSAGES_NUM_DEFAULT >= mAllMessagesArrayList.size() ? mAllMessagesArrayList.size() - 1 : index + NEAREST_MESSAGES_NUM_DEFAULT;
        int startId = mAllMessagesArrayList.get(start);
        int endId = mAllMessagesArrayList.get(end);
        CopyOnWriteArrayList<ReengMessage> list = mMessageBusiness.getMessagesWithinIds(mThreadId, startId, endId);
        try {
            boolean addedCurrentMessage = false;
            for (ReengMessage reengMessage : list) {
                if (!addedCurrentMessage && reengMessage != null && reengMessage.getId() == message.getId()) {
                    mMessageArrayList.add(message);
                    addedCurrentMessage = true;
                } else {
                    mMessageArrayList.add(reengMessage);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "findNearestMessagesFromOne: Exception !!", e);
        }
        return findCurrentMessageIndex(mCurrentReengMessage);
    }

    private void setDescriptionClick() {
        if (mTvDescription == null) return;
        try {
            int messages_num = mMessageArrayList.size();
            String back = getString(R.string.search_message_back_to_conversation);
            SpannableString desctiptionContent = new SpannableString(getString(R.string.search_message_detail_description, searchTag, messages_num - 1));
            SpannableString backContent = new SpannableString(back);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    try {
                        //back to conversation
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    } catch (Exception ex) {

                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.textColorSecond));
                    ds.setUnderlineText(false);
                }
            };
            backContent.setSpan(clickableSpan, 0, back.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            backContent.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.setting_chat_text_color_red)), 0, back.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTvDescription.setText(TextUtils.concat(desctiptionContent, " ", backContent));
            mTvDescription.setMovementMethod(LinkMovementMethod.getInstance());
            mTvDescription.setHighlightColor(Color.TRANSPARENT);
        } catch (Exception e) {

        }
    }

    private void updateBackground() {
        if (mThreadMessage == null) {
            return;
        }
        String background = mThreadMessage.getBackground();
        if (TextUtils.isEmpty(background) &&
                (mThreadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT)) {
            if (mPref.getBoolean(Constants.PREFERENCE.PREF_APPLY_BACKGROUND_ALL, false)) {// apply all
                background = mPref.getString(Constants.PREFERENCE.PREF_DEFAULT_BACKGROUND_PATH, null);
                mThreadMessage.setBackground(background);
            }
        }
        Log.d(TAG, "displayBackgroundOfThreadDetail: updateBackground");
        updateBackgroundUI(background);
    }

    public void updateBackgroundUI(final String filePath) {
        if (mHandler == null) mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ImageLoaderManager.getInstance(getActivity()).displayBackgroundOfThreadDetail(filePath, mImgBackground);
            }
        }, 100);
    }

    @Override
    public void longClickBgrCallback(ReengMessage message) {
        if (message != null && (message.getMessageType() == ReengMessageConstant.MessageType.text || message.getMessageType() == ReengMessageConstant.MessageType.image)) {
            ArrayList<ItemContextMenu> listItemContextMenu = MessageHelper.getListItemContextMenu(mApplication,
                    message, mThreadMessage, languageCode, true, false);
            String title = mMessageBusiness.getContentOfMessage(message, getResources(), mApplication);
            //file, image, voicemail, shareContact
            PopupHelper.getInstance().showContextMenu(mActivity, title, listItemContextMenu, this);
        }
    }

    @Override
    public void retryClickCallBack(ReengMessage message) {
        if (!NetworkHelper.isConnectInternet(mActivity)) {
            mActivity.showToast(R.string.no_connectivity);
            return;
        }
        ReengMessageConstant.MessageType type = message.getMessageType();
        ReengMessageConstant.Direction direction = message.getDirection();
        if (ReengMessageConstant.MessageType.containsNormal(type)) {
            if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                message.setPacketId(PacketMessageId.getInstance().genMessagePacketId(mThreadType, type.toString()));
                //truong hop retry tin nhan gsm thi phai generate packetId moi
                //- do server check trung packetId voi tin nhan SMS Out
            }
            message.setChatMode(ReengMessageConstant.MODE_IP_IP);
            mMessageBusiness.retrySendTextAndContactMessage(message, mThreadMessage);
        } else if (ReengMessageConstant.MessageType.containsFile(type)) {
            if (direction == ReengMessageConstant.Direction.received) {
                message.setStatus(ReengMessageConstant.STATUS_NOT_LOAD);
                mApplication.getTransferFileBusiness().startDownloadMessageFile(message);
            } else if (direction == ReengMessageConstant.Direction.send) {
                message.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
                if (type == ReengMessageConstant.MessageType.shareVideo) {
                    Video v = new Video();
                    v.setThreadId(mThreadId);
                    v.setThreadType(mThreadType);
                    v.setMessId(message.getId());
                    v.setDuration(message.getDuration());
                    v.setFilePath(message.getFilePath());
                    v.setFileSize(message.getSize());
                    try {
                        v.setFileOutputPath(FileHelper.createVideoFile().getCanonicalPath());
                    } catch (IOException e) {
                        Log.e(TAG, "Exception", e);
                    }
                    message.setVideo(v);
                    mApplication.getTransferFileBusiness().startUploadMessageFile(message);
                    Log.d(TAG, "ResendVideo");
                } else {
                    mApplication.getTransferFileBusiness().startUploadMessageFile(message);
                }
            }
            notifyDataAdapter();
        } else {
            mActivity.showToast(R.string.e601_error_but_undefined);
        }
    }

    @Override
    public void replyClickCallBack(ReengMessage message) {
    }

    @Override
    public void textContentClickCallBack(ReengMessage message) {
        Log.i(TAG, "textContentClickCallBack");
        if (mReengAccountBusiness.isTranslatable() &&
                message.getMessageType() == ReengMessageConstant.MessageType.text &&
                message.getDirection() == ReengMessageConstant.Direction.received) {
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT ||
                    mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                message.setShowTranslate(!message.isShowTranslate());
                notifyDataAdapter();
            }
        }
    }

    @Override
    public void fileContentClickCallBack(ReengMessage message, boolean isClickDownload) {
    }

    @Override
    public void imageContentClickCallBack(ReengMessage message) {
        ReengMessageConstant.Direction direction = message.getDirection();
        int messageStatus = message.getStatus();
        if (message.getMessageType() == ReengMessageConstant.MessageType.image_link) {
            String directLink = message.getDirectLinkMedia();
            if (!TextUtils.isEmpty(directLink)) {
                if (directLink.startsWith("mocha://")) {
                    DeepLinkHelper.getInstance().openSchemaLink(mActivity, directLink);
                } else {
                    UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(mApplication, mActivity,
                            directLink);
                }
            }
        } else if (direction == ReengMessageConstant.Direction.send
                || (direction == ReengMessageConstant.Direction.received
                && messageStatus == ReengMessageConstant.STATUS_RECEIVED)) {
            // hide alert stranger
            if (mThreadMessage != null && mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    mThreadMessage.isStranger() && !mThreadMessage.isJoined()) {
                mMessageBusiness.updateJoinThreadStranger(mThreadMessage);
            }
            Intent intent = new Intent(mApplication, PreviewImageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, message.getThreadId());
            intent.putExtra(PreviewImageActivity.PARAM_CURRENT_IMAGE, message.getFilePath());
            mApplication.startActivity(intent);
        }
    }

    @Override
    public void voicemailContentClickCallBack(ReengMessage message) {
    }

    @Override
    public void shareContactClickCallBack(ReengMessage reengMessage) {
    }

    @Override
    public void voiceStickerClickCallBack(ReengMessage reengMessage, View convertView) {
    }

    @Override
    public void gifContentClickCallBack(ReengMessage reengMessage) {
    }

    @Override
    public void onMyAvatarClick() {
        NavigateActivityHelper.navigateToMyProfile(mActivity);
    }

    @Override
    public void onFriendAvatarClick(String numberJid, String friendName) {
        Log.d(TAG, "onFriendAvatarClick:---numberJid-> " + numberJid + " friendName: " + friendName);
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(numberJid);
        if (mListener == null) return;
        if (mPhoneNumber != null) {
            mListener.navigateToContactDetailActivity(mPhoneNumber.getId());
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().
                    getExistStrangerPhoneNumberFromNumber(numberJid);
            if (strangerPhoneNumber != null) {
                mListener.navigateToStrangerDetail(strangerPhoneNumber, null, null);
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                mListener.navigateToNonContactDetailActiviy(numberJid);
            } else if (!TextUtils.isEmpty(numberJid) && !TextUtils.isEmpty(friendName)) {
                mListener.navigateToStrangerDetail(null, numberJid, friendName);
            } else {
                mListener.navigateToNonContactDetailActiviy(numberJid);
            }
        }
    }

    @Override
    public void onAcceptInviteMusicClick(final ReengMessage reengMessage) {
    }

    @Override
    public void onCancelInviteMusicClick(ReengMessage reengMessage) {
    }

    @Override
    public void onReinviteShareMusicClick(ReengMessage reengMessage) {
    }

    @Override
    public void onTryPlayMusicClick(ReengMessage reengMessage) {
    }

    @Override
    public void onInviteMusicViaFBClick(ReengMessage reengMessage) {
    }

    @Override
    public void videoContentClickCallBack(ReengMessage message, View convertView) {
    }

    @Override
    public void onGreetingStickerPreviewCallBack(final StickerItem stickerItem) {
    }

    @Override
    public void shareLocationClickCallBack(ReengMessage reengMessage) {
    }

    @Override
    public void onInfoMessageCallBack() {
        showInfoMsg(!isShowInfo);
    }

    @Override
    public void onAcceptMusicGroupClick(ReengMessage reengMessage) {
    }

    @Override
    public void onFollowRoom(ReengMessage reengMessage) {
    }

    @Override
    public void reportClickCallBack(ReengMessage message) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mActivity.showToast(R.string.error_internet_disconnect);
        } else if (SpamRoomManager.getInstance(mApplication).getTimeLock() > 0) {
            // dang bi lock. khong lam gi
        } else {
            ArrayList<ItemContextMenu> listItemContextMenu = MessageHelper.getListItemReport(mActivity, message);
            //file, image, voicemail, shareContact
            PopupHelper.getInstance().showContextMenu(mActivity, null, listItemContextMenu,
                    this);
        }
    }

    @Override
    public void onAcceptCrbtGift(ReengMessage message, MediaModel songModel) {
    }

    @Override
    public void onSendCrbtGift(ReengMessage message, MediaModel songModel) {
    }

    @Override
    public void onDeepLinkClick(ReengMessage message, String link) {
        String serviceType = message.getFilePath();
        if (TextUtils.isEmpty(link)) {
            mActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        DeepLinkHelper.getInstance().openSchemaLink(mActivity, link, serviceType, message);
    }

    @Override
    public void onFakeMoClick(ReengMessage message) {
    }

    @Override
    public void onPollDetail(ReengMessage message, boolean pollUpdate) {
    }

    @Override
    public void onCall(ReengMessage message) {
    }

    @Override
    public void onLuckyWheelHelpClick(final ReengMessage message) {
    }

    @Override
    public void onWatchVideoClick(ReengMessage message, final boolean isUser) {
    }

    @Override
    public void onBplus(ReengMessage message) {
    }

    @Override
    public void onClickPreviewUrl(ReengMessage message, String url) {
        if (TextUtils.isEmpty(url)) return;
        onClickUrl(url);
    }

    @Override
    public void onClickOpenGiftLixi(final ReengMessage message) {
    }

    private void showInfoMsg(boolean isShow) {
        Log.i(TAG, "ThreadDetailAdapter show info msg");
        isShowInfo = isShow;
        mSearchAdapter.setShowInfoMsg(isShow);
        notifyDataAdapter();
    }

    private void setParentLayoutClick() {
        if (mLvwContent == null) return;
        mLvwContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mDownX = event.getX();
                        mDownY = event.getY();
                        isOnClick = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (isOnClick) {
                            showInfoMsg(!isShowInfo);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (isOnClick && (Math.abs(mDownX - event.getX()) > SCROLL_THRESHOLD
                                || Math.abs(mDownY - event.getY()) > SCROLL_THRESHOLD)) {
                            isOnClick = false;
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onClickReplyLixi(ReengMessage message) {

    }

    @Override
    public void onSmartTextClick(String content, int type) {
        if (type == Constants.SMART_TEXT.TYPE_MOCHA) {
            Log.i(TAG, "smarttext mocha: " + content);
            DeepLinkHelper.getInstance().openSchemaLink(mActivity, content);
        } else if (type == Constants.SMART_TEXT.TYPE_URL) {
            onClickUrl(content);
        } else {
            FragmentManager fragmentManager = getFragmentManager();
            PopupHelper.getInstance().showContextMenuSmartText(mActivity, fragmentManager,
                    content, content, type, this);
        }
    }

    @Override
    public void onClickImageReply(ReplyMessage replyMessage) {

    }

    @Override
    public void onOpenViewReaction(View anchorView, ReengMessage reengMessage) {

    }

    @Override
    public void onOpenListReaction(ReengMessage reengMessage, View view) {

    }

    @Override
    public void onClickActionChangeNumber(ReengMessage message, ReengMessageConstant.ActionChangeNumber actionChangeNumber) {

    }

    @Override
    public void onClickReaction(ReengMessage reengMessage, View viewReact) {

    }

    private void onClickUrl(String content) {
        if (URLUtil.isValidUrl(content)) {
            //kiem tra xem co phai la link mocha video ko (co id video)
            Log.i(TAG, "Click content: " + content);
            if (mApplication.getConfigBusiness().isEnableTabVideo()) {
                if (TextHelper.getInstant().isLinkMochaVideo(content)) { //lay dc id thi la mocha video
//                    DeepLinkHelper.getInstance().handleSchemaMochaVideoWeb(mApplication, mActivity, content);
                } else if (TextHelper.getInstant().isLinkMochaChannel(content)) {
                    String idChannel = TextHelper.getInstant().getMochaChannelId(content);
                    if (TextUtils.isEmpty(idChannel)) {
                        UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(mApplication, mActivity, content);
                    }
//                    else
//                        DeepLinkHelper.getInstance().handleSchemaChannelVideo(mApplication, mActivity, idChannel);
                } else
                    UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(mApplication, mActivity, content);
            } else
                UrlConfigHelper.getInstance(mActivity).gotoWebViewOnMedia(mApplication, mActivity, content);
        }
    }

    @Override
    public void OnClickUser(String msisdn, String name) {
    }

    @Override
    public void onDestroyView() {
        if (mImgBackground != null) {
            mImgBackground.setImageBitmap(null);
        }
        if (mMessageArrayList != null) {
            mMessageArrayList.clear();
        }
        mListener = null;
        super.onDestroyView();
    }

    private void notifyDataAdapter() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSearchAdapter.notifyDataSetChanged();
            }
        });
    }

    public void dispatchForwardContentIntent(ReengMessage reengMessage) {
        Intent i = new Intent(mApplication, ChooseContactActivity.class);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadType);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, reengMessage);
        mActivity.startActivityForResult(i, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT, true);
    }

    private OfficerBusiness.RequestReportRoomListener requestReportRoomListener = new OfficerBusiness
            .RequestReportRoomListener() {
        @Override
        public void onResponse() {
            Log.d(TAG, "requestReportRoomListener -ok ");
        }

        @Override
        public void onError(int errorCode) {
            Log.d(TAG, "requestReportRoomListener -fail " + errorCode);
        }
    };

    @Override
    public void onIconClickListener(View view, final Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.COPY:
                TextHelper.copyToClipboard(mActivity, (String) entry);
                break;
            case Constants.MENU.REENG_CHAT:
                ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread((String) entry);
                if (mListener != null)
                    mListener.navigateToThreadDetail(thread);
                break;

            case Constants.MENU.VIEW_CONTATCT_DETAIL:
                PhoneNumber phone = (PhoneNumber) entry;
                if (mListener != null)
                    mListener.navigateToContactDetailActivity(phone.getId());
                break;
            case Constants.MENU.GO_URL:
                UrlConfigHelper.getInstance(getContext()).gotoWebViewOnMedia(mApplication, mActivity, (String) entry);
                break;

            case Constants.MENU.MESSAGE_COPY:
                ReengMessage msg = (ReengMessage) entry;
                ArrayList<TagMocha> listTag = msg.getListTagContent();
                TextHelper.copyToClipboard(getContext(), TagHelper.getTextTagCopy(msg.getContent(), listTag,
                        mApplication));
                break;
            case Constants.MENU.MESSAGE_DELETE:
                Log.d(TAG, "Delete Message");
                ReengMessage message = (ReengMessage) entry;
                //
                if (message.getMessageType() == ReengMessageConstant.MessageType.voicemail) {
                    // stop voice this mail
                    audioVoicemailPlayerImpl2.stopVoicemail(true);
                }
                //neu la message invite chua timeout thi huy count down
                if (message.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic &&
                        message.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                    CountDownInviteManager.getInstance(mApplication).startCountDownMessage(message);
                }
                if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                    mApplication.getTransferFileBusiness().cancelUploadFileMessage(message);
                }
                //mAllMessagesArrayList.remove((Object)message.getId());
                mMessageArrayList.remove(message);
                mMessageBusiness.deleteAMessage(mThreadMessage, message);
                if (mSearchAdapter != null) {
                    mSearchAdapter.setResultPostion(findCurrentMessageIndex(mCurrentReengMessage));
                }
                notifyDataAdapter();
                break;

            case Constants.MENU.MESSAGE_BLOCK_FORWARD:
                mActivity.showToast(R.string.msg_block_forward_message);
                break;

            case Constants.MENU.MESSAGE_FORWARD:
                // open list contact, choose contact and send
                ReengMessage msgForward = (ReengMessage) entry;
                msgForward.setForwardingMessage(true);
                ShareContentBusiness business = new ShareContentBusiness(mActivity, msgForward);
                business.setTypeSharing(ShareContentBusiness.TYPE_FORWARD_MESSAGE);
                business.showPopupForwardMessage();
                break;

            case Constants.MENU.MESSAGE_DETAIL_DELIVER:
                if (mListener != null) {
                    String abStatus = "";
                    mListener.navigateToStatusMessageFragment(mThreadId, ((ReengMessage) entry).getId(), abStatus);
                }
                break;

            case Constants.MENU.MESSAGE_DETAIL_NOTE:
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && mThreadMessage.isPrivateThread()) {
                    mActivity.showToast(getString(R.string.save_message_not_support));
                } else {
                    NoteMessageHelper.getInstance(mApplication).saveNoteMessage(getContext(), (ReengMessage) entry,
                            mThreadMessage, mContactBusiness, mAvatarBusiness, mThreadType,
                            friendPhoneNumber, userNumber, null);
                    mActivity.showToast(getString(R.string.setting_note_message_success));
                }
                break;
            case Constants.MENU.POPUP_YES:
                ReengMessage reengMessage = (ReengMessage) entry;
                reengMessage.setChatMode(ReengMessageConstant.MODE_GSM);
                reengMessage.setCState(cState);
                mMessageBusiness.sendXMPPMessage(reengMessage, mThreadMessage);
                break;

            case Constants.MENU.MESSAGE_MENU_TRANSLATE:
                mMessageBusiness.translateMessageGoogle((ReengMessage) entry, languageCode, mThreadType);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_profile_chat_layout:
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    String number = friendPhoneNumber;
                    PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
                    if (phoneNumber != null) {
                        if (mListener != null)
                            mListener.navigateToContactDetailActivity(phoneNumber.getId());
                    } else {
                        if (mThreadMessage.isStranger() && mThreadMessage.getStrangerPhoneNumber() != null) {
                            if (mListener != null)
                                mListener.navigateToStrangerDetail(mThreadMessage.getStrangerPhoneNumber(), null, null);
                        } else {
                            if (mListener != null)
                                mListener.navigateToNonContactDetailActiviy(number);
                        }
                    }
                }
                break;
        }
    }

    private void setViewListeners() {
        setParentLayoutClick();
    }

    public void setViewHeaderProfile() {
        if (mThreadMessage == null || mApplication == null || !mApplication.isDataReady()) return;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
            NonContact mNonContact = mContactBusiness.getExistNonContact(friendPhoneNumber);
            cState = mContactBusiness.getCStateMessage(mPhoneNumber, mNonContact);
        } else {
            cState = -1;
        }
    }

    private AvatarBusiness mAvatarBusiness;
}

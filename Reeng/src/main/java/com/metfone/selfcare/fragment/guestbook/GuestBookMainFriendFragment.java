package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.MainAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.ui.recyclerview.headerfooter.StrangerGridLayoutManager;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookMainFriendFragment extends BaseRecyclerViewFragment implements
        View.OnClickListener,
        BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = GuestBookMainFriendFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private GuestBookHelper mGuestBookHelper;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private EllipsisTextView mAbTitle;
    private OnFragmentInteractionListener mListener;
    private View mViewHeader;
    private TextView mTvwHeaderInvite;
    private Button mBtnHeaderInvite;
    private RecyclerView mRecyclerView;
    private StrangerGridLayoutManager mLayoutManager;
    private MainAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private ArrayList<Object> listItems = null;
    private boolean showHeader;
    private String friendNumber;
    private String friendName;

    public GuestBookMainFriendFragment() {

    }

    public static GuestBookMainFriendFragment newInstance(String friendNumber, String friendName) {
        GuestBookMainFriendFragment fragment = new GuestBookMainFriendFragment();
        Bundle args = new Bundle();
        args.putString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER, friendNumber);
        args.putString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME, friendName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        getDataAndDrawDetail();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        mGuestBookHelper.cancelRequest(GuestBookHelper.TAG_GET_BOOKS);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER, friendNumber);
        outState.putString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME, friendName);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRetryClick() {

    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_guestbook_main, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        ImageView mAbAddOption = (ImageView) abView.findViewById(R.id.ab_add_action);
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbAddOption.setVisibility(View.GONE);
        mAbBack.setOnClickListener(this);
        mAbTitle.setText(String.format(mRes.getString(R.string.guest_book_friend_title), friendName));
    }

    private void getData(Bundle savedInstanceState) {
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        if (savedInstanceState != null) {
            friendNumber = savedInstanceState.getString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER);
            friendName = savedInstanceState.getString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME);
        } else if (getArguments() != null) {
            friendNumber = getArguments().getString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER);
            friendName = getArguments().getString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME);
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewHeader = inflater.inflate(R.layout.footer_guest_book_main, container, false);
        mTvwHeaderInvite = (TextView) mViewHeader.findViewById(R.id.footer_guest_book_invite_label);
        mBtnHeaderInvite = (Button) mViewHeader.findViewById(R.id.footer_guest_book_invite_button);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        mBtnHeaderInvite.setOnClickListener(this);
        hideEmptyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.footer_guest_book_invite_button:
                handleInviteFriend();
                break;
        }
    }

    private void getDataAndDrawDetail() {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_HOME_FRIEND);
        Log.d(TAG, "getDataAndDrawDetail");
        if (listItems == null) {
            showProgressLoading();
            mGuestBookHelper.requestGetListBooks(friendNumber, false, getFriendBooksListener);
        } else {
            hideEmptyView();
            showHeader = listItems.isEmpty();
            setAdapter();
            showOrHideHeaderFooter();
        }
    }

    private void drawDataAfterRequest(ArrayList<Book> books) {
        Log.d(TAG, "drawDataAfterRequest");
        ArrayList<Object> items = new ArrayList<>();
        if (books == null || books.isEmpty()) {
            showHeader = true;
        } else {
            showHeader = false;
            items.addAll(books);
        }
        hideEmptyView();
        listItems = items;
        setAdapter();
        showOrHideHeaderFooter();
    }

    private void showOrHideHeaderFooter() {
        Log.d(TAG, "showOrHideHeaderFooter: " + showHeader);
        if (showHeader) {
            mTvwHeaderInvite.setText(String.format(mRes.getString(R.string.guest_book_header_main_friend), friendName));
            mBtnHeaderInvite.setText(mRes.getString(R.string.guest_book_remind));
            RecyclerViewUtils.setHeaderView(mRecyclerView, mViewHeader);
        } else {
            RecyclerViewUtils.removeHeaderView(mRecyclerView);
        }
        mHeaderAndFooterAdapter.notifyDataSetChanged();
    }

    private void setAdapter() {
        if (mAdapter == null || mHeaderAndFooterAdapter == null || mRecyclerView.getAdapter() == null) {
            mLayoutManager = new StrangerGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            mLayoutManager.setAutoMeasureEnabled(false);
            mAdapter = new MainAdapter(mApplication, listItems);
            mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
            setItemListViewListener();
        } else {
            mAdapter.setListItems(listItems);
            mHeaderAndFooterAdapter.notifyDataSetChanged();
        }
    }

    private void setItemListViewListener() {
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof Book) {
                    getBookDetail((Book) object);
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        // long click
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void getBookDetail(Book book) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestGetBookDetail(book.getId(), new GuestBookHelper.GetBookDetailListener() {
            @Override
            public void onSuccess(Book bookDetail) {
                // book.setPages(bookDetail.getPages());
                mGuestBookHelper.setCurrentBookEditor(bookDetail);
                mListener.navigateToBookPreview();
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(int error) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.request_send_error);
            }
        });
    }

    private void handleInviteFriend() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(friendNumber);
        InviteFriendHelper.getInstance().requestDeepLinkCampaign(mApplication, mParentActivity,
                numbers, Constants.GUEST_BOOK.INVITE_GUEST_BOOK_CAMPAIGN_ID, new InviteFriendHelper.DeepLinkCampaignCallBack() {
                    @Override
                    public void onSuccess() {
                        mParentActivity.showToast(R.string.request_send_success);
                        mParentActivity.onBackPressed();
                    }

                    @Override
                    public void onError(int errorCode) {
                        String msg;
                        if (errorCode == -2) {
                            msg = mRes.getString(R.string.request_internet_disconnect);
                        } else {
                            msg = mRes.getString(R.string.request_send_error);
                        }
                        mParentActivity.showToast(msg, Toast.LENGTH_LONG);
                    }
                });
    }

    private GuestBookHelper.GetBooksListener getFriendBooksListener = new GuestBookHelper.GetBooksListener() {
        @Override
        public void onSuccess(ArrayList<Book> books) {
            drawDataAfterRequest(books);
        }

        @Override
        public void onError(int error) {
            drawDataAfterRequest(null);
        }
    };

    public interface OnFragmentInteractionListener {
        void navigateToBookPreview();
    }
}
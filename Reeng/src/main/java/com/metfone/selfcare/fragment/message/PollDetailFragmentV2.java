package com.metfone.selfcare.fragment.message;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.adapter.PhoneNumberAdapter;
import com.metfone.selfcare.adapter.VoteAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.holder.onmedia.feeds.ItemInvisibleHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.CusBottomSheetDialog;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.metfone.selfcare.helper.Constants.CONTACT.CONTACT_VIEW_AVATAR_AND_NAME;

/**
 * Created by hoanganhtuan95ptit on 10/26/2018.
 */
public class PollDetailFragmentV2 extends BaseFragment implements ApiCallbackV2<PollObject>,
        View.OnClickListener,
        OnInternetChangedListener {

    @BindView(R.id.iv_poll)
    ImageView ivPoll;
    @BindView(R.id.tv_poll_owner)
    TextView tvPollOwner;
    @BindView(R.id.tv_poll_status)
    TextView tvPollStatus;
    @BindView(R.id.tv_poll_name)
    TextView tvPollName;
    @BindView(R.id.rec_poll)
    RecyclerView recPoll;
    @BindView(R.id.ll_anonymous)
    LinearLayout llAnonymous;
    @BindView(R.id.tv_expire)
    TextView tvExpire;
    @BindView(R.id.ll_expire)
    LinearLayout llExpire;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_add)
    EditText etAdd;
    @BindView(R.id.tv_add)
    ImageView tvAdd;
    @BindView(R.id.ll_add_vote)
    LinearLayout llAddVote;
    @BindView(R.id.progress_loading)
    ProgressLoading progressLoading;
    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.frame_content)
    NestedScrollView frameContent;
    Unbinder unbinder;
    @BindView(R.id.iv_more)
    ImageView ivMore;
    @BindView(R.id.btnEditSurvey)
    Button btnEditSurvey;
    private boolean pollUpdate = false;
    private boolean didAddItem = false;
    public static final int TYPE_GONE = -1;
    public static final int TYPE_POLL = 0;
    public static final int TYPE_ADD_POLL = 1;
    @SuppressLint("StaticFieldLeak")
    private static PollDetailFragmentV2 self;

    private static PollDetailFragmentV2 self() {
        return self;
    }

    public static PollDetailFragmentV2 newInstance(String pollId, int threadId, boolean pollUpdate) {

        Bundle args = new Bundle();
        args.putString(Constants.MESSAGE.POLL_ID, pollId);
        args.putBoolean(Constants.MESSAGE.POLL_UPDATE, pollUpdate);
        args.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        PollDetailFragmentV2 fragment = new PollDetailFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    private View btnBack;

    private String pollId;
    //    private ReengMessage currentReengMessage;
    private ThreadMessage mThreadMessage;

    private PollObject mPollObject;
    private boolean isMultiChoose;
    private boolean haveNewVote;
    private boolean callApiError;
    private boolean loading;
    private String myNumber;

    private OnDefaultItemVoteListener onDefaultItemPollListener;
    private VoteAdapter voteAdapter;
    private ArrayList<Object> feeds;
    private ArrayList<String> selectedIdOld;
    private ArrayList<String> selectedContentOld;
    private ArrayList<String> selectedContentNew;

    private ArrayList<String> optionsOld = new ArrayList<>();
    private ArrayList<String> optionsNew = new ArrayList<>();

    @Nullable
    private PollDetailDialogFragment pollDetailDialogFragment;
    @Nullable
    private ListenerUtils mListenerUtils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = this;

        mListenerUtils = application.getApplicationComponent().providerListenerUtils();
        mListenerUtils.addListener(this);

        int mThreadId = getArguments() != null ? getArguments().getInt(ReengMessageConstant.MESSAGE_THREAD_ID) : 0;
        pollId = getArguments() != null ? getArguments().getString(Constants.MESSAGE.POLL_ID) : null;
        pollUpdate = getArguments() != null && getArguments().getBoolean(Constants.MESSAGE.POLL_UPDATE);
        mThreadMessage = application.getMessageBusiness().getThreadById(mThreadId);
        myNumber = application.getReengAccountBusiness().getJidNumber();

        if (!mThreadMessage.getPhoneNumbers().contains(myNumber)) {
            mThreadMessage.getPhoneNumbers().add(myNumber);
        }
        onDefaultItemPollListener = new OnDefaultItemVoteListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_poll_detail_v2, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSaveInstance = false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        self = null;
        hideKeyboard();
        /*currentReengMessage = ChatActivity.pollReengMessageFocus;
        if (currentReengMessage != null && pollUpdate && didAddItem) {
            MessageBusiness messageBusiness = ApplicationController.self().getMessageBusiness();
            currentReengMessage.setContent(PollRequestHelper.getInstance(ApplicationController.self()).getContentMessageCreatePoll(mPollObject));
            messageBusiness.updateAllFieldsOfMessage(currentReengMessage);
            messageBusiness.refreshThreadWithoutNewMessage(mThreadMessage.getId());
        }
        currentReengMessage = null;*/
        /*if (mThreadMessage != null)
            application.getMessageBusiness().refreshThreadWithoutNewMessage(mThreadMessage.getId());*/
        ChatActivity.pollReengMessageFocus = null;
        if (mListenerUtils != null) {
            mListenerUtils.removerListener(this);
        }
        if (pollDetailDialogFragment != null && !isSaveInstance) {
            pollDetailDialogFragment.dismissAllowingStateLoss();
        }
        unbinder.unbind();
        super.onDestroyView();
    }

    boolean isSaveInstance;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isSaveInstance = true;
    }

    @Override
    public void onInternetChanged() {
        if (!loading && callApiError && NetworkHelper.isConnectInternet(activity)) {
            getPollDetail();
        }
    }

    @Override
    public void onSuccess(String lastId, PollObject poll) throws JSONException {
        loading = false;
        callApiError = false;
        if (progressLoading == null) return;
        progressLoading.setVisibility(View.GONE);

        mPollObject = poll;
        ArrayList<PollItem> pollItems = mPollObject.getListItems();
        selectedIdOld = new ArrayList<>();
        selectedContentOld = new ArrayList<>();
        selectedIdOld.addAll(mPollObject.getListSelected());

        optionsOld.clear();
        if (Utilities.notEmpty(pollItems)) {
            for (PollItem pollItem : pollItems) {

                ArrayList<String> memberNews = new ArrayList<>();
                ArrayList<String> memberCurrents = pollItem.getMemberVoted();
                for (int i = 0; i < memberCurrents.size(); i++) {
                    if (!memberNews.contains(memberCurrents.get(i))) {
                        memberNews.add(memberCurrents.get(i));
                    }
                }
                pollItem.setMemberVoted(memberNews);

                optionsOld.add(pollItem.getTitle());
                if (selectedIdOld.contains(pollItem.getItemId())) {
                    pollItem.setSelected(true);
                } else {
                    pollItem.setSelected(false);
                }
                if (pollItem.isSelected()) {
                    selectedContentOld.add(pollItem.getTitle());
                }
            }
        }
        Collections.sort(pollItems, new Comparator<PollItem>() {
            @Override
            public int compare(PollItem fruit2, PollItem fruit1) {
                if (fruit2.getMemberVoted().size() > fruit1.getMemberVoted().size()) return -1;
                else if (fruit2.getMemberVoted().size() < fruit1.getMemberVoted().size()) return 1;
                else return 0;
            }
        });

        isMultiChoose = mPollObject.getChoice() != 1;

        feeds.addAll(pollItems);

        if (mPollObject.getClose() != 1)
            if (mPollObject.getEnableAddVote() == 1 || mPollObject.getCreator().equals(myNumber))
                feeds.add(VoteAdapter.ADD_POLL);
        voteAdapter.bindData(feeds);
        voteAdapter.setPollObject(mPollObject);
        calculatorTotalVote();

        if (Utilities.equals(poll.getCreator(), myNumber)) {
            tvPollOwner.setText(String.format(getString(R.string.poll_detail_creator), getString(R.string.you)));
        } else {
            ArrayList<String> creatorNumbers = new ArrayList<>();
            creatorNumbers.add(poll.getCreator());
            tvPollOwner.setText(String.format(getString(R.string.poll_detail_creator), application.getContactBusiness().getListNameOfListNumber(creatorNumbers)));
        }
        if (mPollObject.getExpireAt() <= 0) {
            llExpire.setVisibility(View.GONE);
        } else {
            llExpire.setVisibility(View.VISIBLE);
            tvExpire.setText(Html.fromHtml(provideStr(getString(R.string.poll_detail_notify_expire) + ": ", TimeHelper.formatSeparatorTimeOfMessage(poll.getExpireAt(), getResources()))));
        }
        if (mPollObject.getAnonymous() == 0) {
            llAnonymous.setVisibility(View.GONE);
        } else {
            llAnonymous.setVisibility(View.VISIBLE);
        }

        Map<String, String> member = new HashMap<>();
        if (mPollObject != null) {// cập nhật số người vote
            ArrayList<Object> items = voteAdapter.getItems();
            for (int i = 0; i < items.size(); i++) {
                Object o = items.get(i);
                if (o instanceof PollItem) {
                    PollItem pollItem = (PollItem) o;
                    for (int i1 = 0; i1 < pollItem.getMemberVoted().size(); i1++) {
                        member.put(pollItem.getMemberVoted().get(i1), "");
                    }
                }
            }
        }
        if (member.size() == 0)
            tvPollStatus.setText(TimeHelper.formatSeparatorTimeOfMessage(mPollObject.getCreatedAt(), getResources()));
        else
            tvPollStatus.setText(String.format(getString(R.string.poll_detail_status), TimeHelper.formatSeparatorTimeOfMessage(mPollObject.getCreatedAt(), getResources()), String.valueOf(member.size())));
        tvPollName.setText(mPollObject.getTitle());

        btnEditSurvey.setVisibility(View.VISIBLE);
        frameContent.setVisibility(View.VISIBLE);
        application.getMessageBusiness().refreshThreadWithoutNewMessage(mThreadMessage.getId());

        if (mPollObject.getClose() == 1) {
            btnEditSurvey.setText(activity.getResources().getString(R.string.poll_closed));
            btnEditSurvey.setTextColor(ContextCompat.getColor(activity, R.color.videoColorDisSelect));
            btnEditSurvey.setEnabled(false);
            btnEditSurvey.setOnClickListener(null);
            ivMore.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError(String s) {
        loading = false;
        callApiError = true;
        if (progressLoading != null) {
            progressLoading.setVisibility(View.GONE);
        }
        showError();
        if (!NetworkHelper.isConnectInternet(activity)) {
            showErrorNetwork();
            hideErrorDataEmpty();
        } else {
            showErrorDataEmpty();
            hideErrorNetwork();
        }
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            activity.onBackPressed();
        } else if (v == btnEditSurvey) {
            activity.showLoadingDialog("", getString(R.string.loading));
            selectedContentNew = new ArrayList<>();
            for (PollItem pollItem : mPollObject.getListItems()) {
                if (pollItem.isSelected()) {
                    selectedContentNew.add(pollItem.getTitle());
                }
            }
            if (haveNewVote) {
                updatePoll();
            } else {
                votePoll();
            }
        } else if (v == ivMore) {
            new BottomSheetMenu(activity, true)
                    .setListItem(getOverFlowMenus())
                    .setListener(new BottomSheetListener() {
                        @Override
                        public void onItemClick(int itemId, Object entry) {
                            switch (itemId) {
                                case Constants.MENU.MENU_POLL_PIN:
                                    if (mPollObject.getPinStatus() == PollObject.STATUS_PIN) {
                                        if (mThreadMessage.isPrivateThread() && !mThreadMessage.isAdmin()) {
                                            activity.showToast(R.string.poll_pin_in_private_group);
                                        } else
                                            onActionPoll(PollObject.STATUS_UNPIN);
                                    } else if (mPollObject.getPinStatus() == PollObject.STATUS_UNPIN) {
                                        if (mThreadMessage.isPrivateThread() && !mThreadMessage.isAdmin()) {
                                            activity.showToast(R.string.poll_pin_in_private_group);
                                            return;
                                        }
                                        if (mThreadMessage.getPinMessage() != null
                                                && !TextUtils.isEmpty(mThreadMessage.getPinMessage().getContent())) {
                                            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
                                            dialogConfirm.setMessage(activity.getResources().getString(R.string.poll_change_pin));
                                            dialogConfirm.setPositiveLabel(activity.getResources().getString(R.string.poll_pin_this_survey));
                                            dialogConfirm.setNegativeLabel(activity.getResources().getString(R.string.no));
                                            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                                                @Override
                                                public void onPositive(Object result) {
                                                    onActionPoll(PollObject.STATUS_PIN);
                                                    application.getMessageBusiness().sendPinMessage(mThreadMessage, null, activity, true);
                                                    mThreadMessage.setPinMessage("");
                                                    application.getMessageBusiness().updateThreadMessage(mThreadMessage);
                                                }
                                            });
                                            dialogConfirm.show();
                                            return;
                                        } else
                                            onActionPoll(PollObject.STATUS_PIN);
                                    }
                                    break;
                                case Constants.MENU.MENU_POLL_PUSH:
                                    onActionPoll(PollObject.STATUS_PUSH_TOP);
                                    break;
                                case Constants.MENU.MENU_POLL_CLOSE:
                                    DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
                                    dialogConfirm.setLabel(activity.getResources().getString(R.string.poll_title_close_survey));
                                    dialogConfirm.setMessage(activity.getResources().getString(R.string.poll_msg_close_survey));
                                    dialogConfirm.setNegativeLabel(activity.getResources().getString(R.string.cancel));
                                    dialogConfirm.setPositiveLabel(activity.getResources().getString(R.string.poll_close));
                                    dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                                        @Override
                                        public void onPositive(Object result) {
                                            onActionPoll(PollObject.STATUS_CLOSE);
                                        }
                                    });
                                    dialogConfirm.show();

                                    break;
                            }
                        }
                    }).show();
        }
    }

    private void onActionPoll(final int status) {
        activity.showLoadingDialog("", R.string.loading);
        PollRequestHelper.getInstance(application).setStatusPoll(mThreadMessage, mPollObject.getPollId(), status, new ApiCallbackV2<PollObject>() {
            @Override
            public void onSuccess(String msg, PollObject result) throws JSONException {
                PollRequestHelper pollHelper = PollRequestHelper.getInstance(application);
                ReengMessage message = new ReengMessage();
                message.setSender(application.getReengAccountBusiness().getJidNumber());
                message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                message.setThreadId(mThreadMessage.getId());
                message.setDirection(ReengMessageConstant.Direction.send);
                // set time
                message.setTime(System.currentTimeMillis());
                message.setMessageType(ReengMessageConstant.MessageType.poll_action);

                message.setContent(pollHelper.getContentPollActionFromStatus(status,
                        application.getReengAccountBusiness().getJidNumber(), "", result));
                message.setFileId(result.getPollId());
                application.getMessageBusiness().insertNewMessageToDB(mThreadMessage, message);
                application.getMessageBusiness().notifyNewMessage(message, mThreadMessage);

                mPollObject = result;
                if (status == PollObject.STATUS_UNPIN) {
                    activity.showToast(R.string.poll_unpinned_survey_success);
                } else if (status == PollObject.STATUS_PIN) {
                    activity.showToast(R.string.poll_pinned_survey_success);
                } else if (status == PollObject.STATUS_PUSH_TOP) {
                    activity.showToast(R.string.poll_pushed_to_top);
                } else if (status == PollObject.STATUS_CLOSE) {
                    activity.showToast(R.string.poll_close_survey_success);
                }
            }

            @Override
            public void onError(String s) {
                activity.showToast(R.string.e601_error_but_undefined);
            }

            @Override
            public void onComplete() {
                activity.hideLoadingDialog();
            }
        });
    }

    private ArrayList<ItemContextMenu> getOverFlowMenus() {
        ArrayList<ItemContextMenu> mOverFlowItems = new ArrayList<>();
        ItemContextMenu pin = new ItemContextMenu(getString(mPollObject.getPinStatus() == 1
                ? R.string.poll_setting_unpin_survey : R.string.poll_setting_pin_survey),
                R.drawable.ic_bottom_pin, null, Constants.MENU.MENU_POLL_PIN);
        mOverFlowItems.add(pin);

        ItemContextMenu push = new ItemContextMenu(getString(R.string.poll_push_to_top),
                R.drawable.ic_bottom_push_top, null, Constants.MENU.MENU_POLL_PUSH);
        mOverFlowItems.add(push);

        ItemContextMenu close = new ItemContextMenu(getString(R.string.poll_close),
                R.drawable.ic_bottom_close, null, Constants.MENU.MENU_POLL_CLOSE);
        mOverFlowItems.add(close);

        return mOverFlowItems;

    }

    private void updatePoll() {
        optionsNew.clear();
        for (PollItem pollItem : mPollObject.getListItems()) {
            String title = pollItem.getTitle();
            if (Utilities.isEmpty(title)) continue;

            if (!optionsOld.contains(title)) {
                optionsNew.add(title);
            }
        }
        PollRequestHelper.getInstance(application).addMoreV2(mThreadMessage.getServerId(),
                mPollObject.getPollId(),
                optionsOld,
                optionsNew,
                new ApiCallbackV2<PollObject>() {
                    @Override
                    public void onSuccess(String lastId, PollObject pollObject) throws JSONException {
                        for (int i = 0; i < pollObject.getListItems().size(); i++) {
                            PollItem pollItem = pollObject.getListItems().get(i);
                            mPollObject.getListItems().remove(pollItem);
                            mPollObject.getListItems().add(pollItem);
                        }
                        fakeMessageAddVote(pollObject);
                        votePoll();
                    }

                    @Override
                    public void onError(String s) {
                        activity.hideLoadingDialog();
                        try {
                            int errorCode = Integer.valueOf(s);
                            if (errorCode == 418) {
                                Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.poll_detail_expire), Toast.LENGTH_LONG).show();
//                                activity.showToast(R.string.poll_detail_expire);
                            } else {
                                Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
//                                activity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
//                            activity.showToast(R.string.e601_error_but_undefined);
                        }
//                        activity.showToast(R.string.e601_error_but_undefined);
//                        Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void fakeMessageAddVote(PollObject pollObject) {
        MessageBusiness messageBusiness = ApplicationController.self().getMessageBusiness();
        ReengMessage reengMessage = messageBusiness.createMessagePollAfterRequest(mThreadMessage, pollObject, false, true);
        HashMap<String, Integer> hashMap = mThreadMessage.getPollLastId();
        if (hashMap.containsKey(reengMessage.getFileId())) {
            int lastId = hashMap.get(reengMessage.getFileId());
            if (lastId < reengMessage.getId())
                hashMap.put(reengMessage.getFileId(), reengMessage.getId());
        } else {
            hashMap.put(reengMessage.getFileId(), reengMessage.getId());
        }
        messageBusiness.notifyNewMessage(reengMessage, mThreadMessage);
    }

    private void votePoll() {
//        if (Utilities.isEmpty(selectedContentNew) && !haveNewVote) {
//            activity.showToast(R.string.poll_detail_notify_please_select_the_option);
//            activity.hideLoadingDialog();
//            return;
//        }
        if (selectedContentNew.size() == selectedContentOld.size()) {
            boolean haveVoteNew = false;
            for (String s : selectedContentNew) {
                if (!selectedContentOld.contains(s)) {
                    haveVoteNew = true;
                }
            }
            if (!haveNewVote && !haveVoteNew) {// bạn không thêm vote và bạn không vote
                if (selectedContentNew.size() == 0) {
                    activity.showToast(R.string.poll_detail_notify_please_select_the_option);
                } else {
                    activity.showToast(R.string.poll_detail_notify_your_answer_is_the_same_as_the_last_time_you_voted);
                }
                activity.hideLoadingDialog();
                return;
            }
            if (!haveVoteNew) {
                activity.hideLoadingDialog();
                activity.onBackPressed();
                return;
            }
        }
        ArrayList<String> selectedNew = new ArrayList<>();
        for (PollItem item : mPollObject.getListItems()) {
            if (selectedContentNew.contains(item.getTitle())) {
                selectedNew.add(item.getItemId());
            }
        }
        PollRequestHelper.getInstance(application).voteV2(mPollObject.getPollId(),
                selectedNew,
                selectedIdOld,
                new ApiCallbackV2<PollObject>() {
                    @Override
                    public void onError(String s) {
                        activity.hideLoadingDialog();
                        try {
                            int errorCode = Integer.valueOf(s);
                            if (errorCode == 418) {
                                Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.poll_detail_expire), Toast.LENGTH_LONG).show();
//                                activity.showToast(R.string.poll_detail_expire);
                            } else {
                                Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
//                                activity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
//                            activity.showToast(R.string.e601_error_but_undefined);
                        }
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onSuccess(String lastIdStr, PollObject pollObject) throws JSONException {
                        activity.hideLoadingDialog();

                        MessageBusiness messageBusiness = ApplicationController.self().getMessageBusiness();
                        ReengMessage reengMessage = messageBusiness.createMessagePollAfterRequest(mThreadMessage, pollObject, false, false);
                        HashMap<String, Integer> hashMap = mThreadMessage.getPollLastId();
                        if (hashMap.containsKey(reengMessage.getFileId())) {
                            int lastId = hashMap.get(reengMessage.getFileId());
                            if (lastId < reengMessage.getId())
                                hashMap.put(reengMessage.getFileId(), reengMessage.getId());
                        } else {
                            hashMap.put(reengMessage.getFileId(), reengMessage.getId());
                        }
                        messageBusiness.notifyNewMessage(reengMessage, mThreadMessage);
                        activity.onBackPressed();
                    }
                }, mThreadMessage == null ? -1 : mThreadMessage.getId());
    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        getPollDetail();
    }

    @OnClick(R.id.iv_close)
    public void onIvCloseClicked() {
        llAddVote.setVisibility(View.GONE);
        if (mPollObject.getEnableAddVote() == 1 || mPollObject.getCreator().equals(myNumber)) {
            feeds.add(VoteAdapter.ADD_POLL);
        }
        voteAdapter.bindData(feeds);
        hideKeyboard();
    }

    @OnClick(R.id.tv_add)
    public void onTvAddClicked() {
        if (etAdd.getText() != null && etAdd.getText().toString().trim().length() > 0) {
            String voteName = etAdd.getText().toString();
            boolean addVote = true;
            for (PollItem pollItem : mPollObject.getListItems()) {
                if (Utilities.equals(pollItem.getTitle().toUpperCase(), voteName.toUpperCase())) {
                    addVote = false;
                    break;
                }
            }
            if (addVote) {
                haveNewVote = true;
                etAdd.setText("");
//                llAddVote.setVisibility(View.GONE);
                PollItem pollItem = new PollItem();
                pollItem.setItemId(String.valueOf(System.currentTimeMillis()));
                pollItem.setTitle(voteName);

                mPollObject.addPollItem(pollItem);

                feeds.add(0, pollItem);
//                feeds.remove(VoteAdapter.ADD_POLL);
//                if (mPollObject.getEnableAddVote() == 1 || mPollObject.getCreator().equals(myNumber))
//                    feeds.add(VoteAdapter.ADD_POLL);
                voteAdapter.bindData(feeds);

                btnEditSurvey.setTextColor(ContextCompat.getColor(ApplicationController.self(), R.color.white));
//                onItemPollChoseClick(pollItem);
//                hideKeyboard();
            } else {
                activity.showToast(R.string.poll_detail_notify_exist_vote);
            }
        } else {
            activity.showToast(R.string.poll_detail_notify_enter_vote);
        }
    }

    private void initView() {
        initToolBar();
        feeds = new ArrayList<>();

        voteAdapter = new VoteAdapter(application);
        voteAdapter.setOnItemVoteListener(onDefaultItemPollListener);
        voteAdapter.setPollObject(mPollObject);
        voteAdapter.setThreadMessage(mThreadMessage);
        calculatorTotalVote();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(application.getResources().getDrawable(R.drawable.xml_divider_item_decoration));

        recPoll.setLayoutManager(new LinearLayoutManager(activity));
        recPoll.addItemDecoration(dividerItemDecoration);
        recPoll.setNestedScrollingEnabled(false);
        recPoll.setAdapter(voteAdapter);

        getPollDetail();
        ivMore.setVisibility(Config.Features.FLAG_SUPPORT_PIN_VOTE ? View.VISIBLE : View.GONE);
    }

    private void getPollDetail() {
        loading = true;
        hideError();
        btnEditSurvey.setVisibility(View.GONE);
        frameContent.setVisibility(View.GONE);
        progressLoading.setVisibility(View.VISIBLE);
        PollRequestHelper.getInstance(application).findByIdV2(pollId, this, mThreadMessage == null ? -1 : mThreadMessage.getId());
    }

    @SuppressLint("InflateParams")
    private void initToolBar() {
        activity.setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_vote_detail, null));
        TextView tvTitle = activity.getToolBarView().findViewById(R.id.ab_title);
        TextView tvStatus = activity.getToolBarView().findViewById(R.id.ab_status_text);
        btnBack = activity.getToolBarView().findViewById(R.id.ab_back_btn);
        btnEditSurvey.setVisibility(View.GONE);

        tvTitle.setText(R.string.poll_detail_title);
        tvStatus.setText(ApplicationController.self().getMessageBusiness().getThreadName(mThreadMessage));
//        btnEditSurvey.setText(R.string.save);
//        btnEditSurvey.setTextColor(ContextCompat.getColor(ApplicationController.self(), R.color.videoColorNormal));

        btnBack.setOnClickListener(this);
        btnEditSurvey.setOnClickListener(this);
        ivMore.setOnClickListener(this);
    }

    private void onItemPollChoseClick(PollItem item) {
        String myNumber = application.getReengAccountBusiness().getJidNumber();
        if (item.isSelected()) {
            item.setSelectedAndChangeCount(false, myNumber);
        } else if (isMultiChoose) {
            item.setSelectedAndChangeCount(true, myNumber);
        } else {
            for (PollItem vote : mPollObject.getListItems()) {
                if (vote.isSelected()) {
                    vote.setSelectedAndChangeCount(false, myNumber);
                }
            }
            item.setSelectedAndChangeCount(true, myNumber);
        }
        calculatorTotalVote();

        voteAdapter.notifyDataSetChanged();

        Map<String, String> member = new HashMap<>();
        if (mPollObject != null) {// cập nhật số người vote
            ArrayList<Object> items = voteAdapter.getItems();
            for (int i = 0; i < items.size(); i++) {
                Object o = items.get(i);
                if (o instanceof PollItem) {
                    PollItem pollItem = (PollItem) o;
                    for (int i1 = 0; i1 < pollItem.getMemberVoted().size(); i1++) {
                        member.put(pollItem.getMemberVoted().get(i1), "");
                    }
                }
            }
        }
        tvPollStatus.setText(String.format(getString(R.string.poll_detail_status), TimeHelper.formatSeparatorTimeOfMessage(mPollObject.getCreatedAt(), getResources()), String.valueOf(member.size())));
        /*
         * lấy danh sách mảng vote hiện tại
         */
        selectedContentNew = new ArrayList<>();
        for (PollItem pollItem : mPollObject.getListItems()) {
            if (pollItem.isSelected()) {
                selectedContentNew.add(pollItem.getTitle());
            }
        }
        /*
         * so sánh với mảng vote cũ
         */
        if (selectedContentNew.size() == selectedContentOld.size()) {
            boolean haveVoteNew = false;
            for (String s : selectedContentNew) {
                if (!selectedContentOld.contains(s)) {
                    haveVoteNew = true;
                }
            }
            if (!haveNewVote && !haveVoteNew) {// bạn không thêm vote và bạn không vote
//                btnEditSurvey.setTextColor(ContextCompat.getColor(ApplicationController.self(), R.color.videoColorNormal));
            } else {
//                btnEditSurvey.setTextColor(ContextCompat.getColor(ApplicationController.self(), R.color.videoColorSelect));
            }
        } else {
//            btnEditSurvey.setTextColor(ContextCompat.getColor(ApplicationController.self(), R.color.videoColorSelect));
        }
    }

    private void calculatorTotalVote() {
        int totalVote = 0;
        if (mPollObject != null && mPollObject.getListItems() != null) {
            for (PollItem item : mPollObject.getListItems()) {
                totalVote = totalVote + item.getTotalVoted();
            }
        }
        voteAdapter.setTotalVote(totalVote);
    }

    private void onItemPollDetailClick(PollItem pollItem) {
        if (pollDetailDialogFragment != null) {
            pollDetailDialogFragment.dismiss();
        }
        pollDetailDialogFragment = PollDetailDialogFragment.newInstance(mPollObject, mPollObject.getListItems().indexOf(pollItem));
        if (pollDetailDialogFragment != null) {
            pollDetailDialogFragment.show(getChildFragmentManager(), "dialog");
        }
    }

    private void onItemVoteAddClick() {
        feeds.remove(VoteAdapter.ADD_POLL);
        voteAdapter.bindData(feeds);
        llAddVote.setVisibility(View.VISIBLE);
        etAdd.requestFocus();

        showKeyboard();
    }
    private void onShowAddClick(int number) {
        switch (number) {
            case TYPE_ADD_POLL:
                llAddVote.setVisibility(View.VISIBLE);
            case TYPE_GONE:
            default:
        }
    }


    private void setSelected(PollItem pollItem, boolean selected) {
        pollItem.setSelected(selected);
        if (pollItem.isSelected()) {
            pollItem.getMemberVoted().add(myNumber);
        } else {
            pollItem.getMemberVoted().remove(myNumber);
        }
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(etAdd, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(etAdd.getWindowToken(), 0);
        }
    }

    private void showErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.VISIBLE);
    }

    private void hideErrorDataEmpty() {
        if (emptyText == null) return;
        emptyText.setVisibility(View.GONE);
    }

    private void showErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.VISIBLE);
        emptyRetryText2.setVisibility(View.VISIBLE);
    }

    private void hideErrorNetwork() {
        if (emptyRetryButton == null || emptyRetryText2 == null) return;
        emptyRetryButton.setVisibility(View.GONE);
        emptyRetryText2.setVisibility(View.GONE);
    }

    private void showError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        if (frameEmpty == null) return;
        frameEmpty.setVisibility(View.GONE);
    }


    private static class OnDefaultItemVoteListener implements OnItemVoteListener {

        WeakReference<PollDetailFragmentV2> pollDetailFragmentV2WeakReference;

        OnDefaultItemVoteListener(PollDetailFragmentV2 pollDetailFragmentV2) {
            this.pollDetailFragmentV2WeakReference = new WeakReference<>(pollDetailFragmentV2);
        }

        @Override
        public void onItemVoteDetailClick(PollItem pollItem) {
            @Nullable PollDetailFragmentV2 pollDetailFragmentV2 = pollDetailFragmentV2WeakReference.get();
            if (pollDetailFragmentV2 != null) {
                pollDetailFragmentV2.onItemPollDetailClick(pollItem);
            }
        }

        @Override
        public void onItemVoteChoseClick(PollItem pollItem) {
            @Nullable PollDetailFragmentV2 pollDetailFragmentV2 = pollDetailFragmentV2WeakReference.get();
            if (pollDetailFragmentV2 != null) {
                pollDetailFragmentV2.onItemPollChoseClick(pollItem);
            }
        }

        @Override
        public void onItemVoteAddClick() {
            @Nullable PollDetailFragmentV2 pollDetailFragmentV2 = pollDetailFragmentV2WeakReference.get();
            if (pollDetailFragmentV2 != null) {
                pollDetailFragmentV2.onItemVoteAddClick();
            }
        }

        @Override
        public void onShowAddClick(int addPoll) {
            @Nullable PollDetailFragmentV2 pollDetailFragmentV2 = pollDetailFragmentV2WeakReference.get();
            if (pollDetailFragmentV2 != null) {
                pollDetailFragmentV2.onShowAddClick(addPoll);
            }
        }
    }

    public static class PollDetailDialogFragment extends BottomSheetDialogFragment {

        private static final String POLL_OBJECT = "POLL_OBJECT";
        private static final String POLL_POSITION = "POLL_POSITION";

        @BindView(R.id.tab_layout)
        TabLayout tabLayout;
        @BindView(R.id.view_pager)
        ViewPager viewPager;
        Unbinder unbinder;

        private static PollDetailDialogFragment newInstance(PollObject pollObject, int position) {

            Bundle args = new Bundle();
            args.putSerializable(POLL_OBJECT, pollObject);
            args.putInt(POLL_POSITION, position);
            PollDetailDialogFragment fragment = new PollDetailDialogFragment();
            fragment.setArguments(args);
            return fragment;
        }

        private PollObject pollObject;
        private int position;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            pollObject = getArguments() != null ? (PollObject) (getArguments().getSerializable(POLL_OBJECT)) : null;
            position = getArguments() != null ? getArguments().getInt(POLL_POSITION) : 0;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final CusBottomSheetDialog dialog = new CusBottomSheetDialog(getContext(), getTheme());
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    try {
                        FrameLayout bottomSheet = (FrameLayout) dialog.findViewById(R.id.design_bottom_sheet);
                        BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                        dialog.setLockDragging(true);
                    } catch (Exception e) {
                        Log.d("PollDetailFragmentV2", "show: Can not expand", e);
                    }
                }
            });

            return dialog;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_dialog_poll_detail, container, false);
            unbinder = ButterKnife.bind(this, view);
            initView();
            return view;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    CusBottomSheetDialog dialog = (CusBottomSheetDialog) getDialog();
                    FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                    BottomSheetBehavior behavior;

                    if (bottomSheet != null) {
                        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();
                        layoutParams.height = Math.min(Utilities.dpToPx(350), ScreenManager.getHeight(getActivity()) / 2);
                        bottomSheet.setLayoutParams(layoutParams);

                        behavior = BottomSheetBehavior.from(bottomSheet);
                        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        behavior.setPeekHeight(0);
                    }
                }
            });
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            unbinder.unbind();
        }

        private void initView() {
            PollDetailPagerAdapter pollDetailPagerAdapter = new PollDetailPagerAdapter(getChildFragmentManager());
            for (PollItem pollItem : pollObject.getListItems()) {
                pollDetailPagerAdapter.add(pollItem.getTitle() + " (" + pollItem.getMemberVoted().size() + ")", PollVoteFragment.newInstance(pollItem));
            }
            viewPager.setAdapter(pollDetailPagerAdapter);
            viewPager.setCurrentItem(position);

            if (pollObject.getListItems().size() <= 3) {
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
            }
            tabLayout.setupWithViewPager(viewPager);
        }

        private static class PollDetailPagerAdapter extends FragmentStatePagerAdapter {

            private ArrayList<String> titles;
            private ArrayList<Fragment> fragments;

            PollDetailPagerAdapter(FragmentManager fm) {
                super(fm);
                titles = new ArrayList<>();
                fragments = new ArrayList<>();
            }

            void add(String title, Fragment fragment) {
                titles.add(title);
                fragments.add(fragment);
                notifyDataSetChanged();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return titles.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        }

        public static class PollVoteFragment extends BaseFragment {

            private static final String POLL_ITEM = "POLL_ITEM";

            @BindView(R.id.rec_vote)
            RecyclerView recVote;
            Unbinder unbinder;
            @BindView(R.id.tv_empty)
            TextView tvEmpty;

            public static PollVoteFragment newInstance(PollItem pollItem) {

                Bundle args = new Bundle();
                args.putSerializable(POLL_ITEM, pollItem);
                PollVoteFragment fragment = new PollVoteFragment();
                fragment.setArguments(args);
                return fragment;
            }

            private PollItem pollItem;

            @Override
            public void onCreate(@Nullable Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                pollItem = (PollItem) (getArguments() != null ? getArguments().getSerializable(POLL_ITEM) : null);
            }

            @Nullable
            @Override
            public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.fragment_poll_vote, container, false);
                unbinder = ButterKnife.bind(this, view);
                initView();
                return view;
            }

            @Override
            public void onDestroyView() {
                super.onDestroyView();
                unbinder.unbind();
            }

            private void initView() {
                ArrayList<PhoneNumber> phoneNumbers;
                if (Utilities.notEmpty(pollItem.getMemberVoted())) {
                    tvEmpty.setVisibility(View.GONE);
                    phoneNumbers = ApplicationController.self().getContactBusiness().getListPhoneNumberFormListNumber(pollItem.getMemberVoted());
                    for (PhoneNumber phoneNumber : phoneNumbers) {
                        if (Utilities.equals(phoneNumber.getJidNumber(), PollDetailFragmentV2.self().myNumber)) {
                            phoneNumber.setName(getString(R.string.you));
                        }
                    }
                } else {
                    tvEmpty.setVisibility(View.VISIBLE);
                    phoneNumbers = new ArrayList<>();
                }
                PhoneNumberAdapter voteAdapter = new PhoneNumberAdapter(ApplicationController.self(),
                        phoneNumbers,
                        new ClickListener.IconListener() {
                            @Override
                            public void onIconClickListener(View view, Object entry, int menuId) {

                            }
                        }, CONTACT_VIEW_AVATAR_AND_NAME);
                recVote.setAdapter(voteAdapter);
                recVote.setLayoutManager(new LinearLayoutManager(activity));
            }

        }
    }

    public interface OnItemVoteListener {
        void onItemVoteDetailClick(PollItem pollItem);

        void onItemVoteChoseClick(PollItem pollItem);

        void onItemVoteAddClick();

        void onShowAddClick(int addPoll);
    }

    private String provideStr(String key, String value) {
        return String.format("<font color=\"#6D6D6D\">%s </font> <font color=\"#f26522\">%s</font>", key, value);
    }
    public LinearLayout getLlAddVote() {
        return llAddVote;
    }
}

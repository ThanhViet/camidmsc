package com.metfone.selfcare.fragment.changenumber;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.ChangeNumberActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.view.PinEntryEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 11/14/2018.
 */

public class InputOTPFragment extends Fragment implements /*SmsReceiverListener,*/ ClickListener.IconListener, MySMSBroadcastReceiver.OTPReceiveListener {

    /*@BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.tvCode1)
    TextView tvCode1;
    @BindView(R.id.tvCode2)
    TextView tvCode2;
    @BindView(R.id.tvCode3)
    TextView tvCode3;
    @BindView(R.id.tvCode4)
    TextView tvCode4;
    @BindView(R.id.tv_code_5)
    TextView tvCode5;
    @BindView(R.id.tv_code_6)
    TextView tvCode6;*/
    @BindView(R.id.tvDesInfo)
    TextView tvDesInfo;
    @BindView(R.id.tvNumber)
    TextView tvNumber;
    @BindView(R.id.password_note_sms)
    TextView passwordNoteSms;
    @BindView(R.id.txt_pin_entry)
    PinEntryEditText txtPinEntry;


    Unbinder unbinder;


    private ClickListener.IconListener mClickHandler;
    private String password;


    private long countDownTimer;
    private static final int TIME_DEFAULT = 60000;

    private int START_TEXT = 85;
    private int END_TEXT = 0;

    private ChangeNumberActivity mActivity;
    private ApplicationController mApp;
    private Resources mRes;
    private Handler mHandler;

    public static InputOTPFragment newInstance() {
        InputOTPFragment fragment = new InputOTPFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ChangeNumberActivity) getActivity();
        mApp = (ApplicationController) mActivity.getApplication();
        mRes = mApp.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_otp_change_number, container, false);
        unbinder = ButterKnife.bind(this, view);
        mHandler = new Handler();
        setPassEditTextListener();
        startCountDown();
//        etPassword.requestFocus();
        drawInfo();

        mApp.registerSmsOTPObserver();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mClickHandler = this;
//        SmsReceiver.addSMSReceivedListener(this);
//        MySMSBroadcastReceiver.addSMSReceivedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mClickHandler = null;
//        SmsReceiver.removeSMSReceivedListener(this);
//        MySMSBroadcastReceiver.removeSMSReceivedListener(this);
        InputMethodUtils.hideSoftKeyboard(txtPinEntry, mActivity);
    }

    @Override
    public void onDestroy() {
        if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    private void setResendPassListener() {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View textView) {
                showDialogConfirmResendCode();
            }
        };
        String textNormal = String.format(
                mRes.getString(R.string.login_sms_notice), "0") + " ";
        String textToSpan = mRes.getString(R.string.request_again);
        String textToShow = textNormal + textToSpan;
        SpannableString spannableString = new SpannableString(textToShow);
        START_TEXT = textNormal.length();
        END_TEXT = textToShow.length();
        spannableString.setSpan(clickableSpan, START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color
                        .red)),
                START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        passwordNoteSms.setText(spannableString);
        passwordNoteSms.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void startCountDown() {
        countDownTimer = TIME_DEFAULT;
        String countDownString = String.format(
                mRes.getString(R.string.login_sms_notice), String
                        .valueOf((int) (TIME_DEFAULT / 1000.0))) + " " + mRes.getString(R.string
                .request_again);
        passwordNoteSms.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                int remain = (int) (countDownTimer / 1000.0);
                String countDownString = String.format(
                        mRes.getString(R.string.login_sms_notice), String
                                .valueOf(remain)) + " " + mRes.getString(R.string.request_again);
                if (passwordNoteSms != null) {
                    passwordNoteSms.setText(countDownString);
                    if (remain == 0) {
                        setResendPassListener();
                    } else {
                        mHandler.postDelayed(this, 1000);
                    }
                }
            }
        }, 1000);
    }

    private void showDialogConfirmResendCode() {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_confirm_resend_code);
        String msg = String.format(mRes.getString(R.string.msg_confirm_resend_code), tvNumber.getText()
                .toString());
        PopupHelper.getInstance().showDialogConfirm(mActivity, title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    private void drawInfo() {
        PhoneNumberUtil phoneUtil = mApp.getPhoneUtil();
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(phoneUtil, mActivity.getCurrentNumberJid(), mActivity.getCurrentRegionCode());
        StringBuilder sb = new StringBuilder();
        if (PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil,
                phoneNumberProtocol)) {
            sb.append("(+");
            sb.append(String.valueOf(phoneNumberProtocol.getCountryCode())).append(") ");
            sb.append(String.valueOf(phoneNumberProtocol.getNationalNumber()));
            tvNumber.setText(sb.toString());
        } else {
            tvNumber.setText(mActivity.getCurrentNumberJid());
        }
        int codeGenOtp = mActivity.getOtpCode();
        if (codeGenOtp == 200) {
            tvDesInfo.setText(mRes.getString(R.string.login_header_notice_2));
        } else if (codeGenOtp == 201) {
            tvDesInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_201), mActivity.getCurrentNumberJid()));
        } else if (codeGenOtp == 202) {
            tvDesInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_202), mActivity.getCurrentNumberJid()));
        }
    }

    private void setPassEditTextListener() {
        /*etPassword.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                showCode(s.toString());
            }
        });

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String loginPassword = etPassword.getText().toString();
                    if (etPassword.length() >= 6) {
                        password = loginPassword;
                        return true;
                    } else {
                        mActivity.showToast(R.string.code_invalid_format);
                    }
                }
                return false;
            }
        });*/

        txtPinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                if (str.toString().length() == 6) {
                    password = str.toString();
                    mActivity.processChangeNumber();
                }
            }
        });
    }

    /*private void showCode(String code) {
        clearCode();
        switch (code.length()) {
            case 0:

                break;
            case 1:
                char a = code.charAt(0);
                tvCode1.setText(String.valueOf(a));
                break;
            case 2:
                tvCode1.setText(String.valueOf(code.charAt(0)));
                tvCode2.setText(String.valueOf(code.charAt(1)));
                break;
            case 3:
                tvCode1.setText(String.valueOf(code.charAt(0)));
                tvCode2.setText(String.valueOf(code.charAt(1)));
                tvCode3.setText(String.valueOf(code.charAt(2)));
                break;
            case 4:
                tvCode1.setText(String.valueOf(code.charAt(0)));
                tvCode2.setText(String.valueOf(code.charAt(1)));
                tvCode3.setText(String.valueOf(code.charAt(2)));
                tvCode4.setText(String.valueOf(code.charAt(3)));
                break;
            case 5:
                tvCode1.setText(String.valueOf(code.charAt(0)));
                tvCode2.setText(String.valueOf(code.charAt(1)));
                tvCode3.setText(String.valueOf(code.charAt(2)));
                tvCode4.setText(String.valueOf(code.charAt(3)));
                tvCode5.setText(String.valueOf(code.charAt(4)));
                break;
            case 6:
                tvCode1.setText(String.valueOf(code.charAt(0)));
                tvCode2.setText(String.valueOf(code.charAt(1)));
                tvCode3.setText(String.valueOf(code.charAt(2)));
                tvCode4.setText(String.valueOf(code.charAt(3)));
                tvCode5.setText(String.valueOf(code.charAt(4)));
                tvCode6.setText(String.valueOf(code.charAt(5)));
                break;
        }

    }

    private void clearCode() {
        tvCode1.setText("");
        tvCode2.setText("");
        tvCode3.setText("");
        tvCode4.setText("");
        tvCode5.setText("");
        tvCode6.setText("");
    }*/


    public String getPassword() {
        password = txtPinEntry.getText().toString().trim();
        return password;
    }


//    @Override
//    public void onSmsPasswordReceived(String pass) {
//        this.password = pass;
//        mActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                txtPinEntry.setText(password);
//                mHandler.removeCallbacksAndMessages(null);
////                mActivity.processChangeNumber();
//            }
//        });
//    }
//
//
//    @Override
//    public void onSmsPasswordLixiReceived(String password) {
//
//    }
//
//    @Override
//    public boolean onSmsChatReceived(String smsSender, String smsContent) {
//        return false;
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*@OnClick({R.id.etPassword, R.id.tvCode1})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etPassword:
                break;
            case R.id.tvCode1:
                break;
        }
    }*/

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                mActivity.generateNewAuthCodeByServer(false);
                break;
            default:
                break;
        }
    }


    //XU ly phan sms otp
//    @Override
//    public void onOTPReceived(String otp) {
//        this.password = otp;
//        mActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                txtPinEntry.setText(password);
//                mHandler.removeCallbacksAndMessages(null);
////                mActivity.processChangeNumber();
//            }
//        });
//        return true;
//    }

    @Override
    public void onOTPReceived(Intent intent) {

    }

    @Override
    public void onOTPTimeOut() {
    }
}

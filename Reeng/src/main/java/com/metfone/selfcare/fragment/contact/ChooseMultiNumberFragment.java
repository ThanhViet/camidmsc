package com.metfone.selfcare.fragment.contact;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.UiThread;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.PollMessageActivity;
import com.metfone.selfcare.adapter.PhoneNumberAdapter;
import com.metfone.selfcare.adapter.PhoneNumberRecentAdapter;
import com.metfone.selfcare.adapter.PhoneNumberRecyclerAdapter;
import com.metfone.selfcare.adapter.PhoneNumbersSelectedAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.SectionCharecter;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.SyncContactListner;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.FlowLayout;
import com.metfone.selfcare.ui.HorizontalListView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.indexable.IndexableRecyclerView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.metfone.selfcare.helper.Constants.CHOOSE_CONTACT.REF_PREFIX_CHANGE;

/**
 * Created by toanvk2 on 7/3/14.
 */
public class ChooseMultiNumberFragment extends Fragment implements ClickListener.IconListener,
        ContactChangeListener, ConfigGroupListener, InitDataListener, SyncContactListner {
    private static final String LIST_CHECKED = "list_checked";
    private final String TAG = ChooseMultiNumberFragment.class.getSimpleName();
    boolean isSelectAllContact = false;
    private ApplicationController mApplication;
    private ChooseContactActivity mParentActivity;
    private Handler mHandler;
    private OnFragmentInteractionListener mListener;
    private static final int THREAD_PERSON_CHAT = 1;
    InviteFriendHelper.InviteFromRoomCallBack roomInviteCallback = new InviteFriendHelper.InviteFromRoomCallBack() {
        @Override
        public void onRequestSuccess() {
            if (mListener != null) {
                mListener.returnRoomInvite();
            }
        }

        @Override
        public void onRequestFailed() {
            if (mListener != null) {
                mListener.returnRoomInvite();
            }
        }
    };
    private Resources mRes;
    private int actionType, mThreadId, mTypeScreen = -1;
    private ThreadMessage mThreadGroup;
    //variable
    private ArrayList<String> mMemberThread = new ArrayList<>();
    private ArrayList<PhoneNumber> mListPhoneNumbers;
    private ArrayList<PhoneNumber> mListContactPhoneNumbers;
    private HashSet<PhoneNumber> mPhoneInGroup;
    private PhoneNumberRecyclerAdapter mHeaderAndFooterAdapter;
    private PhoneNumberAdapter mAdapter;
    private ArrayList<String> mListNumberChecked = new ArrayList<>();
    private ArrayList<PhoneNumber> mListChecked = new ArrayList<>();
    private ArrayList<PhoneNumber> mListShareChecked = new ArrayList<>();


    private IndexableRecyclerView mRecyclerView;
    private TextView mTvSizeNumberSelected, mTvNote;
    private ProgressLoading mPbLoading;
    private String myNumber, mRoomId, mDeepLinkCampaignId;
    private ReengSearchView mSearchView;
    private GetListPhoneNumberAsyncTask mGetListAsyncTask;
    private SearchContactTask mSearchTask;
    private View mViewActionBar, mViewSizeSelected;
    private ImageView mImBack;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mAccountBusiness;
    private ContactBusiness mContactBusiness;
    private HorizontalListView mListViewPhoneSelected;
    private LinearLayout llSelected, llSearch;
    private TextView mTvwNoteBroadcast, txtContacts, tvNoteBroadcastPrefixChange, mTvwHideBroadcast, mImDone, mTvRecent;
    private View mHeaderView;
    private TextView mTvTitle;
    private ImageView btnMore;
    private View llPrefixChange;
    private ImageView cbSelectAllContact;
    private ArrayList<PhoneNumber> mListPhoneSelected = new ArrayList<>();
    private ArrayList<PhoneNumber> mListPhoneSearch = new ArrayList<>();
    private PhoneNumbersSelectedAdapter mAdapterPhoneSelected;
    private ReengMessage message;
    private PhoneNumberUtil mPhoneUtil;
    private View mViewInviteMore;
    private RelativeLayout mRltInviteMore, viewTextContacts, viewChooseNumber, viewChatHeaderChooseNumber;
    private EllipsisTextView mTvwInviteMoreNumber;
    private PhoneNumberRecentAdapter mPhoneNumberRecentAdapter;
    private List<PhoneNumber> mListPhoneRecent = new ArrayList<>() ;
    private RecyclerView mRecyclePhoneRecent;
    private ImageView ivSearch;
    private ArrayList<SectionCharecter> mListSectionChar;
    private View mViewHeader, llOptionCreateChat, llFavorite;
    private View rootView;
    public ChooseMultiNumberFragment() {
        // Required empty public constructor
    }

    public static ChooseMultiNumberFragment newInstance(ArrayList<String> listNumbers,
                                                        int actionType, int threadId, String roomID) {
        ChooseMultiNumberFragment fragment = new ChooseMultiNumberFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listNumbers);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadId);
        args.putString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID, roomID);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * @param listNumbers
     * @param actionType
     * @param threadId
     * @return fragment
     */
    public static ChooseMultiNumberFragment newInstance(ArrayList<String> listNumbers,
                                                        int actionType, int threadId) {
        ChooseMultiNumberFragment fragment = new ChooseMultiNumberFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listNumbers);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadId);
        fragment.setArguments(args);
        return fragment;
    }
    public static ChooseMultiNumberFragment newInstance(ArrayList<String> listNumbers,
                                                        int actionType, int threadId, int typeScreen) {
        ChooseMultiNumberFragment fragment = new ChooseMultiNumberFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listNumbers);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadId);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE_CREATE_GROUP_SETTING, typeScreen);

        fragment.setArguments(args);
        return fragment;
    }
    public static ChooseMultiNumberFragment newInstance(ArrayList<String> listNumbers,
                                                        int actionType, int threadId, ReengMessage message) {
        ChooseMultiNumberFragment fragment = new ChooseMultiNumberFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listNumbers);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadId);
        args.putSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChooseMultiNumberFragment newInstance(int actionType, String deepLinkCampaignId) {
        ChooseMultiNumberFragment fragment = new ChooseMultiNumberFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN, deepLinkCampaignId);
        fragment.setArguments(args);
        return fragment;
    }

    private void setMemberInThread(ArrayList<String> lists) {
        if (lists == null || lists.isEmpty()) {
            mMemberThread = new ArrayList<>();
        } else {
            mMemberThread = lists;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach :");
        mParentActivity = (ChooseContactActivity) activity;
        mRes = mParentActivity.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.fragment_choose_number, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mParentActivity.setColorIconStatusBar();
        }

        getData();
        findComponentViews(rootView, container, inflater);
        initAdapter();
        setListener();
        mPhoneUtil = mApplication.getPhoneUtil();
        setTitle();
        setClickDetail();
        return rootView;
    }

    private void initAdapter() {
        mAdapter = new PhoneNumberAdapter(mApplication, new ArrayList<PhoneNumber>(), this,
                    Constants.CONTACT.CONTACT_VIEW_CHECKBOX);

        mAdapter.setMemberThread(mMemberThread);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        mHeaderAndFooterAdapter = new PhoneNumberRecyclerAdapter(mAdapter, mApplication);
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
            mHeaderAndFooterAdapter.setListSectionCharecter(mListSectionChar);
        }
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        mHeaderAndFooterAdapter.addHeaderView(mViewInviteMore);
        mHeaderAndFooterAdapter.addHeaderView(mViewHeader);
        setItemListViewListener();
        return;
    }

    private void setTitle() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mTvTitle.getLayoutParams();
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mTvTitle.setGravity(Gravity.CENTER);
        mTvTitle.setTextColor(Color.WHITE);
        btnMore.setVisibility(View.GONE);
        mImDone.setVisibility(View.VISIBLE);
        mImDone.setText(R.string.action_done);
//        mImDone.setPaintFlags(mImDone.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
            mTvTitle.setText(mRes.getString(R.string.chat_create_new_group));
            mImDone.setText(getString(R.string.done));
            viewTextContacts.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_color_bubble_send));
            txtContacts.setTextSize(14);
            mRecyclePhoneRecent.setVisibility(View.VISIBLE);
        } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST) {
            mTvTitle.setText(mRes.getString(R.string.create_broadcast));
            mImDone.setText(R.string.done);
            mRecyclePhoneRecent.setVisibility(View.VISIBLE);
        } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
            mTvTitle.setTextColor(Color.BLACK);
            mTvTitle.setText(mRes.getString(R.string.contact));
            mTvRecent.setVisibility(View.GONE);
            mImDone.setTextColor(mViewActionBar.getResources().getColor(R.color.bg_button_red));
        } else {
            mTvTitle.setText(mRes.getString(R.string.avno_title_select));
            mTvRecent.setVisibility(View.GONE);
            mRecyclePhoneRecent.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID);
            actionType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mMemberThread = savedInstanceState.getStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER);
            mListNumberChecked = savedInstanceState.getStringArrayList(LIST_CHECKED);
            message = (ReengMessage) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE);
            mRoomId = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID);
            mDeepLinkCampaignId = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN);
            mTypeScreen = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE_CREATE_GROUP_SETTING);

        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        ListenerHelper.getInstance().addInitDataListener(this);
        ListenerHelper.getInstance().addSyncContactListner(this);
        if (mApplication.isDataReady()) {
            ListenerHelper.getInstance().addContactChangeListener(this);
            mApplication.getMessageBusiness().addConfigGroupListener(this);
            getListPhoneAsynctask();
        } else {
            mPbLoading.setVisibility(View.VISIBLE);
            mPbLoading.setEnabled(true);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        ListenerHelper.getInstance().removeInitDataListener(this);
        mApplication.getMessageBusiness().removeConfigGroupListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        ListenerHelper.getInstance().removeSyncContactListner(this);
        mHandler = null;
        if (mGetListAsyncTask != null) {
            mGetListAsyncTask.cancel(true);
            mGetListAsyncTask = null;
        }
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, mThreadId);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        outState.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, mMemberThread);
        outState.putStringArrayList(LIST_CHECKED, mListNumberChecked);
        outState.putSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, message);
        outState.putString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID, mRoomId);
        outState.putString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN, mDeepLinkCampaignId);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE_CREATE_GROUP_SETTING, mTypeScreen);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (mListPhoneNumbers != null)
            for (PhoneNumber phone : mListPhoneNumbers) {
                if (phone.isChecked() || phone.isDisable()) {
                    phone.setChecked(false);
                    phone.setDisable(false);
                }
            }
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) {
            return;
        }
        ListenerHelper.getInstance().addContactChangeListener(this);
        mApplication.getMessageBusiness().addConfigGroupListener(this);
        mHandler.post(() -> getListPhoneAsynctask());
    }

    //check box click
    @Override
    public void onIconClickListener(View view, Object entry, int arg) {
        Log.i(TAG, "onIconClickListener " + entry);
        switch (arg) {
            default:
                break;
        }
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        mHandler.post(this::getListPhoneAsynctask);
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        mHandler.post(() -> {
            if (mAdapter != null)
                mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onRosterChange() {
    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
        if (mThreadGroup == null) {
            return;
        }
        if (threadMessage.getId() != mThreadGroup.getId()) {
            return;
        }
        if (actionChange == Constants.MESSAGE.CHANGE_GROUP_MEMBER) {
            mHandler.post(() -> {
                ArrayList<String> listMemberThread = mThreadGroup.getPhoneNumbers();
                setMemberInThread(listMemberThread);
                getListPhoneAsynctask();
            });
        }
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @SuppressLint("ResourceAsColor")
    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewActionBar = mParentActivity.getToolBarView();
        View content = inflater.inflate(R.layout.chat_header_choose_number, null);
        mParentActivity.setCustomViewToolBar(content);

        RelativeLayout rltToolbar;
        rltToolbar = mViewActionBar.findViewById(R.id.ab_detail);

//        mViewFooter = mLayoutInflater.inflate(R.layout.button_layout, container, false);
        mImBack = mViewActionBar.findViewById(R.id.ab_back_btn);
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
            //mImBack.setVisibility(View.GONE);
            TextView cancel = mViewActionBar.findViewById(R.id.ab_cancel_text);
            //cancel.setVisibility(View.VISIBLE);
            cancel.setPaintFlags(cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cancel.setOnClickListener(v -> mParentActivity.onBackPressed());
        }

        mImDone = mViewActionBar.findViewById(R.id.ab_agree_text);
        mImDone.setTextColor(Color.WHITE);

        btnMore = mViewActionBar.findViewById(R.id.ab_more_btn);
        mTvTitle = mViewActionBar.findViewById(R.id.ab_title);
        mTvTitle.setTextColor(Color.WHITE);

        View vDivider = mViewActionBar.findViewById(R.id.vDivider);
        vDivider.setVisibility(View.GONE);
        mSearchView = rootView.findViewById(R.id.etSearch);
        ivSearch = rootView.findViewById(R.id.ivSearch);
        mSearchView.setHint(R.string.sms_search_contact);
        mSearchView.setTextColor(Color.parseColor("#FFFFFF"));
//        mSearchView.postDelayed(() -> InputMethodUtils.showSoftKeyboardNew(mParentActivity, mSearchView), 300);
        mHeaderView = rootView.findViewById(R.id.layout_phone_choose);
        mRecyclerView = rootView.findViewById(R.id.fragment_choose_number_listview);
        mRecyclerView.setHideFastScroll(true);
        mTvSizeNumberSelected = rootView.findViewById(R.id.fragment_choose_number_size_text);
        mViewSizeSelected = rootView.findViewById(R.id.fragment_choose_number_size_layout);

        mTvNote = rootView.findViewById(R.id.fragment_choose_number_note_text);
        mPbLoading = rootView.findViewById(R.id.fragment_choose_number_loading_progressbar);
        mViewHeader = inflater.inflate(R.layout.item_recent_favourite_contact, null);
        mRecyclePhoneRecent = mViewHeader.findViewById(R.id.fragment_choose_number_recent_listview);
        llPrefixChange = rootView.findViewById(R.id.llBroadcastPrefixChange);
        cbSelectAllContact = rootView.findViewById(R.id.cbSelectAllContact);
        tvNoteBroadcastPrefixChange = rootView.findViewById(R.id.tvNoteBroadcastPrefixChange);
        mTvRecent = mViewHeader.findViewById(R.id.fragment_choose_number_tv_recent);
        llOptionCreateChat = mViewHeader.findViewById(R.id.llOptionCreateChat);
        llFavorite = mViewHeader.findViewById(R.id.llFavorite);
//        mBtninvite = (Button) mViewFooter.findViewById(R.id.button_invite);
        mTvNote.setVisibility(View.GONE);
        /*mTvNote.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        mTvNote.setPadding(0, 0, 0, 15);*/
        mPbLoading.setEnabled(false);
        mPbLoading.setVisibility(View.GONE);
        llOptionCreateChat.setVisibility(View.GONE);
        llFavorite.setVisibility(View.GONE);
        InputMethodUtils.hideKeyboardWhenTouch(mHeaderView, mParentActivity);
        //mSearchView.requestFocus();
        mSearchView.postDelayed(new Runnable() {
            @Override
            public void run() {
                //mSearchView.requestFocus();
            }
        }, 100);
        //        mRecyclerView.addFooterView(mViewFooter);

        // invite voi so chua luu
        mViewInviteMore = inflater.inflate(R.layout.header_invite_more, container, false);
        mRltInviteMore = mViewInviteMore.findViewById(R.id.invite_more_layout);
        viewTextContacts = rootView.findViewById(R.id.viewTextContacts);
        viewChooseNumber = rootView.findViewById(R.id.viewChooseNumber);
        llSearch = rootView.findViewById(R.id.llSearch);
        txtContacts = rootView.findViewById(R.id.txtContacts);
        viewChatHeaderChooseNumber = mViewActionBar.findViewById(R.id.viewChatHeaderChooseNumber);
        mTvwInviteMoreNumber = mViewInviteMore.findViewById(R.id.invite_more_number_content);
        setVisibleInviteMore(View.GONE);
        initListPhoneSelected(rootView);
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
            viewChooseNumber.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            viewTextContacts.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_color_bubble_send));
            txtContacts.setTextColor(getActivity().getResources().getColor(R.color.color_tvphone));
            viewChatHeaderChooseNumber.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            mImBack.setColorFilter(getActivity().getResources().getColor(R.color.black));
            mTvTitle.setTextColor(getActivity().getResources().getColor(R.color.black));
            mImDone.setTextColor(getActivity().getResources().getColor(R.color.bg_mocha));
            llSearch.setBackgroundResource(R.drawable.chat_bg_edt_search_share_contact);
            viewTextContacts.setVisibility(View.VISIBLE);
            mSearchView.setTextColor(getActivity().getResources().getColor(R.color.color_text_term_agreement));
            mSearchView.setHintTextColor(getActivity().getResources().getColor(R.color.color_text_term_agreement));
            //mSearchView.setAlpha(0.5f);
            ivSearch.setColorFilter(getActivity().getResources().getColor(R.color.text_message_time));
            mRecyclePhoneRecent.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
            } else {
                getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
                }
            }
        }
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
            viewTextContacts.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_color_bubble_send));
            txtContacts.setTextSize(14);
            txtContacts.setTextColor(getActivity().getResources().getColor(R.color.black));
            viewTextContacts.setVisibility(View.GONE);
        }
        rootView.findViewById(R.id.iv_keyboard).setVisibility(View.GONE);
    }

    private void setVisibleInviteMore(int visible) {
        mViewInviteMore.setVisibility(visible);
        mRltInviteMore.setVisibility(visible);
    }

    private void setViewInviteMoreListener() {
        mViewInviteMore.setOnClickListener(view -> {
            String content = mTvwInviteMoreNumber.getText().toString();
            content = content.replaceAll(mRes.getString(R.string.invite_more), "");
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(mApplication.getPhoneUtil(), content, mAccountBusiness.getRegionCode());
            if (phoneNumberProtocol == null) {
                mParentActivity.showToast(mRes.getString(R.string.invite_not_phone_number), Toast.LENGTH_SHORT);
            } else {
                // lay number jid
                String jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat
                                .E164));
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                if (jidNumber != null && myNumber.equals(jidNumber)) {
                    mParentActivity.showToast(mRes.getString(R.string.invite_not_send_me), Toast.LENGTH_SHORT);
                } else if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                        phoneNumberProtocol)) {
                    mParentActivity.showToast(mRes.getString(R.string.invite_not_phone_number), Toast.LENGTH_SHORT);
                } else {
                    //add contact to invite list
                    String rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mPhoneUtil,
                            phoneNumberProtocol, mAccountBusiness.getRegionCode());
                    PhoneNumber phone = new PhoneNumber();
                    phone.setJidNumber(jidNumber);
                    phone.setRawNumber(rawNumber);
                    phone.setContactId("");
                    boolean isCheck = !phone.isChecked();
                    boolean isOk = updateAfterClick(phone, isCheck);
                    if (isOk) {
                        mAdapter.notifyDataSetChanged();
                        mSearchView.setText("");
                    } else {
                        mParentActivity.showToast(R.string.group_over_size);
                    }
                }
            }
        });
    }

    private void showOrHideItemMoreNumber(String textSearch, ArrayList<PhoneNumber> result) {
        int countSpace = TextHelper.countSpace(textSearch);
        if ((countSpace == 0) && PhoneNumberHelper.getInstant().isValidNumberNotRemoveChar(textSearch)
                && actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND) {
            mTvNote.setVisibility(View.GONE);
            if (result == null || result.isEmpty()) {
                // neu ko tim dc kq nao hien ngay option chat more
                mTvwInviteMoreNumber.setText(mRes.getString(R.string.invite_more) + textSearch);
                setVisibleInviteMore(View.VISIBLE);
            } else {
                setVisibleInviteMore(View.GONE);
            }
        } else {
            // khong hien option chat more
            setVisibleInviteMore(View.GONE);
            if ((!TextUtils.isEmpty(textSearch)) && (result == null || result.isEmpty())) {
                // khong tim thay kq
                mTvNote.setVisibility(View.VISIBLE);
            } else {
                mTvNote.setVisibility(View.GONE);
            }
        }
    }

    private void initListPhoneSelected(View rootView) {
        mListViewPhoneSelected = rootView.findViewById(R.id.listview_horizon_choose_number);
        llSelected = rootView.findViewById(R.id.llSelected);
        mTvwNoteBroadcast = rootView.findViewById(R.id.choose_number_broadcast_note);
        mTvwHideBroadcast = rootView.findViewById(R.id.choose_number_broadcast_hide);
        mTvwNoteBroadcast.setText(Html.fromHtml(getString(R.string.chat_send_the_same_content_many_people)));
        if (mListPhoneSelected == null) {
            mListPhoneSelected = new ArrayList<>();
        }

        mAdapterPhoneSelected = new PhoneNumbersSelectedAdapter(mParentActivity, this, mListPhoneSelected);
        mListViewPhoneSelected.setAdapter(mAdapterPhoneSelected);
        mAdapterPhoneSelected.notifyDataSetChanged();
        //mListViewPhoneSelected.setSelectionEnd(); // srcoll den phan tu cuoi cung cua list

        //init phone recent
        mPhoneNumberRecentAdapter = new PhoneNumberRecentAdapter(mListPhoneRecent,getContext());
        mPhoneNumberRecentAdapter.setOnItemPhoneRecentClickListener(new PhoneNumberRecentAdapter.IOnItemPhoneRecentClickListener() {
            @Override
            public void onClick(int position, PhoneNumber item) {
                chooseContact(item);
            }
        });
        mRecyclePhoneRecent.setAdapter(mPhoneNumberRecentAdapter);
        mPhoneNumberRecentAdapter.notifyDataSetChanged();
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
            llSelected.setVisibility(View.GONE);
        }
    }

    public boolean removePhoneNumberSelected(int position) {
        PhoneNumber phone = (PhoneNumber) mAdapterPhoneSelected.getItem(position);
        if (phone == null || phone.getContactId() == null) {
            return false;
        }
        if (phone.isDisable()) {
            return false;
        }
        setSelectionItemOnListViewPhoneNumbers(mListPhoneNumbers, phone);
        boolean isCheck = !phone.isChecked();
        boolean isOk = updateAfterClick(phone, isCheck);
        if (isOk) {
            mAdapter.notifyDataSetChanged();
        } else {
            // show error
            mParentActivity.showToast(R.string.group_over_size);
        }
        return mListChecked.remove(phone);
    }

    private void setSelectionItemOnListViewPhoneNumbers(ArrayList<PhoneNumber> listPhoneNumbers, PhoneNumber phone) {
        mSearchView.setText("");
        notifiChangeAdapter(listPhoneNumbers, true);
        int pos = getPositionInListPhoneNumbers(phone, listPhoneNumbers);

        try {
            if (mAdapter != null && pos >= 0 && pos < mAdapter.getItemCount())
                mRecyclerView.scrollToPosition(pos + 1);
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    private int getPositionInListPhoneNumbers(PhoneNumber phone, ArrayList<PhoneNumber> listPhones) {
        int position = -1;
        if (phone == null || listPhones == null)
            return position;

        for (int i = 0; i < listPhones.size(); i++) {
            if (listPhones.get(i).equals(phone)) {
                position = i;
                break;
            }
        }

        return position;
    }

    private void notifiChangeAdapter(ArrayList<PhoneNumber> phoneNumbers, boolean isSetList) {
        mListPhoneSearch = phoneNumbers;
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            mTvNote.setVisibility(View.VISIBLE);
        } else {
            mTvNote.setVisibility(View.GONE);
        }
        if (!isSetList) {
            mAdapter.notifyDataSetChanged();
        } else {
            mAdapter.setListPhoneNumbers(phoneNumbers);
            mHeaderAndFooterAdapter.notifyDataSetChanged();
        }
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        if (isSetList) {
            mRecyclerView.changeAdapter(mAdapter);
        }
    }

    private void getData() {
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        // lay danh sach ban be da co trong thread
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        myNumber = mAccountBusiness.getJidNumber();
        ArrayList<String> listMember = new ArrayList<>();
        if (getArguments() != null) {
            //param1 - contactId
            listMember = getArguments().getStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER);
            actionType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mThreadId = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID);
            message = (ReengMessage) getArguments().getSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE);
            mThreadGroup = mMessageBusiness.getThreadById(mThreadId);
            mRoomId = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID);
            mDeepLinkCampaignId = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN);
            mTypeScreen = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_TYPE_CREATE_GROUP_SETTING);
        }
        setMemberInThread(listMember);
    }

    private void setListener() {
        setBroadCastHintListener();
        setSearchViewListener();
        setButtonBackListener();
        setButtonDoneListener();
        setViewInviteMoreListener();
        setCBSelectAllContactListener();
    }

    private void setBroadCastHintListener() {
        mTvwNoteBroadcast.setOnClickListener(view -> hideNoteBroadcast());
        mTvwHideBroadcast.setOnClickListener(view -> hideNoteBroadcast());
    }
    private void hideNoteBroadcast(){
        mTvwNoteBroadcast.setVisibility(View.GONE);
        mTvwHideBroadcast.setVisibility(View.GONE);
    }
    private void setCBSelectAllContactListener() {
        cbSelectAllContact.setOnClickListener(v -> {
            if (isSelectAllContact) {
                unselectedAllContact();
            } else {
                selectedAllContact();
            }
            cbSelectAllContact.setSelected(!isSelectAllContact);
        });
    }

    private void selectedAllContact() {
        if (isSelectAllContact) return;
        isSelectAllContact = true;
        cbSelectAllContact.setImageResource(R.drawable.ic_checkbox_selected);
        CopyOnWriteArrayList<PhoneNumber> list = new CopyOnWriteArrayList<>(mListPhoneSearch);
        mListChecked = new ArrayList<>(list);
        setCheckboxAllContact(true);
        notifiChangeAdapter(mListPhoneSearch, true);
        mAdapterPhoneSelected.notifyDatasetChanged();
        mImDone.setEnabled(true);
        drawActionDone(true);
        initHeaderView();
    }

    private void unselectedAllContact() {
        if (!isSelectAllContact) return;
        isSelectAllContact = false;
        cbSelectAllContact.setImageResource(R.drawable.ic_checkbox);
        mListChecked = new ArrayList<>();
        mListPhoneSelected = new ArrayList<>();
        mListNumberChecked = new ArrayList<>();
        setCheckboxAllContact(false);
        notifiChangeAdapter(mListPhoneNumbers, true);
        mAdapterPhoneSelected.notifyDatasetChanged();
        mImDone.setEnabled(false);
        drawActionDone(false);
        initHeaderView();
    }

    private void setCheckboxAllContact(boolean isCheck) {
        for (PhoneNumber p : mListPhoneSearch) {
            p.setChecked(isCheck);
            if (isCheck) {
                if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
                    mListShareChecked.add(p);
                } else {
                    mListNumberChecked.add(p.getJidNumber());
                }
            }
        }
    }

    private void setButtonBackListener() {
        mImBack.setOnClickListener(view -> clickBack());
    }
    private void clickBack() {
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP && mTypeScreen == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP_SETTING) {
            mParentActivity.onBackCreateGroup();
       } else {
            mParentActivity.onBackPressed();
       }
    }
    private void setButtonDoneListener() {
        mImDone.setOnClickListener(view -> {
            InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
            switch (actionType) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                    ArrayList<String> listMembers = new ArrayList<>();
                    if (mPhoneInGroup != null) {
                        for (PhoneNumber number : mPhoneInGroup)
                            listMembers.add(number.getJidNumber());
                    }
                    listMembers.addAll(mListNumberChecked);
                    if (listMembers.size() == THREAD_PERSON_CHAT) {
                        if (mListener != null) {
                            mListener.navigateToChatIfNeed(listMembers);
                        }
                        return;
                    }
                    CreateGroupAsynctask createGroupAsynctask = new CreateGroupAsynctask();
                    createGroupAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, listMembers);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP:
                    ThreadMessage threadById = mMessageBusiness.getThreadById(mThreadId);
                    ArrayList<String> invitingMember = mListNumberChecked;
                    InviteToGroupAsyncTask inviteToGroupAsynctask = new InviteToGroupAsyncTask(invitingMember,
                            threadById);
                    inviteToGroupAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_INVITE_MAKE_TURN_LW:
                case Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND:
                    InviteFriendHelper.getInstance().inviteFriends(mApplication, mParentActivity,
                            mListNumberChecked, true);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_INVITE_NOPHOSO:
                    Log.i(TAG, "TYPE_INVITE_NOPHOSO");
                   /* InviteFriendHelper.getInstance().inviteNopHoso(mApplication, mParentActivity,
                            getFragmentManager(), mListNumberChecked, true, message);*/
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER:
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", mListNumberChecked);
                    mParentActivity.setResult(Activity.RESULT_OK, returnIntent);
                    mParentActivity.finish();
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                    ArrayList<String> listMembers2 = new ArrayList<>();
                    if (mPhoneInGroup != null) {
                        for (PhoneNumber number : mPhoneInGroup)
                            listMembers2.add(number.getJidNumber());
                    }
                    listMembers2.addAll(mListNumberChecked);
                    listMembers2.remove(myNumber);
                    if (listMembers2.size() > 0) {
                        if (listMembers2.size() == THREAD_PERSON_CHAT) {
                            if (mListener != null) {
                                mListener.navigateToChatIfNeed(listMembers2);
                            }
                            return;
                        }
                        ThreadMessage thread = mMessageBusiness.createBroadcast("",
                                mRes.getString(R.string.broadcast_name_default), listMembers2,
                                ThreadMessageConstant.STATE_SHOW, false);
                        if (mListener != null) {
                            mListener.returnCreatedThread(thread);
                        }
                    }
                    mParentActivity.trackingEvent(R.string.ga_category_home, R.string
                            .ga_action_click_new_broadcast, R.string.ga_action_click_new_broadcast);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST:
                    ThreadMessage broadcastThread = mMessageBusiness.getThreadById(mThreadId);
                    ArrayList<String> invitingMember2 = mListNumberChecked;
                    mMessageBusiness.inviteBroadcast(invitingMember2, broadcastThread);
                    if (mListener != null) {
                        mListener.notifyInviteToGroupComplete(broadcastThread);
                    }
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM:
                    InviteFriendHelper.getInstance().inviteInRoom(mApplication, mParentActivity,
                            mListNumberChecked, mRoomId, roomInviteCallback);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_SOS_MAKE_TURN_LW:
                    doLuckyWheelSosFriend(mListNumberChecked);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN:
                    if (REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId)) {
                        ArrayList<String> listNewFriends = new ArrayList<>();
                        for (String s : mListNumberChecked) {
                            if (!TextUtils.isEmpty(s) && !PrefixChangeNumberHelper.getInstant(mApplication).isSendBroadcast(s)
                                    && !s.equals(myNumber)) {
                                listNewFriends.add(s);
                            }
                        }
                        if (!listNewFriends.isEmpty()) {
                            Log.i(TAG, "doDeepLinkCampaign listNewFriends");
                            doDeepLinkCampaign(listNewFriends, mDeepLinkCampaignId);
                        } else {
                            Log.i(TAG, "doDeepLinkCampaign listNewFriends empty");
                            mParentActivity.showToast(R.string.request_success);
                            mParentActivity.onBackPressed();
                        }
                    } else
                        doDeepLinkCampaign(mListNumberChecked, mDeepLinkCampaignId);
                    break;

                case Constants.CHOOSE_CONTACT.TYPE_BROADCAST_CHANGE_NUMBER:
                    doRequestBroadcastChangeNumber(mListNumberChecked);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT:
                    mListener.returnResultShareContact(mListShareChecked);
                default:
                    break;
            }
        });
    }

    private void doRequestBroadcastChangeNumber(ArrayList<String> mListNumberChecked) {
        Log.i(TAG, "doRequestBroadcastChangeNumber: " + mListNumberChecked.size());
        AVNOHelper.getInstance(mApplication).requestBroadcastChangeNumberAVNO(mApplication, mParentActivity,
                mListNumberChecked, new AVNOHelper.BroadcastChangeNumberAVNOListener() {
                    @Override
                    public void onBroadcastSuccess(String msg) {
                        mParentActivity.showToast(msg);
                        mParentActivity.onBackPressed();
                    }

                    @Override
                    public void onBroadcastError(int errorCode, String msg) {
                        mParentActivity.showToast(msg, Toast.LENGTH_LONG);
                    }
                });
    }

    private void doLuckyWheelSosFriend(ArrayList<String> listFriends) {
        mParentActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
        LuckyWheelHelper.getInstance(mApplication).requestSosFriends(listFriends, new LuckyWheelHelper
                .SosFriendRequestListener() {
            @Override
            public void onError(int code) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

            @Override
            public void onSuccess(String desc) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                mParentActivity.onBackPressed();
            }
        });
    }

    private void doDeepLinkCampaign(final ArrayList<String> listFriends, final String campaignId) {
        InviteFriendHelper.getInstance().requestDeepLinkCampaign(mApplication, mParentActivity,
                listFriends, campaignId, new InviteFriendHelper.DeepLinkCampaignCallBack() {
                    @Override
                    public void onSuccess() {
                        mParentActivity.showToast(R.string.request_success);
                        mParentActivity.onBackPressed();
                        if (REF_PREFIX_CHANGE.equals(campaignId)) {
                            PrefixChangeNumberHelper.getInstant(mApplication).saveListNumber(listFriends);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        String msg;
                        if (errorCode == -2) {
                            msg = mRes.getString(R.string.error_internet_disconnect);
                        } else {
                            msg = mRes.getString(R.string.e601_error_but_undefined);
                        }
                        mParentActivity.showToast(msg, Toast.LENGTH_LONG);
                    }
                });
    }
    private void chooseContact(Object item){
        try {
            //  chon contact
            PhoneNumber phone = (PhoneNumber) item;
            if (phone == null || phone.getContactId() == null) {
                return;
            }
            if (phone.isDisable()) {
                return;
            }
            boolean isCheck = !phone.isChecked();
            boolean isOk = updateAfterClick(phone, isCheck);
            if (isOk) {
                mAdapter.notifyDataSetChanged();
                if (isCheck) {
                    mSearchView.selectAll();
                }
            } else {
                // show error
                mParentActivity.showToast(R.string.group_over_size);
            }
        } catch (Exception e) {
            Log.e(TAG, "setItemListViewListener", e);
        }
    }
    private void setItemListViewListener() {
        mPhoneNumberRecentAdapter.setOnItemPhoneRecentClickListener(new PhoneNumberRecentAdapter.IOnItemPhoneRecentClickListener() {
            @Override
            public void onClick(int position, PhoneNumber item) {
                chooseContact(item);
            }
        });
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                chooseContact(object);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        // hide keyboard when scroll
        mRecyclerView.addOnScrollListener(mApplication.
                getPauseOnScrollRecyclerViewListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
                    }
                }));
        // list selected
        mListViewPhoneSelected.setOnItemClickListener((parent, view, position, id) -> {
            PhoneNumber phone = (PhoneNumber) parent.getItemAtPosition(position);
            if (phone == null || phone.getContactId() == null) {
                return;
            }
            setSelectionItemOnListViewPhoneNumbers(mListPhoneNumbers, phone);
        });

        mListViewPhoneSelected.setOnItemLongClickListener((parent, view, position, id) -> {
            PhoneNumber phone = (PhoneNumber) parent.getItemAtPosition(position);
            if (phone == null || phone.getContactId() == null) {
                return false;
            }
            if (phone.isDisable()) {
                return false;
            }

            setSelectionItemOnListViewPhoneNumbers(mListPhoneNumbers, phone);

            boolean isCheck = !phone.isChecked();
            boolean isOk = updateAfterClick(phone, isCheck);
            if (isOk) {
                mAdapter.notifyDataSetChanged();
            } else {
                // show error
                mParentActivity.showToast(R.string.group_over_size);
            }

            boolean isCanRemove = mListChecked.remove(phone);
            if (isCanRemove) {
            }
            return false;
        });
    }

    private void initHeaderView() {

        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT,
                FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(1, 3, 1, 3);
        int size = 1;
        if (mListPhoneSelected == null)
            mListPhoneSelected = new ArrayList<>();
        else
            mListPhoneSelected.clear();
        if (mPhoneInGroup != null && !mPhoneInGroup.isEmpty()) {
            size = size + mPhoneInGroup.size();
            try {
                mListPhoneSelected.addAll(mPhoneInGroup);
            } catch (Exception ex) {
                Log.e(TAG, "Exception", ex);
            }
        }

        //
        boolean isEnable;
        if (mListChecked != null && !mListChecked.isEmpty()) {
            size = size + mListChecked.size();
            mListPhoneSelected.addAll(mListChecked);
            isEnable = true;
        } else {
            isEnable = actionType == Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER;
        }
        mImDone.setEnabled(isEnable);
        drawActionDone(isEnable);
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_BROADCAST_CHANGE_NUMBER ||
                (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN
                        && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId))) {
            int maxSize;
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST) {
                maxSize = Config.Message.BROAD_CAST_MAX_SIZE;
            } else {
                maxSize = mApplication.getReengAccountBusiness().isCambodia() ? Config.Message.GROUP_MAX_SIZE_CAM : Config.Message.GROUP_MAX_SIZE;
            }
            if ((actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST) && size <= 1) {
                llSelected.setVisibility(View.GONE);
                mViewSizeSelected.setVisibility(View.GONE);
                mTvwNoteBroadcast.setVisibility(View.VISIBLE);
                mTvwHideBroadcast.setVisibility(View.VISIBLE);
                mTvSizeNumberSelected.setText("");
                mImDone.setText(getString(R.string.done));
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_BROADCAST_CHANGE_NUMBER && size <= 1) {
                llSelected.setVisibility(View.GONE);
                mViewSizeSelected.setVisibility(View.GONE);
                mTvwNoteBroadcast.setVisibility(View.VISIBLE);
                mTvwHideBroadcast.setVisibility(View.VISIBLE);
                mTvwNoteBroadcast.setText(mRes.getString(R.string.msg_broadcast_change_number));
                mTvSizeNumberSelected.setText("");
                mImDone.setText(getString(R.string.next));
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN
                    && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId) && size <= 1) {
                llSelected.setVisibility(View.GONE);
                mViewSizeSelected.setVisibility(View.GONE);
                hideNoteBroadcast();
                llPrefixChange.setVisibility(View.VISIBLE);
                tvNoteBroadcastPrefixChange.setVisibility(View.VISIBLE);
                mTvSizeNumberSelected.setVisibility(View.GONE);
                mImDone.setText(getString(R.string.next));
            } else if (size > 1) {
                llSelected.setVisibility(View.VISIBLE);
                mViewSizeSelected.setVisibility(View.VISIBLE);
                hideNoteBroadcast();
                mTvSizeNumberSelected.setText((size - 1) + " "+ getString(R.string.contact));
                mImDone.setText(getString(R.string.done));
                if (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN
                        && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId)) {
                    llPrefixChange.setVisibility(View.VISIBLE);
                    tvNoteBroadcastPrefixChange.setVisibility(View.VISIBLE);
                    mTvSizeNumberSelected.setVisibility(View.GONE);
                } else {
                    llPrefixChange.setVisibility(View.GONE);
                }

            } else {
                llSelected.setVisibility(View.GONE);
                mViewSizeSelected.setVisibility(View.GONE);
                hideNoteBroadcast();
                llPrefixChange.setVisibility(View.GONE);
            }
        } else {
            hideNoteBroadcast();
            llPrefixChange.setVisibility(View.GONE);
            size = size - 1;// khong + chinh minh
            if (size > 0) {
                if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT){
                    llSelected.setVisibility(View.GONE);
                    mListViewPhoneSelected.setVisibility(View.GONE);
                    mRecyclePhoneRecent.setVisibility(View.GONE);
                    mViewSizeSelected.setVisibility(View.GONE);
                    mTvSizeNumberSelected.setVisibility(View.GONE);
                } else {
                    llSelected.setVisibility(View.VISIBLE);
                    mViewSizeSelected.setVisibility(View.VISIBLE);
                    mTvSizeNumberSelected.setText("" + size);
                }

            } else {
                llSelected.setVisibility(View.GONE);
                mViewSizeSelected.setVisibility(View.GONE);
            }
        }
        mAdapterPhoneSelected.setDatas(mListPhoneSelected);
        mAdapterPhoneSelected.setMemberThread(mMemberThread);
        Log.i(TAG, "size list: " + mListPhoneSelected.size());
        mAdapterPhoneSelected.notifyDataSetChanged();
        mListViewPhoneSelected.setSelectionEnd(); // srcoll den phan tu cuoi cung cua list
    }

    // chua qua so thanh vien group thi update view, qua thi show dialog
    private boolean updateAfterClick(PhoneNumber phoneNumber, boolean isChecked) {
        int size = 0;
        // nhieu hon 1 thanh vien (1 thanh vien la chinh minh)
        // thanh vien da co trong group
        if (mMemberThread != null) {
            size = size + mMemberThread.size();
        }
        if (mListNumberChecked == null) {
            mListNumberChecked = new ArrayList<>();
        } else if (!mListNumberChecked.isEmpty()) {
            size = size + mListNumberChecked.size();
        }

        if (!isChecked && actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN
                && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId)) {
            isSelectAllContact = false;
            cbSelectAllContact.setImageResource(R.drawable.ic_checkbox);
        }

        int maxSize = mApplication.getReengAccountBusiness().isCambodia() ? Config.Message.GROUP_MAX_SIZE_CAM : Config.Message.GROUP_MAX_SIZE;
        if ((actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM) &&
                (isChecked && size >= (maxSize - 1))) {
            return false;
        } else if ((actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST) &&
                (isChecked && size >= (Config.Message.BROAD_CAST_MAX_SIZE - 1))) {
            return false;
        } else {
            ArrayList<PhoneNumber> listPhone = mContactBusiness.getListPhoneNumberFromNumber(phoneNumber.getJidNumber
                    ());
            if (listPhone != null) {
                for (PhoneNumber item : listPhone) {
                    item.setChecked(isChecked);
                }
                if (isChecked) {
                    mListShareChecked.add(phoneNumber);
                    mListChecked.add(phoneNumber);
                    mListNumberChecked.add(phoneNumber.getJidNumber());
                } else {
                    mListShareChecked.removeAll(listPhone);
                    mListChecked.removeAll(listPhone);
                    mListNumberChecked.remove(phoneNumber.getJidNumber());
                }
            } else {
                phoneNumber.setChecked(isChecked);
                if (isChecked) {
                    mListShareChecked.add(phoneNumber);
                    mListChecked.add(phoneNumber);
                    mListNumberChecked.add(phoneNumber.getJidNumber());
                } else {
                    mListShareChecked.remove(phoneNumber);
                    mListChecked.remove(phoneNumber);
                    mListNumberChecked.remove(phoneNumber.getJidNumber());
                }
            }
            initHeaderView();
            return true;
        }
    }

    /**
     * lay danh sach contact reeng trong truong hop tao thread
     *
     * @param listChecked
     * @return
     */
    private ArrayList<PhoneNumber> getListPhoneNumberReeng(ArrayList<String> listChecked, boolean isGroupClass) {
        //        ArrayList<PhoneNumber> listAll = mContactBusiness.removeConflictPhoneNumber();
        CopyOnWriteArrayList<PhoneNumber> listAll = new CopyOnWriteArrayList<>(mContactBusiness.getListNumberAlls());
        ArrayList<PhoneNumber> listReeng = new ArrayList<>();
        if (listAll == null || listAll.isEmpty()) {
            return listReeng;
        }
        for (PhoneNumber phone : listAll) {
            // so dung reeng
            if (phone.getId() != null && (phone.isReeng() || (isGroupClass && phone.isViettel()))) {
                if (myNumber != null && phone.getJidNumber().equals(myNumber)) {
                    // so chinh minh. khong add
                } else if (mMemberThread != null && !mMemberThread.isEmpty() &&
                        mMemberThread.contains(phone.getJidNumber())) {
                    // so da co trong thread
                    if (actionType != Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER) {//block thi  disable
                        phone.setDisable(true);
                    }
                    phone.setChecked(true);
                    listReeng.add(phone);
                } else if (listChecked != null && !listChecked.isEmpty() &&
                        listChecked.contains(phone.getJidNumber())) {
                    // so chua co trong thread da duoc chon
                    phone.setDisable(false);
                    phone.setChecked(true);
                    listReeng.add(phone);
                } else {
                    // so chua co trong thread va chua dc chon
                    phone.setDisable(false);
                    phone.setChecked(false);
                    listReeng.add(phone);
                }
            }
        }
        // sort oder by name
        listReeng = mContactBusiness.sortNumberByName(listReeng);
        return listReeng;
    }

    private ArrayList<PhoneNumber> getListPhoneNumberBroadcastChangeNumber(ArrayList<String> listChecked) {
        ArrayList<PhoneNumber> listAll = mContactBusiness.getListNumberAlls();
        ArrayList<PhoneNumber> listBroadcast = new ArrayList<>();
        if (listAll == null || listAll.isEmpty()) {
            return listBroadcast;
        }
        for (PhoneNumber phone : listAll) {
            // so dung reeng
            if (phone.getId() != null && (phone.isReeng() || phone.isViettel())) {
                if (myNumber != null && phone.getJidNumber().equals(myNumber)) {
                    // so chinh minh. khong add
                } else if (listChecked != null && !listChecked.isEmpty() &&
                        listChecked.contains(phone.getJidNumber())) {
                    // so chua co trong thread da duoc chon
                    phone.setDisable(false);
                    phone.setChecked(true);
                    listBroadcast.add(phone);
                } else {
                    // so chua co trong thread va chua dc chon
                    phone.setDisable(false);
                    phone.setChecked(false);
                    listBroadcast.add(phone);
                }
            }
        }

        Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
        Collections.sort(listBroadcast, comparator);
        listBroadcast = mContactBusiness.sortNumberByName(listBroadcast);
        return listBroadcast;
    }

    private ArrayList<PhoneNumber> getListPhoneNumberInviteRoom(ArrayList<String> listChecked) {
        ArrayList<PhoneNumber> listAll = new ArrayList<>(mContactBusiness.getListNumberAlls());
        ArrayList<PhoneNumber> listInviteRoom = new ArrayList<>();
        if (listAll.isEmpty()) {
            return listInviteRoom;
        }
        for (PhoneNumber phone : listAll) {
            // so dung reeng
            if (phone.getId() != null && (phone.isReeng() || phone.isViettel())) {
                if (myNumber != null && phone.getJidNumber().equals(myNumber)) {
                    // so chinh minh. khong add
                } else if (listChecked != null && !listChecked.isEmpty() &&
                        listChecked.contains(phone.getJidNumber())) {
                    // so chua co trong thread da duoc chon
                    phone.setDisable(false);
                    phone.setChecked(true);
                    listInviteRoom.add(phone);
                } else {
                    // so chua co trong thread va chua dc chon
                    phone.setDisable(false);
                    phone.setChecked(false);
                    listInviteRoom.add(phone);
                }
            }
        }

        Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
        Collections.sort(listInviteRoom, comparator);
        listInviteRoom = mContactBusiness.sortNumberByName(listInviteRoom);
        return listInviteRoom;
    }

    /**
     * lay danh sach contact khong dung reeng trong truong hop invite
     *
     * @param listChecked
     * @return
     */
    private ArrayList<PhoneNumber> getListPhoneNumber(ArrayList<String> listChecked, boolean checkNotReeng) {
        mListChecked = new ArrayList<>();
        ArrayList<PhoneNumber> listAllOld = mContactBusiness.getListNumberAlls();
        if (listAllOld == null) listAllOld = new ArrayList<>();
        CopyOnWriteArrayList<PhoneNumber> listAll = new CopyOnWriteArrayList<>(listAllOld);
        if (listAll == null || listAll.isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<PhoneNumber> listNonReeng = new ArrayList<>();
        for (PhoneNumber phone : listAll) {
            // so khong dung reeng
            if (phone.getId() != null &&
                    (!checkNotReeng || !phone.isReeng())) {
                if (listChecked != null && !listChecked.isEmpty() && listChecked.contains(phone.getJidNumber())) {
                    // so chua co trong thread da duoc chon
                    phone.setDisable(false);
                    phone.setChecked(true);
                    mListChecked.add(phone);
                }
                listNonReeng.add(phone);
            }
            /*else if (phone.getId() == null && phone.getName() != null &&
                    !phone.getName().contains(rowMochaSupport)) {
                listNonReeng.add(phone);
            }*/
        }
        Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
        Collections.sort(listNonReeng, comparator);
        listNonReeng = mContactBusiness.sortNumberByName(listNonReeng);
        // sort oder by name
        return listNonReeng;
    }

    private HashSet<PhoneNumber> getListNumberInGroup(ArrayList<PhoneNumber> listReengs) {
        ArrayList<PhoneNumber> listMemberDefault = mContactBusiness.createListPhoneNumberDefault(mMemberThread);
        ArrayList<PhoneNumber> listChecked = new ArrayList<>();
        if (listMemberDefault == null || listMemberDefault.isEmpty()) {
            return null;
        }
        ArrayList<PhoneNumber> listMember = new ArrayList<>();
        if (listReengs == null || listReengs.isEmpty()) {
            return new HashSet<>(listMemberDefault);
        }
        for (PhoneNumber phone : listReengs) {
            if (phone.getContactId() != null && phone.isChecked() && phone.isDisable()
                    && !containsNumber(listMember, phone)) {
                listMember.add(phone);
            } else if (phone.getContactId() != null && phone.isChecked() &&
                    !phone.isDisable() && !containsNumber(listChecked, phone)) {
                listChecked.add(phone);
            }
        }
        if (listMember.isEmpty()) {
            return new HashSet<>(listMemberDefault);
        }
        listMemberDefault = removeAllNumberDuplicate(listMemberDefault, listMember);
        if (listMemberDefault != null) {
            listMember.addAll(listMemberDefault);
        }
        mListChecked = listChecked;
        return new HashSet<>(listMember);
    }

    private boolean containsNumber(ArrayList<PhoneNumber> list, PhoneNumber phone) {
        if (list == null || list.isEmpty()) {
            return false;
        }
        for (PhoneNumber item : list) {
            if (item.getJidNumber().equals(phone.getJidNumber())) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<PhoneNumber> removeAllNumberDuplicate(ArrayList<PhoneNumber> listDefault,
                                                            ArrayList<PhoneNumber> listMember) {
        ArrayList<PhoneNumber> list = new ArrayList<>();
        for (PhoneNumber itemDefault : listDefault)
            for (PhoneNumber itemMember : listMember) {
                if (itemDefault.getJidNumber().equals(itemMember.getJidNumber())) {
                    list.add(itemDefault);
                }
            }
        listDefault.removeAll(list);
        return listDefault;
    }

    private void getListPhoneAsynctask() {
        if (mGetListAsyncTask != null) {
            mGetListAsyncTask.cancel(true);
            mGetListAsyncTask = null;
        }
        mGetListAsyncTask = new GetListPhoneNumberAsyncTask();
        mGetListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * search Contact
     */
    private void setSearchViewListener() {
        rootView.clearFocus();
        mSearchView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    ivSearch.setVisibility(View.GONE);
                    mSearchView.setText(getString(R.string.sms_to));
                    searchContactAsyncTask("");
                }else {
                    mSearchView.setHint(getString(R.string.hint_enter_name_or_phone));
                    ivSearch.setVisibility(View.VISIBLE);
                    searchContactAsyncTask("");
                    KeyboardUtils.hideSoftInput(getActivity());
                }
            }
        });
        mSearchView.setDrawableClickListener(new ReengSearchView.DrawableClickListener() {
            @Override
            public void onClick(DrawablePosition target) {
                if(target == DrawablePosition.RIGHT){
                    mSearchView.setHint(getString(R.string.hint_enter_name_or_phone));
                    ivSearch.setVisibility(View.VISIBLE);
                    KeyboardUtils.hideSoftInput(getActivity());
                    rootView.clearFocus();
                }
            }

            @Override
            public void onBackClick(int keyCode) {

            }
        });
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(ivSearch.getVisibility() == View.VISIBLE){
                    return;
                }
                if(!s.toString().startsWith(getString(R.string.sms_to))){
                    mSearchView.setText(getString(R.string.sms_to));
                    Selection.setSelection(mSearchView.getText(), mSearchView.getText().length());
                }else if (!TextUtils.isEmpty(s.toString().trim())) {
                    searchContactAsyncTask((s.toString()+ " ").split(":")[1].trim());
                } else {
                    notifiChangeAdapter(mListPhoneNumbers, true);
                }
            }
        });

        mSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
                if (keyCode == EditorInfo.IME_ACTION_DONE) {
                    InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
                }
                return false;
            }
        });
//        mSearchView.setOnTouchListener((view, motionEvent) -> {
//            // assus zenphone
//            InputMethodUtils.showSoftKeyboard(mParentActivity, mSearchView);
//            return false;
//        });
    }

    // new searchAsynctask
    private void searchContactAsyncTask(String textSearch) {
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
            if (mListContactPhoneNumbers == null) return;
        } else {
            if (mListPhoneNumbers == null) return;
        }
        mSearchTask = new SearchContactTask(mApplication);
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
            mSearchTask.setDatas(mListContactPhoneNumbers);
        } else {
            mSearchTask.setDatas(mListPhoneNumbers);
        }
        mSearchTask.setComparatorKeySearch(SearchUtils.getComparatorKeySearchForSearch());
        mSearchTask.setComparatorName(SearchUtils.getComparatorNameForSearch());
        mSearchTask.setComparatorContact(SearchUtils.getComparatorContactForSearch());
        mSearchTask.setComparatorMessage(SearchUtils.getComparatorMessageForSearch());
        mSearchTask.setListener(new SearchContactsListener() {
            @Override
            public void onPrepareSearchContact() {

            }

            @Override
            public void onFinishedSearchContact(String keySearch, ArrayList<PhoneNumber> list) {
                notifiChangeAdapter(list, true);
                showOrHideItemMoreNumber(keySearch, list);
            }
        });
        mSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
    }

    @Override
    public void onSyncCompleted() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getListPhoneAsynctask();
            }
        });
    }

    private void drawActionDone(boolean isEnable) {
//        if (actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP ||
//                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST ||
//                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP ||
//                actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
//                actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM) {
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT){
            mImDone.setTextColor(getResources().getColor(R.color.bg_button_red));
        } else {
            mImDone.setTextColor(isEnable ? getResources().getColor(R.color.white) :
                    getResources().getColor(R.color.white_50));
        }
//        } else {
//            mImDone.setImageResource(isEnable ? R.drawable.ic_ab_action_accept : R.drawable
//                    .ic_ab_action_accept_disable);
//        }

    }

    private void setClickDetail() {
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId)) {
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                    ds.setUnderlineText(true);    // this remove the underline
                }

                @Override
                public void onClick(View textView) {
                    DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://survey?ref=http://mocha.com.vn/hdchuyenso.html");
                }
            };
            String textNormal = mRes.getString(R.string.broadcast_prefix_change);
            String textToSpan = mRes.getString(R.string.detail);
            String textToShow = textNormal + textToSpan;
            SpannableString spannableString = new SpannableString(textToShow);
            int START_TEXT = textNormal.length();
            int END_TEXT = textToShow.length();
            spannableString.setSpan(clickableSpan, START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mParentActivity, R.color
                            .bg_mocha)),
                    START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvNoteBroadcastPrefixChange.setText(spannableString);
            tvNoteBroadcastPrefixChange.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    public interface OnFragmentInteractionListener {
        void notifyInviteToGroupComplete(ThreadMessage invitingGroup);

        void returnCreatedThread(ThreadMessage threadMessage);

        void navigateToChatIfNeed(ArrayList<String> listMembers2);

        void returnRoomInvite();

        void returnResultShareContact(ArrayList<PhoneNumber> listPhone);
    }

    private interface SearchContactsListener {
        void onPrepareSearchContact();

        void onFinishedSearchContact(String keySearch, ArrayList<PhoneNumber> list);
    }

    private static class SearchContactTask extends AsyncTask<String, Void, ArrayList<PhoneNumber>> {
        private final String TAG = "SearchContactTask";
        private WeakReference<ApplicationController> application;
        private SearchContactsListener listener;
        private String keySearch;
        private CopyOnWriteArrayList<PhoneNumber> datas;
        private Comparator comparatorName;
        private Comparator comparatorKeySearch;
        private Comparator comparatorContact;
        private Comparator comparatorMessage;
        private long startTime;
        private int totalDatas;

        public SearchContactTask(ApplicationController application) {
            this.application = new WeakReference<>(application);
            this.datas = new CopyOnWriteArrayList<>();
        }

        public void setListener(SearchContactsListener listener) {
            this.listener = listener;
        }

        public void setDatas(ArrayList<PhoneNumber> datas) {
            if (this.datas != null && datas != null)
                this.datas.addAll(datas);
        }

        public void setComparatorName(Comparator comparatorName) {
            this.comparatorName = comparatorName;
        }

        public void setComparatorKeySearch(Comparator comparatorKeySearch) {
            this.comparatorKeySearch = comparatorKeySearch;
        }

        public void setComparatorContact(Comparator comparatorContact) {
            this.comparatorContact = comparatorContact;
        }

        public void setComparatorMessage(Comparator comparatorMessage) {
            this.comparatorMessage = comparatorMessage;
        }

        @Override
        protected void onPreExecute() {
            if (listener != null) listener.onPrepareSearchContact();
        }

        @Override
        protected void onPostExecute(ArrayList<PhoneNumber> result) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                        + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
            if (listener != null) listener.onFinishedSearchContact(keySearch, result);
        }

        @Override
        protected ArrayList<PhoneNumber> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            totalDatas = 0;
            ArrayList<PhoneNumber> list = new ArrayList<>();
            keySearch = params[0];
            if (Utilities.notNull(application) && Utilities.notEmpty(datas)) {
                totalDatas = datas.size();
                ResultSearchContact result = SearchUtils.searchMessagesAndContacts(application.get()
                        , keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage, comparatorContact);
                if (result != null) {
                    keySearch = result.getKeySearch();
                    if (Utilities.notEmpty(result.getResult())) {
                        for (Object obj : result.getResult()) {
                            if (obj instanceof PhoneNumber) {
                                list.add((PhoneNumber) obj);
                            }
                        }
                    }
                }
            }
            return list;
        }
    }

    private class GetListPhoneNumberAsyncTask extends AsyncTask<Void, Void, ArrayList<PhoneNumber>> {
        @Override
        protected void onPreExecute() {
            mPbLoading.setVisibility(View.VISIBLE);
            mPbLoading.setEnabled(true);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<PhoneNumber> doInBackground(Void... params) {
            ArrayList<PhoneNumber> numbers = new ArrayList<>();
            if (mContactBusiness.getListContacs().isEmpty() || mContactBusiness.getListNumbers().isEmpty()) {
                return new ArrayList<>();
            }
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_SOS_MAKE_TURN_LW) {
                if (mListNumberChecked == null || mListNumberChecked.isEmpty()) {
                    mListNumberChecked = new ArrayList<>();
                }
                numbers = getListPhoneNumberInviteRoom(mListNumberChecked);
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST) {
                boolean isClass;
                if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                        actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST) {
                    isClass = mApplication.getReengAccountBusiness().isViettel();
                } else {
                    isClass = mThreadGroup != null && mThreadGroup.getGroupClass() == ThreadMessageConstant
                            .GROUP_TYPE_CLASS;
                }
                // chon contact tao group
                numbers = getListPhoneNumberReeng(mListNumberChecked, isClass);
                mPhoneInGroup = getListNumberInGroup(numbers);
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN) {
                /*if (REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId)) {
                    CopyOnWriteArrayList<PhoneNumber> listAll = new CopyOnWriteArrayList<>(mContactBusiness.getListNumberAlls());
                    for (PhoneNumber p : listAll) {
                        if (!TextUtils.isEmpty(p.getId())) {
                            numbers.add(p);
                        }
                    }
                } else {*/
                numbers = getListPhoneNumberReeng(mListNumberChecked, true);
                mPhoneInGroup = getListNumberInGroup(numbers);
//                }
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_MAKE_TURN_LW) {
                // chon contact de moi qua sms
                if (mListNumberChecked == null || mListNumberChecked.isEmpty()) {
                    mListNumberChecked = new ArrayList<>();
                    mListNumberChecked.addAll(mMemberThread);
                } else if (mMemberThread != null) {
                    for (String numberSelected : mMemberThread) {
                        if (!mListNumberChecked.contains(numberSelected)) {
                            mListNumberChecked.add(numberSelected);
                        }
                    }
                }
                numbers = getListPhoneNumber(mListNumberChecked, true);
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_INVITE_NOPHOSO) {
                if (mListNumberChecked == null || mListNumberChecked.isEmpty()) {
                    mListNumberChecked = new ArrayList<>();
                    mListNumberChecked.addAll(mMemberThread);
                } else if (mMemberThread != null) {
                    for (String numberSelected : mMemberThread) {
                        if (!mListNumberChecked.contains(numberSelected)) {
                            mListNumberChecked.add(numberSelected);
                        }
                    }
                }
                numbers = getListPhoneNumber(mListNumberChecked, false);
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER) {
                // get non-mocha number
                if (mListNumberChecked == null || mListNumberChecked.isEmpty()) {
                    mListNumberChecked = new ArrayList<>();
                    mListNumberChecked.addAll(mMemberThread);
                } else if (mMemberThread != null) {
                    //get mocha number
                    for (String numberSelected : mMemberThread) {
                        if (!mListNumberChecked.contains(numberSelected)) {
                            mListNumberChecked.add(numberSelected);
                        }
                    }
                }
                numbers = getListPhoneNumber(mListNumberChecked, false);
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_BROADCAST_CHANGE_NUMBER) {
                if (mListNumberChecked == null || mListNumberChecked.isEmpty()) {
                    mListNumberChecked = new ArrayList<>();
                }
                numbers = getListPhoneNumberBroadcastChangeNumber(mListNumberChecked);
            }
            ArrayList<PhoneNumber> result = new ArrayList<>();
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
                //add recent
                //mListPhoneRecent = new ArrayList<>();

                mParentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<PhoneNumber> list = new ArrayList<>();
                        List<PhoneNumber> listRecent = new ArrayList<>();
                        list = mContactBusiness.getRecentPhoneNumber();
                        if (Utilities.notEmpty(list)) {
                            mListPhoneRecent.clear();
                            mTvRecent.setVisibility(View.VISIBLE);
                            mRecyclePhoneRecent.setVisibility(View.VISIBLE);
                            listRecent = removePhoneRecentDuplicates(list);
                            mListPhoneRecent = listRecent;
                            mPhoneNumberRecentAdapter = new PhoneNumberRecentAdapter(mListPhoneRecent,getContext());
                            mPhoneNumberRecentAdapter.setOnItemPhoneRecentClickListener(new PhoneNumberRecentAdapter.IOnItemPhoneRecentClickListener() {
                                @Override
                                public void onClick(int position, PhoneNumber item) {
                                    chooseContact(item);
                                }
                            });
                            mRecyclePhoneRecent.setAdapter(mPhoneNumberRecentAdapter);
                            mPhoneNumberRecentAdapter.notifyDataSetChanged();
                        } else {
                            mTvRecent.setVisibility(View.GONE);
                            mRecyclePhoneRecent.setVisibility(View.GONE);
                        }
                    }
                });

                //add favorite
                ArrayList<PhoneNumber> favoriteList = mContactBusiness.getListNumberFavorites();
//                if (Utilities.notEmpty(favoriteList) && actionType != Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
//                    PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.list_favorite), -1);
//                    result.add(sectionContacts);
//                    result.addAll(favoriteList);
//                    PhoneNumber divider = new PhoneNumber(null, null, -4);
//                    result.add(divider);
//                }
                if (Utilities.notEmpty(numbers)) {
                    if (actionType != Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
                        PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), -1, true);
                        result.add(sectionContacts);
                    }
                    result.addAll(numbers);
                    mListContactPhoneNumbers = numbers;
                }
                if (actionType != Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
                    viewTextContacts.setVisibility(View.GONE);
                }
                return result;
            }
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
                mListSectionChar = mContactBusiness.getListSectionCharAlls();
                numbers = mContactBusiness.getListNumberAlls();
                if (numbers.get(0).getName().equals(mRes.getString(R.string.menu_contacts))){
                    numbers.remove(0);
                }
            }
            //add contact
            if (Utilities.notEmpty(numbers) && actionType != Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT && actionType != Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP) {
                PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), -1, true);
                result.add(sectionContacts);
            }
            result.addAll(numbers);
            return result;
        }
        public List<PhoneNumber>  removePhoneRecentDuplicates(List<PhoneNumber> list){
            Set<PhoneNumber> set = new TreeSet(new Comparator<PhoneNumber>() {

                @Override
                public int compare(PhoneNumber o1, PhoneNumber o2) {
                    if(o1.getJidNumber().equalsIgnoreCase(o2.getJidNumber())){
                        return 0;
                    }
                    return 1;
                }
            });
            set.addAll(list);

            final ArrayList newList = new ArrayList(set);
            return newList;
        }
        @Override
        protected void onPostExecute(ArrayList<PhoneNumber> params) {
            mPbLoading.setVisibility(View.GONE);
            mPbLoading.setEnabled(false);
            mListPhoneNumbers = params;
            if (mListPhoneNumbers == null || mListPhoneNumbers.isEmpty()) {
                mTvNote.setText(mRes.getString(R.string.list_empty));
            } else {
                mTvNote.setText(mRes.getString(R.string.not_find));
            }
            //kiem tra search box xem da nhap text chua
            String contentSearch = "";
            if(mSearchView.getText().toString().contains(":")){
                contentSearch = (mSearchView.getText().toString()+" ").split(":")[1].trim();
            }
            if (!TextUtils.isEmpty(contentSearch)) {
                searchContactAsyncTask(contentSearch);
            } else {
                notifiChangeAdapter(mListPhoneNumbers, true);
            }
            initHeaderView();
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN
                    && REF_PREFIX_CHANGE.equals(mDeepLinkCampaignId))
                selectedAllContact();
            super.onPostExecute(params);
        }
    }

    private class InviteToGroupAsyncTask extends AsyncTask<Void, Void, XMPPResponseCode> {
        ArrayList<String> invitingMembers;
        ThreadMessage invitingGroup;
        private String title = mRes.getString(R.string.title_activity_choose_number);

        public InviteToGroupAsyncTask(ArrayList<String> invitingMembers, ThreadMessage group) {
            this.invitingMembers = invitingMembers;
            this.invitingGroup = group;
        }

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(title, mRes.getString(R.string.processing));
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(Void... params) {
            return mMessageBusiness.inviteGroup(invitingMembers, invitingGroup);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mParentActivity.hideLoadingDialog();
            if (params != null) {
                int responseCode = params.getCode();
                if (responseCode == XMPPCode.E200_OK) {
                    if (mListener != null) {
                        mListener.notifyInviteToGroupComplete(invitingGroup);
                    }
                } else {
                    mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
                }
            }
            super.onPostExecute(params);
        }
    }

    // create group, invite group, invite friend
    private class CreateGroupAsynctask extends AsyncTask<ArrayList<String>, Void, XMPPResponseCode> {
        private String title = mRes.getString(R.string.title_create_group);

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(title, mRes.getString(R.string.processing));
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(ArrayList... params) {
            ArrayList<String> listMember = params[0];
            listMember.remove(myNumber);
            //return mMessageBusiness.createGroup(mRes.getString(R.string.group_name_default), listMember);
            XMPPResponseCode createXmppCode = mMessageBusiness.createGroup(mRes.getString(R.string
                    .group_name_default), listMember);
            return createXmppCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mParentActivity.hideLoadingDialog();
            if (params != null) {
                int responseCode = params.getCode();
                if (responseCode == XMPPCode.E200_OK) {
                    ThreadMessage thread = (ThreadMessage) params.getContain();
                    if (mListener != null) {
                        mListener.returnCreatedThread(thread);
                    }
                } else {
                    mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
                }
            }
            super.onPostExecute(params);
        }
    }
}
package com.metfone.selfcare.fragment.contact;

import android.Manifest;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.adapter.PhoneNumberAdapter;
import com.metfone.selfcare.adapter.PhoneNumberRecyclerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.ContactListInterface;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OfficerAccountChangeListener;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.indexable.IndexableRecyclerView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 6/27/14.
 */
public class ContactListFragment extends Fragment implements ClickListener.IconListener,
        ContactChangeListener, InitDataListener, OfficerAccountChangeListener {
    private static final String TAG = ContactListFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private ContactBusiness mContactBusiness;
    private ClickListener.IconListener mClickHandler;
    private Handler mHandler;
    private Resources mRes;
    private ContactListInterface mListener;
    private ContactListActivity mParentActivity;
    private View rootView, abView, mViewAbDetail, mViewAbSearch;//, mViewHeader;
    //private View mHeaderStrangerList;
    private IndexableRecyclerView mRecyclerView;
    private TextView mTvNoteView;
    private EllipsisTextView mTvTitle;
    private ProgressLoading mProgressLoading;
    private PhoneNumberRecyclerAdapter mHeaderAndFooterAdapter;
    private PhoneNumberAdapter mAdapter;
    private ArrayList<PhoneNumber> mListAlls;
    private ArrayList<Object> mListObj;
    private GetContactListAsyncTask mGetContactAsyncTask;
    private ImageView mImgSearch, mImgMore, mImgSearchBack, mImgBack;
    private ReengSearchView mReengSearchView;
    private SearchContactTask mSearchTask;
    private ArrayList<ItemContextMenu> listItemOverFlow;
    private String myNumber;
    private boolean runSearch = true;
    private ReengAccountBusiness mAccountBusiness;

    public ContactListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactListFragment.
     */
    public static ContactListFragment newInstance() {
        Log.i(TAG, "ContactListFragment newInstance ...");
        ContactListFragment fragment = new ContactListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ContactListActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (ContactListInterface) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mRes = mParentActivity.getResources();
        mContactBusiness = mApplication.getContactBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        myNumber = mAccountBusiness.getJidNumber();
        initOverFlowMenu();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "ContactListFragment onCreate ...");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        rootView = inflater.inflate(R.layout.fragment_contact_list, container, false);
        findComponentViews(rootView, container, inflater);
        setViewListeners();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "OnResume");
        ListenerHelper.getInstance().addInitDataListener(this);
        ListenerHelper.getInstance().addOfficerAccountListener(this);
        mHandler = new Handler();
        if (mApplication.isDataReady()) {
            ListenerHelper.getInstance().addContactChangeListener(this);
            mClickHandler = this;
            reloadContactAsyncTask();
        } else {
            mProgressLoading.setVisibility(View.VISIBLE);
            mProgressLoading.setEnabled(true);
        }
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        mClickHandler = null;
        ListenerHelper.getInstance().removeContactChangeListener(this);
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeOfficerAcountListener(this);
        if (mGetContactAsyncTask != null) {
            mGetContactAsyncTask.cancel(true);
            mGetContactAsyncTask = null;
        }
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        mHandler = null;
        super.onPause();
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        mAdapter = null;
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDataReady() {
        Log.d(TAG, "load data complate");
        if (mHandler == null) {
            return;
        }
        ListenerHelper.getInstance().addContactChangeListener(this);
        mClickHandler = this;
        mHandler.post(() -> reloadContactAsyncTask());
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        Log.d(TAG, "onContactChange");
        mHandler.post(() -> reloadContactAsyncTask());
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        Log.d(TAG, "presence change: ");
        mHandler.post(() -> {
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void onStickyOfficerChange() {
        Log.d(TAG, "onStickyOfficerChange");
        mHandler.post(() -> reloadContactAsyncTask());
    }

    @Override
    public void onIconClickListener(View view, Object entry, int arg) {
        switch (arg) {
            case Constants.MENU.VIEW_CONTATCT_DETAIL:
                PhoneNumber mPhone = (PhoneNumber) entry;
                // an search view khi click vao 1 item
                showOrHideSearchView(false, false);
                InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
                if (myNumber != null && myNumber.equals(mPhone.getJidNumber())) {
                    NavigateActivityHelper.navigateToMyProfile(mParentActivity);
                } else {
                    NavigateActivityHelper.navigateToContactDetail(mParentActivity, mPhone);
                }
                break;
            case Constants.MENU.DELETE:
                deleteContact(((PhoneNumber) entry).getContactId());
                break;
            case Constants.MENU.EDIT:
                // an search view khi click vao 1 item
                showOrHideSearchView(false, false);
                mListener.editContactActivity(((PhoneNumber) entry).getContactId());
                break;
            case Constants.MENU.ADD_FAV:
                mContactBusiness.changeFavorite(((PhoneNumber) entry).getContactId(), true);
                break;
            case Constants.MENU.DEL_FAV:
                mContactBusiness.changeFavorite(((PhoneNumber) entry).getContactId(), false);
                break;
            case Constants.MENU.ADD_CONTACT:
                mListener.addNewContactActivity();
                break;
            case Constants.MENU.MENU_ADD_FAV:
                mListener.navigateToAddFavarite();
                break;
            case Constants.MENU.MENU_ADD_GROUP:
                mParentActivity.showToast(mRes.getString(R.string.e666_not_support_function), Toast.LENGTH_SHORT);
                break;
            case Constants.MENU.POPUP_YES:
                DeleteContactAsyncTask deleteAsynctask = new DeleteContactAsyncTask();
                deleteAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (String) entry);
                break;
            case Constants.ACTION.HOLDER_INVITE_FRIEND:
                mListener.navigateToInviteFriendActivity(((PhoneNumber) entry).getJidNumber());
                break;
            case Constants.MENU.INVITE_FRIEND:
                mListener.navigateToInviteFriendActivity("");
                break;
            /*case Constants.MENU.POPUP_CONFIRM_REGISTER_VIP:
                handleRegisterVip(mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                        .REGISTER_VIP_CMD));
                break;*/
            default:
                break;
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        abView = mParentActivity.getToolBarView();
        ((Toolbar) abView).removeAllViews();
        View content = inflater.inflate(R.layout.ab_contacts, null);
        ((Toolbar) abView).addView(content);

        mImgBack = abView.findViewById(R.id.ab_back_btn);
        mImgMore = abView.findViewById(R.id.ab_more_btn);
        mImgSearch = abView.findViewById(R.id.ab_search_btn);
        mTvTitle = abView.findViewById(R.id.ab_title);
        mImgSearchBack = abView.findViewById(R.id.ab_search_back);
        mViewAbDetail = abView.findViewById(R.id.ab_detail_ll);
        mViewAbSearch = abView.findViewById(R.id.ab_search_ll);
        mReengSearchView = abView.findViewById(R.id.ab_search_view);
        mTvTitle.setText(mRes.getString(R.string.title_contact_list));
        mImgSearch.setVisibility(View.VISIBLE);

        mRecyclerView = rootView.findViewById(R.id.fragment_contact_listview);
        mTvNoteView = rootView.findViewById(R.id.fragment_contact_note_text);
        mProgressLoading = rootView.findViewById(R.id.fragment_contact_loading_progressbar);

        mTvNoteView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
        showOrHideSearchView(false, true);
    }

    private void setViewListeners() {
        setAbBackListener();
        setButtonSearchListener();
        setButtonMoreListener();
        setButtonBackSearchListener();
        setReengSearchViewListener();
    }

    public boolean backPressListener() {
        if (mViewAbSearch.getVisibility() == View.VISIBLE) {
            InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
            showOrHideSearchView(false, true);
            return true;
        } else {
            return false;
        }
    }

    private void showOrHideSearchView(boolean show, boolean runSearch) {
        this.runSearch = runSearch;
        mReengSearchView.setText("");
        if (show) {
            mViewAbSearch.setVisibility(View.VISIBLE);
            mViewAbDetail.setVisibility(View.GONE);
            mTvNoteView.setText(mRes.getString(R.string.not_find));
        } else {
            mViewAbSearch.setVisibility(View.GONE);
            mViewAbDetail.setVisibility(View.VISIBLE);
            mTvNoteView.setText(mRes.getString(R.string.list_empty));
        }
    }

    private void setButtonBackSearchListener() {
        mImgSearchBack.setOnClickListener(view -> {
            InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
            showOrHideSearchView(false, true);
        });
    }

    /**
     * search Contact
     */
    private void setReengSearchViewListener() {
        mReengSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String content = s.toString();
                mRecyclerView.setHideFastScroll(!TextUtils.isEmpty(content));
                if (runSearch) {
                    searchContactAsyncTask(content);
                }
            }
        });

        mReengSearchView.setOnEditorActionListener((textView, keyCode, keyEvent) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
            }
            return false;
        });
        mReengSearchView.setOnTouchListener((view, motionEvent) -> {
            // assus zenphone
            //mSearchView.clearFocus();
            InputMethodUtils.showSoftKeyboard(mParentActivity, mReengSearchView);
            return false;
        });
    }

    private void handleRegisterVip(String id) {
        if (!TextUtils.isEmpty(id)) {
            ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, mParentActivity, null, id, "contacts");
        }
    }

    private void initOverFlowMenu() {
        listItemOverFlow = new ArrayList<>();
        ItemContextMenu addContact = new ItemContextMenu(mRes.getString(R.string.add_new_contact),
                -1, null, Constants.MENU.ADD_CONTACT);
        listItemOverFlow.add(addContact);
        ///
        ItemContextMenu inviteFriend = new ItemContextMenu(mRes.getString(R.string.title_invite_friend),
                -1, null, Constants.MENU.INVITE_FRIEND);
        listItemOverFlow.add(inviteFriend);
    }

    // new searchAsynctask
    private void searchContactAsyncTask(String textSearch) {
        Log.d(TAG, "searchContactAsynctask ");
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        if (mListObj == null) {
            return;
        }
        mSearchTask = new SearchContactTask(mApplication);
        mSearchTask.setDatas(mListObj);
        mSearchTask.setComparatorKeySearch(SearchUtils.getComparatorKeySearchForSearch());
        mSearchTask.setComparatorName(SearchUtils.getComparatorNameForSearch());
        mSearchTask.setComparatorContact(SearchUtils.getComparatorContactForSearch());
        mSearchTask.setComparatorMessage(SearchUtils.getComparatorMessageForSearch());
        mSearchTask.setListener(new SearchContactsListener() {
            @Override
            public void onPrepareSearchContact() {

            }

            @Override
            public void onFinishedSearchContact(String keySearch, ArrayList<Object> list) {
                notifiChangeAdapter(list);
            }
        });
        mSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
    }

    private void setAbBackListener() {
        mImgBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }

    private void setButtonSearchListener() {
        mImgSearch.setOnClickListener(view -> {
            showOrHideSearchView(true, true);
            mReengSearchView.requestFocus();
            mReengSearchView.setSelection(0);
            InputMethodUtils.showSoftKeyboard(mParentActivity, mReengSearchView);
        });
    }

    private void setButtonMoreListener() {
        mImgMore.setOnClickListener(view -> PopupHelper.getInstance().
                showOrHideOverFlowMenu(mImgMore, mClickHandler, listItemOverFlow));
    }

    private void reloadContactAsyncTask() {
        if (mGetContactAsyncTask != null) {
            mGetContactAsyncTask.cancel(true);
            mGetContactAsyncTask = null;
        }
        mGetContactAsyncTask = new GetContactListAsyncTask();
        mGetContactAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setAdapter(ArrayList<Object> objects) {
        Log.i(TAG, "setAdapter ... ");
        if (objects == null || objects.isEmpty()) {
            mTvNoteView.setVisibility(View.VISIBLE);
        } else {
            mTvNoteView.setVisibility(View.GONE);
        }
        mAdapter = new PhoneNumberAdapter(mApplication, objects,
                this, Constants.CONTACT.CONTACT_VIEW_NOMAL, true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mHeaderAndFooterAdapter = new PhoneNumberRecyclerAdapter(mAdapter, mApplication);
        mHeaderAndFooterAdapter.setListSectionCharecter(mContactBusiness.getListSectionCharAlls());
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        setItemListViewListener();
    }

    private void notifiChangeAdapter(ArrayList<Object> objects) {
        if (objects == null || objects.isEmpty()) {
            mTvNoteView.setVisibility(View.VISIBLE);
        } else {
            mTvNoteView.setVisibility(View.GONE);
        }
        if (mAdapter == null) {
            setAdapter(objects);
            return;
        }
        mAdapter.setListObjects(objects);
        mHeaderAndFooterAdapter.setListSectionCharecter(mContactBusiness.getListSectionCharAlls());
        mHeaderAndFooterAdapter.notifyDataSetChanged();
        mRecyclerView.changeAdapter(mHeaderAndFooterAdapter);
    }

    private void setItemListViewListener() {
        // on touch
        mRecyclerView.setOnTouchListener((v, event) -> {
            InputMethodUtils.hideSoftKeyboard(mParentActivity);
            return false;
        });
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) object;
                    if (phone.getContactId() == null) {
                        return;
                    }
                    ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                    if (myNumber != null && myNumber.equals(phone.getJidNumber())) {
                        mParentActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                    } else if (phone.isReeng()
                            || (mApplication.getReengAccountBusiness().isViettel() && phone.isViettel())) {// so co su dung hoac acc viettel, friend viettel
                        // an search view khi click vao 1 item
                        showOrHideSearchView(false, false);
                        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(phone.getJidNumber());
                        mListener.navigateToThreadDetail(thread);
                    } else {
                        InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, mParentActivity,
                                phone.getName(), phone.getJidNumber(), false);
                    }
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {
                if (object instanceof PhoneNumber) {
                    PhoneNumber entry = (PhoneNumber) object;
                    if (entry.getContactId() != null) {
                        PopupHelper.getInstance().showContextMenu(mParentActivity,
                                entry.getName(), getListItemContextMenu(entry), mClickHandler);
                    }
                }
            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private ArrayList<ItemContextMenu> getListItemContextMenu(PhoneNumber phone) {
        ItemContextMenu deleteItem = new ItemContextMenu(mRes.getString(R.string.del_contact), -1, phone, Constants.MENU.DELETE);
        ItemContextMenu editItem = new ItemContextMenu(mRes.getString(R.string.edit_contact), -1, phone, Constants.MENU.EDIT);
        ArrayList<ItemContextMenu> list = new ArrayList<>();
        list.add(editItem);
        list.add(deleteItem);
        return list;
    }

    private void drawListView(ArrayList<Object> objects) {
        String contentSearch = mReengSearchView.getText().toString();
        if (mViewAbSearch.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(contentSearch.trim())) {
            searchContactAsyncTask(contentSearch.trim());
        } else {
            if (mAdapter == null) {
                setAdapter(objects);
            } else {
                notifiChangeAdapter(objects);
            }
        }
    }

    private void deleteContact(final String id) {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(mParentActivity,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_DELETE_CONTACT);
        } else {
            String labelOK = mRes.getString(R.string.ok);
            String labelCancel = mRes.getString(R.string.cancel);
            String title = mRes.getString(R.string.title_delete_contact);
            String msg = mRes.getString(R.string.msg_delete_contact);
            PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                    msg, labelOK, labelCancel, mClickHandler, id, Constants.MENU.POPUP_YES);
        }
    }

    /*private void confirmRegisterVip() {
        String labelOK = mRes.getString(R.string.register);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.REGISTER_VIP_BUTTON);
        String msg = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.REGISTER_VIP_CONFIRM);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, null, Constants.MENU.POPUP_CONFIRM_REGISTER_VIP);
    }*/

    private interface SearchContactsListener {
        void onPrepareSearchContact();

        void onFinishedSearchContact(String keySearch, ArrayList<Object> list);
    }

    private static class SearchContactTask extends AsyncTask<String, Void, ArrayList<Object>> {
        private final String TAG = "SearchContactTask";
        private WeakReference<ApplicationController> application;
        private SearchContactsListener listener;
        private String keySearch;
        private CopyOnWriteArrayList<Object> datas;
        private Comparator comparatorName;
        private Comparator comparatorKeySearch;
        private Comparator comparatorContact;
        private Comparator comparatorMessage;
        private long startTime;
        private int totalDatas;

        public SearchContactTask(ApplicationController application) {
            this.application = new WeakReference<>(application);
            this.datas = new CopyOnWriteArrayList<>();
        }

        public void setListener(SearchContactsListener listener) {
            this.listener = listener;
        }

        public void setDatas(ArrayList<Object> datas) {
            if (this.datas != null && datas != null)
                this.datas.addAll(datas);
        }

        public void setComparatorName(Comparator comparatorName) {
            this.comparatorName = comparatorName;
        }

        public void setComparatorKeySearch(Comparator comparatorKeySearch) {
            this.comparatorKeySearch = comparatorKeySearch;
        }

        public void setComparatorContact(Comparator comparatorContact) {
            this.comparatorContact = comparatorContact;
        }

        public void setComparatorMessage(Comparator comparatorMessage) {
            this.comparatorMessage = comparatorMessage;
        }

        @Override
        protected void onPreExecute() {
            if (listener != null) listener.onPrepareSearchContact();
        }

        @Override
        protected void onPostExecute(ArrayList<Object> result) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                        + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
            if (listener != null) listener.onFinishedSearchContact(keySearch, result);
        }

        @Override
        protected ArrayList<Object> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            totalDatas = 0;
            ArrayList<Object> list = new ArrayList<>();
            keySearch = params[0];
            if (Utilities.notNull(application) && Utilities.notEmpty(datas)) {
                totalDatas = datas.size();
                ResultSearchContact result = SearchUtils.searchMessagesAndContacts(application.get()
                        , keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage, comparatorContact);
                if (result != null) {
                    keySearch = result.getKeySearch();
                    if (Utilities.notEmpty(result.getResult())) {
                        list.addAll(result.getResult());
                    }
                }
            }
            return list;
        }
    }

    private class GetContactListAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            if (mAdapter == null) {
                mProgressLoading.setVisibility(View.VISIBLE);
                mProgressLoading.setEnabled(true);
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (!mContactBusiness.isInitArrayListReady()) {
                mContactBusiness.initArrayListPhoneNumber();
            }
            if (mListObj == null) {
                mListObj = new ArrayList<>();
            } else {
                mListObj.clear();
            }
            mListAlls = mContactBusiness.getListNumberAlls();
            if (mListAlls != null && !mListAlls.isEmpty()) {
                mListObj.addAll(mListAlls);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mProgressLoading.setEnabled(false);
            mProgressLoading.setVisibility(View.GONE);
            String contentSearch = mReengSearchView.getText().toString();
            if (mViewAbSearch.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(contentSearch.trim())) {
                searchContactAsyncTask(contentSearch.trim());
            } else {
                drawListView(mListObj);
            }
            super.onPostExecute(aVoid);
        }
    }

    private class DeleteContactAsyncTask extends AsyncTask<String, Void, Boolean> {
        private String title = mRes.getString(R.string.title_delete_contact);
        private String msgError = mRes.getString(R.string.msg_delete_contact_error);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private String contactId;

        @Override
        protected void onPreExecute() {
            ContentObserverBusiness.getInstance(mParentActivity).setAction(Constants.ACTION.DEL_CONTACT);
            mParentActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            contactId = params[0];
            if (mContactBusiness.deleteContactDBAndDevices(contactId)) {
                mContactBusiness.initArrayListPhoneNumber();
                mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            ContentObserverBusiness.getInstance(mParentActivity).setAction(-1);
            mParentActivity.hideLoadingDialog();
            if (!aBoolean) {
                mParentActivity.showError(msgError, title);
            }
            super.onPostExecute(aBoolean);
        }
    }
}
package com.metfone.selfcare.fragment.viettelIQ;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import com.metfone.selfcare.activity.ViettelIQActivity;
import com.metfone.selfcare.activity.WebViewNewActivity;
import com.metfone.selfcare.adapter.GameIQWinnerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ViettelIQApi;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WinViettelIQFragment extends Fragment {

    private static final String TAG = WinViettelIQFragment.class.getSimpleName();
    private static final String START_TIME = "startTime";
    private static final String MATCH_ID = "matchId";

    @BindView(R.id.tv_number_win)
    TextView tvNumberWin;
    @BindView(R.id.tv_total_prize_value)
    TextView tvTotalPrizeValue;
    @BindView(R.id.win_recycler_view)
    RecyclerView winRecyclerView;
    @BindView(R.id.tv_share_facebook)
    RoundTextView tvShareFacebook;
    Unbinder unbinder;
    @BindView(R.id.tvNextTimeEvent)
    TextView tvNextTimeEvent;
    @BindView(R.id.root_info)
    Space rootInfo;
    @BindView(R.id.tvRule)
    RoundTextView tvRule;
    @BindView(R.id.rlAction)
    RelativeLayout rlAction;

    private ApplicationController mApp;
    private ViettelIQActivity activity;
    private ArrayList<UserInfo> listWinner = new ArrayList<>();
    private GameIQWinnerAdapter adapter;
    private String matchId;
    private long startTime;
    private String urlRule = "http://mocha.com.vn";
    private EventOnMediaHelper eventOnMediaHelper;

    public static Fragment newInstance(long startTime, String matchId) {
        Bundle args = new Bundle();
        args.putLong(START_TIME, startTime);
        args.putString(MATCH_ID, matchId);
        WinViettelIQFragment fragment = new WinViettelIQFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (ViettelIQActivity) getActivity();
        mApp = (ApplicationController) activity.getApplication();
        matchId = getArguments().getString(MATCH_ID);
        startTime = getArguments().getLong(START_TIME);
        eventOnMediaHelper = new EventOnMediaHelper(activity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viettel_iq_win, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        activity.showLoadingDialog("", R.string.loading);
        ViettelIQApi.getInstance().getReport(activity.getCurrentIdGame(), matchId, this);
        return view;
    }

    private void initView() {
        winRecyclerView.setLayoutManager(new LinearLayoutManager(mApp, LinearLayoutManager.VERTICAL, false));
        adapter = new GameIQWinnerAdapter(listWinner, mApp);
        winRecyclerView.setAdapter(adapter);
        adapter.setListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                UserInfo userInfo = listWinner.get(pos);
                userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                userInfo.setStateMocha(1);
                eventOnMediaHelper.processUserClick(userInfo);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void onGetListDone(String decryptData) {
        if (activity == null || activity.isFinishing() || winRecyclerView == null) return;
        activity.hideLoadingDialog();
        if (TextUtils.isEmpty(decryptData)) {
            ReportHelper.reportError(ApplicationController.self(), "GET_WIN", "empty " + decryptData);
            activity.showToast(R.string.e601_error_but_undefined);
            DialogMessage dialogMessage = new DialogMessage(activity, false);
            dialogMessage.setLabelButton(getResources().getString(R.string.retry));
            dialogMessage.setMessage(getResources().getString(R.string.e601_error_but_undefined));
            dialogMessage.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    activity.getSub();
                }
            });
            dialogMessage.show();
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(decryptData);
            if (jsonObject.optInt("code") == 200) {
                listWinner = parseListWinner(jsonObject);
//                long numPlayer = jsonObject.optLong("numPlayer");
//                long numViewer = jsonObject.optLong("numViewer");
//                Log.i(TAG, "-----IQ-----onGetListDone: numPlayer: " + numPlayer + " numViewer: " + numViewer);
//                EventBus.getDefault().post(new GameIQHelper.IQGameEvent(activity.getCurrentIdGame(), numPlayer, numViewer));
                long numTotal = jsonObject.optLong("totalSub");
                activity.updateTextEndGame(numTotal);
                tvNextTimeEvent.setText(jsonObject.optString("nextTime"));
                tvNumberWin.setText(jsonObject.optString("titlePrize"));
                tvTotalPrizeValue.setText(jsonObject.optString("prizeValue"));
                urlRule = jsonObject.optString("rulesUrl", "http://mocha.com.vn");

                if (!listWinner.isEmpty()) {
                    winRecyclerView.setVisibility(View.VISIBLE);
                    adapter.setListWinner(listWinner);
                    adapter.notifyDataSetChanged();
                    tvShareFacebook.setVisibility(View.VISIBLE);
                } else {
                    winRecyclerView.setVisibility(View.INVISIBLE);
                    tvShareFacebook.setVisibility(View.GONE);
                }
            } else {
                activity.showToast(R.string.e601_error_but_undefined);
            }
        } catch (Exception e) {
            Log.e(TAG, "JSONException", e);
            activity.showToast(R.string.e601_error_but_undefined);
        }
    }

    private ArrayList<UserInfo> parseListWinner(JSONObject jso) {
        ArrayList<UserInfo> listWinner = new ArrayList<>();
        JSONArray jsa = jso.optJSONArray("listWinner");
        if (jsa == null || jsa.length() == 0) return listWinner;
        for (int i = 0; i < jsa.length(); i++) {
            JSONObject js = jsa.optJSONObject(i);
            if (js != null) {
                UserInfo userInfo = new UserInfo();
                userInfo.setMsisdn(js.optString("msisdn"));
                userInfo.setAvatar(js.optString("avatar"));
                userInfo.setName(js.optString("name"));
                userInfo.setPrize(js.optString("prize"));
                listWinner.add(userInfo);
            }
        }
        return listWinner;
    }

    @OnClick({R.id.tv_share_facebook, R.id.tvRule})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_share_facebook:
                Bitmap b = getBitmapFromView();
                rlAction.setVisibility(View.VISIBLE);
                activity.shareImageFacebook(b, "");
                break;

            case R.id.tvRule:
                Utilities.gotoWebViewFullScreen(mApp, activity, urlRule, WebViewNewActivity.ORIENTATION_AUTO, "", "", false);
                break;
        }
    }

    private Bitmap getBitmapFromView() {
        rlAction.setVisibility(View.GONE);
        Bitmap scaledBitmap = Bitmap.createBitmap(mApp.getWidthPixels(), mApp.getHeightPixels(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        activity.getParentView().draw(canvas);
        return scaledBitmap;
    }
}

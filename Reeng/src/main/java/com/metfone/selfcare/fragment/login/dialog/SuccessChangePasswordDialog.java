package com.metfone.selfcare.fragment.login.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;

import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.roundview.RoundTextView;

public class SuccessChangePasswordDialog extends Dialog implements View.OnClickListener {
    public LoginActivity activity;
    private RoundTextView btnDone;
    String phoneNumber;
    public SuccessChangePasswordDialog(@NonNull Activity activity, String phoneNumber) {
        super(activity);
        this.activity = (LoginActivity) activity;
        this.phoneNumber = phoneNumber;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_success_change_password);
        btnDone = findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDone:
                activity.displayLoginPasswordScreen(phoneNumber);
                dismiss();
                break;
            default:
                break;
        }
    }

}

package com.metfone.selfcare.fragment.setting;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by thanhnt72 on 11/30/2018.
 */

public class AccountSettingFragment extends BaseSettingFragment {

    private static final String TAG = AccountSettingFragment.class.getSimpleName();
    @BindView(R.id.setting_backup)
    LinearLayoutCompat settingBackup;
    //    @BindView(R.id.setting_change_number)
//    LinearLayoutCompat settingChangeNumber;
    @BindView(R.id.setting_deactive_account)
    LinearLayoutCompat settingDeactiveAccount;
    @BindView(R.id.setting_hide_thread_chat)
    LinearLayoutCompat settingHideThreadChat;
    private SettingFragment.OnFragmentSettingListener mListener;

    public static AccountSettingFragment newInstance() {
        return new AccountSettingFragment();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            mListener = (SettingFragment.OnFragmentSettingListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    public String getName() {
        return "AccountSettingFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_account_setting_v5;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setTitle(R.string.title_account);
    }

    @Override
    protected void initView(View view) {
        if (!Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES) {
            settingBackup.setVisibility(View.GONE);
        } else {
            settingBackup.setVisibility(View.VISIBLE);
        }
//        if (mApplication.getReengAccountBusiness().isEnableChangeNumber()) {
//            settingChangeNumber.setVisibility(View.VISIBLE);
//        } else
//            settingChangeNumber.setVisibility(View.GONE);
        settingHideThreadChat.setVisibility(Config.Features.FLAG_SUPPORT_HIDE_THREAD ? View.VISIBLE : View.GONE);
    }


    @OnClick({R.id.setting_backup, R.id.setting_deactive_account,
            R.id.setting_hide_thread_chat, R.id.settingLockApp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.setting_backup:
                if (mListener != null)
                    mListener.navigateToBackup();
                break;
//            case R.id.setting_change_number:
//                DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://changenumber");
//                break;
            case R.id.setting_deactive_account:
                showDeactiveAccountPopup(mParentActivity);
                break;
            case R.id.setting_hide_thread_chat:
                if (mListener != null)
                    mListener.navigateToHideThread();
                break;
            case R.id.settingLockApp:
                if (mListener != null)
                    mListener.navigateToLockApp();
                break;
        }
    }

    public void showDeactiveAccountPopup(final SettingActivity acitivity) {
//        Resources res = acitivity.getResources();
//        new DialogConfirm(acitivity, true).setLabel(res.getString(R.string.logout)).setMessage(res.getString(R
//                .string.logout_body)).
//                setNegativeLabel(res.getString(R.string.bk_backup)).setPositiveLabel(res.getString(R.string.logout)).
//                setPositiveListener(new PositiveListener<Object>() {
//                    @Override
//                    public void onPositive(Object result) {
//                        if (mListener != null)
//                            mListener.deactiveAccount();
//                    }
//                })
//                .setNegativeListener(new NegativeListener() {
//                    @Override
//                    public void onNegative(Object result) {
//                        DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://backup");
//                    }
//                }).show();

        com.metfone.selfcare.v5.dialog.DialogConfirm dialogConfirm =
                com.metfone.selfcare.v5.dialog.DialogConfirm.newInstance(getString(R.string.logout),
                        getString(R.string.logout_body)
                        , com.metfone.selfcare.v5.dialog.DialogConfirm.CONFIRM_TYPE, R.string.bk_backup, R.string.logout);
        dialogConfirm.setSelectListener(new BaseDialogFragment.DialogListener() {
            @Override
            public void dialogRightClick(int value) {
                if (mListener != null)
                    mListener.deactiveAccount();
            }

            @Override
            public void dialogLeftClick() {
                DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, "mocha://backup");
            }
        });
        dialogConfirm.show(getChildFragmentManager(), "");
    }
}

package com.metfone.selfcare.fragment.avno;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.adapter.avno.ItemInfoAVNOAdapter;
import com.metfone.selfcare.adapter.avno.PackageAVNONewAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.InfoAVNO;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.listeners.ChargeMoneyAVNOListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 7/16/2018.
 */

public class AVNOManagerFragmentNew extends Fragment implements
        AVNOHelper.GetInfoAVNOListener,
        AVNOHelper.OnClickActionPackage {

    private static final String TAG = AVNOManagerFragmentNew.class.getSimpleName();
    @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tvAvatarName)
    TextView tvAvatarName;
    @BindView(R.id.frameAvatar)
    FrameLayout frameAvatar;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvNumberAVNO)
    TextView tvNumberAVNO;
    @BindView(R.id.llInfoUser)
    LinearLayout llInfoUser;
    @BindView(R.id.tvActive)
    RoundTextView tvActive;
    @BindView(R.id.tvAccountBalance)
    TextView tvAccountBalance;
    @BindView(R.id.rvInfoPackage)
    RecyclerView rvInfoPackage;
    @BindView(R.id.llInfoPackage)
    LinearLayout llInfoPackage;
    @BindView(R.id.rvPackage)
    RecyclerView rvPackage;
    @BindView(R.id.llPackage)
    LinearLayout llPackage;
    @BindView(R.id.rvPrice)
    RecyclerView rvPrice;
    @BindView(R.id.llPrice)
    LinearLayout llPrice;
    @BindView(R.id.view_parent)
    NestedScrollView viewParent;
    Unbinder unbinder;
    @BindView(R.id.tvTopUp)
    RoundTextView tvTopUp;
    @BindView(R.id.viewDividerPlan)
    View viewDividerPlan;
    @BindView(R.id.viewDividerPayAs)
    View viewDividerPayAs;


    private AVNOActivity mActivity;
    private ApplicationController mApplication;
    private Resources mRes;

    private ItemInfoAVNOAdapter itemInfoAVNOAdapter;
    private PackageAVNONewAdapter packageAVNOAdapter;
    private PackageAVNONewAdapter priceAVNOAdapter;


    public static AVNOManagerFragmentNew newInstance() {
        AVNOManagerFragmentNew fragment = new AVNOManagerFragmentNew();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AVNOActivity) activity;
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_avno_manager_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        rvInfoPackage.setLayoutManager(
                new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        rvPackage.setLayoutManager(
                new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        rvPrice.setLayoutManager(
                new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));


        itemInfoAVNOAdapter = new ItemInfoAVNOAdapter(mActivity, new ArrayList<ItemInfo>());
        rvInfoPackage.setAdapter(itemInfoAVNOAdapter);

        packageAVNOAdapter = new PackageAVNONewAdapter(mApplication, new ArrayList<PackageAVNO>(), this);
        rvPackage.setAdapter(packageAVNOAdapter);

        priceAVNOAdapter = new PackageAVNONewAdapter(mApplication, new ArrayList<PackageAVNO>(), null);
        rvPrice.setAdapter(priceAVNOAdapter);

        viewParent.setVisibility(View.INVISIBLE);
        setToolbar(inflater);
        getDataAVNO();

        return view;
    }


    private void setToolbar(LayoutInflater inflater) {
        View abView = mActivity.getToolBarView();
        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        ImageView mBtnBack = abView.findViewById(R.id.ab_back_btn);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });

        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mRes.getString(R.string.avno_manager_title));
        ImageView mImgMore = abView.findViewById(R.id.ab_more_btn);
        mImgMore.setVisibility(View.GONE);
    }

    public void getDataAVNO() {
        Log.i(TAG, "getDataAVNO");
        mActivity.showLoadingDialog("", R.string.loading);
        AVNOHelper.getInstance(mApplication).getInfoAVNOV4(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onGetInfoAVNOSuccess(InfoAVNO infoAVNO) {
        viewParent.setVisibility(View.VISIBLE);
        mActivity.hideLoadingDialog();
//        viewRetryLoading.setVisibility(View.GONE);
        tvNumberAVNO.setText(infoAVNO.getAvnoNumber());
        tvName.setText(mApplication.getReengAccountBusiness().getUserName());
        tvAccountBalance.setText(infoAVNO.getBalanceAccount());
        if (!infoAVNO.getAccountInfoList().isEmpty()) {
            llInfoPackage.setVisibility(View.VISIBLE);
            itemInfoAVNOAdapter.setListItem(infoAVNO.getAccountInfoList());
            itemInfoAVNOAdapter.notifyDataSetChanged();
        } else {
            llInfoPackage.setVisibility(View.GONE);
        }

        if (!infoAVNO.getListPackageActive().isEmpty()) {
            llPackage.setVisibility(View.VISIBLE);
            packageAVNOAdapter.setListItem(infoAVNO.getListPackageActive());
            packageAVNOAdapter.notifyDataSetChanged();
            viewDividerPlan.setVisibility(View.VISIBLE);
        } else {
            llPackage.setVisibility(View.GONE);
            viewDividerPlan.setVisibility(View.GONE);
        }

        ArrayList<PackageAVNO> listPrice = new ArrayList<>();
        /*if (!infoAVNO.getListChargeCall().isEmpty()) {
            listPrice.addAll(infoAVNO.getListChargeCall());
        }
        if (!infoAVNO.getListChargeSms().isEmpty()) {
            listPrice.addAll(infoAVNO.getListChargeSms());
        }*/
        if (!listPrice.isEmpty()) {
            llPrice.setVisibility(View.VISIBLE);
            priceAVNOAdapter.setListItem(listPrice);
            priceAVNOAdapter.notifyDataSetChanged();
            viewDividerPayAs.setVisibility(View.VISIBLE);
        } else {
            llPrice.setVisibility(View.GONE);
            viewDividerPayAs.setVisibility(View.GONE);
        }


        if (infoAVNO.getActiveState() == 0) {
            tvActive.setVisibility(View.VISIBLE);
        } else {
            tvActive.setVisibility(View.GONE);
        }

        mApplication.getAvatarBusiness().setMyAvatar(ivAvatar,
                tvAvatarName, null,
                mApplication.getReengAccountBusiness().getCurrentAccount(), null);
    }

    @Override
    public void onGetInfoAVNOError(int code, String msg) {
        mActivity.hideLoadingDialog();
        mActivity.showToast(msg);
    }

    @Override
    public void onClickActionPackage(final PackageAVNO packageAVNO) {
        final boolean isRegister = packageAVNO.getStatus() == PackageAVNO.PACKAGE_AVAILABLE;
        DialogConfirm dialogConfirm = new DialogConfirm(mActivity, true);
        dialogConfirm.setTitle(isRegister ? R.string.title_confirm_register_pkg_avno :
                R.string.title_confirm_cancel_pkg_avno);
        dialogConfirm.setMessage(isRegister ?
//                String.format(mRes.getString(R.string.msg_confirm_register_pkg_avno), packageAVNO.getTitle()) :
                packageAVNO.getAlert() :
                String.format(mRes.getString(R.string.msg_confirm_cancel_pkg_avno), packageAVNO.getTitle()));
        dialogConfirm.setPositiveLabel(mRes.getString(R.string.confirm));
        dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {

            @Override
            public void onPositive(Object result) {
                mActivity.showLoadingDialog("", R.string.loading);
                AVNOHelper.getInstance(mApplication).actionPackageAVNO(isRegister, String.valueOf(packageAVNO.getId()),
                        new AVNOHelper.ActionPackageAVNOListener() {

                            @Override
                            public void onActionPackageSuccess(String msg) {
                                mActivity.hideLoadingDialog();
                                mActivity.showToast(msg);
                                getDataAVNO();
                            }

                            @Override
                            public void onActionPackageError(int code, String msg) {
                                mActivity.hideLoadingDialog();
                                mActivity.showToast(msg);
                            }
                        });
            }
        });
        dialogConfirm.show();
    }

    @Override
    public void onClickRegisterFreeNumber(PackageAVNO packageAVNO) {

    }

    @Override
    public void onClickContentPackage(final PackageAVNO packageAVNO) {
        if (packageAVNO.getStatus() == PackageAVNO.PACKAGE_AVAILABLE
                || packageAVNO.getStatus() == PackageAVNO.PACKAGE_ACTIVE) {
            final boolean isRegister = packageAVNO.getStatus() == PackageAVNO.PACKAGE_AVAILABLE;

            DialogMessage dialogMessage = new DialogMessage(mActivity, true)
                    .setLabelButton(isRegister ? mRes.getString(R.string.register) : mRes.getString(R.string.cancel))
                    .setLabel(packageAVNO.getTitle())
                    .setMessage(packageAVNO.getDesc())
                    .setNegativeListener(new NegativeListener() {
                        @Override
                        public void onNegative(Object result) {

                            onClickActionPackage(packageAVNO);
                        }
                    });
            dialogMessage.show();
        }

    }

    @OnClick({R.id.tvActive, R.id.tvTopUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvActive:
                AVNOHelper.getInstance(mApplication).gotoWebViewCharge(mActivity);
                break;
            case R.id.tvTopUp:
                mActivity.showDialogChargeMoney(new ChargeMoneyAVNOListener() {
                    @Override
                    public void onChargeMoneySuccess() {
                        getDataAVNO();
                    }

                    @Override
                    public void onChargeError(String msg) {
                        //ko gọi show toast vì bên trong đã show rồi
                    }
                });

                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAVNOManagerMessage(final AVNOManagerMessage event) {
        Log.i(TAG, "onAVNOManagerMessage");
        getDataAVNO();

//        EventBus.getDefault().removeStickyEvent(event);
    }

    public static class AVNOManagerMessage {

    }
}

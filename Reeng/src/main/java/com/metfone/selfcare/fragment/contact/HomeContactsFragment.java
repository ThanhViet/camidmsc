package com.metfone.selfcare.fragment.contact;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.QRCodeActivity;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.MochaFriendAccountWrapper;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.OfficerAccountWrapper;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.fragment.call.KeyBoardDialog;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.ContactsTabInterface;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.model.Divider;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.OVER_SCROLL_NEVER;

/**
 * Created by toanvk2 on 8/31/2017.
 */

public class HomeContactsFragment extends BaseLoginAnonymousFragment implements InitDataListener,
        ContactsTabInterface,
        ContactChangeListener,
        BaseLoginAnonymousFragment.EmptyViewListener,
        View.OnClickListener {
    private static final String TAG = HomeContactsFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private HomeActivity mParentActivity;
    private Resources mRes;
    private RecyclerView mRecyclerView;
    private HomeContactsAdapter mAdapter;

    private InitListAsyncTask mInitListAsyncTask;
    private ArrayList<Object> listData;
    private ArrayList<PhoneNumber> listPhoneNumbers;
    private ArrayList<PhoneNumber> listFavorites;
    private ArrayList<PhoneNumber> listRecent;
    private CopyOnWriteArrayList<OfficerAccount> listOA = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<SocialFriendInfo> listSocialFriend;
    private ConstraintLayout mLayoutScanQRCode;
    private ImageView mImgButtonShowKeyBoard;
    private String contentEmpty;
    /*private InitListAsyncTask mInitListAsyncTask;
    private SearchAsyncTask mSearchAsyncTask;*/
    private boolean isPageVisible = false, isLoadingOA, isLoadingFriend;

//    private SharedPreferences mPref;

    public static HomeContactsFragment newInstance() {
        Log.i(TAG, "HomeContactsFragment newInstance ...");
        return new HomeContactsFragment();
    }

    public HomeContactsFragment() {
        // Required empty public constructor
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (HomeActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mRes = mParentActivity.getResources();
        super.onAttach(activity);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            isPageVisible = savedInstanceState.getBoolean("PageStateVisible", false);
        } /*else if (mParentActivity != null && mParentActivity.getCurrentTab() == 0) {
            isPageVisible = isUserVisible;
        }*/
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (mParentActivity !=  null && mParentActivity.getCheckTabMobile() != false && mParentActivity.getCurrentSubTabMobile() == 1) {
            ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
        }
//        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        findComponentViews(rootView, inflater, container);
        ListenerHelper.getInstance().addContactChangeListener(this);
        ListenerHelper.getInstance().addInitDataListener(this);
        if (isPageVisible && mApplication.isDataReady()) {
            reloadDataAsyncTask();
        }
        if (mApplication.getReengAccountBusiness().isAnonymousLogin())
            hideEmptyView();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        Log.d(TAG, "OnResume");
        if (!mApplication.isDataReady() && !mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            showProgressLoading();
        }
        if (mParentActivity !=  null && mParentActivity.getCheckTabMobile() != false && mParentActivity.getCurrentSubTabMobile() == 1) {
            ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("PageStateVisible", isPageVisible);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mInitListAsyncTask != null) {
            mInitListAsyncTask.cancel(true);
            mInitListAsyncTask = null;
        }
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
    }

    @Override
    protected String getSourceClassName() {
        return "Contacts";
    }

    @Override
    public void onDataReady() {
        Log.d(TAG, "load data complate");
        if (isPageVisible) {
            reloadDataAsyncTask();
        }
    }

    @Override
    public void onContactChange() {
        checkAndReloadData();
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        checkAndReloadData();
    }

    @Override
    public void onRosterChange() {
        checkAndReloadData();
    }

    @Override
    public void initListContactComplete(int sizeSupport) {
        checkAndReloadData();
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint: " + isVisibleToUser);
        super.setUserVisibleHint(isVisibleToUser);
//        isUserVisible = isVisibleToUser;
        // mở từ deeplink sẽ ko nhận trạng thái onPageStateVisible
       /* if (mParentActivity != null && mParentActivity.getCurrentTab() == 0) {
            isPageVisible = isUserVisible;
        }*/
        onPageStateVisible(isVisibleToUser);
        if (isVisibleToUser) {
            checkFirstLastVisible();
        }
    }

    private void checkFirstLastVisible() {
        if (mRecyclerView != null && mAdapter != null) {
            LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
            if (layoutManager == null) return;
            int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
            int findLastCompletelyVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition();

            if (findFirstCompletelyVisibleItemPosition == 0 && findLastCompletelyVisibleItemPosition
                    == (mAdapter.getItemCount() - 1)) {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(true));
                mRecyclerView.setNestedScrollingEnabled(false);
            } else {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(false));
                mRecyclerView.setNestedScrollingEnabled(true);
            }
        }
    }

    @Override
    public void onPageStateVisible(boolean isVisible) {
        Log.d(TAG, "onPageStateVisible: " + isVisible);
        isPageVisible = isVisible;
        if (mRecyclerView != null && isPageVisible && mApplication.isDataReady()) {
            if (listData == null)
                reloadDataAsyncTask();
            else if (listSocialFriend == null)
                getAndDrawOfficial();
        }
    }

    @Override
    public void onTabReselected() {
        if (mRecyclerView != null) {
            mRecyclerView.scrollToPosition(0);
        }
    }

    @Override
    public void onSearchChange(String textSearch) {
    }

    private void checkAndReloadData() {
        if (isPageVisible || mAdapter != null) {
            mParentActivity.runOnUiThread(this::reloadDataAsyncTask);
        }
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mLayoutScanQRCode = rootView.findViewById(R.id.ll_scan_qr);
        mImgButtonShowKeyBoard = rootView.findViewById(R.id.img_show_keyboard);
        mImgButtonShowKeyBoard.setVisibility(View.VISIBLE);
        mLayoutScanQRCode.setVisibility(View.VISIBLE);
        mLayoutScanQRCode.setOnClickListener(this);
        mImgButtonShowKeyBoard.setOnClickListener(this);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        mRecyclerView.setOverScrollMode(OVER_SCROLL_NEVER);
        //mRecyclerViewMessage.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new HomeContactsAdapter(mApplication, new ArrayList<>(), false, false);
        mAdapter.setShowSync(true);
        mRecyclerView.setAdapter(mAdapter);
        setItemListViewListener();
//        mHeaderAdapter.addHeaderView(mViewHeader);
        createView(inflater, mRecyclerView, this);
        // [Start] CamID
//        initViewLogin(inflater, container, rootView);
        // [End] CamID
        contentEmpty = mRes.getString(R.string.list_empty);
//        mTvViewAll.setOnClickListener(this);
    }

    private void notifyChangeAdapter() {
        mAdapter.setData(listData);
        mAdapter.notifyDataSetChanged();
//        drawHeaderDetail();
        if ((listData == null || listData.isEmpty()) &&
                (listFavorites == null || listFavorites.isEmpty())) {
            showEmptyNote(contentEmpty);
        } else
            hideEmptyView();
    }

    private void setItemListViewListener() {
        mAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onAudioCall(Object entry) {
                super.onAudioCall(entry);
                HomeContactsHelper.getInstance(mApplication).handleCallContactsClick(mParentActivity, entry);
            }

            @Override
            public void onVideoCall(Object entry) {
                super.onVideoCall(entry);
                if (entry instanceof PhoneNumber) {
                    mApplication.getCallBusiness().checkAndStartVideoCall(mParentActivity, (PhoneNumber) entry, false);
                }
            }

            @Override
            public void onActionLabel(Object entry) {
                super.onActionLabel(entry);
                if (entry instanceof PhoneNumber) {
                    InviteFriendHelper.getInstance().showInviteFriendPopup(mApplication, mParentActivity, entry, false);
                }
            }

            @Override
            public void onSectionClick(Object entry) {
                super.onSectionClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleSectionContactsClick(mParentActivity, entry);
            }

            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mParentActivity, entry);
            }

            @Override
            public void onAvatarClick(Object entry) {
                super.onAvatarClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mParentActivity, entry);
            }

            @Override
            public void onItemLongClick(Object object) {
                super.onItemLongClick(object);
                if (object instanceof PhoneNumber) {
                    PhoneNumber entry = (PhoneNumber) object;
                    if (entry.getContactId() != null) {
                        ItemContextMenu deleteItem = new ItemContextMenu(mRes.getString(R.string.del_contact), -1, entry, Constants.MENU.DELETE);
                        ArrayList<ItemContextMenu> list = new ArrayList<>();
                        list.add(deleteItem);
                        PopupHelper.getInstance().showContextMenu(mParentActivity,
                                entry.getName(), list, (view, object1, menuId) -> deleteContact(((PhoneNumber) object1).getContactId()));
                    }
                }
            }
        });
    }

    private void getAndDrawOfficial() {
        if (isLoadingOA || isLoadingFriend) return;
        isLoadingOA = true;
        HomeContactsHelper.getInstance(mApplication).getOfficialAccounts(new HomeContactsHelper.GetSectionOfficialListener() {
            @Override
            public void onResponse(List<OfficerAccount> listRoom) {
                isLoadingOA = false;
                listOA = new CopyOnWriteArrayList<>(listRoom);
                isLoadingFriend = true;
                mApplication.getStrangerBusiness().getSocialFriendRequest(StrangerBusiness.TYPE_SOCIAL_FRIEND, false, listener);
            }

            @Override
            public void onError(String e) {
                isLoadingOA = false;
            }
        });
    }

    private StrangerBusiness.onSocialFriendsRequestListener listener = new StrangerBusiness.onSocialFriendsRequestListener() {
        @Override
        public void onResponse(int type, ArrayList<SocialFriendInfo> list) {
            isLoadingFriend = false;
            if (list.size() >= 5) {
                listSocialFriend = new CopyOnWriteArrayList<>(list.subList(0, 5));
            } else {
                listSocialFriend = new CopyOnWriteArrayList<>(list);
            }
            processListOAFriend();
        }

        @Override
        public void onLoadMoreResponse(int type, ArrayList<SocialFriendInfo> list, boolean isNoMore) {

        }

        @Override
        public void onError(int type, int code, boolean isLoadMore) {

        }
    };

    private void processListOAFriend() {
        ArrayList<Object> listTmp = new ArrayList<>();

        if (listOA != null && !listOA.isEmpty()) {
            PhoneNumber sectionOfficial = new PhoneNumber(null, mRes.getString(R.string.menu_officials), NumberConstant.SECTION_TYPE_OFFICIAL);
            listTmp.add(sectionOfficial);
            listTmp.add(new OfficerAccountWrapper(listOA));
            listTmp.add(new Divider());
        }
        if (listSocialFriend != null && !listSocialFriend.isEmpty()) {
            PhoneNumber sectionFriendMocha = new PhoneNumber(null, mRes.getString(R.string.social_tab_friend), NumberConstant.SECTION_TYPE_NEW_FRIENDS);
            listTmp.add(sectionFriendMocha);
            listTmp.add(new MochaFriendAccountWrapper(listSocialFriend));
            listTmp.add(new Divider());
        }
        int listSizeFavor = listFavorites.size();
        listData.addAll(listSizeFavor > 0 ? (listSizeFavor + 2) : 0, listTmp); //listSizeFavor + sectionFavor + dividerFavor
      //  notifyChangeAdapter();
    }

    private void reloadDataAsyncTask() {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) return;
        Log.d(TAG, "reloadDataAsyncTask");
        if (mInitListAsyncTask != null) {
            mInitListAsyncTask.cancel(true);
            mInitListAsyncTask = null;
        }
        mInitListAsyncTask = new InitListAsyncTask();
        mInitListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvViewAll:
                NavigateActivityHelper.navigateToContactList(mParentActivity, Constants.CONTACT.FRAG_LIST_UTIL);
                break;
            case R.id.ll_scan_qr:
//                startActivity(new Intent(getActivity(), QRCodeActivity.class));
                NavigateActivityHelper.navigateToQRScanKh((BaseSlidingFragmentActivity) getActivity());
                break;
            case  R.id.img_show_keyboard:
                KeyBoardDialog dialog = new KeyBoardDialog(mParentActivity, "");
                if (dialog.getWindow() != null) {
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogKeyBoardAnimation;
                dialog.show();
            }
                break;
        }
    }

    private class InitListAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            if (mAdapter == null) {
                showProgressLoading();
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            if (!contactBusiness.isInitArrayListReady()) {
                contactBusiness.initArrayListPhoneNumber();
            }
            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }
            listRecent = contactBusiness.getRecentPhoneNumber();
            listPhoneNumbers = contactBusiness.getListNumberAlls();
            listFavorites = contactBusiness.getListNumberFavorites();
            Log.d(TAG, "listFavorites: " + (listFavorites != null ? listFavorites.size() : "null"));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            hideEmptyView();
            //lich su goi
//            if(listRecent != null && !listRecent.isEmpty()){
//                PhoneNumber sectionHistory = new PhoneNumber(null, mRes.getString(R.string.contact_history_call), NumberConstant.SECTION_TYPE_CONTACT_HISTORY);
//                listRecent.add(0, sectionHistory);
//                listData.addAll(listRecent);
//            }
            if (listPhoneNumbers != null && !listPhoneNumbers.isEmpty()) {

                if (listPhoneNumbers.get(0).getContactId() == null
                        && "#".equals(listPhoneNumbers.get(0).getName())) {
                    PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), NumberConstant.SECTION_TYPE_CONTACT);
                    listPhoneNumbers.add(0, sectionContacts);
                }  else if (listPhoneNumbers.get(0).getContactId() == null && !"A".equals(listPhoneNumbers.get(0).getName())) {
                    listPhoneNumbers.remove(0);
                    PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), NumberConstant.SECTION_TYPE_CONTACT);
                    listPhoneNumbers.add(0, sectionContacts);
                } else {
                    PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), NumberConstant.SECTION_TYPE_CONTACT);
                    listPhoneNumbers.add(0, sectionContacts);
                }
                listData.addAll(listPhoneNumbers);
            }

            if (listFavorites != null && listFavorites.size() > 0 && listPhoneNumbers != null && listPhoneNumbers.size() > 0) {
                PhoneNumber sectionFavor = new PhoneNumber(null, mRes.getString(R.string.list_favorite), -1);
                listData.add(0, sectionFavor);
                listData.addAll(1, listFavorites);
                if (listData.size() > listFavorites.size())
                    listData.add(listFavorites.size() + 1, new Divider());
            }
            notifyChangeAdapter();
            getAndDrawOfficial();
            super.onPostExecute(aVoid);
        }
    }

    private void deleteContact(final String id) {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(mParentActivity,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_DELETE_CONTACT);
        } else {
            new DialogConfirm(mParentActivity, true).
                    setLabel(mRes.getString(R.string.title_delete_contact)).
                    setMessage(mRes.getString(R.string.msg_delete_contact)).
                    setNegativeLabel(mRes.getString(R.string.cancel)).
                    setPositiveLabel(mRes.getString(R.string.ok)).
                    setEntry(id).
                    setPositiveListener(result -> {
                        String contactId = (String) result;
                        ContentObserverBusiness.getInstance(mParentActivity).setAction(Constants.ACTION.DEL_CONTACT);
                        if (mApplication.getContactBusiness().deleteContactDBAndDevices(contactId)) {
                            mApplication.getContactBusiness().initArrayListPhoneNumber();
                            //mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
                            reloadDataAsyncTask();
                        }
                        ContentObserverBusiness.getInstance(mParentActivity).setAction(-1);
                    }).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReloadDataHomeContactEvent(final ReloadDataHomeContactEvent event) {
        Log.i(TAG, "onReloadDataHomeContactEvent: ");
//        reloadDataAsyncTask();
    }

    public static class ReloadDataHomeContactEvent {

    }
}

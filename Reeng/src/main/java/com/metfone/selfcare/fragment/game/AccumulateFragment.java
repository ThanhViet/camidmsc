package com.metfone.selfcare.fragment.game;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.adapter.AccumulateTabAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengViewPager;
import com.metfone.selfcare.util.Log;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by toanvk2 on 11/29/2016.
 */
public class AccumulateFragment extends Fragment implements AccumulatePointHelper.GetAccumulateListener,
        AccumulatePointHelper.AccumulateDataChange {
    private static final String TAG = AccumulateFragment.class.getSimpleName();
    private ListGamesActivity mParentActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private LinearLayout mViewHeader, mViewContent;
    private ProgressLoading mPrgLoading;
    private TextView mTvwHeaderTitle, mTvwHeaderCount, mTvwNote, tvVtPlusPoint;
    private TabLayout mTabLayout;
    private AccumulateTabAdapter mTabAdapter;
    private ReengViewPager mViewPager;

    public AccumulateFragment() {
    }

    public static AccumulateFragment newInstance() {
        AccumulateFragment fragment = new AccumulateFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");
        mParentActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mRes = mApplication.getResources();
        AccumulatePointHelper.getInstance(mApplication).addAccumulateListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accumulate, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findViewComponent(rootView);
        if (AccumulatePointHelper.getInstance(mApplication).isReady()) {
            showDetail();
            drawDetail();
        } else {
            showLoading();
        }
        AccumulatePointHelper.getInstance(mApplication).getAccumulateDetail(this);
        setViewListener();
        return rootView;
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        AccumulatePointHelper.getInstance(mApplication).removeAccumulateListener(this);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(AccumulatePointHelper.TAG_GET_DETAIL);
        AccumulatePointHelper.getInstance(mApplication).resetData();
        super.onDetach();
    }

    @Override
    public void onAccumulateDataChanged(final boolean changeList) {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String sPoint = NumberFormat.getNumberInstance(Locale.UK).format(
                        Integer.valueOf(AccumulatePointHelper.getInstance(mApplication).getTotalPoint())).replaceAll(",", ".");
                String headerCount = String.format(mRes.getString(R.string.accumulate_header_count),
                        sPoint);
                mTvwHeaderCount.setText(TextHelper.fromHtml(headerCount));
                mTvwHeaderCount.setSelected(true);

                String vPoint = NumberFormat.getNumberInstance(Locale.UK).format(
                        Integer.valueOf(AccumulatePointHelper.getInstance(mApplication).getViettelPlusPoint())).replaceAll(",", ".");
                String vtPlusPoint = String.format(mRes.getString(R.string.accumulate_header_count_vtplus), vPoint);
                tvVtPlusPoint.setText(TextHelper.fromHtml(vtPlusPoint));
            }
        });
    }

    @Override
    public void onSuccess() {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDetail();
                drawDetail();
            }
        });
    }

    @Override
    public void onError(final int errorCode, String desc) {
        Log.d(TAG, "onError: " + desc);
        if (!AccumulatePointHelper.getInstance(mApplication).isReady()) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String msg;
                    if (errorCode == -2) {
                        msg = mRes.getString(R.string.error_internet_disconnect);
                    } else {
                        msg = mRes.getString(R.string.e601_error_but_undefined);
                    }
                    showEmptyNote(msg);
                }
            });
        }
    }

    private void setToolbar(LayoutInflater inflater) {
        View abView = mParentActivity.getToolBarView();
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        ImageView mBtnBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        TextView mTvwTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mRes.getString(R.string.accumulate_title));
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

        }
    }

    private void findViewComponent(View rootView) {
        mPrgLoading = (ProgressLoading) rootView.findViewById(R.id.accumulate_loading);
        mTvwNote = (TextView) rootView.findViewById(R.id.accumulate_note);
        mViewHeader = (LinearLayout) rootView.findViewById(R.id.accumulate_header);
        mTvwHeaderTitle = (TextView) rootView.findViewById(R.id.accumulate_header_title);
        mTvwHeaderCount = (TextView) rootView.findViewById(R.id.accumulate_header_count);
        mViewContent = (LinearLayout) rootView.findViewById(R.id.accumulate_content);
        mTabLayout = (TabLayout) rootView.findViewById(R.id.accumulate_tabs);
        mViewPager = (ReengViewPager) rootView.findViewById(R.id.accumulate_pager);
        tvVtPlusPoint = rootView.findViewById(R.id.accumulate_header_count_vtplus);
    }

    private void showEmptyNote(String content) {
        mViewHeader.setVisibility(View.GONE);
        mViewContent.setVisibility(View.GONE);
        mPrgLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.VISIBLE);
        mTvwNote.setText(content);
    }

    private void showLoading() {
        mViewHeader.setVisibility(View.GONE);
        mViewContent.setVisibility(View.GONE);
        mPrgLoading.setVisibility(View.VISIBLE);
        mTvwNote.setVisibility(View.GONE);
    }

    private void showDetail() {
        mViewHeader.setVisibility(View.VISIBLE);
        mViewContent.setVisibility(View.VISIBLE);
        mPrgLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.GONE);
    }

    private void drawDetail() {
        /*String myRawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, mApplication
                .getReengAccountBusiness().getJidNumber());*/
        String myRawNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String headerTitle = String.format(mRes.getString(R.string.accumulate_header_title), myRawNumber);

        String sPoint = NumberFormat.getNumberInstance(Locale.UK).format(
                Integer.valueOf(AccumulatePointHelper.getInstance(mApplication).getTotalPoint())).replaceAll(",", ".");
        String vPoint = NumberFormat.getNumberInstance(Locale.UK).format(
                Integer.valueOf(AccumulatePointHelper.getInstance(mApplication).getViettelPlusPoint())).replaceAll(",", ".");

        String headerCount = String.format(mRes.getString(R.string.accumulate_header_count), sPoint);
        String vtPlusPoint = String.format(mRes.getString(R.string.accumulate_header_count_vtplus), vPoint);
        mTvwHeaderTitle.setText(TextHelper.fromHtml(headerTitle));
        mTvwHeaderCount.setText(TextHelper.fromHtml(headerCount));
        tvVtPlusPoint.setText(TextHelper.fromHtml(vtPlusPoint));
        mTvwHeaderCount.setSelected(true);
        initAdapter();
    }

    private void setViewListener() {

    }

    private void initAdapter() {
        if (mTabAdapter == null) {
            ArrayList<String> titles = new ArrayList<>();
            titles.add(mRes.getString(R.string.accumulate_tab_make));
            titles.add(mRes.getString(R.string.accumulate_tab_exchange));
            titles.add(mRes.getString(R.string.accumulate_tab_history));
            mTabAdapter = new AccumulateTabAdapter(getFragmentManager(), mParentActivity, titles);
            mViewPager.setAdapter(mTabAdapter);
            //mViewPager.addOnPageChangeListener(mPageChangeListener);
            mViewPager.setOffscreenPageLimit(2);
            mViewPager.setCurrentItem(0, false);
            mTabLayout.setupWithViewPager(mViewPager);
            /*for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                tab.setCustomView(mTabAdapter.getTabView(i));
            }*/
        } else {
            AccumulatePointHelper.getInstance(mApplication).onAccumulateChanged(true);
        }
    }
}
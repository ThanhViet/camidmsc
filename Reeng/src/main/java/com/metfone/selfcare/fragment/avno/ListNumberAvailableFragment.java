package com.metfone.selfcare.fragment.avno;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.adapter.avno.AVNONumberAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.avno.NumberAVNO;
import com.metfone.selfcare.database.model.avno.ResultOtp;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.fragment.setting.EditProfileFragment;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogInputOtp;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by thanhnt72 on 2/24/2018.
 */

public class ListNumberAvailableFragment extends BaseRecyclerViewFragment {

    private static final String TAG = ListNumberAvailableFragment.class.getSimpleName();

    private AVNOActivity mActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private RecyclerView mRecyclerListNumber;
    private TextView mTvwMochaSuggest;
    private AVNONumberAdapter avnoNumberAdapter;

    private ImageView mImgSearch;
    private TextView mTvwSearch;
    private EllipsisTextView mTvwTitle;
    private ReengSearchView mViewSearch;

    private LinearLayout mLoadmoreFooterView;

    private HeaderAndFooterRecyclerViewAdapter wrapperAdapter;

    private LayoutInflater inflater;

    private boolean isLoading = false;
    private int pageSearch = 1;
    private int pageSuggest = 1;
    private boolean listEnd;

    private int callBackSuccess;

    public static ListNumberAvailableFragment newInstance(int callBackSuccess) {
        ListNumberAvailableFragment fragment = new ListNumberAvailableFragment();
        Bundle args = new Bundle();
        args.putInt(AVNOActivity.CALL_BACK_SUCCESS, callBackSuccess);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AVNOActivity) activity;
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            callBackSuccess = getArguments().getInt(AVNOActivity.CALL_BACK_SUCCESS);
        }
        View rootView = inflater.inflate(R.layout.fragment_list_number_avno, container, false);
        this.inflater = inflater;

        setToolbar(rootView);
        setViewComponent(rootView, inflater, container);
        setViewListener();
        getListNumberSuggest();
        Log.i(TAG, "onCreateView");
        return rootView;
    }

    private void getListNumberSuggest() {
        AVNOHelper.getInstance(mApplication).getSuggestNumberAVNO(new AVNOHelper.GetListNumberAVNOLIstener() {
            @Override
            public void onSuccess(ArrayList<NumberAVNO> listNumber) {
                Log.i(TAG, "onSuccess: " + listNumber.size());
                mLoadmoreFooterView.setVisibility(View.GONE);
                isLoading = false;
                hideEmptyView();
                mTvwMochaSuggest.setVisibility(View.VISIBLE);
                if (listNumber.isEmpty()) listEnd = true;
                else {
                    avnoNumberAdapter.addListItem(listNumber);
                    avnoNumberAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(int code, String msg) {
                hideEmptyView();
                mLoadmoreFooterView.setVisibility(View.GONE);
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        }, pageSuggest);
    }

    public void setToolbar(View rootView) {
        mImgSearch = rootView.findViewById(R.id.img_search);
        mTvwSearch = rootView.findViewById(R.id.btnSearch);
        mViewSearch = rootView.findViewById(R.id.ab_search_view);
        mViewSearch.setVisibility(View.GONE);
        ImageView mBtnBack = rootView.findViewById(R.id.ab_back_btn);
        mTvwTitle = rootView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mRes.getString(R.string.avno_title_select));
        mTvwTitle.setVisibility(View.VISIBLE);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });
        mImgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTvwTitle.setVisibility(View.GONE);
                mViewSearch.setVisibility(View.VISIBLE);
                mViewSearch.requestFocus();
                mTvwSearch.setVisibility(View.VISIBLE);
                mImgSearch.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context
                        .INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });

        mViewSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)) {
                    mTvwSearch.setTextColor(ContextCompat.getColor(mActivity, R.color.color_un_sub));
                } else {
                    mTvwSearch.setTextColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTvwSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageSearch = 1;
                handleSearchNumber();
                mActivity.hideKeyboard();
            }
        });
    }

    private DialogInputOtp dialogInputOtp;
    private String sectionIdOtp;
    private boolean isResend = false;

    private void setViewListener() {
        avnoNumberAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
//                mActivity.showLoadingDialog("", R.string.loading);
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                Log.d(TAG, "số sim ảo hiện tại : " + account.getAvnoNumber());

                if (!TextUtils.isEmpty(account.getAvnoICBack()) && !TextUtils.isEmpty(account.getAvnoICFront())) {
                    NumberAVNO registerAVNO = (NumberAVNO) object;
                    Log.d(TAG, "register AVNO : " + registerAVNO);
                    // TODO: 3/30/2020 Bo sung luong Otp
                    String phoneNuber = registerAVNO.getNumber();
                    phoneNuber = phoneNuber.replace("+84", "0");
                    AVNOHelper.getInstance(mApplication).createOtpBuyPackgeAVNO(phoneNuber, new AVNOHelper.GetResultOtpAVNOListener() {
                        @Override
                        public void onGetResultOtpAVNOSuccess(ResultOtp resultOtp) {
                            dialogInputOtp = new DialogInputOtp(mActivity) {
                                @Override
                                public void onSubmitBuyOtp(String otp) {
                                    mActivity.showLoadingDialog("", R.string.loading);
                                    if (!isResend) {
                                        sectionIdOtp = resultOtp.getSession_id();
                                    }
                                    AVNOHelper.getInstance(mApplication).buyVituralNumberByOtpCode(mActivity, registerAVNO, otp, sectionIdOtp, new AVNOHelper.RegisterAVNOListener() {
                                        @Override
                                        public void onSuccess(String msg) {
                                            mActivity.hideLoadingDialog();
                                            // TODO: 3/30/2020 Sau khi mua thanh cong 
                                            if (dialogInputOtp != null) dialogInputOtp.dismiss();
                                            DialogConfirm dialogConfirm = new DialogConfirm(mActivity, false);
                                            dialogConfirm.setMessage(msg);
                                            dialogConfirm.setPositiveLabel(mActivity.getResources().getString(R.string.close));
                                            dialogConfirm.show();
                                            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                                                @Override
                                                public void onPositive(Object result) {
                                                    mActivity.finish();
                                                }
                                            });
                                        }

                                        @Override
                                        public void onError(int code, String msg) {
                                            // TODO: 3/30/2020 Mua that bai
                                            mActivity.hideLoadingDialog();
                                            if (code == HTTPCode.E0_OK) {
                                                if (dialogInputOtp != null) {
                                                    dialogInputOtp.clearTextOtp();
                                                    dialogInputOtp.showContentResent();
                                                }
                                            } else {
                                                showDialogConfirmResult(msg);
                                                dismiss();
                                            }
                                        }
                                    });
                                    isResend = false;
                                }

                                @Override
                                public void onCancel() {
                                    this.dismiss();
                                }

                                @Override
                                public void onResend() {
                                    if (dialogInputOtp != null) {
                                        dialogInputOtp.setTime(180);
                                        dialogInputOtp.initCoundownTimer();
                                    }
                                    AVNOHelper.getInstance(mApplication).createOtpBuyPackgeAVNO(registerAVNO.getNumber(), new AVNOHelper.GetResultOtpAVNOListener() {
                                        @Override
                                        public void onGetResultOtpAVNOSuccess(ResultOtp resultOtp) {
                                            sectionIdOtp = resultOtp.getSession_id();
                                            isResend = true;
                                        }

                                        @Override
                                        public void onGetResultOtpAVNOError(int code, String msg) {
                                            showDialogConfirmResult(msg);
                                        }
                                    });
                                }
                            };
                            dialogInputOtp.setCanceledOnTouchOutside(false);
                            dialogInputOtp.setCancelable(false);
                            dialogInputOtp.show();
                        }

                        @Override
                        public void onGetResultOtpAVNOError(int code, String msg) {
                            showDialogConfirmResult(msg);
                            if (dialogInputOtp != null) dialogInputOtp.dismiss();
                        }
                    });
                } else {
                    DialogConfirm dialogMessage = new DialogConfirm(mActivity, false);
                    dialogMessage.setMessage(mRes.getString(R.string.avno_update_cmnd));
                    dialogMessage.setPositiveLabel(mRes.getString(R.string.ok));
                    dialogMessage.setNegativeLabel(mRes.getString(R.string.cancel));
                    dialogMessage.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            EditProfileFragment.startActivity(mActivity, true);
//                            NavigateActivityHelper.navigateToMyProfile(mActivity, true);
                        }
                    });
                    dialogMessage.show();
                }

            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });

        mRecyclerListNumber.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(new RecyclerView
                .OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (avnoNumberAdapter.getListItem() == null || avnoNumberAdapter.getListItem().isEmpty()) {
                    return;
                }
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager mRecyclerManager = (LinearLayoutManager) mRecyclerListNumber.getLayoutManager();
                    if (mRecyclerManager == null) return;
                    visibleItemCount = mRecyclerManager.getChildCount();
                    totalItemCount = mRecyclerManager.getItemCount();
                    pastVisiblesItems = mRecyclerManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !listEnd) {
                        if (mViewSearch.getVisibility() != View.VISIBLE) {
                            onLoadMoreSuggest();
                        } else {
                            Log.i(TAG, "onLoadMoreSearch");
                            onLoadMoreSearch();
                        }
                    }
                }
            }
        }));

        mViewSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo
                        .IME_ACTION_DONE)) {
                    pageSearch = 1;
                    handleSearchNumber();
                }
                return false;
            }
        });
    }

    private void showDialogConfirmResult(String msg) {
        if (mActivity == null) return;
        DialogConfirm dialogConfirm = new DialogConfirm(mActivity, false);
        dialogConfirm.setMessage(msg);
        dialogConfirm.setPositiveLabel(mActivity.getResources().getString(R.string.close));
        dialogConfirm.show();
    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public void setViewComponent(View rootView, LayoutInflater inflater, ViewGroup container) {
        mRecyclerListNumber = (RecyclerView) rootView.findViewById(R.id.recycler_view_list_number_avno);
        createView(inflater, mRecyclerListNumber, null);
        mTvwMochaSuggest = (TextView) rootView.findViewById(R.id.tvw_mocha_suggest);
        mRecyclerListNumber.setLayoutManager(new LinearLayoutManager(mApplication));

        View mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLoadmoreFooterView = mFooterView.findViewById(R.id.load_more_layout);
        mLoadmoreFooterView.setVisibility(View.GONE);

        avnoNumberAdapter = new AVNONumberAdapter(mApplication, new ArrayList<NumberAVNO>());
        wrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(avnoNumberAdapter);
        wrapperAdapter.addFooterView(mFooterView);
        mRecyclerListNumber.setAdapter(wrapperAdapter);

        mTvwMochaSuggest.setVisibility(View.GONE);
        showProgressLoading();
    }

    private void onLoadMoreSearch() {
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        isLoading = true;
        pageSearch = pageSearch + 1;
        handleSearchNumber();
    }

    private void onLoadMoreSuggest() {
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        isLoading = true;
        pageSuggest = pageSuggest + 1;
        getListNumberSuggest();
    }

    private void handleSearchNumber() {
        final String textSearch = mViewSearch.getText().toString().trim();
        if (!TextUtils.isEmpty(textSearch)) {
            if (pageSearch == 1) {
                showProgressLoading();
            }
            isLoading = true;
            listEnd = false;
            AVNOHelper.getInstance(mApplication).searchNumberAVNO(textSearch, new
                    AVNOHelper.GetListNumberAVNOLIstener() {

                        @Override
                        public void onSuccess(ArrayList<NumberAVNO> listNumber) {
                            Log.i(TAG, "onSuccess: " + listNumber.size());
                            isLoading = false;
                            mLoadmoreFooterView.setVisibility(View.GONE);
                            hideEmptyView();
                            if (listNumber.isEmpty()) {
                                if (pageSearch == 1) {
                                    mTvwMochaSuggest.setText(String.format(mRes.getString(R.string
                                            .avno_search_no_result), textSearch));
//                                    showEmptyNote(mRes.getString(R.string.avno_no_number_available));
                                    avnoNumberAdapter.setListItem(listNumber);
                                    avnoNumberAdapter.notifyDataSetChanged();
                                }
                                listEnd = true;
                            } else {
                                if (pageSearch == 1) {
                                    mTvwMochaSuggest.setText(String.format(mRes.getString(R.string
                                            .avno_search_result), textSearch));
                                    avnoNumberAdapter.setListItem(listNumber);
                                } else {
                                    avnoNumberAdapter.addListItem(listNumber);
                                }
                                avnoNumberAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onError(int code, String msg) {
                            hideEmptyView();
                            mLoadmoreFooterView.setVisibility(View.GONE);
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }, pageSearch);
        }
    }
}

package com.metfone.selfcare.fragment.onmedia;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaListLikeShareActivity;
import com.metfone.selfcare.adapter.UserActionAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedAction;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants.ONMEDIA;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.recyclerview.DividerItemDecoration;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 30/09/2015.
 */
public class ListUserLikeFragment extends BaseRecyclerViewFragment implements BaseRecyclerViewFragment
        .EmptyViewListener {
    public String TAG = ListUserLikeFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private OnMediaListLikeShareActivity mParentActivity;
    private UserActionAdapter.FeedUserInteractionListener mListener;
    private Resources mRes;
    private ImageView mAbBack;
    private TextView mAbTitle;
    private RecyclerView mRecyclerView;
    private UserActionAdapter mAdapter;
    private ArrayList<UserInfo> mUserLikes;
    private String url;
    private int mPageLoaded = 0;
    private boolean isLoading = false, isNoMore = false;

    private View mFooterView;
    private LinearLayout mLoadmoreFooterView;

    public static ListUserLikeFragment newInstance(String url) {
        ListUserLikeFragment fragment = new ListUserLikeFragment();
        Bundle args = new Bundle();
        args.putString(ONMEDIA.EXTRAS_DATA, url);
        fragment.setArguments(args);
        return fragment;
    }

    public ListUserLikeFragment() {

    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof Activity) {
            this.mParentActivity = (OnMediaListLikeShareActivity) context;
            this.mApplication = (ApplicationController) context.getApplicationContext();
            this.mRes = mApplication.getResources();
            try {
                mListener = (UserActionAdapter.FeedUserInteractionListener) context;
            } catch (ClassCastException e) {
                Log.e(TAG, "ClassCastException", e);
                throw new ClassCastException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mParentActivity.getToolBarView().setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        setActionBar(v, inflater);
        findComponentViews(v, inflater, container);
        initDataView(savedInstanceState);
        setListener();
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ONMEDIA.EXTRAS_DATA, url);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        /*mTvwNotEmpty = (TextView) rootView.findViewById(R.id.text_note_empty);
        mPrgLoading = (ProgressLoading) rootView.findViewById(R.id.progress);
        mPrgLoading.setVisibility(View.VISIBLE);*/

        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLoadmoreFooterView = (LinearLayout) mFooterView.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);

    }

    private void initDataView(Bundle savedInstanceState) {
        if (getArguments() != null) {
            this.url = getArguments().getString(ONMEDIA.EXTRAS_DATA);
        } else if (savedInstanceState != null) {
            this.url = savedInstanceState.getString(ONMEDIA.EXTRAS_DATA);
        }
        if (!TextUtils.isEmpty(url)) {
            requestAndDrawDetail(1);
        }
    }

    private void setListener() {
        setListViewListener();
    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void setListViewListener() {
        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mRecyclerView == null) return;
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !isNoMore) {
                        Log.i(TAG, "needToLoad");
                        requestAndDrawDetail(mPageLoaded + 1);
                    }
                }
            }
        };

        mRecyclerView.addOnScrollListener(mOnScrollListener);
    }

    private void requestAndDrawDetail(final int page) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            hideEmptyView();
            if (page == 1) {
                showRetryView();
            }
            return;
        }
        if (page > 1) {
            mLoadmoreFooterView.setVisibility(View.VISIBLE);
        } else {
            showProgressLoading();
        }
        isLoading = true;
        WSOnMedia rest = new WSOnMedia(mApplication);
        rest.getLikeOrShare(url, page, new OnMediaInterfaceListener.GetListFeedActionListener() {
            @Override
            public void onGetListFeedAction(RestAllFeedAction restAllFeedAction) {
                hideEmptyView();
                isLoading = false;
                mLoadmoreFooterView.setVisibility(View.GONE);
                if (restAllFeedAction.getCode() == HTTPCode.E200_OK) {
                    processResponse(restAllFeedAction.getUserLikes(), page);
                }
            }
        } , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isLoading = false;
                if (page == 1) {
                    showEmptyNote(mRes.getString(R.string.e601_error_but_undefined));
                }
                mLoadmoreFooterView.setVisibility(View.GONE);
                Log.e(TAG, "requestAndDrawDetail", volleyError);
            }
        }, true);
    }

    private void processResponse(ArrayList<UserInfo> result, int page) {
        mLoadmoreFooterView.setVisibility(View.GONE);
        if (result == null) {
            this.mPageLoaded = page - 1;
            isNoMore = true;
            if (mUserLikes == null || mUserLikes.isEmpty()) {
                showEmptyNote(mRes.getString(R.string.e601_error_but_undefined));
            } else {
                hideEmptyView();
            }
        } else {
            hideEmptyView();
            this.mPageLoaded = page;
            if (!result.isEmpty()) {
                isNoMore = false;
                if (page == 1) {
                    this.mUserLikes = result;
                } else {
                    mUserLikes.addAll(result);
                }
                notifyAdapter();
            } else {
                isNoMore = true;
                if(page == 1){
                    showEmptyNote(mRes.getString(R.string.list_empty));
                }
//                mParentActivity.showToast(R.string.msg_loadmore_empty);
            }
        }
    }

    private void notifyAdapter() {
        if (mUserLikes == null) {
            mUserLikes = new ArrayList<>();
        }
        if (mAdapter == null) {
            mAdapter = new UserActionAdapter(mApplication, mUserLikes, mListener);

            HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter =
                    new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setAdapter(mHeaderAndFooterRecyclerViewAdapter);
            mHeaderAndFooterRecyclerViewAdapter.addFooterView(mFooterView);
        } else {
            mAdapter.setListData(mUserLikes);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setActionBar(View v, LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_contacts, null));
        mParentActivity.getToolBarView().setVisibility(View.VISIBLE);
        View abView = mParentActivity.getToolBarView();
        View agreeText = abView.findViewById(R.id.ab_agree_text);
        agreeText.setVisibility(View.GONE);
        View viewSearch = abView.findViewById(R.id.ab_search_btn);
        viewSearch.setVisibility(View.GONE);
        View viewMore = abView.findViewById(R.id.ab_more_btn);
        viewMore.setVisibility(View.GONE);



        mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mParentActivity.onBackPressed();
            }
        });
        mAbTitle = (TextView) abView.findViewById(R.id.ab_title);
        mAbTitle.setText(mRes.getString(R.string.onmedia_title_user_likes));
        mAbBack.setColorFilter(ContextCompat.getColor(mParentActivity, R.color.gray));
    }

    @Override
    public void onRetryClick() {
        requestAndDrawDetail(1);
    }
}
package com.metfone.selfcare.fragment.viettelIQ;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ViettelIQActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ViettelIQApi;
import com.metfone.selfcare.database.model.viettelIQ.AnswerIQ;
import com.metfone.selfcare.database.model.viettelIQ.QuestionIQ;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.GameIQHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.ui.dialog.DialogGameIQ;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class QuestionViettelIQFragment extends Fragment {

    private static final String TAG = QuestionViettelIQFragment.class.getSimpleName();

    private static final String QUESTION_IQ = "QuestionIQ";
    private static final String TOTAL_QUESTION = "totalQuestion";
    private static final String GAME_ID = "gameId";
    private static final String MATCH_ID = "matchId";
    private static final String START_TIME = "startTime";
    private static final String ANSWER_TIME = "answerTime";
    private static final String DISPLAY_TIME = "displayTime";

    @BindView(R.id.iv_status_question)
    ImageView ivStatusQuestion;
    @BindView(R.id.tv_count_down_answer)
    TextView tvCountDownAnswer;
    @BindView(R.id.tv_question)
    TextView tvQuestion;
    @BindView(R.id.tv_answer_1)
    TextView tvAnswer1;
    @BindView(R.id.tv_number_player_selected_1)
    TextView tvNumberPlayerSelected1;
    @BindView(R.id.root_answer_1)
    RoundLinearLayout rootAnswer1;
    @BindView(R.id.tv_answer_2)
    TextView tvAnswer2;
    @BindView(R.id.tv_number_player_selected_2)
    TextView tvNumberPlayerSelected2;
    @BindView(R.id.root_answer_2)
    RoundLinearLayout rootAnswer2;
    @BindView(R.id.tv_answer_3)
    TextView tvAnswer3;
    @BindView(R.id.tv_number_player_selected_3)
    TextView tvNumberPlayerSelected3;
    @BindView(R.id.root_answer_3)
    RoundLinearLayout rootAnswer3;
    @BindView(R.id.tv_count_down_next_question)
    TextView tvCountDownNextQuestion;
    @BindView(R.id.tv_status_player)
    TextView tvStatusPlayer;
    Unbinder unbinder;

    public static QuestionViettelIQFragment newInstance(QuestionIQ questionIQ,
                                                        String currentIdGame,
                                                        String matchId,
                                                        int totalQuestion,
                                                        long startTime,
                                                        long answerTime,
                                                        long displayTime) {
        Bundle args = new Bundle();
        args.putSerializable(QUESTION_IQ, questionIQ);
        args.putInt(TOTAL_QUESTION, totalQuestion);
        args.putLong(DISPLAY_TIME, displayTime);
        args.putString(GAME_ID, currentIdGame);
        args.putLong(ANSWER_TIME, answerTime);
        args.putLong(START_TIME, startTime);
        args.putString(MATCH_ID, matchId);
        QuestionViettelIQFragment fragment = new QuestionViettelIQFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private int totalQuestion;

    private long startTime;
    private long answerTime;
    private long displayTime;

    private boolean isCanAnswer = true;

    private String gameId;
    private String matchId;

    private QuestionIQ currentQuestionIQ;
    private QuestionIQ nextQuestionIQ;

    private String preSelected = "-1";
    private String selected = "-1";

    private String answerTrue = "";

    private boolean isAudioPlayed = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentQuestionIQ = (QuestionIQ) getArguments().getSerializable(QUESTION_IQ);
        totalQuestion = getArguments().getInt(TOTAL_QUESTION);
        displayTime = getArguments().getLong(DISPLAY_TIME);
        answerTime = getArguments().getLong(ANSWER_TIME);
        startTime = getArguments().getLong(START_TIME);
        matchId = getArguments().getString(MATCH_ID);
        gameId = getArguments().getString(GAME_ID);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viettel_iq_question, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (tvQuestion == null) return;
        tvQuestion.removeCallbacks(scheduleDisplayAnswerRunnable);
        tvQuestion.removeCallbacks(scheduleAnswerRunnable);
        tvQuestion.removeCallbacks(scheduleGetStat);
    }

    @OnClick({R.id.root_answer_1, R.id.root_answer_2, R.id.root_answer_3})
    public void onViewClicked(View view) {
        ((ViettelIQActivity) getActivity()).playSoundClick();
        if (!isCanAnswer) {
            if (!isShowingAnswer) {
                tvCountDownNextQuestion.setText(getResources().getString(R.string.gameiq_cant_answer));
                tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
            }
            return;
        }
        AnswerIQ answerIQ = null;
        switch (view.getId()) {
            case R.id.root_answer_1:
                changeColorAnswer(rootAnswer1);
                changeColorNormal(rootAnswer2);
                changeColorNormal(rootAnswer3);
                answerIQ = currentQuestionIQ.getListResponse().get(0);
                break;
            case R.id.root_answer_2:
                changeColorAnswer(rootAnswer2);
                changeColorNormal(rootAnswer1);
                changeColorNormal(rootAnswer3);
                answerIQ = currentQuestionIQ.getListResponse().get(1);
                break;
            case R.id.root_answer_3:
                changeColorAnswer(rootAnswer3);
                changeColorNormal(rootAnswer2);
                changeColorNormal(rootAnswer1);
                answerIQ = currentQuestionIQ.getListResponse().get(2);
                break;
        }
        if (answerIQ != null) {
            String temp = selected;
            preSelected = temp;
            selected = answerIQ.getId();
            ViettelIQApi.getInstance().answer(gameId, matchId, currentQuestionIQ.getId(), currentQuestionIQ.getQuestInx(), currentQuestionIQ.getContent(), answerIQ.getContent(), preSelected, selected, this);
        }
    }

    public void onAnswer(String decryptData) throws JSONException {
        Log.i(TAG, "onAnswer: " + decryptData);
        JSONObject jsonObject = new JSONObject(decryptData);
        int code = jsonObject.optInt("code");
        updateServerTime(jsonObject);
        if (code == 200) {
//            answerTrue = jsonObject.optString("correctId");
            long numPlayer = jsonObject.optLong("numPlayer");
            long numViewer = jsonObject.optLong("numViewer");
            Log.i(TAG, "-----IQ-----onAnswer: numPlayer " + numPlayer + " numView: " + numViewer);
            EventBus.getDefault().post(new GameIQHelper.IQGameEvent(gameId, numPlayer, numViewer));
            nextQuestionIQ = ApplicationController.self().getGson().fromJson(jsonObject.optString("nextQuestion"), QuestionIQ.class);
        } else if (code == 205) {    //câu trả lời hiện tại khác câu hỏi đang dùng trên sv, gọi lại sub
            Log.f(TAG, "-----GAME IQ----- câu trả lời hiện tại khác câu hỏi đang dùng trên sv, gọi lại sub");
            Activity activity = getActivity();
            if (activity instanceof ViettelIQActivity)
                ((ViettelIQActivity) activity).getSub();
        }
    }

    public void onUseHeart(String decryptData) throws JSONException {
        Activity activity = getActivity();
        if (activity instanceof ViettelIQActivity) {
            ViettelIQActivity act = (ViettelIQActivity) activity;

            act.hideLoadingDialog();
            JSONObject jsonObject = new JSONObject(decryptData);
            int code = jsonObject.optInt("code");
            updateServerTime(jsonObject);
            String desc = jsonObject.optString("desc");
            if (code == 200) {
                int hearts = jsonObject.optInt("hearts");
                act.setHeartRemain(hearts);
                usedHeart = true;
                act.setPlayer(true);
                setStatePlayer();
                act.setHeartUsed(true);
            } else if (code == 209 || code == 210) {
                act.showToast(desc);
            } else {
                act.showToast(R.string.e601_error_but_undefined);
            }
        }
    }

    private void initView() {
        if (tvQuestion == null || currentQuestionIQ == null) return;
        setStatePlayer();

        tvQuestion.setText(currentQuestionIQ.getContent());
        setDataAnswer(false);

        tvQuestion.removeCallbacks(scheduleAnswerRunnable);
        tvQuestion.postDelayed(scheduleAnswerRunnable, 100);
    }

    public void setStatePlayer() {
        Activity activity = getActivity();
        if (activity instanceof ViettelIQActivity) {
            if (((ViettelIQActivity) activity).isPlayer()) {
                tvStatusPlayer.setText(getResources().getString(R.string.you_are_playing));
                tvStatusPlayer.setTextColor(Color.parseColor("#6bff08"));
            } else {
                tvStatusPlayer.setText(getResources().getString(R.string.you_are_watching));
                tvStatusPlayer.setTextColor(Color.parseColor("#c1b5ff"));
                disableAnswer();
            }
        }
    }

    private boolean getStated = false;
    private Runnable scheduleAnswerRunnable = new Runnable() {
        @Override
        public void run() {
            if (tvQuestion == null) return;
            if (currentQuestionIQ == null) {
                ReportHelper.reportError(ApplicationController.self(), "ON_ANSWER", "currentQuestionIQ == null");
                showDialogRetry();
                return;
            }
            long timeEndAnswer = (startTime + ((answerTime + displayTime) * (currentQuestionIQ.getQuestInx() - 1) + answerTime) * 1000) - ((ViettelIQActivity) getActivity()).getCurrentTime();
            if (timeEndAnswer <= 100) {
                Log.i(TAG, "timeEndAnswer <= 100: " + timeEndAnswer);
//                showAnswer();
//                disableAnswer();*/
                tvQuestion.removeCallbacks(scheduleGetStat);

                long timeDelay;
                if (((ViettelIQActivity) getActivity()).isPlayer())
                    timeDelay = 3000;
                else {
                    if (timeEndAnswer <= (6 - displayTime) * 1000) {
                        timeDelay = 0;
                    } else {
                        timeDelay = 6000 + timeEndAnswer;
                        if (timeDelay < 0) timeDelay = 0;
                    }
                    /*ivStatusQuestion.setVisibility(View.GONE);
                        tvCountDownAnswer.setVisibility(View.VISIBLE);
                        tvCountDownAnswer.setText("0");
                    */
                    tvCountDownAnswer.setVisibility(View.GONE);
                    ivStatusQuestion.setVisibility(View.VISIBLE);
                    ivStatusQuestion.setImageResource(R.drawable.ic_game_iq_clock);
                    tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_wait));
                    tvCountDownNextQuestion.setText(getResources().getString(R.string.gameiq_waiting_for_get_stat));
                }
                Log.i(TAG, "timeDelay: " + timeDelay);

                /*if (timeEndAnswer <= (6 - displayTime) * 1000)
                    timeDelay = 0;
                else {
                    timeDelay = ((ViettelIQActivity) getActivity()).isPlayer() ? 3 * 1000 : 6 * 1000;
                }*/
                tvQuestion.postDelayed(scheduleGetStat, timeDelay);
                getStated = true;
                waitingForGetStat = true;


                tvQuestion.removeCallbacks(scheduleDisplayAnswerRunnable);
                tvQuestion.postDelayed(scheduleDisplayAnswerRunnable, 200);

                disableAnswer();
            } else {
                ivStatusQuestion.setVisibility(View.GONE);
                tvCountDownAnswer.setVisibility(View.VISIBLE);

                long timeRemain = Math.min(timeEndAnswer / 1000, answerTime);
                if (timeRemain <= 0) {
                    Log.i(TAG, "timeRemain <= 0");
                    isShowingAnswer = true;
//                    if (waitingForGetStat) {
                    tvCountDownAnswer.setVisibility(View.GONE);
                    ivStatusQuestion.setVisibility(View.VISIBLE);
                    ivStatusQuestion.setImageResource(R.drawable.ic_game_iq_clock);
                    tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_wait));
                    tvCountDownNextQuestion.setText(getResources().getString(R.string.gameiq_waiting_for_get_stat));
                    /*} else {
                        tvCountDownNextQuestion.setText(
                                String.format(getResources().getString(R.string.gameiq_next_quest),
                                        String.valueOf(((ViettelIQActivity) getActivity()).getDisplayTime())));
                        tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_green));
                    }*/
                }
                /*if (timeEndAnswer > 0 && timeEndAnswer < 700) {
                    Log.i(TAG, "need disable answer");
                    disableAnswer();
                }*/
                tvCountDownAnswer.setText(String.valueOf(timeRemain));
                tvQuestion.removeCallbacks(this);
                tvQuestion.postDelayed(this, 100);
            }
        }
    };

    public void onStat(String decryptData) throws JSONException {
        if (tvQuestion == null) return;
        waitingForGetStat = false;
        Log.i(TAG, "onStat: " + decryptData);
        tvQuestion.removeCallbacks(scheduleGetStat);
        JSONObject jsonObject = new JSONObject(decryptData);
        updateServerTime(jsonObject);

        QuestionIQ questionIQ = ApplicationController.self().getGson().fromJson(jsonObject.optString("currentQuestion"), QuestionIQ.class);
        for (AnswerIQ answerIQ : currentQuestionIQ.getListResponse()) {
            for (AnswerIQ answer : questionIQ.getListResponse()) {
                if (answerIQ.getId().equals(answer.getId())) {
                    answerIQ.setStat(answer.getStat());
//                    answerIQ.setIsCorrect(answer.getIsCorrect());
                }
            }
        }
        if (currentQuestionIQ == null) {
            ReportHelper.reportError(ApplicationController.self(), "GET_STAT", "currentQuestionIQ == null | " + decryptData);
            /*
             * không có dữ liệu về câu tiếp theo, có thể do lỗi mạng hoặc api không trả về kịp
             */
            showDialogRetry();
            return;
        }
        answerTrue = jsonObject.optString("correctId");
        /*if (BuildConfig.DEBUG) {
            ((ViettelIQActivity) getActivity()).showToast("selected answer: " + selected + " answer true: " + answerTrue);
        }*/
        /*for (AnswerIQ answerIQ : currentQuestionIQ.getListResponse()) {
            if (answerIQ.getIsCorrect() == 1) {
                answerTrue = answerIQ.getId();
            }
        }*/

        setDataAnswer(true);

        nextQuestionIQ = ApplicationController.self().getGson().fromJson(jsonObject.optString("nextQuestion"), QuestionIQ.class);
        if (nextQuestionIQ == null) {
            ReportHelper.reportError(ApplicationController.self(), "GET_STAT", "nextQuestionIQ == null | " + decryptData);

        }
        boolean canShowDialog = showAnswer();
        long numPlayer = jsonObject.optLong("numPlayer");
        long numViewer = jsonObject.optLong("numViewer");
        Log.i(TAG, "-----IQ----- onStat numPlayer: " + numPlayer + " numViewer: " + numViewer);
        EventBus.getDefault().post(new GameIQHelper.IQGameEvent(gameId, numPlayer, numViewer));

        Log.i(TAG, "canShowDialog: " + canShowDialog);
        if (canShowDialog && (getActivity()) != null && ((ViettelIQActivity) getActivity()).isPlayer())
            showDialogIncorrectAnswer(canUseHeart());
        if (getActivity() != null && nextQuestionIQ != null) {
            ((ViettelIQActivity) getActivity()).setCanUseHeart(nextQuestionIQ.getCanUseHeart());
        }
        /*boolean canShowDialog = showAnswer();
        if (canShowDialog && (getActivity()) != null && ((ViettelIQActivity) getActivity()).isPlayer())
            showDialogIncorrectAnswer(canUseHeart());
        long numPlayer = jsonObject.optLong("numPlayer");
        long numViewer = jsonObject.optLong("numViewer");
        Log.i(TAG, "-----IQ----- onStat numPlayer: " + numPlayer + " numViewer: " + numViewer);
        EventBus.getDefault().post(new GameIQHelper.IQGameEvent(gameId, numPlayer, numViewer));
        if (getActivity() != null && nextQuestionIQ != null) {
            ((ViettelIQActivity) getActivity()).setCanUseHeart(nextQuestionIQ.getCanUseHeart());
        }*/
    }

    private void setDataAnswer(boolean isGetStat) {
        if (currentQuestionIQ == null) return;
        for (int i = 0; i < currentQuestionIQ.getListResponse().size(); i++) {
            AnswerIQ answerIQ = currentQuestionIQ.getListResponse().get(i);
            switch (i) {
                case 0:
                    tvAnswer1.setText(answerIQ.getContent());
                    if (isGetStat) {
                        tvNumberPlayerSelected1.setText(String.valueOf(answerIQ.getStat()));
                        tvNumberPlayerSelected1.setVisibility(View.VISIBLE);
                    } else
                        tvNumberPlayerSelected1.setVisibility(View.GONE);
                    break;

                case 1:
                    tvAnswer2.setText(answerIQ.getContent());
                    if (isGetStat) {
                        tvNumberPlayerSelected2.setText(String.valueOf(answerIQ.getStat()));
                        tvNumberPlayerSelected2.setVisibility(View.VISIBLE);
                    } else
                        tvNumberPlayerSelected2.setVisibility(View.GONE);
                    break;

                case 2:
                    tvAnswer3.setText(answerIQ.getContent());
                    if (isGetStat) {
                        tvNumberPlayerSelected3.setText(String.valueOf(answerIQ.getStat()));
                        tvNumberPlayerSelected3.setVisibility(View.VISIBLE);
                    } else
                        tvNumberPlayerSelected3.setVisibility(View.GONE);
                    break;
            }
        }
    }

    private boolean isShowingAnswer;
    private boolean usedHeart;
    private boolean waitingForGetStat;
    private Runnable scheduleDisplayAnswerRunnable = new Runnable() {
        @Override
        public void run() {
            if (tvQuestion == null || currentQuestionIQ == null) return;
            long timeCountLong = (startTime + (answerTime + displayTime) * currentQuestionIQ.getQuestInx() * 1000 - ((ViettelIQActivity) getActivity()).getCurrentTime());
            long timeCount = timeCountLong / 1000;
            if (timeCountLong < 1000 && timeCount <= 0) {
                currentQuestionIQ.setQuestInx(currentQuestionIQ.getQuestInx() + 1);
                if (currentQuestionIQ.getQuestInx() > totalQuestion) {
                    /*
                     * mở màn hình win
                     */
                    Activity activity = getActivity();
                    if (activity instanceof ViettelIQActivity)
                        ((ViettelIQActivity) activity).openWinner();
                } else {
                    if (nextQuestionIQ == null) {
                        /*
                         * không có dữ liệu về câu tiếp theo, có thể do lỗi mạng hoặc api không trả về kịp
                         */
                        ReportHelper.reportError(ApplicationController.self(), "NEXT_QUESTION", "nextQuestionIQ == null");
                        showDialogRetry();
                    } else {
                        /*
                         * mở câu hỏi tiếp theo
                         */
                        Activity activity = getActivity();
                        if (activity instanceof ViettelIQActivity) {
                            ((ViettelIQActivity) activity).openQuestionByIndex(nextQuestionIQ);
                            if (selected == null || answerTrue == null || answerTrue.isEmpty() || !selected.equals(answerTrue)) {
                                if (!usedHeart) {
                                    ((ViettelIQActivity) activity).setPlayer(false);// người dùng chuyển sang chế độ xem
                                }
                            }
                        }
                    }
                }
            } else {
                if (currentQuestionIQ.getQuestInx() == totalQuestion) {
                    if (waitingForGetStat) {
                        tvCountDownAnswer.setVisibility(View.GONE);
                        ivStatusQuestion.setVisibility(View.VISIBLE);
                        ivStatusQuestion.setImageResource(R.drawable.ic_game_iq_clock);
                        tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_wait));
                        tvCountDownNextQuestion.setText(getResources().getString(R.string.gameiq_waiting_for_get_stat));
                    } else {
//                        ivStatusQuestion.setVisibility(View.GONE);
                        tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_green));
                        tvCountDownNextQuestion.setText(
                                String.format(getResources().getString(R.string.gameiq_next_report), String.valueOf(timeCount)));
                    }
                } else {
                    if (timeCount <= displayTime - 6000 && !getStated) {      //vào sau khi game bắt đầu và đang đếm ngược sang câu tiếp theo
                        Log.i(TAG, "vào sau khi game bắt đầu và đang đếm ngược sang câu tiếp theo timeCount <= displayTime - 6000");
                        tvQuestion.removeCallbacks(scheduleGetStat);
                        tvQuestion.post(scheduleGetStat);
                        getStated = true;
                    }

                    if (waitingForGetStat) {
                        tvCountDownAnswer.setVisibility(View.GONE);
                        ivStatusQuestion.setVisibility(View.VISIBLE);
                        ivStatusQuestion.setImageResource(R.drawable.ic_game_iq_clock);
                        tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_wait));
                        tvCountDownNextQuestion.setText(getResources().getString(R.string.gameiq_waiting_for_get_stat));
                    } else {
                        tvCountDownNextQuestion.setTextColor(ContextCompat.getColor(getContext(), R.color.color_iq_game_green));
                        tvCountDownNextQuestion.setText(
                                String.format(getResources().getString(R.string.gameiq_next_quest), String.valueOf(timeCount)));
                    }
                }

                tvQuestion.removeCallbacks(this);
                tvQuestion.postDelayed(this, 100);
                isShowingAnswer = true;
            }
        }
    };
    private Runnable scheduleGetStat = new Runnable() {
        @Override
        public void run() {
            ViettelIQApi.getInstance().getStat(gameId, matchId, currentQuestionIQ.getId(), currentQuestionIQ.getQuestInx(), QuestionViettelIQFragment.this);
        }
    };

    private void showDialogRetry() {
        DialogMessage dialogMessage = new DialogMessage((BaseSlidingFragmentActivity) getActivity(), false);
        dialogMessage.setLabelButton(getResources().getString(R.string.retry));
        dialogMessage.setMessage(getResources().getString(R.string.e601_error_but_undefined));
        dialogMessage.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                /*
                 * khỏi chạy lại toàn bộ
                 */
                Activity activity = getActivity();
                if (activity instanceof ViettelIQActivity)
                    ((ViettelIQActivity) activity).getSub();
            }
        });
        dialogMessage.show();
    }

    private boolean showAnswer() {
        Log.i(TAG, "showAnswer: " + answerTrue);
        if (Utilities.isEmpty(answerTrue)) return true;
        tvCountDownAnswer.setVisibility(View.GONE);
        ivStatusQuestion.setVisibility(View.VISIBLE);
        if (!selected.equals(answerTrue)) {
            showAnswerFalse();
            ivStatusQuestion.setImageResource(R.drawable.img_viettel_iq_false);
            if (!isAudioPlayed) ((ViettelIQActivity) getActivity()).playResultAudio(false);
            isAudioPlayed = true;
            showAnswerTrue();
            return true;
        } else {
            ivStatusQuestion.setImageResource(R.drawable.img_viettel_iq_true);
            if (!isAudioPlayed) ((ViettelIQActivity) getActivity()).playResultAudio(true);
            isAudioPlayed = true;
            showAnswerTrue();
            return false;
        }
    }

    private void showAnswerTrue() {
        for (int i = 0; i < currentQuestionIQ.getListResponse().size(); i++) {
            AnswerIQ answerIQ = currentQuestionIQ.getListResponse().get(i);
            if (answerIQ.getId().equals(answerTrue)) {
                if (tvAnswer1.getText().toString().equals(answerIQ.getContent())) {

                }
                changeColorAnswerTrue(i == 0 ? rootAnswer1 : i == 1 ? rootAnswer2 : rootAnswer3);
                return;
            }
        }
    }

    private void showAnswerFalse() {

        for (int i = 0; i < currentQuestionIQ.getListResponse().size(); i++) {
            AnswerIQ answerIQ = currentQuestionIQ.getListResponse().get(i);
            if (answerIQ.getId().equals(selected)) {
                changeColorAnswerFalse(i == 0 ? rootAnswer1 : i == 1 ? rootAnswer2 : rootAnswer3);
                return;
            }
        }

    }

    /**
     * cập nhật lại time server
     *
     * @param jsonObject dữ liệu từ server trả về
     */
    private void updateServerTime(JSONObject jsonObject) {
        try {
            long serverTime = jsonObject.optLong("serverTime");
            Activity activity = getActivity();
            if (activity != null && serverTime > 0) {
                ((ViettelIQActivity) activity).setServerTime(serverTime);
                Log.e(TAG, "updateServerTime: " + serverTime + "       " + ((ViettelIQActivity) activity).getDeltaTime());
            }
        } catch (Exception ignored) {
        }
    }

    private boolean canUseHeart() {
        ViettelIQActivity act = (ViettelIQActivity) getActivity();
        if (act == null) return false;
        Log.i(TAG, "getHeartRemain: " + act.getHeartRemain()
                + " getCanUseHeart: " + act.getCanUseHeart()
                + " isplayer: " + act.isPlayer()
                + "heartUsed: " + act.isHeartUsed());
        if (act.getHeartRemain() > 0 && act.getCanUseHeart() == 1 && act.isPlayer() && !act.isHeartUsed())
            return true;
        return false;
    }


    boolean showingDialog;

    private void showDialogIncorrectAnswer(final boolean canUseHeart) {
        Log.i(TAG, "showDialogIncorrectAnswer: " + canUseHeart);
        if (showingDialog) return;

        final BaseSlidingFragmentActivity activity = (BaseSlidingFragmentActivity) getActivity();
        String message = canUseHeart ? getResources().getString(R.string.des_incorrect_can_use_heart_game_iq) :
                getResources().getString(R.string.des_incorrect_game_iq);
        DialogGameIQ dialogGameIQ = new DialogGameIQ(activity, true, canUseHeart);
        dialogGameIQ.setMessage(message);
        dialogGameIQ.setDismissListener(new DismissListener() {
            @Override
            public void onDismiss() {
                showingDialog = false;
            }
        });
        dialogGameIQ.setDialogGameIQListener(new DialogGameIQ.DialogGameIQListener() {

            @Override
            public void onClickWatch() {
                //do nothing
            }

            @Override
            public void onClickUseHeart() {
                if (canUseHeart) {
                    activity.showLoadingDialog("", R.string.loading);
                    ViettelIQApi.getInstance().useHeart(((ViettelIQActivity) activity).getCurrentIdGame(),
                            matchId, currentQuestionIQ.getId(), currentQuestionIQ.getQuestInx(),
                            ((ViettelIQActivity) activity).getHeartRemain(), QuestionViettelIQFragment.this);
                } else {
                    String deeplink = "mocha://campaign?ref=gameIQ." + String.valueOf(gameId);
                    DeepLinkHelper.getInstance().openSchemaLink(activity, deeplink);
                }
            }
        });
        showingDialog = true;
        dialogGameIQ.show();
    }

    private void changeColorAnswerTrue(RoundLinearLayout roundTextView) {
        roundTextView.setStroke(Color.parseColor("#ffffff"), 1);
        roundTextView.setBackgroundColorRound(Color.parseColor("#6bff08"));
        if (roundTextView == rootAnswer1) {
            tvAnswer1.setTextColor(Color.parseColor("#3f29b1"));
            tvNumberPlayerSelected1.setTextColor(Color.parseColor("#3f29b1"));
        } else if (roundTextView == rootAnswer2) {
            tvAnswer2.setTextColor(Color.parseColor("#3f29b1"));
            tvNumberPlayerSelected2.setTextColor(Color.parseColor("#3f29b1"));
        } else if (roundTextView == rootAnswer3) {
            tvAnswer3.setTextColor(Color.parseColor("#3f29b1"));
            tvNumberPlayerSelected3.setTextColor(Color.parseColor("#3f29b1"));
        }
    }

    private void changeColorAnswerFalse(RoundLinearLayout roundTextView) {
        roundTextView.setStroke(Color.parseColor("#ffffff"), 1);
        roundTextView.setBackgroundColorRound(Color.parseColor("#ff0808"));
    }

    private void changeColorAnswer(RoundLinearLayout roundTextView) {
        roundTextView.setStroke(Color.parseColor("#ffffff"), 1);
        roundTextView.setBackgroundColorRound(Color.parseColor("#00aeef"));
    }

    private void changeColorNormal(RoundLinearLayout roundTextView) {
        roundTextView.setStroke(Color.parseColor("#706d76"), 1);
        roundTextView.setBackgroundColorRound(ContextCompat.getColor(getContext(), R.color.color_bg_answer));
    }

    private void disableAnswer() {
        isCanAnswer = false;
        tvAnswer1.setClickable(false);
        tvAnswer2.setClickable(false);
        tvAnswer3.setClickable(false);
    }

}

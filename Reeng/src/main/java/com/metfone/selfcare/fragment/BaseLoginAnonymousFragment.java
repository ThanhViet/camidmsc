package com.metfone.selfcare.fragment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.call.CallHistoryFragment;
import com.metfone.selfcare.fragment.contact.HomeContactsFragment;
import com.metfone.selfcare.fragment.contact.HomeGroupsFragment;
import com.metfone.selfcare.fragment.home.WapHomeFragment;
import com.metfone.selfcare.fragment.message.ThreadListFragmentRecycleView;
import com.metfone.selfcare.fragment.musickeeng.StrangerMusicFragment;
import com.metfone.selfcare.fragment.onmedia.OnMediaHotFragment;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.LoginFromAnonymousListener;
import com.metfone.selfcare.module.myviettel.fragment.MyViettelFragment;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thanhnt72 on 4/23/2019.
 */

public abstract class BaseLoginAnonymousFragment extends Fragment implements LoginFromAnonymousListener {

    private Context mContext;
    private EmptyViewListener mCallBack;
    private View emptyView;

    protected ProgressLoading mPrbLoading;
    private TextView mTvwNote;
    private ImageView mBtnRetry;
    private TextView mTvwRetry1, mTvwRetry2;

    private View viewLogin;
    private RoundTextView btnLogin;
    private ImageView ivThumb;
    private TextView tvLogin;
    private ApplicationController mApp;
    private boolean isShowLogin;

    protected void initViewLogin(LayoutInflater inflater, ViewGroup viewGroup, View parent) {
        if (mApp.getReengAccountBusiness().isAnonymousLogin()) {
            viewLogin = parent.findViewById(R.id.llViewLogin);
            ivThumb = parent.findViewById(R.id.ivThumbLogin);
            tvLogin = parent.findViewById(R.id.tvContentLogin);
            btnLogin = viewLogin.findViewById(R.id.btnLogin);

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseSlidingFragmentActivity activity = (BaseSlidingFragmentActivity) getActivity();
                    if (activity != null) activity.loginFromAnonymous(getSourceClassName());
                }
            });
            viewLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            /*ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            viewGroup.addView(viewLogin, params);*/
            ListenerHelper.getInstance().addLoginAnonymousListener(this);

            viewLogin.setVisibility(View.VISIBLE);
            btnLogin.setText(mContext.getString(R.string.login));
            drawThumbLogin();
            isShowLogin = true;
        } else {
            isShowLogin = false;
        }
    }

    private void drawThumbLogin() {
        if (this instanceof ThreadListFragmentRecycleView) {
            tvLogin.setText(mContext.getString(R.string.login_inbox));
        } else if (this instanceof CallHistoryFragment) {
            tvLogin.setText(mContext.getString(R.string.login_call));
        } else if (this instanceof HomeContactsFragment) {
            tvLogin.setText(mContext.getString(R.string.login_contact));
        } else if (this instanceof HomeGroupsFragment) {
            tvLogin.setText(mContext.getString(R.string.login_group));
        } else if (this instanceof StrangerMusicFragment) {
            tvLogin.setText(mContext.getString(R.string.login_stranger));
        } else if (this instanceof OnMediaHotFragment) {
            tvLogin.setText(mContext.getString(R.string.login_onmedia));
        } else if (this instanceof MyViettelFragment) {
            tvLogin.setText(mContext.getString(R.string.login_my_viettel));
        } else if (this instanceof WapHomeFragment) {
            tvLogin.setText(mContext.getString(R.string.login_tab_wap));
        }
        Glide.with(mApp).load(R.drawable.thumb_login).into(ivThumb);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mApp = ApplicationController.self();
        mContext = context;
    }

    @Override
    public void onDestroyView() {
        ListenerHelper.getInstance().removeLoginAnonymousListener(this);
        super.onDestroyView();
    }

    @Override
    public void onLoginSuccess() {
        if (viewLogin != null)
            viewLogin.setVisibility(View.GONE);
        isShowLogin = false;
    }

    protected void createView(LayoutInflater inflater, RecyclerView recyclerView, EmptyViewListener callBack) {
        this.mCallBack = callBack;
        emptyView = inflater.inflate(R.layout.view_empty, null);
        mPrbLoading = emptyView.findViewById(R.id.empty_progress);
        mTvwNote = emptyView.findViewById(R.id.empty_text);
        mBtnRetry = emptyView.findViewById(R.id.empty_retry_button);
        mTvwRetry1 = emptyView.findViewById(R.id.empty_retry_text1);
        mTvwRetry2 = emptyView.findViewById(R.id.empty_retry_text2);
        ViewGroup viewGroup = (ViewGroup) recyclerView.getParent();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(emptyView, params);
        //container.addView(emptyView, 0)
        if (mBtnRetry != null) mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    showProgressLoading();
                    mCallBack.onRetryClick();
                }
            }
        });
        //hideEmptyView();
        showProgressLoading();
    }

    protected void initEmptyView(@NonNull View emptyView, EmptyViewListener callBack) {
        this.emptyView = emptyView;
        this.mCallBack = callBack;
        mPrbLoading = emptyView.findViewById(R.id.empty_progress);
        mTvwNote = emptyView.findViewById(R.id.empty_text);
        mBtnRetry = emptyView.findViewById(R.id.empty_retry_button);
        mTvwRetry1 = emptyView.findViewById(R.id.empty_retry_text1);
        mTvwRetry2 = emptyView.findViewById(R.id.empty_retry_text2);
        if (mBtnRetry != null) mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    showProgressLoading();
                    mCallBack.onRetryClick();
                }
            }
        });
        showProgressLoading();
    }

    protected void hideEmptyView() {
        if (emptyView != null) emptyView.setVisibility(View.GONE);
    }

    protected void showEmptyNote(String content) {
        if (emptyView != null) emptyView.setVisibility(View.VISIBLE);
        if (mPrbLoading != null) mPrbLoading.setVisibility(View.GONE);
        if (mTvwNote != null) {
            mTvwNote.setVisibility(View.VISIBLE);
            mTvwNote.setText(content);
        }
        if (mBtnRetry != null) mBtnRetry.setVisibility(View.GONE);
        if (mTvwRetry1 != null) mTvwRetry1.setVisibility(View.GONE);
        if (mTvwRetry2 != null) mTvwRetry2.setVisibility(View.GONE);
    }

    protected void showEmptyNote() {
        showEmptyNote(mContext.getResources().getString(R.string.list_empty));
    }

    protected void showProgressLoading() {
        if (emptyView != null) emptyView.setVisibility(View.VISIBLE);
        if (mPrbLoading != null) mPrbLoading.setVisibility(View.VISIBLE);
        if (mTvwNote != null) mTvwNote.setVisibility(View.GONE);
        if (mBtnRetry != null) mBtnRetry.setVisibility(View.GONE);
        if (mTvwRetry1 != null) mTvwRetry1.setVisibility(View.GONE);
        if (mTvwRetry2 != null) mTvwRetry2.setVisibility(View.GONE);
    }

    protected boolean isShowProgressLoading() {
        if (emptyView == null || mPrbLoading == null) return false;
        return emptyView.getVisibility() == View.VISIBLE && mPrbLoading.getVisibility() == View.VISIBLE;
    }

    protected void showRetryView() {
        if (emptyView != null) emptyView.setVisibility(View.VISIBLE);
        if (mPrbLoading != null) mPrbLoading.setVisibility(View.GONE);
        if (mTvwNote != null) mTvwNote.setVisibility(View.GONE);
        if (mBtnRetry != null) mBtnRetry.setVisibility(View.VISIBLE);
        if (mTvwRetry1 != null) mTvwRetry1.setVisibility(View.VISIBLE);
        if (mTvwRetry2 != null) mTvwRetry2.setVisibility(View.VISIBLE);
    }

    protected void showEmptyOrRetryView() {
        if (NetworkHelper.isConnectInternet(mContext)) {
            showEmptyNote(mContext.getResources().getString(R.string.list_empty));
        } else {
            showRetryView();
        }
    }

    public interface EmptyViewListener {
        void onRetryClick();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isShowLogin) {

        }
    }

    protected abstract String getSourceClassName();
}

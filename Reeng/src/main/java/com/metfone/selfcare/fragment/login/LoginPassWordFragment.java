package com.metfone.selfcare.fragment.login;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_NUMBER;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.SPUtils;
import com.metfone.esport.common.Constant;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.IFingerListener;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.SignIn;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.games.activity.ContainerActivity;
import com.metfone.selfcare.module.newdetails.utils.CustomBase64;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.dialog.FingerDialog;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPassWordFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = LoginPassWordFragment.class.getSimpleName();
    OnFragmentInteractionListener fragmentInteractionListener;
    String phoneNumber;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private AppCompatImageView ivBack;
    private LoginActivity mLoginActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private ProgressBar pbLoading;
    private RelativeLayout mRllUsePassword;
    private RoundTextView btnUseOTP;
    private CamIdTextView tvForgotPassword, btnLogin;
    private PinView pinPassword;
    private PinView hintPinView;
    private FrameLayout flSecondPinView;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;
    private boolean isRunningLogin = false;
    private ImageView mIvLoginWithFinger;
    private RoundLinearLayout rlnlPassword;

    public static LoginPassWordFragment newInstance(String phoneNumber) {

        Bundle args = new Bundle();
        args.putString(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        LoginPassWordFragment fragment = new LoginPassWordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        mLoginActivity = (LoginActivity) activity;
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mRes = mLoginActivity.getResources();
        try {
            fragmentInteractionListener = (LoginPassWordFragment.OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_pass_word, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initViewListeners();
        initData();


    }

    private void initData() {
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
            mCurrentRegionCode = "KH";
            if (phoneNumber != null) {
                mCurrentNumberJid = "+855" + phoneNumber.substring(1);
                ;
            }
        }
    }

    private void initViewListeners() {
        mIvLoginWithFinger.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        btnUseOTP.setOnClickListener(this);
        flSecondPinView.setOnClickListener(this);
        pinPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "onTextChanged() called with: s = [" + s + "], start = [" + start + "], before = [" + before + "], count = [" + count + "]");
                if (s.length() == 6) {
//                    signIn();
                    signInWithPassword(s.toString());
                    btnLogin.setEnabled(true);
                    btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_confirm));
                } else {
                    btnLogin.setEnabled(false);
                    btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
//    pinPassword.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
//        @Override
//        public void onPinEntered(CharSequence str) {
//            if (str.length() == 6) {
//                    signIn();
//                }
//        }
//    });
        pinPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "onFocusChange: " + hasFocus + ", " + v.isFocused());
                if (hasFocus) {
                    rlnlPassword.setStroke(ContextCompat.getColor(mLoginActivity, R.color.resend), 1);
                } else {
                    rlnlPassword.setStroke(ContextCompat.getColor(mLoginActivity, R.color.color_edt_full_name_stroke), 1);
                }
            }
        });
    }

    private void initViews(@NonNull View view) {
        btnUseOTP = view.findViewById(R.id.btnUseOTP);
        btnLogin = view.findViewById(R.id.btnLogin);
        tvForgotPassword = view.findViewById(R.id.tvForgotPassword);
        ivBack = view.findViewById(R.id.ivBack_);
        pbLoading = view.findViewById(R.id.pbLoading);
        mRllUsePassword = view.findViewById(R.id.rll_usepassword);
        pinPassword = view.findViewById(R.id.pinPassword);
        hintPinView = view.findViewById(R.id.hintPinView);
        mIvLoginWithFinger = view.findViewById(R.id.iv_login_with_finger);
        rlnlPassword = view.findViewById(R.id.rlnlPassword);
        flSecondPinView = view.findViewById(R.id.flSecondPinView);
        pinPassword.setPasswordHidden(true);
        hintPinView.setPasswordHidden(true);
        btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
    }

    private void signInWithPassword(String password) {
        if (isRunningLogin) {
            return;
        }
        isRunningLogin = true;
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
//        String phoneNumber = SharedPrefs.getInstance().get(PREF_PHONE_NUMBER, String.class);
        Log.d("TAG", "signIn: phone " + phoneNumber);
        SignIn signIn = new SignIn();
//        signIn.setOtp("112233");
        signIn.setType("password");
        signIn.setPassword(password);
//        signIn.setPassword(edtPassword.getEditText().getText().toString());
//        signIn.setPassword(pinPassword.getText().toString());
        signIn.setPhone_number(phoneNumber);

        SignInRequest signInRequest = new SignInRequest(signIn, "", "", "", "");

        apiService.signIn(DeviceUtils.getUniqueDeviceId(), signInRequest).enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, retrofit2.Response<SignInResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                // TODO start
                if (response.body() == null) {

                    Toast.makeText(mLoginActivity, R.string.login_fail, Toast.LENGTH_LONG).show();
                } else {
                    if (response.body().getCode().equals("00")) {
                        String token = "Bearer " + response.body().getData().getAccess_token();
                        SharedPrefs.getInstance().put(PREF_PHONE_NUMBER, phoneNumber);
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefresh_token());
                        DynamicSharePref.getInstance().put(Constants.KEY_FINGER_PHONE, phoneNumber);
                        if (!TextUtils.isEmpty(pinPassword.getText().toString())) {
                            DynamicSharePref.getInstance().put(Constants.KEY_FINGER_ENCODE_PW, CustomBase64.getEncoder().encodeToString(pinPassword.getText().toString().getBytes()));
                        }
                        if (signIn != null) {
                            SPUtils.getInstance().put(Constant.PREF_USERNAME_ACCOUNT_ESPORT, signIn.getPhone_number());
                            SPUtils.getInstance().put(Constant.PREF_PASSWORD_ACCOUNT_ESPORT, signIn.getPassword());
                        }
                        getUserInformation();
                        generateOldMochaOTP(response.body().getData().getAccess_token());
                    } else {
                        Toast.makeText(mLoginActivity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
                isRunningLogin = false;
                // TODO end
            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Toast.makeText(getContext(), getString(R.string.txt_sign_up_failed), Toast.LENGTH_SHORT).show();
                isRunningLogin = false;
            }
        });
    }

    public void addGameHomeFragment() {
        Intent intent = new Intent(getActivity(), ContainerActivity.class);
        startActivity(intent);
    }

    private void getUserInformation() {
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        FireBaseHelper.getInstance(ApplicationController.self()).checkServiceAndRegister(true);
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(mLoginActivity);
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        RestoreManager.setRestoring(true);
                        userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                            @Override
                            public void onStartDownload() {

                            }

                            @Override
                            public void onDownloadProgress(int percent) {

                            }

                            @Override
                            public void onDownloadComplete() {

                            }

                            @Override
                            public void onDowloadFail(String message) {

                            }

                            @Override
                            public void onStartRestore() {

                            }

                            @Override
                            public void onRestoreProgress(int percent) {

                            }

                            @Override
                            public void onRestoreComplete(int messageCount, int threadMessageCount) {
                                try {
                                    SharedPreferences pref = getContext().getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                                    if (pref != null) {
                                        pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                                        com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                                    }
                                } catch (Exception e) {
                                }
                                RestoreManager.setRestoring(false);
                                mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                                mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                                mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                                try {
                                    if (getActivity() != null)
                                        NavigateActivityHelper.navigateToHomeScreenActivity((BaseSlidingFragmentActivity) getActivity(), false, true);
                                } catch (Exception ex) {
                                    Log.e(TAG, "Natigate: " + ex.toString());
                                }

                            }

                            @Override
                            public void onRestoreFail(String message) {
                                RestoreManager.setRestoring(false);
                                NavigateActivityHelper.navigateToHomeScreenActivity((BaseSlidingFragmentActivity) getActivity(), false, true);
                            }
                        }, mLoginActivity);
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(getContext(), "Get user failed");
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                ToastUtils.showToast(getContext(), getString(R.string.error_time_out));
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etNumber:
                break;
            case R.id.btnUseOTP:
                fragmentInteractionListener.displayLoginOTPScreen(phoneNumber, "", 0);
                break;
            case R.id.ivBack_:
                mLoginActivity.onBackPressed();
                break;
            case R.id.edtPassword:
                break;
            case R.id.tvForgotPassword:
                fragmentInteractionListener.displayOTPForgotScreen(phoneNumber);
                break;
            case R.id.iv_login_with_finger:
                loginWithFinger();
                break;
            case R.id.btnLogin:
                String password = pinPassword.getText().toString();
                if (TextUtils.isEmpty(password) || password.length() < 6) {
                    ToastUtils.showToast(getContext(), getString(R.string.password_consists_of_6_characters));
                } else {
                    signInWithPassword(password);
                }
                break;
        }
    }

    private void loginWithFinger() {
        String account = DynamicSharePref.getInstance().get(Constants.KEY_FINGER_PHONE, String.class);
        if (account.equals(phoneNumber)) {
            FingerDialog fingerDialog = new FingerDialog(getContext(), new IFingerListener() {
                @Override
                public void getFingerprintSuccess() {
                    signInWithPassword(new String(CustomBase64.getDecoder()
                            .decode(DynamicSharePref.getInstance().get(Constants.KEY_FINGER_ENCODE_PW, String.class))));
                }

                @Override
                public void getFingerprintError() {

                }
            });
            fingerDialog.show();
        } else {
            Toast.makeText(mLoginActivity, "Please enter your password at the first login!", Toast.LENGTH_SHORT).show();
        }
    }

    private void generateOldMochaOTP(String token) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = "+855" + phoneNumber.substring(1);
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(mLoginActivity).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(getContext(), "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), error.getMessage());
            }
        });

    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new LoginPassWordFragment.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    @Override
    public void onResume() {
        super.onResume();
//        tvPassword.requestFocus();
        pinPassword.requestFocus();
        flSecondPinView.requestFocus();
        rlnlPassword.requestFocus();
        UIUtil.showKeyboardPasswordLogin(getContext(), rlnlPassword);
        UIUtil.showKeyboard(getContext(), pinPassword);
        UIUtil.showKeyboardPassword(getContext(), flSecondPinView);
        setupUI(mRllUsePassword);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) mLoginActivity.getApplication();

            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);
//            mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
            try {

                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    getUserInformation();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
//                    if (tvError != null) {
//                        tvError.setVisibility(View.GONE);
//                    }
                    mApplication.logEventFacebookSDKAndFirebase(mLoginActivity.getString(R.string.c_login_complete));
                } else {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
//                    mLoginActivity.showError(responseCode.getDescription(), null);
//                    if (tvError != null) {
//                        tvError.setVisibility(View.VISIBLE);
//                    }
                    ToastUtils.showToast(mLoginActivity, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(mLoginActivity.getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }

    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void displayLoginOTPScreen(String numberJid, String regionCode, int code);

        void displayOTPForgotScreen(String phoneNumber);

        void displayLoginPasswordScreen(String phoneNumber);

        void displaySetNewPasswordScreen(String phoneNumber, String otp);
    }
}
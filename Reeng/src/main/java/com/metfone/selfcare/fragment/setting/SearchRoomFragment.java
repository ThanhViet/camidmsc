package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.OfficerListAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 1/4/2016.
 */
public class SearchRoomFragment extends BaseRecyclerViewFragment {
    private static final String TAG = SearchRoomFragment.class.getSimpleName();
    private SettingActivity mParentActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private View rootView;
    private AppCompatImageView mImgBack;
    private AppCompatEditText mReengSearchView;
    private SearchAsyncTask mSearchAsyncTask;
    private LinearLayoutManager mRecyclerManager;
    private RecyclerView mRecyclerView;
    private OfficerListAdapter mAdapter;
    private ArrayList<OfficerAccount> mRoomSearchable;

    private OnFragmentSearchRoomListener mListener;
    private OnFragmentInteractionListener mInteractionListener;

    public static SearchRoomFragment newInstance() {
        SearchRoomFragment fragment = new SearchRoomFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SettingActivity) activity;
        mApplication = (ApplicationController) activity.getApplication();
        mRes = mParentActivity.getResources();
        mRoomSearchable = mApplication.getOfficerBusiness().getmListRoomTab();
        try {
            mListener = (OnFragmentSearchRoomListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentListRoomListener");
        }

        try {
            mInteractionListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSearchAsyncTask != null) {
            mSearchAsyncTask.cancel(true);
            mSearchAsyncTask = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search_user_v5, container, false);
        findComponentViews(rootView, container, inflater);
        showSearch();
        setReengSearchViewListener();
        setButtonBackSearchListener();
        return rootView;
    }

    private void findComponentViews(View view, ViewGroup container, LayoutInflater inflater) {
        mImgBack = view.findViewById(R.id.icBackToolbar);
        mReengSearchView = view.findViewById(R.id.edtSearch);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mReengSearchView.setHint(mRes.getString(R.string.hint_search_room));
        createView(inflater, mRecyclerView, null);
    }

    private void showSearch() {
        mReengSearchView.setText("");
        hideEmptyView();
        mReengSearchView.setFocusable(true);
        mReengSearchView.requestFocus();
        InputMethodUtils.showSoftKeyboard(mParentActivity, mReengSearchView);
        searchAsyncTask("");
    }


    // search asyntask
    private class SearchAsyncTask extends AsyncTask<String, Void, ArrayList<OfficerAccount>> {
        private ArrayList<OfficerAccount> list;

        public void setListOfficers(ArrayList<OfficerAccount> officers) {
            this.list = officers;
        }

        @Override
        protected ArrayList<OfficerAccount> doInBackground(String... params) {
            String textSearch = params[0];
            return officerSearchList(textSearch, list);
        }

        @Override
        protected void onPostExecute(ArrayList<OfficerAccount> result) {
            notifiChangeAdapter(result);
            super.onPostExecute(result);
        }
    }

    // thread search list
    private ArrayList<OfficerAccount> officerSearchList(String contentSearch, ArrayList<OfficerAccount> listAccounts) {
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());
        if (contentSearch == null || contentSearch.length() <= 0) {
            return new ArrayList<>(listAccounts);
        } else {
            ArrayList<OfficerAccount> listSearchs;
            String[] listTexts = contentSearch.split("\\s+");
            if (listAccounts != null) {
                ArrayList<OfficerAccount> list;
                listSearchs = new ArrayList<>(listAccounts);
                for (String item : listTexts) {
                    if (item.trim().length() > 0) {
                        //item = TextHelper.getInstant().convertUnicodeToAscci(item);
                        list = new ArrayList<>();
                        for (OfficerAccount account : listSearchs) {
                            String name = TextHelper.getInstant().convertUnicodeToAscci(account.getName());
                            if (account.getServerId() != null && name != null && name.contains(item) && !isContainItem(list, account)) {
                                list.add(account);
                            }
                        }
                        listSearchs = list;
                    }
                }
            } else {
                listSearchs = new ArrayList<>();
            }
            return listSearchs;
        }
    }

    private boolean isContainItem(ArrayList<OfficerAccount> list, OfficerAccount account) {
        for (OfficerAccount item : list) {
            if (item.getServerId().equals(account.getServerId()))
                return true;
        }
        return false;
    }

    private void notifiChangeAdapter(ArrayList<OfficerAccount> list) {
        if (list == null || list.isEmpty()) {
            showEmptyNote();
        } else {
            hideEmptyView();
        }
        if (mAdapter == null) {
            setAdapter(list);
        } else {
            mAdapter.setOfficerList(list);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setAdapter(ArrayList<OfficerAccount> list) {
        mAdapter = new OfficerListAdapter(mApplication, Constants.CONTACT.CONTACT_VIEW_NOMAL, list);
        mRecyclerManager = new LinearLayoutManager(mParentActivity, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mRecyclerManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        setViewListeners();
    }

    private void setReengSearchViewListener() {
        mReengSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchAsyncTask(s.toString());
            }
        });

        mReengSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
                if (keyCode == EditorInfo.IME_ACTION_DONE) {
                    InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
                }
                return false;
            }
        });

        mReengSearchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // assus zenphone
                //mSearchView.clearFocus();
                InputMethodUtils.showSoftKeyboard(mParentActivity, mReengSearchView);
                return false;
            }
        });
    }

    private void searchAsyncTask(String textSearch) {
        Log.d(TAG, "searchContactAsynctask ");
        if (mSearchAsyncTask != null) {
            mSearchAsyncTask.cancel(true);
            mSearchAsyncTask = null;
        }

        if (mRoomSearchable == null || mRoomSearchable.size() < 1) {
            return;
        }
        mSearchAsyncTask = new SearchAsyncTask();
        mSearchAsyncTask.setListOfficers(mRoomSearchable);
        mSearchAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
    }

    private void setButtonBackSearchListener() {
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
                getActivity().onBackPressed();
            }
        });
    }

    private void setViewListeners() {
        // on touch
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodUtils.hideSoftKeyboard(mParentActivity);
                return false;
            }
        });
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                OfficerAccount account = (OfficerAccount) object;
                if (account == null || TextUtils.isEmpty(account.getServerId())) {
                    return;
                }
//                DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mParentActivity,
//                        account.getServerId(), account.getName(), account.getAvatarUrl());
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    public interface OnFragmentSearchRoomListener {
        void navigateToListRoomFragmnet();
    }

    public interface OnFragmentInteractionListener {
        void navigateToThreadDetail(ThreadMessage threadMessage);
    }
}

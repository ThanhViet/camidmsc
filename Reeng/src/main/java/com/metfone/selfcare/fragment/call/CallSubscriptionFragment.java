package com.metfone.selfcare.fragment.call;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.CallHistoryDetailActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CallHistoryHelper;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.call.CallSubscription;
import com.metfone.selfcare.database.model.call.CallSubscriptionFakeMo;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 6/16/2017.
 */

public class CallSubscriptionFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = CallSubscriptionFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private CallHistoryDetailActivity mParentActivity;
    private Resources mRes;
    private ImageView mAbBack, mAbMore;
    private EllipsisTextView mAbTitle;
    private TextView mTvwRemain, mTvwUserFreeName, mTvwUserFreeDesc, mTvwMoreCallOut, mTvwFakeMoDesc;
    private RoundTextView mBtnSetUserFree, mBtnChangeUserFree, mBtnFakeMo;
    private LinearLayout mLlFakeMo;
    private AspectImageView mImgBanner;
    private CallSubscription callSubscription;
    private String callOutFreeUser;

    public static CallSubscriptionFragment newInstance(CallSubscription callSubscription) {
        CallSubscriptionFragment fragment = new CallSubscriptionFragment();
        Bundle args = new Bundle();
        args.putSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA, callSubscription);
        fragment.setArguments(args);
        return fragment;
    }

    public CallSubscriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (CallHistoryDetailActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        /*try {
            mListener = (CallHistoryDetailFragment.OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_call_subscription, container, false);
        findComponentViews(rootView, inflater, container);
        getData(savedInstanceState);
        drawDetail();
        setClickListener();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA, callSubscription);
        outState.putString(CallHistoryConstant.FRAGMENT_CALLOUT_FREE_USER, callOutFreeUser);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.call_subscription_user_free_btn:
            case R.id.call_subscription_user_change_btn:
                Intent assign = new Intent(mParentActivity, ChooseContactActivity.class);
                assign.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_CALL_OUT_FREE_USER);
                startActivityForResult(assign, Constants.CHOOSE_CONTACT.TYPE_CALL_OUT_FREE_USER);
                break;
            case R.id.call_subscription_banner:
            case R.id.call_subscription_more_callout_view:
                UrlConfigHelper.getInstance(mParentActivity).gotoWebViewOnMedia(mApplication,
                        mParentActivity, mRes.getString(R.string.call_subscription_more_site));
                break;
            /*case R.id.call_subscription_faq:
                UrlConfigHelper.getInstance(mParentActivity).gotoWebViewOnMedia(mApplication,
                        mParentActivity, mRes.getString(R.string.call_subscription_faq_site));
                break;*/
            case R.id.call_subscription_user_free_name:
                if (!TextUtils.isEmpty(callOutFreeUser)) {
                    ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(callOutFreeUser);
                    mApplication.getCallBusiness().handleStartCall(threadMessage, mParentActivity, true, false);
                }
                break;
            case R.id.call_subscription_fake_mo_button:
                String cmd = callSubscription.getFakeMo().getCmd();
                String confirm = callSubscription.getFakeMo().getConfirm();
                ReportHelper.showConfirmAndReconfirmSubscription(mApplication, mParentActivity, confirm, null, cmd, "callSubscription");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mParentActivity.setActivityForResult(false);
        mParentActivity.setTakePhotoAndCrop(false);
        Log.d(TAG, "onActivityResult: " + requestCode + " -- " + resultCode);
        if (requestCode == Constants.CHOOSE_CONTACT.TYPE_CALL_OUT_FREE_USER && data != null) {
            String contactName = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME);
            String jidNumber = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
            showConfirmChangeNumber(jidNumber, contactName);
            //sendCallOutFreeUser(jidNumber);
        }
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        View actionBarView = mParentActivity.getToolBarView();
        mAbBack = actionBarView.findViewById(R.id.ab_back_btn);
        mAbTitle = actionBarView.findViewById(R.id.ab_title);
        mAbMore = actionBarView.findViewById(R.id.ab_more_btn);

        mLlFakeMo = rootView.findViewById(R.id.call_subscription_fake_mo_layout);
        mTvwFakeMoDesc = rootView.findViewById(R.id.call_subscription_fake_mo_desc);
        mBtnFakeMo = rootView.findViewById(R.id.call_subscription_fake_mo_button);
        mTvwRemain = rootView.findViewById(R.id.call_subscription_remain);
        mBtnSetUserFree = rootView.findViewById(R.id.call_subscription_user_free_btn);
        mBtnChangeUserFree = rootView.findViewById(R.id.call_subscription_user_change_btn);
        mTvwUserFreeName = rootView.findViewById(R.id.call_subscription_user_free_name);
        mTvwUserFreeDesc = rootView.findViewById(R.id.call_subscription_user_free_desc);
        mTvwMoreCallOut = rootView.findViewById(R.id.call_subscription_more_callout_view);
        mImgBanner = rootView.findViewById(R.id.call_subscription_banner);
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            callSubscription = (CallSubscription) savedInstanceState.getSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA);
            callOutFreeUser = savedInstanceState.getString(CallHistoryConstant.FRAGMENT_CALLOUT_FREE_USER);
        } else if (getArguments() != null) {
            callSubscription = (CallSubscription) getArguments().getSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA);
            if (callSubscription != null && callSubscription.getFreeNumbers() != null && !callSubscription.getFreeNumbers().isEmpty()) {
                callOutFreeUser = callSubscription.getFreeNumbers().get(0);
            }
        }
        if (callSubscription == null) mParentActivity.finish();
    }

    private void drawDetail() {
        //TODO đã thử load bằng glide nhưng ảnh load xong chất lượng ko dc tốt, giữ nguyên cơ chế set src chấp nhận render chậm 1 chút
        //Glide.with(this).load(R.drawable.banner_footer_callout).into(mImgBanner);
        mTvwMoreCallOut.setText(TextHelper.fromHtml(mRes.getString(R.string.call_subscription_more_callout)));
        mAbTitle.setText(mRes.getString(R.string.call_subscription_title));
        mAbMore.setVisibility(View.GONE);
        mTvwRemain.setText(callSubscription.getRemainTime());
        //mTvwRemain.setText("100 phút 35 giây");
        drawFakeMoDetail();
        drawUserFreeDetail();
    }

    private void drawFakeMoDetail() {
        if (callSubscription.getFakeMo() == null || TextUtils.isEmpty(callSubscription.getFakeMo().getCmd())) {
            mLlFakeMo.setVisibility(View.GONE);
        } else {
            CallSubscriptionFakeMo fakeMo = callSubscription.getFakeMo();
            mLlFakeMo.setVisibility(View.VISIBLE);
            mTvwFakeMoDesc.setText(fakeMo.getDesc());
            mBtnFakeMo.setText(fakeMo.getLabel());
        }
    }

    private void drawUserFreeDetail() {
        if (TextUtils.isEmpty(callOutFreeUser)) {
            mTvwUserFreeName.setEnabled(false);
            mTvwUserFreeDesc.setVisibility(View.GONE);
            mTvwUserFreeName.setText(mRes.getString(R.string.call_subscription_user_free_no_choose));
            mTvwUserFreeName.setTextColor(ContextCompat.getColor(mParentActivity, R.color.text_song_listen));
            mBtnSetUserFree.setVisibility(View.VISIBLE);
            mBtnChangeUserFree.setVisibility(View.GONE);
        } else {
            mTvwUserFreeName.setEnabled(true);
            mTvwUserFreeDesc.setVisibility(View.VISIBLE);
            mTvwUserFreeDesc.setText(callSubscription.getChangeSubPrice());
            mTvwUserFreeName.setTextColor(ContextCompat.getColor(mParentActivity, R.color.bg_mocha));
            mBtnSetUserFree.setVisibility(View.GONE);
            mBtnChangeUserFree.setVisibility(View.VISIBLE);
            PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(callOutFreeUser);
            if (phoneNumber != null) {
                mTvwUserFreeName.setText(String.format(mRes.getString(R.string.call_subscription_user_free_name), phoneNumber.getName(), callOutFreeUser));
            } else {
                mTvwUserFreeName.setText(callOutFreeUser);
            }
        }
    }

    private void setClickListener() {
        mAbBack.setOnClickListener(this);
        mBtnSetUserFree.setOnClickListener(this);
        mBtnChangeUserFree.setOnClickListener(this);
        mTvwMoreCallOut.setOnClickListener(this);
        mTvwUserFreeName.setOnClickListener(this);
        mImgBanner.setOnClickListener(this);
        mBtnFakeMo.setOnClickListener(this);
    }

    private void sendCallOutFreeUser(final String friendJid) {
        Log.d(TAG, "sendCallOutFreeUser: " + friendJid);
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        /*CallHistoryHelper.getInstance(mApplication).requestSetCallOutFreeUser(friendJid, new CallHistoryHelper.SetCallOutFreeUserListener() {
            @Override
            public void onSuccess(String desc) {
                mParentActivity.hideLoadingDialog();
                callOutFreeUser = friendJid;
                drawUserFreeDetail();
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
            }

            @Override
            public void onError(String desc) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
            }
        });*/
    }

    private void showConfirmChangeNumber(String friendJid, String friendName) {
        String msg;
        if (TextUtils.isEmpty(callOutFreeUser)) {
            msg = String.format(mRes.getString(R.string.call_subscription_confirm_choose_user), friendJid);
        } else {// đổi số
            if (TextUtils.isEmpty(callSubscription.getChangeSubConfirm())) {
                msg = String.format(mRes.getString(R.string.call_subscription_confirm_change_user), friendJid);
            } else {
                try {
                    msg = String.format(callSubscription.getChangeSubConfirm(), friendJid);
                } catch (Exception e) {// trường hợp sv trả về ko đúng format thì lấy text default
                    Log.e(TAG, "Exception", e);
                    msg = String.format(mRes.getString(R.string.call_subscription_confirm_change_user), friendJid);
                }
            }
        }
        new DialogConfirm(mParentActivity, true).
                setLabel(null).
                setMessage(msg).
                setNegativeLabel(mRes.getString(R.string.non_cancel)).
                setPositiveLabel(mRes.getString(R.string.non_ok)).
                setEntry(friendJid).
                setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        sendCallOutFreeUser((String) result);
                    }
                }).show();
    }

    public void reloadData() {
        mParentActivity.showLoadingDialog("", R.string.loading);
        CallHistoryHelper.getInstance().requestGetCallOutRemain(new CallHistoryHelper
                .GetCallOutRemainListener() {
            @Override
            public void onSuccess(CallSubscription entry) {
                mParentActivity.hideLoadingDialog();
                callSubscription = entry;
                drawDetail();
            }

            @Override
            public void onError(String desc) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
            }
        });
    }
}
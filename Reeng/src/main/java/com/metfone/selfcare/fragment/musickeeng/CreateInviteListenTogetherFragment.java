package com.metfone.selfcare.fragment.musickeeng;

import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.SearchHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by daipr on 4/17/2020
 */
public class CreateInviteListenTogetherFragment extends Fragment implements TextWatcher {

    public static final String TAB_POSITION_START = "position_start";

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.edtSearch)
    AppCompatEditText edtSearch;
    @BindView(R.id.tvCancel)
    AppCompatTextView tvCancel;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.icDeleteSearch)
    AppCompatImageView ivDeleteSearch;
    int tabSelected;

    private FragmentStatePagerItemAdapter viewPagerAdapter;
    private Unbinder unbinder;

    public static CreateInviteListenTogetherFragment newInstance() {
        Bundle args = new Bundle();
        CreateInviteListenTogetherFragment fragment = new CreateInviteListenTogetherFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_invite_listen_together, container, false);
        unbinder = ButterKnife.bind(this, view);
        setupAdapter();
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
        edtSearch.addTextChangedListener(this);
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    textSearch = mSearchView.getText().toString().trim();
//                    handleSearch(page);
//                    mPrbLoading.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        ivDeleteSearch.setOnClickListener(view1 -> {
            edtSearch.setText("");
        });
        tvCancel.setPaintFlags(tvCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvCancel.setText(R.string.btn_cancel);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setupAdapter() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(getActivity());
        Bundle argOne = new Bundle();
        argOne.putBoolean("hide_appbar", true);
        creator.add(FragmentPagerItem.of(getString(R.string.top_hit_keeng), TopSongFragment.class));
        creator.add(FragmentPagerItem.of(getString(R.string.music_from_your_device), UploadSongFragment.class, argOne));
        viewPagerAdapter = new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }


    @Override
    public void onDestroyView() {
        InputMethodUtils.hideSoftKeyboard(edtSearch, getActivity());
        super.onDestroyView();
        unbinder.unbind();
        unbinder = null;
    }

    @OnClick({R.id.tvCancel})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.tvCancel: {
                getActivity().onBackPressed();
                break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String query = SearchHelper.convertQuery(s.toString());
        if (query == null || query.isEmpty()) {
            ivDeleteSearch.setVisibility(View.GONE);
        } else {
            ivDeleteSearch.setVisibility(View.VISIBLE);
        }
        if (viewPager.getCurrentItem() == 0) {
            Fragment fragment = getChildFragmentManager().getFragments().get(0);
            if (fragment != null && fragment instanceof TopSongFragment) {
                ((TopSongFragment) (fragment)).actionSearchOnlineSuggest(query);
            }
        } else {
            Fragment fragment = getChildFragmentManager().getFragments().get(1);
            if (fragment != null && fragment instanceof UploadSongFragment) {
                ((UploadSongFragment) (fragment)).actionSearchSong(query);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

package com.metfone.selfcare.fragment.setting;

/**
 * Created by tungt on 7/27/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.adapter.ListAppAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MoreAppInteractHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.holder.AppInfo;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MoreAppsFragment2 extends BaseRecyclerViewFragment implements BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = MoreAppsFragment2.class.getSimpleName();
    private static final String URL = "http://vip.service.keeng.vn:8080/KeengWSRestful/ws/common/getAppInfo";
    public static final int TYPE_MORE_APP = 1;
    public static final int TYPE_USEFUL = 2;
    private ListGamesActivity mParentActivity;
    private Resources mRes;
    private ApplicationController mApplication;
    private SharedPreferences mPref;
    //    private ListView mLvApps;
//    private ProgressBar mProgressLoading;
//    private TextView mTvwNoteView;
    private ArrayList<AppInfo> mListAppInfo;
    private ListAppAdapter mAdapter;
    private int type;
    private RecyclerView mRecyclerView;

    public static MoreAppsFragment2 newInstance(int type) {
        MoreAppsFragment2 fragment = new MoreAppsFragment2();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mPref = mParentActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        mRes = mParentActivity.getResources();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        initActionBar(inflater);
        initComponents(root, inflater);
        if (type == TYPE_MORE_APP) {
            getDataAndDrawAppsList();
        } else {
            getDataAndDrawUsefulApps();
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("type", type);
        super.onSaveInstanceState(outState);
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            type = savedInstanceState.getInt("type");
        } else if (getArguments() != null) {
            type = getArguments().getInt("type");
        } else {
            type = TYPE_MORE_APP;
        }
    }

    private void initActionBar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        View mActionBarView = mParentActivity.getToolBarView();
        TextView mTvwTitle = (EllipsisTextView) mActionBarView.findViewById(R.id.ab_title);
        ImageView mImgBack = (ImageView) mActionBarView.findViewById(R.id.ab_back_btn);
        ImageView mImgOption = (ImageView) mActionBarView.findViewById(R.id.ab_more_btn);
        mTvwTitle.setText(mRes.getString(R.string.list_game));
        mImgOption.setVisibility(View.GONE);
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private void initComponents(View view, LayoutInflater inflater) {
//        mLvApps = (ListView) view.findViewById(R.id.listview_apps);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        RecyclerView.LayoutManager mRecyclerManager = new LinearLayoutManager(mParentActivity,
                LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mRecyclerManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mProgressLoading = (ProgressBar) view.findViewById(R.id.progressbar_loading);
//        mTvwNoteView = (TextView) view.findViewById(R.id.tv_emptylist);

//        mLvApps.setOnItemClickListener(this);
//        mTvwNoteView.setVisibility(View.GONE);
//        mProgressLoading.setVisibility(View.GONE);
    }

    private void getDataAndDrawAppsList() {

        String prefResponse = mPref.getString(Constants.PREFERENCE.PREF_MOREAPP_RESPONSE, "");
        if (TextUtils.isEmpty(prefResponse)) {
            sendGetAppsListRequest(true);


//            mProgressLoading.setVisibility(View.VISIBLE);
        } else {
            try {
                sendGetAppsListRequest(false);
                JSONObject responseObject = new JSONObject(prefResponse);
                parseAppsList(responseObject);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private void getDataAndDrawUsefulApps() {

    }

    private void sendGetAppsListRequest(final boolean isFirstTime) {

        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
//            hideEmptyView();
//            showEmptyNote(mApplication.getResources().getString(R.string.e601_error_but_undefined));
//            mParentActivity.showToast(R.string.no_connectivity_check_again);
            if (isFirstTime)
                mPrbLoading.setVisibility(View.GONE);
            showRetryView();

            return;
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, URL, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response.toString());
                        if (isFirstTime) {

                            parseAppsList(response);
                        }
                        hideEmptyView();
//                        mProgressLoading.setVisibility(View.GONE);
                        mPref.edit().putString(Constants.PREFERENCE.PREF_MOREAPP_RESPONSE, response.toString()).apply();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        mPrbLoading.setVisibility(View.GONE);
                        showRetryView();

//                        mProgressLoading.setVisibility(View.GONE);
//                        showEmptyNote(mApplication.getResources().getString(R.string.e601_error_but_undefined));
//                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(jsObjRequest, TAG, true);
    }

//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        onItemClick((AppInfo) parent.getItemAtPosition(position));
//    }

    private void parseAppsList(JSONObject response) {
        ArrayList<AppInfo> appsList = new ArrayList<>();
        String packetName = mApplication.getPackageName();
        JSONArray jsonArray = response.optJSONArray("data");
        if (jsonArray == null || jsonArray.length() < 1) {
//            mTvwNoteView.setVisibility(View.VISIBLE);
            showEmptyNote();
            return;
        } else {
            hideEmptyView();
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.optJSONObject(i);
            AppInfo appInfor = new AppInfo();
            appInfor.setName(obj.optString("name"));
            appInfor.setDesc(obj.optString("sapo"));
            appInfor.setUrlIcon(obj.optString("icon"));
            appInfor.setLink(obj.optString("link"));
            appInfor.setPacketName(obj.optString("package"));
            appInfor.setBundleId(obj.optString("bundle_id"));
            if (!appInfor.getPacketName().equals(packetName)) {
                appsList.add(appInfor);
            }
        }
        setAdapter(appsList);
    }

    private void setAdapter(ArrayList<AppInfo> appsList) {
        if (appsList == null) {
            appsList = new ArrayList<>();
        }
        mListAppInfo = appsList;
        if (mAdapter == null) {
            mAdapter = new ListAppAdapter(mListAppInfo, mParentActivity);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setDataList(mListAppInfo);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void onItemClick(AppInfo appInfo) {
        String appPackageName = appInfo.getPacketName();
        MoreAppInteractHelper.openMoreApp(mParentActivity, appPackageName);
    }

    @Override
    public void onRetryClick() {
        if (type == TYPE_MORE_APP) {
            getDataAndDrawAppsList();
        } else {
            getDataAndDrawUsefulApps();
        }
    }
}
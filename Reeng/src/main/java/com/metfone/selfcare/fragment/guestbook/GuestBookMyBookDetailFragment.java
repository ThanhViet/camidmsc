package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.BookDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Common;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.guestbook.BookDetailPageHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.recyclerview.DividerItemDecoration;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookMyBookDetailFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener,
        BookDetailPageHolder.OnPageEditListener,
        ClickListener.IconListener,
        View.OnClickListener {
    private static final String TAG = GuestBookMainFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private GuestBookHelper mGuestBookHelper;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private EllipsisTextView mAbTitle;
    private ImageView mAbMoreOption;
    private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private BookDetailAdapter mAdapter;
    private String bookId;
    private Book currentBook;
    private ExportBookAsyncTask exportBookAsyncTask;
    private Page currentPageAssigned;

    public GuestBookMyBookDetailFragment() {

    }

    public static GuestBookMyBookDetailFragment newInstance() {
        GuestBookMyBookDetailFragment fragment = new GuestBookMyBookDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        getDataAnDraw(savedInstanceState);
        setViewListener();
        return rootView;
    }

    @Override
    public void onResume() {
        if (mGuestBookHelper.isNeedChangedPageData()) {
            mGuestBookHelper.setNeedChangedPageData(false);
            changeAdapter();
        }
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.GUEST_BOOK.BOOK_ID, bookId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        if (exportBookAsyncTask != null) {
            exportBookAsyncTask.cancel(true);
            exportBookAsyncTask = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mParentActivity.setActivityForResult(false);
        mParentActivity.setTakePhotoAndCrop(false);
        Log.d(TAG, "onActivityResult: " + requestCode + " -- " + resultCode);
        if (requestCode == Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED && data != null) {
            //String contactName = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME);
            String jidNumber = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
            if (currentPageAssigned != null) {
                currentPageAssigned.setAssigned(jidNumber);
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.ab_more_btn:
                showOrHideOverFlowMenu();
                break;
            /*case R.id.ab_add_page:

                break;*/
        }
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onEditPage(Page page) {
        mListener.navigateToEditPage(page);
    }

    @Override
    public void onGuideClick() {
        UrlConfigHelper.getInstance(mApplication).gotoWebViewOnMedia(mApplication, mParentActivity, Constants.GUEST_BOOK.LINK_GUIDE);
    }

    @Override
    public void onShare(Page page) {

    }

    @Override
    public void onDelete(Page page) {
        showConfirmDeletePage(page);
    }

    @Override
    public void onAssignFriend(Page page) {
        String bookId = page.getBookId();
        int pageId = page.getPage();
        currentPageAssigned = page;
        Intent assign = new Intent(mParentActivity, ChooseContactActivity.class);
        assign.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED);
        assign.putExtra(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID, bookId);
        assign.putExtra(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID, pageId);
        startActivityForResult(assign, Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED);
    }

    @Override
    public void onAssignedAvatarClick(Page page) {
        String numberAssigned = page.getAssigned();
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        if (numberAssigned != null && numberAssigned.equals(accountBusiness.getJidNumber())) {
            NavigateActivityHelper.navigateToMyProfile(mParentActivity);
        } else {
            Intent profile = new Intent(mParentActivity, ContactDetailActivity.class);
            Bundle bundle = new Bundle();
            PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberAssigned);
            if (phoneNumber != null && phoneNumber.getId() != null) {
                bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                bundle.putString(NumberConstant.ID, phoneNumber.getId());

            } else {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(numberAssigned);
                if (strangerPhoneNumber != null) {
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
                    bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
                    bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
                    bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
                } else {
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                    bundle.putString(NumberConstant.NUMBER, numberAssigned);
                }
            }
            profile.putExtras(bundle);
            mParentActivity.startActivity(profile, true);
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_GUEST_BOOK_DELETE_BOOK:
                handleDeleteBook((Book) entry);
                break;
            case Constants.MENU.POPUP_GUEST_BOOK_DELETE_PAGE:
                handleDeletePage((Page) entry);
                break;
            case Constants.MENU.MENU_GUEST_BOOK_PREVIEW:
                mListener.navigateToBookPreview();
                break;
            case Constants.MENU.MENU_GUEST_BOOK_ADD_PAGE:
                handleCreateNewPage();
                break;
            case Constants.MENU.MENU_GUEST_BOOK_EXPORT:
                handleExportBook();
                break;
            case Constants.MENU.MENU_GUEST_BOOK_LOCK:
                handleLockBook(Constants.GUEST_BOOK_STATUS.STATE_LOCK);
                break;
            case Constants.MENU.MENU_GUEST_BOOK_UN_LOCK:
                handleLockBook(Constants.GUEST_BOOK_STATUS.STATE_UN_LOCK);
                break;
            case Constants.MENU.MENU_GUEST_BOOK_PRINT:
                handleRequestPrint();
                break;
            case Constants.MENU.MENU_GUEST_BOOK_CHANGE_INFO:
                mListener.navigateToUpdateBookInfo();
                break;
            case Constants.MENU.MENU_GUEST_BOOK_DELETE_BOOK:
                showConfirmDeleteBook(currentBook);
                break;
        }
    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        //mAbTitle.setText(mRes.getString(R.string.guest_book_title));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        ImageView search = (ImageView) abView.findViewById(R.id.ab_search_btn);
        TextView mAbAction = (TextView) abView.findViewById(R.id.ab_agree_text);
        mAbMoreOption = (ImageView) abView.findViewById(R.id.ab_more_btn);
        search.setVisibility(View.GONE);
        mAbAction.setVisibility(View.GONE);
        mAbMoreOption.setVisibility(View.VISIBLE);
        mAbMoreOption.setOnClickListener(this);
        mAbBack.setOnClickListener(this);
    }

    private void getDataAnDraw(Bundle savedInstanceState) {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_NON);
        if (savedInstanceState != null) {
            bookId = savedInstanceState.getString(Constants.GUEST_BOOK.BOOK_ID);
        }
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        currentBook = mGuestBookHelper.getCurrentBookEditor();
        if (currentBook != null) {
            mAbTitle.setText(currentBook.getBookName());
            bookId = currentBook.getId();
            drawDetail();
        } else {
            showEmptyNote(mRes.getString(R.string.request_send_error));
           /* showProgressLoading();
            mGuestBookHelper.requestGetBookDetail(bookId, new GuestBookHelper.GetBookDetailListener() {
                @Override
                public void onSuccess(Book bookDetail) {

                }

                @Override
                public void onError(int error) {
                    hideEmptyView();

                }
            });*/
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }


    private void setViewListener() {
    }

    private void drawDetail() {
        hideEmptyView();
        setAdapter();
    }

    private void setAdapter() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mAdapter = new BookDetailAdapter(mApplication, currentBook.getPages(), this);
        mRecyclerView.setAdapter(mAdapter);
        setItemListener();
    }

    private void changeAdapter() {
        mAdapter.setListItems(currentBook.getPages());
        mAdapter.notifyDataSetChanged();
        setItemListener();
    }

    private void setItemListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                mListener.navigateToEditPage((Page) object);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void showOrHideOverFlowMenu() {
        PopupHelper.getInstance().showOrHideOverFlowMenu(mAbMoreOption, this, initOverFlowMenu());
    }

    public ArrayList<ItemContextMenu> initOverFlowMenu() {
        ArrayList<ItemContextMenu> items = new ArrayList<>();
        ItemContextMenu createPage = new ItemContextMenu(mRes.getString(R.string.guest_book_menu_create), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_ADD_PAGE);
        ItemContextMenu preview = new ItemContextMenu(mRes.getString(R.string.guest_book_menu_preview), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_PREVIEW);
        ItemContextMenu export = new ItemContextMenu(mRes.getString(R.string.guest_book_menu_export), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_EXPORT);
        /*ItemContextMenu lock = new ItemContextMenu(mParentActivity, mRes.getString(R.string.guest_book_menu_lock), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_LOCK);
        ItemContextMenu unLock = new ItemContextMenu(mParentActivity, mRes.getString(R.string.guest_book_menu_un_lock), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_UN_LOCK);*/
       /* ItemContextMenu print = new ItemContextMenu(mParentActivity, mRes.getString(R.string.guest_book_menu_print), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_PRINT);*/
        ItemContextMenu info = new ItemContextMenu(mRes.getString(R.string.guest_book_menu_info), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_CHANGE_INFO);
        ItemContextMenu delete = new ItemContextMenu(mRes.getString(R.string.guest_book_menu_delete), -1,
                null, Constants.MENU.MENU_GUEST_BOOK_DELETE_BOOK);
        //
        items.add(createPage);
        items.add(preview);
        /*if (currentBook.getLockState() == 0) {
            items.add(lock);
        } else {
            items.add(unLock);
        }*/
        items.add(export);
        items.add(delete);
        items.add(info);
        /*if (currentBook.getPages() != null && currentBook.getPages().size() >= Constants.GUEST_BOOK.MIN_PAGE_PRINT) {
            items.add(print);
        }*/
        return items;
    }

    private void showConfirmDeletePage(Page page) {
        if (currentBook.getPages() == null || currentBook.getPages().size() <= 1) {
            mParentActivity.showToast(R.string.guest_book_alert_delete_last_page);
        } else {
            String title = mRes.getString(R.string.guest_book_title_delete);
            String btnYes = mRes.getString(R.string.ok);
            String btnCancel = mRes.getString(R.string.cancel);
            String msg = mRes.getString(R.string.guest_book_confirm_delete_page);
            PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                    title, msg, btnYes, btnCancel, this, page, Constants.MENU.POPUP_GUEST_BOOK_DELETE_PAGE);
        }
    }

    private void handleDeletePage(final Page page) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestProcess(page.getBookId(), page.getPage(), Constants.GUEST_BOOK_STATUS.STATE_DELETE,
                Constants.GUEST_BOOK_SAVE.UPDATE_PAGE, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                        if (currentBook != null && currentBook.getPages() != null) {
                            currentBook.getPages().remove(page);
                            // update index
                            int i = 0;
                            for (Page item : currentBook.getPages()) {
                                item.setPosition(i);
                                i++;
                            }
                            changeAdapter();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(error, Toast.LENGTH_LONG);
                    }
                });
    }

    private void showConfirmDeleteBook(Book book) {
        String title = mRes.getString(R.string.guest_book_title_delete);
        String btnYes = mRes.getString(R.string.ok);
        String btnCancel = mRes.getString(R.string.cancel);
        String msg = mRes.getString(R.string.guest_book_confirm_delete_book);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                title, msg, btnYes, btnCancel, this, book, Constants.MENU.POPUP_GUEST_BOOK_DELETE_BOOK);
    }

    private void handleDeleteBook(final Book book) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestProcess(book.getId(), 0, Constants.GUEST_BOOK_STATUS.STATE_DELETE,
                Constants.GUEST_BOOK_SAVE.UPDATE_BOOK, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        if (mGuestBookHelper.getMyBooks() != null) {
                            for (Book item : mGuestBookHelper.getMyBooks()) {
                                if (item.getId() != null && item.getId().equals(book.getId())) {
                                    boolean removeOk = mGuestBookHelper.getMyBooks().remove(item);
                                    Log.d(TAG, "handleDeleteBook: " + removeOk);
                                    break;
                                }
                            }
                        }
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                        mParentActivity.onBackPressed();
                    }

                    @Override
                    public void onError(String error) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(error, Toast.LENGTH_LONG);
                    }
                });
    }

    private void handleCreateNewPage() {
        ArrayList<Page> pages = currentBook.getPages();
        if (pages == null || pages.isEmpty()) {
            mParentActivity.showToast(R.string.request_send_error);
        } else if (pages.size() >= Constants.GUEST_BOOK.MAX_PAGES) {
            mParentActivity.showToast(String.format(mRes.getString(R.string.guest_book_alert_max_pages),
                    Constants.GUEST_BOOK.MAX_PAGES), Toast.LENGTH_LONG);
        } else {
            String preview = null;
            Page lastPage = pages.get(pages.size() - 1);
            if (lastPage.getBackground() != null) {
                preview = lastPage.getBackground().getPath();
            }
            final Page newPage = new Page();
            newPage.setName(lastPage.getName());
            newPage.setPreview(preview);
            newPage.setBookId(lastPage.getBookId());
            newPage.setPage(lastPage.getPage() + 1);
            newPage.setPosition(pages.size());
            newPage.setCommon(new Common(lastPage.getCommon()));
            newPage.setBackground(new Background(lastPage.getBackground()));
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestSavePage(newPage, new GuestBookHelper.ProcessBookListener() {
                @Override
                public void onSuccess(String desc) {
                    mParentActivity.hideLoadingDialog();
                    currentBook.getPages().add(newPage);
                    changeAdapter();
                    mListener.navigateToEditPage(newPage);
                }

                @Override
                public void onError(String desc) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                }
            });
        }
    }

    private void handleLockBook(final int lockState) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);

        mGuestBookHelper.requestProcess(currentBook.getId(), 0, lockState,
                Constants.GUEST_BOOK_SAVE.UPDATE_BOOK, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                        if (lockState == Constants.GUEST_BOOK_STATUS.STATE_LOCK)
                            currentBook.setLockState(1);
                        else
                            currentBook.setLockState(0);
                    }

                    @Override
                    public void onError(String error) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(error, Toast.LENGTH_LONG);
                    }
                });
    }

    private void handleRequestPrint() {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        // key trung menu
        mGuestBookHelper.requestProcess(currentBook.getId(), 0, Constants.GUEST_BOOK_STATUS.STATE_PRINT,
                Constants.GUEST_BOOK_SAVE.UPDATE_BOOK, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onError(String error) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(error, Toast.LENGTH_LONG);
                    }
                });
    }

    private void handleExportBook() {
        if (currentBook == null || currentBook.getPages() == null) {
            mParentActivity.showToast(R.string.request_send_error);
        } else {
            if (exportBookAsyncTask != null) {
                exportBookAsyncTask.cancel(true);
                exportBookAsyncTask = null;
            }
            exportBookAsyncTask = new ExportBookAsyncTask();
            exportBookAsyncTask.execute();
        }
    }

    private class ExportBookAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (Page page : currentBook.getPages()) {
                if (!TextUtils.isEmpty(page.getPreview())) {
                    String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(page.getPreview());
                    Bitmap bitmap = ImageLoaderManager.getInstance(mApplication).loadGuestBookPreviewSync(url);
                    if (bitmap != null) {
                        String fileName = ImageHelper.IMAGE_GUEST_BOOK + System.currentTimeMillis() + Constants.FILE.JPEG_FILE_SUFFIX;
                        ImageHelper.saveBitmapToGallery(mParentActivity, fileName, bitmap, mApplication);
                        bitmap.recycle();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mParentActivity.hideLoadingDialog();
        }
    }

    public interface OnFragmentInteractionListener {
        void navigateToEditPage(Page page);

        void navigateToUpdateBookInfo();

        void navigateToBookPreview();
    }
}
package com.metfone.selfcare.fragment.message;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;

import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.ThreadListAdapterRecyclerView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.api.CommonApi;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.fragment.setting.SettingNotificationFragment;
import com.metfone.selfcare.fragment.setting.hidethread.PINSettingFragment;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.MoreAppInteractHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.helper.message.MarkReadMessageAsyncTask;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.helper.workmanager.SettingWorkManager;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.listeners.ThreadListInterface;
import com.metfone.selfcare.listeners.UpdateMessageListener;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.security.helper.SecurityApi;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.dialog.DialogConfirmChat;
import com.metfone.selfcare.ui.dialog.DialogConfirmHideConversation;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.view.drag_select.DragSelectTouchListener;
import com.metfone.selfcare.ui.view.drag_select.DragSelectionProcessor;
import com.metfone.selfcare.ui.view.load_more.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.packet.ReengMessagePacket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static android.view.View.OVER_SCROLL_NEVER;

/**
 * Created by thanhnt72 on 9/27/2016.
 */

public class ThreadListFragmentRecycleView extends BaseLoginAnonymousFragment implements
        ReengMessageListener,
        ContactChangeListener,
        InitDataListener,
        ConfigGroupListener,
        ContentConfigBusiness.OnConfigBannerChangeListener,
        ThreadListInterface,
        ClickListener.IconListener,
        View.OnClickListener, UpdateMessageListener,
        PrefixChangeNumberHelper.UpdateListThread,
        ProfileListener {
    private static final String TAG = ThreadListFragmentRecycleView.class.getSimpleName();
    private static int TIME_DEFAULT_SHOW_TYPING = 10000;
    long beginResumeTime;
    private DeleteNLeaveGroupAsynctask leaveGroupAsynctask;

    private ThreadListAdapterRecyclerView mThreadListAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter;
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ContactBusiness mContactBusiness;
    private ContentConfigBusiness mConfigBusiness;
    private ReengAccountBusiness accountBusiness;
    private HomeActivity mHomeActivity;
    private Resources mRes;
    private Handler mHandler;
    private View mViewFooter;//mViewActionBar
    private RelativeLayout mRltChatMore;
    private EllipsisTextView mTvwChatMoreNumber;
    private ArrayList<BannerMocha> mBanners;
    private ArrayList<ThreadMessage> mListThreads;
    private ArrayList<Object> mListObjects;
    //--------------Variable Views-----------------
    private RecyclerView mRecyclerViewMessage;
    private TextView mTvwNoteEmpty, mTvwChatMoreText, mTvwNumberContact, mTvwFromMocha;
    private ProgressLoading mProgressLoading;
    private boolean isPagerVisible = false;
    //--------------------End---------------------
    private View rootView, mViewNoMessage, rlNoThreadRoot;
    private ImageView imgNewMessage;
    private CountDownTimer countDownTimerReceiveTyping;
    private ThreadMessage mThreadTyping = null;
    private int contactSupport = -1;
    private boolean isDataReady = false;
    private TextView mTvwThereAre;
    private TextView mTvwContacts;
    private boolean isSelectMode = false;
    private boolean isDisableScrollRecyclerview = false;

    private DragSelectTouchListener mDragSelectTouchListener;
    private SettingBusiness mSettingBusiness;
    private LottieAnimationView lav_empty;
    private LinearLayout mTvwNotifyOff;

    private boolean hasMessageYet = false;

    public ThreadListFragmentRecycleView() {
        // Required empty public constructor
    }

    public static ThreadListFragmentRecycleView newInstance() {
        ThreadListFragmentRecycleView fragment = new ThreadListFragmentRecycleView();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        Log.i("ThreadListFragmentRecycleView", "newInstance");
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "onAttach");
        mHomeActivity = (HomeActivity) activity;
        mApplication = (ApplicationController) mHomeActivity.getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        accountBusiness = mApplication.getReengAccountBusiness();
        mConfigBusiness = mApplication.getConfigBusiness();
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        mRes = mHomeActivity.getResources();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        mListObjects = new ArrayList<>();
        ListenerHelper.getInstance().addUpdateMessageListener(this);
        ListenerHelper.getInstance().addUpdateAllThreadListener(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        rootView = inflater.inflate(R.layout.fragment_thread_list_recycleview, container, false);
        hasMessageYet = mApplication.getPref().getBoolean(Constants.PREFERENCE.PREF_HAS_MESSAGE_YET, false);
        ListenerHelper.getInstance().addProfileChangeListener(this);
        if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
            ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
        } else  {
            if (UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT &&
                    new UserInfoBusiness(getContext()).getLogInMode() != 0 &&
                    UserInfoBusiness.checkEmptyPhoneNumber(getContext()) == true){
                ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
            } else {
                ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.VISIBLE);
                mHomeActivity.setCheckTabMobile(false);
            }
        }

        findComponentViews(rootView, container, inflater);
        setAdapter();
        setViewListeners();
        hideOrVisibleFooter(false, false);
        // [Start] CamID
//        initViewLogin(inflater, container, rootView);
        // [End]
//        mapViewInviteUser(rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "[] onResume");
        beginResumeTime = System.currentTimeMillis();
        if (mHandler == null) {
            mHandler = new Handler();
        }
        ListenerHelper.getInstance().addInitDataListener(this);
        ListenerHelper.getInstance().addConfigBannerListener(this);
        mHomeActivity.setThreadListHandler(this);
        //        showConnectionStatus();

        if (mApplication.isDataReady()) {
            Log.i(TAG, "[] onResume data ready");
            isDataReady = true;
            mMessageBusiness.addReengMessageListener(this);
            mMessageBusiness.addConfigGroupListener(this);
            ListenerHelper.getInstance().addContactChangeListener(this);
            //            startAsynctaskInitThreads();
            mProgressLoading.setVisibility(View.GONE);
            if (mConfigBusiness != null) {
                mBanners = mConfigBusiness.getListBanners();
            } else {
                mBanners = new ArrayList<>();
            }
            displaySearchResultOrAllThread();
            contactSupport = mContactBusiness.getSizeContactSupport();
            if (contactSupport >= 0) {
                mTvwNumberContact.setText(String.valueOf(contactSupport));
                displayNoMessageView();
            }
            logGaViewBanner();
            /*if (!mApplication.getConfigBusiness().isDisableChangePrefix() && isPagerVisible) {
                long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_SHOW_DIALOG_CHANGE_PREFIX, 0L);
                if (!TimeHelper.checkTimeInDay(lastShow)) {
                    PrefixChangeNumberHelper.getInstant(mApplication).showDialogConfirmChangePrefixContact(mHomeActivity, false);
                }
            }*/
            doSomethingOnFirstTimeDay();
        } else {
            mProgressLoading.setVisibility(View.VISIBLE);
            //data loading
        }
        Log.i(TAG, "[] onResume take " + (System.currentTimeMillis() - beginResumeTime) + " ms");
        beginResumeTime = System.currentTimeMillis();

        if (NotificationManagerCompat.from(mApplication).areNotificationsEnabled() && mSettingBusiness.getPrefSettingOnNoti()) {
            Log.i(TAG, "enable notify");
            mTvwNotifyOff.setVisibility(View.GONE);
        } else {
            Log.i(TAG, "disable notify");
            mTvwNotifyOff.setVisibility(View.VISIBLE);
        }

        if (mApplication.isParseNewMessage()) {
            mHomeActivity.showLoadingDialog("", R.string.loadding_data);
        }
    }

    private void doSomethingOnFirstTimeDay() {
        checkUploadLog();
        mConfigBusiness.getConfigFromServer(false);
//        checkShowDialogSendSmsWithoutInternet();
//        checkLoadSavingBudget();
        checkLoadSecurityData();
        checkLoadAdsMainTabData();
    }

    /*private void checkShowDialogSendSmsWithoutInternet() {
        if (!mApplication.getConfigBusiness().isEnableSmsNoInternet()) return;
        if (!mApplication.getReengAccountBusiness().isViettel() || NetworkHelper.isConnectInternet(mApplication) || !isPagerVisible)
            return;
        long timeShowDialog = mApplication.getPref().getLong(PREF_HAS_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, 0);
        if (TimeHelper.checkTimeInDay(timeShowDialog)) {
            return;
        }
        long timeUse = mApplication.getPref().getLong(PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, 0);
        if (TimeHelper.checkTimeInDay(timeUse)) {
            return;
        }
        DialogConfirm dialogRightClick = new DialogConfirm(mHomeActivity, true);
        dialogRightClick.setLabel(mRes.getString(R.string.send_sms_without_internet_title));
        dialogRightClick.setMessage(mRes.getString(R.string.send_sms_without_internet_msg_popup));
        dialogRightClick.setPositiveLabel(mRes.getString(R.string.send));
        dialogRightClick.setNegativeLabel(mRes.getString(R.string.skip));
        dialogRightClick.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                NavigateActivityHelper.navigateToCreateChat(mHomeActivity,
                        Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT, Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT);
            }
        });
        dialogRightClick.show();
        mApplication.getPref().edit().putLong(PREF_HAS_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, System.currentTimeMillis()).apply();
    }*/

    private void checkUploadLog() {
        if (!mApplication.getReengAccountBusiness().isEnableUploadLog()) return;
        long lastUploadTime = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOG, 0L);
        if (TimeHelper.checkTimeInDay(lastUploadTime)) {
            Log.i(TAG, "File was uploaded today");
            return;
        }
        Log.i(TAG, "start upload after 30 second");
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mApplication != null) {
                    LogKQIHelper.getInstance(mApplication).uploadLogContent();
                }
            }
        }, 30000);
    }

    private void checkLoadSavingBudget() {
        if (mApplication.getConfigBusiness().isEnableSaving() && mApplication.getReengAccountBusiness().isOperatorViettel()) {
            final long lastTime = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_SAVING_BUDGET, 0L);
            if (TimeHelper.checkTimeInDay(lastTime)) {
                Log.i(TAG, "Saving data was updated today");
                return;
            }
            Log.i(TAG, "start get data Saving after 30 second");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mApplication != null && !TimeHelper.checkTimeInDay(lastTime)) {
                        AVNOHelper.getInstance(mApplication).getSavingBudgetNum();
                    }
                }
            }, 30000);
        }
    }

    private void checkLoadSecurityData() {
        if (mApplication.getConfigBusiness().isEnableTabSecurity()) {
            final long lastTime = SharedPref.newInstance(mApplication).getLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_SECURITY_DATA, 0L);
            if (TimeHelper.checkTimeInDay(lastTime)) {
                Log.i(TAG, "Security data was updated today");
                return;
            }
            Log.i(TAG, "start get data Security after 30 second");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mApplication != null && !TimeHelper.checkTimeInDay(lastTime)) {
                        SecurityApi.getInstance().getConfig(null);
                        SecurityApi.getInstance().getLog(null);
                    }
                }
            }, 30000);
        }
    }

    private void checkLoadAdsMainTabData() {
        if (mApplication.isLoadedAdsMainTab()) {
            CommonApi.getInstance().setLoadingAdsMainTab(false);
        } else {
            CommonApi.getInstance().getAdsMainTab();
            return;
        }
        final long lastTime = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LAST_TIME_GET_ADS_MAIN_TAB_DATA, Long.class);
        if (TimeHelper.checkTimeInDay(lastTime)) {
            Log.i(TAG, "AdsMainTab data was updated today");
            return;
        }
        Log.i(TAG, "start get data AdsMainTab after 30 second");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mApplication != null && !TimeHelper.checkTimeInDay(lastTime)) {
                    CommonApi.getInstance().getAdsMainTab();
                }
            }
        }, 30000);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void displaySearchResultOrAllThread() {
        mListThreads = getAndSortThreadMessages();
        if(getParentFragment() != null){
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
            } else  {
                if (UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT &&
                        new UserInfoBusiness(getContext()).getLogInMode() != 0 &&
                        UserInfoBusiness.checkEmptyPhoneNumber(getContext()) == true){
                    ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
                } else if (mHomeActivity.getCheckTabMobile() != false) {
                    ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.GONE);
                } else {
                    ((TabMobileFragment)getParentFragment()).setFlMenuVisible(View.VISIBLE);
                }
            }
        }
        if (isSelectMode)
            setStateCheckBoxAll();
        notifyChangeAdapter();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        ListenerHelper.getInstance().removeConfigBannerListener(this);
        ListenerHelper.getInstance().removeInitDataListener(this);
        mMessageBusiness.removeReengMessageListener(this);
        mMessageBusiness.removeConfigGroupListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        mHomeActivity.setThreadListHandler(null);
        // reset typing
        if (countDownTimerReceiveTyping != null) {
            countDownTimerReceiveTyping.cancel();
            countDownTimerReceiveTyping = null;
        }
        if (mThreadTyping != null) {
            mThreadTyping.setShowTyping(false);
        }
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
//        isPagerVisible = false;
        super.onStop();
    }

    public void onDestroy() {
        ListenerHelper.getInstance().removeUpdateMessageListener(this);
        ListenerHelper.getInstance().removeUpdateAllThreadListener(this);
        Http.cancel(CommonApi.TAG_GET_LIST_MEDIA_BOX);
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        Log.i(TAG, "onDestroy");
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ListenerHelper.getInstance().removeProfileChangeListener(this);
    }

    @Override
    protected String getSourceClassName() {
        return "Chat";
    }

    @Override
    public void onDataReady() {
        Log.d(TAG, "[] onDataReady begin");
        isDataReady = true;
        mMessageBusiness.addReengMessageListener(this);
        ListenerHelper.getInstance().addContactChangeListener(this);
        contactSupport = mContactBusiness.getSizeContactSupport();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgressLoading.setVisibility(View.GONE);
                Log.i(TAG, "onDataReady update UI contactSupport = " + contactSupport);
                if (contactSupport >= 0) {
                    mTvwNumberContact.setText(String.valueOf(contactSupport));
                    displayNoMessageView();
                }
                mBanners = mConfigBusiness.getListBanners();
                displaySearchResultOrAllThread();
                logGaViewBanner();
            }
        });
    }

    @Override
    public void onConfigBannerChanged(ArrayList<BannerMocha> listBanners) {
        Log.d(TAG, "onConfigBannerChanged: ");
        if (mHandler == null) {
            mHandler = new Handler();
        }
        if (mApplication.isDataReady()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mBanners = mConfigBusiness.getListBanners();
                    displaySearchResultOrAllThread();
                    logGaViewBanner();
                }
            });
        }
    }

    @Override
    public void onConfigBannerCallChanged(ArrayList<BannerMocha> listBannersCall) {

    }

    @Override
    public void onAbDeleteClick() {
        Log.d(TAG, "onAbDeleteClick-->");
        ThreadListFragmentRecycleView.DeleteThreadListAsyncTask asyncTask = new ThreadListFragmentRecycleView
                .DeleteThreadListAsyncTask();
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onPageStateVisible(boolean isVisible, int prePos) {
        Log.i(TAG, "onPageStateVisible: " + isVisible);
        isPagerVisible = isVisible;
        if (isPagerVisible) {
            logGaViewBanner();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isPagerVisible = isVisibleToUser;
        if (isVisibleToUser) {
            checkFirstLastVisible();
        }
        Log.i(TAG, "setUserVisibleHint: " + isVisibleToUser);
    }

    private void checkFirstLastVisible() {
        if (mRecyclerViewMessage != null && mHeaderAndFooterRecyclerViewAdapter != null) {
            if (mListObjects == null || mListObjects.isEmpty()) {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(true));
                mRecyclerViewMessage.setNestedScrollingEnabled(false);
                return;
            }

            LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerViewMessage.getLayoutManager());
            if (layoutManager == null) return;
            int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
            int findLastCompletelyVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition();
            if (findFirstCompletelyVisibleItemPosition == 0 && findLastCompletelyVisibleItemPosition
                    == (mHeaderAndFooterRecyclerViewAdapter.getItemCount() - 1)) {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(true));
                mRecyclerViewMessage.setNestedScrollingEnabled(false);
            } else {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(false));
                mRecyclerViewMessage.setNestedScrollingEnabled(true);
            }
        }
    }

    @Override
    public void onTabReselected() {
        if (mRecyclerViewMessage != null) {
            if (Config.Extras.SMOOTH_SCROLL)
                mRecyclerViewMessage.smoothScrollToPosition(0);
            else
                mRecyclerViewMessage.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onMarkAllAsRead() {
        handleMarkRead(true, false);
    }

    private void setAdapter() {
        mThreadListAdapter = new ThreadListAdapterRecyclerView(mHomeActivity,
                Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE, this);
        mThreadListAdapter.setShowIconPin(true);
//        mRecyclerViewMessage.setHasFixedSize(true);
        mHeaderAndFooterRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(mThreadListAdapter);
        mRecyclerViewMessage.setAdapter(mHeaderAndFooterRecyclerViewAdapter);

        mHeaderAndFooterRecyclerViewAdapter.addFooterView(mViewFooter);
    }

    private void setViewListeners() {
        setListViewItemClickListener();
        mRltChatMore.setOnClickListener(this);
        mTvwNotifyOff.setOnClickListener(this);
        rlNoThreadRoot.setOnClickListener(this);
        imgNewMessage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.rlNoThreadRoot:
            case R.id.newMessage:
                NavigateActivityHelper.navigateToCreateChat(mHomeActivity,
                        Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT, Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT);
                break;
            case R.id.tvw_disable_notify:
                if (!NotificationManagerCompat.from(mApplication).areNotificationsEnabled()) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", mHomeActivity.getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        mHomeActivity.showToast(R.string.e601_error_but_undefined);
                    }
                } else if (mSettingBusiness != null && !mSettingBusiness.getPrefSettingOnNoti()) {
                    // bật thông báo của mocha
                    mSettingBusiness.setPrefSettingOnOffNoti(true, -1);
                    SettingWorkManager.cancelSettingNotiWork();
                    if (mTvwNotifyOff != null) {
                        if (NotificationManagerCompat.from(mApplication).areNotificationsEnabled() && mSettingBusiness.getPrefSettingOnNoti()) {
                            mTvwNotifyOff.setVisibility(View.GONE);
                        } else {
                            mTvwNotifyOff.setVisibility(View.VISIBLE);
                        }
                    }
                    Toast.makeText(getContext(), getString(R.string.enable_noti_success), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public HashSet<Integer> getThreadSelection() {
        if (mListThreads == null || mListThreads.isEmpty()) return new HashSet<>();
        HashSet<Integer> listSelected = new HashSet<>();
        int bannerSize = 0;
        if (mBanners != null) bannerSize = mBanners.size();
        try {
            for (int i = 0; i < mListThreads.size(); i++) {
                ThreadMessage threadMessage = mListThreads.get(i);
                if (threadMessage.isChecked()) {
                    listSelected.add(i + bannerSize);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "getThreadSelection exception", e);
        }
        return listSelected;
    }

    public void selectRange(int start, int end, boolean isSelected) {
        if (mListThreads == null || mListThreads.isEmpty()) return;
        if (!isSelectMode) return;
        if (start > end || start < 0 || end >= mListThreads.size()) return;
        try {
            int bannerSize = 0;
            if (mBanners != null) bannerSize = mBanners.size();
            for (int i = start; i <= end; i++) {
                ThreadMessage threadMessage = mListThreads.get(i);
                if (threadMessage != null) {
                    threadMessage.setChecked(isSelected);
                }
            }
            mThreadListAdapter.notifyItemRangeChanged(start + bannerSize, end - start + 1);
            if (isSelectMode) {
                setStateCheckBoxAll();
                boolean selectAll = mListThreads != null && sizeChecked == mListThreads.size();
                boolean statePin = getStateIconPinThread();
                EventBus.getDefault().post(new TabMobileFragment.SelectItemEvent(sizeChecked, selectAll, statePin));
            }
        } catch (Exception e) {
            Log.e(TAG, "selectRange exception", e);
        }
    }

    private boolean getStateIconPinThread() {
        if (mListThreads == null || mListThreads.isEmpty()) return false;
        ArrayList<ThreadMessage> listSelected = new ArrayList<>();

        for (ThreadMessage threadMessage : mListThreads) {
            if (threadMessage.isChecked()) {
                listSelected.add(threadMessage);
            }
        }

        if (listSelected.isEmpty()) return true;
        else {
            for (ThreadMessage threadMessage : listSelected) {
                if (threadMessage.getLastTimePinThread() == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private void handlePinThread() {
        if (mListThreads == null || mListThreads.isEmpty()) return;
        ArrayList<ThreadMessage> listSelected = new ArrayList<>();

        for (ThreadMessage threadMessage : mListThreads) {
            if (threadMessage.isChecked()) {
                listSelected.add(threadMessage);
            }
        }
        boolean statePin = false;

        if (listSelected.isEmpty()) statePin = true;
        else {
            statePin = false;
            for (ThreadMessage threadMessage : listSelected) {
                if (threadMessage.getLastTimePinThread() == 0) {
                    statePin = true;
                    break;
                }
            }
        }

        if (!listSelected.isEmpty()) {
            if (listSelected.size() > 3 && !statePin) {
                mHomeActivity.showToast(R.string.option_pin_max_item);
            } else {
                int sizePin = 0;
                for (ThreadMessage threadMessage : mListThreads) {
                    if (threadMessage.getLastTimePinThread() > 0) {
                        boolean hasSelected = false;
                        for (int i = 0; i < listSelected.size(); i++) {
                            if (threadMessage.getId() == listSelected.get(i).getId()) {
                                hasSelected = true;
                                break;
                            }
                        }
                        if (!hasSelected)
                            sizePin++;
                    }
                }
                if ((listSelected.size() + sizePin) > 3 && statePin) {
                    mHomeActivity.showToast(R.string.option_pin_max_item);
                } else {
                    PinThreadAsyncTask pinThreadAsyncTask = new PinThreadAsyncTask(listSelected, statePin);
                    pinThreadAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        }
    }

    private void handleMarkRead(boolean isAll, boolean isBackAfterComplete) {
        if (isAll) {
            new MarkReadMessageAsyncTask(mApplication, mHomeActivity,
                    mApplication.getMessageBusiness().getThreadMessageArrayList(), isBackAfterComplete).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            CopyOnWriteArrayList<ThreadMessage> listSelected = new CopyOnWriteArrayList<>();
            if (mListThreads != null) {
                for (ThreadMessage thread : mListThreads) {
                    if (thread.isChecked()) {
                        listSelected.add(thread);
                    }
                }
            }
            new MarkReadMessageAsyncTask(mApplication, mHomeActivity, listSelected, isBackAfterComplete).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void setListViewItemClickListener() {
        mThreadListAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View view, int position, Object itemObj) {
                if(getActivity() != null && getActivity() instanceof HomeActivity){
                    ((HomeActivity)getActivity()).logActiveTab(FirebaseEventBusiness.TabLog.TAB_CHAT);
                }
                Log.i(TAG, "-------------------------- onClick: " + position);
                //- mHeaderAndFooterRecyclerViewAdapter.getHeaderViewsCount() + mHeaderAndFooterRecyclerViewAdapter
                // .getFooterViewsCount()
                if (itemObj instanceof ThreadMessage) {
                    ThreadMessage threadMessage = (ThreadMessage) itemObj;
                    if (isSelectMode) {
                        ImageView cbx = view.findViewById(R.id.holder_checkbox);
                        cbx.setSelected(!cbx.isSelected());
                        threadMessage.setChecked(cbx.isSelected());
                        if (view != null) {
                            view.setSelected(cbx.isSelected());
                        }
                        setStateCheckBoxAll();
                        boolean selectAll = mListThreads != null && sizeChecked == mListThreads.size();
                        boolean statePin = getStateIconPinThread();
                        EventBus.getDefault().post(new TabMobileFragment.SelectItemEvent(sizeChecked, selectAll, statePin));
                    } else {
                        hideSearch(false);
                        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                            String roomId = threadMessage.getServerId();
                            String roomName = threadMessage.getThreadName();
                            String roomAvatar = null;
                            OfficerAccount accountRoomChat = mApplication.getOfficerBusiness()
                                    .getOfficerAccountByServerId(roomId);
                            if (accountRoomChat != null) {
                                roomAvatar = accountRoomChat.getAvatarUrl();
                            }
//                            DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mHomeActivity,
//                                    roomId, roomName, roomAvatar);
                        } else {
                            //DeepLinkHelper.getInstance().openSchemaLink(mHomeActivity, "mocha://home/stranger");
                            // TODO test
                            NavigateActivityHelper.navigateToChatDetail(mHomeActivity, threadMessage);
                        }
                    }
                } else if (itemObj instanceof PhoneNumber) {
                    PhoneNumber phoneNumber = (PhoneNumber) itemObj;
                    if (phoneNumber.getContactId() != null) {
                        String myNumber = accountBusiness.getJidNumber();
                        String number = phoneNumber.getJidNumber();
                        if (myNumber == null || number.equals(myNumber)) {
                            mHomeActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_LONG);
                        } else if (phoneNumber.isReeng() || (accountBusiness.isViettel() && phoneNumber.isViettel())) {
                            ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread(phoneNumber
                                    .getJidNumber());
                            // an search view khi click vao 1 item
                            hideSearch(true);
                            NavigateActivityHelper.navigateToChatDetail(mHomeActivity, thread);
                        } else {
                            //click vao row man hinh home ma so la so ngoai mang, chua cai mocha
                            InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, mHomeActivity,
                                    phoneNumber.getName(), phoneNumber.getJidNumber(), false);
                        }
                    }
                } else if (itemObj instanceof BannerMocha) {
                    if (isSelectMode) return;
                    BannerMocha banner = (BannerMocha) itemObj;
                    processClickBanner(banner);
                }
//                else if (itemObj instanceof MediaBoxItem) {
//                    if (isSelectMode) return;
//                    processClickMediaBoxItem((MediaBoxItem) itemObj);
//                }
                else {
                    Log.i(TAG, "object j do:");
                }
            }

            @Override
            public void onLongClick(View view, int position, Object itemObj) {
                Log.i(TAG, "onLongClick: " + position);
                if (itemObj instanceof ThreadMessage) {
                    if (mDragSelectTouchListener != null) {
                        mDragSelectTouchListener.startDragSelection(position);
                    }
                    if (!isSelectMode) {
                        int idSelected = -1;
                        idSelected = ((ThreadMessage) itemObj).getId();
                        ((ThreadMessage) itemObj).setChecked(true);
                        isSelectMode = true;
                        mThreadListAdapter.setSelectMode(true);
                        TabMobileFragment.TabMobileMessageEvent event = new TabMobileFragment.TabMobileMessageEvent(idSelected);
                        event.setSelectMode(true);
                        event.setSelectAll(mListThreads != null && mListThreads.size() == 1);
                        event.setPosScroll(mRecyclerViewMessage.computeVerticalScrollOffset());
                        event.setStatePin(getStateIconPinThread());
                        EventBus.getDefault().post(event);
                        mThreadListAdapter.notifyDataSetChanged();
                        startSelectModeAnimation(true);
                        setStateCheckBoxAll();
                    }
                }

//                showOrHideAbDeleteView(true);
                /*if (itemObj instanceof ThreadMessage) {
                    ThreadMessage threadMessage = (ThreadMessage) itemObj;
                    ImageView cbx = (ImageView) view.findViewById(R.id.holder_checkbox);
                    cbx.setSelected(!cbx.isSelected());
                    threadMessage.setChecked(cbx.isSelected());
                    setStateCheckBoxAll();
                }*/
                //showPopupContextMenu(itemObj);
            }
        });
        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mRecyclerViewMessage.getLayoutManager() != null) {
                    int currentFirstVisible = ((LinearLayoutManager) mRecyclerViewMessage.getLayoutManager()).findFirstVisibleItemPosition();
                    if (currentFirstVisible > firstVisibleInListview) {
                        Log.i(TAG, "scroll up!");
                        EventBus.getDefault().post(new TabMobileFragment.RecyclerViewScrollEvent(false));
                    } else {
                        Log.i(TAG, "scroll down!");
                        EventBus.getDefault().post(new TabMobileFragment.RecyclerViewScrollEvent(true));
                    }
                    firstVisibleInListview = currentFirstVisible;
                }
            }
        };

        mRecyclerViewMessage.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

//    private void processClickMediaBoxItem(MediaBoxItem itemObj) {
//        String type = itemObj.getType();
//        String url = itemObj.getUrl();
//        if (TextUtils.isEmpty(type)) return;
//        mApplication.getConfigBusiness().saveItemMediaBoxUnNew(itemObj);
//        if (MediaBoxItem.ItemType.MOVIE.equals(type) || MediaBoxItem.ItemType.MUSIC.equals(type)) {
//            Utilities.processOpenLink(mApplication, mHomeActivity, url);
//        } else if (MediaBoxItem.ItemType.VIDEO.equals(type)) {
//            if (mApplication.getConfigBusiness().isEnableTabVideo()) {
//                Video video = new Video();
//                video.setChannel(null);
//                video.setLink(url);
//                video.setOriginalPath(url);
//                mApplication.getApplicationComponent().providesUtils().openVideoDetail(mHomeActivity, video);
//            } else
//                UrlConfigHelper.getInstance(mHomeActivity).gotoWebViewOnMedia(mApplication, mHomeActivity, url);
//        } else if (MediaBoxItem.ItemType.NEWS.equals(type)) {
//            //mHomeActivity.readFastNews(url);
//            Utilities.processOpenLink(mApplication, mHomeActivity, url);
//        } else if (MediaBoxItem.ItemType.DEEPLINK.equals(type)) {
//            DeepLinkHelper.getInstance().openSchemaLink(mHomeActivity, itemObj.getUrl());
//            notifyChangeAdapter();
//        } else {
//            Log.e(TAG, "CLGT???");
//        }
//
//    }

    private void startSelectModeAnimation(boolean isSelectMode) {
        if (mRes == null || mRecyclerViewMessage == null) return;
        if (mRecyclerViewMessage.getAnimation() != null) {
            mRecyclerViewMessage.getAnimation().cancel();
            mRecyclerViewMessage.clearAnimation();
        }
        final int marginLeft = mRes.getDimensionPixelSize(R.dimen.margin_more_content_40);
        if (isSelectMode) {
            isDisableScrollRecyclerview = true;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerViewMessage.getLayoutParams();
            layoutParams.leftMargin = -marginLeft;
            mRecyclerViewMessage.setLayoutParams(layoutParams);
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);
                    int marginLeftTime = (int) (interpolatedTime * marginLeft - marginLeft);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerViewMessage.getLayoutParams();
                    layoutParams.leftMargin = marginLeftTime;
                    mRecyclerViewMessage.setLayoutParams(layoutParams);
                }

                @Override
                public void cancel() {
                    super.cancel();
                    isDisableScrollRecyclerview = false;
                }
            };
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    isDisableScrollRecyclerview = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isDisableScrollRecyclerview = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animation.setDuration(400);
            mRecyclerViewMessage.startAnimation(animation);
        } else {
            isDisableScrollRecyclerview = false;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerViewMessage.getLayoutParams();
            layoutParams.leftMargin = marginLeft;
            mRecyclerViewMessage.setLayoutParams(layoutParams);
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);
                    int marginLeftTime = (int) (marginLeft - interpolatedTime * marginLeft);
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mRecyclerViewMessage.getLayoutParams();
                    layoutParams.leftMargin = marginLeftTime;
                    mRecyclerViewMessage.setLayoutParams(layoutParams);
                }

                @Override
                public void cancel() {
                    super.cancel();
                }
            };
            animation.setDuration(400);
            mRecyclerViewMessage.startAnimation(animation);
        }
    }

    private int firstVisibleInListview;

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mRecyclerViewMessage = rootView.findViewById(R.id.message_list_recycleview);
        mRecyclerViewMessage.setLayoutManager(new LinearLayoutManager(mRecyclerViewMessage.getContext()) {
            @Override
            public boolean canScrollVertically() {
                return !isDisableScrollRecyclerview && super.canScrollVertically();
            }
        });
        //mRecyclerViewMessage.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerViewMessage.setOverScrollMode(OVER_SCROLL_NEVER);
        mRecyclerViewMessage.setItemAnimator(null);
        setDragSelection();

        mTvwNoteEmpty = rootView.findViewById(R.id.message_list_note_empty);
        imgNewMessage = rootView.findViewById(R.id.newMessage);
        mTvwFromMocha = rootView.findViewById(R.id.tvFromMocha);
        mProgressLoading = rootView.findViewById(R.id.message_list_loading_progressbar);
        //        mViewConnectivityStatus = rootView.findViewById(R.id.connectivity_status_layout);
        mViewNoMessage = rootView.findViewById(R.id.no_message_layout);
        rlNoThreadRoot = rootView.findViewById(R.id.rlNoThreadRoot);
        mTvwNumberContact = rootView.findViewById(R.id.number_contact);
        mTvwThereAre = rootView.findViewById(R.id.note_there_are);
        mTvwContacts = rootView.findViewById(R.id.note_contacts);
        lav_empty = (LottieAnimationView) rootView.findViewById(R.id.lav_empty);

        InputMethodUtils.hideKeyboardWhenTouch(rootView, mHomeActivity);
        // chat voi so chua luu
        //footer hold to show more option
        mViewFooter = inflater.inflate(R.layout.footer_hold_more_option, container, false);
        mRltChatMore = mViewFooter.findViewById(R.id.chat_more_layout);
        mTvwChatMoreText = mViewFooter.findViewById(R.id.chat_more_option_text);
        mTvwChatMoreNumber = mViewFooter.findViewById(R.id.chat_more_number_content);
        mProgressLoading.setEnabled(true);
        showOrHideSearchView();
        actionOpenning();
        mTvwNotifyOff = rootView.findViewById(R.id.tvw_disable_notify);
    }
    private void actionOpenning() {
        lav_empty.playAnimation();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                lav_empty.cancelAnimation();
//            }
//        }, 2000);
    }
    private void showOrHideSearchView() {
        mTvwNoteEmpty.setVisibility(View.GONE);
        displayNoMessageView();
        if (mListThreads == null || mListThreads.isEmpty()) {
            hideOrVisibleFooter(false, false);
        } else {
            hideOrVisibleFooter(true, false);
        }
    }

    private void notifyChangeAdapter() {
        Log.i(TAG, "notifyChangeAdapter");
        if (mProgressLoading == null) return;
        mProgressLoading.setVisibility(View.GONE);
        displayNoMessageView();
        if (mListThreads == null || mListThreads.isEmpty()) {
            hideOrVisibleFooterInNotifyDataChange(false, false);
        } else {
            hideOrVisibleFooterInNotifyDataChange(true, false);
        }
        if (mListThreads != null) {
            mListObjects.clear();
            mListObjects.addAll(mListThreads);

//            boolean closeUntilNew = mApplication.getConfigBusiness().isCloseMediaBoxUntilHasNew();
//            if (!closeUntilNew) {
//                ArrayList<MediaBoxItem> listMediaBox = mApplication.getConfigBusiness().getListActiveMediaBox();
//                if (hasMessageYet && listMediaBox != null && !listMediaBox.isEmpty()) {
//                    if (mListThreads.size() >= 4) {
//                        mListObjects.addAll(4, listMediaBox);
//                    } else
//                        mListObjects.addAll(mListObjects.size(), listMediaBox);
//                }
//            }

            if (!mListThreads.isEmpty() && mBanners != null && !mBanners.isEmpty()) {
                //Check expire banner
                ArrayList<BannerMocha> bannerRemove = new ArrayList<>();
                for (BannerMocha banner : mBanners) {
                    if (banner.isExpired())
                        bannerRemove.add(banner);
                }
                mBanners.removeAll(bannerRemove);
                mListObjects.addAll(0, mBanners);
            }
        }
        if (mThreadListAdapter == null) {
            setAdapter();
        }

        mThreadListAdapter.setThreadList(mListObjects);
        mThreadListAdapter.notifyDataSetChanged();
        if (mHandler == null) {
            mHandler = new Handler();
        }
        checkFirstLastVisible();
    }

    @Override
    public void initListContactComplete(int sizeSupport) {
        contactSupport = sizeSupport;
        if (mHandler != null && contactSupport >= 0) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTvwNumberContact.setText("" + contactSupport);
                    displayNoMessageView();
                }
            });
        }
    }

    @Override
    public void onContactChange() {
        Log.d(TAG, "onContactChange");
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    displaySearchResultOrAllThread();
                }
            });
        }
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mThreadListAdapter != null)
                        mThreadListAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onRosterChange() {

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onIconClickListener(View view, Object entry, int arg) {
        switch (arg) {
            case Constants.MENU.DELETE:
                ThreadMessage threadMessage = (ThreadMessage) entry;
                if (threadMessage == null)
                    return;
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    showDialogConfirmLeave(threadMessage);
                } else {
                    // xoa khong rời nhóm nên không cần confirm
                    mMessageBusiness.deleteThreadMessage(threadMessage);
                }
                break;

            case Constants.MENU.ADD_CONTACT:
                String friendPhoneNumber = (String) entry;
                mApplication.getContactBusiness().navigateToAddContact(mHomeActivity, friendPhoneNumber,
                        friendPhoneNumber);
                break;
            case Constants.MENU.INFO:
                PhoneNumber phoneNumber = (PhoneNumber) entry;
                String myNumber = accountBusiness.getJidNumber();
                if (myNumber != null && myNumber.equals(phoneNumber.getJidNumber())) {
                    NavigateActivityHelper.navigateToMyProfile(mHomeActivity);
                } else {
                    NavigateActivityHelper.navigateToContactDetail(mHomeActivity, phoneNumber);
                }
                break;
            case Constants.MENU.POPUP_YES:
                if (leaveGroupAsynctask != null) {
                    leaveGroupAsynctask.cancel(true);
                    leaveGroupAsynctask = null;
                }
                leaveGroupAsynctask = new ThreadListFragmentRecycleView.DeleteNLeaveGroupAsynctask();
                leaveGroupAsynctask.execute((ThreadMessage) entry);
                break;
            case Constants.MENU.MENU_CONTACTS:
                NavigateActivityHelper.navigateToContactList(mHomeActivity, -1);
                break;
            case Constants.MENU.MENU_SETTING:
                NavigateActivityHelper.navigateToSettingActivity(mHomeActivity, -1);
                break;
            case Constants.MENU.MENU_PROFILE:
                NavigateActivityHelper.navigateToSettingActivity(mHomeActivity, Constants.SETTINGS.CHANGE_STATUS);
                break;
            case Constants.ACTION.HOLDER_INVITE_FRIEND:
                NavigateActivityHelper.navigateToChooseContact(mHomeActivity, Constants.CHOOSE_CONTACT
                                .TYPE_INVITE_FRIEND,
                        ((PhoneNumber) entry).getJidNumber(), null, null, false, true, -1, true);
                break;
            case Constants.MENU.POPUP_LEAVE_ROOM:
                ThreadMessage mThread = (ThreadMessage) entry;
                handleLeaveRoom(mThread);
                break;
            case Constants.MENU.LEAVE_GROUP:
                new LeaveGroupAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (ThreadMessage) entry);
                break;
            case Constants.MENU.DELETE_AND_LEAVE_GROUP:
                confirmDeleteThreadGroup((ThreadMessage) entry);
                break;
            default:
                break;
        }
    }

    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
        // co message moi maf trung thread voi thread dang show typing
        if (mThreadTyping != null && mThreadTyping.getId() == thread.getId()) {
            if (countDownTimerReceiveTyping != null) {
                countDownTimerReceiveTyping.cancel();
                countDownTimerReceiveTyping = null;
            }
            mThreadTyping.setShowTyping(false);
        }
//        mMessageBusiness.insertNewMessageToMemory(thread, message);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                displaySearchResultOrAllThread();
            }
        });
        mApplication.updateCountNotificationIcon();
    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mThreadListAdapter.notifyDataSetChanged();
                displayNoMessageView();
            }
        });
    }

    @Override
    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {
        // cap nhat trang thai thread cu neu co
        // truong hop typing den tu nhieu thread
        if (mThreadTyping != null) {
            mThreadTyping.setShowTyping(false);
        }
        ThreadMessage threadCorrect = null;
        if (thread == null) {
            threadCorrect = findThreadInList(phoneNumber);
        } else if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            threadCorrect = thread;
            threadCorrect.setPhoneNumberTyping(phoneNumber);
        }
        if (threadCorrect != null) {// ton tai thread 1-1 trong danh sach
            mThreadTyping = threadCorrect;
            mThreadTyping.setShowTyping(true);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "phoneNumber: " + "notifyDataSetChanged");
                    mThreadListAdapter.notifyDataSetChanged();
                    if (countDownTimerReceiveTyping != null) {
                        countDownTimerReceiveTyping.cancel();
                        countDownTimerReceiveTyping = null;
                    }
                    startCountdownTimerReceiveTyping();
                }
            });
        }
    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {
        Log.d(TAG, "onUpdateMediaDetail from sv: " + threadId);
    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {

    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {

    }

    @Override
    public void onRefreshMessage(final int threadId) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                displaySearchResultOrAllThread();
            }
        });
    }

    @Override
    public void onGSMSendMessageError(ReengMessage message, XMPPResponseCode responseCode) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mThreadListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, final boolean showAlert, final String
            msgError) {
        if (msgError != null && msgError.length() > 0) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!showAlert) {
                        mHomeActivity.showToast(msgError, Toast.LENGTH_LONG);
                    }
                }
            });
        }
    }

    @Override
    public void onUpdateStateRoom() {

    }

    @Override
    public void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread) {

    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mThreadListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @Override
    public void notifyNewOutgoingMessage() {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mThreadListAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void displayNoMessageView() {
        imgNewMessage.setVisibility(View.GONE);
        if (contactSupport <= 1) {
            mTvwThereAre.setText(R.string.there_is);
            mTvwContacts.setText(R.string.had);
        } else {
            mTvwThereAre.setText(R.string.has);
            mTvwContacts.setText(R.string.had);
        }
        mTvwFromMocha.setText(R.string.from_camid);
        if (mListThreads != null && !mListThreads.isEmpty()) {
            mViewNoMessage.setVisibility(View.GONE);
            //            hideOrVisibleFooter(true);
        } else if (mProgressLoading.getVisibility() == View.VISIBLE) {
            mViewNoMessage.setVisibility(View.GONE);
            //            hideOrVisibleFooter(true);
        } else {
            if (contactSupport >= 0 && isDataReady) {
//                boolean closeUntilNew = mApplication.getConfigBusiness().isCloseMediaBoxUntilHasNew();
//                if (!closeUntilNew) {
//                    ArrayList<MediaBoxItem> listMediaBox = mApplication.getConfigBusiness().getListActiveMediaBox();
//                    if (hasMessageYet && listMediaBox != null && !listMediaBox.isEmpty()) {
//                        mViewNoMessage.setVisibility(View.GONE);
//                    } else
//                        mViewNoMessage.setVisibility(View.VISIBLE);
//                } else
                mViewNoMessage.setVisibility(View.VISIBLE);
            } else {
                mViewNoMessage.setVisibility(View.GONE);
            }
            //            hideOrVisibleFooter(false);
        }
        mTvwNoteEmpty.setVisibility(View.GONE);
    }

    private void hideOrVisibleFooter(boolean isVisible, boolean visibleChatMore) {
        if (mHeaderAndFooterRecyclerViewAdapter == null) return;
        Log.i(TAG, "hideOrVisibleFooter - isVisible: " + isVisible + " visibleChatMore: " + visibleChatMore);
        if (isVisible) {
            mViewFooter.setVisibility(View.VISIBLE);
            if (visibleChatMore) {
                mRltChatMore.setVisibility(View.VISIBLE);
                mTvwChatMoreText.setVisibility(View.GONE);
            } else {
                mRltChatMore.setVisibility(View.GONE);
                mTvwChatMoreText.setVisibility(View.VISIBLE);
            }
        } else {
            if (visibleChatMore) {
                mViewFooter.setVisibility(View.VISIBLE);
                mRltChatMore.setVisibility(View.VISIBLE);
                mTvwChatMoreText.setVisibility(View.GONE);
            } else {
                mViewFooter.setVisibility(View.GONE);
                mRltChatMore.setVisibility(View.GONE);
                mTvwChatMoreText.setVisibility(View.GONE);
            }
        }
    }

    private void hideOrVisibleFooterInNotifyDataChange(boolean isVisible, boolean visibleChatMore) {
        if (mHeaderAndFooterRecyclerViewAdapter == null) return;
        Log.i(TAG, "hideOrVisibleFooter - isVisible: " + isVisible + " visibleChatMore: " + visibleChatMore);
        if (isVisible) {
            mViewFooter.setVisibility(View.VISIBLE);
            if (visibleChatMore) {
                mRltChatMore.setVisibility(View.VISIBLE);
                mTvwChatMoreText.setVisibility(View.GONE);
            } else {
                mRltChatMore.setVisibility(View.GONE);
                mTvwChatMoreText.setVisibility(View.VISIBLE);
            }
        } else {
            if (visibleChatMore) {
                mViewFooter.setVisibility(View.VISIBLE);
                mRltChatMore.setVisibility(View.VISIBLE);
                mTvwChatMoreText.setVisibility(View.GONE);
            } else {
                mViewFooter.setVisibility(View.GONE);
                mRltChatMore.setVisibility(View.GONE);
                mTvwChatMoreText.setVisibility(View.GONE);
            }
        }
    }

    /**
     * kiem tra so dien thoai co trong danh sach thread tim kiem dc khong
     *
     * @param number
     * @param listObjects
     * @return boolean
     */
    private boolean isExistThread(String number, ArrayList<Object> listObjects) {
        if (TextUtils.isEmpty(number) || listObjects == null || listObjects.isEmpty()) {
            return false;
        } else {
            for (Object item : listObjects) {
                if (item instanceof PhoneNumber) {
                    return true;
                } else if (item instanceof ThreadMessage) {
                    ThreadMessage thread = (ThreadMessage) item;
                    //&&(thread.getStringFromListPhone().contains(number)
                    if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private ArrayList<ThreadMessage> getAndSortThreadMessages() {
        long beginTime = System.currentTimeMillis();
        try {
            ArrayList<ThreadMessage> listThreads = new ArrayList<>();
            ArrayList<ThreadMessage> listPins = new ArrayList<>();
            if (mMessageBusiness.getThreadMessageArrayList() == null ||
                    mMessageBusiness.getThreadMessageArrayList().isEmpty()) {
                return listThreads;
            }
            for (ThreadMessage threadMessage : mMessageBusiness.getThreadMessageArrayList()) {
                if (threadMessage.getHiddenThread() != 1)
                    if (threadMessage.getLastTimePinThread() != 0) {
                        listPins.add(threadMessage);
                    } else if (threadMessage.isReadyShow(mMessageBusiness)) {
                        listThreads.add(threadMessage);
                    }
            }
            if (!listThreads.isEmpty()) {
                if (!hasMessageYet) {
                    updateHasMessageYet();
                }
                Collections.sort(listThreads, ComparatorHelper.getComparatorThreadMessageByLastTime());
            }
            if (!listPins.isEmpty()) {
                Collections.sort(listPins, ComparatorHelper.getComparatorThreadMessageByLastTimePin());
                for (int i = 0; i < listPins.size(); i++) {
                    if (i == listPins.size() - 1)
                        listPins.get(i).setLastPin(true);
                    else
                        listPins.get(i).setLastPin(false);
                }
                listThreads.addAll(0, listPins);
            }
            Log.i(TAG, "[] getAndSortThreadMessages take " + (System.currentTimeMillis() - beginTime) + " ms");
            return listThreads;
        } catch (Exception e) {// ArrayIndexOutOfBoundsException
            Log.e(TAG, "Exception", e);
        }
        return new ArrayList<>();
    }

    private void updateHasMessageYet() {
        hasMessageYet = true;
        mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_HAS_MESSAGE_YET, true).apply();
    }

    private void confirmDeleteThreadGroup(ThreadMessage threadMessage) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_delete_groupchat);
        String msg = mRes.getString(R.string.msg_delete_group);
        PopupHelper.getInstance().showDialogConfirm(mHomeActivity, title,
                msg, labelOK, labelCancel, this, threadMessage, Constants.MENU.POPUP_YES);
    }

    private ThreadMessage findThreadInList(String number) {
        if (number == null || number.length() <= 0) {
            return null;
        }
        if (mListThreads == null || mListThreads.isEmpty()) {
            return null;
        }
        for (ThreadMessage thread : mListThreads) {
            if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    thread.getSoloNumber().equals(number)) {
                return thread;
            }
        }
        return null;
    }

    // typing
    private void startCountdownTimerReceiveTyping() {
        Log.d(TAG, "startCountdownTimerReceiveTyping");
        countDownTimerReceiveTyping = new CountDownTimer(TIME_DEFAULT_SHOW_TYPING, TIME_DEFAULT_SHOW_TYPING) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Log.d(TAG, "onFinish");
                if (mThreadTyping != null) {
                    Log.d(TAG, "onFinish---mThreadTyping != null");
                    mThreadTyping.setShowTyping(false);
                    if (mHandler != null) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "onFinish---mThreadTyping != notifyDataSetChanged");
                                mThreadListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        }.start();
    }

    public void showDialogConfirmLeave(ThreadMessage mThread) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_delete_room);
        String msg = mRes.getString(R.string.msg_delete_room);
        PopupHelper.getInstance().showDialogConfirm(mHomeActivity, title,
                msg, labelOK, labelCancel, this, mThread, Constants.MENU.POPUP_LEAVE_ROOM);
    }

    private void handleLeaveRoom(final ThreadMessage mThread) {
        OfficerAccount mOfficerAccountRoomChat = mApplication.getOfficerBusiness().
                getOfficerAccountByServerId(mThread.getServerId());
        if (mOfficerAccountRoomChat != null) {
            OfficerBusiness.RequestLeaveRoomChatListener leaveRoomChatListener = new OfficerBusiness
                    .RequestLeaveRoomChatListener() {
                @Override
                public void onResponseLeaveRoomChat() {
                    mHomeActivity.hideLoadingDialog();
                    mMessageBusiness.removePacketIdWhenDeleteRoom(mThread);
                    mMessageBusiness.deleteThreadMessage(mThread);
                }

                @Override
                public void onErrorLeave(int errorCode, String msg) {
                    mHomeActivity.hideLoadingDialog();
                    mHomeActivity.showToast(R.string.e601_error_but_undefined);
                }
            };
            mHomeActivity.showLoadingDialog(mRes.getString(R.string.loading), "");
            RoomChatRequestHelper.getInstance(mApplication).
                    leaveRoomChat(mOfficerAccountRoomChat.getServerId(), leaveRoomChatListener);
        }
    }

    @Override
    public void onUpdateMessageDone() {
        mHomeActivity.hideLoadingDialog();
    }

    @Override
    public void onProfileChange() {

    }

    @Override
    public void onRequestFacebookChange(String fullName, String birthDay, int gender) {

    }

    @Override
    public void onAvatarChange(String avatarPath) {
        mThreadListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCoverChange(String coverPath) {

    }

    @Override
    public void onUpdateListThread(HashSet<ThreadMessage> updatedThread) {
        if (updatedThread != null && updatedThread.size() != 0) {
            Log.i(TAG, "onUpdateListThread: " + updatedThread.size());
            mListThreads = getAndSortThreadMessages();
            notifyChangeAdapter();
        }
    }

    private class LeaveGroupAsynctask extends AsyncTask<ThreadMessage, Void, XMPPResponseCode> {
        private String title = mRes.getString(R.string.title_leave_groupchat);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;

        @Override
        protected void onPreExecute() {
            mHomeActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(ThreadMessage... params) {
            threadMessage = params[0];
            mHomeActivity.trackingEvent(R.string.ga_category_message_setting, R.string.leave_group, R.string
                    .leave_group);
            return mApplication.getMessageBusiness().leaveGroup(threadMessage);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mHomeActivity.hideLoadingDialog();
            int responseCode = params.getCode();
            if (responseCode == XMPPCode.E200_OK ||
                    responseCode == XMPPCode.E404_GROUP_NO_LONGER_EXIST ||
                    responseCode == XMPPCode.E415_NOT_ALLOW_LEAVE_ROOM ||
                    responseCode == XMPPCode.E416_NOT_ALLOW_INVITE) {
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                String msgNotify = mRes.getString(R.string.msg_noti_leave);
                Date date = new Date();
                mApplication.getMessageBusiness().insertNewMessageNotifyGroup(mApplication, threadMessage,
                        "packetId-leave-111",
                        threadMessage.getServerId(), myNumber, msgNotify, ReengMessagePacket.SubType.leave, date
                                .getTime(), null, MessageConstants.NOTIFY_TYPE.leave.name());
                mApplication.getMusicBusiness().onLeaveMusic(false);
            } else {
                mHomeActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(params);
        }
    }

    private class DeleteNLeaveGroupAsynctask extends AsyncTask<ThreadMessage, Void, XMPPResponseCode> {
        private String title = mRes.getString(R.string.title_delete_groupchat);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;

        @Override
        protected void onPreExecute() {
            mHomeActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(ThreadMessage... params) {
            //SystemClock.sleep(2000);
            threadMessage = params[0];
            MessageBusiness messageBusiness = mApplication.getMessageBusiness();
            return messageBusiness.leaveAndDeleteGroup(threadMessage);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mHomeActivity.hideLoadingDialog();
            int responseCode = params.getCode();
            if (responseCode == XMPPCode.E200_OK ||
                    responseCode == XMPPCode.E404_GROUP_NO_LONGER_EXIST ||
                    responseCode == XMPPCode.E415_NOT_ALLOW_LEAVE_ROOM ||
                    responseCode == XMPPCode.E416_NOT_ALLOW_INVITE) {
                //khong lam gi
            } else {
                mHomeActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(params);
        }
    }

    private void processClickBanner(BannerMocha banner) {
        int bannerType = banner.getType();
        String link = banner.getLink();
        String fakeMoConfirm = banner.getConfirm();
        if (TextUtils.isEmpty(link)) return;
        mHomeActivity.trackingEvent(mRes.getString(R.string.ga_category_banner),
                banner.getTitle(), mRes.getString(R.string.ga_label_banner_click));
        //xoa
        mBanners.remove(banner);
        notifyChangeAdapter();
        mConfigBusiness.saveConfigBannerToPref(mBanners, Constants.PREFERENCE.PREF_CONFIG_BANNER_CONTENT);
        //chuyen activity
        if (bannerType == Constants.BANNER.TYPE_WEB_VIEW) {
            UrlConfigHelper.getInstance(mApplication).gotoWebViewOnMedia(mApplication, mHomeActivity, link);
        } else if (bannerType == Constants.BANNER.TYPE_FEATURES) {
            if (mApplication != null)
                mApplication.getLogApi().logBanner(link, LogApi.TypeService.INBOX, null);// thêm log hành vi người dùng khi click vào banner
            DeepLinkHelper.getInstance().openSchemaLink(mHomeActivity, link);
        } else if (bannerType == Constants.BANNER.TYPE_OPEN_APP) {
            MoreAppInteractHelper.openMoreApp(mHomeActivity, link);
        } else if (bannerType == Constants.BANNER.TYPE_FAKE_MO) {
            ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, mHomeActivity, fakeMoConfirm, link,
                    "inbox_banner");
        }
    }

    private void logGaViewBanner() {
        if (!isPagerVisible) return;// page khong visible thi ko log
        // thread message trong thi ko hien banner
        if (mListThreads != null && !mListThreads.isEmpty() && mBanners != null && !mBanners.isEmpty()) {
            BannerMocha firstBanner = mBanners.get(0);
            if (firstBanner.getTitle() != null)
                mHomeActivity.trackingEvent(mRes.getString(R.string.ga_category_banner),
                        firstBanner.getTitle(), mRes.getString(R.string.ga_label_banner_view));
        }
    }

    private int sizeChecked = 0;

    private void setStateCheckBoxAll() {
        sizeChecked = 0;
        if (mListThreads != null) {
            for (ThreadMessage thread : mListThreads) {
                if (thread.isChecked()) {
                    sizeChecked++;
                }
            }
        }
    }

    private class DeleteThreadListAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            mHomeActivity.showLoadingDialog(mRes.getString(R.string.delete), mRes.getString(R.string.waiting));
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<ThreadMessage> listDelete = new ArrayList<>();
            if (mListThreads != null) {
                for (ThreadMessage thread : mListThreads) {
                    if (thread.isChecked()) {
                        listDelete.add(thread);
                    }
                }
            }
            mMessageBusiness.deleteListThreadMessage(listDelete);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mHomeActivity.hideLoadingDialog();
            mHomeActivity.onBackPressed();
            mMessageBusiness.notifyDeleteThreadMessage(-1);
            super.onPostExecute(aVoid);
        }
    }

    public void hideSearch(boolean needCheckFooter) {
//        mHomeActivity.enableSwipWhenSearchOrDelete(true);
        mHomeActivity.getNavigationBar().setVisibility(View.VISIBLE);
        if (needCheckFooter && mListThreads != null && !mListThreads.isEmpty())
            hideOrVisibleFooter(true, false);
    }

    private void setCheckAll(boolean checked) {
        if (mListThreads == null || mListThreads.isEmpty()) return;
        for (ThreadMessage threadMessage : mListThreads) {
            threadMessage.setChecked(checked);
        }
        if (checked) {
            sizeChecked = mListThreads.size();
        } else {
            sizeChecked = 0;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangePrefixContact(ChangePrefixContact event) {
        if (event.done) {
//            mHomeActivity.hideLoadingDialog();
//            IMService.getInstance().connectByToken(false);
            mApplication.connectByToken(false);
        } else {
            mApplication.getXmppManager().manualDisconnect();
//            mHomeActivity.showLoadingDialog("Đang cập nhật danh bạ", R.string.loading);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressEvent(final TabMobileFragment.BackPressEvent event) {
        if (event.isDeselectMode()) {
            isSelectMode = false;
            setCheckAll(false);
            if (mThreadListAdapter != null) {
                mThreadListAdapter.setSelectMode(false);
                mThreadListAdapter.notifyDataSetChanged();
                startSelectModeAnimation(false);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageFooterEvent(final TabMobileFragment.MessageFooterEvent event) {
        if (event == null) return;
        switch (event.getType()) {
            case TabMobileFragment.MessageFooterEvent.DELETE_THREAD_TYPE:
                if (sizeChecked == 0) return;
                boolean deleteAll = mListThreads != null && mListThreads.size() == sizeChecked && mListThreads.size() != 1;
                if (deleteAll) {
                    DialogConfirmChat dialogConfirm = new DialogConfirmChat(mHomeActivity, false);
                    dialogConfirm.setLabel(mRes.getString(R.string.delete_all_msg));
                    dialogConfirm.setMessage(mRes.getString(R.string.confirm_delete_all_msg));
                    dialogConfirm.setPositiveLabel(mRes.getString(R.string.delete));
                    dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
                    dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            onAbDeleteClick();
                        }
                    });
                    dialogConfirm.show();
                } else {
//                    onAbDeleteClick();
                    DialogConfirmChat dialogConfirm = new DialogConfirmChat(mHomeActivity, false);
                    dialogConfirm.setLabel(mRes.getString(R.string.delete_msg));
                    dialogConfirm.setMessage(mRes.getString(R.string.confirm_delete_msg));
                    dialogConfirm.setPositiveLabel(mRes.getString(R.string.delete));
                    dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
                    dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            onAbDeleteClick();
                        }
                    });
                    dialogConfirm.show();
                }
                break;

            case TabMobileFragment.MessageFooterEvent.PIN_THREAD_TYPE:
                handlePinThread();
                break;

            case TabMobileFragment.MessageFooterEvent.MARK_THREAD_TYPE:
                if (sizeChecked == 0) return;
                boolean isAll = mListThreads != null && mListThreads.size() == sizeChecked;
                handleMarkRead(isAll, true);
                break;

            case TabMobileFragment.MessageFooterEvent.HIDE_THREAD_TYPE:
                handleHideThread();
                break;
        }
    }

    private void handleHideThread() {
        if (mListThreads == null || mListThreads.isEmpty()) return;
        ArrayList<ThreadMessage> listSelected = new ArrayList<>();

        for (ThreadMessage threadMessage : mListThreads) {
            if (threadMessage.isChecked()) {
                listSelected.add(threadMessage);
            }
        }

        if (!listSelected.isEmpty()) {
            String currentEncryptPIN = mApplication.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
            processHideThread(!TextUtils.isEmpty(currentEncryptPIN), listSelected);
        }
    }

    private ArrayList<ThreadMessage> listSelectedHide;

    private void processHideThread(boolean hasPIN, ArrayList<ThreadMessage> listSelected) {
        listSelectedHide = listSelected;
        Intent intent = new Intent(mHomeActivity, SettingActivity.class);
        if (hasPIN) {
            intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_HIDE_THREAD);
            mHomeActivity.startActivity(intent, true);
        } else {
            DialogConfirmHideConversation dialogConfirm = new DialogConfirmHideConversation(mHomeActivity, true);
            if (listSelected.size() == 1) {
                dialogConfirm.setMessage(mHomeActivity.getString(R.string.msg_popup_hide_thread_chat));
                dialogConfirm.setLabel(mHomeActivity.getString(R.string.hide_thread_chat));
            } else {
                dialogConfirm.setMessage(mHomeActivity.getString(R.string.msg_popup_hide_multi_thread_chat));
                dialogConfirm.setLabel(mHomeActivity.getString(R.string.hide_multi_thread_chat));
            }
            dialogConfirm.setPositiveLabel(mHomeActivity.getString(R.string.set_pin));
            dialogConfirm.setNegativeLabel(mHomeActivity.getString(R.string.cancel));

            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    Intent intent = new Intent(mHomeActivity, SettingActivity.class);
                    intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_CREATE_PIN_HIDE_THREAD);
                    mHomeActivity.startActivity(intent, true);
                }
            });
            dialogConfirm.show();

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSettingNotiEvent(SettingNotificationFragment.SettingNotificationEvent event) {
        if (event.isOn()) {
            // bật thông báo của mocha
            if (mSettingBusiness != null) {
                mSettingBusiness.setPrefSettingOnOffNoti(true, -1);
                if (mTvwNotifyOff != null) {
                    if (NotificationManagerCompat.from(mApplication).areNotificationsEnabled() && mSettingBusiness.getPrefSettingOnNoti()) {
                        mTvwNotifyOff.setVisibility(View.GONE);
                    } else {
                        mTvwNotifyOff.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTabMobileHeaderEvent(final TabMobileFragment.HeaderEvent event) {
        if (event == null) return;
        switch (event.getType()) {
            case TabMobileFragment.HeaderEvent.SELECT_ALL_TYPE:
                boolean allChecked = mListThreads != null && mListThreads.size() == sizeChecked;
                if (allChecked) {
                    if (mHomeActivity != null) {
                        mHomeActivity.onBackPressed();
                    }
                } else {
                    setCheckAll(!allChecked);
                    boolean statePin = getStateIconPinThread();
                    EventBus.getDefault().post(new TabMobileFragment.SelectItemEvent(sizeChecked, !allChecked, statePin));
                    if (mThreadListAdapter != null) {
                        mThreadListAdapter.notifyDataSetChanged();
                    }
                }
                break;

            case TabMobileFragment.HeaderEvent.CANCEL_TYPE:
                break;
        }
    }

    public static class ChangePrefixContact {

        boolean done;

        public ChangePrefixContact(boolean done) {
            this.done = done;
        }
    }

    private class PinThreadAsyncTask extends AsyncTask<Void, Void, Void> {

        private ArrayList<ThreadMessage> mThreadMessages;
        private boolean statePin;

        public PinThreadAsyncTask(ArrayList<ThreadMessage> mThreadMessages, boolean statePin) {
            this.mThreadMessages = mThreadMessages;
            this.statePin = statePin;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mApplication.getMessageBusiness().pinThreadMessage(mThreadMessages, statePin);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            String desc;
            if (mThreadMessages.size() == 1) {
                if (statePin)
                    desc = mRes.getString(R.string.option_pin_1_item);
                else
                    desc = mRes.getString(R.string.option_unpin_1_item);
            } else {
                if (statePin)
                    desc = String.format(mRes.getString(R.string.option_pin_more_item), mThreadMessages.size());
                else
                    desc = String.format(mRes.getString(R.string.option_unpin_more_item), mThreadMessages.size());
            }
            mHomeActivity.showToast(desc);
            ListenerHelper.getInstance().onUpdateAllThread(new HashSet<ThreadMessage>(mThreadMessages));
//            mHomeActivity.onBackPressed();
        }
    }

    private void setDragSelection() {
        //Add the DragSelectListener
        DragSelectionProcessor dragSelectionProcessor = new DragSelectionProcessor(new DragSelectionProcessor.ISelectionHandler() {
            @Override
            public HashSet<Integer> getSelection() {
                return getThreadSelection();
            }

            @Override
            public boolean isSelected(int index) {
                return getThreadSelection().contains(index);
            }

            @Override
            public void updateSelection(int start, int end, boolean isSelected, boolean calledFromOnStart) {
                int bannerSize = 0;
                if (mBanners != null) bannerSize = mBanners.size();
                selectRange(start - bannerSize, end - bannerSize, isSelected);
            }
        }).withMode(DragSelectionProcessor.Mode.TOGGLE_AND_UNDO);
        mDragSelectTouchListener = new DragSelectTouchListener().withSelectListener(dragSelectionProcessor);
        if (mRecyclerViewMessage != null) {
            mRecyclerViewMessage.addOnItemTouchListener(mDragSelectTouchListener);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHideThreadChatEvent(final HideThreadChatEvent event) {
        if (event == null) return;
        if (event.type == PINSettingFragment.TYPE_SET_HIDE_THREAD) {
            if (listSelectedHide != null && !listSelectedHide.isEmpty() && isVisible()) {
                StringBuilder sbName = new StringBuilder();
                for (int i = 0; i < listSelectedHide.size(); i++) {
                    listSelectedHide.get(i).setHiddenThread(1);
                    mApplication.getMessageBusiness().updateThreadMessage(listSelectedHide.get(i));

                    sbName.append(listSelectedHide.get(i).getThreadName());
                    if (i < listSelectedHide.size() - 1) sbName.append(", ");
                }

                if (listSelectedHide.size() == 1) {
                    mHomeActivity.showToast(String.format(mHomeActivity.getString(R.string.hide_thread_success), sbName.toString()));
                } else {
                    mHomeActivity.showToast(String.format(mHomeActivity.getString(R.string.hide_multi_thread_success), sbName.toString()));
                }
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new TabMobileFragment.BackPressEvent(false, true));
                        mListThreads = getAndSortThreadMessages();
                        notifyChangeAdapter();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                TabMobileFragment.TabMobileMessageEvent event = new TabMobileFragment.TabMobileMessageEvent();
                                event.setSetHiddenThreadSucces(true);
                                EventBus.getDefault().post(event);
                            }
                        }, 30);
                    }
                }, 100);

            }
        } else if (event.type == PINSettingFragment.TYPE_SET_NEW_PIN) {
            for (int i = 0; i < listSelectedHide.size(); i++) {
                listSelectedHide.get(i).setHiddenThread(1);
                mApplication.getMessageBusiness().updateThreadMessage(listSelectedHide.get(i));
            }
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new TabMobileFragment.BackPressEvent(false, true));
                    mListThreads = getAndSortThreadMessages();
                    notifyChangeAdapter();

                    DialogMessage dialogMessage = new DialogMessage(mHomeActivity, true);
                    dialogMessage.setLabel(mHomeActivity.getString(R.string.setup_successfull));
                    dialogMessage.setMessage(mHomeActivity.getString(R.string.msg_setup_pin_success));
                    dialogMessage.setLabelButton(mHomeActivity.getString(R.string.permission_understood));
                    dialogMessage.setDismissListener(new DismissListener() {
                        @Override
                        public void onDismiss() {
                            TabMobileFragment.TabMobileMessageEvent event = new TabMobileFragment.TabMobileMessageEvent();
                            event.setSetUpPINSuccess(true);
                            EventBus.getDefault().post(event);
                        }
                    });



                    dialogMessage.show();
                }
            }, 50);
        }
    }

    public static class HideThreadChatEvent {

        int type;
        ThreadMessage threadMessage;

        public HideThreadChatEvent(int type) {
            this.type = type;
        }

        public HideThreadChatEvent(int type, ThreadMessage threadMessage) {
            this.type = type;
            this.threadMessage = threadMessage;
        }

        public ThreadMessage getThreadMessage() {
            return threadMessage;
        }

        public int getType() {
            return type;
        }
    }

    public ArrayList<ThreadMessage> getListThreads() {
        return mListThreads;
    }

    public ThreadListAdapterRecyclerView getThreadListAdapter() {
        return mThreadListAdapter;
    }

    public ArrayList<Object> getListObjects() {
        return mListObjects;
    }
}
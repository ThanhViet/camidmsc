package com.metfone.selfcare.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.adapter.IntroViewPagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.JSONHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.viewpagerindicator.CirclePageIndicator;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.Connection;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tungt on 9/8/2015.
 */
public class IntroduceFragment extends Fragment implements ClickListener.IconListener {
    private static final String TAG = IntroduceFragment.class.getSimpleName();
    private static final String REQUEST_TAG = "requestAutoDetect";

    private CirclePageIndicator mCirclePageIndicator;
    private ViewPager mViewPager;
    private IntroViewPagerAdapter mAdapter;
    private RelativeLayout mBtnRegister;
    private Resources mRes;
    private LoginActivity mLoginActivity;

    private String[] mTitles;
    private String[] mIntros;
    private String[] mImages;

    private ApplicationController mApplication;
    private OnFragmentDetectPhoneNumberListener listener;
    private ClickListener.IconListener mClickHandler;

    private boolean isDetectPhoneNumberFaile = false;
    private boolean isLoginDone = false;
    private boolean isLoginFalse = false;

    private int mResponseCode;
    private String mResponse;
    public String mMSISDN, mPassCode;
    private String mPhoneDetectBySim;
    private String mCurrentRegionCode = "VN";
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private String keyXxtea;

    public IntroduceFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mLoginActivity = (LoginActivity) activity;
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mRes = mLoginActivity.getResources();
        try {
            listener = (OnFragmentDetectPhoneNumberListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRes = getResources();

        getKeyFireBase();
    }

    private void getKeyFireBase() {
        keyXxtea = BuildConfig.KEY_XXTEA;

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        mFirebaseRemoteConfig.fetch(0)
                .addOnCompleteListener(mLoginActivity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.i(TAG, "Fetch Success");

                            // Once the config is successfully fetched it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        } else {
                            Log.i(TAG, "Fetch Failed");
                        }
                        keyXxtea = mFirebaseRemoteConfig.getString("KEY_XXTEA");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_introduce, container, false);
        tryDetectPhoneNumberByPhone();
        findComponent(rootView);
        initViewPager();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mClickHandler = this;
    }

    @Override
    public void onStart() {
        if (mBtnRegister != null) {
            mBtnRegister.setEnabled(true);
        }
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        mClickHandler = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void findComponent(View rootView) {
        mViewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        mCirclePageIndicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        mBtnRegister = (RelativeLayout) rootView.findViewById(R.id.btn_register);
        //onclick
        mBtnRegister.setOnClickListener(registerClickListener);
    }

    private void initViewPager() {
        mTitles = mRes.getStringArray(R.array.intro_titles);
        mIntros = mRes.getStringArray(R.array.intro_contents);
        mImages = mRes.getStringArray(R.array.intro_image_name);

        mAdapter = new IntroViewPagerAdapter(mLoginActivity, mTitles, mIntros, mImages);

        mViewPager.setAdapter(mAdapter);

        mCirclePageIndicator.setViewPager(mViewPager);
    }

    private void tryDetectPhoneNumberByPhone() {
        Phonenumber.PhoneNumber phoneNumberDetect = PhoneNumberHelper.getInstant().
                detectPhoneNumberFromDevice(mApplication);
        if (phoneNumberDetect != null) {
            PhoneNumberUtil phoneUtil = mApplication.getPhoneUtil();
            mPhoneDetectBySim = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    phoneUtil.format(phoneNumberDetect, PhoneNumberUtil.PhoneNumberFormat.E164));
            mCurrentRegionCode = phoneUtil.getRegionCodeForNumber(phoneNumberDetect);
        } else {
            String rc = PhoneNumberHelper.getInstant().detectRegionCodeFromDevice(mApplication);
            if (rc != null) {
                mCurrentRegionCode = rc;
            }
        }
    }

    public void autoDetectAndLoginByServer() {
        if (NetworkHelper.checkTypeConnection(mApplication) == NetworkHelper.TYPE_MOBILE) {
            String url;
            if (Config.Server.FREE_15_DAYS) {
                url = UrlConfigHelper.getInstance(mLoginActivity).getUrlGenOTP(Config.UrlEnum
                        .AUTO_DETECT_FREE_URL_V23);
            } else {
                url = UrlConfigHelper.getInstance(mLoginActivity).getUrlGenOTP(Config.UrlEnum.AUTO_DETECT_URL_V23);
            }
            StringBuilder sb = new StringBuilder("?platform=Android").
                    append("&os_version=").append(HttpHelper.EncoderUrl(Build.VERSION.RELEASE)).
                    append("&device=").append(HttpHelper.EncoderUrl(Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build
                    .MODEL)).
                    append("&revision=").append(HttpHelper.EncoderUrl(Config.REVISION)).
                    append("&version=").append(HttpHelper.EncoderUrl(BuildConfig.VERSION_NAME)).
                    append("&clientType=").append(HttpHelper.EncoderUrl(Constants.HTTP.CLIENT_TYPE_STRING));
            url += sb.toString();
            Log.i(TAG, "detect url=" + url);
            StringRequest request = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(final String s) {
                            String decryptResponse = HttpHelper.decryptResponse(s, keyXxtea);
                            processAutoLogin(decryptResponse);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mLoginActivity.trackingEvent(R.string.ga_category_register, R.string.ga_action_detect_phone, R.string
                            .ga_action_detecting_phone_fail);
                    isDetectPhoneNumberFaile = true;
                    if (listener != null) {
                        listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                    }
                }
            }
            );
            VolleyHelper.getInstance(mApplication).addRequestToQueue(request, REQUEST_TAG, false);
            mLoginActivity.trackingEvent(R.string.ga_category_register, R.string.ga_action_detect_phone, R.string
                    .detecting_phone);
        } else {
            String urlGetCountry = UrlConfigHelper.getInstance(mLoginActivity).getUrlConfigOfFile(
                    Config.UrlEnum.GET_COUNTRY);
            StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, urlGetCountry, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    String decryptResponse = HttpHelper.decryptResponse(s, keyXxtea);
                    if (!TextUtils.isEmpty(decryptResponse)) {
                        try {
                            JSONObject jsonObject = new JSONObject(decryptResponse);
                            String countryCode = jsonObject.optString("countryCode");
                            Log.i(TAG, "countryCode: " + countryCode);
                            if (!TextUtils.isEmpty(countryCode)) {
                                mCurrentRegionCode = countryCode;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (listener != null) {
                        listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    if (listener != null) {
                        listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("clientType", "Android");
                    params.put("revision", Config.REVISION);
                    Log.i(TAG, "params: " + params.toString());
                    return params;
                }
            };
            VolleyHelper.getInstance(mApplication).addRequestToQueue(request, REQUEST_TAG, false);
        }

    }

    private void processAutoLogin(String decryptResponse) {
        Log.i(TAG, "decryptResponse" + decryptResponse);
        try {
            JSONObject jsonObject = new JSONObject(decryptResponse);
            if (jsonObject.has(Constants.HTTP.REST_ERROR_CODE)) {
                mResponseCode = Integer.parseInt(jsonObject.getString(Constants.HTTP.REST_ERROR_CODE));
            }
            if (mResponseCode == HTTPCode.E0_OK) {
                if (jsonObject.has(Constants.HTTP.REST_MSISDN)) {
                    mMSISDN = jsonObject.getString(Constants.HTTP.REST_MSISDN);
                }
                if (jsonObject.has("code")) {
                    mPassCode = jsonObject.getString("code");
                }
                UrlConfigHelper.getInstance(mLoginActivity).detectSubscription(decryptResponse);
                mMSISDN = mMSISDN.trim();
                mPassCode = mPassCode.trim();
                isDetectPhoneNumberFaile = false;
                doLoginAction(mMSISDN, mPassCode);
                mLoginActivity.trackingEvent(R.string.ga_category_register,
                        R.string.ga_action_detect_phone,
                        String.format(mRes.getString(R.string.ga_action_detecting_phone_success), mMSISDN));
            } else if (mResponseCode == HTTPCode.E_NOT_DETECT_DEVICE) {
                isDetectPhoneNumberFaile = true;
                mLoginActivity.trackingEvent(R.string.ga_category_register, R.string
                        .ga_action_detect_phone, R.string.number_out_of_list_detect);
                String countryCode = jsonObject.optString("countryCode");
                Log.i(TAG, "countryCode: " + countryCode);
                if (!TextUtils.isEmpty(countryCode)) {
                    mCurrentRegionCode = countryCode;
                }
                if (listener != null) {
                    listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                }
            } else if (mResponseCode == HTTPCode.E_INTERNAL_SERVER_ERROR) {
                isDetectPhoneNumberFaile = true;
                mLoginActivity.showError(HTTPCode.getResourceOfCode(HTTPCode
                        .E501_INTERNAL_SERVER_ERROR), null);
                mLoginActivity.trackingEvent(R.string.ga_category_register, R.string
                        .ga_action_detect_phone, R.string.e500_internal_server_error);
            } else if (mResponseCode == HTTPCode.E_OUT_OF_WHITELIST) {
                isDetectPhoneNumberFaile = true;
                String labelExit = mRes.getString(R.string.close);
                String title = mRes.getString(R.string.note_title);
                String msg = JSONHelper.getResponseDescFromJSON(decryptResponse);
                PopupHelper.getInstance().showDialogConfirm(mLoginActivity, title,
                        msg, labelExit, null, mClickHandler, null, Constants.MENU.POPUP_EXIT);
            } else {
                isDetectPhoneNumberFaile = true;
                if (listener != null) {
                    listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            isDetectPhoneNumberFaile = true;
            if (listener != null) {
                listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
            }
        }
    }

    private void doLoginAction(String mPhoneNumber, String mLoginPassword) {
        new LoginByCodeAsyncTask(mLoginActivity.getApplicationContext()).
                executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mPhoneNumber, mLoginPassword);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPhoneNumber;
        String mCode;
        Context mContext;

        public LoginByCodeAsyncTask(Context ctx) {
            mContext = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //            mLoginActivity.showLoadingDialog("", R.string.processing);
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPhoneNumber = params[0];
            mCode = params[1];
            ApplicationController applicationController = (ApplicationController) mLoginActivity.getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            //TODO
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController,
                    mPhoneNumber, mCode, "", true, Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            //            mBtnRegister.setEnabled(true);
            Log.d(TAG, "responseCode = " + responseCode.getCode());
            if (responseCode.getCode() == XMPPCode.E200_OK) {
                isLoginDone = true;
                if (listener != null) {
                    listener.navigateToNextScreen();
                }
            } else {
                /*mLoginActivity.hideLoadingDialog();
                mLoginActivity.showError(responseCode.getDescription(), null);*/
                isLoginFalse = true;
                if (listener != null) {
                    listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                }
            }
        }
    }

    private View.OnClickListener registerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            autoDetectAndLoginByServer();
            mLoginActivity.showLoadingDialog("", R.string.processing);
            mBtnRegister.setEnabled(false);
        }
    };

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_EXIT:
                if (listener != null) {
                    listener.navigateToSetRegisterScreen(mPhoneDetectBySim, mCurrentRegionCode);
                }
                break;
            default:
                break;
        }
    }

    public interface OnFragmentDetectPhoneNumberListener {
        // Container Activity must implement this interface
        void displayPersonalInfo();

        void navigateToNextScreen();

        void navigateToSetRegisterScreen(String numberInput, String region);

    }
}

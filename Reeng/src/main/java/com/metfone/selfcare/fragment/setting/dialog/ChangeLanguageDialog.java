package com.metfone.selfcare.fragment.setting.dialog;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeLanguageDialog extends BottomSheetDialog {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.list_group)
    LinearLayout listGroup;

    private Activity context;
    private View bottomSheet;

    private ArrayList<String> keys;
    private ArrayList<String> values;

    public ChangeLanguageDialog(@NonNull Activity context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_change_language);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);

        getData();
        bindData();
        setOnShowDialog();
    }

    private void getData() {
        String[] myResArray = context.getResources().getStringArray(R.array.setting_language);
        List<String> myResArrayList = Arrays.asList(myResArray);

        if (Utilities.notEmpty(myResArrayList))
            for (String s : myResArrayList) {
                String[] language = s.split(";", 2);
                addData(language[1], language[0]);
            }
    }

    private void addData(String key, String value) {
        if (keys == null)
            keys = new ArrayList<>();
        if (values == null)
            values = new ArrayList<>();


        keys.add(key);
        values.add(value);
    }

    private void bindData() {
        if (keys == null || keys.size() <= 0) return;

        View view;
        TextView tvCheckBox;
        ImageView ivCheckBox;

        String currentLanguage = LocaleManager.getLanguage(context);


        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = values.get(i);


            view = LayoutInflater.from(context).inflate(R.layout.layout_radio_button, null, false);
            view.setOnClickListener(mOnClickListener);

            ivCheckBox = view.findViewById(R.id.iv_check_box);

            tvCheckBox = view.findViewById(R.id.tv_check_box);
            tvCheckBox.setText(value);

            if (currentLanguage.equals(key))
                checkView(ivCheckBox, tvCheckBox);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            listGroup.addView(view, layoutParams);
        }
    }

    private void checkView(ImageView ivCheckBox, TextView tvCheckBox) {
        ivCheckBox.setImageResource(R.drawable.ic_checkbox_video);
        tvCheckBox.setTextColor(ContextCompat.getColor(context, R.color.videoColorSelect));
    }

    private void setOnShowDialog() {
        bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Resources resources = getContext().getResources();
                if (bottomSheet != null && resources != null && bottomSheet.getWidth() > bottomSheet.getHeight()) {
                    BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                    int height = ScreenManager.getWidth(context) / 2;

                    if (bottomSheetBehavior != null)
                        if (height > bottomSheet.getHeight())
                            bottomSheetBehavior.setPeekHeight(bottomSheet.getHeight());
                        else
                            bottomSheetBehavior.setPeekHeight(height);
                }
            }
        });
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (listGroup != null) {

                int position = listGroup.indexOfChild(view);
                int idx = Math.min(Math.max(position, 0), keys.size() - 1);

                LocaleManager.setNewLocale(context, keys.get(idx));

                Intent intent = new Intent(context, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId,    intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                System.exit(0);

//                ApplicationController.self().setNeedChangeLanguageResource(true);

//                Intent i = new Intent(context, HomeActivity.class);
//                context.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            dismiss();
        }
    };
}

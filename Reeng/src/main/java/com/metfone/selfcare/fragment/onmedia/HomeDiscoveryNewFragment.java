package com.metfone.selfcare.fragment.onmedia;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.HomeDiscoveryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.model.onmedia.ContentDiscovery;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.ItemOffsetDecoration;
import com.metfone.selfcare.ui.SpannedGridLayoutManager;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/7/17.
 */

public class HomeDiscoveryNewFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener, RecyclerClickListener {

    private static final String TAG = HomeDiscoveryNewFragment.class.getSimpleName();

    private OnMediaActivityNew mActivity;
    private ApplicationController mApp;
    private MusicBusiness mMusicBusiness;
    private Resources mRes;
    private WSOnMedia rest;

    private ImageView mBack;
    private TextView mTitle;
    private RecyclerView mRecyclerView;
    private HomeDiscoveryAdapter adapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private SpannedGridLayoutManager layoutManager;
    private boolean isLoadMore, isEndList;
    private boolean isShowDetail = false;
    private int page = 1;
    private View mLayoutFooter, mLoadmoreFooterView;
    private int firstVisibleItem;
    private int lastVisibleItem;
    private int totalItemCount;
    private ArrayList<ContentDiscovery> listData = new ArrayList<>();
    private int currentItemIndex = 0;

    private int gaCategory;

    private HomeDiscoveryDetailFragment homeDiscoveryDetailFragment;

    public static HomeDiscoveryNewFragment newInstance() {
        HomeDiscoveryNewFragment fragment = new HomeDiscoveryNewFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (OnMediaActivityNew) activity;
        mApp = (ApplicationController) mActivity.getApplication();
        mMusicBusiness = mApp.getMusicBusiness();
        rest = new WSOnMedia(mApp);
        mRes = mApp.getResources();
        gaCategory = R.string.ga_category_discover;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discovery_new, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.addItemDecoration(new ItemOffsetDecoration(1));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLongClickable(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        layoutManager = new SpannedGridLayoutManager(new SpannedGridLayoutManager.GridSpanLookup() {
            @Override
            public SpannedGridLayoutManager.SpanInfo getSpanInfo(int position) {

                if ((position - 7) % 9 == 0 || position % 9 == 0) {
                    return new SpannedGridLayoutManager.SpanInfo(2, 2);
                } else {
                    return new SpannedGridLayoutManager.SpanInfo(1, 1);
                }
            }
        }, 3, 0.55f);
        mRecyclerView.setLayoutManager(layoutManager);

        //Loadmore
        mLayoutFooter = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLoadmoreFooterView = mLayoutFooter.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);
        RecyclerViewUtils.setFooterView(mRecyclerView, mLayoutFooter);

        createView(inflater, mRecyclerView, this);
        setAtionBar(view);
        page = 0;
        onLoadMore();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initDataView();
        initViewDetail();
        setListener();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.i(TAG, "setUserVisibleHint: " + isVisibleToUser);
        if (isVisibleToUser && mActivity != null) {
            mActivity.trackingEvent(gaCategory, R.string.ga_action_open_discover, R.string.ga_action_open_discover);
            page = 1;
            if (listData == null || listData.isEmpty()) {
                showProgressLoading();
            }
            rest.getContentDiscovery(new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    handleResponseGetListDiscovery(page, s);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "error: " + volleyError.toString());
                    showEmptyNote(mRes.getString(R.string.e601_error_but_undefined));
                }
            }, page);
        }
    }

    private void setAtionBar(View v) {
        View abView = v.findViewById(R.id.ab_detail);
        mBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        mTitle = (TextView) abView.findViewById(R.id.ab_title);
        mTitle.setText(R.string.title_discovery);
    }

    private void initDataView() {

        adapter = new HomeDiscoveryAdapter(mApp, listData, this);
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(adapter);
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
    }

    private void initViewDetail() {
        homeDiscoveryDetailFragment = new HomeDiscoveryDetailFragment();
        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.layout_detail, homeDiscoveryDetailFragment)
                .commitAllowingStateLoss();
    }

    private void handleResponseGetListDiscovery(int page, String s) {
        Log.i(TAG, "response: " + s);
        Log.i(TAG, "page: " + page);
        isLoadMore = false;
        try {
            ArrayList<ContentDiscovery> mListData = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(s);
            if (jsonObject.optInt("error_code") == HTTPCode.E200_OK) {
                if (jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    mListData.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jso = jsonArray.getJSONObject(i);
                        String title = jso.optString("title");
                        String thumb = jso.optString("thumb");
                        String path = jso.optString("android_path");
                        String videoType = jso.optString("video_type", "");
                        String linkNetnews = jso.optString("netnews_link", "");
                        ContentDiscovery contentDiscovery = new ContentDiscovery(thumb, path, title, videoType,
                                linkNetnews);
                        mListData.add(contentDiscovery);
                    }
                    if (mListData.isEmpty()) {
                        mApp.getFeedBusiness().getListContentDiscoveries().clear();
                        if (page == 1) {
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        } else {
                            isEndList = true;
                        }
                    } else {
                        if (page == 1) {
                            listData.clear();
                            mApp.getFeedBusiness().getListContentDiscoveries().clear();
                        }
                        int pos = listData.size();
                        listData.addAll(mListData);
                        if (page != 1) {
                            mHeaderAndFooterAdapter.notifyItemInserted(pos);
                        } else {
                            mHeaderAndFooterAdapter.notifyDataSetChanged();
                            mRecyclerView.post(new Runnable() {
                                @Override
                                public void run() {
                                    layoutManager.scrollToPosition(0);
                                }
                            });
                        }
                        mApp.getFeedBusiness().getListContentDiscoveries().addAll(mListData);
                    }
                    mApp.getFeedBusiness().initHashmapDiscover();
                }
            } else {
//                mActivity.showToast(R.string.e601_error_but_undefined);
                isEndList = true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            hideEmptyView();
//            mActivity.showToast(R.string.e601_error_but_undefined);
            isEndList = true;
        }

        hideEmptyView();
    }

    private void setListener() {
        mRecyclerView.addOnScrollListener(mApp.getPauseOnScrollRecyclerViewListener(new RecyclerView
                .OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
                if (!isEndList && newState == 0) {
                    boolean needToLoad = (lastVisibleItem >= totalItemCount) && (firstVisibleItem > 0);
                    if (!isLoadMore && needToLoad) {// dang ko load more
                        isLoadMore = true;
                        onLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                SpannedGridLayoutManager mGridLayoutManager = (SpannedGridLayoutManager) recyclerView
                        .getLayoutManager();
                if (mGridLayoutManager == null) return;

                firstVisibleItem = mGridLayoutManager.getFirstVisiblePosition();
                lastVisibleItem = mGridLayoutManager.getLastVisiblePosition();
                totalItemCount = mRecyclerView.getChildCount();
            }
        }));
    }

    private void onLoadMore() {
        Log.i(TAG, "onLoadMore");
        page = page + 1;
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        rest.getContentDiscovery(new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                handleResponseGetListDiscovery(page, s);
                mLoadmoreFooterView.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "error: " + volleyError.toString());
                mActivity.showToast(R.string.e601_error_but_undefined);
                mLoadmoreFooterView.setVisibility(View.GONE);
                isEndList = true;
            }
        }, page);
    }

    @Override
    public void onRetryClick() {

    }

    public void updateCurrentItemClick(int pos) {
        try {
            View currentView = mRecyclerView.getChildAt(currentItemIndex);
            View nextView = mRecyclerView.getChildAt(pos);

            if (currentView != null)
                currentView.setVisibility(View.VISIBLE);
            if (nextView != null) {
                nextView.setVisibility(View.GONE);
                if (homeDiscoveryDetailFragment != null) {
                    homeDiscoveryDetailFragment.setItemViewClick(nextView);
                }
            }
            currentItemIndex = pos;
        } catch (NullPointerException ex) {
            Log.e(TAG, "NullPointerException", ex);
            Log.e(TAG, "Error UpdateCurrentItemClick null: currentItemIndex-" + currentItemIndex);
        } catch (IndexOutOfBoundsException ex) {
            Log.e(TAG, "IndexOutOfBoundsException", ex);
        }
    }

    @Override
    public void onClick(View v, int pos, Object object) {
        if (homeDiscoveryDetailFragment != null) {
            if (mMusicBusiness.isExistListenMusic()) {
                checkMusicTogether(v, pos, object);
            } else {
                openDetailDiscovery(v, pos, object);
            }
        }
    }

    private void checkMusicTogether(final View v, final int pos, final Object object) {
        ContentDiscovery contentDiscovery = listData.get(pos);
        String currentNumberInRoom = mMusicBusiness.getCurrentNumberFriend();
        MessageBusiness mMessageBusiness = mApp.getMessageBusiness();
        ClickListener.IconListener clickListener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                mMusicBusiness.closeMusic();
                mApp.getPlayMusicController().pauseMusic();
                openDetailDiscovery(v, pos, object);
            }
        };
        if (!TextUtils.isEmpty(currentNumberInRoom)) {
            String currentName = mMessageBusiness.getFriendName(currentNumberInRoom);
            PopupHelper.getInstance().showDialogConfirm(mActivity,
                    "", String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                            currentName),
                    mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                    clickListener, contentDiscovery, Constants.MENU.POPUP_EXIT);
        } else if (mMusicBusiness.isWaitingStrangerMusic()) {// cung nghe voi nguoi la
            PopupHelper.getInstance().showDialogConfirm(mActivity,
                    "", String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                            mRes.getString(R.string.stranger)),
                    mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                    clickListener, contentDiscovery, Constants.MENU.POPUP_EXIT);
        } else {
            mMusicBusiness.closeMusic();
            mApp.getPlayMusicController().pauseMusic();
            openDetailDiscovery(v, pos, object);
        }
    }

    public void openDetailDiscovery(View v, int pos, Object object) {
        currentItemIndex = pos;
        v.setVisibility(View.GONE);
        homeDiscoveryDetailFragment.setItemViewClick(v);
        homeDiscoveryDetailFragment.setCurrentPosition(pos);
        homeDiscoveryDetailFragment.setParentFragment(this);
        showViewDetail();
        ContentDiscovery contentDiscovery = listData.get(pos);
        if (contentDiscovery != null) {
            String name = contentDiscovery.getTitle();
            Log.i(TAG, "contentDiscovery: " + contentDiscovery.toString());
            mActivity.trackingEvent(gaCategory, R.string.ga_action_open_video_discover, name);
        }
    }

    public boolean isShowDetail() {
        return isShowDetail;
    }

    public void hideViewDetail() {
        Log.i(TAG, "hideViewDetail");
        isShowDetail = false;
//        mActivity.enableSwipe();
        //mActivity.showStatusBar();
        mActivity.showOrHideStatusBar(true);
        if (homeDiscoveryDetailFragment != null) {
            homeDiscoveryDetailFragment.clear();
        }
    }

    public void showViewDetail() {
        Log.i(TAG, "showViewDetail");
        isShowDetail = true;
//        mActivity.disableSwipe();
        //mActivity.hideStatusBar();
        mActivity.showOrHideStatusBar(false);
    }

    @Override
    public void onLongClick(View v, int pos, Object object) {

    }


}

package com.metfone.selfcare.fragment.message;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.PreviewImageActivity;
import com.metfone.selfcare.adapter.PreviewImageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by thanhnt72 on 12/27/2017.
 */

public class PreviewImageThreadFragment extends Fragment implements View.OnClickListener, ClickListener.IconListener {

    private static final String TAG = PreviewImageThreadFragment.class.getSimpleName();
    private ApplicationController mApp;
    private PreviewImageActivity mActivity;
    private PreviewImageAdapter mPreviewImageAdapter;
    private int numberOfImages;
    private int threadId;
    private String oldCurrentImagePath;
    private String linkImage;
    private ThreadMessage mThreadMessage;
    private ViewPager mViewPager;
    private View mViewActionBar;
    private TextView mTvwThreadName;
    private TextView mTvwThreadCounter;
    private View mBackBtn, mBtnDownload;
    private View btnShare;

    private boolean isHideStatus = false;
    private Animation animFadeIn, animFadeOut;
    private MessageBusiness mMessageBusiness;
    private ArrayList<String> mListImage;
    private ArrayList<ReengMessage> mListMessageImage;

    public PreviewImageThreadFragment() {
    }

    public static PreviewImageThreadFragment newInstance(int threadId, String currentImage) {
        PreviewImageThreadFragment fragment = new PreviewImageThreadFragment();
        Bundle args = new Bundle();
        args.putInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, threadId);
        args.putString(PreviewImageActivity.PARAM_CURRENT_IMAGE, currentImage);
        fragment.setArguments(args);
        return fragment;
    }

    public static PreviewImageThreadFragment newInstance(int threadId, String currentImage, String linkImage) {
        PreviewImageThreadFragment fragment = new PreviewImageThreadFragment();
        Bundle args = new Bundle();
        args.putInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, threadId);
        args.putString(PreviewImageActivity.PARAM_CURRENT_IMAGE, currentImage);
        args.putString(PreviewImageActivity.PARAM_LINK_IMAGE, linkImage);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseSlidingFragmentActivity) {
            mActivity = (PreviewImageActivity) context;
            mApp = (ApplicationController) mActivity.getApplication();
            mMessageBusiness = mApp.getMessageBusiness();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_preview_image, container, false);
        mActivity.setWhiteStatusBar();
        setComponentViews(rootView);
        initData(savedInstanceState);
        setListener();
        return rootView;
    }

    private void setComponentViews(View rootView) {
        mViewActionBar = rootView.findViewById(R.id.ab_preview_image);
        mTvwThreadName = rootView.findViewById(R.id.ab_preview_image_title);
        mTvwThreadCounter = rootView.findViewById(R.id.ab_preview_image_counter);
        mViewPager = rootView.findViewById(R.id.preview_image_view_pager);
        mBackBtn = rootView.findViewById(R.id.ab_preview_image_back_btn);
        mBtnDownload = rootView.findViewById(R.id.ab_download_btn);
        btnShare = rootView.findViewById(R.id.img_ab_share);
        if (mActivity.isPrivateEncrypt()) {
            mBtnDownload.setVisibility(View.GONE);
            btnShare.setVisibility(View.GONE);
        } else {
            mBtnDownload.setVisibility(View.VISIBLE);
            btnShare.setVisibility(View.VISIBLE);
        }
//        mViewPager.setClipToPadding(false);
       // mViewPager.setPadding(-10, 0, -10, 0);
        mViewPager.setPageMargin(0);
//        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
//            @Override public void transformPage(View page, float position) {
//                if (mViewPager.getCurrentItem() == 0) {
//                    page.setTranslationX(-10);
//                } else if (mViewPager.getCurrentItem() == mPreviewImageAdapter.getCount() - 1) {
//                    page.setTranslationX(10);
//                } else {
//                    page.setTranslationX(0);
//                }
//            }
//        });
        animFadeIn = AnimationUtils.loadAnimation(mApp, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(mApp, R.anim.fade_out);
//        cardView.setUseCompatPadding(true);
//        cardView.setContentPadding(10, 0, 10, 0);
//        cardView.setPreventCornerOverlap(true);
//        cardView.setCardBackgroundColor(Color.WHITE);
//        cardView.setCardElevation(2.1f);
//        cardView.setRadius(0);
//        cardView.setMaxCardElevation(3f);
        View layoutView = rootView.findViewById(R.id.layout_preview_image);
        layoutView.setBackgroundColor(ContextCompat.getColor(mApp, R.color.black));
    }

    private void initData(Bundle savedInstanceState) {
        if (getArguments() != null) {
            threadId = getArguments().getInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID);
            oldCurrentImagePath = getArguments().getString(PreviewImageActivity.PARAM_CURRENT_IMAGE);
            linkImage = getArguments().getString(PreviewImageActivity.PARAM_LINK_IMAGE);
        } else if (savedInstanceState != null) {
            threadId = savedInstanceState.getInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID);
            oldCurrentImagePath = savedInstanceState.getString(PreviewImageActivity.PARAM_CURRENT_IMAGE);
            linkImage = savedInstanceState.getString(PreviewImageActivity.PARAM_LINK_IMAGE);
        }
        mThreadMessage = mMessageBusiness.findThreadByThreadId(threadId);
        if (mThreadMessage == null) {
            mActivity.finish();
            return;
        }
        // load data
        mListMessageImage = mMessageBusiness.getImageMessageOfThread(String.valueOf(threadId), true);
        mListImage = new ArrayList<>();
        for (ReengMessage message : mListMessageImage) {
            if (new File(message.getFilePath()).exists()) {
                mListImage.add(message.getFilePath());
            } else if (!TextUtils.isEmpty(message.getDirectLinkMedia())){
                mListImage.add(UrlConfigHelper.getInstance(mApp).getDomainImage() + message.getDirectLinkMedia());
            }
        }
        // room chat thi add them image tren mem
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && mThreadMessage
                .getAllMessages() != null) {
            for (ReengMessage mes : mThreadMessage.getAllMessages()) {
                if (mes.getMessageType() == ReengMessageConstant.MessageType.image &&
                        mes.getFilePath() != null && !mListImage.contains(mes.getFilePath())) {
                    if (new File(mes.getFilePath()).exists()) {
                        mListImage.add(0, mes.getFilePath());
                    } else if (!TextUtils.isEmpty(mes.getDirectLinkMedia())){
                        mListImage.add(0, UrlConfigHelper.getInstance(mApp).getDomainImage() + mes.getDirectLinkMedia());
                    }
                }
            }
        }
        Collections.reverse(mListImage);
        int currentItem = 0;
        boolean needLoadLink;
        numberOfImages = mListImage.size();
        if (oldCurrentImagePath != null) {
            needLoadLink = !new File(oldCurrentImagePath).exists();
            if (mListImage.isEmpty()) {
                mListImage.add(oldCurrentImagePath);
                numberOfImages = mListImage.size();
            }
            //
            for (int i = 0; i < numberOfImages; i++) {
                if (needLoadLink && linkImage != null && linkImage.equals(mListImage.get(i))) {
                    currentItem = i;
                    break;
                }
                if (oldCurrentImagePath.equals(mListImage.get(i))) {
                    currentItem = i;
                    break;
                }
            }
        }
        mTvwThreadName.setText(getActivity().getResources().getString(R.string.thread_shared_image));
        drawDetail(currentItem);
    }

    private void drawCounter(int position) {
        String counter = (position + 1) + "/" + numberOfImages + " " + getResources().getString(R.string.message_image);
        mTvwThreadCounter.setText(counter);
    }

    private void drawDetail(int currentItem) {
        if (mListImage == null) mListImage = new ArrayList<>();
        numberOfImages = mListImage.size();
        if (numberOfImages <= 0) {
            mActivity.finish();
        } else {
            if (currentItem < 0) currentItem = 0;
            drawCounter(currentItem);
            if (mPreviewImageAdapter == null) {
                mPreviewImageAdapter = new PreviewImageAdapter(mApp, mListImage);
                mPreviewImageAdapter.setLoadLocal(true);
                mViewPager.setAdapter(mPreviewImageAdapter);
                mViewPager.setCurrentItem(currentItem);
            } else {
                mPreviewImageAdapter.setListImage(mListImage);
                mPreviewImageAdapter.notifyDataSetChanged();
                mViewPager.setCurrentItem(currentItem);
            }
            mPreviewImageAdapter.setImageClickListener(new PreviewImageAdapter.ImageClickListener() {
                @Override
                public void onClickImage() {
                    if (isHideStatus) {
                        //showStatusBarPreviewImage();
                    } else {
                        //hideStatusBar(true);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (mActivity == null || mActivity.isFinishing()) return;
        switch (v.getId()) {
            case R.id.ab_preview_image_back_btn:
                mActivity.onBackPressed();
                break;

            case R.id.ab_download_btn: {
                ReengMessage message = getCurrentMessageImage();
                if (message != null) FileHelper.saveImageToGallery(mActivity, mApp, message);
                break;
            }

            case R.id.img_ab_share: {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    mActivity.showDialogLogin();
                } else {
                    ReengMessage message = getCurrentMessageImage();
                    if (message != null) {
                        message.setForwardingMessage(true);
                        ReengMessage msgForward = (ReengMessage) message;
                        msgForward.setForwardingMessage(true);
                        ShareContentBusiness business = new ShareContentBusiness(mActivity, msgForward);
                        business.setTypeSharing(ShareContentBusiness.TYPE_FORWARD_MESSAGE);
                        business.showPopupForwardMessage();
//                        ShareContentBusiness business = new ShareContentBusiness(mActivity, message);
//                        business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_FROM_IMAGE_PREVIEW);
//                        business.setTitleDialogChooseContact(mActivity.getString(R.string.share));
//                        business.showPopupShareContent();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        int i = mViewPager.getCurrentItem();
        String path = mListImage.get(i);
        Log.d(TAG, "onIconClickListener-> mViewPager.getCurrentItem: " + i + " path: " + path);
        switch (menuId) {
            case Constants.MENU.MENU_FORWARD:
                forwardImage();
                break;
            case Constants.MENU.MENU_PREVIEW_SHARE:
                shareImage(path);
                break;
            case Constants.MENU.MENU_PREVIEW_AVATAR:
                mActivity.cropAvatarImage(path);
                break;
            case Constants.MENU.MENU_PREVIEW_BACKGROUND:
                mActivity.cropBackgroundImage(path, threadId);
                break;
            case Constants.MENU.MENU_PREVIEW_DELETE:
                deleteReengMessage();
                break;
            case Constants.MENU.POS_ON_SOCIAL:
                Log.i(TAG, "onIconClickListener PostSocial: ");

                break;
            default:
                break;
        }
    }

    private void forwardImage() {
        ReengMessage message = getCurrentMessageImage();
        if (message != null) {
            forwardContentIntent(message);
        }
    }

    private ReengMessage getCurrentMessageImage() {
        String imagePath = mListImage.get(mViewPager.getCurrentItem());
        Log.d(TAG, "getCurrentMessageImage: " + imagePath);
        if (TextUtils.isEmpty(imagePath) || mListMessageImage.isEmpty()) return null;
        for (ReengMessage message : mListMessageImage) {
            if (message.getFilePath().equals(imagePath)) {
                return message;
            }
            if (!TextUtils.isEmpty(message.getDirectLinkMedia()) &&
                    (UrlConfigHelper.getInstance(mApp).getDomainImage() + message.getDirectLinkMedia()).equals(imagePath)) {
                return message;
            }
        }
        return null;
    }

    private void shareImage(String filePath) {
        Uri imageUri = FileHelper.fromFile(mApp, new File(filePath));
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        // startActivity(Intent.createChooser(shareIntent, ""));
        startActivity(shareIntent);
    }

    private void forwardContentIntent(ReengMessage reengMessage) {
        Intent i = new Intent(mApp, ChooseContactActivity.class);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadMessage.getThreadType());
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, reengMessage);
        mActivity.startActivityForResult(i, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT, true);
    }

    private void deleteReengMessage() {
        ReengMessage message = getCurrentMessageImage();
        if (message != null) {
            String imagePath = message.getFilePath();
            for (String path : mListImage) {
                if (path.equals(imagePath)) {
                    mListImage.remove(path);
                    break;
                }
            }
            // xoa message tren mem
            if (mThreadMessage.getAllMessages() != null) {
                for (ReengMessage mes : mThreadMessage.getAllMessages()) {
                    if (mes.getMessageType() == ReengMessageConstant.MessageType.image &&
                            mes.getFilePath() != null && mes.getFilePath().equals(imagePath)) {
                        mThreadMessage.getAllMessages().remove(mes);
                        break;
                    }
                }
            }
            mThreadMessage.getAllMessages().remove(message);
            mListMessageImage.remove(message);
            Log.d(TAG, "mListMessageImage.size(): " + mListMessageImage.size());
            mMessageBusiness.deleteAMessage(mThreadMessage, message);
            drawDetail(mViewPager.getCurrentItem() - 1);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void hideStatusBar(boolean animation) {
        Log.i(TAG, "hide");
        isHideStatus = true;
        /*if (Build.VERSION.SDK_INT < 16) {
            mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = mActivity.getWindow().getDecorView();
            // hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
        if (animation) {
            mViewActionBar.clearAnimation();
            mViewActionBar.startAnimation(animFadeOut);
        } else {
            mViewActionBar.setVisibility(View.GONE);
        }
    }

    public void showStatusBarPreviewImage() {
        Log.i(TAG, "show");
        isHideStatus = false;
        /*if (Build.VERSION.SDK_INT < 16) {
            mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = mActivity.getWindow().getDecorView();
            // show the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
        mViewActionBar.clearAnimation();
        mViewActionBar.startAnimation(animFadeIn);
        /*if (mHandler == null) {
            mHandler = new Handler();
        } else {
            mHandler.removeCallbacks(runnable);
        }
        mHandler.postDelayed(runnable, TIME_DELAY_SHOW);*/
    }

    private void setListener() {
        mBackBtn.setOnClickListener(this);
        mBtnDownload.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                drawCounter(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        setAnimationListener();
    }

    private void setAnimationListener() {
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                mViewActionBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewActionBar.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }
}

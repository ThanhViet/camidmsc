package com.metfone.selfcare.fragment.avno;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.AgencyTransferMoneyActivity;
import com.metfone.selfcare.adapter.avno.CallOutGlobalAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.DeeplinkItem;
import com.metfone.selfcare.database.model.avno.FakeMOItem;
import com.metfone.selfcare.database.model.avno.InfoAVNO;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ChargeMoneyAVNOListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 3/9/2018.
 */

public class CalloutGlobalFragment extends Fragment implements AVNOHelper.GetInfoAVNOListener, AVNOHelper.PackageListener {

    private static final String TAG = CalloutGlobalFragment.class.getSimpleName();
    @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tvAvatarName)
    TextView tvAvatarName;
    @BindView(R.id.frameAvatar)
    FrameLayout frameAvatar;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvNumberAVNO)
    TextView tvNumberAVNO;
    @BindView(R.id.llInfoUser)
    LinearLayout llInfoUser;
    @BindView(R.id.tvActive)
    RoundTextView tvActive;
    @BindView(R.id.tvAccountBalance)
    TextView tvAccountBalance;
    @BindView(R.id.tvTopUp)
    RoundTextView tvTopUp;
    @BindView(R.id.tvTransferMoney)
    RoundTextView tvTransferMoney;
    @BindView(R.id.rvInfoPackage)
    RecyclerView rvInfoPackage;
    @BindView(R.id.view_parent)
    NestedScrollView viewParent;
    Unbinder unbinder;
    @BindView(R.id.llInfoBalance)
    LinearLayout llInfoBalance;
    @BindView(R.id.ivBgCallOut)
    AspectImageView ivBgCallOut;
    @BindView(R.id.ivBgAVNO)
    AspectImageView ivBgAVNO;
    @BindView(R.id.rlBalanceTopUp)
    RelativeLayout rlBalanceTopUp;
    @BindView(R.id.divider_center)
    View dividerCenter;

    private AVNOActivity mActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private CallOutGlobalAdapter adapter;
    private ArrayList<PackageAVNO> listPackage = new ArrayList<>();


    public static CalloutGlobalFragment newInstance() {
        CalloutGlobalFragment fragment = new CalloutGlobalFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (AVNOActivity) activity;
        mApplication = (ApplicationController) mActivity.getApplicationContext();
        mRes = mApplication.getResources();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_avno_manager, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        rvInfoPackage.setLayoutManager(
                new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        adapter = new CallOutGlobalAdapter(mActivity, listPackage, this);
        rvInfoPackage.setAdapter(adapter);
        getDataAVNO();
        rvInfoPackage.setNestedScrollingEnabled(false);
//        viewParent.setNestedScrollingEnabled(true);
        viewParent.setVisibility(View.INVISIBLE);
        setToolbar(rootView);
        return rootView;
    }

    @OnClick({R.id.tvActive, R.id.tvTopUp, R.id.tvTransferMoney})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvActive:
                AVNOHelper.getInstance(mApplication).gotoWebViewCharge(mActivity);
                break;
            case R.id.tvTopUp:
                mActivity.showDialogChargeMoney(new ChargeMoneyAVNOListener() {
                    @Override
                    public void onChargeMoneySuccess() {
                        getDataAVNO();
                    }

                    @Override
                    public void onChargeError(String msg) {
                        //ko gọi show toast vì bên trong đã show rồi
                    }
                });
                break;
            case R.id.tvTransferMoney:
                AgencyTransferMoneyActivity.startActivity(mActivity);
                break;
        }
    }

    private void setToolbar(View rootView) {
        ImageView mBtnBack = rootView.findViewById(R.id.ab_back_btn);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });

        EllipsisTextView mTvwTitle = rootView.findViewById(R.id.ab_title);
        /*if (type == AVNOHelper.TypeAvno.AVNO.getValue())
            mTvwTitle.setText(mRes.getString(R.string.avno_manager_title));
        else*/
        mTvwTitle.setText(mRes.getString(R.string.title_mocha_callout));
        ImageView mImgMore = rootView.findViewById(R.id.ab_more_btn);
        mImgMore.setVisibility(View.GONE);
    }

    public void getDataAVNO() {
        Log.i(TAG, "getDataAVNO");
        mActivity.showLoadingDialog("", R.string.loading);
        AVNOHelper.getInstance(mApplication).getInfoAVNOV4(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAVNOManagerMessage(final AVNOActivity.AVNOManagerMessage event) {
        Log.i(TAG, "onAVNOManagerMessage");
        getDataAVNO();

//        EventBus.getDefault().removeStickyEvent(event);
    }

    @Override
    public void onGetInfoAVNOSuccess(InfoAVNO infoAVNO) {
        mActivity.hideLoadingDialog();
        if (infoAVNO == null) return;
        viewParent.setVisibility(View.VISIBLE);
//        viewRetryLoading.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(infoAVNO.getAvnoNumber())) {
            tvNumberAVNO.setText(infoAVNO.getAvnoNumber());
        } else {
            tvNumberAVNO.setText(mApplication.getReengAccountBusiness().getJidNumber());
        }
        tvName.setText(mApplication.getReengAccountBusiness().getUserName());

        if (TextUtils.isEmpty(infoAVNO.getBalanceAccount()) && infoAVNO.getTopUp() != 1) {
            rlBalanceTopUp.setVisibility(View.GONE);
            ivBgAVNO.setVisibility(View.GONE);
            ivBgCallOut.setVisibility(View.VISIBLE);
            dividerCenter.setVisibility(View.GONE);
        } else {
            rlBalanceTopUp.setVisibility(View.VISIBLE);
            ivBgAVNO.setVisibility(View.VISIBLE);
            ivBgCallOut.setVisibility(View.GONE);
            dividerCenter.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(infoAVNO.getBalanceAccount())) {
                llInfoBalance.setVisibility(View.VISIBLE);
                tvAccountBalance.setText(infoAVNO.getBalanceAccount());
            } else {
                llInfoBalance.setVisibility(View.INVISIBLE);
            }

            if (infoAVNO.getTransferMoney() == 1) {
                tvTopUp.setVisibility(View.GONE);
                tvTransferMoney.setVisibility(View.VISIBLE);
            } else {
                tvTransferMoney.setVisibility(View.GONE);
                if (infoAVNO.getTopUp() == 1) {
                    tvTopUp.setText(mRes.getString(R.string.avno_title_button_charger));
                    tvTopUp.setVisibility(View.VISIBLE);
                } else {
                    tvTopUp.setVisibility(View.INVISIBLE);
                }
            }
        }

        mApplication.getAvatarBusiness().setMyAvatar(ivAvatar,
                tvAvatarName, null,
                mApplication.getReengAccountBusiness().getCurrentAccount(), null);
        if (infoAVNO.getActiveState() == 0) {
            tvActive.setVisibility(View.VISIBLE);
        } else {
            tvActive.setVisibility(View.GONE);
        }

        ArrayList<PackageAVNO> listActive = infoAVNO.getListPackageActive();
//        ArrayList<PackageAVNO> listAvailable = infoAVNO.getListPackageAvailable();
        listPackage = new ArrayList<>(listActive);
//        listPackage.addAll(listAvailable);
        adapter.setListItem(listPackage);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onGetInfoAVNOError(int code, String msg) {
        mActivity.hideLoadingDialog();
        mActivity.showToast(msg);
    }

    /*@Override
    public void onClickActionPackage(final PackageAVNO packageAVNO) {
        final boolean isRegister = packageAVNO.getStatus() == PackageAVNO.PACKAGE_AVAILABLE;
        DialogConfirm dialogRightClick = new DialogConfirm(mActivity, true);
        dialogRightClick.setTitle(isRegister ? R.string.title_confirm_register_pkg_avno :
                R.string.title_confirm_cancel_pkg_avno);
        dialogRightClick.setMessage(isRegister ?
//                String.format(mRes.getString(R.string.msg_confirm_register_pkg_avno), packageAVNO.getTitle()) :
                packageAVNO.getAlert() :
                String.format(mRes.getString(R.string.msg_confirm_cancel_pkg_avno), packageAVNO.getTitle()));
        dialogRightClick.setPositiveLabel(mRes.getString(R.string.ok));
        dialogRightClick.setNegativeLabel(mRes.getString(R.string.cancel));
        dialogRightClick.setPositiveListener(new PositiveListener<Object>() {

            @Override
            public void onPositive(Object result) {
                mActivity.showLoadingDialog("", R.string.loading);
                AVNOHelper.getInstance(mApplication).actionPackageAVNOV2(isRegister, packageAVNO.getId(), type,
                        new AVNOHelper.ActionPackageAVNOListener() {

                            @Override
                            public void onActionPackageSuccess(String msg) {
                                mActivity.hideLoadingDialog();
                                mActivity.showToast(msg);
                                getDataAVNO();
                            }

                            @Override
                            public void onActionPackageError(int code, String msg) {
                                mActivity.hideLoadingDialog();
                                mActivity.showToast(msg);
                            }
                        });
            }
        });
        dialogRightClick.show();
    }*/

    /*@Override
    public void onClickRegisterFreeNumber(PackageAVNO packageAVNO) {

    }

    @Override
    public void onClickContentPackage(PackageAVNO packageAVNO) {

    }*/

    @Override
    public void onClickDeeplink(DeeplinkItem deeplinkItem) {
        DeepLinkHelper.getInstance().openSchemaLink(mActivity, deeplinkItem.getUrl());
    }

    @Override
    public void onClickFakeMO(FakeMOItem fakeMOItem) {
        String cmd = fakeMOItem.getCmd();
        String content = fakeMOItem.getContent();
        ReportHelper.showConfirmAndReconfirmSubscription(mApplication, mActivity, content, fakeMOItem.getConfirmFakeMO(), cmd, TAG);
    }
}

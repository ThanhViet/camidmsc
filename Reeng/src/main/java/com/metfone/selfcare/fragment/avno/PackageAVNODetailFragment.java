package com.metfone.selfcare.fragment.avno;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 6/5/2019.
 */

public class PackageAVNODetailFragment extends Fragment {

    private static final String TAG = PackageAVNODetailFragment.class.getSimpleName();
    @BindView(R.id.tvDescPackage)
    TextView tvDescPackage;
    @BindView(R.id.rvNumber)
    RecyclerView rvNumber;
    @BindView(R.id.tvAddNumber)
    RoundTextView tvAddNumber;
    Unbinder unbinder;


    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApp;
    private PackageAVNO packageAVNO;
    private Resources mRes;

    private ArrayList<Object> listPhoneNumbers = new ArrayList<>();
    private HomeContactsAdapter mAdapter;


    public static PackageAVNODetailFragment newInstance(PackageAVNO packageAVNO) {
        PackageAVNODetailFragment fragment = new PackageAVNODetailFragment();
        Bundle args = new Bundle();
        args.putSerializable("package", packageAVNO);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mApp = (ApplicationController) activity.getApplication();
        mRes = mApp.getResources();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_package_avno, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (getArguments() != null) {
            try {
                packageAVNO = (PackageAVNO) getArguments().getSerializable("package");
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        if (packageAVNO == null) {
            activity.showToast(R.string.e601_error_but_undefined);
            activity.onBackPressed();
        }

        drawDetail();
        setToolbar(rootView);
        return rootView;
    }

    private void setToolbar(View rootView) {
        ImageView mBtnBack = rootView.findViewById(R.id.ab_back_btn);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });

        EllipsisTextView mTvwTitle = rootView.findViewById(R.id.ab_title);
        /*if (type == AVNOHelper.TypeAvno.AVNO.getValue())
            mTvwTitle.setText(mRes.getString(R.string.avno_manager_title));
        else*/
        mTvwTitle.setText(mRes.getString(R.string.avno_your_package));
        ImageView mImgMore = rootView.findViewById(R.id.ab_more_btn);
        mImgMore.setVisibility(View.GONE);
    }

    private void drawDetail() {
        ArrayList<String> listNumb = packageAVNO.getListNumber();
        for (String s : listNumb) {
            PhoneNumber phoneNumber = mApp.getContactBusiness().getPhoneNumberFromNumber(s);
            if (phoneNumber == null) {
                phoneNumber = new PhoneNumber(s, s);
                phoneNumber.setContactId("-1");
            }
            listPhoneNumbers.add(phoneNumber);
        }


        tvDescPackage.setText(packageAVNO.getDesc());
        rvNumber.setLayoutManager(new LinearLayoutManager(activity));
        mAdapter = new HomeContactsAdapter(mApp, listPhoneNumbers, true);
        mAdapter.setShowCallAVNO(true);
        mAdapter.setClickListener(new ContactsHolderCallBack() {
            @Override
            public void onAudioCall(Object entry) {
                HomeContactsHelper.getInstance(mApp).handleCallContactsClick(activity, entry);
            }

            @Override
            public void onVideoCall(Object entry) {

            }

            @Override
            public void onSectionClick(Object entry) {

            }

            @Override
            public void onItemClick(Object entry) {

            }

            @Override
            public void onAvatarClick(Object entry) {

            }

            @Override
            public void onItemLongClick(Object entry) {

            }

            @Override
            public void onActionLabel(Object entry) {

            }

            @Override
            public void onSocialCancel(Object entry) {

            }
        });
        rvNumber.setAdapter(mAdapter);
        rvNumber.setNestedScrollingEnabled(false);

        if (packageAVNO.getDeeplinkItems() != null
                && packageAVNO.getDeeplinkItems().size() > 0
                && !TextUtils.isEmpty(packageAVNO.getDeeplinkItems().get(0).getNextLabel())) {

            tvAddNumber.setVisibility(View.VISIBLE);
            tvAddNumber.setText(packageAVNO.getDeeplinkItems().get(0).getNextLabel());
        } else
            tvAddNumber.setVisibility(View.GONE);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.tvAddNumber)
    public void onViewClicked() {
        if (packageAVNO.getDeeplinkItems() != null
                && packageAVNO.getDeeplinkItems().size() > 0
                && !TextUtils.isEmpty(packageAVNO.getDeeplinkItems().get(0).getUrl()))
            DeepLinkHelper.getInstance().openSchemaLink(activity, packageAVNO.getDeeplinkItems().get(0).getUrl());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddNumberMessage(final AddNumberMessage event) {
        Log.i(TAG, "onAddNumberMessage: " + event.getNumber());
        ArrayList<String> listNumb = packageAVNO.getListNumber();
        listNumb.add(event.getNumber());
        listPhoneNumbers = new ArrayList<>();
        for (String s : listNumb) {
            PhoneNumber phoneNumber = mApp.getContactBusiness().getPhoneNumberFromNumber(s);
            if (phoneNumber == null) {
                phoneNumber = new PhoneNumber(s, s);
            }
            listPhoneNumbers.add(phoneNumber);
        }
        mAdapter.setData(listPhoneNumbers);
        mAdapter.notifyDataSetChanged();
    }

    public static class AddNumberMessage {
        String number;

        public AddNumberMessage(String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }
    }
}

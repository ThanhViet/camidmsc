package com.metfone.selfcare.fragment.avno;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.AgencyTransferMoneyActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.avno.AVNOAdapter;
import com.metfone.selfcare.adapter.avno.AVNOGiftAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.InfoAVNO;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.listeners.ChargeMoneyAVNOListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 6/5/2019.
 */

public class AVNOFragment extends Fragment implements AVNOHelper.PackageAVNOListener, AVNOHelper.GetInfoAVNOListener {

    private static final String TAG = AVNOFragment.class.getSimpleName();
    @BindView(R.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R.id.tvAvatarName)
    TextView tvAvatarName;
    @BindView(R.id.frameAvatar)
    FrameLayout frameAvatar;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvNumberAVNO)
    TextView tvNumberAVNO;
    @BindView(R.id.llInfoUser)
    LinearLayout llInfoUser;
    @BindView(R.id.divider_center)
    View dividerCenter;
    @BindView(R.id.tvTitleBalance)
    TextView tvTitleBalance;
    @BindView(R.id.tvAccountBalance)
    TextView tvAccountBalance;
    @BindView(R.id.tvTopUp)
    RoundTextView tvTopUp;
    @BindView(R.id.tvTransferMoney)
    RoundTextView tvTransferMoney;
    @BindView(R.id.llInfoBalance)
    RelativeLayout llInfoBalance;
    @BindView(R.id.rlBalanceTopUp)
    LinearLayout rlBalanceTopUp;
    @BindView(R.id.tvTitleGift)
    TextView tvTitleGift;
    @BindView(R.id.ivHelpGift)
    ImageView ivHelpGift;
    @BindView(R.id.rvGift)
    RecyclerView rvGift;
    @BindView(R.id.llGift)
    LinearLayout llGift;
    @BindView(R.id.ivHelpListPackage)
    ImageView ivHelpListPackage;
    @BindView(R.id.rvPackageHot)
    RecyclerView rvPackageHot;
    @BindView(R.id.rvPackage)
    RecyclerView rvPackage;
    @BindView(R.id.llPackage)
    LinearLayout llPackage;
    @BindView(R.id.view_parent)
    NestedScrollView viewParent;
    Unbinder unbinder;
    @BindView(R.id.tvTitleListPackage)
    TextView tvTitleListPackage;
    @BindView(R.id.tvTitleIntro)
    TextView tvTitleIntro;
    @BindView(R.id.tvDescIntro)
    TextView tvDescIntro;
    @BindView(R.id.tvView)
    RoundTextView tvView;
    @BindView(R.id.llIntro)
    LinearLayout llIntro;

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApp;
    private Resources mRes;

    private AVNOAdapter adapterPackage, adapterPackageHot;
    private AVNOGiftAdapter adapterGift;
    private ArrayList<PackageAVNO> listPackage = new ArrayList<>();
    private ArrayList<PackageAVNO> listPackageHot = new ArrayList<>();
    private ArrayList<ItemInfo> listGift = new ArrayList<>();
    private ArrayList<ItemInfo> listInfo;


    public static AVNOFragment newInstance() {
        AVNOFragment fragment = new AVNOFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mApp = (ApplicationController) activity.getApplication();
        mRes = mApp.getResources();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_avno, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        rvGift.setLayoutManager(
                new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        rvPackage.setLayoutManager(
                new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        rvPackageHot.setLayoutManager(
                new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        adapterPackage = new AVNOAdapter(activity, listPackage, this);
        adapterPackageHot = new AVNOAdapter(activity, listPackageHot, this);
        adapterGift = new AVNOGiftAdapter(activity, listGift, this);
        rvPackage.setAdapter(adapterPackage);
        rvPackageHot.setAdapter(adapterPackageHot);
        rvGift.setAdapter(adapterGift);
        getDataAVNO();
        rvGift.setNestedScrollingEnabled(false);
        rvGift.setVisibility(View.GONE);
        rvPackage.setVisibility(View.GONE);
        rvPackageHot.setVisibility(View.GONE);
//        viewParent.setNestedScrollingEnabled(true);
        viewParent.setVisibility(View.INVISIBLE);
        setToolbar(rootView);

        return rootView;
    }

    private void setToolbar(View rootView) {
        ImageView mBtnBack = rootView.findViewById(R.id.ab_back_btn);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });

        EllipsisTextView mTvwTitle = rootView.findViewById(R.id.ab_title);
        /*if (type == AVNOHelper.TypeAvno.AVNO.getValue())
            mTvwTitle.setText(mRes.getString(R.string.avno_manager_title));
        else*/
        mTvwTitle.setText(mRes.getString(R.string.title_mocha_callout));
        ImageView mImgMore = rootView.findViewById(R.id.ab_more_btn);
        mImgMore.setVisibility(View.GONE);
    }

    public void getDataAVNO() {
        Log.i(TAG, "getDataAVNO");
        activity.showLoadingDialog("", R.string.loading);
        AVNOHelper.getInstance(mApp).getInfoAVNOV4(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.tvTopUp, R.id.ivHelpGift, R.id.ivHelpListPackage, R.id.tvView, R.id.tvTransferMoney})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvTopUp:
                activity.showDialogChargeMoney(new ChargeMoneyAVNOListener() {
                    @Override
                    public void onChargeMoneySuccess() {
                        getDataAVNO();
                    }

                    @Override
                    public void onChargeError(String msg) {
                        //ko gọi show toast vì bên trong đã show rồi
                    }
                });
//                AVNOHelper.getInstance(mApp).gotoWebViewCharge(activity);
                break;
            case R.id.ivHelpGift:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://survey?ref=http%3A%2F%2Fmocha.com.vn&backConfirm=0");
                break;
            case R.id.ivHelpListPackage:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://survey?ref=http%3A%2F%2Fmocha.com.vn&backConfirm=0");
                break;
            case R.id.tvView:
                if (listInfo != null)
                    ((AVNOActivity) activity).navigateToPreSelectNumberAVNOFragment(listInfo);
                else {
                    activity.showToast(R.string.e601_error_but_undefined);
                }
                break;
            case R.id.tvTransferMoney:
                AgencyTransferMoneyActivity.startActivity(activity);
                break;
        }
    }

    @Override
    public void onClickAction(PackageAVNO packageAVNO) {
        if (packageAVNO.getInUse() == -1) {
            //TODO xem chi tiet goi cuoc
            ((AVNOActivity) activity).navigateToPackageAVNOFragment(packageAVNO);
        } else {
            //TODO dang ky goi cuoc
            DeepLinkHelper.getInstance().openSchemaLink(activity, packageAVNO.getDeeplinkItems().get(0).getUrl());
        }
    }

    @Override
    public void onClickGift(ItemInfo gift) {
        DeepLinkHelper.getInstance().openSchemaLink(activity, gift.getDeeplink());
    }


    @Override
    public void onGetInfoAVNOSuccess(InfoAVNO infoAVNO) {
        activity.hideLoadingDialog();
        if (infoAVNO == null) return;
        viewParent.setVisibility(View.VISIBLE);
//        viewRetryLoading.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(infoAVNO.getAvnoNumber())) {
            tvNumberAVNO.setText(infoAVNO.getAvnoNumber());
        } else {
            tvNumberAVNO.setText(mApp.getReengAccountBusiness().getJidNumber());
        }
        tvName.setText(mApp.getReengAccountBusiness().getUserName());

        tvAccountBalance.setText(infoAVNO.getBalanceAccount());
        tvTitleListPackage.setText(infoAVNO.getIntroPackage());
        if (infoAVNO.getIntro() != null) {
            llIntro.setVisibility(View.VISIBLE);
            tvTitleIntro.setText(infoAVNO.getIntro().getTitle());
            tvDescIntro.setText(infoAVNO.getIntro().getDesc());
            tvView.setText(infoAVNO.getIntro().getDeeplinkLabel());
        } else
            llIntro.setVisibility(View.GONE);


        mApp.getAvatarBusiness().setMyAvatar(ivAvatar,
                tvAvatarName, null,
                mApp.getReengAccountBusiness().getCurrentAccount(), null);

        ArrayList<PackageAVNO> listPackage = infoAVNO.getListPackageActive();
        ArrayList<PackageAVNO> listPackageHot = infoAVNO.getListPackageAvailable();
        ArrayList<ItemInfo> listGift = infoAVNO.getListGift();

        if ((listPackage == null || listPackage.isEmpty()) && (listPackageHot == null || listPackageHot.isEmpty()))
            llPackage.setVisibility(View.GONE);
        else {
            llPackage.setVisibility(View.VISIBLE);
            if (listPackage != null && !listPackage.isEmpty()) {
                Log.i(TAG, "listPackage: " + listPackage.size());
                rvPackage.setVisibility(View.VISIBLE);
                this.listPackage = listPackage;
                adapterPackage.setListItem(listPackage);
                adapterPackage.notifyDataSetChanged();
            }
            if (listPackageHot != null && !listPackageHot.isEmpty()) {
                Log.i(TAG, "listPackageHot: " + listPackageHot.size());
                rvPackageHot.setVisibility(View.VISIBLE);
                this.listPackageHot = listPackageHot;
                adapterPackageHot.setListItem(listPackageHot);
                adapterPackageHot.notifyDataSetChanged();
            }
        }
        if (listGift != null && !listGift.isEmpty()) {
            Log.i(TAG, "listGift: " + listGift.size());
            llGift.setVisibility(View.VISIBLE);
            rvGift.setVisibility(View.VISIBLE);
            this.listGift = listGift;
            adapterGift.setListItem(listGift);
            adapterGift.notifyDataSetChanged();
        } else
            llGift.setVisibility(View.GONE);
        listInfo = infoAVNO.getAccountInfoList();

        tvTopUp.setVisibility((infoAVNO.getTopUp() == 1) ? View.VISIBLE : View.GONE);
        if (infoAVNO.getTransferMoney() == 1) {
            tvTransferMoney.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetInfoAVNOError(int code, String msg) {
        activity.hideLoadingDialog();
        activity.showToast(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAVNOManagerMessage(final AVNOActivity.AVNOManagerMessage event) {
        Log.i(TAG, "onAVNOManagerMessage");
        getDataAVNO();
    }
}

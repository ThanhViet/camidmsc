package com.metfone.selfcare.fragment.setting;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;

/**
 * Created by nickgun on 8/14/14.
 */
public class HelpFragment extends Fragment {
    private static final String TAG = HelpFragment.class.getSimpleName();
    private int imageRes;
    private ImageView helpImage;

    public static HelpFragment newInstance(int imageRes) {
        HelpFragment f = new HelpFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("imageRes", imageRes);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageRes = getArguments().getInt("imageRes");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        findComponentViews(view);
        setViewListener();
        return view;
    }

    private void findComponentViews(View view) {
        helpImage = (ImageView) view.findViewById(R.id.intro_image);
        helpImage.setImageResource(imageRes);
    }

    private void setViewListener() {
    }
}

package com.metfone.selfcare.fragment.login.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.roundview.RoundTextView;

public class CamIDAccountDialog extends Dialog implements View.OnClickListener {
    public Activity activity;
    RoundTextView btnContinue;

    public CamIDAccountDialog(@NonNull Activity activity) {
        super(activity);
        this.activity = activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_camid_account);
        btnContinue = findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnContinue:
                SetPasswordAccountDialog setPasswordAccountDialog = new SetPasswordAccountDialog(activity);
                setPasswordAccountDialog.show();
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

}

package com.metfone.selfcare.fragment.avno;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.listeners.SimpleResponseListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 6/5/2019.
 */

public class PreSelectNumberAVNOFragment extends Fragment {

    private static final String TAG = PreSelectNumberAVNOFragment.class.getSimpleName();
    @BindView(R.id.tvTitle1)
    TextView tvTitle1;
    @BindView(R.id.tvDesc1)
    TextView tvDesc1;
    @BindView(R.id.rl1)
    LinearLayout rl1;
    @BindView(R.id.tvTitle2)
    TextView tvTitle2;
    @BindView(R.id.tvDesc2)
    TextView tvDesc2;
    @BindView(R.id.rl2)
    LinearLayout rl2;
    @BindView(R.id.tvTitle3)
    TextView tvTitle3;
    @BindView(R.id.tvDesc3)
    TextView tvDesc3;
    @BindView(R.id.rl3)
    LinearLayout rl3;
    Unbinder unbinder;
    @BindView(R.id.tvView)
    RoundTextView tvView;

    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApp;
    private Resources mRes;

    private ArrayList<ItemInfo> listInfo;

    public static PreSelectNumberAVNOFragment newInstance(ArrayList<ItemInfo> itemInfos) {
        PreSelectNumberAVNOFragment fragment = new PreSelectNumberAVNOFragment();
        Bundle args = new Bundle();
        args.putSerializable("info", itemInfos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mApp = (ApplicationController) activity.getApplication();
        mRes = mApp.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pre_select_number_avno, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (getArguments() != null) {
            try {
                listInfo = (ArrayList<ItemInfo>) getArguments().getSerializable("info");
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        if (listInfo == null) {
            activity.showToast(R.string.e601_error_but_undefined);
            activity.onBackPressed();
        }
//        drawDetail();
        setToolbar(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        drawDetail();
    }

    private void setToolbar(View rootView) {
        ImageView mBtnBack = rootView.findViewById(R.id.ab_back_btn);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });

        EllipsisTextView mTvwTitle = rootView.findViewById(R.id.ab_title);
        /*if (type == AVNOHelper.TypeAvno.AVNO.getValue())
            mTvwTitle.setText(mRes.getString(R.string.avno_manager_title));
        else*/
        mTvwTitle.setText(mRes.getString(R.string.avno_number_mocha));
        ImageView mImgMore = rootView.findViewById(R.id.ab_more_btn);
        mImgMore.setVisibility(View.GONE);
    }

    private void drawDetail() {
        if (listInfo.size() >= 1) {
            rl1.setVisibility(View.VISIBLE);
            ItemInfo info = listInfo.get(0);
            tvTitle1.setText(info.getTitle());
            tvDesc1.setText(info.getDesc());
        }
        if (listInfo.size() >= 2) {
            rl2.setVisibility(View.VISIBLE);
            ItemInfo info = listInfo.get(1);
            tvTitle2.setText(info.getTitle());
            tvDesc2.setText(info.getDesc());
        }
        if (listInfo.size() >= 3) {
            rl3.setVisibility(View.VISIBLE);
            ItemInfo info = listInfo.get(2);
            tvTitle3.setText(info.getTitle());
            tvDesc3.setText(info.getDesc());
        }
        if (!TextUtils.isEmpty(mApp.getReengAccountBusiness().getAVNONumber())) {
            tvView.setText(mRes.getString(R.string.avno_delete_number));
        } else
            tvView.setText(mRes.getString(R.string.avno_buy_number));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.tvView)
    public void onViewClicked() {

        if (!TextUtils.isEmpty(mApp.getReengAccountBusiness().getAVNONumber())) {
            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setMessage(String.format(mRes.getString(R.string.avno_msg_delete_number), mApp.getReengAccountBusiness().getAVNONumber()));
            dialogConfirm.setPositiveLabel(mRes.getString(R.string.ok));
            dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    activity.showLoadingDialog("", R.string.loading);
                    AVNOHelper.getInstance(mApp).deleteAVNONumber(mApp.getReengAccountBusiness().getAVNONumber(), new SimpleResponseListener() {
                        @Override
                        public void onSuccess(String msg) {
                            activity.hideLoadingDialog();
                            activity.showToast(msg);
                            ReengAccount reengAccount = mApp.getReengAccountBusiness().getCurrentAccount();
                            reengAccount.setAvnoNumber("");
                            mApp.getReengAccountBusiness().updateReengAccount(reengAccount);
                            activity.finish();
                        }

                        @Override
                        public void onError(int code, String msg) {
                            activity.hideLoadingDialog();
                            activity.showToast(msg);
                        }
                    });
                }
            });
            dialogConfirm.show();
        } else {
            Intent intent = new Intent(activity, AVNOActivity.class);
            intent.putExtra(AVNOActivity.FRAGMENT_DATA, AVNOActivity.FRAGMENT_LIST_NUMBER);
            activity.startActivity(intent);
        }
//            ((AVNOActivity) activity).navigateToListNumberAvailable(true);
    }

}

package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Song;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.guestbook.DialogSelectMusic;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookUpdateBookInfoFragment extends Fragment implements
        View.OnClickListener,
        DialogSelectMusic.OnSelectMusic {
    private static final String TAG = GuestBookMainFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private GuestBookHelper mGuestBookHelper;
    private Handler mHandler = new Handler();
    private String bookId;
    private Book currentBook;
    private Song currentSongSelect;

    private TextView mAbTitle;
    private EditText mEdtName;
    private EditText mEdtClass;
    private TextView mTvwMusicName;
    private View mViewChangeMusic;
    private View mViewChangePrivate;
    private ImageView mImgPrivate;
    private Button mBtnUpdate;

    public GuestBookUpdateBookInfoFragment() {

    }

    public static GuestBookUpdateBookInfoFragment newInstance() {
        GuestBookUpdateBookInfoFragment fragment = new GuestBookUpdateBookInfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_guest_book_update_info, container, false);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        getDataAnDraw(savedInstanceState);
        setViewListener();
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.GUEST_BOOK.BOOK_ID, bookId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.guest_book_update_button:
                handleUpdateInfo();
                break;
            case R.id.guest_book_update_music:
                handlePickMusic();
                break;
            case R.id.guest_book_update_private:
                mImgPrivate.setSelected(!mImgPrivate.isSelected());
                break;
        }
    }

    @Override
    public void onMusicSelected(Song song) {
        Log.d(TAG, "onMusicSelected: " + song.toString());
        currentSongSelect = song;
        mTvwMusicName.setText(currentSongSelect.getName());
    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (TextView) abView.findViewById(R.id.ab_title);
        mAbTitle.setText(mRes.getString(R.string.guest_book_setting_title));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbBack.setOnClickListener(this);
    }

    private void getDataAnDraw(Bundle savedInstanceState) {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_NON);
        if (savedInstanceState != null) {
            bookId = savedInstanceState.getString(Constants.GUEST_BOOK.BOOK_ID);
        }
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        currentBook = mGuestBookHelper.getCurrentBookEditor();
        if (currentBook != null) {
            bookId = currentBook.getId();
            drawDetail();
        } else {
            mParentActivity.showToast(R.string.request_send_error);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mParentActivity.onBackPressed();
                }
            }, 1000);
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mEdtName = (EditText) rootView.findViewById(R.id.guest_book_update_name);
        mEdtClass = (EditText) rootView.findViewById(R.id.guest_book_update_class);
        mTvwMusicName = (TextView) rootView.findViewById(R.id.guest_book_update_music_song);
        mViewChangeMusic = rootView.findViewById(R.id.guest_book_update_music);
        mViewChangePrivate = rootView.findViewById(R.id.guest_book_update_private);
        mImgPrivate = (ImageView) rootView.findViewById(R.id.guest_book_update_private_checkbox);
        mBtnUpdate = (Button) rootView.findViewById(R.id.guest_book_update_button);
    }

    private void setViewListener() {
        mBtnUpdate.setOnClickListener(this);
        mViewChangeMusic.setOnClickListener(this);
        mViewChangePrivate.setOnClickListener(this);
        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (TextUtils.isEmpty(charSequence)) {
                    mBtnUpdate.setEnabled(false);
                } else {
                    mBtnUpdate.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void drawDetail() {
        currentSongSelect = currentBook.getSong();
        if (currentSongSelect != null) {
            mTvwMusicName.setText(currentSongSelect.getName());
        } else {
            mTvwMusicName.setText("");
        }
        mEdtName.setText(currentBook.getBookName());
        mEdtClass.setText(currentBook.getBookClass());
        if (currentBook.getPublicState() == 0) {
            mImgPrivate.setSelected(false);
        } else {
            mImgPrivate.setSelected(true);
        }
    }

    private void handlePickMusic() {
        if (mGuestBookHelper.getMusics().isEmpty()) {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestMusics(new GuestBookHelper.GetDataListener() {
                @Override
                public void onSuccess() {
                    mParentActivity.hideLoadingDialog();
                    DialogSelectMusic selectMusic = DialogSelectMusic.newInstance(mGuestBookHelper.getMusics(),
                            GuestBookUpdateBookInfoFragment.this);
                    selectMusic.show(getFragmentManager(), TAG);
                }

                @Override
                public void onError(int error) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(R.string.request_send_error);
                }
            });
        } else {
            DialogSelectMusic selectMusic = DialogSelectMusic.newInstance(mGuestBookHelper.getMusics(), this);
            selectMusic.show(getFragmentManager(), TAG);
        }
    }

    private void handleUpdateInfo() {
        String bookName = mEdtName.getText().toString().trim();
        String bookClass = mEdtClass.getText().toString().trim();
        int statePublic = mImgPrivate.isSelected() ? 1 : 0;
        if (isInfoChanged(bookName, bookClass, statePublic, currentSongSelect)) {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestSaveBookInfo(currentBook.getId(), currentBook.getStatus(), bookName, bookClass,
                    currentSongSelect, statePublic, new GuestBookHelper.ProcessBookListener() {
                        @Override
                        public void onSuccess(String desc) {
                            mParentActivity.hideLoadingDialog();
                            mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                            mGuestBookHelper.updateCurrentBookInfo(currentBook, false);
                            mParentActivity.onBackPressed();
                        }

                        @Override
                        public void onError(String desc) {
                            mParentActivity.hideLoadingDialog();
                            mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                        }
                    });
        } else {
            Log.d(TAG, "handleUpdateInfo no data changed");
            mParentActivity.onBackPressed();
        }
    }

    private boolean isInfoChanged(String bookName, String bookClass, int statePublic, Song song) {
        if (currentBook == null) return false;
        if (song != null && song.equals(currentBook.getSong()) &&
                statePublic == currentBook.getPublicState() &&
                (TextUtils.isEmpty(bookName) && TextUtils.isEmpty(currentBook.getBookName())) &&
                bookName.equals(currentBook.getBookName()) &&
                (TextUtils.isEmpty(bookClass) && TextUtils.isEmpty(currentBook.getBookClass())) &&
                bookClass.equals(currentBook.getBookClass())) {
            return false;
        }
        return true;
    }
}
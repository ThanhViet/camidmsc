package com.metfone.selfcare.fragment.home.tabmobile;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.SearchUserAdapter;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 6/26/2018.
 */

public class AddFriendFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = AddFriendFragment.class.getSimpleName();
    @BindView(R.id.etSearch)
    ReengSearchView etSearch;
    @BindView(R.id.iv_keyboard)
    ImageView ivKeyboard;
    @BindView(R.id.btnSearch)
    TextView btnSearch;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.llDefault)
    LinearLayout llDefault;
    @BindView(R.id.recycler_view_search)
    RecyclerView recyclerViewSearch;
    @BindView(R.id.empty_progress)
    ProgressLoading mPrbLoading;


    private LinearLayout llInviteFriend;
    private LinearLayout llScanQRCode;
    private LinearLayout llFindAround;

    private ApplicationController mApp;
    private BaseSlidingFragmentActivity mActivity;
    private HomeContactsAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderRecycler;
    private ArrayList<Object> mListData = new ArrayList<>();
    private ArrayList<Object> listRequestTmp = new ArrayList<>();

    private boolean isSuggestLoaded = false, isRequestLoaded = false, isFriendLoaded = false;
    private List<SocialFriendInfo> listSocialRequest;


    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private SearchUserAdapter mAdapterSearchUser;
    private ArrayList<PhoneNumber> mListUser = new ArrayList<>();
    private View mFooterView, mHeaderView;
    private LinearLayout mLoadMoreFooterView;
    private LinearLayoutManager mRecyclerManager;
    private boolean isLoading;
    private boolean listEnd;
    private int page = 1;
    private TextView mTvwSearchResult;


    public static AddFriendFragment newInstance() {
        return new AddFriendFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BaseSlidingFragmentActivity) getActivity();
        mApp = (ApplicationController) mActivity.getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        View rootView = inflater.inflate(R.layout.fragment_add_friend, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mActivity.getToolBarView();
        View mImgBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        TextView mTvTitle = (TextView) abView.findViewById(R.id.ab_title);
        mTvTitle.setText(mActivity.getResources().getString(R.string.title_add_friend));
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        ivKeyboard.setVisibility(View.GONE);

        setAdapter();
        loadDataFromServer(true);
        etSearch.addTextChangedListener(textWatcher);
        etSearch.setHint(R.string.search);
        btnSearch.setVisibility(View.VISIBLE);

        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mHeaderView = inflater.inflate(R.layout.view_header_search, container, false);
        mLoadMoreFooterView = (LinearLayout) mFooterView.findViewById(R.id.layout_loadmore);
        mLoadMoreFooterView.setVisibility(View.GONE);
        mTvwSearchResult = (TextView) mHeaderView.findViewById(R.id.tvw_search_user);
        mTvwSearchResult.setVisibility(View.GONE);
        mAdapterSearchUser = new SearchUserAdapter(mApp, new ArrayList<PhoneNumber>());
        mRecyclerManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        recyclerViewSearch.setLayoutManager(mRecyclerManager);
        recyclerViewSearch.setItemAnimator(new DefaultItemAnimator());
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapterSearchUser);
        recyclerViewSearch.setAdapter(mHeaderAndFooterAdapter);
        mHeaderAndFooterAdapter.addFooterView(mFooterView);
        mHeaderAndFooterAdapter.addHeaderView(mHeaderView);

        View viewHeader = inflater.inflate(R.layout.item_header_add_friend, container, false);
        llInviteFriend = viewHeader.findViewById(R.id.llInviteFriend);
        llScanQRCode = viewHeader.findViewById(R.id.llScanQRCode);
        llFindAround = viewHeader.findViewById(R.id.llFindAround);
        mHeaderRecycler.addHeaderView(viewHeader);
        setViewClickListener();

        return rootView;
    }

    private void setViewClickListener() {
        llInviteFriend.setOnClickListener(this);
        llScanQRCode.setOnClickListener(this);
        llFindAround.setOnClickListener(this);
        mAdapterSearchUser.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                PhoneNumber phoneNumber = (PhoneNumber) object;
                if (phoneNumber == null)
                    return;
                NavigateActivityHelper.navigateToFriendProfile(mApp, mActivity,
                        phoneNumber.getJidNumber(), phoneNumber.getNickName(),
                        phoneNumber.getLastChangeAvatar(), phoneNumber.getStatus(), phoneNumber.getGender());
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        recyclerViewSearch.addOnScrollListener(mApp.getPauseOnScrollRecyclerViewListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mListUser == null || mListUser.isEmpty()) {
                    return;
                }
                if (TextUtils.isEmpty(etSearch.getText().toString())) {
                    return;
                }
                if (mRecyclerManager == null) return;
                int totalItemCount = mRecyclerManager.getItemCount();
                int lastInScreen = mRecyclerManager.findLastVisibleItemPosition();
                if ((lastInScreen == (totalItemCount - 3)) && !isLoading && !listEnd) {
                    Log.i(TAG, "needToLoad");
                    onLoadMore();
                }
            }
        }));

        InputMethodUtils.hideKeyboardWhenTouch(recyclerView, mActivity);
        InputMethodUtils.hideKeyboardWhenTouch(recyclerViewSearch, mActivity);
    }

    private void onLoadMore() {
        if (mListUser == null || mListUser.isEmpty()) {
            return;
        }
        isLoading = true;
        mLoadMoreFooterView.setVisibility(View.VISIBLE);
        //TODO load more
        page = page + 1;
        handleSearch(page);
    }

    private void handleSearch(final int page) {
        listEnd = false;
        ContactRequestHelper.getInstance(mApp).searchUserMocha(etSearch.getText().toString(), page, new ContactRequestHelper.onResponseContact() {
            @Override
            public void onResponse(ArrayList<PhoneNumber> responses) {
                isLoading = false;
                mPrbLoading.setVisibility(View.GONE);
                mLoadMoreFooterView.setVisibility(View.GONE);
                if (!responses.isEmpty()) {
                    if (page == 1) {
                        mListUser.clear();
                        mTvwSearchResult.setVisibility(View.VISIBLE);
                        String text = String.format(mActivity.getResources().getString(R.string.search_user_with_name),
                                etSearch.getText().toString().trim());
                        mTvwSearchResult.setText(text);
                    }
                    /*if (mListUser.isEmpty()) {
                        mListUser.addAll(responses);
                    } else {
                        mListUser.addAll(mListUser.size() - 1, responses);
                    }*/

                    mListUser.addAll(responses);
                    setAdapterListUser(mListUser);
                } else {
                    if (page == 1) {
                        mTvwSearchResult.setVisibility(View.VISIBLE);
                        mTvwSearchResult.setText(mActivity.getResources().getString(R.string.search_not_found));
                        mListUser.clear();
                        setAdapterListUser(mListUser);
                    }
                    listEnd = true;
                }
            }

            @Override
            public void onError(int errorCode) {
                Log.e(TAG, "error");
                isLoading = false;
                if (mPrbLoading != null)
                    mPrbLoading.setVisibility(View.GONE);
                if (mLoadMoreFooterView != null)
                    mLoadMoreFooterView.setVisibility(View.GONE);
                if (mActivity != null)
                    mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    private void setAdapterListUser(ArrayList<PhoneNumber> listUser) {
        if (listUser == null) listUser = new ArrayList<>();
        mAdapterSearchUser.setListUser(listUser);
        mAdapterSearchUser.notifyDataSetChanged();
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (TextUtils.isEmpty(s.toString())) {
                llDefault.setVisibility(View.VISIBLE);
                btnSearch.setClickable(false);
                btnSearch.setTextColor(ContextCompat.getColor(mActivity, R.color.color_un_sub));
            } else {
                llDefault.setVisibility(View.GONE);
                btnSearch.setClickable(true);
                btnSearch.setTextColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void setAdapter() {
        mAdapter = new HomeContactsAdapter(mApp, new ArrayList<>());
        mHeaderRecycler = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mHeaderRecycler);

        setItemListViewListener();
    }

    private void setItemListViewListener() {
        mAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                HomeContactsHelper.getInstance(mApp).handleItemContactsClick(mActivity, entry);
            }

            @Override
            public void onAvatarClick(Object entry) {
                super.onAvatarClick(entry);
                HomeContactsHelper.getInstance(mApp).handleItemContactsClick(mActivity, entry);
            }

            @Override
            public void onActionLabel(Object entry) {
                super.onActionLabel(entry);
                if (entry instanceof SocialFriendInfo) {
                    SocialFriendInfo social = (SocialFriendInfo) entry;
                    if (!NetworkHelper.isConnectInternet(mApp)) {
                        mActivity.showToast(R.string.error_internet_disconnect);
                        return;
                    }
                    if (social.getSocialType() == StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED) {
                        processFollow(social, Constants.CONTACT.FOLLOW_STATE_BE_FOLLOWED);
                    }
                }
            }

            @Override
            public void onSocialCancel(Object entry) {
                super.onSocialCancel(entry);
                if (entry instanceof SocialFriendInfo) {
                    socialFriendCancel((SocialFriendInfo) entry);
                }
            }

            @Override
            public void onSectionClick(Object entry) {
                super.onSectionClick(entry);
                if (entry instanceof PhoneNumber) {
                    int sectionType = ((PhoneNumber) entry).getSectionType();
                    if (sectionType == NumberConstant.SECTION_TYPE_SOCIAL_REQUEST) {
                        NavigateActivityHelper.navigateToContactList(mActivity, Constants.CONTACT.FRAG_LIST_SOCIAL_REQUEST);
                    } else {
//                    NavigateActivityHelper.navigateToContactList(mActivity, Constants.CONTACT.FRAG_LIST_);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.btnSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                if (TextUtils.isEmpty(etSearch.getText().toString().trim())) return;
                page = 1;
                handleSearch(page);
                mPrbLoading.setVisibility(View.VISIBLE);
                break;
        }
    }


    private void loadDataFromServer(boolean forceGetRequest) {
        if (forceGetRequest)
            mActivity.showLoadingDialog("", R.string.loading);
        if (!isSuggestLoaded) {
            HomeContactsHelper.getInstance(mApp).getSuggestFriend(new HomeContactsHelper.GetSocialListener<ArrayList<UserInfo>>() {
                @Override
                public void onResponse(ArrayList<UserInfo> suggestFriends) {
                    isSuggestLoaded = true;
                    if (suggestFriends != null && !suggestFriends.isEmpty()) {
                        PhoneNumber sectionRequest = new PhoneNumber();
                        sectionRequest.setContactId(null);

                        sectionRequest.setName(mActivity.getResources().getString(R.string.title_suggest_friend));
                        /*if (suggestFriends.size() > 3)
                            sectionRequest.setSectionType(NumberConstant.SECTION_TYPE_SUGGEST_FRIEND);*/
                        mListData.add(sectionRequest);

                        for (int i = 0; i < suggestFriends.size(); i++) {
                            mListData.add(suggestFriends.get(i));
                            if (i == 9) break;
                        }

                        mAdapter.setData(mListData);
                        mAdapter.notifyDataSetChanged();
                    }

                    mActivity.hideLoadingDialog();
                }

                @Override
                public void onError() {
                    isSuggestLoaded = true;
                    mActivity.hideLoadingDialog();
                }

                @Override
                public void onEmptyList() {

                }
            });
        }

        if (!isRequestLoaded || listSocialRequest == null || forceGetRequest) {
            Log.i(TAG, "forceGetRequest: " + forceGetRequest);
            HomeContactsHelper.getInstance(mApp).getSocialsRequest(new HomeContactsHelper.GetSocialListener<List<SocialFriendInfo>>() {
                @Override
                public void onResponse(List<SocialFriendInfo> result) {
                    mActivity.hideLoadingDialog();

                    isRequestLoaded = true;
                    listSocialRequest = result;
                    listRequestTmp = getListSocialRequest(result);
                    mListData.addAll(0, listRequestTmp);
                    mAdapter.setData(mListData);
                    mAdapter.notifyDataSetChanged();
//                    initAndDrawSocial();
                }

                @Override
                public void onError() {
                    mActivity.hideLoadingDialog();
                    isRequestLoaded = true;
                }

                @Override
                public void onEmptyList() {
                    mActivity.hideLoadingDialog();
                }
            });
        }

    }

    private ArrayList<Object> getListSocialRequest(List<SocialFriendInfo> result) {
        ArrayList<Object> list = new ArrayList<>();
        if (result != null && !result.isEmpty()) {
            PhoneNumber sectionRequest = new PhoneNumber();
            sectionRequest.setContactId(null);
            sectionRequest.setName(mActivity.getResources().getString(R.string.social_tab_be_followed));
            if (result.size() > 3)
                sectionRequest.setSectionType(NumberConstant.SECTION_TYPE_SOCIAL_REQUEST);
            list.add(sectionRequest);
        }

        for (int i = 0; i < result.size(); i++) {
            list.add(result.get(i));
            if (i == 2) break;
        }

        return list;
    }


    private void processFollow(final SocialFriendInfo socialFriendInfo, int state) {
        mActivity.showLoadingDialog(null, R.string.waiting);
        VolleyHelper.getInstance(mApp).cancelPendingRequests(StrangerBusiness.TAG_SOCIAL_REQUEST);
        ContactRequestHelper.getInstance(mApp).requestSocialFriend(socialFriendInfo.getUserFriendJid(),
                socialFriendInfo.getUserName(), Constants.CONTACT.SOCIAL_SOURCE_MOCHA,
                state, socialFriendInfo.getStringRowId(), new ContactRequestHelper.onFollowResponse() {
                    @Override
                    public void onResponse(int status, String rowIdRequest) {
                        mActivity.hideLoadingDialog();
                        mActivity.showToast(R.string.request_success);
                        if (listSocialRequest.size() > 3) {
                            mListData.add(4, listSocialRequest.get(3));
                            mListData.remove(socialFriendInfo);
                        }
                        listSocialRequest.remove(socialFriendInfo);
//                        listSocialRequest = HomeContactsHelper.getInstance(mApp).getNextSocialRequest();
                        socialFriendInfo.setSocialType(StrangerBusiness.TYPE_SOCIAL_FRIEND);
                        mAdapter.notifyDataSetChanged();
//                        initAndDrawSocial();
                    }

                    @Override
                    public void onError(int errorCode) {
                        mActivity.hideLoadingDialog();
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void socialFriendCancel(final SocialFriendInfo socialFriendInfo) {
        mActivity.showLoadingDialog(null, R.string.waiting);
        VolleyHelper.getInstance(mApp).cancelPendingRequests(StrangerBusiness.TAG_SOCIAL_REQUEST);
        mApp.getStrangerBusiness().cancelSocialFriendRequest(socialFriendInfo, StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED, new StrangerBusiness
                .onCancelSocialFriend() {
            @Override
            public void onError(int type, int code) {
                mActivity.hideLoadingDialog();
                if (code == -2) {
                    mActivity.showToast(R.string.error_internet_disconnect);
                } else {
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onResponse(int type, ArrayList<SocialFriendInfo> list) {
                mActivity.hideLoadingDialog();
                mActivity.showToast(R.string.request_success);
//                listSocialRequest = HomeContactsHelper.getInstance(mApp).getNextSocialRequest();
                if (listSocialRequest.size() > 3) {
                    mListData.add(4, listSocialRequest.get(3));
                    mListData.remove(socialFriendInfo);
                }
                listSocialRequest.remove(socialFriendInfo);
                mAdapter.notifyDataSetChanged();
//                initAndDrawSocial();
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateListEvent(final AddFriendEventMessage event) {
        for (int i = 0; i < listRequestTmp.size(); i++) {
            mListData.remove(0);
        }
        listRequestTmp = getListSocialRequest(mApp.getStrangerBusiness().getSocialFriends(StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED));
        Log.i(TAG, "onUpdateListEvent: ");
        if (!listRequestTmp.isEmpty())
            mListData.addAll(0, listRequestTmp);
        mAdapter.setData(mListData);
        mAdapter.notifyDataSetChanged();



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llInviteFriend:
                NavigateActivityHelper.navigateToInviteFriends(mActivity, Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND);
                break;
            case R.id.llScanQRCode:
                NavigateActivityHelper.navigateToQRScan(mActivity);
                break;
            case R.id.llFindAround:
                NavigateActivityHelper.navigateToSettingActivity(mActivity, Constants.SETTINGS.NEAR_YOU);
                break;
        }
    }

    public static class AddFriendEventMessage {
    }


}

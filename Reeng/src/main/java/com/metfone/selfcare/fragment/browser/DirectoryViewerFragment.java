package com.metfone.selfcare.fragment.browser;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.adapter.image.ImageDirectoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageDirectory;
import com.metfone.selfcare.listeners.IImageBrowser.SelectImageDirectoryListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by huybq on 11/5/2014.
 */
public class DirectoryViewerFragment extends Fragment implements SelectImageDirectoryListener {
    private static String TAG = DirectoryViewerFragment.class.getSimpleName();

    private ImageBrowserActivity mImageBrowserActivity;
    //
    private ImageView imgNoMedia;
    private RecyclerView mRecyclerView;
    private TextView mTvwTitle;

    private ApplicationController mApp;
    private ImageDirectoryAdapter mAdapter;

    //
    public DirectoryViewerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate DirectoryViewerFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_directory_viewer, container, false);
        mApp = (ApplicationController) mImageBrowserActivity.getApplication();
        findComponentViews(rootView);
        checkImageStatus();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        mImageBrowserActivity = (ImageBrowserActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        setTitleActionBar();
        super.onResume();
    }


    private void findComponentViews(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_directory);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mApp, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        mAdapter = new ImageDirectoryAdapter(mImageBrowserActivity, this);
        mRecyclerView.setAdapter(mAdapter);
        imgNoMedia = (ImageView) rootView.findViewById(R.id.imgNoMedia);

        View abView = mImageBrowserActivity.getToolBarView();
        mTvwTitle = (TextView) abView.findViewById(R.id.ab_title);
    }

    @Override
    public void onSelectImageDirectory(ImageDirectory directory, int position) {
        if (mImageBrowserActivity != null) {
            mImageBrowserActivity.onSelectImageDirectory(directory, position);
        }
    }

    private void checkImageStatus() {
        if (mAdapter.getItemCount() == 0) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }

    private void setTitleActionBar() {
        mTvwTitle.setText(getString(R.string.browser_title));
        //
    }
}

package com.metfone.selfcare.fragment.game;

import android.app.Activity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.adapter.ListGameAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.GameHTML5Info;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListGameFragment extends BaseRecyclerViewFragment implements BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = ListGameFragment.class.getSimpleName();

    private ListGamesActivity mParentActivity;
    private ApplicationController mApplication;

    private RecyclerView mRecyclerView;
    private ListGameAdapter mAdapter;

    // TODO: Rename and change types and number of parameters
    public static ListGameFragment newInstance() {
        return new ListGameFragment();
    }

    public ListGameFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        initActionBar(inflater);
        initComponents(root, inflater);
        setListener();
        getData();
        return root;
    }

    @Override
    public void onRetryClick() {
        getData();
    }

    private void getData() {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
//            hideEmptyView();
            showRetryView();
            return;
        }
        showProgressLoading();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_LIST_GAME);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(mApplication.getReengAccountBusiness().getJidNumber())
                .append(mApplication.getReengAccountBusiness().getToken()).append(timeStamp);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", mApplication.getReengAccountBusiness().getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mApplication.getReengAccountBusiness().getToken()));
        params.addParam("clientType",Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("countryCode", mApplication.getReengAccountBusiness().getRegionCode());

        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, params.toString(),
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {
                        Log.i(TAG, "onResponse: " + s);
                        hideEmptyView();
                        parseResponse(s);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse", volleyError);
//                hideEmptyView();
//                showEmptyNote();
                showRetryView();
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG, false);
    }

    private void parseResponse(String s) {
        ArrayList<GameHTML5Info> listGame = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            int errorCode = jsonObject.optInt("code");
            if (errorCode == HTTPCode.E200_OK && jsonObject.has("lstGame")) {
                JSONArray jsonArray = jsonObject.getJSONArray("lstGame");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jso = jsonArray.getJSONObject(i);
                    int id = jso.optInt("id");
                    String title = jso.optString("title");
                    String desc = jso.optString("desc");
                    String link = jso.optString("link");
                    String thumb = jso.optString("thumb");
                    GameHTML5Info game = new GameHTML5Info(id, title, desc, link, thumb);
                    listGame.add(game);
                }

                mAdapter.setListGame(listGame);
                mAdapter.notifyDataSetChanged();
            } else {
//                mParentActivity.showToast(R.string.e601_error_but_undefined);
                showEmptyNote(mApplication.getResources().getString(R.string.e601_error_but_undefined));
//                showEmptyNote();
            }
        } catch (Exception e) {
            Log.e(TAG, "JSONException", e);
//            showEmptyNote(mApplication.getResources().getString(R.string.e601_error_but_undefined));
            showRetryView();

        }
    }

    private void initActionBar(LayoutInflater inflater) {
        View mActionBarView = mParentActivity.getToolBarView();
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));

        TextView mTvwTitle = (TextView) mActionBarView.findViewById(R.id.ab_title);
        ImageView mImgBack = (ImageView) mActionBarView.findViewById(R.id.ab_back_btn);
        mTvwTitle.setText(mParentActivity.getResources().getString(R.string.list_game));
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private void initComponents(View view, LayoutInflater inflater) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        RecyclerView.LayoutManager mRecyclerManager = new LinearLayoutManager(mParentActivity,
                LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mRecyclerManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ListGameAdapter(mParentActivity);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setListener() {

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
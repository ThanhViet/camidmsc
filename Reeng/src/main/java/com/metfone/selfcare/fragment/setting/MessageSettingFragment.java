package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.ui.EllipsisTextView;

/**
 * Created by huybq on 10/22/14
 */
public class MessageSettingFragment extends Fragment implements
        View.OnClickListener {
    private final String TAG = MessageSettingFragment.class.getSimpleName();
    private static final int REQUEST_SELECT_SOUND = 10;
    private SettingBusiness mSettingBusiness;
    private ReengAccountBusiness accountBusiness;
    private SettingActivity mParentActivity;
    private ApplicationController mApplication;
    private TextView mTvwTitle;
    private ImageView mImgBack;

    public static MessageSettingFragment newInstance() {
        MessageSettingFragment fragment = new MessageSettingFragment();
        return fragment;
    }

    public MessageSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSettingBusiness = SettingBusiness.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_setting_message, container, false);
        //findComponentViews(rootView, inflater, container);
        //setViewListeners();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SettingActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        accountBusiness = mApplication.getReengAccountBusiness();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        /*drawSettingNotificationSound();*/
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        View mViewActionBar = mParentActivity.getToolBarView();
        //
        ImageView mImgMore = (ImageView) mViewActionBar.findViewById(R.id.ab_more_btn);
        mImgMore.setVisibility(View.GONE);
        mTvwTitle = (EllipsisTextView) mViewActionBar.findViewById(R.id.ab_title);
        mTvwTitle.setText(mParentActivity.getResources().getString(R.string.setting_message));
        mImgBack = (ImageView) mViewActionBar.findViewById(R.id.ab_back_btn);
        //
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn: {
                mParentActivity.onBackPressed();
            }
            break;

            default:
                break;
        }
    }


   /* private void saveSettingShowBirthday() {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
            cbShowBirthday.setSelected(accountBusiness.isPermissionShowBirthday());
            return;
        }
        int permission = cbShowBirthday.isSelected() ? 1 : 0;
        mParentActivity.showLoadingDialog("", R.string.processing);
        ProfileRequestHelper.onResponseSettingPermissionListener listener = new ProfileRequestHelper.onResponseSettingPermissionListener() {
            @Override
            public void onResponse() {
                mParentActivity.showToast(R.string.change_setting_success);
                mParentActivity.hideLoadingDialog();
                accountBusiness.updatePermissionBirthday(cbShowBirthday.isSelected());
            }

            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                Log.d(TAG, "onError: " + errorCode);
                mParentActivity.showToast(getString(R.string.e490_illegal_state_exception), Toast.LENGTH_SHORT);
                cbShowBirthday.setSelected(accountBusiness.isPermissionShowBirthday());
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setPermission(accountBusiness.getCurrentAccount(), permission, listener);
    }*/

    /*private void saveSettingHideHistoryStranger() {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
            cbSaveLogListen.setSelected(accountBusiness.isHideStrangerHistory());
            return;
        }
        int permission = cbSaveLogListen.isSelected() ? 1 : 0;
        mParentActivity.showLoadingDialog("", R.string.processing);
        ProfileRequestHelper.onResponseSettingPermissionListener listener = new ProfileRequestHelper.onResponseSettingPermissionListener() {
            @Override
            public void onResponse() {
                mParentActivity.showToast(R.string.change_setting_success);
                mParentActivity.hideLoadingDialog();
                accountBusiness.updatePermissionHideStrangerHistory(cbSaveLogListen.isSelected());
            }

            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                Log.d(TAG, "onError: " + errorCode);
                mParentActivity.showToast(getString(R.string.e490_illegal_state_exception), Toast.LENGTH_SHORT);
                cbSaveLogListen.setSelected(accountBusiness.isHideStrangerHistory());
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setPermissionHideStrangerHistory(accountBusiness.getCurrentAccount(), permission, listener);
    }*/


}
package com.metfone.selfcare.fragment.metfoneservice.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.roundview.RoundTextView;

public class DialogAddAccoutCamIDFromOTPVerify extends Dialog implements View.OnClickListener {
    private RoundTextView btnContinue;
    private TextView tvBack;

    public DialogAddAccoutCamIDFromOTPVerify(@NonNull Activity activity) {
        super(activity);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_account_camid_otp_verify);
        btnContinue.setOnClickListener(this);
        tvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnContinue:
                break;
            case R.id.tvBack:
                break;
        }
    }

}

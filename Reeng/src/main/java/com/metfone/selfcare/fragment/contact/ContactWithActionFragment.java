package com.metfone.selfcare.fragment.contact;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.KeyboardUtils;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.fragment.call.KeyBoardDialog;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.ContactListInterface;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.model.Divider;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.indexable.IndexableRecyclerView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 6/28/2018.
 */

public class ContactWithActionFragment extends Fragment implements
        ContactChangeListener, View.OnClickListener {

    private static final String TAG = ContactWithActionFragment.class.getSimpleName();
    @BindView(R.id.tv_sms_out_to)
    EllipsisTextView tvSmsOutTo;
    @BindView(R.id.etSearch)
    ReengSearchView etSearch;
    @BindView(R.id.btnSearch)
    TextView btnSearch;
    @BindView(R.id.fragment_choose_number_listview)
    IndexableRecyclerView mRecyclerView;
    @BindView(R.id.fragment_choose_number_loading_progressbar)
    ProgressLoading fragmentChooseNumberLoadingProgressbar;
    Unbinder unbinder;
    @BindView(R.id.create_chat_header_chat_more_number)
    TextView createChatHeaderChatMoreNumber;
    @BindView(R.id.ivSearch)
    View ivSearch;
    @BindView(R.id.tvSearchTo)
    TextView tvSearchTo;
    @BindView(R.id.fragment_choose_number_note_text)
    TextView mTvNote;
    @BindView(R.id.tv_contact)
    TextView tvContact;
    @BindView(R.id.fragment_choose_number_size_text)
    TextView tvChooseSize;
    @BindView(R.id.img_show_keyboard)
    ImageView imgShowKeyBoard;
    @BindView(R.id.viewChooseNumber)
    RelativeLayout viewChooseNumber;
    @BindView(R.id.viewTextContacts)
    RelativeLayout viewTextContacts;

    private ApplicationController mApplication;
    private Resources mRes;
    private ContactListInterface mListener;
    private ContactListActivity mParentActivity;

    private HomeContactsAdapter mAdapter;

    private InitListAsyncTask mInitListAsyncTask;
    private ArrayList<Object> listData;
    private ArrayList<PhoneNumber> listPhoneNumbers;
    private ArrayList<PhoneNumber> listFavorites;
    private SearchContactTask mSearchTask;
    private int type = 0;
    public ContactWithActionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactWithActionFragment.
     */
    public static ContactWithActionFragment newInstance() {
        Log.i(TAG, "ContactWithActionFragment newInstance ...");
        ContactWithActionFragment fragment = new ContactWithActionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public static ContactWithActionFragment newInstance(int type) {
        Log.i(TAG, "ContactWithActionFragment newInstance ...");
        ContactWithActionFragment fragment = new ContactWithActionFragment();
        fragment.type = type;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ContactListActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (ContactListInterface) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mRes = mParentActivity.getResources();
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "ContactWithActionFragment onCreate ...");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_choose_number, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mParentActivity.setColorIconStatusBar();
        }
        unbinder = ButterKnife.bind(this, rootView);
        tvContact.setText(getString(R.string.thread_contact));
        tvChooseSize.setVisibility(View.GONE);
        imgShowKeyBoard.setVisibility(View.VISIBLE);
        imgShowKeyBoard.setOnClickListener(this);
        rootView.findViewById(R.id.iv_keyboard).setVisibility(View.GONE);
        ivSearch.setVisibility(View.VISIBLE);
        tvSearchTo.setVisibility(View.GONE);
        mTvNote.setVisibility(View.GONE);
        if (type == Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION) {
            viewTextContacts.setVisibility(View.VISIBLE);
        }

        etSearch.setHint(getString(R.string.sms_search_contact));
        setToolbar(inflater);
        setRecycleView(inflater);
        reloadDataAsyncTask();
        setReengSearchViewListener();
        setItemListViewListener();
        ListenerHelper.getInstance().addContactChangeListener(this);
        return rootView;
    }

    private void setRecycleView(LayoutInflater inflater) {
        mRecyclerView.setHideFastScroll(true);
        View mViewHeader = inflater.inflate(R.layout.item_contact_with_action_header, null);
        mViewHeader.findViewById(R.id.create_chat_header_call).setOnClickListener(view -> showKeyboardDialog());
        mAdapter = new HomeContactsAdapter(mApplication, new ArrayList<Object>());
        HeaderAndFooterRecyclerViewAdapter mHeaderFavoriteAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.setAdapter(mHeaderFavoriteAdapter);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        //mRecyclerViewMessage.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
      //  mHeaderFavoriteAdapter.addHeaderView(mViewHeader);
    }

    private void showKeyboardDialog() {
        KeyBoardDialog dialog = new KeyBoardDialog(mParentActivity, "");
        if (dialog.getWindow() != null) {
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogKeyBoardAnimation;
        }
        dialog.show();
    }

    private void setToolbar(LayoutInflater inflater) {
        View mActionBarView = mParentActivity.getToolBarView();
        View content = inflater.inflate(R.layout.ab_detail, null);
        mParentActivity.setCustomViewToolBar(content);
        TextView mTvwTitle = mActionBarView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mRes.getString(R.string.title_new_call));
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mTvwTitle.getLayoutParams();
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mTvwTitle.setGravity(Gravity.CENTER);
        ImageView mImgBack = mActionBarView.findViewById(R.id.ab_back_btn);
        mActionBarView.findViewById(R.id.ab_more_btn).setVisibility(View.GONE);
        mImgBack.setOnClickListener(v -> mParentActivity.onBackPressed());
        mImgBack.setVisibility(View.GONE);
        TextView cancel = mActionBarView.findViewById(R.id.ab_cancel_text);
        ImageView imgCancel = mActionBarView.findViewById(R.id.ab_back_btn);
        imgCancel.setVisibility(View.VISIBLE);
        imgCancel.setOnClickListener(v -> mParentActivity.onBackPressed());
        cancel.setVisibility(View.GONE);
        cancel.setPaintFlags(cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        cancel.setOnClickListener(v -> mParentActivity.onBackPressed());
        View vDivider = mActionBarView.findViewById(R.id.vDivider);
        vDivider.setVisibility(View.GONE);
    }

    private void setItemListViewListener() {
        mAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onAudioCall(Object entry) {
                super.onAudioCall(entry);
                HomeContactsHelper.getInstance(mApplication).handleCallContactsClick(mParentActivity, entry);
            }

            @Override
            public void onVideoCall(Object entry) {
                super.onVideoCall(entry);
                mApplication.getCallBusiness().checkAndStartVideoCall(mParentActivity, (PhoneNumber) entry, false);
            }

            @Override
            public void onActionLabel(Object entry) {
                super.onActionLabel(entry);
                if (entry instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) entry;
                    InviteFriendHelper.getInstance().showInviteFriendPopup(mApplication, mParentActivity,
                            phone.getName(), phone.getJidNumber(), false);
                }
            }

            @Override
            public void onSectionClick(Object entry) {
                super.onSectionClick(entry);
//                HomeContactsHelper.getInstance(mApplication).handleSectionContactsClick(mParentActivity, entry);
            }

            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mParentActivity, entry);
            }

            @Override
            public void onAvatarClick(Object entry) {
                super.onAvatarClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleAvatarContactsClick(mParentActivity, entry);
            }

            @Override
            public void onItemLongClick(Object object) {
                super.onItemLongClick(object);
                /*if (object instanceof PhoneNumber) {
                    PhoneNumber entry = (PhoneNumber) object;
                    if (entry.getContactId() != null) {
                        ItemContextMenu deleteItem = new ItemContextMenu(mParentActivity, mRes.getString(R.string.del_contact), -1, entry, Constants.MENU.DELETE);
                        ArrayList<ItemContextMenu> list = new ArrayList<>();
                        list.add(deleteItem);
                        PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(),
                                entry.getName(), list, new ClickListener.IconListener() {
                                    @Override
                                    public void onIconClickListener(View view, Object object, int menuId) {
                                        deleteContact(((PhoneNumber) object).getContactId());
                                    }
                                });
                    }
                }*/
            }
        });

        InputMethodUtils.hideKeyboardWhenTouch(mRecyclerView, mParentActivity);
    }

    private void deleteContact(final String id) {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(mParentActivity,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_DELETE_CONTACT);
        } else {
            new DialogConfirm(mParentActivity, true).
                    setLabel(mRes.getString(R.string.title_delete_contact)).
                    setMessage(mRes.getString(R.string.msg_delete_contact)).
                    setNegativeLabel(mRes.getString(R.string.cancel)).
                    setPositiveLabel(mRes.getString(R.string.ok)).
                    setEntry(id).
                    setPositiveListener(result -> {
                        String contactId = (String) result;
                        ContentObserverBusiness.getInstance(mParentActivity).setAction(Constants.ACTION.DEL_CONTACT);
                        if (mApplication.getContactBusiness().deleteContactDBAndDevices(contactId)) {
                            mApplication.getContactBusiness().initArrayListPhoneNumber();
                            //mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
                            reloadDataAsyncTask();
                        }
                        ContentObserverBusiness.getInstance(mParentActivity).setAction(-1);
                    }).show();
        }
    }
    @SuppressLint("ClickableViewAccessibility")
    private void setReengSearchViewListener() {
        etSearch.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (KeyboardUtils.keyboardShown(etSearch.getRootView())){
                    imgShowKeyBoard.setVisibility(View.GONE);
                }else{
                    imgShowKeyBoard.setVisibility(View.VISIBLE);
                }
            }
        });

        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    ivSearch.setVisibility(View.GONE);
                    etSearch.setText(getString(R.string.sms_to));
                    Selection.setSelection(etSearch.getText(), etSearch.getText().length());
                }else {
                    etSearch.setHint(getString(R.string.hint_enter_name_or_phone));
                    ivSearch.setVisibility(View.VISIBLE);
                    KeyboardUtils.hideSoftInput(getActivity());
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(ivSearch.getVisibility() == View.VISIBLE){
                    return;
                }
                if(!etSearch.getText().toString().startsWith(getString(R.string.sms_to))){
                    etSearch.setText(getString(R.string.sms_to));
                    Selection.setSelection(etSearch.getText(), etSearch.getText().length());
                }else {
                    String content = (s.toString()+ " ").split(":")[1].trim();
                    searchContactAsyncTask(content);
                }
            }
        });

        etSearch.setOnEditorActionListener((textView, keyCode, keyEvent) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                InputMethodUtils.hideSoftKeyboard(etSearch, mParentActivity);
            }
            return false;
        });
        etSearch.setOnTouchListener((view, motionEvent) -> {
            // assus zenphone
            //mSearchView.clearFocus();
            InputMethodUtils.showSoftKeyboard(mParentActivity, etSearch);
            return false;
        });
//        etSearch.postDelayed(() -> InputMethodUtils.showSoftKeyboardNew(mParentActivity, etSearch), 300);
    }

    private void searchContactAsyncTask(String textSearch) {
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        if (listData == null) {
            return;
        }
        if (TextUtils.isEmpty(textSearch)) {
            reloadDataAsyncTask();
        } else {
            mSearchTask = new SearchContactTask(mApplication);
            mSearchTask.setDatas(listPhoneNumbers);
            mSearchTask.setComparatorKeySearch(SearchUtils.getComparatorKeySearchForSearch());
            mSearchTask.setComparatorName(SearchUtils.getComparatorNameForSearch());
            mSearchTask.setComparatorContact(SearchUtils.getComparatorContactForSearch());
            mSearchTask.setComparatorMessage(SearchUtils.getComparatorMessageForSearch());
            mSearchTask.setListener(new SearchContactsListener() {
                @Override
                public void onPrepareSearchContact() {

                }

                @Override
                public void onFinishedSearchContact(String keySearch, ArrayList<PhoneNumber> list) {
                    notifyChangeAdapter(list, null);
                    showOrHideItemMoreNumber(keySearch, list);
                }
            });
            mSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
        }
    }
    private void showOrHideItemMoreNumber(String textSearch, ArrayList<PhoneNumber> result) {
        int countSpace = TextHelper.countSpace(textSearch);
        // khong hien option chat more
        if ((!TextUtils.isEmpty(textSearch)) && (result == null || result.isEmpty())) {
            // khong tim thay kq
            mTvNote.setVisibility(View.VISIBLE);
        } else {
            mTvNote.setVisibility(View.GONE);
        }
    }
    private void reloadDataAsyncTask() {
        Log.d(TAG, "reloadDataAsyncTask");
        if (mInitListAsyncTask != null) {
            mInitListAsyncTask.cancel(true);
            mInitListAsyncTask = null;
        }
        mInitListAsyncTask = new InitListAsyncTask();
        mInitListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
        if (mInitListAsyncTask != null) {
            mInitListAsyncTask.cancel(true);
            mInitListAsyncTask = null;
        }
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        ListenerHelper.getInstance().removeContactChangeListener(this);
    }

    @Override
    public void onContactChange() {
        reloadDataAsyncTask();
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        new Handler().post(() -> {
            if (mAdapter != null)
                mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    private void notifyChangeAdapter(ArrayList<PhoneNumber> list, ArrayList<PhoneNumber> listFavorites) {
        if (list.isEmpty()) {
            mTvNote.setVisibility(View.VISIBLE);
        } else {
            mTvNote.setVisibility(View.GONE);
        }
        if (listData == null) listData = new ArrayList<>();
        else listData.clear();
        if (Utilities.notEmpty(listFavorites)) {
            if (type != Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION) {
                if (listFavorites.get(0).getContactId() != null) {
                    PhoneNumber sectionFavo = new PhoneNumber(null, mRes.getString(R.string.list_favorite), -1);
                    listData.add(0, sectionFavo);
                }
                listData.addAll(listFavorites);
            }
        }
        if (Utilities.notEmpty(list)) {
            if (type != Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION) {
                listData.add(new Divider());
            }
            listData.addAll(list);
        }
        if (mAdapter != null) {
            mAdapter.setData(listData);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_show_keyboard:
                showKeyboardDialog();
                break;
        }
    }

    private interface SearchContactsListener {
        void onPrepareSearchContact();

        void onFinishedSearchContact(String keySearch, ArrayList<PhoneNumber> list);
    }

    private static class SearchContactTask extends AsyncTask<String, Void, ArrayList<PhoneNumber>> {
        private final String TAG = "SearchContactTask";
        private WeakReference<ApplicationController> application;
        private SearchContactsListener listener;
        private String keySearch;
        private CopyOnWriteArrayList<PhoneNumber> datas;
        private Comparator comparatorName;
        private Comparator comparatorKeySearch;
        private Comparator comparatorContact;
        private Comparator comparatorMessage;
        private long startTime;
        private int totalDatas;

        public SearchContactTask(ApplicationController application) {
            this.application = new WeakReference<>(application);
            this.datas = new CopyOnWriteArrayList<>();
        }

        public void setListener(SearchContactsListener listener) {
            this.listener = listener;
        }

        public void setDatas(ArrayList<PhoneNumber> datas) {
            if (this.datas != null && datas != null)
                this.datas.addAll(datas);
        }

        public void setComparatorName(Comparator comparatorName) {
            this.comparatorName = comparatorName;
        }

        public void setComparatorKeySearch(Comparator comparatorKeySearch) {
            this.comparatorKeySearch = comparatorKeySearch;
        }

        public void setComparatorContact(Comparator comparatorContact) {
            this.comparatorContact = comparatorContact;
        }

        public void setComparatorMessage(Comparator comparatorMessage) {
            this.comparatorMessage = comparatorMessage;
        }

        @Override
        protected void onPreExecute() {
            if (listener != null) listener.onPrepareSearchContact();
        }

        @Override
        protected void onPostExecute(ArrayList<PhoneNumber> result) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                        + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
            if (listener != null) listener.onFinishedSearchContact(keySearch, result);
        }
        @Override
        protected ArrayList<PhoneNumber> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            totalDatas = 0;
            ArrayList<PhoneNumber> list = new ArrayList<>();
            keySearch = params[0];
            if (Utilities.notNull(application) && Utilities.notEmpty(datas)) {
                totalDatas = datas.size();
                ResultSearchContact result = SearchUtils.searchMessagesAndContacts(application.get()
                        , keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage, comparatorContact);
                if (result != null) {
                    keySearch = result.getKeySearch();
                    if (Utilities.notEmpty(result.getResult())) {
                        for (Object obj : result.getResult()) {
                            if (obj instanceof PhoneNumber)
                                list.add((PhoneNumber) obj);
                        }
                    }
                }
            }
            return list;
        }
    }

    private class InitListAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
//            if (mAdapter == null) {
////                showProgressLoading();
//            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            if (!contactBusiness.isInitArrayListReady()) {
                contactBusiness.initArrayListPhoneNumber();
            }
            if (listData == null) {
                listData = new ArrayList<>();
            } else {
                listData.clear();
            }
//            listPhoneNumbers = contactBusiness.getListNumberSupport();
            listPhoneNumbers = contactBusiness.getListNumberAlls();
            listFavorites = contactBusiness.getListNumberFavorites();
            Log.d(TAG, "listFavorites: " + (listFavorites != null ? listFavorites.size() : "null"));
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
//            hideEmptyView();
            if (Utilities.notEmpty(listPhoneNumbers)) {
                if (type != Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION) {
                    if (listPhoneNumbers.get(0).getContactId() == null && !mRes.getString(R.string.menu_contacts).equals(listPhoneNumbers.get(0).getName())) {
                        PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), -1);
                        listPhoneNumbers.add(0, sectionContacts);
                    }
                } else if (listPhoneNumbers.get(1).getName().equals("#")){
                    listPhoneNumbers.remove(0);
                }
                else if (listPhoneNumbers.get(1).getName().equals("A")) {
                    listPhoneNumbers.remove(0);
                } else {
                    if (listPhoneNumbers.get(0).getContactId()!=null){
                        PhoneNumber sectionContactsNumber = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), -1);
                        listPhoneNumbers.add(0, sectionContactsNumber);
                    }
                    if (listPhoneNumbers.get(0).getName().equals(mRes.getString(R.string.menu_contacts))) {
                        listPhoneNumbers.remove(0);
                        PhoneNumber sectionContacts = new PhoneNumber(null, "A", -1);
                        listPhoneNumbers.add(0, sectionContacts);
                    }
                    if (listPhoneNumbers.get(0).getName().equals("A")&&listPhoneNumbers.get(1).getName().equals(R.string.menu_contacts)){
                        listPhoneNumbers.remove(0);
                    }
                }
                listData.addAll(listPhoneNumbers);
            }
            notifyChangeAdapter(listPhoneNumbers, listFavorites);
            super.onPostExecute(aVoid);
        }
    }
}

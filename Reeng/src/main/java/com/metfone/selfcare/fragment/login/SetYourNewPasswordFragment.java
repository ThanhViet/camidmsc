package com.metfone.selfcare.fragment.login;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;


public class SetYourNewPasswordFragment extends Fragment {


    public static SetYourNewPasswordFragment newInstance(String param1, String param2) {
        SetYourNewPasswordFragment fragment = new SetYourNewPasswordFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_set_your_new_password, container, false);
    }
}
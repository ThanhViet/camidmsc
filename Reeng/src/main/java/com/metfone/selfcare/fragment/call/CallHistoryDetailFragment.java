package com.metfone.selfcare.fragment.call;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.metfone.selfcare.activity.CallHistoryDetailActivity;
import com.metfone.selfcare.adapter.CallHistoryDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.FragmentCallHistoryDetailBinding;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.fragment.call.viewmodel.CallHistoryDetailViewModel;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 10/14/2016.
 */

public class CallHistoryDetailFragment extends Fragment implements View.OnClickListener {
    private FragmentCallHistoryDetailBinding binding;
    private CallHistoryDetailActivity mParentActivity;
    private Resources mRes;
    private OnFragmentInteractionListener mListener;
    private CallHistoryDetailAdapter mAdapter;
    public CallHistoryDetailViewModel viewModel;

    public static CallHistoryDetailFragment newInstance(long callId) {
        CallHistoryDetailFragment fragment = new CallHistoryDetailFragment();
        Bundle args = new Bundle();
        args.putLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID, callId);
        fragment.setArguments(args);
        return fragment;
    }

    public CallHistoryDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(CallHistoryDetailViewModel.class);
        setUpObserver();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (CallHistoryDetailActivity) activity;
        mRes = ApplicationController.self().getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParentActivity.getToolBarView().setVisibility(View.GONE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_call_history_detail, container, false);
        getData(savedInstanceState);
        drawDetail();
        setClickListener();
        mParentActivity.changeWhiteStatusBar();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        viewModel.updateListHistory();
        changeAdapter();
        super.onResume();
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID, viewModel.historyId);
    }

    @Override
    public void onClick(View view) {
        if (viewModel.callHistory == null || viewModel.threadMessage == null) return;
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.rlProfile:
                mListener.navigateToFriendProfile(viewModel.getFriendNumber());
                break;
            case R.id.rlCallAction:
                viewModel.callBusiness.checkAndStartCall(mParentActivity, viewModel.getFriendNumber());
                break;
            case R.id.rlVideoCallAction:
                viewModel.callBusiness.handleStartCall(viewModel.threadMessage, mParentActivity, false, true);
                break;
            case R.id.rlMessageAction:
                mListener.navigateToChatActivity(viewModel.getFriendNumber());
                break;
            case R.id.rlBlockAction:
                viewModel.setActionBlock();
                break;
        }
    }

    private void setUpObserver() {
        viewModel.showLoadingLive.observe(this, data -> {
            if (data != null && data) {
                mParentActivity.showLoadingDialog("", mRes.getString(R.string.processing), true);
            } else {
                mParentActivity.hideLoadingDialog();
            }
        });
        viewModel.isBlockLive.observe(this, isBlock -> {
            if (isBlock == null) return;
            binding.ivBlockAction.setImageResource(isBlock ? R.drawable.ic_unblock_setting : R.drawable.ic_block_setting);
            binding.tvBlockAction.setText(isBlock ? R.string.unblock : R.string.block);
        });
        viewModel.errorMessageLive.observe(this, msg -> {
            if (msg == null) return;
            mParentActivity.showError(msg, null);
        });
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            viewModel.setHistoryId(savedInstanceState.getLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID));
        } else if (getArguments() != null) {
            viewModel.setHistoryId(getArguments().getLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID, -1));
        }
    }

    private void drawDetail() {
        if (viewModel.callHistory == null) {
            return;
        }
        int sizeAvatar = (int) mParentActivity.getResources().getDimension(R.dimen.avatar_small_size);
        PhoneNumber phoneNumber = viewModel.getPhoneNumberFromNumber();
        boolean isEnableVideo;
        if (phoneNumber != null) {
            viewModel.setPhoneNumberAvatar(binding.ivAvatar, binding.tvAvatar, phoneNumber, sizeAvatar);
            binding.tvName.setText(phoneNumber.getName());
            isEnableVideo = phoneNumber.isReeng();
        } else {
            StrangerPhoneNumber stranger = viewModel.getExistStrangerPhoneNumberFromNumber();
            if (stranger != null) {
                if (TextUtils.isEmpty(stranger.getFriendName())) {
                    binding.tvName.setText(Utilities.hidenPhoneNumber(stranger.getFriendName()));
                } else {
                    binding.tvName.setText(stranger.getFriendName());
                }

                viewModel.setStrangerAvatar(binding.ivAvatar, binding.tvAvatar, stranger, null, null, sizeAvatar);
                isEnableVideo = false;
            } else {
                binding.tvName.setText(viewModel.getFriendNumber());
                viewModel.setUnknownNumberAvatar(binding.ivAvatar, binding.tvAvatar, sizeAvatar);
                NonContact nonContact = viewModel.getExistNonContact();
                isEnableVideo = nonContact != null && nonContact.isReeng();
            }
        }
        binding.tvPhoneNumber.setText(viewModel.getFriendNumber());
//        binding.tvCallDate.setText(TimeHelper.formatTimeCallDetail(mRes, viewModel.getCallTime()));
        binding.ivCallOutAction.setVisibility(View.GONE);
        binding.ivFreeCallAction.setVisibility(View.VISIBLE);
        binding.rlVideoCallAction.setVisibility(isEnableVideo ? View.VISIBLE : View.GONE);
        binding.ivBlockAction.setImageResource(
                viewModel.isBlockNumber() ? R.drawable.ic_unblock_setting : R.drawable.ic_block_setting);
        binding.tvBlockAction.setText(
                viewModel.isBlockNumber() ? R.string.unblock : R.string.block);
    }

    private void setAdapter() {
        mAdapter = new CallHistoryDetailAdapter(ApplicationController.self(), viewModel.historyDetails);
        binding.rvCallHistoryDetail.setLayoutManager(new LinearLayoutManager(mParentActivity));
        binding.rvCallHistoryDetail.setItemAnimator(new DefaultItemAnimator());
        binding.rvCallHistoryDetail.setAdapter(mAdapter);
    }

    private void changeAdapter() {
        if (mAdapter == null) {
            setAdapter();
        } else {
            mAdapter.setListData(viewModel.historyDetails);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setClickListener() {
        binding.abBackBtn.setOnClickListener(this);
        binding.rlProfile.setOnClickListener(this);
        binding.rlCallAction.setOnClickListener(this);
        binding.rlVideoCallAction.setOnClickListener(this);
        binding.rlMessageAction.setOnClickListener(this);
        binding.rlBlockAction.setOnClickListener(this);
    }

    public interface OnFragmentInteractionListener {
        void navigateToFriendProfile(String number);

        void navigateToChatActivity(String number);
    }
}
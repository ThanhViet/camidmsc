package com.metfone.selfcare.fragment;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.ProgressLoading;

/**
 * Created by toanvk2 on 2/22/2016.
 */
public class BaseListViewFragment extends Fragment {
    private EmptyViewListener mCallBack;
    private View emptyView;

    protected ProgressLoading mPrbLoading;
    private TextView mTvwNote;
    private ImageView mBtnRetry;
    private TextView mTvwRetry1, mTvwRetry2;

    protected void createView(LayoutInflater inflater, ListView listView, EmptyViewListener callBack) {
        this.mCallBack = callBack;
        emptyView = inflater.inflate(R.layout.view_empty, null);
        mPrbLoading = (ProgressLoading) emptyView.findViewById(R.id.empty_progress);
        mTvwNote = (TextView) emptyView.findViewById(R.id.empty_text);
        mBtnRetry = (ImageView) emptyView.findViewById(R.id.empty_retry_button);
        mTvwRetry1 = (TextView) emptyView.findViewById(R.id.empty_retry_text1);
        mTvwRetry2 = (TextView) emptyView.findViewById(R.id.empty_retry_text2);
        ViewGroup viewGroup = (ViewGroup) listView.getParent();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(emptyView, params);
        //container.addView(emptyView, 0);
        mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    showProgressLoading();
                    mCallBack.onRetryClick();
                }
            }
        });
        //hideEmptyView();
        showProgressLoading();
    }

    protected void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    protected void showEmptyNote(String content) {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.VISIBLE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
        mTvwNote.setText(content);
    }

    protected void showEmptyNote() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.VISIBLE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
    }

    protected void showProgressLoading() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.VISIBLE);
        mTvwNote.setVisibility(View.GONE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
    }

    protected boolean isShowProgressLoading() {
        if (emptyView == null || mPrbLoading == null) return false;
        return emptyView.getVisibility() == View.VISIBLE && mPrbLoading.getVisibility() == View.VISIBLE;
    }

    protected void showRetryView() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.GONE);
        mBtnRetry.setVisibility(View.VISIBLE);
        mTvwRetry1.setVisibility(View.VISIBLE);
        mTvwRetry2.setVisibility(View.VISIBLE);
    }

    public interface EmptyViewListener {
        void onRetryClick();
    }
}
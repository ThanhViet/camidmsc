package com.metfone.selfcare.fragment.metfoneservice;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.metfoneservice.dialog.DialogVerifyOTP;
import com.metfone.selfcare.ui.roundview.RoundTextView;


public class NewPassWordFragment extends Fragment implements View.OnClickListener {
    RoundTextView btnConfirm;

    public static NewPassWordFragment newInstance(String param1, String param2) {
        NewPassWordFragment fragment = new NewPassWordFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_pass_word, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnConfirm = view.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConfirm:
                DialogVerifyOTP dialogVerifyOTP = new DialogVerifyOTP(getActivity());
                dialogVerifyOTP.show();
                break;
        }
    }
}
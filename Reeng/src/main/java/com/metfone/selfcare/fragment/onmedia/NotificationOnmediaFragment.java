package com.metfone.selfcare.fragment.onmedia;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.onmedia.NotificationOnMediaAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.NotificationModel;
import com.metfone.selfcare.database.model.onmedia.RestAllNotifyModel;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayoutDirection;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 11/4/2015.
 */
public class NotificationOnmediaFragment extends Fragment implements
        InitDataListener,
        RecyclerClickListener {

    private static final String TAG = NotificationOnmediaFragment.class.getSimpleName();

    private OnMediaActivityNew mParentActivity;
    private ApplicationController mApp;
    private FeedBusiness mFeedBusiness;
    //    private ListView mLvNotification;
    private RecyclerView mRecyclerViewNotify;
    private boolean isLoading = false;
    private boolean isNoMoreNotify;
    private View mLayoutProgress;
    private LinearLayout mLayoutNoNotify;
    private View mFooterView;
    private WSOnMedia rest;
    //    private OnMediaNotificationAdapter notificationAdapter;
    private NotificationOnMediaAdapter notificationAdapter;
    private HeaderAndFooterRecyclerViewAdapter headerAndFooterRecyclerViewAdapter;

    private SwipyRefreshLayout mSwipyRefreshLayout;

    private int gaCategoryId, gaActionId;
    private boolean needToGetData;


    public static NotificationOnmediaFragment newInstance() {
        NotificationOnmediaFragment fragment = new NotificationOnmediaFragment();
        return fragment;
    }

    public NotificationOnmediaFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_notify;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_onmedia_notification, container, false);
        setAtionBar(inflater);

//        mLvNotification = (ListView) rootView.findViewById(R.id.listview);
        mRecyclerViewNotify = (RecyclerView) rootView.findViewById(R.id.recycler_view_notify_om);
        mRecyclerViewNotify.setLayoutManager(new LinearLayoutManager(mApp));
        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLayoutProgress = mFooterView.findViewById(R.id.layout_loadmore);
        mLayoutNoNotify = (LinearLayout) rootView.findViewById(R.id.layout_no_notify);
        mLayoutNoNotify.setVisibility(View.GONE);

        mSwipyRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.onmedia_swipy_layout);

        ArrayList<NotificationModel> notifyPref = mFeedBusiness.getListNotifyFromPref();
        /*if (notifyPref != null && !notifyPref.isEmpty()) {
            mProgressLoading.setVisibility(View.GONE);
        }*/

        notificationAdapter = new NotificationOnMediaAdapter(mApp, notifyPref, this);
        headerAndFooterRecyclerViewAdapter = new
                HeaderAndFooterRecyclerViewAdapter(notificationAdapter);
        mRecyclerViewNotify.setAdapter(headerAndFooterRecyclerViewAdapter);
        headerAndFooterRecyclerViewAdapter.addFooterView(mFooterView);
        //Cho nay ko load data nua, chi load so luong notify thoi, khi no click vao se refresh list
//        loadData("");
//        getNumberNotify();

        listViewListener();
        loadData("");
        return rootView;
    }

    private void setAtionBar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.app_bar_home_notification, null));
        mParentActivity.getToolBarView().setVisibility(View.VISIBLE);
        View abView = mParentActivity.getToolBarView();
        ImageView mBack = (ImageView) abView.findViewById(R.id.iv_back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mParentActivity.onBackPressed();
            }
        });
        TextView mTitle = (TextView) abView.findViewById(R.id.tv_title);
        mTitle.setText(R.string.notify_title);
        View more = abView.findViewById(R.id.line);
        more.setVisibility(View.VISIBLE);
    }

    /*private void getNumberNotify() {
        rest.getNumberNotify(new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "getNumberNotify: " + s);
                try {
                    int code = 0;
                    int number = 0;
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                        code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                        if (code == HTTPCode.E200_OK) {
                            number = jsonObject.getInt("number");
                            mFeedBusiness.setTotalNotify(number);
                        }
                    }
                } catch (JSONException e) {
                    Log.e(TAG,"Exception",e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "error: " + volleyError.toString());
            }
        });
    }*/

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void listViewListener() {
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if (isLoading) {
                    Log.i(TAG, "isloading....");
                } else {
                    Log.i(TAG, "loaddata onRefresh");
                    loadData("");
                }
            }
        });

        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewNotify.getLayoutManager();
                    if (layoutManager == null) {
                        Log.e(TAG, "null layoutManager");
                        return;
                    }
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isNoMoreNotify && !isLoading
                            && !mFeedBusiness
                            .getListNotification().isEmpty()) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }

            }
        };
        mRecyclerViewNotify.addOnScrollListener(mApp.getPauseOnScrollRecyclerViewListener(mOnScrollListener));
    }

    private void handleClickNotify(NotificationModel notificationModel) {
        if (notificationModel == null) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        notificationModel.setActive(NotificationModel.FEED_SEEN);
        notificationAdapter.notifyDataSetChanged();
        if (notificationModel.getContent() == null
                || TextUtils.isEmpty(notificationModel.getContent().getUrl())) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            Log.e(TAG, "content error");
            return;
        }

        if (notificationModel.getType() == NotificationModel.TYPE_DEEPLINK) {
            DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, notificationModel.getContent().getUrl());
        } else {
            FeedModelOnMedia feedModelOnMedia = mFeedBusiness.getFeedFromRowId(notificationModel.getFeedId());
            if (feedModelOnMedia == null) {
                Log.i(TAG, "create new feed");
                feedModelOnMedia = new FeedModelOnMedia();
                feedModelOnMedia.setBase64RowId(notificationModel.getFeedId());
                feedModelOnMedia.setFeedContent(notificationModel.getContent());
                feedModelOnMedia.setActionType(notificationModel.getActionType());
                UserInfo userInfo = notificationModel.getUserInfo();
                feedModelOnMedia.setUserInfo(userInfo == null ? new UserInfo(notificationModel.getMsisdn(), "") : userInfo);
                feedModelOnMedia.setTimeServer(System.currentTimeMillis());
                feedModelOnMedia.setTimeStamp(notificationModel.getTime());
                feedModelOnMedia.setUserStatus(notificationModel.getStatus());
                feedModelOnMedia.setListTag(notificationModel.getListTag());
                mFeedBusiness.preProcessFeedModelListTag(feedModelOnMedia);
            }

            if (notificationModel.isReply()) {
                mParentActivity.navigateToReplyFromNotify(feedModelOnMedia,
                        notificationModel.getUrlSubComment(), notificationModel.getFeedId());
            } else if ((OnMediaHelper.isFeedViewVideo(feedModelOnMedia) || OnMediaHelper.isFeedViewMovie(feedModelOnMedia))
                    && (feedModelOnMedia.getActionType() == FeedModelOnMedia.ActionLogApp.POST
                    || feedModelOnMedia.getActionType() == FeedModelOnMedia.ActionLogApp.SHARE)) {
                mApp.getApplicationComponent().providesUtils().openVideoDetail(mParentActivity, feedModelOnMedia);
            } else {
                mParentActivity.navigateToCommentWithPreview(feedModelOnMedia, notificationModel.getBase64RowId());
            }
        }
        mFeedBusiness.setNotifySeen(notificationModel.getBase64RowId());
        mFeedBusiness.setNeedToRefreshListNotify(true);
    }

    public void loadData(final String lastRowId) {
        Log.i(TAG, "load list notify");
        needToGetData = false;
        isNoMoreNotify = false;
        if (mParentActivity != null) {
            isLoading = true;
            rest.getListNotify(lastRowId, new OnMediaInterfaceListener.GetListNotifyListener() {
                @Override
                public void onGetListNotifyDone(RestAllNotifyModel restAllNotifyModel) {
                    Log.i(TAG, "loaddata success code: " + restAllNotifyModel.getCode());
                    mSwipyRefreshLayout.setRefreshing(false);
                    isLoading = false;
//                    mProgressBar.setVisibility(View.GONE);
                    if (restAllNotifyModel.getCode() == HTTPCode.E200_OK) {
                        mFeedBusiness.setDeltaTimeServer(
                                System.currentTimeMillis() - restAllNotifyModel.getCurrentTimeServer());
                        loadDataDone(restAllNotifyModel.getData(), lastRowId);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    isLoading = false;
//                    mProgressBar.setVisibility(View.GONE);
                    mLayoutProgress.setVisibility(View.GONE);
                    mSwipyRefreshLayout.setRefreshing(false);
                    if (mFeedBusiness.getListNotification().isEmpty()) {
                        mLayoutNoNotify.setVisibility(View.VISIBLE);
                    }
                    if (volleyError instanceof NoConnectionError) {
                        mParentActivity.showToast(R.string.no_connectivity_check_again);
                    }
                    Log.i(TAG, "error");
                }
            });
        }
    }

    private void loadDataDone(ArrayList<NotificationModel> data, String lastRowId) {
        mLayoutProgress.setVisibility(View.GONE);
        if (data != null && !data.isEmpty()) {
            /*if (data.size() < WSOnMedia.NUMBER_PER_PAGE) {
                isNoMoreNotify = true;
            } else {
                isNoMoreNotify = false;
            }*/

            mLayoutNoNotify.setVisibility(View.GONE);
//            Collections.reverse(result);
            if (TextUtils.isEmpty(lastRowId)) {
                mFeedBusiness.setListNotification(data);
            } else {
                mFeedBusiness.getListNotification().addAll(data);
//                keepSelectionAndNotify(result.size());
            }
            notificationAdapter.setListNotification(mFeedBusiness.getListNotification());
            notificationAdapter.notifyDataSetChanged();
            mFeedBusiness.setListNotifyToPref();
            Log.i(TAG, "load data done: " + mFeedBusiness.getListNotification().size());

        } else {
            Log.i(TAG, "nomore feed");
            isNoMoreNotify = true;
            if (TextUtils.isEmpty(lastRowId)) {
                mLayoutNoNotify.setVisibility(View.VISIBLE);
                mFeedBusiness.setListNotification(new ArrayList<NotificationModel>());
                notificationAdapter.setListNotification(mFeedBusiness.getListNotification());
                notificationAdapter.notifyDataSetChanged();
                mFeedBusiness.setListNotifyToPref();
            } else {
//                mParentActivity.showToast(R.string.msg_loadmore_empty);
            }
        }
    }

    private void onLoadMore() {
        if (isLoading) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            mLayoutProgress.setVisibility(View.GONE);
            return;
        }
        Log.i(TAG, "loaddata onLoadMore " + getLastRowId());
        mLayoutProgress.setVisibility(View.VISIBLE);
        loadData(getLastRowId());
    }

    private String getLastRowId() {
        ArrayList<NotificationModel> list = mFeedBusiness.getListNotification();
        if (list == null || list.isEmpty()) {
            return "";
        } else {
            return list.get(list.size() - 1).getBase64RowId();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mParentActivity = (OnMediaActivityNew) activity;
        mApp = (ApplicationController) mParentActivity.getApplication();
        rest = new WSOnMedia(mApp);
        mFeedBusiness = mApp.getFeedBusiness();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onresume");
        ListenerHelper.getInstance().addInitDataListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onpause");
        ListenerHelper.getInstance().removeInitDataListener(this);
        mFeedBusiness.setListNotifyToPref();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setNeedToGetData(boolean needToGetData) {
        this.needToGetData = needToGetData;
    }

    @Override
    public void onDataReady() {
        if (needToGetData) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadData("");
                }
            });
        }
    }

    public void showLoadingWhenOpenDrawer() {
        mSwipyRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onClick(View v, int pos, Object object) {
        if (mFeedBusiness.getListNotification().isEmpty())
            return;
        NotificationModel notificationModel = (NotificationModel) notificationAdapter.getItem(pos);
        handleClickNotify(notificationModel);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_notify);
    }

    @Override
    public void onLongClick(View v, int pos, Object object) {

    }
}

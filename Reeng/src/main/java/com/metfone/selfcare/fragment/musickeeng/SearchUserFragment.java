package com.metfone.selfcare.fragment.musickeeng;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.SearchSongActivity;
import com.metfone.selfcare.adapter.SearchUserAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 6/6/2016.
 */
public class SearchUserFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener, TextWatcher {
    private final String TAG = SearchUserFragment.class.getSimpleName();
    private SearchSongActivity mParentActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private View rootView;
    private LinearLayoutManager mRecyclerManager;
    private RecyclerView mRecyclerView;
    private View mFooterView, mHeaderView;
    private LinearLayout mLoadMoreFooterView;
    private AppCompatEditText mSearchView;
    private AppCompatImageView icDeleteSearch;
    //
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private SearchUserAdapter mAdapter;
    private ArrayList<PhoneNumber> mListUser;
    private ArrayList<PhoneNumber> mListUserSave = new ArrayList<>(); //max 3 thang
    private String msgError;
    private boolean isLoading = false;
    private int page = 1;

    private AppCompatImageView mImgBack;
    private SharedPreferences mPref;
    private TextView mTvwSearchResult;
    private String textSearch;
    private boolean listEnd = false;

    public static SearchUserFragment newInstance() {
        SearchUserFragment fragment = new SearchUserFragment();
        return fragment;
    }

    public SearchUserFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search_user_v5, container, false);
        findComponentViews(inflater, container);
        getData();
        setViewListeners();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SearchSongActivity) activity;
        mApplication = (ApplicationController) activity.getApplication();
        mRes = mApplication.getResources();
        mPref = mParentActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        savePhoneNumber();
    }

    private void findComponentViews(LayoutInflater inflater, ViewGroup container) {
        mSearchView = rootView.findViewById(R.id.edtSearch);
        mSearchView.setHint(R.string.hint_type_name);
        mImgBack =  rootView.findViewById(R.id.icBackToolbar);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mHeaderView = inflater.inflate(R.layout.view_header_search, container, false);
        mLoadMoreFooterView = (LinearLayout) mFooterView.findViewById(R.id.layout_loadmore);
        mLoadMoreFooterView.setVisibility(View.GONE);
        msgError = mRes.getString(R.string.list_empty);
        mSearchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        mPrbLoading.setVisibility(View.GONE);
        mTvwSearchResult = (TextView) mHeaderView.findViewById(R.id.tvw_search_user);
        icDeleteSearch = rootView.findViewById(R.id.icDeleteSearch);

    }

    @Override
    public void onRetryClick() {

    }

    private void setRecyclerListener() {
        // on touch
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodUtils.hideSoftKeyboard(mParentActivity);
                return false;
            }
        });
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                PhoneNumber phoneNumber = (PhoneNumber) object;
                if (phoneNumber == null)
                    return;
                mParentActivity.navigateToProfile(phoneNumber);
                addPhoneNumber(phoneNumber);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mListUser == null || mListUser.isEmpty()) {
                    return;
                }
                if (TextUtils.isEmpty(mSearchView.getText().toString())) {
                    return;
                }
                if (mRecyclerManager == null) return;
                int totalItemCount = mRecyclerManager.getItemCount();
                int lastInScreen = mRecyclerManager.findLastVisibleItemPosition();
                if ((lastInScreen == (totalItemCount - 3)) && !isLoading && !listEnd) {
                    Log.i(TAG, "needToLoad");
                    onLoadMore();
                }
            }
        }));
    }

    private void setViewListeners() {
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
        setSearchViewListener();
        icDeleteSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchView.setText("");
            }
        });
    }

    private void savePhoneNumber() {

        try {
            JSONArray jsonArray = new JSONArray();
            for (PhoneNumber phone : mListUserSave) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.HTTP.STRANGER_MUSIC.MSISDN, phone.getJidNumber());
                jsonObject.put(Constants.HTTP.STRANGER_MUSIC.NAME, phone.getName());
                jsonObject.put(Constants.HTTP.STRANGER_MUSIC.STATUS, phone.getStatus());
                jsonObject.put(Constants.HTTP.STRANGER_MUSIC.GENDER, phone.getGender());
                jsonObject.put(Constants.HTTP.STRANGER_MUSIC.BIRTHDAY, phone.getBirthdayString());
                jsonObject.put(Constants.HTTP.STRANGER_MUSIC.LAST_AVATAR, phone.getLastChangeAvatar());
                jsonArray.put(jsonObject);
            }
            mPref.edit().putString(Constants.PREFERENCE.PREF_SAVE_SEARCH_USER, jsonArray.toString()).apply();
        } catch (Exception ex) {
            Log.i(TAG, "Exception", ex);
        }

    }

    private void setSearchViewListener() {
        mSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    page = 1;
                    textSearch = mSearchView.getText().toString().trim();
                    handleSearch(page);
                    mPrbLoading.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        mSearchView.addTextChangedListener(this);
    }

    private void handleSearch(final int page) {
        listEnd = false;
        ContactRequestHelper.getInstance(mApplication).searchUserMocha(textSearch, page, new ContactRequestHelper.onResponseContact() {
            @Override
            public void onResponse(ArrayList<PhoneNumber> responses) {
                isLoading = false;
                mPrbLoading.setVisibility(View.GONE);
                mLoadMoreFooterView.setVisibility(View.GONE);
                if (!responses.isEmpty()) {
                    if (page == 1) {
                        mListUser.clear();
                        mTvwSearchResult.setVisibility(View.VISIBLE);
                        String text = String.format(mRes.getString(R.string.search_user_with_name),
                                mSearchView.getText().toString().trim());
                        mTvwSearchResult.setText(text);
                    }
                    /*if (mListUser.isEmpty()) {
                        mListUser.addAll(responses);
                    } else {
                        mListUser.addAll(mListUser.size() - 1, responses);
                    }*/

                    mListUser.addAll(responses);
                    setAdapter(mListUser);
                } else {
                    if (page == 1) {
                        mTvwSearchResult.setVisibility(View.VISIBLE);
                        mTvwSearchResult.setText(mRes.getString(R.string.search_not_found));
                        mListUser.clear();
                        setAdapter(mListUser);
                    }
                    listEnd = true;
                }
            }

            @Override
            public void onError(int errorCode) {
                Log.e(TAG, "error");
                isLoading = false;
                mPrbLoading.setVisibility(View.GONE);
                mLoadMoreFooterView.setVisibility(View.GONE);
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }

    private void getData() {
        mListUser = new ArrayList<>();
        //TODO load data offline
        String saveData = mPref.getString(Constants.PREFERENCE.PREF_SAVE_SEARCH_USER, "");
        Log.i(TAG, "savedata: " + saveData);
        if (!TextUtils.isEmpty(saveData)) {
            try {
                JSONArray jsonArray = new JSONArray(saveData);
                if (jsonArray.length() > 0) {
                    int lengthArray = jsonArray.length();
                    for (int i = 0; i < lengthArray; i++) {
                        JSONObject objectContact = jsonArray.getJSONObject(i);
                        PhoneNumber phoneNumber = new PhoneNumber();
                        phoneNumber.setJsonObjectSearchUser(objectContact, 0);
                        phoneNumber.setAddRoster(1);
                        mListUserSave.add(phoneNumber);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        if (mListUserSave.isEmpty()) {
            mTvwSearchResult.setVisibility(View.GONE);
        } else {
            mTvwSearchResult.setVisibility(View.VISIBLE);
            mTvwSearchResult.setText(mRes.getString(R.string.search_user_recent));
        }
        setAdapter(mListUserSave);
    }

    private void setAdapter(ArrayList<PhoneNumber> listUser) {
        if (listUser == null) listUser = new ArrayList<>();
        if (mAdapter == null) {
            mAdapter = new SearchUserAdapter(mApplication, listUser);
            mRecyclerManager = new LinearLayoutManager(mParentActivity, LinearLayoutManager.VERTICAL, false);
            mRecyclerView.setLayoutManager(mRecyclerManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
            mHeaderAndFooterAdapter.addFooterView(mFooterView);
            mHeaderAndFooterAdapter.addHeaderView(mHeaderView);
//            RecyclerViewUtils.setHeaderView(mRecyclerView, mHeaderView);
//            RecyclerViewUtils.setFooterView(mRecyclerView, mFooterView);
            setRecyclerListener();
        } else {
            mAdapter.setListUser(listUser);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void onLoadMore() {
        if (mListUser == null || mListUser.isEmpty()) {
            return;
        }
        isLoading = true;
        mLoadMoreFooterView.setVisibility(View.VISIBLE);
        //TODO load more
        page = page + 1;
        handleSearch(page);
    }


    private void notifyChangeAdapter(ArrayList<PhoneNumber> phoneNumbers) {
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            msgError = mRes.getString(R.string.not_find);
            showEmptyNote(msgError);
            phoneNumbers = new ArrayList<>();
        } else {
            msgError = mRes.getString(R.string.list_empty);
            hideEmptyView();
        }
        setAdapter(phoneNumbers);
    }

    private void addPhoneNumber(PhoneNumber phoneNumber) {
        if (mListUserSave.isEmpty()) {
            mListUserSave.add(phoneNumber);
        } else {
            int index = -1;
            for (int i = 0; i < mListUserSave.size(); i++) {
                if (mListUserSave.get(i).getJidNumber().equals(phoneNumber.getJidNumber())) {
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                mListUserSave.remove(index);
            }
            if (mListUserSave.size() == 3) {
                mListUserSave.remove(2);
            }
            Log.i(TAG, "add phone:" + phoneNumber);
            mListUserSave.add(0, phoneNumber);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(charSequence != null && charSequence.length() != 0){
            icDeleteSearch.setVisibility(View.VISIBLE);
        }else {
            icDeleteSearch.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
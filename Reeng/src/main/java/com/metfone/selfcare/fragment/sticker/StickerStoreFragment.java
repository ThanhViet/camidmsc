package com.metfone.selfcare.fragment.sticker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.StickerActivity;
import com.metfone.selfcare.adapter.StickerStoreAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.task.DownloadStickerAsynctask;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THANHNT72 on 5/7/2015.
 */
public class StickerStoreFragment extends Fragment implements ClickListener.IconListener,
        DownloadStickerAsynctask.DownloadStickerCallBack {

    private static final String TAG = StickerStoreFragment.class.getSimpleName();

    private ApplicationController mApplication;
    private StickerActivity mActivity;
    private StickerBusiness mStickerBusiness;
    private ListView mList;
    private ProgressLoading mProgress;
    private LinearLayout mLayoutRetry;
    private TextView mTvwMessage;
    private Button mBtnRetry;
    private DownloadStickerAsynctask downloadStickerAsynctask;
    private ProgressDialog barProgressDialog;
    private boolean isDownloadCancel = false;
    //private StickerCollection mStickerCollection;
    private Toolbar mToolBar;
    private TextView mTitleTextView;
    private TextView mEmptyTv;

    private Handler mHandler;
    private ClickListener.IconListener mIconListener;
    private StickerStoreAdapter mAdapter;
    private List<StickerCollection> mListSticker;
    private ArrayList<Integer> mListNewStickerId;
    private int type = -1;
    private boolean checkScreenRatio = false;

    public StickerStoreFragment() {
    }

    public static StickerStoreFragment newInstance(int type) {
        StickerStoreFragment fragment = new StickerStoreFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.STICKER.FRAGMENT_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = (StickerActivity) activity;
        mApplication = (ApplicationController) mActivity.getApplication();
        mStickerBusiness = mApplication.getStickerBusiness();
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new Handler();
        mIconListener = this;
        if (type == Constants.STICKER.STORE) {
//            mBtnMore.setVisibility(View.VISIBLE);
        } else {
//            mBtnMore.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(Constants.STICKER.FRAGMENT_TYPE, Constants.STICKER.STORE);
            Log.i(TAG, "onCreate" + type);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "oncreateView" + type);
        View rootView = inflater.inflate(R.layout.fragment_sticker_store, container, false);
        findComponentViews(rootView);
        setViewListeners();
        mListNewStickerId = mStickerBusiness.getStickersNew();
//        mListSticker = mStickerBusiness.getAllEnabledStickerCollection();
        mListSticker = null;
        if (type == Constants.STICKER.COLLECTION) {
            mListSticker = mStickerBusiness.getAllDowloadedSticker();
//            mBtnMore.setVisibility(View.GONE);
        } else {
            mListSticker = mStickerBusiness.getAllNotDowloadSticker();
//            mBtnMore.setVisibility(View.VISIBLE);
        }
        if (mListSticker == null || mListSticker.isEmpty()) {
            if (type == Constants.STICKER.STORE) {
                mStickerBusiness.getStickerCollectionFromServer(true);
            } else {
                mTvwMessage.setText(mActivity.getResources().getString(R.string.list_empty));
                mTvwMessage.setVisibility(View.VISIBLE);
            }
        } else {
            mProgress.setVisibility(View.INVISIBLE);
            mAdapter = new StickerStoreAdapter(mActivity, mListSticker, this, mListNewStickerId,checkScreenRatio);
            mList.setAdapter(mAdapter);
            mList.setVisibility(View.VISIBLE);
        }

        DisplayMetrics metrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        int screenRatio = height / width;
        if (screenRatio == 2) {
            checkScreenRatio = true;
        } else if (screenRatio > 2) {
           checkScreenRatio = false;
        }


        return rootView;
    }

    private void refreshStickerList() {
        if (type == Constants.STICKER.COLLECTION) {
            mListSticker = mStickerBusiness.getAllDowloadedSticker();
        } else {
            mListSticker = mStickerBusiness.getAllNotDowloadSticker();
        }

        if (mListSticker.isEmpty()) {
            mTvwMessage.setText(mActivity.getResources().getString(R.string.list_empty));
            mTvwMessage.setVisibility(View.VISIBLE);
        } else {
            mTvwMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setViewListeners() {
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i(TAG, " mList.setOnItemClickListener");
                view.setSelected(true);
                StickerCollection mStickerCollection = mStickerBusiness.getStickerCollectionFromStoreById(mListSticker.get(i).getServerId());
                mStickerBusiness.insertOrUpdate(mStickerCollection);
                mActivity.goToPreviewSticker(mListSticker.get(i).getServerId(), false, true);
            }
        });

        mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getAllStickerCollectionFromServer();
            }
        });
    }

    private void findComponentViews(View rootView) {
        if (mActivity.getCheckPreview()) {
            mActivity.getShowTablayout();
        }
        mProgress = (ProgressLoading) rootView.findViewById(R.id.sticker_loading_progressbar);
        mProgress.setVisibility(View.GONE);
        mList = (ListView) rootView.findViewById(R.id.all_sticker_store);
        mList.setVisibility(View.GONE);
        mListSticker = new ArrayList<>();

        mLayoutRetry = (LinearLayout) rootView.findViewById(R.id.layout_retry);
        mTvwMessage = (TextView) rootView.findViewById(R.id.text_message);
        mTvwMessage.setVisibility(View.GONE);
        mBtnRetry = (Button) rootView.findViewById(R.id.btn_retry);
        mBtnRetry.setVisibility(View.GONE);

        mToolBar = mActivity.getToolBarView();
        mTitleTextView = (TextView) mToolBar.findViewById(R.id.ab_title);
        mTitleTextView.setText(R.string.sticker_store);
        mTitleTextView.setTypeface(null, Typeface.BOLD);
//        mTitleTextView.setText
//                (mActivity.getResources().getString(
//                        (type == Constants.STICKER.COLLECTION)
//                                ? R.string.title_user_sticker
//                                : R.string.sticker_store));

    }

    public void showDialogConfirmDelete(StickerCollection stickerCollection) {
        String message = String.format(mActivity.getString(
                R.string.confirm_delete_sticker_collection), stickerCollection.getCollectionName());
        PopupHelper.getInstance().showDialogConfirm(mActivity,
                "", message, mActivity.getString(R.string.ok), mActivity.getString(R.string.cancel),
                mIconListener, stickerCollection, Constants.MENU.POPUP_DELETE_STICKER_COLLECTION);
    }

    public void downloadStickerCollection(StickerCollection stickerCollection, boolean isUpdate) {
        //mStickerCollection = stickerCollection;
        if (downloadStickerAsynctask != null) {
            downloadStickerAsynctask.cancel(true);
            downloadStickerAsynctask = null;
        }
        downloadStickerAsynctask = new DownloadStickerAsynctask(mApplication,
                stickerCollection, isUpdate, StickerStoreFragment.this);
        downloadStickerAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void launchBarDialog(String message) {
        barProgressDialog = new ProgressDialog(mActivity);
        barProgressDialog.setTitle(mActivity.getString(R.string.loading));
        barProgressDialog.setMessage(message);
        barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        Drawable drawable = ContextCompat.getDrawable(mActivity, R.drawable.bg_voice_send_progress_bar);
        barProgressDialog.setProgressDrawable(drawable);
        barProgressDialog.setProgress(0);
        barProgressDialog.setMax(100);
        barProgressDialog.setCancelable(true);
        barProgressDialog.setCanceledOnTouchOutside(false);
        barProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                mActivity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        barProgressDialog.dismiss();
                        if (downloadStickerAsynctask != null) {
                            downloadStickerAsynctask.cancel(true);
                            downloadStickerAsynctask = null;
                        }
                        isDownloadCancel = true;
                    }
                });
        barProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (downloadStickerAsynctask != null) {
                    downloadStickerAsynctask.cancel(true);
                    downloadStickerAsynctask = null;
                }
                isDownloadCancel = true;
            }
        });
        isDownloadCancel = false;
        barProgressDialog.show();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_DELETE_STICKER_COLLECTION:
                StickerCollection mStickerCollection = (StickerCollection) entry;
                mStickerBusiness.deleteStickerCollection(mStickerCollection);
                mListNewStickerId = mStickerBusiness.getStickersNew();
                refreshStickerList();
                mAdapter.setListSticker(mListSticker);
                mAdapter.setListNewSticker(mListNewStickerId);
                mAdapter.notifyDataSetChanged();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPreExecute(StickerCollection stickerCollection) {
        launchBarDialog(stickerCollection.getCollectionName());
    }

    @Override
    public void onPostExecute(Boolean result, StickerCollection stickerCollection) {
        if (mHandler != null) {
            if (result) { // download ok
                barProgressDialog.dismiss();
                stickerCollection.setDownloaded(true);
                mActivity.showToast(mActivity.getString(R.string.sticker_download_done), Toast.LENGTH_LONG);
                refreshStickerList();
                mAdapter.setListSticker(mListSticker);
                mAdapter.notifyDataSetChanged();
            } else { // download fail
                barProgressDialog.dismiss();
                stickerCollection.setDownloaded(false);
                if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                    mActivity.showToast(mActivity.getString(R.string.prepare_photo_fail), Toast.LENGTH_LONG);
                } else {
                    mActivity.showToast(mActivity.getString(R.string.sticker_download_fail), Toast.LENGTH_LONG);
                }
            }
        }
    }

    @Override
    public void onProgressUpdate(int progress) {
        Log.d(TAG, "onPregressUpdate: " + progress);
        if (mHandler != null) {
            if (progress < 0) {
                barProgressDialog.setIndeterminate(true);
            } else {
                barProgressDialog.setIndeterminate(false);
                barProgressDialog.setProgress(progress);
            }
        }
    }
}

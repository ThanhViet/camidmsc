package com.metfone.selfcare.fragment.transfermoney.agency;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.AgencyTransferMoneyActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.adapter.avno.AgencyHistoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.AgencyHistory;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.load_more.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 9/19/2019.
 */

public class CreateNewTransferFragment extends Fragment implements AgencyHistoryAdapter.AgencyHistoryListener {

    private static final String TAG = CreateNewTransferFragment.class.getSimpleName();
    @BindView(R.id.ab_title)
    EllipsisTextView abTitle;
    @BindView(R.id.tilNumb)
    TextInputLayout tilNumb;
    @BindView(R.id.ivChooseContact)
    ImageView ivChooseContact;
    @BindView(R.id.tilAmount)
    TextInputLayout tilAmount;
    @BindView(R.id.tilContent)
    TextInputLayout tilContent;
    @BindView(R.id.tilPass)
    TextInputLayout tilPass;
    @BindView(R.id.btnTransfer)
    RoundTextView btnTransfer;
    @BindView(R.id.tvChangePass)
    TextView tvChangePass;
    Unbinder unbinder;

    EditText etNumb, etAmount, etContent, etPass;
    @BindView(R.id.rvHistory)
    RecyclerView rvHistory;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;


    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApp;
    private Resources mRes;

    private AgencyHistoryAdapter adapter;
    private ArrayList<AgencyHistory> listItem = new ArrayList<>();
    private HeaderAndFooterRecyclerViewAdapter wrapperAdapter;

    int page = 1;
    private String amount;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean isLoading;
    private boolean isNoMoreComment = false;
    private View viewLoadmore;


    public static CreateNewTransferFragment newInstance() {
        CreateNewTransferFragment fragment = new CreateNewTransferFragment();
        /*Bundle args = new Bundle();
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mApp = (ApplicationController) activity.getApplication();
        mRes = mApp.getResources();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_agency_create_transfer, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        etAmount = tilAmount.getEditText();
        etContent = tilContent.getEditText();
        etNumb = tilNumb.getEditText();
        etPass = tilPass.getEditText();

        abTitle.setText(getResources().getString(R.string.agency_transfer_money));
        adapter = new AgencyHistoryAdapter(activity, listItem, this);
        wrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(adapter);
        rvHistory.setAdapter(wrapperAdapter);
        ViewCompat.setNestedScrollingEnabled(rvHistory, false);

        View mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        viewLoadmore = mFooterView.findViewById(R.id.layout_loadmore);
        viewLoadmore.setVisibility(View.GONE);
        wrapperAdapter.addFooterView(mFooterView);

        AVNOHelper.getInstance(mApp).getHistoryAgency(page, new AVNOHelper.AgencyApiLister() {

            @Override
            public void onGetHistoryDone(ArrayList<AgencyHistory> list) {
                super.onGetHistoryDone(list);
                listItem.addAll(list);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFail(int code, String msg) {
                activity.showToast(msg);
            }
        });

        etAmount.addTextChangedListener(textWatcher);


        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (!isNoMoreComment && v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //code to fetch more data for endless scrolling
                        onLoadMore();
                    }
                }
            }
        });


        /*rvHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) rvHistory.getLayoutManager();
                    if (layoutManager == null) {
                        Log.e(TAG, "null layoutManager");
                        return;
                    }
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 3)) && !isLoading
                            && !isNoMoreComment) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }
            }
        });*/

        return rootView;
    }

    private void onLoadMore() {
        Log.i(TAG, "onloadmore");
        viewLoadmore.setVisibility(View.VISIBLE);
        AVNOHelper.getInstance(mApp).getHistoryAgency(page + 1, new AVNOHelper.AgencyApiLister() {

            @Override
            public void onGetHistoryDone(ArrayList<AgencyHistory> list) {
                super.onGetHistoryDone(list);
                viewLoadmore.setVisibility(View.GONE);
                if (list.size() > 0) {
                    listItem.addAll(list);
                    adapter.notifyDataSetChanged();
                    page++;

                } else
                    isNoMoreComment = true;
            }

            @Override
            public void onFail(int code, String msg) {
                viewLoadmore.setVisibility(View.GONE);
                activity.showToast(msg);
            }
        });
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!TextUtils.isEmpty(etAmount.getText()) && !validAmount(etAmount.getText().toString())) {
                etAmount.setText(amount);
                etAmount.setSelection(amount.length());
                activity.showToast(R.string.agency_number_dot);
            } else
                amount = etAmount.getText().toString();
        }
    };


    private boolean validAmount(String amount) {
        if (amount.contains(".")) {
            String[] s = amount.split("\\.");
            if (s.length > 1 && s[1].length() > 1) return false;
        }

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ivChooseContact, R.id.btnTransfer, R.id.tvChangePass, R.id.ab_back_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivChooseContact:
                Intent assign = new Intent(activity, ChooseContactActivity.class);
                assign.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_TRANSFER_MONEY);
                activity.startActivityForResult(assign, Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_TRANSFER_MONEY, false);
                break;
            case R.id.btnTransfer:
                onClickTransferMoney();
                break;
            case R.id.tvChangePass:
                ((AgencyTransferMoneyActivity) activity).showResetPassFragment();
                break;
            case R.id.ab_back_btn:
                activity.onBackPressed();
                break;
        }
    }

    public void onSelectContact(String jid) {
        etNumb.setText(jid);
        etNumb.setSelection(jid.length());
        scrollView.smoothScrollTo(0, 0);
    }

    private void onClickTransferMoney() {
        if (checkValidData()) {
            String jidFriend = etNumb.getText().toString().trim();
            String pass = etPass.getText().toString().trim();
            String amount = etAmount.getText().toString().trim();
            String content = etContent.getText().toString().trim();
            activity.showLoadingDialog("", R.string.loading);
            AVNOHelper.getInstance(mApp).createSessionAgency(jidFriend, AVNOHelper.TypeActionAgency.TRANSFER,
                    pass, amount, content, new AVNOHelper.AgencyApiLister() {

                        @Override
                        public void onCreateSessionDone(String session) {
                            super.onCreateSessionDone(session);
                            activity.hideLoadingDialog();
                            showDialogInputOtp(session);
                        }

                        @Override
                        public void onFail(int code, String msg) {
                            activity.hideLoadingDialog();
                            activity.showToast(msg);
                        }
                    });
        } else
            scrollView.smoothScrollTo(0, 0);
    }

    private void showDialogInputOtp(final String session) {
        String content = String.format(mRes.getString(R.string.agency_confirm_transfer_msg),
                etAmount.getText().toString().trim(), etNumb.getText().toString().trim());
        DialogEditText dialog = new DialogEditText(activity, true)
                .setCheckEnable(true)
                .setSelectAll(true)
                .setLabel(mRes.getString(R.string.agency_confirm_transfer))
                .setMessage(content)
                .setContentTop(true)
                .setTextHint(mRes.getString(R.string.agency_input_otp))
                .setMaxLength(12)
                .setNegativeLabel(mRes.getString(R.string.cancel))
                .setPositiveLabel(mRes.getString(R.string.agency_transfer_money))
                .setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String result) {
                        result = result.trim();
                        activity.showLoadingDialog("", R.string.loading);
                        AVNOHelper.getInstance(mApp).transferMoneyAgency(session, result, new AVNOHelper.AgencyApiLister() {
                            @Override
                            public void onTransferMoneyDone(String msg) {
                                super.onTransferMoneyDone(msg);
                                activity.hideLoadingDialog();
                                activity.showToast(msg);
                                resetView();
                            }

                            @Override
                            public void onFail(int code, String msg) {
                                activity.showToast(msg);
                                activity.hideLoadingDialog();
                            }
                        });
                    }
                }).setDismissListener(new DismissListener() {
                    @Override
                    public void onDismiss() {
                        activity.hideKeyboard();
                    }
                });
        dialog.show();
    }

    private void resetView() {
        etNumb.setText("");
        etAmount.setText("");
        etContent.setText("");
        etPass.setText("");
        EventBus.getDefault().post(new AVNOActivity.AVNOManagerMessage());
    }

    private boolean checkValidData() {
        //TODO check data

        String numb = etNumb.getText().toString().trim();
        String amount = etAmount.getText().toString().trim();
        String content = etContent.getText().toString().trim();
        String pass = etPass.getText().toString().trim();


        if (TextUtils.isEmpty(numb)) {
            activity.showToast(R.string.agency_invalid_numb);
            etNumb.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(amount)) {
            activity.showToast(R.string.agency_invalid_amount_money);
            etAmount.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(pass)) {
            activity.showToast(R.string.agency_empty_pass);
            etPass.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onSelectHistory(AgencyHistory agencyHistory) {
        etNumb.setText(agencyHistory.getMsisdn());
//        etAmount.setText(agencyHistory.getAmount());
        etNumb.setSelection(agencyHistory.getMsisdn().length());
        etNumb.requestFocus();
        scrollView.smoothScrollTo(0, 0);
    }
}

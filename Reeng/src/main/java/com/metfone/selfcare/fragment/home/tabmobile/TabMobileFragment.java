package com.metfone.selfcare.fragment.home.tabmobile;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.common.utils.screen.ScreenUtilsImpl;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.fragment.call.KeyBoardDialog;
import com.metfone.selfcare.fragment.contact.HomeContactsFragment;
import com.metfone.selfcare.fragment.contact.HomeGroupsFragment;
import com.metfone.selfcare.fragment.message.ThreadListFragmentRecycleView;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.model.SuggestContent;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.OpenRewardCamID;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.DeativatableViewPager;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.metfone.selfcare.helper.PermissionHelper.PERMISSION_GRANTED;

/**
 * Created by thanhnt72 on 6/21/2018.
 */

public class TabMobileFragment extends Fragment implements ViewPager.OnPageChangeListener
        , TabLayout.OnTabSelectedListener, ReengMessageListener, ClickListener.IconListener {

    private static final String TAG = TabMobileFragment.class.getSimpleName();
    @BindView(R.id.menu_call)
    FloatingActionButton menuCall;
    @BindView(R.id.menu_Message)
    FloatingActionButton menuMessage;
    @BindView(R.id.menu_Account)
    FloatingActionButton menuAccount;
    @BindView(R.id.headerController)
    LinearLayout headerController;
    @BindView(R.id.layout_delete)
    LinearLayout layoutDelete;
    @BindView(R.id.viewSubContent)
    ViewStub viewSubContent;
    @BindView(R.id.progressLoading)
    ProgressLoading progressLoading;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    Unbinder unbinder;
    @BindView(R.id.tv_time_remain_callout)
    RoundTextView tvTimeRemainCallout;

    @BindView(R.id.tv_saving_short_cut)
    RoundTextView tvSavingShortCut;

    @BindView(R.id.header_tab_event)
    View headerTabEvent;

    @BindView(R.id.header_tab_mobile)
    View headerTabMobile;

    @BindView(R.id.ivCheckbox)
    ImageView headerCheckbox;

    @BindView(R.id.tvw_cancel_delete)
    TextView headerCancelDelete;

    @BindView(R.id.tvw_delete_title)
    TextView headerDeleteTitle;

    @BindView(R.id.tvName)
    AppCompatTextView tvName;

    @BindView(R.id.tvRankName)
    AppCompatTextView tvRankName;

    @BindView(R.id.img_avatar)
    CircleImageView ivProfileAvatar;

    @BindView(R.id.menu)
    FloatingActionMenu flMenu;

    @BindView(R.id.layout_guest)
    View layout_guest;

    @BindView(R.id.layout_user)
    View layout_user;

    @BindView(R.id.img_grade)
    AppCompatImageView imgCrown;

    @BindView(R.id.header)
    LinearLayout header;

    @BindView(R.id.layout_no_data)
    LinearLayout layout_no_data;

    @BindView(R.id.layout_rank)
    RelativeLayout layout_rank;

    LinearLayout footerActionLayout;
    TextView tvTitlePin;
    ImageView ivPinThread;
    private TabLayout mTabLayout;
    private View mPinView, mDeleteView, mReadMark, viewHideThread, layoutReqSignup, cancle_thread;
    private TextView tvTitleRq, tvSubTitleRg, btnSignup;

    private DeativatableViewPager viewPager;
    private AppBarLayout appBarLayout;
    private LinearLayout viewAppBar;

    private ApplicationController mApp;
    private HomeActivity mActivity;
    private Resources mRes;

    private boolean needLoadOnCreate = false;
    private int currentTab;
    private String remainTimeCallout, savingShortCut;
    private boolean isSelectMode = false;

    private UserInfoBusiness userInfoBusiness;
    private UserInfo currentUser;
    private Animation animationSearch;
    private boolean checkAddNumber = false;
    private boolean isAllowPermissionContact = true; //  default = true -> not load contact on resume state
    private KhHomeClient khHomeClient;

    public static Fragment create() {
        return new TabMobileFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mApp.getMessageBusiness().addReengMessageListener(this);
        loadBannerSuggestVideo();
        if (needGetNewMediaBox) {
//            mApp.getConfigBusiness().requestListMediaBox();
            needGetNewMediaBox = false;
        }
        if (!isAllowPermissionContact) {
            if (checkPermissionContact()) {
                mApp.reLoadContactAfterPermissionsResult();
                isAllowPermissionContact = true;

            }
        }

        if (NetworkHelper.isConnectInternet(getActivity())) {
            layout_no_data.setVisibility(View.GONE);
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                layoutReqSignup.setVisibility(View.VISIBLE);
                setFlMenuVisible(View.GONE);
                ivSearch.setVisibility(View.GONE);
                ivMore.setVisibility(View.GONE);
            } else {
                if (UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT &&
                        new UserInfoBusiness(getContext()).getLogInMode() != EnumUtils.LoginModeTypeDef.PHONE_NUMBER &&
                        UserInfoBusiness.checkEmptyPhoneNumber(getContext()) == true) {
                    checkAddNumber = true;
                    layoutReqSignup.setVisibility(View.VISIBLE);
                    setFlMenuVisible(View.GONE);
                    ivSearch.setVisibility(View.GONE);
                    ivMore.setVisibility(View.GONE);
                } else {
                    layoutReqSignup.setVisibility(View.GONE);
                    ivSearch.setVisibility(View.VISIBLE);
                    ivMore.setVisibility(View.VISIBLE);
                    mTabLayout.setVisibility(View.VISIBLE);
                }
            }
            viewPager.setAdapter(createAdapterPage());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mApp.getMessageBusiness().removeReengMessageListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        Log.i(TAG, "onCreateView---------");
        long time = System.currentTimeMillis();
        View view = inflater.inflate(R.layout.fragment_home_tab_mobile, container, false);
        unbinder = ButterKnife.bind(this, view);
        mActivity = (HomeActivity) getActivity();
        mApp = (ApplicationController) mActivity.getApplication();
        mRes = mActivity.getResources();
        userInfoBusiness = new UserInfoBusiness(getContext());

        mapViewInviteUser(view);

        layout_guest.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true);
            }
        });
        ivProfileAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                NavigateActivityHelper.navigateToSetUpProfile(getActivity());
            }
        });

        animationSearch = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        animationSearch.setInterpolator(interpolator);
        animationSearch.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                ImageViewCompat.setImageTintList(ivSearch, ColorStateList.valueOf(ContextCompat.getColor(mApp, R.color.red)));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                checkUnreadHiddenThread();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        checkUnreadHiddenThread();
        checkReloadData();
        initView(view);
        Utilities.adaptViewForInserts(header);
        if (checkAddNumber == true) {
            tvTitleRq.setText(getContext().getText(R.string.open_chat));
            tvSubTitleRg.setText(getContext().getText(R.string.open_chat_subtitle));
            btnSignup.setText(getContext().getText(R.string.add_phone_number));
            view.findViewById(R.id.btnSignup).setOnClickListener(v -> {
                NavigateActivityHelper.navigateAddLoginActivity((BaseSlidingFragmentActivity) getActivity());
            });
        } else {
            view.findViewById(R.id.btnSignup).setOnClickListener(v -> {
                NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true);
            });
            view.findViewById(R.id.layout_guest).setOnClickListener(v -> {
                NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true);
            });
        }

        try {
            HomePagerFragment.self().getBottomNavigationBar().setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - time));
        return view;
    }

    private void checkUnreadHiddenThread() {
        if (mApp.getMessageBusiness().isHasUnreadHiddenThread())
            ImageViewCompat.setImageTintList(ivSearch, ColorStateList.valueOf(ContextCompat.getColor(mApp, R.color.white)));
        else {
            ImageViewCompat.setImageTintList(ivSearch, ColorStateList.valueOf(ContextCompat.getColor(mApp, R.color.white)));
        }
    }

    public void checkReloadData() {
        if (NetworkHelper.isConnectInternet(getActivity())) {
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                setFlMenuVisible(View.GONE);
                ivSearch.setVisibility(View.GONE);
                ivMore.setVisibility(View.GONE);
            } else {
                if (UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT &&
                        new UserInfoBusiness(getContext()).getLogInMode() != EnumUtils.LoginModeTypeDef.PHONE_NUMBER &&
                        UserInfoBusiness.checkEmptyPhoneNumber(getContext()) == true) {
                    checkAddNumber = true;
                    setFlMenuVisible(View.GONE);
                    ivSearch.setVisibility(View.GONE);
                    ivMore.setVisibility(View.GONE);
                } else {
                    ivSearch.setVisibility(View.VISIBLE);
                    ivMore.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getView() == null)
                needLoadOnCreate = true;
            /*else
                ChangeABColorHelper.changeTabColor(bgHeader, HomeTab.tab_chat, null,
                        mActivity.getCurrentTab(), mActivity.getCurrentColorTabWap());*/
            if (appBarLayout != null) appBarLayout.setExpanded(true);
        } else {
            isCanShow = false;
            hideVideoInviteUser();
        }
    }

    private void initView(View rootView) {
        viewSubContent.setLayoutResource(R.layout.layout_tab_mobile_content);
        View view = viewSubContent.inflate();

        tvTitleRq = rootView.findViewById(R.id.tvTitleRq);
        tvSubTitleRg = rootView.findViewById(R.id.tvSubTitleRg);
        btnSignup = rootView.findViewById(R.id.btnSignup);
        viewHideThread = rootView.findViewById(R.id.message_list_footer_hide_thread);
        footerActionLayout = rootView.findViewById(R.id.message_footer_action_layout);
        mPinView = rootView.findViewById(R.id.llPinThread);
        mDeleteView = rootView.findViewById(R.id.message_list_footer_delete);
        mReadMark = rootView.findViewById(R.id.message_list_footer_mark_read);
        cancle_thread = rootView.findViewById(R.id.cancle_thread);

        mTabLayout = view.findViewById(R.id.tab);
        viewPager = view.findViewById(R.id.viewPager);
        appBarLayout = view.findViewById(R.id.appBarLayout);
        viewAppBar = view.findViewById(R.id.viewAppBar);
        ivPinThread = view.findViewById(R.id.ivPinThread);
        tvTitlePin = view.findViewById(R.id.tvTitlePin);
        if (Config.Features.FLAG_SUPPORT_HIDE_THREAD)
            viewHideThread.setVisibility(View.VISIBLE);
        else
            viewHideThread.setVisibility(View.GONE);

        mPinView.setOnClickListener(mFooterViewClickListener);
        mDeleteView.setOnClickListener(mFooterViewClickListener);
        mReadMark.setOnClickListener(mFooterViewClickListener);
        viewHideThread.setOnClickListener(mFooterViewClickListener);
        cancle_thread.setOnClickListener(mFooterViewClickListener);
        viewPager.setAdapter(createAdapterPage());
        if (viewPager.getAdapter() != null)
            viewPager.setOffscreenPageLimit(viewPager.getAdapter().getCount());
        viewPager.setCurrentItem(mActivity.getCurrentSubTabMobile());
        viewPager.addOnPageChangeListener(this);
        mTabLayout.setupWithViewPager(viewPager);
        layoutReqSignup = rootView.findViewById(R.id.layout_req_signup);
        layoutReqSignup.setOnTouchListener((v, event) -> true);
        /*tab.setTabGravity(TabLayout.GRAVITY_FILL);
        tab.setTabMode(TabLayout.MODE_FIXED);*/
        /*tab.setTabTextColors(
                ContextCompat.getColor(mActivity, R.color.text_tab_gray),
                ContextCompat.getColor(mActivity, R.color.bg_mocha)
        );*/
//        tab.setSelectedTabIndicatorColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
        mTabLayout.addOnTabSelectedListener(this);

        progressLoading.setVisibility(View.GONE);
        if (needLoadOnCreate) {
            setUserVisibleHint(true);
            onPageSelected(mActivity.getCurrentSubTabMobile());
        }


    }

    private PagerAdapter createAdapterPage() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(mActivity);

        creator.add(FragmentPagerItem.of(mRes.getString(R.string.sub_tab_message), ThreadListFragmentRecycleView.class));
//        creator.add(FragmentPagerItem.of(mRes.getString(R.string.sub_tab_call), CallHistoryFragment.class));
        creator.add(FragmentPagerItem.of(mRes.getString(R.string.home_contact_tab_contacts), HomeContactsFragment.class));
        creator.add(FragmentPagerItem.of(mRes.getString(R.string.home_contact_tab_groups), HomeGroupsFragment.class));

        return new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener mFooterViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == null) return;
            switch (v.getId()) {
                case R.id.message_list_footer_delete:
                    EventBus.getDefault().post(new MessageFooterEvent(MessageFooterEvent.DELETE_THREAD_TYPE));
                    break;

                case R.id.message_list_footer_mark_read:
                    EventBus.getDefault().post(new MessageFooterEvent(MessageFooterEvent.MARK_THREAD_TYPE));
                    break;

                case R.id.llPinThread:
                    EventBus.getDefault().post(new MessageFooterEvent(MessageFooterEvent.PIN_THREAD_TYPE));
                    if (mActivity != null) {
                        mActivity.onBackPressed();
                    }
                    break;
                case R.id.message_list_footer_hide_thread:
                    EventBus.getDefault().post(new MessageFooterEvent(MessageFooterEvent.HIDE_THREAD_TYPE));
                    break;
                case R.id.cancle_thread:
                    if (mActivity != null) {
                        mActivity.onBackPressed();
                    }
                    break;
            }
        }
    };

    @OnClick({R.id.ivMore, R.id.ivSearch, R.id.tv_time_remain_callout
            , R.id.tv_saving_short_cut, R.id.checkbox_layout, R.id.tvw_cancel_delete, R.id.menu_call, R.id.menu_Account, R.id.menu_Message, R.id.img_avatar, R.id.layout_rank, R.id.tvName})
    public void onViewClicked(View view) {
        logActiveChat();
        switch (view.getId()) {
            case R.id.menu_call:
                getKhHomeClient().wsAppLog(new MPApiCallback<BaseResponse>() {
                    @Override
                    public void onResponse(Response<BaseResponse> response) {
                        if(response.isSuccessful()){
                            android.util.Log.d(TAG, String.format("wsLogApp => succeed [%s]", Constants.LOG_APP.MAKE_NEW_CALL));
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        android.util.Log.d(TAG, "wsLogApp => failed");
                    }
                }, Constants.LOG_APP.MAKE_NEW_CALL);
                NavigateActivityHelper.navigateToContactList(mActivity, Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION);
                break;
            case R.id.menu_Account:
                NavigateActivityHelper.navigateToChooseContact(mActivity, Constants
                        .CHOOSE_CONTACT.TYPE_CREATE_GROUP, null, null, null, false, true, Constants
                        .CHOOSE_CONTACT.TYPE_CREATE_GROUP, true);
                break;
            case R.id.menu_Message:
                NavigateActivityHelper.navigateToCreateChat(mActivity,
                        Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT, Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT);
                break;
            case R.id.ivMore:
//                DialogUtils.showOptionTabMobile(mActivity, this);
                //todo: theo giao diện mới không qua tabmobile chuyển thẳng đến history call
                NavigateActivityHelper.navigateToCallHistory(mActivity);
                break;
            case R.id.ivSearch:
                Utilities.openSearch(mActivity, Constants.TAB_MOBILE_HOME);
                break;
            case R.id.tv_time_remain_callout:
                if (mApp.getReengAccountBusiness().isEnableCallOut() && mApp.getReengAccountBusiness().isViettel()) {
                    NavigateActivityHelper.navigateToAVNOManager(mActivity);
                }
                break;
            case R.id.tv_saving_short_cut:
                if (mApp.getConfigBusiness().isEnableSaving() && mApp.getReengAccountBusiness().isOperatorViettel()) {
                    NavigateActivityHelper.navigateToTabSaving(mActivity);
                }
                break;
            case R.id.checkbox_layout:
                EventBus.getDefault().post(new HeaderEvent(HeaderEvent.SELECT_ALL_TYPE));
                break;
            case R.id.tvw_cancel_delete:
                if (mActivity != null) {
                    mActivity.onBackPressed();
                }
                break;
            case R.id.img_avatar:
            case R.id.tvName:
                NavigateActivityHelper.navigateToSetUpProfile(mActivity);
                break;
            case R.id.layout_rank:
                EventBus.getDefault().post(new OpenRewardCamID(TabHomeHelper.HomeTab.tab_chat));
                break;
        }
    }

    private void setStateCheckBoxAll(int sizeChecked, boolean selectAll) {
        if (headerDeleteTitle == null || headerCheckbox == null) return;
        if (selectAll) {
            headerCheckbox.setImageResource(R.drawable.ic_cb_selected);
//            headerCheckbox.setColorFilter(ContextCompat.getColor(mApp, R.color.v5_text));
        } else {
            headerCheckbox.setImageResource(R.drawable.ic_cb_unselected);
//            headerCheckbox.setColorFilter(ContextCompat.getColor(mApp, R.color.v5_text));
        }
        headerDeleteTitle.setText(String.format(mRes.getString(R.string.ab_delete_title), sizeChecked));
    }

    private boolean checkPermissionContact() {
        if (getActivity() != null) {
            int readContact = PermissionHelper.checkPermissionStatus(getActivity(), Manifest.permission.READ_CONTACTS);
            int writeContact = PermissionHelper.checkPermissionStatus(getActivity(), Manifest.permission.WRITE_CONTACTS);
            if (readContact == PERMISSION_GRANTED && writeContact == PERMISSION_GRANTED) {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.PERMISSION.PERMISSION_CONTACT) {
            if (PermissionHelper.onlyCheckPermissionContact(getContext())) {
                mApp.reLoadContactAfterPermissionsResult();
                isAllowPermissionContact = true;
            }
        }
    }

    public void showKeypad(String numb) {
        KeyBoardDialog dialog = new KeyBoardDialog(mActivity, numb);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogKeyBoardAnimation;
        dialog.show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.i(TAG, "onPageSelected: " + position);
        currentTab = position;
        mActivity.setCurrentSubTabMobile(currentTab);
        appBarLayout.setExpanded(true, true);
        if (UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT)
            switch (position) {
                case 0: {
                    setItemVisible(mApp.getConfigBusiness().isEnableSaving() && mApp.getReengAccountBusiness().isOperatorViettel());
                    flMenu.setVisibility(View.VISIBLE);
                    mActivity.setCheckTabMobile(false);
                    break;
                }
                case 1: {
                    setItemVisible(true);
                    flMenu.setVisibility(View.GONE);
                    mActivity.setCheckTabMobile(true);
                    break;
                }
                case 2: {
                    setItemVisible(false);
                    // check and show dialog require permission contact
                    isAllowPermissionContact = PermissionHelper.checkPermissionContact(this);
                    flMenu.setVisibility(View.VISIBLE);
                    mActivity.setCheckTabMobile(false);
                    break;
                }
                case 3:
                    setItemVisible(false);
                    break;

            }

        if (position == 0) {
            Log.i(TAG, "-------------onPageselected");
            showVideoInviteUser();
        } else
            hideVideoInviteUser();
    }

    private void setItemVisible(boolean isTabcall) {
        tvSavingShortCut.setVisibility(View.GONE);
        tvTimeRemainCallout.setVisibility(View.GONE);
        ivMore.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final TabMobileMessageEvent event) {
        if (!TextUtils.isEmpty(event.getNumbKeyPad())) {
            showKeypad(event.getNumbKeyPad());
        } else {
            if (event.isDismissDialog()) {
                appBarLayout.setExpanded(true, true);
                onPageSelected(currentTab);
                mActivity.hideKeyboard();
            } else if (event.isSelectMode()) {
                setSelectMode(true);
                setStateCheckBoxAll(1, event.selectAll);
                setStateIconPinThread(event.statePin);

            } else if (event.isSelectedTabMobile()) {
                onPageSelected(currentTab);
            } else {
                if (event.isSetHiddenThreadSucces()) {
//                    setSelectMode(false);
//                    setStateCheckBoxAll(0, false);
//                    setStateIconPinThread(false);
                    ivSearch.startAnimation(animationSearch);
                    onBackPressEvent(new TabMobileFragment.BackPressEvent(false, true));
                } else if (event.isSetUpPINSuccess()) {
                /*markSearchRed = false;
                ivSearch.startAnimation(animationSearch);*/
                    checkUnreadHiddenThread();
                    onBackPressEvent(new TabMobileFragment.BackPressEvent(false, true));
                } else {
                    setSelectMode(false);
                    setStateCheckBoxAll(0, false);
                    setStateIconPinThread(false);
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectItemEvent(final SelectItemEvent event) {
        setStateCheckBoxAll(event.size, event.selectAll);
        setStateIconPinThread(event.statePin);
    }

    private void setSelectMode(boolean selectMode) {
        isSelectMode = selectMode;
        if (rootInviteUserWatchVideo != null) {
            if (selectMode) {
                rootInviteUserWatchVideo.setVisibility(View.GONE);
            } else {
                Log.i(TAG, "-----------setSelectMode false");
                showVideoInviteUser();
            }
        }

        if (viewPager != null) {
            viewPager.setPagingEnable(!selectMode);
        }
        if (footerActionLayout != null) {
            layoutDelete.setVisibility(selectMode ? View.VISIBLE : View.GONE);
        }

        if (HomePagerFragment.self() != null && HomePagerFragment.self().getBottomNavigationBar() != null) {
            HomePagerFragment.self().getBottomNavigationBar().setVisibility(View.VISIBLE);
            HomePagerFragment.self().setSelectMode(selectMode);
        }

        if (headerController != null) {
            headerController.setVisibility(selectMode ? View.GONE : View.VISIBLE);
            //startHeaderTabAnimation(selectMode);
        }

//        if (headerTabMobile != null) {
//            headerTabMobile.setVisibility(selectMode ? View.GONE : View.VISIBLE);
//        }
        enableTabClick(selectMode);
    }

    private void startHeaderTabAnimation(boolean isSelectMode) {
        if (mRes == null || headerController == null) return;
        if (headerController.getAnimation() != null) {
            headerController.getAnimation().cancel();
            headerController.clearAnimation();
        }
        final int marginLeft = mRes.getDimensionPixelSize(R.dimen.margin_more_content_30);
        if (isSelectMode) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) headerController.getLayoutParams();
            layoutParams.leftMargin = -marginLeft;
            headerController.setLayoutParams(layoutParams);
            headerController.setAlpha(0.0f);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) viewAppBar.getLayoutParams();
            lp.topMargin = 0;
            viewAppBar.setLayoutParams(lp);
            Animation animation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    super.applyTransformation(interpolatedTime, t);
                    int marginLeftTime = (int) (interpolatedTime * marginLeft - marginLeft);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) headerController.getLayoutParams();
                    layoutParams.leftMargin = marginLeftTime;
                    headerController.setLayoutParams(layoutParams);
                    headerController.setAlpha(interpolatedTime);
                }

                @Override
                public void cancel() {
                    super.cancel();
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) headerController.getLayoutParams();
                    layoutParams.leftMargin = 0;
                    headerController.setLayoutParams(layoutParams);
                    headerController.setAlpha(1.0f);
                }
            };
            animation.setDuration(400);
            headerController.startAnimation(animation);
        }
    }

    private void setStateIconPinThread(boolean statePin) {
        if (tvTitlePin == null || ivPinThread == null) return;
        if (statePin) {
            ivPinThread.setImageResource(R.drawable.ic_option_pin_thread);
            tvTitlePin.setText(mRes.getString(R.string.option_pin_thread_chat));
        } else {
//            ivPinThread.setImageResource(R.drawable.ic_option_unpin_thread);
            ivPinThread.setImageResource(R.drawable.ic_option_pin_thread);
            tvTitlePin.setText(mRes.getString(R.string.option_unpin_thread_chat));
        }
    }

    private void enableTabClick(final boolean selectMode) {
        if (mTabLayout == null) return;
        mTabLayout.setAlpha(selectMode ? 0.6f : 1.0f);
        LinearLayout tabStrip = ((LinearLayout) mTabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return selectMode;
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSetExpandAppBarLayoutEvent(final SetExpandAppBarLayoutEvent event) {
//        mRecyclerView.setNestedScrollingEnabled(false);
        Log.i(TAG, "onSetExpandAppBarLayoutEvent: " + event.expanded);
        if (event.expanded)
            appBarLayout.setExpanded(true);
//        if (mApp.getMessageBusiness().isHasUnreadHiddenThread())
//            ImageViewCompat.setImageTintList(ivSearch, ColorStateList.valueOf(ContextCompat.getColor(mApp, R.color.red)));
//        else {
//            ImageViewCompat.setImageTintList(ivSearch, ColorStateList.valueOf(ContextCompat.getColor(mApp, R.color.black)));
//        }
    }

    private boolean needGetNewMediaBox = true;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressEvent(final BackPressEvent event) {
        Log.i(TAG, "onBackPressEvent: " + event.goToLaucher);
        if (event.goToLaucher) {
            needGetNewMediaBox = true;
            isCanShow = false;
            if (rootInviteUserWatchVideo != null) {
                if (inviteUserRunnable != null)
                    rootInviteUserWatchVideo.removeCallbacks(inviteUserRunnable);
                rootInviteUserWatchVideo.setVisibility(View.GONE);
            }
        } else {
//            viewPager.setCurrentItem(0);
            if (event.deselectMode) {
                setSelectMode(false);
                setStateCheckBoxAll(0, false);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRecyclerViewScrollEvent(final RecyclerViewScrollEvent event) {
        Log.i(TAG, "onRecyclerViewScrollEvent: ");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSetSubTabMobileEvent(final SetSubTabMobileEvent event) {
        Log.i(TAG, "onSetSubTabMobileEvent: ");
        viewPager.setCurrentItem(event.subTab);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSubTabCallEvent(final SubTabCallEvent event) {
        Log.i(TAG, "onSubTabCallEvent: " + event);
        if (mApp.getConfigBusiness().isEnableSaving() && mApp.getReengAccountBusiness().isOperatorViettel()) {
            if (event.getTotalSavingShortcut() != null) {
                savingShortCut = event.getTotalSavingShortcut();
                if (TextUtils.isEmpty(savingShortCut)) {
                    savingShortCut = mRes.getString(R.string.privilege);
                }
                tvSavingShortCut.setText(savingShortCut);
            }
        } else if (mApp.getReengAccountBusiness().isEnableCallOut() && mApp.getReengAccountBusiness().isViettel()) {
            if (event.getTimeRemain() != null) {
                remainTimeCallout = event.getTimeRemain();
                if (TextUtils.isEmpty(remainTimeCallout)) {
                    remainTimeCallout = mRes.getString(R.string.ab_call_subscription_non);
                }
                tvTimeRemainCallout.setText(remainTimeCallout);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        if (viewPager.getAdapter() == null) return;
        Object obj = viewPager.getAdapter().instantiateItem(viewPager, currentTab);
        if (obj == null) return;
        switch (currentTab) {
            case 0:
                ((ThreadListFragmentRecycleView) obj).onTabReselected();
                flMenu.setVisibility(View.VISIBLE);
                break;

            case 1:
                ((HomeContactsFragment) obj).onTabReselected();
                flMenu.setVisibility(View.GONE);
                break;

            case 2:
                ((HomeGroupsFragment) obj).onTabReselected();
                flMenu.setVisibility(View.VISIBLE);
                break;
        }

    }


    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
        if (thread.getHiddenThread() == 1) {
            ivSearch.startAnimation(animationSearch);
        }
    }

    @Override
    public void onRefreshMessage(int threadId) {

    }

    @Override
    public void notifyNewOutgoingMessage() {

    }

    @Override
    public void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {

    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {

    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {

    }

    @Override
    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {

    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {

    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {

    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {

    }

    @Override
    public void onUpdateStateRoom() {

    }

    @Override
    public void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread) {

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.MENU_MARK_READ:
                if (mActivity.getThreadListHandler() != null) {
                    mActivity.getThreadListHandler().onMarkAllAsRead();
                }
                break;
            case Constants.MENU.MENU_HISTORY:
                NavigateActivityHelper.navigateToCallHistory(mActivity);
                break;
            case Constants.MENU.MENU_CREATE_GROUP:
                NavigateActivityHelper.navigateToChooseContact(mActivity, Constants
                        .CHOOSE_CONTACT.TYPE_CREATE_GROUP, null, null, null, false, true, Constants
                        .CHOOSE_CONTACT.TYPE_CREATE_GROUP, true);
                break;
            case Constants.MENU.MENU_CALL:
                NavigateActivityHelper.navigateToContactList(mActivity, Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION);
                break;
            case Constants.MENU.MENU_MESS:
                NavigateActivityHelper.navigateToCreateChat(mActivity,
                        Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT, Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT);
                break;
        }
    }

    public static class TabMobileMessageEvent {
        private int idThread;
        boolean dismissDialog;
        int posScroll;
        boolean isSelectMode;
        boolean selectAll;
        boolean statePin;
        boolean setUpPINSuccess;
        boolean setHiddenThreadSucces;
        boolean selectedTabMobile;
        String numbKeyPad;

        public TabMobileMessageEvent() {
        }

        public TabMobileMessageEvent(int idThread) {
            this.idThread = idThread;
        }

        public TabMobileMessageEvent(boolean dismissDialog) {
            this.dismissDialog = dismissDialog;
        }

        public int getIdThread() {
            return idThread;
        }

        public boolean isDismissDialog() {
            return dismissDialog;
        }

        public int getPosScroll() {
            return posScroll;
        }

        public void setPosScroll(int posScroll) {
            this.posScroll = posScroll;
        }

        public boolean isSelectMode() {
            return isSelectMode;
        }

        public void setSelectMode(boolean selectMode) {
            isSelectMode = selectMode;
        }

        public boolean isSelectAll() {
            return selectAll;
        }

        public void setSelectAll(boolean selectAll) {
            this.selectAll = selectAll;
        }

        public boolean isStatePin() {
            return statePin;
        }

        public void setStatePin(boolean statePin) {
            this.statePin = statePin;
        }

        public boolean isSetUpPINSuccess() {
            return setUpPINSuccess;
        }

        public void setSetUpPINSuccess(boolean setUpPINSuccess) {
            this.setUpPINSuccess = setUpPINSuccess;
        }

        public boolean isSetHiddenThreadSucces() {
            return setHiddenThreadSucces;
        }

        public void setSetHiddenThreadSucces(boolean setHiddenThreadSucces) {
            this.setHiddenThreadSucces = setHiddenThreadSucces;
        }

        public boolean isSelectedTabMobile() {
            return selectedTabMobile;
        }

        public void setSelectedTabMobile(boolean selectedTabMobile) {
            this.selectedTabMobile = selectedTabMobile;
        }

        public String getNumbKeyPad() {
            return numbKeyPad;
        }

        public void setNumbKeyPad(String numbKeyPad) {
            this.numbKeyPad = numbKeyPad;
        }
    }

    public static class SubTabCallEvent {
        private String timeRemain;
        private String totalSavingShortcut;

        public SubTabCallEvent() {
        }

        public String getTimeRemain() {
            return timeRemain;
        }

        public void setTimeRemain(String timeRemain) {
            this.timeRemain = timeRemain;
        }

        public String getTotalSavingShortcut() {
            return totalSavingShortcut;
        }

        public void setTotalSavingShortcut(String totalSavingShortcut) {
            this.totalSavingShortcut = totalSavingShortcut;
        }

        @Override
        public String toString() {
            return "SubTabCallEvent{" +
                    "timeRemain='" + timeRemain + '\'' +
                    ", totalSavingShortcut='" + totalSavingShortcut + '\'' +
                    '}';
        }
    }

    public static class SetExpandAppBarLayoutEvent {
        boolean expanded;

        public SetExpandAppBarLayoutEvent(boolean expanded) {
            this.expanded = expanded;
        }
    }

    public static class BackPressEvent {
        boolean goToLaucher;
        boolean deselectMode;

        public BackPressEvent(boolean goToLaucher) {
            this.goToLaucher = goToLaucher;
        }

        public BackPressEvent(boolean goToLaucher, boolean deselectMode) {
            this.goToLaucher = goToLaucher;
            this.deselectMode = deselectMode;
        }

        public boolean isGoToLaucher() {
            return goToLaucher;
        }

        public void setGoToLaucher(boolean goToLaucher) {
            this.goToLaucher = goToLaucher;
        }

        public boolean isDeselectMode() {
            return deselectMode;
        }

        public void setDeselectMode(boolean deselectMode) {
            this.deselectMode = deselectMode;
        }
    }

    public static class MessageFooterEvent {
        int type;

        public MessageFooterEvent(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public static final int DELETE_THREAD_TYPE = 1;
        public static final int PIN_THREAD_TYPE = 2;
        public static final int MARK_THREAD_TYPE = 3;
        public static final int MARK_UNREAD_THREAD_TYPE = 4;
        public static final int HIDE_THREAD_TYPE = 5;
    }

    public static class HeaderEvent {
        int type;

        public HeaderEvent(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public static final int SELECT_ALL_TYPE = 1;
        public static final int CANCEL_TYPE = 2;
    }

    public static class SelectItemEvent {
        int size;
        boolean selectAll;
        boolean statePin;

        public SelectItemEvent(int size, boolean selectAll, boolean statePin) {
            this.size = size;
            this.selectAll = selectAll;
            this.statePin = statePin;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public boolean isSelectAll() {
            return selectAll;
        }

        public void setSelectAll(boolean selectAll) {
            this.selectAll = selectAll;
        }

        public boolean isStatePin() {
            return statePin;
        }

        public void setStatePin(boolean statePin) {
            this.statePin = statePin;
        }
    }

    public static class RecyclerViewScrollEvent {
        boolean isScrollUp;

        public RecyclerViewScrollEvent(boolean isScrollUp) {
            this.isScrollUp = isScrollUp;
        }
    }

    public static class SetSubTabMobileEvent {
        int subTab;

        public SetSubTabMobileEvent(int subTab) {
            this.subTab = subTab;
        }
    }

    /**
     * 19/6/2018
     * <p>
     * hoanganhtuan95ptit thêm tính năng mời người dùng xem video
     */

    private ImageView ivClose;
    private ImageView ivVideo;
    private ImageView ivFocus;
    private ImageView ivPlay;
    private TextView tvTitleVideo;
    private View rootInviteUserWatchVideo;
    private boolean isCanShow = false;

    /**
     * khởi tạo view của invite
     *
     * @param view rootView
     */
    private void mapViewInviteUser(View view) {
        if (view == null) return;
//        if (TabHomeHelper.getInstant(mApp).findPositionFromDeepLink(HomeTab.tab_video) <= 0) return;

        ivVideo = view.findViewById(R.id.ivVideo);
        ivFocus = view.findViewById(R.id.ivFocus);
        ivClose = view.findViewById(R.id.ivClose);
        ivPlay = view.findViewById(R.id.ivPlay);
        tvTitleVideo = view.findViewById(R.id.tvTitleVideo);
        rootInviteUserWatchVideo = view.findViewById(R.id.rootInviteUserWatchVideo);

        if (ivClose != null)
            ivClose.setOnClickListener(onViewInviteUserClickListener);
        if (rootInviteUserWatchVideo != null)
            rootInviteUserWatchVideo.setOnClickListener(onViewInviteUserClickListener);
//        loadBannerSuggestVideo();

    }

    private void loadBannerSuggestVideo() {
        if (rootInviteUserWatchVideo != null && inviteUserRunnable != null
                && mApp.getConfigBusiness().getSuggestContent() != null) {
            long timeWatchVideo = mApp.getPref().getLong(Constants.PREFERENCE.PREF_LAST_CLICK_SUGGEST_CONTENT, 0);
            if (!TimeHelper.checkTimeInDay(timeWatchVideo)) {
                isCanShow = true;
                rootInviteUserWatchVideo.removeCallbacks(inviteUserRunnable);
                rootInviteUserWatchVideo.postDelayed(inviteUserRunnable, 5 * 1000L);
            }
        }
    }

    /**
     * lắng nghe sự kiện click
     */
    private View.OnClickListener onViewInviteUserClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            isCanShow = false;
            closeClicked();
            SuggestContent suggestContent = mApp.getConfigBusiness().getSuggestContent();
            if (view == rootInviteUserWatchVideo && mApp != null && suggestContent != null) {
                mApp.getPref()
                        .edit()
                        .putLong(Constants.PREFERENCE.PREF_LAST_CLICK_SUGGEST_CONTENT, System.currentTimeMillis())
                        .apply();
                mApp.getLogApi().logBanner(suggestContent.getDeeplink(), LogApi.TypeService.SUGGEST_CONTENT, null);
                //                VideoPlayerActivity.start(mApp, video, "", true);
                DeepLinkHelper.getInstance().openSchemaLink(mActivity, suggestContent.getDeeplink());
            }
        }

        private void closeClicked() {
            hideVideoInviteUser();
        }
    };

    /**
     * bộ dịnh thời kiểm tra xem người dùng có xem video hay không
     */
    private Runnable inviteUserRunnable = new Runnable() {
        @Override
        public void run() {
            if (mApp != null) {
                isCanShow = true;
                Log.i(TAG, "----------inviteUserRunnable");
                showVideoInviteUser();
            }
        }
    };

    /**
     * gọi api lấy video đầu tiên trong tab hot
     */

    /**
     * ẩn giao diện
     */
    private void hideVideoInviteUser() {

        if (rootInviteUserWatchVideo != null && mApp != null) {
            rootInviteUserWatchVideo
                    .animate()
                    .translationX(ScreenUtilsImpl.getScreenWidth(mApp))
                    .setDuration(300L)
                    .setInterpolator(new OvershootInterpolator(1.25f))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            if (rootInviteUserWatchVideo != null)
                                rootInviteUserWatchVideo.setVisibility(View.GONE);
                        }
                    }).start();
        }
    }

    /**
     * hiển thị giao diện mời người dùng xem video
     */
    private void showVideoInviteUser() {
        SuggestContent suggestContent = mApp.getConfigBusiness().getSuggestContent();
        Log.i(TAG, "-----------showVideo: isSelectMode: " + isSelectMode + " isCanShow: " + isCanShow + " suggest: " + suggestContent);
        if (!isSelectMode && isCanShow && suggestContent != null && mApp != null && viewPager != null && viewPager.getCurrentItem() == 0) {
            //Setup vi tri
            if (ivFocus != null) {
                ivFocus.setVisibility(View.GONE);
                /*int[] width = Utilities.getMeasurementsForShiftingMode(mApp, ScreenManager.getWidth(), mApp.getConfigBusiness().countTabHome(), false);

                int itemNormalWidth = width[0];
                int itemActiveWidth = width[1];

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivFocus.getLayoutParams();
                params.leftMargin = itemActiveWidth + itemNormalWidth / 2 - Utilities.dpToPixels(24, getResources()) / 2;
                ivFocus.setLayoutParams(params);*/
            }
            if (ivVideo != null)
                ImageManager.showImage(suggestContent.getImg(), ivVideo);
            if (tvTitleVideo != null)
                tvTitleVideo.setText(suggestContent.getTitle());
            if (ivPlay != null)
                ivPlay.setVisibility(suggestContent.getType() == 0 ? View.VISIBLE : View.GONE);

            if (rootInviteUserWatchVideo != null) {
                rootInviteUserWatchVideo.setTranslationX(ScreenManager.getWidth(getActivity()));
                rootInviteUserWatchVideo.setVisibility(View.VISIBLE);
                rootInviteUserWatchVideo
                        .animate()
                        .translationX(0)
                        .setDuration(300L)
                        .setListener(null)
                        .setInterpolator(new OvershootInterpolator(1.25f))
                        .start();
            }
        }
    }

    class MyBounceInterpolator implements android.view.animation.Interpolator {
        private double mAmplitude = 1;
        private double mFrequency = 10;

        MyBounceInterpolator(double amplitude, double frequency) {
            mAmplitude = amplitude;
            mFrequency = frequency;
        }

        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                    Math.cos(mFrequency * time) + 1);
        }
    }

    public void setFlMenuVisible(int visibility) {
        if (flMenu != null) {
            flMenu.setVisibility(visibility);
        }
    }

    private void drawProfile(AccountRankDTO accountRankDTO, UserInfo userInfo) {
        tvName.setText(userInfo.getFull_name());
        KhUserRank myRank = KhUserRank.getById(accountRankDTO.rankId);
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_default)
                    .error(R.drawable.ic_avatar_default)
                    .into(ivProfileAvatar);
        } else {
            ivProfileAvatar.setImageResource(myRank.resAvatar);
        }
        imgCrown.setImageDrawable(ResourceUtils.getDrawable(myRank.resRank));
        tvRankName.setText(getString(myRank.resRankName));
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        if (event.loadType == LoadInfoUser.LoadType.LOADING) {
            layout_user.setVisibility(View.GONE);
            layout_guest.setVisibility(View.GONE);
            layout_no_data.setVisibility(View.VISIBLE);
        } else if (event.loadType == LoadInfoUser.LoadType.NOT_LOGIN) {
            layout_user.setVisibility(View.GONE);
            layout_guest.setVisibility(View.VISIBLE);
            layout_no_data.setVisibility(View.GONE);
        } else {
            layout_user.setVisibility(View.VISIBLE);
            layout_guest.setVisibility(View.GONE);
            layout_no_data.setVisibility(View.GONE);
            drawProfile(event.rankDTO, event.userInfo);
        }
    }

    private void logActiveChat(){
        ((HomeActivity)getActivity()).logActiveTab(FirebaseEventBusiness.TabLog.TAB_CHAT);
    }

    private KhHomeClient getKhHomeClient(){
        if(khHomeClient == null){
            khHomeClient = new KhHomeClient();
        }
        return khHomeClient;
    }
}


package com.metfone.selfcare.fragment.setting;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.text.Html;
import android.text.Spanned;
import android.view.View;

import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.setting.ConfigTabHomeAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.MochaShortcutManager;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.ui.recyclerview.drag.OnStartDragListener;
import com.metfone.selfcare.ui.recyclerview.drag.SimpleItemTouchHelperCallback;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.dialog.DialogCommon;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.OnClick;

//import com.metfone.selfcare.ui.dialog.DialogConfirm;

/**
 * Created by thanhnt72 on 12/11/2018.
 */

public class ConfigTabHomeFragment extends BaseSettingFragment implements OnStartDragListener {

    private static final String TAG = ConfigTabHomeFragment.class.getSimpleName();

    //@BindView(R.id.tvHelp)
//    TextView tvHelp;
//    @BindView(R.id.tvNotShowAgain)
//    TextView tvNotShowAgain;
    @BindView(R.id.recyclerViewConfig)
    RecyclerView rvTabHome;
    //    @BindView(R.id.rlHelp)
//    RelativeLayout rlHelp;

    boolean currentStateButton;
    private ConfigTabHomeAdapter adapter;
    private CopyOnWriteArrayList<ConfigTabHomeItem> listItem = new CopyOnWriteArrayList<>();
    private String oldData;
    private ItemTouchHelper mItemTouchHelper;
    private int positionRequestAddShortcut;
    private boolean isStartFirst;

    public static ConfigTabHomeFragment newInstance() {
        ConfigTabHomeFragment fragment = new ConfigTabHomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        positionRequestAddShortcut = -1;
        isStartFirst = true;
    }

    @Override
    public String getName() {
        return "Config tab setting";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_config_tab_setting_v5;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        oldData = TabHomeHelper.getInstant(mApplication).listTabHomeToString(listItem);
    }

    @Override
    protected void initView(View view) {
        setDataRecyclerView();
        setTitle(R.string.customize_news);
        icOptionToolbar.setVisibility(View.VISIBLE);
        icOptionToolbar.setImageResource(R.drawable.ic_v5_info_toolbar);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (positionRequestAddShortcut >= 0) {
            ((ConfigTabHomeAdapter.ConfigTabHomeHolder) rvTabHome.findViewHolderForAdapterPosition(positionRequestAddShortcut)).setIconShortcut();
            positionRequestAddShortcut = -1;
        } else {
            // all item tab always show on screen, don't need check item count display.
            // redraw icon short cut when activity resume
            if (!isStartFirst) {
                for (int i = 0; i < listItem.size(); i++) {
                    if (rvTabHome.findViewHolderForAdapterPosition(i) instanceof ConfigTabHomeAdapter.ConfigTabHomeHolder) {
                        ((ConfigTabHomeAdapter.ConfigTabHomeHolder) rvTabHome.findViewHolderForAdapterPosition(i)).setIconShortcut();
                    }
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isStartFirst = false;
    }

    private void setDataRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,
                false);
        rvTabHome.setLayoutManager(layoutManager);
        CopyOnWriteArrayList<ConfigTabHomeItem> listTmp = TabHomeHelper.getInstant(mApplication).getListAllItem();
        for (ConfigTabHomeItem item : listTmp) {
            try {
                listItem.add(item.clone());
            } catch (CloneNotSupportedException e) {
                Log.i(TAG, "CloneNotSupportedException", e);
            }
        }

        ConfigTabHomeListener listener = new ConfigTabHomeListener() {
            @Override
            public void onMoveItem(int from, int to) {
//                setStateButtonSave(checkStateButtonSave());
                currentStateButton = true;
            }

            @Override
            public boolean onChangeStateItem(int index) {
                final ConfigTabHomeItem item = listItem.get(index);
                int oldState = item.getState();
                if (oldState == ConfigTabHomeItem.STATE_NOT_ACTIVE) {
                    boolean hasMax = checkMaxItemEnable();
                    if (hasMax) {
                        DialogConfirm dialogConfirm = new DialogConfirm.Builder(DialogConfirm.CONFIRM_TYPE)
                                .setTitle(getString(R.string.max_config_tab_title))
                                .setMessage(getString(R.string.max_config_tab_body))
                                .setTitleRightButton(R.string.message_add_newcontact_btn)
                                .setTitleLeftButton(R.string.cancel)
                                .create();
                        dialogConfirm.setSelectListener(new BaseDialogFragment.DialogListener() {
                            @Override
                            public void dialogRightClick(int value) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    if (item.getHomeTab() == TabHomeHelper.HomeTab.tab_wap) {
                                        MochaShortcutManager.addShortcutToHomeWrap(item, mActivity);
//                                        MochaShortcutManager.addDynamicShortcutWrap(item, mActivity);
                                    } else {
//                                        MochaShortcutManager.addShortcutToHome(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
//                                        MochaShortcutManager.addDynamicShortcut(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
                                    }
                                }
                            }

                            @Override
                            public void dialogLeftClick() {

                            }
                        });

                        dialogConfirm.show(getChildFragmentManager(), "");
                        return false;
                    }
                    item.setState(ConfigTabHomeItem.STATE_ACTIVE);
                    setStateButtonSave(checkStateButtonSave());
                    return true;
                } else {
                    item.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
                    setStateButtonSave(checkStateButtonSave());
                    return true;
                }

            }

            @Override
            public void addShortcut(final ConfigTabHomeItem item, final int position) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (MochaShortcutManager.checkShortcutAdded(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mApplication)) {
                            ToastUtils.showToast(getContext(), getString(R.string.add_short_cut_exists));
                        // remove shortcut
//                        DialogConfirm dialogConfirm = DialogConfirm.newInstance(" Thoong", "Bạn có chắc muốn mục này khỏi màn hình chính",
//                                DialogConfirm.INTRO_TYPE, R.string.cancel, R.string.confirm);
//                        dialogConfirm.setSelectListener(new BaseDialogFragment.DialogListener() {
//                            @Override
//                            public void dialogRightClick(int value) {
//                                MochaShortcutManager.deleteShortcut(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
//                            }
//
//                            @Override
//                            public void dialogLeftClick() {
//
//                            }
//                        });
//                        dialogConfirm.show(getChildFragmentManager(), "");
                    } else {
                        if (item.getHomeTab() == TabHomeHelper.HomeTab.tab_wap) {
                            MochaShortcutManager.addShortcutToHomeWrap(item, mActivity);
//                        MochaShortcutManager.addDynamicShortcutWrap(item, mActivity);
                            positionRequestAddShortcut = position;
                        } else {
                            int icShortcut;
                            int titleShortcut;
                            switch (item.getHomeTab()) {
                                case tab_video:
                                    icShortcut = R.drawable.ic_func_video;
                                    titleShortcut = R.string.tab_video;
                                    break;
                                case tab_music:
                                    icShortcut = R.drawable.ic_func_music;
                                    titleShortcut = R.string.tab_music;
                                    break;
                                case tab_movie:
                                    icShortcut = R.drawable.ic_func_movie;
                                    titleShortcut = R.string.tab_movie;
                                    break;
                                case tab_news:
                                    icShortcut = R.drawable.ic_func_news;
                                    titleShortcut = R.string.tab_news;
                                    break;
                                case tab_stranger:
                                    icShortcut = R.drawable.ic_func_make_friend;
                                    titleShortcut = R.string.tab_stranger;
                                    break;
                                case tab_tiins:
                                    icShortcut = R.drawable.ic_func_tiin;
                                    titleShortcut = R.string.tab_tiin;
                                    break;
                                case tab_hot:
                                    icShortcut = R.drawable.ic_func_interactive;
                                    titleShortcut = R.string.tab_onmedia;
                                    break;
                                default: {
                                    icShortcut = R.drawable.ic_ab_mocha;
                                    titleShortcut = R.string.app_name;
                                }

                            }
                            DialogConfirm dialogConfirm = new DialogConfirm.Builder(DialogConfirm.CONFIRM_TYPE)
                                    .setTitle(getString(R.string.add_to_home_screen))
                                    .setMessage(getString(R.string.press_and_hold_to_add_to_home_screen))
                                    .setTitleLeftButton(R.string.cancel)
                                    .setTitleRightButton(R.string.poll_detail_add)
                                    .setTitleShortcut(titleShortcut)
                                    .setIdIconShortCut(icShortcut)
                                    .create();
                            dialogConfirm.setSelectListener(new BaseDialogFragment.DialogListener() {
                                @Override
                                public void dialogRightClick(int value) {
//                                    MochaShortcutManager.addShortcutToHome(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
//                                    MochaShortcutManager.addDynamicShortcut(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
                                    positionRequestAddShortcut = position;
                                }

                                @Override
                                public void dialogLeftClick() {

                                }
                            });
                            dialogConfirm.show(getChildFragmentManager(), "");
                        }
                    }
//=======
//                    if(item.getHomeTab() == TabHomeHelper.HomeTab.tab_wap)
//                    {
//                        MochaShortcutManager.addShortcutToHomeWrap(item, mActivity);
////                        MochaShortcutManager.addDynamicShortcutWrap(item, mActivity);
//                    }
//                    else
//                    {
//                        MochaShortcutManager.addShortcutToHome(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
//                        MochaShortcutManager.addDynamicShortcut(MochaShortcutManager.convertShortcutId(item.getHomeTab()), mActivity);
//                    }
//>>>>>>> MochaNewUIv2
                }
            }
        };

        adapter = new ConfigTabHomeAdapter(listItem, mApplication, this, listener);
        rvTabHome.setAdapter(adapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(rvTabHome);
    }

    private boolean checkMaxItemEnable() {
        int countItemEnable = 1;
        for (ConfigTabHomeItem item : listItem) {
            if (item.getState() == ConfigTabHomeItem.STATE_ACTIVE) {
                countItemEnable++;
                if (countItemEnable > 3) return true;
            }
        }
        return false;
    }

    private boolean checkStateButtonSave() {
        String newData = TabHomeHelper.getInstant(mApplication).listTabHomeToString(listItem);
        return !newData.equals(oldData);
    }

//    private void setTextHelp() {

//        tvNotShowAgain.setPaintFlags(tvNotShowAgain.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        tvNotShowAgain.setText(mRes.getString(R.string.content_help_customize_tab_understand));
//
//        String textHelp = mRes.getString(R.string.content_help_customize_tab);
//        Spanned spanned = Html.fromHtml(textHelp, new Html.ImageGetter() {
//            @Override
//            public Drawable getDrawable(String arg0) {
//                int id = 0;
//
//                try {
//                    if (arg0.equals("ic_checkbox_selected.png")) {
//                        id = R.drawable.ic_switch_on;
//                    } else if (arg0.equals("ic_add_tab.png")) {
//                        id = R.drawable.ic_switch_off;
//                    } else if (arg0.equals("ic_hold_move.png")) {
//                        id = R.drawable.ic_hold_move;
//                    } else if (arg0.equals("ic_add_shortcut.png")) {
//                        id = R.drawable.ic_add_shortcut;
//                    }
//                } catch (Exception ex) {
//
//                }
//
//                LevelListDrawable d = new LevelListDrawable();
//                Drawable empty = getResources().getDrawable(id);
//                double width = empty.getIntrinsicWidth() * 0.6;
//                double height = empty.getIntrinsicHeight() * 0.6;
//                d.addLevel(0, 0, empty);
//                d.setBounds(0, 0, (int) width, (int) height);
//                return d;
//            }
//        }, null);
//        tvHelp.setText(spanned);
//    }

//    private void initActionbar(LayoutInflater inflater) {
//        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
//        View mViewActionBar = mActivity.getToolBarView();
//        TextView mTvwTitle = (EllipsisTextView) mViewActionBar.findViewById(R.id.ab_title);
//        View mImgBack = mViewActionBar.findViewById(R.id.ab_back_btn);
//        mTvwTitle.setText(mRes.getString(R.string.title_customize_tab));
//        mImgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mActivity.onBackPressed();
//            }
//        });
//        View more = mViewActionBar.findViewById(R.id.ab_more_btn);
//        more.setVisibility(View.GONE);
//        tvSave = mViewActionBar.findViewById(R.id.tvSave);
//        tvSave.setVisibility(View.VISIBLE);
//        tvSave.setText(mRes.getString(R.string.save));
//        tvSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onSaveList();
//            }
//        });
//        setStateButtonSave(false);
//    }

    public void onSaveList() {
        if (!currentStateButton) return;
        TabHomeHelper.getInstant(mApplication).setListAllItem(listItem);
        Intent intent = new Intent(mApplication, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mApplication.startActivity(intent);
    }

    private void setStateButtonSave(boolean enable) {
        currentStateButton = enable;
//        if (enable) {
//            tvSave.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
//            tvSave.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 1);
//        } else {
//            tvSave.setTextColor(ContextCompat.getColor(mApplication, R.color.color_disable));
//            tvSave.setStroke(ContextCompat.getColor(mApplication, R.color.color_disable), 1);
//        }
//        tvSave.setEnabled(enable);
    }

    public boolean isCurrentStateButton() {
        return currentStateButton;
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }


    @OnClick(R.id.icOption)
    public void onViewClicked() {
//        mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_DONT_SHOW_TIP_CUSTOMIZE_TAB, true).apply();
        DialogCommon dialogCommon = new DialogCommon(mParentActivity, true);
        String textHelp = getString(R.string.content_help_customize_tab);
        Spanned spanned = Html.fromHtml(textHelp, arg0 -> {
            int id = 0;
            int width, height;
            float scale = 1f;
            try {
                if (arg0.equals("ic_checkbox_selected.png")) {
                    id = R.drawable.ic_v5_toggle_on;
                    scale = 0.3f;
                } else if (arg0.equals("ic_add_tab.png")) {
                    id = R.drawable.ic_v5_toggle_off;
                    scale = 0.3f;
                } else if (arg0.equals("ic_hold_move.png")) {
                    id = R.drawable.ic_v5_drag_setting;
                    scale = 0.6f;
                } else if (arg0.equals("ic_add_shortcut.png")) {
                    id = R.drawable.ic_v5_star_setting;
                    scale = 0.6f;
                }
            } catch (Exception ex) {

            }
            LevelListDrawable d = new LevelListDrawable();
            Drawable empty = getResources().getDrawable(id);
            width = (int) (empty.getIntrinsicWidth() * scale);
            height = (int) (empty.getIntrinsicHeight() * scale);
            d.addLevel(0, 0, empty);
            d.setBounds(0, 0, width, height);
            return d;
        }, null);
        dialogCommon.setTitle(getString(R.string.guideline_config_home));
        dialogCommon.setMessageSpan(spanned);
        dialogCommon.setLabelAction1(getString(R.string.btn_understood));
        dialogCommon.show();
    }

    public interface ConfigTabHomeListener {
        void onMoveItem(int from, int to);

        boolean onChangeStateItem(int index);

        void addShortcut(ConfigTabHomeItem item, int position);
    }

}

package com.metfone.selfcare.fragment.metfoneservice.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.roundview.RoundTextView;

public class DialogVerifyOTP extends Dialog implements View.OnClickListener {

    public DialogVerifyOTP(@NonNull Activity activity) {
        super(activity);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_verify_otp);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


        }
        dismiss();
    }

}

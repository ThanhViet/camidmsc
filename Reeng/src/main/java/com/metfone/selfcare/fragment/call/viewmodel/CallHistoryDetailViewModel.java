package com.metfone.selfcare.fragment.call.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.BlockContactBusiness;
import com.metfone.selfcare.business.CallBusiness;
import com.metfone.selfcare.business.CallHistoryHelper;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.model.BlockContactModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.database.model.call.CallHistoryDetail;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

public class CallHistoryDetailViewModel extends ViewModel {

    private static final String TAG = CallHistoryDetailViewModel.class.getSimpleName();

    public CallBusiness callBusiness;
    public ContactBusiness contactBusiness;
    public AvatarBusiness avatarBusiness;
    private BlockContactBusiness blockContactBusiness;
    private MessageBusiness messageBusiness;
    private StrangerBusiness strangerBusiness;

    public long historyId;
    public CallHistory callHistory;
    public ThreadMessage threadMessage;
    public ArrayList<CallHistoryDetail> historyDetails = new ArrayList<>();
    private CallHistoryHelper callHistoryHelper;

    public MutableLiveData<Boolean> showLoadingLive = new MutableLiveData<>();
    public MutableLiveData<Boolean> isBlockLive = new MutableLiveData<>();
    public MutableLiveData<String> errorMessageLive = new MutableLiveData<>();

    public CallHistoryDetailViewModel() {
        blockContactBusiness = ApplicationController.self().getBlockContactBusiness();
        callBusiness = ApplicationController.self().getCallBusiness();
        contactBusiness = ApplicationController.self().getContactBusiness();
        avatarBusiness = ApplicationController.self().getAvatarBusiness();
        messageBusiness = ApplicationController.self().getMessageBusiness();
        strangerBusiness = ApplicationController.self().getStrangerBusiness();
        callHistoryHelper = CallHistoryHelper.getInstance();
    }

    public void setHistoryId(long id) {
        historyId = id;
        callHistory = callHistoryHelper.getCallHistoryById(historyId);
        threadMessage = messageBusiness.findExistingOrCreateNewThread(getFriendNumber());
    }

    public void updateListHistory() {
        historyDetails = callHistoryHelper.getListHistoryDetailById(historyId);
    }

    public boolean isBlockNumber() {
        return blockContactBusiness.isBlockNumber(getFriendNumber());
    }

    private void addBlockNumber(String number) {
        blockContactBusiness.addBlockNumber(number);
    }

    private void removeBlockNumber(String number) {
        blockContactBusiness.removeBlockNumber(number);
    }

    public long getCallTime() {
        return callHistory.getCallTime();
    }

    public String getFriendNumber(){
        return callHistory.getFriendNumber();
    }

    public StrangerPhoneNumber getExistStrangerPhoneNumberFromNumber() {
        return strangerBusiness.getExistStrangerPhoneNumberFromNumber(getFriendNumber());
    }

    public NonContact getExistNonContact() {
        return contactBusiness.getExistNonContact(getFriendNumber());
    }

    public PhoneNumber getPhoneNumberFromNumber() {
        return contactBusiness.getPhoneNumberFromNumber(getFriendNumber());
    }

    public void setStrangerAvatar(ImageView avatarImage, TextView avatarText,
                                  StrangerPhoneNumber stranger,
                                  String friendName, String lAvatar, int size) {
        avatarBusiness.setStrangerAvatar(avatarImage, avatarText, stranger, getFriendNumber(), friendName, lAvatar, size);
    }

    public void setUnknownNumberAvatar(ImageView avatarImage, TextView avatarText, int size) {
        avatarBusiness.setUnknownNumberAvatar(avatarImage, avatarText, getFriendNumber(), size);
    }

    public void setPhoneNumberAvatar(ImageView avatarImage, TextView avatarText,
                                     PhoneNumber phoneNumber, int size) {
        avatarBusiness.setPhoneNumberAvatar(avatarImage, avatarText, phoneNumber, size);
    }

    public void setActionBlock() {
        final String friendPhoneNumber = threadMessage.getSoloNumber();
        final boolean isBlock = !isBlockNumber();
        ArrayList<BlockContactModel> blockList = new ArrayList<>();
        blockList.add(new BlockContactModel(friendPhoneNumber, isBlock ? 1 : 0));
        showLoadingLive.setValue(true);
        ContactRequestHelper.getInstance(ApplicationController.self()).handleBlockNumbers(blockList,
                new ContactRequestHelper.onResponseBlockContact() {
                    @Override
                    public void onResponse(String response) {
                        showLoadingLive.setValue(false);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                if (isBlock) {
                                    addBlockNumber(friendPhoneNumber);
                                } else {
                                    removeBlockNumber(friendPhoneNumber);
                                }
                                isBlockLive.setValue(isBlock);
                            } else {
                                String msg = ApplicationController.self().getString(R.string
                                        .e601_error_but_undefined);
                                errorMessageLive.setValue(msg);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception", e);
                            String msg = ApplicationController.self().getString(R.string.e601_error_but_undefined);
                            errorMessageLive.setValue(msg);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        showLoadingLive.setValue(false);
                        String msg;
                        if (errorCode == -2) {
                            msg = ApplicationController.self().getString(R.string.error_internet_disconnect);
                        } else {
                            msg = ApplicationController.self().getString(R.string.e601_error_but_undefined);
                        }
                        errorMessageLive.setValue(msg);
                    }
                });
    }
}

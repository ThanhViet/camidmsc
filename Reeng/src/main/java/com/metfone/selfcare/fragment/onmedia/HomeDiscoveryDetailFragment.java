package com.metfone.selfcare.fragment.onmedia;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.onmedia.ContentDiscovery;
import com.metfone.selfcare.helper.HomeWatcher;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.VideoViewCustom;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/8/17.
 */

public class HomeDiscoveryDetailFragment extends Fragment implements
        HomeWatcher.OnHomePressedListener {

    private static final String TAG = HomeDiscoveryDetailFragment.class.getSimpleName();

    private OnMediaActivityNew mActivity;
    private HomeDiscoveryNewFragment parentFragment;
    private ApplicationController mApp;
    private Resources mRes;

    private View viewShadow;
    private View layoutDiscoveryDetail;
    private View layoutVideo;
    private VideoViewCustom videoView;
    private ImageView btnPlayPause;
    private View mViewProgress;
    private View btnShare;
    private ImageView mImgThumb;
    private View layoutInfo;
    private TextView tvRead;
    private TextView tvTitle;
    boolean isTouchLayoutInfo = false;
    int heightScreen = 0;
    int widthScreen = 0;
    private Handler mHandler;
    private int position;
    private ArrayList<ContentDiscovery> listDiscoverSameTypes = new ArrayList<>();
    private ContentDiscovery contentDiscovery;
    private FeedBusiness mFeedBusiness;

    private View itemViewClick;
    int NONE = 0;
    int DRAG = 1;
    int mode = NONE;

    private View view;
    private int width;
    private int height;

    //    private ImageViewAwareTargetSize imageViewAwareTargetSize;
    private int lastPositionVideo;

    private HomeWatcher mHomeWatcher;
    private int gaCategory;

    public static HomeDiscoveryDetailFragment newInstance() {
        HomeDiscoveryDetailFragment fragment = new HomeDiscoveryDetailFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (OnMediaActivityNew) activity;
        mApp = (ApplicationController) mActivity.getApplication();
        mRes = mApp.getResources();
        gaCategory = R.string.ga_category_discover;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_discovery_detail, container, false);

        createViewDetail(view);
        mFeedBusiness = mApp.getFeedBusiness();

        mHomeWatcher = new HomeWatcher(mActivity);
        mHomeWatcher.setOnHomePressedListener(this);

        return view;
    }

    @Override
    public void onPause() {
        if (videoView != null) {
            lastPositionVideo = videoView.getCurrentPosition();
            Log.i(TAG, "onPause pause video");
            if (lastPositionVideo > 0) {
                btnPlayPause.setVisibility(View.VISIBLE);
            }
            mImgThumb.setVisibility(View.VISIBLE);
            videoView.pause();
        }
        mHomeWatcher.stopWatch();
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        mHomeWatcher.startWatch();
    }

    private void createViewDetail(View v) {
        width = mApp.getWidthPixels();
        height = mApp.getHeightPixels();
        float ratio = (float) width / (float) height;
        if (width > 600) {
            width = 600;
            height = Math.round(width / ratio);
        }


        widthScreen = mApp.getWidthPixels();
        heightScreen = mApp.getHeightPixels();

        viewShadow = v.findViewById(R.id.viewShadow);
        layoutDiscoveryDetail = v.findViewById(R.id.layoutDiscoveryDetail);
        layoutVideo = v.findViewById(R.id.layout_video);
        videoView = v.findViewById(R.id.textureVideo);
        videoView.setScaleType(VideoViewCustom.ScaleType.CENTER_CROP);
        mImgThumb = v.findViewById(R.id.img_thumb_content_discovery);
        btnShare = v.findViewById(R.id.btnShare);
        tvTitle = v.findViewById(R.id.tvTitle);
        btnPlayPause = v.findViewById(R.id.btnPlayPause);
        mViewProgress = v.findViewById(R.id.view_progress_loading_video);
        layoutInfo = v.findViewById(R.id.layout_info);
        tvRead = v.findViewById(R.id.tvRead);
        layoutInfo.setY(heightScreen);
        layoutInfo.setVisibility(View.VISIBLE);
        onTouchAction();
        mHandler = new Handler();
        Log.d(TAG, "imageViewAwareTargetSize ratio: " + ratio + " width: " + width + " --- " + height);
//        imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgThumb, width, height);
    }

    public void setCurrentPosition(int currentPosition) {
        this.position = 0;
        if (currentPosition >= 0) {
            contentDiscovery = mFeedBusiness.getListContentDiscoveries().get(currentPosition);
            listDiscoverSameTypes = mFeedBusiness.getListDiscoverSameType(contentDiscovery);
            int size = listDiscoverSameTypes.size();
            for (int i = 0; i < size; i++) {// lấy position hashMap
                if (listDiscoverSameTypes.get(i).getPosition() == currentPosition) {
                    position = i;
                    break;
                }
            }
            doShowDetail(contentDiscovery);
        }
    }

    private void doShowDetail(final ContentDiscovery contentDiscovery) {
        if (contentDiscovery == null) return;
        Log.i(TAG, "doShowDetail, play video");
        mHomeWatcher.startWatch();
        viewShadow.setVisibility(View.VISIBLE);
        layoutDiscoveryDetail.setX(0);
        layoutDiscoveryDetail.setY(0);
        layoutDiscoveryDetail.setScaleX(1);
        layoutDiscoveryDetail.setScaleY(1);

        layoutDiscoveryDetail.setVisibility(View.GONE);
        float scaleX = (float) itemViewClick.getWidth() / (float) widthScreen;
        float scaleY = (float) itemViewClick.getHeight() / (float) heightScreen;
        int actionBarHeight = (int) getContext().getResources().getDimension(R.dimen.action_bar_height);
        layoutDiscoveryDetail.animate()
                .scaleX(scaleX)
                .scaleY(scaleY)
                .translationXBy(itemViewClick.getX() - layoutDiscoveryDetail.getX() - widthScreen * (1 - scaleX) / 2)
                .translationYBy(itemViewClick.getY() - layoutDiscoveryDetail.getY() - heightScreen * (1 - scaleY) / 2
                        + actionBarHeight)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        layoutDiscoveryDetail.setVisibility(View.VISIBLE);
                        layoutDiscoveryDetail.animate().setListener(null);
                        layoutDiscoveryDetail.animate()
                                .scaleX(1)
                                .scaleY(1)
                                .translationX(0)
                                .translationY(0)
                                .setDuration(400).start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                }).setDuration(0).start();

        mImgThumb.setVisibility(View.VISIBLE);
        btnPlayPause.setVisibility(View.GONE);
//        ImageLoader.setImage(mApp.getApplicationContext(), contentDiscovery.getImageUrl(), mImgThumb);
        ImageLoaderManager.getInstance(mApp).setImageDiscovery(mImgThumb, contentDiscovery.getImageUrl(), width, height);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (contentDiscovery != null && !TextUtils.isEmpty(contentDiscovery.getVideoUrl())) {
                    playVideo(contentDiscovery.getVideoUrl());
                }
            }
        }, 200);
    }

    boolean isLongClick = false;
    boolean isShowLongClick = false;
    float startX = 0;
    float startY = 0;
    float endX = 0;
    float endY = 0;

    int ERROR_SUPPORT_MEDIA = -38;

    private void onTouchAction() {
        tvRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                layoutInfo.setY(heightScreen);
                layoutInfo.animate().translationY(0).setDuration(500).start();
            }
        });

        videoView.setListener(new VideoViewCustom.MediaPlayerListener() {
            @Override
            public void onVideoPrepared() {
                Log.i(TAG, "onVideoPrepared");
                videoView.start();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewProgress.setVisibility(View.GONE);
                        mImgThumb.setVisibility(View.GONE);
                        btnPlayPause.setVisibility(View.GONE);
                    }
                }, 1000);
            }

            @Override
            public void onVideoEnd() {
                Log.i(TAG, "onVideoEnd");
                lastPositionVideo = 0;
                if (parentFragment != null) {

//                        if(parentFragment.isShowDetail() && !videoView.isStop())
                    if (parentFragment.isShowDetail()) {
                        videoView.setKeepScreenOn(false);
                        Log.i(TAG, "auto next");
                        nextVideo(true);
                    }
                }
            }

            @Override
            public void onBufferStart() {
                Log.i(TAG, "onBufferStart");
                mViewProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onBufferEnd() {
                Log.i(TAG, "onBufferEnd");
                mViewProgress.setVisibility(View.GONE);
                mImgThumb.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

//                if(parentFragment != null)
//                {
//                    if(parentFragment.isShowDetail())
//                    {
//                        if (!NetworkHelper.isConnectInternet(mActivity)) {
//                            mActivity.showToast(getString(R.string.error_internet_disconnect),
//                                    Toast.LENGTH_SHORT);
//                        }
//                        else
//                        {
//                            mActivity.showToast(R.string.video_load_fail_support2);
//                        }
//                    }
//                }
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.i(TAG, "what: " + what + " extra: " + extra);
                if (what == ERROR_SUPPORT_MEDIA && extra == 0) {
                    showError2();
                    return true;
                }
                Log.i(TAG, "onError" + what + "/" + extra);
                showError();
                // showError();
                return true;
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelLongClick();
                handleActionShare(contentDiscovery);
            }
        });

        layoutDiscoveryDetail.setOnTouchListener(new View.OnTouchListener() {

            float dx = 0, dy = 0, dxLayoutInfo = 0, dyLayoutInfo = 0;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                endX = motionEvent.getRawX();
                endY = motionEvent.getRawY();

                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        isLongClick = false;
                        handler.postDelayed(mLongPressed, 1000);

                        dx = endX - view.getX();
                        dy = endY - view.getY();
                        dxLayoutInfo = endX - layoutInfo.getX();
                        dyLayoutInfo = endY - layoutInfo.getY();

                        startX = motionEvent.getRawX();
                        startY = motionEvent.getRawY();

                        mode = DRAG;
                        break;
                    case MotionEvent.ACTION_MOVE:

                        if (checkIsClick(startX, startY, endX, endY))
                            break;

                        if (mode == DRAG && !isTouchLayoutInfo && !isShowLongClick) {
                            view.setX(endX - dx);
                            view.setY(endY - dy);

                            float alpha = view.getY() / (float) (heightScreen / 2);
                            if (alpha > 1) alpha = 1;
                            viewShadow.setAlpha(1 - alpha);

                            if (view.getY() <= 0) {
                                view.setY(0);
//                                layoutInfo.setY(endY - dyLayoutInfo);
                            } else if (view.getY() > heightScreen) {
                                view.setY(heightScreen);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        handler.removeCallbacks(mLongPressed);
                        mode = NONE;
                        if (!isLongClick) {
                            //Long click cancel
                            if (isShowLongClick) {
                                cancelLongClick();
                                break;
                            }

                            //On Click
                            if (checkIsClick(startX, startY, endX, endY)) {
                                if (endX <= widthScreen / 3) {
                                    if (videoView != null && videoView.isPlaying()) {
                                        int pos = videoView.getCurrentPosition();
                                        pos = pos / 1000;
                                        Log.i(TAG, "previous video: " + pos);
                                        mActivity.trackingEvent(gaCategory, R.string
                                                .ga_action_discover_click_previous_video, String.valueOf(pos));
                                    }
                                    prevVideo(false);
                                } else if (endX >= widthScreen * 2 / 3) {
                                    Log.i(TAG, "click next video");
                                    if (videoView != null && videoView.isPlaying()) {
                                        int pos = videoView.getCurrentPosition();
                                        pos = pos / 1000;
                                        Log.i(TAG, "next video: " + pos);
                                        mActivity.trackingEvent(gaCategory, R.string
                                                .ga_action_discover_click_next_video, String.valueOf(pos));
                                    }
                                    nextVideo(false);
                                } else {
                                    //Pause
                                    if (videoView != null) {
                                        if (videoView.isPlaying()) {
                                            videoView.setKeepScreenOn(false);
                                            btnPlayPause.setVisibility(View.VISIBLE);
                                            videoView.pause();
                                        } else {
                                            videoView.setKeepScreenOn(true);
                                            btnPlayPause.setVisibility(View.GONE);
                                            videoView.start();
                                        }
                                    }
                                }
                                break;
                            }

                            //Move
                            if (view.getY() <= 0) {
                                view.setX(0);
                                view.setY(0);
//                            if(layoutInfo.getY() <= heightScreen * 2 / 3)
//                            {
//                                layoutInfo.animate().translationY(0).setDuration(400).start();
//                            }
//                            else if(layoutInfo.getY() > heightScreen * 2 / 3 && layoutInfo.getY() < (heightScreen -
// 5))
//                            {
//                                layoutInfo.animate().translationY(heightScreen).setDuration(400).start();
//                            }
//                            else
//                            {

//                            }
                            } else if (view.getY() > heightScreen) {
                                view.setX(0);
                                view.setY(heightScreen);
                            } else {
                                if (view.getY() >= heightScreen / 4) {
                                    if (parentFragment != null)
                                        parentFragment.hideViewDetail();
                                } else {
                                    view.animate().translationX(0).translationY(0).setDuration(400).start();
                                }
                            }
                        }
                        break;
                }
                return true;
            }

        });

        btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoView != null) {
                    if (videoView.isPlaying()) {
                        //Pause
//                        btnPlayPause.setVisibility(View.VISIBLE);
//                        btnPlayPause.setImageResource(R.drawable.ic_onmedia_play);
                    } else {
                        //play
                        btnPlayPause.setVisibility(View.GONE);
                        if (lastPositionVideo != 0) {
                            videoView.seekTo(lastPositionVideo);
                        }
                        videoView.start();
                    }
                }
            }
        });
    }

    private boolean isShowingDialogError = false;

    private void showError2() {
        if (isShowingDialogError) return;
        AlertDialog.Builder adb = new AlertDialog.Builder(mActivity);
        adb.setMessage("" + getString(R.string.video_load_fail_support));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                isShowingDialogError = false;
                mActivity.onBackPressed();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        adb.show();
        isShowingDialogError = true;
    }

    private void showError() {
        if (isShowingDialogError) return;
        AlertDialog.Builder adb = new AlertDialog.Builder(mActivity);
        adb.setMessage("" + getString(R.string.video_load_fail_support2));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                isShowingDialogError = false;
                mActivity.onBackPressed();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        adb.show();
        isShowingDialogError = true;
    }

    private void playVideo(String filePath) {
        if (videoView != null) {
            mViewProgress.setVisibility(View.VISIBLE);
//            videoView.stop();
//            videoView.setDataSource(filePath);
//            videoView.play();
//            videoView.setLooping(false);

            Uri videoUri = Uri.parse(filePath);
            videoView.setVideoURI(videoUri);
            videoView.setKeepScreenOn(true);
            Log.i(TAG, "play video: " + filePath);
        }
    }

    private void nextVideo(boolean canReplay) {
        if (position < listDiscoverSameTypes.size() - 1) {
            position++;
            contentDiscovery = listDiscoverSameTypes.get(position);
            initVideoDetail();
        } else {
            if (canReplay) {
                videoView.openVideo();
            }
        }
    }

    private void prevVideo(boolean canReplay) {
        if (position > 0) {
            position--;
            contentDiscovery = listDiscoverSameTypes.get(position);
            initVideoDetail();
        } else {
            if (canReplay) {
                videoView.openVideo();
            }
        }
    }

    private void initVideoDetail() {
        Log.i(TAG, "initVideoDetail");
        lastPositionVideo = 0;
        btnPlayPause.setVisibility(View.GONE);
//        ImageLoader.setImage(mApp.getApplicationContext(), contentDiscovery.getImageUrl(), mImgThumb);
        ImageLoaderManager.getInstance(mApp).setImageDiscovery(mImgThumb, contentDiscovery.getImageUrl(), width, height);
        mImgThumb.setVisibility(View.VISIBLE);
        playVideo(contentDiscovery.getVideoUrl());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mImgThumb.setTransitionName(contentDiscovery.getImageUrl());
        }
        if (parentFragment != null)
            parentFragment.updateCurrentItemClick(contentDiscovery.getPosition());
    }

    public void clear() {
        Log.i(TAG, "clear");
        if (videoView != null && videoView.isPlaying()) {
            int pos = videoView.getCurrentPosition();
            pos = pos / 1000;
            Log.i(TAG, "back: " + pos);
            mActivity.trackingEvent(gaCategory, R.string
                    .ga_action_discover_back, String.valueOf(pos));
        }


        if (itemViewClick != null) {
            float scaleX = (float) itemViewClick.getWidth() / (float) layoutDiscoveryDetail.getWidth();
            float scaleY = (float) itemViewClick.getHeight() / (float) layoutDiscoveryDetail.getHeight();
            int actionBarHeight = (int) getContext().getResources().getDimension(R.dimen.action_bar_height);
            layoutDiscoveryDetail.animate()
                    .scaleX(scaleX)
                    .scaleY(scaleY)
                    .translationXBy(itemViewClick.getX() - layoutDiscoveryDetail.getX() - layoutDiscoveryDetail
                            .getWidth() * (1 - scaleX) / 2)
                    .translationYBy(itemViewClick.getY() - layoutDiscoveryDetail.getY() - layoutDiscoveryDetail
                            .getHeight() * (1 - scaleY) / 2 + actionBarHeight)
                    .setDuration(400).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    layoutDiscoveryDetail.setVisibility(View.GONE);
                    layoutDiscoveryDetail.setScaleX(1f);
                    layoutDiscoveryDetail.setScaleY(1f);
                    layoutDiscoveryDetail.animate().setListener(null);
                    itemViewClick.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            }).start();
        } else {
            layoutDiscoveryDetail.setVisibility(View.GONE);
            layoutDiscoveryDetail.setScaleX(1f);
            layoutDiscoveryDetail.setScaleY(1f);
            layoutDiscoveryDetail.animate().setListener(null);
        }

//        if (videoView != null && videoView.getMediaPlayer() != null) {
//            videoView.stop();
//            videoView.setKeepScreenOn(false);
//        }

        if (videoView != null) {
            videoView.stopPlayback();
            videoView.setKeepScreenOn(false);
        }

        btnPlayPause.setVisibility(View.GONE);
        tvTitle.setVisibility(View.GONE);
        viewShadow.setAlpha(0);
        viewShadow.setVisibility(View.GONE);
        btnShare.setVisibility(View.GONE);
        layoutDiscoveryDetail.setPadding(0, 0, 0, 0);
        isLongClick = false;
        isShowLongClick = false;
    }

    private void actionLongClick() {
        viewShadow.setAlpha(1);
        isLongClick = true;
        isShowLongClick = true;
        //Code for long click
        layoutDiscoveryDetail.animate()
                .scaleX(0.8f)
                .scaleY(0.8f)
                .setDuration(200)
                .start();

        layoutDiscoveryDetail.setPadding(1, 1, 1, 1);

        btnShare.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.VISIBLE);
    }

    private void cancelLongClick() {
        viewShadow.setAlpha(0);
        isLongClick = false;
        isShowLongClick = false;
        handler.removeCallbacks(mLongPressed);
        btnShare.setVisibility(View.GONE);
        layoutDiscoveryDetail.animate()
                .scaleX(1)
                .scaleY(1)
                .setDuration(200)
                .start();
        layoutDiscoveryDetail.setPadding(0, 0, 0, 0);
        tvTitle.setVisibility(View.GONE);
    }

    final Handler handler = new Handler();
    final Runnable mLongPressed = new Runnable() {
        public void run() {
            if (checkIsClick(startX, startY, endX, endY)) {
                //actionLongClick();
                //TODO disable
            } else {
                isLongClick = false;
                handler.removeCallbacks(mLongPressed);
            }
        }
    };

    private boolean checkIsClick(float startX, float startY, float endX, float endY) {
        return Math.abs(startX - endX) <= 10 && Math.abs(startY - endY) <= 10;
    }

    public void setItemViewClick(View view) {
        itemViewClick = view;
    }

    public void setParentFragment(HomeDiscoveryNewFragment fragment) {
        parentFragment = fragment;
    }

    private void handleActionShare(ContentDiscovery discovery) {
        if (discovery != null) {
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_SUBJECT, discovery.getTitle());
            share.putExtra(Intent.EXTRA_TEXT, discovery.getUrlNetnews());
            startActivity(Intent.createChooser(share, discovery.getTitle()));
        } else {
            Log.e(TAG, "handleActionShare discovery == null");
        }
    }

    @Override
    public void onHomePressed() {
        if (videoView != null && videoView.isPlaying()) {
            int pos = videoView.getCurrentPosition();
            pos = pos / 1000;
            Log.i(TAG, "home press: " + pos);
            mActivity.trackingEvent(gaCategory, R.string
                    .ga_action_discover_click_home, String.valueOf(pos));
        }
    }

    @Override
    public void onHomeLongPressed() {
        Log.i(TAG, "onHomeLongPressed");
    }
}

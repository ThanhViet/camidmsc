package com.metfone.selfcare.fragment.call;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.ui.edittext.autofit.AutoFitEditText;
import com.metfone.selfcare.util.Log;

import androidx.annotation.NonNull;

/**
 * Created by toanvk2 on 8/4/2017.
 */

public class KeyBoardDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = KeyBoardDialog.class.getSimpleName();
    private BaseSlidingFragmentActivity mParentActivity;
    private View parent;
    private ApplicationController mApplication;
    private Resources mRes;
    private CheckInputAsyncTask checkInputAsyncTask;

    private ImageView mPad1, mPad2, mPad3, mPad4, mPad5, mPad6, mPad7, mPad8, mPad9, mPad0, mPadStar, mPadPound;
    private ImageView mPadVideo, mPadCall, mPadDelete;
    private ImageView mImgClose;
    private TextView mTvwAddContact, mTvwContact;
    private AutoFitEditText mTvwInputNumber;
    private String inputString = "";

    private int minDistance;
    private float downY, moveY;

    private KeyEvent keyDel, keyStar, keyPound, key0, key1, key2, key3, key4, key5, key6, key7, key8, key9, keyAdd;

    private PhoneNumber currentPhoneNumber;

    public KeyBoardDialog(BaseSlidingFragmentActivity activity, String numb) {
        super(activity, R.style.DialogFullscreenDim);
        this.mParentActivity = activity;
        mRes = mParentActivity.getResources();
        minDistance = mRes.getDimensionPixelSize(R.dimen.margin_more_content_30);
        mApplication = (ApplicationController) mParentActivity.getApplication();
        this.inputString = numb;
        this.setCancelable(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //mParentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.dialog_keyboard);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            mParentActivity.setColorIconStatusBar();
//        }
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            );
        }
        findComponentsView();
        setItemClickListener();
        initKeyEvent();
        if (inputString == null) inputString = "";
        mTvwInputNumber.setText(inputString);
        //parent.setOnTouchListener(this);
    }

    private void initKeyEvent() {
        keyDel = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL);
        keyStar = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_STAR);
        keyPound = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_POUND);
        keyAdd = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_NUMPAD_ADD);
        key0 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_0);
        key1 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_1);
        key2 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_2);
        key3 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_3);
        key4 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_4);
        key5 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_5);
        key6 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_6);
        key7 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_7);
        key8 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_8);
        key9 = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_9);
    }

    @Override
    public void dismiss() {
        Log.d(TAG, "dismiss");
        if (checkInputAsyncTask != null) {
            checkInputAsyncTask.cancel(true);
            checkInputAsyncTask = null;
        }
        //mParentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        /*if (v.getId() != R.id.menu_home_ab_close && getWindow() != null) {
            getWindow().setWindowAnimations(R.style.DialogHomeNoAnimation);
        }*/
        switch (v.getId()) {
            case R.id.dialog_keyboard_number_0:
                keyClick(key0);
                break;
            case R.id.dialog_keyboard_number_1:
                keyClick(key1);
                break;
            case R.id.dialog_keyboard_number_2:
                keyClick(key2);
                break;
            case R.id.dialog_keyboard_number_3:
                keyClick(key3);
                break;
            case R.id.dialog_keyboard_number_4:
                keyClick(key4);
                break;
            case R.id.dialog_keyboard_number_5:
                keyClick(key5);
                break;
            case R.id.dialog_keyboard_number_6:
                keyClick(key6);
                break;
            case R.id.dialog_keyboard_number_7:
                keyClick(key7);
                break;
            case R.id.dialog_keyboard_number_8:
                keyClick(key8);
                break;
            case R.id.dialog_keyboard_number_9:
                keyClick(key9);
                break;
            case R.id.dialog_keyboard_number_star:
                keyClick(keyStar);
                break;
            case R.id.dialog_keyboard_number_pound:
                keyClick(keyPound);
                break;
            case R.id.dialog_keyboard_number_delete:
                keyClick(keyDel);
                break;
            case R.id.dialog_keyboard_number_video:
                mApplication.getCallBusiness().checkAndStartCallUnknownNumber(mParentActivity,
                        inputString, true, false);
                break;
            case R.id.dialog_keyboard_number_call:
                mApplication.getCallBusiness().checkAndStartCallUnknownNumber(mParentActivity,
                        inputString, false, false);
                /*ThreadMessage threadMessage = mApplication.getMessageBusiness()
                        .findExistingOrCreateNewThread(mTvwInputNumber.getText().toString().trim());
                mApplication.getCallBusiness().handleStartCall(threadMessage, mParentActivity, true, false);*/
                break;
            case R.id.dialog_keyboard_close:
                dismiss();
                break;
            case R.id.dialog_keyboard_add_contact:
                checkNumberAndAddContact(inputString);
                break;
           /* case R.id.dialog_keyboard_test:
                dismiss();
                break;*/
        }
    }

    private void findComponentsView() {
        //find components
        //mAbClose = (ImageView) findViewById(R.id.menu_home_ab_close);
        //mAbSetting = (ImageView) findViewById(R.id.menu_home_ab_setting);
        parent = findViewById(R.id.dialog_keyboard_parent);
        mTvwInputNumber = findViewById(R.id.dialog_keyboard_input_number);
        mTvwInputNumber.setCursorVisible(false);
        mTvwInputNumber.setRawInputType(InputType.TYPE_CLASS_TEXT);
        mTvwInputNumber.setTextIsSelectable(true);


        mTvwAddContact = findViewById(R.id.dialog_keyboard_add_contact);
        mTvwContact = findViewById(R.id.dialog_keyboard_contact_number);

        mPad0 = findViewById(R.id.dialog_keyboard_number_0);
        mPad1 = findViewById(R.id.dialog_keyboard_number_1);
        mPad2 = findViewById(R.id.dialog_keyboard_number_2);
        mPad3 = findViewById(R.id.dialog_keyboard_number_3);
        mPad4 = findViewById(R.id.dialog_keyboard_number_4);
        mPad5 = findViewById(R.id.dialog_keyboard_number_5);
        mPad6 = findViewById(R.id.dialog_keyboard_number_6);
        mPad7 = findViewById(R.id.dialog_keyboard_number_7);
        mPad8 = findViewById(R.id.dialog_keyboard_number_8);
        mPad9 = findViewById(R.id.dialog_keyboard_number_9);
        mPadStar = findViewById(R.id.dialog_keyboard_number_star);
        mPadPound = findViewById(R.id.dialog_keyboard_number_pound);
        mPadVideo = findViewById(R.id.dialog_keyboard_number_video);
        mPadCall = findViewById(R.id.dialog_keyboard_number_call);
        mPadDelete = findViewById(R.id.dialog_keyboard_number_delete);

        mImgClose = findViewById(R.id.dialog_keyboard_close);
//        mImgClose.setPaintFlags(mImgClose.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvwAddContact.setVisibility(View.INVISIBLE);
        mTvwContact.setVisibility(View.INVISIBLE);
    }

    private void setItemClickListener() {
        //mAbClose.setOnClickListener(this);
        //mAbSetting.setOnClickListener(this);
        mTvwAddContact.setOnClickListener(this);
        mPad0.setOnClickListener(this);
        mPad1.setOnClickListener(this);
        mPad2.setOnClickListener(this);
        mPad3.setOnClickListener(this);
        mPad4.setOnClickListener(this);
        mPad5.setOnClickListener(this);
        mPad6.setOnClickListener(this);
        mPad7.setOnClickListener(this);
        mPad8.setOnClickListener(this);
        mPad9.setOnClickListener(this);
        mPadStar.setOnClickListener(this);
        mPadPound.setOnClickListener(this);
        mPadVideo.setOnClickListener(this);
        mPadCall.setOnClickListener(this);
        mPadDelete.setOnClickListener(this);
        mImgClose.setOnClickListener(this);

        mPad0.setOnLongClickListener(v -> {
            keyClick(keyAdd);
            return true;
        });

        mTvwInputNumber.setOnClickListener(v -> mTvwInputNumber.setCursorVisible(true));
        mTvwInputNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    int inType = mTvwInputNumber.getInputType(); // backup the input type
                    mTvwInputNumber.setInputType(InputType.TYPE_NULL); // disable soft input
                    mTvwInputNumber.onTouchEvent(event); // call native handler
                    mTvwInputNumber.setInputType(inType); // restore input type
                    return true; // consume touch even
                }
                return false;
            }
        });
        mTvwInputNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                inputString = mTvwInputNumber.getText().toString();
                if (inputString.startsWith("00")) {
                    inputString = inputString.replaceFirst("00", "+");
                }
                checkInput(inputString);

            }
        });
    }

    private void onTopToBottomSwipe() {
        Log.i(TAG, "onTopToBottomSwipe!");
        //dismiss();
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        Log.d(TAG, "onTouch: " + event.getAction() + " getX: " + event.getX() + " getY: " + event.getY());
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                downY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY = event.getY();
                if (moveY - downY >= minDistance) {
                    onTopToBottomSwipe();
                } else {
                    Log.i(TAG, "Swipe was only " + Math.abs(moveY - downY));
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void checkNumberAndAddContact(String number) {
        if (currentPhoneNumber == null) {
            mApplication.getContactBusiness().navigateToAddContact(mParentActivity, number, null);
        }
    }

    private void keyClick(KeyEvent key) {
        mTvwInputNumber.dispatchKeyEvent(key);
    }

    private void checkInput(String number) {


        if (checkInputAsyncTask != null) {
            checkInputAsyncTask.cancel(true);
        }
        if (TextUtils.isEmpty(number)) {
            mTvwAddContact.setVisibility(View.INVISIBLE);
            mTvwContact.setVisibility(View.INVISIBLE);
        } else {
            checkInputAsyncTask = new CheckInputAsyncTask();
            checkInputAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, number);
        }
    }

    private class CheckInputAsyncTask extends AsyncTask<String, Void, PhoneNumber> {
        @Override
        protected PhoneNumber doInBackground(String... params) {
            String number = params[0];
            String jidNumber;
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(mApplication.getPhoneUtil(), number, mApplication.getReengAccountBusiness().getRegionCode());
            if (phoneNumberProtocol != null) {
                jidNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            } else {
                jidNumber = number;
            }
            return mApplication.getContactBusiness().getPhoneNumberFromNumber(jidNumber);
        }

        @Override
        protected void onPostExecute(PhoneNumber phoneNumber) {
            super.onPostExecute(phoneNumber);
            currentPhoneNumber = phoneNumber;
            if (phoneNumber == null) {
                mTvwAddContact.setVisibility(View.VISIBLE);
                mTvwContact.setVisibility(View.INVISIBLE);
            } else {
                mTvwAddContact.setVisibility(View.INVISIBLE);
                mTvwContact.setVisibility(View.VISIBLE);
                mTvwContact.setText(phoneNumber.getName());
            }
        }
    }
}

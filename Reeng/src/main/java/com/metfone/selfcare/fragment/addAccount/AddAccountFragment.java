package com.metfone.selfcare.fragment.addAccount;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.AddAccountActivity;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.addAccount.AddAccountSendIdResponse;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Response;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;

public class AddAccountFragment extends Fragment implements View.OnClickListener {
    String TAG = AddAccountFragment.class.getName();
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    @BindView(R.id.etNumber)
    AppCompatEditText etNumber;
    @BindView(R.id.rll_add_service)
    RelativeLayout mRllAddService;
    @BindView(R.id.tvEnterNumber)
    CamIdTextView tvEnterNumber;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    @BindView(R.id.tvEnterNumberDesChild)
    CamIdTextView tvEnterNumberDesChild;
    @BindView(R.id.view_loading)
    FrameLayout loading;
    private OnFragmentInteractionListener onFragmentInteractionListener;
    private AddAccountActivity metFoneServiceActivity;
    private boolean isVerifyScreen;
    public static boolean isNeedRefreshAccount = false;

    public static AddAccountFragment newInstance() {
        AddAccountFragment fragment = new AddAccountFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        metFoneServiceActivity = (AddAccountActivity) context;
        onFragmentInteractionListener = (OnFragmentInteractionListener) context;
        OnBackPressedCallback callback = new OnBackPressedCallback(
                true // default to enabled
        ) {
            @Override
            public void handleOnBackPressed() {
                if (isVerifyScreen) {
                    getParentFragmentManager().popBackStack();
                } else {
                    getActivity().finish();
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(
                this, // LifecycleOwner
                callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupUI(mRllAddService);
        metFoneServiceActivity.setColorStatusBar(R.color.color_tvdelete);
        if (getActivity() != null) {
            setSystemBarTheme((getActivity()), true);
        }
    }

    @Override
    @Optional
    @OnClick({R.id.ivBack, R.id.btnContinue, R.id.tvCancel})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                if (isVerifyScreen) {
                    getParentFragmentManager().popBackStack();
                } else {
                    getActivity().finish();
                }
                break;
            case R.id.btnContinue:
                String value = etNumber.getText().toString().trim();
                if (isVerifyScreen) {
                    if (TextUtils.isEmpty(value)) {
                        ToastUtils.showToast(getContext(), getString(R.string.add_account_verify_id_id_must_be_not_empty));
                    } else {
                        verifyId(getArguments().getString(EnumUtils.OBJECT_KEY), value);
                    }
                } else {
                    if (TextUtils.isEmpty(value)) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                    } else {
                        sendId(value);
                    }
                }
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        etNumber.requestFocus();
        UIUtil.showKeyboard(getContext(), etNumber);
    }

    private void sendId(String phoneNumber) {
        metFoneServiceActivity.showLoadingDialog("", R.string.waiting);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.sendId(phoneNumber, new ApiCallback<BaseResponse<AddAccountSendIdResponse>>() {
            @Override
            public void onResponse(Response<BaseResponse<AddAccountSendIdResponse>> response) {
                metFoneServiceActivity.hideLoadingDialog();
                Log.e(TAG, "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(() -> UIUtil.showKeyboard(getContext(), etNumber), 200);
                if (response.body() != null) {
                    if (Integer.valueOf(response.body().getCode()) ==  0) {
                        onFragmentInteractionListener.displayVerifyIdFragment(phoneNumber, 0);
                    } else {
                        if ("107".equals(response.body().getMessage()))
                        {
                            ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_107));
                        }
                        else if ("108".equals(response.body().getMessage()))
                        {
                            ToastUtils.showToast(getContext(),getContext().getResources().getString( R.string.tab_mobile_ftth_expected_error_107));
                        }
                        else if ("109".equals(response.body().getMessage()))
                        {
                            ToastUtils.showToast(getContext(),getContext().getResources().getString( R.string.tab_mobile_ftth_expected_error_107));
                        }
                        else
                        {
                            ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_99));
                        }
                      //  ToastUtils.showToast(getContext(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                metFoneServiceActivity.hideLoadingDialog();
                ToastUtils.showToast(getContext(), error.getMessage());
                Log.e(TAG, "onError: ", error);
            }
        });
    }

    private void verifyId(String phoneNumber, String accountId) {
        metFoneServiceActivity.showLoadingDialog("", R.string.waiting);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.verifyId(phoneNumber, accountId, new ApiCallback<BaseResponse<AddAccountSendIdResponse>>() {
            @Override
            public void onResponse(Response<BaseResponse<AddAccountSendIdResponse>> response) {
                metFoneServiceActivity.hideLoadingDialog();
                Log.e(TAG, "onResponse: " + response.body());
                if (response.body() != null) {
                    if (Integer.valueOf(response.body().getCode()) ==  0) {
                        isNeedRefreshAccount = true;
                        if (getActivity().getIntent().getBooleanExtra(EnumUtils.IS_FROM_METFONE_TAB,false)) {
                            getActivity().setResult(EnumUtils.MY_REQUEST_CODE, new Intent());
                        } else {
                            NavigateActivityHelper.navigateToSetUpProfile(getActivity());
                        }
                        getActivity().finish();
                    } else {

                        if ("107".equals(response.body().getMessage()))
                        {
                            ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_107));
                        }
                        else if ("108".equals(response.body().getMessage()))
                        {
                            ToastUtils.showToast(getContext(),getContext().getResources().getString( R.string.tab_mobile_ftth_expected_error_107));
                        }
                        else if ("109".equals(response.body().getMessage()))
                        {
                            ToastUtils.showToast(getContext(),getContext().getResources().getString( R.string.tab_mobile_ftth_expected_error_107));
                        }
                        else
                        {
                            ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_99));
                        }
                       // ToastUtils.showToast(getContext(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                metFoneServiceActivity.hideLoadingDialog();
                if ("107".equals(error.getMessage()))
                {
                    ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_107));
                }
               else if ("108".equals(error.getMessage()))
                {
                    ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_107));
                }
                else if ("109".equals(error.getMessage()))
                {
                    ToastUtils.showToast(getContext(),getContext().getResources().getString( R.string.tab_mobile_ftth_expected_error_107));
                }
                else
                {
                    ToastUtils.showToast(getContext(), getContext().getResources().getString(R.string.tab_mobile_ftth_expected_error_99));
                }
               // ToastUtils.showToast(getContext(), error.getMessage());
                Log.e(TAG, "onError: ", error);
            }
        });
    }

    public void setupUI(View view) {
        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);

            }
        }
        if (getArguments() != null) {
            isVerifyScreen = getArguments().getBoolean(EnumUtils.IS_VERIFY_SCREEN);
            if (isVerifyScreen) {
                tvEnterNumberDes.setVisibility(View.GONE);
                tvEnterNumber.setText(getContext().getResources().getString(R.string.add_account_verify_id));
                tvEnterNumberDesChild.setText(getContext().getResources().getString(R.string.add_account_verify_id_enter_your_id_to_confirm));
                etNumber.setInputType(InputType.TYPE_CLASS_TEXT);
            } else {
                etNumber.setInputType(InputType.TYPE_CLASS_PHONE);
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void displayVerifyIdFragment(String phoneNumber, int checkPhoneCode);
    }
}
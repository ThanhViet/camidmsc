package com.metfone.selfcare.fragment.message;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.activity.PreviewImageActivity;
import com.metfone.selfcare.activity.SearchMessageActivity;
import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.AlbumProfileAdapter;
import com.metfone.selfcare.adapter.PhoneNumberAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.BlockContactBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.BlockContactModel;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.setting.hidethread.PINSettingFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.GroupAvatarHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.ThreadDetailSettingListener;
import com.metfone.selfcare.network.file.UploadListener;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogConfirmChat;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by toanvk on 7/8/2014.
 */
public class ThreadDetailSettingFragment extends Fragment implements
        ContactChangeListener,
        ConfigGroupListener,
        InitDataListener,
        ClickListener.IconListener,
        ThreadDetailSettingListener,
        View.OnClickListener {
    private final String TAG = ThreadDetailSettingFragment.class.getSimpleName();
    private SharedPreferences mPref;
    private ChatActivity mParentActivity;
    private Resources mRes;
    private ApplicationController mApplication;
    private ContactBusiness mContactBusiness;
    private MessageBusiness mMessageBusiness;
    private ClickListener.IconListener mIconListener;
    private LayoutInflater mLayoutInflater;
    private ArrayList<PhoneNumber> mListFriends;
    private Button mBtnAddFriends, mDeleteBroadCastList;
    private RelativeLayout mRltBackground, mRltSilent, mRltGroupProfile, mRltGroupPrivate, mRltBlockPerson,
            mRltCreatePoll, mRltPersonProfile, mRltButtonAddMember, mRltGroupDocument, mRltLeaveGroup,
            mRltGroupDeleteAllMsg, mRltImageTogether, mRltSearchMessages, rlHiddenThread, rlEncrypt, mRltPersonSearchMessages, personCreateGroupLayout, mRltImageTogetherPerson, lstImageShareImage;
    private ToggleButton mCbxSilent, mCbxGroupPrivate, mCbxBlockPerson, mCbxEnrypt, mCbxSilentPerson;
    private ImageView mImgBackground, mImgAvatarEdit, mImgAbBack, ivHiddenThread, mImgAbBackPerson;
    private RoundedImageView mImgAvatarPerson;
    private RoundedImageView mImgAvatar, messageSettingGroupAvatarImg, messageSettingGroupAvatarImg2, messageSettingGroupAvatarImg3;
    private RecyclerView mRecyclerView, mRecyclerImageView;
    private View mViwFooter, mViwHeader, mViewAvatar, mViewName, vCall, vCallVideo, rlBlockNoti, messageSettingHeaderGroup;
    private TextView mTvwGroupName, mTvwTitleAdmins, mTvwTitleMakeMeAdmin, mTvwShowFullMember, txtHolderNoti,
            mTvwAvatar, mTvwAvatarPerson, mTvwAddFriend, tvHiddenThread,messageSettingGroupAvatarImgText, messageSettingGroupAvatarImg2Text, messageSettingGroupAvatarImg3Text, messageSettingGroupAvatarImg4Text, txtShareImage;
    private EllipsisTextView mTvwNamePerson, mTvwStatusPerson;
    private View rlAvatarGroup;
    private LinearLayout messageSettingListAvatar, viewGroupListOption;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private PhoneNumberAdapter mAdapter;
    private AlbumProfileAdapter mAdapterImage;
    private OnFragmentInteractionListener mListener;
    private int mThreadId;
    private ThreadMessage mThreadMessage;
    private GetFriendListAsyncTask getFriendAsyncTask;
    private Handler mHandler;
    private BlockContactBusiness mBlockBusines;
    private ArrayList<String> mListImage;
    private boolean checkBlockPerson = false;
    //    private AvatarGroupUploadAsyncTask mUploadAvatarGroupAsyncTask;
    //
    private boolean isShowFullMember = false; // trang thai da click show full chua
    private boolean showTitleFullMember = false;// trang thai co hien thi title show full khong
    private boolean showTitleMakeAdminMe = false;
    private boolean isReady = false;
    private ArrayList<ReengMessage> mListMessageImage;
    private ThreadMessage mCurrentThread;
    private boolean isClickCreateGroup = false;
    private int countClick = 0;

    public static ThreadDetailSettingFragment newInstance(int threadId) {
        ThreadDetailSettingFragment fragment = new ThreadDetailSettingFragment();
        Bundle args = new Bundle();
        args.putInt(ThreadMessageConstant.THREAD_ID, threadId);
        fragment.setArguments(args);
        return fragment;
    }

    public void changeThread(ThreadMessage threadMessage) {
        mThreadMessage = threadMessage;
        mThreadId = threadMessage.getId();
    }

    public void setFragmentType() {
        if (isReady) {
            setComponentViews();
        }
    }

    public ThreadDetailSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = mParentActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        mParentActivity.setDetailSettingCallBack(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "[perform] onCreateView");
        mContactBusiness = mApplication.getContactBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ThreadMessageConstant.THREAD_ID);
            if (mThreadId != -1 && mApplication.isDataReady()) {
                mThreadMessage = mMessageBusiness.findThreadByThreadId(mThreadId);
            }
        }
        View rootView = inflater.inflate(R.layout.fragment_recycle_setting_view, container, false);

        getData(savedInstanceState);
        findComponentViews(rootView, container);
        setViewListeners();
        return rootView;
    }

    private void getData(Bundle savedInstanceState) {
        Bundle bundle = getActivity().getIntent().getExtras();
        int threadId = bundle.getInt(ThreadMessageConstant.THREAD_ID);
        mCurrentThread = mMessageBusiness.getThreadById(threadId);
        mListMessageImage = mMessageBusiness.getImageMessageOfThread(String.valueOf(threadId), false);
        mListImage = new ArrayList<>();
        for (ReengMessage message : mListMessageImage) {
            if (new File(message.getFilePath()).exists()) {
                mListImage.add(message.getFilePath());
            } else if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                mListImage.add(UrlConfigHelper.getInstance(mApplication).getDomainImage() + message.getDirectLinkMedia());
            }
        }
        // room chat thi add them image tren mem
        if (mCurrentThread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && mCurrentThread
                .getAllMessages() != null) {
            for (ReengMessage mes : mCurrentThread.getAllMessages()) {
                if (mes.getMessageType() == ReengMessageConstant.MessageType.image &&
                        mes.getFilePath() != null && !mListImage.contains(mes.getFilePath())) {
                    if (new File(mes.getFilePath()).exists()) {
                        mListImage.add(0, mes.getFilePath());
                    } else if (!TextUtils.isEmpty(mes.getDirectLinkMedia())) {
                        mListImage.add(0, UrlConfigHelper.getInstance(mApplication).getDomainImage() + mes.getDirectLinkMedia());
                    }
                }
            }
        }
        Collections.reverse(mListImage);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated: ");
        if (getArguments() != null) {
            mThreadId = getArguments().getInt(ThreadMessageConstant.THREAD_ID);
        } else if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ThreadMessageConstant.THREAD_ID);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mParentActivity = (ChatActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mBlockBusines = mApplication.getBlockContactBusiness();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        mHandler = new Handler();
        mIconListener = this;
        countClick = 0;
        ListenerHelper.getInstance().addInitDataListener(this);
        if (mApplication.isDataReady()) {
            mMessageBusiness.addConfigGroupListener(this);
            ListenerHelper.getInstance().addContactChangeListener(this);
            setComponentViews();
            isShowFullMember = false;
            //refreshListFriend();
        }
        isReady = true;
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        mMessageBusiness.removeConfigGroupListener(this);
        mIconListener = null;
        mHandler = null;
        if (getFriendAsyncTask != null) {
            getFriendAsyncTask.cancel(true);
            getFriendAsyncTask = null;
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ThreadMessageConstant.THREAD_ID, mThreadId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDataReady() {
        if (mHandler == null)
            return;
        mMessageBusiness.addConfigGroupListener(this);
        ListenerHelper.getInstance().addContactChangeListener(this);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                setComponentViews();
                isShowFullMember = false;
                //refreshListFriend();
            }
        });
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                //refreshListFriend();
                setComponentViews();
            }
        });
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
                setComponentViews();
            }
        });
    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
        if (threadMessage.getId() != mThreadMessage.getId())
            return;
        if (actionChange == Constants.MESSAGE.CHANGE_GROUP_NAME) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTvwGroupName.setText(mThreadMessage.getThreadName());
                }
            });
        } else if (actionChange == Constants.MESSAGE.CHANGE_GROUP_MEMBER) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    setComponentViews();
                    refreshListFriend();
                    mTvwGroupName.setText(mThreadMessage.getThreadName());
                }
            });
        } else if (actionChange == Constants.MESSAGE.CHANGE_GROUP_PRIVATE) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    setPrivateDetail();
                    setClassDetail();
                }
            });
        } else if (actionChange == Constants.MESSAGE.CHANGE_GROUP_AVATAR) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    setAvatarGroupViews();
                }
            });
        }
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                new LeaveGroupAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mThreadMessage);
                break;
            case Constants.MENU.DELETE_BROADCAST:
                new DeleteBroadcastAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mThreadMessage);
                break;
            case Constants.MENU.MAKE_ADMIN:
                MakeAdminGroupAsyncTask makeAdminAsyncTask = new MakeAdminGroupAsyncTask(mThreadMessage, (
                        (PhoneNumber) entry).getJidNumber(), true);
                makeAdminAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case Constants.MENU.MAKE_MEMBER:
                MakeAdminGroupAsyncTask makeMemberAsyncTask = new MakeAdminGroupAsyncTask(mThreadMessage, (
                        (PhoneNumber) entry).getJidNumber(), false);
                makeMemberAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case Constants.MENU.KICK_MEMBER:
                KickMemberGroupAsyncTask kickMemberAsyncTask = new KickMemberGroupAsyncTask(mThreadMessage, (
                        (PhoneNumber) entry).getJidNumber());
                kickMemberAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case Constants.MENU.VIEW_MEMBER_CONTACT:
                PhoneNumber phoneNumber = (PhoneNumber) entry;
                processGotoProfile(phoneNumber);
                break;
            case Constants.MENU.ADD_MEMBER_CONTACT:
                mListener.addNewContact(((PhoneNumber) entry).getJidNumber(), null);
                break;
            case Constants.MENU.REMOVE_CONTACT_BROADCAST:
                String phone = (String) entry;
                ArrayList<String> members = new ArrayList<>();
                members.add(phone);
                mMessageBusiness.removeBroadcast(members, mThreadMessage);
                mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string
                        .ga_action_click_remove_member_broadcast, R.string.ga_action_click_remove_member_broadcast);
                break;
            case Constants.MENU.CAPTURE_IMAGE:
                mListener.takePhotoChangeGroupAvatar();
                break;
            case Constants.MENU.SELECT_GALLERY:
                mListener.openGalleryChangeGroupAvatar();
                break;
            case Constants.MENU.MENU_DELETE_ALL_MSG:
                new DeleteAllMsgGroupAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mThreadMessage);
                break;
            default:
                break;
        }
    }

    @Override
    public void transferGroupAvatar(String filePath) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mParentActivity.showToast(R.string.update_fail_not_internet);
            return;
        }

        UploadListener uploadListener = new UploadListener() {
            @Override
            public void onUploadStarted(UploadRequest uploadRequest) {
                mParentActivity.showLoadingDialog("", R.string.waiting);
            }

            @Override
            public void onUploadComplete(UploadRequest uploadRequest, String response) {
                mParentActivity.hideLoadingDialog();
                try {
                    int errorCode = 0;
                    JSONObject object = new JSONObject(response);
                    if (object.has(Constants.HTTP.REST_CODE)) {
                        errorCode = object.getInt(Constants.HTTP.REST_CODE);
                    }
                    if (errorCode == 200) {
                        String time = "";
                        if (object.has("avatar_date")) {
                            time = object.getString("avatar_date");
                            mThreadMessage.setGroupAvatar(time);
                            mApplication.getMessageBusiness().updateThreadMessage(mThreadMessage);
                        }
                        onUploadAvatarGroupSuccess(time);
                    } else {
                        if (errorCode == 403) {
                            mParentActivity.showToast(R.string.e402_no_permission_admin);
                        } else {
                            mParentActivity.showToast(R.string.update_infor_fail);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "JSONException", e);
                    mParentActivity.showToast(R.string.update_infor_fail);
                }
            }

            @Override
            public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.update_infor_fail);
            }

            @Override
            public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress,
                                   long speed) {

            }
        };
        mApplication.getTransferFileBusiness().uploadAvatarGroup(mThreadMessage.getServerId(), filePath,
                uploadListener);
    }

    protected void onUploadAvatarGroupSuccess(final String result) {
        if (mParentActivity == null) {
            return;
        }
        if (!TextUtils.isEmpty(result)) {   // upload ok
            mApplication.getAvatarBusiness().loadGroupAvatarUrl(mThreadMessage, result, new
                    GlideImageLoader.ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted() {

                        }

                        @Override
                        public void onLoadingFailed(String imageUri, GlideException e) {
                            notifyChangeAvatar(result);
                        }

                        @Override
                        public void onLoadingComplete(Bitmap loadedImage) {
                            notifyChangeAvatar(result);
                        }

                    });
        }
    }

    private void notifyChangeAvatar(String groupAvatar) {
        mParentActivity.hideLoadingDialog();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String msgNotify = String.format(mApplication.getResources().getString(R.string.msg_noti_change_avatar_group),
                mApplication.getResources().getString(R.string.you));
        Date date = new Date();
        mApplication.getMessageBusiness().insertNewMessageNotifyGroup(mApplication, mThreadMessage,
                "packetId-leave-111",
                mThreadMessage.getServerId(), myNumber, msgNotify, ReengMessagePacket.SubType.groupAvatar, date
                        .getTime(), groupAvatar, MessageConstants.NOTIFY_TYPE.changeAvatar.name());
        mApplication.getMessageBusiness().notifyChangeGroupAvatar(mThreadMessage);
        GroupAvatarHelper.getInstance(mApplication).deleteBitmapCache(mThreadMessage.getId());
    }

    private void findComponentViews(View rootView, ViewGroup container) {
        mRes = mParentActivity.getResources();

        mLayoutInflater = (LayoutInflater) mParentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViwFooter = mLayoutInflater.inflate(R.layout.footer_message_setting, container, false);
        mViwHeader = mLayoutInflater.inflate(R.layout.header_detail_setting, container, false);
        mViwHeader.setOnKeyListener(new View.OnKeyListener() {

            // true if the listener has consumed the event, false otherwise.
            // the key event happens twice, when pressing and taking off.
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK) {
                    mParentActivity.checkRightSlideMenuToClose();
                    return true;
                } else {
                    return false;
                }
            }
        });
        mTvwShowFullMember = (TextView) mViwFooter.findViewById(R.id.message_setting_show_full_member);
        mBtnAddFriends = (Button) mViwHeader.findViewById(R.id.message_setting_add_friend_btn);
        mRltGroupProfile = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_group_profile);
        mRltPersonProfile = (RelativeLayout) mViwHeader.findViewById(R.id.layout_info_contact_solo);
        mRltButtonAddMember = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_layout_add_friend);
        mRltBackground = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_background_layout);
        mRltSilent = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_slient_layout);
        mRltBlockPerson = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_block_contact);
        mRltCreatePoll = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_create_poll);
        mRltGroupPrivate = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_group_private_layout);
        mRltGroupDocument = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_group_document_layout);
        mRltLeaveGroup = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_group_leave_layout);
        mRltGroupDeleteAllMsg = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_delete_all_msg);
        mRltSearchMessages = (RelativeLayout) mViwHeader.findViewById(R.id.message_searching_layout);
        mRltImageTogether = (RelativeLayout) mViwHeader.findViewById(R.id.message_setting_image_together_layout);
        mRltImageTogetherPerson = (RelativeLayout) mViwHeader.findViewById(R.id.person_message_setting_image_together_layout);
        lstImageShareImage = (RelativeLayout) mViwHeader.findViewById(R.id.lstImageShareImage);
        mCbxSilent = (ToggleButton) mViwHeader.findViewById(R.id.message_setting_slient_checkbox);
        mCbxSilentPerson = (ToggleButton) mViwHeader.findViewById(R.id.message_setting_person_checkbox);
        txtHolderNoti = (TextView) mViwHeader.findViewById(R.id.txtHolderNoti);
        mCbxGroupPrivate = (ToggleButton) mViwHeader.findViewById(R.id.message_setting_group_private_checkbox);
        mCbxBlockPerson = (ToggleButton) mViwHeader.findViewById(R.id.message_setting_block_checkbox);
        mTvwGroupName = (TextView) mViwHeader.findViewById(R.id.message_setting_group_name_text);
        mTvwTitleAdmins = (TextView) mViwHeader.findViewById(R.id.message_setting_title_admins);
        mTvwTitleMakeMeAdmin = (TextView) mViwHeader.findViewById(R.id.message_setting_title_make_admin);
        mTvwAddFriend = (TextView) mViwHeader.findViewById(R.id.tvw_add_friend);
        mImgBackground = (ImageView) mViwHeader.findViewById(R.id.message_setting_background_image);
        mViewAvatar = mViwHeader.findViewById(R.id.message_setting_group_avatar);
        mViewName = mViwHeader.findViewById(R.id.message_setting_group_name);
        mImgAvatar = (RoundedImageView) mViwHeader.findViewById(R.id.thread_avatar);
        mImgAvatarPerson = mViwHeader.findViewById(R.id.img_avatar_contact);
        mImgAvatarEdit = (ImageView) mViwHeader.findViewById(R.id.message_setting_group_avatar_edit);
        mTvwAvatar = (TextView) mViwHeader.findViewById(R.id.message_setting_group_avatar_text);
        mTvwAvatarPerson = (TextView) mViwHeader.findViewById(R.id.tvw_avatar_contact);
        mTvwNamePerson = (EllipsisTextView) mViwHeader.findViewById(R.id.tvw_contact_name);
        mTvwStatusPerson = (EllipsisTextView) mViwHeader.findViewById(R.id.tvw_contact_status);
        mImgAbBack = (ImageView) mViwHeader.findViewById(R.id.ab_back_btn);
        mImgAbBackPerson = (ImageView) mViwHeader.findViewById(R.id.person_ab_back_btn);

        mImgBackground.setImageBitmap(null);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerImageView = (RecyclerView) mViwHeader.findViewById(R.id.recycler_view_image);
        txtShareImage = (TextView) mViwHeader.findViewById(R.id.txtShareImage);
        mDeleteBroadCastList = rootView.findViewById(R.id.btn_delete_broadcast_list);
        rlHiddenThread = mViwHeader.findViewById(R.id.rlHiddenThread);
        rlHiddenThread.setVisibility(View.GONE);
        tvHiddenThread = mViwHeader.findViewById(R.id.tvHiddenThread);
        ivHiddenThread = mViwHeader.findViewById(R.id.ivHiddenThread);
        rlEncrypt = mViwHeader.findViewById(R.id.rlEncryptMsg);
        mCbxEnrypt = mViwHeader.findViewById(R.id.tgbEncryptMsg);
        messageSettingGroupAvatarImg = mViwHeader.findViewById(R.id.message_setting_group_avatar_img1);
        messageSettingGroupAvatarImg2 = mViwHeader.findViewById(R.id.message_setting_group_avatar_img2);
        messageSettingGroupAvatarImg3 = mViwHeader.findViewById(R.id.message_setting_group_avatar_img3);
        messageSettingGroupAvatarImgText = mViwHeader.findViewById(R.id.message_setting_group_avatar_img_text);
        messageSettingGroupAvatarImg2Text = mViwHeader.findViewById(R.id.message_setting_group_avatar_img2_text);
        messageSettingGroupAvatarImg3Text = mViwHeader.findViewById(R.id.message_setting_group_avatar_img3_text);
        messageSettingGroupAvatarImg4Text = mViwHeader.findViewById(R.id.message_setting_group_avatar_img4);
        messageSettingListAvatar = mViwHeader.findViewById(R.id.message_setting_list_avatar);
        rlAvatarGroup = mViwHeader.findViewById(R.id.rlAvatarGroup);
        mRltBackground.setAddStatesFromChildren(true);
        mRltBlockPerson.setAddStatesFromChildren(true);
        mRltCreatePoll.setAddStatesFromChildren(true);
//        mRltPersonProfile.setAddStatesFromChildren(true);
        mRltSilent.setAddStatesFromChildren(true);
        mRltGroupPrivate.setAddStatesFromChildren(true);
        mRltSearchMessages.setVisibility(Config.Features.FLAG_SUPPORT_SEARCH_MESSAGE ? View.VISIBLE : View.GONE);
        vCall = mViwHeader.findViewById(R.id.rlCall);
        vCallVideo = mViwHeader.findViewById(R.id.rlCallVideo);
        mRltPersonSearchMessages = (RelativeLayout) mViwHeader.findViewById(R.id.person_message_searching_layout);
        personCreateGroupLayout = (RelativeLayout) mViwHeader.findViewById(R.id.person_create_group_layout);
        rlBlockNoti = mViwHeader.findViewById(R.id.rlBlockNoti);
        messageSettingHeaderGroup = mViwHeader.findViewById(R.id.message_setting_ab_layout);
        viewGroupListOption = mViwHeader.findViewById(R.id.viewGroupListOption);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mApplication, 3);
        mRecyclerImageView.setLayoutManager(gridLayoutManager);
        mAdapterImage = new AlbumProfileAdapter(mApplication, false, true);
        mAdapterImage.setListImageString(mListImage);
        if (mListImage.size() > 0) {
            txtShareImage.setText(getActivity().getResources().getString(R.string.thread_shared_image) + "(" + mListImage.size() + ")");
        } else {
            txtShareImage.setText(getActivity().getResources().getString(R.string.thread_shared_image) + "(" + 0 + ")");
        }

        mRecyclerImageView.setAdapter(mAdapterImage);
        mAdapterImage.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
//                showImageDetail(pos);
                if (countClick != 1) {
                    Intent intentPersonLst = new Intent(mApplication, PreviewImageActivity.class);
                    intentPersonLst.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentPersonLst.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, mThreadId);
                    intentPersonLst.putExtra(PreviewImageActivity.PARAM_PRIVATE, mThreadMessage.isPrivateOrEncrypt());
                    mParentActivity.startActivity(intentPersonLst);
                    countClick = 1;
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        //rlHiddenThread.setVisibility(Config.Features.FLAG_SUPPORT_HIDE_THREAD ? View.VISIBLE : View.GONE);
        mRltImageTogetherPerson.setEnabled(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.message_setting_group_name:
                if (mThreadMessage == null) {
                    return;
                }
                if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    if (mThreadMessage.isJoined()) {
                        handleChangeThreadName(mRes.getString(R.string.title_rename_group), mThreadMessage
                                .getThreadName());
                    } else {
                        mParentActivity.showToast(mRes.getString(R.string.e416_not_allow_invite), Toast.LENGTH_LONG);
                    }
                } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                    handleChangeThreadName(mRes.getString(R.string.title_rename_broadcast), mThreadMessage
                            .getThreadName());
                }
                break;
            case R.id.message_setting_group_avatar:
                if (!mThreadMessage.isJoined() || mThreadMessage.isPrivateThread() && !mThreadMessage.isAdmin()) {
                    return;
                }
                showPopupUploadImage();
                break;
            case R.id.message_setting_background_layout:
                mParentActivity.checkRightSlideMenuToClose();
                mParentActivity.dispatchSetBackgroundIntent();
                break;
            case R.id.message_setting_slient_layout:
                boolean isChecked = !mCbxSilent.isChecked();
                mCbxSilent.setChecked(isChecked);
                SettingBusiness.getInstance(mParentActivity).updateSilentThread(mThreadMessage.getId(), !isChecked);
                break;
            case R.id.message_setting_slient_checkbox:
                SettingBusiness.getInstance(mParentActivity).
                        updateSilentThread(mThreadMessage.getId(), !mCbxSilent.isChecked());
                break;
            case R.id.message_setting_add_friend_btn:
                /*if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    ArrayList<String> listNumbers = mThreadMessage.getPhoneNumbers();
                    mListener.navigateToChooseFriendsActivity(listNumbers, mThreadMessage);
                } else*/
                if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    mListener.navigateToInvite(mThreadMessage.getServerId());
                } else {
                    ArrayList<String> listNumberGroup = mThreadMessage.getPhoneNumbers();
                    mListener.navigateToChooseFriendsActivity(listNumberGroup, mThreadMessage, ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT_SETTTING);
                }
                break;
            case R.id.message_setting_title_make_admin:
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                MakeAdminGroupAsyncTask makeAdminAsyncTask = new MakeAdminGroupAsyncTask(mThreadMessage, myNumber,
                        true);
                makeAdminAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case R.id.message_setting_show_full_member:
                isShowFullMember = true;
                refreshListFriend();
                break;
            case R.id.message_setting_group_leave_layout:
                int threadType = 0;
                if (mThreadMessage != null)
                    threadType = mThreadMessage.getThreadType();
                if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                    confirmDeleteBroadcast();
                } else {
                    confirmLeaveGroup();
                }
                break;
            case R.id.message_setting_group_private_layout:
                boolean isGroupPrivate = !mCbxGroupPrivate.isChecked();
                mCbxGroupPrivate.setChecked(isGroupPrivate);
                setPrivateGroup(isGroupPrivate);
                break;
            case R.id.message_setting_group_private_checkbox:
                setPrivateGroup(mCbxGroupPrivate.isChecked());
                break;
            case R.id.btn_delete_broadcast_list:
                showDialogConfirmDeleteBroadcastdList();
                break;

//            case R.id.layout_info_contact_solo:
//                if (mThreadMessage != null) {
//                    int mThreadType = mThreadMessage.getThreadType();
//                    if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
//                        String number = mThreadMessage.getSoloNumber();
//                        PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
//                        if (phoneNumber != null) {
//                            mListener.navigateToContactDetailActivity(phoneNumber.getId());
//                        } else {
//                            if (mThreadMessage.isStranger() && mThreadMessage.getStrangerPhoneNumber() != null) {
//                                mListener.navigateToStrangerDetail(mThreadMessage.getStrangerPhoneNumber(), null, null);
//                            } else {
//                                mListener.navigateToNonContactDetailActiviy(number);
//                            }
//                        }
//                    } /*else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
//                        mListener.navigateToOfficialDetail(mThreadMessage.getServerId());
//                    }*/
//                }
//                break;
            case R.id.message_setting_create_poll:
                mListener.navigateToPollActivity(mThreadMessage, null, null, Constants.MESSAGE.TYPE_CREATE_POLL, false);
                break;
            case R.id.message_setting_block_checkbox:
                boolean isCheckedBlock = mCbxBlockPerson.isChecked();
                if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    String type = mThreadMessage.isStranger() ? ContactRequestHelper.BLOCK_ALERT_STRANGER :
                            ContactRequestHelper.BLOCK_NON;
                    setActionBlock(mThreadMessage.getSoloNumber(), isCheckedBlock, type);
                }
                break;
            case R.id.message_setting_block_contact:
                boolean isCheckedBlockLayout = !mCbxBlockPerson.isChecked();
                mCbxBlockPerson.setChecked(isCheckedBlockLayout);
                if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    String type = mThreadMessage.isStranger() ? ContactRequestHelper.BLOCK_ALERT_STRANGER :
                            ContactRequestHelper.BLOCK_NON;
                    setActionBlock(mThreadMessage.getSoloNumber(), isCheckedBlockLayout, type);
                }
                break;
            case R.id.message_setting_group_document_layout:
                mListener.navigateToChooseDocument(mThreadMessage);
                break;
            case R.id.ab_back_btn:
                mParentActivity.checkRightSlideMenuToClose();
                break;
            case R.id.message_setting_delete_all_msg:
                confirmDeleteAllMsgGroup();
                break;
            case R.id.message_setting_image_together_layout:
                Intent intent = new Intent(mApplication, PreviewImageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, mThreadId);
                intent.putExtra(PreviewImageActivity.PARAM_PRIVATE, mThreadMessage.isPrivateOrEncrypt());
                mParentActivity.startActivity(intent);
                break;
            case R.id.person_message_setting_image_together_layout:
                mRltImageTogetherPerson.setEnabled(false);
                Intent intentPerson = new Intent(mApplication, PreviewImageActivity.class);
                intentPerson.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentPerson.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, mThreadId);
                intentPerson.putExtra(PreviewImageActivity.PARAM_PRIVATE, mThreadMessage.isPrivateOrEncrypt());
                mParentActivity.startActivity(intentPerson);
                break;
            case R.id.lstImageShareImage:
                Intent intentPersonLst = new Intent(mApplication, PreviewImageActivity.class);
                intentPersonLst.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentPersonLst.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, mThreadId);
                intentPersonLst.putExtra(PreviewImageActivity.PARAM_PRIVATE, mThreadMessage.isPrivateOrEncrypt());
                mParentActivity.startActivity(intentPersonLst);
                break;
            case R.id.person_ab_back_btn:
                mParentActivity.checkRightSlideMenuToClose();
                break;
            case R.id.message_searching_layout:
                Intent searchActivityIntent = new Intent(mApplication, SearchMessageActivity.class);
                searchActivityIntent.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, mThreadId);
                mParentActivity.startActivityForResult(searchActivityIntent, Constants.SEARCH_MESSAGE.TYPE_CREATE_SEARCH_MESSAGE);
                break;
            case R.id.person_message_searching_layout:
                Intent searchActivityIntent1 = new Intent(mApplication, SearchMessageActivity.class);
                searchActivityIntent1.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, mThreadId);
                mParentActivity.startActivityForResult(searchActivityIntent1, Constants.SEARCH_MESSAGE.TYPE_CREATE_SEARCH_MESSAGE);
                break;

            case R.id.rlHiddenThread:
                if (mThreadMessage != null) {
                    if (mThreadMessage.getHiddenThread() == 1) {
                        processUnHideThread();
                    } else {
                        String currentEncryptPIN = mApplication.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
                        processHideThread(!TextUtils.isEmpty(currentEncryptPIN));
                    }
                }
                break;
            case R.id.tgbEncryptMsg:
                boolean isCheckedEncrypt = mCbxEnrypt.isChecked();
                processEncryptMsg(isCheckedEncrypt ? 1 : 0);
                break;

            case R.id.rlEncryptMsg:
                if (mThreadMessage != null) {
                    if (mThreadMessage.isEncryptThread()) {
                        processEncryptMsg(0);
                    } else
                        processEncryptMsg(1);
                }
                break;
            case R.id.rlCall:
                mApplication.getCallBusiness().checkAndStartCall(mParentActivity, mThreadMessage.getSoloNumber());
                break;
            case R.id.person_create_group_layout:
                isClickCreateGroup = true ;
                ArrayList<String> listNumberGroup = mThreadMessage.getPhoneNumbers();
                mListener.navigateToChooseFriendsActivity(listNumberGroup, mThreadMessage, ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT_SETTTING);
                break;
            case R.id.rlCallVideo:
                mApplication.getCallBusiness().checkAndStartVideoCall(mParentActivity, mThreadMessage, false);
                break;
            case R.id.rlBlockNoti:
                boolean isChecked1 = !mCbxSilentPerson.isChecked();
                mCbxSilentPerson.setChecked(isChecked1);
                SettingBusiness.getInstance(mParentActivity).updateSilentThread(mThreadMessage.getId(), !isChecked1);
                if (!SettingBusiness.getInstance(mParentActivity).checkExistInSilentThread(mThreadId)) {
                    txtHolderNoti.setText(mParentActivity.getResources().getString(R.string.turn_on));
                } else {
                    txtHolderNoti.setText(mParentActivity.getResources().getString(R.string.turn_off));
                }
                break;
            case R.id.message_setting_person_checkbox:
                SettingBusiness.getInstance(mParentActivity).
                        updateSilentThread(mThreadMessage.getId(), !mCbxSilentPerson.isChecked());
                if (!SettingBusiness.getInstance(mParentActivity).checkExistInSilentThread(mThreadId)) {
                    txtHolderNoti.setText(mParentActivity.getResources().getString(R.string.turn_on));
                } else {
                    txtHolderNoti.setText(mParentActivity.getResources().getString(R.string.turn_off));
                }
                break;
            default:
                break;
        }
    }
    private void showDialogConfirmDeleteBroadcastdList () {
        DialogConfirmChat dialogConfirm = new DialogConfirmChat(mParentActivity, false);
        dialogConfirm.setLabel(mRes.getString(R.string.delete_msg));
        dialogConfirm.setMessage(mRes.getString(R.string.confirm_delete_msg));
        dialogConfirm.setPositiveLabel(mRes.getString(R.string.delete));
        dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                deleteBroadcastdList();
            }
        });
        dialogConfirm.show();
    }
    private void deleteBroadcastdList() {
        Log.d(TAG, "onAbDeleteClick-->");
        ThreadDetailSettingFragment.DeleteThreadListAsyncTask asyncTask = new ThreadDetailSettingFragment
                .DeleteThreadListAsyncTask();
        asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class DeleteThreadListAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
//            mHomeActivity.showLoadingDialog(mRes.getString(R.string.delete), mRes.getString(R.string.waiting));
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<ThreadMessage> listDelete = new ArrayList<>();
            listDelete.add(mThreadMessage);
            mMessageBusiness.deleteListThreadMessage(listDelete);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mParentActivity.checkRightSlideMenuToClose();
            mParentActivity.hideLoadingDialog();
            mParentActivity.onBackPressed();
            mMessageBusiness.notifyDeleteThreadMessage(-1);
            super.onPostExecute(aVoid);
        }
    }
    private void processEncryptMsg(int state) {
        if (mThreadMessage == null) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
        }
        if (mApplication.getXmppManager().isAuthenticated()) {
            mApplication.getMessageBusiness().sendMessageEnableEncrypt(state, mThreadMessage);
            mCbxEnrypt.setChecked(state == 1);
            mParentActivity.checkThreadEncrypt();
        } else {
            mParentActivity.showToast(R.string.e604_error_connect_server);
            mCbxEnrypt.setChecked(state != 1);
        }


    }

    private void processUnHideThread() {
        if (mThreadMessage == null) return;
        DialogConfirm dialogConfirm = new DialogConfirm(mParentActivity, true);
        dialogConfirm.setLabel(mParentActivity.getString(R.string.confirm));
        dialogConfirm.setMessage(String.format(mParentActivity.getString(R.string.msg_popup_unhide_thread_with), mThreadMessage.getThreadName()));
        dialogConfirm.setPositiveLabel(mParentActivity.getString(R.string.ok));
        dialogConfirm.setNegativeLabel(mParentActivity.getString(R.string.cancel));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                mThreadMessage.setHiddenThread(0);
                mApplication.getMessageBusiness().updateThreadMessage(mThreadMessage);
                setViewHiddenThread(false);
            }
        });
        dialogConfirm.show();
    }

    private void setViewHiddenThread(boolean isHidden) {
        if (isHidden) {
            tvHiddenThread.setText(mParentActivity.getString(R.string.unhide_thread));
            ivHiddenThread.setImageResource(R.drawable.ic_chat_setting_show_thread);
        } else {
            ivHiddenThread.setImageResource(R.drawable.ic_chat_setting_hidden_thread);
            tvHiddenThread.setText(mParentActivity.getString(R.string.hide_thread_chat));
        }
    }


    private void processHideThread(boolean hasPIN) {

        Intent intent = new Intent(mParentActivity, SettingActivity.class);
        if (hasPIN) {
            intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_HIDE_THREAD);
            mParentActivity.startActivity(intent, true);
        } else {
            DialogConfirm dialogConfirm = new DialogConfirm(mParentActivity, true);
            dialogConfirm.setMessage(mParentActivity.getString(R.string.msg_popup_hide_thread_chat));
            dialogConfirm.setPositiveLabel(mParentActivity.getString(R.string.set_pin));
            dialogConfirm.setLabel(mParentActivity.getString(R.string.hide_thread_chat));
            dialogConfirm.setNegativeLabel(mParentActivity.getString(R.string.cancel));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    Intent intent = new Intent(mParentActivity, SettingActivity.class);
                    intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_CREATE_PIN_HIDE_THREAD);
                    mParentActivity.startActivity(intent, true);
                }
            });
            dialogConfirm.show();

        }

    }

    private void setViewListeners() {
        mViewName.setOnClickListener(this);
        mViewAvatar.setOnClickListener(this);
        mRltBackground.setOnClickListener(this);
//        mRltPersonProfile.setOnClickListener(this);
        mRltCreatePoll.setOnClickListener(this);
        mRltBlockPerson.setOnClickListener(this);
        mRltSilent.setOnClickListener(this);
        mCbxSilent.setOnClickListener(this);
        mCbxSilentPerson.setOnClickListener(this);
        mBtnAddFriends.setOnClickListener(this);
        mTvwTitleMakeMeAdmin.setOnClickListener(this);
        mTvwShowFullMember.setOnClickListener(this);
        mRltLeaveGroup.setOnClickListener(this);
        mRltGroupDeleteAllMsg.setOnClickListener(this);
        mRltImageTogether.setOnClickListener(this);
        mRltImageTogetherPerson.setOnClickListener(this);
        lstImageShareImage.setOnClickListener(this);
        mRltSearchMessages.setOnClickListener(this);
        mRltGroupPrivate.setOnClickListener(this);
        mCbxGroupPrivate.setOnClickListener(this);
        mCbxBlockPerson.setOnClickListener(this);
        mRltGroupDocument.setOnClickListener(this);
        mImgAbBack.setOnClickListener(this);
        mImgAbBackPerson.setOnClickListener(this);
        rlHiddenThread.setOnClickListener(this);
        rlEncrypt.setOnClickListener(this);
        mCbxEnrypt.setOnClickListener(this);
        vCall.setOnClickListener(this);
        mRltPersonSearchMessages.setOnClickListener(this);
        personCreateGroupLayout.setOnClickListener(this);
        vCallVideo.setOnClickListener(this);
        rlBlockNoti.setOnClickListener(this);
        mDeleteBroadCastList.setOnClickListener(this);

    }

    private void setComponentViews() {
        mContactBusiness = mApplication.getContactBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        if (mThreadMessage == null) {// vua xoa thread thi notifi data change
            //mChatActivity.onBackPressed();
            return;
        }
        Log.d(TAG, "setComponentViews setting");
        /*if (mThreadMessage.getThreadType() != ThreadMessageConstant
                .TYPE_THREAD_BROADCAST_CHAT) {
            drawViewSetting();
        } else {
            drawViewManager();
            refreshListFriend();
        }*/
        isShowFullMember = false;
        drawView();
        getData(null);
        mAdapterImage.setListImageString(mListImage);
        if (mListImage.size() > 0) {
            txtShareImage.setText(getActivity().getResources().getString(R.string.thread_shared_image) + "(" + mListImage.size() + ")");
        } else {
            txtShareImage.setText(getActivity().getResources().getString(R.string.thread_shared_image) + "(" + 0 + ")");
        }
        mRecyclerImageView.setAdapter(mAdapterImage);
        mAdapterImage.notifyDataSetChanged();
    }

    private void setActionBlock(final String friendPhoneNumber, final boolean isBlock, String type) {
        ArrayList<BlockContactModel> blockList = new ArrayList<>();
        blockList.add(new BlockContactModel(friendPhoneNumber, isBlock ? 1 : 0));
        mParentActivity.showLoadingDialog("", mRes.getString(R.string.processing), true);
        ContactRequestHelper.getInstance(mApplication).handleBlockNumbers(blockList,
                new ContactRequestHelper.onResponseBlockContact() {
                    @Override
                    public void onResponse(String response) {
                        mParentActivity.hideLoadingDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                String msg;
                                if (isBlock) {
                                    msg = String.format(mRes.getString(R.string.msg_block_success), mThreadMessage
                                            .getThreadName());
                                    mBlockBusines.addBlockNumber(friendPhoneNumber);
                                    checkBlockPerson = true;
                                } else {
                                    msg = String.format(mRes.getString(R.string.msg_unblock_success), mThreadMessage
                                            .getThreadName());
                                    mBlockBusines.removeBlockNumber(friendPhoneNumber);
                                    checkBlockPerson = false;
                                }
                                ReengMessage message = mMessageBusiness.insertMessageNotify(msg,
                                        friendPhoneNumber, mApplication.getReengAccountBusiness().getJidNumber(),
                                        mThreadMessage, ReengMessageConstant.READ_STATE_READ, TimeHelper
                                                .getCurrentTime());
                                mMessageBusiness.notifyNewMessage(message, mThreadMessage);
                                drawActionBlock(friendPhoneNumber);
                            } else {
                                String msg = mParentActivity.getResources().getString(R.string
                                        .e601_error_but_undefined);
                                mParentActivity.showError(msg, null);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception", e);
                            String msg = mParentActivity.getResources().getString(R.string.e601_error_but_undefined);
                            mParentActivity.showError(msg, null);
                        }

                        //mChatActivity.showToast(msg, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        String msg;
                        if (errorCode == -2) {
                            msg = mRes.getString(R.string.error_internet_disconnect);
                        } else {
                            msg = mRes.getString(R.string.e601_error_but_undefined);
                        }
                        mCbxBlockPerson.setChecked(!isBlock);
                        mParentActivity.showError(msg, null);
                    }
                });
    }

    private void drawActionBlock(String friendPhoneNumber) {
        if (mApplication.getBlockContactBusiness().isBlockNumber(friendPhoneNumber)) {
//            mTvwAlertStrangerBlock.setText(mRes.getString(R.string.unblock));
        } else {
//            mTvwAlertStrangerBlock.setText(mRes.getString(R.string.block));
        }
    }

    private void drawView() {
        mCbxSilent.setChecked(!SettingBusiness.getInstance(mParentActivity).checkExistInSilentThread(mThreadId));
        mCbxSilentPerson.setChecked(!SettingBusiness.getInstance(mParentActivity).checkExistInSilentThread(mThreadId));
        if (!SettingBusiness.getInstance(mParentActivity).checkExistInSilentThread(mThreadId)) {
            txtHolderNoti.setText(mParentActivity.getResources().getString(R.string.turn_on));
        } else {
            txtHolderNoti.setText(mParentActivity.getResources().getString(R.string.turn_off));
        }
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            setDetailViewPerson();
            drawThumbBackground();
        } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            setDetailViewGroup();
            drawThumbBackground();
        } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            setDetailViewOfficial();
            drawThumbBackground();
        } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            setDetailViewRoom();
        } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            mViewAvatar.setVisibility(View.VISIBLE);
            setDetailViewBroadcast();
            drawThumbBackground();
        } else {
            Log.e(TAG, "invalid type thread chat: " + mThreadMessage.getThreadType());
            mRltGroupProfile.setVisibility(View.GONE);
            viewGroupListOption.setVisibility(View.GONE);
            mRltPersonProfile.setVisibility(View.GONE);
            messageSettingHeaderGroup.setVisibility(View.GONE);
            mRltCreatePoll.setVisibility(View.GONE);
            mRltSilent.setVisibility(View.GONE);
            mRltBackground.setVisibility(View.GONE);
            mRltBlockPerson.setVisibility(View.GONE);
            mRltGroupPrivate.setVisibility(View.GONE);
            mRltGroupDocument.setVisibility(View.GONE);
            mRltButtonAddMember.setVisibility(View.GONE);
            mTvwTitleAdmins.setVisibility(View.GONE);
            mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
            mViwFooter.setVisibility(View.GONE);
            mRltLeaveGroup.setVisibility(View.GONE);
            mRltGroupDeleteAllMsg.setVisibility(View.GONE);
            mRltImageTogether.setVisibility(View.GONE);
            mRltImageTogetherPerson.setVisibility(View.GONE);
            rlEncrypt.setVisibility(View.GONE);
            setAdapter(new ArrayList<PhoneNumber>());
        }
        if (mThreadMessage.getHiddenThread() == 1) {
            setViewHiddenThread(true);
        } else
            setViewHiddenThread(false);
    }

    private void setDetailViewPerson() {
        mRltGroupProfile.setVisibility(View.GONE);
        viewGroupListOption.setVisibility(View.GONE);
        mRltPersonProfile.setVisibility(View.VISIBLE);
        messageSettingHeaderGroup.setVisibility(View.GONE);
        mTvwAddFriend.setText(mRes.getString(R.string.add_friend_chat_solo));
        mRltCreatePoll.setVisibility(View.GONE);
        mRltSilent.setVisibility(View.VISIBLE);
        mRltBackground.setVisibility(View.GONE);
        mRltBlockPerson.setVisibility(View.VISIBLE);
        mRltGroupPrivate.setVisibility(View.GONE);
        mRltGroupDocument.setVisibility(View.GONE);
        mTvwTitleAdmins.setVisibility(View.GONE);
        mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
        mViwFooter.setVisibility(View.GONE);
        mRltLeaveGroup.setVisibility(View.GONE);
        mRltGroupDeleteAllMsg.setVisibility(View.GONE);
        mRltImageTogether.setVisibility(View.VISIBLE);
        mRltImageTogetherPerson.setVisibility(View.VISIBLE);
        rlEncrypt.setVisibility(View.GONE);
        messageSettingListAvatar.setVisibility(View.GONE);
        mCbxBlockPerson.setChecked(mApplication.getBlockContactBusiness().
                isBlockNumber(mThreadMessage.getSoloNumber()));
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        String preKey = mApplication.getReengAccountBusiness().getCurrentAccount().getPreKey();
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(mThreadMessage
                .getSoloNumber());
        if (phoneNumber != null) {

            if (mApplication.getReengAccountBusiness().isE2eEnable() && phoneNumber.isReeng()) {
                if (!TextUtils.isEmpty(preKey) && !TextUtils.isEmpty(phoneNumber.getPreKey())) {
                    rlEncrypt.setVisibility(View.VISIBLE);
                    mCbxEnrypt.setChecked(mThreadMessage.isEncryptThread());
                } else
                    rlEncrypt.setVisibility(View.GONE);
            } else
                rlEncrypt.setVisibility(View.GONE);


            String stt = phoneNumber.getStatus();
            if (stt == null || TextUtils.isEmpty(stt)) {
                mTvwStatusPerson.setText(mRes.getString(R.string.setting_profile));
            } else {
                mTvwStatusPerson.setEmoticon(mApplication, stt, phoneNumber.getIdInt(), phoneNumber);
            }
            mTvwNamePerson.setText(phoneNumber.getName());
            mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatarPerson, mTvwAvatarPerson, phoneNumber,
                    size);
            if (phoneNumber.isReeng()) {
                mRltButtonAddMember.setVisibility(View.GONE);
            } else {
                mRltButtonAddMember.setVisibility(View.GONE);
            }
        } else {
            // so chua luu danh ba, thread lam quen, hien ten stranger
            String friendName, stt = "N/A";
            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(mThreadMessage
                    .getSoloNumber());
            if (mThreadMessage.isStranger()) {
                StrangerPhoneNumber strangerPhoneNumber = mThreadMessage.getStrangerPhoneNumber();
                if (strangerPhoneNumber != null) {
                    friendName = strangerPhoneNumber.getFriendName();
                    mApplication.getAvatarBusiness().
                            setStrangerAvatar(mImgAvatarPerson, mTvwAvatarPerson, strangerPhoneNumber,
                                    mThreadMessage.getSoloNumber(), friendName, null, size);
                } else {
                    friendName = mThreadMessage.getSoloNumber();
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatarPerson,
                            mTvwAvatarPerson, mThreadMessage.getSoloNumber(), size);
                }
                mRltButtonAddMember.setVisibility(View.GONE);
            } else {
                if (nonContact != null) {
                    friendName = nonContact.getNickName();
                    stt = nonContact.getStatus();
                    if (nonContact.isReeng()) {
                        mRltButtonAddMember.setVisibility(View.GONE);
                    } else {
                        mRltButtonAddMember.setVisibility(View.GONE);
                    }

                    if (mApplication.getReengAccountBusiness().isE2eEnable() && nonContact.isReeng()) {
                        if (!TextUtils.isEmpty(preKey) && !TextUtils.isEmpty(nonContact.getPreKey())) {
                            rlEncrypt.setVisibility(View.VISIBLE);
                            mCbxEnrypt.setChecked(mThreadMessage.isEncryptThread());
                        } else
                            rlEncrypt.setVisibility(View.GONE);
                    } else
                        rlEncrypt.setVisibility(View.GONE);
                } else {
                    friendName = mThreadMessage.getSoloNumber();
                    mRltButtonAddMember.setVisibility(View.GONE);
                }
                mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatarPerson,
                        mTvwAvatarPerson, mThreadMessage.getSoloNumber(), size);
            }
            if (nonContact != null) {
                stt = nonContact.getStatus();
            }
            if (friendName == null) friendName = mThreadMessage.getThreadName();
            if (stt == null || TextUtils.isEmpty(stt)) {
                mTvwStatusPerson.setText(mRes.getString(R.string.setting_profile));
            } else {
                mTvwStatusPerson.setEmoticon(mApplication, stt, stt.hashCode(), stt);
            }
            mTvwNamePerson.setText(friendName);
        }
        setAdapter(new ArrayList<PhoneNumber>());
    }

    private void setDetailViewGroup() {
        mRltGroupProfile.setVisibility(View.VISIBLE);
        viewGroupListOption.setVisibility(View.VISIBLE);
        mRltPersonProfile.setVisibility(View.GONE);
        messageSettingHeaderGroup.setVisibility(View.VISIBLE);
        mRltCreatePoll.setVisibility(View.VISIBLE);
        mRltSilent.setVisibility(View.VISIBLE);
        mRltBackground.setVisibility(View.GONE);
        mRltBlockPerson.setVisibility(View.GONE);
        mRltButtonAddMember.setVisibility(View.GONE);
        mRltGroupDeleteAllMsg.setVisibility(View.VISIBLE);
        mRltImageTogether.setVisibility(View.VISIBLE);
        mRltImageTogetherPerson.setVisibility(View.VISIBLE);
        mViewAvatar.setVisibility(View.GONE);
        setPrivateDetail();
        setClassDetail();
        mTvwShowFullMember.setText(R.string.show_full_member);
        mTvwGroupName.setText(mThreadMessage.getThreadName());
        mBtnAddFriends.setText(mRes.getString(R.string.add_friend_chat_group));
        // set group avatar
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        avatarBusiness.setGroupThreadAvatar(mImgAvatar, rlAvatarGroup, mThreadMessage);
        if (mThreadMessage.isJoined()) {
            mTvwTitleAdmins.setVisibility(View.VISIBLE);
            mTvwTitleAdmins.setText(getTitleAdmin());
            mRltLeaveGroup.setVisibility(View.VISIBLE);
            mBtnAddFriends.setEnabled(true);
            drawStateAdmin();
        } else {
            mTvwTitleAdmins.setVisibility(View.GONE);
            mRltLeaveGroup.setVisibility(View.GONE);
            mTvwShowFullMember.setVisibility(View.GONE);
            mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
            mBtnAddFriends.setEnabled(false);
        }
        mViwFooter.setVisibility(View.VISIBLE);
        setAdapter(new ArrayList<PhoneNumber>());
        refreshListFriend();
    }

    private String getTitleAdmin() {
        if (mThreadMessage.getAdminNumbers() != null)
            return mRes.getString(R.string.title_admin_list) + " (" + mThreadMessage.getAdminNumbers()
                    .size() + ")";
        else
            return mRes.getString(R.string.title_admin_list);
    }

    private String getTitleMember() {
        int sizeMember = 0;
        if (mThreadMessage.getAdminNumbers() != null)
            sizeMember = mThreadMessage.getAdminNumbers().size();

        return mRes.getString(R.string.friend_list) + " (" + (mThreadMessage.getPhoneNumbers().size() - sizeMember + 1) + ")"; //+1 la chinh minh
    }

    private void setPrivateDetail() {
        if (mThreadMessage.isAdmin()) {
            mRltGroupPrivate.setVisibility(View.VISIBLE);
            mCbxGroupPrivate.setChecked(mThreadMessage.isPrivateThread());
            mImgAvatarEdit.setVisibility(View.VISIBLE);
        } else {
            mRltGroupPrivate.setVisibility(View.GONE);
            if (mThreadMessage.isPrivateThread()) {
                mImgAvatarEdit.setVisibility(View.GONE);
            } else {
                mImgAvatarEdit.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setClassDetail() {
        /*if (mThreadMessage.getGroupClass() == ThreadMessageConstant.GROUP_TYPE_CLASS) {
            mRltGroupDocument.setVisibility(View.VISIBLE);
        } else {*/
        mRltGroupDocument.setVisibility(View.GONE);
//        }
    }

    private void setDetailViewOfficial() {
        mRltGroupProfile.setVisibility(View.GONE);
        viewGroupListOption.setVisibility(View.GONE);
        mRltPersonProfile.setVisibility(View.VISIBLE);
        messageSettingHeaderGroup.setVisibility(View.GONE);
        mRltCreatePoll.setVisibility(View.GONE);
        mRltSilent.setVisibility(View.VISIBLE);
        mRltBackground.setVisibility(View.GONE);
        mRltBlockPerson.setVisibility(View.GONE);
        mRltGroupPrivate.setVisibility(View.GONE);
        mRltGroupDocument.setVisibility(View.GONE);
        mRltButtonAddMember.setVisibility(View.GONE);
        mTvwTitleAdmins.setVisibility(View.GONE);
        mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
        mViwFooter.setVisibility(View.GONE);
        mRltLeaveGroup.setVisibility(View.GONE);
        mRltGroupDeleteAllMsg.setVisibility(View.GONE);
        mRltImageTogether.setVisibility(View.GONE);
        mRltImageTogetherPerson.setVisibility(View.GONE);
        messageSettingListAvatar.setVisibility(View.GONE);
        mTvwNamePerson.setText(mThreadMessage.getThreadName());
        mTvwStatusPerson.setText(mRes.getString(R.string.menu_officials));
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        OfficerAccount accountRoomChat = mApplication.getOfficerBusiness().
                getOfficerAccountByServerId(mThreadMessage.getServerId());
        mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatarPerson, size, accountRoomChat.getServerId(),
                accountRoomChat, false);
        setAdapter(new ArrayList<PhoneNumber>());
    }

    private void setDetailViewRoom() {
        mRltGroupProfile.setVisibility(View.GONE);
        viewGroupListOption.setVisibility(View.GONE);
        mRltPersonProfile.setVisibility(View.VISIBLE);
        messageSettingHeaderGroup.setVisibility(View.GONE);
        mRltButtonAddMember.setVisibility(View.GONE);
        mTvwAddFriend.setText(mRes.getString(R.string.add_friend_chat_group));
        mBtnAddFriends.setText(mRes.getString(R.string.add_friend_to_room));
        mRltCreatePoll.setVisibility(View.GONE);
        mRltSilent.setVisibility(View.VISIBLE);
        mRltBackground.setVisibility(View.GONE);
        mRltBlockPerson.setVisibility(View.GONE);
        mRltGroupPrivate.setVisibility(View.GONE);
        mRltGroupDocument.setVisibility(View.GONE);
        mTvwTitleAdmins.setVisibility(View.GONE);
        mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
        mViwFooter.setVisibility(View.GONE);
        mRltLeaveGroup.setVisibility(View.GONE);
        mRltGroupDeleteAllMsg.setVisibility(View.GONE);
        mRltImageTogether.setVisibility(View.GONE);
        mRltImageTogetherPerson.setVisibility(View.GONE);

        mTvwNamePerson.setText(mThreadMessage.getThreadName());
        mTvwStatusPerson.setText(mRes.getString(R.string.music_room));
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        OfficerAccount accountRoomChat = mApplication.getOfficerBusiness().
                getOfficerAccountByServerId(mThreadMessage.getServerId());
        mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatarPerson, size, accountRoomChat.getServerId(),
                accountRoomChat, false);
        setAdapter(new ArrayList<PhoneNumber>());
        refreshListFriend();
    }

    private void setDetailViewBroadcast() {
        mRltGroupProfile.setVisibility(View.VISIBLE);
        viewGroupListOption.setVisibility(View.VISIBLE);
        mRltPersonProfile.setVisibility(View.GONE);
        messageSettingHeaderGroup.setVisibility(View.VISIBLE);
        mRltButtonAddMember.setVisibility(View.VISIBLE);
        mTvwAddFriend.setText(mRes.getString(R.string.add_friend_chat_group));
        mBtnAddFriends.setText(mRes.getString(R.string.add_friend_chat_group));
        mRltCreatePoll.setVisibility(View.GONE);
        mRltSilent.setVisibility(View.GONE);
        mRltBackground.setVisibility(View.GONE);
        mRltBlockPerson.setVisibility(View.GONE);
        mRltGroupPrivate.setVisibility(View.GONE);
        mRltGroupDocument.setVisibility(View.GONE);
        mTvwTitleAdmins.setVisibility(View.GONE);
        mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
        mViwFooter.setVisibility(View.VISIBLE);
        mRltLeaveGroup.setVisibility(View.GONE);
        mRltGroupDeleteAllMsg.setVisibility(View.GONE);
        mRltImageTogether.setVisibility(View.GONE);
        mRltImageTogetherPerson.setVisibility(View.GONE);
        rlHiddenThread.setVisibility(View.GONE);
        mImgAvatarEdit.setVisibility(View.GONE);
        mTvwGroupName.setText(mThreadMessage.getThreadName());
        mImgAvatar.setImageResource(R.drawable.chat_ic_broadcast_group);
        mTvwAvatar.setVisibility(View.GONE);
        mRltSearchMessages.setVisibility(View.GONE);
        messageSettingListAvatar.setVisibility(View.GONE);
        mDeleteBroadCastList.setVisibility(View.VISIBLE);
        setAdapter(mListFriends);
        refreshListFriend();
    }

    private void setAvatarGroupViews() {
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
            avatarBusiness.setGroupThreadAvatar(mImgAvatar, rlAvatarGroup, mThreadMessage);
        }
    }

    private void drawThumbBackground() {
        //TODO giao dien moi bo view nay
        /*String background = mThreadMessage.getBackground();
        if (TextUtils.isEmpty(background)) {
            if (mPref.getBoolean(Constants.PREFERENCE.PREF_APPLY_BACKGROUND_ALL, false)) {
                background = mPref.getString(Constants.PREFERENCE.PREF_DEFAULT_BACKGROUND_PATH, null);
            }
        }
        ImageLoaderManager.getInstance(mParentActivity).displayThumbBackgroundOfThreadDetail(background,
                mImgBackground);*/
    }

    private void setAdapter(ArrayList<PhoneNumber> listNumbers) {
        if (listNumbers == null || listNumbers.isEmpty())
            listNumbers = new ArrayList<>();
        if (mAdapter == null) {
            mAdapter = new PhoneNumberAdapter(mApplication, listNumbers, null, Constants.CONTACT
                    .CONTACT_VIEW_MEMBER_GROUP);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
            RecyclerViewUtils.setHeaderView(mRecyclerView, mViwHeader);
            RecyclerViewUtils.setFooterView(mRecyclerView, mViwFooter);
            setItemListViewListener();
        } else {
            mAdapter.setListPhoneNumbers(listNumbers);
            mAdapter.notifyDataSetChanged();
        }
        if (listNumbers.size() == 0 ||
                mThreadMessage == null ||
                mThreadMessage.getAdminNumbers() == null ||
                mThreadMessage.getAdminNumbers().isEmpty() ||
                mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            mTvwTitleAdmins.setVisibility(View.GONE);
        } else {
            mTvwTitleAdmins.setVisibility(View.VISIBLE);
        }
    }

    private void drawStateAdmin() {
        if (showTitleMakeAdminMe) {
            mTvwTitleMakeMeAdmin.setVisibility(View.VISIBLE);
        } else {
            mTvwTitleMakeMeAdmin.setVisibility(View.GONE);
        }
        if (showTitleFullMember) {
            mTvwShowFullMember.setVisibility(View.VISIBLE);
        } else {
            mTvwShowFullMember.setVisibility(View.GONE);
        }
    }

    private void showPopupUploadImage() {
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu selectGallery = new ItemContextMenu(mRes.getString(R.string
                .select_from_gallery),
                -1, null, Constants.MENU.SELECT_GALLERY);
        ItemContextMenu capture = new ItemContextMenu(mRes.getString(R.string.capture_image),
                -1, null, Constants.MENU.CAPTURE_IMAGE);
        listItem.add(capture);
        listItem.add(selectGallery);
        PopupHelper.getInstance().showContextMenu(mParentActivity, null, listItem, this);
    }

    private void showPopupContextMenu(PhoneNumber phoneNumber) {
        ArrayList<String> admins = mThreadMessage.getAdminNumbers();
        String jidNumber = phoneNumber.getJidNumber();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu makeAdminItem = new ItemContextMenu(mRes.getString(R.string.make_admin), -1,
                phoneNumber, Constants.MENU.MAKE_ADMIN);
        ItemContextMenu makeMemberItem = new ItemContextMenu(mRes.getString(R.string.make_member), -1,
                phoneNumber, Constants.MENU.MAKE_MEMBER);
//        ItemContextMenu viewProfileItem = new ItemContextMenu(mRes.getString(R.string.view_profile),
//                -1,
//                phoneNumber, Constants.MENU.VIEW_MEMBER_CONTACT);
        ItemContextMenu kickMemberItem = new ItemContextMenu(Html.fromHtml(mRes.getString(R.string.kick_member)), -1,
                phoneNumber, Constants.MENU.KICK_MEMBER);
        ItemContextMenu removeMemberItem = new ItemContextMenu(mRes.getString(R.string
                .title_remove_member_broadcast), -1,
                phoneNumber.getJidNumber(), Constants.MENU.REMOVE_CONTACT_BROADCAST);
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            if ((!TextUtils.isEmpty(jidNumber) && !TextUtils.isEmpty(myNumber) && jidNumber.equals(myNumber))
                    || phoneNumber.getId() != null) {
              //  listMenu.add(viewProfileItem);
            }
            if (!TextUtils.isEmpty(jidNumber) && !TextUtils.isEmpty(myNumber) && !jidNumber.equals(myNumber)) {
                listMenu.add(removeMemberItem);
            }
        } else {
          //  listMenu.add(viewProfileItem);
            if (admins.contains(jidNumber)) {// so dang la admin
                listMenu.add(makeMemberItem);
            } else {
                listMenu.add(makeAdminItem);
            }
            if (!jidNumber.equals(myNumber)) {
                listMenu.add(kickMemberItem);
            }
        }
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mParentActivity,
                    null, listMenu, this);
        }
    }

    private void setItemListViewListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                PhoneNumber phoneNumber = (PhoneNumber) object;
                //if (phoneNumber == null)
                if (mThreadMessage == null || phoneNumber == null || phoneNumber.getContactId() == null)//title
                    return;
                if (mThreadMessage.isAdmin()) {
                    showPopupContextMenu(phoneNumber);
                } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                    showPopupContextMenu(phoneNumber);
                } else {
                    processGotoProfile(phoneNumber);
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {
                PhoneNumber phoneNumber = (PhoneNumber) object;
                if (mThreadMessage == null || phoneNumber == null || phoneNumber.getContactId() == null)//title
                    return;
                int threadType = mThreadMessage.getThreadType();
                if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                    showPopupContextMenu(phoneNumber);
                }
            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void setPrivateGroup(boolean isPrivate) {
        new PrivateGroupAsyncTask(mThreadMessage, isPrivate).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void confirmDeleteAllMsgGroup() {
        Log.i(TAG, "confirmDeleteAllMsgGroup");
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.delete_all_msg_in_group);
        String msg = mRes.getString(R.string.msg_delete_all_msg_in_group);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, mIconListener, null, Constants.MENU.MENU_DELETE_ALL_MSG);
    }

    private void confirmLeaveGroup() {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_leave_groupchat);
        String msg = mRes.getString(R.string.msg_leave_group);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, mIconListener, null, Constants.MENU.POPUP_YES);
    }

    private void confirmDeleteBroadcast() {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_delete_broadcast);
        String msg = mRes.getString(R.string.msg_delete_broadcast);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, mIconListener, null, Constants.MENU.DELETE_BROADCAST);
    }

    private void refreshListFriend() {
        if (mThreadMessage == null) {
            return;
        }
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            mTvwTitleAdmins.setVisibility(View.VISIBLE);
            mTvwTitleAdmins.setText(getTitleAdmin());
            if (getFriendAsyncTask != null) {
                getFriendAsyncTask.cancel(true);
                getFriendAsyncTask = null;
            }
            getFriendAsyncTask = new GetFriendListAsyncTask(isShowFullMember);
            getFriendAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            mTvwTitleAdmins.setVisibility(View.GONE);
            if (getFriendAsyncTask != null) {
                getFriendAsyncTask.cancel(true);
                getFriendAsyncTask = null;
            }
            getFriendAsyncTask = new GetFriendListAsyncTask(isShowFullMember);
            getFriendAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class GetFriendListAsyncTask extends AsyncTask<Void, Void, ArrayList<PhoneNumber>> {
        private boolean isShowFull = false;

        public GetFriendListAsyncTask(boolean isShowFull) {
            Log.d(TAG, "GetFriendListAsyncTask: " + isShowFull);
            this.isShowFull = isShowFull;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList doInBackground(Void... params) {
            //        mThreadMessage.set
            long begin = System.currentTimeMillis();
            ArrayList<PhoneNumber> listAdminAndMembers = getGroupFriendList(isShowFull);
            Log.d(TAG, "GetFriendListAsynctask take: " + (System.currentTimeMillis() - begin));
            return listAdminAndMembers;
        }

        @Override
        protected void onPostExecute(ArrayList params) {
            super.onPostExecute(params);
            mListFriends = params;
            setAdapter(mListFriends);
            drawStateAdmin();
            Log.d(TAG, "GetFriendListAsyncTask onPostExecute setting");
//            mListener.notifyGetListMemberThread(mListFriends);

        }
    }

    private ArrayList<PhoneNumber> getGroupFriendList(boolean isLoadFull) {
        if (mThreadMessage == null) {
            return new ArrayList<>();
        }
        if (!mThreadMessage.isJoined()) {
            showTitleMakeAdminMe = false;
            showTitleFullMember = false;
            return new ArrayList<>();
        }
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<PhoneNumber> listAdminAndMembers = new ArrayList<>();
        ArrayList<String> adminNumbers = mThreadMessage.getAdminNumbers();
        ArrayList<String> phoneNumbers = mThreadMessage.getPhoneNumbers();
        int threadType;
        threadType = mThreadMessage.getThreadType();
        if (adminNumbers == null) adminNumbers = new ArrayList<>();
        if (phoneNumbers == null) phoneNumbers = new ArrayList<>();
        HashSet<String> hashSetAdmins = new HashSet<>(adminNumbers);
        HashSet<String> hashSetMembers = new HashSet<>(phoneNumbers);
        // bo member trung admin di
        hashSetMembers.removeAll(hashSetAdmins);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
            hashSetMembers.remove(myNumber);

        if (!hashSetAdmins.isEmpty()) {
            ArrayList<PhoneNumber> listPhoneAdmins = mContactBusiness.getListPhoneNumbersFromMemberGroup(new
                    ArrayList<>(hashSetAdmins));
            listAdminAndMembers.addAll(listPhoneAdmins);
            showTitleMakeAdminMe = false;
        } else {    // chua co ai lam admin
            showTitleMakeAdminMe = threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
        }
        ArrayList<PhoneNumber> listPhoneMembers = mContactBusiness.getListPhoneNumbersFromMemberGroup(new
                ArrayList<>(hashSetMembers));
        if (listPhoneMembers.size() >= 1) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        if (listPhoneMembers.get(0).getLastChangeAvatar() != null) {
//                            messageSettingGroupAvatarImgText.setVisibility(View.GONE);
//                            mApplication.getAvatarBusiness().setPhoneNumberAvatar(messageSettingGroupAvatarImg, messageSettingGroupAvatarImgText, listPhoneMembers.get(0), 55);
//                        } else {
//                            messageSettingGroupAvatarImg.setVisibility(View.GONE);
//                            if (listPhoneMembers.get(0).getName() != null && !listPhoneMembers.get(0).getName().isEmpty()) {
//                                messageSettingGroupAvatarImgText.setText(listPhoneMembers.get(0).getName().substring(0,1));
//                            }
                            mApplication.getAvatarBusiness().setPhoneNumberAvatar(messageSettingGroupAvatarImg, messageSettingGroupAvatarImgText, listPhoneMembers.get(1), 55);
                       // }
                    }
                });
        }
        if (listPhoneMembers.size() >= 2) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    if (listPhoneMembers.get(1).getLastChangeAvatar() != null) {
//                       // messageSettingGroupAvatarImg2Text.setVisibility(View.GONE);
//                        mApplication.getAvatarBusiness().setPhoneNumberAvatar(messageSettingGroupAvatarImg2, messageSettingGroupAvatarImg2Text, listPhoneMembers.get(1), 55);
//                    } else {
//                        messageSettingGroupAvatarImg2.setVisibility(View.GONE);
//                        if (listPhoneMembers.get(1).getName() != null && !listPhoneMembers.get(1).getName().isEmpty()) {
//                            messageSettingGroupAvatarImgText.setText(listPhoneMembers.get(1).getName().substring(0,1));
//                        }
                        mApplication.getAvatarBusiness().setPhoneNumberAvatar(messageSettingGroupAvatarImg2, messageSettingGroupAvatarImg2Text, listPhoneMembers.get(2), 55);
//                    }
                }
            });
        }
        if (listPhoneMembers.size() >= 3) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    if (listPhoneMembers.get(2).getLastChangeAvatar() != null) {
//                        mApplication.getAvatarBusiness().setPhoneNumberAvatar(messageSettingGroupAvatarImg3, messageSettingGroupAvatarImg3Text, listPhoneMembers.get(2), 55);
//                    } else {
                       // messageSettingGroupAvatarImg3.setVisibility(View.GONE);
//                        if (listPhoneMembers.get(2).getName() != null && !listPhoneMembers.get(2).getName().isEmpty()) {
//                            messageSettingGroupAvatarImgText.setText(listPhoneMembers.get(2).getName().substring(0,1));
//                        }
                        mApplication.getAvatarBusiness().setPhoneNumberAvatar(messageSettingGroupAvatarImg3, messageSettingGroupAvatarImg3Text, listPhoneMembers.get(3), 55);
                   // }
                }
            });
        }

        if (listPhoneMembers.size() > 4) {
            messageSettingGroupAvatarImg4Text.setText("+" + ((listPhoneMembers.size() + 1) - 3));
        } else {
            if(messageSettingGroupAvatarImg4Text != null) {
                messageSettingGroupAvatarImg4Text.setVisibility(View.GONE);
            }
        }
        List<PhoneNumber> listCompacts = new ArrayList<>();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && !mThreadMessage.isAdmin()) {// minh ko la
            // admin thi add minh len dau
            PhoneNumber myPhone = new PhoneNumber();
            myPhone.setJidNumber(myNumber);
            myPhone.setContactId("-2");
            listPhoneMembers.add(0, myPhone);
        }

        //
        if (!isLoadFull && listPhoneMembers.size() > Constants.CONTACT.MAX_MEMBER_GROUP_SHOW) {
            listCompacts = listPhoneMembers.subList(0, Constants.CONTACT.MAX_MEMBER_GROUP_SHOW);
            showTitleFullMember = true;
        } else {
            listCompacts = listPhoneMembers;
            showTitleFullMember = false;
        }
        if (!listCompacts.isEmpty()) {// danh sach member # rong thi moi add
            PhoneNumber rowTitleMembers = new PhoneNumber();
            if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT)
                rowTitleMembers.setName(mRes.getString(R.string.list_members_broadcast));
            else
                rowTitleMembers.setName(getTitleMember());
            rowTitleMembers.setContactId(null);
            listCompacts.add(0, rowTitleMembers);
            listAdminAndMembers.addAll(listCompacts);
        }
        return listAdminAndMembers;
    }

    private class RenameGroupAsynctask extends AsyncTask<Void, Void, XMPPResponseCode> {
        private String groupName;
        private ThreadMessage threadMessage;
        private String title;

        public RenameGroupAsynctask(ThreadMessage threadMessage, String groupName) {
            this.groupName = groupName;
            this.threadMessage = threadMessage;
        }

        @Override
        protected void onPreExecute() {
            title = mRes.getString(R.string.title_rename_group);
            mParentActivity.showLoadingDialog(title, mRes.getString(R.string.processing));
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(Void... params) {
            //            SystemClock.sleep(2000);
            mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string.title_rename_group, R.string.title_rename_group);
            return mMessageBusiness.renameGroup(threadMessage, groupName);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode param) {
            mParentActivity.hideLoadingDialog();
            int responseCode = param.getCode();
            if (responseCode != XMPPCode.E200_OK) {
                mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(param);
        }
    }

    private class LeaveGroupAsynctask extends AsyncTask<ThreadMessage, Void, XMPPResponseCode> {
        private String title = mRes.getString(R.string.title_leave_groupchat);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(ThreadMessage... params) {
            threadMessage = params[0];
            mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string.leave_group, R.string
                    .leave_group);
            return mMessageBusiness.leaveGroup(threadMessage);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mParentActivity.hideLoadingDialog();
            int responseCode = params.getCode();
            if (responseCode == XMPPCode.E200_OK ||
                    responseCode == XMPPCode.E404_GROUP_NO_LONGER_EXIST ||
                    responseCode == XMPPCode.E415_NOT_ALLOW_LEAVE_ROOM ||
                    responseCode == XMPPCode.E416_NOT_ALLOW_INVITE) {
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                String msgNotify = mRes.getString(R.string.msg_noti_leave);
                Date date = new Date();
                mMessageBusiness.insertNewMessageNotifyGroup(mApplication, threadMessage, "packetId-leave-111",
                        threadMessage.getServerId(), myNumber, msgNotify, ReengMessagePacket.SubType.leave, date
                                .getTime(), null, MessageConstants.NOTIFY_TYPE.leave.name());
                setComponentViews();
                //refreshListFriend();
                mApplication.getMusicBusiness().onLeaveMusic(false);
                if (mThreadMessage.getPinMessage() != null) {
                    mThreadMessage.setPinMessage("");
                    mMessageBusiness.updateThreadMessage(mThreadMessage);
                    mMessageBusiness.notifyPinMessageUpdate(mThreadMessage);
                }
            } else {
                mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(params);
        }
    }

    private class MakeAdminGroupAsyncTask extends AsyncTask<Void, Void, XMPPResponseCode> {
        private String title = "";
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;
        private boolean isMakeAdmin;
        private String number;

        public MakeAdminGroupAsyncTask(ThreadMessage threadMessage, String memberNumber, boolean isMakeAdmin) {
            if (isMakeAdmin) {
                title = mRes.getString(R.string.title_make_admin_groupchat);
            } else {
                title = mRes.getString(R.string.title_make_member_groupchat);
            }
            this.isMakeAdmin = isMakeAdmin;
            this.number = memberNumber;
            this.threadMessage = threadMessage;
        }

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(Void... params) {
            mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string.leave_group, R.string
                    .leave_group);
            return mMessageBusiness.makeAdminGroup(number, threadMessage, isMakeAdmin);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mParentActivity.hideLoadingDialog();
            int responseCode = params.getCode();
            if (responseCode != XMPPCode.E200_OK) {
                mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(params);
        }
    }

    private class KickMemberGroupAsyncTask extends AsyncTask<Void, Void, XMPPResponseCode> {
        private String title = mRes.getString(R.string.title_kick_member_groupchat);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;
        private String number;

        public KickMemberGroupAsyncTask(ThreadMessage threadMessage, String memberNumber) {
            this.threadMessage = threadMessage;
            this.number = memberNumber;
        }

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(null, msgWait);
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(Void... params) {
            mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string.leave_group, R.string
                    .leave_group);
            return mMessageBusiness.kickMemberGroup(number, threadMessage);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode params) {
            mParentActivity.hideLoadingDialog();
            int responseCode = params.getCode();
            if (responseCode != XMPPCode.E200_OK) {
                mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(params);
        }
    }

    private class DeleteBroadcastAsynctask extends AsyncTask<ThreadMessage, Void, Void> {
        private String title = mRes.getString(R.string.title_leave_groupchat);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(ThreadMessage... params) {
            threadMessage = params[0];
            mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string.delete_broadcast, R.string
                    .delete_broadcast);
            mMessageBusiness.deleteThreadMessage(threadMessage);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mParentActivity.hideLoadingDialog();
            mParentActivity.finish();
            super.onPostExecute(aVoid);
        }
    }

    private class PrivateGroupAsyncTask extends AsyncTask<Void, Void, XMPPResponseCode> {
        private boolean isPrivate;
        private ThreadMessage threadMessage;
        private String title;

        public PrivateGroupAsyncTask(ThreadMessage threadMessage, boolean isPrivate) {
            this.isPrivate = isPrivate;
            this.threadMessage = threadMessage;
        }

        @Override
        protected void onPreExecute() {
            title = mRes.getString(R.string.title_private_group);
            mParentActivity.showLoadingDialog(title, mRes.getString(R.string.processing));
            super.onPreExecute();
        }

        @Override
        protected XMPPResponseCode doInBackground(Void... params) {
            return mMessageBusiness.makeGroupPrivate(threadMessage, isPrivate);
        }

        @Override
        protected void onPostExecute(XMPPResponseCode param) {
            mParentActivity.hideLoadingDialog();
            int responseCode = param.getCode();
            if (responseCode != XMPPCode.E200_OK) {
                mCbxGroupPrivate.setChecked(!isPrivate);
                mParentActivity.showError(mRes.getString(XMPPCode.getResourceOfCode(responseCode)), title);
            }
            super.onPostExecute(param);
        }
    }

    private class DeleteAllMsgGroupAsynctask extends AsyncTask<ThreadMessage, Void, Void> {
        private String title = mRes.getString(R.string.title_leave_groupchat);
        private String msgWait = mRes.getString(R.string.msg_waiting);
        private ThreadMessage threadMessage;

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(title, msgWait);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(ThreadMessage... params) {
            threadMessage = params[0];
            mParentActivity.trackingEvent(R.string.ga_category_message_setting, R.string.delete_broadcast, R.string
                    .delete_broadcast);
            mMessageBusiness.deleteAllMsgGroupInThreadMessage(threadMessage);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mParentActivity.hideLoadingDialog();
            if (mListener != null) mListener.notifyReloadMessage(threadMessage);
//            mMessageBusiness.refreshThreadWithoutNewMessage(threadMessage.getId());
            super.onPostExecute(aVoid);
        }
    }

    private void processGotoProfile(PhoneNumber phoneNumber) {
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String friendJid = phoneNumber.getJidNumber();
        if (myNumber != null && myNumber.equals(friendJid)) {
            NavigateActivityHelper.navigateToMyProfile(mParentActivity);
        } else {
            if (phoneNumber.getId() != null) {
                mListener.navigateToContactDetailActivity(phoneNumber.getId());
            } else {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().
                        getExistStrangerPhoneNumberFromNumber(friendJid);
                if (strangerPhoneNumber != null) {
                    mListener.navigateToStrangerDetail(strangerPhoneNumber, null, null);
                } else {
                    NonContact existNonContact = mContactBusiness.getExistNonContact(friendJid);
                    if (existNonContact == null) {
                        mContactBusiness.insertNonContact(phoneNumber, false);
                    }
                    mListener.navigateToNonContactDetailActiviy(friendJid);
                }
            }
        }
    }

    private void handleChangeThreadName(String title, String oldName) {
        DialogEditText dialog = new DialogEditText(mParentActivity, true)
                .setCheckEnable(true)
                .setSelectAll(true)
                .setLabel(title)
                .setMessage(null)
                .setOldContent(oldName)
                .setMaxLength(Constants.MESSAGE.GROUP_NAME_MAX_LENGTH)
                .setNegativeLabel(mRes.getString(R.string.exit))
                .setPositiveLabel(mRes.getString(R.string.save))
                .setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String result) {
                        result = result.trim();
                        if (NetworkHelper.isConnectInternet(mParentActivity)) {
                            if (mThreadMessage != null && mThreadMessage.getThreadType() == ThreadMessageConstant
                                    .TYPE_THREAD_GROUP_CHAT) {
                                new RenameGroupAsynctask(mThreadMessage, result).executeOnExecutor(AsyncTask
                                        .THREAD_POOL_EXECUTOR);
                            } else if (mThreadMessage != null && mThreadMessage.getThreadType() ==
                                    ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                                mMessageBusiness.renameBroadcast(result, mThreadMessage);
                            }
                        } else {
                            mParentActivity.showError(mRes.getString(R.string.error_internet_disconnect), null);
                        }
                    }
                }).setDismissListener(new DismissListener() {
                    @Override
                    public void onDismiss() {
                        mParentActivity.hideKeyboard();
                    }
                });
        dialog.show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHideThreadChatEvent(final ThreadListFragmentRecycleView.HideThreadChatEvent event) {
        if (event == null) return;
        if (event.type == PINSettingFragment.TYPE_SET_HIDE_THREAD) {
            if (mThreadMessage != null) {
                mThreadMessage.setHiddenThread(1);
                mApplication.getMessageBusiness().updateThreadMessage(mThreadMessage);
//                mParentActivity.showToast(String.format(mParentActivity.getString(R.string.hide_thread_success), mThreadMessage.getThreadName()));
                tvHiddenThread.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogMessage dialogMessage = new DialogMessage(mParentActivity, true);
                        dialogMessage.setLabel(mParentActivity.getString(R.string.setup_successfull));
                        dialogMessage.setMessage(mParentActivity.getString(R.string.msg_setup_pin_success));
                        dialogMessage.setLabelButton(mParentActivity.getString(R.string.permission_understood));
                        dialogMessage.show();
                    }
                }, 200);
            }
        } else if (event.type == PINSettingFragment.TYPE_SET_NEW_PIN) {
            mThreadMessage.setHiddenThread(1);
            mApplication.getMessageBusiness().updateThreadMessage(mThreadMessage);

            tvHiddenThread.postDelayed(new Runnable() {
                @Override
                public void run() {
                    DialogMessage dialogMessage = new DialogMessage(mParentActivity, true);
                    dialogMessage.setLabel(mParentActivity.getString(R.string.setup_successfull));
                    dialogMessage.setMessage(mParentActivity.getString(R.string.msg_setup_pin_success));
                    dialogMessage.setLabelButton(mParentActivity.getString(R.string.permission_understood));
                    dialogMessage.show();
                }
            }, 200);

        }
    }


    // fragment interface
    public interface OnFragmentInteractionListener {
        void navigateToContactDetailActivity(String phoneId);

        void navigateToStrangerDetail(StrangerPhoneNumber strangerPhoneNumber,
                                      String friendJid, String friendName);

        void navigateToNonContactDetailActiviy(String phoneNumber);

        void addNewContact(String number, String name);

        void navigateToChooseFriendsActivity(ArrayList<String> listNumberString, ThreadMessage threadMessage, int typeScreen);

        void takePhotoChangeGroupAvatar();

        void openGalleryChangeGroupAvatar();

        void navigateToPollActivity(ThreadMessage threadMessage, ReengMessage reengMessage, String pollId, int type, boolean pollUpdate);

        void navigateToOfficialDetail(String officialId);

        void navigateToInvite(String officialId);

        void navigateToChooseDocument(ThreadMessage threadMessage);

        void notifyReloadMessage(ThreadMessage threadMessage);

        void notifyGetListMemberThread(ArrayList<PhoneNumber> listPhone);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onThreadDetailSettingEvent(final ThreadDetailSettingEvent event) {
        if (event == null || event.getReengMessage() == null) return;
        if (event.getIdThread() == mThreadId) {
            if (event.getReengMessage().getMessageType() == ReengMessageConstant.MessageType.enable_e2e) {
                mCbxEnrypt.setChecked(event.getReengMessage().getStateEnableE2E() == 1);
            } else if ((event.getReengMessage().getMessageType() == ReengMessageConstant.MessageType.text
                    && !TextUtils.isEmpty(event.getReengMessage().getPreKeyTmp()))) {
                if (!mThreadMessage.isEncryptThread()) {
                    mCbxEnrypt.setChecked(true);
                }
            }
        }

    }


    public static class ThreadDetailSettingEvent {
        private int idThread;
        private ReengMessage reengMessage;

        public ThreadDetailSettingEvent(int idThread, ReengMessage reengMessage) {
            this.idThread = idThread;
            this.reengMessage = reengMessage;
        }

        public int getIdThread() {
            return idThread;
        }

        public ReengMessage getReengMessage() {
            return reengMessage;
        }
    }
}
package com.metfone.selfcare.fragment.transfermoney;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.TransferMoneyActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.TransferMoneyBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.ui.DecimalTextWatcher;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 6/19/2015.
 */
public class TransferMoneyFragment extends Fragment
        implements InitDataListener, TransferMoneyBusiness.RequestGenOtpCallback {
    private static final String TAG = TransferMoneyFragment.class.getSimpleName();
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final long MIN_TRANSFER_THRESHOLD = 1;
    private static final long MAX_TRANSFER_THRESHOLD = 300 * 1000;
    private static final String MIN_TRANSFER_THRESHOLD_TEXT = "0";
    private static final String MAX_TRANSFER_THRESHOLD_TEXT = "300,000";
    private Resources mRes;
    private String mFriendPhoneNumber;
    private TransferMoneyActivity mParentActivity;
    private ApplicationController mApplication;
    private EditText mEdtMoneyAmount;
    private Button mBtnTransferMoney;
    private EllipsisTextView mAbTitle;
    private ImageView mAbMoreBtn, mAbBackBtn;
    private TransferMoneyBusiness transferMoneyBusiness;
    private long moneyAmount;
    private TextView mTvwHeader, mTvwHeader2;
    private String friendName;

    public static TransferMoneyFragment newInstance(String friendPhoneNumber) {
        TransferMoneyFragment fragment = new TransferMoneyFragment();
        Bundle args = new Bundle();
        args.putString(PHONE_NUMBER, friendPhoneNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof Activity) {
            mParentActivity = (TransferMoneyActivity) context;
            mApplication = (ApplicationController) mParentActivity.getApplication();
            mRes = mParentActivity.getResources();
            transferMoneyBusiness = TransferMoneyBusiness.getInstance(mApplication);
        }
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mFriendPhoneNumber = savedInstanceState.getString(PHONE_NUMBER);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transfer_money, container, false);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
//        mMessageBusiness = mApplication.getMessageBusiness();
//        mContactBusiness = mApplication.getContactBusiness();
//        mRes = mParentActivity.getResources();
        findComponentViews(rootView, inflater, container);
        setListener();
        getDataAndDraw(savedInstanceState);
        return rootView;
    }

    private void setListener() {
        mBtnTransferMoney.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String moneyInput = mEdtMoneyAmount.getText().toString().trim();
                        if (TextUtils.isEmpty(moneyInput)) {
                            String msg = mRes.getString(R.string.error_transfer_input_empty);
                            showDialogError(msg, MIN_TRANSFER_THRESHOLD_TEXT, Constants.ACTION.MONEY_AMOUNT_MIN);
                        } else {
                            try {
                                moneyAmount = transferMoneyBusiness.getMoneyAmountFromText(moneyInput);
                                if (validateMoneyAmount(moneyAmount)) {
                                    mParentActivity.showLoadingDialog("", R.string.processing);
                                    transferMoneyBusiness.doGenOtpRequest(TransferMoneyFragment.this,
                                            mFriendPhoneNumber,
                                            String.valueOf(moneyAmount), null);
                                }
                            } catch (NumberFormatException e) {
                                Log.e(TAG, "Exception", e);
                                TransferMoneyFragment.this.onError(mRes.getString(R.string.e601_error_but_undefined));
                            }
                        }
                    }
                }
        );
        mAbBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private boolean validateMoneyAmount(long moneyAmount) {
        if (moneyAmount < MIN_TRANSFER_THRESHOLD) {
            String msg = mRes.getString(R.string.error_transfer_input_empty);
            showDialogError(msg, MIN_TRANSFER_THRESHOLD_TEXT, Constants.ACTION.MONEY_AMOUNT_MIN);
            return false;
        } else {
            //money is valid
            return true;
        }
    }

    private void showDialogError(String msg, String content, int actionId) {
        ClickListener.IconListener listener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                if (menuId == Constants.ACTION.MONEY_AMOUNT_MIN) {
                    mEdtMoneyAmount.setText((String) entry);
                } else if (menuId == Constants.ACTION.MONEY_AMOUNT_MAX) {
                    mEdtMoneyAmount.setText((String) entry);
                }
            }
        };
        String title = mRes.getString(R.string.note_title);
        String labelOk = mRes.getString(R.string.ok);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOk, null, listener, content, actionId);
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mEdtMoneyAmount = (EditText) rootView.findViewById(R.id.money_amount_content);
        mBtnTransferMoney = (Button) rootView.findViewById(R.id.btn_transfer_money);
        View abView = mParentActivity.getmViewActionBar();
        mAbTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        mAbMoreBtn = (ImageView) abView.findViewById(R.id.ab_more_btn);
        mAbBackBtn = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mTvwHeader = (TextView) rootView.findViewById(R.id.text_note_enter_money);
        mTvwHeader2 = (TextView) rootView.findViewById(R.id.text_note_enter_money_2);
        mEdtMoneyAmount.addTextChangedListener(new DecimalTextWatcher(mEdtMoneyAmount));
        String content = mEdtMoneyAmount.getText().toString();
        if (!TextUtils.isEmpty(content)) {
            mEdtMoneyAmount.setSelection(content.length());
        }
        mEdtMoneyAmount.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEdtMoneyAmount.requestFocus();
            }
        }, 150);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void drawActionbar() {
        mAbTitle.setText(R.string.transfer_money_subscribe);
        mAbMoreBtn.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER, mFriendPhoneNumber);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void getDataAndDraw(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mFriendPhoneNumber = getArguments().getString(PHONE_NUMBER);
        }
        drawActionbar();
        friendName = mApplication.getMessageBusiness().getFriendName(mFriendPhoneNumber);
        mTvwHeader.setVisibility(View.GONE);
        mTvwHeader2.setText(String.format(mRes.getString(R.string.enter_money_to_transfer), friendName));
    }

    @Override
    public void onDataReady() {
        getDataAndDraw(null);
    }


    @Override
    public void onSuccess(String requestId) {
        //gen otp success
        mParentActivity.hideLoadingDialog();
        InputMethodUtils.hideSoftKeyboard(mEdtMoneyAmount, mParentActivity);
        String moneyAmountFormat = mEdtMoneyAmount.getText().toString();
        mParentActivity.navigateToMoneyConfirmFragment(requestId,
                mFriendPhoneNumber, moneyAmount, moneyAmountFormat);
    }

    @Override
    public void onError(String message) {
        //gen otp error
        mParentActivity.hideLoadingDialog();
        mParentActivity.showError(message, "");
    }

    public interface OnFragmentInteractionListener {
        void navigateToMoneyConfirmFragment(String requestId, String friendPhoneNumber,
                                            long moneyAmount, String moneyAmountFormat);
    }
}
package com.metfone.selfcare.fragment.viettelIQ;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.ViettelIQActivity;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CountFirstQuestionViettelIQFragment extends Fragment {

    private static final String TAG = CountFirstQuestionViettelIQFragment.class.getSimpleName();

    private static final String START_TIME = "startTime";

    @BindView(R.id.tv_count_down)
    TextView tvCountDown;
    @BindView(R.id.root_count_down)
    LinearLayout rootCountDown;
    Unbinder unbinder;

    public static CountFirstQuestionViettelIQFragment newInstance(long startTime) {
        Bundle args = new Bundle();
        args.putLong(START_TIME, startTime);
        CountFirstQuestionViettelIQFragment fragment = new CountFirstQuestionViettelIQFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private long startTime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startTime = getArguments().getLong(START_TIME);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viettel_iq_count_first_question, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (tvCountDown == null) return;
        tvCountDown.removeCallbacks(scheduleRunnable);
    }

    private void initView() {
        if (tvCountDown == null) return;
        tvCountDown.removeCallbacks(scheduleRunnable);
        tvCountDown.post(scheduleRunnable);
    }

    private Runnable scheduleRunnable = new Runnable() {
        @Override
        public void run() {
            if (tvCountDown == null) return;
            if (startTime - ((ViettelIQActivity) getActivity()).getCurrentTime() < 1000) {
                Activity activity = getActivity();
                if (activity instanceof ViettelIQActivity)
                    ((ViettelIQActivity) activity).openQuestionByIndex();
            } else {
                setTime();
                tvCountDown.postDelayed(this, 200);
            }
        }
    };

    private void setTime() {
        if (tvCountDown == null) return;
        long currentTimeMillis = ((ViettelIQActivity) getActivity()).getCurrentTime();
        long countDown = (startTime - currentTimeMillis) / 1000;

        if (countDown < 10) {
            tvCountDown.setText(String.format("0%s giây", String.valueOf(countDown)));
        } else {
            tvCountDown.setText(String.format("%s giây", String.valueOf(countDown)));
        }
    }

}

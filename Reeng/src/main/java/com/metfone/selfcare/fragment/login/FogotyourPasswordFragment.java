package com.metfone.selfcare.fragment.login;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;


public class FogotyourPasswordFragment extends Fragment {


    public static FogotyourPasswordFragment newInstance(String param1, String param2) {
        FogotyourPasswordFragment fragment = new FogotyourPasswordFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fogotyour_password, container, false);
    }
}
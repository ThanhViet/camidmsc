package com.metfone.selfcare.fragment.message;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.bplus.sdk.BplusSdk;
import com.bplus.sdk.ResultListener;
import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.google.gson.Gson;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.activity.PreviewImageActivity;
import com.metfone.selfcare.activity.StickerActivity;
import com.metfone.selfcare.adapter.EmoticonsGridAdapter;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.adapter.ThreadDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.KeyguardGoneReceiver;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.BlockContactBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.business.OfficerBusiness.RequestLeaveRoomChatListener;
import com.metfone.selfcare.business.OfficialActionManager;
import com.metfone.selfcare.business.OutgoingMessageProcessor;
import com.metfone.selfcare.business.PubSubManager;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.controllers.BuzzStickerController;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.BlockContactModel;
import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.database.model.GiftLixiModel;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ReengMessageWrapper;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.CPresence;
import com.metfone.selfcare.database.model.message.GiftLixiMessage;
import com.metfone.selfcare.database.model.message.PinMessage;
import com.metfone.selfcare.database.model.message.ReplyMessage;
import com.metfone.selfcare.database.model.message.SoloSendImageMessage;
import com.metfone.selfcare.database.model.message.SoloSendShareContactMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.database.model.message.SoloSendVoicemailMessage;
import com.metfone.selfcare.database.model.message.SoloShareLocationMessage;
import com.metfone.selfcare.database.model.message.SoloShareVideoMessage;
import com.metfone.selfcare.database.model.message.SoloVoiceStickerMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.database.model.popup.PopupIntroDetail;
import com.metfone.selfcare.database.model.popup.PopupIntroModel;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.MoreAppInteractHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.NoteMessageHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TagHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonManager;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.CrbtRequestHelper;
import com.metfone.selfcare.helper.httprequest.InAppHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.helper.message.BankPlusHelper;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.helper.message.ProgressFileManager;
import com.metfone.selfcare.helper.message.SpamRoomManager;
import com.metfone.selfcare.helper.message.TypingManager;
import com.metfone.selfcare.helper.sticker.VoiceStickerPlayer;
import com.metfone.selfcare.helper.video.Video;
import com.metfone.selfcare.helper.voicemail.AudioVoicemailPlayerImpl2;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.FilePickerListener;
import com.metfone.selfcare.listeners.ICusKeyboard;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.KeyguardGoneListener;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.module.keeng.widget.floatingView.MusicFloatingView;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.network.fileTransfer.DownloadMultiFileAsysncTask;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.notification.ReengNotificationManager;
import com.metfone.selfcare.ui.BottomSheetContextMenu;
import com.metfone.selfcare.ui.CusKeyboardController;
import com.metfone.selfcare.ui.CusRelativeLayout;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.PopupListener;
import com.metfone.selfcare.ui.PopupZodiac;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReactionListBottomDialog;
import com.metfone.selfcare.ui.ReactionPopup;
import com.metfone.selfcare.ui.WindowShaker;
import com.metfone.selfcare.ui.chatviews.BottomChatOption;
import com.metfone.selfcare.ui.chatviews.MultiLineEdittextTag;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PopupReceiveLixi;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.popup.PopupIntro;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.ui.tokenautocomplete.FilteredArrayAdapter;
import com.metfone.selfcare.util.ControlMedia;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.packet.ReengMusicPacket;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifDrawableBuilder;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIRST_SHOW_POPUP_INTRO_SMS_NO_INTERNET;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET;
import static com.metfone.selfcare.listeners.ClickListener.IconListener;

public class ThreadDetailFragment extends Fragment implements
        EmoticonsGridAdapter.KeyClickListener,
        FilePickerListener,
        ReengMessageListener,
        SmartTextClickListener,
        IconListener,
        MessageInteractionListener,
        ContactChangeListener,
        XMPPConnectivityChangeListener,
        ConfigGroupListener,
        InitDataListener,
        ICusKeyboard.SendReengClickListener,
        ICusKeyboard.OpenCusKeyboarListener,
        ICusKeyboard.CloseCusKeyboarListener,
        ICusKeyboard.ChangeListMessageSizeListener,
        ICusKeyboard.ChangeNoMessageViewSizeListener,
        PlayMusicController.OnPlayMusicStateChange,
        KeyguardGoneListener, NetworkHelper.NetworkChangedCallback,
        MusicBusiness.OnActionMusicResponseListener,
        ControlMedia.OnStateMusicBarChange,
        RequestLeaveRoomChatListener,
        View.OnClickListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        TypingManager.TypingManagerInterface,
        MediaPlayer.OnErrorListener,
        TagMocha.OnClickTag, ContentConfigBusiness.OnConfigChangeListener,
        PrefixChangeNumberHelper.EditContactSuccess,
        PrefixChangeNumberHelper.UpdateListThread,
        ReactionPopup.OnReactImageClickListener {
    public static final String TAG = ThreadDetailFragment.class.getSimpleName();
    public static final int DEFAULT_PREVIOUS_STICKER = -2;
    private static final long DELAY_DRAW_DETAIL = 1000;
    private static final long TIME_DELAY_CHECK_NETWORK = 10000;
    private static int TIME_DEFAULT_RESEND_TYPING = 5000;
    private static int TIME_DEFAULT_SHOW_TYPING = 10000;
    private static int INTERVAL_COUNTDOWN_MSG_INFO = 2000;
    private static int TIME_CHECK_MSG_INFO = 3000;
    private static int LENGTH_TO_SEND_TYPING = 1;
    private final float SCROLL_THRESHOLD = 10;
    public boolean isSaveInstance = false;
    boolean isPrepared = false;
    boolean isCountingHidePlayer = false;
    int ERROR_SUPPORT_MEDIA = -38;
    AlertDialog mAlertDialog;
    int lastState = TypingManager.STATE_GONE;
    private SharedPreferences mPref;
    private OnFragmentInteractionListener mListener;
    private IconListener mIconListener;
    private SmartTextClickListener mSmartTextListener;
    private ThreadMessage mThreadMessage;
    private ThreadDetailAdapter mThreadDetailAdapter;
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mReengAccountBusiness;
    private SettingBusiness mSettingBusiness;
    private AvatarBusiness mAvatarBusiness;
    private ContactBusiness mContactBusiness;
    private BlockContactBusiness mBlockBusines;
    private OfficerBusiness mOfficerBusiness;
    private ChatActivity mChatActivity;
    OfficerBusiness.RequestFollowRoomChatListener followRoomListener = new OfficerBusiness
            .RequestFollowRoomChatListener() {
        @Override
        public void onResponseFollowRoomChat(ThreadMessage thread) {
            Log.i(TAG, "onResponseFollowRoomChat when reconnect");
            PubSubManager.getInstance(mApplication).subscribeRoomChat(mThreadMessage);
        }

        @Override
        public void onErrorFollow(int errorCode, String msg) {
            mChatActivity.goPreviousOrHomeWhenBackPress();
        }
    };
    private Resources mRes;
    private CopyOnWriteArrayList<ReengMessage> mMessageArrayList;
    private MultiLineEdittextTag etMessageContent;
    private CusRelativeLayout rootView;
    private Handler mHandler;
    private String userNumber, mStatus, friendPhoneNumber;
    private String mStringCompareTyping, draftMessage, mCurentStringTyping, urlAvatarTmp;
    private int mThreadId, mThreadType;
    private int gaCategoryId, mMediaControlHeight, currentScrollState;
    private ReengNotificationManager mReengNotificationManager;
    private boolean mGsmMode = false, mIsReengUser = false, isDraft = false, isCreated;
    private ListView mLvwContent;
    private View mViewAbProfile, mHeaderAddContact, mViewConnectivityStatus, mViewParent,
            mViewSuggestSticker, mViewTypingManager;
    private View mBtnLoadMore, mBtnInviteSMS, mFooterStateSeen, abView, mFooterAlertStranger;
    private View mLlNotifyShareMusic, popupViewPreview, mViewAcceptStranger, popupViewMoreMsgUnred;
    private Button mBtnAddContact, mBtnAcceptStranger;
    private ImageView mImgMenuVideoCall, ab_back_btn;
    private ImageView mImgRoomStar, mImgBackground, mSticker1, mSticker2, mSticker3, imgPreview, mImgAddFriend, mImgMenuAttach,mImgMenuStatus,mImgMenuCall,imgIcon;
    private LinearLayout mLlAddContact, mLlAlertStrangerBlock, mLlAlertStrangerChat, mViewBackLayout;
    private LinearLayout mLayoutProfile, rltNoMessage;
    private RelativeLayout mRltProfile;
    private CircleImageView mImgProfileAvatar, tvw_avatar_profile,mHeaderAvatar;
//    private CircleImageView mImgProfileAvatar;
    private TextView mTvStatus, mTvwAddContact, mTvwAlertStrangerBlock,tvSavMessage;
    private TextView mTvwProfileAvtText, mTvwProfileName, txtRqMetphone;
    private EllipsisTextView mTvwProfileStatus, mTvName;
    private RelativeLayout mLayoutPopupPreview;
    private ProgressLoading mProgressPreview;
    private CusKeyboardController mKeyboardController;
    Runnable checkNetWork = new Runnable() {
        @Override
        public void run() {
            checkToShowNetworkState();
        }
    };
    private CountDownTimer countDownTimerTyping, countDownTimerReceiveTyping;
    private TypingManager mTypingManager;
    //new view intro group
    private ImageView mImgFirstIntro, mImgSecondIntro, mImgThirdIntro;
    private CircleImageView mImgBgFristIntro, mImgBgSecondIntro, mImgBgThirdIntro;
    private TextView mTvwFirstIntro, mTvwSecondIntro, mTvwThirdIntro;
    private View mLayoutFirstIntro, mLayoutSecondIntro, mLayoutThirdIntro, mLayoutIntroGroup;
    //
    private MusicBusiness mMusicBusiness;
    private ControlMedia mControlMedia;
    //    private ArrayList<ItemContextMenu> listItemOverFlow;
    private boolean showBtnAddContact = false;
    private VoiceStickerPlayer mVoiceStickerPlayer;
    private PopupWindow popupWindowPreview;
    private GifDrawable gifDrawable, gifDrawableDetail;
    private boolean isShowPreviewDownloading;
    private boolean settingAutoPlaySticker = false; //co tu dong play sticker hay ko?
    private int preCollectionIDSticker, prePossitionSticker;
    private GetMoreMessageTask getMoreMessageTask;
    private ApplicationStateManager mAppStateManager;
    private KeyguardGoneReceiver mKeyguardGoneReceiver;
    private StickerBusiness mStickerBusiness;
    private ReengMessage mForwardingMessage;
    private BuzzStickerController mBuzzStickerController;
    private WindowShaker.Option option;
    private CountDownTimer mConnectingTimer, mHideConnectStateTimer;
    private ReengMessageWrapper arrayMessage;
    //cac bien nay dung de check listview scroll hay click
    private float mDownX, mDownY;
    private boolean isOnClick, isShowInfo = false, needToShowHeaderProfile = false;
    private Handler mHandlerCountDown;
    private SoloShareVideoMessage sendVideoMessage;
    private OfficerAccount mOfficerAccountRoomChat;
    private String languageCode;
    private ArrayList<StickerItem> suggestStrangerSticker = new ArrayList<>();
    private ReengMessage reengMesssageReply;
    private EventOnMediaHelper eventOnMediaHelper;
    private FacebookHelper facebookHelper;
    private View mViewVideo, mLayoutControlVideo;
    private VideoView mVideo;
    private ImageView mImgTogglePlay;
    private TextView mTvwTimeVideo, mTvwDurationVideo;
    //    private SeekBar mSeekBarVideo;
    private ProgressBar mProgressPlayVideo;
    private ProgressLoading mProgressLoadingVideo;
    private TextView mTvwTitleVideo;
    private ImageView mImgCloseVideo, mImgThumbVideo, mImgToggleScreen;
    private String msToClockTime;
    private boolean isPauseVideo = false;
    private CountDownTimer cdHide;
    private boolean isFullScreen = false;
    private int defaultScreenOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
    private int heightVideoView = 0;
    private int positionSeekVideo;
    private boolean needPauseVideo = false;
    private int countRetryFail = 0;
    private int cState = -1;
    private AudioVoicemailPlayerImpl2 audioVoicemailPlayerImpl2;
    private TagOnMediaAdapter adapterUserTag;
    private boolean enableClickTag = true;
    private View viewPinMessage;
    private TextView mTvwTitlePinMsg;
    private EllipsisTextView mTvwContentPinMsg;
    private ImageView mImgPinMsg;
    private ImageView mImgThumbPin;
    private ImageView mImgClosePinMsg;
    private boolean isShowingPinMessage;
    private boolean isViettel;
    private String friendOperator;
    private int usingDesktop;
    private PopupListener mPopupListener;
    //    private boolean showIntroReaction = false;
    private boolean isSendSMSout;
    private View rlSuggestSendMsgNoInternet;
    private ImageView ivCloseMsgNoInternet;
    private TextView tvTryNow;
    private int srcScreenId;
    private BaseMPConfirmDialog confirmDialogMessenger;
    private Runnable runnableMsgNoInternet = new Runnable() {
        @Override
        public void run() {
            checkShowPopupIntroSendMsgNoInternet();
        }
    };
    private Runnable mUpdateTimeTask = new Runnable() {

        public void run() {
            if (!mVideo.isPlaying()) {
                return;
            }
            int totalDuration = mVideo.getDuration();
            long currentDuration = mVideo.getCurrentPosition();
            mTvwTimeVideo.setText(Utilities.milliSecondsToTimer(currentDuration));
            mTvwDurationVideo.setText(msToClockTime);
            int progress = Utilities.getProgressPercentage(currentDuration, totalDuration);
            mProgressPlayVideo.setProgress(progress);
            mHandler.postDelayed(this, Constants.ONMEDIA.DELAY_UPDATE_SEEKBAR);

        }
    };
    private ReengMessage messageMusicReInvite;
    private int sizeListMessage;
    private int sizeViewNoMessage;
    private ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int i, float v, int i1) {
            dismissStickerPopupWindow();
        }

        @Override
        public void onPageSelected(int i) {
            dismissStickerPopupWindow();
        }

        @Override
        public void onPageScrollStateChanged(int i) {
            dismissStickerPopupWindow();
        }
    };
    private OfficerBusiness.RequestReportRoomListener requestReportRoomListener = new OfficerBusiness
            .RequestReportRoomListener() {
        @Override
        public void onResponse() {
            Log.d(TAG, "requestReportRoomListener -ok ");
        }

        @Override
        public void onError(int errorCode) {
            Log.d(TAG, "requestReportRoomListener -fail " + errorCode);
        }
    };
    private long lastClick;
    //xu ly ban tin config sticky banner, tuong tu pin message
    private PinMessage currentStickyBanner;

    public ThreadDetailFragment() {
        // Required empty public constructor
    }

    public static ThreadDetailFragment newInstance(int threadId, int threadType, ReengMessage forwardingMessage, int srcScreenId) {
        ThreadDetailFragment fragment = new ThreadDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ThreadMessageConstant.THREAD_ID, threadId);
        args.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadType);
        args.putSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, forwardingMessage);
        args.putInt(Constants.SRC_SCREEN_ID, srcScreenId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ThreadDetailFragment newInstance(int threadId, int threadType, ReengMessageWrapper arrayMessage, int srcScreenId) {
        ThreadDetailFragment fragment = new ThreadDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ThreadMessageConstant.THREAD_ID, threadId);
        args.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadType);
        args.putSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE, arrayMessage);
        args.putInt(Constants.SRC_SCREEN_ID, srcScreenId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "--------------------------Attach");
        mChatActivity = (ChatActivity) activity;
        mApplication = (ApplicationController) mChatActivity.getApplicationContext();
        mAppStateManager = mApplication.getAppStateManager();
        mMessageBusiness = mApplication.getMessageBusiness();
        mMusicBusiness = mApplication.getMusicBusiness();
        mReengNotificationManager = ReengNotificationManager.getInstance(mApplication);
        mReengAccountBusiness = mApplication.getReengAccountBusiness();
        mAvatarBusiness = mApplication.getAvatarBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        mStickerBusiness = mApplication.getStickerBusiness();
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        mBlockBusines = mApplication.getBlockContactBusiness();
        mOfficerBusiness = mApplication.getOfficerBusiness();
        userNumber = mReengAccountBusiness.getJidNumber();
        mSmartTextListener = this;
        mRes = mChatActivity.getResources();
        eventOnMediaHelper = new EventOnMediaHelper(mChatActivity);
        facebookHelper = new FacebookHelper(mChatActivity);
        if (mPref == null) {
            mPref = mChatActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        String deviceLanguage = mReengAccountBusiness.getDeviceLanguage();
        languageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
        Log.d(TAG, "end onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        //show off check box delete
        Bundle bundle = getArguments();
        if (bundle != null) {
            mThreadId = bundle.getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = bundle.getInt(ThreadMessageConstant.THREAD_IS_GROUP);
            mForwardingMessage = (ReengMessage) bundle.getSerializable(Constants.CHOOSE_CONTACT
                    .DATA_REENG_MESSAGE);
            arrayMessage = (ReengMessageWrapper) bundle.getSerializable(Constants.CHOOSE_CONTACT
                    .DATA_ARRAY_MESSAGE);
            srcScreenId = bundle.getInt(Constants.SRC_SCREEN_ID);
        }
        getFriendPhoneNumber();
        gaCategoryId = R.string.ga_category_chat_screen;
        mChatActivity.setFilePickerListener(this);
        settingAutoPlaySticker = mSettingBusiness.getPrefAutoPlaySticker();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            settingAutoPlaySticker = false;
        } /*else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT)
            isSendSMSout = SettingBusiness.getInstance(mApplication).checkExistAutoSmsoutThread(mThreadId);*/
        audioVoicemailPlayerImpl2 = new AudioVoicemailPlayerImpl2(mApplication);

        changeColorWhiteStatusbar();

        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mHeaderAvatar.setVisibility(View.GONE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void changeColorWhiteStatusbar() {
        Window window = mChatActivity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(Color.parseColor("#FFFFFF"));
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart");
        super.onStart();
        if (mThreadMessage != null && mThreadMessage.isForeUpdateBg()) {
            updateBackground();
            mThreadMessage.setForeUpdateBg(false);
        }
        mChatActivity.setFilePickerListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        long beginTime = System.currentTimeMillis();
        long firstTime = System.currentTimeMillis();
        isCreated = true;
        rootView = (CusRelativeLayout) inflater.inflate(R.layout.fragment_thread_detail, container, false);
        tvSavMessage=rootView.findViewById(R.id.tvSavMessage);
        imgIcon=rootView.findViewById(R.id.img);
        Log.d(TAG, "--------------------------[perform] - inflater take " + (System.currentTimeMillis() - beginTime));
        beginTime = System.currentTimeMillis();
        findComponentViews(rootView, inflater, container);
        Log.d(TAG, "--------------------------[perform] - findComponentViews take " + (System.currentTimeMillis() - beginTime) + "ms");
        beginTime = System.currentTimeMillis();
        setAdapter();
        setViewListeners();
        initSticker();
        initWindowShaker();

        Log.d(TAG, "--------------------------[perform] - setAdapter --- initWindowShaker take " + (System.currentTimeMillis() - beginTime) +
                "ms");
        beginTime = System.currentTimeMillis();
        mTypingManager = new TypingManager(mApplication);


        Window window = getActivity().getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(0x00000000);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int flags = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
            window.addFlags(flags);
        }
        initKeyboardController(rootView);
        mKeyboardController.onCreateView();
        Log.d(TAG, "--------------------------[perform] - initKeyboardController take " + (System.currentTimeMillis() - beginTime) + "ms");
        beginTime = System.currentTimeMillis();
        // draft message
        if (mApplication.isDataReady()) {


            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
            if (mThreadMessage != null && mThreadMessage.getState() == ThreadMessageConstant.STATE_NEED_GET_INFO) {
                mMessageBusiness.getInfoGroupIfNeeded(mThreadMessage);
            }
            //o group k co avatar
            if(mThreadMessage.getAdminNumbers() != null && mThreadMessage.getAdminNumbers().size() > 0 ){
                mHeaderAvatar.setVisibility(View.GONE);
            }
            adapterUserTag = new TagOnMediaAdapter(mApplication, getGroupFriendList(), etMessageContent);
            etMessageContent.setAdapter(adapterUserTag);
            etMessageContent.setThreshold(0);
            adapterUserTag.setListener(new FilteredArrayAdapter.OnChangeItem() {
                @Override
                public void onChangeItem(int count) {
                    Log.i(TAG, "onChangeItem: " + count);
                    if (count > 2) {
                        int height = mRes.getDimensionPixelOffset(R.dimen.max_height_drop_down_tag);
                        etMessageContent.setDropDownHeight(height);
                    } else
                        etMessageContent.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            });
            /*if (mApplication.getReengAccountBusiness().isCambodia())
                etMessageContent.setThreshold(0);
            else
                etMessageContent.setThreshold(1);*/
            etMessageContent.setDropDownWidth(mApplication.getWidthPixels());
            etMessageContent.setDropDownBackgroundResource(R.color.white);
            getFriendPhoneNumber();
            checkViettel();
            mKeyboardController.setViettel(isViettel);
            if (!isForwardMessageText()) {
                setContentDraftMessage();
            }
            // update background
            updateBackground();
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                mOfficerAccountRoomChat = mOfficerBusiness.
                        getOfficerAccountByServerId(mThreadMessage.getServerId());
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                InAppHelper.getInstance(mApplication).checkShowPopupOrSendRequest(getFragmentManager(),
                        mChatActivity, friendPhoneNumber);
            }
            setViewHeaderProfile();
            OfficialActionManager.getInstance(mApplication).checkAndGetDefaultOfficialAction(mThreadMessage);
        }
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            TextHelper.getInstant().initSensitiveWordPatterns(mApplication);
        }
        PlayMusicController.addPlayMusicStateChange(this);
        ListenerHelper.getInstance().addContactChangeListener(this);
        ListenerHelper.getInstance().addConfigChangeListener(this);
        ListenerHelper.getInstance().addUpdateAllThreadListener(this);
        NetworkHelper.setNetworkChangedCallback(this);
        checkPrefixNumber();

        rlSuggestSendMsgNoInternet.postDelayed(runnableMsgNoInternet, 2000);

        etMessageContent.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    mApplication.getMessageBusiness().checkAndInsertMessageBannerLixi(mMessageArrayList, mThreadMessage);
                } catch (Exception e) {
                }
            }
        }, 50);

//        showIntroReaction = mApplication.getPref().getBoolean(Constants.PREFERENCE.PREF_DID_SHOW_INTRO_NEW_REACTION, false);

        long drawTime = System.currentTimeMillis() - firstTime;
        Log.d(TAG, "--------------------------[perform] - finish onCreateView " + drawTime + "ms");
        LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.CHAT_DRAW_VIEW,
                String.valueOf(drawTime), String.valueOf(firstTime), Constants.LogKQI.StateKQI.SUCCESS.getValue());

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mThreadMessage.isActionInvite()){
            etMessageContent.setText(mThreadMessage.getMessageInvite());
            mThreadMessage.setActionInvite(false);
            onSendReengClick();
        }
    }

    private void checkViettel() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            CPresence cPresence = mMessageBusiness.getCPresenceByJid(friendPhoneNumber);

            friendOperator = cPresence.getOperatorPresence();
            int newState = cPresence.getStatePresence();
            if (newState != -1)
                cState = newState;

            int newUsingDesktop = cPresence.getUsingDesktop();
            if (newUsingDesktop != -1)
                usingDesktop = newUsingDesktop;

            if (!TextUtils.isEmpty(friendOperator)) {
                isViettel = PhoneNumberHelper.isViettel(friendOperator);
            } else {
                isViettel = PhoneNumberHelper.getInstant().isViettelNumber(friendPhoneNumber);
            }

            /*if (!mThreadMessage.isStranger()) {
                if (isViettel) {
                    mKeyboardController.getChatFooterView().setupSendBtnAvailable(STATUS_SEND_ALL_VISIBLE);
                } else {
                    PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
                    if (phoneNumber != null && phoneNumber.isViettel() && !phoneNumber.isReeng()) {
                        mKeyboardController.getChatFooterView().setupSendBtnAvailable(STATUS_SEND_SMS_VISIBLE);
                    } else
                        mKeyboardController.getChatFooterView().setupSendBtnAvailable(STATUS_SEND_REENG_VISIBLE);
                }
            } else
                mKeyboardController.getChatFooterView().setupSendBtnAvailable(STATUS_SEND_REENG_VISIBLE);*/
        } /*else {
            mKeyboardController.getChatFooterView().setupSendBtnAvailable(STATUS_SEND_REENG_VISIBLE);
        }*/
    }

    private void initViewSuggestSticker() {
        StickerCollection collectionDefault = mStickerBusiness.getDefaultCollection();
        if (!mApplication.getReengAccountBusiness().isVietnam() ||
                mThreadMessage.isHideHeaderSuggestSticker() ||
                collectionDefault == null ||
                !collectionDefault.isDownloaded()) {
            mViewSuggestSticker.setVisibility(View.GONE);
        } else {
            final int id = collectionDefault.getServerId();
            ArrayList<StickerItem> defaultStickerItems = mStickerBusiness.getListStickerItemByCollectionId(id);
            if (defaultStickerItems == null || defaultStickerItems.isEmpty()) {
                mViewSuggestSticker.setVisibility(View.GONE);
            } else {
                mViewSuggestSticker.setVisibility(View.VISIBLE);
                int stickerSize = defaultStickerItems.size();
                suggestStrangerSticker = new ArrayList<>();
                suggestStrangerSticker.add(defaultStickerItems.get(stickerSize > 2 ? 2 : stickerSize - 1));//index3
                suggestStrangerSticker.add(defaultStickerItems.get(stickerSize > 7 ? 7 : stickerSize - 1));//index8
                suggestStrangerSticker.add(defaultStickerItems.get(stickerSize > 8 ? 8 : stickerSize - 1));//index9
                ImageLoaderManager.getInstance(mApplication).displayStickerImage(mSticker1, id,
                        suggestStrangerSticker.get(0).getItemId());
                ImageLoaderManager.getInstance(mApplication).displayStickerImage(mSticker2, id,
                        suggestStrangerSticker.get(1).getItemId());
                ImageLoaderManager.getInstance(mApplication).displayStickerImage(mSticker3, id,
                        suggestStrangerSticker.get(2).getItemId());
            }
        }
    }

    public void setViewHeaderProfile() {
        if (mThreadMessage == null) return;
        if (mMessageArrayList == null || mMessageArrayList.isEmpty()) {
            rltNoMessage.setVisibility(View.VISIBLE);
        } else {
            rltNoMessage.setVisibility(View.GONE);
        }
        // an truoc khi request API check metphone
        txtRqMetphone.setVisibility(View.GONE);
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
            mLayoutIntroGroup.setVisibility(View.GONE);
            PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
            NonContact mNonContact = mContactBusiness.getExistNonContact(friendPhoneNumber);
            StrangerPhoneNumber stranger = mThreadMessage.getStrangerPhoneNumber();
            cState = mMessageBusiness.getStatePresenceFriend(mPhoneNumber, mNonContact);
            checkCarrier(friendPhoneNumber);
            if (cState == -1)
                cState = mContactBusiness.getCStateMessage(mPhoneNumber, mNonContact);
            if (mPhoneNumber != null) {
                urlAvatarTmp = mAvatarBusiness.getAvatarUrl(mPhoneNumber.getLastChangeAvatar(),
                        mPhoneNumber.getJidNumber(), size);
                mAvatarBusiness.setPhoneNumberAvatar(mHeaderAvatar, mTvwProfileAvtText, mPhoneNumber, size);
                String status = mPhoneNumber.getStatus();
                if (mPhoneNumber.isReeng()) {
                    mIsReengUser = true;
                    if (status != null && status.length() > 0) {
                        mTvwProfileStatus.setEmoticon(mApplication, status, status.hashCode(), status);
                        mTvwProfileStatus.setVisibility(View.VISIBLE);
                    } else {
                        mTvwProfileStatus.setText("");
                        mTvwProfileStatus.setVisibility(View.GONE);
                    }
                    mLayoutProfile.setVisibility(View.VISIBLE);
                } else if (!mPhoneNumber.isViettel()) {// ko dung reeng, ko phai vt
                    mLayoutProfile.setVisibility(View.GONE);
                    mLlNotifyShareMusic.setVisibility(View.GONE);
                } else {
                    mLayoutProfile.setVisibility(View.GONE);
                    mLlNotifyShareMusic.setVisibility(View.GONE);
                }
            } else {
                if (mThreadMessage.isStranger()) {
                    if (stranger != null) {
                        mAvatarBusiness.setStrangerAvatar(mHeaderAvatar, mTvwProfileAvtText,
                                stranger, stranger.getPhoneNumber(), null, null, size);
                        urlAvatarTmp = mApplication.getAvatarBusiness().getAvatarUrl(stranger.getFriendAvatarUrl(),
                                friendPhoneNumber, size);
                        Log.d(TAG, "stranger != null urlAvatarTmp = " + urlAvatarTmp);
                    } else {
                        mAvatarBusiness.setUnknownNumberAvatar(mHeaderAvatar, mTvwProfileAvtText,
                                friendPhoneNumber, size);
                    }
                    if (mIsReengUser) {
                        mLayoutProfile.setVisibility(View.VISIBLE);
                        mLlNotifyShareMusic.setVisibility(View.VISIBLE);
                    } else {
                        mLayoutProfile.setVisibility(View.GONE);
                        mLlNotifyShareMusic.setVisibility(View.GONE);
                    }
                } else if (mNonContact != null) {
                    mAvatarBusiness.setUnknownNumberAvatar(mHeaderAvatar, mTvwProfileAvtText,
                            mNonContact.getJidNumber(), size);
                    urlAvatarTmp = mAvatarBusiness.getAvatarUrl(mNonContact.getLAvatar(),
                            mNonContact.getJidNumber(), size);
                    Log.d(TAG, "mNonContact != null urlAvatarTmp = " + urlAvatarTmp);
                } else {
                    mAvatarBusiness.setUnknownNumberAvatar(mHeaderAvatar, mTvwProfileAvtText, friendPhoneNumber,
                            size);
                    mLayoutProfile.setVisibility(View.GONE);
                    mLlNotifyShareMusic.setVisibility(View.GONE);
                }

                if (mNonContact != null) {
                    String status = mNonContact.getStatus();
                    if (mNonContact.getState() == Constants.CONTACT.ACTIVE) {   //neu dung mocha
                        mIsReengUser = true;
                        mLayoutProfile.setVisibility(View.VISIBLE);
                        mLlNotifyShareMusic.setVisibility(View.VISIBLE);
                        if (status != null && status.length() > 0) {
                            mTvwProfileStatus.setEmoticon(mApplication, status, status.hashCode(), status);
                            mTvwProfileStatus.setVisibility(View.VISIBLE);
                        } else {
                            mTvwProfileStatus.setText("");
                            mTvwProfileStatus.setVisibility(View.GONE);
                        }
                    } else if (!mNonContact.isViettel()) {// ko
                        // dung reeng, ko phai vt
                        mLlNotifyShareMusic.setVisibility(View.GONE);
                        mLayoutProfile.setVisibility(View.GONE);
                    } else {
                        mLlNotifyShareMusic.setVisibility(View.GONE);
                        mLayoutProfile.setVisibility(View.GONE);
                    }
                } else {
                    mLlNotifyShareMusic.setVisibility(View.GONE);
                    mLayoutProfile.setVisibility(View.GONE);
                }
            }
            if (!TextUtils.isEmpty(getFriendName())) {
                Log.i(TAG, "-------------friendName: " + getFriendName());
                mTvwProfileName.setText(getFriendName());
            } else {
                mTvwProfileName.setText("");
            }
            Log.i(TAG, "thread loadmore: " + mThreadMessage.isLoadMoreFinish());
            if (!mThreadMessage.isLoadMoreFinish()) {
                if (needToShowHeaderProfile) {
                    mLayoutProfile.setVisibility(View.VISIBLE);
                } else {
                    mLayoutProfile.setVisibility(View.GONE);
                }
            }
            initViewSuggestSticker();
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            cState = -1;
            mLayoutProfile.setVisibility(View.GONE);
            mLayoutIntroGroup.setVisibility(View.GONE);

            if (mMessageArrayList == null || mMessageArrayList.isEmpty()) {
                needToShowHeaderProfile = true;
            }
            if (mThreadMessage.isLoadMoreFinish()) {
                mLayoutIntroGroup.setVisibility(View.GONE);
            } else {
                if (needToShowHeaderProfile) {
                    mLayoutIntroGroup.setVisibility(View.GONE);
                } else {
                    mLayoutIntroGroup.setVisibility(View.GONE);
                }
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(mHeaderAvatar, 0,
                    mThreadMessage.getServerId(), null, false);
            cState = -1;
            mLayoutProfile.setVisibility(View.GONE);
            mLayoutIntroGroup.setVisibility(View.GONE);
        } else {
            cState = -1;
            mLayoutProfile.setVisibility(View.GONE);
            mLayoutIntroGroup.setVisibility(View.GONE);
        }
    }

    private void updateBackground() {
        if (mThreadMessage == null) {
            return;
        }
        String background = mThreadMessage.getBackground();
        if (TextUtils.isEmpty(background) &&
                (mThreadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT)) {
            if (mPref.getBoolean(Constants.PREFERENCE.PREF_APPLY_BACKGROUND_ALL, false)) {// apply all
                background = mPref.getString(Constants.PREFERENCE.PREF_DEFAULT_BACKGROUND_PATH, null);
                mThreadMessage.setBackground(background);
            }
        }
        Log.d(TAG, "displayBackgroundOfThreadDetail: updateBackground");
        updateBackgroundUI(background);
    }

    private void setMediaControllerListener() {
        mControlMedia.setStateMusicBarListener(this);
        mControlMedia.setOnClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.media_play) {
                    if (!mApplication.getPlayMusicController().isPlaying()) {
                        audioVoicemailPlayerImpl2.stopVoicemail(false);
                        audioVoicemailPlayerImpl2.resetUI(mThreadDetailAdapter, mChatActivity);
                    } else {
                        audioVoicemailPlayerImpl2.setMusicPlaying(false);
                    }
                    mApplication.getPlayMusicController().toggleMusic();
                    //true - chuyen pause -> play
                    // false - chuyen tu play -> pause
                    mApplication.getPlayMusicController().setStatePauseChangeSong(false);
                } else if (view.getId() == R.id.media_close) {
                    mApplication.getPlayMusicController().closeMusic();
                    mControlMedia.showMediaPlayer(false);
                    //dung cung nghe
                    mMusicBusiness.closeMusic();
                    mChatActivity.trackingEvent(R.string.ga_category_invite_music, R.string
                            .ga_cancel_listening_music, R.string.ga_cancel_listening_music);
                } else if (view.getId() == R.id.media_next) {
                    if (mApplication.getMusicBusiness().isExistListenerRoom()) {// nghe room
                        // cung nghe room thi next bai #
                        mApplication.getPlayMusicController().playNextMusic();
                    } else {
                        goToSelectSongOrShowError(false);
                    }
                }
            }
        });
        MediaModel song = mApplication.getPlayMusicController().getCurrentSong();
        mControlMedia.setInfo(song, mApplication.getPlayMusicController().getStatePlaying(), mThreadMessage);
    }

    private void initSticker() {
        mVoiceStickerPlayer = new VoiceStickerPlayer(mApplication);
        LayoutInflater layoutInflater = LayoutInflater.from(mChatActivity);
        popupViewPreview = layoutInflater.inflate(R.layout.popup_preview_sticker, null);
        popupWindowPreview = new PopupWindow(
                popupViewPreview,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        imgPreview = popupViewPreview.findViewById(R.id.img_sticker_preview);
        mLayoutPopupPreview = popupViewPreview.findViewById(R.id.layout_sticker_preview);
        mProgressPreview = popupViewPreview.findViewById(R.id.progress_loading_sticker);
        popupWindowPreview.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                resetDefaultStickerID();
                mVoiceStickerPlayer.stopVoice();
                isShowPreviewDownloading = false;
            }
        });
        mBuzzStickerController = new BuzzStickerController(mChatActivity, mLvwContent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = savedInstanceState.getInt(ThreadMessageConstant.THREAD_IS_GROUP);
            mForwardingMessage = (ReengMessage) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT
                    .DATA_REENG_MESSAGE);
            arrayMessage = (ReengMessageWrapper) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT
                    .DATA_ARRAY_MESSAGE);
            mStatus = savedInstanceState.getString("ab_status");
        }
        super.onActivityCreated(savedInstanceState);
    }

    private void initKeyboardController(CusRelativeLayout rootView) {
        mKeyboardController = new CusKeyboardController(mApplication, mChatActivity);
        mKeyboardController.initKeyboardController(mChatActivity, rootView,
                rootView.findViewById(R.id.drawer),
                this, this, mGsmMode, this,
                changeListener, this, mThreadType, mThreadId, isViettel);

        mKeyboardController.setChangeNoMessageViewSizeListener(this);

        //TODO
        mKeyboardController.setSendReengClickListener(this);
        mKeyboardController.setOpenCusKeyboarListener(this);
        mKeyboardController.setCloseCusKeyboarListener(this);
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "onDetach");
        if (gifDrawable != null)
            gifDrawable.recycle();
        if (gifDrawableDetail != null)
            gifDrawableDetail.recycle();
        super.onDetach();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        enableClickTag = true;
        isSaveInstance = false;
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mKeyboardController.setRefreshMediaView(true);
//        checkToShowNetworkState();
        XMPPManager.addXMPPConnectivityChangeListener(this);
        ListenerHelper.getInstance().addInitDataListener(this);

        MusicBusiness.addActionMusicResponseListener(this);
        initKeyguardReceiver();
        if (mApplication.isDataReady()) {
            mMessageBusiness.addReengMessageListener(this);
            mMessageBusiness.addConfigGroupListener(this);
            if (mThreadMessage == null) {
                mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
                getFriendPhoneNumber();
            }
            if (!mAppStateManager.isScreenLocker()) {
                drawThreadDetail();
            }
            if (mForwardingMessage != null && mForwardingMessage.isForwardingMessage()) {
                sendForwardingMessage(mForwardingMessage);
            }
            resetDraftMessageWhenShowQuickReply();
            if (arrayMessage != null) {
                shareMessage();
            }
        }
        resetDefaultStickerID();
        mKeyboardController.onResume();
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            initHandleCountDown();
        }
        mApplication.updateCountNotificationIcon();
        handlePinMessage(null, false);
        onProcessConfigStickyBanner();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        InputMethodUtils.hideSoftKeyboard(etMessageContent, mChatActivity);
        XMPPManager.removeXMPPConnectivityChangeListener(this);
        ListenerHelper.getInstance().removeInitDataListener(this);
        mMessageBusiness.removeReengMessageListener(this);
        mMessageBusiness.removeConfigGroupListener(this);
        MusicBusiness.removeActionMusicResponseListener(this);
        unRegisterKeyguardReceiver();
        InAppHelper.getInstance(mApplication).cancelRequestCheckSms();
        ///
        audioVoicemailPlayerImpl2.stopVoicemail(false);

        mIconListener = null;
        ReengNotificationManager.setCurrentUIThreadId(-1);
        stopCountDownTyping();
        if (countDownTimerReceiveTyping != null) {
            countDownTimerReceiveTyping.cancel();
            countDownTimerReceiveTyping = null;
        }
        if (mVoiceStickerPlayer != null && mVoiceStickerPlayer.isPlaying()) {
            mVoiceStickerPlayer.stopVoice();
        }
        dismissStickerPopupWindow();
        if (mBuzzStickerController != null) mBuzzStickerController.hideBuzzSticker();
        if (getMoreMessageTask != null) {
            getMoreMessageTask.cancel(true);
        }
        if (mKeyboardController != null) {
            mKeyboardController.onPause();
        }
        if (mHandlerCountDown != null) {
            mHandlerCountDown.removeCallbacksAndMessages(null);
        }
        mMessageBusiness.freeListMessageProcessInfo();
        if (mThreadMessage != null)
            mThreadMessage.setDraftMessage(EmoticonUtils.getRawTextFromSpan(etMessageContent.getText()).trim());
        if (mVideo != null && mVideo.isPlaying()) {
            Log.i(TAG, "pause video");
            positionSeekVideo = mVideo.getCurrentPosition();
            isPauseVideo = true;
            mVideo.pause();
            setBackgroundImageMediaControll(false);
        }
    }

    public boolean isCustomKeyboardOpened() {
        if (mKeyboardController != null) {
            return mKeyboardController.isOpened();
        } else {
            return false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ThreadMessageConstant.THREAD_ID, mThreadId);
        outState.putInt(ThreadMessageConstant.THREAD_IS_GROUP, mThreadType);
        outState.putSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, mForwardingMessage);
        outState.putSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE, arrayMessage);
        if (mTvStatus != null && mTvStatus.getVisibility() == View.VISIBLE) {
            outState.putString(Constants.MESSAGE.AB_DESC, mTvStatus.getText().toString());
        } else {
            outState.putString(Constants.MESSAGE.AB_DESC, "");
        }
        isSaveInstance = true;
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
        PopupHelper.getInstance().destroyBottomChatOption();
        PopupHelper.getInstance().destroyPopupPlayGif();
        dismissPopupMoreMsgUnread();
        String newDraftMsg = EmoticonUtils.getRawTextFromSpan(etMessageContent.getText()).trim();
        if (TextUtils.isEmpty(draftMessage) && TextUtils.isEmpty(newDraftMsg)) {
            // khong lam gi vi khong thay doi noi dung
        } else {
            if (TextUtils.isEmpty(draftMessage) || !draftMessage.equals(newDraftMsg)) {
                Log.i(TAG, "save draft: " + newDraftMsg);
                mThreadMessage.setDraftMessage(newDraftMsg);
                mThreadMessage.setLastTimeSaveDraft(System.currentTimeMillis());
                mMessageBusiness.updateAndNotifyThreadMessage(mThreadMessage);
            }
        }
        PubSubManager.getInstance(mApplication).startCountDownUnSubRoomChat(mThreadMessage);
        mKeyboardController.onStop();

        if (mThreadMessage != null) {
            Log.i(TAG, "percent: " + positionSeekVideo);
            mThreadMessage.setLastPositionVideo(positionSeekVideo);
        }
        if (mThreadDetailAdapter != null) mThreadDetailAdapter.clearNewMessage();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onDestroyView() {
        Log.i(TAG, "onDestroyView");
        NetworkHelper.setNetworkChangedCallback(null);
        if (mPopupListener != null) {
            mPopupListener.dismissPopup();
            mPopupListener = null;
        }
        if (rlSuggestSendMsgNoInternet != null) {
            rlSuggestSendMsgNoInternet.removeCallbacks(runnableMsgNoInternet);
        }
        ListenerHelper.getInstance().removeUpdateAllThreadListener(this);
        PopupHelper.getInstance().destroyBottomChatOption();
        PopupHelper.getInstance().destroyPopupPlayGif();
        mKeyboardController.freeController();
        if (mTypingManager != null) {
            mTypingManager.releaseController();
            mTypingManager = null;
        }
        imgPreview.setImageBitmap(null);
        mImgBackground.setImageBitmap(null);
        CountDownInviteManager.getInstance(mApplication).destroyFragment();
        ProgressFileManager.fressSource();
        PlayMusicController.removePlayMusicStateChange(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        ListenerHelper.getInstance().removeConfigChangeListener(this);
        mChatActivity.dismissPrefixDialog();
        mHandler = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {

        Log.i(TAG, "onDestroy");
        if (mConnectingTimer != null) {
            mConnectingTimer.cancel();
            mConnectingTimer = null;
        }
        if (mHideConnectStateTimer != null) {
            mHideConnectStateTimer.cancel();
            mHideConnectStateTimer = null;
        }
        NetworkHelper.setNetworkChangedCallback(null);
        super.onDestroy();
    }

    private void resetDraftMessageWhenShowQuickReply() {
        // reset draft and et content when send text in quickreply
        if (etMessageContent != null && mThreadMessage != null && (mForwardingMessage == null ||
                mForwardingMessage.getMessageType() != ReengMessageConstant.MessageType.text)) {
            String content = etMessageContent.getText().toString();
            String currentDraft = mThreadMessage.getDraftMessage();
            if ((TextUtils.isEmpty(currentDraft) && !TextUtils.isEmpty(content)) ||
                    (!TextUtils.isEmpty(currentDraft) && !currentDraft.equals(draftMessage))) {
                draftMessage = currentDraft;
                if (TextUtils.isEmpty(draftMessage)) {
                    etMessageContent.setText(draftMessage);
                } else {
                    new SetContentDraftAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, draftMessage);
                }
            }
        }
    }

    private void setContentDraftMessage() {
        if (mThreadMessage == null) {
            return;
        }
        draftMessage = mThreadMessage.getDraftMessage();
        Log.i(TAG, "set draft: draftMessage: " + draftMessage);
        if (draftMessage == null || draftMessage.length() <= 0) {
            return;
        }
        if (etMessageContent == null) {
            return;
        }
        Log.i(TAG, "------------- start draft");
        new SetContentDraftAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, draftMessage);
    }

    @Override
    public void stickerClicked(StickerItem stickerItem) {
        showPreviewStickerPopupFromStickerItem(stickerItem);
    }

    private void showPreviewStickerPopupFromStickerItem(StickerItem stickerItem) {
        if (stickerItem == null) {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        final int collectionId = stickerItem.getCollectionId();
        final int itemId = stickerItem.getItemId();
        if (collectionId == preCollectionIDSticker && prePossitionSticker == itemId) {
            dismissStickerPopupWindow();
            sendMessageVoiceSticker(collectionId, itemId);
        } else {
            preCollectionIDSticker = collectionId;
            prePossitionSticker = itemId;
            if (!TextUtils.isEmpty(stickerItem.getType()) && stickerItem.getType().equals(StickerConstant
                    .STICKER_TYPE_GIF)) {
                try {
                    gifDrawable = new GifDrawableBuilder().from(stickerItem.getImagePath()).build();
                    imgPreview.setImageDrawable(gifDrawable);
                    gifDrawable.setLoopCount(200);
                } catch (IOException e) {
                    Log.e(TAG, "Exception", e);
                }
            } else {
                ImageLoaderManager.getInstance(mApplication).displayStickerImage(imgPreview, collectionId, itemId);
            }
            dismissPopupMoreMsgUnread();
            if (popupWindowPreview != null) {
                if (!popupWindowPreview.isShowing()) {
                    popupWindowPreview.showAtLocation(etMessageContent, Gravity.CENTER, 0, 0);
                }
                mLayoutPopupPreview.setVisibility(View.VISIBLE);
                mProgressPreview.setVisibility(View.GONE);
                mVoiceStickerPlayer.playVoiceSticker(collectionId, itemId);
                //shake when click buzz sticker
                if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID
                        && itemId == EmoticonUtils.getGeneralStickerDefault()[EmoticonUtils.BUZZ_STICKER_POSITION]
                        .getItemId()) {
                    if (mHandler == null) {
                        mHandler = new Handler();
                    }
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            WindowShaker.shake(mChatActivity, option);
                        }
                    }, 200);
                }
                popupViewPreview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissStickerPopupWindow();
                        sendMessageVoiceSticker(collectionId, itemId);
                    }
                });
            }
        }
    }

    private void dismissStickerPopupWindow() {
        if (popupWindowPreview != null && popupWindowPreview.isShowing()) {
            popupWindowPreview.dismiss();
        }
    }

    private void resetDefaultStickerID() {
        preCollectionIDSticker = DEFAULT_PREVIOUS_STICKER;
        prePossitionSticker = DEFAULT_PREVIOUS_STICKER;
    }

    //Dung trong truong hop QuickReply o trong man hinh chat chi tiet
    @Override
    public void onKeyguardGone() {
        Log.i(TAG, "onKeyguardGone");
        if (mApplication.isDataReady()) {
            drawThreadDetail();
        }
    }

    @Override
    public void onNetworkChanged(final boolean isNetworkAvailable) {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideConnectStateView();
            }
        });
        if (mHandler == null) mHandler = new Handler();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                hideConnectStateView();
            }
        });
        mHandler.removeCallbacks(checkNetWork);
        mHandler.postDelayed(checkNetWork, TIME_DELAY_CHECK_NETWORK);
    }

    @Override
    public void onResponseLeaveRoomChat() {
        Log.i(TAG, "onResponseLeaveRoomChat");
        mChatActivity.hideLoadingDialog();
        mMessageBusiness.removePacketIdWhenDeleteRoom(mThreadMessage);
        mMessageBusiness.deleteThreadMessage(mThreadMessage);
        mChatActivity.goPreviousOrHomeWhenBackPress();
    }

    @Override
    public void onErrorLeave(int errorCode, String msg) {
        mChatActivity.hideLoadingDialog();
        mChatActivity.showToast(R.string.e601_error_but_undefined);
        mChatActivity.goPreviousOrHomeWhenBackPress();
        Log.i(TAG, "onErrorLeave");
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        setBackgroundImageMediaControll(false);
        mThreadMessage.setLastPositionVideo(0);
        positionSeekVideo = 0;
        mLayoutControlVideo.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        if (what == ERROR_SUPPORT_MEDIA && extra == 0) {
            showError2();
            return true;
        }
        Log.i(TAG, "onError" + what + "/" + extra);
        showError();
        // showError();
        return true;
    }

    private void showError2() {
        Log.i(TAG, "showError2");
        if (mAlertDialog != null && mAlertDialog.isShowing()) return;
        AlertDialog.Builder adb = new AlertDialog.Builder(mChatActivity);
        adb.setMessage("" + getString(R.string.video_load_fail_support));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mChatActivity.finish();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        mAlertDialog = adb.create();
        mAlertDialog.show();
    }

    private void showError() {
        Log.i(TAG, "showError");
        if (mAlertDialog != null && mAlertDialog.isShowing()) return;
        AlertDialog.Builder adb = new AlertDialog.Builder(mChatActivity);
        adb.setMessage("" + getString(R.string.video_load_fail_support2));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mChatActivity.finish();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        mAlertDialog = adb.create();
        mAlertDialog.show();
    }

    private void showErrorWithoutFinish() {
        Log.i(TAG, "showErrorWithoutFinish");
        if (mAlertDialog != null && mAlertDialog.isShowing()) return;
        AlertDialog.Builder adb = new AlertDialog.Builder(mChatActivity);
        adb.setMessage("" + getString(R.string.video_load_fail_support2));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onCloseWatchVideo();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        mAlertDialog = adb.create();
        mAlertDialog.show();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.i(TAG, "onPrepared");
        isPrepared = true;
        if (isPauseVideo) return;
        mImgThumbVideo.setVisibility(View.GONE);
        mediaPlayer.start();
        msToClockTime = TimeHelper.msToClockTime(mVideo.getDuration());
        setBackgroundImageMediaControll(true);
        mTvwDurationVideo.setText(msToClockTime);
        mProgressLoadingVideo.setVisibility(View.GONE);
        mImgTogglePlay.setVisibility(View.VISIBLE);
        if (positionSeekVideo == 0) {
            mThreadMessage.setLastPositionVideo(0);
        } else {
            mHandler.removeCallbacks(mUpdateTimeTask);
            mVideo.seekTo(positionSeekVideo);
        }
        updateProgressBar();
        startHideBar();
    }

    /*@Override
    public void onEditNonContact(boolean isSuccess, HashMap<String, String> listChangeSuccess, HashMap<String, String> listNonContactChange) {
        processEditMember(listChangeSuccess);

        mMessageBusiness.updateAllMessageOldPrefixNumber(mThreadMessage, listChangeSuccess);
        drawThreadDetail();
    }*/

    private void startHideBar() {
        mLayoutControlVideo.setVisibility(View.VISIBLE);
        if (cdHide == null) {
            initCountDownHideBar();
        } else if (isCountingHidePlayer) {
            cdHide.cancel();
            Log.i(TAG, "cdHide.cancel");
        }
        cdHide.start();
        isCountingHidePlayer = true;
        Log.i(TAG, "cdHide.start");
    }

    private void initCountDownHideBar() {
        Log.i(TAG, "initCountDownHideBar");
        cdHide = new CountDownTimer(Constants.ONMEDIA.TIME_DELAY_HIDE_PLAYER_VIDEO,
                Constants.ONMEDIA.TIME_DELAY_HIDE_PLAYER_VIDEO) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "onFinish");
                if (mVideo != null && mVideo.isPlaying()) {
                    isCountingHidePlayer = false;
                    mLayoutControlVideo.setVisibility(View.GONE);
                }
            }
        };
    }

    private void fullScreen() {
        mChatActivity.hideKeyboard();
        mChatActivity.setEnableBothSideSlideMenu(false);
        startHideBar();
        mChatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        mChatActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        isFullScreen = true;
        rotateScreen();
        mImgToggleScreen.setImageResource(R.drawable.ic_toogle_video_small);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mViewVideo
                .getLayoutParams();
        params.width = mApplication.getHeightPixels();
        params.height = mApplication.getWidthPixels();
        params.leftMargin = 0;
        params.topMargin = 0;
        params.bottomMargin = 0;
        params.rightMargin = 0;
        Log.i(TAG, "VideoPlayerActivity.fullScreen() p " + mApplication.getWidthPixels() + "/ " + mApplication
                .getHeightPixels());
        mViewVideo.setLayoutParams(params);
        mViewVideo.requestLayout();
        abView.setVisibility(View.GONE);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void rotateScreen() {
        if (isFullScreen) {
            if (defaultScreenOrientation != -1) {
                defaultScreenOrientation = mChatActivity.getRequestedOrientation();
            }
            if ((android.provider.Settings.System.getInt(mChatActivity.getContentResolver(),
                    Settings.System.ACCELEROMETER_ROTATION, 0) == 1) && defaultScreenOrientation != ActivityInfo
                    .SCREEN_ORIENTATION_PORTRAIT) {
                mChatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mChatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                    }
                }, 1000);
            } else {
                mChatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else if ((android.provider.Settings.System.getInt(mChatActivity.getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0) == 1)) {
            mChatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mChatActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                }
            }, 1000);
        } else {
            mChatActivity.setRequestedOrientation(defaultScreenOrientation);
        }
    }

    @Override
    public void OnClickUser(String numberJid, String friendName) {
        Log.i(TAG, "OnclickUser: " + numberJid + " enableclick: " + enableClickTag);
        if (enableClickTag) {
            enableClickTag = false;
            if (numberJid.equals(userNumber)) {
                NavigateActivityHelper.navigateToMyProfile(mChatActivity);
            } else {
                PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(numberJid);
                if (mPhoneNumber != null) {
                    mListener.navigateToContactDetailActivity(mPhoneNumber.getId());
                } else {
                    StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().
                            getExistStrangerPhoneNumberFromNumber(numberJid);
                    if (strangerPhoneNumber != null) {
                        mListener.navigateToStrangerDetail(strangerPhoneNumber, null, null);
                    } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                        mListener.navigateToNonContactDetailActiviy(numberJid);
                    } else if (!TextUtils.isEmpty(numberJid) && !TextUtils.isEmpty(friendName)) {
                        mListener.navigateToStrangerDetail(null, numberJid, friendName);
                    } else {
                        mListener.navigateToNonContactDetailActiviy(numberJid);
                    }
                }
            }
        } else {
            enableClickTag = true;
        }
    }

    @Override
    public void onConfigStickyBannerChanged() {
        onProcessConfigStickyBanner();
    }

    @Override
    public void onConfigTabChange() {

    }

    @Override
    public void onConfigBackgroundHeaderHomeChange() {
    }

    @Override
    public void onEditContact(boolean isSuccess, final HashMap<String, String> newNumber,
                              final HashMap<String, String> listChangeNonContact) {
        if (mThreadMessage != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    PrefixChangeNumberHelper.getInstant(mApplication).processEditMember(newNumber, listChangeNonContact, mThreadMessage);
                    mMessageBusiness.updateThreadMessageDataSource(mThreadMessage);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    mMessageBusiness.updateAllMessageOldPrefixNumber(mThreadMessage, newNumber,
                            new PrefixChangeNumberHelper.UpdateThreadChat() {

                                @Override
                                public void onUpdateThreadChat(int threadId) {
                                    Log.i(TAG, "-------PREFIX----- onUpdateThreadChat: " + threadId + " current: " + mThreadId);
                                    if (threadId == mThreadId) {
                                        if (mHandler != null) {
                                            mHandler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    drawThreadDetail(true);
                                                }
                                            }, DELAY_DRAW_DETAIL);
                                        }

                                    }
                                    mChatActivity.hideLoadingDialog();
                                    if (!newNumber.isEmpty())
                                        mChatActivity.showToast(mRes.getString(R.string.change_prefix_update_success));
                                }
                            });
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onUpdateListThread(HashSet<ThreadMessage> updatedThread) {
        if (updatedThread.contains(mThreadId))
            mThreadDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public void onReactImageClick(ReengMessage reengMessage, ReengMessageConstant.Reaction reaction) {
        mMessageBusiness.sendReactionMessage(mChatActivity, reengMessage, reaction, mThreadMessage);
    }

    private void setEnableOptionAddContact() {
        // nhan dc su kien thay doi contact nhung chua load xong message
        if (mThreadMessage == null || mMessageArrayList == null) {
            return;
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            // thread 1-1, so ko luu danh ba
            if (mMessageBusiness.checkNumberThreadNonContact(mThreadMessage)) {
                mContactBusiness.getInfoNumber(friendPhoneNumber);
                // an btn add contact neu la lam quen hoac chat nguoi la
                // luôn luôn ẩn vì đang gặp lỗi ơ luồng này
               // showBtnAddContact = !mThreadMessage.isStranger();
                showBtnAddContact = false;
            } else {   // truong hop so co luu danh ba. vao man hinh chat lan dau
                showBtnAddContact = false;
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// group
            if (!mThreadMessage.isLoadNonContact()) {
                mContactBusiness.getInfoListNumber(mContactBusiness.getListNumberUnknownFromMembersGroup
                                (mThreadMessage.getPhoneNumbers()),
                        new ContactRequestHelper.onResponse() {
                            @Override
                            public void onResponse() {
                                mThreadMessage.setLoadNonContact(true);
                            }
                        });
            }
            showBtnAddContact = false;
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {// group
            mContactBusiness.getInfoListNumber(mContactBusiness.
                    getListNumberUnknownFromMembersGroup(mThreadMessage.getPhoneNumbers()), null);
            showBtnAddContact = false;
        } else {// offical
            showBtnAddContact = false;
        }
        showLoadMoreAndAddContact(mMessageArrayList.size());
    }

    private void setVisibleFooterAcceptStranger() {
        if (mThreadMessage == null) {
            mViewAcceptStranger.setVisibility(View.GONE);
            Log.i(TAG, "setVisibleFooterAcceptStranger thread null");
            return;
        }
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                && mThreadMessage.isStranger()) {
            if (mThreadMessage.getStrangerPhoneNumber() != null &&
                    mThreadMessage.getStrangerPhoneNumber().getStrangerType() == StrangerPhoneNumber.StrangerType
                            .other_app_stranger &&
                    mThreadMessage.getStrangerPhoneNumber().getState() == StrangerPhoneNumber.StateAccept.not_accept) {
                mViewAcceptStranger.setVisibility(View.VISIBLE);
            } else {
                mViewAcceptStranger.setVisibility(View.GONE);
            }
        } else {
            mViewAcceptStranger.setVisibility(View.GONE);
        }
    }

    private void showLoadMoreAndAddContact(int totalMessageLoaded) {
        // room chat khong hien load more
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT ||
                mThreadMessage.isLoadMoreFinish() || totalMessageLoaded < Constants.MESSAGE.MESSAGE_INCRE_SIZE) {
            mBtnLoadMore.setVisibility(View.GONE);
            showBtnAddContact();
        } else {
            mBtnLoadMore.setVisibility(View.VISIBLE);
        }
    }

    private void showBtnAddContact() {
        if (showBtnAddContact) {
            mLlAddContact.setVisibility(View.VISIBLE);
            mTvwAddContact.setVisibility(View.VISIBLE);
            mBtnAddContact.setVisibility(View.VISIBLE);
            drawActionBlock();
        } else {
            mLlAddContact.setVisibility(View.GONE);
            mTvwAddContact.setVisibility(View.GONE);
            mBtnAddContact.setVisibility(View.GONE);
        }
    }

    /**
     * check load detail khi vao thread lan dau tien
     *
     * @param isHuman neu nguoi dung chu dong load more thi ko check, nguoc lai chi chay lan dau tien
     */
    private void checkLoadMoreOrLoadDetail(boolean isHuman) {
        needToShowHeaderProfile = false;
        if (mThreadMessage == null) return;
        Log.i(TAG, "[check_unread] checkLoadMoreOrLoadDetail: isHuman " + isHuman + " isLoadDetail: " +
                mThreadMessage.isLoadDetail());
        // thread da load detail thi ko load lai nua
        int numberUnread = mThreadMessage.getNumOfUnreadMessage();
        if (!isHuman && mThreadMessage.isLoadDetail()) {
            needToShowHeaderProfile = true;
            if (numberUnread > 0) {
                mMessageBusiness.markAllMessageIsReadAndCheckSendSeen(mThreadMessage, mThreadMessage.getAllMessages());
                checkAutoPlayVideoOnFirstTime();// vao thread co tin chua doc da load detail roi
                if (isCreated)
                    scrollListViewTo(numberUnread);
            } else if (isCreated) {
                scrollListViewToBottom();
            }
            checkShowHeaderProfile();
            return;
        }
        // room chat da load detail thi ko load more
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && mThreadMessage.isLoadDetail()) {
            checkAutoPlayVideoOnFirstTime();// room chat
            scrollListViewToBottom();
            return;
        }
        // thread chua load detail hoac view load more visible thi load

        if ((!mThreadMessage.isLoadDetail() || mBtnLoadMore.getVisibility() == View.VISIBLE)) {
            //if (isCreated && numberUnread <= 0) {
            if (isCreated) {
                scrollListViewToBottom();
            }
            getMoreMessageTask = new GetMoreMessageTask();
            if (!mThreadMessage.isLoadDetail()) {// chua load detail
                int limitSize = Constants.MESSAGE.MESSAGE_LIMIT;
                if (numberUnread > Constants.MESSAGE.MESSAGE_LIMIT) {
                    limitSize = numberUnread;
                }
                limitSize = limitSize - mThreadMessage.getAllMessages().size();
                if (limitSize < 0) {
                    limitSize = 0;
                }
                getMoreMessageTask.setIncreSize(limitSize);
            } else {// da load detail
                getMoreMessageTask.setIncreSize(Constants.MESSAGE.MESSAGE_INCRE_SIZE);
            }
//            getMoreMessageTask.execute();
            getMoreMessageTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (isCreated) {
            if (numberUnread > 0 && mThreadMessage.isLoadDetail()) {
                scrollListViewTo(numberUnread);
            } else {
                scrollListViewToBottom();
            }
        }
    }

    private void checkShowHeaderProfile() {
        if (mThreadMessage != null && mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            if (mIsReengUser) {
                mRltProfile.setVisibility(View.GONE);
                mLayoutProfile.setVisibility(View.VISIBLE);
            } else {
                mLayoutProfile.setVisibility(View.GONE);
            }
        }
    }

    private void drawThreadDetail() {
        drawThreadDetail(false);
    }

    private void drawThreadDetail(boolean resub) {
        Log.d(TAG, "drawThreadDetail: ");
        // dang ky subscribe number
        PubSubManager pubSubManager = PubSubManager.getInstance(mApplication);
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            pubSubManager.processSubscribeRoomChat(mThreadMessage, mOfficerAccountRoomChat, followRoomListener);
        } else {
            if (resub) {
                pubSubManager.forceSubscribe(mThreadMessage.getSoloNumber());
            } else
                pubSubManager.pushToSubscribeList(mThreadMessage);
        }
        if (mMessageBusiness.checkShowAlertStranger(mThreadMessage)) {
            mChatActivity.showToast(R.string.alert_stranger_chat);
        }
        mKeyboardController.setStranger(mThreadMessage != null && mThreadMessage.isStranger());
        setDataAndDraw(mThreadMessage != null & mThreadMessage.isLoadDetail() & mThreadMessage.getNumOfUnreadMessage
                () > 0);
        if (mTypingManager == null)
            mTypingManager = new TypingManager(mApplication);
        mTypingManager.initViewController(mChatActivity, mThreadMessage, mViewTypingManager, etMessageContent, this);
        handleChangePrivate(mThreadMessage);
        setEnableOptionAddContact();
        setVisibleFooterAcceptStranger();
        drawActionbar(null);
        drawAlertStranger();
        ReengNotificationManager.setCurrentUIThreadId(mThreadId);
        mIconListener = this;
        playStickerWhenCreateThread();
        checkLoadMoreOrLoadDetail(false);
        //set listview POSITION
        checkLastVideo();
        checkThreadEncrypt();
    }

    public void checkThreadEncrypt() {
        if (mThreadMessage != null && mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT)
            if (mThreadMessage.isEncryptThread()) {
                mChatActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
            } else
                mChatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
    }

    public void setListMemberTag() {
        if (mThreadMessage != null && mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            //TODO list memberfd
            adapterUserTag.setListObject(getGroupFriendList());
            adapterUserTag.notifyDataSetChanged();
            etMessageContent.setDropDownAnchor(R.id.person_chat_detail_input_text);
        }
    }

    private void checkLastVideo() {
        if (mThreadMessage.getLastPositionVideo() == 0 ||
                mThreadMessage.getMediaModel() == null) {
            return;
        }
        if (mThreadMessage.getLastPositionVideo() > 0) {
            isPauseVideo = true;
        }
        if (isPauseVideo) {
            if (mThreadMessage.getMediaModel() != null) {
                mImgThumbVideo.setVisibility(View.VISIBLE);
                Glide.with(mApplication).load(mThreadMessage.getMediaModel().getImage()).into(mImgThumbVideo);
//                mApplication.getUniversalImageLoader().displayImage(mThreadMessage.getMediaModel().getImage(),
//                        mImgThumbVideo);
            }
            positionSeekVideo = mThreadMessage.getLastPositionVideo();
            String info;
            if (TextUtils.isEmpty(mThreadMessage.getMediaModel().getSinger())) {
                info = mThreadMessage.getMediaModel().getName();
            } else {
                info = mThreadMessage.getMediaModel().getName() + " - " + mThreadMessage.getMediaModel().getSinger();
            }
            mTvwTitleVideo.setText(info);
            mViewVideo.setVisibility(View.VISIBLE);
            mProgressLoadingVideo.setVisibility(View.GONE);
            mVideo.requestFocus();
            mLayoutControlVideo.setVisibility(View.VISIBLE);
            return;
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                playVideo(mThreadMessage.getMediaModel(), mThreadMessage.getLastPositionVideo());
            }
        });
    }

    private void notifyDataAdapter() {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mMessageArrayList.size() == 0){
                    rltNoMessage.setVisibility(View.VISIBLE);
                } else {
                    rltNoMessage.setVisibility(View.GONE);
                }
                mThreadDetailAdapter.notifyDataSetChanged();
            }
        });
    }

    private void notifyChangeAndSetSelection() {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initViewSuggestSticker();
                mThreadDetailAdapter.notifyDataSetChanged();
                scrollListViewToBottom();
            }
        });
        Log.i(TAG, "end notifyChangeAndSetSelection");
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        //        getStateConnectionHeight(rootView);
        long beginTime = System.currentTimeMillis();
        getMediaControlHeight(rootView);
        this.mViewParent = rootView;
        mHeaderAddContact = inflater.inflate(R.layout.header_add_contact, null);
        mViewSuggestSticker = mHeaderAddContact.findViewById(R.id.sticker_suggest_rl);
        mSticker1 = mViewSuggestSticker.findViewById(R.id.sticker_1);
        mSticker2 = mViewSuggestSticker.findViewById(R.id.sticker_2);
        mSticker3 = mViewSuggestSticker.findViewById(R.id.sticker_3);

        mLlAddContact = mHeaderAddContact.findViewById(R.id.add_contact_layout);
        mTvwAddContact = mHeaderAddContact.findViewById(R.id.add_contact_title);
        mBtnAddContact = mHeaderAddContact.findViewById(R.id.add_contact_button);
        mBtnLoadMore = mHeaderAddContact.findViewById(R.id.load_more_button);
        mLlNotifyShareMusic = mHeaderAddContact.findViewById(R.id.layout_notify_share_music);
        mBtnInviteSMS = mHeaderAddContact.findViewById(R.id.layout_header_invite_friend);
        //===============footer typing & seen
        mFooterStateSeen = inflater.inflate(R.layout.footer_state_seen, null);
        Log.d(TAG, "[perform] - footer and header take " + (System.currentTimeMillis() - beginTime) + "ms");
        beginTime = System.currentTimeMillis();
        mViewTypingManager = rootView.findViewById(R.id.chat_detail_typing_manager);
        mFooterAlertStranger = rootView.findViewById(R.id.chat_detail_alert_stranger);
        mLlAlertStrangerChat = mFooterAlertStranger.findViewById(R.id.chat_bar_alert_stranger_chat);
        mLlAlertStrangerBlock = mFooterAlertStranger.findViewById(R.id.chat_bar_alert_stranger_block);
        mTvwAlertStrangerBlock = mFooterAlertStranger.findViewById(R.id.chat_bar_alert_stranger_block_label);
        mViewAcceptStranger = mFooterStateSeen.findViewById(R.id.footer_accept_stranger);
        mBtnAcceptStranger = mFooterStateSeen.findViewById(R.id.btn_footer_accept_stranger);
        mLvwContent = rootView.findViewById(R.id.person_chat_detail_content);
        rltNoMessage=rootView.findViewById(R.id.rltNoMessage);
        etMessageContent = rootView.findViewById(R.id.person_chat_detail_input_text);
        etMessageContent.setUseJid(true);
//        etMessageContent.setShowDropDownAlwaysAbove(true);

        abView = mChatActivity.getToolBarView();
        mChatActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_message_detail, null));
        mHeaderAvatar = abView.findViewById(R.id.ab_avatar);
        mTvName = abView.findViewById(R.id.ab_title);
        mTvStatus = abView.findViewById(R.id.ab_status_text);
        mImgRoomStar = abView.findViewById(R.id.img_room_star);
        mImgMenuAttach = abView.findViewById(R.id.message_menu_attach);
        mImgMenuCall = abView.findViewById(R.id.message_menu_call);
        mImgMenuVideoCall = abView.findViewById(R.id.message_menu_video_call);
        mImgMenuStatus = abView.findViewById(R.id.message_menu_status);
        mImgAddFriend = abView.findViewById(R.id.message_menu_add_friend);
        mViewConnectivityStatus = rootView.findViewById(R.id.connectivity_status_layout);
        mViewBackLayout = abView.findViewById(R.id.ab_back_layout);
        ab_back_btn = abView.findViewById(R.id.ab_back_btn);
        mViewAbProfile = abView.findViewById(R.id.ab_profile_chat_layout);
        //profile trong chat detail
        mLayoutProfile = mHeaderAddContact.findViewById(R.id.layout_header_profile);
        mRltProfile = mHeaderAddContact.findViewById(R.id.rlt_header_profile);
        //mImgProfileAvatar = mHeaderAddContact.findViewById(R.id.img_profile_avatar);
        txtRqMetphone = mHeaderAddContact.findViewById(R.id.txtRqMetphone);
        mTvwProfileAvtText = mHeaderAddContact.findViewById(R.id.mTvwProfileAvtText);
        tvw_avatar_profile = mHeaderAddContact.findViewById(R.id.tvw_avatar_profile);
        mTvwProfileName = mHeaderAddContact.findViewById(R.id.tvw_profile_name);
        mTvwProfileStatus = mHeaderAddContact.findViewById(R.id.tvw_profile_status);
        //init header intro group
        mImgFirstIntro = mHeaderAddContact.findViewById(R.id.img_first_intro);
        mImgSecondIntro = mHeaderAddContact.findViewById(R.id.img_second_intro);
        mImgThirdIntro = mHeaderAddContact.findViewById(R.id.img_third_intro);
        mImgBgFristIntro = mHeaderAddContact.findViewById(R.id.img_bg_first_intro);
        mImgBgSecondIntro = mHeaderAddContact.findViewById(R.id.img_bg_second_intro);
        mImgBgThirdIntro = mHeaderAddContact.findViewById(R.id.img_bg_third_intro);
//        mImgSecondIntro.setColorFilter(ContextCompat.getColor(mChatActivity, R.color.white));
        mTvwFirstIntro = mHeaderAddContact.findViewById(R.id.tvw_first_intro);
        mTvwFirstIntro.setText(TextHelper.fromHtml(mRes.getString(R.string.intro_group_click_headphone)));
        mTvwSecondIntro = mHeaderAddContact.findViewById(R.id.tvw_second_intro);
        mTvwSecondIntro.setText(TextHelper.fromHtml(mRes.getString(R.string.intro_group_reply)));
        mTvwThirdIntro = mHeaderAddContact.findViewById(R.id.tvw_third_intro);
        mTvwThirdIntro.setText(TextHelper.fromHtml(mRes.getString(R.string.intro_group_create_vote)));
        mLayoutFirstIntro = mHeaderAddContact.findViewById(R.id.layout_first_intro);
        mLayoutSecondIntro = mHeaderAddContact.findViewById(R.id.layout_second_intro);
        mLayoutThirdIntro = mHeaderAddContact.findViewById(R.id.layout_third_intro);
        mLayoutIntroGroup = mHeaderAddContact.findViewById(R.id.layout_intro_group);
        //end init view header intro group

        mImgBackground = rootView.findViewById(R.id.layout_background);
        popupViewMoreMsgUnred = rootView.findViewById(R.id.layout_more_msg_unread);
        mLayoutProfile.setVisibility(View.GONE);
        mLayoutIntroGroup.setVisibility(View.GONE);
        popupViewMoreMsgUnred.setVisibility(View.GONE);
        Log.d(TAG, "[perform] - findView take " + (System.currentTimeMillis() - beginTime) + "ms");
        // an mHeaderAddContact vi khong co trong design
//        mLvwContent.addHeaderView(mHeaderAddContact, null, false);
        mViewAcceptStranger.setVisibility(View.GONE);
        //============footer typing
        mLvwContent.addFooterView(mFooterStateSeen, null, false);
        mControlMedia = new ControlMedia(rootView.findViewById(R.id.media_player), mApplication);
        if (DeviceHelper.isTablet(mChatActivity)) {
            etMessageContent.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        } else if (SettingBusiness.getInstance(mChatActivity).isSetupKeyboardSend()) {
            etMessageContent.setEditerAction(true, EditorInfo.IME_ACTION_SEND);
        } else {
            etMessageContent.setEditerAction(false, EditorInfo.IME_ACTION_NONE);
        }


        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT){
            mHeaderAvatar.setVisibility(View.GONE);
        }
        mViewVideo = rootView.findViewById(R.id.layout_video);
        RelativeLayout.LayoutParams paramsRow = (RelativeLayout.LayoutParams) mViewVideo.getLayoutParams();
        Log.i(TAG, "video width: " + paramsRow.width + " video height: " + paramsRow.height);
        paramsRow.width = mApplication.getWidthPixels();
        paramsRow.height = 9 * (paramsRow.width / 16);
        heightVideoView = paramsRow.height;
        Log.i(TAG, "AFTER SET video width: " + paramsRow.width + " video height: " + paramsRow.height);
        mViewVideo.requestLayout();

        mLayoutControlVideo = rootView.findViewById(R.id.layout_media_control);
        mVideo = rootView.findViewById(R.id.video);
        mImgTogglePlay = rootView.findViewById(R.id.img_control_play);
        mTvwTimeVideo = rootView.findViewById(R.id.tvw_video_time);
        mTvwDurationVideo = rootView.findViewById(R.id.tvw_full_time_video);
        //mSeekBarVideo = (SeekBar) rootView.findViewById(R.id.seekbar_video);
        mProgressPlayVideo = rootView.findViewById(R.id.progress_video);
        mProgressLoadingVideo = rootView.findViewById(R.id.video_progress);
        mTvwTitleVideo = rootView.findViewById(R.id.tvw_title_video);
        mImgCloseVideo = rootView.findViewById(R.id.img_close_video);
        mImgThumbVideo = rootView.findViewById(R.id.img_thumb_video);
        mImgToggleScreen = rootView.findViewById(R.id.img_toggle_screen);

        viewPinMessage = rootView.findViewById(R.id.view_pin_message);
        viewPinMessage.setVisibility(View.GONE);
        mTvwTitlePinMsg = rootView.findViewById(R.id.tvw_title_pin);
        mTvwContentPinMsg = rootView.findViewById(R.id.tvw_content_pin);
        mImgPinMsg = rootView.findViewById(R.id.img_pin_msg);
        mImgThumbPin = rootView.findViewById(R.id.img_thumb_pin);
        mImgClosePinMsg = rootView.findViewById(R.id.img_close_pin);

        rlSuggestSendMsgNoInternet = rootView.findViewById(R.id.rlSuggestSendMsgNoInternet);
        ivCloseMsgNoInternet = rootView.findViewById(R.id.ivClose);
        tvTryNow = rootView.findViewById(R.id.tvTryNow);
        SpannableString content = new SpannableString(mRes.getString(R.string.see_more));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvTryNow.setText(content);

        //mHeaderAvatar.setImageResource(mThreadId); them ảnh ở đây
    }

    private void getMediaControlHeight(View parentView) {
        mMediaControlHeight = mRes.getDimensionPixelSize(R.dimen.detail_media_height);
    }

    private void setAdapter() {
            mThreadDetailAdapter = new ThreadDetailAdapter(mChatActivity,
                    mThreadType, mSmartTextListener, this, this);
            mLvwContent.setAdapter(mThreadDetailAdapter);
            mLvwContent.setSelectionFromTop(mLvwContent.getCount() - 1, -300000);
    }

    private void checkCarrier(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.equals("")) return;
        if (phoneNumber.contains("+855")) {
            phoneNumber = "0" + phoneNumber.substring(4);
        }
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        String finalPhoneNumber = phoneNumber;
        retrofitInstance.checkCarrier(phoneNumber, new ApiCallback<CheckCarrierResponse>() {
            @Override
            public void onResponse(Response<CheckCarrierResponse> response) {
                if (response.body() != null) {
                    String code = response.body().getCode();
                    android.util.Log.d(TAG, "onResponse: " + response.body().getMessage());
                    if ("00".equals(code)) {
                        if (response.body().getData().getMetfone()) {
                            txtRqMetphone.setVisibility(View.GONE);
                        } else {
                            if (mTvStatus.getText().toString().equals(mChatActivity.getString(R.string.message_fake_lastseen))) {
                                mTvStatus.setText(R.string.not_install_camId);
                            }
                            txtRqMetphone.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.e(TAG, "onError: ", error);
            }
        });
    }
    @Override
    public void onClick(View view) {
        if (etMessageContent != null) {
            etMessageContent.dismissDropDown();
        }
        switch (view.getId()) {
            case R.id.add_contact_button:
                if (mListener != null) {
                    mListener.addNewContact(friendPhoneNumber, null);
                }
                break;
            case R.id.message_menu_attach:
                mChatActivity.showViewSetting();
                break;
            case R.id.message_menu_add_friend:
                if (mListener != null) {
                    ArrayList<String> listNumberGroup = mThreadMessage.getPhoneNumbers();
                    mListener.navigateToChooseFriendsActivity(listNumberGroup, mThreadMessage, -2);
                }
                break;
            case R.id.chat_bar_alert_stranger_block:
                if (!mBlockBusines.isBlockNumber(friendPhoneNumber)) {
                    setActionBlock(true, ContactRequestHelper.BLOCK_ALERT_STRANGER);
                } else {
                    setActionBlock(false, ContactRequestHelper.BLOCK_ALERT_STRANGER);
                }
                break;
            case R.id.message_menu_call:
                if (mThreadMessage.isStranger()) {
                    mChatActivity.showToast("tâm sự", Toast.LENGTH_LONG);
                } else {
                    mApplication.getCallBusiness().checkAndStartCall(mChatActivity, mThreadMessage, true);
                }
                break;
            case R.id.message_menu_video_call:
                mApplication.getCallBusiness().checkAndStartVideoCall(mChatActivity, mThreadMessage, true);
                break;
            case R.id.ab_back_layout:
                if (!NetworkHelper.isConnectInternet(mApplication)) {
                    mChatActivity.showToast(mChatActivity.getResources().getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG);
                } else if (!showDialogConfirmLeave()) {
                    mChatActivity.goPreviousOrHomeWhenBackPress();
                }
                break;
            case R.id.ab_back_btn:
                if (!NetworkHelper.isConnectInternet(mApplication)) {
                    mChatActivity.showToast(mChatActivity.getResources().getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG);
                } else if (!showDialogConfirmLeave()) {
                    mChatActivity.goPreviousOrHomeWhenBackPress();
                }
                break;
            case R.id.ab_profile_chat_layout:
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    String number = friendPhoneNumber;
                    PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
                    if (phoneNumber != null) {
                        mListener.navigateToContactDetailActivity(phoneNumber.getId());
                    } else {
                        if (mThreadMessage != null && mThreadMessage.isStranger() && mThreadMessage.getStrangerPhoneNumber() != null) {
                            mListener.navigateToStrangerDetail(mThreadMessage.getStrangerPhoneNumber(), null, null);
                        } else {
                            mListener.navigateToNonContactDetailActiviy(number);
                        }
                    }
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    mChatActivity.showViewManager();
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                    mChatActivity.showViewManager();
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT && mThreadMessage != null) {
//                    mListener.navigateToOfficialDetail(mThreadMessage.getServerId());
                }
                break;
            case R.id.layout_header_invite_friend:
                ArrayList<String> listPhone = new ArrayList<>();
                listPhone.add(friendPhoneNumber);
                InviteFriendHelper.getInstance().inviteFriends(mApplication, mChatActivity,
                        listPhone, false);
                break;
            case R.id.btn_footer_accept_stranger:
                if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                        mThreadMessage.isStranger()) {
                    StrangerPhoneNumber stranger = mThreadMessage.getStrangerPhoneNumber();
                    if (stranger != null && stranger.getStrangerType() == StrangerPhoneNumber.StrangerType
                            .other_app_stranger &&
                            stranger.getState() == StrangerPhoneNumber.StateAccept.not_accept) {
                        mChatActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                        StrangerBusiness.onAcceptStrangeKeengListener listener = new StrangerBusiness.
                                onAcceptStrangeKeengListener() {
                            @Override
                            public void onResponse() {
                                mChatActivity.hideLoadingDialog();
                                setVisibleFooterAcceptStranger();
                            }

                            @Override
                            public void onError(int errorCode) {
                                mChatActivity.hideLoadingDialog();
                                mChatActivity.showToast(mRes.getString(R.string.e601_error_but_undefined), Toast
                                        .LENGTH_LONG);
                            }
                        };
                        mApplication.getStrangerBusiness().requestAcceptStranger(stranger.getPhoneNumber(),
                                stranger.getMyName(), listener);
                    }
                }
                break;
            case R.id.img_profile_avatar:
                String lAvatar = null;
                String strangerAvatarUrl = null;
                PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
                NonContact mNonContact = mContactBusiness.getExistNonContact(friendPhoneNumber);
                StrangerPhoneNumber mStrangerPhoneNumber = mThreadMessage.getStrangerPhoneNumber();
                if (mThreadMessage.isStranger()) {// neu la stranger
                    if (mStrangerPhoneNumber != null) {
                        if (mStrangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType
                                .other_app_stranger) {
                            strangerAvatarUrl = mStrangerPhoneNumber.getFriendAvatarUrl();
                        } else if (mNonContact != null && mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                            lAvatar = mNonContact.getLAvatar();
                        } else {
                            lAvatar = mStrangerPhoneNumber.getFriendAvatarUrl();
                        }
                    } else if (mNonContact != null) {
                        if (mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                            lAvatar = mNonContact.getLAvatar();
                        }
                    }
                } else {
                    if (mPhoneNumber != null && mPhoneNumber.isReeng()) {
                        lAvatar = mPhoneNumber.getLastChangeAvatar();
                    } else if (mPhoneNumber == null && mNonContact != null
                            && mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                        lAvatar = mNonContact.getLAvatar();
                    }
                }
                if (!TextUtils.isEmpty(lAvatar) || !TextUtils.isEmpty(strangerAvatarUrl)) {
                    mApplication.getAvatarBusiness().displayFullAvatar(friendPhoneNumber, lAvatar,
                            getFriendName(), strangerAvatarUrl, urlAvatarTmp);
                }
                break;
            /*case R.id.layout_profile_birthday:
                if (mTvwProfileAge.getText().equals(mBirthDayStr)) {
                    mTvwProfileAge.setText(String.valueOf(mYearOld));
                } else {
                    mTvwProfileAge.setText(mBirthDayStr);
                }
                break;*/
            case R.id.sticker_1:
                if (suggestStrangerSticker.size() < 1) {
                    mChatActivity.showToast(R.string.e601_error_but_undefined);
                } else {
                    showPreviewStickerPopupFromStickerItem(suggestStrangerSticker.get(0));
                }
                mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_suggest_stranger_sticker, "sticker1");
                break;
            case R.id.sticker_2:
                if (suggestStrangerSticker.size() < 2) {
                    mChatActivity.showToast(R.string.e601_error_but_undefined);
                } else {
                    showPreviewStickerPopupFromStickerItem(suggestStrangerSticker.get(1));
                }
                mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_suggest_stranger_sticker, "sticker2");
                break;
            case R.id.sticker_3:
                if (suggestStrangerSticker.size() < 3) {
                    mChatActivity.showToast(R.string.e601_error_but_undefined);
                } else {
                    showPreviewStickerPopupFromStickerItem(suggestStrangerSticker.get(2));
                }
                mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_suggest_stranger_sticker, "sticker3");
                break;
            case R.id.layout_more_msg_unread:
                dismissPopupMoreMsgUnread();
                scrollListViewToBottom();
                break;
            case R.id.layout_notify_share_music:
                goToSelectSongOrShowError(false);
                break;
            case R.id.chat_bar_alert_stranger_chat:
                mMessageBusiness.updateJoinThreadStranger(mThreadMessage);
                drawAlertStranger();
                notifyDataAdapter();
                break;
            case R.id.img_close_video:
                onCloseWatchVideo();
                break;
            case R.id.img_close_pin:
                if (isShowingPinMessage) {
                    if (mThreadMessage.getPinMessage() != null &&
                            !TextUtils.isEmpty(mThreadMessage.getPinMessage().getContent())) {
                        if (mThreadMessage.getPinMessage().getType() == PinMessage
                                .TypePin.TYPE_DEEPLINK.VALUE) {
                            mThreadMessage.setPinMessage("");
                            mMessageBusiness.updateThreadMessage(mThreadMessage);
                            viewPinMessage.setVisibility(View.GONE);
                        } else if (mThreadMessage.getPinMessage().getType() == PinMessage.TypePin.TYPE_MESSAGE.VALUE
                                || mThreadMessage.getPinMessage().getType() == PinMessage.TypePin.TYPE_VOTE.VALUE) {
                            if (mThreadMessage.isPrivateThread() && !mThreadMessage.isAdmin()) {
                                //neu la private thi phai la admin moi dc xoa pin
                                mChatActivity.showToast(R.string.toast_pin_message_only_admin);
                            } else {
                                DialogConfirm dialogConfirm = new DialogConfirm(mChatActivity, true);
                                dialogConfirm.setMessage(mRes.getString(R.string.message_confirm_delete_pin_message));
                                dialogConfirm.setPositiveLabel(mRes.getString(R.string.ok));
                                dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
                                dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                                    @Override
                                    public void onPositive(Object result) {
                                        if (mThreadMessage.getPinMessage().getType() == PinMessage.TypePin.TYPE_VOTE.VALUE) {
                                            onActionPoll(PollObject.STATUS_UNPIN, mThreadMessage.getPinMessage().getTarget(), null);
                                        } else {
                                            mMessageBusiness.sendPinMessage(mThreadMessage, null, mChatActivity, true);
                                            mThreadMessage.setPinMessage("");
//                                            mMessageBusiness.updateThreadMessage(mThreadMessage);
                                            viewPinMessage.setVisibility(View.GONE);
                                        }
                                    }
                                });
                                dialogConfirm.show();
                            }
                        }
                    }
                } else if (currentStickyBanner != null) {
                    showViewPin(false);
                    mApplication.getConfigBusiness().removeConfigStickyBanner(currentStickyBanner);
                }
                break;

            case R.id.ivClose:
                hideSuggestSendMsgNoInternet();
                break;

            case R.id.tvTryNow:
                hideSuggestSendMsgNoInternet();
                showPopupIntroSendMsgNoInternet();
                break;
        }
    }

    private void onActionPoll(final int status, String pollId, final ReengMessage reengmessage) {
        mChatActivity.showLoadingDialog("", R.string.loading);
        PollRequestHelper.getInstance(mApplication).setStatusPoll(mThreadMessage, pollId, status, new ApiCallbackV2<PollObject>() {
            @Override
            public void onSuccess(String msg, PollObject result) throws JSONException {

                PollRequestHelper pollHelper = PollRequestHelper.getInstance(mApplication);
                ReengMessage message = new ReengMessage();
                message.setSender(mApplication.getReengAccountBusiness().getJidNumber());
                message.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                message.setThreadId(mThreadMessage.getId());
                message.setDirection(ReengMessageConstant.Direction.received);
                // set time
                message.setTime(System.currentTimeMillis());
                message.setMessageType(ReengMessageConstant.MessageType.poll_action);

                message.setContent(pollHelper.getContentPollActionFromStatus(status,
                        mApplication.getReengAccountBusiness().getJidNumber(), "", result));
                message.setFileId(result.getPollId());

                if (status == PollObject.STATUS_CLOSE) {
                    message.setSongId(1);
                } else if (status == PollObject.STATUS_PUSH_TOP) {

                } else if (status == PollObject.STATUS_PIN) {

                } else if (status == PollObject.STATUS_UNPIN) {
                    mThreadMessage.setPinMessage("");
                    mMessageBusiness.updateThreadMessage(mThreadMessage);
                    viewPinMessage.setVisibility(View.GONE);
                }
                mApplication.getMessageBusiness().insertNewMessageToDB(mThreadMessage, message);
                mApplication.getMessageBusiness().notifyNewMessage(message, mThreadMessage);

                if (reengmessage != null) {
                    setPinMessage(reengmessage);
                }

            }

            @Override
            public void onError(String s) {
                mChatActivity.showToast(R.string.e601_error_but_undefined);
            }

            @Override
            public void onComplete() {
                mChatActivity.hideLoadingDialog();
            }
        });
    }

    private void setViewListeners() {
        mBtnAddContact.setOnClickListener(this);
        mImgMenuAttach.setOnClickListener(this);
        mImgMenuCall.setOnClickListener(this);
        mImgMenuVideoCall.setOnClickListener(this);
        mImgAddFriend.setOnClickListener(this);
        mViewBackLayout.setOnClickListener(this);
        ab_back_btn.setOnClickListener(this);
        mViewAbProfile.setOnClickListener(this);
        mBtnInviteSMS.setOnClickListener(this);
        mBtnAcceptStranger.setOnClickListener(this);
        mHeaderAvatar.setOnClickListener(this);
        mSticker1.setOnClickListener(this);
        mSticker2.setOnClickListener(this);
        mSticker3.setOnClickListener(this);
        popupViewMoreMsgUnred.setOnClickListener(this);
        mLlAlertStrangerBlock.setOnClickListener(this);
        mLlAlertStrangerChat.setOnClickListener(this);
        mImgCloseVideo.setOnClickListener(this);
        mImgClosePinMsg.setOnClickListener(this);
        ivCloseMsgNoInternet.setOnClickListener(this);
        tvTryNow.setOnClickListener(this);
        setListViewListener();
        setParentLayoutClick();
        setTypingMessageListener();
        setMediaControllerListener();
        setVideoControlListener();
        setViewIntroGroupListener();

        setEnterListener();
    }

    private void setEnterListener() {
        if (mReengAccountBusiness.isEmulatorCam()) {
            etMessageContent.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        // Perform action on key press
                        sendText();
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    private void setVideoControlListener() {
        mImgTogglePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (needPauseVideo) {
                    checkMusicAndSeekVideo();
                } else {
                    if (mVideo == null) return;
                    if (!isPrepared) {
                        playVideo(mThreadMessage.getMediaModel(), mThreadMessage.getLastPositionVideo());
                        return;
                    }
                    if (mVideo.isPlaying()) {
                        mVideo.pause();
                        setBackgroundImageMediaControll(false);
                    } else {
                        if (isPauseVideo) {
                            mImgThumbVideo.setVisibility(View.GONE);
                            mVideo.seekTo(positionSeekVideo);
                        }
                        mVideo.start();
                        isPauseVideo = false;
                        setBackgroundImageMediaControll(true);
                        updateProgressBar();
                        startHideBar();
                        mChatActivity.trackingEvent(R.string.ga_category_watch_video, R.string.ga_action_interaction,
                                R.string.ga_label_click_play_icon_watch_video);
                    }
                }
            }
        });

        mImgToggleScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleScreen(true);
            }
        });

        mVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onclick mVideo");
                if (mVideo != null && mVideo.isPlaying()) {
                    if (mLayoutControlVideo.getVisibility() == View.GONE) {
                        mLayoutControlVideo.setVisibility(View.VISIBLE);
                    }
                    startHideBar();
                }
            }
        });
        mViewVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onclick mViewVideo");
                startHideBar();
            }
        });
    }

    public void toggleScreen(boolean fromClickToggle) {
        if (!isFullScreen) {
            fullScreen();
        } else {
            smallScreen();
        }
    }

    private void smallScreen() {
        mChatActivity.setEnableBothSideSlideMenu(true);
        startHideBar();
        isFullScreen = false;
        rotateScreen();
        mImgToggleScreen.setImageResource(R.drawable.ic_toogle_video_full);
        mChatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mChatActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        RelativeLayout.LayoutParams paramsRow = (RelativeLayout.LayoutParams) mViewVideo.getLayoutParams();
        Log.i(TAG, "video width: " + paramsRow.width + " video height: " + paramsRow.height);
        paramsRow.width = mApplication.getWidthPixels();
        paramsRow.height = 9 * (paramsRow.width / 16);
        mViewVideo.setLayoutParams(paramsRow);
        mViewVideo.requestLayout();
        Log.i(TAG, "AFTER SET video width: " + paramsRow.width + " video height: " + paramsRow.height);
        abView.setVisibility(View.VISIBLE);
        scrollListViewToBottom();
    }

    public void updateProgressBar() {
        mHandler.removeCallbacks(mUpdateTimeTask);
        if (mVideo != null) {
            mHandler.postDelayed(mUpdateTimeTask, Constants.ONMEDIA.DELAY_UPDATE_SEEKBAR);
        }
    }

    private void setBackgroundImageMediaControll(boolean isPlay) {
        if (isPlay) {
            mImgTogglePlay.setImageResource(R.drawable.ic_video_chat_pause);
        } else {
            mImgTogglePlay.setImageResource(R.drawable.ic_video_chat_play);
        }
    }

    private void setViewIntroGroupListener() {
        mLayoutFirstIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIntroGroupChat(0);
            }
        });
        mLayoutSecondIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIntroGroupChat(1);
            }
        });
        mLayoutThirdIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIntroGroupChat(2);
            }
        });
    }

    private void checkToShowNetworkState() {
       /* mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {*/
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            showDisconnectState();
        } else {
            hideConnectStateView();
        }
        /*    }
        }, 10);*/
    }

    private void hideConnectStateView() {
        if (mViewConnectivityStatus != null && mViewConnectivityStatus.getVisibility() == View.VISIBLE) {
            mViewConnectivityStatus.setVisibility(View.GONE);
            mKeyboardController.updateStateConnection(true);
        }
    }

    private void showConnectingState() {
        mViewConnectivityStatus.setVisibility(View.VISIBLE);
        ((TextView) mViewConnectivityStatus
                .findViewById(R.id.connectivity_status)).setText(R.string.connecting);
        mKeyboardController.updateStateConnection(false);
    }

    private void showDisconnectState() {
        if (mViewConnectivityStatus != null && mViewConnectivityStatus.getVisibility() == View.GONE) {
            mViewConnectivityStatus.setVisibility(View.VISIBLE);
            ((TextView) mViewConnectivityStatus
                    .findViewById(R.id.connectivity_status)).setText(R.string.no_connectivity);
            mKeyboardController.updateStateConnection(false);
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                mTvStatus.setVisibility(View.GONE);//an last seen khi mat ket noi
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setListViewListener() {
        OnScrollListener onScrollListener = new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                ThreadDetailFragment.this.currentScrollState = scrollState;
                if (mLvwContent.getFirstVisiblePosition() == 0 &&
                        mLvwContent.getChildAt(0).getTop() >= 0 &&
                        ThreadDetailFragment.this.currentScrollState == AbsListView.OnScrollListener
                                .SCROLL_STATE_IDLE) {
                    // stop scroll on Top listview
                    checkLoadMoreOrLoadDetail(true);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    dismissPopupMoreMsgUnread();
                }
            }
        };
        mLvwContent.setOnScrollListener(mApplication.getPauseOnScrollListener(onScrollListener));
    }

    /**
     * if click on parent layout -> hide emoticon and more option
     */
    private void setParentLayoutClick() {
        mLvwContent.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mBuzzStickerController.hideBuzzSticker();
                dismissStickerPopupWindow();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mChatActivity.setEnableBothSideSlideMenu(true);
                        mDownX = event.getX();
                        mDownY = event.getY();
                        isOnClick = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (isOnClick) {
                            showInfoMsg(!isShowInfo);
                            mKeyboardController.hideSoftAndCustomKeyboard();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mChatActivity.setEnableBothSideSlideMenu(true);
                        if (isOnClick && (Math.abs(mDownX - event.getX()) > SCROLL_THRESHOLD
                                || Math.abs(mDownY - event.getY()) > SCROLL_THRESHOLD)) {
                            isOnClick = false;
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    private void goToSelectSongOrShowError(boolean uploadSong) {
        Log.i(TAG, "goToSelectSongOr ShowError " + mMusicBusiness.getCurrentMusicSessionId());
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        if (!mMusicBusiness.isExistListenMusic()) {//chua cung nghe
            if (uploadSong)
                mChatActivity.dispatchUploadSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
            else
                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
        } else if (mMusicBusiness.isExistListenerGroup() || mMusicBusiness.isExistListenerRoom()) {// nghe cung room,
            // group
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                showToastOrChangeSongGroup(null, uploadSong);
                return;
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                mMusicBusiness.clearSessionAndNotifyMusic();
            }
            if (uploadSong)
                mChatActivity.dispatchUploadSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
            else
                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
        } else {// dang nghe 1-1 nguoi la
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                String currentNumberInRoom = mMusicBusiness.getCurrentNumberFriend();
                showConfirmLeaveAndReInviteOrChangeSong(currentNumberInRoom, null, uploadSong);
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {//group
                showConfirmLeaveAndInviteOrAcceptGroupMusic(mMusicBusiness.getCurrentNumberFriend(), null, uploadSong);
            } else {//room chat
                if (uploadSong)
                    mChatActivity.dispatchUploadSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
                else
                    mChatActivity.dispatchSelectSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
            }
        }
    }

    private void onCloseWatchVideo() {
        if (isFullScreen) {
            smallScreen();
        }
        positionSeekVideo = 0;
        isPauseVideo = false;
        mVideo.stopPlayback();
        mViewVideo.setVisibility(View.GONE);
        mKeyboardController.updateListMessageHeight();
        mThreadMessage.setLastPositionVideo(0);
        mThreadMessage.setMediaModel(null);
        mVideo.setOnPreparedListener(null);
        mVideo.setOnCompletionListener(null);
        mVideo.setOnErrorListener(null);
    }

    private void showInfoMsg(boolean isShow) {
        Log.i(TAG, "ThreadDetailAdapter show info msg");
        isShowInfo = isShow;
        mThreadDetailAdapter.setShowInfoMsg(isShow);
        notifyDataAdapter();
    }

    /**
     * xu ly khi click vao cung nghe khi dang cung nghe voi 1 nguoi
     *
     * @param currentNumber
     * @param message
     */
    private void showConfirmLeaveAndReInviteOrChangeSong(String currentNumber, ReengMessage message, final boolean uploadSong) {
        messageMusicReInvite = message;
        String friendNumber = mThreadMessage.getSoloNumber();
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT || TextUtils.isEmpty(friendNumber)) {
            return;
        }
        mMusicBusiness = mApplication.getMusicBusiness();
        String friendName = mMessageBusiness.getFriendName(friendNumber);

        mMusicBusiness.showConfirmOrNavigateToSelectSong(mChatActivity, mThreadType,
                friendNumber, friendName, new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
                        if (messageMusicReInvite == null) {
                            if (uploadSong)
                                mChatActivity.dispatchUploadSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
                            else
                                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
                        } else {
                            MediaModel songModel = messageMusicReInvite.getSongModel(mApplication.getMusicBusiness());
                            removeMessageAndReinviteMusic(messageMusicReInvite, songModel);
                        }
                    }

                    @Override
                    public void onGotoChange() {
                        if (messageMusicReInvite == null) {
                            if (uploadSong)
                                mChatActivity.dispatchUploadSongIntent(Constants.ACTION.CHANGE_SONG, mThreadId);
                            else
                                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.CHANGE_SONG, mThreadId);
                        } else {
                            MediaModel songModel = messageMusicReInvite.getSongModel(mApplication.getMusicBusiness());
                            mMessageArrayList.remove(messageMusicReInvite);
                            mMessageBusiness.deleteAMessage(mThreadMessage, messageMusicReInvite);
                            notifyDataAdapter();
                            mMessageBusiness.createAndSendMessageChangeMusic(mThreadMessage, songModel,
                                    mChatActivity, false);
                        }
                    }
                });
    }

    /*private boolean showDialogSuggestSms() {

        long timeShowDialog = mApplication.getPref().getLong(PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, 0);
        if (TimeHelper.checkTimeInDay(timeShowDialog)) {
            return false;
        }

        PhoneNumber p = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
        if (p != null) {
            if (!p.isViettel()) return false;
            DialogConfirm dialogRightClick = new DialogConfirm(mChatActivity, true);
            dialogRightClick.setLabel(mRes.getString(R.string.send_sms_without_internet_title));
            dialogRightClick.setMessage(mRes.getString(R.string.send_sms_without_internet_msg));
            dialogRightClick.setPositiveLabel(mRes.getString(R.string.send));
            dialogRightClick.setNegativeLabel(mRes.getString(R.string.skip));
            dialogRightClick.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    mApplication.getPref().edit().putLong(PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, System.currentTimeMillis()).apply();
                    ReengMessage message = mMessageBusiness.insertMessageNotify(mRes.getString(R.string.send_sms_without_internet_content),
                            friendPhoneNumber, mApplication.getReengAccountBusiness().getJidNumber(), mThreadMessage,
                            ReengMessageConstant.READ_STATE_READ, TimeHelper.getCurrentTime());
                    mMessageBusiness.notifyNewMessage(message, mThreadMessage);
                    sendSmsWithoutInternet();
                }
            });
            dialogRightClick.show();

            return true;
        }
        return false;

    }*/

    /**
     * xu ly khi click vao cung nghe group khi dang cung nghe voi 1 nguoi, hoac cung nghe nguoi la
     *
     * @param currentNumber
     * @param message
     */
    private void showConfirmLeaveAndInviteOrAcceptGroupMusic(String currentNumber,
                                                             ReengMessage message,
                                                             final boolean uploadSong) {
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            return;
        }
        messageMusicReInvite = message;
        mMusicBusiness = mApplication.getMusicBusiness();
        mMusicBusiness.showConfirmOrNavigateToSelectSong(mChatActivity, mThreadType,
                mThreadMessage.getServerId(), mThreadMessage.getThreadName(), new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
                        if (messageMusicReInvite == null) {
                            if (uploadSong)
                                mChatActivity.dispatchUploadSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
                            else
                                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
                        } else {
                            MediaModel songModel = messageMusicReInvite.getSongModel(mApplication.getMusicBusiness());
                            removeMessageAndReinviteMusic(messageMusicReInvite, songModel);
                        }
                    }

                    @Override
                    public void onGotoChange() {
                        if (messageMusicReInvite == null) {
                            if (uploadSong)
                                mChatActivity.dispatchUploadSongIntent(Constants.ACTION.CHANGE_SONG, mThreadId);
                            else
                                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.CHANGE_SONG, mThreadId);
                        } else {
                            MediaModel songModel = messageMusicReInvite.getSongModel(mApplication.getMusicBusiness());
                            mMessageArrayList.remove(messageMusicReInvite);
                            mMessageBusiness.deleteAMessage(mThreadMessage, messageMusicReInvite);
                            notifyDataAdapter();
                            mMessageBusiness.createAndSendMessageChangeMusic(mThreadMessage, songModel,
                                    mChatActivity, false);
                        }
                    }
                });
    }

    private void showToastOrChangeSongGroup(ReengMessage message, boolean uploadSong) {
        mMusicBusiness = mApplication.getMusicBusiness();
        if (mMusicBusiness.isShowPlayerGroup(mThreadMessage.getServerId())) {
            if (message == null) {
                mChatActivity.dispatchSelectSongIntent(Constants.ACTION.CHANGE_SONG, mThreadId);
            } else if (message.getSongModel(mMusicBusiness) != null && mMusicBusiness.getCurrentSong() != null &&
                    message.getSongModel(mMusicBusiness).getId().equals(mMusicBusiness.getCurrentSong().getId())) {
                mChatActivity.showToast(R.string.msg_not_change_current_song);
            } else if (mThreadMessage.isAdmin() || message.getMusicState() != ReengMessageConstant
                    .MUSIC_STATE_REQUEST_CHANGE) {
                mMessageArrayList.remove(message);
                mMessageBusiness.deleteAMessage(mThreadMessage, message);
                notifyDataAdapter();
                MediaModel songModel = message.getSongModel(mMusicBusiness);
                boolean isRequestChange = !mThreadMessage.isAdmin();
                mMessageBusiness.createAndSendMessageChangeMusic(mThreadMessage, songModel, mChatActivity,
                        isRequestChange);
            } else {
                mChatActivity.showToast(R.string.msg_not_admin_change_song);
            }
        } else {
            mMusicBusiness.clearSessionAndNotifyMusic();
            mChatActivity.dispatchSelectSongIntent(Constants.ACTION.SELECT_SONG, mThreadId);
        }
    }

    private void removeMessageAndReinviteMusic(ReengMessage oldMessage, MediaModel mediaModel) {
        // xoa tin nhan suggest
        mMessageArrayList.remove(oldMessage);
        mMessageBusiness.deleteAMessage(mThreadMessage, oldMessage);
        notifyDataAdapter();

        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        mMessageBusiness.createAndSendMessageInviteMusic(mThreadMessage, mediaModel, mChatActivity);
    }

    private void showPopupMoreMsgUnread() {
        popupViewMoreMsgUnred.setVisibility(View.VISIBLE);
    }

    private void dismissPopupMoreMsgUnread() {
        popupViewMoreMsgUnred.setVisibility(View.GONE);
    }

    @Override
    public void onSendReengClick() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                && !mThreadMessage.isStranger()
                && mApplication.getConfigBusiness().isEnableSmsNoInternet()
                && mApplication.getReengAccountBusiness().isViettel()
                && !NetworkHelper.isConnectInternet(mApplication)) {
           /* boolean showDialog = showDialogSuggestSms();
            if (!showDialog) {*/
            sendSmsWithoutInternet();
//            }
        } else {
            sendText();
            scrollListViewToBottom();
            stopCountDownTyping();
        }

    }

    private void sendSmsWithoutInternet() {
        StringBuilder sb = new StringBuilder("MOCHA SMS ");
        sb.append(friendPhoneNumber).append(" ");
        sb.append(TextHelper.removeAccent(etMessageContent.getText().toString().trim()));
        NavigateActivityHelper.openAppSMS(mChatActivity, "5005", sb.toString());
    }

    @Override
    public void onSendReengKeyboardClick() {
        Log.d(TAG, "onSendReengKeyboardClick----");
        sendText();
        scrollListViewToBottom();
        stopCountDownTyping();
    }

    @Override
    public void onSendVoicemail(String filePath, int elapsedTimeSecs, String fileName) {
        sendMessageVoiceMail(filePath, fileName, elapsedTimeSecs);
    }

    @Override
    public void onSendImage(String filePath) {
        ArrayList<String> listImages = new ArrayList<>();
        listImages.add(filePath);
        sendMessageImage(listImages);
    }

    @Override
    public void onOpenPreviewMedia() {
        mChatActivity.dispatchPickPictureIntent();
        //        mChatActivity.dispatchPickVideo();
    }

    @Override
    public void onClearReplyClick() {
        Log.d(TAG, "onClearReplyClick ->>");
    }

    @Override
    public void onChatBarClick() {
        dismissStickerPopupWindow();
    }

    @Override
    public void onSwitchSendButtonComplete(boolean isSendSms) {
        isSendSMSout = isSendSms;
        SettingBusiness.getInstance(mApplication).updateAutoSmsoutThread(mThreadId, isSendSms);
    }

    @Override
    public void onChatBarClickBottomAction(int action) {
        if (action == BottomChatOption.Constants.OPTION_ZODIAC) { //TODO divine
            if (!NetworkHelper.isConnectInternet(mApplication)) {
                mChatActivity.showToast(R.string.error_internet_disconnect);
                return;
            }
            mChatActivity.showLoadingDialog(null, R.string.waiting);
            ContactRequestHelper.getInstance(mApplication).getDivineDetail(friendPhoneNumber,
                    mContactBusiness.getFriendBirthdayStr(friendPhoneNumber), new ContactRequestHelper
                            .onDivineResponse() {
                        @Override
                        public void onResponse(String percent, String content) {
                            mChatActivity.hideLoadingDialog();
                            showPopupZodiac(percent, content);
                        }

                        @Override
                        public void onError(int errorCode) {
                            mChatActivity.hideLoadingDialog();
                            mChatActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    });
        } else if (action == BottomChatOption.Constants.OPTION_BPLUS) {
            BankPlusHelper.getInstance(mApplication).checkAndTransferMoney(mChatActivity, friendPhoneNumber,
                    getFriendName());
        } else if (action == BottomChatOption.Constants.OPTION_GIFT_LIXI) {
            /*if (!mReengAccountBusiness.isViettel()) {
                mChatActivity.showToast(R.string.lixi_only_for_viettel);
                return;
            }*/

            ArrayList<String> listFriendJid = new ArrayList<>();
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
                if (phoneNumber != null) {
                    listFriendJid.add(friendPhoneNumber);
                    /*if (phoneNumber.isViettel())
                        listFriendJid.add(friendPhoneNumber);
                    else {
                        mChatActivity.showToast(R.string.lixi_only_for_viettel);
                        return;
                    }*/
                } else {
                    NonContact nonContact = mContactBusiness.getExistNonContact(friendPhoneNumber);
                    if (nonContact != null) {
                        listFriendJid.add(friendPhoneNumber);
                        /*if (nonContact.isViettel())
                            listFriendJid.add(friendPhoneNumber);
                        else {
                            mChatActivity.showToast(R.string.lixi_only_for_viettel);
                            return;
                        }*/
                    } else {
                        listFriendJid.add(friendPhoneNumber);
                        /*if (isViettel)
                            listFriendJid.add(friendPhoneNumber);
                        else {
                            mChatActivity.showToast(R.string.lixi_only_for_viettel);
                            return;
                        }*/
                    }
                }
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                ArrayList<String> listFriend = mThreadMessage.getListAllMemberIncludeAdmin(mApplication);
                if (listFriend != null && !listFriend.isEmpty()) {
                    for (String s : listFriend) {
                        if (!s.equals(mApplication.getReengAccountBusiness().getJidNumber()) /*&& PhoneNumberHelper.getInstant().isViettelNumber(s)*/) {
                            listFriendJid.add(s);
                        }
                    }
                }

            } else {
                mChatActivity.showToast(R.string.e601_error_but_undefined);
                return;
            }

            if (listFriendJid.isEmpty()) {
                mChatActivity.showToast(R.string.e601_error_but_undefined);
            } else {
//                mChatActivity.navigateToSendGiftLixi(listFriendJid, mThreadId);
                BankPlusHelper.getInstance(mApplication).checkAndSendLixi(mChatActivity, listFriendJid, mThreadId);
            }
        }
    }

    private void showPopupZodiac(String percent, String content) {
        Object obj;
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
        if (mPhoneNumber != null) {
            obj = mPhoneNumber;
        } else {
            NonContact mNonContact = mContactBusiness.getExistNonContact(friendPhoneNumber);
            if (mNonContact != null) {
                obj = mNonContact;
            } else {
                StrangerPhoneNumber stranger = mThreadMessage.getStrangerPhoneNumber();
                if (stranger != null) {
                    obj = stranger;
                } else {
                    obj = friendPhoneNumber;
                }
            }
        }
        new PopupZodiac(mChatActivity, obj, percent, content, new PopupZodiac.ZodiacListener() {
            @Override
            public void onShare(Bitmap bitmap) {
                Log.d(TAG, "onShare-");
                mChatActivity.shareImageFacebook(bitmap, mRes.getString(R.string
                        .ga_facebook_label_share_zodiac));
                hideCusKeyboard();
            }

            @Override
            public void onSend(String filePath) {
                Log.d(TAG, "onSend-");
                ArrayList<String> filePaths = new ArrayList<>();
                filePaths.add(filePath);
                sendMessageImage(filePaths);
            }

            @Override
            public void onError() {
                mChatActivity.showToast(R.string.file_not_found_exception);
            }
        }).show();
    }

    /**
     * goi khi Custom Keyboard Open
     */
    @Override
    public void onOpenCusKeyboard() {
        scrollListViewToBottom();
        dismissPopupMoreMsgUnread();
        mChatActivity.setEnableBothSideSlideMenu(false);
        mBuzzStickerController.hideBuzzSticker();
        hideSuggestSendMsgNoInternet();
    }

    @Override
    public void onClickCusKeyboard() {
        Log.i(TAG, "onClickCusKeyboard");
        mBuzzStickerController.hideBuzzSticker();
        dismissStickerPopupWindow();
        dismissPopupMoreMsgUnread();
    }

    @Override
    public void onCloseCusKeyboard() {
        dismissPopupMoreMsgUnread();
        mChatActivity.setEnableBothSideSlideMenu(true);
    }

    @Override
    public void onChangeListMessageSize(final int sizeListMsg) {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sizeListMessage = sizeListMsg;
                Log.d(TAG, "onChangeListMessageSize: " + sizeListMessage);
                ViewGroup.LayoutParams lParams = mLvwContent.getLayoutParams();
                if (mControlMedia.getVisibility() != View.GONE) {
                    sizeListMessage -= mMediaControlHeight;
                } else if (mViewVideo.getVisibility() != View.GONE) {
                    sizeListMessage -= heightVideoView;
                }
                lParams.height = sizeListMessage;
                mLvwContent.setLayoutParams(lParams);
            }
        });

    }

    @Override
    public void onChangeNoMessageViewSize(final int size) {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (rltNoMessage != null && rltNoMessage.getVisibility() == View.VISIBLE) {
                    sizeViewNoMessage = size;

                    ViewGroup.LayoutParams lParams = rltNoMessage.getLayoutParams();
                    if (mControlMedia.getVisibility() != View.GONE) {
                        sizeViewNoMessage -= mMediaControlHeight;
                    } else if (mViewVideo.getVisibility() != View.GONE) {
                        sizeViewNoMessage -= heightVideoView;
                    }

                    lParams.height = sizeViewNoMessage;
                    rltNoMessage.setLayoutParams(lParams);
                }
            }
        });
    }

    private void showFooterReplyMessage(ReengMessage message) {
        reengMesssageReply = message;
        String link = "", filePath = "";
        if (message.getMessageType() == ReengMessageConstant.MessageType.image) {
            link = message.getDirectLinkMedia();
            filePath = message.getFilePath();
        }
        mKeyboardController.showFooterReplyView(message.getSender(), TagHelper.getTextTagCopy(message.getContent(),
                message.getListTagContent(), mApplication), link, filePath);
    }

    private void hideFooterReplyMessage() {
        reengMesssageReply = null;
        mKeyboardController.hideFooterReplyView();
    }

    private void sendText() {
        String mEdtContent = etMessageContent.getText().toString().trim();
        if (mEdtContent.length() > 0) {
            rltNoMessage.setVisibility(View.GONE);
            Spannable contentSpannable = etMessageContent.getText();
            TextHelper.getInstant().removeUnderlines(contentSpannable);

            String messageContent;
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                messageContent = etMessageContent.getTextTag();
                Log.i(TAG, "messageContent: " + messageContent);
            } else {
                messageContent = EmoticonUtils.getRawTextFromSpan(contentSpannable);
            }

            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                messageContent = TextHelper.getInstant().deleteMultiLine(messageContent);
                messageContent = TextHelper.getInstant().filterSensitiveWords(messageContent);
            }
            int contentLength = messageContent.length();
            // show toast ko cho gui
            int MAX_LENGTH_DEFAULT = mReengAccountBusiness.isCambodia()
                    ? Constants.MESSAGE.TEXT_IP_MAX_LENGTH_CAM : Constants.MESSAGE.TEXT_IP_MAX_LENGTH;
            if ((mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && contentLength > Constants.MESSAGE
                    .TEXT_ROOM_CHAT_MAX_LENGTH) ||
                    (mThreadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && mGsmMode && contentLength >
                            Constants.MESSAGE.TEXT_GSM_MAX_LENGTH) ||
                    (mThreadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && !mGsmMode && contentLength >
                            MAX_LENGTH_DEFAULT)) {
                int maxLength;
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    maxLength = Constants.MESSAGE.TEXT_ROOM_CHAT_MAX_LENGTH;
                } else {
                    maxLength = mGsmMode ? Constants.MESSAGE.TEXT_GSM_MAX_LENGTH : MAX_LENGTH_DEFAULT;
                }
                String msgAlert = String.format(getResources().getString(R.string.alert_max_length_msg), maxLength);
                mChatActivity.showToast(msgAlert, Toast.LENGTH_LONG);
            } else {
                //EmoticonManager.getInstance(mChatActivity).addSpannedToEmoticonCache(messageContent, contentSpannable);
                //gui text
                etMessageContent.setText("");
                ReplyMessage replyMessage = null;
                if (reengMesssageReply != null && mKeyboardController.isShowFooterReply()) {
                    replyMessage = new ReplyMessage();
                    replyMessage.setSubType(TextUtils.isEmpty(reengMesssageReply.getDirectLinkMedia()) ? ReengMessagePacket.SubType.text : ReengMessagePacket.SubType.image);
                    replyMessage.setBody(TagHelper.getTextTagCopy(reengMesssageReply.getContent(), reengMesssageReply
                            .getListTagContent(), mApplication));
                    replyMessage.setMember(reengMesssageReply.getSender());
                    replyMessage.setImgLink(reengMesssageReply.getDirectLinkMedia());
                    replyMessage.setFilePath(reengMesssageReply.getFilePath());
                    replyMessage.setMsgId(reengMesssageReply.getPacketId());
                }
                //TODO disable
                /*if (messageContent.startsWith("@@")) {
                    String n = messageContent.substring(2);
                    int number = 10;
                    try {
                        number = Integer.parseInt(n);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                    AutoSendTask autoSendTask = new AutoSendTask(number);
                    Timer timer = new Timer();
                    timer.schedule(autoSendTask, 1000, 1000);
                } else {
                    final ArrayList<TagMocha> mListTagFake =
                            TagHelper.getListTagFromListPhoneNumber(etMessageContent.getUserInfo(), true);
                    etMessageContent.resetObject();
                    sendMessageText(messageContent.trim(), replyMessage, mListTagFake);
                }*/
                final ArrayList<TagMocha> mListTagFake =
                        TagHelper.getListTagFromListPhoneNumber(etMessageContent.getUserInfo(), true);
                etMessageContent.resetObject();
                sendMessageText(messageContent.trim(), replyMessage, mListTagFake);
            }
        }
    }

    public void scrollListViewToBottom() {
        isCreated = false;
        if (mChatActivity != null) {
            mChatActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "[check_unread] running setSelectionFromTop"); // vao la chay 1 phat, tu 1 fragment
                    // khac ve chay 1
                    // lan nua, resume chay 1 lan
                    mLvwContent.setSelectionFromTop(mLvwContent.getCount() - 1, -300000);
                }
            });
        }
    }

    private void scrollListViewTo(final int pos) {
        isCreated = false;
        Log.i(TAG, "[check_unread] begin scrollListViewTo: " + pos);
        if (mChatActivity != null) {

            mLvwContent.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "[check_unread] begin scrollListViewTo: selection " + (mLvwContent.getCount() - pos - 1));
                    int index = mLvwContent.getCount() - pos - 1;
                    mLvwContent.smoothScrollToPositionFromTop(index, 0, 0);
                }
            });

            /*mChatActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "[check_unread] running scrollListViewTo: " + (mLvwContent.getCount() - pos));
                    int actionBarHeight = mRes.getDimensionPixelSize(R.dimen.action_bar_height) + mApplication.getHeightPixels() / 4;
//                    mLvwContent.setSelectionFromTop(mLvwContent.getCount() - pos, actionBarHeight);
                    mLvwContent.setSelection(mLvwContent.getCount() - pos -1);
                }
            });*/
        }
    }

    public void setDataAndDraw(boolean isNotifyNewMsg) {
        mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        if (mThreadMessage == null || mLvwContent == null) {
            String errorString = "mThreadMessage = null mThreadId = " + mThreadId + " userNumber = " + userNumber;
            Log.d(TAG, errorString);
            mApplication.logDebugContent("setDataAndDraw: " + errorString);
            mApplication.trackingException(errorString, false);
            mChatActivity.finish();
            return;
        }
        mMessageArrayList = mThreadMessage.getAllMessages();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            friendPhoneNumber = mThreadMessage.getSoloNumber();
            if (mControlMedia != null)
                checkShowMedia();
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT ||
                mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            if (mControlMedia != null)
                checkShowMedia();
        }
        if (mMessageArrayList.size() == 0){
            rltNoMessage.setVisibility(View.VISIBLE);
        } else {
            rltNoMessage.setVisibility(View.GONE);
        }
        mThreadDetailAdapter.setMessages(mMessageArrayList);
        mThreadDetailAdapter.setThreadMessage(mThreadMessage);
        initViewSuggestSticker();
        notifyDataAdapter();
        Log.i(TAG, "ThreadDetailAdapter setDataAndDraw");
        if (isNotifyNewMsg && !isListviewShowBottom()) {
            showPopupMoreMsgUnread();

        }
        checkToShowListViewNotifyHeader();
    }

    private void checkToShowListViewNotifyHeader() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mLlNotifyShareMusic.setVisibility(View.GONE);
            mBtnInviteSMS.setVisibility(View.GONE);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mMessageArrayList != null) {
            if (mIsReengUser) {
                if (mThreadMessage.isStranger()) {
                    mLlNotifyShareMusic.setVisibility(View.GONE);
                } else {
                    mLlNotifyShareMusic.setVisibility(View.VISIBLE);
                }
                drawActionBlock();
                mBtnInviteSMS.setVisibility(View.GONE);
            } else {
                if (mThreadMessage.isStranger()) {// thread lam quen thi ko hien invite
                    mBtnInviteSMS.setVisibility(View.GONE);
                } else {       //chua cai mocha thi show Moi cai Mocha
                    if (mGsmMode) {
                        mBtnInviteSMS.setVisibility(View.GONE);
                    } else {// an het btn invite
                        mBtnInviteSMS.setVisibility(View.GONE);
                    }
                }
                mLlNotifyShareMusic.setVisibility(View.GONE);
            }
            //chi show notify MOcha SMS doi voi so ko dung Mocha va la so viettel
            mLlNotifyShareMusic.setOnClickListener(this);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            mLlNotifyShareMusic.setVisibility(View.GONE);
            mBtnInviteSMS.setVisibility(View.GONE);
        } else {
            mLlNotifyShareMusic.setVisibility(View.GONE);
            mBtnInviteSMS.setVisibility(View.GONE);
        }
    }

    private void checkShowMedia() {
        // cung nghe 1-1 ,group, hoac room
        boolean isShow = false;
        mMusicBusiness = mApplication.getMusicBusiness();
        if (friendPhoneNumber != null && mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            isShow = mMusicBusiness.isShowPlayer(friendPhoneNumber);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            isShow = mMusicBusiness.isShowPlayerGroup(mThreadMessage.getServerId());
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            isShow = mMusicBusiness.isShowPlayerRoom(mThreadMessage.getServerId());
        }
        if (mApplication.getPlayMusicController() != null) {
            if (isShow && mApplication.getPlayMusicController().getCurrentSong() != null) {
                mControlMedia.showMediaPlayer(true);
                MediaModel song = mApplication.getPlayMusicController().getCurrentSong();
                mControlMedia.setInfo(song, mApplication.getPlayMusicController().getStatePlaying(), mThreadMessage);
            } else {
                mControlMedia.showMediaPlayer(false);
            }
        } else {
            mControlMedia.showMediaPlayer(false);
        }
    }

    @Override
    public void onDataReady() {
        Log.d(TAG, "[perform] -  onDataReady");
        if (mHandler == null) {
            return;
        }
        mMessageBusiness.addReengMessageListener(this);
        mMessageBusiness.addConfigGroupListener(this);
        ListenerHelper.getInstance().addContactChangeListener(this);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
                getFriendPhoneNumber();
                if (!isForwardMessageText()) {
                    setContentDraftMessage();
                }
                drawThreadDetail();
                checkToShowNetworkState();
                setViewHeaderProfile();
                OfficialActionManager.getInstance(mApplication).checkAndGetDefaultOfficialAction(mThreadMessage);
            }
        });
        // send forward otherwise
        if (mForwardingMessage != null && mForwardingMessage.isForwardingMessage()) {
            sendForwardingMessage(mForwardingMessage);
        }
        if (arrayMessage != null) {
            shareMessage();
        }
    }

    /**
     * Forward content
     *
     * @param forwardingMessage
     */
    //TODO đoạn này có thể tối ưu lại thành 1 hàm, và viết ra business để đỡ rối fragment
    private void sendForwardingMessage(ReengMessage forwardingMessage) {
        forwardingMessage.setForwardingMessage(false);
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        ReengMessageConstant.MessageType messageType = forwardingMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.text) {
            // insert content vao input text,
            if (etMessageContent != null) {
                etMessageContent.setText(forwardingMessage.getContent());
                etMessageContent.setSelection(etMessageContent.getText().length());
                etMessageContent.requestFocus();
                Log.i(TAG, "------------- forwarding text: " + forwardingMessage.getContent());
            }
            //mSendMessageAgent.sendTextMessage(forwardingMessage.getContent());
        } else if (messageType == ReengMessageConstant.MessageType.image) {
            // send image
            forwardImageMessage(forwardingMessage);
        } else if (messageType == ReengMessageConstant.MessageType.voicemail) {
            // send voicemailCi
            forwardVoiceMailMessage(forwardingMessage);
        } else if (messageType == ReengMessageConstant.MessageType.file) {
            // TODO khong lam gi sao ???????????????
        } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
            // constact Content
            sendMessageContact(forwardingMessage.getContent(), forwardingMessage.getFileName());
        } else if (messageType == ReengMessageConstant.MessageType.voiceSticker) {
            // voicesticker Content
            int collectionId = TextHelper.parserIntFromString(forwardingMessage.getFileName(), -1);
            sendMessageVoiceSticker(collectionId, (int) forwardingMessage.getSongId());
        } else if (messageType == ReengMessageConstant.MessageType.shareVideo) {
            forwardVideoMessage(forwardingMessage);
        } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
            sendMessageLocation(forwardingMessage.getContent(),
                    forwardingMessage.getFilePath(), forwardingMessage.getImageUrl());
        }
    }

    private void forwardImageMessage(ReengMessage forwardingMessage) {
        SoloSendImageMessage sendImageMessage;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            sendImageMessage = new SoloSendImageMessage(mThreadMessage, userNumber, mThreadMessage.getServerId(),
                    forwardingMessage.getFilePath());
        } else {
            sendImageMessage = new SoloSendImageMessage(mThreadMessage, userNumber, friendPhoneNumber,
                    forwardingMessage.getFilePath());
        }
        sendImageMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (TextUtils.isEmpty(forwardingMessage.getFileId()) && forwardingMessage.getSongId() != -1) {
            sendImageMessage.setFileId(String.valueOf(forwardingMessage.getSongId()));
        } else {
            sendImageMessage.setFileId(forwardingMessage.getFileId());
        }
        sendImageMessage.setDirectLinkMedia(forwardingMessage.getDirectLinkMedia());
        sendImageMessage.setVideoContentUri(forwardingMessage.getVideoContentUri());
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, sendImageMessage)) {
            sendImageMessage.setCState(cState);
            mMessageBusiness.sendXMPPMessage(sendImageMessage, mThreadMessage);
            notifyChangeAndSetSelection();
        }
    }

    private void forwardVoiceMailMessage(ReengMessage forwardingMessage) {
        SoloSendVoicemailMessage sendVoicemailMessage;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            sendVoicemailMessage = new SoloSendVoicemailMessage(mThreadMessage, userNumber, mThreadMessage
                    .getServerId(),
                    forwardingMessage.getFilePath(), forwardingMessage.getDuration(), forwardingMessage.getFileName());
        } else {
            sendVoicemailMessage = new SoloSendVoicemailMessage(mThreadMessage, userNumber, friendPhoneNumber,
                    forwardingMessage.getFilePath(), forwardingMessage.getDuration(), forwardingMessage.getFileName());
        }
        sendVoicemailMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (TextUtils.isEmpty(forwardingMessage.getFileId()) && forwardingMessage.getSongId() != -1) {
            sendVoicemailMessage.setFileId(String.valueOf(forwardingMessage.getSongId()));
        } else {
            sendVoicemailMessage.setFileId(forwardingMessage.getFileId());
        }
        sendVoicemailMessage.setDirectLinkMedia(forwardingMessage.getDirectLinkMedia());
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, sendVoicemailMessage)) {
            sendVoicemailMessage.setCState(cState);
            mMessageBusiness.sendXMPPMessage(sendVoicemailMessage, mThreadMessage);
            notifyChangeAndSetSelection();
        }
    }

    private void forwardVideoMessage(ReengMessage forwardingMessage) {
        SoloShareVideoMessage sendVideoMessage;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            sendVideoMessage = new SoloShareVideoMessage(mThreadMessage, userNumber, mThreadMessage.getServerId(),
                    forwardingMessage.getFilePath(), forwardingMessage.getDuration(),
                    forwardingMessage.getFileName(), forwardingMessage.getSize(),
                    forwardingMessage.getVideoContentUri(), forwardingMessage.getImageUrl());
        } else {
            sendVideoMessage = new SoloShareVideoMessage(mThreadMessage, userNumber, friendPhoneNumber,
                    forwardingMessage.getFilePath(), forwardingMessage.getDuration(),
                    forwardingMessage.getFileName(), forwardingMessage.getSize(),
                    forwardingMessage.getVideoContentUri(), forwardingMessage.getImageUrl());
        }
        sendVideoMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (TextUtils.isEmpty(forwardingMessage.getFileId()) && forwardingMessage.getSongId() != -1) {
            sendVideoMessage.setFileId(String.valueOf(forwardingMessage.getSongId()));
        } else {
            sendVideoMessage.setFileId(forwardingMessage.getFileId());
        }
        sendVideoMessage.setDirectLinkMedia(forwardingMessage.getDirectLinkMedia());
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, sendVideoMessage)) {
            sendVideoMessage.setCState(cState);
            mMessageBusiness.sendXMPPMessage(sendVideoMessage, mThreadMessage);
            notifyChangeAndSetSelection();
        }
    }

    private void shareMessage() {
        for (ReengMessage msg : arrayMessage.getReengMessages()) {
            switch (msg.getMessageType()) {
                case shareContact:
                    sendMessageContact(msg.getContent(), msg.getFileName());
                    break;
                case image:
                    ArrayList<String> imageLists = new ArrayList<>();
                    imageLists.add(msg.getFilePath());
                    sendMessageImage(imageLists);
                    break;
                case text:
                    String textToShare = msg.getContent();
                    //            mSendMessageAgent.sendTextMessage(textToShare);
                    if (etMessageContent != null) {
                        etMessageContent.setText(textToShare);
                        etMessageContent.setSelection(etMessageContent.getText().length());
                        etMessageContent.requestFocus();
                    }
                    break;
                case file:
                    sendMessageFile(msg.getFilePath(), msg.getFileName(), FileHelper.getExtensionFile(msg.getFilePath()));
                    break;
            }
        }


        /*ReengMessageConstant.MessageType type = arrayMessage.getReengMessages().get(0).getMessageType();
        if (type == ReengMessageConstant.MessageType.shareContact) {
            for (ReengMessage msg : arrayMessage.getReengMessages()) {
                sendMessageContact(msg.getContent(), msg.getFileName());
            }
        } else if (type == ReengMessageConstant.MessageType.image) {
            ArrayList<String> imageLists = new ArrayList<>();
            for (ReengMessage msg : arrayMessage.getReengMessages()) {
                imageLists.add(msg.getFilePath());
            }
            sendMessageImage(imageLists);
        } else if (type == ReengMessageConstant.MessageType.text) {
            String textToShare = arrayMessage.getReengMessages().get(0).getContent();
            //            mSendMessageAgent.sendTextMessage(textToShare);
            if (etMessageContent != null) {
                etMessageContent.setText(textToShare);
                etMessageContent.setSelection(etMessageContent.getText().length());
                etMessageContent.requestFocus();
            }
        } else if (type == ReengMessageConstant.MessageType.file) {
            for (ReengMessage msg : arrayMessage.getReengMessages()) {
                sendMessageFile(msg.getFilePath(), msg.getFileName(), FileHelper.getExtensionFile(msg.getFilePath()));
            }
        }*/
        arrayMessage = null; //sau khi gui xong thi clear no di
    }

    @Override
    public void onIconClickListener(View view, final Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.COPY:
                TextHelper.copyToClipboard(mChatActivity, (String) entry);
                break;
            case Constants.MENU.REENG_CHAT:
                ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread((String) entry);
                mListener.navigateToThreadDetail(thread);
                break;
            case Constants.MENU.ADD_CONTACT:
                mListener.addNewContact((String) entry, null);
                break;
            case Constants.MENU.VIEW_CONTATCT_DETAIL:
                PhoneNumber phone = (PhoneNumber) entry;
                mListener.navigateToContactDetailActivity(phone.getId());
                break;
            case Constants.MENU.GO_URL:
                UrlConfigHelper.getInstance(mChatActivity).gotoWebViewOnMedia(mApplication, mChatActivity, (String)
                        entry);
                break;
            case Constants.MENU.SHARE_ONMEDIA:
                eventOnMediaHelper.getPreviewUrl((String) entry,
                        R.string.ga_category_onmedia, R.string.ga_onmedia_action_thread_chat, FeedModelOnMedia
                                .ActionFrom.onmedia);
                break;
            case Constants.MENU.SEND_EMAIL:
                mListener.navigateToSendEmail((String) entry);
                break;
            case Constants.MENU.MESSAGE_COPY:
                ReengMessage msg = (ReengMessage) entry;
                ArrayList<TagMocha> listTag = msg.getListTagContent();
                TextHelper.copyToClipboard(mChatActivity, TagHelper.getTextTagCopy(msg.getContent(), listTag,
                        mApplication));
                break;
            case Constants.MENU.MESSAGE_DELETE:
                Log.d(TAG, "Delete Message");
                ReengMessage message = (ReengMessage) entry;
                //
                if (message.getMessageType() == ReengMessageConstant.MessageType.voicemail) {
                    // stop voice this mail
                    audioVoicemailPlayerImpl2.stopVoicemail(true);
                }
                //neu la message invite chua timeout thi huy count down
                if (message.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic &&
                        message.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                    CountDownInviteManager.getInstance(mApplication).startCountDownMessage(message);
                }
                if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                    mApplication.getTransferFileBusiness().cancelUploadFileMessage(message);
                }
                if (message.getMessageType() == ReengMessageConstant.MessageType.poll_action) {
                    HashMap<String, Integer> hashMap = mThreadMessage.getPollLastId();
                    if (hashMap.containsKey(message.getFileId())) {
                        int lastId = hashMap.get(message.getFileId());
                        if (lastId == message.getId()) {
                            hashMap.remove(message.getFileId());
                        }

                    }
                }
                mMessageArrayList.remove(message);
                mMessageBusiness.deleteAMessage(mThreadMessage, message);
                notifyDataAdapter();
                break;

            case Constants.MENU.MESSAGE_BLOCK_FORWARD:
                mChatActivity.showToast(R.string.msg_block_forward_message);
                break;

            case Constants.MENU.MESSAGE_FORWARD:
                // open list contact, choose contact and send
                ReengMessage msgForward = (ReengMessage) entry;
                msgForward.setForwardingMessage(true);
                ShareContentBusiness business = new ShareContentBusiness(mChatActivity, msgForward);
                business.setTypeSharing(ShareContentBusiness.TYPE_FORWARD_MESSAGE);
                business.showPopupForwardMessage();
                break;

            case Constants.MENU.MESSAGE_SHARE:
                try {
                    // open list contact, choose contact and send
                    ReengMessage msgShare = (ReengMessage) entry;
                    if (msgShare.getMessageType() == ReengMessageConstant.MessageType.text) {
                        ArrayList<TagMocha> listTagShare = msgShare.getListTagContent();
                        String contentShare = TagHelper.getTextTagCopy(msgShare.getContent(), listTagShare, mApplication);
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, contentShare);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                    } else if (msgShare.getMessageType() == ReengMessageConstant.MessageType.image) {
                        File fileShare = new File(msgShare.getFilePath());
                        if (!TextUtils.isEmpty(msgShare.getFilePath()) && fileShare.exists()) {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("image/jpeg");
                            share.putExtra(Intent.EXTRA_STREAM, FileHelper.fromFile(mApplication, fileShare));
                            startActivity(Intent.createChooser(share, mRes.getString(R.string.share)));
                        } else {
                            mChatActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
//                    ReengMessage msgForward1 = (ReengMessage) entry;
//                    msgForward1.setForwardingMessage(true);
//                    ShareContentBusiness business1 = new ShareContentBusiness(mChatActivity, msgForward1);
//                    business1.setTypeSharing(ShareContentBusiness.TYPE_SHARE_CONTENT);
//                    business1.showPopupShareContent();
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
                break;

            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                ReengMessage msgShare = (ReengMessage) entry;
                ShareUtils.shareMes(mChatActivity, msgShare.getContent(), msgShare.getImageUrl(), msgShare.getFilePath());
                break;
            case Constants.MENU.SEND_MESSAGE:
                ReengMessage msgShare1 = (ReengMessage) entry;
                onClickFacebook(msgShare1);
                break;
            case Constants.ACTION.ACTION_MOCHA_TO_SMS:
                mMessageBusiness.getOutgoingMessageProcessor().sendMochaToSmsUnlimited((ReengMessage) entry,
                        mChatActivity, mThreadMessage, friendOperator);
                break;
            case Constants.MENU.MESSAGE_RESTORE:
                handleRestoreReengMessage((ReengMessage) entry);
                break;
            case Constants.MENU.MESSAGE_DETAIL_SEND_RECEIVE:
                break;
            case Constants.MENU.MESSAGE_DETAIL_DELIVER:
                if (mListener != null) {
                    String abStatus = "";
                    if (mTvStatus != null && mTvStatus.getVisibility() == View.VISIBLE) {
                        abStatus = mTvStatus.getText().toString();
                    }
                    mListener.navigateToStatusMessageFragment(mThreadId, ((ReengMessage) entry).getId(), abStatus);
                }
                break;
            case Constants.MENU.MESSAGE_REPLY:
                showFooterReplyMessage((ReengMessage) entry);
                break;
            case Constants.MENU.MESSAGE_DETAIL_NOTE:
                android.util.Log.d("tungnt", "onIconClickListener: ");
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && mThreadMessage.isPrivateThread()) {
                    mChatActivity.showToast(getString(R.string.save_message_not_support));
                } else {
                    NoteMessageHelper.getInstance(mApplication).saveNoteMessage(getContext(), (ReengMessage) entry,
                            mThreadMessage, mContactBusiness, mAvatarBusiness, mThreadType,
                            friendPhoneNumber, userNumber, mOfficerAccountRoomChat);
                    tvSavMessage.setVisibility(View.VISIBLE);
                    imgIcon.setVisibility(View.VISIBLE);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            tvSavMessage.setVisibility(View.GONE);
                            imgIcon.setVisibility(View.GONE);
                        }
                    }, 3000);

                }
                break;
            case Constants.MENU.POPUP_YES:
                ReengMessage reengMessage = (ReengMessage) entry;
                reengMessage.setChatMode(ReengMessageConstant.MODE_GSM);
                reengMessage.setCState(cState);
                mMessageBusiness.sendXMPPMessage(reengMessage, mThreadMessage);
                break;
            case Constants.MENU.MENU_ADD_GROUP:
                ArrayList<String> listNumbers = mThreadMessage.getPhoneNumbers();
                mChatActivity.checkRightSlideMenuToClose();
                mListener.navigateToChooseFriendsActivity(listNumbers, mThreadMessage, -2);
                break;
            case Constants.MENU.MENU_GSM_CALL:
                mListener.navigateToCall(mThreadMessage.getSoloNumber());
                break;
            case Constants.MENU.POPUP_OK_AUTO_PLAY_VOICE_STICKER:
                ReengMessage xxx = (ReengMessage) entry;
                int collectionId = Integer.valueOf(xxx.getFileName());
                int itemId = (int) xxx.getSongId();
                playStickerMessage(collectionId, itemId);
                //update setting
                mChatActivity.showToast(R.string.can_change_in_setting);
                settingAutoPlaySticker = true;
                mSettingBusiness.setPrefAutoPlaySticker(true);
                break;
            case Constants.MENU.POPUP_OK_GG_PLAY_SERVICE:
                NavigateActivityHelper.navigateToPlayStore(mChatActivity, Constants.PACKET_NAME.GG_PLAY_SERVICE);
                break;
            // moi lai hoac dong y suggest thi xoa message cu, gui invite
            case Constants.MENU.MENU_SETTING_SOLO:
                mChatActivity.showViewSetting();
                break;
            case Constants.MENU.MENU_SETTING_GROUP:
                mChatActivity.showViewSetting();
                break;
            case Constants.MENU.MENU_MANAGE_GROUP:
                mChatActivity.showViewManager();
                break;
            case Constants.MENU.MENU_ADD_FRIEND_GROUP:
                mListener.navigateToChooseFriendsActivity(mThreadMessage.getPhoneNumbers(), mThreadMessage, -2);
                break;
            case Constants.MENU.MENU_MANAGE_BROADCAST:
                mChatActivity.showViewManager();
                break;
            case Constants.MENU.MENU_BLOCK_PERSON:
                setActionBlock(true, ContactRequestHelper.BLOCK_NON);
                break;
            case Constants.MENU.MENU_UNBLOCK_PERSON:
                setActionBlock(false, ContactRequestHelper.BLOCK_NON);
                break;
            case Constants.MENU.POPUP_REPORT_SPAM:
                RoomChatRequestHelper.getInstance(mApplication).reportRoomChat((ReengMessage) entry,
                        Constants.MESSAGE.ACTION_REPORT_SPAM, requestReportRoomListener);
                mChatActivity.showToast(R.string.report_room_success);
                break;
            case Constants.MENU.POPUP_REPORT_BADWORD:
                RoomChatRequestHelper.getInstance(mApplication).reportRoomChat((ReengMessage) entry,
                        Constants.MESSAGE.ACTION_REPORT_BADWORD, requestReportRoomListener);
                mChatActivity.showToast(R.string.report_room_success);
                break;
            case Constants.MENU.POPUP_REPORT_CHEAT:
                RoomChatRequestHelper.getInstance(mApplication).reportRoomChat((ReengMessage) entry,
                        Constants.MESSAGE.ACTION_REPORT_CHEAT, requestReportRoomListener);
                mChatActivity.showToast(R.string.report_room_success);
                break;
            case Constants.MENU.MENU_CALL:
                mListener.navigateToCall((String) entry);
                break;
            case Constants.MENU.MESSAGE_MENU_TRANSLATE:
                mMessageBusiness.translateMessageGoogle((ReengMessage) entry, languageCode, mThreadType);
                break;
            case Constants.MENU.MENU_CREATE_POLL:
                mListener.navigateToPollActivity(mThreadMessage, null, null, Constants.MESSAGE.TYPE_CREATE_POLL, false);
                break;
            case Constants.MENU.SHARE_SIEU_HAI:
                mChatActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                ReportHelper.requestShareLinkSieuHai(mApplication, (String) entry, new ReportHelper
                        .onShareSieuHaiListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mChatActivity.hideLoadingDialog();
                        mChatActivity.showToast(desc, Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onError(String desc) {
                        mChatActivity.hideLoadingDialog();
                        mChatActivity.showToast(desc, Toast.LENGTH_LONG);
                    }
                });
                break;
            case Constants.MENU.MENU_CALL_FREE:
                ThreadMessage threadFree = mApplication.getMessageBusiness().findExistingOrCreateNewThread((String)
                        entry);
                mApplication.getCallBusiness().handleStartCall(threadFree, mChatActivity, false, false, true);
                break;
            case Constants.MENU.MENU_CALL_OUT:
                ThreadMessage threadOut = mApplication.getMessageBusiness().findExistingOrCreateNewThread((String)
                        entry);
                mApplication.getCallBusiness().handleStartCall(threadOut, mChatActivity, true, false, true);
                break;

            case Constants.MENU.MESSAGE_PIN:
                if (mThreadMessage.isPrivateThread() && !mThreadMessage.isAdmin()) {
                    mChatActivity.showToast(R.string.toast_pin_message_only_admin);
                    return;
                }
                final PinMessage pinMessage = mThreadMessage.getPinMessage();
                if (pinMessage != null
                        && !TextUtils.isEmpty(pinMessage.getContent())) {
                    DialogConfirm dialogConfirm = new DialogConfirm(mChatActivity, true);
                    dialogConfirm.setMessage(mRes.getString(R.string.message_confirm_pin_another_message));
                    dialogConfirm.setPositiveLabel(mRes.getString(R.string.ok));
                    dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
                    dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            if (pinMessage.getType() == PinMessage.TypePin.TYPE_VOTE.VALUE)
                                onActionPoll(PollObject.STATUS_UNPIN, pinMessage.getTarget(), (ReengMessage) entry);
                            else
                                setPinMessage((ReengMessage) entry);

                        }
                    });
                    dialogConfirm.show();
                } else {
                    setPinMessage((ReengMessage) entry);
                }
                break;
            case Constants.MENU.MENU_REPORT_DHVTT:
                ReportHelper.reportDhVtt(mApplication, mChatActivity, mThreadMessage, (ReengMessage) entry);
                break;
        }
    }

    private void setPinMessage(ReengMessage oldPinMsg) {
        mMessageBusiness.sendPinMessage(mThreadMessage, oldPinMsg, mChatActivity, false);
        final ReengMessage fakeMsgPin = new ReengMessage();
        fakeMsgPin.setSize(PinMessage.ActionPin.ACTION_PIN.VALUE);
        fakeMsgPin.setSongId(PinMessage.TypePin.TYPE_MESSAGE.VALUE);
        fakeMsgPin.setImageUrl("");
        fakeMsgPin.setFileName(oldPinMsg.getSender());
        fakeMsgPin.setFilePath("");
        fakeMsgPin.setContent(oldPinMsg.getContent());
        handlePinMessage(fakeMsgPin, false);

    }

    public void sendVideoWatchTogether(MediaModel video) {
        if (mApplication.getPlayMusicController().isPlaying()) {
            mApplication.getPlayMusicController().closeMusic();
        }
        //TODO send video
        playVideo(video, 0);
        mMessageBusiness.createAndSendMessageWatchVideo(mThreadMessage, video, mChatActivity);
    }

    private void playVideo(final MediaModel mediaModel, int positionVideo) {
        if (mApplication != null) {
            VideoService.stop(mApplication);
            MusicFloatingView.stop(mApplication);
        }
        isPauseVideo = false;
        String info;
        if (TextUtils.isEmpty(mediaModel.getSinger())) {
            info = mediaModel.getName();
        } else {
            info = mediaModel.getName() + " - " + mediaModel.getSinger();
        }
        mTvwTitleVideo.setText(info);
        mViewVideo.setVisibility(View.VISIBLE);
        viewPinMessage.setVisibility(View.GONE);
        mTvwDurationVideo.setText("--:--");
        mTvwTimeVideo.setText("--:--");
        mProgressPlayVideo.setProgress(0);
        mVideo.requestFocus();
        mThreadMessage.setMediaModel(mediaModel);
        mThreadMessage.setLastPositionVideo(positionVideo);
        positionSeekVideo = positionVideo;
        mImgThumbVideo.setVisibility(View.VISIBLE);
        mImgTogglePlay.setVisibility(View.GONE);
        mProgressLoadingVideo.setVisibility(View.VISIBLE);
//        mApplication.getUniversalImageLoader().displayImage(mediaModel.getImage(), mImgThumbVideo);
        Glide.with(mApplication).load(mediaModel.getImage()).into(mImgThumbVideo);
        mKeyboardController.updateListMessageHeight();
        if (mMusicBusiness.isExistListenerSolo()) {
            Log.i(TAG, "chua thoat cung nghe 1-1");
            mProgressLoadingVideo.setVisibility(View.GONE);
            mImgTogglePlay.setVisibility(View.VISIBLE);
            needPauseVideo = true;
            return;
        }
        needPauseVideo = false;
        countRetryFail = 0;
        Uri uri = UrlConfigHelper.getInstance(mApplication).formatUrlBeforeWatchVideo(mApplication, mediaModel
                .getMedia_url());
        if (uri == null) return;
        mVideo.setVideoURI(uri);
        mVideo.setOnPreparedListener(ThreadDetailFragment.this);
        mVideo.setOnCompletionListener(ThreadDetailFragment.this);
        mVideo.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                countRetryFail++;
                if (countRetryFail == 3) {
                    Log.i(TAG, "retry 3");
                    showErrorWithoutFinish();
                    return true;
                }
                Log.i(TAG, "error --- what: " + what + " extra: " + extra);
                mMusicBusiness.getDetailWatchVideo(new MusicBusiness.OnGetDetailWatchVideoListener() {
                    @Override
                    public void onResponse(MediaModel video) {
                        Uri uri = UrlConfigHelper.getInstance(mApplication).formatUrlBeforeWatchVideo(mApplication,
                                video.getMedia_url());
                        if (uri == null) return;
                        mVideo.setVideoURI(uri);
                        mVideo.seekTo(positionSeekVideo);
                    }

                    @Override
                    public void onError(int errorCode, String desc) {
                        showErrorWithoutFinish();
                    }
                }, mediaModel);
                return true;
            }
        });
        mImgTogglePlay.setVisibility(View.GONE);
        mProgressLoadingVideo.setVisibility(View.VISIBLE);
        if (mApplication.getPlayMusicController().isPlaying()) {
            mApplication.getPlayMusicController().closeMusic();
        }
        mVideo.seekTo(positionSeekVideo);
    }

    private void acceptMusicMessage(ReengMessage message) {
        mMusicBusiness = mApplication.getMusicBusiness();
        message.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
        message.setFileName(String.format(mRes.getString(R.string.invite_share_music_accepted), TextHelper
                .textBoldWithHTML(getFriendName())));
        message.setContent(String.format(mRes.getString(R.string.invite_share_music_accepted), getFriendName()));
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            // huy ban tin moi truoc do neu co (th A moi B, sau do B moi lai, A dong y thi tu huy loi moi trc, ko can
            // gui leave)
            mMusicBusiness.cancelLastMusicInviteMessage(mThreadMessage);
            CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(message);
            // ban invite nhan dc thi session id trung packet id
            mMusicBusiness.onAcceptAndPlayMusic(message.getImageUrl(),
                    friendPhoneNumber, message.getSongModel(mMusicBusiness));
        } else {
            mMusicBusiness.startGroupMusic(mThreadMessage.getServerId(), message);
        }
        notifyChangeAndSetSelection();
        mMessageBusiness.updateAllFieldsOfMessage(message);
        mChatActivity.trackingEvent(R.string.ga_category_invite_music,
                R.string.ga_accept_invite_music, R.string.ga_accept_invite_music);
    }

    @Override
    public void onSmartTextClick(String content, int type) {
        /*if (enableClickSmartText) {
            enableClickSmartText = false;
        } else {
            enableClickSmartText = true;
        }*/
        if (lastClick == 0) {
            lastClick = System.currentTimeMillis();
        } else {
            long deltaTime = System.currentTimeMillis() - lastClick;
            lastClick = 0;
            if (deltaTime < 200) {
                return;
            }
        }
        if (type == Constants.SMART_TEXT.TYPE_MOCHA) {
            Log.i(TAG, "smarttext mocha: " + content);
            DeepLinkHelper.getInstance().openSchemaLink(mChatActivity, content);
        } else if (type == Constants.SMART_TEXT.TYPE_URL) {
            //onClickUrl(content);
            Utilities.processOpenLink(mApplication, mChatActivity, content);
        } else {
            FragmentManager fragmentManager = getFragmentManager();
            PopupHelper.getInstance().showContextMenuSmartText(mChatActivity, fragmentManager,
                    content, content, type, mIconListener);
        }
    }

    public void onClickFacebook( ReengMessage msgShare) {
        if (!Utilities.isPackageInstalled(getContext(), "com.facebook.orca")) {
            confirmDialogMessenger = new BaseMPConfirmDialog(R.string.info,
                    R.string.warning_messenger_was_not_installed, R.string.download_app, R.string.cancel, view -> {
                try {
                    getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.facebook.orca")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.orca")));
                }
                if (confirmDialogMessenger != null) {
                    confirmDialogMessenger.dismiss();
                }
            }, view -> {
                if (confirmDialogMessenger != null) {
                    confirmDialogMessenger.dismiss();
                }
            });
            confirmDialogMessenger.show(getActivity().getSupportFragmentManager(), null);
            return;
        }
        ShareLinkContent contentMessage = new ShareLinkContent.Builder()
                .setContentTitle("contentShare")
                .setImageUrl(Uri.fromFile(new File(msgShare.getFilePath())))
                .build();
        MessageDialog.show(mChatActivity, contentMessage);
    }
    @Override
    public void onClickImageReply(ReplyMessage replyMessage) {
        Intent intent = new Intent(mApplication, PreviewImageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, -2);
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String friendName;
        if (replyMessage.getMember().equals(myNumber)) {
            friendName = mApplication.getReengAccountBusiness().getUserName();
        } else {
            PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(replyMessage.getMember());
            if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                friendName = phoneNumber.getName();
            } else {                      //hien thi so dien thoai neu la group chat
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    friendName = mApplication.getMessageBusiness().getFriendName(replyMessage.getMember());
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(replyMessage.getMember());
                    if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                        friendName = nonContact.getNickName();
                    } else {
                        friendName = replyMessage.getMember();
                    }
                }
            }
        }
        intent.putExtra(PreviewImageActivity.PARAM_TITLE, friendName);
        intent.putExtra(PreviewImageActivity.PARAM_CURRENT_IMAGE, replyMessage.getFilePath());
        intent.putExtra(PreviewImageActivity.PARAM_LINK, replyMessage.getImgLink());
        intent.putExtra(PreviewImageActivity.PARAM_PRIVATE, mThreadMessage.isPrivateOrEncrypt());
        mApplication.startActivity(intent);
    }

    @Override
    public void onOpenViewReaction(View anchorView, ReengMessage reengMessage) {
        if (anchorView != null) {
            mPopupListener = ReactionPopup.showReactionPopup(mChatActivity, anchorView, reengMessage, this);
        }
    }

    @Override
    public void onOpenListReaction(ReengMessage reengMessage, View viewReact) {
        if (reengMessage == null || reengMessage.getListReaction() == null || reengMessage.getListReaction().isEmpty())
            return;
        ReactionListBottomDialog dialog = ReactionListBottomDialog.newInstance(reengMessage);
        dialog.setReactImageClickListener(this);
        dialog.show(getChildFragmentManager(), ReactionListBottomDialog.TAG);
    }

    @Override
    public void onClickReaction(final ReengMessage reengMessage, final View viewReact) {
        MessageHelper.showAnimReation(mChatActivity, viewReact, R.drawable.ic_reaction_love);
        mMessageBusiness.sendReactionMessage(mChatActivity, reengMessage, ReengMessageConstant.Reaction.LOVE, mThreadMessage);



        /*if (!showIntroReaction) {
            try {
                new MaterialIntroView.Builder(mChatActivity)
                        .enableDotAnimation(false)
                        .enableIcon(false)
                        .dismissOnTouch(true)
                        .setFocusGravity(FocusGravity.CENTER)
                        .setFocusType(Focus.MINIMUM)
                        .setDelayMillis(100)
                        .enableFadeAnimation(true)
                        .setListener(new MaterialIntroListener() {
                            @Override
                            public void onUserClicked(String materialIntroViewId) {
                                MessageHelper.showAnimReation(mChatActivity, viewReact, R.drawable.ic_reaction_love);
                                mMessageBusiness.sendReactionMessage(mChatActivity, reengMessage, ReengMessageConstant.Reaction.LOVE, mThreadMessage);
                            }
                        })
                        .setInfoText(mRes.getString(R.string.new_reaction_des))
                        .setShape(ShapeType.CIRCLE)
                        .setTarget(viewReact)
                        .setUsageId("intro_new_reaction") //THIS SHOULD BE UNIQUE ID
                        .show();
                showIntroReaction = true;
                mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_DID_SHOW_INTRO_NEW_REACTION, true).apply();
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        } else {
            MessageHelper.showAnimReation(mChatActivity, viewReact, R.drawable.ic_reaction_love);
            mMessageBusiness.sendReactionMessage(mChatActivity, reengMessage, ReengMessageConstant.Reaction.LOVE, mThreadMessage);
        }*/
    }

    @Override
    public void onClickActionChangeNumber(ReengMessage message, ReengMessageConstant.ActionChangeNumber actionChangeNumber) {
        switch (actionChangeNumber) {
            case saveContact:
                handleSaveContact(message);
                break;

            case createChat:
//                handleCreateChat(message);
                break;

            case syncChat:
                handleSyncChat(message);
                break;
        }
    }

    private void handleSyncChat(ReengMessage message) {
        mChatActivity.showToast("handleSyncChat nothing");
        HashMap<String, String> number = new HashMap<>();
        number.put(message.getSender(), message.getImageUrl());
        onEditContact(true, number, null);
    }

//    private void onClickUrl(String content) {
//        if (URLUtil.isValidUrl(content)) {
//            //kiem tra xem co phai la link mocha video ko (co id video)
//            Log.i(TAG, "Click content: " + content);
//            if (TextHelper.isLinkPhimKeeng(content) || TextHelper.isLinkMusicKeeng(content) || TextHelper.isLinkNetNews(content)) {
//                boolean check = Utilities.processOpenLink(mApplication, mChatActivity, content);
//                if (check)
//                    return;
//            }
//
//            if (mApplication.getConfigBusiness().isEnableTabVideo()) {
//                if (TextHelper.getInstant().isLinkMochaVideo(content)) { //lay dc id thi la mocha video
//                    DeepLinkHelper.getInstance().handleSchemaMochaVideoWeb(mApplication, mChatActivity, content);
//                } else if (TextHelper.getInstant().isLinkMochaChannel(content)) {
//                    String idChannel = TextHelper.getInstant().getMochaChannelId(content);
//                    if (TextUtils.isEmpty(idChannel)) {
//                        UrlConfigHelper.getInstance(mChatActivity).gotoWebViewOnMedia(mApplication, mChatActivity, content);
//                    } else
//                        DeepLinkHelper.getInstance().handleSchemaChannelVideo(mApplication, mChatActivity, idChannel);
//                } else
//                    UrlConfigHelper.getInstance(mChatActivity).gotoWebViewOnMedia(mApplication, mChatActivity, content);
//            } else
//                UrlConfigHelper.getInstance(mChatActivity).gotoWebViewOnMedia(mApplication, mChatActivity, content);
//        }
//    }

    private void handleSaveContact(ReengMessage message) {
        /*PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
        if (phoneNumber != null) {
            NavigateActivityHelper.navigateToEditContactWithNewNumber(mChatActivity, phoneNumber.getContactId(), message.getImageUrl());
        } else {
            mApplication.getContactBusiness().navigateToAddContact(mChatActivity, message.getImageUrl(), "");
        }*/
        mApplication.getContactBusiness().navigateToAddContact(mChatActivity, message.getImageUrl(), "");
    }

    @Override
    public void emoClicked(String emoPath, String emoText, int emoIndex) {
        Spanned s = EmoticonUtils.getSpanned(mChatActivity.getApplicationContext(), emoText, etMessageContent
                .getTextSize());
        etMessageContent.getText().append(s);
        dismissStickerPopupWindow();
        mKeyboardController.showEditTextWhenSendEmo(); //click emo thi show edit text
    }

    @Override
    public void transferPickedFile(String filePath, String fileName, String extension) {
        Log.i(TAG, "transferPickedFile: " + filePath + " -- " + extension);
        sendMessageFile(filePath, fileName, extension);
    }

    @Override
    public void transferPickedPhoto(ArrayList<String> filePaths) {
        sendMessageImage(filePaths);
    }

    @Override
    public void transferTakenPhoto(String filePath) {
        ArrayList<String> filePaths = new ArrayList<>();
        filePaths.add(filePath);
        sendMessageImage(filePaths);
    }

    @Override
    public void transferSelectedContact(ArrayList<PhoneNumber> lstPhone) {
        for (int i = 0; i < lstPhone.size(); i++) {
            sendMessageContact(lstPhone.get(i).getName(), lstPhone.get(i).getJidNumber());
        }
    }

    @Override
    public void transferSelectedDocument(DocumentClass document) {
        if (document != null && !TextUtils.isEmpty(document.getDesc())) {
            sendMessageText(document.getDesc(), null, null);
        } else {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
        }
    }

    @Override
    public void transferTakenVideo1(String filePath, String fileName, int duration, int fileSize, Video v) {
        sendMessageVideo(filePath, fileName, duration, fileSize, v);
    }

    @Override
    public void transferShareLocation(String content, String latitude, String longitude) {
        sendMessageLocation(content, latitude, longitude);
    }

    @Override
    public void sendVideo(String filePath, String fileName, int duration, int fileSize) {
        Log.d("Messs detail", sendVideoMessage.getId() + "");
        sendVideoMessage.setFilePath(filePath);
        mMessageBusiness.updateAllFieldsOfMessage(sendVideoMessage);
        mApplication.getTransferFileBusiness().startUploadMessageFile(sendVideoMessage);
    }

    public void notifyNewOutgoingMessage() {
        Log.i(TAG, "notifyNewOutgoingMessage");
        scrollListViewToBottom();
    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {
        if (mThreadId == threadId) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "ThreadDetailAdapter notify message sent successfully");
                    notifyDataAdapter();
                    //play sound
                    mReengNotificationManager.playSoundSendMessage(mChatActivity.getApplicationContext(), mThreadId);
                }
            });
        }
    }

    @Override
    public void onUpdateStateTyping(final String phoneNumber, final ThreadMessage thread) {
        Log.i(TAG, "onUpdateStateTyping chat person");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                        && friendPhoneNumber.equals(phoneNumber) && thread == null) {
                    mTvStatus.setText(mRes.getString(R.string.is_typing));
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && thread != null) {
                    String friendName = mMessageBusiness.getFriendName(phoneNumber);
                    String textTyping = friendName + " " + mRes.getString(R.string.is)
                            + " " + mRes.getString(R.string.is_typing).toLowerCase();
                    mTvStatus.setText(textTyping.trim());
                } else {
                    return;
                }
                if (countDownTimerReceiveTyping != null) {
                    countDownTimerReceiveTyping.cancel();
                    countDownTimerReceiveTyping = null;
                }
                startCountdownTimerReceiveTyping();
            }
        });
    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {
        if (threadId == mThreadId) {
            // kiem tra suggest message
            if (mHandler != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "ThreadDetailAdapter update mediaDetail");
                        notifyDataAdapter();
                    }
                });
            }
        }
    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    setVisibleFooterAcceptStranger();
                }
            });
        }
    }

    /**
     * notify tin nhan loi
     *
     * @param reengMessageList
     */
    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {

    }

    @Override
    public void notifyNewIncomingMessage(final ReengMessage message, final ThreadMessage thread) {
        mApplication.logDebugContent("notifyNewIncomingMessage: " + message.getPacketId()
                + " | threadName: " + thread.getThreadName() + " | threadID: " + thread.getId() + " current thread: " + mThreadId);
        if (mThreadId == message.getThreadId()) {
            if (!mAppStateManager.isShowQuickReply()) {
                if (message.getReadState() == ReengMessageConstant.READ_STATE_UNREAD) {
                    message.setReadState(ReengMessageConstant.READ_STATE_READ);
                }
            }

            if (thread.isHasNewMessage()) {
                message.setNewMessage(true);
            }
            mThreadMessage.setNumOfUnreadMessage(0);
            processVoteIncoming(message);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
//                    mMessageBusiness.insertNewMessageToMemory(mThreadMessage, message);
                    //phai goi ham insert vao memory truoc roi moi goi ham notifyDataSetChanged()
                    // neu la thread chat va ko scroll o bottom thi hien popup more msg unread
                    setDataAndDraw(true);
                    mApplication.logDebugContent("insert to memory & notify adapter");
                    Log.f(TAG, "insert to memory & notify adapter");
                    // play sound
                    mReengNotificationManager.playSoundReceiveMessage(mChatActivity.getApplicationContext(), mThreadId);
                    if (message.getMessageType() == ReengMessageConstant.MessageType.voiceSticker) {
                        final int collectionId = Integer.valueOf(message.getFileName());
                        final int itemId = (int) message.getSongId();
                        if (settingAutoPlaySticker) {
                            playStickerMessage(collectionId, itemId);
                        }
                    } else if (message.getMessageType() == ReengMessageConstant.MessageType.watch_video) {
                        if (mMessageBusiness.isAutoPlayMessageVideo(message)) {
                            onWatchVideoClick(message, false);
                        }
                    } else if (message.getMessageType() == ReengMessageConstant.MessageType.pin_message) {
                        if (message.getSize() == PinMessage.ActionPin.ACTION_UNPIN.VALUE) {
                            viewPinMessage.setVisibility(View.GONE);
                        } else {
                            handlePinMessage(message, false);
                        }
                    } else if (message.getMessageType() == ReengMessageConstant.MessageType.enable_e2e) {
                        checkThreadEncrypt();
                    } else if (message.getMessageType() == ReengMessageConstant.MessageType.text && !TextUtils.isEmpty(message.getPreKeyTmp())) {
                        checkThreadEncrypt();
                    } else if (Config.Features.FLAG_SUPPORT_PIN_VOTE && (message.getMessageType() == ReengMessageConstant.MessageType.poll_action ||
                            message.getMessageType() == ReengMessageConstant.MessageType.poll_create)) {
                        PollObject pollObject = PollRequestHelper.getInstance(mApplication).getPollObjectLocal(message.getFileId(), thread.getId());
                        if (pollObject != null) {
                            if (pollObject.getPinStatus() == PollObject.STATUS_UNPIN && mThreadMessage.getPinMessage() != null
                                    && TextUtils.isEmpty(mThreadMessage.getPinMessage().getTarget())) {
                                viewPinMessage.setVisibility(View.GONE);
                            } else if (pollObject.getPinStatus() == PollObject.STATUS_PIN) {
                                handlePinMessage(null, false);
                            }
                        }
                    }
                }
            });
            mMessageBusiness.updateThreadMessage(mThreadMessage);
            mMessageBusiness.updateAllFieldsOfMessage(message);
            mApplication.logDebugContent("database updateThreadMessage updateAllFieldsOfMessage");
            Log.f(TAG, "database updateThreadMessage updateAllFieldsOfMessage");
        } else {
            mApplication.logDebugContent("# thread, insert to memory");
            Log.f(TAG, "# thread, insert to memory");
//            mMessageBusiness.insertNewMessageToMemory(thread, message);
        }
    }

    private void processVoteIncoming(ReengMessage message) {
        if (message.getMessageType() == ReengMessageConstant.MessageType.poll_create ||
                message.getMessageType() == ReengMessageConstant.MessageType.poll_action) {
            HashMap<String, Integer> hashMap = mThreadMessage.getPollLastId();
            if (hashMap.containsKey(message.getFileId())) {
                int lastId = hashMap.get(message.getFileId());
                if (lastId < message.getId())
                    hashMap.put(message.getFileId(), message.getId());
            } else {
                hashMap.put(message.getFileId(), message.getId());
            }
        }

    }

    @Override
    public void onRefreshMessage(int threadId) {
        if (mThreadId == threadId) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "ThreadDetailAdapter on refresh message");
                    notifyDataAdapter();
                }
            });
        }
    }

    @Override
    public void onGSMSendMessageError(ReengMessage message, final XMPPResponseCode responseCode) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mChatActivity.showError(responseCode.getDescription(), null);
            }
        });
    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, final boolean showAlert, final String
            msgError) {
        if (mThreadId != threadId) return;
        if (!TextUtils.isEmpty(msgError)) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (showAlert)
                        mChatActivity.showError(msgError, null);
                    else
                        mChatActivity.showToast(msgError, Toast.LENGTH_LONG);
                    Log.i(TAG, "ThreadDetailAdapter non contact response");
                    notifyDataAdapter();
                }
            });
        } else {
            mChatActivity.showError(mRes.getString(R.string.e666_not_support_function), mRes.getString(R.string
                    .message));
        }
    }

    @Override
    public void onUpdateStateRoom() {
        Log.d(TAG, "onUpdateStateRoom");
    }

    @Override
    public void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread) {
        Log.i(TAG, "onBannerDeepLinkUPdate");
        handlePinMessage(message, false);
    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, final int actionChange) {
        if (threadMessage.getId() != mThreadMessage.getId()) {
            return;
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (actionChange == Constants.MESSAGE.CHANGE_GROUP_NAME) {
                    mTvName.setText(mThreadMessage.getThreadName());
                } else if (actionChange == Constants.MESSAGE.CHANGE_GROUP_MEMBER) {
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setText(mContactBusiness.getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
                    handleChangePrivate(mThreadMessage);
                    mTvName.setText(mThreadMessage.getThreadName());
                    setListMemberTag();
                } else if (actionChange == Constants.MESSAGE.CHANGE_GROUP_PRIVATE) {
                    handleChangePrivate(mThreadMessage);
                }
            }
        });
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {
        if (mThreadMessage == null || TextUtils.isEmpty(roomId) || !roomId.equals(mThreadMessage.getServerId())) {
            return;
        }
        if (unFollow) {
            mChatActivity.showToast(R.string.e415_not_allow_leave_room);
            mChatActivity.goPreviousOrHomeWhenBackPress();
        } else {
            drawStatus();
            Log.d(TAG, "displayBackgroundOfThreadDetail: onConfigRoomChange");
            updateBackgroundUI(mThreadMessage.getBackground());
        }
    }

    //MessageInteractionListener
    @Override
    public void longClickBgrCallback(ReengMessage message) {
        mChatActivity.setEnableBothSideSlideMenu(true);
        showInfoMsg(false);
        mKeyboardController.hideSoftAndCustomKeyboard();

        ArrayList<ItemContextMenu> listItemContextMenu = MessageHelper.getListItemContextMenu(mApplication,
                message, mThreadMessage, languageCode, false, true);
        String title = mMessageBusiness.getContentOfMessage(message, mRes, mApplication);
        //file, image, voicemail, shareContact
        if (Config.Features.FLAG_SUPPORT_REACTION_MESSAGES) {
            InputMethodUtils.hideSoftKeyboard(mChatActivity);
            mPopupListener = BottomSheetContextMenu.showContextMenu(mChatActivity, mThreadMessage, message, listItemContextMenu, this, this);
        } else {
            PopupHelper.getInstance().showContextMenu(mChatActivity, title, listItemContextMenu, this);
        }
    }

    @Override
    public void retryClickCallBack(ReengMessage message) {
        if (!NetworkHelper.isConnectInternet(mChatActivity)) {
            mChatActivity.showToast(R.string.no_connectivity);
            return;
        }
        ReengMessageConstant.MessageType type = message.getMessageType();
        ReengMessageConstant.Direction direction = message.getDirection();
        if (ReengMessageConstant.MessageType.containsNormal(type)) {
            if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                message.setPacketId(PacketMessageId.getInstance().genMessagePacketId(mThreadType, type.toString()));
                //truong hop retry tin nhan gsm thi phai generate packetId moi
                //- do server check trung packetId voi tin nhan SMS Out
            }
            message.setChatMode(ReengMessageConstant.MODE_IP_IP);
            mMessageBusiness.retrySendTextAndContactMessage(message, mThreadMessage);
        } else if (ReengMessageConstant.MessageType.containsFile(type)) {
            if (direction == ReengMessageConstant.Direction.received) {
                message.setStatus(ReengMessageConstant.STATUS_NOT_LOAD);
                mApplication.getTransferFileBusiness().startDownloadMessageFile(message);
            } else if (direction == ReengMessageConstant.Direction.send) {
                message.setStatus(ReengMessageConstant.STATUS_NOT_SEND);
                if (type == ReengMessageConstant.MessageType.shareVideo) {
                    Video v = new Video();
                    v.setThreadId(mThreadId);
                    v.setThreadType(mThreadType);
                    v.setMessId(message.getId());
                    v.setDuration(message.getDuration());
                    v.setFilePath(message.getFilePath());
                    v.setFileSize(message.getSize());
                    try {
                        v.setFileOutputPath(FileHelper.createVideoFile().getCanonicalPath());
                    } catch (IOException e) {
                        Log.e(TAG, "Exception", e);
                    }
                    message.setVideo(v);
                    mApplication.getTransferFileBusiness().startUploadMessageFile(message);
                    Log.d(TAG, "ResendVideo");
                } else {
                    mApplication.getTransferFileBusiness().startUploadMessageFile(message);
                }
            }
            notifyDataAdapter();
        } else {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
        }
    }

    @Override
    public void replyClickCallBack(ReengMessage message) {
        showFooterReplyMessage(message);
    }

    @Override
    public void textContentClickCallBack(ReengMessage message) {
        Log.i(TAG, "textContentClickCallBack");
        if (mReengAccountBusiness.isTranslatable() &&
                message.getMessageType() == ReengMessageConstant.MessageType.text &&
                message.getDirection() == ReengMessageConstant.Direction.received) {
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT ||
                    mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                message.setShowTranslate(!message.isShowTranslate());
                notifyDataAdapter();
            }
        }
    }

    @Override
    public void fileContentClickCallBack(ReengMessage message, boolean isClickDownload) {
        Log.d(TAG, "file clicked");
        MessageHelper.checkDownloadOrOpenFile(mApplication, mChatActivity, message);
    }

    @Override
    public void imageContentClickCallBack(ReengMessage message) {
        ReengMessageConstant.Direction direction = message.getDirection();
        int messageStatus = message.getStatus();
        if (message.getMessageType() == ReengMessageConstant.MessageType.image_link) {
            String directLink = message.getDirectLinkMedia();
            if (!TextUtils.isEmpty(directLink)) {
                if (directLink.startsWith("mocha://")) {
                    DeepLinkHelper.getInstance().openSchemaLink(mChatActivity, directLink);
                } else {
                    UrlConfigHelper.gotoWebViewOnMedia(mApplication, mChatActivity, directLink);
                }
            }
        } else if (direction == ReengMessageConstant.Direction.send
                || (direction == ReengMessageConstant.Direction.received
                && (messageStatus == ReengMessageConstant.STATUS_RECEIVED || messageStatus == ReengMessageConstant.STATUS_SEEN))) {

            if (enableClickTag) {
                enableClickTag = false;
                // hide alert stranger
                if (mThreadMessage != null && mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                        mThreadMessage.isStranger() && !mThreadMessage.isJoined()) {
                    mMessageBusiness.updateJoinThreadStranger(mThreadMessage);
                    drawAlertStranger();
                }
                Intent intent = new Intent(mApplication, PreviewImageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, message.getThreadId());
                if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                    intent.putExtra(PreviewImageActivity.PARAM_LINK_IMAGE, UrlConfigHelper.getInstance(mApplication).getDomainImage() + message
                            .getDirectLinkMedia());
                }
                intent.putExtra(PreviewImageActivity.PARAM_CURRENT_IMAGE, message.getFilePath());
                intent.putExtra(PreviewImageActivity.PARAM_PRIVATE, mThreadMessage.isPrivateOrEncrypt());
                mApplication.startActivity(intent);
            } else
                enableClickTag = true;
        }
    }

    @Override
    public void voicemailContentClickCallBack(ReengMessage message) {
        Log.d(TAG, "voicemail clicked");
        if (message.getDirection() == ReengMessageConstant.Direction.send) {
            audioVoicemailPlayerImpl2.playVoicemail(message, mThreadDetailAdapter, mChatActivity);
            // voicemailPlayer.playVoicemail(message, position);
        } else if (message.getDirection() == ReengMessageConstant.Direction.received) {
            if (message.getStatus() == ReengMessageConstant.STATUS_RECEIVED) {
                // da tai thanh cong voicemail va luu vao file
                if (message.getFilePath() != null) {
                    audioVoicemailPlayerImpl2.playVoicemail(message, mThreadDetailAdapter, mChatActivity);
                } else {
                    // chua luu vao file
                    Log.d(TAG, "status == Message.STATUS_RECEIVED but filePath = null");
                    mChatActivity.showToast(R.string.cant_open_file);
                }
            }
        }
    }

    @Override
    public void shareContactClickCallBack(ReengMessage reengMessage) {
        String contactNumber = reengMessage.getFileName();
        String contactName = reengMessage.getContent();
        String myNumber = mReengAccountBusiness.getJidNumber();
        if (myNumber != null && myNumber.equals(contactNumber)) {
            NavigateActivityHelper.navigateToMyProfile(mChatActivity);
        } else {
            PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(contactNumber);
            if (mPhoneNumber == null) {
                mListener.addNewContact(contactNumber, contactName);
            } else {
                mListener.navigateToContactDetailActivity(mPhoneNumber.getId());
            }
        }
    }

    @Override
    public void voiceStickerClickCallBack(ReengMessage reengMessage, View convertView) {
        int collectionId = EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, itemId = 0;
        try {
            collectionId = Integer.valueOf(reengMessage.getFileName());
            itemId = (int) reengMessage.getSongId();
        } catch (NumberFormatException nfe) {
            Log.e(TAG, "Exception", nfe);
        }
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            playStickerMessage(collectionId, itemId);
        } else {
            StickerItem mStickerItem = mStickerBusiness.getStickerItem(collectionId, itemId);
            StickerCollection stickerCollection = mStickerBusiness.getStickerCollectionById(collectionId);
            if (stickerCollection == null ||
                    stickerCollection.getCollectionState() != StickerConstant.COLLECTION_STATE_ENABLE) {
                if (mStickerItem != null) {
                    playStickerMessage(collectionId, itemId);
                }
                //khong co thong tin bo hoac bo bi disable thi ko lam gi
            } else {
                if (stickerCollection.isDefault() == 1) {//bo general
                    if (mStickerItem != null) {
                        playStickerMessage(collectionId, itemId);
                    }
                } else if (stickerCollection.isDownloaded()) {
                    if (mStickerItem != null) {// item da down play luon
                        //hien thi binh thuong
                        playStickerMessage(collectionId, itemId);
//                        playGifStickerMessage(collectionId, itemId, convertView);
                    } else if (stickerCollection.isUpdateCollection()) {// chua co item -> update
                        setIntentUpdateSticker(collectionId);
                    }
                } else {// chua down collection
                    setIntentDownloadSticker(collectionId);
                }
            }
        }
    }

    @Override
    public void gifContentClickCallBack(ReengMessage reengMessage) {
        if (reengMessage.getStatus() == ReengMessageConstant.STATUS_RECEIVED) {
            InputMethodUtils.hideSoftKeyboard(etMessageContent, mChatActivity);
            PopupHelper.getInstance().showOrHidePopupPlayGif(mViewParent, reengMessage.getGifImgPath());
        }
    }

    private void setIntentDownloadSticker(int collectionId) {
        Intent intent = new Intent(mChatActivity, StickerActivity.class);
        intent.putExtra(StickerActivity.PARAM_STICKER_COLLECTION_ID, collectionId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mChatActivity.startActivityForResult(intent, Constants.ACTION.SET_DOWNLOAD_STICKER);
    }

    private void setIntentUpdateSticker(int collectionId) {
        Intent intent = new Intent(mChatActivity, StickerActivity.class);
        intent.putExtra(StickerActivity.PARAM_STICKER_COLLECTION_ID, collectionId);
        intent.putExtra(StickerActivity.PARAM_UPDATE_STICKER, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mChatActivity.startActivityForResult(intent, Constants.ACTION.SET_DOWNLOAD_STICKER);
    }

    private void initWindowShaker() {
        boolean isVibrate = mSettingBusiness.getPrefVibrate();
        option = new WindowShaker.Option.Builder(mChatActivity).setOffsetY(0).setVibrate(isVibrate).build();
    }

    private void playStickerMessage(int collectionId, int itemId) {
        try {
            mVoiceStickerPlayer.playVoiceSticker(collectionId, itemId);
            if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID
                    && itemId == EmoticonUtils.getGeneralStickerDefault()[EmoticonUtils.BUZZ_STICKER_POSITION].getItemId
                    ()) {
                if (mHandler == null) {
                    mHandler = new Handler();
                }
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        WindowShaker.shake(mChatActivity, option);
                    }
                }, 200);
            }
        } catch (Exception nfe) {
            Log.e(TAG, "Exception", nfe);
        }
    }

    private void playGifStickerMessage(final int collectionId, final int itemId, View convertView) {
        StickerItem mStickerItem = mStickerBusiness.getStickerItem(collectionId, itemId);
        if (mStickerItem == null || TextUtils.isEmpty(mStickerItem.getImagePath())) {
            return;
        }
        if (!TextUtils.isEmpty(mStickerItem.getType())
                && mStickerItem.getType().equals(StickerConstant.STICKER_TYPE_GIF)) {
            final ImageView gifImageView = convertView.findViewById(R.id.message_detail_file_item_content);
            try {
                gifDrawableDetail = new GifDrawableBuilder().from(mStickerItem.getImagePath()).build();
                gifImageView.setImageDrawable(gifDrawableDetail);
                gifDrawableDetail.setLoopCount(0);
                gifDrawableDetail.addAnimationListener(new AnimationListener() {
                    @Override
                    public void onAnimationCompleted(int loopNumber) {
                        /*if (loopNumber >= 3) {
                            if (gifDrawableDetail != null) {
                                gifDrawableDetail.stop();
                            }
                            ImageLoaderManager.getInstance(mApplication).displayStickerImage(gifImageView,
                                    collectionId, itemId);
                        }*/
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    @Override
    public void onMyAvatarClick() {
        NavigateActivityHelper.navigateToMyProfile(mChatActivity);
    }

    @Override
    public void onFriendAvatarClick(String numberJid, String friendName) {
        Log.d(TAG, "onFriendAvatarClick:---numberJid-> " + numberJid + " friendName: " + friendName);
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(numberJid);
        if (mPhoneNumber != null) {
            mListener.navigateToContactDetailActivity(mPhoneNumber.getId());
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().
                    getExistStrangerPhoneNumberFromNumber(numberJid);
            if (strangerPhoneNumber != null) {
                mListener.navigateToStrangerDetail(strangerPhoneNumber, null, null);
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                mListener.navigateToNonContactDetailActiviy(numberJid);
            } else if (!TextUtils.isEmpty(numberJid) && !TextUtils.isEmpty(friendName)) {
                mListener.navigateToStrangerDetail(null, numberJid, friendName);
            } else {
                mListener.navigateToNonContactDetailActiviy(numberJid);
            }
        }
    }

    @Override
    public void onAcceptInviteMusicClick(final ReengMessage reengMessage) {
        mMusicBusiness.showConfirmOrNavigateToSelectSong(mChatActivity, mThreadType,
                mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity), getFriendName(), new
                        MusicBusiness.OnConfirmMusic() {
                            @Override
                            public void onGotoSelect() {
                                acceptMusicMessage(reengMessage);
                            }

                            @Override
                            public void onGotoChange() {
                                acceptMusicMessage(reengMessage);
                            }
                        });
    }

    @Override
    public void onCancelInviteMusicClick(ReengMessage reengMessage) {
        reengMessage.setMusicState(ReengMessageConstant.MUSIC_STATE_CANCEL);
        String content = String.format(mApplication.getString(R.string.invite_share_music_canceled), mApplication
                .getString(R.string.you));
        reengMessage.setContent(content);
        reengMessage.setFileName(content);
        notifyChangeAndSetSelection();
        mMessageBusiness.updateAllFieldsOfMessage(reengMessage);
        if (friendPhoneNumber.equals(mMusicBusiness.getCurrentNumberFriend())) {
            mMusicBusiness.onLeaveMusic(false);
            mChatActivity.trackingEvent(R.string.ga_category_invite_music,
                    R.string.ga_cancel_invite_music, R.string.ga_cancel_invite_music);
        }
    }

    @Override
    public void onAcceptMusicGroupClick(ReengMessage reengMessage) {
        Log.d(TAG, "onAcceptMusicGroupClick");
        mMusicBusiness = mApplication.getMusicBusiness();
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        if (mMusicBusiness.isShowPlayerGroup(mThreadMessage.getServerId())) {
            showToastOrChangeSongGroup(reengMessage, false);
        } else if (reengMessage.getMusicState() == ReengMessageConstant.MUSIC_STATE_REQUEST_CHANGE) {
            mChatActivity.showToast(R.string.music_group_no_longer_exist);
        } else if (!mMusicBusiness.isExistListenMusic() ||
                mMusicBusiness.isExistListenerGroup() ||
                mMusicBusiness.isExistListenerRoom()) {
            acceptMusicMessage(reengMessage);
        } else {
            showConfirmLeaveAndInviteOrAcceptGroupMusic(mMusicBusiness.getCurrentNumberFriend(), reengMessage, false);
        }
    }

    @Override
    public void onFollowRoom(ReengMessage reengMessage) {
        Log.i(TAG, "onfllowroom");
        String roomId = reengMessage.getFilePath();
        String roomName = reengMessage.getFileName();
        String roomAvatar = reengMessage.getImageUrl();
//        DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mChatActivity,
//                roomId, roomName, roomAvatar);
    }

    @Override
    public void onReinviteShareMusicClick(ReengMessage reengMessage) {
        if (mViewVideo.getVisibility() != View.GONE) {
            onCloseWatchVideo();
        }
        if (!mMusicBusiness.isExistListenMusic()) {// khong trong cung nghe
            MediaModel songModel = reengMessage.getSongModel(mApplication.getMusicBusiness());
            removeMessageAndReinviteMusic(reengMessage, songModel);
        } else if (mMusicBusiness.isExistListenerGroup() ||
                mMusicBusiness.isExistListenerRoom()) {// cung nghe group,cung nghe room
            mMusicBusiness.clearSessionAndNotifyMusic();
            MediaModel songModel = reengMessage.getSongModel(mApplication.getMusicBusiness());
            removeMessageAndReinviteMusic(reengMessage, songModel);
        } else {
            String currentNumberInRoom = mMusicBusiness.getCurrentNumberFriend();
            showConfirmLeaveAndReInviteOrChangeSong(currentNumberInRoom, reengMessage, false);
        }
    }

    @Override
    public void onTryPlayMusicClick(ReengMessage reengMessage) {
        MediaModel songModel = reengMessage.getSongModel(mApplication.getMusicBusiness());
        if (songModel == null) {
            mChatActivity.showToast(mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
        } else {
            MoreAppInteractHelper.onPlayMediaClick(mChatActivity, songModel, mApplication, false);
        }
    }

    @Override
    public void onInviteMusicViaFBClick(ReengMessage reengMessage) {
        MediaModel songModel = reengMessage.getSongModel(mApplication.getMusicBusiness());
        if (songModel == null) {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
        } else {
            facebookHelper.shareContentToFacebook(mChatActivity, mChatActivity.getCallbackManager(), songModel.getUrl(),
                    null, null, null, null);
        }
    }

    /**
     * listener action change song
     *
     * @param packet
     */
    @Override
    public void onActionResponse(ReengMusicPacket packet) {
        Log.d(TAG, "onActionResponse: " + packet.toXML());
        //reset current message id
    }

    @Override
    public void onTimeOutRequest(ReengMusicPacket packet) {
        Log.d(TAG, "onTimeOutRequest: " + packet.toXML());
        if (mThreadMessage != null && mThreadMessage.isStranger()) {
            return;// khong lam gi
        }
        mApplication.getMusicBusiness().showDialogTimeoutChangeSong(packet, mChatActivity, getFragmentManager());
    }

    @Override
    public void videoContentClickCallBack(ReengMessage message, View convertView) {
        Log.d(TAG, "videoContentClickCallBack" + message.getStatus());
        MessageHelper.checkDownloadOrOpenVideo(mApplication, mChatActivity, message);
    }

    @Override
    public void onGreetingStickerPreviewCallBack(final StickerItem stickerItem) {
        Log.i(TAG, "onGreetingStickerPreviewCallBack");
        if (stickerItem == null) {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        File stickerFile = null;
        if (!TextUtils.isEmpty(stickerItem.getImagePath())) {
            stickerFile = new File(stickerItem.getImagePath());
        }
        if (stickerFile != null && stickerFile.exists() && stickerItem.isDownloadVoice()) {
            showPreviewStickerPopupFromStickerItem(stickerItem);
        } else {
            Log.i(TAG, "download voice va img");
            if (popupWindowPreview != null) {
                if (!popupWindowPreview.isShowing()) {
                    popupWindowPreview.showAtLocation(etMessageContent, Gravity.CENTER, 0, 0);
                }
            }
            popupViewPreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismissStickerPopupWindow();
                    sendMessageVoiceSticker(stickerItem.getCollectionId(), stickerItem.getItemId());
                }
            });
            mLayoutPopupPreview.setVisibility(View.VISIBLE);
            ImageLoaderManager.getInstance(mApplication).displayGifStickerNetwork(imgPreview,
                    stickerItem.getUrlImg());
            mProgressPreview.setVisibility(View.GONE);
            isShowPreviewDownloading = true;
            DownloadMultiFileAsysncTask downloadVoice = new DownloadMultiFileAsysncTask(mApplication, stickerItem) {
                @Override
                protected void onPostExecute(StickerItem result) {
                    super.onPostExecute(result);
                    if (result != null) {
                        mLayoutPopupPreview.setVisibility(View.VISIBLE);
                        mProgressPreview.setVisibility(View.GONE);
                        mStickerBusiness.updateStickerItem(result);
                        notifyDataAdapter();
                        if (isShowPreviewDownloading) {
                            showPreviewStickerPopupFromStickerItem(result);
                            isShowPreviewDownloading = false;
                        }
                    } else {
                        dismissStickerPopupWindow();
                        mChatActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }
            };
            downloadVoice.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void reportClickCallBack(ReengMessage message) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mChatActivity.showToast(R.string.error_internet_disconnect);
        } else if (SpamRoomManager.getInstance(mApplication).getTimeLock() > 0) {
            // dang bi lock. khong lam gi
        } else {
            ArrayList<ItemContextMenu> listItemContextMenu = MessageHelper.getListItemReport(mChatActivity, message);
            //file, image, voicemail, shareContact
            PopupHelper.getInstance().showContextMenu(mChatActivity, null, listItemContextMenu,
                    this);
        }
    }

    @Override
    public void onAcceptCrbtGift(ReengMessage message, MediaModel songModel) {
        mChatActivity.showLoadingDialog(null, R.string.waiting);
        CrbtRequestHelper.getInstance(mApplication).acceptCrbtMusic(mThreadMessage, message, message.getSender(),
                message.getCrbtGiftSession(), songModel,
                new CrbtRequestHelper.onResponseAcceptCrbt() {
                    @Override
                    public void onResponse() {
                        mChatActivity.hideLoadingDialog();
                        mChatActivity.showToast(R.string.request_success);
                        notifyDataAdapter();
                    }

                    @Override
                    public void onError(String msgError) {
                        mChatActivity.hideLoadingDialog();
                        mChatActivity.showToast(msgError, Toast.LENGTH_LONG);
                    }
                });
    }

    @Override
    public void onSendCrbtGift(ReengMessage message, MediaModel songModel) {
        if (songModel == null) {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        showConfirmGiftSong(message.getReceiver(), songModel);
    }

    @Override
    public void onDeepLinkClick(ReengMessage message, String link) {
        String serviceType = message.getFilePath();
        if (TextUtils.isEmpty(link)) {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        DeepLinkHelper.getInstance().openSchemaLink(mChatActivity, link, serviceType, message);
    }

    @Override
    public void onFakeMoClick(ReengMessage message) {
        String cmd = message.getFileId();
        if (!TextUtils.isEmpty(cmd)) {
            String confirmMsg = message.getVideoContentUri();
            ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, mChatActivity, confirmMsg, cmd, "unknown");
        }
    }

    @Override
    public void shareLocationClickCallBack(ReengMessage reengMessage) {
        if (mListener != null) {
            if (!LocationHelper.getInstant(mApplication).checkSupportGLEV2()) {
                mChatActivity.showToast(mRes.getString(R.string.not_support_gle_v2), Toast.LENGTH_LONG);
            } else if (LocationHelper.getInstant(mApplication).checkGooglePlayService()) {
                if (PermissionHelper.allowedPermission(mChatActivity, Manifest.permission.ACCESS_COARSE_LOCATION) && PermissionHelper.allowedPermission(mChatActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    mListener.navigateToViewLocation(reengMessage);
                } else {
                    PermissionHelper.requestPermissionWithGuide(mChatActivity,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
                }
            } else {
                LocationHelper.getInstant(mApplication).showDialogGGPlayService(mChatActivity, mIconListener);
            }
        }
    }

    @Override
    public void onInfoMessageCallBack() {
        showInfoMsg(!isShowInfo);
    }

    @Override
    public void onPollDetail(ReengMessage message, boolean pollUpdate) {
        mListener.navigateToPollActivity(mThreadMessage, message, message.getFileId(), Constants.MESSAGE.TYPE_DETAIL_POLL, pollUpdate);
    }

    @Override
    public void onCall(ReengMessage message) {
        if (message.getMessageType() == ReengMessageConstant.MessageType.talk_stranger) {
            //DeepLinkHelper.getInstance().handlerPostStrangerConfide(mApplication, mChatActivity, true);
        } else if (message.getChatMode() == ReengMessageConstant.MODE_VIDEO_CALL) {
            mApplication.getCallBusiness().checkAndStartVideoCall(mChatActivity, mThreadMessage, true);
        } else {
            mApplication.getCallBusiness().checkAndStartCall(mChatActivity, mThreadMessage, true);
        }
    }

    @Override
    public void onLuckyWheelHelpClick(final ReengMessage message) {
        LuckyWheelHelper.getInstance(mApplication).doRequestSosAccept(mChatActivity, message.getSender(), new
                LuckyWheelHelper.SosFriendAcceptListener() {
                    @Override
                    public void onSuccess() {
                        message.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
                        mMessageBusiness.updateAllFieldsOfMessage(message);
                        notifyDataAdapter();
                    }
                });
    }

    @Override
    public void onWatchVideoClick(ReengMessage message, final boolean isUser) {

        if (!mApplication.getConfigBusiness().isEnableWatchVideoTogether()) {
            if (mApplication.getReengAccountBusiness().isCambodia() && mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {

            } else {
                mChatActivity.showToast(R.string.e666_not_support_function);
                return;
            }
        }
        MediaModel mediaModel = new Gson().fromJson(message.getDirectLinkMedia(), MediaModel.class);
        if (mediaModel == null) {
            mChatActivity.showToast(R.string.e666_not_support_function);
            return;
        }
        mMusicBusiness.showConfirmCloseMusicAndPlayVideo(mChatActivity, new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                mMusicBusiness.closeMusic();
                if (mApplication.getPlayMusicController().isPlaying()) {
                    mApplication.getPlayMusicController().closeMusic();
                }
                MediaModel videoModel = (MediaModel) result;
                if (mViewVideo.getVisibility() == View.VISIBLE && videoModel.getId().equals(mThreadMessage
                        .getMediaModel().getId())) {
                    if (isUser && !needPauseVideo && mVideo != null && isPrepared && !mVideo.isPlaying()) {
                        if (isPauseVideo) {
                            mImgThumbVideo.setVisibility(View.GONE);
                            mVideo.seekTo(positionSeekVideo);
                        }
                        mVideo.start();
                        isPauseVideo = false;
                        setBackgroundImageMediaControll(true);
                        updateProgressBar();
                        startHideBar();
                    } else {
                        mChatActivity.showToast(R.string.text_watching_video);
                    }
                    return;
                }
                playVideo(videoModel, 0);
            }
        }, mediaModel);
    }

    @Override
    public void onBplus(ReengMessage message) {
        if (mApplication.getConfigBusiness().isEnableBankplus()) {
            long bplusAmount = TextHelper.convertTextDecemberToLong(message.getImageUrl(), 0);
            BankPlusHelper.getInstance(mApplication).checkAndTransferMoney(mChatActivity, friendPhoneNumber,
                    getFriendName(),
                    bplusAmount, message.getVideoContentUri(), message.getFileId());
        } else {
            mChatActivity.showToast(R.string.e666_not_support_function);
        }
    }

    @Override
    public void onClickPreviewUrl(ReengMessage message, String url) {
        Utilities.processOpenLink(mApplication, mChatActivity, url);
//        if (TextUtils.isEmpty(url)) return;
//        onClickUrl(url);
//        UrlConfigHelper.getInstance(mChatActivity).gotoWebViewOnMedia(mApplication, mChatActivity, url);
        /*FragmentManager fragmentManager = getFragmentManager();
        PopupHelper.getInstance(mChatActivity).showContextMenuSmartText(mChatActivity, fragmentManager,
                url, url, Constants.SMART_TEXT.TYPE_URL, mIconListener);*/
    }

    @Override
    public void onClickOpenGiftLixi(final ReengMessage message) {
        if (mApplication.getConfigBusiness().isEnableBankplus()) {
            if (message.getSize() == 0) {
                mChatActivity.showLoadingDialog("", R.string.loadding_data);
                BankPlusHelper.CheckAccountCallBack callBack = new BankPlusHelper.CheckAccountCallBack() {
                    @Override
                    public void onResult(boolean isUserBPlus) {
                        if (isUserBPlus) {
                            BankPlusHelper.getInstance(mApplication).openGiftLixi(message.getFilePath(), new BankPlusHelper
                                    .RequestOpenLixiCallBack() {

                                @Override
                                public void onSuccess(long amountMoney) {
                                    mChatActivity.hideLoadingDialog();
                                    message.setImageUrl(String.valueOf(amountMoney));
                                    message.setSize(1);
                                    mMessageBusiness.updateAllFieldsOfMessage(message);
                                    notifyDataAdapter();
                                    BankPlusHelper.getInstance(mApplication).showDialogLixi(mChatActivity, mThreadMessage, message,
                                            new PopupReceiveLixi.LixiListener() {
                                                @Override
                                                public void onShare(Bitmap bitmap) {
                                                    mChatActivity.shareImageFacebook(bitmap, mRes.getString(R.string
                                                            .ga_facebook_label_share_lixi));
                                                    hideCusKeyboard();
                                                }
                                            });
                                }

                                @Override
                                public void onError(int errorCode, String msg) {
                                    mChatActivity.hideLoadingDialog();
                                    if (errorCode == -1) {
                                        mChatActivity.showToast(R.string.e601_error_but_undefined);
                                    } else {
                                        mChatActivity.showToast(msg);
                                    }
                                }
                            });
                        } else {
                            mChatActivity.hideLoadingDialog();
                            DialogConfirm dialog = new DialogConfirm(mChatActivity, true)
                                    .setLabel(null)
                                    .setMessage(mRes.getString(R.string.bplus_msg_register_open))
                                    .setNegativeLabel(mRes.getString(R.string.non_skip))
                                    .setPositiveLabel(mRes.getString(R.string.non_register))
                                    .setPositiveListener(new PositiveListener<Object>() {
                                        @Override
                                        public void onPositive(Object result) {
                                            BplusSdk.register(mChatActivity, new ResultListener<String>() {
                                                @Override
                                                public void success(String s) {
                                                    Log.d(TAG, "BplusSdk.register success - s: " + s);
                                                    mChatActivity.showToast(R.string.onmedia_action_success);
                                                }

                                                @Override
                                                public void failed(int i, String s) {// không thông báo gì, sdk b+ thông báo
                                                    Log.d(TAG, "BplusSdk.register failed: " + i + " - s: " + s);
                                                }
                                            });
                                        }
                                    });
                            dialog.show();
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        mChatActivity.hideLoadingDialog();
                        mChatActivity.showToast(R.string.e601_error_but_undefined);
                    }
                };
                BankPlusHelper.getInstance(mApplication).checkUserViettelPay(mChatActivity, callBack);
            } else {
                BankPlusHelper.getInstance(mApplication).showDialogLixi(mChatActivity, mThreadMessage, message,
                        new PopupReceiveLixi.LixiListener() {
                            @Override
                            public void onShare(Bitmap bitmap) {
                                mChatActivity.shareImageFacebook(bitmap, mRes.getString(R.string
                                        .ga_facebook_label_share_lixi));
                                hideCusKeyboard();
                            }
                        });
            }
        } else {
            mChatActivity.showToast(R.string.e666_not_support_function);
        }
    }

    @Override
    public void onClickReplyLixi(ReengMessage message) {
        if (mApplication.getConfigBusiness().isEnableLixi()) {
            onChatBarClickBottomAction(BottomChatOption.Constants.OPTION_GIFT_LIXI);
        } else {
            mChatActivity.showToast(R.string.e666_not_support_function);
        }
    }

    //TODO click intro
    public void onClickIntroGroupChat(int index) {
        switch (index) {
            case 0: //click tai nghe
                /*if (mThreadMessage != null &&
                        mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                        mThreadMessage.getStateOnlineStar() != 1) {
                    mChatActivity.showToast(R.string.warning_star_offline);
                    return;
                }
                // voi man hinh nho
                goToSelectSongOrShowError(false);
                if (mVideo != null && mViewVideo.getVisibility() != View.GONE) {
                    onCloseWatchVideo();
                }*/
                mChatActivity.showViewSetting();
                break;
            case 1:
                Log.i(TAG, "click 1");
                break;
            case 2: //ba cham, tao cuoc tham do
//                onClick(mImgMenuAttach);
                if (mListener != null) {
                    mListener.navigateToPollActivity(mThreadMessage, null, null, Constants.MESSAGE.TYPE_CREATE_POLL, false);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "----------PREFIX----------onContactChange");
                setEnableOptionAddContact();
                drawActionbar(null);
                setViewHeaderProfile();
                notifyDataAdapter();
            }
        });
    }

    @Override
    public void onPresenceChange(final ArrayList<PhoneNumber> phoneNumbers) {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "onPresenceChange: phone=" + phoneNumber.getJidNumber());
                for (PhoneNumber phoneNumber : phoneNumbers) {
                    if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                            friendPhoneNumber.equals(phoneNumber.getJidNumber())) {
                        if (!TextUtils.isEmpty(phoneNumber.getOperatorPresence())) {
                            Log.f(TAG, "old operator: " + friendOperator + " new operator: " + phoneNumber.getOperatorPresence());
                            friendOperator = phoneNumber.getOperatorPresence();
                        }
                        if (phoneNumber.getStatePresence() != -1)
                            cState = phoneNumber.getStatePresence();

                        if (phoneNumber.getUsingDesktop() != -1)
                            usingDesktop = phoneNumber.getUsingDesktop();

                        String newStatus = mContactBusiness.getLastSeenFromPhoneNumber(phoneNumber);
                        if (newStatus != null && newStatus.equals(getResources().getString(R.string.online))) {
                            mImgMenuStatus.setVisibility(View.VISIBLE);
                        }else{
                            mImgMenuStatus.setVisibility(View.INVISIBLE);
                        }
                        if (TextUtils.isEmpty(newStatus) && !TextUtils.isEmpty(mStatus)) {
                            //ko set lại status ở case này
                        } else {
                            mStatus = newStatus;
                        }
                        mApplication.logDebugContent("onPresenceChange lastseen " + phoneNumber.getJidNumber() + ": " +
                                mStatus);
                        Log.d(TAG, "----------PREFIX-------------onPresenceChange: " + phoneNumber.toString());
                        drawActionbar(mStatus);
                        if (phoneNumber.isReeng()) {
                            mIsReengUser = true;
                            mGsmMode = false;
                            checkToShowListViewNotifyHeader();
                            onGsmModeChangeListener(mGsmMode, mIsReengUser);
                        } else {
                            mIsReengUser = false;
                        }
                        checkViettel();
                        mKeyboardController.setViettel(isViettel);
                        setViewHeaderProfile();
                    } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                        // group (truong hop thay ten, thay avatar... thanh vien trong nhom)
                        Log.i(TAG, "ThreadDetailAdapter on presence change");
                    }
                }
                notifyDataAdapter();
            }
        });
    }

    @Override
    public void onRosterChange() {
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    Log.d(TAG, "onRosterChange");
                    drawActionbar(null);
                } else {   // group (truong hop thay ten, thay avatar... thanh vien trong nhom)
                    Log.i(TAG, "ThreadDetailAdapter on roster change");
                    notifyDataAdapter();
                }
            }
        });
    }

    public void onGsmModeChangeListener(boolean gsmMode, boolean isReengUser) {
        this.mGsmMode = gsmMode;
        this.mIsReengUser = isReengUser;
        if (mKeyboardController != null) {
            mKeyboardController.setGsmMode(mGsmMode, mThreadType);
        } else {
            Log.i(TAG, "onGsmModeChangeListener mKeyboardController = null");
        }
        if (mGsmMode) {
            Log.d(TAG, "chua lam gi");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged ");
        super.onConfigurationChanged(newConfig);
        if (mHandler == null) {
            mHandler = new Handler(Looper.getMainLooper());
        }
        PopupHelper.getInstance().destroyBottomChatOption();
        PopupHelper.getInstance().destroyPopupPlayGif();
        mKeyboardController.onScreenStateChange(newConfig, mHandler);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (mVideo != null && mViewVideo.getVisibility() == View.VISIBLE) {
                if (isFullScreen) {
                    smallScreen();
                }
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (mVideo != null && mViewVideo.getVisibility() == View.VISIBLE) {
                if (!isFullScreen) {
                    fullScreen();
                }
            }
        }
    }

    private void drawActionbar(String status) {
        Log.d(TAG, "----------PREFIX----------drawActionbar: " + status);
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }

        if (mThreadMessage == null) {
            mChatActivity.showToast(R.string.e601_error_but_undefined);
            mChatActivity.finish();
        }

        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            mImgAddFriend.setVisibility(View.GONE);
            mImgRoomStar.setVisibility(View.GONE);
            mChatActivity.setEnableBothSideSlideMenu(true);
            String number = friendPhoneNumber;
            boolean showCallFree;
            String friendName = getFriendName();
            if (friendName != null) {
                mTvName.setText(friendName);
            } else {
                mTvName.setText(number);
            }
            PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
            if (phoneNumber != null) {
//                mStatus = mContactBusiness.getLastSeenFromPhoneNumber(phoneNumber);
                if (!TextUtils.isEmpty(status)) {
                    mStatus = status;
                } else {
                    mStatus = mContactBusiness.getLastSeenFromPhoneNumber(phoneNumber);
                    Log.d(TAG, "----------PREFIX----------getLastSeenFromPhoneNumber: " + mStatus);
                }
                showCallFree = phoneNumber.isReeng();
            } else {
                NonContact nonContact = mContactBusiness.getExistNonContact(number);
                showCallFree = nonContact != null && nonContact.isReeng();
                if (status != null) {// status so chua luu danh ba
                    mStatus = status;
                    Log.d(TAG, "----------PREFIX----------status so chua luu danh ba: " + mStatus);
                }
            }
            if (mThreadMessage == null) {
                mImgMenuCall.setVisibility(View.GONE);
                mImgMenuVideoCall.setVisibility(View.GONE);
                mImgMenuStatus.setVisibility(View.INVISIBLE);
            } else if (mThreadMessage.isStranger()) {
                //mImgMenuCall.setImageResource(R.drawable.ic_ab_callout);
                mImgMenuCall.setVisibility(View.GONE);
                mImgMenuVideoCall.setVisibility(View.GONE);
                mImgMenuStatus.setVisibility(View.GONE);
            } else if ((showCallFree && mApplication.getReengAccountBusiness().isCallEnable())
                    || mApplication.getReengAccountBusiness().isEnableCallOut()) {
                mImgMenuCall.setImageResource(R.drawable.chat_ic_call);
                mImgMenuCall.setVisibility(View.VISIBLE);
                if (!showCallFree) {
                    mImgMenuVideoCall.setVisibility(View.GONE);
                    mImgMenuStatus.setVisibility(View.GONE);
                } else
                    mImgMenuVideoCall.setVisibility(View.VISIBLE);
            } else {
                mImgMenuCall.setVisibility(View.GONE);
                mImgMenuVideoCall.setVisibility(View.GONE);
                mImgMenuStatus.setVisibility(View.GONE);
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            mImgAddFriend.setVisibility(View.VISIBLE);
            mImgRoomStar.setVisibility(View.GONE);
            mChatActivity.setEnableBothSideSlideMenu(true);
            //draw title of group
            mTvName.setText(mThreadMessage.getThreadName());
            mImgMenuCall.setVisibility(View.GONE);
            mImgMenuVideoCall.setVisibility(View.GONE);
            mImgMenuStatus.setVisibility(View.INVISIBLE);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mImgAddFriend.setVisibility(View.GONE);
            mChatActivity.setOnlyLeftSlideMenuEnable();
            mTvName.setText(mThreadMessage.getThreadName());
            mImgRoomStar.setVisibility(View.VISIBLE);
            mImgRoomStar.setImageResource(R.drawable.ic_room_star);
            mImgMenuCall.setVisibility(View.GONE);
            mImgMenuVideoCall.setVisibility(View.GONE);
            mImgMenuStatus.setVisibility(View.INVISIBLE);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mChatActivity.setOnlyLeftSlideMenuEnable();
            mTvName.setText(mMessageBusiness.getThreadName(mThreadMessage));
            mImgRoomStar.setVisibility(View.VISIBLE);
            mImgRoomStar.setImageResource(R.drawable.ic_ab_thread_official);
            mImgMenuCall.setVisibility(View.GONE);
            mImgMenuVideoCall.setVisibility(View.GONE);
            mImgMenuStatus.setVisibility(View.INVISIBLE);
        } else {// broadcast
            mImgAddFriend.setVisibility(View.GONE);
            mImgRoomStar.setVisibility(View.GONE);
            mChatActivity.setOnlyLeftSlideMenuEnable();
            mTvName.setText(mMessageBusiness.getThreadName(mThreadMessage));
            mTvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_ab_broadcast, 0);
            mImgMenuCall.setVisibility(View.GONE);
            mImgMenuVideoCall.setVisibility(View.GONE);
            mImgMenuStatus.setVisibility(View.INVISIBLE);
        }
        drawStatus();
    }

    @Override
    public void onXMPPConnected() {
        // thread 1-1, so ko luu danh ba
        if (mMessageBusiness.checkNumberThreadNonContact(mThreadMessage)) {
            mContactBusiness.getInfoNumber(friendPhoneNumber);
        }
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideConnectStateView();
            }
        });

        // subcribe lai khi reconnect
        PubSubManager pubSubManager = PubSubManager.getInstance(mApplication);
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            pubSubManager.processSubscribeRoomChat(mThreadMessage, mOfficerAccountRoomChat, followRoomListener);
        } else {
            pubSubManager.pushToSubscribeList(mThreadMessage);
        }
    }

    @Override
    public void onXMPPDisconnected() {
       /* PubSubManager.getInstance(mApplication).clearSubscribeList();
        PubSubManager.getInstance(mApplication).clearSubscribeRoom();*/
    }

    @Override
    public void onXMPPConnecting() {
        Log.d(TAG, "onXmppConnecting");
    }

    public void hideCusKeyboard() {
        mKeyboardController.hideSoftAndCustomKeyboard();
        dismissStickerPopupWindow();
    }

    private void stopCountDownTyping() {
        if (countDownTimerTyping != null) {
            countDownTimerTyping.cancel();
            countDownTimerTyping = null;
            mStringCompareTyping = "";
        }
    }

    private void startCountDown() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            mMessageBusiness.sendTypingMessage(friendPhoneNumber, mThreadType);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            //TODO send typing group
            mMessageBusiness.sendTypingMessage(mThreadMessage.getServerId(), mThreadType);
        }
        countDownTimerTyping = new CountDownTimer(TIME_DEFAULT_RESEND_TYPING, TIME_DEFAULT_RESEND_TYPING) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                try {
                    countDownTimerTyping = null;
                    mStringCompareTyping = etMessageContent.getText().toString().trim();
                    Log.i(TAG, "finish typing countDownTimerTyping=null");
                } catch (Exception e) {
                    Log.i(TAG, "Exception", e);
                }
            }
        }.start();
    }

    private void setTypingMessageListener() {
        if (mThreadType <= ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            etMessageContent.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String content = etMessageContent.getText().toString();
                    if (mTypingManager != null) {
                        mTypingManager.onTextChanged(content);
                    }
                    if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                        mCurentStringTyping = content.trim();
                        if (isDraft) {
                            isDraft = false;
                        } else if (countDownTimerTyping == null
                                && mCurentStringTyping.length() >= LENGTH_TO_SEND_TYPING
                                && !mCurentStringTyping.equals(mStringCompareTyping)) {
                            if (!TextUtils.isEmpty(mStatus) && mStatus.equals(mRes.getString(R.string.online))) {
                                mStringCompareTyping = etMessageContent.getText().toString().trim();
                                Log.i(TAG, "onTextChanged: " + mStringCompareTyping);
                                startCountDown();
                            }
                        }
                        /*if(TextUtils.isEmpty(mCurentStringTyping)){
                            mFabListener.setVisibility(View.GONE);
                        } else
                            mFabListener.setVisibility(View.VISIBLE);*/
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
    }

    private void startCountdownTimerReceiveTyping() {
        Log.i(TAG, "startCountdownTimerReceiveTyping");
        countDownTimerReceiveTyping = new CountDownTimer(TIME_DEFAULT_SHOW_TYPING, TIME_DEFAULT_SHOW_TYPING) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "End Typing");
                drawStatus();
            }
        }.start();
    }

    ///////////////////////////////////////////// gui reeng message////////////////////////////

    /**
     * gui message text
     *
     * @param content
     */
    // TODO đoạn send message này cũng có thể gộp chung vào 1 hàm và để ở business cho đỡ rối (giống hàm forward
    // message)
    private void sendMessageText(String content, ReplyMessage replyMessage, ArrayList<TagMocha> mListTag) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            mChatActivity.showToast(mChatActivity.getResources().getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        String receiver = mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        if (OfficialActionManager.getInstance(mApplication).checkAndSendOfficialActionFromString(mThreadMessage,
                content)) {
            notifyChangeAndSetSelection();
            return;// goi api official action
        }
        SoloSendTextMessage textMessage = new SoloSendTextMessage(mThreadMessage, userNumber, receiver, content);
        textMessage.setReplyMessage(replyMessage);
        if (mListTag != null && !mListTag.isEmpty()) {
            String textTag = TagHelper.getTextTag(mListTag);
            Log.i(TAG, "textTag: " + textTag);
            textMessage.setTagContent(textTag);
            textMessage.setListTagContent(mListTag);
        }
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && isSendSMSout) {
            textMessage.setChatMode(ReengMessageConstant.MODE_GSM);
        } else {
            textMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        }
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, textMessage)) {
            textMessage.setCState(cState);
            notifyChangeAndSetSelection();
            boolean isSuccess = mMessageBusiness.sendXMPPMessage(textMessage, mThreadMessage);
            hideFooterReplyMessage();
            if (isSuccess && srcScreenId == Constants.SCREEN.SRC_CREATE_CHAT) {

            }
        }
    }

    /**
     * gui message image
     *
     * @param paths
     */
    private void sendMessageImage(ArrayList<String> paths) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mMessageBusiness.createAndSendMessageImage(mChatActivity, mThreadMessage, paths, false);
        notifyChangeAndSetSelection();
        mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_send_message, R.string.send_image);
    }

    // tam thoi chua gui file
    private void sendMessageFile(String filePath, String fileName, String extension) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        String fileType = ReengMessageConstant.FileType.fromString(extension).toString();
        mMessageBusiness.createAndSendMessageFile(mChatActivity, mThreadMessage, filePath, fileType, fileName);
        notifyChangeAndSetSelection();
        mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_send_message, R.string.send_file);
    }

    /**
     * gui message voice mail
     *
     * @param filePath
     * @param fileName
     * @param duration
     */
    private void sendMessageVoiceMail(String filePath, String fileName, int duration) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return;// room chat khong cho gui voice mail
        }
        String receiver = mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        SoloSendVoicemailMessage voicemailMessage = new
                SoloSendVoicemailMessage(mThreadMessage, userNumber, receiver,
                filePath, duration, fileName);
        voicemailMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, voicemailMessage)) {
            mApplication.getTransferFileBusiness().startUploadMessageFile(voicemailMessage);
            notifyChangeAndSetSelection();
            mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_send_message, R.string.send_voicemail);
        }
    }

    /**
     * send message contact
     *
     * @param contactName
     * @param jidNumber
     */
    private void sendMessageContact(String contactName, String jidNumber) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return;// room chat khong cho gui contacts
        }
        String receiver = mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        SoloSendShareContactMessage shareContactMessage =
                new SoloSendShareContactMessage(mThreadMessage, userNumber, receiver,
                        contactName, jidNumber);
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, shareContactMessage)) {
            shareContactMessage.setCState(cState);
            notifyChangeAndSetSelection();
            mMessageBusiness.sendXMPPMessage(shareContactMessage, mThreadMessage);
            mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_send_message, R.string.send_share_contact);
        }
    }

    /**
     * send voice sticker
     *
     * @param collectionId
     * @param itemId
     */
    private void sendMessageVoiceSticker(int collectionId, int itemId) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        String receiver = mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        SoloVoiceStickerMessage voiceStickerMessage = new SoloVoiceStickerMessage(mThreadMessage,
                userNumber, receiver, collectionId, itemId);
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, voiceStickerMessage)) {
            voiceStickerMessage.setCState(cState);
            notifyChangeAndSetSelection();
            mMessageBusiness.sendXMPPMessage(voiceStickerMessage, mThreadMessage);
            trackingEventSendVoiceSticker(voiceStickerMessage);
            // insert hoac update thanh cong thi moi refress
            if (mStickerBusiness.insertOrUpdateIfExistStickerRecent(itemId, collectionId)) {
                if (mKeyboardController != null) {//refresh
                    mKeyboardController.notifyChangeRecentSticker(mStickerBusiness.getListRecentSticker());
                }
            }
        }
    }

    /**
     * send location
     *
     * @param content
     * @param latitude
     * @param longitude
     */
    private void sendMessageLocation(String content, String latitude, String longitude) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            return;// room chat khong cho gui location
        }
        String receiver = mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        SoloShareLocationMessage locationMessage = new SoloShareLocationMessage(mThreadMessage,
                userNumber, receiver, content, latitude, longitude);
        locationMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, locationMessage)) {
            locationMessage.setCState(cState);
            notifyChangeAndSetSelection();
            mMessageBusiness.sendXMPPMessage(locationMessage, mThreadMessage);
            mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_send_message, R.string.send_share_location);
        }
    }

    private void sendMessageVideo(String filePath, String fileName, int duration, int fileSize, Video video) {
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mMessageBusiness.notifyWhenGroupNoExist(mChatActivity, mThreadMessage)) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT ||
                mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mChatActivity.showToast(mChatActivity.getString(R.string.not_support), Toast.LENGTH_SHORT);
            return;// room chat khong cho gui video
        }
        String receiver = mMessageBusiness.getReceivedWhenSendMessage(mThreadMessage, mChatActivity);
        if (TextUtils.isEmpty(receiver)) {
            return;
        }
        String contentUri = FileHelper.getVideoContentUri(mApplication, filePath);
        sendVideoMessage = new SoloShareVideoMessage(mThreadMessage, userNumber, friendPhoneNumber,
                filePath, duration, fileName, fileSize, contentUri, "");
        sendVideoMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        video.setThreadId(mThreadId);
        video.setThreadType(mThreadType);
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, sendVideoMessage)) {
            sendVideoMessage.setVideo(video);
            mApplication.getTransferFileBusiness().startUploadMessageFile(sendVideoMessage);
            notifyChangeAndSetSelection();
        }
    }

    /**
     * gui thong ke ga send voice sticker
     *
     * @param voiceStickerMessage
     */
    private void trackingEventSendVoiceSticker(SoloVoiceStickerMessage voiceStickerMessage) {
        int gaStickerCategory = R.string.ga_category_voice_sticker;
        // set action default
        int collectionId = Integer.valueOf(voiceStickerMessage.getFileName());
        int itemId = (int) voiceStickerMessage.getSongId();
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            mChatActivity.trackingEvent(gaStickerCategory,
                    mRes.getString(R.string.ga_action_voice_sticker_defaule),
                    String.format(mRes.getString(R.string.ga_label_voice_sticker_item), itemId));
        } else {
            StickerCollection stickerCollection = mStickerBusiness.getStickerCollectionById(collectionId);
            if (stickerCollection != null) {
                String collectionName = stickerCollection.getCollectionName();
                mChatActivity.trackingEvent(gaStickerCategory, collectionName,
                        String.format(mRes.getString(R.string.ga_label_voice_sticker_item), itemId));
            }
        }
        mChatActivity.trackingEvent(gaCategoryId, R.string.ga_action_send_message, R.string.send_sticker);
    }

    public void getFriendPhoneNumber() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mThreadMessage != null) {
            friendPhoneNumber = mThreadMessage.getSoloNumber();
        }
    }

    private String getFriendName() {
        if (mThreadMessage == null) {
            Log.e(TAG, "mThreadMessage null @@");
            return mRes.getString(R.string.app_name);
        }
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            friendPhoneNumber = mThreadMessage.getSoloNumber(); //bo sung de tranh truong hop null
            return mMessageBusiness.getFriendName(friendPhoneNumber);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            return mThreadMessage.getThreadName();
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return mThreadMessage.getThreadName();
        } else { //official
            return mThreadMessage.getThreadName();
        }
    }

    //===============SERVICE MEDIA======//
    @Override
    public void onChangeStateRepeat(int state) {

    }

    @Override
    public void onChangeStateNone() {
        Log.i(TAG, "onChangeStateNone ");
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mControlMedia != null) {
                    mControlMedia.setStateMediaPlayer(Constants.PLAY_MUSIC.PLAYING_NONE);
                    checkShowMedia();
                }
            }
        });
    }

    @Override
    public void onChangeStateGetData() {
        Log.i(TAG, "onChangeStateGetData ");
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mControlMedia != null) {
                    mControlMedia.setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
                }
            }
        });
    }

    @Override
    public void onChangeStatePreparing(MediaModel song) {
        Log.i(TAG, "onChangeStatePreparing ");
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mViewVideo.getVisibility() != View.GONE) {
                    onCloseWatchVideo();
                }
                if (mControlMedia != null) {
                    mControlMedia.setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
                    checkShowMedia();
                }
            }
        });
    }

    @Override
    public void onChangeStatePlaying(MediaModel song) {
        Log.i(TAG, "onChangeStatePlaying ");
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mControlMedia != null) {
                    checkShowMedia();
                }
            }
        });
    }

    @Override
    public void onActionFromUser(MediaModel song) {

    }

    @Override
    public void onShowMusicBar(boolean isShow) {
        mKeyboardController.updateListMessageHeight();
        if (isShow) {
            viewPinMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCloseMusic() {
        Log.i(TAG, "CLose music");
    }

    @Override
    public void onCallLoadDataFail(String message) {

    }

    @Override
    public void onUpdateSeekBarProgress(final int percent, final int mediaPosition) {
        //fai chay tai ui thread
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mControlMedia != null) {
                    mControlMedia.setProgress(percent, mediaPosition);
                }
            }
        });
    }

    @Override
    public void onUpdateSeekBarBuffering(int percent) {

    }

    @Override
    public void onChangeStateInfo(int state) {

    }

    @Override
    public void onUpdateData(MediaModel song) {

    }

    public void updateBackgroundUI(final String filePath) {
        if (mHandler == null) mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ImageLoaderManager.getInstance(mChatActivity).displayBackgroundOfThreadDetail(filePath, mImgBackground);
            }
        }, 100);
    }

    private void playStickerWhenCreateThread() {
        if (settingAutoPlaySticker) {
            final ReengMessage message = mMessageBusiness.getLastVoiceStickerUnread(mMessageArrayList);
            if (message != null && message.getSize() != 1) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int collectionId = Integer.valueOf(message.getFileName());
                        int itemId = (int) message.getSongId();
                        playStickerMessage(collectionId, itemId);
                    }
                }, 300);
            }
        }
    }

    private void initKeyguardReceiver() {
        if (mKeyguardGoneReceiver == null) {
            mKeyguardGoneReceiver = new KeyguardGoneReceiver();
            mKeyguardGoneReceiver.setKeyguardGoneListener(this);
        }
        IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
        mChatActivity.registerReceiver(mKeyguardGoneReceiver, filter);
    }

    protected void unRegisterKeyguardReceiver() {
        try {
            if (mKeyguardGoneReceiver != null) {
                mChatActivity.unregisterReceiver(mKeyguardGoneReceiver);
                mKeyguardGoneReceiver = null;
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    private void handleRestoreReengMessage(final ReengMessage message) {
        OutgoingMessageProcessor.OnResponseHanderMessageListener listener = new OutgoingMessageProcessor
                .OnResponseHanderMessageListener() {
            @Override
            public void onRestoreSuccess() {
                mChatActivity.hideLoadingDialog();
                notifyDataAdapter();
            }

            @Override
            public void onError(String msgError) {
                mChatActivity.hideLoadingDialog();
                mChatActivity.showToast(msgError, Toast.LENGTH_LONG);
            }
        };
        int groupSize = 0;
        if (mThreadMessage.getPhoneNumbers() != null) {
            groupSize = mThreadMessage.getPhoneNumbers().size();
        }
        mChatActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
        mMessageBusiness.getOutgoingMessageProcessor().
                handleRestoreReengMessage(message, mThreadType, groupSize, listener);
    }

    private boolean isListviewShowBottom() {
        try {
            if (mLvwContent == null || mMessageArrayList == null) {
                return false;
            } else return mLvwContent.getLastVisiblePosition() == (mMessageArrayList.size() - 1 +
                    mLvwContent.getHeaderViewsCount()) && mLvwContent.getChildAt(mLvwContent.getChildCount() - 1)
                    .getBottom() <= mLvwContent.getHeight();
        } catch (Exception e) {
            // something getChildAt().getBottom() null
            Log.e(TAG, "Exception", e);
        }
        return false;
    }

    private boolean isForwardMessageText() {
        if (mForwardingMessage == null) {
            return false;
        } else {
            return mForwardingMessage.isForwardingMessage()
                    && mForwardingMessage.getMessageType() == ReengMessageConstant.MessageType.text;
        }
    }

    private void setActionBlock(final boolean isBlock, String type) {
        mChatActivity.showLoadingDialog("", mRes.getString(R.string.processing), true);
        ArrayList<BlockContactModel> blockList = new ArrayList<>();
        blockList.add(new BlockContactModel(friendPhoneNumber, isBlock ? 1 : 0));
        ContactRequestHelper.getInstance(mApplication).handleBlockNumbers(blockList, new
                ContactRequestHelper.onResponseBlockContact() {
                    @Override
                    public void onResponse(String response) {
                        mChatActivity.hideLoadingDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                String msg;
                                if (isBlock) {
                                    msg = String.format(mRes.getString(R.string.msg_block_success),
                                            mTvName.getText().toString().trim());
                                    mBlockBusines.addBlockNumber(friendPhoneNumber);
                                } else {
                                    msg = String.format(mRes.getString(R.string.msg_unblock_success),
                                            mTvName.getText().toString().trim());
                                    mBlockBusines.removeBlockNumber(friendPhoneNumber);
                                }
                                ReengMessage message = mMessageBusiness.insertMessageNotify(msg,
                                        friendPhoneNumber, mApplication.getReengAccountBusiness().getJidNumber(),
                                        mThreadMessage, ReengMessageConstant.READ_STATE_READ, TimeHelper
                                                .getCurrentTime());
                                mMessageBusiness.notifyNewMessage(message, mThreadMessage);
                                drawActionBlock();
                            } else {
                                String msg = mChatActivity.getResources().getString(R.string.e601_error_but_undefined);
                                mChatActivity.showError(msg, null);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception", e);
                            String msg = mChatActivity.getResources().getString(R.string.e601_error_but_undefined);
                            mChatActivity.showError(msg, null);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        mChatActivity.hideLoadingDialog();
                        String msg;
                        if (errorCode == -2) {
                            msg = mRes.getString(R.string.error_internet_disconnect);
                        } else {
                            msg = mRes.getString(R.string.e601_error_but_undefined);
                        }
                        mChatActivity.showError(msg, null);
                    }
                });
    }

    private void initHandleCountDown() {
        mMessageBusiness.initListMessageProcessInfo();
        mHandlerCountDown = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                checkMessageInProcess(mMessageBusiness.getListMessageProcessInfo());
                mHandlerCountDown.postDelayed(this, INTERVAL_COUNTDOWN_MSG_INFO);
            }
        };
        mHandlerCountDown.postDelayed(r, INTERVAL_COUNTDOWN_MSG_INFO);
    }

    private void checkMessageInProcess(CopyOnWriteArrayList<ReengMessage> list) {
        if (list == null || list.isEmpty()) return;
        long currentTime = System.currentTimeMillis();
        boolean needToNotify = false;
        for (ReengMessage message : list) {
            if (currentTime - message.getTimeSeen() >= TIME_CHECK_MSG_INFO) {
                message.setIsForceShow(false);
                list.remove(message);
                if (message.getThreadId() == mThreadId) {
                    needToNotify = true;
                }
            }
        }
        if (needToNotify) {
            Log.i(TAG, "ThreadDetailAdapter check message in progress");
            notifyDataAdapter();
        }
    }

    public boolean showDialogConfirmLeave() {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            return false;
        }
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT || mOfficerAccountRoomChat == null
                || mOfficerAccountRoomChat.getRoomState() != OfficerAccountConstant.ROOM_STATE_SUCCESS) {
            return false;
        }
        new DialogConfirm(mChatActivity, true).setLabel(mRes.getString(R.string.exit)).setMessage(mRes.getString(R
                .string.msg_exit_room_star)).
                setNegativeLabel(mRes.getString(R.string.no)).setPositiveLabel(mRes.getString(R.string.yes))
                .setNegativeListener(new NegativeListener() {
                    @Override
                    public void onNegative(Object result) {
                        if (mOfficerAccountRoomChat != null) {
                            mChatActivity.showLoadingDialog(mRes.getString(R.string.loading), "");
                            RoomChatRequestHelper.getInstance(mApplication).leaveRoomChat(mOfficerAccountRoomChat
                                    .getServerId
                                            (), ThreadDetailFragment.this);
                        } else {
                            mChatActivity.goPreviousOrHomeWhenBackPress();
                        }
                    }
                }).setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                if (mOfficerAccountRoomChat != null) {
                    mOfficerAccountRoomChat.setRoomState(OfficerAccountConstant.ROOM_STATE_FOLLOW);
                    mOfficerBusiness.updateOfficerAccount(mOfficerAccountRoomChat);
                    Log.i(TAG, "updateOfficerAccount: " + mOfficerAccountRoomChat.toString());
                }
                mChatActivity.goPreviousOrHomeWhenBackPress();
            }
        }).show();
        return true;
    }

    private void drawStatus() {
        Log.d(TAG, "drawStatus");
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (countDownTimerReceiveTyping != null) {
                    countDownTimerReceiveTyping.cancel();
                    countDownTimerReceiveTyping = null;
                }
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    if (TextUtils.isEmpty(mStatus)) {
                        mTvStatus.setVisibility(View.VISIBLE);
                    } else {
                        mTvStatus.setVisibility(View.VISIBLE);
                        mTvStatus.setText(mStatus);
                    }
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setText(mContactBusiness.getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setText(mContactBusiness.getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                        mThreadMessage.getFollowStar() > 0) {// rom chat co so follow >0
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setText(String.format(mRes.getString(R.string.status_star_follow), mThreadMessage
                            .getFollowStar()));
                } else {
                    mTvStatus.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void showConfirmGiftSong(final String receiver, MediaModel songModel) {
        String msgConfirm = String.format(mRes.getString(R.string.warning_accept_send_crbt_gift),
                songModel.getName(),
                mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.PREF_CRBT_PRICE));
        new DialogConfirm(mChatActivity, true).setLabel(null).setMessage(msgConfirm).setNegativeLabel(mRes.getString
                (R.string.cancel))
                .setPositiveLabel(mRes.getString(R.string.ok)).setEntry(songModel).setPositiveListener(
                new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        MediaModel song = (MediaModel) result;
                        CrbtRequestHelper.getInstance(mApplication).sendCrbtMusic(mThreadMessage,
                                friendPhoneNumber,
                                song, new CrbtRequestHelper.onResponseGiftCrbt() {
                                    @Override
                                    public void onResponse(String session) {
                                        mChatActivity.showToast(R.string.request_success);
                                    }

                                    @Override
                                    public void onError(String msgError) {
                                        mChatActivity.showToast(msgError, Toast.LENGTH_LONG);
                                    }
                                });
                    }
                })
                .show();
    }

    private void drawActionBlock() {
        if (mBlockBusines.isBlockNumber(friendPhoneNumber)) {
            mTvwAlertStrangerBlock.setText(mRes.getString(R.string.unblock));
        } else {
            mTvwAlertStrangerBlock.setText(mRes.getString(R.string.block));
        }
    }

    private void drawAlertStranger() {
        if (mThreadMessage == null || TextUtils.isEmpty(friendPhoneNumber) || !mThreadMessage.isStranger() ||
                mThreadType != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT || mThreadMessage.isJoined()) {
            mFooterAlertStranger.setVisibility(View.GONE);
        } else {
            mFooterAlertStranger.setVisibility(View.VISIBLE);
            if (mBlockBusines.isBlockNumber(friendPhoneNumber)) {
                mTvwAlertStrangerBlock.setText(R.string.unblock);
            } else {
                mTvwAlertStrangerBlock.setText(R.string.block);
            }
        }
    }

    private void handleChangePrivate(ThreadMessage thread) {
        if (thread != null && thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT &&
                thread.isPrivateThread()) { // block screenshot
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
//            mTvwFirstIntro.setText(mRes.getString(R.string.intro_group_private_admin));
//            mTvwSecondIntro.setText(mRes.getString(R.string.intro_group_private_lock_take_picture));
//            mTvwThirdIntro.setText(mRes.getString(R.string.intro_group_private_lock_reply));

//            mImgBgFristIntro.setImageResource(R.drawable.ic_group_private_add_member);
//            mImgBgSecondIntro.setImageResource(R.drawable.ic_group_private_block_screenshot);
//            mImgBgThirdIntro.setImageResource(R.drawable.ic_group_private_block_reply);
//            mImgFirstIntro.setVisibility(View.GONE);
//            mImgSecondIntro.setVisibility(View.GONE);
//            mImgThirdIntro.setVisibility(View.GONE);

            mLayoutFirstIntro.setEnabled(false);
            mLayoutSecondIntro.setEnabled(false);
            mLayoutThirdIntro.setEnabled(false);
        } else {
            mChatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
            mTvwFirstIntro.setText(TextHelper.fromHtml(mRes.getString(R.string.intro_group_click_headphone)));
            mTvwSecondIntro.setText(TextHelper.fromHtml(mRes.getString(R.string.intro_group_reply)));
            mTvwThirdIntro.setText(TextHelper.fromHtml(mRes.getString(R.string.intro_group_create_vote)));
//
//            mImgFirstIntro.setVisibility(View.VISIBLE);
//            mImgSecondIntro.setVisibility(View.VISIBLE);
//            mImgThirdIntro.setVisibility(View.VISIBLE);
//            mImgBgFristIntro.setImageResource(R.color.bg_stranger_music);
//            mImgBgSecondIntro.setImageResource(R.color.bg_button_blue);
//            mImgBgThirdIntro.setImageResource(R.color.bg_mocha);

            mLayoutFirstIntro.setEnabled(true);
            mLayoutSecondIntro.setEnabled(true);
            mLayoutThirdIntro.setEnabled(true);
        }
    }

    public void sendMessageGiftLixi(GiftLixiModel giftLixiModel) {
        //TODO sendMsg
        GiftLixiMessage msg;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            msg = new GiftLixiMessage(mThreadMessage, userNumber, mThreadMessage.getServerId(), giftLixiModel);
        } else {
            msg = new GiftLixiMessage(mThreadMessage, userNumber, friendPhoneNumber, giftLixiModel);
        }
        msg.setChatMode(ReengMessageConstant.MODE_IP_IP);
        if (mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, msg)) {
            msg.setCState(cState);
            mMessageBusiness.sendXMPPMessage(msg, mThreadMessage);
            notifyChangeAndSetSelection();
        }
    }

    private void checkMusicAndSeekVideo() {
        mMusicBusiness.showConfirmCloseMusicAndPlayVideo(mChatActivity, new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                mMusicBusiness.closeMusic();
                mApplication.getPlayMusicController().pauseMusic();
                playVideo(mThreadMessage.getMediaModel(), positionSeekVideo);
            }
        }, null);
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void showMediaPreview() {
        if (mKeyboardController != null) {
            mKeyboardController.showFooterPreviewImage();
        }
    }

    private void checkAutoPlayVideoOnFirstTime() {
        if (mHandler == null || mLvwContent == null || mThreadMessage == null) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new CheckAutoPlayAsyncTask(mThreadMessage.getAllMessages(),
                        mLvwContent.getFirstVisiblePosition() - 1,
                        mLvwContent.getLastVisiblePosition() - 1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }, 600);
    }

    //typing manager
    @Override
    public void onStateChange(int state) {
        Log.i(TAG, "state: " + state);
        if (state == TypingManager.STATE_INSERT_MESAGE) {
            scrollListViewToBottom();
        } else {
            if (state != lastState) {
                if (state == TypingManager.STATE_VISIBLE) {
                    etMessageContent.dismissDropDown();
                    if (reengMesssageReply != null)
                        etMessageContent.setDropDownAnchor(R.id.person_chat_detail_input_text);
                    else
                        etMessageContent.setDropDownAnchor(R.id.view_anchor_popup);
                    etMessageContent.showDropDown();
                } else if (state == TypingManager.STATE_GONE) {
                    etMessageContent.dismissDropDown();
                    etMessageContent.setDropDownAnchor(R.id.person_chat_detail_input_text);
                    etMessageContent.showDropDown();
                }
            }
        }
        lastState = state;
    }

    @Override
    public void onSelectedVideo(MediaModel mediaModel) {
        mMusicBusiness.showConfirmCloseMusicAndPlayVideo(mChatActivity, new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                mMusicBusiness.closeMusic();
                mApplication.getPlayMusicController().pauseMusic();
                sendVideoWatchTogether((MediaModel) result);
            }
        }, mediaModel);
    }

    @Override
    public void onClickUploadSong() {
        goToSelectSongOrShowError(true);
        etMessageContent.setText("");
    }

    private ArrayList<PhoneNumber> getGroupFriendList() {
        if (mThreadMessage == null) {
            return new ArrayList<>();
        }
        if (!mThreadMessage.isJoined()) {
            return new ArrayList<>();
        }
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) return new ArrayList<>();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ArrayList<PhoneNumber> listAdminAndMembers = new ArrayList<>();
        ArrayList<String> adminNumbers = mThreadMessage.getAdminNumbers();
        ArrayList<String> phoneNumbers = mThreadMessage.getPhoneNumbers();
        int threadType;
        threadType = mThreadMessage.getThreadType();
        if (adminNumbers == null) adminNumbers = new ArrayList<>();
        if (phoneNumbers == null) phoneNumbers = new ArrayList<>();
        HashSet<String> hashSetAdmins = new HashSet<>(adminNumbers);
        HashSet<String> hashSetMembers = new HashSet<>(phoneNumbers);
        // bo member trung admin di
        hashSetMembers.removeAll(hashSetAdmins);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
            hashSetMembers.remove(myNumber);

        if (!hashSetAdmins.isEmpty()) {
            ArrayList<PhoneNumber> listPhoneAdmins = mContactBusiness.getListPhoneNumbersFromMemberGroup(new
                    ArrayList<>(hashSetAdmins));
            listAdminAndMembers.addAll(listPhoneAdmins);
        }
        ArrayList<PhoneNumber> listPhoneMembers = mContactBusiness.getListPhoneNumbersFromMemberGroup(new
                ArrayList<>(hashSetMembers));
        listAdminAndMembers.addAll(listPhoneMembers);

        if (!listAdminAndMembers.isEmpty()) {
            Comparator<Object> comparator = ComparatorHelper.getComparatorNumberByName();
            Collections.sort(listAdminAndMembers, comparator);
        }
        return listAdminAndMembers;
    }

    private void showViewPin(final boolean isShow) {
        isShowingPinMessage = isShow;
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isShow) {
                    viewPinMessage.setVisibility(View.VISIBLE);
                } else {
                    viewPinMessage.setVisibility(View.GONE);
                }
            }
        });
    }

    private void handlePinMessage(ReengMessage msg, boolean forceHide) {
        if (forceHide) {
            showViewPin(false);
        } else if (mControlMedia.getVisibility() == View.VISIBLE || mViewVideo.getVisibility() == View.VISIBLE) {
            showViewPin(false);
        } else if (msg != null && msg.getSize() == PinMessage.ActionPin.ACTION_UNPIN.VALUE) {
            showViewPin(false);
        } else {
            final long type;
            final String content;
            String title;
            String image;
            final String target;

            if (msg != null) {
                type = msg.getSongId();
                content = msg.getContent();
                title = msg.getFileName();
                image = msg.getImageUrl();
                target = msg.getFilePath();
            } else {
                if (mThreadMessage == null) {
                    showViewPin(false);
                    return;
                }
                PinMessage pin = mThreadMessage.getPinMessage();
                if (pin != null) {
                    type = pin.getType();
                    content = pin.getContent();
                    title = pin.getTitle();
                    image = pin.getImage();
                    target = pin.getTarget();
                } else {
                    isShowingPinMessage = false;
                    return;
                }
            }
            if (TextUtils.isEmpty(content)) {
                viewPinMessage.setVisibility(View.GONE);
                isShowingPinMessage = false;
                return;
            }
            isShowingPinMessage = true;
            resetPinView();
            mChatActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    viewPinMessage.setVisibility(View.VISIBLE);
                    mTvwContentPinMsg.setEmoticon(mApplication, content, content.hashCode(), content);
                }
            });

            mTvwContentPinMsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setClickPin(target, type);
                }
            });

            viewPinMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setClickPin(target, type);
                }
            });

            if (type == PinMessage.TypePin.TYPE_DEEPLINK.VALUE) {
                mTvwTitlePinMsg.setText(title);
                mImgPinMsg.setVisibility(View.VISIBLE);
                mImgThumbPin.setVisibility(View.GONE);
                ImageLoaderManager.getInstance(mApplication).displayGifStickerNetwork(mImgPinMsg, image);
            } else if (type == PinMessage.TypePin.TYPE_MESSAGE.VALUE) {
                mImgPinMsg.setVisibility(View.GONE);
                mImgThumbPin.setVisibility(View.VISIBLE);
                mImgThumbPin.setImageResource(R.drawable.ic_pin_message);
                if (mReengAccountBusiness.getJidNumber().equals(title)) {
                    mTvwTitlePinMsg.setText(mRes.getString(R.string.you));
                } else {
                    String senderName;
                    PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(title);
                    if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                        senderName = phoneNumber.getName();
                    } else {                      //hien thi so dien thoai neu la group chat
                        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(title);
                        if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                            senderName = nonContact.getNickName();
                        } else {
                            senderName = title;
                        }
                    }
                    mTvwTitlePinMsg.setText(senderName);
                }
            } else if (type == PinMessage.TypePin.TYPE_VOTE.VALUE) {
                mImgPinMsg.setVisibility(View.GONE);
                mImgThumbPin.setVisibility(View.GONE);
                mImgThumbPin.setImageResource(R.drawable.ic_notify_msg_vote);
                String senderName;
                if (title.equals(mApplication.getReengAccountBusiness().getJidNumber()))
                    senderName = mRes.getString(R.string.you);
                else {
                    PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(title);
                    if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                        senderName = phoneNumber.getName();
                    } else {                      //hien thi so dien thoai neu la group chat
                        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(title);
                        if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                            senderName = nonContact.getNickName();
                        } else {
                            senderName = title;
                        }
                    }
                }
                mTvwTitlePinMsg.setText(senderName);
            } else {
                Log.e(TAG, "wtf type pinMessage: " + type);
            }

        }
    }

    private void setClickPin(String target, long type) {
        if (type == PinMessage.TypePin.TYPE_DEEPLINK.VALUE) {
            onSmartTextClick(target, Constants.SMART_TEXT.TYPE_MOCHA);
        } else if (type == PinMessage.TypePin.TYPE_MESSAGE.VALUE) {
            //TODO goto message with id
            Log.i(TAG, "display full content");
            if (mTvwContentPinMsg.getMaxLines() == 2) {
                mTvwContentPinMsg.setMaxLines(10);
                mTvwContentPinMsg.setVerticalScrollBarEnabled(true);
                mTvwContentPinMsg.setMovementMethod(new ScrollingMovementMethod());
            } else {
                resetPinView();
            }
        } else if (type == PinMessage.TypePin.TYPE_VOTE.VALUE) {
            mListener.navigateToPollActivity(mThreadMessage, null, target, Constants.MESSAGE.TYPE_DETAIL_POLL, false);
        }
    }

    private void resetPinView() {
        mTvwContentPinMsg.setMaxLines(2);
        mTvwContentPinMsg.setVerticalScrollBarEnabled(false);
        mTvwContentPinMsg.setMovementMethod(null);
    }

    private void onProcessConfigStickyBanner() {
        if (isShowingPinMessage) return;

        if (mControlMedia.getVisibility() == View.VISIBLE || mViewVideo.getVisibility() == View.VISIBLE) {
            showViewPin(false);
            return;
        }
        currentStickyBanner = mApplication.getConfigBusiness().getStickyBannerFromThreadType(mThreadMessage);
        if (currentStickyBanner == null) return;
        if (System.currentTimeMillis() > currentStickyBanner.getExpired()) {
            mApplication.getConfigBusiness().removeConfigStickyBanner(currentStickyBanner);
        } else {
            showStickyBanner(currentStickyBanner);
        }
    }

    private void showStickyBanner(final PinMessage pin) {
        final String content = pin.getContent();
        String title = pin.getTitle();
        String image = pin.getImage();
        final String target = pin.getTarget();

        if (TextUtils.isEmpty(content)) {
            viewPinMessage.setVisibility(View.GONE);
            return;
        }
        mChatActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewPinMessage.setVisibility(View.VISIBLE);
                mTvwContentPinMsg.setEmoticon(mApplication, content, content.hashCode(), content);
            }
        });

        viewPinMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showViewPin(false);
                mApplication.getConfigBusiness().removeConfigStickyBanner(pin);
                onSmartTextClick(target, Constants.SMART_TEXT.TYPE_MOCHA);
            }
        });
        mTvwTitlePinMsg.setText(title);
        mImgPinMsg.setVisibility(View.VISIBLE);
        mImgThumbPin.setVisibility(View.GONE);
        ImageLoaderManager.getInstance(mApplication).displayGifStickerNetwork(mImgPinMsg, image);
        resetPinView();
    }

    private void checkPrefixNumber() {
        if (mThreadMessage != null) {
            if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                processPrefixThreadSolo();
            } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT
                    || mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
                processPrefixThreadMember();
            }

        }
    }

    private void processPrefixThreadMember() {
        if (mApplication.getConfigBusiness().isDisableChangePrefix()) return;
        if (PrefixChangeNumberHelper.getInstant(mApplication).hasShowInDay(mThreadId)) return;
        HashMap<String, String> listChangeNonContact = new HashMap<>();
        HashMap<String, PhoneNumber> listChangePhoneNumber = new HashMap<>();
        ArrayList<String> listMember = mThreadMessage.getPhoneNumbers();
        ArrayList<String> listAdmin = mThreadMessage.getAdminNumbers();
        if (!listMember.isEmpty()) {
            for (String numb : listMember) {
                checkPhoneNumber(numb, listChangeNonContact, listChangePhoneNumber);
            }
        }

        if (listAdmin != null && !listAdmin.isEmpty()) {
            for (String numb : listAdmin) {
                checkPhoneNumber(numb, listChangeNonContact, listChangePhoneNumber);
            }
        }

        if (listChangePhoneNumber.isEmpty() && !listChangeNonContact.isEmpty()) {
            onEditContact(true, new HashMap<String, String>(), listChangeNonContact);
        } else
            PrefixChangeNumberHelper.getInstant(mApplication).showDialogGroupChangePrefix(
                    listChangePhoneNumber, listChangeNonContact, mChatActivity, this, mThreadId);

    }

    private void checkPhoneNumber(String numb, HashMap<String, String> listChangeNonContact,
                                  HashMap<String, PhoneNumber> listChangePhoneNumber) {
        if (!numb.equals(userNumber)) {
            if (numb.length() == 11) {
                String newNumb = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(numb);
                if (newNumb != null) {
                    PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(numb);
                    if (phoneNumber != null) {
                        phoneNumber.setNewNumber(newNumb);
                        listChangePhoneNumber.put(numb, phoneNumber);
                    } else {
                        listChangeNonContact.put(numb, newNumb);
                    }
                }
            } else if (numb.length() == 10) {
                String oldNumb = PrefixChangeNumberHelper.getInstant(mApplication).getOldNumber(numb);
                if (oldNumb != null) {
                    PhoneNumber p = mContactBusiness.getPhoneNumberFromNumber(oldNumb);
                    if (p != null) {
                        p.setNewNumber(numb);
                        listChangePhoneNumber.put(oldNumb, p);
                    } else {
//                        listChangeNonContact.put(numb, numb);
                    }
                }
            }
        }
    }

    private void processPrefixThreadSolo() {
        if (mApplication.getConfigBusiness().isDisableChangePrefix()) return;
        if (PrefixChangeNumberHelper.getInstant(mApplication).hasShowInDay(mThreadId)) return;
        new ProcessPrefixAsynctask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /*public void btnMusicClicked() {
        int typeMusic;
        final String nameFriend = getFriendName();
        final String jidFriend;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            typeMusic = Constants.TYPE_MUSIC.TYPE_PERSON_CHAT;
            jidFriend = friendPhoneNumber;
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            typeMusic = Constants.TYPE_MUSIC.TYPE_GROUP_CHAT;
            jidFriend = mThreadMessage.getServerId();
        } else {
            Log.e(TAG, "thread type error: " + mThreadType);
            return;
        }

        mMusicBusiness.showConfirmOrNavigateToSelectSong(mChatActivity, typeMusic,
                null, mRes.getString(R.string.stranger), new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
//                        notifyChangeAdapter();


                        NavigateActivityHelper.navigateToSelectSongAndListenr(mApplication, mChatActivity,
                                mThreadId, nameFriend, jidFriend);
                    }

                    @Override
                    public void onGotoChange() {
                        //khong vao day
                    }
                });
    }*/

    private void checkShowPopupIntroSendMsgNoInternet() {
        if (!mApplication.getConfigBusiness().isEnableSmsNoInternet()) return;
        if (!mApplication.getReengAccountBusiness().isViettel()) return;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mThreadMessage != null && !mThreadMessage.isStranger()) {
            if (!isViettel) return;
            boolean hasShow = mPref.getBoolean(PREF_FIRST_SHOW_POPUP_INTRO_SMS_NO_INTERNET, false);
            if (!hasShow) {
                showPopupIntroSendMsgNoInternet();
                mPref.edit().putBoolean(PREF_FIRST_SHOW_POPUP_INTRO_SMS_NO_INTERNET, true).apply();
            } else {
                if (NetworkHelper.isConnectInternet(mApplication)) {
                    return;
                }
                long lastShow = mPref.getLong(PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, 0);
                if (TimeHelper.checkTimeInDay(lastShow)) {
                   // showSuggestSendMsgNoInternet();
                    return;
                }
                showPopupIntroSendMsgNoInternet();
            }
        }
    }

    private void showPopupIntroSendMsgNoInternet() {

        PopupIntroModel popupIntroModel = new PopupIntroModel();
        popupIntroModel.setTitle(mRes.getString(R.string.suggest_send_msg_no_internet));
        popupIntroModel.setLabelBtn(mRes.getString(R.string.try_now));
        PopupIntroDetail popupIntroDetail = new PopupIntroDetail();
        popupIntroDetail.setDesc(mRes.getString(R.string.content_send_msg_no_internet_1));
        popupIntroDetail.setResId(R.drawable.ic_no_internet_1);

        PopupIntroDetail popupIntroDetail2 = new PopupIntroDetail();
        popupIntroDetail2.setDesc(mRes.getString(R.string.content_send_msg_no_internet_2));
        popupIntroDetail2.setResId(R.drawable.ic_no_internet_2);

        ArrayList<PopupIntroDetail> listIntro = new ArrayList<>();
        listIntro.add(popupIntroDetail);
        listIntro.add(popupIntroDetail2);
        popupIntroModel.setIntros(listIntro);
        PopupIntro popupIntro = new PopupIntro(mChatActivity, popupIntroModel);
        popupIntro.setClickButtonListener(new PopupIntro.ClickButtonListener() {
            @Override
            public void onClickBtnDeeplink() {
                InputMethodUtils.showSoftKeyboard(mChatActivity, etMessageContent);
            }

            @Override
            public void onClickBtnDeeplinkNext() {

            }
        });
        if (!mChatActivity.isFinishing()) {
            popupIntro.show();
            mPref.edit().putLong(PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET, System.currentTimeMillis()).apply();
        } else {
            Log.e(TAG, "mChatActivity.isFinishing()");
        }
    }

    private void showSuggestSendMsgNoInternet() {
        if (rlSuggestSendMsgNoInternet != null) {
            rlSuggestSendMsgNoInternet.setTranslationX(ScreenManager.getWidth(mChatActivity));
            rlSuggestSendMsgNoInternet.setVisibility(View.VISIBLE);
            rlSuggestSendMsgNoInternet
                    .animate()
                    .translationX(0)
                    .setDuration(300L)
                    .setListener(null)
                    .setInterpolator(new OvershootInterpolator(1.25f))
                    .start();
        }
    }

    private void hideSuggestSendMsgNoInternet() {
        if (rlSuggestSendMsgNoInternet != null) {
            rlSuggestSendMsgNoInternet
                    .animate()
                    .translationX(mApplication.getWidthPixels())
                    .setDuration(300L)
                    .setInterpolator(new OvershootInterpolator(1.25f))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            if (rlSuggestSendMsgNoInternet != null)
                                rlSuggestSendMsgNoInternet.setVisibility(View.GONE);
                        }
                    }).start();
        }
    }

    public interface OnFragmentInteractionListener {
        void navigateToThreadDetail(ThreadMessage threadMessage);

        void navigateToContactDetailActivity(String phoneId);

        void navigateToStrangerDetail(StrangerPhoneNumber strangerPhoneNumber,
                                      String friendJid, String friendName);

        void navigateToNonContactDetailActiviy(String phoneNumber);

        void navigateToOfficialDetail(String officialId);

        void addNewContact(String number, String name);

        void navigateToSendEmail(String email);

        void navigateToStatusMessageFragment(int threadId, int messageId, String abStatus);

        void navigateToChooseFriendsActivity(ArrayList<String> listNumberString, ThreadMessage threadMessage, int typeScreen);

        void navigateToCall(String number);

        void navigateToViewLocation(ReengMessage message);

        void navigateToPollActivity(ThreadMessage threadMessage, ReengMessage reengMessage, String pollId, int type, boolean pollUpdate);
    }

    private class SetContentDraftAsyncTask extends AsyncTask<String, Void, Spanned> {

        @Override
        protected Spanned doInBackground(String... params) {
            String draftMessage = params[0];
            Html.ImageGetter imageGetter = EmoticonManager.getInstance(mChatActivity).getImageGetter();
            draftMessage = TextHelper.getInstant().escapeXml(draftMessage);
            draftMessage = EmoticonUtils.emoTextToTag(draftMessage);
            return TextHelper.fromHtml(draftMessage, imageGetter, null);
        }

        @Override
        protected void onPostExecute(Spanned content) {
            isDraft = true;
            etMessageContent.setText(content);
            etMessageContent.setSelection(etMessageContent.getText().length());
            etMessageContent.requestFocus();
            Log.d(TAG, "onPostExecute ------------- draft: " + content);
            super.onPostExecute(content);
        }
    }

    private class AutoSendTask extends TimerTask {
        int count = 1;
        int maxCount;

        public AutoSendTask(int maxCount) {
            this.maxCount = maxCount;
        }

        @Override
        public void run() {
            if (count > maxCount || mHandler == null) return;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // mMessageBusiness.testSpam(mThreadMessage.getPhoneNumbers().get(0));
                    // mSendMessageAgent.sendTextMessage("spam " + count);
                    sendMessageText("" + count, null, null);
                    count++;
                }
            });
        }
    }

    /**
     * excute when click loacmore
     */
    private class GetMoreMessageTask extends AsyncTask<Void, Void, CopyOnWriteArrayList<ReengMessage>> {
        CopyOnWriteArrayList<ReengMessage> listBuf;//copy tam cac tin nhan hien tai sang list nay
        private int sizeOld = 0;
        private int distance = 0;
        private int idx;
        private int increSize = Constants.MESSAGE.MESSAGE_INCRE_SIZE;

        public void setIncreSize(int increSize) {
            Log.d(TAG, "GetMoreMessageTask increSize : " + increSize);
            this.increSize = increSize;
        }

        @Override
        protected void onPreExecute() {
            sizeOld = mMessageArrayList.size();
            listBuf = mMessageArrayList;//copy
            super.onPreExecute();
        }

        @Override
        protected CopyOnWriteArrayList<ReengMessage> doInBackground(Void... params) {
            Log.d(TAG, "[check_unread] GetMoreMessageTask increSize : " + increSize);
            int firstMsgId = -1;
            if (mMessageArrayList != null && !mMessageArrayList.isEmpty()) {
                firstMsgId = mMessageArrayList.get(0).getId();
            }
            return mMessageBusiness.loadLimitMessage(mThreadId, increSize, firstMsgId);
        }

        @Override
        protected void onPostExecute(CopyOnWriteArrayList<ReengMessage> result) {
            boolean isLoadDetail;
            final int numberUnread = mThreadMessage.getNumOfUnreadMessage();
            if (mThreadMessage.isLoadDetail()) {// load more thi chi check message load them
                mMessageBusiness.markAllMessageIsReadAndCheckSendSeen(mThreadMessage, result);
                isLoadDetail = true;
            } else {
                mMessageBusiness.markAllMessageIsReadAndCheckSendSeen(mThreadMessage, mThreadMessage.getAllMessages());
                isLoadDetail = false;
            }
            mThreadMessage.setLoadDetail(true);
            if (result == null) {
                result = new CopyOnWriteArrayList<>();
            }
            // lay thong so de restore current item visible
            idx = mLvwContent.getFirstVisiblePosition() + 1;
            View first = mLvwContent.getChildAt(idx);
            if (first != null) {
                distance = first.getTop();
            }
            mThreadMessage.addMoreMessage(result);
            // Call onRefreshComplete when the list has been refreshed.
            mMessageArrayList = mThreadMessage.getAllMessages();
            mThreadDetailAdapter.setMessages(mMessageArrayList);
            Log.i(TAG, "ThreadDetailAdapter get more message task");
            notifyDataAdapter();
            int totalMessage = mMessageArrayList.size();
            // so message load duoc
            int delta = totalMessage - sizeOld;
            if (delta < increSize) {
                mThreadMessage.setLoadMoreFinish(true);
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    if (mIsReengUser) {
                        mRltProfile.setVisibility(View.GONE);
                        mLayoutProfile.setVisibility(View.VISIBLE);
                    } else {
                        mLayoutProfile.setVisibility(View.GONE);
                        mRltProfile.setVisibility(View.GONE);
                    }
                    mLayoutIntroGroup.setVisibility(View.GONE);
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    mLayoutProfile.setVisibility(View.GONE);
                    mLayoutIntroGroup.setVisibility(View.GONE);
                }
            } else {
                mThreadMessage.setLoadMoreFinish(false);
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    mLayoutProfile.setVisibility(View.GONE);
                    mLayoutIntroGroup.setVisibility(View.GONE);
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    mLayoutProfile.setVisibility(View.GONE);
                    mLayoutIntroGroup.setVisibility(View.GONE);
                }
            }
            showLoadMoreAndAddContact(totalMessage);
            Log.d(TAG, "[check_unread] setSelectionFromTop - delta: " + delta + " -idx: " + idx
                    + " -distance: " + distance + " numberUnread: " + numberUnread);
            if (isLoadDetail) {
                mLvwContent.setSelectionFromTop(delta + idx, distance);
            } else {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollListViewTo(numberUnread);
                    }
                }, 100);
                checkAutoPlayVideoOnFirstTime();
            }
            super.onPostExecute(result);
        }
    }

    private class CheckAutoPlayAsyncTask extends AsyncTask<Void, Void, ReengMessage> {
        private CopyOnWriteArrayList<ReengMessage> reengMessages;
        private int first, last;

        private CheckAutoPlayAsyncTask(CopyOnWriteArrayList<ReengMessage> messages, int first, int last) {
            this.reengMessages = messages;
            this.first = first;
            this.last = last;
        }

        @Override
        protected ReengMessage doInBackground(Void... params) {
            return mMessageBusiness.isAutoPlayMessageVideoFirstTime(reengMessages, first, last);
        }

        @Override
        protected void onPostExecute(ReengMessage message) {
            super.onPostExecute(message);
            if (message != null) {
                onWatchVideoClick(message, true);
            }
        }
    }

    class ProcessPrefixAsynctask extends AsyncTask<Void, Void, PrefixChangeNumberHelper.ChangeNumberObject> {

        @Override
        protected PrefixChangeNumberHelper.ChangeNumberObject doInBackground(Void... voids) {
            PrefixChangeNumberHelper.ChangeNumberObject changeNumberObject = null;
            Log.i(TAG, "friendPhoneNumber: " + friendPhoneNumber);
            if (mThreadMessage.isStranger()) {
                if (friendPhoneNumber.length() == 11) {
                    String newNumberStranger = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(friendPhoneNumber);
                    if (newNumberStranger != null) {
                        ArrayList<String> listNumber = new ArrayList<>();
                        listNumber.add(newNumberStranger);
                        mThreadMessage.setPhoneNumbers(listNumber);
                        mMessageBusiness.updateThreadMessage(mThreadMessage);
                        StrangerPhoneNumber strangerPhoneNumber = mApplication.
                                getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendPhoneNumber);
                        if (strangerPhoneNumber != null) {
                            strangerPhoneNumber.setPhoneNumber(newNumberStranger);
                            mApplication.getStrangerBusiness().updateStrangerPhoneNumber(strangerPhoneNumber);
                        }
                        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendPhoneNumber);
                        if (nonContact != null) {
                            nonContact.setJidNumber(newNumberStranger);
                            mApplication.getContactBusiness().updateNonContact(nonContact);
                        }
                        changeNumberObject = new PrefixChangeNumberHelper.ChangeNumberObject(PrefixChangeNumberHelper.ChangeNumberObject.STATE_AUTO_UPDATE_VIEW);
                        Log.i(TAG, "edit stranger number ");
                    }
                }
            } else {
                // với 11 số, kiểm tra xem số đó có trong danh bạ ko, nếu có thì thông báo đổi số
                // nếu ko có trong danh bạ và nó là noncontact thì thông báo cập nhật thread chat
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendPhoneNumber);
                if (friendPhoneNumber.length() == 11) {
                    final String newNumber = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(friendPhoneNumber);
                    if (newNumber != null) {
                        if (phoneNumber != null) {
                            //nằm trong dải đổi số
                            phoneNumber.setNewNumber(newNumber);
                            changeNumberObject = new PrefixChangeNumberHelper.ChangeNumberObject(
                                    PrefixChangeNumberHelper.ChangeNumberObject.STATE_FRIEND_NEED_CHANGE,
                                    null, newNumber, phoneNumber);
                        } else {
                            PhoneNumber phoneNew = mApplication.getContactBusiness().getPhoneNumberFromNumber(newNumber);
                            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendPhoneNumber);
                            if (phoneNew != null) {        //số này đã được đổi ngoài danh bạ từ 11 sang 10, và đang có threadchat
                                ArrayList<String> listNumber = new ArrayList<>();
                                listNumber.add(newNumber);
                                mThreadMessage.setPhoneNumbers(listNumber);
                                mMessageBusiness.updateThreadMessage(mThreadMessage);
                                changeNumberObject = new PrefixChangeNumberHelper.ChangeNumberObject(PrefixChangeNumberHelper.ChangeNumberObject.STATE_AUTO_UPDATE_VIEW);
                                Log.i(TAG, "số này đã được đổi ngoài danh bạ từ 11 sang 10, và đang có threadchat");
                            } else if (nonContact != null) {
                                nonContact.setNewJidNumber(newNumber);
                                changeNumberObject = new PrefixChangeNumberHelper.ChangeNumberObject(
                                        PrefixChangeNumberHelper.ChangeNumberObject.STATE_NON_CONTACT);
                                changeNumberObject.setNonContact(nonContact);
                                Log.i(TAG, "edit non contact threadchat");
                            }
                        }
                    }
                } else {    //nếu là 10 số thì chỉ cần check số 11 có trong danh bạ ko
                    String oldNumber = PrefixChangeNumberHelper.getInstant(mApplication).getOldNumber(friendPhoneNumber);
                    if (oldNumber != null) {
                        PhoneNumber phoneNumberOld = mApplication.getContactBusiness().getPhoneNumberFromNumber(oldNumber);
                        if (phoneNumberOld != null) {        // số 11 số đã nằm trong danh bạ, thông báo rằng đây là số 11 số của thằng đó
                            phoneNumberOld.setNewNumber(friendPhoneNumber);
                            changeNumberObject = new PrefixChangeNumberHelper.ChangeNumberObject(
                                    PrefixChangeNumberHelper.ChangeNumberObject.STATE_FRIEND_CHANGED,
                                    oldNumber, friendPhoneNumber, phoneNumberOld);
                            Log.i(TAG, "số 11 số đã nằm trong danh bạ, thông báo rằng đây là số 11 số của thằng đó");

                        }
                    }
                }
            }
            return changeNumberObject;
        }

        @Override
        protected void onPostExecute(final PrefixChangeNumberHelper.ChangeNumberObject changeNumberObject) {
            super.onPostExecute(changeNumberObject);
            if (changeNumberObject != null) {
                switch (changeNumberObject.getState()) {
                    case PrefixChangeNumberHelper.ChangeNumberObject.STATE_NON_CONTACT:
                        PrefixChangeNumberHelper.getInstant(mApplication).showDialogNonContactChangePrefix(
                                changeNumberObject.getNonContact(),
                                mChatActivity, new PositiveListener<Object>() {
                                    @Override
                                    public void onPositive(Object result) {
                                        mChatActivity.showLoadingDialog("", R.string.loading);
                                        ArrayList<String> numb = new ArrayList<>();
                                        friendPhoneNumber = changeNumberObject.getNonContact().getNewJidNumber();
                                        numb.add(changeNumberObject.getNonContact().getNewJidNumber());
                                        mThreadMessage.setPhoneNumbers(numb);
                                        mMessageBusiness.updateThreadMessage(mThreadMessage);
                                        NonContact nonContact = mContactBusiness.getExistNonContact(changeNumberObject.getOldNumber());
                                        if (nonContact != null) {
                                            mContactBusiness.updateNonContact(nonContact);
                                        }
                                        mChatActivity.hideLoadingDialog();
                                        if (mHandler != null) {
                                            mHandler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    drawThreadDetail(true);
                                                }
                                            }, DELAY_DRAW_DETAIL);
                                        }
                                    }
                                }, mThreadId);
                        break;

                    case PrefixChangeNumberHelper.ChangeNumberObject.STATE_FRIEND_NEED_CHANGE:
                        PrefixChangeNumberHelper.getInstant(mApplication).showDialogFriendChangePrefix(
                                changeNumberObject.getPhoneNumber(), changeNumberObject.getNewNumber(),
                                mChatActivity, ThreadDetailFragment.this, mThreadId);
                        break;

                    case PrefixChangeNumberHelper.ChangeNumberObject.STATE_FRIEND_CHANGED:
                        PrefixChangeNumberHelper.getInstant(mApplication).showDialogFriendNewNumber(
                                changeNumberObject.getPhoneNumber(), changeNumberObject.getNewNumber(),
                                mChatActivity, ThreadDetailFragment.this, mThreadId);
                        break;
                    case PrefixChangeNumberHelper.ChangeNumberObject.STATE_AUTO_UPDATE_VIEW:
                        onContactChange();
                        break;
                }
            }
        }
    }

}
package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.MusicRoomTabsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengViewPager;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 1/4/2016.
 */
public class ListRoomFragment extends Fragment {
    private static final String TAG = ListRoomFragment.class.getSimpleName();
    private SettingActivity mParentActivity;
    private ApplicationController mApplication;

    //action bar
    private View appbarLayout;
    private AppCompatImageView mBtnBack, mBtnSearch;
    private AppCompatTextView mTitle;
    private ReengViewPager mViewPager;
    private TabLayout mTabLayout;
    private ProgressLoading mProgressLoading;
    private TextView mTvNoteView;

    private OnFragmentListRoomListener mListener;
    private MusicRoomTabsAdapter mMusicRoomAdapter;
    private Resources mRes;

    private OfficerBusiness mOfficerBusiness;

    ArrayList<String> mListTitle = new ArrayList<>();
    ArrayList<OfficerAccount> mListRoomTab = new ArrayList<>();

    public static ListRoomFragment newInstance() {
        ListRoomFragment fragment = new ListRoomFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SettingActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mRes = mParentActivity.getResources();
        mOfficerBusiness = mApplication.getOfficerBusiness();
        try {
            mListener = (OnFragmentListRoomListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentListRoomListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "On Resume");
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_room, container, false);
        findComponentViews(root, inflater);
        setUpToolBar();
        getListRoomTab();
        return root;
    }

    private void findComponentViews(View view, LayoutInflater inflater) {
//        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        appbarLayout = view.findViewById(R.id.appBar);
        mViewPager = view.findViewById(R.id.fragment_pager);
        mTabLayout = view.findViewById(R.id.tabs);
        mProgressLoading = view.findViewById(R.id.fragment_group_loading_progressbar);
        mTvNoteView = view.findViewById(R.id.fragment_group_note_text);
        mTvNoteView.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.INVISIBLE);
    }

    private void setUpToolBar() {
        mBtnBack = appbarLayout.findViewById(R.id.icBackToolbar);
        mBtnSearch = appbarLayout.findViewById(R.id.icOption);
        mBtnSearch.setVisibility(View.VISIBLE);
        mTitle = appbarLayout.findViewById(R.id.txtTitleToolbar);
        mTitle.setText(mRes.getString(R.string.music_room));
        mBtnSearch.setImageResource(R.drawable.ic_search);
        mBtnSearch.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.navigateToSearchFragmnet();
            }
        });
        mBtnBack.setOnClickListener(v -> mParentActivity.onBackPressed());
    }

    private void setUpTab() {
        OfficerBusiness officerBusiness = mApplication.getOfficerBusiness();
        mListTitle = officerBusiness.getTabName();
        mTabLayout.setVisibility(View.VISIBLE);
        if (mMusicRoomAdapter == null) {
            mMusicRoomAdapter = new MusicRoomTabsAdapter(mParentActivity.getSupportFragmentManager(), mParentActivity, mListTitle, mListRoomTab);
            mViewPager.setAdapter(mMusicRoomAdapter);
            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setCurrentItem(0, true);
            mTabLayout.setupWithViewPager(mViewPager);
            for (int i = 0; i < mTabLayout.getTabCount(); i++) {//TODO custom tab view
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null) {
                    tab.setCustomView(mMusicRoomAdapter.getTabView(i));
                }
            }
        }
    }

    private void getListRoomTab() {
        if (mOfficerBusiness.getmListRoomTab().size() > 0) {
            mListRoomTab = mOfficerBusiness.getmListRoomTab();
            mProgressLoading.setVisibility(View.GONE);
            mTvNoteView.setVisibility(View.GONE);
            setUpTab();
            return;
        }

        OfficerBusiness.RequestListRoomTAbResponseListener listener = new OfficerBusiness.RequestListRoomTAbResponseListener() {
            @Override
            public void onResponse(ArrayList<OfficerAccount> list) {
                mListRoomTab = list;
                mProgressLoading.setVisibility(View.GONE);
                mTvNoteView.setVisibility(View.GONE);
                setUpTab();
            }

            @Override
            public void onError(int errorCode, String msg) {
                mProgressLoading.setVisibility(View.GONE);
                mTvNoteView.setVisibility(View.VISIBLE);
                mTvNoteView.setText(mRes.getString(R.string.e500_internal_server_error));
            }
        };
        mProgressLoading.setVisibility(View.VISIBLE);
        RoomChatRequestHelper.getInstance(mApplication).getListStarRoomWithTab(listener);
    }

    public interface OnFragmentListRoomListener {
        void navigateToSearchFragmnet();
    }
}
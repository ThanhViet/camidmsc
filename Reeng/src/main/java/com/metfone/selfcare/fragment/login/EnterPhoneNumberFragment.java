package com.metfone.selfcare.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.CheckPhoneInfo;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_NUMBER;
import static com.metfone.selfcare.util.EnumUtils.LOG_MODE;
import static com.metfone.selfcare.util.EnumUtils.PRE_LOG_MODE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class EnterPhoneNumberFragment extends Fragment implements ClickListener.IconListener {
    private static final String TAG = EnterPhoneNumberFragment.class.getSimpleName();
    private static final String PHONE_NUMBER_PARAM = "param1";// so nhan dien dc (dau vao)
    private static final String REGION_CODE_PARAM = "param2";
    private static final String NUMBER_JID_PARAM = "number_jid_param";
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.etNumber)
    AppCompatEditText etNumber;
    @BindView(R.id.rlt_phone_number)
    RelativeLayout rlt_phone_number;
    @BindView(R.id.cbTerm)
    AppCompatCheckBox cbTerm;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    Unbinder unbinder;
    /*@BindView(R.id.country_code)
    Spinner spCountryCode;*/
    @BindView(R.id.tvTryNowDes)
    CamIdTextView tvTryNowDes;
    @BindView(R.id.llCountryCode)
    LinearLayout llCountryCode;
    @BindView(R.id.ivFlCountry)
    ImageView ivFlCountry;
    @BindView(R.id.tvCountry)
    TextView tvCountry;
    @BindView(R.id.tv_rules)
    CamIdTextView tvRules;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    @BindView(R.id.tv_we_will)
    CamIdTextView mTvEnterPhone;
    @BindView(R.id.tvWrongPhone)
    CamIdTextView tvWrongPhone;
    @BindView(R.id.llInputNumber)
    LinearLayout llInputNumber;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.viewOldUser)
    LinearLayout viewOldUser;
    @BindView(R.id.txtOldUser)
    CamIdTextView txtOldUser;
    private OnFragmentInteractionListener fragmentInteractionListener;
    private LoginActivity mLoginActivity;
    private ApplicationController mApplication;
    private Resources mRes;

    private String mCurrentRegionCode = "VN";// mac dinh vn
    private String mCurrentNumberJid;
    private ClickListener.IconListener mClickHandler;

    private boolean isSaveInstanceState = false;
    private boolean isRegisterDone = false;
    private String responseString = "";
    private RegionSpinnerAdapter mRegionAdapter;
    private ArrayList<Region> mRegions;
    private String numberInput;
    private int position;

    private String fromSource = "";
    //--------------------End---------------------

    public EnterPhoneNumberFragment() {
        // Required empty public constructor
    }

    public static EnterPhoneNumberFragment newInstance(String numberInput, String region, String fromSource) {
        EnterPhoneNumberFragment fragment = new EnterPhoneNumberFragment();
        Bundle args = new Bundle();
        args.putString(PHONE_NUMBER_PARAM, numberInput);
        args.putString(REGION_CODE_PARAM, region);
        args.putString(Constants.KEY_POSITION, fromSource);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        getData(savedInstanceState);
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        findComponentViews(rootView);
        setSpinnerView();
        setViewListeners();
        initView();
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            ivBack.setVisibility(View.VISIBLE);
            tvTryNowDes.setVisibility(View.GONE);
        } else {
            ivBack.setVisibility(View.VISIBLE);
            tvTryNowDes.setVisibility(View.GONE);
        }
        tvRules.setText(Html.fromHtml(mLoginActivity.getString(R.string.agree_term)));
        return rootView;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mLoginActivity = (LoginActivity) activity;
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mLoginActivity.setTitle(getResources().getString(R.string.register).toUpperCase());
        mRes = mApplication.getResources();
        try {
            fragmentInteractionListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentInteractionListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(etNumber, mLoginActivity);
        mClickHandler = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        etNumber.requestFocus();
        UIUtil.showKeyboard(getContext(),etNumber);
        mClickHandler = this;
        Log.i(TAG, "ResponseString: " + responseString);
       /* if (!responseString.equals("")) {
            actionResponseCodeForResume(responseString);
            responseString = "";
        }*/
        if (!TextUtils.isEmpty(EnumUtils.numberInput)){
            etNumber.setText(EnumUtils.numberInput);
            etNumber.setSelection(EnumUtils.numberInput.length());
            EnumUtils.numberInput = null;
        }
        isSaveInstanceState = false;
        enableOrDisableContinueButton();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, numberInput);
        outState.putString(REGION_CODE_PARAM, mCurrentRegionCode);
        outState.putString(NUMBER_JID_PARAM, mCurrentNumberJid);
        outState.putString(Constants.KEY_POSITION, fromSource);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    public void updateRegionCode(int index) {
        mRegions.get(position).setSelected(false);

        position = index;
        Region region = mRegions.get(index);
        region.setSelected(true);
        mCurrentRegionCode = region.getRegionCode();
        String patch = "file:///android_asset/fl_country/" + region.getRegionCode().toLowerCase() + ".png";
        Glide.with(mLoginActivity)
                .load(Uri.parse(patch))
                .apply(new RequestOptions()
                        .error(R.color.gray)
                        .fitCenter())
                .into(ivFlCountry);

        String text = region.getRegionName();
        text = text.split("\\(")[1];
        text = text.substring(0, text.length() - 1);
        tvCountry.setText(text);
    }

    /**
     * find all View
     */
    private void findComponentViews(View rootView) {
        etNumber.setText("");
        if (numberInput != null && numberInput.length() > 0) {// check xem co lay dc tu sim khong
            etNumber.setText(numberInput);
            etNumber.setSelection(numberInput.length());
        }

        etNumber.requestFocus();
    }

    private void setViewListeners() {
        enableOrDisableContinueButton();
        setPhoneNumberEditTextListener();
        tvRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UrlConfigHelper.gotoWebViewOnMocha(ApplicationController.self(), mLoginActivity, "http://mocha.com.vn/vi/provision.html");
            }
        });
    }

    private void setSpinnerView() {
        mRegions = mApplication.getLoginBusiness().getAllRegions();
        updateRegionCode(35);
    }

    /**
     * listener for change of phoneNumber EditText -> enable or disable button
     */
    private void setPhoneNumberEditTextListener() {
        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (tvWrongPhone.getVisibility() == View.VISIBLE){
                    tvWrongPhone.setText(getString(R.string.text_wrong_phone));
                    tvWrongPhone.setVisibility(View.GONE);
                    llInputNumber.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_border_blue));
                }
                enableOrDisableContinueButton();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String phone = etNumber.getText().toString().trim();
                    if (TextUtils.isEmpty(phone)) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                    } else if (phone.length() < 8|| phone.length() > 15 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    } else {
                        processClickContinue();
                    }
                }
                return false;
            }
        });
    }

    /**
     * enable or disable continue Button
     */
    private void enableOrDisableContinueButton() {
        if (etNumber.getText().toString().trim().length() >= Constants.CONTACT.MIN_LENGTH_NUMBER) {
            setEnableButton(true);
        } else {
            setEnableButton(false);
        }
    }

    private void setEnableButton(boolean enableButton) {
        if (mLoginActivity == null) return;
        if (btnContinue != null) {
            btnContinue.setClickable(enableButton);
            if (enableButton) {
                btnContinue.setTextColor(ContextCompat.getColor(mLoginActivity, R.color.v5_text_7));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(mLoginActivity, R.color.button_continue),
                        ContextCompat.getColor(mLoginActivity, R.color.button_continue));
            } else {
                btnContinue.setTextColor(ContextCompat.getColor(mLoginActivity, R.color.v5_text_3));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(mLoginActivity, R.color.v5_cancel),
                        ContextCompat.getColor(mLoginActivity, R.color.v5_cancel));
            }
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_EXIT:
                //                mLoginActivity.finish();
                break;
            default:
                break;
        }
    }


    // xu ly khi gui yc sinh ma
    private void processClickContinue() {

        if (LOG_MODE != PRE_LOG_MODE){
            LOG_MODE = PRE_LOG_MODE;
        }

            //signup
        if (!NetworkHelper.isConnectInternet(getActivity())) {
            mLoginActivity.showError(getString(R.string.error_internet_disconnect), null);
        }else {
            checkPhone();
        }
    }

    private void progessPhoneOtp() {
        LOG_MODE = EnumUtils.LogModeTypeDef.SIGN_UP;
        checkPhone();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cbTerm, R.id.btnContinue, R.id.ivBack, R.id.tvTryNow, R.id.txtOldUser})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cbTerm:
                break;
            case R.id.btnContinue:
                String phone = etNumber.getText().toString();
                if (phone.startsWith("0")) {
                    phone = etNumber.getText().toString();
                } else {
                    phone = "0" + etNumber.getText().toString();
                }

                SharedPrefs.getInstance().put(PREF_PHONE_NUMBER, phone);

                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
//                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    llInputNumber.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_border_red));
                    tvWrongPhone.startAnimation(UserInfoBusiness.shakeError());
                    tvWrongPhone.setVisibility(View.VISIBLE);
                } else {
                    processClickContinue();
                }
//                CamIDAccountDialog ccc = new CamIDAccountDialog(getActivity());
//                ccc.show();
                break;
            case R.id.ivBack:
                EnumUtils.numberInput = null;
                getActivity().finish();
//                NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getContext(), true);
                break;
            case R.id.txtOldUser:
                progessPhoneOtp();
                break;

        }
    }


    private void checkPhone() {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);

        //Api loi, chi tra ve 00 cho so dien thoai 0882980208
        String phoneNumber = etNumber.getText().toString();
        if (phoneNumber.startsWith("0")) {
            phoneNumber = etNumber.getText().toString();
        } else {
            phoneNumber = "0" + etNumber.getText().toString();
        }

        String num = etNumber.getText().toString();

        CheckPhoneInfo checkPhoneInfo = new CheckPhoneInfo(phoneNumber);
        SharedPrefs.getInstance().put(PREF_PHONE_NUMBER, phoneNumber);
        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest(checkPhoneInfo, "", "", "", "");
        String finalPhoneNumber = phoneNumber;
        apiService.checkPhone(checkPhoneRequest).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, retrofit2.Response<BaseResponse<String>> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
//                EnumUtils.numberInput = etNumber.getText().toString();
                /*
                * fix bug firebase: 'android.text.Editable androidx.appcompat.widget.AppCompatEditText.getText()' on a null object reference
                * */
                EnumUtils.numberInput = num;
                if (response.body() != null) {
                    if (LOG_MODE == EnumUtils.LogModeTypeDef.SIGN_UP) {
                        if (response.body().getCode().equals("00") || response.body().getCode().equals("42") || response.body().getCode().equals("49")) {
//                        ToastUtils.showToast(getContext(), response.body().getMessage());
                            LOG_MODE = EnumUtils.LogModeTypeDef.LOGIN;
                            fragmentInteractionListener.displayLoginPasswordScreen(finalPhoneNumber);
//                        SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
                        } else {
                            LOG_MODE = EnumUtils.LogModeTypeDef.SIGN_UP;
                            fragmentInteractionListener.displayLoginOTPScreen(finalPhoneNumber, mCurrentRegionCode, Integer.parseInt(response.body().getCode()));
//                        SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                        }
                    }else {
                        if (response.body().getCode().equals("00") || response.body().getCode().equals("42") || response.body().getCode().equals("49")) {
                            fragmentInteractionListener.displayLoginPasswordScreen(finalPhoneNumber);
                        } else if (response.body().getCode().equals("68")) {
                            if (response.body().getMessage() != null && llInputNumber != null && viewOldUser != null && getContext() != null){
                                llInputNumber.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_border_red));
                                viewOldUser.startAnimation(UserInfoBusiness.shakeError());
                                viewOldUser.setVisibility(View.VISIBLE);
                            }else {
                                if (getContext() != null)
                                    ToastUtils.showToast(getContext(), "Check phone failed!");
                            }
                        } else {
                            if (response.body().getMessage() != null && tvWrongPhone != null && llInputNumber != null && getContext() != null){
                                tvWrongPhone.setText(response.body().getMessage());
                                llInputNumber.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_border_red));
                                tvWrongPhone.startAnimation(UserInfoBusiness.shakeError());
                                tvWrongPhone.setVisibility(View.VISIBLE);
                            }else {
                                if (getContext() != null)
                                    ToastUtils.showToast(getContext(), "Check phone failed!");
                            }
                        }
                    }

                }else {
                    if (getContext() != null)
                        ToastUtils.showToast(getContext(), "Check phone failed!");
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                if (getContext() != null)
                    ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });
    }
    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if(innerView instanceof RoundTextView){
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    @OnClick(R.id.llCountryCode)
    public void onViewClicked() {
//        SelectCountryActivity.startActivityForResult(mLoginActivity, position, LoginActivity.REQUEST_CODE_COUNTRY);
    }

    private void initView() {
        if (SharedPrefs.getInstance().get(PREF_IS_LOGIN, Boolean.class)) {
            tvEnterNumberDes.setText("");
        } else {
            tvEnterNumberDes.setText(getText(R.string.mc_login));
            mTvEnterPhone.setText(getText(R.string.we_will_text_to_confirm_your_number));

        }
        setupUI(rlt_phone_number);
    }

    public interface OnFragmentInteractionListener {
        void displayLoginOTPScreen(String numberJid, String regionCode, int code);

        void displayLoginPasswordScreen(String phoneNumber);

        void displayEnterPhoneNumberScreen(String phoneNumber);

        void displayCreateAccountScreen(String phoneNumber);

        void displayCreateAccountScreen(String phoneNumber, String otp, String code);
    }
}
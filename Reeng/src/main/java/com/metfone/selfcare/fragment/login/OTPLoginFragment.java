package com.metfone.selfcare.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.DeviceUtils;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.SignIn;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_NUMBER;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.util.EnumUtils.LOG_MODE;


public class OTPLoginFragment extends Fragment implements /*SmsReceiverListener,*/ ClickListener.IconListener, MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String TAG = OTPLoginFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PHONE_NUMBER_PARAM = "param1";
    private static final String REGION_CODE_PARAM = "param2";
    private static final String CODE_PARAM = "code";
    private static final String PASSWORD = "password";
    //    private CountDownTimer countDownTimer;
    private static final int REQ_USER_CONSENT = 6;
    private static final int TIME_DEFAULT = 60000;
    private static final int code = 0;
    private static boolean handlerflag = false;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvPhoneNumber)
    CamIdTextView tvPhoneNumber;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.tvNotReceivedOtp)
    CamIdTextView tvNotReceivedOtp;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.ivWrongOTP)
    CamIdTextView ivWrongOTP;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.tv_error)
    AppCompatTextView tvError;
    @BindView(R.id.tvEnterNumber)
    CamIdTextView tvEnterNumber;
    @BindView(R.id.rll_otp_login)
    RelativeLayout mRllOtpLogin;
    @BindView(R.id.tvEnterNumberTitle)
    CamIdTextView tvEnterNumberTitle;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    RoundTextView btnPassword;
    @BindView(R.id.btnLogin)
    CamIdTextView btnLogin;
    @Nullable
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    String phoneNumber = "";
    int remain = 0;

    /*@BindView(R.id.btnContinue)
    RoundTextView btnContinue;*/
    Unbinder unbinder;
    String countDownString;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private int codeGenOtp;
    private String password = "";
    private OnFragmentInteractionListener mListener;
    private EnterPhoneNumberFragment.OnFragmentInteractionListener fragmentInteractionListener;
    private LoginActivity mLoginActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private boolean isPhoneExisted = false;
    private ClickListener.IconListener mClickHandler;
    //------------Variable Views-------------------
    private Handler mHandler;
    private int START_TEXT = 85;
    private int END_TEXT = 0;
    private boolean isSaveInstanceState = false;
    private boolean isLoginDone = false;
    private long countDownTimer;
    private MySMSBroadcastReceiver mySMSBroadcastReceiver;


    public OTPLoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param phoneNumber Parameter 1.
     * @param regionCode  Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    public static OTPLoginFragment newInstance(String phoneNumber, String regionCode, int code, String password) {
        OTPLoginFragment fragment = new OTPLoginFragment();
        Bundle args = new Bundle();
        args.putString(PHONE_NUMBER_PARAM, phoneNumber);
        args.putString(REGION_CODE_PARAM, regionCode);
        args.putInt(CODE_PARAM, code);
        args.putString(PASSWORD, password);
        fragment.setArguments(args);
        return fragment;
    }

    public static OTPLoginFragment newInstance() {

        Bundle args = new Bundle();
        OTPLoginFragment fragment = new OTPLoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void initView() {
        if (LOG_MODE == EnumUtils.LogModeTypeDef.LOGIN) {
            btnPassword.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.VISIBLE);
            tvEnterNumber.setText(getResources().getString(R.string.Use_OTP_to_login));
        } else {
            btnPassword.setVisibility(View.GONE);
            tvEnterNumber.setText(getString(R.string.Verify));
            btnLogin.setVisibility(View.GONE);
            tvEnterNumberTitle.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        handlerflag = true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        findComponentViews(rootView);
        setViewListeners();
        tvEnterNumberDes.setText(fomatString());//
//       tvEnterNumberDes.setText(getResources().getString(R.string.msg_otp_login_)+phoneNumber+".");
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        mLoginActivity.registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
        mySMSBroadcastReceiver.initOTPListener(this);
        btnPassword = rootView.findViewById(R.id.btnPassword);
        btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
        fragmentInteractionListener = (EnterPhoneNumberFragment.OnFragmentInteractionListener) getActivity();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                signIn();
            }
        });
        btnPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (LOG_MODE == EnumUtils.LogModeTypeDef.LOGIN) {
                    fragmentInteractionListener.displayLoginPasswordScreen(tvPhoneNumber.getText().toString());
                }
            }
        });
        mApplication.registerSmsOTPObserver();
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.BLACK);
        return rootView;
    }

    private void signIn() {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        SignIn signIn = new SignIn();
        String otp = etOtp.getText().toString();
        signIn.setOtp(otp);
        signIn.setType("otp");
        signIn.setPhone_number(phoneNumber);
        SignInRequest signInRequest = new SignInRequest(signIn, "", "", "", "");
        apiService.signIn(DeviceUtils.getUniqueDeviceId(), signInRequest).enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, retrofit2.Response<SignInResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                SignInResponse signInResponse = response.body();

                if (signInResponse != null) {
                    //success
                    if ("00".equals(signInResponse.getCode())) {
                        String token = "Bearer " + signInResponse.getData().getAccess_token();
                        SharedPrefs.getInstance().put(PREF_PHONE_NUMBER, phoneNumber);
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefresh_token());
                        generateOldMochaOTP(signInResponse.getData().getAccess_token());
                        //wrong otp
                    } else if ("1".equals(signInResponse.getCode())) {
                        etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_pin_otp_red));
                        ivWrongOTP.setVisibility(View.VISIBLE);
                    } else {
                        ToastUtils.showToast(mLoginActivity, signInResponse.getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(mLoginActivity, getString(R.string.service_error));
            }
        });
    }

    private void getUserInformation() {
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if (response.body() != null && response.body().getData() != null && response.body().getData().getUser() != null) {
                    if (response.body().getCode().equals("00")) {
//                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(requireContext());
                        /*
                        * fix bug firebase: Fragment OTPLoginFragment... not attached to a context.
                        * */
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        RestoreManager.setRestoring(true);
                        userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                            @Override
                            public void onStartDownload() {

                            }

                            @Override
                            public void onDownloadProgress(int percent) {

                            }

                            @Override
                            public void onDownloadComplete() {

                            }

                            @Override
                            public void onDowloadFail(String message) {

                            }

                            @Override
                            public void onStartRestore() {

                            }

                            @Override
                            public void onRestoreProgress(int percent) {

                            }

                            @Override
                            public void onRestoreComplete(int messageCount, int threadMessageCount) {
                                try {
                                    SharedPreferences pref = getContext().getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                                    if (pref != null) {
                                        pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                                        com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                                    }
                                } catch (Exception e) {
                                }
                                RestoreManager.setRestoring(false);
                                mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                                mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                                mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                                NavigateActivityHelper.navigateToHomeScreenActivity((BaseSlidingFragmentActivity) getActivity(), false, true);
                            }

                            @Override
                            public void onRestoreFail(String message) {
                                RestoreManager.setRestoring(false);
                                NavigateActivityHelper.navigateToHomeScreenActivity((BaseSlidingFragmentActivity) getActivity(), false, true);
                            }
                        }, mLoginActivity);
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(getContext(), getString(R.string.get_user_infor_fail));
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });
    }

    private void generateOldMochaOTP(String token) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(mLoginActivity).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(getContext(), "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), error.getMessage());
            }
        });

    }


    @Override
    public void onAttach(Activity activity) {
        mLoginActivity = (LoginActivity) activity;
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mRes = mLoginActivity.getResources();
        mLoginActivity.setTitle(getResources().getString(R.string.enter_code).toUpperCase());
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        etOtp.requestFocus();
        setupUI(mRllOtpLogin);
        UIUtil.showKeyboard(getContext(), etOtp);
        mClickHandler = this;
        isSaveInstanceState = false;
        // etOtp.setText("");
        if (!handlerflag && remain > 0) {
            startCountDown(remain * 1000);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mClickHandler = null;
        handlerflag = false;
        InputMethodUtils.hideSoftKeyboard(etOtp, mLoginActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }


    private void findComponentViews(View rootView) {
        etOtp.requestFocus();
//      InputMethodUtils.hideKeyboardWhenTouch(rootView, mLoginActivity);
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(PHONE_NUMBER_PARAM);
            mCurrentRegionCode = "KH";
            if (phoneNumber != null) {
                mCurrentNumberJid = "+855" + phoneNumber.substring(1);
            }

        }
        tvPhoneNumber.setText(phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3));
    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        Bundle args = new Bundle();
        generateOtpByServer();
        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
                if (LOG_MODE == EnumUtils.LogModeTypeDef.LOGIN) {
                    signIn();
                } else {
                    fragmentInteractionListener.displayCreateAccountScreen(phoneNumber, str.toString(), String.valueOf(getArguments().getInt(CODE_PARAM)));
                }
            }
        });

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() < 6) {
                    if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                        etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_pin_otp));
                        ivWrongOTP.setVisibility(View.GONE);
                        btnLogin.setEnabled(false);
                        btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
                    } else {
                        btnLogin.setEnabled(true);
                        btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_confirm));
                    }
                }
                if (s.length() == 6) {
                    btnLogin.setEnabled(true);
                    btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_confirm));
                } else {
                    btnLogin.setEnabled(false);
                    btnLogin.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etOtp.setOnKeyListener((v, keyCode, event) -> {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                //this is for backspace
                if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                    etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_pin_otp));
                    ivWrongOTP.setVisibility(View.GONE);
                }
            }
            return false;
        });
    }


    private void setResendPassListener() {
        tvResendOtp.setText(mLoginActivity.getString(R.string.mc_resend_otp));
        tvResendOtp.setTextColor(ContextCompat.getColor(mLoginActivity, R.color.resend));
        tvResendOtp.setOnClickListener(view -> showDialogConfirmResendCode());
    }

    private String fomatString() {
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        String mPhoneNumber = phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3);
        fmt.format(getString(R.string.text_otp_has_been_sent_to), mPhoneNumber);
        return sbuf.toString();
    }

    private void startCountDown(int time) {
        tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    setResendPassListener();
                } else if (remain > 0) {
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                } else {
                    remain = 0;
                }
                String countDownString = getString(R.string.mc_resend_otp);
                tvResendOtp.setText(countDownString);
                tvTimeResendOtp.setText(remain + " " + getString(R.string.text_seconds));
            }
        }, 1000);
    }

    public void generateOtpByServer() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.generateOtp(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                startSmsUserConsent();
                android.util.Log.d("TAG", "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(), etOtp);
                    }
                }, 200);
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.d("TAG", "onError: " + error.getMessage());

            }
        });


    }

    private void showDialogConfirmResendCode() {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_confirm_resend_code);
        String msg = String.format(mRes.getString(R.string.msg_confirm_resend_code), tvPhoneNumber.getText()
                .toString());
        PopupHelper.getInstance().showDialogConfirm(mLoginActivity, title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    private void setPasswordEditText(String pass) {
        etOtp.setText(pass);
        Selection.setSelection(etOtp.getText(), etOtp.getText().length());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mLoginActivity.unregisterReceiver(mySMSBroadcastReceiver);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    @OnClick({R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                mLoginActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onOTPTimeOut() {
    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    public interface OnFragmentInteractionListener {
        void displayPersonalInfo();

        void navigateToNextScreen();
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) mLoginActivity.getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            Log.i(TAG, "responseCode: " + responseCode);

            try {
//                mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    getUserInformation();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
                    if (tvError != null) {
                        tvError.setVisibility(View.GONE);
                    }
                    mApplication.logEventFacebookSDKAndFirebase(mLoginActivity.getString(R.string.c_login_complete));
                } else {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
//                    mLoginActivity.showError(responseCode.getDescription(), null);

                    if (tvError != null) {
                        tvError.setText(responseCode.getDescription());
                        tvError.setVisibility(View.VISIBLE);
                    }
                    mApplication.logEventFacebookSDKAndFirebase(mLoginActivity.getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }

    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == RESULT_OK && data != null) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (message != null) {
                    getOtpFromMessage(message);
                }

            }

        }
    }

    private void startSmsUserConsent() {
        // Fix crash firebase : Null activity is not permitted.
        if(getActivity() != null){
            SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

            Task task = client.startSmsUserConsent(null);

            task.addOnSuccessListener(new OnSuccessListener() {
                @Override
                public void onSuccess(Object o) {
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }
    }

    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            etOtp.setText(matcher.group(0));
        }
    }

    @Override
    public void onOTPReceived(Intent intent) {
        startActivityForResult(intent, REQ_USER_CONSENT);
    }


}
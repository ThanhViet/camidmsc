package com.metfone.selfcare.fragment.game;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.adapter.AccumulatePagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.ui.recyclerview.DividerItemDecoration;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 11/30/2016.
 */
public class AccumulatePagerHistoryFragment extends BaseRecyclerViewFragment implements AccumulatePointHelper.AccumulateHistoryListener,
        AccumulatePagerAdapter.ItemListener,
        BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = AccumulatePagerHistoryFragment.class.getSimpleName();
    private ListGamesActivity mParentActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private RecyclerView mRecyclerView;
    private AccumulatePagerAdapter mAdapter;
    private ArrayList<AccumulatePointItem> pointItems = new ArrayList<>();

    public AccumulatePagerHistoryFragment() {
    }

    public static AccumulatePagerHistoryFragment newInstance() {
        AccumulatePagerHistoryFragment fragment = new AccumulatePagerHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        AccumulatePointHelper.getInstance(mApplication).addAccumulateHistoryListener(this);
       /* try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        findViewComponent(inflater, rootView);
        drawDetail();
        return rootView;
    }

    @Override
    public void onDetach() {
        AccumulatePointHelper.getInstance(mApplication).removeAccumulateHistoryListener(this);
        super.onDetach();
    }

    @Override
    public void onSuccess() {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setAdapter();
            }
        });
    }

    @Override
    public void onError(int errorCode, String desc) {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pointItems == null || pointItems.isEmpty()) {
                    showEmptyNote(mRes.getString(R.string.list_empty));
                } else {
                    hideEmptyView();
                }
            }
        });
    }

    @Override
    public void onDeepLinkClick(AccumulatePointItem item) {
        String schemaLink = item.getDeepLink();
        DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, schemaLink);
    }

    @Override
    public void onExchangeClick(AccumulatePointItem item) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        AccumulatePointHelper.getInstance(mApplication).convertGiftPoint(item, new AccumulatePointHelper.GiftPointListener() {
            @Override
            public void onSuccess(String desc) {
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(int errorCode, String desc) {
                mParentActivity.hideLoadingDialog();
                if (!TextUtils.isEmpty(desc)) {
                    mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                } else {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onError(int errorCode, String desc, String tranId) {

            }
        });
    }

    @Override
    public void onInstallAppClick(AccumulatePointItem item) {

    }

    @Override
    public void onRetryClick() {

    }

    private void findViewComponent(LayoutInflater inflater, View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }

    private void drawDetail() {
        if (AccumulatePointHelper.getInstance(mApplication).isHistoryReady()) {
            setAdapter();
        } else {
            showProgressLoading();
        }
    }

    private void setAdapter() {
        int mType = AccumulatePointHelper.TAB_HISTORY;
        Log.i(TAG, "setAdapter ---------------> " + mType);
        pointItems = AccumulatePointHelper.getInstance(mApplication).getAccumulatePointItem(mType);
        if (pointItems == null) pointItems = new ArrayList<>();
        if (mAdapter == null) {
            mAdapter = new AccumulatePagerAdapter(mApplication, pointItems);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
            mRecyclerView.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
            setItemListener();
        } else {
            mAdapter.setListItem(pointItems);
            mAdapter.notifyDataSetChanged();
        }
        if (pointItems.isEmpty()) {
            showEmptyNote(mRes.getString(R.string.list_empty));
        } else {
            hideEmptyView();
        }
    }

    private void setItemListener() {
        mAdapter.setListener(this);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }
}
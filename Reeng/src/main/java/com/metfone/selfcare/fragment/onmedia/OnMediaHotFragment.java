package com.metfone.selfcare.fragment.onmedia;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.onmedia.OMFeedAdapter;
import com.metfone.selfcare.adapter.onmedia.QuickNewsOnMediaAdaper;
import com.metfone.selfcare.adapter.onmedia.SieuHaiAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.PubSubManager;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.QuickNewsOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.JSONHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.FeedOnMediaListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
@SuppressWarnings({"unused", "SameParameterValue"})
public class OnMediaHotFragment extends BaseLoginAnonymousFragment implements OnMediaHolderListener, ClickListener.IconListener
        , FeedOnMediaListener, InitDataListener, XMPPConnectivityChangeListener, TagMocha.OnClickTag
        , FeedBusiness.FeedBusinessInterface, PlayMusicController.OnPlayMusicStateChange
        , OnChannelChangedDataListener {

    private static final String TAG = OnMediaHotFragment.class.getSimpleName();
    //    private static final String PREFIX_HTTP = "http";
//    private static final String ADD_PREFIX = "://";
//    private static final String URL_DISCOVERY_MUSIC = "http://mobile.keeng.vn/";
//    private static final String URL_DISCOVERY_VIDEO = "https://m.youtube.com/";
//    private static final String URL_DISCOVERY_NEWS = "http://m.netnews.vn/default.html";
//    private static final long TIME_DELAY_RETRY = 1000;
    private OMFeedAdapter feedAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter;
    private BaseSlidingFragmentActivity mParentActivity;
    private boolean isLoading = false;
    private ApplicationController mApplication;
    //    private MusicBusiness mMusicBusiness;
    private Resources mRes;
    private boolean noMoreFeed = false;

    private FeedBusiness mFeedBusiness;
    private boolean isPagerVisible = false;

    //private OnMediaInterfaceListener connectionFeedInterface;
    private WSOnMedia rest;

    private View rootView;
    private FeedContent mFeedPost;
    private ReengAccount mAccount;
    private boolean isOnTopList = true;

    private View mFooterView, mHeader;
    private TextView mTvwWriteStatus;
    private View mViewPostPhoto, mViewPostLink;
    private LinearLayout mLoadmoreFooterView;

    private ImageView ivBack;
    private TextView tvTitle;

    private RoundedImageView mImgAvatarHeader;
    private TextView mTvwAvatarHeader;
    private View viewAvatarHeader;
    private RecyclerView mRecyclerViewQuickNews;
    private QuickNewsOnMediaAdaper mQuickNewsOnMediaAdaper;
    private OnMediaInterfaceListener connectionFeedInterface;

    private RecyclerView recyclerViewSieuHai;
    private View mViewFakeMarginBottom;
    private SieuHaiAdapter sieuHaiAdapter;
    private Animation logoMoveAnimation;

//    private Animation animScaleIn, animScaleOut;

    private SharedPreferences mPref;
    private long checkShowGuide;

    private boolean isSaveInstanceState = false;
    private boolean needShowDialog = false;
    private String urlGetData;

    private int gaCategoryId, gaActionId;

    private boolean enableClickTag = false;
    private ClickListener.IconListener iconListener;
    private EventOnMediaHelper eventOnMediaHelper;

    private boolean isFirstShowHoroscopeOnDay;
    private Handler mHandler;

    //    private boolean everShowFragment = false;           //check xem da show bao gio chua
    private boolean needLoadOnCreate = false;           //dung cho deeplink
    private static final int START_PAGE = 0;
    private static final int LIMIT = 20;
    private int currentPageSieuHai = START_PAGE;
    private boolean isLoadingSieuHai;
    private boolean isNoMoreSieuHai;
    private int pastVisiblesSieuHai, visibleSieuHaiCount, totalSieuHaiCount;
    // action bar
    private View mViewAbSuggestFriend, headerController;
    private TextView mTvwBadgerNumberFriendRequest;
    private View mViewNumberFriendRequest;
    ImageView ivFriend;
    private ListenerUtils listenerUtils;

    private ViewGroup container;

    //============view stub==============
    private ViewStub viewStub;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private RecyclerView mRecyclerView;
    private LinearLayout layoutHasNewFeed;
    private View mLayoutProgress;

    //=================================

    public static OnMediaHotFragment newInstance() {
        OnMediaHotFragment fragment = new OnMediaHotFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mParentActivity = (BaseSlidingFragmentActivity) getActivity();
        if (mParentActivity == null) return;
        if (mParentActivity instanceof ModuleActivity)
            needLoadOnCreate = true;
        mRes = mParentActivity.getResources();
        mApplication = (ApplicationController) mParentActivity.getApplication();
        rest = new WSOnMedia(mApplication);
//        mMusicBusiness = mApplication.getMusicBusiness();
//        mMediaService = mApplication.getMusicBusiness().getMediaService();
        mFeedBusiness = mApplication.getFeedBusiness();
        mAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        mPref = mParentActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        mHandler = new Handler();
        try {
            connectionFeedInterface = (OnMediaInterfaceListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectionFeedInterface");
        }
    }

    @Override
    public void onDestroyView() {
        XMPPManager.removeXMPPConnectivityChangeListener(this);
//        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
//        FeedBusiness.setFeedBusinessInterface(null);
        PlayMusicController.removePlayMusicStateChange(this);
        if (listenerUtils != null) listenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    protected String getSourceClassName() {
        return "OnMedia";
    }

    @Override
    public void onDetach() {
        mHandler = null;
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_tab_hot;
        iconListener = this;
        eventOnMediaHelper = new EventOnMediaHelper(mParentActivity);
        long timeLastShowHoroscope = mPref.getLong(Constants.PREFERENCE.PREF_ONMEDIA_LAST_TIME_SHOW_HOROSCOPE, 0L);
        isFirstShowHoroscopeOnDay = !TimeHelper.checkTimeInDay(timeLastShowHoroscope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        long time = System.currentTimeMillis();
        checkShowGuide = mPref.getLong(Constants.INTRO.CLICK_TO_PASTE_LINK, Constants.INTRO.INTRO_NOT_SHOW_YET);
        Log.i(TAG, "checkShowGuide: " + checkShowGuide);
        rootView = inflater.inflate(R.layout.fragment_onmedia_hot, container, false);
        this.container = container;
        findComponent(inflater, container);

        listenerUtils = mApplication.getApplicationComponent().providerListenerUtils();
        listenerUtils.addListener(this);

        XMPPManager.addXMPPConnectivityChangeListener(this);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
        ListenerHelper.getInstance().addNotifyFeedOnMediaListener(this);
        PlayMusicController.addPlayMusicStateChange(this);
        if (needLoadOnCreate) {
            Log.i(TAG, "load on create");
            initViewStub();
            onInitViewStubDone();
        }
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            ivFriend.setVisibility(View.GONE);
        }
        Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - time));
        return rootView;
    }

    boolean initViewStubDone = false;

    @SuppressLint("InflateParams")
    private void initViewStub() {
        if (initViewStubDone) return;
        Log.i(TAG, "initViewStub");

        View view = viewStub.inflate();
        mRecyclerView = view.findViewById(R.id.list_feed_onmedia);
        mSwipyRefreshLayout = view.findViewById(R.id.onmedia_swipy_layout);
        mLayoutProgress = view.findViewById(R.id.layout_progress_onmedia);
        layoutHasNewFeed = view.findViewById(R.id.layout_new_feed);
        initViewLogin(null, null, view);

        mFooterView = LayoutInflater.from(mParentActivity).inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLoadmoreFooterView = mFooterView.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);

        mHeader = LayoutInflater.from(mParentActivity).inflate(R.layout.header_hot_onmedia, null);
        viewAvatarHeader = mHeader.findViewById(R.id.layout_avatar_write_status);
        mTvwWriteStatus = mHeader.findViewById(R.id.tvw_write_status);
        mViewPostPhoto = mHeader.findViewById(R.id.llPostPicture);
        mViewPostLink = mHeader.findViewById(R.id.llEmotion);
        mImgAvatarHeader = mHeader.findViewById(R.id.img_onmedia_avatar_header);
        mTvwAvatarHeader = mHeader.findViewById(R.id.tvw_onmedia_avatar_header);

        mViewFakeMarginBottom = mHeader.findViewById(R.id.view_fake_margin_bottom);
        logoMoveAnimation = AnimationUtils.loadAnimation(mApplication, R.anim.appear_view);
        recyclerViewSieuHai = mHeader.findViewById(R.id.recyclerview_sieuhai);
        recyclerViewSieuHai.setLayoutManager(new LinearLayoutManager(mApplication, LinearLayoutManager.HORIZONTAL,
                false));
        recyclerViewSieuHai.setVisibility(View.GONE);
        mViewFakeMarginBottom.setVisibility(View.GONE);

        sieuHaiAdapter = new SieuHaiAdapter(mParentActivity, new ArrayList<>());
        recyclerViewSieuHai.setAdapter(sieuHaiAdapter);

        feedAdapter = new OMFeedAdapter(mApplication, new ArrayList<>(), this, this);
        feedAdapter.setActivity(mParentActivity);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mRecyclerView.setItemAnimator(null);

        mHeaderAndFooterRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(feedAdapter);
        mRecyclerView.setAdapter(mHeaderAndFooterRecyclerViewAdapter);

        mHeaderAndFooterRecyclerViewAdapter.addFooterView(mFooterView);
        mHeaderAndFooterRecyclerViewAdapter.addHeaderView(mHeader);

        setViewListener();
        initViewStubDone = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisible) {
        super.setUserVisibleHint(isVisible);
        Log.i(TAG, "setUserVisibleHint: " + isVisible);

        isPagerVisible = isVisible;
        if (isVisible) {
            if (getView() == null) {
                if (mParentActivity == null || mParentActivity instanceof HomeActivity)
                    needLoadOnCreate = true;

            }
            // log GA
            else {
                if (mParentActivity == null) return;
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_label_on_media_tab_view);
                if (mParentActivity instanceof HomeActivity) {
                    initViewStub();
//                    ChangeABColorHelper.changeTabColor(bgHeader, HomeTab.tab_hot, null,
//                            ((HomeActivity) mParentActivity).getCurrentTab(), ((HomeActivity) mParentActivity).getCurrentColorTabWap());
                }
                onInitViewStubDone();
            }

        }
    }

    private void onInitViewStubDone() {
        if (initViewStubDone) {
            mRecyclerView.postDelayed(this::getNumberNotify, 100);

            if (mApplication.isDataReady()) {
                Log.i(TAG, "load on create");
                mRecyclerView.postDelayed(this::loadDataFeed, 100);
                mApplication.getAvatarBusiness().setMyAvatar(mImgAvatarHeader, mTvwAvatarHeader, mTvwAvatarHeader,
                        mApplication.getReengAccountBusiness().getCurrentAccount(), null);

            }
            changeUnreadOnMediaBadge();
            checkPlayer();
        }
    }

    @Override
    public void onUpdateNotifyFeed(int totalNotify) {
        Log.i(TAG, "total: " + totalNotify);
        mParentActivity.runOnUiThread(this::changeUnreadOnMediaBadge);
    }

    private void changeUnreadOnMediaBadge() {
        /*mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mFeedBusiness.getTotalNotify() == 0) {
                    mViewAbBadgerNotify.setVisibility(View.GONE);
                } else {
                    mTvwBadgerNumberNotify.setText(TextHelper.getTextNumberNotify(mFeedBusiness.getTotalNotify()));
                    mViewAbBadgerNotify.setVisibility(View.VISIBLE);
                }
            }
        });*/
    }

    private void findComponent(LayoutInflater inflater, ViewGroup container) {

        viewStub = rootView.findViewById(R.id.viewStub);
        // ab
        mViewAbSuggestFriend = rootView.findViewById(R.id.layout_discover);
        headerController = rootView.findViewById(R.id.headerController);
        mViewNumberFriendRequest = rootView.findViewById(R.id.item_number_discover);
        mViewNumberFriendRequest.setVisibility(View.GONE);
        mTvwBadgerNumberFriendRequest = rootView.findViewById(R.id.tvw_number_friend);
        ivBack = rootView.findViewById(R.id.ivBack);
        tvTitle = rootView.findViewById(R.id.home_ab_title);
        ivFriend = rootView.findViewById(R.id.ivFriend);
        if (mParentActivity instanceof HomeActivity) {
            ivBack.setVisibility(View.GONE);
            Utilities.setMargins(tvTitle, Utilities.dpToPixel(R.dimen.v5_spacing_normal, getResources()), 0, tvTitle.getRight(), tvTitle.getBottom());
            Utilities.setMargins(headerController, headerController.getLeft(), 0, headerController.getRight(), headerController.getBottom());
        } else {
            ivBack.setVisibility(View.VISIBLE);
            Utilities.setMargins(tvTitle, 0, tvTitle.getTop(), tvTitle.getRight(), tvTitle.getBottom());
            Utilities.setMargins(headerController, headerController.getLeft(), 0, headerController.getRight(), headerController.getBottom());
        }
        ivBack.setOnClickListener(v -> mParentActivity.onBackPressed());
    }

    private void handleDeleteFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        Log.i(TAG, "rowID: " + feed.getBase64RowId());
        if (TextUtils.isEmpty(feed.getBase64RowId()) || "[]".equals(feed.getBase64RowId()) ||
                "null".equals(feed.getBase64RowId())) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp
                        .DELETE, "",
                feed.getBase64RowId(), "", null,
                response -> {
                    Log.i(TAG, "DeleteFeed: on response: " + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                            int code = JSONHelper.getInt(jsonObject, Constants.HTTP.REST_CODE, 0);
                            if (code == HTTPCode.E200_OK) {
//                                    mParentActivity.showToast(R.string.onmedia_action_success);
                            } else {
                                throw new Exception();
                            }
                        } else {
                            throw new Exception();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
//                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }

                }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));

        mParentActivity.runOnUiThread(() -> {
            mFeedBusiness.getListFeed().remove(feed);
            feedAdapter.setListFeed(mFeedBusiness.getListFeed());
            feedAdapter.notifyDataSetChanged();
        });

    }

    @Override
    public void onResume() {
        enableClickTag = true;
        ListenerHelper.getInstance().addInitDataListener(this);
        isSaveInstanceState = false;
        if (needShowDialog && !TextUtils.isEmpty(urlGetData)) {
            showConfirmRetryGetMetadata(urlGetData);
            needShowDialog = false;
        }
        mParentActivity.runOnUiThread(() -> {
            if (feedAdapter != null && feedAdapter.getItemCount() != 0
                    && !mFeedBusiness.getListFeed().isEmpty()) {
                feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                feedAdapter.notifyDataSetChanged();
                Log.i(TAG, "notify when resume");
            }
        });
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged ");
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
                || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (mHeaderAndFooterRecyclerViewAdapter != null && mHeaderAndFooterRecyclerViewAdapter.getItemCount() > 0) {
                mHeaderAndFooterRecyclerViewAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onPause() {
        ListenerHelper.getInstance().removeInitDataListener(this);
        InputMethodUtils.hideSoftKeyboard(mParentActivity);
        PubSubManager.getInstance(mApplication).startCountDownUnSubConnectionFeeds();
        isPagerVisible = false;
        super.onPause();
    }

    @Override
    public void onStop() {
        isPagerVisible = false;
        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    private void updateFeedFromPresence(final boolean animation) {
        mParentActivity.runOnUiThread(() -> {
            mFeedBusiness.setListFeed(mFeedBusiness.getListFeedAfterDeleteDuplicate());
            feedAdapter.setListFeed(mFeedBusiness.getListFeed());
            feedAdapter.notifyDataSetChanged();
            hideButtonNewStories(animation);
            mFeedBusiness.clearListFeedPresence();
        });
    }

    private void loadDataFeed() {
        if (isFirstShowHoroscopeOnDay) {
            isFirstShowHoroscopeOnDay = false;
            mPref.edit().putLong(Constants.PREFERENCE.PREF_ONMEDIA_LAST_TIME_SHOW_HOROSCOPE,
                    System.currentTimeMillis()).apply();
            Log.i(TAG, "---------------loadDataFeed: isFirstShowHoroscopeOnDay " + isFirstShowHoroscopeOnDay);
            loadData("", 1, true);
        } else {
            loadData("", 0, true);
        }
    }

    private void setViewListener() {

        /*recyclerViewSieuHai.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerViewSieuHai.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleSieuHaiCount = layoutManager.getChildCount();
                    totalSieuHaiCount = layoutManager.getItemCount();
                    pastVisiblesSieuHai = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleSieuHaiCount + pastVisiblesSieuHai) >= (totalSieuHaiCount - 5)) && !isLoadingSieuHai &&
                            !isNoMoreSieuHai) {
                        isLoadingSieuHai = true;
                        currentPageSieuHai += LIMIT;
                        getListVideoSieuHai(currentPageSieuHai);
                        Log.i(TAG, "loadmore sieu hai");
                    }
                }
            }
        });

        sieuHaiAdapter.setOnClickItemSieuHai(new OnMediaInterfaceListener.OnClickItemSieuHai() {
            @Override
            public void onClickVideoSieuHai(Video sieuHaiModel, int position) {
                mApplication.getApplicationComponent().providesUtils().openVideoDetail(mParentActivity, sieuHaiModel);
            }
        });*/

        layoutHasNewFeed.setOnClickListener(view -> mParentActivity.runOnUiThread(() -> {
            mFeedBusiness.setListFeed(mFeedBusiness.getListFeedAfterDeleteDuplicate());
            feedAdapter.setListFeed(mFeedBusiness.getListFeed());
            feedAdapter.notifyDataSetChanged();
//                        layoutHasNewFeed.setVisibility(View.GONE);
            hideButtonNewStories(true);
            scrollSmothToTop();
            mFeedBusiness.clearListFeedPresence();
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string
                    .ga_onmedia_label_click_new_stories);
        }));

        mSwipyRefreshLayout.setOnRefreshListener(direction -> {
            if (isLoading) {
                Log.i(TAG, "isloading....");
                mLoadmoreFooterView.setVisibility(View.GONE);
            } else {
                Log.i(TAG, "loaddata onRefresh");
                loadData("", 0, false);
            }
        });

        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !mFeedBusiness
                            .getListFeed().isEmpty() && !noMoreFeed) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }

            }
        };

//        mRecyclerView.setOnScrollListener(mApplication.getPauseOnScrollListener(onScrollListener));
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));

        mHeader.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
            mParentActivity.startActivity(intent);
        });

        mTvwWriteStatus.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
            mParentActivity.startActivity(intent);
        });

        mViewPostPhoto.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_IMAGE);
            mParentActivity.startActivity(intent);
        });

        mViewPostLink.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
//            intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_LINK);
            mParentActivity.startActivity(intent);
        });

        viewAvatarHeader.setOnClickListener(v -> NavigateActivityHelper.navigateToMyProfile(mParentActivity));

        mViewAbSuggestFriend.setOnClickListener(v -> {
            mViewNumberFriendRequest.setVisibility(View.GONE);
            resetNotifySuggestFriend();
            NavigateActivityHelper.navigateToContactList(mParentActivity, Constants.CONTACT.FRAG_LIST_SOCIAL_REQUEST);
        });

    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void showPopupContextMenuLongClick(FeedModelOnMedia feed) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(mRes.getString(R.string.onmedia_copy_text), -1,
                feed, Constants.MENU.MENU_COPY_TEXT);
        listMenu.add(copyItem);
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mParentActivity,
                    null, listMenu, this);
        }
    }

    private void onLoadMore() {
        if (isLoading) {
            Log.i(TAG, "loaddata onLoadMore isLoading");
        } else {
            if (!noMoreFeed) {
                String lastRowId = getLastRowId();
                Log.i(TAG, "loaddata onLoadMore " + lastRowId);
                loadData(lastRowId, 0, false);
            } else {
                Log.i(TAG, "loaddata onLoadMore nomorefeed");
            }
        }
    }

    private String getLastRowId() {
        if (mFeedBusiness.getListFeed() == null || mFeedBusiness.getListFeed().isEmpty()) {
            return "";
        } else {
            return mFeedBusiness.getListFeed().get(mFeedBusiness.getListFeed().size() - 1).getBase64RowId();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!(mParentActivity instanceof HomeActivity)) {

        }
    }

    private void loadDataFromPref() {
        if (mHandler == null) return;
        mHandler.postDelayed(() -> {
            ArrayList<FeedModelOnMedia> feedPref = mFeedBusiness.getListFeedFromPref();
            if (feedPref != null && !feedPref.isEmpty()) {
                Log.i(TAG, "loadDataFeed from PREF");
                mLayoutProgress.setVisibility(View.GONE);
                feedAdapter.setListFeed(feedPref);
                feedAdapter.notifyDataSetChanged();
            }
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);
    }

    private void loadData(final String lastRowId, int type, final boolean firstLoad) {
//        mFeedBusiness.setIndexCurrentPlaySieuHai(-1); //chua play video nao tu sieu hai
        if (mParentActivity != null) {
            if (!NetworkHelper.isConnectInternet(mParentActivity)) {
//                mParentActivity.showToast(R.string.no_connectivity_check_again);
                mSwipyRefreshLayout.setRefreshing(false);
                mLayoutProgress.setVisibility(View.GONE);
                if (firstLoad) {
                    loadDataFromPref();
                }
                return;
            }
            isLoading = true;
            if (!firstLoad) {
                mHandler.post(() -> mLoadmoreFooterView.setVisibility(View.VISIBLE));
            }

//            rest.test();
            final long t = System.currentTimeMillis();
            rest.getFeedOnMedia(lastRowId, type, restAllFeedsModel -> {
                if (TextUtils.isEmpty(lastRowId)) {
                    mApplication.trackingSpeed(mRes.getString(R.string.ga_category_load_api),
                            mRes.getString(R.string.ga_label_speed_load_tab_hot),
                            (System.currentTimeMillis() - t));
                } else {
                    mApplication.trackingSpeed(mRes.getString(R.string.ga_category_load_api),
                            mRes.getString(R.string.ga_label_speed_load_more_tab_hot),
                            (System.currentTimeMillis() - t));
                }

                Log.i(TAG, "onResponse: getFeedOnMedia code: " + restAllFeedsModel.getCode());
//                    RecyclerViewStateUtils.setFooterViewState(mRecyclerView, LoadingFooter.State.Normal);
                isLoading = false;
                mLayoutProgress.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (restAllFeedsModel.getCode() == HTTPCode.E200_OK) {
                    mFeedBusiness.setDeltaTimeServer(
                            System.currentTimeMillis() - restAllFeedsModel.getCurrentTimeServer());
                    onLoadDataDone(restAllFeedsModel.getData(), restAllFeedsModel.getListQuickNews(), lastRowId);
                } else {
                    //load loi
                    loadDataFromPref();
                }
            }, volleyError -> {
                isLoading = false;
                mLayoutProgress.setVisibility(View.GONE);
                mLoadmoreFooterView.setVisibility(View.GONE);
                mSwipyRefreshLayout.setRefreshing(false);
                if (firstLoad) {
                    loadDataFromPref();
                }
//                    RecyclerViewStateUtils.setFooterViewState(mParentActivity, mRecyclerView, LoadingFooter.State
// .NetWorkError, mFooterClick);
                Log.e(TAG, "error: " + volleyError.toString());
            });
            /*if (TextUtils.isEmpty(lastRowId)) {
                currentPageSieuHai = START_PAGE;
                getListVideoSieuHai(currentPageSieuHai);
            }*/
        }

    }

    /*private View.OnClickListener mFooterClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RecyclerViewStateUtils.setFooterViewState(mParentActivity, mRecyclerView, LoadingFooter.State.Loading,
            null);
            onLoadMore();
        }
    };*/

    private void onLoadDataDone(ArrayList<FeedModelOnMedia> result, ArrayList<QuickNewsOnMedia> listQuickNews,
                                String lastRowId) {
        mLoadmoreFooterView.setVisibility(View.GONE);
        if (result != null && !result.isEmpty()) {
            noMoreFeed = false;
            result = mFeedBusiness.preProcessListFeedModelListTag(result);
            if (TextUtils.isEmpty(lastRowId)) {
                mFeedBusiness.clearListFeedPresence();
                mFeedBusiness.setListFeed(result);
                mFeedBusiness.insertListPostToListFeed();

                feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                feedAdapter.notifyDataSetChanged();
                mFeedBusiness.setListFeedToPref();

                getListSuggestFriend(1);
                //TODO tam thoi an view quicknews
                /*if (listQuickNews != null && !listQuickNews.isEmpty()) {
                    mRecyclerViewQuickNews.setVisibility(View.VISIBLE);
                    mQuickNewsOnMediaAdaper.setListQuickNews(listQuickNews);
                    mQuickNewsOnMediaAdaper.notifyDataSetChanged();
                } else {
                    mRecyclerViewQuickNews.setVisibility(View.GONE);
                }*/
            } else {
                mFeedBusiness.getListFeed().addAll(result);

                feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                feedAdapter.notifyDataSetChanged();
            }
            Log.i(TAG, "onLoadDataDone: lastRowId: " + lastRowId);
            Log.i(TAG, "load data done: " + mFeedBusiness.getListFeed().size());

        } else {
            Log.i(TAG, "nomore feed");
            noMoreFeed = true;
            if (TextUtils.isEmpty(lastRowId)) {
                mFeedBusiness.setListFeed(new ArrayList<>());
                feedAdapter.setListFeed(new ArrayList<>());
                feedAdapter.notifyDataSetChanged();
                mFeedBusiness.setListFeedToPref();
            }
        }
    }

    private void getListVideoSieuHai(final int page) {
        if (!NetworkHelper.isConnectInternet(mParentActivity) ||
                !mApplication.getReengAccountBusiness().isVietnam()) {// không phai so vietnam thi ko hien thi sieu hai
//            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }

        //noinspection deprecation
        mApplication.getApplicationComponent().providerVideoApi().getVideosByCategoryId(null, "7", LIMIT, page, "", new
                OnVideoCallback() {
                    @Override
                    public void onGetVideosSuccess(ArrayList<Video> listVideo) {
                        isLoadingSieuHai = false;
                        if (listVideo == null || listVideo.isEmpty()) {
                            //TODO ko load more nua
                            isNoMoreSieuHai = true;
                            Log.i(TAG, "null or emply");
                        } else {
                            Log.i(TAG, "size listvideo " + listVideo.size());
                            if (page == START_PAGE) {
                                sieuHaiAdapter.setListVideo(listVideo);
                                if (recyclerViewSieuHai.getVisibility() != View.VISIBLE) {
                                    recyclerViewSieuHai.startAnimation(logoMoveAnimation);
                                    recyclerViewSieuHai.setVisibility(View.VISIBLE);
                                    mViewFakeMarginBottom.setVisibility(View.VISIBLE);
                                } else {
                                    recyclerViewSieuHai.scrollToPosition(0);
                                }
                                mFeedBusiness.getListSieuHai().clear();
                            } else {
                                sieuHaiAdapter.getListVideo().addAll(listVideo);
                            }
                            mFeedBusiness.getListSieuHai().addAll(listVideo);
                            sieuHaiAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onGetVideosError(String s) {
                        isLoadingSieuHai = false;
                        Log.e(TAG, "getListVideoSieuHai: " + s);
                    }

                    @Override
                    public void onGetVideosComplete() {

                    }
                });

    }

    private void getListSuggestFriend(final int page) {
        if (!NetworkHelper.isConnectInternet(mParentActivity) ||
                !mApplication.getReengAccountBusiness().isVietnam()) {// không phai so vietnam thi ko hien thi sieu hai
//            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.getListSuggestFriend(new OnMediaInterfaceListener.GetListSuggestFriend() {
            @Override
            public void onGetListSuggestFriendDone(ArrayList<UserInfo> listUser, String title) {
                if (!listUser.isEmpty()) {
                    FeedModelOnMedia feedSuggestFriend = new FeedModelOnMedia();
                    FeedContent feedContent = new FeedContent();
                    feedContent.setItemType(FeedContent.ITEM_TYPE_SUGGEST_FRIEND);
                    feedSuggestFriend.setFeedContent(feedContent);
                    feedSuggestFriend.setListSuggestFriend(listUser);
                    feedSuggestFriend.setUserStatus(title);
                    mFeedBusiness.getListFeed().add(1, feedSuggestFriend);
                    feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                    feedAdapter.notifyDataSetChanged();
                    scrollSmothToTop();
                }
                Log.i(TAG, "title: " + title);
            }

            @Override
            public void onError(int code, String des) {
                Log.e(TAG, "onError getListSuggestFriend: " + code + " des: " + des);
            }
        }, page);
    }

    @Override
    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        final boolean isLiked = feed.getIsLike() == 1;
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        mFeedBusiness.addUrlActionLike(feed.getFeedContent().getUrl(), feed.getBase64RowId(), action, feed.getFeedContent());
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        //neu la like thi tang like them 1, neu la unlike thi tru 1.
        int delta;
        if (isLike) {
            delta = 1;
            feed.setIsLike(1);
        } else {
            delta = -1;
            feed.setIsLike(0);
        }
        long countLike = feed.getFeedContent().getCountLike();
        feed.getFeedContent().setCountLike(countLike + delta);
        feedAdapter.setListFeed(mFeedBusiness.getListFeed());
        feedAdapter.notifyDataSetChanged();
    }

    private void setShareFeed(FeedModelOnMedia feed, int delta) {
        long countShare = feed.getFeedContent().getCountShare();
//        long countComment = feed.getFeedContent().getCountComment();
        feed.getFeedContent().setCountShare(countShare + delta);
//        feed.getFeedContent().setCountComment(countComment + delta);
        if (delta == 1) {
            feed.setIsShare(1);
        } else {
            feed.setIsShare(0);
        }
        feedAdapter.setListFeed(mFeedBusiness.getListFeed());
        feedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickCommentFeed(FeedModelOnMedia feed) {
        displayCommentStatusFragment(feed);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_comment);
    }

    @Override
    public void onClickShareFeed(final FeedModelOnMedia feed) {
        eventOnMediaHelper.showPopupContextMenuShare(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share);
    }

    private void handleShareNow(final FeedModelOnMedia feed) {
        if (feed.getIsShare() == 1) {
            mParentActivity.showToast(R.string.onmedia_already_shared);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        final String url = feed.getFeedContent().getUrl();
        rest.logAppV6(url, "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE, "", feed.getBase64RowId(),
                "",
                null,
                response -> {
                    Log.i(TAG, "handleShareNow: onresponse: " + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                            int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                mParentActivity.showToast(R.string.onmedia_share_success);
                                // fake new feed
                                FeedModelOnMedia feedModelOnMedia = new FeedModelOnMedia();
                                FeedModelOnMedia feedDuplicate = mFeedBusiness.getFeedModelFromUrl(url);
                                if (feedDuplicate != null) {
                                    feedModelOnMedia = feedDuplicate;
                                    mFeedBusiness.getListFeed().remove(feedDuplicate);
                                } else {
                                    feedModelOnMedia.setFeedContent(mFeedPost);
                                    feedModelOnMedia.setIsLike(0);
                                }
                                feedModelOnMedia.setIsShare(1);
                                long countShare = feedModelOnMedia.getFeedContent().getCountShare();
                                feedModelOnMedia.getFeedContent().setCountShare(countShare + 1);
                                feedModelOnMedia.setUserStatus("");
                                UserInfo mUserInfo = new UserInfo(mAccount.getJidNumber(), mAccount.getName());
                                feedModelOnMedia.setUserInfo(mUserInfo);
                                feedModelOnMedia.setTimeStamp(System.currentTimeMillis());
                                feedModelOnMedia.setTimeServer(System.currentTimeMillis());
                                feedModelOnMedia.setActionType(FeedModelOnMedia.ActionLogApp.SHARE);
                                feedModelOnMedia.setBase64RowId("");

//                                    mRecyclerView.insert(0, feedModelOnMedia);
                                mFeedBusiness.getListFeed().add(0, feedModelOnMedia);
                                feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                                feedAdapter.notifyDataSetChanged();
                                mFeedBusiness.getListPost().add(feedModelOnMedia);
                            } else {
                                mParentActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } else {
                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));
    }

    @Override
    public void onClickUser(UserInfo userInfo) {
        if (userInfo != null) {
            eventOnMediaHelper.processUserClick(userInfo);
        }
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_avatar);
    }

    @Override
    public void onClickMediaItem(FeedModelOnMedia feed) {
        eventOnMediaHelper.handleClickMediaItem(feed, gaCategoryId, gaActionId, connectionFeedInterface, iconListener);
    }

    @Override
    public void onClickImageItem(FeedModelOnMedia feed, int positionImage) {
        eventOnMediaHelper.handleClickImage(feed, positionImage, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_TAB_HOT);
    }

    @Override
    public void onClickMoreOption(FeedModelOnMedia feed) {
//        showPopupContextMenu(feed);
        eventOnMediaHelper.handleClickMoreOption(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_option_feed);
    }

    @Override
    public void onClickButtonTotal(View rowView, FeedModelOnMedia feed) {
        eventOnMediaHelper.handleShareTotal(rowView, feed, gaCategoryId, gaActionId);
    }

    @Override
    public void onLongClickStatus(FeedModelOnMedia feed) {
        if (feed != null) {
            showPopupContextMenuLongClick(feed);
        }
    }

    @Override
    public void onDeepLinkClick(FeedModelOnMedia feed, String link) {
        Log.i(TAG, "onDeepLinkClick: " + link);
        eventOnMediaHelper.handleDeeplink(feed, link);
    }

    @Override
    public void onClickSuggestFriend(UserInfo userInfo) {
        if (userInfo.isAddFriend()) {
            ThreadMessage threadMessage = mApplication.getStrangerBusiness().
                    createMochaStrangerAndThread(userInfo.getMsisdn(), userInfo.getName(), userInfo.getAvatar(),
                            Constants
                                    .CONTACT.STRANGER_MOCHA_ID, true);
            NavigateActivityHelper.navigateToChatDetail(mParentActivity, threadMessage);

        } else {
            ContactRequestHelper.getInstance(mApplication).requestSocialFriend(userInfo.getMsisdn(), userInfo.getName(),
                    Constants.CONTACT.SOCIAL_SOURCE_MOCHA, Constants.CONTACT.FOLLOW_STATE_NONE,
                    null, new ContactRequestHelper.onFollowResponse() {
                        @Override
                        public void onResponse(int status, String rowId) {
                            Log.i(TAG, "add friend status: " + status + " rowId: " + rowId);
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.e(TAG, "add friend error: " + errorCode);
                        }
                    });
        }

    }

    @Override
    public void openChannelInfo(FeedModelOnMedia feed) {
        Channel channelVideo = Channel.convertFromChannelOnMedia(feed.getFeedContent().getChannel());
        if (channelVideo == null) return;
        mApplication.getApplicationComponent().providesUtils().openChannelInfo(mParentActivity, channelVideo);
    }

    @Override
    public void openPlayStore(FeedModelOnMedia feed, String packageName) {
        NavigateActivityHelper.navigateToPlayStore(mParentActivity, packageName);
    }

    @Override
    public void onSubscribeChannel(FeedModelOnMedia feed, Channel channel) {
        if (channel != null) {
            ApplicationController.self().getListenerUtils().notifyChannelSubscriptionsData(channel);
            ApplicationController.self().getApplicationComponent().providerChannelApi().callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_EXIT:
                FeedModelOnMedia item = (FeedModelOnMedia) entry;
                eventOnMediaHelper.handleClickPopupExitListenTogether(item, connectionFeedInterface);
                break;
            /*case Constants.MENU.MENU_SETTING:
                NavigateActivityHelper.navigateToOnMediaDetail(mParentActivity, null, Constants.ONMEDIA.SETTING, -1);
                break;*/
            case Constants.MENU.NOW_PLAYING:
                FeedModelOnMedia itemNowPlaying = (FeedModelOnMedia) entry;
                mFeedBusiness.setFeedPlayList(itemNowPlaying);
                NavigateActivityHelper.navigateToOnMediaDetail(mParentActivity, itemNowPlaying.getFeedContent()
                        .getUrl(), Constants.ONMEDIA.ALBUM_DETAIL, -1, false);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string
                        .ga_onmedia_label_click_button_now_playing);
                break;
            case Constants.MENU.MENU_SHARE_LINK:
                FeedModelOnMedia feed = (FeedModelOnMedia) entry;
//                handleShareLink(feed);
                handleShareNow(feed);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share_now);
                break;

            case Constants.MENU.MENU_WRITE_STATUS:
                FeedModelOnMedia feedWrite = (FeedModelOnMedia) entry;
                handleWriteStatus(feedWrite);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_write_status);
                break;
            case Constants.MENU.EDIT:
                FeedModelOnMedia feedEdit = (FeedModelOnMedia) entry;
                handleEditFeed(feedEdit);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_edit);
                break;

            case Constants.MENU.DELETE:
                FeedModelOnMedia feedDelete = (FeedModelOnMedia) entry;
                confirmDelete(feedDelete);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_delete);
                break;

            case Constants.MENU.UNFOLLOW:
                FeedModelOnMedia feedUnfollow = (FeedModelOnMedia) entry;
                confirmUnfollow(feedUnfollow);
                break;

            case Constants.MENU.REPORT:
                FeedModelOnMedia feedReport = (FeedModelOnMedia) entry;
                confirmReport(feedReport);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_report);
                break;

            case Constants.MENU.COPY:
                FeedModelOnMedia feedCopy = (FeedModelOnMedia) entry;
                TextHelper.copyToClipboard(mParentActivity, feedCopy.getFeedContent().getLink());
                mParentActivity.showToast(R.string.copy_to_clipboard);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy);
                break;

            case Constants.MENU.POPUP_UNFOLLOW:
                FeedModelOnMedia feedPopupUnfollow = (FeedModelOnMedia) entry;
                handleUnfollow(feedPopupUnfollow);
                break;

            case Constants.MENU.POPUP_REPORT:
                FeedModelOnMedia feedPopupReport = (FeedModelOnMedia) entry;
                handleReport(feedPopupReport);
                break;
            case Constants.MENU.POPUP_DELETE:
                FeedModelOnMedia feedPopupDelete = (FeedModelOnMedia) entry;
                handleDeleteFeed(feedPopupDelete);
                break;
            case Constants.MENU.MENU_LIST_SHARE:
                FeedModelOnMedia feedListShare = (FeedModelOnMedia) entry;
                if (!TextUtils.isEmpty(feedListShare.getFeedContent().getUrl())) {
                    NavigateActivityHelper.navigateToOnMediaLikeOrShare(mParentActivity, feedListShare.getFeedContent()
                            .getUrl(), false);
                }
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_share);
                break;
            case Constants.MENU.MENU_COPY_TEXT:
                FeedModelOnMedia feedCopyText = (FeedModelOnMedia) entry;
                String textCopy;
                if (FeedContent.ITEM_TYPE_SOCIAL.equals(feedCopyText.getFeedContent().getItemType())) {
                    textCopy = mFeedBusiness.getTextTagCopy(feedCopyText.getFeedContent().getContentStatus(), feedCopyText.getFeedContent().getContentListTag());
                } else
                    textCopy = mFeedBusiness.getTextTagCopy(feedCopyText.getUserStatus(), feedCopyText.getListTag());
                TextHelper.copyToClipboard(mParentActivity, textCopy);
                mParentActivity.showToast(R.string.copy_to_clipboard);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy_text);
                break;
            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                FeedModelOnMedia feedShareFb = (FeedModelOnMedia) entry;
                OnMediaHelper.shareFacebookOnMedia(feedShareFb, mParentActivity);
                break;
            case Constants.MENU.SEND_MESSAGE:
                if (entry instanceof FeedModelOnMedia) {
                    ShareUtils.sendToFriend(mParentActivity, (FeedModelOnMedia) entry);
                }
                break;
            case Constants.MENU.MORE:
                if (entry instanceof FeedModelOnMedia) {
                    ShareUtils.shareWithIntent(mParentActivity, (FeedModelOnMedia) entry);
                }
                break;
            default:
                break;
        }
    }

    private void handleEditFeed(FeedModelOnMedia feedEdit) {
        if (TextUtils.isEmpty(feedEdit.getBase64RowId())) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        PopupHelper.getInstance().navigateToEditPostOnMedia(mParentActivity, feedEdit);
    }

    private void handleWriteStatus(FeedModelOnMedia feedWrite) {
        eventOnMediaHelper.navigateToPostOnMedia(mParentActivity,
                feedWrite.getFeedContent(), "", feedWrite.getBase64RowId(), false,
                FeedModelOnMedia.ActionFrom.onmedia);
    }

    public void displayCommentStatusFragment(FeedModelOnMedia mItem) {
        boolean showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(mItem);
        NavigateActivityHelper.navigateToOnMediaDetail(mParentActivity, mItem.getFeedContent().getUrl(),
                Constants.ONMEDIA.COMMENT, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_TAB_HOT, showMenuCopy);
    }

    private void confirmUnfollow(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.onmedia_setting_unfollow_text1);
        String msg = mRes.getString(R.string.onmedia_message_unfollow);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_UNFOLLOW);
    }

    private void confirmReport(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.onmedia_setting_report);
        String msg = mRes.getString(R.string.onmedia_message_report);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_REPORT);
    }

    private void confirmDelete(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.delete);
        String msg = mRes.getString(R.string.onmedia_message_delete);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_DELETE);
    }

    private void handleUnfollow(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.unfollowOfficialAndDelete(feed.getUserInfo().getMsisdn(), feed.getBase64RowId(), s -> {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(s);
                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                    int code = JSONHelper.getInt(jsonObject, Constants.HTTP.REST_CODE, 0);
                    if (code == HTTPCode.E200_OK) {
                        mParentActivity.showToast(R.string.onmedia_unfollow_success);
                        mFeedBusiness.getListFeed().remove(feed);
                        feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                        feedAdapter.notifyDataSetChanged();
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

        }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));
    }

    private void handleReport(FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.reportViolation(feed, s -> {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(s);
                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                    int code = JSONHelper.getInt(jsonObject, Constants.HTTP.REST_CODE, 0);
                    if (code == HTTPCode.E200_OK) {
                        mParentActivity.showToast(R.string.onmedia_action_success);
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

        }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));
    }

    private void getNumberNotify() {
        rest.getNumberNotify(s -> {
            Log.i(TAG, "getNumberNotify: " + s);
            try {
                int code;
                int number;
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                    code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                    if (code == HTTPCode.E200_OK) {
                        number = jsonObject.getInt("number");
                        mFeedBusiness.setTotalNotify(number);
                        int numberFriend = jsonObject.optInt("number_req_friend");
                        if (numberFriend == 0) {
                            mViewNumberFriendRequest.setVisibility(View.GONE);
                        } else {
                            mViewNumberFriendRequest.setVisibility(View.VISIBLE);
                            mTvwBadgerNumberFriendRequest.setText(TextHelper.getTextNumberNotify(numberFriend));
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }, volleyError -> Log.e(TAG, "error: " + volleyError.toString()));
    }

    private void resetNotifySuggestFriend() {
        new WSOnMedia(mApplication).resetNotify("", Constants.ONMEDIA.TYPE_RESET_NOTIFY.TYPE_SUGGEST, s -> Log.i(TAG, "resetNotifySuggestFriend response: " + s), volleyError -> Log.i(TAG, "err: " + volleyError.toString()));
    }

    private void showButtonNewStories() {
        if (layoutHasNewFeed.getVisibility() == View.GONE) {
            layoutHasNewFeed.setVisibility(View.VISIBLE);
        }
    }

    private void hideButtonNewStories(boolean animation) {
        if (layoutHasNewFeed.getVisibility() == View.VISIBLE) {
            layoutHasNewFeed.setVisibility(View.GONE);
        }
    }

    @Override
    public void onXMPPConnected() {
        if (isPagerVisible) {
            PubSubManager.getInstance(mApplication).processSubscribeConnectionFeeds();
        }
    }

    @Override
    public void onXMPPDisconnected() {
        if (PubSubManager.getInstance(mApplication).isSubscribeConnectionFeeds()) {
            PubSubManager.getInstance(mApplication).unSubscribeConnectionFeeds();
        }
    }

    @Override
    public void onXMPPConnecting() {

    }

    @Override
    public void notifyFeedOnMedia(boolean needNotify, final boolean needToSetSelection) {
        if (!initViewStubDone) return;
        if (!needNotify) {
            if (isOnTopList) {
                Log.i(TAG, "ontoplist, update now");
                updateFeedFromPresence(false);
            } else {
                Log.i(TAG, "show button update");
                mParentActivity.runOnUiThread(this::showButtonNewStories);
            }
        } else {
            mParentActivity.runOnUiThread(() -> {
                Log.i(TAG, "notify nowwwwwwwwwwwwww");
                feedAdapter.setListFeed(mFeedBusiness.getListFeed());
                feedAdapter.notifyDataSetChanged();
                if (needToSetSelection) {
                    scrollSmothToTop();
                }
            });

        }
    }

    private void scrollSmothToTop() {
        mRecyclerView.smoothScrollToPosition(0);
    }

    private void showConfirmRetryGetMetadata(String url) {
        new DialogConfirm(mParentActivity, true).setLabel(null).setMessage(mRes.getString(R.string
                .onmedia_get_metadata_fail)).
                setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok))
                .setEntry(url).
                setPositiveListener(result -> {
                    /*String url = (String) result;
                    getMetaDataFromServer(url);*/
                }).show();
    }

    private void checkAndShowAlertDiscover() {
        mViewNumberFriendRequest.setVisibility(View.GONE);
        /*long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_AB_DISCOVER, 0L);
        if (TimeHelper.checkTimeInDay(lastShow)) {
            mViewNumberFriendRequest.setVisibility(View.GONE);
        } else {
            mViewNumberFriendRequest.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void onDataReady() {
//        if (everShowFragment) {
        mParentActivity.runOnUiThread(() -> {
            Log.i(TAG, "load ondataready");
            if (isPagerVisible) {
                loadDataFeed();
                changeUnreadOnMediaBadge();
            }
        });
//        }
    }

    @Override
    public void OnClickUser(String msisdn, String name) {
        if (enableClickTag) {
            //ko hieu sao cai clickableSpan bi nhay vao 2 lan lien @@
            enableClickTag = false;
            Log.i(TAG, "msisdn: " + msisdn);
            UserInfo userInfo = new UserInfo();
            userInfo.setMsisdn(msisdn);
            userInfo.setName(name);
            userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
            userInfo.setStateMocha(1);
            eventOnMediaHelper.processUserClick(userInfo);
        } else {
            enableClickTag = true;
        }
    }

    @Override
    public void onPostFeed(FeedModelOnMedia feed) {
        if (mFeedBusiness != null && feedAdapter != null && !mFeedBusiness.getListFeed().contains(feed)) {
            Log.i(TAG, "onpostfeed");
            mFeedBusiness.getListFeed().add(0, feed);
            mFeedBusiness.getListPost().add(feed);
            mFeedBusiness.getListFeedProfile().add(0, feed);
            feedAdapter.setListFeed(mFeedBusiness.getListFeed());
            feedAdapter.notifyDataSetChanged();
        }
    }

    private void checkPlayer() {
        if (mApplication.getPlayMusicController() != null &&
                mApplication.getPlayMusicController().isPlaying() &&
                mApplication.getPlayMusicController().isPlayFromFeed()) {
            changeStatePlayList(true);
        } else {
            changeStatePlayList(false);
        }
    }

    private void changeStatePlayList(final boolean isShowPlayList) {
        /*mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isShowPlayList) {
                    mViewAbPlaying.setVisibility(View.VISIBLE);
                } else {
                    mViewAbPlaying.setVisibility(View.GONE);
                }
            }
        });*/
    }

    @Override
    public void onChangeStateRepeat(int state) {

    }

    @Override
    public void onChangeStateNone() {

    }

    @Override
    public void onChangeStateGetData() {

    }

    @Override
    public void onChangeStatePreparing(MediaModel song) {

    }

    @Override
    public void onChangeStatePlaying(MediaModel song) {
        Log.i(TAG, "onChangeStatePlaying");
        checkPlayer();
    }

    @Override
    public void onActionFromUser(MediaModel song) {

    }

    @Override
    public void onCloseMusic() {
        Log.i(TAG, "onCloseMusic");
        checkPlayer();
    }

    @Override
    public void onCallLoadDataFail(String message) {

    }

    @Override
    public void onUpdateSeekBarProgress(int percent, int currentMediaPosition) {

    }

    @Override
    public void onUpdateSeekBarBuffering(int percent) {

    }

    @Override
    public void onChangeStateInfo(int state) {

    }

    @Override
    public void onUpdateData(MediaModel song) {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void onTabReselected() {
        if (mRecyclerView != null) {
            mRecyclerView.smoothScrollToPosition(0);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final OnMediaTabHotEvent event) {
        if (event.reSelected) {
            onTabReselected();
        }
    }

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        if (channel != null && feedAdapter != null) {
            boolean notifyAdapter = false;
            int size = feedAdapter.getItemCount();
            for (int i = 0; i < size; i++) {
                if (feedAdapter.getItem(i) instanceof FeedModelOnMedia) {
                    FeedModelOnMedia item = (FeedModelOnMedia) feedAdapter.getItem(i);
                    FeedContent feedContent = item.getFeedContent();
                    if (feedContent != null && feedContent.getChannel() != null
                            && !TextUtils.isEmpty(feedContent.getChannel().getId())
                            && feedContent.getChannel().getId().equals(channel.getId())) {
                        feedContent.getChannel().setFollow(channel.isFollow());
                        feedContent.getChannel().setNumFollow(channel.getNumFollow());
                        notifyAdapter = true;
                    }
                }
            }
            if (notifyAdapter) feedAdapter.notifyDataSetChanged();
        }
    }

    public static class OnMediaTabHotEvent {
        boolean reSelected;

        public OnMediaTabHotEvent(boolean reSelected) {
            this.reSelected = reSelected;
        }
    }
}

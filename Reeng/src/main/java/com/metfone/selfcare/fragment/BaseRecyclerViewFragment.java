package com.metfone.selfcare.fragment;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.ui.ProgressLoading;

/**
 * Created by toanvk2 on 10/6/2016.
 */

public class BaseRecyclerViewFragment extends Fragment {
    private Context mContext;
    private EmptyViewListener mCallBack;
    private View emptyView;

    protected ProgressLoading mPrbLoading;
    private TextView mTvwNote;
    private ImageView mBtnRetry;
    private TextView mTvwRetry1, mTvwRetry2;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    protected void createView(LayoutInflater inflater, RecyclerView recyclerView, EmptyViewListener callBack) {
        this.mCallBack = callBack;
        emptyView = inflater.inflate(R.layout.view_empty, null);
        mPrbLoading = (ProgressLoading) emptyView.findViewById(R.id.empty_progress);
        mTvwNote = (TextView) emptyView.findViewById(R.id.empty_text);
        mBtnRetry = (ImageView) emptyView.findViewById(R.id.empty_retry_button);
        mTvwRetry1 = (TextView) emptyView.findViewById(R.id.empty_retry_text1);
        mTvwRetry2 = (TextView) emptyView.findViewById(R.id.empty_retry_text2);
        ViewGroup viewGroup = (ViewGroup) recyclerView.getParent();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(emptyView, params);
        //container.addView(emptyView, 0)
        mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    showProgressLoading();
                    mCallBack.onRetryClick();
                }
            }
        });
        //hideEmptyView();
        showProgressLoading();
    }

    protected void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    protected void showEmptyNote(String content) {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.VISIBLE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
        mTvwNote.setText(content);
    }

    protected void showEmptyNote() {
        showEmptyNote(mContext.getResources().getString(R.string.list_empty));
    }

    protected void showProgressLoading() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.VISIBLE);
        mTvwNote.setVisibility(View.GONE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
    }

    protected boolean isShowProgressLoading() {
        if (emptyView == null || mPrbLoading == null) return false;
        return emptyView.getVisibility() == View.VISIBLE && mPrbLoading.getVisibility() == View.VISIBLE;
    }

    protected void showRetryView() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.GONE);
        mBtnRetry.setVisibility(View.VISIBLE);
        mTvwRetry1.setVisibility(View.VISIBLE);
        mTvwRetry2.setVisibility(View.VISIBLE);
    }

    protected void showEmptyOrRetryView() {
        if (NetworkHelper.isConnectInternet(mContext)) {
            showEmptyNote(mContext.getResources().getString(R.string.list_empty));
        } else {
            showRetryView();
        }
    }

    public interface EmptyViewListener {
        void onRetryClick();
    }
}

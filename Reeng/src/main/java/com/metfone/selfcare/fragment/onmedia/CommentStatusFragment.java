package com.metfone.selfcare.fragment.onmedia;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.adapter.onmedia.CommentOnMediaAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.controllers.PlayMusicController;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedAudioViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedChannelViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedImageAlbumViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedImageSingleViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedMovieViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedNewsViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedTotalViewHolder;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedVideoViewHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.FeedOnMediaListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnMediaCommentHolderListener;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.event.CommentEvent;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.event.LikeEvent;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.FilteredArrayAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CommentStatusFragment extends Fragment implements
        PlayMusicController.OnPlayMusicStateChange, ClickListener.IconListener, InitDataListener,
        FeedOnMediaListener, OnMediaCommentHolderListener, TagMocha.OnClickTag, RecyclerClickListener,
        SeekBar.OnSeekBarChangeListener, Response.ErrorListener, OnMediaInterfaceListener.GetListFeedListener {
    private static final String TAG = CommentStatusFragment.class.getSimpleName();
    private static final long TIME_DELAY = 1000;
    ViewGroup container;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private RecyclerView mRecyclerViewComment;
    private CommentOnMediaAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mWrapperAdapter;
    private OnMediaActivityNew mActivity;
    private ApplicationController mApplication;
    private boolean isLoading = false;
    private ArrayList<FeedAction> datas = new ArrayList<>();
    private FeedModelOnMedia mFeed, mFeedInList;
    private TagsCompletionView mEditText;
    private TagOnMediaAdapter adapterUserTag;
    private ImageButton mSend;
    private ImageView mImgABLike;
    private TextView mTitle;
    private boolean isNoMoreComment = false;
    private ProgressLoading mProgressBar;
    private Resources mRes;
    private ContactBusiness mContactBusiness;
    private ReengAccount mAccount;
    private View mLayoutProgress;
    private View view;
    //content not found
    private View llContentNotFound;
    private SeekBar mSeekbar;
    private ImageView mImgPlay;
    private ProgressLoading mProgress;
    private TextView mTvwTime;
    private boolean isPlayMusicKeeng;
    private WSOnMedia rest;
    private FeedBusiness mFeedBusiness;
    //    private AvatarBusiness mAvatarBusiness;
    private String myPhoneNumber, myName;
    //    private TextView mTvwUserCmt;
    private String urlAction;
    private String rowId;
    private int feedType;
    private int feedFrom = -1;
    private boolean showMenuCopy = true;
    private boolean isShowPreview = false;
    private boolean isLoadingFeedPreview = false;
    private ArrayList<UserInfo> userLikesInComment = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private Handler mHandler;
    private boolean isSaveInstance = false;
    private boolean enableClickListLike = true;
    private int gaCategoryId, gaActionId;
    private boolean needGetData = false;
    private TextView mLayoutNoCommnet;
    private boolean enableClickTag = true;
    private SharedPreferences mPref;
    private OnMediaInterfaceListener feedInterface;
    private EventOnMediaHelper eventOnMediaHelper;
    private boolean needGetFromUrl = false;
    private OMFeedTotalViewHolder viewTotal;
    private OMFeedImageSingleViewHolder viewImageSingle;
    private OMFeedNewsViewHolder viewNews;
    private OMFeedImageAlbumViewHolder viewImageAlbum;
    private OMFeedVideoViewHolder viewVideo;
    private OMFeedAudioViewHolder viewAudio;
    private OMFeedMovieViewHolder viewMovie;
    private OMFeedChannelViewHolder viewChannel;
    private boolean isStartTrackingTouch = false;
    private boolean getFeedById;
    private OnMediaHolderListener onMediaHolderListener = new OnMediaHolderListener() {
        @Override
        public void onClickLikeFeed(FeedModelOnMedia feed) {

        }

        @Override
        public void onClickCommentFeed(FeedModelOnMedia feed) {

        }

        @Override
        public void onClickShareFeed(FeedModelOnMedia feed) {

        }

        @Override
        public void onClickUser(UserInfo userInfo) {
            if (userInfo != null) {
                eventOnMediaHelper.processUserClick(userInfo);
            }
            mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_avatar);
        }

        @Override
        public void onClickMediaItem(FeedModelOnMedia feed) {
            eventOnMediaHelper.handleClickMediaItem(feed, gaCategoryId, gaActionId, feedInterface,
                    CommentStatusFragment.this);
        }

        @Override
        public void onClickImageItem(FeedModelOnMedia feed, int positionImage) {
            eventOnMediaHelper.handleClickImage(mFeed, positionImage, Constants.ONMEDIA.PARAM_IMAGE
                    .FEED_FROM_FRIEND_PROFILE);
        }

        @Override
        public void onClickMoreOption(FeedModelOnMedia feed) {

        }

        @Override
        public void onClickButtonTotal(View rowView, FeedModelOnMedia feed) {
            eventOnMediaHelper.handleShareTotal(rowView, feed, gaCategoryId, gaActionId);
        }

        @Override
        public void onLongClickStatus(FeedModelOnMedia feed) {

        }

        @Override
        public void onDeepLinkClick(FeedModelOnMedia feed, String link) {
            eventOnMediaHelper.handleDeeplink(feed, link);
        }

        @Override
        public void onClickSuggestFriend(UserInfo userInfo) {

        }

        @Override
        public void openChannelInfo(FeedModelOnMedia feed) {
            Channel channelVideo = Channel.convertFromChannelOnMedia(feed.getFeedContent().getChannel());
            if (channelVideo == null) return;
            mApplication.getApplicationComponent().providesUtils().openChannelInfo(mActivity, channelVideo);
        }

        @Override
        public void openPlayStore(FeedModelOnMedia feed, String packageName) {
            NavigateActivityHelper.navigateToPlayStore(mActivity, packageName);
        }

        @Override
        public void onSubscribeChannel(FeedModelOnMedia feed, Channel channel) {

        }
    };
    private int currentPosClick;
    private FeedAction currentCommentClick;
    private HashMap<String, FeedAction> listCommentPosting = new HashMap<>();
    /**
     * bộ định thời yêu cầu hiển thi keyboard
     */
    private Runnable showKeyboardRunnable = new Runnable() {
        @Override
        public void run() {
            if (mEditText != null) {
                Activity activity = getActivity();
                if (activity != null) {
                    /*
                     * yêu cầu mEditText Focus
                     */
                    mEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                    mEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                    /*
                     * hiển thị keyboard
                     */
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null)
                        imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        }
    };

    public CommentStatusFragment() {
        isNoMoreComment = false;
    }

    public static CommentStatusFragment newInstance(String urlKey, boolean showMenuCopy) {
        CommentStatusFragment fragment = new CommentStatusFragment();
        Bundle args = new Bundle();
        args.putString(Constants.ONMEDIA.EXTRAS_DATA, urlKey);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        fragment.setArguments(args);
        return fragment;
    }

    public static CommentStatusFragment newInstance(String urlKey, boolean showPreview, String rowid
            , int feedType, String actionType, boolean getDetailUrl, boolean showMenuCopy) {
        CommentStatusFragment fragment = new CommentStatusFragment();
        Bundle args = new Bundle();
        args.putString(Constants.ONMEDIA.EXTRAS_DATA, urlKey);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, showPreview);
        args.putString(Constants.ONMEDIA.EXTRAS_ROW_ID, rowid);
        args.putInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, feedType);
        args.putString(Constants.ONMEDIA.EXTRAS_ACTION, actionType);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, getDetailUrl);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        fragment.setArguments(args);
        return fragment;
    }

    public static CommentStatusFragment newInstance(String urlKey, boolean showPreview, String rowid
            , int feedType, String actionType, FeedContent feedContent, int feedFrom, boolean getDetailUrl
            , boolean showMenuCopy) {
        CommentStatusFragment fragment = new CommentStatusFragment();
        Bundle args = new Bundle();
        args.putString(Constants.ONMEDIA.EXTRAS_DATA, urlKey);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, showPreview);
        args.putString(Constants.ONMEDIA.EXTRAS_ROW_ID, rowid);
        args.putInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, feedType);
        args.putString(Constants.ONMEDIA.EXTRAS_ACTION, actionType);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_CONTENT_DATA, feedContent);
        args.putInt(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, getDetailUrl);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        fragment.setArguments(args);
        return fragment;
    }

    public static CommentStatusFragment newInstance(String urlKey, boolean showPreview, String rowid
            , int feedType, String actionType, FeedModelOnMedia feed, int feedFrom, boolean getDetailUrl
            , boolean showMenuCopy, boolean isPlaySong) {
        CommentStatusFragment fragment = new CommentStatusFragment();
        Bundle args = new Bundle();
        args.putString(Constants.ONMEDIA.EXTRAS_DATA, urlKey);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, showPreview);
        args.putString(Constants.ONMEDIA.EXTRAS_ROW_ID, rowid);
        args.putInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, feedType);
        args.putString(Constants.ONMEDIA.EXTRAS_ACTION, actionType);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feed);
        args.putInt(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, getDetailUrl);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        args.putBoolean(Constants.ONMEDIA.EXTRAS_IS_PLAY_SONG, isPlaySong);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "onAttach fragment comment: " + this.toString() + " | " + Integer.toHexString(System
                .identityHashCode(this)));
        mActivity = (OnMediaActivityNew) activity;
        mApplication = (ApplicationController) mActivity.getApplication();
        rest = new WSOnMedia(mApplication);
        mRes = mApplication.getResources();
        mContactBusiness = mApplication.getContactBusiness();
//        mAvatarBusiness = mApplication.getAvatarBusiness();
//        mImageProfileBusiness = mApplication.getImageProfileBusiness();
//        mMusicBusiness = mApplication.getMusicBusiness();
        mAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        mFeedBusiness = mApplication.getFeedBusiness();
        myPhoneNumber = mAccount.getJidNumber();
        myName = mAccount.getName();
        mHandler = new Handler();
        PlayMusicController.addPlayMusicStateChange(this);
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        try {
            feedInterface = (OnMediaInterfaceListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectionFeedInterface");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mActivity.getToolBarView() != null) {
            mActivity.getToolBarView().setVisibility(View.GONE);
        }
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_comment;
        eventOnMediaHelper = new EventOnMediaHelper(mActivity);
        if (getArguments() != null) {
            urlAction = getArguments().getString(Constants.ONMEDIA.EXTRAS_DATA);
            isShowPreview = getArguments().getBoolean(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW);
            rowId = getArguments().getString(Constants.ONMEDIA.EXTRAS_ROW_ID);
            feedType = getArguments().getInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE);
            String actionType = getArguments().getString(Constants.ONMEDIA.EXTRAS_ACTION);
            needGetFromUrl = getArguments().getBoolean(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL);
            showMenuCopy = getArguments().getBoolean(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY);
            isPlayMusicKeeng = getArguments().getBoolean(Constants.ONMEDIA.EXTRAS_IS_PLAY_SONG);
            Log.i(TAG, "show preview: " + isShowPreview + " rowid: " + rowId + " actionType: " + actionType + " " +
                    "needGetFromUrl: " + needGetFromUrl);
//            maxLength = mRes.getInteger(R.integer.max_length_name_onmedia);
            isNoMoreComment = false;
            if (isShowPreview) {
                mFeed = mFeedBusiness.getFeedFromRowId(rowId);
                if (mFeed != null) {
                    mFeedInList = mFeedBusiness.getFeedModelFromUrl(mFeed.getFeedContent().getUrl());
                } else {
                    mFeed = (FeedModelOnMedia) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA);
                    if (mFeed == null && feedType != Constants.ONMEDIA.FEED_FROM_PUSH) {
                        getFeedById = true;
                        rest.getFeedById(rowId, this, this);
                    }
                }
                if (!TextUtils.isEmpty(actionType)) {
                    reduceTotalNotify();
                    //TODO da vao man hinh notify
                    mPref.edit().putBoolean(Constants.PREFERENCE.PREF_HAD_NOTIFY_TAB_HOT, false).apply();
                }
                if (feedType != Constants.ONMEDIA.FEED_FROM_PUSH)
                    return;
            } else {
                if (feedType == Constants.ONMEDIA.FEED_CONTACT_DETAIL) {
                    mFeed = mFeedBusiness.getFeedProfileProcess();
                } else if (feedType == Constants.ONMEDIA.FEED_PROFILE) {
                    mFeed = mFeedBusiness.getFeedProfileFromUrl(urlAction);
                } else if (feedType == Constants.ONMEDIA.FEED_IMAGE) {
                    feedFrom = getArguments().getInt(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM);
                    getModelFeed(urlAction);
                    if (mFeed == null) {
                        mFeed = new FeedModelOnMedia();
                        FeedContent feedContent = (FeedContent) getArguments().getSerializable(Constants.ONMEDIA
                                .EXTRAS_CONTENT_DATA);
                        if (feedContent == null) {
                            feedContent = new FeedContent();
                            feedContent.setUrl(urlAction);
                        }
                        mFeed.setFeedContent(feedContent);
                    }
                } else if (feedType == Constants.ONMEDIA.FEED_TAB_VIDEO) {
                    mFeed = (FeedModelOnMedia) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA);
                    if (mFeed == null) {
                        mFeed = new FeedModelOnMedia();
                        FeedContent feedContent = new FeedContent();
                        feedContent.setUrl(urlAction);
                        mFeed.setFeedContent(feedContent);
                    }
                } else if (feedType == Constants.ONMEDIA.FEED_TAB_MUSIC) {
                    mFeed = (FeedModelOnMedia) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA);
                    if (mFeed == null) {
                        mFeed = new FeedModelOnMedia();
                        FeedContent feedContent = new FeedContent();
                        feedContent.setUrl(urlAction);
                        mFeed.setFeedContent(feedContent);
                    }
                } else if (feedType == Constants.ONMEDIA.FEED_SONG) {
                    mFeed = mFeedBusiness.getFeedPlayList();
                    if (mFeed != null) urlAction = mFeed.getFeedContent().getUrl();
                } else if (feedType == Constants.ONMEDIA.FEED_TAB_NEWS) {
                    mFeed = (FeedModelOnMedia) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA);
                    if (mFeed == null) {
                        mFeed = new FeedModelOnMedia();
                        FeedContent feedContent = new FeedContent();
                        feedContent.setUrl(urlAction);
                        mFeed.setFeedContent(feedContent);
                    }
                } else {
                    mFeed = mFeedBusiness.getFeedModelFromUrl(urlAction);
                }
            }

            if (mFeed == null && mApplication.isDataReady()) {
                /*mActivity.onBackPressed();
                mActivity.showToast(R.string.e601_error_but_undefined);*/
                if (needGetFromUrl) {
                    getDataFeedFromUrl(urlAction);
                } else {
                    getDataFeedFromRowId(rowId);
                }
                needGetData = true;
            } else {
                needGetData = false;
                if (mFeed != null) {
                    showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(mFeed);
                }
            }
//            mFeed = mFeedBusiness.getFeedModelFromUrl(urlAction);
        } else {
            mActivity.onBackPressed();
            mActivity.showToast(R.string.e601_error_but_undefined);
        }

    }

    private void getDataFeedFromUrl(String urlAction) {
        isLoadingFeedPreview = true;
        rest.getDetailUrl(this, this, urlAction, true);
    }

    private void reduceTotalNotify() {
        rest.resetNotify(rowId, Constants.ONMEDIA.TYPE_RESET_NOTIFY.TYPE_ONMEDIA, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "reduceTotalNotify response: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i(TAG, "err: " + volleyError.toString());
            }
        });
    }

    private void getDataFeedFromRowId(String rowId) {
        if (TextUtils.isEmpty(rowId)) return;
        isLoadingFeedPreview = true;
        rest.getFeedNotify(new OnMediaInterfaceListener.GetListFeedListener() {
            @Override
            public void onGetListFeedDone(RestAllFeedsModel restAllFeedsModel) {
                isLoadingFeedPreview = false;
                needGetData = false;
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
                if (restAllFeedsModel.getCode() == HTTPCode.E200_OK) {
                    if (isFeedEmpty(restAllFeedsModel.getData()))
                        onLoadDataEmpty();
                    else
                        onLoadFeedDone(restAllFeedsModel.getData());
                } else {
                    onLoadDataEmpty();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (volleyError instanceof NoConnectionError) {
                    mActivity.showToast(R.string.no_connectivity_check_again);
                }
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.GONE);
                }
                isLoadingFeedPreview = false;
                Log.e(TAG, volleyError);
            }
        }, rowId);
    }

    private boolean isFeedEmpty(ArrayList<FeedModelOnMedia> result) {
        if (result == null || result.isEmpty()) return true;
        FeedModelOnMedia feedModelOnMedia = result.get(0);
        return feedModelOnMedia == null || feedModelOnMedia.getFeedContent() == null || TextUtils.isEmpty(feedModelOnMedia.getFeedContent().getUrl());
    }

    private void onLoadDataEmpty() {
        llContentNotFound.setVisibility(View.VISIBLE);
        mImgABLike.setVisibility(View.GONE);
        mTitle.setVisibility(View.GONE);
        mEditText.setFocusable(false);
        InputMethodUtils.hideSoftKeyboard(mEditText, mActivity);
    }

    private void onLoadFeedDone(ArrayList<FeedModelOnMedia> result) {
        if (result != null && !result.isEmpty()) {
            result = mFeedBusiness.preProcessListFeedModelListTag(result);
            if (result.get(0) != null) {
                mFeed = result.get(0);
                mFeedInList = mFeedBusiness.getFeedModelFromUrl(mFeed.getFeedContent().getUrl());
                initHeader();
                mRecyclerViewComment.setAdapter(mWrapperAdapter);
                drawPreviewFeed();
                urlAction = mFeed.getFeedContent().getUrl();
                loadComment(urlAction, "");
                showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(mFeed);
            }
        } else {
            Log.i(TAG, "ko tim thay @@");
            onLoadDataEmpty();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        enableClickTag = true;
        Log.i(TAG, "enableClickTag: " + enableClickTag);
        isSaveInstance = false;
        enableClickListLike = true;
        mActivity.getToolBarView().setVisibility(View.GONE);
        ListenerHelper.getInstance().addInitDataListener(this);
        Log.i(TAG, "onresume");
        /*if (mAdapter != null && mAdapter.getItemCount() != 0) {
            mAdapter.notifyDataSetChanged();
            Log.i(TAG, "notify when resume");
        }*/
//        mActivity.getToolBarView().setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(mActivity);
        ListenerHelper.getInstance().removeInitDataListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_onmedia_comment_status, container, false);
        mLayoutInflater = inflater;
        this.container = container;
        ListenerHelper.getInstance().addNotifyFeedOnMediaListener(this);
        findComponentViews(inflater);
        InputMethodUtils.hideKeyboardWhenTouch(mRecyclerViewComment, mActivity);
        if (mHandler == null) {
            mHandler = new Handler();
        }
        /*mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEditText.requestFocus();
                InputMethodUtils.showSoftKeyboard(mActivity, mEditText);
            }
        }, 200);*/
//        TextHelper.getInstant().initBadwordList(mApplication);
        mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_comment);
//        mEditText.requestFocus();

        if (DeviceHelper.isTablet(mActivity)) {
            mEditText.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        } else {
            mEditText.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
//        size = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);

        showKeyboard();
        return view;
    }

    @Override
    public void onDestroyView() {
        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
        super.onDestroyView();
    }

    private void findComponentViews(LayoutInflater inflater) {
//        initAdapterTag(mActivity, mApplication.getContactBusiness().getListNumbers());
//        listView = (ListView) view.findViewById(R.id.list_comment_onmedia);
        mRecyclerViewComment = view.findViewById(R.id.list_comment_onmedia);
        mRecyclerViewComment.setLayoutManager(new LinearLayoutManager(mApplication));
        initDataView();
        mEditText = view.findViewById(R.id.person_chat_detail_input_text);
        ArrayList<PhoneNumber> listPhone = mApplication.getContactBusiness().getListNumberUseMocha();
        if (listPhone == null) listPhone = new ArrayList<>();
        adapterUserTag = new TagOnMediaAdapter(mApplication, listPhone, mEditText);
        mEditText.setAdapter(adapterUserTag);

        mEditText.setThreshold(0);
        adapterUserTag.setListener(new FilteredArrayAdapter.OnChangeItem() {
            @Override
            public void onChangeItem(int count) {
                Log.i(TAG, "onChangeItem: " + count);
                if (count > 2) {
                    int height = mRes.getDimensionPixelOffset(R.dimen.max_height_drop_down_tag);
                    mEditText.setDropDownHeight(height);
                } else
                    mEditText.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        mSend = view.findViewById(R.id.person_chat_detail_send_reeng_text);
        mSend.setVisibility(View.VISIBLE);
        mTitle = view.findViewById(R.id.ab_title);
        mImgABLike = view.findViewById(R.id.img_like_content_ab);
        mImgABLike.setVisibility(View.VISIBLE);
        ImageView mimgLike = view.findViewById(R.id.img_like_content);
        mimgLike.setVisibility(View.GONE);
        ImageView mOpenListLike = view.findViewById(R.id.img_over_flow);
        mOpenListLike.setVisibility(View.GONE);
        mProgressBar = view.findViewById(R.id.progress_comment);
        mProgressBar.setVisibility(View.VISIBLE);
        mEditText.setHint(R.string.onmedia_hint_enter_comment);

        ImageView mImgBack = view.findViewById(R.id.ab_close);

        View mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLayoutProgress = mFooterView.findViewById(R.id.load_more_layout);
        mLayoutNoCommnet = mFooterView.findViewById(R.id.layout_no_comment);
        mLayoutNoCommnet.setVisibility(View.GONE);

        llContentNotFound = view.findViewById(R.id.ll_content_not_found);
        llContentNotFound.setVisibility(View.GONE);
        View backToPrevious = view.findViewById(R.id.tvw_back_to_previous);
        backToPrevious.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        /*TextView mTvwAvatar = (TextView) view.findViewById(R.id.tvw_onmedia_avatar);
        CircleImageView mImgAvatar = (CircleImageView) view.findViewById(R.id.img_onmedia_avatar);
        mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatar, mAccount, null);*/

        RelativeLayout mLayoutMedia = view.findViewById(R.id.layout_media);
        mImgPlay = view.findViewById(R.id.media_play);
        mTvwTime = view.findViewById(R.id.media_duration);
        mProgress = view.findViewById(R.id.media_loading);
        mSeekbar = view.findViewById(R.id.media_buffer);
        mSeekbar.setPadding(0, 0, 0, 0);
        ImageView mImgTurnOff = view.findViewById(R.id.media_close);
        TextView mTvwName = view.findViewById(R.id.media_name);
        CircleImageView mImgSong = view.findViewById(R.id.media_thumb);

        if (mFeed != null && mFeed.getFeedContent() != null && isPlayMusicKeeng) {
            if (OnMediaHelper.isFeedViewAudio(mFeed)) {
                mLayoutMedia.setVisibility(View.VISIBLE);
                mApplication.getMusicBusiness().setListenAlbum(FeedContent.ITEM_TYPE_ALBUM.equals(mFeed.getFeedContent().getItemType()));
                String songTitle, songDesc;
                if (mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_ALBUM)) {
                    songTitle = mRes.getString(R.string.browser_title) + ": " + mFeed.getFeedContent().getItemName();
                    mTvwName.setText(songTitle);
                } else {
                    songTitle = mFeed.getFeedContent().getItemName();
                    songDesc = mFeed.getFeedContent().getSinger();
                    mTvwName.setText(TextHelper.fromHtml(String.format(mRes.getString(R.string.media_title), songTitle,
                            songDesc)));
                }
                mTvwName.setSelected(true);
                mApplication.getAvatarBusiness().setSongAvatar(mImgSong, mFeed.getFeedContent().getImageUrl());
                mApplication.getPlayMusicController().setStateRepeat(Constants.PLAY_MUSIC.REPEAT_All);
                mApplication.getPlayMusicController().setStateShuffle(Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                setStateMediaPlayer(mApplication.getPlayMusicController().getStatePlaying());
            } else {
                mLayoutMedia.setVisibility(View.GONE);
            }
        } else
            mLayoutMedia.setVisibility(View.GONE);

        mSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    mActivity.showDialogLogin();
                } else {
                    sendCommentStatus();
                    mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_comment);
                }
            }
        });
        mImgABLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    mActivity.showDialogLogin();
                } else {
                    if (mFeed != null) {
                        onClickLikeFeed(mFeed);
                    }
                    mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
                }
            }
        });

        mOpenListLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickOverflowMenu();
            }
        });

        mTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToListUserLike();
            }
        });
        initHeader();
        mWrapperAdapter.addFooterView(mFooterView);

        mImgBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });
        mTitle.setText(mRes.getString(R.string.onmedia_fisrt_like));

        mSeekbar.setOnSeekBarChangeListener(this);
        mImgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mApplication.getPlayMusicController().toggleMusic();
            }
        });
        mImgTurnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mApplication.getPlayMusicController().closeMusic();
                mActivity.onBackPressed();
            }
        });
        mSend.setColorFilter(ContextCompat.getColor(mActivity, R.color.gray));
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString())) {
                    mSend.setColorFilter(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                } else {
                    mSend.setColorFilter(ContextCompat.getColor(mActivity, R.color.gray));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        loadComment(urlAction, "");

    }

    public void setStateMediaPlayer(int stateMediaPlayer) {
        switch (stateMediaPlayer) {
            case Constants.PLAY_MUSIC.PLAYING_RETRIEVING:
                mProgress.setVisibility(View.VISIBLE);
                mImgPlay.setVisibility(View.INVISIBLE);
                mSeekbar.setProgress(0);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PLAYING:
                mProgress.setVisibility(View.INVISIBLE);
                mImgPlay.setVisibility(View.VISIBLE);
                mImgPlay.setImageResource(R.drawable.ic_voicemail_pause);
                break;
            case Constants.PLAY_MUSIC.PLAYING_PAUSED:
                mProgress.setVisibility(View.INVISIBLE);
                mImgPlay.setVisibility(View.VISIBLE);
                mImgPlay.setImageResource(R.drawable.ic_voicemail_play);
                break;
            default:
                mProgress.setVisibility(View.VISIBLE);
                mImgPlay.setVisibility(View.INVISIBLE);
                mSeekbar.setProgress(0);
                break;
        }
    }

    private void onClickOverflowMenu() {

        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        if (showMenuCopy) {
            ItemContextMenu copyItem = new ItemContextMenu(mRes.getString(R.string.web_pop_copylink), R
                    .drawable.ic_bottom_copy_link, null, Constants.MENU.COPY);
            listMenu.add(copyItem);
        }
        ItemContextMenu listLike = new ItemContextMenu(mRes.getString(R.string.onmedia_title_user_likes), R
                .drawable.ic_bottom_list_like,
                null, Constants.MENU.MENU_LIST_LIKE);
        listMenu.add(listLike);
        new BottomSheetMenu(mActivity, true)
                .setListItem(listMenu)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onItemClick(int itemId, Object entry) {
                        switch (itemId) {

                            case Constants.MENU.COPY:
                                TextHelper.copyToClipboard(mApplication, urlAction);
                                mActivity.showToast(R.string.copy_to_clipboard);
                                mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy);
                                break;

                            case Constants.MENU.MENU_LIST_LIKE:
                                navigateToListUserLike();
                                break;

                            default:
                                break;
                        }
                    }
                }).show();

    }

    private void showPopupContextMenuLongClick(FeedAction feedAction) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(mRes.getString(R.string.onmedia_copy_text), -1,
                feedAction, Constants.MENU.MENU_COPY_TEXT);
        listMenu.add(copyItem);

        if (feedAction.getUserInfo() != null && !feedAction.getUserInfo().getMsisdn().equals(myPhoneNumber)
                && feedAction.getUserInfo().isUserMocha()) {
            ItemContextMenu replyItem = new ItemContextMenu(mRes.getString(R.string.reply), -1,
                    feedAction, Constants.MENU.MENU_REPLY);
            listMenu.add(replyItem);
        }
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mActivity,
                    null, listMenu, this);
        }
    }

    private void initHeader() {
        if (mFeed != null) {
            FeedContent feedContent = mFeed.getFeedContent();
            if (feedContent != null) {
                String feedType = feedContent.getItemType();
                String itemSubType = feedContent.getItemSubType();
                View mHeaderPreview;
                if (TextUtils.isEmpty(feedType)) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_news, container, false);
                    viewNews = new OMFeedNewsViewHolder(mHeaderPreview, mApplication);
                    viewNews.initCommonView(mHeaderPreview);
                    viewNews.initViewMediaHolder(mHeaderPreview);
//                return TYPE_NEWS;
                } else if (OnMediaHelper.isFeedViewAudio(feedContent)) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_audio, container, false);
                    viewAudio = new OMFeedAudioViewHolder(mHeaderPreview, mApplication, mActivity);
                    viewAudio.initCommonView(mHeaderPreview);
                    viewAudio.initViewMediaHolder(mHeaderPreview);
                } else if (OnMediaHelper.isFeedViewMovie(feedContent)) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_movie, container, false);
                    viewMovie = new OMFeedMovieViewHolder(mHeaderPreview, mApplication, mActivity);
                    viewMovie.initCommonView(mHeaderPreview);
                } else if (OnMediaHelper.isFeedViewVideo(mFeed)) {
//                return TYPE_VIDEO;
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_video, container, false);
                    viewVideo = new OMFeedVideoViewHolder(mHeaderPreview, mApplication);
                    viewVideo.initCommonView(mHeaderPreview);
                    viewVideo.initViewMediaHolder(mHeaderPreview);
                } else if (feedType.equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR) ||
                        feedType.equals(FeedContent.ITEM_TYPE_PROFILE_COVER)) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_image_single, container,
                            false);
                    viewImageSingle = new OMFeedImageSingleViewHolder(mHeaderPreview, mApplication);
                    viewImageSingle.initCommonView(mHeaderPreview);
                } else if (feedType.equals(FeedContent.ITEM_TYPE_PROFILE_STATUS)
                        || (FeedContent.ITEM_TYPE_SOCIAL.equals(feedType)
                        && FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(itemSubType))) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_news, container, false);
                    viewNews = new OMFeedNewsViewHolder(mHeaderPreview, mApplication);
                    viewNews.initCommonView(mHeaderPreview);
                    viewNews.initViewMediaHolder(mHeaderPreview);
                } else if (feedType.equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM)
                        || (FeedContent.ITEM_TYPE_SOCIAL.equals(feedType)
                        && FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(itemSubType))) {
                    if (feedContent.getListImage() != null &&
                            feedContent.getListImage().size() != 0) {
                        if (feedContent.getListImage().size() == 1) {
                            mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_image_single,
                                    container, false);
                            viewImageSingle = new OMFeedImageSingleViewHolder(mHeaderPreview, mApplication);
                            viewImageSingle.initCommonView(mHeaderPreview);
                        } else {
                            mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_image_album,
                                    container, false);
                            viewImageAlbum = new OMFeedImageAlbumViewHolder(mHeaderPreview, mApplication);
                            viewImageAlbum.initCommonView(mHeaderPreview);
                        }
                    } else {
                        mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_news, container, false);
                        viewNews = new OMFeedNewsViewHolder(mHeaderPreview, mApplication);
                        viewNews.initCommonView(mHeaderPreview);
                        viewNews.initViewMediaHolder(mHeaderPreview);
                    }
                } else if (feedType.equals(FeedContent.ITEM_TYPE_TOTAL)) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_total, container, false);
                    viewTotal = new OMFeedTotalViewHolder(mHeaderPreview, mApplication);
                } else if (OnMediaHelper.isFeedViewChannel(feedContent)) {
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_channel, container, false);
                    viewChannel = new OMFeedChannelViewHolder(mHeaderPreview, mApplication, mActivity);
                    viewChannel.initCommonView(mHeaderPreview);
                } else {
//                return TYPE_NEWS;
                    mHeaderPreview = mLayoutInflater.inflate(R.layout.holder_onmedia_feeds_news, container, false);
                    viewNews = new OMFeedNewsViewHolder(mHeaderPreview, mApplication);
                    viewNews.initCommonView(mHeaderPreview);
                    viewNews.initViewMediaHolder(mHeaderPreview);
                }
//                initHeaderPreview(mHeaderPreview);
//                View mViewDivider = mHeaderPreview.findViewById(R.id.layout_divider_comment);
                View mViewFakeDivider = mHeaderPreview.findViewById(R.id.layout_fake_divider);
//                mTvwUserCmt = (TextView) mHeaderPreview.findViewById(R.id.tvw_user_comment);
                View mLayoutAction = mHeaderPreview.findViewById(R.id.item_share_like_com);
//                ImageView mImgMoreOption = mHeaderPreview.findViewById(R.id.img_more_option);
                /*if (mImgMoreOption != null) {
                    mImgMoreOption.setVisibility(View.GONE);
                }*/
                /*if (OnMediaHelper.isFeedViewImage(mFeed) || OnMediaHelper.isFeedViewNew(mFeed))
                    mViewDivider.setVisibility(View.GONE);
                else
                    mViewDivider.setVisibility(View.VISIBLE);*/
                mViewFakeDivider.setVisibility(View.GONE);
//                mTvwUserCmt.setVisibility(View.GONE);
                mLayoutAction.setVisibility(View.GONE);

                if (isShowPreview) {
                    drawPreviewFeed();
                    mWrapperAdapter.addHeaderView(mHeaderPreview);
                } else {
                    if (mFeed.getIsLike() == 1) {
                        mImgABLike.setImageResource(R.drawable.ic_onmedia_like_press);
                    } else {
                        mImgABLike.setImageResource(R.drawable.ic_onmedia_like);
                    }
                }
//                mWrapperAdapter.addHeaderView(mHeaderNoComment);
            }
        }
    }

    private void drawPreviewFeed() {
        if (mFeed != null) {
            if (viewNews != null) {
                viewNews.setOnMediaHolderListener(onMediaHolderListener);
                viewNews.setElement(mFeed);
                viewNews.setHideTopAction(true);
                viewNews.setOnClickTag(this);
            } else if (viewAudio != null) {
                viewAudio.setOnMediaHolderListener(onMediaHolderListener);
                viewAudio.setElement(mFeed);
                viewAudio.setHideTopAction(true);
                viewAudio.setOnClickTag(this);
            } else if (viewMovie != null) {
                viewMovie.setOnMediaHolderListener(onMediaHolderListener);
                viewMovie.setElement(mFeed);
                viewMovie.setHideTopAction(true);
                viewMovie.setOnClickTag(this);
            } else if (viewVideo != null) {
                viewVideo.setOnMediaHolderListener(onMediaHolderListener);
                viewVideo.setElement(mFeed);
                viewVideo.setHideTopAction(true);
                viewVideo.setOnClickTag(this);
            } else if (viewTotal != null) {
                viewTotal.setOnMediaHolderListener(onMediaHolderListener);
                viewTotal.setElement(mFeed);
                viewTotal.setHideTopAction(true);
                viewTotal.setOnClickTag(this);
            } else if (viewImageSingle != null) {
                viewImageSingle.setOnMediaHolderListener(onMediaHolderListener);
                viewImageSingle.setElement(mFeed);
                viewImageSingle.setHideTopAction(true);
                viewImageSingle.setOnClickTag(this);
            } else if (viewImageAlbum != null) {
                viewImageAlbum.setOnMediaHolderListener(onMediaHolderListener);
                viewImageAlbum.setElement(mFeed);
                viewImageAlbum.setHideTopAction(true);
                viewImageAlbum.setOnClickTag(this);
            } else if (viewChannel != null) {
                viewChannel.setOnMediaHolderListener(onMediaHolderListener);
                viewChannel.setElement(mFeed);
                viewChannel.setHideTopAction(true);
                viewChannel.setOnClickTag(this);
            }
            if (mFeed.getIsLike() == 1) {
                mImgABLike.setImageResource(R.drawable.ic_onmedia_like_press);
            } else {
                mImgABLike.setImageResource(R.drawable.ic_onmedia_like);
            }
        } else {
            mLayoutNoCommnet.setVisibility(View.GONE);
            mLayoutProgress.setVisibility(View.GONE);
            mImgABLike.setImageResource(R.drawable.ic_onmedia_like);
        }
    }

    private void navigateToListUserLike() {
        if (mFeed == null) return;
        if (!enableClickListLike) {
            return;
        }
        if (mFeed.getFeedContent().getCountLike() != 0) {
            NavigateActivityHelper.navigateToOnMediaLikeOrShare(mActivity, urlAction, true);
//            mActivity.displayListLikeFragment(urlAction);
            enableClickListLike = false;
        } else {
            mActivity.showToast(R.string.onmedia_fisrt_like);
        }
        mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_like);
    }

    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mActivity)) {
            mActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        if (feed == null || feed.getFeedContent() == null) {
            Log.e(TAG, "null feed || feedcontent");
            mActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }

        final boolean isLiked = feed.getIsLike() == 1;
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);

        Video video = Video.convertFeedContentToVideo(mFeed.getFeedContent());
        video.setLike(mFeed.getIsLike() == 1);
        FeedModelOnMedia.ActionFrom actionFrom = feedType == Constants.ONMEDIA.FEED_TAB_VIDEO ?
                FeedModelOnMedia.ActionFrom.mochavideo : FeedModelOnMedia.ActionFrom.onmedia;
        mApplication.getListenerUtils().notifyVideoLikeChangedData(video);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), action, "", feed.getBase64RowId(),
                "", actionFrom,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionLike: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code != HTTPCode.E200_OK) {
                                    setLikeFeed(feed, isLiked);
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                                } else {
                                    //islike----unlike, !islike----like
                                    if (!isLiked) {
                                        userLikesInComment.add(0, new UserInfo(myPhoneNumber, myName));
                                        if (userLikesInComment.size() > 2) {
                                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                                            listTmp.add(0, userLikesInComment.get(0));
                                            listTmp.add(1, userLikesInComment.get(1));
                                            userLikesInComment = listTmp;
                                        }
                                    } else {
                                        if (userLikesInComment.size() == 0) {
                                            Log.i(TAG, "Loi roi, size phai khac 0");
                                        } else if (userLikesInComment.size() == 1) {
                                            userLikesInComment.clear();
                                        } else if (userLikesInComment.size() == 2) {
                                            int indexToDelete = -1;
                                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                                if (userLikesInComment.get(i).getMsisdn().equals(myPhoneNumber)) {
                                                    indexToDelete = i;
                                                    break;
                                                }
                                            }
                                            if (indexToDelete != -1) {
                                                userLikesInComment.remove(indexToDelete);
                                            }
                                        } else {
                                            Log.i(TAG, "Loi roi, size phai < 3");
                                        }
                                    }
                                    String titleLike = OnMediaHelper.getTitleLikeInHtml(mFeed.getFeedContent()
                                            .getCountLike(), userLikesInComment, mApplication, null);
                                    mTitle.setText(TextHelper.fromHtml(titleLike));

                                    EventHelper.getDefault().postSticky(new LikeEvent(!isLiked));
                                }
                            } else {
                                setLikeFeed(feed, isLiked);
                                mActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            setLikeFeed(feed, isLiked);
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        setLikeFeed(feed, isLiked);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        //neu la like thi tang like them 1, neu la unlike thi tru 1.

        int delta;
        if (isLike) {
            delta = 1;
            feed.setIsLike(1);
//            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like_press);
            mImgABLike.setImageResource(R.drawable.ic_onmedia_like_press);
        } else {
            delta = -1;
            feed.setIsLike(0);
//            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like);
            mImgABLike.setImageResource(R.drawable.ic_onmedia_like);
        }
        long countLike = feed.getFeedContent().getCountLike();
        feed.getFeedContent().setCountLike(countLike + delta);
        if (mFeedInList != null) {
            mFeedInList.getFeedContent().setCountLike(countLike + delta);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onsaveinstancestate");
        isSaveInstance = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!isSaveInstance) {
            Log.i(TAG, "onstop: notify");
            mFeedBusiness.notifyNewFeed(true, false);
        }
        if (mFeed != null) {
            EventBus.getDefault().postSticky(mFeed);
        }
    }

    private void initDataView() {
        datas.clear();
        mAdapter = new CommentOnMediaAdapter(mApplication, datas, this, this, this);
        mWrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
//        adapter = new CommentStatusAdapter(mActivity, datas, this, this);
        if (mFeed != null) {
            mRecyclerViewComment.setAdapter(mWrapperAdapter);
        }

        mRecyclerViewComment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewComment.getLayoutManager();
                    if (layoutManager == null) {
                        Log.e(TAG, "null layoutManager");
                        return;
                    }
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= (totalItemCount - 3)) && !isLoading
                            && !isLoadingFeedPreview && !isNoMoreComment) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }
            }
        });
    }

    private void onLoadMore() {
        String lastRowId = getLastRowId();
        if (!isNoMoreComment) {
            Log.i(TAG, "loaddata onLoadMore " + lastRowId);
            loadComment(urlAction, lastRowId);
        } else {
            Log.i(TAG, "loaddata onLoadMore nomorefeed");
            mLayoutProgress.setVisibility(View.GONE);
        }
    }

    private void loadComment(String urlAction, final String lastRowId) {
        if (isLoadingFeedPreview) {
            return;
        }
        if (mFeed == null /*|| (mFeed.getFeedContent().getCountComment() == 0 &&
                mFeed.getFeedContent().getCountLike() == 0)*/) {
            mProgressBar.setVisibility(View.GONE);
            mLayoutProgress.setVisibility(View.GONE);
            mLayoutNoCommnet.setVisibility(View.VISIBLE);
            return;
        }
        if (mActivity != null) {
            if (!NetworkHelper.isConnectInternet(mActivity)) {
                mActivity.showToast(R.string.no_connectivity_check_again);
                mProgressBar.setVisibility(View.GONE);
                mLayoutProgress.setVisibility(View.GONE);
                mLayoutNoCommnet.setVisibility(View.VISIBLE);
                return;
            }
            isLoading = true;
            isNoMoreComment = false;
            mLayoutProgress.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(lastRowId)) {
                mProgressBar.setVisibility(View.GONE);
            }
            Log.i(TAG, "loadComment fragment comment: " + this.toString() + " | " + Integer.toHexString(System
                    .identityHashCode(this)));
            rest.getListComment(urlAction, lastRowId, new OnMediaInterfaceListener.GetListComment() {
                @Override
                public void onGetListCommentSuccess(ArrayList<FeedAction> listComment, ArrayList<UserInfo>
                        listUserLike, FeedAction feedAction, long timeServer) {
                    isLoading = false;
                    mProgressBar.setVisibility(View.GONE);
                    mLayoutProgress.setVisibility(View.GONE);
                    mFeedBusiness.setDeltaTimeServer(System.currentTimeMillis() - timeServer);
                    loadCommentDone(listComment, lastRowId);
                    if (TextUtils.isEmpty(lastRowId)) {
//                        listView.setSelection(datas.size() - 1);
                        userLikesInComment = listUserLike;
                        String titleLike = OnMediaHelper.getTitleLikeInHtml(mFeed.getFeedContent().getCountLike(),
                                userLikesInComment, mApplication, null);
                        mTitle.setText(TextHelper.fromHtml(titleLike));
                        if (listComment != null && !listComment.isEmpty()) {
                            mLayoutNoCommnet.setVisibility(View.GONE);
                        } else {
                            mLayoutNoCommnet.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mLayoutNoCommnet.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onGetListCommentError(int code, String des) {

                }
            }, Integer.toHexString(System.identityHashCode(this)), false);
        }
    }

    private void loadCommentDone(ArrayList<FeedAction> result, String lastRowId) {
        mLayoutProgress.setVisibility(View.GONE);
        if (result != null && !result.isEmpty()) {
            isNoMoreComment = false;
            //xu ly tag
            result = mFeedBusiness.preProcessFeedActionListTag(result);

            if (TextUtils.isEmpty(lastRowId)) {
                datas = result;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.setDatas(datas);
                        mAdapter.notifyDataSetChanged();
                    }
                });

            } else {
                datas.addAll(result);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.setDatas(datas);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
            Log.i(TAG, "load data done: " + datas.size());
        } else {
            Log.i(TAG, "no more feed");
            isNoMoreComment = true;
        }
    }

    private void addStatus(String content, ArrayList<TagMocha> listTag, String idCmtLocal) {
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        FeedAction commentStatusFeed = new FeedAction(msisdn, content, System.currentTimeMillis(), System
                .currentTimeMillis());
        commentStatusFeed.setIdCmtLocal(idCmtLocal);
        if (!listTag.isEmpty()) {
            commentStatusFeed.setListTag(listTag);
        }
        datas.add(0, commentStatusFeed);
        addComment(commentStatusFeed);
        if (mFeed == null || mFeed.getFeedContent() == null) return;

        long countComment = mFeed.getFeedContent().getCountComment();

        /*if (feedType == Constants.ONMEDIA.FEED_PROFILE) {
            addAllStatusOnProfile(content);
        } else {*/
        mFeed.getFeedContent().setCountComment(countComment + 1);
        if (mFeedInList != null) {
            mFeedInList.getFeedContent().setCountComment(countComment + 1);
        }
//        }
        mAdapter.notifyDataSetChanged();
        mRecyclerViewComment.post(new Runnable() {
            @Override
            public void run() {
                if (isShowPreview) {
                    ((LinearLayoutManager) mRecyclerViewComment.getLayoutManager())
                            .scrollToPositionWithOffset(1, 0);
                } else {
                    ((LinearLayoutManager) mRecyclerViewComment.getLayoutManager())
                            .scrollToPositionWithOffset(0, 0);
                }
            }
        });

//        mTvwNumberStatus.setText(String.valueOf(countComment + 1));
    }

    private void sendCommentStatus() {
        String text = mEditText.getText().toString();
        if (text.length() == 0 || TextUtils.isEmpty(text.trim())) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(mActivity)) {
            mActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }

        Log.i(TAG, "send text: " + mEditText.getText().toString());
        final ArrayList<TagMocha> mListTagFake =
                mFeedBusiness.getListTagFromListPhoneNumber(mEditText.getUserInfo());
        String textTag = mFeedBusiness.getTextTag(mListTagFake);
        String messageContent = TextHelper.trimTextOnMedia(mEditText.getTextTag());
        if (checkSpamComment(messageContent)) {
            mActivity.showToast(R.string.comment_onmedia_fail);
            return;
        }
        mActivity.hideKeyboard();
        mEditText.resetObject();
        Log.i(TAG, "text after getRaw: " + messageContent);
        String idCmtLocal = EncryptUtil.encryptMD5(mApplication.getReengAccountBusiness().getJidNumber()
                + System.currentTimeMillis());
        addStatus(messageContent, mListTagFake, idCmtLocal);
        mLayoutNoCommnet.setVisibility(View.GONE);
//        String content = new Gson().toJson(mFeed.getFeedContent());

        if (mFeed != null && mFeed.getFeedContent() != null) {
            Video video = Video.convertFeedContentToVideo(mFeed.getFeedContent());
            video.setTotalComment(mFeed.getFeedContent().getCountComment());

            mApplication.getListenerUtils().notifyVideoCommentChangedData(video);
        }

        FeedContent feedContent = null;
        try {
            feedContent = mFeed.getFeedContent().clone();
        } catch (CloneNotSupportedException e) {
            Log.e(TAG, "CloneNotSupportedException", e);
        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointerException", e);
        }
        if (feedContent == null) {
            mActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        feedContent.setContentAction(FeedContent.CONTENT_ACTION);
        feedContent.setIdCmtLocal(idCmtLocal);
        FeedModelOnMedia.ActionFrom actionFrom = feedType == Constants.ONMEDIA.FEED_TAB_VIDEO ?
                FeedModelOnMedia.ActionFrom.mochavideo : FeedModelOnMedia.ActionFrom.onmedia;
        rest.logAppV6(feedContent.getUrl(), "", feedContent, FeedModelOnMedia.ActionLogApp.COMMENT,
                messageContent, mFeed.getBase64RowId(), textTag, actionFrom,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionComment: onresponse: " + response);
                        mProgressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    String base64RowId = jsonObject.optString("base64RowID");
                                    String idCmtLocal = jsonObject.optString("id_cmt_local");
                                    boolean findComment = updateComment(idCmtLocal, base64RowId);
                                    if (findComment) {
                                        Log.i(TAG, "find comment success");
                                        mAdapter.notifyDataSetChanged();
                                    } else {
                                        Log.e(TAG, "cant find comment");
                                    }
                                    EventHelper.getDefault().postSticky(new CommentEvent());
                                } else {
                                    mActivity.showToast(R.string.comment_not_success);
                                }
                            } else {
                                mActivity.showToast(R.string.comment_not_success);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            mActivity.showToast(R.string.comment_not_success);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Response error" + volleyError.getMessage());
                        mProgressBar.setVisibility(View.GONE);
                        mActivity.showToast(R.string.comment_not_success);
                    }
                });
    }

    private boolean checkSpamComment(String content) {
        if (datas.size() < 1) return false;
        FeedAction firstComment = datas.get(0);
        String firstMsisdn = firstComment.getUserInfo().getMsisdn();
        return myPhoneNumber.equals(firstMsisdn) && firstComment.getComment().equals(content);
    }

    private String getLastRowId() {
        if (datas == null || datas.isEmpty()) {
            return "";
        } else {
            for (int i = datas.size() - 1; i >= 0; i--) {
                FeedAction cmt = datas.get(i);
                if (!cmt.isReplyCmt()) {
                    return cmt.getBase64RowId();
                }
            }
            isNoMoreComment = true;
            return "";
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        PlayMusicController.removePlayMusicStateChange(this);
        mHandler.removeCallbacksAndMessages(null);
        Log.i(TAG, "onDetach fragment comment: " + this.toString() + " | " + Integer.toHexString(System
                .identityHashCode(this)));
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(Integer.toHexString(System.identityHashCode
                (this)));
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_EXIT:
                FeedModelOnMedia item = (FeedModelOnMedia) entry;
                eventOnMediaHelper.handleClickPopupExitListenTogether(item, feedInterface);
                break;
            case Constants.MENU.MENU_SHARE_LINK:
                FeedModelOnMedia feed = (FeedModelOnMedia) entry;
                handleShareLink(feed);
                break;

            case Constants.MENU.MENU_WRITE_STATUS:
                FeedModelOnMedia feedWrite = (FeedModelOnMedia) entry;
                handleWriteStatus(feedWrite);
//                mActivity.navigateToWriteStatus(feedWrite, false);
                break;

            case Constants.MENU.MENU_COPY_TEXT:
                FeedAction commentCopyText = (FeedAction) entry;
                String textCopy = mFeedBusiness.getTextTagCopy(commentCopyText.getComment(), commentCopyText
                        .getListTag());
                TextHelper.copyToClipboard(mActivity, textCopy);
                mActivity.showToast(R.string.copy_to_clipboard);
                mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy_text);
                break;

            case Constants.MENU.MENU_REPLY:
                FeedAction commentReply = (FeedAction) entry;
                handleReplyComment(commentReply);
                break;

            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                FeedModelOnMedia feedShareFb = (FeedModelOnMedia) entry;
                OnMediaHelper.shareFacebookOnMedia(feedShareFb, mActivity);
                break;

            default:
                break;
        }
    }

    private void handleReplyComment(final FeedAction commentReply) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UserInfo userInfo = commentReply.getUserInfo();
                PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(userInfo.getMsisdn());
                if (phoneNumber == null) {
                    phoneNumber = new PhoneNumber();
                    phoneNumber.setJidNumber(userInfo.getMsisdn());
                    phoneNumber.setName(userInfo.getName());
                    phoneNumber.setNameUnicode(userInfo.getName());
                    phoneNumber.setNickName(userInfo.getName());
                }
                mEditText.resetObject();
                mEditText.setSelectedObject(phoneNumber);
                mEditText.addObject(phoneNumber);
            }
        });
    }

    private void handleWriteStatus(FeedModelOnMedia feedWrite) {
        eventOnMediaHelper.navigateToPostOnMedia(mActivity,
                feedWrite.getFeedContent(), "", feedWrite.getBase64RowId(), false,
                FeedModelOnMedia.ActionFrom.onmedia);
    }

    public void handleShareLink(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mActivity)) {
            mActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        setShareFeed(feed, 1);
        FeedModelOnMedia.ActionFrom actionFrom = feedType == Constants.ONMEDIA.FEED_TAB_VIDEO ?
                FeedModelOnMedia.ActionFrom.mochavideo : FeedModelOnMedia.ActionFrom.onmedia;
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE,
                "",
                feed.getBase64RowId(), "", actionFrom,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionShare: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    mActivity.showToast(R.string.onmedia_share_success);
                                } else {
                                    setShareFeed(feed, -1);
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                                }
                            } else {
                                setShareFeed(feed, -1);
                                mActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            setShareFeed(feed, -1);
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        setShareFeed(feed, -1);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void setShareFeed(FeedModelOnMedia feed, int delta) {
        if (!isShowPreview) {
            return;
        }
        long countShare = feed.getFeedContent().getCountShare();
//        long countComment = feed.getFeedContent().getCountComment();
        feed.getFeedContent().setCountShare(countShare + delta);
        if (mFeedInList != null) {
            mFeedInList.getFeedContent().setCountShare(countShare + delta);
        }
//        feed.getFeedContent().setCountComment(countComment + delta);
        if (delta == 1) {
            feed.setIsShare(1);
        } else {
            feed.setIsShare(0);
        }
        /*if (mFeed.getIsShare() == 1) {
            mImgShare.setBackgroundResource(R.drawable.ic_onmedia_share_press);
        } else {
            mImgShare.setBackgroundResource(R.drawable.ic_onmedia_share);
        }
        mTvwNumberShare.setText(String.valueOf(countShare + delta));*/

//        feedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAvatarClick(UserInfo userInfo) {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin())
            mActivity.showDialogLogin();
        else {
            if (userInfo != null) {
                eventOnMediaHelper
                        .processUserClick(userInfo);
            }
        }
    }

    @Override
    public void onLongClickItem(FeedAction feedAction) {
        if (feedAction != null) {
            showPopupContextMenuLongClick(feedAction);
        }
    }

    @Override
    public void onClickReply(FeedAction feedAction, int pos, boolean needShowKeyboard) {
        Log.i(TAG, "onClickReply");
        currentCommentClick = feedAction;
        currentPosClick = pos;
        if (feedAction.isReplyCmt()) {
            feedAction = datas.get(pos - 1);
        }
        mActivity.showReplyCommentFragment(feedAction, mFeed, needShowKeyboard);
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodUtils.hideSoftKeyboard(mEditText, mActivity);
            }
        });
    }

    @Override
    public void onClickLike(FeedAction feedAction, int pos) {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin())
            mActivity.showDialogLogin();
        else {
            FeedModelOnMedia.ActionLogApp action = feedAction.getIsLike() == 1 ? FeedModelOnMedia.ActionLogApp.LIKE :
                    FeedModelOnMedia.ActionLogApp.UNLIKE;
            mAdapter.notifyItemChanged(pos);
            FeedContent feedContent = null;
            try {
                feedContent = mFeed.getFeedContent().clone();
            } catch (CloneNotSupportedException e) {
                Log.e(TAG, "CloneNotSupportedException", e);
            }
            if (feedContent == null) {
                mActivity.showToast(R.string.e601_error_but_undefined);
                return;
            }
            feedContent.setContentAction(FeedContent.CONTENT_ACTION);
            FeedModelOnMedia.ActionFrom actionFrom = feedType == Constants.ONMEDIA.FEED_TAB_VIDEO ?
                    FeedModelOnMedia.ActionFrom.mochavideo : FeedModelOnMedia.ActionFrom.onmedia;
            rest.logAppV6(feedContent.getUrl(), feedAction.getBase64RowId(), feedContent, action, "", feedAction
                            .getBase64RowId(),
                    "", actionFrom,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i(TAG, "actionLike: onresponse: " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                    int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                    if (code != HTTPCode.E200_OK) {
                                        mActivity.showToast(R.string.e601_error_but_undefined);
                                    } else {
                                    }
                                } else {
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                                mActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    });
        }
    }

    @Override
    public void onClickListLike(FeedAction feedAction) {
        if (feedAction.getNumberLike() > 0) {
            NavigateActivityHelper.navigateToOnMediaLikeOrShare(mActivity, feedAction.getBase64RowId(), true);
        }
    }

    @Override
    public void onDataReady() {
        if (needGetData) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (needGetFromUrl) {
                        getDataFeedFromUrl(urlAction);
                    } else {
                        getDataFeedFromRowId(rowId);
                    }
                    needGetData = false;
                }
            });
        }
    }

    @Override
    public void OnClickUser(String msisdn, String name) {
        Log.i(TAG, "enableClickTag: " + enableClickTag);
        if (enableClickTag) {
            if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                mActivity.showDialogLogin();
            else {
                //ko hieu sao cai clickableSpan bi nhay vao 2 lan lien @@
                enableClickTag = false;
                Log.i(TAG, "msisdn: " + msisdn);
                UserInfo userInfo = new UserInfo();
                userInfo.setMsisdn(msisdn);
                userInfo.setName(name);
                userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                userInfo.setStateMocha(1);
                eventOnMediaHelper.processUserClick(userInfo);
            }
        } else {
            enableClickTag = true;
        }
    }

    private void getModelFeed(String url) {
        switch (feedFrom) {
            case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_TAB_HOT:
                mFeed = mFeedBusiness.getFeedModelFromUrl(url);
                break;
            case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_MY_PROFILE:
                mFeed = mFeedBusiness.getFeedProfileFromUrl(url);
                break;
            case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_FRIEND_PROFILE:
                mFeed = mFeedBusiness.getFeedProfileProcess();
                break;
            case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_HASHMAP:
                Log.i(TAG, "getFeedFromhashmap");
                mFeed = mFeedBusiness.getFeedFromHashMap(url);
                break;
        }
    }

    @Override
    public void notifyFeedOnMedia(boolean needNotify, boolean needToSetSelection) {

    }

    @Override
    public void onPostFeed(FeedModelOnMedia feed) {
        if (mFeedBusiness != null) {
            mFeedBusiness.getListFeed().add(0, feed);
            mFeedBusiness.getListPost().add(feed);
            mFeedBusiness.getListFeedProfile().add(0, feed);
        }
        if (mActivity != null) mActivity.showToast(R.string.onmedia_share_success);
    }

    @Override
    public void onClick(View v, int pos, Object object) {

    }

    @Override
    public void onLongClick(View v, int pos, Object object) {
        FeedAction feedAction = datas.get(pos);
        if (feedAction != null && feedAction.isReplyCmt()) {
            showPopupContextMenuLongClick(feedAction);
        }
    }

    public void notifyDataListComment(FeedAction comment) {
        if (comment != null && currentCommentClick != null && datas.size() > currentPosClick) {
            if (!currentCommentClick.isReplyCmt()) {    //neu phan tu click ko phai la reply thi tang click them 1
                currentPosClick++;
            }
            if (datas.size() > currentPosClick) {
                if (datas.get(currentPosClick).isReplyCmt()) {
                    datas.remove(currentPosClick);
                }
            }
            datas.add(currentPosClick, comment);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void addComment(FeedAction comment) {
        listCommentPosting.put(comment.getIdCmtLocal(), comment);
    }

    private boolean updateComment(String idCmtLocal, String base64RowId) {
        if (listCommentPosting.containsKey(idCmtLocal)) {
            FeedAction cmt = listCommentPosting.get(idCmtLocal);
            cmt.setBase64RowId(base64RowId);
            cmt.setIdCmtLocal("");
            listCommentPosting.remove(idCmtLocal);
            return true;
        }
        return false;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isStartTrackingTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isStartTrackingTouch = false;
        if (mApplication.getPlayMusicController().getMediaPlayer() != null && mSeekbar != null) {
            int totalDuration = mApplication.getPlayMusicController().getMediaPlayer().getDuration();
            int currentPosition = Utilities.progressToTimer(
                    seekBar.getProgress(), totalDuration);
            if (totalDuration >= 0 && currentPosition >= 0) {
                mApplication.getPlayMusicController().getMediaPlayer().seekTo(currentPosition);
            }
        }
    }

    public void updateSeekBarProcess(int percent, int mediaPosition) {
        if (mSeekbar != null && !isStartTrackingTouch &&
                mApplication.getPlayMusicController() != null &&
                mApplication.getPlayMusicController().getMediaPlayer() != null) {
            mSeekbar.setProgress(percent);
            long totalDuration = mApplication.getPlayMusicController().getMediaPlayer().getDuration();
            if (totalDuration > 9000000 || totalDuration < mediaPosition) {
                return;
            }
//            mTvwDuration.setText(Utilities.milliSecondsToTimer(totalDuration));
            mTvwTime.setText(Utilities.milliSecondsToTimer(mediaPosition));
        }
    }

    @Override
    public void onChangeStateRepeat(int state) {

    }

    @Override
    public void onChangeStateNone() {

    }

    @Override
    public void onChangeStateGetData() {

    }

    @Override
    public void onChangeStatePreparing(MediaModel song) {

    }

    @Override
    public void onChangeStatePlaying(MediaModel song) {

    }

    @Override
    public void onActionFromUser(MediaModel song) {

    }

    @Override
    public void onCloseMusic() {

    }

    @Override
    public void onCallLoadDataFail(String message) {

    }

    @Override
    public void onUpdateSeekBarProgress(final int percent, final int mediaPosition) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSeekbar.setProgress(percent);
                Log.i(TAG, "update progess " + percent);
                updateSeekBarProcess(percent, mediaPosition);
                updatePlayerStatePlaying(mApplication.getPlayMusicController().isPlaying());
            }
        });
    }

    @Override
    public void onUpdateSeekBarBuffering(int percent) {

    }

    @Override
    public void onChangeStateInfo(int state) {

    }

    @Override
    public void onUpdateData(MediaModel song) {

    }

    public void updatePlayerStatePlaying(boolean isPlay) {
        if (isPlay) {
            mImgPlay.setImageResource(R.drawable.ic_voicemail_pause);
            mActivity.hideLoadingDialog();
        } else {
            mImgPlay.setImageResource(R.drawable.ic_voicemail_play);
        }
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        isLoadingFeedPreview = false;
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        if (volleyError instanceof NoConnectionError) {
            mActivity.showToast(R.string.no_connectivity_check_again);
        }
        Log.e(TAG, "error: " + volleyError);
    }

    @Override
    public void onGetListFeedDone(RestAllFeedsModel restAllFeedsModel) {
        isLoadingFeedPreview = false;
        needGetData = false;
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        Log.i(TAG, "get feed success: code " + restAllFeedsModel.getCode());
        if (restAllFeedsModel.getCode() == HTTPCode.E200_OK) {
            if (restAllFeedsModel.getData() != null && !restAllFeedsModel.getData().isEmpty() && !getFeedById) {
                restAllFeedsModel.getData().get(0).setUserInfo(null);
            }
            getFeedById = false;
            onLoadFeedDone(restAllFeedsModel.getData());
        }
    }

    /**
     * show keyboard khi bắt đầu vào màn hình
     */
    private void showKeyboard() {
        if (!isShowPreview && mEditText != null && showKeyboardRunnable != null) {
            mEditText.postDelayed(showKeyboardRunnable, 300L);
        }
    }
}

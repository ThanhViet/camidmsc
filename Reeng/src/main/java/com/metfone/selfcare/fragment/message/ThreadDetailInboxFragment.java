package com.metfone.selfcare.fragment.message;

import android.app.Activity;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.adapter.ThreadListAdapterRecyclerView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by toanvk2 on 12/16/14.
 */
public class ThreadDetailInboxFragment extends Fragment implements ReengMessageListener,
        ConfigGroupListener, ContactChangeListener, InitDataListener {
    private final String TAG = ThreadDetailInboxFragment.class.getSimpleName();
    private ChatActivity mChatActivity;
    private Resources mRes;
    private ApplicationController mApplication;
    private OnFragmentInteractionListener mListener;
    private Handler mHandler;
    private MessageBusiness mMessageBusiness;
    private RecyclerView mRecyclerView;
    private ProgressLoading mPrbLoading;
    private TextView mTvwNoteEmpty;
    private View rootView;
    private InitThreadList mInitThreadAsynctask;
    private ThreadListAdapterRecyclerView mAdapter;
    private ArrayList<ThreadMessage> mListThreads;
    private ArrayList<Object> mListObjects;

    public static ThreadDetailInboxFragment newInstance() {
        ThreadDetailInboxFragment fragment = new ThreadDetailInboxFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ThreadDetailInboxFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "[perform] -  onCreateView");
        rootView = inflater.inflate(R.layout.fragment_thread_detail_inbox, container, false);
        findComponentViews(rootView);
        setComponentViews();
        setAdapter();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        /*if (getArguments() != null) {
            mThreadId = getArguments().getInt(ThreadMessageConstant.THREAD_ID);
        } else if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ThreadMessageConstant.THREAD_ID);
        }*/
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mChatActivity = (ChatActivity) activity;
        mApplication = (ApplicationController) mChatActivity.getApplicationContext();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mHandler == null) {
            mHandler = new Handler(Looper.getMainLooper());
        }
        ListenerHelper.getInstance().addInitDataListener(this);
        //        mIconListener = this;
        if (mApplication.isDataReady()) {
            mMessageBusiness.setReengMessageThreadDetaiInbox(this);
            mMessageBusiness.addConfigGroupListener(this);
            ListenerHelper.getInstance().addContactChangeListener(this);
            startAsynctaskInitThreads();
        }
    }

    @Override
    public void onPause() {
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        mMessageBusiness.setReengMessageThreadDetaiInbox(null);
        mMessageBusiness.removeConfigGroupListener(this);
        mHandler = null;
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //        outState.putInt(ThreadMessageConstant.THREAD_ID, mThreadId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        if (mInitThreadAsynctask != null) {
            mInitThreadAsynctask.cancel(true);
            mInitThreadAsynctask = null;
        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
        mChatActivity = null;
        mApplication = null;
    }

    private void findComponentViews(View rootView) {
        mRes = mChatActivity.getResources();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.message_list_list_view);
        mPrbLoading = (ProgressLoading) rootView.findViewById(R.id.message_list_loading_progressbar);
        mTvwNoteEmpty = (TextView) rootView.findViewById(R.id.message_list_note_empty);
    }

    private void setComponentViews() {
        mMessageBusiness = mApplication.getMessageBusiness();
    }

    private void setListViewItemClickListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ThreadMessage) {
                    ThreadMessage threadMessage = (ThreadMessage) object;
                    if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                        String roomId = threadMessage.getServerId();
                        OfficerAccount accountRoomChat = mApplication.getOfficerBusiness().
                                getOfficerAccountByServerId(roomId);
                        String roomAvatar = "";
                        if (accountRoomChat != null) roomAvatar = accountRoomChat.getAvatarUrl();
//                        DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mChatActivity,
//                                roomId, threadMessage.getThreadName(), roomAvatar);
                    } else {
                        mListener.navigateToChatDetailActivity(threadMessage);
                    }
                } else if (object instanceof PhoneNumber) {
                    PhoneNumber phoneNumber = (PhoneNumber) object;
                    String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                    if (myNumber == null || phoneNumber.getJidNumber().equals(myNumber)) {
                        mChatActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_LONG);
                    } else {
                        ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread(phoneNumber
                                .getJidNumber());
                        mListener.navigateToChatDetailActivity(thread);
                    }
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    @Override
    public void onDataReady() {
        if (mHandler == null)
            return;
        mMessageBusiness.setReengMessageThreadDetaiInbox(this);
        mMessageBusiness.addConfigGroupListener(this);
        ListenerHelper.getInstance().addContactChangeListener(this);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                startAsynctaskInitThreads();
            }
        });
    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
        if (actionChange == Constants.MESSAGE.CHANGE_GROUP_NAME) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                startAsynctaskInitThreads();
            }
        });
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
        Log.i(TAG, "notifyNewIncomingMessage");
        // co message moi maf trung thread voi thread dang show
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                startAsynctaskInitThreads();
            }
        });
    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
                displayNoMessageView();
            }
        });
    }

    @Override
    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {

    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {
        Log.d(TAG, "onUpdateMediaDetail from sv: " + threadId);
    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {

    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {

    }

    @Override
    public void onRefreshMessage(int threadId) {
        Log.i(TAG, "onRefreshMessage");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                notifiChangeAdapter();
            }
        });
    }

    @Override
    public void onUpdateStateRoom() {

    }

    @Override
    public void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread) {

    }

    @Override
    public void notifyNewOutgoingMessage() {
        Log.i(TAG, "notifyNewOutgoingMessage");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                startAsynctaskInitThreads();
            }
        });
    }

    @Override
    public void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {
    }

    private void displayNoMessageView() {
        if (mListThreads != null && !mListThreads.isEmpty()) {
            mTvwNoteEmpty.setVisibility(View.GONE);
        } else {
            mTvwNoteEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void notifiChangeAdapter() {
        displayNoMessageView();
        mListObjects = new ArrayList<>();
        if (mListThreads != null)
            mListObjects.addAll(mListThreads);
        if (mAdapter == null) {
            setAdapter();
        }
        mAdapter.setThreadList(mListObjects);
        mAdapter.notifyDataSetChanged();
    }

    private void setAdapter() {
        mAdapter = new ThreadListAdapterRecyclerView(mChatActivity,
                Constants.CONTACT.CONTACT_VIEW_FORWARD_MESSAGE, null);
        mAdapter.setShowIconPin(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        setListViewItemClickListener();
    }

    private void startAsynctaskInitThreads() {
        Log.i(TAG, "startAsynctaskInitThreads");
        if (mInitThreadAsynctask != null) {
            mInitThreadAsynctask.cancel(true);
            mInitThreadAsynctask = null;
        }
        mInitThreadAsynctask = new InitThreadList();
        mInitThreadAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class InitThreadList extends AsyncTask<Void, Void, ArrayList<ThreadMessage>> {
        @Override
        protected void onPreExecute() {
            if (mAdapter == null) {
                mPrbLoading.setVisibility(View.VISIBLE);
                mPrbLoading.setEnabled(true);
            }
            super.onPreExecute();
        }

        @Override
        protected ArrayList<ThreadMessage> doInBackground(Void... params) {
            try {
                ArrayList<ThreadMessage> listThreads = new ArrayList<>();
                ArrayList<ThreadMessage> listPins = new ArrayList<>();
                if (mMessageBusiness.getThreadMessageArrayList() == null ||
                        mMessageBusiness.getThreadMessageArrayList().isEmpty())
                    return null;
                for (ThreadMessage threadMessage : mMessageBusiness.getThreadMessageArrayList()) {
                    if (threadMessage.getLastTimePinThread() != 0) {
                        listPins.add(threadMessage);
                    } else if (threadMessage.isReadyShow(mMessageBusiness)) {
                        listThreads.add(threadMessage);
                    }
                }
                if (!listThreads.isEmpty())
                    Collections.sort(listThreads, ComparatorHelper.getComparatorThreadMessageByLastTime());
                if (!listPins.isEmpty()) {
                    Collections.sort(listPins, ComparatorHelper.getComparatorThreadMessageByLastTimePin());
                    for (int i = 0; i < listPins.size(); i++) {
                        if (i == listPins.size() - 1)
                            listPins.get(i).setLastPin(true);
                        else
                            listPins.get(i).setLastPin(false);
                    }
                    listThreads.addAll(0, listPins);
                }
                return listThreads;
            } catch (Exception e) {// ArrayIndexOutOfBoundsException
                Log.e(TAG, "Exception", e);
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(ArrayList<ThreadMessage> params) {
            mPrbLoading.setEnabled(false);
            mPrbLoading.setVisibility(View.GONE);
            mListThreads = params;
            notifiChangeAdapter();
            super.onPostExecute(params);
        }
    }

    // fragment interface
    public interface OnFragmentInteractionListener {
        void navigateToChatDetailActivity(ThreadMessage threadMessage);
    }
}
package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.GuestBookVoteTabAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ReengViewPager;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/25/2017.
 */
public class GuestBookVoteTabFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = GuestBookVoteTabFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private GuestBookHelper mGuestBookHelper;
    private TextView mAbTitle;
    private int tabPos = 0;
    private ReengViewPager mViewPager;
    private TabLayout mTabLayout;
    private GuestBookVoteTabAdapter mAdapter;

    public GuestBookVoteTabFragment() {

    }

    public static GuestBookVoteTabFragment newInstance() {
        GuestBookVoteTabFragment fragment = new GuestBookVoteTabFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_pager, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        drawDetail();
        setViewListener();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
        }
    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (TextView) abView.findViewById(R.id.ab_title);
        mAbTitle.setText(mRes.getString(R.string.guest_book_vote_title));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbBack.setOnClickListener(this);
    }

    private void getData(Bundle savedInstanceState) {
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewPager = (ReengViewPager) rootView.findViewById(R.id.fragment_pager);
        mTabLayout = (TabLayout) rootView.findViewById(R.id.tabs_strip);
    }

    private void drawDetail() {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_TAB_VOTE);
        initAdapter();
    }

    private void setViewListener() {

    }

    private void initAdapter() {
        Log.d(TAG, "initAdapter");
        mTabLayout.setVisibility(View.VISIBLE);
        ArrayList<String> titles = new ArrayList<>();
        titles.add(mRes.getString(R.string.guest_book_vote_tab_week));
        titles.add(mRes.getString(R.string.guest_book_vote_tab_total));
        //titles.add(mRes.getString(R.string.social_tab_suggest));
        if (mAdapter == null) {
            Log.d(TAG, "initAdapter == null");
            mAdapter = new GuestBookVoteTabAdapter(getFragmentManager(), mParentActivity, titles);
            mViewPager.setAdapter(mAdapter);
            mViewPager.setOffscreenPageLimit(1);
            //mViewPager.setCurrentItem(0, true);
            if (tabPos >= titles.size()) tabPos = 0;// truyen position lon hon size
            mViewPager.setCurrentItem(tabPos, false);
            mTabLayout.setupWithViewPager(mViewPager);
        } else if (mViewPager.getAdapter() == null) {
            Log.d(TAG, "getAdapter() == null");
            mViewPager.setAdapter(mAdapter);
            mViewPager.setOffscreenPageLimit(1);
            if (tabPos >= titles.size()) tabPos = 0;// truyen position lon hon size
            mViewPager.setCurrentItem(tabPos, false);
            mTabLayout.setupWithViewPager(mViewPager);
        } else {
            mAdapter.setListTitle(titles);
            mAdapter.notifyDataSetChanged();
        }
    }
}
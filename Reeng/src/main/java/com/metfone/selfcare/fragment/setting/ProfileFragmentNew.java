package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.activity.AlbumViewActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.activity.ProfileActivity;
import com.metfone.selfcare.adapter.onmedia.OMFeedAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.ProfileHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.FeedOnMediaListener;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.listeners.UploadImageListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.keeng.utils.Utilities;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.autoplay.AspectRelativeLayout;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/16/14
 */
public class ProfileFragmentNew extends Fragment implements ProfileListener, ClickListener.IconListener
        , OnMediaHolderListener, UploadImageListener, FeedOnMediaListener, TagMocha.OnClickTag
        , ProfileHelper.ProfileHelperDrawAlbumListener, OnChannelChangedDataListener {
    private final String TAG = ProfileFragmentNew.class.getSimpleName();
    boolean colorWhite = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ApplicationController mApplication;
    private OnFragmentInteractionListener mListener;
    private ReengAccountBusiness mAccountBusiness;
    private ImageProfileBusiness mImageProfileBusiness;
    private ReengAccount mReengAccount;
    private ProfileActivity mParentActivity;
    private Resources mRes;
    private Handler mHandler;
    private RecyclerView mRecyclerViewFeed;
    private View mFooterView, mHeaderView;
    private LinearLayout mLoadmoreFooterView;

    private ImageView mImgProfileAvatar;
    private EllipsisTextView mTvwProfileStatus;
    private TextView mTvwProfileName;
    private ImageView mImgSex;
    private TextView mTvwAge;
    private int widthBigAvatar, heightBigAvatar;
    //    private ImageView mImgEditStatus;
//    private TextView tvEditStatus;
    private ImageView ivBack;
    private ClickListener.IconListener iconListener;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;
    private OMFeedAdapter mFeedAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter;
    private boolean isLoading = false;
    private boolean uploadLimited = true;
    //==========onmedia
    private WSOnMedia rest;
    private boolean noMoreFeed = false;
    private FeedBusiness mFeedBusiness;
    private int gaCategoryId, gaActionId;
    //======================
    private boolean enableClickTag = false;
    private OnMediaInterfaceListener feedInterface;

    private EventOnMediaHelper eventOnMediaHelper;
    private FeedModelOnMedia feedEdit;
    private int maxHeight = 500;
    private int maxHeightDefaut = 500;
    //    private View mLayoutMyAlbum;
//    private View mLayoutImageAlbum;
//private View mView3Image, mView2Image, mBtnShowAll;
//    private SquareImageView[] mImageViewList = new SquareImageView[5];
//    private ImageView mBtnUpload;
    private long mLastClickTime = 0;
    private ListenerUtils listenerUtils;

    private RoundedImageView mImgAvatarPostOM;
    private TextView mTvwAvatarPostOM;
    private View mViewPostPhoto, mViewPostLink;
    private View vEditStatus;
    private ImageView ivEditProfile;
    private AppCompatTextView tvPostContent;
    private View vPicProfile;
    private AppCompatTextView tvAddImage;

    private AspectRelativeLayout vPic1, vPic2, vPic3;

//    private TextView tvMsisdn;

    public ProfileFragmentNew() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ProfileFragmentNew newInstance() {
        return new ProfileFragmentNew();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_my_profile;
        iconListener = this;
        eventOnMediaHelper = new EventOnMediaHelper(mParentActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        ListenerHelper.getInstance().addNotifyFeedOnMediaListener(this);
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        findComponentViews(rootView, inflater, container);
        initData();
        setViewListeners();
        drawProfile();
        listenerUtils = mApplication.getApplicationComponent().providerListenerUtils();
        listenerUtils.addListener(this);
        if (mApplication.getConfigBusiness().isEnableOnMedia()) {
            loadData("");
        }
        return rootView;
    }

    private void initData() {
        mFeedAdapter = new OMFeedAdapter(mApplication, new ArrayList<>(), this, this);
        mFeedAdapter.setActivity(mParentActivity);
        mRecyclerViewFeed.setLayoutManager(new LinearLayoutManager(mRecyclerViewFeed.getContext()));
        mHeaderAndFooterRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(mFeedAdapter);
        mRecyclerViewFeed.setAdapter(mHeaderAndFooterRecyclerViewAdapter);
        mHeaderAndFooterRecyclerViewAdapter.addFooterView(mFooterView);
        mHeaderAndFooterRecyclerViewAdapter.addHeaderView(mHeaderView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mParentActivity = (ProfileActivity) getActivity();
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mReengAccount = mAccountBusiness.getCurrentAccount();
        mImageProfileBusiness = mApplication.getImageProfileBusiness();
        mRes = mApplication.getResources();
        uploadLimited = true;

        try {
            mListener = mParentActivity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(mParentActivity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            feedInterface = mParentActivity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(mParentActivity.toString()
                    + " must implement ConnectionFeedInterface");
        }
        mFeedBusiness = mApplication.getFeedBusiness();
        rest = new WSOnMedia(mApplication);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        enableClickTag = true;
        if (mHandler == null) {
            mHandler = new Handler();
        }
        ListenerHelper.getInstance().addProfileChangeListener(this);
        ListenerHelper.getInstance().addUploadImageListener(this);
        drawProfile();
        if (mFeedAdapter != null && mFeedAdapter.getItemCount() != 0) {
            mHeaderAndFooterRecyclerViewAdapter.notifyDataSetChanged();
            Log.i(TAG, "notify when resume");
        }
    }

    @Override
    public void onPause() {
        ListenerHelper.getInstance().removeProfileChangeListener(this);
        ListenerHelper.getInstance().removeUploadImageListener(this);
        Log.i(TAG, "onpause profile");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop profile");
        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ProfileActivity.REQUEST_PREVIEW_IMAGE:
                    drawProfile();
                    break;
            }
        }
    }

    @Override
    public void onDestroyView() {
//        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
        if (listenerUtils != null) listenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mHandler = null;
    }

    @Override
    public void onProfileChange() {
        if (mHandler != null) {
            mHandler.post(() -> {
                Log.d(TAG, "onProfileChange");
                drawProfile();
            });
        }
    }

    @Override
    public void onRequestFacebookChange(String fullName, String birthDay, int gender) {

    }

    @Override
    public void onAvatarChange(final String avatarPath) {
        if (mHandler != null) {
            mHandler.post(() -> {
                mApplication.getAvatarBusiness().setMyAvatar(mImgAvatarPostOM, mTvwAvatarPostOM, mTvwAvatarPostOM,
                        mApplication.getReengAccountBusiness().getCurrentAccount(), null);
                mApplication.getAvatarBusiness().setMyAvatar(mImgProfileAvatar, mApplication.getReengAccountBusiness().getCurrentAccount());
            });
        }
    }

    @Override
    public void onCoverChange(final String coverPath) {
        /*if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    ImageProfile imageCover = mImageProfileBusiness.getImageCover();
                    if (imageCover == null)
                        imageCover = new ImageProfile();
                    imageCover.setImagePathLocal(coverPath);
                    mImageProfileBusiness.displayImageProfile(mImgCover, imageCover, true);
                }
            });
        }*/
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        int totalImages = mImageProfileBusiness.getCurrentAlbumSize();
        Log.d(TAG, "totalImages : " + totalImages);
        Log.d(TAG, "maxSizeUpload : " + mImageProfileBusiness.getMaxAlbumImages());

        Log.d(TAG, "" + (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
                + " , " + uploadLimited
                + " , " + (totalImages >= mImageProfileBusiness.getMaxAlbumImages())
                + " , " + (mImageProfileBusiness.getMaxAlbumImages() > 0));

        switch (menuId) {
            case Constants.MENU.CAPTURE_IMAGE:
                if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL
                        && uploadLimited
                        && totalImages >= mImageProfileBusiness.getMaxAlbumImages()
                        && mImageProfileBusiness.getMaxAlbumImages() > 0) {
                    mParentActivity.showToast(String.format(getString(R.string.upload_image_maximum),
                            mImageProfileBusiness.getMaxAlbumImages()), Toast.LENGTH_LONG);
                } else {
                    mListener.takeAPhoto(typeUploadImage);
                }
                break;
            case Constants.MENU.SELECT_GALLERY:
                if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL
                        && uploadLimited &&
                        totalImages >= mImageProfileBusiness.getMaxAlbumImages()
                        && mImageProfileBusiness.getMaxAlbumImages() > 0) {

                    mParentActivity.showToast(String.format(getString(R.string.upload_image_maximum),
                            mImageProfileBusiness.getMaxAlbumImages()), Toast.LENGTH_LONG);
                } else {
                    mListener.openGallery(typeUploadImage, Math.min(Constants.MAX_IMAGE_CHOSEN, mImageProfileBusiness
                            .getMaxAlbumImages() - totalImages));
                }
                break;

            //=====onmedia
            case Constants.MENU.MENU_SHARE_LINK:
                FeedModelOnMedia feed = (FeedModelOnMedia) entry;
                handleShareLink(feed);
                break;

            case Constants.MENU.MENU_WRITE_STATUS:
                FeedModelOnMedia feedWrite = (FeedModelOnMedia) entry;
                handleWriteStatus(feedWrite);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_write_status);
                break;
            case Constants.MENU.EDIT:
                FeedModelOnMedia feedEdit = (FeedModelOnMedia) entry;
                handleEditFeed(feedEdit);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_edit);
                break;

            case Constants.MENU.DELETE:
                FeedModelOnMedia feedDelete = (FeedModelOnMedia) entry;
                confirmDelete(feedDelete);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_delete);
                break;
            case Constants.MENU.COPY:
                FeedModelOnMedia feedCopy = (FeedModelOnMedia) entry;
                TextHelper.copyToClipboard(mParentActivity, feedCopy.getFeedContent().getLink());
                mParentActivity.showToast(R.string.copy_to_clipboard);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy);
                break;
            case Constants.MENU.POPUP_DELETE:
                FeedModelOnMedia feedPopupDelete = (FeedModelOnMedia) entry;
                handleDeleteFeed(feedPopupDelete);
                break;
            case Constants.MENU.MENU_LIST_SHARE:
                FeedModelOnMedia feedListShare = (FeedModelOnMedia) entry;
                if (feedInterface != null) {
                    if (!TextUtils.isEmpty(feedListShare.getFeedContent().getUrl())) {
                        feedInterface.navigateToListShare(feedListShare.getFeedContent().getUrl());
                    }
                }
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_share);
                break;
            case Constants.MENU.POPUP_EXIT:
                FeedModelOnMedia item = (FeedModelOnMedia) entry;
                eventOnMediaHelper.handleClickPopupExitListenTogether(item, feedInterface);
                break;
            case Constants.MENU.MENU_COPY_TEXT:
                FeedModelOnMedia feedCopyText = (FeedModelOnMedia) entry;
                String textCopy = FeedBusiness.getTextTagCopy(feedCopyText.getUserStatus(), feedCopyText.getListTag());
                TextHelper.copyToClipboard(mParentActivity, textCopy);
                mParentActivity.showToast(R.string.copy_to_clipboard);
                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy_text);
                break;
            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                FeedModelOnMedia feedShareFb = (FeedModelOnMedia) entry;
                OnMediaHelper.shareFacebookOnMedia(feedShareFb, mParentActivity);
                break;
            case Constants.MENU.SEND_MESSAGE:
                if (entry instanceof FeedModelOnMedia) {
                    ShareUtils.sendToFriend(mParentActivity, (FeedModelOnMedia) entry);
                }
                break;
            case Constants.MENU.MORE:
                if (entry instanceof FeedModelOnMedia) {
                    ShareUtils.shareWithIntent(mParentActivity, (FeedModelOnMedia) entry);
                }
                break;
            default:
                break;
        }
    }

    private void handleEditFeed(final FeedModelOnMedia feedEdit) {
        if (TextUtils.isEmpty(feedEdit.getBase64RowId())) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        this.feedEdit = feedEdit;
        eventOnMediaHelper.navigateToPostOnMedia(mParentActivity,
                feedEdit.getFeedContent(), feedEdit.getUserStatus(),
                feedEdit.getBase64RowId(), true, null);
    }

    private void handleWriteStatus(FeedModelOnMedia feedWrite) {
        feedEdit = null;
        eventOnMediaHelper.navigateToPostOnMedia(mParentActivity,
                feedWrite.getFeedContent(), "", feedWrite.getBase64RowId(), false, null);
    }

    private void findComponentViews(View view, LayoutInflater inflater, ViewGroup container) {
        mRecyclerViewFeed = view.findViewById(R.id.recycler_view);
        ivBack = view.findViewById(R.id.ivBack);
        /*layoutManager = new LinearLayoutManager(mApplication);
        mRecyclerViewFeed.setLayoutManager(layoutManager);*/
        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mHeaderView = inflater.inflate(R.layout.header_my_profile, container, false);
        mLoadmoreFooterView = mFooterView.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);

//        mImgAvatarPostOM = (CircleImageView) mHeaderView.findViewById(R.id.img_onmedia_avatar_header);
//        mTvwAvatarPostOM = (TextView) mHeaderView.findViewById(R.id.tvw_onmedia_avatar_header);
//        mTvwWriteStatus = (TextView) mHeaderView.findViewById(R.id.tvw_write_status);
//        mViewPostPhoto = mHeaderView.findViewById(R.id.view_post_photo);
//        mViewPostLink = mHeaderView.findViewById(R.id.view_post_link);
//        tvMsisdn = mHeaderView.findViewById(R.id.tvw_profile_msisdn);

        mImgProfileAvatar = mHeaderView.findViewById(R.id.ivAvatar);
        mTvwProfileName = mHeaderView.findViewById(R.id.tvw_profile_name);
        mTvwProfileStatus = mHeaderView.findViewById(R.id.tvw_profile_status);
        /*Typeface face = Typeface.createFromAsset(mApplication.getAssets(), "fonts/utm_Helve.ttf");
        mTvwProfileStatus.setTypeface(face);*/
        mImgSex = mHeaderView.findViewById(R.id.img_profile_sex);
        mTvwAge = mHeaderView.findViewById(R.id.tvw_profile_age);
//        mImgEditStatus = (ImageView) mHeaderView.findViewById(R.id.ivEditStatus);
//        tvEditStatus = mHeaderView.findViewById(R.id.tvEditStatus);

//        mLayoutMyAlbum = mHeaderView.findViewById(R.id.view_my_album);
//        mLayoutMyAlbum.setVisibility(View.VISIBLE);
//        mLayoutImageAlbum = mHeaderView.findViewById(R.id.rl_image_view);

        mImgAvatarPostOM = mHeaderView.findViewById(R.id.ivAvatarPost);
        mTvwAvatarPostOM = mHeaderView.findViewById(R.id.tvAvatarPost);
        mViewPostPhoto = mHeaderView.findViewById(R.id.llPostPicture);
        mViewPostLink = mHeaderView.findViewById(R.id.llEmotion);
        vEditStatus = mHeaderView.findViewById(R.id.llEditStatus);
        ivEditProfile = view.findViewById(R.id.ivEdit);
        tvPostContent = mHeaderView.findViewById(R.id.tvPostContent);
        View vAlbum = mHeaderView.findViewById(R.id.vPicProfile);
        vAlbum.setVisibility(View.VISIBLE);
        vPicProfile = mHeaderView.findViewById(R.id.llPicture);
        tvAddImage = mHeaderView.findViewById(R.id.tvAddImage);
        tvAddImage.setVisibility(View.VISIBLE);

        vPic1 = mHeaderView.findViewById(R.id.rlPic1);
        vPic2 = mHeaderView.findViewById(R.id.rlPic2);
        vPic3 = mHeaderView.findViewById(R.id.rlPic3);

        int screenWidth = ScreenManager.getWidth(mParentActivity);
        int widthParent = screenWidth - Utilities.dpToPx(mParentActivity, 28);
        int widthChild = widthParent / 3;
        ViewGroup.LayoutParams lpPic1 = vPic1.getLayoutParams();
        lpPic1.width = widthChild;
        lpPic1.height = widthChild * 100 / 114;
        vPic1.setLayoutParams(lpPic1);

        ViewGroup.LayoutParams lpPic2 = vPic2.getLayoutParams();
        lpPic2.width = widthChild;
        lpPic2.height = widthChild * 100 / 114;
        vPic2.setLayoutParams(lpPic2);

        ViewGroup.LayoutParams lpPic3 = vPic1.getLayoutParams();
        lpPic3.width = widthChild;
        lpPic3.height = widthChild * 100 / 114;
        vPic3.setLayoutParams(lpPic3);

        DisplayMetrics displayMetrics = mApplication.getResources().getDisplayMetrics();
        int widthScreen = displayMetrics.widthPixels;
        TypedValue outValue = new TypedValue();
        getResources().getValue(R.dimen.ratio_profile_avatar, outValue, true);
        float ratio = outValue.getFloat();
        widthBigAvatar = widthScreen;
        maxHeight = Math.round((float) widthBigAvatar / ratio);
        maxHeightDefaut = maxHeight;
        if (widthBigAvatar > 800) {
            widthBigAvatar = 800;
        }
        heightBigAvatar = Math.round((float) widthBigAvatar / ratio);
        Log.i(TAG, "width avatar: " + widthBigAvatar + " height avatar: " + heightBigAvatar);

        //image album
        /*mBtnShowAll = mHeaderView.findViewById(R.id.tvw_view_all_album);
        mBtnShowAll.setVisibility(View.VISIBLE);

        mView3Image = mHeaderView.findViewById(R.id.ll_image_view_small);
        mView2Image = mHeaderView.findViewById(R.id.ll_image_view_big);

        mImageViewList = new SquareImageView[5];
        mImageViewList[0] = (SquareImageView) mView3Image.findViewById(R.id.img_0);
        mImageViewList[1] = (SquareImageView) mView3Image.findViewById(R.id.img_1);
        mImageViewList[2] = (SquareImageView) mView3Image.findViewById(R.id.img_2);

        mImageViewList[3] = (SquareImageView) mView2Image.findViewById(R.id.img_3);
        mImageViewList[4] = (SquareImageView) mView2Image.findViewById(R.id.img_4);

        mView3Image.setVisibility(View.GONE);
        mView2Image.setVisibility(View.GONE);

        mBtnShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAlbumPreview();
            }
        });
        for (int i = 0; i < 5; i++) {
            mImageViewList[i].setVisibility(View.GONE);
        }

        mBtnUpload = (ImageView) mHeaderView.findViewById(R.id.btn_upload_image);*/
        ivBack.setOnClickListener(view1 -> mParentActivity.onBackPressed());
    }

    private void setColorItemFakeToolbar(boolean isWhite) {
        /*if (isWhite) {
            if (!colorWhite) {
                mTvwProfileNameToolbar.setVisibility(View.INVISIBLE);
                mViewBottomToolbar.setVisibility(View.GONE);
                mImgBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));
                mImgEditProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));
                colorWhite = true;
            }
        } else {
            colorWhite = false;
            mTvwProfileNameToolbar.setVisibility(View.VISIBLE);
            mViewBottomToolbar.setVisibility(View.VISIBLE);
            mImgBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.bg_ab_icon));
            mImgEditProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.bg_ab_icon));
        }*/
    }

    private void setViewListeners() {
        setEditStatusListener();
        setAvatarImageClickListener();
        setListviewListener();
        setImgAvatarListener();
        setImageUploadListener();
        setAbEditProfileListener();

        tvPostContent.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
            startActivity(intent);
        });

        mViewPostPhoto.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_IMAGE);
            startActivity(intent);
        });

        mViewPostLink.setOnClickListener(v -> {
            Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
//            intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_LINK);
            startActivity(intent);
        });

        vEditStatus.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.navigateToEditStatus();
            }
        });
    }

    private void setImageClickListener() {
        vPic1.setOnClickListener(v -> {
            if (mImageProfileBusiness.getImageProfileList().isEmpty()) {
                Log.d(TAG, "not data in imageprofile");
                return;
            }
//                int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfileBusiness
//                        .getImageProfileList(), 0);
//                Log.i(TAG, "pos: " + pos);
            eventOnMediaHelper.showImageDetail(mReengAccount.getName(), mReengAccount.getJidNumber(),
                    null, 0, FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });

        vPic2.setOnClickListener(v -> {
            if (mImageProfileBusiness.getImageProfileList().isEmpty()) {
                Log.d(TAG, "not data in imageprofile");
                return;
            }
//                int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfileBusiness
//                        .getImageProfileList(), 1);
//                Log.i(TAG, "pos: " + pos);
            eventOnMediaHelper.showImageDetail(mReengAccount.getName(), mReengAccount.getJidNumber(),
                    null, 1, FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });

        vPic3.setOnClickListener(v -> {
            if (mImageProfileBusiness.getImageProfileList().isEmpty()) {
                Log.d(TAG, "not data in imageprofile");
                return;
            }
//                int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfileBusiness
//                        .getImageProfileList(), 2);
//                Log.i(TAG, "pos: " + pos);
            if (mImageProfileBusiness.getImageProfileList() != null && mImageProfileBusiness.getImageProfileList().size() > 3) {
                // show activity preview list image
                if (mListener != null) {
                    mListener.openListImage();
                }
            } else {
                eventOnMediaHelper.showImageDetail(mReengAccount.getName(), mReengAccount.getJidNumber(),
                        null, 2, FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
            }
        });

    }

    private void gotoAlbumPreview() {
        Intent intent = new Intent(mApplication, AlbumViewActivity.class);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, mImageProfileBusiness.getImageProfileList());
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME, mReengAccount.getName());
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, mReengAccount.getJidNumber());
        mParentActivity.startActivity(intent);
    }

    private void setAbEditProfileListener() {
        ivEditProfile.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mListener != null) mListener.navigateToEditProfile();
            }
        });
    }

    private int getAlphaforActionBar(int scrollY) {
        int minDist = 0, maxDist = maxHeight;
        if (scrollY > maxDist) {
            return 255;
        } else if (scrollY < minDist) {
            return Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR;
        } else {
            int alpha;
            maxHeight = maxHeightDefaut;
            alpha = (int) ((255.0 / maxDist) * scrollY);
            return Math.max(alpha, Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR);
        }
    }

    private void setImageUploadListener() {
        tvAddImage.setOnClickListener(view -> onBtnUploadClicked());
    }

    @Override
    public void onBtnUploadClicked() {
        int totalImages = 0;
        if (totalImages >= mImageProfileBusiness.getMaxAlbumImages() && mImageProfileBusiness.getMaxAlbumImages() > 0) {
            mParentActivity.showToast(String.format(getString(R.string.upload_image_maximum), mImageProfileBusiness
                    .getMaxAlbumImages()), Toast.LENGTH_LONG);
        } else {
            typeUploadImage = ImageProfileConstant.IMAGE_NORMAL;
            showPopupUploadImage();
        }
        /*if(mListener != null){
            mListener.openListImage();
        }*/
    }

    /*

    private void setImgCoverListener() {
        mImgUpdateCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typeUploadImage = ImageProfileConstant.IMAGE_COVER;
                showPopupUploadImage();
            }
        });

        mImgCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageProfileBusiness.getImageCover() == null) return;
                ArrayList<ImageProfile> list = new ArrayList<>();
                list.add(mImageProfileBusiness.getImageCover());
                eventOnMediaHelper.showImageDetail(mReengAccount.getName(), mReengAccount.getJidNumber(),
                        list, 0, FeedContent.ITEM_TYPE_PROFILE_COVER, -1);

                *//*showImageDetail(mReengAccount.getName(), mReengAccount.getJidNumber(), true, true,
                        Constants.CONTACT_TYPE.USER, 0);*//*
            }
        });
    }*/

    private void setImgAvatarListener() {
        mImgProfileAvatar.setOnClickListener(view -> {
            if (mReengAccount == null || TextUtils.isEmpty(mReengAccount.getLastChangeAvatar()))
                return;
            mApplication.getAvatarBusiness().displayFullAvatar(mReengAccount.getJidNumber(), mReengAccount
                            .getLastChangeAvatar(),
                    mReengAccount.getName(), null, "");
        });
    }

    private void setAvatarImageClickListener() {
        /*mImgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO zoom image
                *//*typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;
                showPopupUploadImage();*//*
                mApplication.getAvatarBusiness().displayFullAvatar(mReengAccount.getJidNumber(), mReengAccount
                                .getLastChangeAvatar(),
                        mReengAccount.getName(), "file://" + mReengAccount.getAvatarPath(), "");
            }
        });*/
    }

    private void setEditStatusListener() {
        /*mImgEditStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.navigateToEditStatus();
                }
            }
        });*/
    }

    private void setOldClickListener() {
        /*mLayoutBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTvwBirthday.getText().equals(birthday)) {
                    mTvwBirthday.setText(String.valueOf(old));
                } else {
                    mTvwBirthday.setText(birthday);
                }
            }
        });*/
    }

    private void checkAndSetAlphaActionBar() {
        /*View c = mRecyclerViewFeed.getChildAt(0);
        if (c != null) {
            if (mHeaderView == c) {
//                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                if (layoutManager == null) return;
                int scrolly = -c.getTop() + layoutManager.findFirstVisibleItemPosition() * c.getHeight();
                int alpha = getAlphaforActionBar(scrolly);
                mDrawableAb.setAlpha(alpha);
                mDrawableStatus.setAlpha(alpha);
                if (alpha < Constants.SETTINGS.ALPHA_SHOW_ACTIONBAR) {
                    setColorItemFakeToolbar(true);
                } else {
                    setColorItemFakeToolbar(false);
                }
            }
        }*/
    }

    private void setListviewListener() {
        /*final View viewRight = mHeaderView.findViewById(R.id.layout_right_profile);
        final int offset = mRes.getDimensionPixelOffset(R.dimen.offset_my_profile_name);
        viewRight.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.i(TAG, "onGlobalLayout w: " + mApplication.getWidthPixels() + "  right: " + viewRight.getWidth()
                        + " offset: " + offset);
                mTvwProfileName.setMaxWidth(mApplication.getWidthPixels() - viewRight.getWidth() - offset);
                if (viewRight.getViewTreeObserver().isAlive()) {
                    viewRight.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });*/

        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                checkAndSetAlphaActionBar();
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !mFeedBusiness
                            .getListFeedProfile().isEmpty() && !noMoreFeed) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }

            }
        };
        mRecyclerViewFeed.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));
    }

    ///========onmedia
    private void onLoadMore() {
        if (isLoading) {
            Log.i(TAG, "loaddata onLoadMore isLoading");
        } else {
            if (!noMoreFeed) {
                mLoadmoreFooterView.setVisibility(View.VISIBLE);
                loadData(getLastRowId());
            } else {
                Log.i(TAG, "loaddata onLoadMore nomorefeed");
            }
        }
    }

    private String getLastRowId() {
        if (mFeedBusiness.getListFeedProfile() == null || mFeedBusiness.getListFeedProfile().isEmpty()) {
            return "";
        } else {
            return mFeedBusiness.getListFeedProfile().get(mFeedBusiness.getListFeedProfile().size() - 1)
                    .getBase64RowId();
        }
    }

    private void showPopupUploadImage() {
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu selectGallery = new ItemContextMenu(mRes.getString(R.string
                .select_from_gallery),
                -1, null, Constants.MENU.SELECT_GALLERY);
        ItemContextMenu capture = new ItemContextMenu(mRes.getString(R.string.capture_image),
                -1, null, Constants.MENU.CAPTURE_IMAGE);
        listItem.add(capture);
        listItem.add(selectGallery);
        PopupHelper.getInstance().showContextMenu(mParentActivity, null, listItem, this);
    }

    public void drawProfile() {
        Log.i(TAG, "drawProfile");
        mReengAccount = mAccountBusiness.getCurrentAccount();
        if (mReengAccount != null) {
            mApplication.getAvatarBusiness().setMyAvatar(mImgAvatarPostOM, mTvwAvatarPostOM, mTvwAvatarPostOM,
                    mApplication.getReengAccountBusiness().getCurrentAccount(), null);
            mApplication.getAvatarBusiness().setMyAvatar(mImgProfileAvatar, mReengAccount);

            String status = mReengAccount.getStatus();
            String userName = mReengAccount.getName();
            //String userNumber = mReengAccount.getJidNumber();
            if (TextUtils.isEmpty(status)) {
                mTvwProfileStatus.setVisibility(View.GONE);
            } else {
                mTvwProfileStatus.setVisibility(View.VISIBLE);
                mTvwProfileStatus.setEmoticonStatus(mParentActivity, status, status.hashCode(), status);
            }
            mTvwProfileName.setText(userName);
            if (mReengAccount.getGender() == 0) {
                mImgSex.setImageResource(R.drawable.ic_v5_profile_female);
            } else {
                mImgSex.setImageResource(R.drawable.ic_v5_profile_male);
            }
            int old = 0;
            if (!TextUtils.isEmpty(mReengAccount.getBirthdayString())) {
                old = TimeHelper.getOldFromBirthday(TimeHelper.convertBirthdayStringToLong(
                        mReengAccount.getBirthdayString()));
            } else {
                if (mReengAccount.getBirthday() != null) {
                    try {
                        old = TimeHelper.getOldFromBirthday(mReengAccount.getBirthdayLong());
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
            if (old <= 0) {
                mTvwAge.setVisibility(View.GONE);
            } else {
                mTvwAge.setText(String.format(mParentActivity.getString(R.string.mc_profile_age), old));
                mTvwAge.setVisibility(View.VISIBLE);
            }
//            tvMsisdn.setText(mReengAccount.getJidNumber());
        }
        setBackgroundStatus();
        displayCoverAndProfile();
    }

    private void displayCoverAndProfile() {
        Log.i(TAG, "displayCoverAndProfile");
        setListImageSmall();
    }

    private void setBackgroundStatus() {
        /*if (mParentActivity != null && mTvwProfileStatus != null)
            mTvwProfileStatus.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = mTvwProfileStatus.getLineCount();
                    Log.d(TAG, "lineCount=" + lineCount);
                    if (lineCount <= 1) {
                        mTvwProfileStatus.setBackgroundResource(R.drawable.ic_quote_single_line);
                    } else if (lineCount > 1) {
                        mTvwProfileStatus.setBackgroundResource(R.drawable.ic_quote_multi_line);
                    }
                }
            });*/
    }

    public void setListImageSmall() {
        setImageClickListener();
        if (mImageProfileBusiness.getImageProfileList() == null
                || mImageProfileBusiness.getImageProfileList().isEmpty()) {
            vPicProfile.setVisibility(View.GONE);
        } else {
            vPicProfile.setVisibility(View.VISIBLE);
            ProfileHelper.drawAlbum(mApplication, mImageProfileBusiness.getImageProfileList(), vPic1,
                    vPic2, vPic3, false);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
                || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (mFeedAdapter != null && mFeedAdapter.getItemCount() != 0) {
                mFeedAdapter.notifyDataSetChanged();
                Log.i(TAG, "notify when resume");
            }
        }
        Log.d(TAG, "newConfig=" + newConfig.orientation);
    }

    //==========onmedia
    private void loadData(final String lastRowId) {
        if (mParentActivity != null) {
            if (!NetworkHelper.isConnectInternet(mParentActivity)) {
                mParentActivity.showToast(R.string.no_connectivity_check_again);
                mLoadmoreFooterView.setVisibility(View.GONE);
                return;
            }
            isLoading = true;
            rest.getUserTimeLine(mReengAccount.getJidNumber(), lastRowId, restAllFeedsModel -> {
                Log.i(TAG, "onResponse: getUserTimeLine code " + restAllFeedsModel.getCode());
                isLoading = false;
                mLoadmoreFooterView.setVisibility(View.GONE);
                if (restAllFeedsModel.getCode() == HTTPCode.E200_OK) {
                    mFeedBusiness.setDeltaTimeServer(
                            System.currentTimeMillis() - restAllFeedsModel.getCurrentTimeServer());
                    onLoadDataDone(restAllFeedsModel.getData(), lastRowId);
                }
            }, volleyError -> {
                isLoading = false;
                mLoadmoreFooterView.setVisibility(View.GONE);
                Log.i(TAG, "error");
            });
        }
    }

    private void onLoadDataDone(ArrayList<FeedModelOnMedia> result, String lastRowId) {
        if (result != null && !result.isEmpty()) {
            noMoreFeed = false;
            result = mFeedBusiness.preProcessListFeedModelListTag(result);
            if (TextUtils.isEmpty(lastRowId)) {
                mFeedBusiness.setListFeedProfile(result);
            } else {
                mFeedBusiness.getListFeedProfile().addAll(result);
            }
            Log.i(TAG, "load data done: " + mFeedBusiness.getListFeedProfile().size());
            mFeedAdapter.setListFeed(mFeedBusiness.getListFeedProfile());
            mFeedAdapter.notifyDataSetChanged();
        } else {
            Log.i(TAG, "nomore feed");
            noMoreFeed = true;
            if (TextUtils.isEmpty(lastRowId)) {
                mFeedBusiness.getListFeedProfile().clear();
                mFeedAdapter.setListFeed(mFeedBusiness.getListFeedProfile());
                mFeedAdapter.notifyDataSetChanged();
            }
            /*else {
                mParentActivity.showToast(R.string.msg_loadmore_empty);
            }*/
        }
    }

    @Override
    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        final boolean isLiked = feed.getIsLike() == 1;
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        mFeedBusiness.addUrlActionLike(feed.getFeedContent().getUrl(), feed.getBase64RowId(), action, feed.getFeedContent());
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
    }

    @Override
    public void onClickCommentFeed(FeedModelOnMedia feed) {
        displayCommentStatusFragment(feed);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_comment);
    }

    @Override
    public void onClickShareFeed(FeedModelOnMedia feed) {
        eventOnMediaHelper.showPopupContextMenuShare(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share);
    }

    @Override
    public void onClickUser(UserInfo userInfo) {
        if (userInfo != null) {
            if (!userInfo.getMsisdn().equals(mAccountBusiness.getJidNumber())) {
                eventOnMediaHelper.processUserClick(userInfo);
            }
        }
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_avatar);
    }

    @Override
    public void onClickMediaItem(FeedModelOnMedia feed) {
        eventOnMediaHelper.handleClickMediaItem(feed, gaCategoryId, gaActionId, feedInterface, iconListener);
    }

    @Override
    public void onClickImageItem(FeedModelOnMedia feed, int positionImage) {
        eventOnMediaHelper.handleClickImage(feed, positionImage, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_MY_PROFILE);
    }

    @Override
    public void onClickMoreOption(FeedModelOnMedia feed) {
//        showPopupContextMenu(feed);
        eventOnMediaHelper.handleClickMoreOption(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_option_feed);
    }

    @Override
    public void onClickButtonTotal(View rowView, FeedModelOnMedia feed) {
        eventOnMediaHelper.handleShareTotal(rowView, feed, gaCategoryId, gaActionId);
    }

    @Override
    public void onLongClickStatus(FeedModelOnMedia feed) {
        if (feed != null) {
            eventOnMediaHelper.showPopupContextMenuLongClick(feed, this);
        }
    }

    @Override
    public void onDeepLinkClick(FeedModelOnMedia feed, String link) {
        eventOnMediaHelper.handleDeeplink(feed, link);
    }

    @Override
    public void onClickSuggestFriend(UserInfo userInfo) {

    }

    @Override
    public void openChannelInfo(FeedModelOnMedia feed) {
        Channel channelVideo = Channel.convertFromChannelOnMedia(feed.getFeedContent().getChannel());
        if (channelVideo == null) return;
        mApplication.getApplicationComponent().providesUtils().openChannelInfo(mParentActivity, channelVideo);
    }

    @Override
    public void openPlayStore(FeedModelOnMedia feed, String packageName) {
        NavigateActivityHelper.navigateToPlayStore(mParentActivity, packageName);
    }

    @Override
    public void onSubscribeChannel(FeedModelOnMedia feed, Channel channel) {
        if (channel != null) {
            ApplicationController.self().getListenerUtils().notifyChannelSubscriptionsData(channel);
            ApplicationController.self().getApplicationComponent().providerChannelApi().callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
        }
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        if (feed.getFeedContent() == null) return;
        String urlKey = feed.getFeedContent().getUrl();
        if (TextUtils.isEmpty(urlKey)) return;
        int indexFeedLike = -1;
        ArrayList<FeedModelOnMedia> listFeed = mFeedBusiness.getListFeedProfile();
        for (int i = 0; i < listFeed.size(); i++) {
            FeedModelOnMedia mFeed = listFeed.get(i);
            if (mFeed.getFeedContent() != null && urlKey.equals(mFeed.getFeedContent().getUrl())) {
                int delta;
                if (isLike) {
                    delta = 1;
                    mFeed.setIsLike(1);
                } else {
                    delta = -1;
                    mFeed.setIsLike(0);
                    if (mFeed.getActionType() == FeedModelOnMedia.ActionLogApp.LIKE) {
                        indexFeedLike = i;
                        Log.i(TAG, "indexFeedLike: " + i);
                    }
                }
                long countLike = listFeed.get(i).getFeedContent().getCountLike();
                mFeed.getFeedContent().setCountLike(countLike + delta);
            }
        }
        if (indexFeedLike != -1) {
            listFeed.remove(indexFeedLike);
        }
        mFeedAdapter.notifyDataSetChanged();
    }

    public void displayCommentStatusFragment(FeedModelOnMedia mItem) {
        if (feedInterface != null) {
            feedInterface.navigateToComment(mItem);
        }
    }

    public void handleShareLink(final FeedModelOnMedia feed) {
        if (feed.getIsShare() == 1) {
            mParentActivity.showToast(R.string.onmedia_already_shared);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        setShareFeed(feed, 1);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE, "",
                feed.getBase64RowId(), "", null,
                response -> {
                    Log.i(TAG, "actionShare: onresponse: " + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                            int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                mParentActivity.showToast(R.string.onmedia_share_success);
                            } else {
                                setShareFeed(feed, -1);
                                mParentActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } else {
                            setShareFeed(feed, -1);
                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        setShareFeed(feed, -1);
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }, volleyError -> {
                    setShareFeed(feed, -1);
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                });
    }

    private void setShareFeed(FeedModelOnMedia feed, int delta) {
        String urlKey = feed.getFeedContent().getUrl();
        ArrayList<FeedModelOnMedia> listFeed = mFeedBusiness.getListFeedProfile();
        for (int i = 0; i < listFeed.size(); i++) {
            if (listFeed.get(i).getFeedContent().getUrl().equals(urlKey)) {
                long countShare = listFeed.get(i).getFeedContent().getCountShare();
//        long countComment = feed.getFeedContent().getCountComment();
                listFeed.get(i).getFeedContent().setCountShare(countShare + delta);
//        feed.getFeedContent().setCountComment(countComment + delta);
                if (delta == 1) {
                    listFeed.get(i).setIsShare(1);
                } else {
                    listFeed.get(i).setIsShare(0);
                }
            }
        }
        mFeedAdapter.notifyDataSetChanged();
    }

    private void confirmDelete(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.delete);
        String msg = mRes.getString(R.string.onmedia_message_delete);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_DELETE);
    }

    private void handleDeleteFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.DELETE, "",
                feed.getBase64RowId(), "", null,
                response -> {
                    Log.i(TAG, "DeleteFeed: onresponse: " + response);
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE, 0);
                            if (code == HTTPCode.E200_OK) {
//                                    mParentActivity.showToast(R.string.onmedia_action_success);
                            } else {
                                throw new Exception();
                            }
                        } else {
                            throw new Exception();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
//                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }

                }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));

        mParentActivity.runOnUiThread(() -> {
            mFeedBusiness.getListFeedProfile().remove(feed);
            mFeedAdapter.notifyDataSetChanged();
        });
    }

    public void notifyNewFeed(final boolean needToSetSelection) {
        mParentActivity.runOnUiThread(() -> {
            Log.i(TAG, "notify nowwwwwwwwwwwwww");
            mFeedAdapter.notifyDataSetChanged();
            if (needToSetSelection) {
                mRecyclerViewFeed.scrollToPosition(1);
            }
        });

    }

    @Override
    public void notifyFeedOnMedia(boolean needNotify, boolean needToSetSelection) {
        Log.i(TAG, "notifyFeedOnMedia: " + needNotify + " selection: " + needToSetSelection);
        notifyNewFeed(needToSetSelection);
    }

    @Override
    public void onPostFeed(FeedModelOnMedia feed) {
        /*if (feedEdit != null) {
            mFeedBusiness.getListFeedProfile().remove(feedEdit);
            mFeedBusiness.getListFeed().add(0, feed);
            mFeedBusiness.getListPost().add(feed);
            mFeedBusiness.getListFeedProfile().add(0, feed);
            mFeedAdapter.setListFeed(mFeedBusiness.getListFeedProfile());
            mFeedAdapter.notifyDataSetChanged();
        } else {*/
        if (mFeedAdapter != null && mFeedBusiness != null && !mFeedBusiness.getListFeed().contains(feed)) {
            Log.i(TAG, "onPostFeed");
            mFeedBusiness.getListFeed().add(0, feed);
            mFeedBusiness.getListPost().add(feed);
            mFeedBusiness.getListFeedProfile().add(0, feed);
            mFeedAdapter.setListFeed(mFeedBusiness.getListFeedProfile());
            mFeedAdapter.notifyDataSetChanged();
        }
//        }
    }

    @Override
    public void onUploadSuccess(final ImageProfile image) {
        Log.i(TAG, "onUploadSuccess");
    }

    @Override
    public void onUploadFail(ImageProfile image) {
        Log.i(TAG, "onUploadFail");
        mParentActivity.hideLoadingDialog();
    }

    @Override
    public void onUploadEnd(final int type, final ArrayList<ImageProfile> imageProfiles, final String message) {
        Log.i(TAG, "type= " + type + ". onUploadEnd");
        mParentActivity.hideLoadingDialog();
        if (mHandler != null) {
            mHandler.post(() -> {
                if (type == Constants.UPLOAD.HTTP_STATE_UPLOAD_SUCCESS) {
                    if (imageProfiles != null && !imageProfiles.isEmpty()) {
                        if (mHandler != null) {
                            mHandler.post(this::setListImageSmall);
                        }
                        int size = imageProfiles.size();
                        if (size > 1) {
                            mParentActivity.showToast(String.format(mParentActivity.getString(R.string
                                    .upload_image_profile_success), size), Toast.LENGTH_LONG);
                        } else {
                            mParentActivity.showToast(String.format(mParentActivity.getString(R.string
                                    .upload_image_profile_success), size), Toast.LENGTH_LONG);
                        }
                        setListImageSmall();
                    } else {
                        mParentActivity.showToast(R.string.upload_image_profile_success3);
                    }
                } else if (type == Constants.UPLOAD.HTTP_STATE_UPLOAD_FAILED_EXCEEDED) {
                    String content = message;
                    try {
                        content = new String(content.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                    } catch (Exception e) {
                        Log.e(TAG, "onUploadExceeded Exception", e);
                    }
                    if (!TextUtils.isEmpty(content))
                        mParentActivity.showToast(content, Toast.LENGTH_LONG);
                    else
                        mParentActivity.showToast(R.string.upload_image_profile_fail);
                } else {
                    mParentActivity.showToast(R.string.upload_image_profile_fail);
                }
            });
        }
    }

    @Override
    public void onStartUpload() {
    }

    @Override
    public void OnClickUser(String msisdn, String name) {
        if (enableClickTag) {
            //ko hieu sao cai clickableSpan bi nhay vao 2 lan lien @@
            enableClickTag = false;
            Log.i(TAG, "msisdn: " + msisdn);
            if (!msisdn.equals(mAccountBusiness.getJidNumber())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setMsisdn(msisdn);
                userInfo.setName(name);
                userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                userInfo.setStateMocha(1);
                eventOnMediaHelper.processUserClick(userInfo);
            }
        } else {
            enableClickTag = true;
        }
    }

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        if (channel != null && mFeedAdapter != null) {
            boolean notifyAdapter = false;
            int size = mFeedAdapter.getItemCount();
            for (int i = 0; i < size; i++) {
                if (mFeedAdapter.getItem(i) instanceof FeedModelOnMedia) {
                    FeedModelOnMedia item = (FeedModelOnMedia) mFeedAdapter.getItem(i);
                    FeedContent feedContent = item.getFeedContent();
                    if (feedContent != null && feedContent.getChannel() != null
                            && !TextUtils.isEmpty(feedContent.getChannel().getId())
                            && feedContent.getChannel().getId().equals(channel.getId())) {
                        feedContent.getChannel().setFollow(channel.isFollow());
                        feedContent.getChannel().setNumFollow(channel.getNumFollow());
                        notifyAdapter = true;
                    }
                }
            }
            if (notifyAdapter) mFeedAdapter.notifyDataSetChanged();
        }
    }

    public interface OnFragmentInteractionListener {

        void navigateToEditStatus();

        void navigateToEditProfile();

        void takeAPhoto(int typeUpload);

        void openGallery(int typeUpload, int maxImages);

        void openListImage();
    }
}
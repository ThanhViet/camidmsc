package com.metfone.selfcare.fragment.onmedia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.adapter.onmedia.CommentOnMediaAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.listeners.OnMediaCommentHolderListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.FilteredArrayAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by thanhnt72 on 4/20/2018.
 */

public class ReplyCommentFragment extends Fragment implements TagMocha.OnClickTag, OnMediaCommentHolderListener,
        RecyclerClickListener {


    public String TAG = ReplyCommentFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private OnMediaActivityNew mActivity;
    private ContactBusiness mContactBusiness;
    private Resources mRes;

    private ImageButton mSend;
    private RecyclerView mRecyclerViewComment;
    private ProgressLoading mProgressBar;
    private View mFooterView;
    private View mLayoutProgress;
    private TextView viewFeed;

    private WSOnMedia rest;
    private CommentOnMediaAdapter mAdapter;
    private TagsCompletionView mEditText;
    private FeedBusiness mFeedBusiness;
    private String textTag;

    private TagOnMediaAdapter adapterUserTag;

    private FeedModelOnMedia mFeed;
    private FeedAction commentParent;
    private boolean needShowKeyboard;
    private String urlComment;
    private String urlParent;
    private String rowid;
    private ArrayList<FeedAction> datas = new ArrayList<>();
    private boolean isLoading;
    private boolean isNoMoreComment;
    private Handler mHandler;
    private FeedAction lastComment;
    private EventOnMediaHelper eventOnMediaHelper;

    private boolean enableClickTag = true;
    private boolean didClickLike = false;


    public static ReplyCommentFragment newInstance(FeedModelOnMedia feed, boolean needShowKeyboard) {
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feed);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_NEED_SHOW_KEYBOARD, needShowKeyboard);
        fragment.setArguments(args);
        return fragment;
    }

    public static ReplyCommentFragment newInstance(String url, String urlComment, String rowIdFeed) {
        ReplyCommentFragment fragment = new ReplyCommentFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.ONMEDIA.EXTRAS_DATA, url);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_URL_SUB_COMMENT, urlComment);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_ROW_ID, rowIdFeed);
        fragment.setArguments(args);
        return fragment;
    }

    public ReplyCommentFragment() {

    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof Activity) {
            this.mActivity = (OnMediaActivityNew) context;
            this.mApplication = (ApplicationController) context.getApplicationContext();
            this.mRes = mApplication.getResources();
            rest = new WSOnMedia(mApplication);
            mHandler = new Handler();
            mFeedBusiness = mApplication.getFeedBusiness();
            mContactBusiness = mApplication.getContactBusiness();
            eventOnMediaHelper = new EventOnMediaHelper(mActivity);
        }
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mActivity.getToolBarView() != null) {
            mActivity.getToolBarView().setVisibility(View.GONE);
        }
        if (getArguments() != null) {
            commentParent = mActivity.getFeedAction();
            mFeed = (FeedModelOnMedia) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA);
            urlComment = getArguments().getString(Constants.ONMEDIA.EXTRAS_URL_SUB_COMMENT);
            urlParent = getArguments().getString(Constants.ONMEDIA.EXTRAS_DATA);
            rowid = getArguments().getString(Constants.ONMEDIA.EXTRAS_ROW_ID);
            needShowKeyboard = getArguments().getBoolean(Constants.ONMEDIA.EXTRAS_NEED_SHOW_KEYBOARD);
            if (commentParent != null && mFeed != null) {
                if (TextUtils.isEmpty(urlComment)) {
                    datas.add(commentParent);
                }
            }

        } else {
            mActivity.onBackPressed();
            mActivity.showToast(R.string.e601_error_but_undefined);
        }
    }

    @Override
    public void onDetach() {
        if (mActivity != null) {
            if (didClickLike || lastComment != null) {
                mActivity.notifyAdapterListComment(lastComment);
            }
        }
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        enableClickTag = true;
        Log.i(TAG, "enableClickTag: " + enableClickTag);
    }

    @Override
    public void onStop() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodUtils.hideSoftKeyboard(mActivity);
            }
        });
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_onmedia_comment_status, container, false);
        setActionBar(v, inflater);
        findComponentViews(v, inflater, container);
        initDataView(savedInstanceState);
        if (commentParent != null || !TextUtils.isEmpty(urlComment)) {
            loadComment(TextUtils.isEmpty(urlComment) ? commentParent.getBase64RowId() : urlComment, "");
        } else {
            mProgressBar.setVisibility(View.GONE);
            mLayoutProgress.setVisibility(View.GONE);
            mActivity.showToast(R.string.e601_error_but_undefined);
        }
        setListener();
        return v;
    }

    private void setActionBar(View v, LayoutInflater inflater) {

    }

    private void findComponentViews(View v, LayoutInflater inflater, ViewGroup container) {
        mProgressBar = (ProgressLoading) v.findViewById(R.id.progress_comment);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerViewComment = v.findViewById(R.id.list_comment_onmedia);
        mRecyclerViewComment.setLayoutManager(new LinearLayoutManager(mApplication));

        mSend = (ImageButton) v.findViewById(R.id.person_chat_detail_send_reeng_text);
        mSend.setVisibility(View.VISIBLE);
        ImageView mimgLike = v.findViewById(R.id.img_like_content);
        mimgLike.setVisibility(View.GONE);
        ImageView ivLike = v.findViewById(R.id.img_like_content_ab);
        ivLike.setVisibility(View.GONE);
        ImageView mImgMore = v.findViewById(R.id.img_over_flow);
        mImgMore.setVisibility(View.GONE);
        TextView mTvwTitle = v.findViewById(R.id.ab_title);
        mTvwTitle.setText(mRes.getString(R.string.title_reply_comment));
        mTvwTitle.setTextColor(ContextCompat.getColor(mApplication, R.color.text_ab_title));
        mTvwTitle.setTextSize(17.0f);
        mTvwTitle.setTypeface(mTvwTitle.getTypeface(), Typeface.BOLD);
        mTvwTitle.setOnClickListener(null);
        ImageView mImgBack = v.findViewById(R.id.ab_close);
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });


        mEditText = (TagsCompletionView) v.findViewById(R.id.person_chat_detail_input_text);
        mEditText.setHint(R.string.onmedia_hint_enter_comment);
        ArrayList<PhoneNumber> listPhone = mApplication.getContactBusiness().getListNumberUseMocha();
        if (listPhone == null) listPhone = new ArrayList<>();
        adapterUserTag = new TagOnMediaAdapter(mApplication, listPhone, mEditText);
        mEditText.setAdapter(adapterUserTag);
        if (needShowKeyboard) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodUtils.showSoftKeyboard(mActivity, mEditText);
                    mEditText.requestFocus();
                }
            }, 100);
        }

        mEditText.setThreshold(0);
        adapterUserTag.setListener(new FilteredArrayAdapter.OnChangeItem() {
            @Override
            public void onChangeItem(int count) {
                Log.i(TAG, "onChangeItem: " + count);
                if (count > 2) {
                    int height = mRes.getDimensionPixelOffset(R.dimen.max_height_drop_down_tag);
                    mEditText.setDropDownHeight(height);
                } else
                    mEditText.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLayoutProgress = mFooterView.findViewById(R.id.load_more_layout);
        TextView mLayoutNoCommnet = (TextView) mFooterView.findViewById(R.id.layout_no_comment);
        mLayoutNoCommnet.setVisibility(View.GONE);


        mAdapter = new CommentOnMediaAdapter(mApplication, datas, this, this, this);
        mAdapter.setReplyFragment(true);
        HeaderAndFooterRecyclerViewAdapter wrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        wrapperAdapter.addFooterView(mFooterView);
        mRecyclerViewComment.setAdapter(wrapperAdapter);

        viewFeed = v.findViewById(R.id.view_feed);
        viewFeed.setText(Html.fromHtml(mRes.getString(R.string.back_to_original_post)));
        /*if (TextUtils.isEmpty(rowid)) {
            viewFeed.setVisibility(View.GONE);
        } else
            viewFeed.setVisibility(View.VISIBLE);*/

    }

    private void initDataView(Bundle savedInstanceState) {

    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void setListener() {
        mRecyclerViewComment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewComment.getLayoutManager();
                    if (layoutManager == null) {
                        Log.e(TAG, "null layoutManager");
                        return;
                    }
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading
                            && !isNoMoreComment) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }
            }
        });

        InputMethodUtils.hideKeyboardWhenTouch(mRecyclerViewComment, mActivity);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                    mActivity.showDialogLogin();
                else
                    sendCommentStatus();
//                mActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_comment);
            }
        });

        mSend.setColorFilter(ContextCompat.getColor(mActivity, R.color.gray));
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString())) {
                    mSend.setColorFilter(ContextCompat.getColor(mActivity, R.color.bg_mocha));
                } else {
                    mSend.setColorFilter(ContextCompat.getColor(mActivity, R.color.gray));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        viewFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent onClickIntent = new Intent(mActivity, OnMediaActivityNew.class);
                onClickIntent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
                onClickIntent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, true);
                onClickIntent.putExtra(Constants.ONMEDIA.EXTRAS_ROW_ID, rowid);
                mActivity.startActivity(onClickIntent, true);
            }
        });
    }

    private void onLoadMore() {
        if (!isNoMoreComment) {
            Log.i(TAG, "loaddata onLoadMore " + getLastRowId());
            loadComment(commentParent.getBase64RowId(), getLastRowId());
        } else {
            Log.i(TAG, "loaddata onLoadMore nomorefeed");
            mLayoutProgress.setVisibility(View.GONE);
        }
    }

    private String getLastRowId() {
        if (datas == null || datas.isEmpty()) {
            return "";
        } else {
            return datas.get(datas.size() - 1).getBase64RowId();
        }
    }


    @Override
    public void OnClickUser(String msisdn, String name) {
        Log.i(TAG, "enableClickTag: " + enableClickTag);
        if (enableClickTag) {
            if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                mActivity.showDialogLogin();
            else {
                //ko hieu sao cai clickableSpan bi nhay vao 2 lan lien @@
                enableClickTag = false;
                Log.i(TAG, "msisdn: " + msisdn);
                UserInfo userInfo = new UserInfo();
                userInfo.setMsisdn(msisdn);
                userInfo.setName(name);
                userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                userInfo.setStateMocha(1);
                eventOnMediaHelper.processUserClick(userInfo);
            }
        } else {
            enableClickTag = true;
        }
    }

    @Override
    public void onAvatarClick(UserInfo userInfo) {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin())
            mActivity.showDialogLogin();
        else {
            if (userInfo != null) {
                eventOnMediaHelper
                        .processUserClick(userInfo);
            }
        }
    }

    @Override
    public void onLongClickItem(FeedAction feedAction) {

    }

    @Override
    public void onClickReply(FeedAction feedAction, int pos, boolean needShowKeyboard) {
        handleReplyComment(feedAction);
    }

    private void handleReplyComment(final FeedAction commentReply) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UserInfo userInfo = commentReply.getUserInfo();
                PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(userInfo.getMsisdn());
                if (phoneNumber == null) {
                    phoneNumber = new PhoneNumber();
                    phoneNumber.setJidNumber(userInfo.getMsisdn());
                    phoneNumber.setName(userInfo.getName());
                    phoneNumber.setNameUnicode(userInfo.getName());
                    phoneNumber.setNickName(userInfo.getName());
                }
                mEditText.resetObject();
                mEditText.setSelectedObject(phoneNumber);
                mEditText.addObject(phoneNumber);
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodUtils.showSoftKeyboard(mActivity, mEditText);
                int selection = mEditText.getText().toString().length();
                Log.i(TAG, "selection: " + selection);
                mEditText.setSelection(selection);
                mEditText.requestFocus();
            }
        }, 100);
    }

    @Override
    public void onClickLike(FeedAction feedAction, int pos) {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin())
            mActivity.showDialogLogin();
        else {
            FeedModelOnMedia.ActionLogApp action = feedAction.getIsLike() == 1 ? FeedModelOnMedia.ActionLogApp.LIKE :
                    FeedModelOnMedia.ActionLogApp.UNLIKE;
            didClickLike = true;
            mAdapter.notifyItemChanged(pos);

            FeedContent feedContent = null;
            if (!TextUtils.isEmpty(urlParent) && !TextUtils.isEmpty(urlComment)) {
                feedContent = new FeedContent();
                feedContent.setUrl(urlParent);
            } else {
                try {
                    feedContent = mFeed.getFeedContent().clone();
                } catch (Exception e) {
                    Log.e(TAG, "CloneNotSupportedException", e);
                }
                if (feedContent == null) {
                    mActivity.showToast(R.string.e601_error_but_undefined);
                    return;
                }
            }

            feedContent.setContentAction(FeedContent.CONTENT_ACTION);
            rest.logAppV6(feedContent.getUrl(), commentParent.getBase64RowId(), feedContent, action, "", feedAction
                            .getBase64RowId(),
                    "", null,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i(TAG, "actionLike: onresponse: " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                    int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                    if (code != HTTPCode.E200_OK) {
                                        mActivity.showToast(R.string.e601_error_but_undefined);
                                    } else {
                                    }
                                } else {
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                                mActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    });
        }
    }

    @Override
    public void onClickListLike(FeedAction feedAction) {
        if (feedAction.getNumberLike() > 0) {
            NavigateActivityHelper.navigateToOnMediaLikeOrShare(mActivity, feedAction.getBase64RowId(), true);
        }
    }

    @Override
    public void onClick(View v, int pos, Object object) {

    }

    @Override
    public void onLongClick(View v, int pos, Object object) {

    }


    private void loadComment(String urlAction, final String lastRowId) {
        if (commentParent != null && commentParent.getNumberCmt() == 0) {
            mProgressBar.setVisibility(View.GONE);
            mLayoutProgress.setVisibility(View.GONE);
            return;
        }
        if (mActivity != null) {
            if (!NetworkHelper.isConnectInternet(mActivity)) {
                mActivity.showToast(R.string.no_connectivity_check_again);
                mProgressBar.setVisibility(View.GONE);
                mLayoutProgress.setVisibility(View.GONE);
                return;
            }
            isLoading = true;
            mLayoutProgress.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(lastRowId)) {
                mProgressBar.setVisibility(View.GONE);
            }
            Log.i(TAG, "loadComment fragment comment: " + this.toString() + " | " + Integer.toHexString(System
                    .identityHashCode(this)));
            rest.getListComment(urlAction, lastRowId, new OnMediaInterfaceListener.GetListComment() {
                @Override
                public void onGetListCommentSuccess(ArrayList<FeedAction> listComment,
                                                    ArrayList<UserInfo> listUserLike,
                                                    FeedAction cmtParent,
                                                    long timeServer) {
                    isLoading = false;
                    if (TextUtils.isEmpty(lastRowId) && !TextUtils.isEmpty(urlComment)) {
                        commentParent = cmtParent;
                        datas.add(commentParent);
                        viewFeed.setVisibility(View.VISIBLE);
                    }
                    mProgressBar.setVisibility(View.GONE);
                    mLayoutProgress.setVisibility(View.GONE);
                    loadCommentDone(listComment, lastRowId);
                }

                @Override
                public void onGetListCommentError(int code, String des) {

                }
            }, Integer.toHexString(System.identityHashCode(this)), true);

        }
    }

    private void loadCommentDone(ArrayList<FeedAction> result, String lastRowId) {
        mLayoutProgress.setVisibility(View.GONE);
        if (result != null && !result.isEmpty()) {
            isNoMoreComment = false;
//            Collections.reverse(result);
            datas.addAll(result);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.setDatas(datas);
                    mAdapter.notifyDataSetChanged();
                }
            });
            Log.i(TAG, "load data done: " + datas.size());

        } else {
            Log.i(TAG, "nomore feed");
            isNoMoreComment = true;
        }
    }


    private void sendCommentStatus() {
        String text = mEditText.getText().toString();
        if (text.length() == 0 || TextUtils.isEmpty(text.trim())) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(mActivity)) {
            mActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        /*if (TextHelper.countWords(mEditText.getText().toString()) < Constants.ONMEDIA.MIN_WORDS_COMMENT) {
            mActivity.showToast(R.string.msg_text_minimum);
            return;
        }*/

        Log.i(TAG, "send text: " + mEditText.getText().toString());
        final ArrayList<TagMocha> mListTagFake =
                mFeedBusiness.getListTagFromListPhoneNumber(mEditText.getUserInfo());
        textTag = mFeedBusiness.getTextTag(mListTagFake);
        String messageContent = TextHelper.trimTextOnMedia(mEditText.getTextTag());
        if (mFeedBusiness.checkSpamReplyComment(messageContent, datas,
                mApplication.getReengAccountBusiness().getJidNumber())) {
            mActivity.showToast(R.string.comment_onmedia_fail);
            return;
        }
        mEditText.resetObject();
        Log.i(TAG, "text after getRaw: " + messageContent);
        String idCmtLocal = EncryptUtil.encryptMD5(mApplication.getReengAccountBusiness().getJidNumber()
                + String.valueOf(System.currentTimeMillis()));
        addStatus(messageContent, mListTagFake, idCmtLocal);

        FeedContent feedContent = null;
        if (!TextUtils.isEmpty(urlParent) && !TextUtils.isEmpty(urlComment)) {
            feedContent = new FeedContent();
            feedContent.setUrl(urlParent);
        } else {
            try {
                feedContent = mFeed.getFeedContent().clone();
            } catch (Exception e) {
                Log.e(TAG, "CloneNotSupportedException", e);
            }
            if (feedContent == null) {
                mActivity.showToast(R.string.e601_error_but_undefined);
                return;
            }
        }
        feedContent.setContentAction(FeedContent.CONTENT_ACTION);
        feedContent.setIdCmtLocal(idCmtLocal);
        rest.logAppV6(feedContent.getUrl(), commentParent.getBase64RowId(), feedContent, FeedModelOnMedia
                        .ActionLogApp.COMMENT,
                messageContent, "", textTag, null,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionComment: onresponse: " + response);
                        mProgressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    String base64RowId = jsonObject.optString("base64RowID");
                                    String idCmtLocal = jsonObject.optString("id_cmt_local");
                                    boolean findComment = updateComment(idCmtLocal, base64RowId);
                                    if (findComment) {
                                        Log.i(TAG, "find comment success");
                                        mAdapter.notifyDataSetChanged();
                                    } else {
                                        Log.e(TAG, "cant find comment");
                                    }
                                } else {
                                    mActivity.showToast(R.string.comment_not_success);
                                }
                            } else {
                                mActivity.showToast(R.string.comment_not_success);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            mActivity.showToast(R.string.comment_not_success);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Response error" + volleyError.getMessage());
                        mProgressBar.setVisibility(View.GONE);
                        mActivity.showToast(R.string.comment_not_success);
                    }
                });
    }

    private void addStatus(String content, ArrayList<TagMocha> listTag, String idCmtLocal) {
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        FeedAction commentStatusFeed = new FeedAction(msisdn, content, System.currentTimeMillis(), System
                .currentTimeMillis());
        commentStatusFeed.setReplyCmt(true);
        commentStatusFeed.setIdCmtLocal(idCmtLocal);
        if (!listTag.isEmpty()) {
            commentStatusFeed.setListTag(listTag);
        }
        datas.add(1, commentStatusFeed);
        addComment(commentStatusFeed);

        commentParent.setNumberCmt(commentParent.getNumberCmt() + 1);
        lastComment = commentStatusFeed;
        mAdapter.notifyDataSetChanged();
        mRecyclerViewComment.post(new Runnable() {
            @Override
            public void run() {
                ((LinearLayoutManager) mRecyclerViewComment.getLayoutManager())
                        .scrollToPositionWithOffset(0, 0);
            }
        });

    }

    private HashMap<String, FeedAction> listCommentPosting = new HashMap<>();

    private void addComment(FeedAction comment) {
        listCommentPosting.put(comment.getIdCmtLocal(), comment);
    }

    private boolean updateComment(String idCmtLocal, String base64RowId) {
        if (listCommentPosting.containsKey(idCmtLocal)) {
            FeedAction cmt = listCommentPosting.get(idCmtLocal);
            cmt.setBase64RowId(base64RowId);
            cmt.setIdCmtLocal("");
            listCommentPosting.remove(idCmtLocal);
            return true;
        }
        return false;
    }
}

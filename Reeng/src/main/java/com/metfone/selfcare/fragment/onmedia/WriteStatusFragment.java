package com.metfone.selfcare.fragment.onmedia;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.adapter.ListImageOnMediaAdapter;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.ChannelOnMedia;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.helper.video.Video;
import com.metfone.selfcare.listeners.FilePickerListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 5/31/2017.
 */
public class WriteStatusFragment extends Fragment implements
        FilePickerListener,
        TransferFileBusiness.UploadImageSocialOnMediaListener,
        ListImageOnMediaAdapter.OnListImageChange {

    private static final String TAG = WriteStatusFragment.class.getSimpleName();
    private static final String PREFIX_HTTP = "http";
    private static final String ADD_PREFIX = "://";
    private ApplicationController mApp;
    private OnMediaActivityNew mActivity;
    private Resources mRes;
    private TagOnMediaAdapter adapterUserTag;
    private WSOnMedia wsOnMedia;
    private FeedBusiness mFeedBusiness;
    private RoundedImageView mImgAvatar;
    private TextView mTvwAvatar;
    private TextView mTvwName;
    private TagsCompletionView mEdtStatus;
    private ProgressLoading mProgressLoadItem;
    private View itemNews, itemVideo, itemMovie, itemAudio, itemChannel;
    private RecyclerView itemListImage;
    private ImageView mImgClearItemPreview;
    private View viewAttachImage, viewAttachLink;
    private TextView mImgBack;
    private TextView mTvwAbTitle, mTvwAbPost;
    private ListImageOnMediaAdapter adapter;
    private FeedContent currentFeedContent;
    private int actionWriteStatus;
    private Handler mHandler;
    private boolean enabledPost = true;
    private FeedModelOnMedia currentFeedEdit;
    private boolean enableAutoDetect = true;
    private ArrayList<String> listImg = new ArrayList<>();

    public WriteStatusFragment() {
    }

    public static WriteStatusFragment newInstance(int action, String url, FeedContent feedContent) {
        WriteStatusFragment writeStatusFragment = new WriteStatusFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ONMEDIA.EXTRAS_ACTION, action);
        args.putString(Constants.ONMEDIA.EXTRAS_DATA, url);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_CONTENT_DATA, feedContent);
        writeStatusFragment.setArguments(args);
        return writeStatusFragment;
    }

    public static WriteStatusFragment newInstance(ArrayList<String> listImage) {
        WriteStatusFragment writeStatusFragment = new WriteStatusFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_IMAGE);
        args.putStringArrayList(Constants.ONMEDIA.EXTRAS_LIST_IMAGE, listImage);
        writeStatusFragment.setArguments(args);
        return writeStatusFragment;
    }

    public static WriteStatusFragment newInstance(int action, FeedModelOnMedia feedModelOnMedia) {
        WriteStatusFragment writeStatusFragment = new WriteStatusFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ONMEDIA.EXTRAS_ACTION, action);
        args.putSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feedModelOnMedia);
        writeStatusFragment.setArguments(args);
        return writeStatusFragment;
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof Activity) {
            mActivity = (OnMediaActivityNew) context;
            mApp = (ApplicationController) mActivity.getApplication();
            mRes = mApp.getResources();
            wsOnMedia = new WSOnMedia(mApp);
            mFeedBusiness = mApp.getFeedBusiness();
            mActivity.setFilePickerListener(this);
            mHandler = new Handler();
        }
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_onmedia_write_status, container, false);
        setActionBar(inflater);
        findComponentViews(v, inflater, container);
        initDataView(savedInstanceState);
        setListener();
        getActionWriteStatus();
        return v;
    }

    private void getActionWriteStatus() {
        String url = "";
        FeedContent feedContent = null;
        if (getArguments() != null) {
            actionWriteStatus = getArguments().getInt(Constants.ONMEDIA.EXTRAS_ACTION);
            url = getArguments().getString(Constants.ONMEDIA.EXTRAS_DATA);
            feedContent = (FeedContent) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_CONTENT_DATA);
            currentFeedEdit = (FeedModelOnMedia) getArguments().getSerializable(Constants.ONMEDIA.EXTRAS_FEEDS_DATA);
            listImg = getArguments().getStringArrayList(Constants.ONMEDIA.EXTRAS_LIST_IMAGE);
            Log.i(TAG, "url: " + url);
        }

        if (currentFeedEdit != null) {
            feedContent = currentFeedEdit.getFeedContent();
        }

        if (feedContent != null) {
            String itemType = feedContent.getItemType();
            String itemSubType = feedContent.getItemSubType();
            String contentUrl = feedContent.getLink();

            if (itemType.equals(FeedContent.ITEM_TYPE_NEWS) ||
                    (itemType.equals(FeedContent.ITEM_TYPE_SOCIAL) && itemSubType.equals(FeedContent
                            .ITEM_SUB_TYPE_SOCIAL_LINK))) {
                try {
                    currentFeedContent = feedContent.clone();
                } catch (CloneNotSupportedException e) {
                    Log.e(TAG, "CloneNotSupportedException", e);
                    currentFeedContent = feedContent;
                }
                currentFeedContent.setContentUrl(contentUrl);
                currentFeedContent.setCountLike(0);
                currentFeedContent.setCountComment(0);
                currentFeedContent.setCountShare(0);
                showPreviewFeedNews();
            } else if (OnMediaHelper.isFeedViewVideo(feedContent)) {
                try {
                    currentFeedContent = feedContent.clone();
                } catch (CloneNotSupportedException e) {
                    Log.e(TAG, "CloneNotSupportedException", e);
                    currentFeedContent = feedContent;
                }
                currentFeedContent.setContentUrl(contentUrl);
                currentFeedContent.setCountLike(0);
                currentFeedContent.setCountComment(0);
                currentFeedContent.setCountShare(0);
                showPreviewFeedVideo();
            } else if (OnMediaHelper.isFeedViewMovie(feedContent)) {
                try {
                    currentFeedContent = feedContent.clone();
                } catch (CloneNotSupportedException e) {
                    Log.e(TAG, "CloneNotSupportedException", e);
                    currentFeedContent = feedContent;
                }
                currentFeedContent.setContentUrl(contentUrl);
                currentFeedContent.setCountLike(0);
                currentFeedContent.setCountComment(0);
                currentFeedContent.setCountShare(0);
                showPreviewFeedMovie();
            } else if (OnMediaHelper.isFeedViewAudio(feedContent)) {
                try {
                    currentFeedContent = feedContent.clone();
                } catch (CloneNotSupportedException e) {
                    Log.e(TAG, "CloneNotSupportedException", e);
                    currentFeedContent = feedContent;
                }
                currentFeedContent.setContentUrl(contentUrl);
                currentFeedContent.setCountLike(0);
                currentFeedContent.setCountComment(0);
                currentFeedContent.setCountShare(0);
                showPreviewFeedAudio();
            } else if (OnMediaHelper.isFeedViewChannel(feedContent)) {
                try {
                    currentFeedContent = feedContent.clone();
                } catch (CloneNotSupportedException e) {
                    Log.e(TAG, "CloneNotSupportedException", e);
                    currentFeedContent = feedContent;
                }
                currentFeedContent.setContentUrl(contentUrl);
                currentFeedContent.setCountLike(0);
                currentFeedContent.setCountComment(0);
                currentFeedContent.setCountShare(0);
                showPreviewFeedChannel();
            } else {
                if (!TextUtils.isEmpty(contentUrl)) {
                    Log.d(TAG, "contentUrl: " + contentUrl);
                }
            }
        } else {
            if (actionWriteStatus == Constants.ONMEDIA.ACTION_ATTACH_IMAGE) {
                if (listImg != null && !listImg.isEmpty()) {
                    transferPickedPhoto(listImg);
                } else {
                    mHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mActivity.dispatchPickPictureIntent(new ArrayList<String>());
                        }
                    }, 400);
                }
            } else if (actionWriteStatus == Constants.ONMEDIA.ACTION_ATTACH_LINK) {
                if (TextUtils.isEmpty(url)) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            handleAttachLink();
                        }
                    }, 400);
                } else {
                    checkUrlAndGetMetaData(url);
                }
            }
        }


        /*if (!TextUtils.isEmpty(url)) {
            onIconClickListener(null, url, Constants.MENU.POPUP_SAVE);
        }*/
    }

    private void findComponentViews(View v, LayoutInflater inflater, ViewGroup container) {
        mImgAvatar = v.findViewById(R.id.img_onmedia_avatar);
        mTvwAvatar = v.findViewById(R.id.tvw_onmedia_avatar);
        mTvwName = v.findViewById(R.id.tvw_my_name);
        mEdtStatus = v.findViewById(R.id.edt_write_status);
        itemNews = v.findViewById(R.id.view_feed_news);
        itemVideo = v.findViewById(R.id.view_feed_video);
        itemMovie = v.findViewById(R.id.view_feed_movie);
        itemAudio = v.findViewById(R.id.view_feed_audio);
        itemChannel = v.findViewById(R.id.view_feed_channel);

        itemListImage = v.findViewById(R.id.recycler_view_list_img);
        itemListImage.setLayoutManager(new LinearLayoutManager(mApp, LinearLayoutManager.HORIZONTAL, false));
        adapter = new ListImageOnMediaAdapter(mApp, this);
        itemListImage.setAdapter(adapter);
        mProgressLoadItem = v.findViewById(R.id.progress_load_item);
        mImgClearItemPreview = v.findViewById(R.id.img_clear_item);
        viewAttachImage = v.findViewById(R.id.view_attach_image);
        viewAttachLink = v.findViewById(R.id.view_attach_link);

        ArrayList<PhoneNumber> listPhone = mApp.getContactBusiness().getListNumberUseMocha();
        if (listPhone == null) listPhone = new ArrayList<>();
        adapterUserTag = new TagOnMediaAdapter(mApp, listPhone, mEdtStatus);
        mEdtStatus.setAdapter(adapterUserTag);
        mEdtStatus.setThreshold(0);

        mApp.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatar,
                mApp.getReengAccountBusiness().getCurrentAccount(), null);
        mTvwName.setText(mApp.getReengAccountBusiness().getUserName());
    }

    private void initDataView(Bundle savedInstanceState) {

    }

    private void setListener() {
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        viewAttachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchPickPictureIntent();
            }
        });
        viewAttachLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleAttachLink();
            }
        });

        mTvwAbPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!enabledPost) return;
                if (currentFeedContent != null && currentFeedContent.getItemSubType().equals(FeedContent
                        .ITEM_SUB_TYPE_SOCIAL_IMAGE)) {
                    mActivity.showLoadingDialog("", R.string.text_uploading);
                    mApp.getTransferFileBusiness().uploadImageSocialOnMedia(adapter.getListImg(), WriteStatusFragment
                            .this);
                    setEnablePost(false);
                } else {
                    handlePostOnMedia();
                }
            }
        });

        mImgClearItemPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImgClearItemPreview.setVisibility(View.GONE);
                itemListImage.setVisibility(View.GONE);
                itemVideo.setVisibility(View.GONE);
                itemNews.setVisibility(View.GONE);
                itemMovie.setVisibility(View.GONE);
                itemAudio.setVisibility(View.GONE);
                itemChannel.setVisibility(View.GONE);
                currentFeedContent = null;
                if (!validPostStatus()) setEnablePost(false);
            }
        });

        mEdtStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!validPostStatus() && currentFeedContent == null) {
                    setEnablePost(false);
                } else {
                    setEnablePost(true);
                    if (enableAutoDetect) {
                        checkTextToGetMetadata(s.toString());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEdtStatus.setTextMenuSelect(new TagsCompletionView.TextMenuSelect() {
            @Override
            public void onMenuPaste() {
                Log.i(TAG, "onMenuPaste: " + mEdtStatus.getText().toString());
                if (enableAutoDetect && mEdtStatus.getText() != null) {
                    String s = mEdtStatus.getText().toString();
                    if (!TextUtils.isEmpty(s) && Patterns.WEB_URL.matcher(s).matches()) {
                        enableAutoDetect = false;
                        handleGetMetaData(s);
                    }
                }
            }
        });
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE) {
//            if (PermissionHelper.allowedPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                    && PermissionHelper.allowedPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                dispatchPickPictureIntent();
//                mApp.initFolder();
//            }
//        }
//    }

    public void dispatchPickPictureIntent() {
        mActivity.dispatchPickPictureIntent(adapter.getListImg());
    }

    private void checkTextToGetMetadata(String s) {
        if (s.endsWith(" ")) {
            try {
                String[] splited = s.split("\\s+");
                if (splited.length > 0) {
                    for (String aSplited : splited) {
                        if (Patterns.WEB_URL.matcher(aSplited).matches()) {
                            enableAutoDetect = false;
                            handleGetMetaData(aSplited);
                            return;
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }

        }
    }

    private void handleAttachLink() {
        String urlClipboard = TextHelper.getTextFromClipboard(mApp).trim();
        Log.i(TAG, "urlClipboard: " + urlClipboard);
        if (!urlClipboard.startsWith(PREFIX_HTTP)) {
            urlClipboard = PREFIX_HTTP + ADD_PREFIX + urlClipboard;
        }
        if (!Patterns.WEB_URL.matcher(urlClipboard).matches()) {
            urlClipboard = "";
        }
        DialogEditText dialog = new DialogEditText(mActivity, true)
                .setCheckEnable(false)
                .setLabel(mRes.getString(R.string.title_enter_url))
                .setTextHint(mRes.getString(R.string.hint_enter_url))
                .setNegativeLabel(mRes.getString(R.string.cancel))
                .setPositiveLabel(mRes.getString(R.string.ok))
                .setOldContent(urlClipboard)
                .setMaxLength(-1)
                .setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String url) {
                        Log.i(TAG, "url: " + url);
                        checkUrlAndGetMetaData(url);
                    }
                }).setDismissListener(new DismissListener() {
                    @Override
                    public void onDismiss() {

                    }
                });
        if (mActivity != null && !mActivity.isFinishing()) {
            dialog.show();
        }
    }

    private void checkUrlAndGetMetaData(String url) {
        if (!url.startsWith(PREFIX_HTTP)) {
            url = PREFIX_HTTP + ADD_PREFIX + url;
        }
        if (URLUtil.isValidUrl(url)) {
            handleGetMetaData(url);
        } else {
            mActivity.showToast(R.string.onmedia_invalid_url);
        }
    }

    private void handlePostOnMedia() {
        /*if (text.length() == 0 || TextUtils.isEmpty(text.trim())) {
            return;
        }*/
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodUtils.hideSoftKeyboard(mActivity);
            }
        });

        if (!NetworkHelper.isConnectInternet(mActivity)) {
            mActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }

        Log.i(TAG, "send text: " + mEdtStatus.getText().toString());
        final ArrayList<TagMocha> mListTagFake =
                mFeedBusiness.getListTagFromListPhoneNumber(mEdtStatus.getUserInfo());
//        String textTag = mFeedBusiness.getTextTag(mListTagFake);
        String messageContent = TextHelper.trimTextOnMedia(mEdtStatus.getTextTag());
//        mEdtStatus.resetObject();
        if (currentFeedContent != null) {

        } else {
            currentFeedContent = new FeedContent();
            currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
            currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS);
        }
        String urlKey;
        FeedModelOnMedia.ActionLogApp actionLogApp;
        String rowId = "";
        if (currentFeedEdit != null) {
            FeedContent feedContent = currentFeedEdit.getFeedContent();
            urlKey = feedContent.getUrl();
            actionLogApp = FeedModelOnMedia.ActionLogApp.EDIT;
            rowId = currentFeedEdit.getBase64RowId();
        } else {
            urlKey = mFeedBusiness.generateUrlSocial();
            actionLogApp = FeedModelOnMedia.ActionLogApp.POST;
        }
        currentFeedContent.setContentStatus(messageContent);
        currentFeedContent.setContentListTag(mListTagFake);
        currentFeedContent.setUrl(urlKey);
        if (currentFeedEdit != null) {
            currentFeedEdit.setFeedContent(currentFeedContent);
        }

        Log.i(TAG, "text after getRaw: " + messageContent);

        mTvwAbPost.setEnabled(false);
        mActivity.showLoadingDialog("", R.string.loading);
        wsOnMedia.logAppV6(urlKey, "", currentFeedContent, actionLogApp,
                "", rowId, "", null,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "handlePostOnMedia: onresponse: " + response);
//                        mProgressBar.setVisibility(View.GONE);
                        mActivity.hideLoadingDialog();
                        mTvwAbPost.setEnabled(true);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    handlePostSuccess();
                                } else {
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                                }
                            } else {
                                mActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            mActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Response error" + volleyError.getMessage());
                        mActivity.hideLoadingDialog();
                        mTvwAbPost.setEnabled(true);
//                        mProgressBar.setVisibility(View.GONE);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void handlePostSuccess() {
        if (currentFeedContent != null) {
            if (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(currentFeedContent.getItemSubType())) {
                LuckyWheelHelper.getInstance(mApp).doMission(Constants.LUCKY_WHEEL.ITEM_SHARE_ONMEDIA);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(currentFeedContent.getItemSubType())) {
                LuckyWheelHelper.getInstance(mApp).doMission(Constants.LUCKY_WHEEL.ITEM_UPLOAD_ALBUM);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(currentFeedContent.getItemSubType())) {
                LuckyWheelHelper.getInstance(mApp).doMission(Constants.LUCKY_WHEEL.ITEM_STATUS);
            }
        }
        FeedModelOnMedia feedModelOnMedia;
        if (currentFeedEdit != null && currentFeedContent != null && !TextUtils.isEmpty(currentFeedContent.getUrl())) {
            FeedModelOnMedia feedDuplicate = mFeedBusiness.getFeedModelFromUrl(currentFeedContent.getUrl());
            if (feedDuplicate != null) {
                mFeedBusiness.getListFeed().remove(feedDuplicate);
            }
            feedModelOnMedia = currentFeedEdit;
            feedModelOnMedia.setActionType(FeedModelOnMedia.ActionLogApp.EDIT);
        } else {
            feedModelOnMedia = new FeedModelOnMedia();
            feedModelOnMedia.setFeedContent(currentFeedContent);
            feedModelOnMedia.setIsLike(0);
            feedModelOnMedia.setIsShare(0);

            feedModelOnMedia.setBase64RowId("");
            long countShare = feedModelOnMedia.getFeedContent().getCountShare();
            feedModelOnMedia.getFeedContent().setCountShare(countShare + 1);
            UserInfo mUserInfo = new UserInfo(
                    mApp.getReengAccountBusiness().getJidNumber(),
                    mApp.getReengAccountBusiness().getUserName());
            feedModelOnMedia.setUserInfo(mUserInfo);
            feedModelOnMedia.setActionType(FeedModelOnMedia.ActionLogApp.POST);
            feedModelOnMedia.getFeedContent().setUserInfo(mUserInfo);
            feedModelOnMedia.getFeedContent().setStamp(System.currentTimeMillis());
        }
        feedModelOnMedia.setTimeStamp(System.currentTimeMillis());
        feedModelOnMedia.setTimeServer(System.currentTimeMillis());

        ListenerHelper.getInstance().onPostFeedSuccess(feedModelOnMedia);
        mActivity.finish();
    }

    public void setActionBar(LayoutInflater inflater) {
        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.app_bar_onmedia_status, null));
        mActivity.getToolBarView().setVisibility(View.VISIBLE);
        View abView = mActivity.getToolBarView();
        mImgBack =  abView.findViewById(R.id.tvBack);
        mTvwAbTitle =  abView.findViewById(R.id.tv_title);
        mTvwAbTitle.setText(mRes.getString(R.string.new_posts));
        mTvwAbPost =  abView.findViewById(R.id.tvPost);
        mTvwAbPost.setTextColor(ContextCompat.getColorStateList(mApp, R.color.text_color_purple));
        mTvwAbPost.setTypeface(mTvwAbPost.getTypeface(), Typeface.BOLD);
        mTvwAbPost.setText(mRes.getString(R.string.post_cap));
        setEnablePost(false);
    }

    private void setEnablePost(boolean enable) {
        if (enable && enabledPost) return;
        if (!enable && !enabledPost) return;
        enabledPost = enable;
        mTvwAbPost.setEnabled(enable);
        /*if (enable) {
            mTvwAbPost.setTextColor(ContextCompat.getColor(mApp, R.color.text_ab_title));
        } else {
            mTvwAbPost.setTextColor(ContextCompat.getColor(mApp, R.color.gray));
        }*/
    }

    private void handleGetMetaData(String url) {
        mProgressLoadItem.setVisibility(View.VISIBLE);
        wsOnMedia.getMetaData(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                mProgressLoadItem.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has("code")) {
                        int code = Integer.parseInt(jsonObject.getString("code"));
                        if (code == HTTPCode.E200_OK) {
                            if (jsonObject.has("content")) {
                                String jsonContent = jsonObject.getString("content");
                                Log.i(TAG, "content: " + jsonContent);
                                FeedContent feedContent = new Gson().fromJson(jsonContent, FeedContent.class);
                                if (feedContent != null) {
                                    Log.i(TAG, "feedcontent: ok");
                                    currentFeedContent = feedContent;
                                    currentFeedContent.setContentUrl(feedContent.getUrl());
                                    if (OnMediaHelper.isFeedViewVideo(currentFeedContent)) {
                                        showPreviewFeedVideo();
                                    } else if (OnMediaHelper.isFeedViewMovie(currentFeedContent)) {
                                        showPreviewFeedMovie();
                                    } else if (OnMediaHelper.isFeedViewAudio(currentFeedContent)) {
                                        showPreviewFeedAudio();
                                    } else if (OnMediaHelper.isFeedViewChannel(currentFeedContent)) {
                                        showPreviewFeedChannel();
                                    } else {
                                        showPreviewFeedNews();
                                    }
                                }
//                                else {
////                                    mActivity.showToast(R.string.e601_error_but_undefined);
//                                }
                            }
//                            else {
////                                mActivity.showToast(R.string.e601_error_but_undefined);
//                            }
                        }
//                        else {
//                            /*if (jsonObject.has("desc")) {
//                                String desc = jsonObject.getString("desc");
//                                mActivity.showToast(desc, Toast.LENGTH_SHORT);
//                            } else {
//                                mActivity.showToast(R.string.e601_error_but_undefined);
//                            }*/
//                        }
                    }
//                    else {
////                        mActivity.showToast(R.string.e601_error_but_undefined);
//                    }
                } catch (Exception e) {
                    Log.e(TAG, "getMetaDataFromServer parse error", e);
//                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
                mProgressLoadItem.setVisibility(View.GONE);
//                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        }, true);
    }

    private void showPreviewFeedNews() {
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK);
        itemNews.setVisibility(View.VISIBLE);
        itemVideo.setVisibility(View.GONE);
        itemMovie.setVisibility(View.GONE);
        itemAudio.setVisibility(View.GONE);
        itemChannel.setVisibility(View.GONE);
        itemListImage.setVisibility(View.GONE);
        mImgClearItemPreview.setVisibility(View.VISIBLE);

        ImageView ivCover = itemNews.findViewById(R.id.iv_cover_news);
        TextView tvTitle = itemNews.findViewById(R.id.tv_title_news);
        TextView tvSite = itemNews.findViewById(R.id.tv_site_news);

        String imageUrl = currentFeedContent.getImageUrl();
        String title = currentFeedContent.getItemName();
        String site = currentFeedContent.getSite();

        if (TextUtils.isEmpty(title)) {
            title = site;
        }
//        if (TextUtils.isEmpty(imageUrl)) {
//            tvTitle.setMaxLines(1);
//            tvTitle.setVisibility(View.GONE);
//        } else {
//            tvTitle.setVisibility(View.VISIBLE);
        int maxLine = mRes.getInteger(R.integer.max_line_title_onmedia);
        tvTitle.setMaxLines(maxLine);
        int width = mRes.getDimensionPixelOffset(R.dimen.width_thumb_news);
        int height = mRes.getDimensionPixelOffset(R.dimen.height_thumb_news);
        ImageLoaderManager.getInstance(mActivity).setImageFeeds(ivCover, imageUrl, width, height);
//        }
        tvTitle.setText(title);
        tvSite.setText(site);
        setEnablePost(true);
    }

    private void showPreviewFeedVideo() {
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_VIDEO);
        itemNews.setVisibility(View.GONE);
        itemVideo.setVisibility(View.VISIBLE);
        itemMovie.setVisibility(View.GONE);
        itemAudio.setVisibility(View.GONE);
        itemChannel.setVisibility(View.GONE);
        itemListImage.setVisibility(View.GONE);
        mImgClearItemPreview.setVisibility(View.VISIBLE);
        ImageView ivCover = itemVideo.findViewById(R.id.iv_cover_video);
        int width = ScreenManager.getWidth(mActivity);
        int height = width * 9 / 16;
        ImageLoaderManager.getInstance(mActivity).setImageFeeds(ivCover, currentFeedContent.getImageUrl(), width, height);
        TextView tvTitle = itemVideo.findViewById(R.id.tv_title_video);
        tvTitle.setText(currentFeedContent.getItemName());
        setEnablePost(true);
    }

    private void showPreviewFeedMovie() {
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_MOVIE);
        itemNews.setVisibility(View.GONE);
        itemVideo.setVisibility(View.GONE);
        itemMovie.setVisibility(View.VISIBLE);
        itemAudio.setVisibility(View.GONE);
        itemChannel.setVisibility(View.GONE);
        itemListImage.setVisibility(View.GONE);
        mImgClearItemPreview.setVisibility(View.VISIBLE);
        ImageView ivCover = itemMovie.findViewById(R.id.iv_cover_movie);
        int width = ScreenManager.getWidth(mActivity);
        int height = width * 9 / 16;
        ImageLoaderManager.getInstance(mActivity).setImageFeeds(ivCover, currentFeedContent.getImageUrl(), width, height);
        TextView tvTitle = itemMovie.findViewById(R.id.tv_title_movie);
        tvTitle.setText(currentFeedContent.getItemName());
        ImageView ivPoster = itemMovie.findViewById(R.id.iv_poster_movie);
        ImageBusiness.setPosterMovie(currentFeedContent.getPosterUrl(), ivPoster);
        setEnablePost(true);
    }

    private void showPreviewFeedAudio() {
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_AUDIO);
        itemNews.setVisibility(View.GONE);
        itemVideo.setVisibility(View.GONE);
        itemMovie.setVisibility(View.GONE);
        itemAudio.setVisibility(View.VISIBLE);
        itemListImage.setVisibility(View.GONE);
        mImgClearItemPreview.setVisibility(View.VISIBLE);
        ImageView ivCover = itemAudio.findViewById(R.id.iv_cover_audio);
        ImageBusiness.setImageBlur(ivCover, currentFeedContent.getImageUrl());
        TextView tvTitle = itemAudio.findViewById(R.id.tv_title_audio);
        tvTitle.setText(currentFeedContent.getItemName());
        ImageView ivAvatar = itemAudio.findViewById(R.id.iv_avatar_audio);
        int width = mRes.getDimensionPixelSize(R.dimen.size_thumb_song);
        ImageLoaderManager.getInstance(mActivity).setImageFeedsSong(ivAvatar, currentFeedContent.getImageUrl(), width, width);
        setEnablePost(true);
    }

    private void showPreviewFeedChannel() {
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_CHANNEL);
        itemNews.setVisibility(View.GONE);
        itemVideo.setVisibility(View.GONE);
        itemMovie.setVisibility(View.GONE);
        itemAudio.setVisibility(View.GONE);
        itemChannel.setVisibility(View.VISIBLE);
        itemListImage.setVisibility(View.GONE);
        mImgClearItemPreview.setVisibility(View.VISIBLE);
        ChannelOnMedia channel = currentFeedContent.getChannel();
        if (channel != null) {
            TextView tvTitle = itemChannel.findViewById(R.id.tv_title_channel);
            tvTitle.setText(channel.getName());
            TextView tvNumFollow = itemChannel.findViewById(R.id.tv_number_follow_channel);
            if (channel.getNumFollow() <= 0) {
                tvNumFollow.setVisibility(View.INVISIBLE);
                tvNumFollow.setText("");
            } else {
                tvNumFollow.setVisibility(View.VISIBLE);
                tvNumFollow.setText(String.format(getString(R.string.people_subscription), com.metfone.selfcare.util.Utilities.shortenLongNumber(channel.getNumFollow())));
            }
            ImageView ivAvatar = itemChannel.findViewById(R.id.iv_avatar_channel);
            ImageBusiness.setAvatarChannelOnFeed(ivAvatar, channel.getAvatarUrl());
            ImageView ivCover = itemChannel.findViewById(R.id.iv_cover_channel);
            ImageBusiness.setCoverChannelOnFeed(ivCover, channel.getCoverUrl());
        }
        setEnablePost(true);
    }

    public boolean canBackPress() {
        Log.i(TAG, "canBackPress");
        if (!enabledPost) {
            return true;
        } else {
            new DialogConfirm(mActivity, true).setLabel(null).setMessage(mRes.getString(R.string
                    .onmedia_alert_save_content))
                    .setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok))
                    .setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            mActivity.finish();
                        }
                    }).show();
            return false;
        }
    }

    @Override
    public void transferPickedFile(String filePath, String fileName, String extension) {

    }

    @Override
    public void transferPickedPhoto(ArrayList<String> filePath) {
        //TODO preview image
        Log.i(TAG, "size file: " + filePath.size());
        mImgClearItemPreview.setVisibility(View.GONE);
        itemListImage.setVisibility(View.VISIBLE);
        itemNews.setVisibility(View.GONE);
        itemVideo.setVisibility(View.GONE);
        itemMovie.setVisibility(View.GONE);
        itemAudio.setVisibility(View.GONE);
        adapter.setListImg(filePath);
        adapter.notifyDataSetChanged();
        currentFeedContent = new FeedContent();
        currentFeedContent.setItemType(FeedContent.ITEM_TYPE_SOCIAL);
        currentFeedContent.setItemSubType(FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE);
        setEnablePost(true);
    }

    @Override
    public void transferTakenPhoto(String filePath) {

    }

    @Override
    public void transferSelectedContact(ArrayList<PhoneNumber> lstPhone) {

    }

    @Override
    public void transferSelectedDocument(DocumentClass document) {

    }

    @Override
    public void transferTakenVideo1(String filePath, String fileName, int duration, int fileSize, Video v) {

    }

    @Override
    public void transferShareLocation(String content, String latitude, String longitude) {

    }

    @Override
    public void sendVideo(String filePath, String fileName, int duration, int fileSize) {

    }

    @Override
    public void onUploadCompleted(ArrayList<String> response) {
        if (response != null && !response.isEmpty()) {
            Log.i(TAG, "response upload: " + response.size());
            for (int i = 0; i < response.size(); i++) {
                try {
                    String resp = response.get(i);
                    JSONObject jsonObject = new JSONObject(resp);
                    String idImg = jsonObject.optString("desc");
                    String linkImg = jsonObject.optString("link");
                    String thumb = jsonObject.optString("thumb");
                    thumb = TextUtils.isEmpty(thumb) ? linkImg : thumb;
                    float ratio = BigDecimal.valueOf(jsonObject.getDouble("ratio")).floatValue();
                    currentFeedContent.addImageContent(idImg, linkImg, thumb, ratio);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }

            }
            if (!currentFeedContent.getListImage().isEmpty()) {
                handlePostOnMedia();
            } else {
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        }

    }

    @Override
    public void onUploadFailed(int code, String msg, ArrayList<String> listSuccess) {
        Log.e(TAG, "onUploadFailed: " + code + " " + msg);
    }

    @Override
    public void onRemoveImage() {
        if (adapter.getListImg() == null || adapter.getListImg().isEmpty()) {
            currentFeedContent = null;
            if (!validPostStatus()) {
                setEnablePost(false);
            }
        }
    }

    private boolean validPostStatus() {
        String s = mEdtStatus.getText().toString();
        if (!TextUtils.isEmpty(s)) s = s.trim();
        return !TextUtils.isEmpty(s);
    }
}

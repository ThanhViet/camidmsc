package com.metfone.selfcare.fragment.sharelocation;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.ShareLocationActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

/**
 * Created by toanvk2 on 19/05/2015.
 */
public class ShareLocationFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleMap.OnCameraChangeListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener,
        ClickListener.IconListener,
        PermissionHelper.RequestPermissionsResult {
    private static final String TAG = ShareLocationFragment.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private ShareLocationActivity mParentActivity;
    private ApplicationController mApplication;
    private OnFragmentAppInfoListener mListener;
    private Handler mHandler;
    private Resources mRes;
    private int mType;// =1 send. =2 la received
    private String mReceivedAddress, mReceivedLatitude, mReceivedLongitude;
    private Marker mReceivedMarker;
    private LatLng mReceivedLatLng;
    private Location myLocation;
    private View mActionBarView;
    private ArrayList<ItemContextMenu> listItemOverFlow;
    private ClickListener.IconListener mClickHandler;
    private View rootView, markerInfoView;
    //
    private EllipsisTextView mTvwTitle;
    private TextView mTvwContinue;
    private ImageView mImgBack;
    private ImageView mImgOption;
    private GoogleMap mMap;
    private LatLng mCurrentLatLngCenter;
    private String mCurrentAddress;
    private LocationManager mLocationManager;
    private SupportMapFragment mapFragment;
    private ConstraintLayout mLlCenterMarkerLayout;
    private TextView mTvwCenterMarkerTitle, mTvwCenterMarkerAddress;
    private ProgressLoading mPgbCenterMarkerLoading;
    private ImageView mImgCenterMakerIcon, mImgFriendLocation;
    private getCurrentAddressAsyncTask mGetCurrentAddressAsyncTask;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private boolean updateMyLocation = false;

    View mapView;

    public ShareLocationFragment() {
    }

    public static ShareLocationFragment newInstance(int type, String address, String latitude, String longitude) {
        ShareLocationFragment fragment = new ShareLocationFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.LOCATION.DATA_INPUT_TYPE, type);
        bundle.putString(Constants.LOCATION.DATA_ADDRESS, address);
        bundle.putString(Constants.LOCATION.DATA_LATITUDE, latitude);
        bundle.putString(Constants.LOCATION.DATA_LONGITUDE, longitude);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        initGoogleApiClient();
        if (!hasPermissionLocation()) {
            showDialogRequestPermission();
        }
        changerColorStatusbar();
    }
    private void changerColorStatusbar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR|View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            getActivity().getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (ShareLocationActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mLocationManager = (LocationManager) mParentActivity.getSystemService(Context.LOCATION_SERVICE);
        mRes = mParentActivity.getResources();
        initOverFlowMenu();
        try {
            mListener = (OnFragmentAppInfoListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        PermissionHelper.removeCallBack(this);
        mListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_share_location, container, false);
        getData(saveInstanceState);
        findComponentViews(rootView, inflater);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        setListener();
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.LOCATION.DATA_INPUT_TYPE, mType);
        outState.putString(Constants.LOCATION.DATA_ADDRESS, mReceivedAddress);
        outState.putString(Constants.LOCATION.DATA_LATITUDE, mReceivedLatitude);
        outState.putString(Constants.LOCATION.DATA_LONGITUDE, mReceivedLongitude);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        mClickHandler = this;
        //addLocationChangeListener();
        connectLocationUpdates();
        mHandler = new Handler();
        super.onResume();
    }

    @SuppressWarnings({"MissingPermission"})
    private void connectLocationUpdates() {
        if (mGoogleApiClient == null) {
            initGoogleApiClient();
        }
        if (mGoogleApiClient.isConnected()) {
            if (hasPermissionLocation()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } else {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    public void onPause() {
        if (mGetCurrentAddressAsyncTask != null) {
            mGetCurrentAddressAsyncTask.cancel(true);
            mGetCurrentAddressAsyncTask = null;
        }
        mClickHandler = null;
        //mLocationManager.removeUpdates(this);
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        mHandler = null;
        super.onPause();
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getData(Bundle saveInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mType = bundle.getInt(Constants.LOCATION.DATA_INPUT_TYPE);
            mReceivedAddress = bundle.getString(Constants.LOCATION.DATA_ADDRESS);
            mReceivedLatitude = bundle.getString(Constants.LOCATION.DATA_LATITUDE);
            mReceivedLongitude = bundle.getString(Constants.LOCATION.DATA_LONGITUDE);
        } else if (saveInstanceState != null) {
            mType = saveInstanceState.getInt(Constants.LOCATION.DATA_INPUT_TYPE);
            mReceivedAddress = saveInstanceState.getString(Constants.LOCATION.DATA_ADDRESS);
            mReceivedLatitude = saveInstanceState.getString(Constants.LOCATION.DATA_LATITUDE);
            mReceivedLongitude = saveInstanceState.getString(Constants.LOCATION.DATA_LONGITUDE);
        }
    }

    private boolean hasPermissionLocation() {
        return !(PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private void findComponentViews(View view, LayoutInflater inflater) {
        // maps
        mapFragment = (SupportMapFragment) getChildFragmentManager().
                findFragmentById(R.id.fragment_send_location_map_layout);
        mLlCenterMarkerLayout = view.findViewById(R.id.marker_option_layout);
        mTvwCenterMarkerTitle = view.findViewById(R.id.marker_option_title);
        mTvwCenterMarkerAddress = view.findViewById(R.id.marker_option_content);
        mPgbCenterMarkerLoading = view.findViewById(R.id.marker_option_progress);
        mImgCenterMakerIcon = view.findViewById(R.id.fragment_send_location_marker_center_icon);
        mImgFriendLocation = view.findViewById(R.id.fragment_send_location_friend_btn);
        // action bar
//        mActionBar = mParentActivity.getSupportActionBar();
        mActionBarView = mParentActivity.getmToolBarView();
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.chat_header_share_location, null));
//        mActionBar.setCustomView(mActionBarView);
        mTvwTitle = mActionBarView.findViewById(R.id.ab_title);
        mImgBack = mActionBarView.findViewById(R.id.ab_back_btn);
        mImgOption = mActionBarView.findViewById(R.id.ab_more_btn);
        mTvwContinue = mActionBarView.findViewById(R.id.ab_agree_text);
        //
        markerInfoView = inflater.inflate(R.layout.marker_option_layout, null);
//        mTvwContinue.setText(mRes.getString(R.string.send_location));
        mTvwContinue.setText(R.string.send_location);
        mTvwContinue.setEnabled(false);
        mLlCenterMarkerLayout.setEnabled(false);
        drawDetail();
    }

    private void setListener() {
        setBackListener();
        //mapCallbackListener();
        setAbSendListener();
        setMarkerCenterListener();
        setButtonMoreListener();
        setButtonFriendLocationListener();
    }

    private void setBackListener() {
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private void setAbSendListener() {
        mTvwContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnShareLocation();
            }
        });
    }

    private void setMarkerCenterListener() {
        mLlCenterMarkerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnShareLocation();
            }
        });
    }

    private void mapCallbackListener() {
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                Log.i(TAG, "onMarkerDragStart >");
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                Log.i(TAG, "onMarkerDrag >");
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.i(TAG, "onMarkerDragEnd >");
            }
        });
    }

    private void setButtonMoreListener() {
        mImgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupHelper.getInstance().
                        showOrHideOverFlowMenu(mImgOption, mClickHandler, listItemOverFlow);
            }
        });
    }

    private void setButtonFriendLocationListener() {
        mImgFriendLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mType == Constants.LOCATION.TYPE_RECEIVED) {
                    if (mReceivedLatLng == null) {
                        mReceivedLatLng = new LatLng(Double.parseDouble(mReceivedLatitude),
                                Double.parseDouble(mReceivedLongitude));
                    }
                    moveCamera(mReceivedLatLng, 13.0f);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady-->>");
        initMaps(googleMap);
        processOnMapReady();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            handleNewLocation(location);
        }
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onConnected(Bundle bundle) {
        if (hasPermissionLocation()) {
            Log.d(TAG, "onConnected: +");
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                handleNewLocation(location);
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(mParentActivity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, "Exception", e);
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (mType == Constants.LOCATION.TYPE_SEND) {    // neu la send location thi request dia diem
            getCurrentAdddress(cameraPosition.target);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "onMarkerClick");
        return true;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if (hasPermissionLocation()) {
            checkShowDialogSettingLocation();
        } else {
            showDialogRequestPermission();
        }
        return false;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.MENU_DIRECT_MAPS:
                if (myLocation != null) {
                    mListener.navigateToDirect(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                            mReceivedLatLng);
                } else {
                    mParentActivity.showToast(mRes.getString(R.string.msg_not_load_location), Toast.LENGTH_LONG);
                }
                break;
            case Constants.MENU.MENU_OPEN_MAPS:
                mListener.navigateToMaps(mReceivedLatLng, mReceivedAddress);
                break;
            case Constants.ACTION.ACTION_SETTING_LOCATION:
                try {
                    if (mParentActivity != null) {
                        mParentActivity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                } catch (ActivityNotFoundException ex) {
                    Log.e(TAG, "Exception" + ex);
                }
                break;
            default:
                break;
        }
    }

    private void initOverFlowMenu() {
        listItemOverFlow = new ArrayList<>();
        ItemContextMenu direct = new ItemContextMenu(mRes.getString(R.string.menu_location_direct),
                -1, null, Constants.MENU.MENU_DIRECT_MAPS);
        ItemContextMenu openMaps = new ItemContextMenu(mRes.getString(R.string.menu_location_open_maps),
                -1, null, Constants.MENU.MENU_OPEN_MAPS);
        listItemOverFlow.add(direct);
        listItemOverFlow.add(openMaps);
    }

    private void drawDetail() {
        if (mType == Constants.LOCATION.TYPE_SEND) {
            mImgOption.setVisibility(View.GONE);
            mTvwContinue.setVisibility(View.VISIBLE);
            mTvwTitle.setText(mRes.getString(R.string.filter_stranger_location));
            mLlCenterMarkerLayout.setVisibility(View.VISIBLE);
            mImgCenterMakerIcon.setVisibility(View.VISIBLE);
            mImgFriendLocation.setVisibility(View.GONE);
            showOrHideDetailMarkerCenter(false);
        } else {
            mImgOption.setVisibility(View.VISIBLE);
            mTvwContinue.setVisibility(View.GONE);
            mTvwTitle.setText(mRes.getString(R.string.filter_stranger_location));
            mLlCenterMarkerLayout.setVisibility(View.GONE);
            mImgCenterMakerIcon.setVisibility(View.GONE);
            mImgFriendLocation.setVisibility(View.VISIBLE);
            showOrHideDetailMarkerCenter(false);
        }
    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mParentActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    @SuppressWarnings({"MissingPermission"})
    private void initMaps(GoogleMap googleMap) {
        mMap = googleMap;
        // loai ban do
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // hien option my location
        if (hasPermissionLocation()) {
            mMap.setMyLocationEnabled(true);
        }
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        customInfoWindownAdapterMaps();
        setLayoutLocationButton();
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        setPositionLocation();
    }

    private void setPositionLocation() {
        Resources r = getResources();
        float pxRightMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, r.getDisplayMetrics());
        float pxBottomMargin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 112, r.getDisplayMetrics());

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            ImageView btnMyLocation = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            btnMyLocation.setImageResource(R.mipmap.ic_location);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    btnMyLocation.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, (int) pxRightMargin, (int) pxBottomMargin);
        }
    }

    private void customInfoWindownAdapterMaps() {
        // Setting a custom info window adapter for the google map
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                if (arg0 != null) {
                    mTvwCenterMarkerAddress.setText(arg0.getSnippet());
                    if (mType == Constants.LOCATION.TYPE_RECEIVED) {
                        mLlCenterMarkerLayout.setVisibility(View.GONE);
                    }

                }
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {
                // Getting the position from the marker
                return null;
            }
        });
    }

    @SuppressWarnings({"ResourceType"})
    private void setLayoutLocationButton() {
        try {
            View locationButton = mapFragment.getView().findViewById(0x02);
            if (locationButton != null) {
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                rlp.setMargins(0, -20, -80, 30);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void showDialogRequestPermission() {
        PermissionHelper.setCallBack(this);
        PermissionHelper.requestPermissionWithGuide(mParentActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onPermissionsResult: " + requestCode);
        if (mHandler != null && requestCode == Constants.PERMISSION.PERMISSION_REQUEST_LOCATION &&
                PermissionHelper.verifyPermissions(grantResults)) {
            moveToLastLocation();
        } else {
            if (!PermissionHelper.verifyPermissions(grantResults) && getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    private void processOnMapReady() {
        String locationProvider = LocationHelper.getInstant(mApplication).
                getBestAvailableLocationProvider(mLocationManager);
        if (!TextUtils.isEmpty(locationProvider)) {
            if (hasPermissionLocation()) {
                try {
                    Location lastLocation = mLocationManager.getLastKnownLocation(locationProvider);
                    if (lastLocation != null) {
                        myLocation = lastLocation;
                    }
                } catch (SecurityException e) {
                    Log.e(TAG, "SecurityException", e);
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "IllegalArgumentException", e);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            } else {// lay vi tri cuoi cung
                showDialogRequestPermission();
            }
        }
        if (mType == Constants.LOCATION.TYPE_SEND) {
            if (myLocation != null) {
                moveCamera(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 13.0f);
            }
        } else {
            mReceivedLatLng = new LatLng(Double.parseDouble(mReceivedLatitude), Double.parseDouble(mReceivedLongitude));
            moveCamera(mReceivedLatLng, 13.0f);
            mReceivedMarker = createMarkerOption(mReceivedLatLng,
                    mRes.getString(R.string.title_market_received_location), mReceivedAddress);
        }
    }

    private void moveToLastLocation() {
        String locationProvider = LocationHelper.getInstant(mApplication).
                getBestAvailableLocationProvider(mLocationManager);
        if (!TextUtils.isEmpty(locationProvider)) {// lay vi tri cuoi cung
            try {
                Location lastLocation = mLocationManager.getLastKnownLocation(locationProvider);
                if (lastLocation != null) {
                    myLocation = lastLocation;
                    moveCamera(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 13.0f);
                }
            } catch (SecurityException e) {
                Log.e(TAG, "SecurityException", e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException", e);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    /**
     * xy ly khi nhan duoc su kien location change
     *
     * @param location
     */
    private void handleNewLocation(Location location) {
        Log.d(TAG, "handleNewLocation: " + location.getLatitude() + " longitude: " + location.getLongitude());
        myLocation = location;
        if (!updateMyLocation) {
            updateMyLocation = true;
            if (mType == Constants.LOCATION.TYPE_SEND) {
                moveCamera(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 13.0f);
            }
        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        }
       /* CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)             // Sets the center of the map to location user
                .zoom(zoom)                  // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
    }

    private Marker createMarkerOption(LatLng latLng, String title, String content) {
        //Thêm MarketOption cho Map:
        MarkerOptions option = new MarkerOptions();
//        option.title(title);
        option.snippet(content);
        option.position(latLng);
        option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));
        Marker marker = mMap.addMarker(option);
        marker.showInfoWindow();
        return marker;
    }

    private void showOrHideDetailMarkerCenter(boolean isShow) {
        if (isShow) {
            mPgbCenterMarkerLoading.setVisibility(View.GONE);
            mTvwCenterMarkerTitle.setVisibility(View.VISIBLE);
            mTvwCenterMarkerAddress.setVisibility(View.VISIBLE);
            mTvwCenterMarkerAddress.setText(mCurrentAddress);
        } else {
            mPgbCenterMarkerLoading.setVisibility(View.VISIBLE);
            mTvwCenterMarkerTitle.setVisibility(View.GONE);
            mTvwCenterMarkerAddress.setVisibility(View.VISIBLE);
        }
    }

    private void getCurrentAdddress(LatLng latLng) {
        if (mGetCurrentAddressAsyncTask != null) {
            mGetCurrentAddressAsyncTask.cancel(true);
            mGetCurrentAddressAsyncTask = null;
        }
        mGetCurrentAddressAsyncTask = new getCurrentAddressAsyncTask();
        mGetCurrentAddressAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, latLng);
    }

    private class getCurrentAddressAsyncTask extends AsyncTask<LatLng, Void, String> {
        private LatLng currentLatLng;

        @Override
        protected void onPreExecute() {
            showOrHideDetailMarkerCenter(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(LatLng... latLngss) {
            currentLatLng = latLngss[0];
            return LocationHelper.getInstant(mApplication).
                    getAddressesFromLocation(currentLatLng.latitude, currentLatLng.longitude);
        }

        @Override
        protected void onPostExecute(String s) {
            if (TextUtils.isEmpty(s)) {
                mCurrentAddress = mRes.getString(R.string.address_market_default);
            } else {
                mCurrentAddress = s;
            }
            Log.d(TAG, "mCurrentAddress: " + mCurrentAddress);
            mCurrentLatLngCenter = currentLatLng;
            mTvwContinue.setEnabled(true);
            mLlCenterMarkerLayout.setEnabled(true);
            showOrHideDetailMarkerCenter(true);
            super.onPostExecute(s);
        }
    }

    private void returnShareLocation() {
        if (TextUtils.isEmpty(mCurrentAddress)) {
            mCurrentAddress = mRes.getString(R.string.address_market_default);
        }
        if (mCurrentLatLngCenter == null) {// ko hien nua, th click vao marker thi coi nhu bi disable
            //mParentActivity.showToast(mRes.getString(R.string.msg_not_load_location), Toast.LENGTH_LONG);
        } else {
            String latitude = String.valueOf(mCurrentLatLngCenter.latitude);
            String longitude = String.valueOf(mCurrentLatLngCenter.longitude);
            mListener.returnResultShareLocation(mCurrentAddress, latitude, longitude);
        }
    }

    private void checkShowDialogSettingLocation() {
        if (!LocationHelper.getInstant(mApplication).isLocationServiceEnabled()) {
            LocationHelper.getInstant(mApplication).showDialogSettingLocationProviders(mParentActivity, mClickHandler);
        } else if (!LocationHelper.getInstant(mApplication).isNetworkLocationEnabled()) {
            LocationHelper.getInstant(mApplication).showDialogSettingHighLocation(mParentActivity, mClickHandler);
        }
    }

    public interface OnFragmentAppInfoListener {
        void returnResultShareLocation(String address, String latitude, String longitude);

        void navigateToDirect(LatLng fromlaLng, LatLng toLatLng);

        void navigateToMaps(LatLng toLatLng, String address);
    }
}
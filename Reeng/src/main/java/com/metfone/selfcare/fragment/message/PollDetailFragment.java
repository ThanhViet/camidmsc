package com.metfone.selfcare.fragment.message;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.PollMessageActivity;
import com.metfone.selfcare.adapter.PollDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseListViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/22/2016.
 */
public class PollDetailFragment extends BaseListViewFragment implements
        View.OnClickListener,
        BaseListViewFragment.EmptyViewListener,
        PollDetailAdapter.Listener,
        PollRequestHelper.PollRequestListener {
    private static final String TAG = PollDetailFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private PollMessageActivity mParentActivity;
    private OnFragmentInteractionListener mListener;
    private Resources mRes;
    private String emptyNote;
    private ThreadMessage mThreadMessage;
    private EllipsisTextView mTvwTitle;
    private TextView mAbCreate, mTvwAbStatus;
    private View mAbBack;
    private String pollId;
    private int mThreadId;
    private ListView mListView;
    private PollDetailAdapter mAdapter;
    private PollObject mCurrentPoll;
    private ArrayList<PollItem> pollItems = new ArrayList<>();
    private boolean isMultiChoose = true;
    private boolean isChangeOption = false;

    public PollDetailFragment() {
        // Required empty public constructor
    }

    public static PollDetailFragment newInstance(String pollId, int threadId) {
        PollDetailFragment fragment = new PollDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.MESSAGE.POLL_ID, pollId);
        args.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        setHasOptionsMenu(true);
        Log.i(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_listview, container, false);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mRes = mParentActivity.getResources();
        setToolBar(inflater);
        findComponentViews(rootView, inflater, container);
        getData(savedInstanceState);
        drawDetail();
        setListener();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (PollMessageActivity) activity;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.MESSAGE.POLL_ID, pollId);
        outState.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, mThreadId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.ab_agree_text:
                handlerPostVote();
                break;
        }
    }

    @Override
    public void onCheckBoxClick(PollItem item, View cbx) {
        Log.d(TAG, "onCheckBoxClick: " + item.isSelected() + " , isMultiChoose: " + isMultiChoose);
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (item.isSelected()) {
            item.setSelectedAndChangeCount(false, myNumber);
        } else if (isMultiChoose) {
            item.setSelectedAndChangeCount(true, myNumber);
        } else {
            for (PollItem vote : pollItems) {
                if (vote.isSelected()) {
                    vote.setSelectedAndChangeCount(false, myNumber);
                }
            }
            item.setSelectedAndChangeCount(true, myNumber);
        }
        setAdapter(false);
        isChangeOption = true;
        setEnableButtonSubmit();
        //checkEnableButtonCreate();
    }

    @Override
    public void onItemClick(PollItem item) {
        if (item.getTotalVoted() > 0) {
            mListener.navigateToPollItemDetail(pollId, item.getItemId());
        } else {
            mParentActivity.showToast(R.string.poll_msg_no_voted);
        }
    }

    @Override
    public void onError(int errorCode) {
        hideEmptyView();
        showEmptyNote(emptyNote);
        mAbCreate.setEnabled(false);
    }

    @Override
    public void onSuccess(PollObject poll) {
        hideEmptyView();
        Log.d(TAG, "poll: " + poll.toString());
        mCurrentPoll = poll;
        isMultiChoose = mCurrentPoll.getChoice() != 1;
        PollRequestHelper.getInstance(mApplication).processPollObjectAfterRequestDetail(poll);
        pollItems = poll.getListItems();
        setAdapter(true);
        if (pollItems.isEmpty()) {
            showEmptyNote(emptyNote);
        } else {
            hideEmptyView();
        }
        setEnableButtonSubmit();
    }

    private void setToolBar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_vote_detail, null));
        mTvwTitle = (EllipsisTextView) mParentActivity.getToolBarView().findViewById(R.id.ab_title);
        mTvwAbStatus = (TextView) mParentActivity.getToolBarView().findViewById(R.id.ab_status_text);
        mAbBack = mParentActivity.getToolBarView().findViewById(R.id.ab_back_btn);
        mAbCreate = (TextView) mParentActivity.getToolBarView().findViewById(R.id.ab_agree_text);
        mTvwTitle.setText(R.string.poll_detail);
        mAbCreate.setVisibility(View.VISIBLE);
        mAbCreate.setText(R.string.poll_submit);
        mAbCreate.setEnabled(false);
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mListView = (ListView) rootView.findViewById(R.id.list_view);
        ColorDrawable cd = new ColorDrawable(mRes.getColor(android.R.color.transparent));
        mListView.setDivider(cd);
//        mListView.setDividerHeight(mRes.getDimensionPixelOffset(R.dimen.margin_more_content_10));
        createView(inflater, mListView, this);
        emptyNote = mRes.getString(R.string.list_empty);
        /*showEmptyNote(contentEmpty);
        hideEmptyView();*/
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            pollId = savedInstanceState.getString(Constants.MESSAGE.POLL_ID);
            mThreadId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
        } else if (getArguments() != null) {
            pollId = getArguments().getString(Constants.MESSAGE.POLL_ID);
            mThreadId = getArguments().getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
        }
        mThreadMessage = mApplication.getMessageBusiness().getThreadById(mThreadId);

    }

    private void drawDetail() {
        if (mThreadMessage == null) {
            hideEmptyView();
            return;
        }
        showProgressLoading();
        mTvwTitle.setText(mApplication.getMessageBusiness().getThreadName(mThreadMessage));
        mTvwAbStatus.setText(mApplication.getContactBusiness().getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
        pollItems = new ArrayList<>();
        /*for (int i = 0; i < 20; i++) {
            PollItem item = new PollItem();
            item.setTitle("Noi dung vote " + i);
            pollItems.add(item);
        }*/
        setAdapter(true);
        PollRequestHelper.getInstance(mApplication).getPollDetail(pollId, this, mThreadId);
    }

    private void setAdapter(boolean isChangeItems) {
        if (mAdapter == null) {
            mAdapter = new PollDetailAdapter(mApplication, pollItems, this);
            mListView.setAdapter(mAdapter);
        } else {
            if (mListView.getAdapter() == null) {
                mListView.setAdapter(mAdapter);
            }
            if (isChangeItems)
                mAdapter.setListData(pollItems);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setListener() {
        mAbBack.setOnClickListener(this);
        mAbCreate.setOnClickListener(this);
    }

    private void checkEnableButtonCreate() {
        if (pollItems != null && !pollItems.isEmpty()) {
            boolean isEnable = false;
            for (PollItem item : pollItems) {
                if (item.isSelected()) {
                    isEnable = true;
                    break;
                }
            }
            if (isEnable) {
                mAbCreate.setEnabled(true);
            } else {
                mAbCreate.setEnabled(false);
            }
        } else {
            mAbCreate.setEnabled(false);
        }
    }

    private void handlerPostVote() {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        ArrayList<String> selected = new ArrayList<>();
        for (PollItem item : pollItems) {
            if (item.isSelected()) {
                selected.add(item.getItemId());
            }
        }
        PollRequestHelper.getInstance(mApplication).votePoll(pollId, selected, mCurrentPoll.getListSelected(), new PollRequestHelper.PollRequestListener() {
            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

            @Override
            public void onSuccess(PollObject poll) {
                mParentActivity.hideLoadingDialog();
                MessageBusiness messageBusiness = mApplication.getMessageBusiness();
                ReengMessage reengMessage = messageBusiness.createMessagePollAfterRequest(mThreadMessage, poll, false,false);
                messageBusiness.notifyNewMessage(reengMessage, mThreadMessage);
                // messageBusiness.notifyReengMessage(mApplication, mThreadMessage, reengMessage, mThreadMessage.getThreadType());
                mParentActivity.onBackPressed();
            }
        }, -1);
    }

    private void setEnableButtonSubmit() {
        if (pollItems.isEmpty() || !isChangeOption) {
            mAbCreate.setEnabled(false);
        } else {
            mAbCreate.setEnabled(true);
        }
    }

    public interface OnFragmentInteractionListener {
        void navigateToPollItemDetail(String pollId, String pollItemId);
    }
}
package com.metfone.selfcare.fragment.musickeeng;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.StrangerMusicAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.business.PubSubManager;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerSticky;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.MENU;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper.HomeTab;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.StrangerListInterface;
import com.metfone.selfcare.listeners.StrangerMusicInteractionListener;
import com.metfone.selfcare.listeners.StrangerStickyInteractionListener;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.ui.DetailGridItemDecoration;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayoutDirection;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderSpanSizeLookup;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import co.mobiwise.materialintro.animation.MaterialIntroListener;
import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;

@SuppressWarnings("unused")
public class StrangerMusicFragment extends BaseLoginAnonymousFragment implements
        MusicBusiness.OnLoadDataResponseListener,
        StrangerMusicInteractionListener,
        SwipyRefreshLayout.OnRefreshListener,
        StrangerListInterface,
        ClickListener.IconListener,
        InitDataListener,
        View.OnClickListener,
        StrangerStickyInteractionListener, StrangerFilterHelper.FilterChangeListener {
    private static String TAG = StrangerMusicFragment.class.getSimpleName();

    public static final String CONFIDE_TAB_KEY = "is_first_tab";

    private BaseSlidingFragmentActivity mParentActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private MusicBusiness mMusicBusiness;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private RecyclerView mRecyclerView;
    private TextView mTvwNoteEmpty;//mTvwAbTitle,
    private ProgressLoading mProgressLoading;
    private View mLayoutProgressPreLoad;
    private View mLayoutFooter, mLayoutHeader;
    //            mViewStickyTab;
    private LinearLayout mLayoutNewRoom, mLoadmoreFooterView;
    private boolean mStickyTabVisible = false;
    //
    private Handler mHandler;
    private StrangerMusicAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private GridLayoutManager mGridLayoutManager;
    private ArrayList<StrangerMusic> mStrangerMusics;
    private ArrayList<Object> mStickyStrangerMusics;
    //    private ArrayList<StrangerSticky> mStickyStrangerMusics;
    private boolean isReadyLoadMore = false;
    private boolean isLoadMore = false;
    private boolean isShowNewRoom = false;
    private boolean isPagerVisible = false;
    private CountDownTimer countDownCheckRefresh;

    //list sticky
    private ArrayList<ItemContextMenu> mOverFlowItems;
//    private RecyclerView mRecyclerHeader;
//    private OverflowPagerIndicator mIndicatorHeader;

    // location tab
//    private TextView mTvwTabFirst;
//    private TextView mTvwStickyTabFirst, mTvwFilter, mTvwFilterSticky;
//    private View mViewTabLast, mViewStickyTabLast, mViewTabLastNew, mViewStickyTabLastNew;
//    private ImageView mImgReload, mImgReloadSticky;
    private int mBannerListSize;
    private boolean everShowFragment = false;           //check xem da show bao gio chua
    private boolean needLoadOnCreate = false;           //dung cho deeplink
    // action bar
//    private ImageView mAbInviteMusic, mAbInviteConfider, mAbOverFlow;
    private ImageView ivBack;
    private TextView tvTitle;
    private int next;
    private LinearLayoutManager mLayoutManagerHeader;
    private int firstVisibleItem;
    private int lastVisibleItem;
    private int totalItemCount;
//    private StrangerMusicHeaderAdapter mAdapterHeader;

    private ViewStub viewStub;

    //dainv new design
    private AppCompatImageView ivFilter, ivFilterSticky;
    private AppCompatTextView tvFilter, mTvwFilterSticky;
    private View mViewStickyTab;
    private boolean isConfideTab;
    private StrangerFilterHelper filterHelper;
    private boolean filterRecentChanged;
    private AppCompatImageView ivRefreshSticky, ivRefresh;

    private Handler handlerLoadmore = new Handler() {
        @Override
        public void handleMessage(@androidx.annotation.NonNull Message msg) {
            super.handleMessage(msg);
            onLoadMore();
        }
    };

    private MusicBusiness.onAcceptStrangerRoomListener acceptStrangerMusicListener = new MusicBusiness.onAcceptStrangerRoomListener() {
        @Override
        public void onResponse(ThreadMessage threadMessage, StrangerMusic strangerMusic, boolean isConfide) {
            mParentActivity.hideLoadingDialog();
            notifyChangeAdapter();
            if (mHandler != null) {
                Log.d(TAG, "acceptStrangerMusicListener: " + isConfide);
                if (isConfide) {
                    mApplication.getCallBusiness().handleStartStrangerConfide(threadMessage, mParentActivity, strangerMusic);
                } else {
                    NavigateActivityHelper.navigateToChatDetail(mParentActivity, threadMessage);
                }
            }
        }

        @Override
        public void onError(int errorCode, String posterName, boolean isConfider) {
            mParentActivity.hideLoadingDialog();
            notifyChangeAdapter();
            mParentActivity.showToast(mMusicBusiness.getMessageErrorByErrorCode(errorCode, posterName, isConfider), Toast.LENGTH_LONG);
        }
    };
    private MusicBusiness.onReInviteListener reInviteStrangerListener = new MusicBusiness.onReInviteListener() {
        @Override
        public void onResponse(ThreadMessage threadMessage) {
            mParentActivity.hideLoadingDialog();
            NavigateActivityHelper.navigateToChatDetail(mParentActivity, threadMessage);
        }
    };

    //
    public StrangerMusicFragment() {
    }

    public static StrangerMusicFragment newInstance() {
        Bundle args = new Bundle();
        StrangerMusicFragment fragment = new StrangerMusicFragment();
        fragment.setArguments(args);
        return new StrangerMusicFragment();
    }

    public static StrangerMusicFragment newInstance(boolean isFirstTab) {
        Bundle args = new Bundle();
        args.putBoolean("is_first_tab", isFirstTab);
        StrangerMusicFragment fragment = new StrangerMusicFragment();
        fragment.setArguments(args);
        return new StrangerMusicFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filterHelper = StrangerFilterHelper.getInstance(mApplication);
        Log.d(TAG, "onCreate");
        if (savedInstanceState != null) {
            isPagerVisible = savedInstanceState.getBoolean("PageStateVisible", false);
            isConfideTab = savedInstanceState.getBoolean(CONFIDE_TAB_KEY, true);
        } else {
            if (getArguments() != null) {
                isConfideTab = getArguments().getBoolean(CONFIDE_TAB_KEY);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //  Inflate the layout for this fragment
        mHandler = new Handler();
        View rootView = inflater.inflate(R.layout.fragment_stranger_music, container, false);
        findComponentViews(rootView, container, inflater);
        setViewListeners();
        ListenerHelper.getInstance().addInitDataListener(this);
        Log.i(TAG, "onCreateView");
        if (needLoadOnCreate || isPagerVisible) {
            if (mParentActivity instanceof ModuleActivity)
                initViewStub();
            onPageStateVisible(true, TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_stranger, null));
        }
//        else {
//            showIntro();
//        }

//        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
//            mAbInviteMusic.setVisibility(View.GONE);
//            mAbInviteConfider.setVisibility(View.GONE);
//            mAbOverFlow.setVisibility(View.GONE);
//        }
        filterHelper.addFilterChange(this);
        return rootView;
    }

    boolean initViewStubDone = false;

    @SuppressLint("InflateParams")
    private void initViewStub() {
        if (initViewStubDone) return;
        Log.i(TAG, "initViewStub");
        View view = viewStub.inflate();
        mLayoutHeader = LayoutInflater.from(mParentActivity).inflate(R.layout.layout_filter_stranger_v5, null);
        ivFilter = mLayoutHeader.findViewById(R.id.ivFilter);
//        mRecyclerHeader = mLayoutHeader.findViewById(R.id.recycler_top);
//        mIndicatorHeader = mLayoutHeader.findViewById(R.id.indicator_top);

//        mHeaderTab = mLayoutHeader.findViewById(R.id.header_stranger_music_tab);
//        mTvwTabFirst = mHeaderTab.findViewById(R.id.sticky_stranger_tab_first);
//        mViewTabLast = mHeaderTab.findViewById(R.id.sticky_stranger_tab_last);
//        mViewTabLastNew = mHeaderTab.findViewById(R.id.sticky_stranger_tab_last_new);
//        mImgReload = mHeaderTab.findViewById(R.id.sticky_stranger_reload);
//        View mViewFilter = mHeaderTab.findViewById(R.id.sticky_stranger_filter);
        tvFilter = mLayoutHeader.findViewById(R.id.tvFilter);
        ivRefresh = mLayoutHeader.findViewById(R.id.ivRefresh);
        //
        mViewStickyTab = view.findViewById(R.id.stranger_music_sticky_tab);
//        mTvwStickyTabFirst = mViewStickyTab.findViewById(R.id.sticky_stranger_tab_first);
//        mViewStickyTabLast = mViewStickyTab.findViewById(R.id.sticky_stranger_tab_last);
//        mViewStickyTabLastNew = mViewStickyTab.findViewById(R.id.sticky_stranger_tab_last_new);
//        mImgReloadSticky = mViewStickyTab.findViewById(R.id.sticky_stranger_reload);
        ivFilterSticky = mViewStickyTab.findViewById(R.id.ivFilter);
        mTvwFilterSticky = mViewStickyTab.findViewById(R.id.tvFilter);
        ivRefreshSticky = mViewStickyTab.findViewById(R.id.ivRefresh);

        mSwipyRefreshLayout = view.findViewById(R.id.stranger_music_swipy_layout);
        mRecyclerView = view.findViewById(R.id.stranger_music_recycleview);
        mProgressLoading = view.findViewById(R.id.stranger_music_loading_progressbar);
        mLayoutProgressPreLoad = view.findViewById(R.id.layout_progress_music);
        mLayoutProgressPreLoad.setVisibility(View.VISIBLE);
        mTvwNoteEmpty = view.findViewById(R.id.stranger_music_note_text);
        mLayoutNewRoom = view.findViewById(R.id.stranger_music_new_room_layout);

        mLayoutFooter = LayoutInflater.from(mParentActivity).inflate(R.layout.item_onmedia_loading_footer, null);
        mLoadmoreFooterView = mLayoutFooter.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);
        mProgressLoading.setVisibility(View.GONE);
        mTvwNoteEmpty.setVisibility(View.GONE);
        initLayoutParamsHeaderView();

        setGridViewListener();
        mLayoutNewRoom.setOnClickListener(this);
        //tab
//        mTvwTabFirst.setOnClickListener(this);
//        mViewTabLast.setOnClickListener(this);
//        mTvwStickyTabFirst.setOnClickListener(this);
//        mViewStickyTabLast.setOnClickListener(this);
//        mImgReload.setOnClickListener(this);
//        mImgReloadSticky.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        ivFilterSticky.setOnClickListener(this);
        ivRefreshSticky.setOnClickListener(this::onClick);
        ivRefresh.setOnClickListener(this::onClick);

        initViewLogin(null, null, view);
        initViewStubDone = true;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (BaseSlidingFragmentActivity) activity;
        if (mParentActivity instanceof ModuleActivity)
            isPagerVisible = true;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mMusicBusiness = mApplication.getMusicBusiness();
        mRes = mParentActivity.getResources();
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume: " + isPagerVisible);
        if (mParentActivity instanceof HomeActivity)
            ((HomeActivity) mParentActivity).addStrangeListInterface(this);
        /*showOrHideNotifyNewRoom();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }*/
        if (isPagerVisible) {
            startCountDownCheckRefresh();
            startAutoChangeBanner();
        }
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("PageStateVisible", isPagerVisible);
        outState.putBoolean(CONFIDE_TAB_KEY, isConfideTab);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.i(TAG, "setUserVisibleHint: " + isVisibleToUser);
        if (isVisibleToUser) {
            if (getView() == null) {
                if (mParentActivity == null || mParentActivity instanceof HomeActivity)
                    needLoadOnCreate = true;
            } else {
                if (mParentActivity == null) return;
                if (mParentActivity instanceof HomeActivity) {
                    initViewStub();
//                    ChangeABColorHelper.changeTabColor(bgHeader, HomeTab.tab_hot, null,
//                            ((HomeActivity) mParentActivity).getCurrentTab(), ((HomeActivity) mParentActivity).getCurrentColorTabWap());
                }
                onPageStateVisible(true, TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_stranger, null));
            }

        }
        if (isVisibleToUser) {
            if (filterRecentChanged) {
                refreshAfterChooseFilter();
                filterRecentChanged = false;
            }
        }
    }

    @Override
    public void onPause() {
        if (mParentActivity instanceof HomeActivity)
            ((HomeActivity) mParentActivity).removeStrangeListInterface(this);
        //isPagerVisible = false; TODO không reset trang thai visible
        stopCountDownCheckRefresh();
        //PubSubManager.getInstance(mApplication).startCountDownUnSubStranger();
        super.onPause();
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        //isPagerVisible = false; //TODO không reset trang thai nay
        stopAutoChangeBanner();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        mHandler = null;
        ListenerHelper.getInstance().removeInitDataListener(this);
        MusicBusiness.removeLoadDataResponseListener(this);
        if (filterHelper != null) {
            filterHelper.removeFilterChangeListener(this);
        }
        super.onDestroyView();
    }

    @Override
    protected String getSourceClassName() {
        return "Stranger";
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
        PopupHelper.getInstance().destroyOverFlowMenu();
        initLayoutParamsHeaderView();
        changeMessageSticky();
        if (mAdapter != null) {
//            mAdapter.initLayoutParams();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDataReady() {
        if (everShowFragment) {
            if (mHandler == null) return;
            mHandler.post(() -> {
                MusicBusiness.addLoadDataResponseListener(StrangerMusicFragment.this);
                initData();
            });
        }
    }

    public void initOverFlowMenu() {
        mOverFlowItems = new ArrayList<>();
        ItemContextMenu musicRoom = new ItemContextMenu(mRes.getString(R.string.music_room),
                R.drawable.ic_v5_profile_listen, null, MENU.MENU_MUSIC_ROOM);
        mOverFlowItems.add(musicRoom);
        /*ItemContextMenu addFriends = new ItemContextMenu(mParentActivity, mRes.getString(R.string.menu_add_friend),
                R.drawable.ic_bottom_add_friend, null, MENU.MENU_ADD_FRIEND);
        mOverFlowItems.add(addFriends);*/
        ItemContextMenu nearYou = new ItemContextMenu(mRes.getString(R.string.menu_near_you),
                R.drawable.ic_v5_location, null, MENU.MENU_NEAR_YOU);
        mOverFlowItems.add(nearYou);
        ItemContextMenu searchUser = new ItemContextMenu(mRes.getString(R.string.search),
                R.drawable.ic_v5_search, null, MENU.MENU_SEARCH);
        mOverFlowItems.add(searchUser);
    }

    private void initLayoutParamsHeaderView() {
//        DisplayMetrics displayMetrics = mRes.getDisplayMetrics();
//        //int avatarHeight = displayMetrics.widthPixels / 2;
//        stickyHeight = displayMetrics.widthPixels / 2;
//        //int contentMargin = avatarHeight / 8;
//        //parrent row
//        ViewGroup.LayoutParams paramsRow = mStickyViewPager.getLayoutParams();
//        //stickyHeight = avatarHeight + contentMargin / 2;
//        paramsRow.height = stickyHeight;// - Utilities.dpToPixels(45, mParentActivity.getResources());
//        //progressWidth = mRes.getDimensionPixelOffset(R.dimen.progress_size);
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        viewStub = rootView.findViewById(R.id.vStub);

        //ab
//        mAbInviteMusic = rootView.findViewById(R.id.img_invite);
//        mAbInviteConfider = rootView.findViewById(R.id.img_confide);
//        mAbOverFlow = rootView.findViewById(R.id.img_over_flow);
//
//        ImageView ivBack = rootView.findViewById(R.id.ivBack);
//        TextView tvTitle = rootView.findViewById(R.id.home_ab_title);
//        View headerController = rootView.findViewById(R.id.headerController);
//        if (mParentActivity instanceof HomeActivity) {
//            ivBack.setVisibility(View.GONE);
//            Utilities.setMargins(tvTitle, Utilities.dpToPixel(R.dimen.v5_spacing_normal, getResources()), tvTitle.getTop(), tvTitle.getRight(), tvTitle.getBottom());
//            Utilities.setMargins(headerController, headerController.getLeft(), Utilities.dpToPixel(R.dimen.v5_margin_top_action_bar_home, getResources()), headerController.getRight(), headerController.getBottom());
//        } else {
//            ivBack.setVisibility(View.VISIBLE);
//            ivBack.setVisibility(View.VISIBLE);
//            tvTitle.setAllCaps(false);
//            tvTitle.setTypeface(tvTitle.getTypeface(), Typeface.BOLD);
//            tvTitle.setTextColor(ContextCompat.getColor(mApplication, R.color.text_ab_title));
//            headerController.setBackgroundColor(ContextCompat.getColor(mApplication, R.color.white));
//            Utilities.setMargins(tvTitle, 0, tvTitle.getTop(), tvTitle.getRight(), tvTitle.getBottom());
//            Utilities.setMargins(headerController, headerController.getLeft(), 0, headerController.getRight(), headerController.getBottom());
//        }
//        ivBack.setOnClickListener(v -> mParentActivity.onBackPressed());
    }

    private void changeMessageSticky() {
        if (mStickyStrangerMusics == null || mStickyStrangerMusics.isEmpty()) return;
        drawHeaderViewDetail();
    }

    private void drawHeaderViewDetail() {
        if (mStickyStrangerMusics == null || mStickyStrangerMusics.isEmpty()) {
            showOrHideHeaderView(false);
        } else {
            stopAutoChangeBanner();

//            if (mPageAdapter == null || mStickyViewPager.getAdapter() == null) {
//                Log.d(TAG, "drawHeaderViewDetail");
//                mPageAdapter = new StrangerStickyPageAdapter(mParentActivity.getApplicationContext(),
//                        mStickyStrangerMusics, this);
//                mStickyViewPager.setAdapter(mPageAdapter);
//                showArrows(0);
//                mCurrentBannerPos = 0;
//                mBannerListSize = mStickyStrangerMusics.size();
//            } else {
//                Log.d(TAG, "notify data change");
//                mPageAdapter.setStrangerList(mStickyStrangerMusics);
//                mPageAdapter.notifyDataSetChanged();
//                showArrows(mStickyViewPager.getCurrentItem());
            mBannerListSize = mStickyStrangerMusics.size();
//            }
//            showOrHideHeaderView(true);
            startAutoChangeBanner();
        }
    }

    private void startAutoChangeBanner() {
//        stopAutoChangeBanner();
        Log.d(TAG, "startAutoChangeBanner: " + mBannerListSize + ", " + isPagerVisible);
//        if (mBannerListSize < 2 || !isPagerVisible) return;
//        mRecyclerHeader.postDelayed(runnableRecyclerHeader, Constants.CHANGE_BANNER_TIME);
    }

    private void stopAutoChangeBanner() {
        Log.d(TAG, "pre stopAutoChangeBanner");
//        if (mRecyclerHeader != null) mRecyclerHeader.postInvalidate();
    }

    private void showOrHideHeaderView(boolean isShowStarRoom) {
//        if (isShowStarRoom) {
//            mRecyclerHeader.setVisibility(View.VISIBLE);
//            mIndicatorHeader.setVisibility(View.VISIBLE);
//        } else {
//            mRecyclerHeader.setVisibility(View.GONE);
//            mIndicatorHeader.setVisibility(View.GONE);
//        }
//        mHeaderTab.setVisibility(View.VISIBLE);
    }

    private void setViewListeners() {

//        setPagerStarListener();
//        mAbInviteMusic.setOnClickListener(this);
//        mAbInviteConfider.setOnClickListener(this);
//        mAbOverFlow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: " + v.getId());
        switch (v.getId()) {
            case R.id.stranger_music_new_room_layout:
                mStrangerMusics = isConfideTab ? mMusicBusiness.getNewStrangerConfides() : mMusicBusiness.getNewStrangerMusics();
                ArrayList<StrangerSticky> newStickyStrangerMusics = mMusicBusiness.getNewStickyStrangerMusics();
                if (mStickyStrangerMusics == null) mStickyStrangerMusics = new ArrayList<>();
                else
                    mStickyStrangerMusics.clear();
                mStickyStrangerMusics.addAll(newStickyStrangerMusics);
                mMusicBusiness.setStrangerList(mStrangerMusics);
                mMusicBusiness.setStickyStrangerMusics(newStickyStrangerMusics);
                notifyChangeAdapter();
                drawHeaderViewDetail();
                isShowNewRoom = false;
                showOrHideNotifyNewRoom();
                break;
//            case R.id.sticky_stranger_tab_first:
//                if (mMusicBusiness.isSelectedConfide()) {
//                    mMusicBusiness.setSelectedConfide(false);
//                    refreshAfterChooseTab();
//                }
//                break;
//            case R.id.sticky_stranger_tab_last:
//                if (!mMusicBusiness.isSelectedConfide()) {
//                    mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_CONFIDE_LAST_TIME_CLICK, TimeHelper.getCurrentTime()).apply();
//                    mMusicBusiness.setSelectedConfide(true);
//                    refreshAfterChooseTab();
//                }
//                break;
            case R.id.ivRefresh:
                doRefreshData();
                break;
            case R.id.ivFilter:
                showDialogFilter();
//                break;
//            case R.id.iv:
//                btnMusicClicked();
//                break;
//            case R.id.img_confide:
//                DeepLinkHelper.getInstance().handlerPostStrangerConfide(mApplication, mParentActivity, false);
//                break;
//            case R.id.img_over_flow:
//                onOverFlowMenuClick();
//                break;
        }
    }

    private void doRefreshData() {
        // dang load hoạc chua du lieu
        if (mMusicBusiness == null || mSwipyRefreshLayout == null || mSwipyRefreshLayout.isRefreshing()) {
            return;
        }
        if (mRecyclerView != null)
            mRecyclerView.scrollToPosition(0);
        mSwipyRefreshLayout.setRefreshing(true);
        mMusicBusiness.loadListStrangerMusic(false, true, false, isConfideTab);
    }

    private String getStringMenuFilter(int optionId) {
        String msg;
        switch (optionId) {
            case MENU.STRANGER_FILTER_MALE_FEMALE:
                msg = mRes.getString(R.string.tab_contact);
                break;
            case MENU.STRANGER_FILTER_MALE:
                msg = mRes.getString(R.string.menu_man_wait);
                break;
            case MENU.STRANGER_FILTER_FEMALE:
                msg = mRes.getString(R.string.menu_woman_wait);
                break;
            case MENU.STRANGER_FILTER_STAR:
                msg = mRes.getString(R.string.menu_filter_star);
                break;
            default:
                msg = mRes.getString(R.string.tab_contact);
                break;
        }
        return msg;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setPagerStarListener() {
//        mStickyViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float v, int i1) {
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                showArrows(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//                mSwipyRefreshLayout.setCanTouch(state == ViewPager.SCROLL_STATE_IDLE);
//            }
//        });

//        mStickyViewPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_MOVE:
//                        mSwipyRefreshLayout.setCanTouch(false);
//                        break;
//                    case MotionEvent.ACTION_UP:
//                    case MotionEvent.ACTION_CANCEL:
//                        mSwipyRefreshLayout.setCanTouch(true);
//                        break;
//                }
//                return false;
//            }
//        });

//        mStickyViewPager.setSwipe(Constants.STICKY_DRAGABLE);
    }

    private void setGridViewListener() {
        //        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
//                if (isReadyLoadMore && newState == 0) {
//                    boolean needToLoad = (lastVisibleItem >= totalItemCount - 8) && (firstVisibleItem > 0);
//                    if (!isLoadMore && needToLoad) {// dang ko load more
//                        mLoadmoreFooterView.setVisibility(View.VISIBLE);
//                        isLoadMore = true;
//                        onLoadMore();
//                    }
//                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                Log.d(TAG, "onScrolled ");
                super.onScrolled(recyclerView, dx, dy);
                if (mGridLayoutManager == null) return;
//                firstVisibleItem = mGridLayoutManager.findFirstVisibleItemPositions(null)[0];
//                lastVisibleItem = mGridLayoutManager.findLastVisibleItemPositions(null)[0];
                firstVisibleItem = mGridLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mGridLayoutManager.findLastVisibleItemPosition();

                totalItemCount = mGridLayoutManager.getItemCount();
                if (firstVisibleItem > 0) {
                    if (!mStickyTabVisible) {
                        Animation slide_down = AnimationUtils.loadAnimation(mApplication,
                                R.anim.slide_down);
                        mViewStickyTab.startAnimation(slide_down);
                        mViewStickyTab.setVisibility(View.VISIBLE);
                        mStickyTabVisible = true;
                    }
                } else {
                    if (mStickyTabVisible) {
                        mViewStickyTab.setVisibility(View.GONE);
                        mStickyTabVisible = false;
                    }
                }
                if (isReadyLoadMore) {
                    boolean needToLoad = (lastVisibleItem >= totalItemCount - 8) && (firstVisibleItem > 0);
                    if (!isLoadMore && needToLoad) {// dang ko load more
                        mLoadmoreFooterView.setVisibility(View.VISIBLE);
                        isLoadMore = true;
                        if (!handlerLoadmore.hasMessages(100)) {
                            handlerLoadmore.sendEmptyMessageDelayed(100, 1000);
                        }
                    }
                }
            }
        }));
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "onRefresh: direction --> " + direction.toString());
        if (!mApplication.isDataReady()) {
            mSwipyRefreshLayout.setRefreshing(false);
            return;
        }
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            stopCountDownCheckRefresh();//stop timer dinh ky refresh truoc
            mMusicBusiness.loadListStrangerMusic(false, true, false, isConfideTab);
        } else {
            onLoadMore();
        }
    }

    private void onLoadMore() {
        Log.d(TAG, "onLoadMore");
        // cua lay dc du lieu khong cho load more
        if (mStrangerMusics == null || mStrangerMusics.isEmpty()) {
            resetLoadMore();
        } else {
            long lastRoomId = mStrangerMusics.get(mStrangerMusics.size() - 1).getRoomId();
            mMusicBusiness.loadMoreStrangerMusic(lastRoomId, isConfideTab);
        }
    }

    private void resetLoadMore() {
        mLoadmoreFooterView.setVisibility(View.GONE);
        isLoadMore = false;
    }

    @Override
    public void onStarting() {
        if (isPagerVisible) {
            startCountDownCheckRefresh();//stop pause, start resume
        }
    }

    @Override
    public void onRefreshDataConfideResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerSticky> stickyStrangerMusics) {
        if (isConfideTab) {
            Log.d(TAG, "onRefreshDataResponse");
            mStrangerMusics = strangerMusics;
            if (mStickyStrangerMusics == null) mStickyStrangerMusics = new ArrayList<>();
            else
                mStickyStrangerMusics.clear();
            mStickyStrangerMusics.addAll(stickyStrangerMusics);
            mProgressLoading.setVisibility(View.GONE);
            mLayoutProgressPreLoad.setVisibility(View.GONE);
            drawHeaderViewDetail();
            notifyChangeAdapter();
            //mPullToRefreshFrameLayout.refreshComplete();
            mSwipyRefreshLayout.setRefreshing(false);
            isReadyLoadMore = true;
            isShowNewRoom = false;
            showOrHideNotifyNewRoom();
            if (isPagerVisible) {
                startCountDownCheckRefresh();//stop pause, start resume
            }
        }
    }

    @Override
    public void onRefreshDataMusicResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerSticky> stickyStrangerMusics) {
        if (!isConfideTab) {
            Log.d(TAG, "onRefreshDataResponse");
            mStrangerMusics = strangerMusics;
            if (mStickyStrangerMusics == null) mStickyStrangerMusics = new ArrayList<>();
            else
                mStickyStrangerMusics.clear();
            mStickyStrangerMusics.addAll(stickyStrangerMusics);
            mProgressLoading.setVisibility(View.GONE);
            mLayoutProgressPreLoad.setVisibility(View.GONE);
            drawHeaderViewDetail();
            notifyChangeAdapter();
            //mPullToRefreshFrameLayout.refreshComplete();
            mSwipyRefreshLayout.setRefreshing(false);
            isReadyLoadMore = true;
            isShowNewRoom = false;
            showOrHideNotifyNewRoom();
            if (isPagerVisible) {
                startCountDownCheckRefresh();//stop pause, start resume
            }
        }
    }

    @Override
    public void onFilterDataError(int errorCode) {
        if (errorCode == -2) {
            mParentActivity.showToast(R.string.error_internet_disconnect);
        } else {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
        }
        enableImageRefresh(true, false);
        onRefreshDataMusicResponse(new ArrayList<>(), new ArrayList<>());
    }

    @Override
    public void onNotifyNewRoomResponse(boolean isNew) {
        Log.d(TAG, "onNotifyNewRoomResponse");
        // truoc do da co new room ma load tiep fail thi van hien
        if (isConfideTab) {
            isShowNewRoom = false;
        } else {
            isShowNewRoom = isNew || isShowNewRoom;
        }
        showOrHideNotifyNewRoom();
        if (isPagerVisible && isReadyLoadMore) {
            startCountDownCheckRefresh();//stop pause, start resume
        }
    }

    @Override
    public void onLoadMoreConfideResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic> dataLoadMore) {
        if (isConfideTab) {
            Log.d(TAG, "onLoadMoreConfideResponse");
            mSwipyRefreshLayout.setRefreshing(false);
            mStrangerMusics = strangerMusics;
            mProgressLoading.setVisibility(View.GONE);
            notifyChangeAdapter();
            resetLoadMore();
        }
    }

    @Override
    public void onLoadMoreMusicResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic> dataLoadMore) {
        if (!isConfideTab) {
            Log.d(TAG, "onLoadMoreMusicResponse");
            mSwipyRefreshLayout.setRefreshing(false);
            mStrangerMusics = strangerMusics;
            mProgressLoading.setVisibility(View.GONE);
            notifyChangeAdapter();
            resetLoadMore();
        }
    }

    @Override
    public void onReloadDataWhenEventMessage() {
        mParentActivity.runOnUiThread(() -> {
            mMusicBusiness.loadListStrangerMusic(false, true, false, isConfideTab);
            Log.d(TAG, "IncomingMessageProcessor -> onReloadDataWhenEventMessage");
        });
    }

    @Override
    public void onRefreshDataWhenReceiverPresence() {
        if (mHandler != null) {
            mHandler.post(() -> {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onError(int errorCode) {
        Log.d(TAG, "onResponseError");
        mParentActivity.hideLoadingDialog();
        mProgressLoading.setVisibility(View.GONE);
        mLayoutProgressPreLoad.setVisibility(View.GONE);
        mSwipyRefreshLayout.setRefreshing(false);
        enableImageRefresh(true, false);
        checkShowNoteEmpty();
        resetLoadMore();
    }

    @Override
    public void onClickPoster(StrangerMusic strangerMusic) {
        mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                R.string.ga_action_interaction, R.string.ga_label_stranger_music_click_avatar);
        if (strangerMusic.isStarRoom()) {
            processFollowStarRoom(strangerMusic);
        } else {
            processClickAvatarUser(strangerMusic.getPosterJid(),
                    strangerMusic.getPosterName(),
                    strangerMusic.getPosterLastAvatar(),
                    strangerMusic.getPosterStatus(),
                    strangerMusic.getPosterBirthDay(),
                    strangerMusic.getPosterGender());
        }
    }

    @Override
    public void onClickAcceptor(StrangerMusic strangerMusic) {
        mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                R.string.ga_action_interaction, R.string.ga_label_stranger_music_click_avatar);
        if (strangerMusic.isStarRoom() && strangerMusic.getState() == Constants.KEENG_MUSIC
                .STRANGE_MUSIC_STATE_STAR_OFF) {
            processFollowStarRoom(strangerMusic);
        } else {
            processClickAvatarUser(strangerMusic.getAcceptorJid(),
                    strangerMusic.getAcceptorName(),
                    strangerMusic.getAcceptorLastAvatar(),
                    strangerMusic.getAcceptorStatus(),
                    strangerMusic.getAcceptorBirthDay(),
                    strangerMusic.getAcceptorGender());
        }
    }

    @Override
    public void onClickAccept(final StrangerMusic strangerMusic) {
        Log.d(TAG, "onClickAccepted");
        if (NetworkHelper.isConnectInternet(mParentActivity)) {
            if (mApplication.getXmppManager().isAuthenticated()) {
                mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                        R.string.ga_action_interaction, R.string.ga_label_stranger_music_accept);
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                if (myNumber != null && myNumber.equals(strangerMusic.getPosterJid())) {
                    mMusicBusiness.showDialogCancelStrangerRoom(mParentActivity, strangerMusic.getSessionId(),
                            mRes.getString(R.string.msg_suggest_remove_stranger_room),
                            strangerMusic.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_CONFIDE, () -> {
                                if (mAdapter != null) {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });
                } else {
                    String newJid = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(strangerMusic.getPosterJid());
                    if (newJid != null) strangerMusic.setPosterJid(newJid);
                    if (strangerMusic.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_CONFIDE) {
                        handleAcceptConfideRoom(strangerMusic);
                    } else {
                        handleAcceptStrangerMusicRoom(strangerMusic);
                    }
                }
            } else {
                mParentActivity.showToast(mRes.getString(R.string.e604_error_connect_server), Toast.LENGTH_LONG);
            }
        } else {
            mParentActivity.showToast(mRes.getString(R.string.error_internet_disconnect), Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onClickReinvite(final StrangerMusic strangerMusic) {
        Log.d(TAG, "onClickReinvite");
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (strangerMusic.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_CONFIDE) {
            if (myNumber != null && myNumber.equals(strangerMusic.getPosterJid())) {
                mMusicBusiness.showDialogCancelStrangerRoom(mParentActivity, strangerMusic.getSessionId(),
                        mRes.getString(R.string.msg_suggest_remove_stranger_room),
                        strangerMusic.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_CONFIDE, () -> {
                            if (mAdapter != null) {
                                mAdapter.notifyDataSetChanged();
                            }
                        });
            } else {
                mParentActivity.showToast(mRes.getString(R.string.e206_error_stranger_music_time_out), Toast.LENGTH_LONG);
            }
        } else if (NetworkHelper.isConnectInternet(mParentActivity)) {
            if (mApplication.getXmppManager().isAuthenticated()) {
                mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                        R.string.ga_action_interaction, R.string.ga_label_stranger_music_reinvite);
                // click so cua minh
                if (myNumber != null && myNumber.equals(strangerMusic.getPosterJid())) {
                    mMusicBusiness.showDialogCancelStrangerRoom(mParentActivity, strangerMusic.getSessionId(),
                            mRes.getString(R.string.msg_suggest_remove_stranger_room), false, () -> {
                                if (mAdapter != null) {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });
                } else {
                    mMusicBusiness.showConfirmOrNavigateToSelectSong(mParentActivity, Constants.TYPE_MUSIC
                                    .TYPE_PERSON_CHAT,
                            strangerMusic.getPosterJid(), strangerMusic.getPosterName(), new MusicBusiness
                                    .OnConfirmMusic() {
                                @Override
                                public void onGotoSelect() {
                                    mMusicBusiness.reInviteStrangerMusic(strangerMusic, reInviteStrangerListener);
                                }

                                @Override
                                public void onGotoChange() {
                                    mMusicBusiness.reInviteStrangerMusic(strangerMusic, reInviteStrangerListener);
                                }
                            });
                }
            } else {
                mParentActivity.showToast(mRes.getString(R.string.e604_error_connect_server), Toast.LENGTH_LONG);
            }
        } else {
            mParentActivity.showToast(mRes.getString(R.string.error_internet_disconnect), Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onClickListen(final StrangerMusic strangerAround) {
        Log.d(TAG, "onClickListen");
        //TODO không con tab quanh day ở tab nay
    }

    /**
     * trang thai page stranger music co duoc chon hay khong
     */
    @Override
    public void onPageStateVisible(boolean isVisible, int prePos) {
        Log.d(TAG, "onPageStateVisible: " + isVisible);
        isPagerVisible = isVisible;
        if (isVisible) {
            if (mParentActivity instanceof HomeActivity) {
                initViewStub();
//                ChangeABColorHelper.changeTabColor(bgHeader, HomeTab.tab_stranger, null,
//                        ((HomeActivity) mParentActivity).getCurrentTab(), ((HomeActivity) mParentActivity).getCurrentColorTabWap());

            }
            if (!everShowFragment) {
                everShowFragment = true;
                if (mApplication.isDataReady()) {
                    MusicBusiness.addLoadDataResponseListener(this);
                    initData();
                }
            } else {
                stopAutoChangeBanner();
                if (isReadyLoadMore) {
                    startCountDownCheckRefresh();//stop pause, start resume
                }
                if (!PubSubManager.getInstance(mApplication).isSubscribeStrangers()) {
                    mRecyclerView.postDelayed(() -> mMusicBusiness.loadListStrangerMusic(false, true,
                            false, isConfideTab), 100);
                }
                //PubSubManager.getInstance(mApplication).stopCountDownUnSubStranger();
                mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                        R.string.ga_action_interaction, R.string.ga_label_stranger_tab_view);
                startAutoChangeBanner();

            }
        } else {
            if (everShowFragment) {
                stopCountDownCheckRefresh();
                //PubSubManager.getInstance(mApplication).startCountDownUnSubStranger();
                stopAutoChangeBanner();
            }
        }
    }

    private void handleAcceptConfideRoom(final StrangerMusic strangerMusic) {
        if (mMusicBusiness.isExistRoomConfide()) {
            mParentActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
            mMusicBusiness.cancelRoomStrangerRoom(mMusicBusiness.getCurrentConfideSession(), new MusicBusiness.onCancelStrangeRoomListener() {
                @Override
                public void onResponse() {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                    mMusicBusiness.acceptRoomStranger(strangerMusic, acceptStrangerMusicListener, true);
                }

                @Override
                public void onError(int errorCode) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(mMusicBusiness.getMessageErrorByErrorCode(errorCode, null, true), Toast.LENGTH_LONG);
                }
            }, true);
        } else {
            mParentActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
            mMusicBusiness.acceptRoomStranger(strangerMusic, acceptStrangerMusicListener, true);
        }
    }

    private void handleAcceptStrangerMusicRoom(final StrangerMusic strangerMusic) {
        mMusicBusiness.showConfirmOrNavigateToSelectSong(mParentActivity, Constants.TYPE_MUSIC
                        .TYPE_PERSON_CHAT,
                strangerMusic.getPosterJid(), strangerMusic.getPosterName(), new MusicBusiness
                        .OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
                        mParentActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                        mMusicBusiness.acceptRoomStranger(strangerMusic, acceptStrangerMusicListener, false);
                    }

                    @Override
                    public void onGotoChange() {
                        mParentActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                        mMusicBusiness.acceptRoomStranger(strangerMusic, acceptStrangerMusicListener, false);
                    }
                });
    }

    @Override
    public void onTabReselected() {
        if (mRecyclerView != null) {
            if (Config.Extras.SMOOTH_SCROLL)
                mRecyclerView.smoothScrollToPosition(0);
            else
                mRecyclerView.scrollToPosition(0);
        }
    }

    @Override
    public void onChangeStrangerList(ArrayList<StrangerMusic> strangerMusics, boolean isConfide) {
        if (isConfideTab == isConfide) {
            mStrangerMusics = strangerMusics;
            notifyChangeAdapter();
        }
    }

    public void onChangeStrangerList(ArrayList<StrangerMusic> strangerMusics) {
        mStrangerMusics = strangerMusics;
        notifyChangeAdapter();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case MENU.POPUP_OK_GG_PLAY_SERVICE:
                NavigateActivityHelper.navigateToPlayStore(mParentActivity, Constants.PACKET_NAME.GG_PLAY_SERVICE);
                break;
            case Constants.ACTION.ACTION_SETTING_LOCATION:
                try {
                    if (mParentActivity != null) {
                        mParentActivity.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                Constants.ACTION.ACTION_SETTING_LOCATION);
                    }
                } catch (ActivityNotFoundException ex) {
                    Log.e(TAG, "Exception" + ex);
                }
                break;
            default:
                break;
        }
    }

    private void initData() {
        //showSuggestAvatar = false;// create thi set =false, neu =true la da show roi, khong show lai nua
        isReadyLoadMore = false;
        if (isConfideTab) {
            mStrangerMusics = mMusicBusiness.getStrangerConfides();
        } else {
            mStrangerMusics = mMusicBusiness.getStrangerMusics();
        }
        ArrayList<StrangerSticky> newStickyStrangerMusics = mMusicBusiness.getStickyStrangerMusics();
        if (mStickyStrangerMusics == null) mStickyStrangerMusics = new ArrayList<>();
        else
            mStickyStrangerMusics.clear();
        mStickyStrangerMusics.addAll(newStickyStrangerMusics);
        if (mStrangerMusics != null && !mStrangerMusics.isEmpty()) {
            mProgressLoading.setVisibility(View.GONE);
            mLayoutProgressPreLoad.setVisibility(View.GONE);
            drawHeaderViewDetail();
            isReadyLoadMore = true;
        } else {
            mProgressLoading.setVisibility(View.VISIBLE);
            showOrHideHeaderView(false);
            // chua co du lieu thi load 1 lan (ko sub)
            handlerLoadmore.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mMusicBusiness.loadListStrangerMusic(false, true, false, isConfideTab);
                }
            }, 1200);
        }
        initAdapter();
        drawTabFilter();
    }

    private void initAdapter() {
//        mAdapterHeader = new StrangerMusicHeaderAdapter(mParentActivity, this);
//        mLayoutManagerHeader = new CustomLinearLayoutManager(mParentActivity, LinearLayoutManager.HORIZONTAL, false);
//        mRecyclerHeader.setLayoutManager(mLayoutManagerHeader);
//        mRecyclerHeader.setNestedScrollingEnabled(false);
//        mRecyclerHeader.setAdapter(mAdapterHeader);
//        mRecyclerHeader.setHasFixedSize(true);
//        mRecyclerHeader.setOnFlingListener(null);
//        mIndicatorHeader.attachToRecyclerView(mRecyclerHeader);
//        new SimpleSnapHelper(mIndicatorHeader).attachToRecyclerView(mRecyclerHeader);
//        mRecyclerHeader.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (mRecyclerHeader == null) return;
//                if (Utilities.notEmpty(mStickyStrangerMusics)) {
//                    try {
//                        int current = mLayoutManagerHeader.findFirstVisibleItemPosition();
//                        if (next == 0) {
//                            next = 1;
//                        } else if (current == 0 && next == -1) {
//                            next = 1;
//                        } else if (current == mStickyStrangerMusics.size() - 1 && next == 1) {
//                            next = -1;
//                        }
//                        int positionNext = current + next;
//                        mLayoutManagerHeader.smoothScrollToPosition(mRecyclerHeader, null, positionNext);
//                        mIndicatorHeader.onPageSelected(positionNext);
//                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                }
//                mRecyclerHeader.postDelayed(this, Constants.CHANGE_BANNER_TIME);
//            }
//        }, Constants.CHANGE_BANNER_TIME);
        if (mRecyclerView == null) return;
        if (mStickyStrangerMusics == null) mStickyStrangerMusics = new ArrayList<>();
//        mAdapterHeader.bindData(mStickyStrangerMusics);
        if (mStrangerMusics == null) {
            mStrangerMusics = new ArrayList<>();
        }
        mAdapter = new StrangerMusicAdapter(mParentActivity, mStrangerMusics, this);
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new DetailGridItemDecoration(2, (int) mParentActivity.getResources().getDimension(R.dimen.v5_spacing_normal),
                false, 1));
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        mGridLayoutManager = new GridLayoutManager(mParentActivity, 2);
//        mGridLayoutManager.setAutoMeasureEnabled(false);
        mGridLayoutManager.setSpanSizeLookup(new HeaderSpanSizeLookup((HeaderAndFooterRecyclerViewAdapter) mRecyclerView.getAdapter(), mGridLayoutManager.getSpanCount()));
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        RecyclerViewUtils.setHeaderView(mRecyclerView, mLayoutHeader);
        RecyclerViewUtils.setFooterView(mRecyclerView, mLayoutFooter);
        checkShowNoteEmpty();
    }

    public void notifyChangeAdapter() {
//        if (mAdapterHeader != null) {
//            mAdapterHeader.bindData(mStickyStrangerMusics);
//        }
        if (mAdapter == null) {
            initAdapter();
        } else {
            if (mStrangerMusics == null) {
                mStrangerMusics = new ArrayList<>();
            }
            //mGridLayoutManager.setSpanCount(2);
            mAdapter.setStrangerStrangers(mStrangerMusics);
            mHeaderAndFooterAdapter.notifyDataSetChanged();
            checkShowNoteEmpty();
        }
    }

    private void checkShowNoteEmpty() {
        if (mProgressLoading == null) {
            mTvwNoteEmpty.setVisibility(View.GONE);
        } else if (mStrangerMusics == null || mStrangerMusics.isEmpty()) {
            if (mProgressLoading.getVisibility() == View.VISIBLE) {
                //chua load xong thi an di
                mTvwNoteEmpty.setVisibility(View.GONE);
            } else if (mStickyStrangerMusics != null && !mStickyStrangerMusics.isEmpty()) {
                // co sticky sao thi ko hien
                mProgressLoading.setVisibility(View.GONE);
                mTvwNoteEmpty.setVisibility(View.GONE);
            } else {
                mProgressLoading.setVisibility(View.INVISIBLE);
                mTvwNoteEmpty.setVisibility(View.VISIBLE);
                mTvwNoteEmpty.setText(mRes.getString(R.string.list_empty));
            }
        } else {
            mTvwNoteEmpty.setVisibility(View.GONE);
        }
    }

    private void processClickAvatarUser(String friendJid, String friendName, String lastAvatar,
                                        String status, String birthdayStr, int gender) {
        String myJid = mApplication.getReengAccountBusiness().getJidNumber();
        if (TextUtils.isEmpty(friendJid)) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
        } else if (myJid.equals(friendJid)) {
            NavigateActivityHelper.navigateToMyProfile(mParentActivity);
        } else {
            NavigateActivityHelper.navigateToFriendProfile(mApplication, mParentActivity,
                    friendJid, friendName, lastAvatar, status, gender);
        }
    }

    /**
     * follow room chat
     *
     * @param strangerMusic {@link StrangerMusic}
     */
    private void processFollowStarRoom(StrangerMusic strangerMusic) {
        String starRoomId = strangerMusic.getPosterJid();
        if (TextUtils.isEmpty(starRoomId)) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        String roomName = strangerMusic.getPosterName();
        String roomAvatar = strangerMusic.getPosterLastAvatar();
//        DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mParentActivity,
//                starRoomId, roomName, roomAvatar);
    }

    private void refreshAfterChooseFilter() {
        mSwipyRefreshLayout.setRefreshing(true);
        mMusicBusiness.loadListStrangerMusic(false, true, true, isConfideTab);
    }

    private void refreshAfterChooseTab() {
        stopCountDownCheckRefresh();
        resetLoadMore();
        mSwipyRefreshLayout.setRefreshing(true);
        isReadyLoadMore = false;
        if (isConfideTab) {
            mStrangerMusics = mMusicBusiness.getStrangerMusics();
        } else {
            mStrangerMusics = mMusicBusiness.getStrangerConfides();
        }
        ArrayList<StrangerSticky> newStickyStrangerMusics = mMusicBusiness.getStickyStrangerMusics();
        if (mStickyStrangerMusics == null) mStickyStrangerMusics = new ArrayList<>();
        else
            mStickyStrangerMusics.clear();
        mStickyStrangerMusics.addAll(newStickyStrangerMusics);
        notifyChangeAdapter();
        drawHeaderViewDetail();
        refreshAfterChooseFilter();
    }

    private void showOrHideNotifyNewRoom() {
        mParentActivity.runOnUiThread(() -> {
            if (isShowNewRoom) {
                mLayoutNewRoom.setVisibility(View.VISIBLE);
            } else {
                mLayoutNewRoom.setVisibility(View.GONE);
            }
        });
    }

    private void enableImageRefresh(boolean isEnable, boolean isRotate) {
//        if (mImgReload == null) return;
        if (isEnable) {
//            mImgReload.setColorFilter(ContextCompat.getColor(mParentActivity, R.color.home_tab_strangers));
//            mImgReloadSticky.setColorFilter(ContextCompat.getColor(mParentActivity, R.color.home_tab_strangers));
//            if (isRotate) {
//                mImgReload.clearAnimation();
//                mImgReloadSticky.clearAnimation();
//                Animation an = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//                an.setDuration(1000);
//                an.setRepeatCount(0);
//                an.setRepeatMode(Animation.RESTART);
//                an.setFillAfter(false);
//                mImgReload.startAnimation(an);
//                mImgReloadSticky.startAnimation(an);
//            }
        } else {
//            mImgReload.clearAnimation();
//            mImgReloadSticky.clearAnimation();
//            mImgReload.setColorFilter(ContextCompat.getColor(mParentActivity, R.color.bg_ab_icon));
//            mImgReloadSticky.setColorFilter(ContextCompat.getColor(mParentActivity, R.color.bg_ab_icon));
        }
    }

    private void stopCountDownCheckRefresh() {
        if (countDownCheckRefresh != null) {
            countDownCheckRefresh.cancel();
            countDownCheckRefresh = null;
        }
        enableImageRefresh(false, false);
    }

    private void startCountDownCheckRefresh() {
        Log.d(TAG, "startCountDownCheckRefresh");
        stopCountDownCheckRefresh();
        countDownCheckRefresh = new CountDownTimer(Constants.STRANGER_MUSIC.TIMER_CHECK_REFRESH,
                Constants.STRANGER_MUSIC.TIMER_CHECK_REFRESH) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    countDownCheckRefresh = null;
                    enableImageRefresh(true, true);
                    //mMusicBusiness.loadListStrangerMusic(true, true, false, mMusicBusiness.isSelectedConfide());
                } catch (Exception e) {
                    Log.e(TAG, "Exception: ", e);
                }
            }
        }.start();
    }

    @Override
    public void onBannerClick(StrangerSticky strangerSticky) {
        //processFollowStarRoom(strangerMusic);
        String action = strangerSticky.getAction();
        if (TextUtils.isEmpty(action)) {
            mParentActivity.showToast(R.string.e666_not_support_function);
        } else if (strangerSticky.getType() == Constants.HTTP.STRANGER_STICKY.TYPE_DEEP_LINK) {
            if (mApplication != null)
                mApplication.getLogApi().logBanner(action, LogApi.TypeService.MUSIC, null);
            DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, action);
            mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                    R.string.ga_action_click_banner, "TYPE_DEEP_LINK: " + action);
        } else if (strangerSticky.getType() == Constants.HTTP.STRANGER_STICKY.TYPE_FAKE_MO) {
            String confirmMsg = strangerSticky.getConfirm();
            ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, mParentActivity, confirmMsg, action, "unknown");
            mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                    R.string.ga_action_click_banner, "TYPE_FAKE_MO: " + action);
        } else if (strangerSticky.getType() == Constants.HTTP.STRANGER_STICKY.TYPE_WEB_VIEW) {
            //noinspection AccessStaticViaInstance
            UrlConfigHelper.getInstance(mApplication).gotoWebViewOnMedia(mApplication, mParentActivity, action);
            mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                    R.string.ga_action_click_banner, "TYPE_WEB_VIEW: " + action);
        } else {
            mParentActivity.showToast(R.string.e666_not_support_function);
        }
    }

    private void showDialogFilter() {
//        new BottomFilterStranger(mParentActivity, true).setListener(result -> {
//            drawTabFilter();
//            refreshAfterChooseFilter();
//        }).show();
        FilterFragment filterFragment = FilterFragment.newInstance();
        filterFragment.setListener(result -> {
            filterRecentChanged = false;
            drawTabFilter();
            refreshAfterChooseFilter();
        });
        filterFragment.show(getChildFragmentManager(), "");
    }

    private void drawTabFilter() {
        String filter = filterHelper.getFormatFilterText(false);
        if (tvFilter != null) tvFilter.setText(filter);
        if (mTvwFilterSticky != null) mTvwFilterSticky.setText(filter);
    }

    @Override
    public void filterChanged() {
        filterRecentChanged = true;
        if (getView() != null) {
            drawTabFilter();
        }
    }

    static class StrangerMusicHeaderHolder extends BaseAdapterV2.ViewHolder {

        @BindView(R.id.iv_cover)
        ImageView ivCover;

        private StrangerSticky model;
        private StrangerStickyInteractionListener listener;

        StrangerMusicHeaderHolder(StrangerStickyInteractionListener listener, LayoutInflater layoutInflater, ViewGroup parent) {
            super(layoutInflater.inflate(R.layout.item_stranger_music_header, parent, false));
            this.listener = listener;
        }

        @Override
        public void bindData(ArrayList<Object> items, int position) {
            Object item = items.get(position);
            if (item instanceof StrangerSticky) {
                model = (StrangerSticky) item;
                ImageManager.showImageNormalV3(model.getImageUrl(), ivCover);
            }
        }

        @OnClick(R.id.root_item_stranger_music_header)
        public void onViewClicked() {
            if (model != null && listener != null) {
                listener.onBannerClick(model);
            }
        }
    }

    static class StrangerMusicHeaderAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

        private StrangerStickyInteractionListener listener;

        StrangerMusicHeaderAdapter(Activity activity, StrangerStickyInteractionListener listener) {
            super(activity);
            this.listener = listener;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new StrangerMusicHeaderHolder(listener, layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof ViewHolder) {
                ((ViewHolder) holder).bindData(items, position);
            }
        }
    }

    private void showIntro() {
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            return;
        }

        final MaterialIntroListener listenerMusic = materialIntroViewId -> new MaterialIntroView.Builder(mParentActivity)
                .enableDotAnimation(false)
                .enableIcon(false)
                .dismissOnTouch(true)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .setInfoText(mRes.getString(R.string.intro_stranger_call))
                .setShape(ShapeType.CIRCLE)
//                .setTarget(mAbInviteConfider)
                .setUsageId("intro_stranger_call") //THIS SHOULD BE UNIQUE ID
                .show();

        new MaterialIntroView.Builder(mParentActivity)
                .enableDotAnimation(false)
                .enableIcon(false)
                .dismissOnTouch(true)
                .setFocusGravity(FocusGravity.CENTER)
                .setFocusType(Focus.MINIMUM)
                .setDelayMillis(100)
                .enableFadeAnimation(true)
                .setListener(listenerMusic)
                .setInfoText(mRes.getString(R.string.intro_stranger_music))
                .setShape(ShapeType.CIRCLE)
//                .setTarget(mAbInviteMusic)
                .setUsageId("intro_stranger_music") //THIS SHOULD BE UNIQUE ID
                .show();
    }

}
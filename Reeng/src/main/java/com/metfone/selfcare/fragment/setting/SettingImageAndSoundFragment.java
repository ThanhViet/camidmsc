package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.widget.SwitchButton;

/**
 * Created by toanvk2 on 12/9/2016.
 */
public class SettingImageAndSoundFragment extends BaseSettingFragment
        implements View.OnClickListener, SwitchButton.OnCheckedChangeListener{
    private static final String TAG = SettingImageAndSoundFragment.class.getSimpleName();
    private static final int REQUEST_SELECT_SOUND = 10;
    private SettingBusiness mSettingBusiness;
    private ConstraintLayout mViewSystemSound,
            mViewSelectSound,
            mViewQualityImage,
            mViewAutoSaveImage, mViewAutoPlaySticker, mViewPlayMusicRoom;
    private SwitchButton mToggleSystemSound, mToggleQualityImage, mToggleAutoSaveImage, mToggleAutoPlaySticker, mTogglePlayMusicRoom;
//    private AppCompatTextView mTvwSelectSoundDesc;
    private String notificationSoundName;

    public static SettingImageAndSoundFragment newInstance() {
        return new SettingImageAndSoundFragment();
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
    }

    @Override
    protected void initView(View view) {
        findComponentViews(view);
        setViewListener();
        setTitle(R.string.setting_image_and_sound);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode);
        if (requestCode == REQUEST_SELECT_SOUND && resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            SettingBusiness.getInstance(mParentActivity).setPrefNotificationSound(uri);
            drawSettingNotificationSound();
            Log.d(TAG, "onActivityResult: -data!=null-->uri:" + uri);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_image_sound_setting_v5;
    }

    private void findComponentViews(View rootView) {
        initActionbar();
        mViewSystemSound = rootView.findViewById(R.id.setting_system_sound);
        mViewSelectSound = rootView.findViewById(R.id.setting_select_sound);
        mViewQualityImage = rootView.findViewById(R.id.setting_image_quality);
        mViewAutoSaveImage = rootView.findViewById(R.id.setting_show_media);
        mViewAutoPlaySticker = rootView.findViewById(R.id.setting_auto_play_sticker);
        mViewPlayMusicRoom = rootView.findViewById(R.id.setting_playmusic_in_room);
//        mTvwSelectSoundDesc = rootView.findViewById(R.id.setting_select_sound_desc);
        mToggleSystemSound = rootView.findViewById(R.id.setting_system_sound_toggle);
        mToggleQualityImage = rootView.findViewById(R.id.setting_image_quality_toggle);
        mToggleAutoSaveImage = rootView.findViewById(R.id.setting_show_media_toggle);
        mToggleAutoPlaySticker = rootView.findViewById(R.id.setting_auto_play_sticker_toggle);
        mTogglePlayMusicRoom = rootView.findViewById(R.id.setting_playmusic_in_room_toggle);
        drawDetail();
    }

    private void initActionbar() {
//        View mViewActionBar = mParentActivity.getToolBarView();
//        mTvwTitle = (EllipsisTextView) mViewActionBar.findViewById(R.id.ab_title);
//        mImgBack = (ImageView) mViewActionBar.findViewById(R.id.ab_back_btn);
//        ImageView mImgOption = (ImageView) mViewActionBar.findViewById(R.id.ab_more_btn);
//        mImgOption.setVisibility(View.GONE);
//        mTvwTitle.setText(mRes.getString(R.string.setting_image_and_sound));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_system_sound:
                mToggleSystemSound.setChecked(!mToggleSystemSound.isChecked());
                break;
            case R.id.setting_select_sound:
                processSettingSelectSound();
                break;
            case R.id.setting_image_quality:
                mToggleQualityImage.setChecked(!mToggleQualityImage.isChecked());
                break;
            case R.id.setting_show_media:
                mToggleAutoSaveImage.setChecked(!mToggleAutoSaveImage.isChecked());
                break;
            case R.id.setting_auto_play_sticker:
                mToggleAutoPlaySticker.setChecked(!mToggleAutoPlaySticker.isChecked());
                break;
            case R.id.setting_playmusic_in_room:
                mTogglePlayMusicRoom.setChecked(!mTogglePlayMusicRoom.isChecked());
                break;
        }
    }


    @Override
    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
        switch (view.getId()) {
            case R.id.setting_playmusic_in_room_toggle:
                processSettingPlayMusicRoom();
                break;
            case R.id.setting_auto_play_sticker_toggle:
                processSettingAutoPlaySticker();
                break;
            case R.id.setting_show_media_toggle:
                processSettingAutoSaveImage();
                break;
            case R.id.setting_image_quality_toggle:
                processSettingQualityImage();
                break;
            case R.id.setting_system_sound_toggle:
                processSystemSound();
                break;
        }
    }

    private void setViewListener() {
//        mImgBack.setOnClickListener(this);
        mViewSystemSound.setOnClickListener(this);
        mViewSelectSound.setOnClickListener(this);
        mViewQualityImage.setOnClickListener(this);
        mViewAutoSaveImage.setOnClickListener(this);
        mViewAutoPlaySticker.setOnClickListener(this);
        mViewPlayMusicRoom.setOnClickListener(this);

        mToggleSystemSound.setOnCheckedChangeListener(this);
        mToggleQualityImage.setOnCheckedChangeListener(this);
        mToggleAutoSaveImage.setOnCheckedChangeListener(this);
        mToggleAutoPlaySticker.setOnCheckedChangeListener(this);
        mTogglePlayMusicRoom.setOnCheckedChangeListener(this);
    }

    private void drawDetail() {
        mViewSystemSound.setVisibility(View.GONE);
        mViewSelectSound.setVisibility(View.GONE);
        mToggleSystemSound.setChecked(mSettingBusiness.isPrefSystemSound());
        mViewSelectSound.setEnabled(mSettingBusiness.isPrefSystemSound());
        mToggleQualityImage.setChecked(mSettingBusiness.getPrefEnableHDImage());
        mToggleAutoSaveImage.setChecked(mSettingBusiness.getPrefShowMedia());
        mToggleAutoPlaySticker.setChecked(mSettingBusiness.getPrefAutoPlaySticker());
        mTogglePlayMusicRoom.setChecked(mSettingBusiness.getPrefOffMusicInRoom());
        drawSettingNotificationSound();
    }

    private void drawSettingNotificationSound() {
        notificationSoundName = SettingBusiness.getInstance(mParentActivity).getNotificationSoundName();
//        mTvwSelectSoundDesc.setText(notificationSoundName);
    }

    private void processSystemSound() {
        boolean state = mToggleSystemSound.isChecked();
        mViewSelectSound.setEnabled(state);
        mSettingBusiness.setPrefSystemSound(state);
    }

    private void processSettingSelectSound() {
        /* RingtoneManager ringtoneManager = new RingtoneManager(mParentActivity);
        ringtoneManager.setType(RingtoneManager.TYPE_NOTIFICATION); */
        SettingBusiness settingBusiness = SettingBusiness.getInstance(mParentActivity);
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, mParentActivity.getResources().getString(R.string.setting_select_sound_summary));
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, settingBusiness.getNotificationSoundUri());
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        startActivityForResult(intent, REQUEST_SELECT_SOUND);
    }

    private void processSettingQualityImage() {
        boolean state = mToggleQualityImage.isChecked();
        mSettingBusiness.setPrefEnableHDImage(state);
    }

    private void processSettingAutoSaveImage() {
        boolean state = mToggleAutoSaveImage.isChecked();
        mSettingBusiness.setPrefShowMedia(state);
    }

    private void processSettingAutoPlaySticker() {
        boolean state = mToggleAutoPlaySticker.isChecked();
        mSettingBusiness.setPrefAutoPlaySticker(state);
    }

    private void processSettingPlayMusicRoom() {
        boolean state = mTogglePlayMusicRoom.isChecked();
        mSettingBusiness.setPrefOffMusicInRoom(state);
    }

}

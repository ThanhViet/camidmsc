package com.metfone.selfcare.fragment.message;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.PollMessageActivity;
import com.metfone.selfcare.adapter.PollCreateAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/22/2016.
 */
public class PollCreateFragment extends Fragment implements View.OnClickListener,
        PollCreateAdapter.Listener {
    private static final String TAG = PollCreateFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private PollMessageActivity mParentActivity;
    private Resources mRes;
    private EllipsisTextView mTvwTitle;
    private TextView mAbCreate, mTvwAvatar;
    private View mAbBack;
    //private
    private CircleImageView mImgAvatar;
    private ListView mListView;
    private EditText mEdtLabel, mEdtAddOption;
    private View mViewMulti;
    private ImageView mImgMulti, mImgAddOption;
    private boolean isMulti = false;
    private int mThreadId;
    private ThreadMessage mThreadMessage;
    private ArrayList<String> listOption = new ArrayList<>();
    private PollCreateAdapter mAdapter;

    public PollCreateFragment() {
        // Required empty public constructor
    }

    public static PollCreateFragment newInstance(int threadId) {
        PollCreateFragment fragment = new PollCreateFragment();
        Bundle args = new Bundle();
        args.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        setHasOptionsMenu(true);
        Log.i(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_poll_create, container, false);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mRes = mParentActivity.getResources();
        setToolBar(inflater);
        findComponentViews(rootView, inflater, container);
        getData(savedInstanceState);
        setAdapter();
        drawDetail();
        setListener();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (PollMessageActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, mThreadId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.create_vote_multi_layout:
                isMulti = !isMulti;
                mImgMulti.setSelected(isMulti);
                break;
            case R.id.ab_agree_text:
                handlerCreateVote();
                break;
            case R.id.create_vote_add_option_img:
                checkAndAddOption();
                break;
        }
    }

    @Override
    public void onClearClick(int position, String item) {
        listOption.remove(position);
        setAdapter();
        checkEnableButtonCreate();
    }

    private void setToolBar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        mTvwTitle = (EllipsisTextView) mParentActivity.getToolBarView().findViewById(R.id.ab_title);
        View mViewOption = mParentActivity.getToolBarView().findViewById(R.id.ab_more_btn);
        mAbBack = mParentActivity.getToolBarView().findViewById(R.id.ab_back_btn);
        mAbCreate = (TextView) mParentActivity.getToolBarView().findViewById(R.id.ab_agree_text);
        mViewOption.setVisibility(View.GONE);
        mTvwTitle.setText(R.string.poll_create);
        mAbCreate.setVisibility(View.VISIBLE);
        mAbCreate.setText(R.string.poll_post);
        mAbCreate.setEnabled(false);
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mTvwAvatar = (TextView) rootView.findViewById(R.id.create_vote_avatar_text);
        mImgAvatar = (CircleImageView) rootView.findViewById(R.id.create_vote_avatar);
        mEdtLabel = (EditText) rootView.findViewById(R.id.create_vote_label_edt);
        mEdtAddOption = (EditText) rootView.findViewById(R.id.create_vote_add_option_edt);
        mListView = (ListView) rootView.findViewById(R.id.list_view);
        mViewMulti = rootView.findViewById(R.id.create_vote_multi_layout);
        mImgAddOption = (ImageView) rootView.findViewById(R.id.create_vote_add_option_img);
        mImgMulti = (ImageView) rootView.findViewById(R.id.create_vote_multi_checkbox);
        //InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
        } else if (getArguments() != null) {
            mThreadId = getArguments().getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
        }
        mThreadMessage = mApplication.getMessageBusiness().findThreadByThreadId(mThreadId);
    }

    private void drawDetail() {
        mEdtAddOption.setImeOptions(KeyEvent.KEYCODE_ENTER);
        mImgMulti.setSelected(isMulti);
        String lAvatar = mApplication.getReengAccountBusiness().getLastChangeAvatar();
        if (!TextUtils.isEmpty(lAvatar)) {
            mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar,
                    mTvwAvatar, null, mApplication.getReengAccountBusiness().getCurrentAccount(), null);
        }
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new PollCreateAdapter(mApplication, listOption, this);
            mListView.setAdapter(mAdapter);
        } else {
            mAdapter.setListData(listOption);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void scrollListViewToBottom() {
        mListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                mListView.setSelection(mAdapter.getCount() - 1);
            }
        });
    }

    private void checkEnableButtonCreate() {
        if (listOption != null && listOption.size() > 1) {
            if (TextUtils.isEmpty(mEdtLabel.getText().toString().trim())) {
                mAbCreate.setEnabled(false);
            } else {
                mAbCreate.setEnabled(true);
            }
        } else {
            mAbCreate.setEnabled(false);
        }
    }

    private void setListener() {
        mAbBack.setOnClickListener(this);
        mViewMulti.setOnClickListener(this);
        mAbCreate.setOnClickListener(this);
        mImgAddOption.setOnClickListener(this);

        mEdtAddOption.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d(TAG, "onEditorAction: " + actionId);
                if (actionId == KeyEvent.KEYCODE_ENTER) {
                    checkAndAddOption();
                    return true;
                }
                return false;
            }
        });
        mEdtLabel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkEnableButtonCreate();
            }
        });
    }

    private void checkAndAddOption() {
        String content = mEdtAddOption.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            mParentActivity.showToast(R.string.poll_msg_item_empty);
        } else if (isExistOption(content)) {
            mParentActivity.showToast(R.string.poll_msg_item_exist);
        } else {
            listOption.add(mEdtAddOption.getText().toString().trim());
            setAdapter();
            mEdtAddOption.setText("");
            scrollListViewToBottom();
            checkEnableButtonCreate();
        }
    }

    private boolean isExistOption(String entry) {
        if (listOption.isEmpty()) {
            return false;
        }
        for (String item : listOption) {
            if (entry.equals(item)) {
                return true;
            }
        }
        return false;
    }

    private void handlerCreateVote() {
        String label = mEdtLabel.getText().toString().trim();
        int choice = 1;
        if (isMulti) {
            choice = listOption.size();
        }
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        PollRequestHelper.getInstance(mApplication).createPoll(mThreadMessage.getServerId(), label, listOption, choice, new PollRequestHelper.PollRequestListener() {
            @Override
            public void onError(int errorCode) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

            @Override
            public void onSuccess(PollObject poll) {
                mParentActivity.hideLoadingDialog();
                ThreadMessage threadMessage = mMessageBusiness.findThreadByThreadId(mThreadId);
                ReengMessage reengMessage = mMessageBusiness.createMessagePollAfterRequest(threadMessage, poll, true, false);
                mMessageBusiness.notifyNewMessage(reengMessage, threadMessage);
                // mMessageBusiness.notifyReengMessage(mApplication, threadMessage, reengMessage, threadMessage.getThreadType());
                mParentActivity.onBackPressed();
                //getPollDetail(poll.getPollId());
            }
        });
        Log.d(TAG, "handlerCreateVote: " + label);
    }
}
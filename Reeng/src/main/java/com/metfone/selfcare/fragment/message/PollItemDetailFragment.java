package com.metfone.selfcare.fragment.message;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.PollMessageActivity;
import com.metfone.selfcare.adapter.PollItemDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.fragment.BaseListViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/29/2016.
 */
public class PollItemDetailFragment extends BaseListViewFragment implements
        View.OnClickListener,
        BaseListViewFragment.EmptyViewListener,
        PollRequestHelper.PollRequestListener {
    private static final String TAG = PollItemDetailFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private PollMessageActivity mParentActivity;
    private PollObject mCurrentPoll;
    private String pollId, pollItemId;
    private String emptyNote;
    private EllipsisTextView mTvwTitle;
    private TextView mAbCreate;
    private View mAbBack;
    private ListView mListView;
    private PollItemDetailAdapter mAdapter;
    private ArrayList<String> listMemberVoted = new ArrayList<>();

    public PollItemDetailFragment() {

    }

    public static PollItemDetailFragment newInstance(String pollId, String pollItemId) {
        PollItemDetailFragment fragment = new PollItemDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constants.MESSAGE.POLL_ID, pollId);
        args.putString(Constants.MESSAGE.POLL_ITEM_ID, pollItemId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        setHasOptionsMenu(true);
        Log.i(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_listview, container, false);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mRes = mParentActivity.getResources();
        setToolBar(inflater);
        findComponentViews(rootView, inflater, container);
        getData(savedInstanceState);
        drawDetail();
        setListener();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (PollMessageActivity) activity;
       /* try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.MESSAGE.POLL_ID, pollId);
        outState.putString(Constants.MESSAGE.POLL_ITEM_ID, pollItemId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onError(int errorCode) {
        hideEmptyView();
        showEmptyNote(emptyNote);
    }

    @Override
    public void onSuccess(PollObject poll) {
        hideEmptyView();
        Log.d(TAG, "poll: " + poll.toString());
        mCurrentPoll = poll;
        //PollRequestHelper.getInstance(mApplication).processPollObjectAfterRequestDetail(poll);
        if (poll.getListItems() != null && !poll.getListItems().isEmpty()) {
            listMemberVoted = poll.getListItems().get(0).getMemberVoted();
        } else {
            listMemberVoted = new ArrayList<>();
        }
        setAdapter();
        if (listMemberVoted.isEmpty()) {
            showEmptyNote(emptyNote);
        }
    }

    private void setToolBar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        mTvwTitle = (EllipsisTextView) mParentActivity.getToolBarView().findViewById(R.id.ab_title);
        View mViewOption = mParentActivity.getToolBarView().findViewById(R.id.ab_more_btn);
        mAbBack = mParentActivity.getToolBarView().findViewById(R.id.ab_back_btn);
        mAbCreate = (TextView) mParentActivity.getToolBarView().findViewById(R.id.ab_agree_text);
        mTvwTitle.setText(R.string.poll_detail);
        mViewOption.setVisibility(View.GONE);
        mAbCreate.setVisibility(View.GONE);
    }


    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mListView = (ListView) rootView.findViewById(R.id.list_view);
        createView(inflater, mListView, this);
        emptyNote = mRes.getString(R.string.list_empty);
        /*showEmptyNote(contentEmpty);
        hideEmptyView();*/
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            pollId = savedInstanceState.getString(Constants.MESSAGE.POLL_ID);
            pollItemId = savedInstanceState.getString(Constants.MESSAGE.POLL_ITEM_ID);
        } else if (getArguments() != null) {
            pollId = getArguments().getString(Constants.MESSAGE.POLL_ID);
            pollItemId = getArguments().getString(Constants.MESSAGE.POLL_ITEM_ID);
        }
    }

    private void drawDetail() {
        showProgressLoading();
        listMemberVoted = new ArrayList<>();
        /*for (int i = 0; i < 20; i++) {
            PollItem item = new PollItem();
            item.setTitle("Noi dung vote " + i);
            pollItems.add(item);
        }*/
        setAdapter();
        PollRequestHelper.getInstance(mApplication).getPollItemDetail(pollId, pollItemId, this);
    }

    private void setListener() {
        mAbBack.setOnClickListener(this);
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new PollItemDetailAdapter(mApplication, listMemberVoted);
            mListView.setAdapter(mAdapter);
        } else {
            mAdapter.setListData(listMemberVoted);
            mAdapter.notifyDataSetChanged();
        }
    }
}
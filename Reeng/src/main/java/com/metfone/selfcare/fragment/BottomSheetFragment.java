package com.metfone.selfcare.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    @BindView(R.id.tvTakeAPhoto)
    TextView tvTakeAPhoto;
    @BindView(R.id.tvSelectFromGallery)
    TextView tvSelectFromGallery;
    @BindView(R.id.tvRemovePhoto)
    TextView tvRemovePhoto;
    private BottomSheetItemClick bottomSheetItemClick;

    public void setBottomSheetItemClick(BottomSheetItemClick bottomSheetItemClick) {
        this.bottomSheetItemClick = bottomSheetItemClick;
    }

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        tvTakeAPhoto.setOnClickListener(this);
        tvSelectFromGallery.setOnClickListener(this);
        tvRemovePhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvTakeAPhoto:
                bottomSheetItemClick.onItemClick(Constants.MENU.CAPTURE_IMAGE);
                break;
            case R.id.tvSelectFromGallery:
                bottomSheetItemClick.onItemClick(Constants.MENU.SELECT_GALLERY);
                break;
            case R.id.tvRemovePhoto:
                bottomSheetItemClick.onItemClick(Constants.MENU.REMOVE_AVATAR);
                break;
        }
        dismiss();
    }

    public interface BottomSheetItemClick{
        void onItemClick(int itemId);
    }
}
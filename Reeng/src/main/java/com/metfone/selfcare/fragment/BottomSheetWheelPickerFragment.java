package com.metfone.selfcare.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aigestudio.wheelpicker.WheelPicker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.VerifyInformationActivity;
import com.metfone.selfcare.util.EnumUtils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

public class BottomSheetWheelPickerFragment extends BottomSheetDialogFragment {
    @BindView(R.id.wheelPicker)
    WheelPicker wheelPicker;
    @BindView(R.id.tvDone)
    TextView tvDone;
    List<String> data;

    public BottomSheetWheelPickerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.NewBottomSheetDialogTheme);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        data = Arrays.asList(getResources().getStringArray(R.array.selected_id_option));
        wheelPicker.setData(data);
        if (getArguments() != null) {
            wheelPicker.setSelectedItemPosition(data.indexOf(getArguments().getString(EnumUtils.OBJECT_KEY)), false);
        }
    }

    @Optional
    @OnClick(R.id.tvDone)
    public void onClick(View view) {
        if (view.getId() == R.id.tvDone) {
            if (getActivity() != null) {
                ((VerifyInformationActivity) getActivity()).setEdtIDType(data.get(wheelPicker.getCurrentItemPosition()));
            }
            dismiss();
        }
    }

    private void setupBottomSheet(DialogInterface dialogInterface) {
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
        View bottomSheet = bottomSheetDialog.findViewById(
                com.google.android.material.R.id.design_bottom_sheet);
//                ?: return
        if (bottomSheet != null)
            bottomSheet.setBackgroundColor(Color.TRANSPARENT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        View contentView = View.inflate(getContext(), R.layout.fragment_bottom_sheet_wheel_picker, null);
        if (getDialog() != null) {
            getDialog().setContentView(contentView);
        }

//        ((View) contentView.getParent()).setBackgroundColor(Color.TRANSPARENT);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                setupBottomSheet(dialogInterface);
            }
        });
        dialog.setContentView(contentView);
        return dialog;
    }

}
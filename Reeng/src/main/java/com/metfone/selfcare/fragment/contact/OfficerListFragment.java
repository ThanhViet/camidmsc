package com.metfone.selfcare.fragment.contact;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.Toast;

import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.adapter.OfficerListNewAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 6/27/14.
 */
public class OfficerListFragment extends BaseRecyclerViewFragment implements
        InitDataListener,
        BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = OfficerListFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private OfficerBusiness mOfficerBusiness;
    private Resources mRes;
    private OnFragmentInteractionListener mListener;
    private ContactListActivity mParentActivity;
    private View rootView, abView, mViewAbDetail, mViewAbSearch;
    private RecyclerView mRecyclerView;
    private EllipsisTextView mTvTitle;
    private OfficerListNewAdapter mAdapter;
    private ArrayList<OfficerAccount> mListOfficers;
    //private InitListAsyncTask mInitListAsyncTask;
    private ImageView mImgSearch, mImgMore, mImgSearchBack, mImgBack;
    private ReengSearchView mReengSearchView;
    private SearchAsyncTask mSearchAsyncTask;
    private boolean runSearch = true;
    private String contentEmpty;
    private int typeListThread;
    private Handler mHandler;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactListFragment.
     */
    public static OfficerListFragment newInstance(int typeList) {
        OfficerListFragment fragment = new OfficerListFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CONTACT.TYPE_LIST, typeList);
        fragment.setArguments(args);
        return fragment;
    }

    public OfficerListFragment() {
        // Required empty public constructor
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ContactListActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mRes = mParentActivity.getResources();
        mOfficerBusiness = mApplication.getOfficerBusiness();
        mHandler = new Handler();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        if (getArguments() != null) {
            typeListThread = getArguments().getInt(Constants.CONTACT.TYPE_LIST, Constants.CONTACT.SHOW_LIST_GROUP);
        } else if (savedInstanceState != null) {
            typeListThread = savedInstanceState.getInt(Constants.CONTACT.TYPE_LIST, Constants.CONTACT.SHOW_LIST_GROUP);
        }
        findComponentViews(rootView, container, inflater);
        setViewListeners();
        mTvTitle.setText(mRes.getString(R.string.menu_officials));
        return rootView;
    }

    @Override
    public void onStart() {
        ListenerHelper.getInstance().addInitDataListener(this);
        mHandler = new Handler();
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "OnResume");
        if (mApplication.isDataReady()) {
            reloadData();
        } else {
            showProgressLoading();
        }
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        ListenerHelper.getInstance().removeInitDataListener(this);
       /* if (mInitListAsyncTask != null) {
            mInitListAsyncTask.cancel(true);
            mInitListAsyncTask = null;
        }*/
        if (mSearchAsyncTask != null) {
            mSearchAsyncTask.cancel(true);
            mSearchAsyncTask = null;
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CONTACT.TYPE_LIST, typeListThread);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        mHandler = null;
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mHandler = null;
    }

    @Override
    public void onDataReady() {
        Log.d(TAG, "load data complate");
        if (mHandler == null) {
            return;
        }
        mHandler.post(this::reloadData);
    }

    @Override
    public void onRetryClick() {
       /* Intent strangerList = new Intent(mParentActivity, HomeActivityOld.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.HOME.TAB, Constants.HOME.STRANGER_TAB);
        strangerList.putExtras(bundle);
        mParentActivity.startActivity(strangerList);
        mParentActivity.finish();*/
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_contacts, null));
        abView = mParentActivity.getToolBarView();
        mImgBack = abView.findViewById(R.id.ab_back_btn);
        mImgMore = abView.findViewById(R.id.ab_more_btn);
        mImgSearch = abView.findViewById(R.id.ab_search_btn);
        mImgSearch.setVisibility(View.GONE);
        mTvTitle = abView.findViewById(R.id.ab_title);
        mImgSearchBack = abView.findViewById(R.id.ab_search_back);
        mViewAbDetail = abView.findViewById(R.id.ab_detail_ll);
        mViewAbSearch = abView.findViewById(R.id.ab_search_ll);
        mReengSearchView = abView.findViewById(R.id.ab_search_view);

        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        contentEmpty = mRes.getString(R.string.list_empty);
        mImgMore.setVisibility(View.GONE);
        //mListView.setFastScrollEnabled(true);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
        showOrHideSearchView(false, true);
    }

    private void setViewListeners() {
        setAbBackListener();
        setButtonSearchListener();
        setButtonBackSearchListener();
        setReengSearchViewListener();
    }

    private void showOrHideSearchView(boolean show, boolean runSearch) {
        this.runSearch = runSearch;
        mReengSearchView.setText("");
        if (show) {
            mViewAbSearch.setVisibility(View.VISIBLE);
            mViewAbDetail.setVisibility(View.GONE);
            contentEmpty = mRes.getString(R.string.not_find);
        } else {
            mViewAbSearch.setVisibility(View.GONE);
            mViewAbDetail.setVisibility(View.VISIBLE);
            contentEmpty = mRes.getString(R.string.list_empty);
        }
    }

    private void setButtonBackSearchListener() {
        mImgSearchBack.setOnClickListener(view -> {
            InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
            showOrHideSearchView(false, true);
        });
    }

    /**
     * search Contact
     */
    private void setReengSearchViewListener() {
        mReengSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String content = s.toString();
                if (!TextUtils.isEmpty(content)) {
                    if (runSearch) {
                        showProgressLoading();
                        searchAsyncTask(content);
                    }
                } else {
                    if (mOfficerBusiness.getOfficerAccountSv() != null && !mOfficerBusiness.getOfficerAccountSv().isEmpty()) {
                        mListOfficers = mOfficerBusiness.getOfficerAccountSv();
                        notifiChangeAdapter(mListOfficers);
                        mHandler.removeCallbacks(search);
                    }
                }

            }
        });

        mReengSearchView.setOnEditorActionListener((textView, keyCode, keyEvent) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                InputMethodUtils.hideSoftKeyboard(mReengSearchView, mParentActivity);
            }
            return false;
        });
        mReengSearchView.setOnTouchListener((view, motionEvent) -> {
            // assus zenphone
            //mSearchView.clearFocus();
            InputMethodUtils.showSoftKeyboard(mParentActivity, mReengSearchView);
            return false;
        });
    }

    // new searchAsynctask
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void searchAsyncTask(String textSearch) {
        Log.d(TAG, "searchContactAsynctask ");
        if (mHandler == null) mHandler = new Handler();
        mHandler.removeCallbacks(search);
        mHandler.postDelayed(search, 1000);

        /*if (mSearchAsyncTask != null) {
            mSearchAsyncTask.cancel(true);
            mSearchAsyncTask = null;
        }
        if (mListOfficers == null) {
            return;
        }
        mSearchAsyncTask = new SearchAsyncTask();
        mSearchAsyncTask.setListOfficers(mListOfficers);
        if (Version.hasHoneycomb()) {
            mSearchAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
        } else {
            mSearchAsyncTask.execute(textSearch);
        }*/
    }

    Runnable search = new Runnable() {
        @Override
        public void run() {
            if (mReengSearchView.getVisibility() != View.VISIBLE) {
                return;
            }
            String content = mReengSearchView.getText().toString();
            if (TextUtils.isEmpty(content)) {
                return;
            }
            OfficerBusiness.RequestOfficerResponseListener listener = new OfficerBusiness
                    .RequestOfficerResponseListener() {
                @Override
                public void onResponse(ArrayList<OfficerAccount> listAccount) {
                    Log.i(TAG, "size: " + listAccount.size());
                    mListOfficers = listAccount;
                    if (mAdapter == null) {
                        setAdapter(mListOfficers);
                    } else {
                        notifiChangeAdapter(mListOfficers);
                    }
                    if (mListOfficers == null || mListOfficers.isEmpty()) {
                        showEmptyNote(contentEmpty);
                    } else {
                        hideEmptyView();
                    }
                }

                @Override
                public void onError(int errorCode, String msg) {
                    hideEmptyView();
                    if (mListOfficers == null || mListOfficers.isEmpty()) {
                        mParentActivity.showToast(mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                    }
                }
            };
            RoomChatRequestHelper.getInstance(mApplication).searchOfficerAccountFromSv(content, listener);
        }
    };

    // search asyntask
    private class SearchAsyncTask extends AsyncTask<String, Void, ArrayList<OfficerAccount>> {
        private ArrayList<OfficerAccount> list;

        public void setListOfficers(ArrayList<OfficerAccount> officers) {
            this.list = officers;
        }

        @Override
        protected ArrayList<OfficerAccount> doInBackground(String... params) {
            String textSearch = params[0];
            return officerSearchList(textSearch, list);
        }

        @Override
        protected void onPostExecute(ArrayList<OfficerAccount> result) {
            notifiChangeAdapter(result);
            super.onPostExecute(result);
        }
    }

    // thread search list
    private ArrayList<OfficerAccount> officerSearchList(String contentSearch, ArrayList<OfficerAccount> listAccounts) {
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());
        if (contentSearch == null || contentSearch.length() <= 0) {
            if (listAccounts != null) {
                return listAccounts;
            } else {
                return new ArrayList<>();
            }
        } else {
            ArrayList<OfficerAccount> listSearchs;
            String[] listTexts = contentSearch.split("\\s+");
            if (listAccounts != null) {
                ArrayList<OfficerAccount> list;
                listSearchs = new ArrayList<>(listAccounts);
                for (String item : listTexts) {
                    if (item.trim().length() > 0) {
                        //item = TextHelper.getInstant().convertUnicodeToAscci(item);
                        list = new ArrayList<>();
                        for (OfficerAccount account : listSearchs) {
                            String name = TextHelper.getInstant().convertUnicodeToAscci(account.getName());
                            if (account.getServerId() != null && name != null && name.contains(item)) {
                                list.add(account);
                            }
                        }
                        listSearchs = list;
                    }
                }
            } else {
                listSearchs = new ArrayList<>();
            }
            return listSearchs;
        }
    }

    private void setAbBackListener() {
        mImgBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }

    private void setButtonSearchListener() {
        mImgSearch.setOnClickListener(view -> {
            showOrHideSearchView(true, true);
            mReengSearchView.requestFocus();
            mReengSearchView.setSelection(0);
            InputMethodUtils.showSoftKeyboard(mParentActivity, mReengSearchView);
        });
    }

    private void reloadData() {
        mListOfficers = mOfficerBusiness.getOfficerAccountSv();
        if (mListOfficers == null) {
            mListOfficers = new ArrayList<>();
        }
        if (!mListOfficers.isEmpty()) {
            hideEmptyView();
        }
        String contentSearch = mReengSearchView.getText().toString();
        if (mViewAbSearch.getVisibility() == View.VISIBLE && contentSearch != null &&
                contentSearch.trim().length() > 0) {
            searchAsyncTask(contentSearch.trim());
        } else {
            drawListView(mListOfficers);
        }
        // reload from sv
        OfficerBusiness.RequestOfficerResponseListener listener = new OfficerBusiness.RequestOfficerResponseListener() {
            @Override
            public void onResponse(ArrayList<OfficerAccount> listAccount) {
                mListOfficers = listAccount;
                drawListView(mListOfficers);
                if (mListOfficers == null || mListOfficers.isEmpty()) {
                    showEmptyNote(contentEmpty);
                } else {
                    hideEmptyView();
                }
            }

            @Override
            public void onError(int errorCode, String msg) {
                hideEmptyView();
                if (mListOfficers == null || mListOfficers.isEmpty()) {
                    mParentActivity.showToast(mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                }
            }
        };
        RoomChatRequestHelper.getInstance(mApplication).getOfficerAccountFromSv(listener);
    }

    private void setAdapter(ArrayList<OfficerAccount> list) {
        mAdapter = new OfficerListNewAdapter(mApplication, list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        // mRecyclerView.addItemDecoration(new DividerItemDecoration(mParentActivity, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        setItemListViewListener();
    }

    private void notifiChangeAdapter(ArrayList<OfficerAccount> list) {
        if (list == null || list.isEmpty()) {
            if (isShowProgressLoading()) {
                //chua load xong thi an di
                // mTvNoteView.setVisibility(View.GONE);
            } else {
                showEmptyNote(contentEmpty);
            }
        } else {
            hideEmptyView();
        }
        if (mAdapter == null) {
            setAdapter(list);
            return;
        }
        mAdapter.setOfficerList(list);
        mAdapter.notifyDataSetChanged();
    }

    private void setItemListViewListener() {
        // on touch
        mRecyclerView.setOnTouchListener((v, event) -> {
            InputMethodUtils.hideSoftKeyboard(mParentActivity);
            return false;
        });
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                OfficerAccount account = (OfficerAccount) object;
                if (account == null || TextUtils.isEmpty(account.getServerId())) {
                    return;
                }
                showOrHideSearchView(false, false);
                ThreadMessage threadMessage;
                if (account.getType() == OfficerAccountConstant.TYPE_OFFICER) {
                    threadMessage = mApplication.getMessageBusiness().
                            findExistingOrCreateOfficerThread(account.getServerId(),
                                    account.getName(), account.getAvatarUrl(), OfficerAccountConstant
                                            .ONMEDIA_TYPE_NONE);
                    if (threadMessage != null) {
                        mListener.navigateToThreadDetail(threadMessage);
                    }
                } else if (account.getType() == OfficerAccountConstant.TYPE_STAR_ROOM) {
//                    DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mParentActivity,
//                            account.getServerId(), account.getName(), account.getAvatarUrl());
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        // long click
        //mListView.setOnItemLongClickListener(itemLongClickHandler);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void drawListView(ArrayList<OfficerAccount> list) {
        String contentSearch = mReengSearchView.getText().toString();
        if (mViewAbSearch.getVisibility() == View.VISIBLE &&
                contentSearch != null && contentSearch.trim().length() > 0) {
            searchAsyncTask(contentSearch.trim());
        } else {
            if (mAdapter == null) {
                setAdapter(list);
            } else {
                notifiChangeAdapter(list);
            }
        }
    }

    public interface OnFragmentInteractionListener {
        void navigateToThreadDetail(ThreadMessage threadMessage);
    }
}
package com.metfone.selfcare.fragment.transfermoney;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.TransferMoneyActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.TransferMoneyBusiness;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;

/**
 * Created by thaodv on 6/24/2015.
 */
public class MoneyConfirmFragment extends Fragment
        implements TransferMoneyBusiness.RequestTransferCallback, /*SmsReceiverListener,*/ MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String REQUEST_ID = "requestId";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String MONEY_AMOUNT = "MONEY_AMOUNT";
    private static final String MONEY_AMOUNT_STRING = "MONEY_AMOUNT_STRING";
    private TextView mTvNotice;
    private EditText mEdtOtp;
    private Button mBtnConfirm;
    private String otpSMS;
    private String requestId, friendPhoneNumber, moneyAmountString;
    private TextView mAbTitle;
    private ImageView mAbMoreBtn, mAbBackBtn;
    private long moneyAmount;
    private TransferMoneyActivity mParentActivity;
    private ApplicationController mApplication;
    private TransferMoneyBusiness transferMoneyBusiness;
    private MessageBusiness messageBusiness;
    private Handler mHandler;
    private Resources mRes;

    public static MoneyConfirmFragment newInstance(String requestId, String friendPhoneNumber,
                                                   long moneyAmount, String moneyAmountString) {
        MoneyConfirmFragment fragment = new MoneyConfirmFragment();
        Bundle args = new Bundle();
        args.putString(REQUEST_ID, requestId);
        args.putString(PHONE_NUMBER, friendPhoneNumber);
        args.putLong(MONEY_AMOUNT, moneyAmount);
        args.putString(MONEY_AMOUNT_STRING, moneyAmountString);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof Activity) {
            mParentActivity = (TransferMoneyActivity) context;
            mApplication = (ApplicationController) mParentActivity.getApplication();
            transferMoneyBusiness = TransferMoneyBusiness.getInstance(mApplication);
            messageBusiness = mApplication.getMessageBusiness();
            mRes = mParentActivity.getResources();
        }
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_money_confirm, container, false);
        findComponentViews(rootView);
        setListeners();
        getDataAndDraw(savedInstanceState);

        mApplication.registerSmsOTPObserver();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(REQUEST_ID, requestId);
        outState.putString(PHONE_NUMBER, friendPhoneNumber);
        outState.putLong(MONEY_AMOUNT, moneyAmount);
        outState.putString(MONEY_AMOUNT_STRING, moneyAmountString);
        super.onSaveInstanceState(outState);
    }

    private void setListeners() {
        mEdtOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                otpSMS = mEdtOtp.getText().toString().trim();
                if (TextUtils.isEmpty(otpSMS)) {
                    mBtnConfirm.setEnabled(false);
                } else {
                    mBtnConfirm.setEnabled(true);
                }
            }
        });

        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //confirm payment
                mParentActivity.showLoadingDialog("", R.string.processing);
                transferMoneyBusiness.doConfirmPayment(MoneyConfirmFragment.this,
                        requestId, otpSMS);
            }
        });

        mAbBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodUtils.hideSoftKeyboard(mEdtOtp, mParentActivity);
                mParentActivity.onBackPressed();
            }
        });
    }

    private void findComponentViews(View rootView) {
        mTvNotice = (TextView) rootView.findViewById(R.id.confirm_text);
        mEdtOtp = (EditText) rootView.findViewById(R.id.otp_input);
        mBtnConfirm = (Button) rootView.findViewById(R.id.confirm_button);
        View abView = mParentActivity.getmViewActionBar();
//        View abView = mChatActivity.getViewCustomActionBar();

        mAbTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        mAbMoreBtn = (ImageView) abView.findViewById(R.id.ab_more_btn);
        mAbBackBtn = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mEdtOtp.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEdtOtp.requestFocus();
            }
        }, 150);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
    }

    private void drawActionBar(String friendName) {
        mAbTitle.setText(mRes.getString(R.string.authentication));
        mAbMoreBtn.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        SmsReceiver.addSMSReceivedListener(this);
//        MySMSBroadcastReceiver.addSMSReceivedListener(this);
        mHandler = new Handler();
    }

    @Override
    public void onPause() {
        super.onPause();
//        MySMSBroadcastReceiver.removeSMSReceivedListener(this);
//        SmsReceiver.removeSMSReceivedListener(this);
        mHandler = null;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


//    @Override
//    public void onSmsPasswordReceived(String password) {
//
//    }
//
//    @Override
//    public void onSmsPasswordLixiReceived(final String password) {
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                if (mEdtOtp != null) {
//                    mEdtOtp.setText(password);
//                    mEdtOtp.setSelection(password.length());
//                }
//            }
//        });
//    }
//
//    @Override
//    public boolean onSmsChatReceived(String smsSender,String smsContent) {
//        return false;
//    }

    private void getDataAndDraw(Bundle savedInstanceState) {
        if (getArguments() != null) {
            requestId = getArguments().getString(REQUEST_ID);
            friendPhoneNumber = getArguments().getString(PHONE_NUMBER);
            moneyAmount = getArguments().getLong(MONEY_AMOUNT);
            moneyAmountString = getArguments().getString(MONEY_AMOUNT_STRING);
        } else if (savedInstanceState != null) {
            requestId = savedInstanceState.getString(REQUEST_ID);
            friendPhoneNumber = savedInstanceState.getString(PHONE_NUMBER);
            moneyAmount = savedInstanceState.getLong(MONEY_AMOUNT);
            moneyAmountString = savedInstanceState.getString(MONEY_AMOUNT_STRING);
        }
        String friendName = messageBusiness.getFriendName(friendPhoneNumber);
        mTvNotice.setText(String.format(mApplication.getString(R.string.confirm_transfer_money),
                moneyAmountString, friendName, friendPhoneNumber));
        drawActionBar(friendName);
    }

    private void showDialogSuccess(String msg) {
        ClickListener.IconListener listener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                if (menuId == Constants.ACTION.MONEY_AMOUNT_TRANSFER_SUCCESS) {
                    mParentActivity.onBackPressed();
                }
            }
        };
        String title = mRes.getString(R.string.note_title);
        String labelOk = getString(R.string.ok);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOk, null, listener, null, Constants.ACTION.MONEY_AMOUNT_TRANSFER_SUCCESS);
    }

    @Override
    public void onSuccess(String timeTransfer, String unitMoney) {
        mParentActivity.hideLoadingDialog();
        // gui ban tin
        if (!TextUtils.isEmpty(friendPhoneNumber)) {
            ThreadMessage threadMessage = messageBusiness.findExistingOrCreateNewThread(friendPhoneNumber);
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (threadMessage != null) {
                transferMoneyBusiness.sendTransferMessage(threadMessage, myNumber,
                        friendPhoneNumber, moneyAmountString, unitMoney, timeTransfer);
            }
        }
        //        showDialogSuccess(mRes.getString(R.string.transfer_money_success));
        //ko show dialog nua ma hien thi toast
        mParentActivity.trackingEvent(R.string.ga_category_transfer_money,
                R.string.ga_action_interaction, R.string.ga_label_success);
        mParentActivity.onBackPressed();
        mParentActivity.showToast(R.string.transfer_money_success);
    }

    @Override
    public void onError(String message) {
        mParentActivity.hideLoadingDialog();
        mParentActivity.showError(message, "");
        mParentActivity.trackingEvent(R.string.ga_category_transfer_money,
                R.string.ga_action_interaction, R.string.ga_label_error);
    }

    //XU ly phan sms otp
//    @Override
//    public boolean onOTPReceived(final String password) {
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                if (mEdtOtp != null) {
//                    mEdtOtp.setText(password);
//                    mEdtOtp.setSelection(password.length());
//                }
//            }
//        });
//        return true;
//    }

    @Override
    public void onOTPReceived(Intent intent) {

    }

    @Override
    public void onOTPTimeOut(){
    }
}
package com.metfone.selfcare.fragment.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.AlbumViewActivity;
import com.metfone.selfcare.adapter.AlbumProfileAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.listeners.UploadImageListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AlbumPreviewFragment extends Fragment {
    private static final String TAG = AlbumPreviewFragment.class.getSimpleName();

    private AlbumViewActivity mParentActivity;
    private ApplicationController mApplication;
    private View mRootView;
    TextView mTitle;
    private Handler mHandler;
    private RecyclerView mRecyclerView;

    private AlbumProfileAdapter mAdapter;

    private ArrayList<ImageProfile> mListImage;
    private UploadImageListener mUploadImageListener;
    private String mName;
    private String phoneNumber;
    private EventOnMediaHelper eventOnMediaHelper;

    public AlbumPreviewFragment() {
    }

    public static AlbumPreviewFragment newInstance(String name, String jid, ArrayList<ImageProfile> listImage) {
        AlbumPreviewFragment fragment = new AlbumPreviewFragment();
        Bundle args = new Bundle();
        args.putString(Constants.ONMEDIA.PARAM_IMAGE.NAME, name);
        args.putString(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, jid);
        args.putSerializable(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, listImage);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mName = getArguments().getString(Constants.ONMEDIA.PARAM_IMAGE.NAME);
            phoneNumber = getArguments().getString(Constants.ONMEDIA.PARAM_IMAGE.MSISDN);
            if (phoneNumber != null && phoneNumber.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
                mListImage = mApplication.getImageProfileBusiness().getImageProfileList();
            } else {
                mListImage = (ArrayList<ImageProfile>) getArguments().getSerializable(Constants.ONMEDIA.PARAM_IMAGE
                        .LIST_IMAGE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        findView();
        setToolBar();
        setListener();
        drawAlbum();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mParentActivity = (AlbumViewActivity) getActivity();
        eventOnMediaHelper = new EventOnMediaHelper(mParentActivity);
        mApplication = (ApplicationController) mParentActivity.getApplication();
    }

    private void setListener() {
        mUploadImageListener = new UploadImageListener() {
            @Override
            public void onUploadSuccess(ImageProfile image) {
                Log.i(TAG, "onUploadSuccess");
            }

            @Override
            public void onUploadFail(ImageProfile image) {
                Log.i(TAG, "onUploadFail");
            }

            @Override
            public void onUploadEnd(final int type, final ArrayList<ImageProfile> imageProfiles, final String message) {
                Log.i(TAG, "type= " + type + ". onUploadEnd");
                if (type == Constants.UPLOAD.HTTP_STATE_UPLOAD_SUCCESS) {
                    if (imageProfiles != null && !imageProfiles.isEmpty()) {
                        int size = imageProfiles.size();
                        if (size > 1) {
                            mParentActivity.showToast(String.format(getString(R.string.upload_image_profile_success),
                                    size), Toast.LENGTH_LONG);
                        } else {
                            mParentActivity.showToast(String.format(getString(R.string.upload_image_profile_success),
                                    size), Toast.LENGTH_LONG);
                        }
                    } else {
                        mParentActivity.showToast(R.string.upload_image_profile_success3);
                    }

                    //refresh
                    if (phoneNumber.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
                        if (mHandler != null) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    mListImage = mApplication.getImageProfileBusiness().getImageProfileList();
                                    if (mAdapter == null) {
                                        drawAlbum();
                                    } else {
                                        mAdapter.setListImage(mListImage);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            });
                        }
                    }
                } else if (type == Constants.UPLOAD.HTTP_STATE_UPLOAD_FAILED_EXCEEDED) {
                    String content = message;
                    try {
                        content = new String(content.getBytes("ISO-8859-1"), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "onUploadExceeded UnsupportedEncodingException", e);
                    } catch (Exception e) {
                        Log.e(TAG, "onUploadExceeded Exception", e);
                    }
                    if (!TextUtils.isEmpty(content))
                        mParentActivity.showToast(content, Toast.LENGTH_LONG);
                    else
                        mParentActivity.showToast(R.string.upload_image_profile_fail);
                } else {
                    mParentActivity.showToast(R.string.upload_image_profile_fail);
                }
            }

            @Override
            public void onStartUpload() {

            }

        };
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHandler = null;
    }

    private void findView() {
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mApplication, 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        boolean isMyProfile = mApplication.getReengAccountBusiness().getJidNumber().equals(phoneNumber);
        mAdapter = new AlbumProfileAdapter(mListImage, (ApplicationController) mParentActivity.getApplication(),
                isMyProfile);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                showImageDetail(pos);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
    }

    private void drawAlbum() {
        mTitle.setText(mName);
        if (mListImage != null && !mListImage.isEmpty()) {
            mAdapter.setListImage(mListImage);
            mAdapter.notifyDataSetChanged();
        } else {
            // mParentActivity.onBackPressed();
            mParentActivity.finish();
        }
    }

    private void setToolBar() {
        Toolbar toolbar = mParentActivity.getToolBarView();
        mTitle = (TextView) toolbar.findViewById(R.id.ab_title);
        mTitle.setVisibility(View.VISIBLE);
        mTitle.setText(mName);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
        drawAlbum();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mHandler == null) {
            mHandler = new Handler();
        }
        ListenerHelper.getInstance().addUploadImageListener(mUploadImageListener);
        if (phoneNumber.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            mListImage = mApplication.getImageProfileBusiness().getImageProfileList();
            if (mAdapter == null) {
                drawAlbum();
            } else {
                mAdapter.setListImage(mListImage);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        ListenerHelper.getInstance().removeUploadImageListener(mUploadImageListener);
    }

    private void showImageDetail(int position) {
        if (mListImage == null || mListImage.isEmpty()) {
            return;
        }
        if (phoneNumber.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            eventOnMediaHelper.showImageDetail(mName, phoneNumber, null, position,
                    FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        } else {
            eventOnMediaHelper.showImageDetail(mName, phoneNumber, mListImage, position,
                    FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        }

        /*Intent intent = new Intent(mParentActivity.getApplication(), PreviewImageProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PreviewImageProfileActivity.PARAM_LIST_IMAGE, mListImage);
        intent.putExtra(PreviewImageProfileActivity.PARAM_CURRENT_IMAGE, position);
        intent.putExtra(PreviewImageProfileActivity.PARAM_THREAD_NAME, mName);
        intent.putExtra(PreviewImageProfileActivity.PARAM_HIRE_REMOVE_BUTTON, mParentActivity.isHideRemove());
        intent.putExtra(PreviewImageProfileActivity.PARAM_CONTACT_TYPE, contactType);
        intent.putExtra(PreviewImageProfileActivity.PARAM_PHONE_NUMBER, phoneNumber);
        mParentActivity.startActivity(intent);*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Flag 1" + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "Flag 2");
            switch (requestCode) {
                case AlbumViewActivity.REQUEST_PREVIEW_IMAGE:
                    Log.d(TAG, "Flag 3");
                    mListImage = (ArrayList<ImageProfile>) data.getSerializableExtra("data");
                    if (mAdapter == null) {
                        drawAlbum();
                    } else {
                        mAdapter.setListImage(mListImage);
                        mAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
    }
}

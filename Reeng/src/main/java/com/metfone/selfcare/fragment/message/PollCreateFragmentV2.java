package com.metfone.selfcare.fragment.message;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.PollMessageActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PollCreateFragmentV2 extends BaseFragment implements View.OnClickListener {

//    @BindView(R.id.iv_avatar)
//    CircleImageView ivAvatar;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.rec_options)
    RecyclerView recOptions;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.et_add)
    EditText etAdd;
    @BindView(R.id.tv_add)
    ImageView tvAdd;
    @BindView(R.id.btnCreateSurvey)
    Button btnCreateSurvey;
//    @BindView(R.id.ll_add_vote)
//    LinearLayout llAddVote;
    Unbinder unbinder;

    @SuppressLint("StaticFieldLeak")
    private static PollCreateFragmentV2 self;

    private static PollCreateFragmentV2 self() {
        return self;
    }

    public static PollCreateFragmentV2 newInstance(int threadId) {

        Bundle args = new Bundle();
        args.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        PollCreateFragmentV2 fragment = new PollCreateFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    private View btnBack;
    private TextView btnSend;

    private ArrayList<Object> feeds;
    private OptionAdapter optionAdapter;

    private ThreadMessage mThreadMessage;
    private Object itemAddOption;
    private String myNumber;
    private int mThreadId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = this;
        itemAddOption = OptionAdapter.TYPE_ADD_OPTION;

        mThreadId = getArguments() != null ? getArguments().getInt(ReengMessageConstant.MESSAGE_THREAD_ID) : 0;
        mThreadMessage = application.getMessageBusiness().getThreadById(mThreadId);
        myNumber = application.getReengAccountBusiness().getJidNumber();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_poll_create_v2, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        changeWhiteStatusBar();
        return view;
    }

    public void changeWhiteStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
           getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.white));
        } else {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
               getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
               getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.white));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        self = null;
        hideKeyboard();
        optionAdapter.setPollCreateFragmentV2(null);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            activity.onBackPressed();
        } else if (v == btnCreateSurvey) {
            if (etName.getText() == null || etName.getText().toString().trim().length() <= 0) {
                activity.showToast(R.string.poll_create_notify_please_enter_name);
                return;
            }
            if (feeds.contains(itemAddOption)) {
                if (feeds.size() <= 2) {
                    activity.showToast(R.string.poll_create_notify_please_add_the_option);
                    return;
                }
            } else {
                if (feeds.size() <= 1) {
                    activity.showToast(R.string.poll_create_notify_please_add_the_option);
                    return;
                }
            }
            ArrayList<String> options = new ArrayList<>();
            for (int i = 0; i < feeds.size(); i++) {
                Object feed = feeds.get(i);
                if (feed instanceof String) {
                    options.add((String) feed);
                }
            }
            ((PollMessageActivity) activity).openSetting(mThreadId, etName.getText().toString(), options);
        }
    }

    @OnClick(R.id.iv_close)
    public void onIvCloseClicked() {
//        llAddVote.setVisibility(View.GONE);
        feeds.add(itemAddOption);
        optionAdapter.bindData(feeds);
        hideKeyboard();
    }

    @OnClick(R.id.tv_add)
    public void onTvAddClicked() {
        if (etAdd.getText() != null && etAdd.getText().toString().trim().length() > 0) {
            String voteName = etAdd.getText().toString();
            for (int i = 0; i < feeds.size(); i++) {
                Object item = feeds.get(i);
                if (item instanceof String) {
                    String vote = (String) item;
                    if (vote.trim().toLowerCase().equals(voteName.trim().toLowerCase())) {
                        activity.showToast(R.string.poll_detail_notify_exist_vote);
                        return;
                    }
                }
            }
            if (!feeds.contains(voteName)) {
                etAdd.setText("");

                feeds.add(voteName);

//                feeds.remove(itemAddOption);
//                feeds.add(itemAddOption);
                optionAdapter.bindData(feeds);
            } else {
                activity.showToast(R.string.poll_detail_notify_exist_vote);
            }
        } else {
            activity.showToast(R.string.poll_detail_notify_enter_vote);
        }
    }

    private void initView() {
        initToolBar();
        feeds = new ArrayList<>();
        feeds.add(itemAddOption);

        optionAdapter = new OptionAdapter(activity);
        optionAdapter.setPollCreateFragmentV2(this);
        optionAdapter.bindData(feeds);

        recOptions.setAdapter(optionAdapter);
        recOptions.setNestedScrollingEnabled(true);
        recOptions.setLayoutManager(new LinearLayoutManager(activity));

//        ((ApplicationController) activity.getApplication()).getAvatarBusiness().setMyAvatar(ivAvatar, ((ApplicationController) activity.getApplication()).getReengAccountBusiness().getCurrentAccount());

        etName.post(new Runnable() {
            @Override
            public void run() {
                if (etName == null) return;

                etName.requestFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.showSoftInput(etName, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });
    }


    @SuppressLint("InflateParams")
    private void initToolBar() {
        activity.setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_vote_detail, null));
        TextView tvTitle = activity.getToolBarView().findViewById(R.id.ab_title);
        TextView tvStatus = activity.getToolBarView().findViewById(R.id.ab_status_text);
        btnBack = activity.getToolBarView().findViewById(R.id.ab_back_btn);
//        btnSend = activity.getToolBarView().findViewById(R.id.ab_agree_text);
        View divider = activity.getToolBarView().findViewById(R.id.divider);
        divider.setVisibility(View.GONE);
        tvTitle.setText(R.string.poll_create_title);
//        btnSend.setText(R.string.next);
//        btnSend.setTextColor(ContextCompat.getColor(application, R.color.videoColorSelect));
        tvStatus.setVisibility(View.GONE);

        btnBack.setOnClickListener(this);
        btnCreateSurvey.setOnClickListener(this);
    }

    private void onItemPollDeleteClick(String option) {
        feeds.remove(option);
        optionAdapter.bindData(feeds);
    }

    private void onItemPollAddClick() {
//        llAddVote.setVisibility(View.VISIBLE);
        feeds.remove(itemAddOption);
        optionAdapter.bindData(feeds);
        etAdd.requestFocus();

        showKeyboard();
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(etAdd, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(etAdd.getWindowToken(), 0);
        }
    }

    static class OptionAdapter extends BaseAdapterV2<Object, LinearLayoutManager, RecyclerView.ViewHolder> {

        static final int TYPE_OPTION = 0;
        static final int TYPE_ADD_OPTION = 1;

        @Nullable
        private PollCreateFragmentV2 pollCreateFragmentV2;

        OptionAdapter(Activity act) {
            super(act);
        }

        void setPollCreateFragmentV2(@Nullable PollCreateFragmentV2 pollCreateFragmentV2) {
            this.pollCreateFragmentV2 = pollCreateFragmentV2;
        }

        @Override
        public int getItemViewType(int position) {
            Object item = items.get(position);
            if (Utilities.equals(item, TYPE_ADD_OPTION)) {
                return TYPE_ADD_OPTION;
            } else {
                return TYPE_OPTION;
            }
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case TYPE_ADD_OPTION:
                    return new AddOptionHolder(layoutInflater, parent);
                default:
                    return new OptionHolder(layoutInflater, parent);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof ViewHolder) {
                ((ViewHolder) holder).bindData(items, position);
            }
        }

        @Override
        public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
            super.onViewAttachedToWindow(holder);
            if (holder instanceof OptionHolder) {
                ((OptionHolder) holder).setOptionAdapter(this);
            } else if (holder instanceof AddOptionHolder) {
                ((AddOptionHolder) holder).setOptionAdapter(this);
            }
        }

        @Override
        public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            if (holder instanceof OptionHolder) {
                ((OptionHolder) holder).setOptionAdapter(null);
            } else if (holder instanceof AddOptionHolder) {
                ((AddOptionHolder) holder).setOptionAdapter(null);
            }
        }

        void onItemPollDeleteClick(String option) {
            if (pollCreateFragmentV2 != null) {
                pollCreateFragmentV2.onItemPollDeleteClick(option);
            }
        }

        void onItemPollAddClick() {
            if (pollCreateFragmentV2 != null) {
                pollCreateFragmentV2.onItemPollAddClick();
            }
        }

        static class OptionHolder extends ViewHolder {

            @BindView(R.id.tv_option_name)
            TextView tvOptionName;

            private String option;

            @Nullable
            private OptionAdapter optionAdapter;

            OptionHolder(LayoutInflater layoutInflater, ViewGroup parent) {
                super(layoutInflater.inflate(R.layout.item_poll_create_option, parent, false));
            }

            void setOptionAdapter(@Nullable OptionAdapter optionAdapter) {
                this.optionAdapter = optionAdapter;
            }

            @Override
            public void bindData(ArrayList<Object> items, int position) {
                super.bindData(items, position);
                Object item = items.get(position);
                if (item instanceof String) {
                    option = (String) item;
                    tvOptionName.setText(option);
                }
            }

            @OnClick(R.id.iv_close)
            public void onViewClicked() {
                if (optionAdapter != null) {
                    optionAdapter.onItemPollDeleteClick(option);
                }
            }

        }

        static class AddOptionHolder extends ViewHolder {

            @Nullable
            private OptionAdapter optionAdapter;

            AddOptionHolder(LayoutInflater layoutInflater, ViewGroup parent) {
                super(layoutInflater.inflate(R.layout.item_poll_create_add_option, parent, false));
            }

            void setOptionAdapter(@Nullable OptionAdapter optionAdapter) {
                this.optionAdapter = optionAdapter;
            }

            @OnClick(R.id.root_item_poll_create)
            public void onViewClicked() {
                if (optionAdapter != null) {
                    optionAdapter.onItemPollAddClick();
                }
            }
        }

    }
}

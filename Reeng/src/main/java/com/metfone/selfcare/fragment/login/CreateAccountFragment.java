package com.metfone.selfcare.fragment.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.CreateYourAccountActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.activity.OTPAddPhoneLoginActivity;
import com.metfone.selfcare.activity.SetYourNewPasswordActivity;
import com.metfone.selfcare.activity.TermsConditionActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.SignUpRequest;
import com.metfone.selfcare.model.account.SignUpRequestData;
import com.metfone.selfcare.model.account.SignUpResponse;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.util.OpenIDErrorCode.ERROR_CODE_00;
import static com.metfone.selfcare.util.OpenIDErrorCode.ERROR_CODE_01;
import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;


public class CreateAccountFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, IOnBackPressed {
    private static final String TAG = CreateAccountFragment.class.getSimpleName();
    private AppCompatImageView ivBack;
    private CamIdTextView tvTermsAndConditions;
    private RoundLinearLayout rledtPassword, rledtConfirmPassword;
    private RoundTextView btnConfirm;
    private FrameLayout flbytapping;
    private ScrollView scr_account;
    private  ProgressBar pbLoading;
    boolean isPasswordVisible = false;
    boolean isConfirmPasswordVisible = false;
    private  ImageView ivPassVisible, ivConfirmPassVisible;
    private  OnFragmentInteractionListener fragmentInteractionListener;
    private LoginActivity mLoginActivity;
    private ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private  FrameLayout flSecondPinView, flPinViewPassword;
    private ApplicationController mApplication;
    private Resources mRes;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private BaseMPSuccessDialog baseMPSuccessDialog;
    private BaseMPSuccessDialog errorOtpDialog;
    private boolean isLoginDone = false;
    private PinView pinPassword, pinConfirmPassword;
    private PinView hintPinView, hintConfirmPinView;
    private TermsConditionActivity mTermsConditionActivity;
    private static final String CODE_PARAM = "code";
    public static CreateAccountFragment newInstance() {
        Bundle args = new Bundle();
        CreateAccountFragment fragment = new CreateAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static CreateAccountFragment newInstance(String phoneNumber, String otp, String code) {
        Bundle args = new Bundle();
        CreateAccountFragment fragment = new CreateAccountFragment();
        args.putString(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        args.putString(EnumUtils.OTP_KEY, otp);
        args.putString(CODE_PARAM, code);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mLoginActivity = (LoginActivity) activity;
        mLoginActivity.setTitle(getResources().getString(R.string.register).toUpperCase());
        mLoginActivity = (LoginActivity) activity;
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mRes = mLoginActivity.getResources();
        try {
            fragmentInteractionListener = (CreateAccountFragment.OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_account, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initViews(view);
        setupUI(scr_account);
        initViewListeners();
    }

    private void initData() {
        mLoginActivity = (LoginActivity) getActivity();
        new Handler().postDelayed(this::detectKeyboard, 500);
        mLoginActivity.setiOnBackPressed(this);
    }

    private void initViews(@NonNull View view) {
        ivBack = view.findViewById(R.id.ivBack);
        flbytapping = view.findViewById(R.id.fl_bytapping);
        tvTermsAndConditions = view.findViewById(R.id.tvTermsAndConditions);
        pbLoading = view.findViewById(R.id.pbLoading);
        rledtPassword = view.findViewById(R.id.rledtPassword);
        rledtConfirmPassword = view.findViewById(R.id.rledtConfirmPassword);
        btnConfirm = view.findViewById(R.id.btnConfirm);
        scr_account = view.findViewById(R.id.scr_account);
        ivPassVisible = view.findViewById(R.id.imgPassVisible);
        ivConfirmPassVisible = view.findViewById(R.id.imgConfirmPassVisible);
        pinPassword = view.findViewById(R.id.pinPassword);
        hintPinView = view.findViewById(R.id.hintPinView);
        pinConfirmPassword = view.findViewById(R.id.pinConfirmPassword);
        hintConfirmPinView = view.findViewById(R.id.hintConfirmPinView);
        flSecondPinView = view.findViewById(R.id.flSecondPinView);
        flPinViewPassword = view.findViewById(R.id.flPinViewPassword);
        pinPassword.setPasswordHidden(true);
        pinConfirmPassword.setPasswordHidden(true);
    }

    private void initViewListeners() {
        flSecondPinView.setOnClickListener(this);
        flPinViewPassword.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        flbytapping.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        tvTermsAndConditions.setOnClickListener(this);
        ivPassVisible.setOnClickListener(view1 -> {
            isPasswordVisible = !isPasswordVisible;
            pinPassword.setPasswordHidden(!isPasswordVisible);
            hintPinView.setVisibility(isPasswordVisible ? View.GONE : View.VISIBLE);
            if (isPasswordVisible) {
                ivPassVisible.setImageResource(R.drawable.ic_visibility_24px_outlined);
            } else {
                ivPassVisible.setImageResource(R.drawable.ic_visibility_off_24px_outlined);
            }
        });
        ivConfirmPassVisible.setOnClickListener(view2 -> {
            isConfirmPasswordVisible = !isConfirmPasswordVisible;
            pinConfirmPassword.setPasswordHidden(!isConfirmPasswordVisible);
            hintConfirmPinView.setVisibility(isConfirmPasswordVisible ? View.GONE : View.VISIBLE);
            if (isConfirmPasswordVisible) {
                ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_24px_outlined);
            } else {
                ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_off_24px_outlined);
            }
        });
        pinPassword.setOnFocusChangeListener(this);
        pinPassword.setOnClickListener(this);
        pinConfirmPassword.setOnClickListener(this);
        pinConfirmPassword.setOnFocusChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pinPassword:
            case R.id.flSecondPinView:
                flSecondPinView.requestFocus();
                flPinViewPassword.clearFocus();
                break;
            case R.id.pinConfirmPassword:
            case R.id.rledtConfirmPassword:
                flSecondPinView.clearFocus();
                flPinViewPassword.requestFocus();
                break;
            case R.id.ivBack:
//                mLoginActivity.onBackPressed();
                mLoginActivity.displayEnterPhoneNumberFragment("", "");
                break;
            case R.id.tvTermsAndConditions:
                NavigateActivityHelper.navigateToTermsAndConditions(getActivity());
                break;
            case R.id.btnConfirm:
                //Validate
                if (TextUtils.isEmpty(pinPassword.getText().toString()) || TextUtils.isEmpty(pinConfirmPassword.getText().toString())) {
                    showDialog(getString(R.string.please_fill_all_the_fields));
                } else if (!pinPassword.getText().toString().equals(pinConfirmPassword.getText().toString())) {
                    showDialog(getString(R.string.confirm_password));
                } else if (pinPassword.getText().toString().length() != 6) {
                    showDialog(getString(R.string.password_consists_of_6_characters));
                } else {
                    //create account
                    if (getArguments() != null) {
                        btnConfirm.setEnabled(false);
                        String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
                        mCurrentRegionCode = "KH";
                        if (phoneNumber != null) {
                            mCurrentNumberJid = "+855" + phoneNumber.substring(1);
                        }
                        String otp = getArguments().getString(EnumUtils.OTP_KEY);
                        String pass = pinPassword.getText().toString();
                        if (!NetworkHelper.isConnectInternet(mLoginActivity)) {
                            mLoginActivity.showError(getString(R.string.error_internet_disconnect), null);
                        } else {
                            signUp(phoneNumber, pass, otp);
                        }
                    }
                }
                break;
        }
    }

    private void generateOldMochaOTP(String token) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(mLoginActivity).detectSubscription(json);
                        doLoginAction(response.body().getOtp());

                    } else {
                        ToastUtils.showToast(getContext(), response.body().getDesc());
                        btnConfirm.setEnabled(true);
                    }
                } else {
                    ToastUtils.showToast(getContext(), "generateOldMochaOTP Failed");
                    btnConfirm.setEnabled(true);
                }

            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), error.getMessage());
                btnConfirm.setEnabled(true);
            }
        });

    }

    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof TextView || innerView instanceof ImageView) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }


    private void signUp(String phoneNumber, String password, String otp) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        SignUpRequestData signUpRequestData = new SignUpRequestData(phoneNumber, password, otp);
        SignUpRequest signUpRequest = new SignUpRequest(signUpRequestData, "", "", "", "");
        apiService.signUp(signUpRequest).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                SignUpResponse signUpResponse = response.body();
                if (signUpResponse != null && signUpResponse.getCode() != null ) {
                    if (signUpResponse.getCode().equals(ERROR_CODE_00)) {
                        String token = "Bearer " + signUpResponse.getData().getAccess_token();
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefresh_token());
                        SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
                        generateOldMochaOTP(signUpResponse.getData().getAccess_token());
                        //   NavigateActivityHelper.navigateToHomeScreenActivity(CreateYourAccountActivity.this, true, true);
                    } else {
                        btnConfirm.setEnabled(true);
//                        if (TextUtils.isEmpty(signUpResponse.getMessage())) {
//                            showDialog(getString(R.string.txt_sign_up_failed));
//                        } else {
//
//                            showDialog(signUpResponse.getMessage());
//                        }
                        if (signUpResponse.getCode().equals(ERROR_CODE_01)) {
                            showErrorOtpDialog(response);
                        } else {
                            showErrorDialog(response);
                        }
                    }
                } else {
                    btnConfirm.setEnabled(true);
//                    showDialog(getString(R.string.txt_sign_up_failed));
                    ToastUtils.showToast(getActivity(), getString(R.string.txt_sign_up_failed));
                }
            }

            private void showErrorOtpDialog(Response<SignUpResponse> response) {
                errorOtpDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
                    errorOtpDialog.setBgWhite(true);
                }
                errorOtpDialog.show();
                errorOtpDialog.setOnDismissListener(listener -> {
                    mLoginActivity.displayLoginOTPScreen(phoneNumber, mCurrentRegionCode, Integer.parseInt(response.body().getCode()));
                });
            }

            private void showErrorDialog(Response<SignUpResponse> response) {
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
                    baseMPSuccessDialog.setBgWhite(true);
                }
                baseMPSuccessDialog.show();
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                btnConfirm.setEnabled(true);
                Toast.makeText(getContext(), getString(R.string.txt_sign_up_failed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void navigateNextScreen(String phoneNumber, String password, String otp, String code) {
        Intent intent = new Intent(getActivity(), CreateYourAccountActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        intent.putExtra("password", password);
        intent.putExtra(EnumUtils.OTP_KEY, otp);
        intent.putExtra("code", code);
        //0 is PHONE METHOD
        intent.putExtra("method", 0);
        startActivity(intent);
        getActivity().finish();
    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new CreateAccountFragment.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    @Override
    public void onResume() {
        pinPassword.requestFocus();
        UIUtil.showKeyboard(getContext(), pinPassword);
        UIUtil.showKeyboardPassword(getContext(), flPinViewPassword);
        UIUtil.showKeyboardPassword(getContext(), flSecondPinView);
        super.onResume();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (v.getId() == R.id.pinPassword) {
                rledtPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.resend), 1);
            } else if (v.getId() == R.id.pinConfirmPassword) {
                rledtConfirmPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.resend), 1);
            }
        } else {
            if (v.getId() == R.id.pinPassword) {
                rledtPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.color_edt_full_name_stroke), 1);
            } else if (v.getId() == R.id.pinConfirmPassword) {
                rledtConfirmPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.color_edt_full_name_stroke), 1);
            }
        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.txt_title_error);
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onBackPressed() {
        mLoginActivity.displayEnterPhoneNumberFragment("", "");
        return false;
    }

    public void detectKeyboard() {
        scr_account.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                scr_account.getWindowVisibleDisplayFrame(r);
                int heightDiff = scr_account.getRootView().getHeight() - (r.bottom - r.top);
                int screenHeight = scr_account.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened

                } else {
                    // keyboard is closed
                    pinPassword.clearFocus();
                    pinConfirmPassword.clearFocus();
                }
            }
        });
    }

    public interface OnFragmentInteractionListener {
        void displayLoginOTPScreen(String numberJid, String regionCode, int code, String password);

        void displayLoginPasswordScreen(String phoneNumber);

        void displayEnterPhoneNumberFragment(String phoneNumber);

        void displayCreateAccountScreen(String phoneNumber);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) mLoginActivity.getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);

            try {
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
                    String otp = getArguments().getString(EnumUtils.OTP_KEY);
                    String pass = pinPassword.getText().toString();
                    navigateNextScreen(phoneNumber, pass, otp, String.valueOf(getArguments().getString(CODE_PARAM)));
                    mApplication.logEventFacebookSDKAndFirebase(mLoginActivity.getString(R.string.c_login_complete));
                } else {
                    btnConfirm.setEnabled(true);
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    ToastUtils.showToast(mLoginActivity, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(mLoginActivity.getString(R.string.c_login_fail));
                }
            } catch (Exception e) {
                btnConfirm.setEnabled(true);
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }
}
package com.metfone.selfcare.fragment.musickeeng;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.activity.SearchSongActivity;
import com.metfone.selfcare.adapter.LocalSongListAdapter;
import com.metfone.selfcare.adapter.UploadSongAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.KeengProfileBusiness;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpMethod;
import com.metfone.selfcare.common.api.http.HttpProgressCallBack;
import com.metfone.selfcare.common.utils.FileUtils;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.SearchHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.task.ScanSongAsyncTask;
import com.metfone.selfcare.ui.UploadSongPopupFragment;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.dialog.UploadSongDialogV5;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.utils.LinearItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.metfone.selfcare.common.api.video.HttpUtils.createBuilder;
import static com.metfone.selfcare.util.Utilities.checkAndGetNewPath;

/**
 * Created by tungt on 3/17/2016.
 */
public class UploadSongFragment extends Fragment implements
        ScanSongAsyncTask.ScanSongInterface,
        UploadSongPopupFragment.OnClick,
        LocalSongListAdapter.OnItemClicked,
        XMPPConnectivityChangeListener, BaseDialogFragment.DialogListener {
    private static final String TAG = UploadSongFragment.class.getSimpleName();

    private SearchSongActivity mActivity;
    private ApplicationController mApplication;

    private Resources mResources;
    private View mRootView, mAppBar;

    private KeengProfileBusiness mKeengProfileBusiness;
    private UserInfo mUserInfo;
    private OnFragmentInteractionListener mListener;

    private RecyclerView recyclerView;
    private TextView mTvwNotFound;
    private View mLoadingView;
    private AppCompatEditText mSearchView;
    private AppCompatImageView icBack;
    private Handler mHandler;

    //    private LocalSongListAdapter mAdapter;
    private ScanSongAsyncTask mScanTask;
    private UploadRequest uploadSongRequest;

    private ProgressDialog mLoadingDialog;
    //    private UploadSongPopupFragment mPopupUpload;
    private SearchSong mSearchSong;

    private boolean mPending = false;
    private LocalSongInfo mPendingSongInfo;
    private boolean scanDone = false, getKeengProfile = false, isRequestingProfile = false;

    private ArrayList<LocalSongInfo> mSongList;
    private ArrayList<LocalSongInfo> mSongUploaded;
    private String keengUserId = "";
    private SharedPreferences mPref;

    private UploadSongAdapter mAdapter;

    private String currentQuery;
    private View viewEmpty;
    private AppCompatImageView icDeleteSearch;

    private UploadSongDialogV5 uploadSongDialogV5;
    private MediaModel mediaModelUploadSuccess;
    private Http oldHttpUploading;

    public static UploadSongFragment newInstance() {
        UploadSongFragment fragment = new UploadSongFragment();
        return fragment;
    }

    public UploadSongFragment() {
    }

    @Override
    public void onAttach(Context activity) {
        Log.d(TAG, "onAttach");
        mActivity = (SearchSongActivity) activity;
        mApplication = (ApplicationController) mActivity.getApplication();
        mResources = mActivity.getResources();
        mKeengProfileBusiness = mApplication.getKeengProfileBusiness();
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        keengUserId = mPref.getString(Constants.PREFERENCE.PREF_KEENG_USER_ID, "");
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        mRootView = inflater.inflate(R.layout.fragment_upload_song, container, false);
        findComponentView();

        mUserInfo = mKeengProfileBusiness.getKeengUserInfo();
        mScanTask = new ScanSongAsyncTask(this, mApplication);
        mAdapter = new UploadSongAdapter(getActivity());
        mAdapter.getItemClickObservable().subscribe(new Consumer<LocalSongInfo>() {
            @Override
            public void accept(LocalSongInfo localSongInfo) throws Exception {
                onClicked(localSongInfo);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new LinearItemDecoration(ViewUtils.dpToPx(15)));
        recyclerView.setAdapter(mAdapter);
        return mRootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        XMPPManager.removeXMPPConnectivityChangeListener(this);
//        InputMethodUtils.hideSoftKeyboard(mSearchView, mActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (mHandler == null) {
            mHandler = new Handler();
        }
        XMPPManager.addXMPPConnectivityChangeListener(this);
        if (!scanDone) {
            scanMp3File();
        }
        if (!getKeengProfile) {
            getKeengProfile(false);
        }

        if (mSongList == null) {
            mSongList = new ArrayList<>();
        }
    }

    private void findComponentView() {
        mAppBar = mRootView.findViewById(R.id.appBarLayout);
        if (getArguments() != null && getArguments().getBoolean("hide_appbar", true)) {
            mAppBar.setVisibility(View.GONE);
        } else {
            mSearchView = mAppBar.findViewById(R.id.edtSearch);
            icBack = mAppBar.findViewById(R.id.icBackToolbar);
            icDeleteSearch = mAppBar.findViewById(R.id.icDeleteSearch);
            setOnClickAppbarListener();
        }
        recyclerView = mRootView.findViewById(R.id.recyclerView);
        mTvwNotFound = (TextView) mRootView.findViewById(R.id.search_song_note_not_found);
        mLoadingView = mRootView.findViewById(R.id.progressLoading);
        viewEmpty = mRootView.findViewById(R.id.viewEmpty);
        mLoadingDialog = new ProgressDialog(mActivity);
        mLoadingDialog.setCancelable(true);
        mLoadingDialog.setTitle("SCANNING SONG");
        mLoadingDialog.hide();

//        mSearchView.setHint(mResources.getString(R.string.search_song_hint));
//        mSearchView.setText("");
    }

    private void setOnClickAppbarListener() {
        icBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodUtils.hideSoftKeyboard(mSearchView, mActivity);
                mActivity.onBackPressed();
            }
        });
        setSearchViewListener();
        icDeleteSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchView.setText("");
            }
        });
    }

    private void setSearchViewListener() {
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String query = SearchHelper.convertQuery(s.toString());
                if (query == null || query.isEmpty()) {
                    icDeleteSearch.setVisibility(View.GONE);
                } else {
                    icDeleteSearch.setVisibility(View.VISIBLE);
                }
                actionSearchSong(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //String content = s.toString();
            }
        });

        mSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
                if (keyCode == EditorInfo.IME_ACTION_DONE) {
                    InputMethodUtils.hideSoftKeyboard(mSearchView, mActivity);
                }
                return false;
            }
        });
        mSearchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // asus zenphone
                InputMethodUtils.showSoftKeyboard(mActivity, mSearchView);
                return false;
            }
        });
    }

    public void actionSearchSong(String query) {
        currentQuery = query;
        mTvwNotFound.setVisibility(View.GONE);
        String txt = Utilities.convert(query).trim();
        if (!TextUtils.isEmpty(txt)) {
            scanMp3File(txt);
        } else {
            notifyChangeAdapter(mSongList);
        }
    }

    private void scanMp3File() {
        if (mScanTask != null) {
            mScanTask.cancel(true);
            mScanTask = null;
        }
        mScanTask = new ScanSongAsyncTask(this, mApplication);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mScanTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            mScanTask.execute();
        }
    }

    private void uploadFile(final LocalSongInfo songInfo, boolean isWait) {
        if (mUserInfo == null) {
            if (isWait) {// click upload mà chua lay dc keeng profile thi get profile
                mPending = true;
                mPendingSongInfo = songInfo;
                getKeengProfile(true);
            } else {// get profile keeng success mà loi thi show err luon
                mActivity.showError(R.string.e601_error_but_undefined, mResources.getString(R.string.error));
            }
        } else if (!TextUtils.isEmpty(mUserInfo.getId()) && !TextUtils.isEmpty(mUserInfo.getSessionToken())) {
            //start asynctask
//            uploadSongRequest = mApplication.getTransferFileBusiness().uploadSong(mUserInfo, songInfo, this);
            checkAndUploadSong(mUserInfo, songInfo);
        } else {//keeng session token khong hop le
            mActivity.showError(R.string.error_session_token, mResources.getString(R.string.error));
        }
    }

    @SuppressLint("CheckResult")
    private void checkAndUploadSong(UserInfo keengInfo, final LocalSongInfo songInfo) {
        if (oldHttpUploading != null) {
            oldHttpUploading.cancel();
        }
//        if (Utilities.checkFilePathUpload(songInfo.getPath())) {
//            mPopupUpload = PopupHelper.getInstance().showUploadPopup(
//                    mActivity.getSupportFragmentManager(),
//                    String.format(mResources.getString(R.string.popup_upload_text_upload), songInfo.getName()),
//                    UploadSongFragment.this,
//                    songInfo);
//            uploadSong(keengInfo, songInfo, songInfo.getPath());
//        } else {
//            mActivity.showToast("please rename file path with valid format character");
//        }
//        mPopupUpload = PopupHelper.getInstance().showUploadPopup(
//                mActivity.getSupportFragmentManager(),
//                String.format(mResources.getString(R.string.popup_upload_text_upload), songInfo.getName()),
//                UploadSongFragment.this,
//                songInfo);
        if (uploadSongDialogV5 != null) {
            uploadSongDialogV5.dismiss();
            uploadSongDialogV5 = null;
        }

        uploadSongDialogV5 = UploadSongDialogV5.newInstance();
        uploadSongDialogV5.setSelectListener(this);
        uploadSongDialogV5.show(getChildFragmentManager(), UploadSongDialogV5.class.getName());
        Observable.create((ObservableOnSubscribe<String>) emitter -> {
            String pathToUpload = checkAndGetNewPath(songInfo.getPath(), getContext(), true);
            if (pathToUpload != null && pathToUpload.equals("false")) {
                emitter.onError(new Throwable());
            } else {
                emitter.onNext(pathToUpload);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (s != null && !s.equals("true")) {
                        uploadSong(keengInfo, songInfo, s);
                    } else {
                        uploadSong(keengInfo, songInfo, songInfo.getPath());
                    }
                }, throwable -> UploadSongFragment.this.onUploadFailed(-1, UploadSongFragment.this.getContext().getString(R.string.e500_internal_server_error)));
    }

    private void uploadSong(UserInfo keengInfo, final LocalSongInfo songInfo, String pathToUpload) {
        String urlUpload = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfMedia2Keeng(Config.UrlKeengEnum
                .MEDIA_UPLOAD_SONG);
        int indexLast = urlUpload.lastIndexOf("/");
        String baseUrl = urlUpload.substring(0, indexLast + 1);
        String url = urlUpload.substring(indexLast + 1);
        oldHttpUploading = createBuilder(mApplication)
                .setBaseUrl(baseUrl)
                .setUrl(url)
                .setMethod(HttpMethod.FILE)
                .putFile(Constants.HTTP.FILE.REST_UP_MUSIC, pathToUpload)
                .putParameter(Constants.HTTP.KEENG_USER_ID, keengInfo.getId())
                .putParameter(Constants.HTTP.KEENG_SESSION_TOKEN_UPLOAD, keengInfo.getSessionToken())
                .putParameter(Constants.HTTP.KEENG_MEDIA_NAME, songInfo.getName())
                .putParameter(Constants.HTTP.KEENG_SINGER_NAME, songInfo.getSinger())
                .withCallBack(new HttpProgressCallBack() {
                    @Override
                    public void onSuccess(String response) {
                        Log.d(TAG, "onUploadComplete: " + response);
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.optInt("status");
                            String songId = jsonObject.optString("song_id");
                            String mediaUrl = jsonObject.optString("content");
                            if (status == 1 && !TextUtils.isEmpty(songId) && !TextUtils.isEmpty(mediaUrl)) {
                                String songThumb = jsonObject.optString("singer_image");
                                String url = null;
                                mActivity.trackingEvent(R.string.ga_category_upload_music, R.string
                                        .ga_action_interaction, R
                                        .string.ga_label_upload_success);
                                mPending = false;
                                // mPendingSongInfo = null;
                                MediaModel songModel = new MediaModel();
                                songModel.setId(songId);
                                songModel.setSongType(MediaModel.SONG_TYPE_UPLOAD);
                                songModel.setMedia_url(mediaUrl);
                                songModel.setUrl(url);
                                songModel.setImage(songThumb);
                                songModel.setName(songInfo.getName());
                                songModel.setSinger(songInfo.getSinger());
                                songInfo.setUploaded(true);
                                songInfo.setMediaUrl(mediaUrl);
                                if (uploadSongDialogV5 != null && uploadSongDialogV5.getDialog() != null &&
                                        uploadSongDialogV5.getDialog().isShowing()) {
                                    mediaModelUploadSuccess = songModel;
//                                    mPopupUpload.setResultSongModel(songModel);
                                    uploadSongDialogV5.uploadCompleted();
                                }
                                refreshListSong();
                            } else {
                                onUploadFailed(-1, "status != 1 || songId<0 || media null");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            onUploadFailed(-1, "JSONException");
                        } finally {
                            if (!songInfo.getPath().equals(pathToUpload)) {
                                FileUtils.delete(pathToUpload);
                                Log.d(TAG, "deleted file copy");
                            }
                        }
                    }

                    @Override
                    public void onProgressUpdate(int position, int sum, int percentage) {
                        Log.d(TAG, "percentage: " + percentage);
                        if (uploadSongDialogV5 != null && uploadSongDialogV5.getDialog() != null && uploadSongDialogV5.getDialog().isShowing())
                            uploadSongDialogV5.updateProgress(percentage);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        onUploadFailed(-1, message);
                        if (!songInfo.getPath().equals(pathToUpload)) {
                            FileUtils.delete(pathToUpload);
                            Log.d(TAG, "deleted file copy");
                        }
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                })
                .execute();

    }


    private void scanMp3File(String searchStr) {
        if (mSearchSong != null) {
            mSearchSong.cancel(true);
            mSearchSong = null;
        }
        mSearchSong = new SearchSong();
        mSearchSong.setListSong(mSongList);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mSearchSong.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, searchStr);
        } else {
            mSearchSong.execute(searchStr);
        }
    }

    @Override
    public void onBtnYesClicked(MediaModel songModel) {
        //mApplication.getMusicBusiness().insertNewMediaModel(songModel);
        if (mListener != null) {
            mListener.processResultSong(songModel);
        }
    }

    @Override
    public void onBtnNoClicked() {
        mActivity.trackingEvent(R.string.ga_category_upload_music, R.string.ga_action_interaction, R.string
                .ga_label_upload_cancel);
        Log.d(TAG, "onBtnNoClicked");
        mApplication.getTransferFileBusiness().cancelUpload(uploadSongRequest);
    }

    public void onUploadFailed(int errorCode, String errorMessage) {
        Log.e(TAG, "onUploadFailed: " + errorMessage);
        mActivity.trackingEvent(
                R.string.ga_category_upload_music,
                R.string.ga_action_interaction,
                R.string.ga_label_upload_fail);
        mPending = false;
        if (uploadSongDialogV5 == null || errorCode == com.metfone.selfcare.network.file.Constants.ERROR_CANCELLED)
            return;
        //mPendingSongInfo = null;
        if (uploadSongDialogV5 != null && uploadSongDialogV5.getDialog() != null
                && uploadSongDialogV5.getDialog().isShowing()){
            uploadSongDialogV5.setError(getString(R.string.connect_server_error));
        }
    }

    @Override
    public void onClicked(LocalSongInfo localSongInfo) {
        if (localSongInfo.isUploaded()) {
            mActivity.trackingEvent(R.string.ga_category_upload_music, R.string.ga_action_interaction, R.string
                    .ga_label_click_listen);
            MediaModel songModel = new MediaModel();
            songModel.setSongType(MediaModel.SONG_TYPE_UPLOAD);
            songModel.setId(String.valueOf(localSongInfo.getId()));
            songModel.setMedia_url(localSongInfo.getMediaUrl());
            songModel.setName(localSongInfo.getName());
            songModel.setSinger(localSongInfo.getSinger());
            songModel.setUrl(null);
            songModel.setImage("");
            //mApplication.getMusicBusiness().insertNewMediaModel(songModel);
            if (mListener != null) {
                mListener.processResultSong(songModel);
            }
        } else {
            mActivity.trackingEvent(R.string.ga_category_upload_music, R.string.ga_action_interaction, R.string
                    .ga_label_click_upload);
            if (NetworkHelper.isConnectInternet(mActivity.getApplicationContext())) {
                uploadFile(localSongInfo, true);
            } else {
                mActivity.showError(R.string.e500_internal_server_error, mResources.getString(R.string.error));
            }
        }
    }

    @Override
    public void onXMPPConnected() {
        if (!getKeengProfile) {
            if (mHandler != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        getKeengProfile(false);
                    }
                });
            }
        }
    }

    @Override
    public void onXMPPDisconnected() {
    }

    @Override
    public void onXMPPConnecting() {
    }

    @Override
    public void dialogRightClick(int value) {
        if (value == 0) {

        } else if (value == 1) {
            if (mListener != null && mediaModelUploadSuccess != null) {
                mListener.processResultSong(mediaModelUploadSuccess);
            }
        }
    }

    @Override
    public void dialogLeftClick() {

    }

    private class SearchSong extends AsyncTask<String, Void, ArrayList<LocalSongInfo>> {
        private ArrayList<LocalSongInfo> listSong;

        public void setListSong(ArrayList<LocalSongInfo> listSong) {
            this.listSong = listSong;
        }

        @Override
        protected ArrayList<LocalSongInfo> doInBackground(String... params) {
            String textSearch = params[0];
            return songSearch(textSearch, listSong);
        }

        @Override
        protected void onPostExecute(ArrayList<LocalSongInfo> result) {
            notifyChangeAdapter(result);
            super.onPostExecute(result);
        }
    }

    private ArrayList<LocalSongInfo> songSearch(String contentSearch, ArrayList<LocalSongInfo> listSong) {
        if (listSong == null) return null;
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());

        if (contentSearch == null || contentSearch.length() <= 0) {
            return listSong;
        }
        String[] listTexts = contentSearch.split("\\s+");
        ArrayList<LocalSongInfo> list;
        ArrayList<LocalSongInfo> listSearch = new ArrayList<>(listSong);
        for (String item : listTexts) {
            if (item.trim().length() > 0) {
                list = new ArrayList<>();
                for (LocalSongInfo song : listSearch) {
                    if (song.getName() != null && song.getAsciiName().contains(item)) {
                        list.add(song);
                    }
                }
                listSearch = list;
            }
        }
        return listSearch;
    }

    private void notifyChangeAdapter(ArrayList<LocalSongInfo> list) {
        mAdapter.setItems(list);
        if (list == null || list.isEmpty()) {
            if (currentQuery != null && !currentQuery.isEmpty()) {
                mTvwNotFound.setVisibility(View.VISIBLE);
                hideEmptyView();
            } else {
                mTvwNotFound.setVisibility(View.GONE);
                addViewEmpty();
            }
            return;
        }
        mTvwNotFound.setVisibility(View.GONE);
        hideEmptyView();
    }

    private KeengProfileBusiness.KeengProfileRequestCallback mCallBack = new KeengProfileBusiness
            .KeengProfileRequestCallback() {
        @Override
        public void onRequestProfileSuccess(UserInfo userInfo) {
            mActivity.hideLoadingDialog();
            Log.d(TAG, "User id" + userInfo.getId());
            Log.d(TAG, "SessionToken " + userInfo.getSessionToken());
            mUserInfo = userInfo;
            getKeengProfile = true;
            isRequestingProfile = false;
            if (mLoadingView != null && scanDone) mLoadingView.setVisibility(View.GONE);
            if (mPending) {
                mPending = false;
                uploadFile(mPendingSongInfo, false);
            }
        }

        @Override
        public void onRequestProfileFail(boolean isUpload) {
            Log.e(TAG, "onRequestProfileFail");
            mActivity.hideLoadingDialog();
            mActivity.trackingEvent(R.string.ga_category_upload_music, R.string.ga_action_interaction, R.string
                    .ga_label_get_user_keeng_fail);
            isRequestingProfile = false;
            getKeengProfile = true;
            if (mLoadingView != null && scanDone) mLoadingView.setVisibility(View.GONE);
            if (isUpload) {
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
//            mActivity.hideLoadingDialog();
//            mActivity.showError(R.string.error_internet_disconnect, mResources.getString(R.string.error));
        }
    };

    private void getKeengProfile(boolean isUpload) {
//        mActivity.showLoadingDialog(null, mResources.getString(R.string.waiting));
        long diffTime = System.currentTimeMillis() - mKeengProfileBusiness.getLastTimeGetInfo();
        diffTime /= 1000;
        if (diffTime >= Constants.TOKEN_EXPIRE_TIME
                || mUserInfo == null
                || TextUtils.isEmpty(mUserInfo.getSessionToken())) {
            isRequestingProfile = true;
            if (isUpload) {// get lại profile khi click upload bai hat thi chi show dialog loading thoi
                mActivity.showLoadingDialog(null, R.string.waiting);
            } else {// get lan dau thi show view loading
                if (mLoadingView != null) mLoadingView.setVisibility(View.VISIBLE);
            }
            mKeengProfileBusiness.doRequestGetMyKeengProfile(mCallBack, isUpload);
        } else {
            getKeengProfile = true;
        }
    }

    private void getListSongsUploaded() {
        String url = UrlConfigHelper.getInstance(mActivity).
                getUrlConfigOfMedia2Keeng(Config.UrlKeengEnum.SERVICE_GET_SONG_UPLOAD);

        ResfulString resfulString = new ResfulString(url);

        resfulString.addParam("msisdn", Utilities.getMyPhoneNumberKeeng(mApplication));
        resfulString.addParam("page", 1);
        resfulString.addParam("num", 20);
        resfulString.addParam("type", 1);
        Log.i(TAG, "url: " + resfulString.toString());
        StringRequest request = new StringRequest(Request.Method.GET, resfulString.toString(), new Response
                .Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse: " + s);
                mLoadingDialog.hide();
                mLoadingView.setVisibility(View.GONE);
                try {
                    JSONObject js = new JSONObject(s);
                    JSONArray dataObject = js.optJSONArray("data");
                    if (dataObject != null && dataObject.length() > 0) {
                        mSongUploaded = new ArrayList<>();
                        LocalSongInfo localSongInfo;
                        for (int i = 0; i < dataObject.length(); i++) {
                            JSONObject object = dataObject.optJSONObject(i);
                            if (object != null) {
                                localSongInfo = new LocalSongInfo();
                                localSongInfo.setId(object.optInt("id"));
                                localSongInfo.setSinger(object.optString("singer"));
                                localSongInfo.setName(object.optString("name"));
                                localSongInfo.setMediaUrl(object.optString("media_url"));
//                                localSongInfo.setUrlThumb(object.optString("song_thumb"));
                                localSongInfo.setUploaded(true);
                                localSongInfo.setFileSize(0);
                                localSongInfo.setAuthor("");
                                localSongInfo.setPath("");
                                Log.d(TAG, localSongInfo.toString());
                                mSongUploaded.add(localSongInfo);
                            }
                        }

                        Log.d(TAG, "song upload: " + mSongUploaded.size());

                        if (mSongUploaded.size() > 0) {
                            //merge list
                            if (mSongList.isEmpty()) {
                                mSongList.addAll(mSongUploaded);
                            } else {
                                for (LocalSongInfo song : mSongList) {
                                    if (!checkContainSong(song, mSongUploaded)) {
                                        mSongUploaded.add(song);
                                    }
                                }
                                mSongList.clear();
                                mSongList.addAll(mSongUploaded);
                                mSongUploaded.clear();
                                refreshListSong();
                            }
                        }
                        Log.i(TAG, "song list: " + mSongList.size());
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mLoadingView.setVisibility(View.GONE);
                mLoadingDialog.hide();
                refreshListSong();
            }
        });

        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, null, false);

    }

    private boolean checkContainSong(LocalSongInfo songInfo, ArrayList<LocalSongInfo> songList) {
        String name = songInfo.getName();
        for (LocalSongInfo song : songList) {
            if (name.equals(song.getName())) {
                return true;
            }
        }
        return false;
    }

    private void refreshListSong() {
        if (mSongList == null) {
            return;
        }
        mAdapter.setItems(mSongList);
    }

    @Override
    public void onScanStart() {
        if (mLoadingView != null) mLoadingView.setVisibility(View.VISIBLE);
        scanDone = false;
    }

    @Override
    public void onScanFinish(ArrayList<LocalSongInfo> array) {
        mLoadingDialog.hide();
        Log.d(TAG, "scan song file finish");
        if (array.size() <= 0) {
            if (currentQuery != null && !currentQuery.isEmpty()) {
                mTvwNotFound.setVisibility(View.VISIBLE);
                hideEmptyView();

            } else {
                addViewEmpty();
                mTvwNotFound.setVisibility(View.GONE);
            }
        } else {
            mTvwNotFound.setVisibility(View.GONE);
            hideEmptyView();
            mSongList = array;
            refreshListSong();
            getListSongsUploaded();
        }
        scanDone = true;
        if (mLoadingView != null && !isRequestingProfile) mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void onScanProgress(int i) {
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mApplication.getTransferFileBusiness().cancelUpload(uploadSongRequest);
        if (mScanTask != null) {
            mScanTask.cancel(true);
            mScanTask = null;
        }
    }


    private void addViewEmpty() {
//        if (viewEmpty == null) {
//            CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            viewEmpty.setLayoutParams(layoutParams);
//            // set image, text and description
//            //
//            AppCompatImageView imageView = viewEmpty.findViewById(R.id.imgEmpty);
//            imageView.setImageResource(R.drawable.ic_empty_music);
//            AppCompatTextView tvTitleEmpty = viewEmpty.findViewById(R.id.tvEmptyTitle);
//            tvTitleEmpty.setText(R.string.no_have_music_uploaded);
//            AppCompatTextView tvDescription = viewEmpty.findViewById(R.id.tvEmptyDescription);
//            tvDescription.setText(R.string.no_have_music_uploaded_des);
//            ((ViewGroup) (getView().findViewById(R.id.viewContainer))).addView(viewEmpty);
//        }
//        AnimationUtil.animationScale((ViewGroup) rootView, viewEmpty);
        viewEmpty.setVisibility(View.VISIBLE);
    }

    public void hideEmptyView() {
        if (viewEmpty != null) {
//            AnimationUtil.animationScale((ViewGroup) rootView, viewEmpty);
            viewEmpty.setVisibility(View.GONE);
        }
    }

    public interface OnFragmentInteractionListener {
        void processResultSong(MediaModel songModel);
    }
}
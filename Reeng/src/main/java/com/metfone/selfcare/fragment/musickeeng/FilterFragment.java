package com.metfone.selfcare.fragment.musickeeng;

import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerLocation;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.ui.dialog.DialogSelectStrangerLocation;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.v5.utils.ToastUtils;
import com.metfone.selfcare.v5.widget.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * Created by daipr on 4/17/2020
 */
public class FilterFragment extends DialogFragment implements RadioGroup.OnCheckedChangeListener {

    public static final String AROUND_KEY = "is_around_key";

    @BindView(R.id.tvCancel)
    AppCompatTextView tvCancel;
    @BindView(R.id.tvDeleteFilter)
    AppCompatTextView tvDeleteFilter;
    @BindView(R.id.viewAddress)
    View viewAddress;
    @BindView(R.id.tvAddressInfor)
    AppCompatTextView tvAddressInfor;
    @BindView(R.id.radioGroupGender)
    RadioGroup radioGroupGender;
    @BindView(R.id.radioGroupAgeOne)
    RadioGroup radioGroupAgeOne;
    @BindView(R.id.radioGroupAgeTwo)
    RadioGroup radioGroupAgeTwo;
    @BindView(R.id.radioAge45_60)
    RadioButton rbAge45_60;
    @BindView(R.id.radioAgeOver60)
    RadioButton rbAgeOver60;
    @BindView(R.id.tvAroundTitle)
    AppCompatTextView tvAroundSettingTitle;
    @BindView(R.id.tvHideAddressTitle)
    AppCompatTextView tvHideLocation;
    @BindView(R.id.toggleHideLocation)
    SwitchButton swHideLocation;
    private Unbinder unbinder;
    private StrangerFilterHelper filterHelper;
    private StrangerLocation currentLocation;
    private ApplicationController applicationController;
    private PositiveListener<Boolean> mCallBack;
    private boolean isAroundFilter;

    public static FilterFragment newInstance() {
        Bundle args = new Bundle();
        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FilterFragment newInstance(boolean isFilterAround) {
        Bundle args = new Bundle();
        args.putBoolean(AROUND_KEY, isFilterAround);
        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public FilterFragment setListener(PositiveListener<Boolean> listener) {
        this.mCallBack = listener;
        return this;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFullscreenV5);
        applicationController = ApplicationController.self();
        filterHelper = StrangerFilterHelper.getInstance(applicationController);
        if (getArguments() != null) {
            isAroundFilter = getArguments().getBoolean(AROUND_KEY, false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter_stranger_v5, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogV5Animation;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getDialog().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getDialog().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.white));
        }
        drawDetail();
        radioGroupAgeTwo.setOnCheckedChangeListener(this);
        radioGroupAgeOne.setOnCheckedChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Optional
    @OnClick({R.id.viewAddress, R.id.btnFilter, R.id.tvDeleteFilter, R.id.tvCancel})
    public void onClickView(View view) {
        if (view.getId() == R.id.viewAddress) {
            if (isAroundFilter) {
                ToastUtils.showToast(getContext(), getString(R.string.nearby_not_support), 300,
                        R.drawable.ic_not_support_v5, ToastUtils.ToastType.customImage);
            } else {
                if (applicationController.getReengAccountBusiness().isVietnam()) {
                    new DialogSelectStrangerLocation((BaseSlidingFragmentActivity) getActivity()).setNegativeListener(new NegativeListener<StrangerLocation>
                            () {
                        @Override
                        public void onNegative(StrangerLocation result) {
                            currentLocation = result;
                            tvAddressInfor.setText(currentLocation.getName());
                        }
                    }).show();
                }
            }
        } else if (view.getId() == R.id.btnFilter) {
            handleFilterConfirm();
        } else if (view.getId() == R.id.tvCancel) {
            dismiss();
        } else if (view.getId() == R.id.tvDeleteFilter) {
            if (filterHelper != null && filterHelper.getListLocations() != null) {
                    for (StrangerLocation strangerLocation : filterHelper.getListLocations()) {
                        strangerLocation.setSelected(false);
                    }

                filterHelper.getListLocations().get(0).setSelected(true);
                filterHelper.setFilterLocation(filterHelper.getListLocations().get(0));
                currentLocation = filterHelper.getListLocations().get(0);
            }
            int gender = -1;
            int minAge = StrangerFilterHelper.AGE_MIN;
            int maxAge = StrangerFilterHelper.AGE_MAX;
            filterHelper.updateFilter(currentLocation, gender, minAge, maxAge, false);
            if (mCallBack != null) {
                mCallBack.onPositive(swHideLocation.isChecked());
            }
            dismiss();
        }
    }

    private void drawDetail() {
        currentLocation = filterHelper.getFilterLocation();
        if (isAroundFilter) {
            radioGroupAgeTwo.setVisibility(View.GONE);
            radioGroupAgeOne.setVisibility(View.GONE);
            tvAddressInfor.setText(R.string.nearby);
            isAroundFilter = true;
            tvAroundSettingTitle.setVisibility(View.VISIBLE);
            swHideLocation.setVisibility(View.VISIBLE);
            tvHideLocation.setVisibility(View.VISIBLE);
            swHideLocation.setChecked(filterHelper.isHideLocation());
            getView().findViewById(R.id.tvAgeTitle).setVisibility(View.GONE);
        } else {
            tvAddressInfor.setText(currentLocation.getName());
        }

        radioGroupAgeTwo.clearCheck();
        radioGroupAgeOne.clearCheck();
        tvDeleteFilter.setPaintFlags(tvDeleteFilter.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvCancel.setPaintFlags(tvCancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvCancel.setText(R.string.btn_cancel);
        tvDeleteFilter.setText(R.string.delete_filter);
//        mRangeSeekBar.setRangeValues(StrangerFilterHelper.AGE_MIN, StrangerFilterHelper.AGE_MAX);
        int currentSex = filterHelper.getFilterSex();
        if (currentSex == Constants.CONTACT.GENDER_MALE) {
            ((RadioButton) getView().findViewById(R.id.radioMale)).setChecked(true);
        } else if (currentSex == Constants.CONTACT.GENDER_FEMALE) {
            ((RadioButton) getView().findViewById(R.id.radioFeMale)).setChecked(true);
        } else {
            ((RadioButton) getView().findViewById(R.id.radioGenderAll)).setChecked(true);
        }
        int ageMin = filterHelper.getRealFilterAgeMin();
        if (ageMin < StrangerFilterHelper.AGE_MIN) {
            ageMin = StrangerFilterHelper.AGE_MIN;
        }
//        mRangeSeekBar.setSelectedMinValue(ageMin);
        // draw
        int ageMax = filterHelper.getRealFilterAgeMax();
        if (ageMax > StrangerFilterHelper.AGE_MAX) {
            ageMax = StrangerFilterHelper.AGE_MAX;
        }
        if (ageMin == StrangerFilterHelper.AGE_MIN && ageMax == StrangerFilterHelper.AGE_MAX) {
            ((RadioButton) getView().findViewById(R.id.radioAgeAll)).setChecked(true);
        } else if (ageMin == 15 && ageMax == 30) {
            ((RadioButton) getView().findViewById(R.id.radioAge15_30)).setChecked(true);
        } else if (ageMin == 30 && ageMax == 45) {
            ((RadioButton) getView().findViewById(R.id.radioAge30_45)).setChecked(true);
        } else if (ageMin == 45 && ageMax == 60) {
            ((RadioButton) getView().findViewById(R.id.radioAge45_60)).setChecked(true);
        } else {
            ((RadioButton) getView().findViewById(R.id.radioAgeOver60)).setChecked(true);
        }
//        mRangeSeekBar.setSelectedMaxValue(ageMax);
    }

    private void handleFilterConfirm() {
        int sex = -1;
        int idGender = radioGroupGender.getCheckedRadioButtonId();
        if (idGender == R.id.radioMale) {
            sex = Constants.CONTACT.GENDER_MALE;
        } else if (idGender == R.id.radioFeMale) {
            sex = Constants.CONTACT.GENDER_FEMALE;
        }
        int minAge = StrangerFilterHelper.AGE_MIN;
        int maxAge = StrangerFilterHelper.AGE_MAX;
        int idAgeOne = radioGroupAgeOne.getCheckedRadioButtonId();
        int idAgeTwo = radioGroupAgeTwo.getCheckedRadioButtonId();
        if (idAgeOne == R.id.radioAgeAll) {
            minAge = StrangerFilterHelper.AGE_MIN;
            maxAge = StrangerFilterHelper.AGE_MAX;
        } else if (idAgeOne == R.id.radioAge15_30) {
            minAge = 15;
            maxAge = 30;
        } else if (idAgeOne == R.id.radioAge30_45) {
            minAge = 30;
            maxAge = 45;
        }
        if (idAgeTwo == R.id.radioAge45_60) {
            minAge = 45;
            maxAge = 60;
        } else if (idAgeTwo == R.id.radioAgeOver60) {
            minAge = 60;
            maxAge = StrangerFilterHelper.AGE_MAX;
        }
        filterHelper.updateFilter(currentLocation, sex, minAge, maxAge, swHideLocation.isChecked());
        if (mCallBack != null) {
            mCallBack.onPositive(swHideLocation.isChecked());
        }
        dismiss();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        unbinder = null;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (group.getId() == R.id.radioGroupAgeOne) {
            radioGroupAgeTwo.setOnCheckedChangeListener(null);
            radioGroupAgeTwo.clearCheck();
            radioGroupAgeTwo.setOnCheckedChangeListener(this::onCheckedChanged);
        } else {
            radioGroupAgeOne.setOnCheckedChangeListener(null);
            radioGroupAgeOne.clearCheck();
            radioGroupAgeOne.setOnCheckedChangeListener(this::onCheckedChanged);
        }
    }
}

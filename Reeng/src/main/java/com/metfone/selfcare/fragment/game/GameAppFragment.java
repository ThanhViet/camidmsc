package com.metfone.selfcare.fragment.game;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;

import com.metfone.selfcare.fragment.setting.MoreAppsFragment2;
import com.metfone.selfcare.util.Log;


/**
 * Created by sonnn00 on 8/30/2017.
 */

public class GameAppFragment extends Fragment {
    private static final String TAG = GameAppFragment.class.getSimpleName();
    private ListGamesActivity mParentActivity;
    private ApplicationController mApplication;
    private ViewPager mViewPager;
    GameAppPagerAdapter mPagerAdapter;
//    private TabLayout tabLayout;
    private Resources mRes;

    public static GameAppFragment newInstance() {
        GameAppFragment fragment = new GameAppFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mRes = mParentActivity.getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_viewpager, container, false);
        initActionBar(inflater);
        initComponents(root);
        setAdapter();
        return root;
    }

    private void initActionBar(LayoutInflater inflater) {
//        View mActionBarView = mParentActivity.getToolBarView();
//        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
//
//        TextView mTvwTitle = (EllipsisTextView) mActionBarView.findViewById(R.id.ab_title);
//        ImageView mImgBack = (ImageView) mActionBarView.findViewById(R.id.ab_back_btn);
//        mTvwTitle.setText(mParentActivity.getResources().getString(R.string.list_game));
//        mImgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mParentActivity.onBackPressed();
//            }
//        });
    }

    private void initComponents(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.mViewPager);
//        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
    }

    private void setAdapter() {
        mPagerAdapter = new GameAppPagerAdapter(mParentActivity.getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
//        tabLayout.setupWithViewPager(mViewPager);
    }

    private class GameAppPagerAdapter extends FragmentStatePagerAdapter {
        public GameAppPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return new ListGameFragment();
            else return MoreAppsFragment2.newInstance(MoreAppsFragment2.TYPE_MORE_APP);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0)
                return mRes.getString(R.string.game);
            else return mRes.getString(R.string.app);
        }
    }
}

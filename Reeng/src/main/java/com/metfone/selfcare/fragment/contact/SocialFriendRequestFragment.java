package com.metfone.selfcare.fragment.contact;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.fragment.home.tabmobile.AddFriendFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.mcinterface.DialogInterface;
import com.metfone.selfcare.v5.utils.DialogCommon;
import com.metfone.selfcare.v5.widget.CustomTypefaceSpan;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/8/2017.
 */

public class SocialFriendRequestFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = SocialFriendRequestFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private ContactListActivity mParentActivity;
    private RecyclerView mRecyclerView;
    private HomeContactsAdapter mAdapter;
    private ArrayList<Object> mListData = new ArrayList<>();
    private boolean isLoading = false;


    private int type;

    public static SocialFriendRequestFragment newInstance() {
        SocialFriendRequestFragment fragment = new SocialFriendRequestFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static Bundle arguments(int type) {
        return new Bundler()
                .putInt("type", type)
                .get();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ContactListActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mRes = mParentActivity.getResources();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        type = bundle != null ? bundle.getInt("type") : 0;
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        findComponentViews(rootView, container, inflater);
        showProgressLoading();
        Log.i(TAG, "type: " + type);
        setAdapter();
        loadData();
        return rootView;
    }

    private void loadData() {
        if (isLoading) return;
        isLoading = true;
        if (type == StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED)
            mApplication.getStrangerBusiness().getSocialFriendRequest(type, false, new StrangerBusiness.onSocialFriendsRequestListener() {
                @Override
                public void onResponse(int type, ArrayList<SocialFriendInfo> list) {
                    Log.i(TAG, "type: " + type + " list: " + list);
                    isLoading = false;
                    mListData = new ArrayList<>();
                    mListData.clear();
                    mListData.addAll(list);
                    drawListView();
                }

                @Override
                public void onLoadMoreResponse(int type, ArrayList<SocialFriendInfo> list, boolean isNoMore) {
                    isLoading = false;
                    mListData.addAll(list);
                    drawListView();
                }

                @Override
                public void onError(int type, int code, boolean isLoadMore) {
                    isLoading = false;
                    hideEmptyView();
                    if (isLoadMore) {
                        isLoading = false;
                        if (code == -2) {
                            mParentActivity.showToast(R.string.error_internet_disconnect);
                        } else {
                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }
            });
        else {
            HomeContactsHelper.getInstance(mApplication).getSuggestFriend(new HomeContactsHelper.GetSocialListener<ArrayList<UserInfo>>() {
                @Override
                public void onResponse(ArrayList<UserInfo> suggestFriends) {
                    isLoading = false;
                    mListData = new ArrayList<>();
                    mListData.clear();
                    mListData.addAll(suggestFriends);
                    drawListView();
                }

                @Override
                public void onError() {
                    isLoading = false;
                }

                @Override
                public void onEmptyList() {
                    isLoading = false;
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "OnResume");
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onRetryClick() {
        loadData();
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        View mViewFooter = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        LinearLayout mLoadMoreFooterView = mViewFooter.findViewById(R.id.layout_loadmore);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        mViewFooter.setVisibility(View.GONE);
        mLoadMoreFooterView.setVisibility(View.GONE);
    }

    private void setAdapter() {
        mAdapter = new HomeContactsAdapter(mApplication, mListData);
        mAdapter.setTypeSocial(type);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        setItemListViewListener();
    }


    private void setItemListViewListener() {
        mAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mParentActivity, entry);
            }

            @Override
            public void onAvatarClick(Object entry) {
                super.onAvatarClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mParentActivity, entry);
            }

            @Override
            public void onActionLabel(Object entry) {
                super.onActionLabel(entry);
                if (entry instanceof SocialFriendInfo) {
                    SocialFriendInfo social = (SocialFriendInfo) entry;
                    if (!NetworkHelper.isConnectInternet(mApplication)) {
                        mParentActivity.showToast(R.string.error_internet_disconnect);
                        return;
                    }
                    if (social.getSocialType() == StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED) {
                        processFollow(social, Constants.CONTACT.FOLLOW_STATE_BE_FOLLOWED);
                    }
                } /*else if (entry instanceof UserInfo)*/
            }

            @Override
            public void onSocialCancel(final Object entry) {
                super.onSocialCancel(entry);
                if (entry instanceof SocialFriendInfo) {
                    String name = ((SocialFriendInfo) entry).getUserName();
                    if (TextUtils.isEmpty(name)) return;
                    String content = String.format(mRes.getString(R.string.delete_request_friend_des), name);
                    int indexFirst = content.lastIndexOf(name);
                    Typeface fontMedium = ResourcesCompat.getFont(mApplication, R.font.sf_ui_text_medium);
                    SpannableStringBuilder ss = new SpannableStringBuilder(content);
                    ss.setSpan(new CustomTypefaceSpan("", fontMedium), indexFirst, indexFirst + name.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);


                    DialogCommon dialogCommon = new DialogCommon(mParentActivity, true);
                    dialogCommon.setTitle(mRes.getString(R.string.delete_request_friend_title));
                    dialogCommon.setMessageSpannable(ss);
                    dialogCommon.setLabelCancel(mRes.getString(R.string.cancel));
                    dialogCommon.setLabelAction1(mRes.getString(R.string.delete));
                    dialogCommon.setDialogListener(new DialogInterface() {
                        @Override
                        public void onClickAction1() {
                            socialFriendCancel((SocialFriendInfo) entry);
                        }

                        @Override
                        public void onClickAction2() {

                        }

                        @Override
                        public void onClickHyperLink(String link) {

                        }

                        @Override
                        public void onClickCancel() {

                        }
                    });
                    dialogCommon.show();
                }
            }
        });
    }

    private void drawListView() {
        mAdapter.setData(mListData);
        mAdapter.notifyDataSetChanged();
        hideEmptyView();
    }

    private void processFollow(final SocialFriendInfo socialFriendInfo, int state) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(StrangerBusiness.TAG_SOCIAL_REQUEST);
        ContactRequestHelper.getInstance(mApplication).requestSocialFriend(socialFriendInfo.getUserFriendJid(),
                socialFriendInfo.getUserName(), Constants.CONTACT.SOCIAL_SOURCE_MOCHA,
                state, socialFriendInfo.getStringRowId(), new ContactRequestHelper.onFollowResponse() {
                    @Override
                    public void onResponse(int status, String rowIdRequest) {
                        mParentActivity.hideLoadingDialog();
                        if (!TextUtils.isEmpty(socialFriendInfo.getUserName())) {
                            mParentActivity.showToastDone(mRes.getString(R.string.request_friend_success,
                                    socialFriendInfo.getUserName()));
                        } else {
                            mParentActivity.showToastDone(mRes.getString(R.string.accept_friend_success));
                        }
                        mApplication.getStrangerBusiness().removeSocialBeRequested(socialFriendInfo);
                        loadData();
                        EventBus.getDefault().post(new AddFriendFragment.AddFriendEventMessage());
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void socialFriendCancel(final SocialFriendInfo socialFriendInfo) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mApplication.getStrangerBusiness().cancelSocialFriendRequest(socialFriendInfo, type, new StrangerBusiness
                .onCancelSocialFriend() {
            @Override
            public void onError(int type, int code) {
                mParentActivity.hideLoadingDialog();
                if (code == -2) {
                    mParentActivity.showToast(R.string.error_internet_disconnect);
                } else {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onResponse(int type, ArrayList<SocialFriendInfo> list) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.request_success);
                mListData.clear();
                mListData.addAll(list);
                drawListView();
                EventBus.getDefault().post(new AddFriendFragment.AddFriendEventMessage());
            }
        });
    }
}

package com.metfone.selfcare.fragment.contact;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.adapter.setting.PhoneNewMessagerRecentAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PrefixChangeNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 12/27/2017.
 */

public class CreateChatFragment extends Fragment implements
        ContactChangeListener,
        InitDataListener,
        View.OnClickListener {
    private final String TAG = CreateChatFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mParentActivity;
    SimpleContactsHolderListener clickContact = new SimpleContactsHolderListener() {
        @Override
        public void onItemClick(Object entry) {
            super.onItemClick(entry);
            if (entry instanceof PhoneNumber) {
                PhoneNumber mPhone = (PhoneNumber) entry;
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                if (myNumber != null && myNumber.equals(mPhone.getJidNumber())) {
//                NavigateActivityHelper.navigateToMyProfile(mParentActivity);
                    mParentActivity.showToast(R.string.msg_not_send_me);
                } else {
                    ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(mPhone.getJidNumber());
                    NavigateActivityHelper.navigateToChatDetail(mParentActivity, threadMessage, Constants.SCREEN.SRC_CREATE_CHAT);
                }
            }
        }

        @Override
        public void onActionLabel(Object entry) {
            super.onActionLabel(entry);
            if (entry instanceof PhoneNumber) {
                PhoneNumber phone = (PhoneNumber) entry;
                InviteFriendHelper.getInstance().showInviteFriendPopup(mApplication, mParentActivity,
                        phone.getName(), phone.getJidNumber(), false);
            }
        }

        @Override
        public void onItemLongClick(Object entry) {
            super.onItemLongClick(entry);
        }
    };
    private Resources mRes;
    private OnFragmentInteractionListener mListener;
    private int actionType;
    //variable
    private HomeContactsAdapter mAdapter;
    private HomeContactsAdapter mFavoriteAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderFavoriteAdapter, mHeaderContact;
    private RecyclerView mRecyclerView, mRvFavorite, mRecyclerRecentView;
    private TextView mTvNote;
    private ProgressLoading mPbLoading;
    private ReengSearchView mSearchView;
    private ImageView ivSearch;
    private TextView tvSearchTo, txtRecent;
    private View mViewHeaderGroup, mViewHeaderBroadcast, mViewHeaderChatMore, mViewOptionChat;
    private TextView mTvwChatMoreNumber;
    private ArrayList<Object> listData = new ArrayList<>();
    private ArrayList<PhoneNumber> listSupport = new ArrayList<>();
    private ArrayList<PhoneNumber> listFavorites;
    private InitContactsListAsyncTask initContactsListAsyncTask;
    private View llFavorite, mViewHeader;
    private SearchContactTask mSearchTask;
    private String textSearch;
    private PhoneNewMessagerRecentAdapter mPhoneNumberRecentAdapter;
    private List<PhoneNumber> mListPhoneRecent = new ArrayList<>() ;
    private View rootView;
    public static CreateChatFragment newInstance() {
        CreateChatFragment fragment = new CreateChatFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach :");
        mParentActivity = (BaseSlidingFragmentActivity) activity;
        mRes = mParentActivity.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParentActivity.setColorIconStatusBar();
        // edited here
         rootView = inflater.inflate(R.layout.fragment_create_chat, container, false);
        findComponentViews(rootView, container, inflater);
        getData(savedInstanceState);
        initAdapter();
        setListener();
        return rootView;
    }

    private void initAdapter() {
        mAdapter = new HomeContactsAdapter(mApplication, new ArrayList<Object>(), false, true);
        mHeaderContact = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.setAdapter(mHeaderContact);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mHeaderContact.addHeaderView(mViewHeader);
        setItemListViewListener();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            actionType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        ListenerHelper.getInstance().addInitDataListener(this);
        if (mApplication.isDataReady()) {
            ListenerHelper.getInstance().addContactChangeListener(this);
            Log.d(TAG, "isDataReady");
            initData();
            reloadFavoriteRecentContact();
        } else {
            mPbLoading.setVisibility(View.VISIBLE);
            mPbLoading.setEnabled(true);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (initContactsListAsyncTask != null) {
            initContactsListAsyncTask.cancel(true);
            initContactsListAsyncTask = null;
        }
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
    }

    @Override
    public void onDataReady() {
        ListenerHelper.getInstance().addContactChangeListener(this);
        mParentActivity.runOnUiThread(this::initData);
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        mParentActivity.runOnUiThread(this::initData);
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        mParentActivity.runOnUiThread(this::initData);
    }

    @Override
    public void onRosterChange() {
        mParentActivity.runOnUiThread(this::initData);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.create_chat_header_group:
                mListener.navigateToChooseContacts(Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP);
                break;
            case R.id.create_chat_header_broadcast:
                mListener.navigateToChooseContacts(Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST);
                break;
            case R.id.create_chat_header_chat_more:
                String oldNumber = textSearch.trim();
                String newNumber = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(oldNumber);
                if (newNumber != null) oldNumber = newNumber;
                mApplication.getMessageBusiness().checkAndStartChatUnknownNumber(mParentActivity, oldNumber);
                break;
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        View mViewActionBar = mParentActivity.getToolBarView();
        View content = inflater.inflate(R.layout.ab_detail, null);
        mParentActivity.setCustomViewToolBar(content);

        View back = mViewActionBar.findViewById(R.id.ab_back_btn);
        //back.setVisibility(View.GONE);
        back.setOnClickListener(v -> mParentActivity.onBackPressed());

        TextView cancel = mViewActionBar.findViewById(R.id.ab_cancel_text);
        //cancel.setVisibility(View.VISIBLE);
        cancel.setPaintFlags(cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        cancel.setOnClickListener(v -> mParentActivity.onBackPressed());

       // (mViewActionBar.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        TextView title = mViewActionBar.findViewById(R.id.ab_title);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) title.getLayoutParams();
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        title.setText(R.string.new_message);

        View vDivider = mViewActionBar.findViewById(R.id.vDivider);
        vDivider.setVisibility(View.GONE);

        /*mImgBack = (ImageView) mViewActionBar.findViewById(R.id.ab_back_btn);
        mImgOverFlow = (ImageView) mViewActionBar.findViewById(R.id.ab_more_btn);*/
        mSearchView = rootView.findViewById(R.id.etSearch);
        //mSearchView.setHint("");
        ivSearch = rootView.findViewById(R.id.ivSearch);
       // ivSearch.setVisibility(View.GONE);
        tvSearchTo = rootView.findViewById(R.id.tvSearchTo);
        tvSearchTo.setVisibility(View.GONE);
        //
        mRecyclerView = rootView.findViewById(R.id.create_chat_recyclerview);

        mTvNote = rootView.findViewById(R.id.create_chat_note_text);
        mPbLoading = rootView.findViewById(R.id.create_chat_progressbar);
        mViewHeaderChatMore = rootView.findViewById(R.id.create_chat_header_chat_more);
        mTvwChatMoreNumber = rootView.findViewById(R.id.create_chat_header_chat_more_number);

        // row chat more
        mTvNote.setVisibility(View.GONE);
        mViewHeaderChatMore.setVisibility(View.GONE);
        mPbLoading.setEnabled(false);
        mPbLoading.setVisibility(View.GONE);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
//        mSearchView.postDelayed(() -> InputMethodUtils.showSoftKeyboardNew(mParentActivity, mSearchView), 300);

        mViewHeader = inflater.inflate(R.layout.item_recent_favourite_contact, null);
        txtRecent = mViewHeader.findViewById(R.id.fragment_choose_number_tv_recent);
        mRecyclerRecentView = mViewHeader.findViewById(R.id.fragment_choose_number_recent_listview);
        mViewOptionChat = mViewHeader.findViewById(R.id.llOptionCreateChat);
        mViewOptionChat.setVisibility(View.VISIBLE);
        mViewHeaderGroup = mViewHeader.findViewById(R.id.create_chat_header_group);
        mViewHeaderBroadcast = mViewHeader.findViewById(R.id.create_chat_header_broadcast);

        mRvFavorite = mViewHeader.findViewById(R.id.rvFavorite);
        mRvFavorite.setLayoutManager(new LinearLayoutManager(mApplication));
        llFavorite = mViewHeader.findViewById(R.id.llFavorite);

        rootView.findViewById(R.id.iv_keyboard).setVisibility(View.GONE);

    }

    private void drawHeaderDetail() {
        if (listFavorites == null || listFavorites.isEmpty()) {
            Log.d(TAG, "favorite empty");
            llFavorite.setVisibility(View.GONE);
        } else {
            Log.d(TAG, "favorite size: " + listFavorites.size());
            llFavorite.setVisibility(View.VISIBLE);
            notifyChangeFavorite();

        }
        if (mListPhoneRecent == null || mListPhoneRecent.isEmpty()) {
            txtRecent.setVisibility(View.GONE);
            mRecyclerRecentView.setVisibility(View.GONE);
        } else {
            txtRecent.setVisibility(View.VISIBLE);
            mRecyclerRecentView.setVisibility(View.VISIBLE);
            notifyChangeRencent();

        }
    }
    private void notifyChangeRencent() {
        if (mListPhoneRecent == null || mListPhoneRecent.isEmpty()) {
            mRecyclerRecentView.setVisibility(View.GONE);
            txtRecent.setVisibility(View.GONE);
            return;
        }
        txtRecent.setVisibility(View.VISIBLE);
        mRecyclerRecentView.setVisibility(View.VISIBLE);
        if (mPhoneNumberRecentAdapter == null) {
            mPhoneNumberRecentAdapter = new PhoneNewMessagerRecentAdapter(mListPhoneRecent,getContext());
            mRecyclerRecentView.setAdapter(mPhoneNumberRecentAdapter);
            mPhoneNumberRecentAdapter.notifyDataSetChanged();
        }
        setItemListRecentViewListener();
    }
    private void notifyChangeFavorite() {
        if (listFavorites == null || listFavorites.isEmpty()) {
            llFavorite.setVisibility(View.GONE);
            return;
        }
        llFavorite.setVisibility(View.VISIBLE);
        if (mFavoriteAdapter == null) {
            mFavoriteAdapter = new HomeContactsAdapter(mApplication, new ArrayList<>(listFavorites), false, true);
            mHeaderFavoriteAdapter = new HeaderAndFooterRecyclerViewAdapter(mFavoriteAdapter);
            mRvFavorite.setAdapter(mHeaderFavoriteAdapter);
            mRvFavorite.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
            mRvFavorite.setLayoutManager(new LinearLayoutManager(mParentActivity));
            mRvFavorite.setItemAnimator(new DefaultItemAnimator());
            mFavoriteAdapter.setClickListener(clickContact);
        } else {
            mFavoriteAdapter.setData(new ArrayList<>(listFavorites));
            mFavoriteAdapter.notifyDataSetChanged();
        }
    }

    private void getData(Bundle savedInstanceState) {
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        if (getArguments() != null) {
            actionType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
        } else if (savedInstanceState != null) {
            actionType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
        }
        mTvNote.setText(mRes.getString(R.string.not_find));
//        mImgOverFlow.setVisibility(View.GONE);
    }

    private void setListener() {
//        mImgBack.setOnClickListener(this);
        mViewHeaderGroup.setOnClickListener(this);
        mViewHeaderBroadcast.setOnClickListener(this);
        mViewHeaderChatMore.setOnClickListener(this);
        setSearchViewListener();
    }

    private void setSearchViewListener() {
            mSearchView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        rootView.findViewById(R.id.ivSearch).setVisibility(View.GONE);
                        mSearchView.setText(getString(R.string.sms_to));
                        Selection.setSelection(mSearchView.getText(), mSearchView.getText().length());
                        searchData("");
                    }else {
                        mSearchView.setHint(getString(R.string.hint_enter_name_or_phone));
                        rootView.findViewById(R.id.ivSearch).setVisibility(View.VISIBLE);
                        KeyboardUtils.hideSoftInput(getActivity());
                        searchData("");
                    }
                }
            });
        mSearchView.setDrawableClickListener(new ReengSearchView.DrawableClickListener() {
            @Override
            public void onClick(DrawablePosition target) {
                if(target == DrawablePosition.RIGHT){
                    mSearchView.setHint(getString(R.string.hint_enter_name_or_phone));
                    rootView.findViewById(R.id.ivSearch).setVisibility(View.VISIBLE);
                    KeyboardUtils.hideSoftInput(getActivity());
                    rootView.clearFocus();
                }
            }

            @Override
            public void onBackClick(int keyCode) {

            }
        });


        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(ivSearch.getVisibility() == View.VISIBLE){
                    return;
                }
                if(!s.toString().startsWith(getString(R.string.sms_to))){
                    mSearchView.setText(getString(R.string.sms_to));
                    Selection.setSelection(mSearchView.getText(), mSearchView.getText().length());
                }else {
                    searchData((s.toString()+" ").split(":")[1].trim());
                }
            }
        });
    }

    private void initData() {
        if (initContactsListAsyncTask != null) {
            initContactsListAsyncTask.cancel(true);
        }
        initContactsListAsyncTask = new InitContactsListAsyncTask();
        initContactsListAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void searchData(String key) {
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        if (TextUtils.isEmpty(key)) {
            drawHeaderDetail();
            initData();
        } else {
            llFavorite.setVisibility(View.GONE);
            textSearch = key;
            mSearchTask = new SearchContactTask(mApplication);
            mSearchTask.setDatas(listSupport);
            mSearchTask.setComparatorKeySearch(SearchUtils.getComparatorKeySearchForSearch());
            mSearchTask.setComparatorName(SearchUtils.getComparatorNameForSearch());
            mSearchTask.setComparatorContact(SearchUtils.getComparatorContactForSearch());
            mSearchTask.setComparatorMessage(SearchUtils.getComparatorMessageForSearch());
            mSearchTask.setListener(new SearchContactsListener() {
                @Override
                public void onPrepareSearchContact() {
                    if (mPbLoading != null) {
                        mPbLoading.setEnabled(true);
                        mPbLoading.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFinishedSearchContact(String keySearch, ArrayList<Object> list) {
                    textSearch = keySearch;
                    if (mPbLoading != null) {
                        mPbLoading.setEnabled(false);
                        mPbLoading.setVisibility(View.GONE);
                    }
                    notifyChangeAdapter(list);
                }
            });
            mSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
        }
    }

    private void notifyChangeAdapter(ArrayList<Object> list) {
        if (!list.isEmpty() && list.get(0) != null) {
            if (((PhoneNumber) list.get(0)).getContactId() == null
                    && "#".equals(((PhoneNumber) list.get(0)).getName())) {
                PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), NumberConstant.SECTION_TYPE_CONTACT, true);
                list.add(0, sectionContacts);
            }  else if (((PhoneNumber) list.get(0)).getContactId() == null && !"A".equals(((PhoneNumber) list.get(0)).getName())) {
                list.remove(0);
                PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), NumberConstant.SECTION_TYPE_CONTACT, true);
                list.add(0, sectionContacts);
            }  else if (((PhoneNumber) list.get(0)).getContactId() == null && "A".equals(((PhoneNumber) list.get(0)).getName())) {
                list.remove(0);
            } else {
                PhoneNumber sectionContacts = new PhoneNumber(null, mRes.getString(R.string.menu_contacts), NumberConstant.SECTION_TYPE_CONTACT, true);
                list.add(0, sectionContacts);
            }
//            if (!(list.get(0) instanceof PhoneNumber) && list.get(0).getContactId() != null && !"A".equals(list.get(0).getName())) {
//                list.add(0, sectionContacts);
//            }  else {
//                ((PhoneNumber) list.get(0)).setContact(true);
//            }
        }
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
        showOrHideItemMoreNumber(textSearch, list);
    }
    private void setItemListRecentViewListener() {
        mPhoneNumberRecentAdapter.setOnItemRecentClickListener(new PhoneNewMessagerRecentAdapter.IOnItemPhoneRecentClickListener() {
            @Override
            public void onClick(int position, PhoneNumber entry) {
                if (entry instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) entry;
                    String number = phone.getJidNumber();
                    String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                    if (TextUtils.isEmpty(number)) {
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    } else if (number.equals(myNumber)) {
                        mParentActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                    } else {
                        mListener.returnCreatedThread(mApplication.getMessageBusiness().findExistingOrCreateNewThread(number));
                    }
                } else {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        });
    }

    private void setItemListViewListener() {
        mAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                if (entry instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) entry;
                    String number = phone.getJidNumber();
                    String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                    if (TextUtils.isEmpty(number)) {
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    } else if (number.equals(myNumber)) {
                        mParentActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                    } else {
                        mListener.returnCreatedThread(mApplication.getMessageBusiness().findExistingOrCreateNewThread(number));
                    }
                } else {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onItemLongClick(Object object) {
                super.onItemLongClick(object);
            }
        });
    }

    private void showOrHideItemMoreNumber(String textSearch, ArrayList<Object> result) {
        int countSpace = TextHelper.countSpace(textSearch);
        if (result == null || result.isEmpty()) {
            if ((countSpace == 0) && PhoneNumberHelper.getInstant().isValidNumberNotRemoveChar(textSearch)) {
                mViewHeaderChatMore.setVisibility(View.VISIBLE);
                mTvwChatMoreNumber.setText(String.format(mRes.getString(R.string.chat_more), textSearch));
                mTvNote.setVisibility(View.GONE);
            } else {
                mTvNote.setVisibility(View.VISIBLE);
            }
        } else {
            mViewHeaderChatMore.setVisibility(View.GONE);
            mTvNote.setVisibility(View.GONE);
        }
    }

    private void reloadFavoriteRecentContact() {
        Log.i(TAG, "reloadFavoriteRecentContact");
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                ContactBusiness contactBusiness = mApplication.getContactBusiness();
                listFavorites = contactBusiness.getListNumberFavorites();
                mListPhoneRecent = new ArrayList<>();;
                List<PhoneNumber> mList = new ArrayList<>();
                mList = contactBusiness.getRecentPhoneNumber();
                if (mList != null) {
                    mListPhoneRecent = removePhoneRecentDuplicates(mList);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                drawHeaderDetail();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    public List<PhoneNumber>  removePhoneRecentDuplicates(List<PhoneNumber> list){
        Set<PhoneNumber> set = new TreeSet(new Comparator<PhoneNumber>() {

            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if(o1.getJidNumber().equalsIgnoreCase(o2.getJidNumber())){
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }
    public interface OnFragmentInteractionListener {
        void navigateToChooseContacts(int type);

        void returnCreatedThread(ThreadMessage threadMessage);
    }

    private interface SearchContactsListener {
        void onPrepareSearchContact();

        void onFinishedSearchContact(String keySearch, ArrayList<Object> list);
    }

    private static class SearchContactTask extends AsyncTask<String, Void, ArrayList<Object>> {
        private final String TAG = "SearchContactTask";
        private WeakReference<ApplicationController> application;
        private SearchContactsListener listener;
        private String keySearch;
        private CopyOnWriteArrayList<PhoneNumber> datas;
        private Comparator comparatorName;
        private Comparator comparatorKeySearch;
        private Comparator comparatorContact;
        private Comparator comparatorMessage;
        private long startTime;
        private int totalDatas;

        public SearchContactTask(ApplicationController application) {
            this.application = new WeakReference<>(application);
            this.datas = new CopyOnWriteArrayList<>();
        }

        public void setListener(SearchContactsListener listener) {
            this.listener = listener;
        }

        public void setDatas(ArrayList<PhoneNumber> datas) {
            if (this.datas != null && datas != null)
                this.datas.addAll(datas);
        }

        public void setComparatorName(Comparator comparatorName) {
            this.comparatorName = comparatorName;
        }

        public void setComparatorKeySearch(Comparator comparatorKeySearch) {
            this.comparatorKeySearch = comparatorKeySearch;
        }

        public void setComparatorContact(Comparator comparatorContact) {
            this.comparatorContact = comparatorContact;
        }

        public void setComparatorMessage(Comparator comparatorMessage) {
            this.comparatorMessage = comparatorMessage;
        }

        @Override
        protected void onPreExecute() {
            if (listener != null) listener.onPrepareSearchContact();
        }

        @Override
        protected void onPostExecute(ArrayList<Object> result) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                        + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
            if (listener != null) listener.onFinishedSearchContact(keySearch, result);
        }

        @Override
        protected ArrayList<Object> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            totalDatas = 0;
            ArrayList<Object> list = new ArrayList<>();
            keySearch = params[0];
            if (Utilities.notNull(application) && Utilities.notEmpty(datas)) {
                totalDatas = datas.size();
                ResultSearchContact result = SearchUtils.searchMessagesAndContacts(application.get()
                        , keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage, comparatorContact);
                if (result != null) {
                    keySearch = result.getKeySearch();
                    if (Utilities.notEmpty(result.getResult())) {
                        list.addAll(result.getResult());
                    }
                }
            }
            return list;
        }
    }

    private class InitContactsListAsyncTask extends AsyncTask<Void, Void, ArrayList<Object>> {
        private ArrayList<PhoneNumber> supports;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Object> doInBackground(Void... params) {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            ArrayList<Object> listAll = new ArrayList<>();
            supports = contactBusiness.getListNumberAlls();
//            supports = getSupportNumbers();
            if (!supports.isEmpty()) {
                listAll.addAll(supports);
            }
            return listAll;
        }

        @Override
        protected void onPostExecute(ArrayList<Object> result) {
            super.onPostExecute(result);
            listData = result;
            listSupport = supports;
            String searchKey = "";
            if(mSearchView.getText().toString().contains(":")){
                searchKey = (mSearchView.getText().toString()+" ").split(":")[1].trim();
            }

            if (TextUtils.isEmpty(searchKey)) {
                mPbLoading.setEnabled(false);
                mPbLoading.setVisibility(View.GONE);
                notifyChangeAdapter(listData);
            } else {
                searchData(searchKey);
            }
        }
    }
}
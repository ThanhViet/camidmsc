package com.metfone.selfcare.fragment.history;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.api.response.PaymentHistoryResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.WsPaymentHistoryRequest;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.CustomSpinner;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.RetrofitInstance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.module.home_kh.api.KhHomeClient.createBaseRequest;

public class HistoryFragment extends Fragment {
    public static String TAG = HistoryFragment.class.getName();
    ApiService apiService;
    private HistoryAdapter historyAdapter;
    ArrayList<Object> listItem = new ArrayList<>();
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.spinner_date_filter)
    CustomSpinner spinner;
    @BindView(R.id.rc_history_payment)
    RecyclerView rcHistory;
    @BindView(R.id.view_loading)
    FrameLayout loading;
    @BindView(R.id.iv_spinner)
    ImageView ivSpinner;
    @BindView(R.id.fl_spinner)
    FrameLayout flSpinner;
    @BindView(R.id.fl_header)
    FrameLayout header;
    public static boolean isHistoryScreen = false;
    private int curentPageIndex = 0, totalPages = 0;
    public static final int RECORDS_NUMBER = 10;
    private String fromDate, toDate;
    private boolean mIsLoadingMore = false;


    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(
                true // default to enabled
        ) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(
                this, // LifecycleOwner
                callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isHistoryScreen = false;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isHistoryScreen = true;
        ButterKnife.bind(this, view);
        String[] stringArray = getResources().getStringArray(R.array.history_filter_arrays);
        spinner.setAdapter(new SpinnerApdater(getContext(), R.layout.item_spinner, stringArray));
        historyAdapter = new HistoryAdapter(getContext(), listItem);
        rcHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        rcHistory.setAdapter(historyAdapter);
        fromDate = getDayAgo(30, 0);
        toDate = getCurrentDate();
        flSpinner.setOnClickListener(v -> spinner.performClick());
        ivBack.setOnClickListener(v -> getParentFragmentManager().popBackStack());
        paymentHistory();
        initScrollListener();
        loadMore();
        header.setPadding(0,getStatusBarHeight(),0,0);

        spinner.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened() {
                ivSpinner.setRotation(180);
            }

            @Override
            public void onSpinnerClosed() {
                ivSpinner.setRotation(0);
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mIsLoadingMore = false;
                curentPageIndex = 0;
                totalPages = 0;
                toDate = getCurrentDate();
                if (position == 0) {
                    fromDate = getDayAgo(30, 0);
                } else if (position == 1) {
                    fromDate = getDayAgo(0, 3);
                } else {
                    fromDate = getDayAgo(0, 6);
                }
                paymentHistory();
                initScrollListener();
                loadMore();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void paymentHistory() {
        if (!mIsLoadingMore) {
            loading.setVisibility(View.VISIBLE);
        }
        String isdn = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        String language = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
        WsPaymentHistoryRequest.WsRequest wsRequest =
                new WsPaymentHistoryRequest.WsRequest(language, isdn, curentPageIndex, RECORDS_NUMBER,
                        convertToServerDate(fromDate), convertToServerDate(toDate));
        WsPaymentHistoryRequest request =
                createBaseRequest(new WsPaymentHistoryRequest(), Constants.WSCODE.WS_PAYMENT_HISTORY);
        request.setWsRequest(wsRequest);
        apiService = RetrofitInstance.getInstance().create(ApiService.class);
        Call<PaymentHistoryResponse> call = apiService.paymentHistory(request);
        call.enqueue(new Callback<PaymentHistoryResponse>() {
            @Override
            public void onResponse(Call<PaymentHistoryResponse> call, Response<PaymentHistoryResponse> response) {
                loading.setVisibility(View.GONE);
                if (response.body() != null && response.body().getRequest().getRequest() != null) {
                    if (!mIsLoadingMore) {
                        listItem.clear();
                    }
                    rcHistory.setVisibility(View.VISIBLE);
                    listItem.addAll(response.body().getRequest().getRequest().getPaymentHistoryList());
                    historyAdapter.notifyDataSetChanged();
                    mIsLoadingMore = false;
                }
            }

            @Override
            public void onFailure(Call<PaymentHistoryResponse> call, Throwable t) {
                loading.setVisibility(View.GONE);
            }
        });
    }

    private void initScrollListener() {
        rcHistory.addOnScrollListener(new OnScrollToLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!mIsLoadingMore) {
                    if (curentPageIndex < totalPages) {
                        //bottom of list!
                        ++curentPageIndex;
                        loadMore();
                    }
                }
            }
        });
    }

    private void loadMore() {
        mIsLoadingMore = true;
        // show loading item at bottom
        if (HistoryAdapter.IS_USE_ITEM_LOADING) {
            listItem.add(null);
            historyAdapter.notifyItemInserted(listItem.size() - 1);
        }
        paymentHistory();
    }


    public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        return df.format(c.getTime());
    }

    public String getDayAgo(int dayAgo, int monthAgo) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(calendar.getTime());
        if (dayAgo > 0) {
            calendar.add(Calendar.DAY_OF_YEAR, -dayAgo);
        } else {
            calendar.add(Calendar.MONTH, -monthAgo);
        }
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        return df.format(calendar.getTime());
    }

    public static String convertToServerDate(String input) {
        DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String s = "";
        try {
            s = outputFormat.format(inputFormat.parse(input));
        } catch (Exception e) {

        }

        return s;
    }

    public class SpinnerApdater extends ArrayAdapter<String> {

        String[] strings;

        public SpinnerApdater(Context context, int textViewResourceId, String[] strings) {
            super(context, textViewResourceId, strings);
            this.strings = strings;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            CamIdTextView label = row.findViewById(R.id.tv_date);
            label.setText(strings[position]);
            return row;
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
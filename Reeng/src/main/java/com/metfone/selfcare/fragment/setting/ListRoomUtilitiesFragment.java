package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.OfficerListAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 12/30/2015.
 */
public class ListRoomUtilitiesFragment extends Fragment {
    private final String TAG = ListRoomUtilitiesFragment.class.getSimpleName();
    private static final String PAGE_NUM = "PAGE_NUM";
    private SettingActivity mParentActivity;
    private ApplicationController mApplication;
    private OnFragmentInteractionListener mListener;

    private View rootView;
    private TextView mNoteEmptyTextView;
    private LinearLayoutManager mRecyclerManager;
    private RecyclerView mRecyclerView;
    private OfficerListAdapter mAdapter;

    private ArrayList<OfficerAccount> mRoomList;
    private int mPageNum = 0;

    public static ListRoomUtilitiesFragment newInstance(int pageNum) {
        ListRoomUtilitiesFragment fragment = new ListRoomUtilitiesFragment();
        Bundle args = new Bundle();
        args.putInt(PAGE_NUM, pageNum);
        fragment.setArguments(args);
        return fragment;
    }

    public ListRoomUtilitiesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNum = getArguments().getInt(PAGE_NUM);
        mRoomList = new ArrayList<>();
        if (mApplication.getOfficerBusiness().getmListRoomTab().size() > 0) {
            for (OfficerAccount account : mApplication.getOfficerBusiness().getmListRoomTab()) {
                if (account.getType() == mPageNum) {
                    mRoomList.add(account);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_starmusic_list, container, false);
        findComponentViews();
        drawListView();
        setViewListeners();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SettingActivity) activity;
        mApplication = (ApplicationController) activity.getApplication();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void findComponentViews() {
        mNoteEmptyTextView = (TextView) rootView.findViewById(R.id.empty_list_textview);
        mNoteEmptyTextView.setVisibility(View.GONE);
        if (mRoomList.size() == 0) {
            mNoteEmptyTextView.setVisibility(View.VISIBLE);
            mNoteEmptyTextView.setText(mApplication.getResources().getString(R.string.list_empty));
        }
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);
    }

    private void setViewListeners() {
        // on touch
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodUtils.hideSoftKeyboard(mParentActivity);
                return false;
            }
        });
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                OfficerAccount account = (OfficerAccount) object;
                if (account == null || TextUtils.isEmpty(account.getServerId())) {
                    return;
                }
//                DeepLinkHelper.getInstance().handleFollowRoom(mApplication,mParentActivity,
//                        account.getServerId(),account.getName(),account.getAvatarUrl());
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void drawListView() {
        mAdapter = new OfficerListAdapter(mApplication, Constants.CONTACT.CONTACT_VIEW_NOMAL, mRoomList);
        mRecyclerManager = new LinearLayoutManager(mParentActivity, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mRecyclerManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }

    public interface OnFragmentInteractionListener {
        void navigateToThreadDetail(ThreadMessage threadMessage);
    }
}

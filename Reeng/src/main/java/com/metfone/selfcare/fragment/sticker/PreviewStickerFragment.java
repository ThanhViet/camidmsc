package com.metfone.selfcare.fragment.sticker;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.StickerActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.task.DownloadStickerAsynctask;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;

/**
 * Created by THANHNT72 on 5/7/2015.
 */
public class PreviewStickerFragment extends Fragment implements ClickListener.IconListener,
        DownloadStickerAsynctask.DownloadStickerCallBack {

    private static final String TAG = PreviewStickerFragment.class.getSimpleName();
    private static final int READ_SIZE = 1024 * 32;
    private StickerActivity mActivity;
    private TextView mTvwCollectionName, mTvwCollectionAmount;
    private ImageView mImgCollectionAvatar, mImgCollectionPreview;
    private ProgressLoading progressBar;
    private Button mBtnCollectionDownload, mBtnCollectionPreviewDownload;
    private RelativeLayout layoutCollectionInfo;

    private LinearLayout mLayoutRetry;
    private TextView mTvwMessage;
    private Button mBtnRetry;
    private Toolbar mToolBar;
//    private ImageView mBtnMore;

    private DownloadStickerAsynctask downloadStickerAsynctask;
    private StickerCollection mStickerCollection;
    private Resources mRes;
    //
    private ApplicationController mApplicationController;
    private StickerBusiness mStickerBusiness;
    private Handler mHandler;
    private long mCollectionId;
    private boolean isDownloadCancel = false;
    private ClickListener.IconListener mIconListener;

    // Progress Dialog
    private ProgressDialog barProgressDialog;

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    private boolean isUpdateSticker;

    public static PreviewStickerFragment newInstance(int serverID, boolean isUpdate) {
        PreviewStickerFragment fragment = new PreviewStickerFragment();
        Bundle args = new Bundle();
        args.putInt(StickerActivity.PARAM_STICKER_COLLECTION_ID, serverID);
        args.putBoolean(StickerActivity.PARAM_UPDATE_STICKER, isUpdate);
        fragment.setArguments(args);
        return fragment;
    }

/*    public static PreviewStickerFragment newInstance(StickerCollection sticker) {
        PreviewStickerFragment fragment = new PreviewStickerFragment();
        Bundle args = new Bundle();
        args.putSerializable(StickerActivity.PARAM_STICKER_COLLECTION, sticker);
        fragment.setArguments(args);
        return fragment;
    }*/

    public PreviewStickerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isUpdateSticker = getArguments().getBoolean(StickerActivity.PARAM_UPDATE_STICKER, false);
            mCollectionId = getArguments().getInt(StickerActivity.PARAM_STICKER_COLLECTION_ID);
            mStickerCollection = mStickerBusiness.getStickerCollectionFromStoreById(mCollectionId);
            if (mStickerCollection != null && mStickerCollection.isNew()) {
                mStickerCollection.setIsNew(false);
                mStickerBusiness.insertOrUpdate(mStickerCollection);
            }
            if (mStickerCollection != null && mStickerCollection.isDownloaded() &&
                    mStickerCollection.getLastLocalUpdate() < mStickerCollection.getLastServerUpdate()) {
                isUpdateSticker = true; // can cap nhat collection
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_preview_sticker, container, false);
        findComponentViews(rootView);
        if (!mStickerCollection.isDownloaded() || isUpdateSticker) {
            mBtnCollectionDownload.setVisibility(View.VISIBLE);
            mBtnCollectionPreviewDownload.setVisibility(View.GONE);
        } else {
            mBtnCollectionDownload.setVisibility(View.GONE);
            mBtnCollectionPreviewDownload.setVisibility(View.VISIBLE);
        }
        setViewListeners();
        return rootView;
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        mIconListener = this;
        super.onResume();
    }

    @Override
    public void onPause() {
        mHandler = null;
        super.onPause();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
/*        if (savedInstanceState != null) {
            mCollectionId = savedInstanceState.getLong(PreviewStickerActivity.PARAM_STICKER_COLLECTION_ID);
            mStickerCollection = (StickerCollection) savedInstanceState.getSerializable(
                    PreviewStickerActivity.PARAM_STICKER_COLLECTION);
        }*/
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(StickerActivity.PARAM_STICKER_COLLECTION_ID, mCollectionId);
//        outState.putSerializable(StickerActivity.PARAM_STICKER_COLLECTION, mStickerCollection);
        super.onSaveInstanceState(outState);
    }

    private void setViewCollection(StickerCollection collection) {
        mTvwCollectionName.setText(collection.getCollectionName());
        String amountString;
        if (collection.getNumberSticker() > 1) {
            amountString = collection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text);
        } else {
            amountString = collection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text_single);
        }
        mTvwCollectionAmount.setText(amountString);
        //mTvwCollectionAmount.setText(collection.getNumberSticker() + " " + mRes.getString(R.string.stickers_text));
        // lay full domain tu get domain confix
        String fullAvatarLink = UrlConfigHelper.getInstance(mApplicationController).getDomainFile()
                + collection.getCollectionIconPath();
        Log.i(TAG, "fullAvatarLink " + fullAvatarLink);
        ImageLoaderManager.getInstance(mApplicationController).
                displayStickerNetwork(mImgCollectionAvatar, fullAvatarLink);
        if (isUpdateSticker) {
            mBtnCollectionDownload.setText(mRes.getString(R.string.update));
            mBtnCollectionDownload.setBackgroundResource(R.drawable.selector_rec_w_border_purple);
            mBtnCollectionDownload.setTextColor(ContextCompat.getColor(mActivity, R.color.bg_mocha));
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setViewListeners() {
        if (mStickerCollection != null) {
            setViewCollection(mStickerCollection);
            mBtnCollectionDownload.setOnClickListener(v -> {
                Log.i(TAG, "mSticker: " + mStickerCollection);
                if (!mStickerCollection.isDownloaded() || isUpdateSticker) {
                    if (downloadStickerAsynctask != null) {
                        downloadStickerAsynctask.cancel(true);
                        downloadStickerAsynctask = null;
                    }
                    downloadStickerAsynctask = new DownloadStickerAsynctask(mApplicationController,
                            mStickerCollection, isUpdateSticker, PreviewStickerFragment.this);
                    downloadStickerAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    String message = String.format(mRes.getString(
                            R.string.confirm_delete_sticker_collection), mStickerCollection.getCollectionName());
                    PopupHelper.getInstance().showDialogConfirm(mActivity,
                            "", message, mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                            mIconListener, mStickerCollection, Constants.MENU.POPUP_DELETE_STICKER_COLLECTION);
                }
            });
        }

        mBtnRetry.setOnClickListener(view -> {
//                getCollectionInfo(mCollectionId);
        });
    }

    private void findComponentViews(View rootView) {
        mTvwCollectionName = rootView.findViewById(R.id.more_sticker_name);
        mTvwCollectionAmount = rootView.findViewById(R.id.more_sticker_amount);
        mImgCollectionAvatar = rootView.findViewById(R.id.more_sticker_avatar);
        mImgCollectionPreview = rootView.findViewById(R.id.img_preview);
        mBtnCollectionDownload = rootView.findViewById(R.id.more_sticker_download_button);
        mBtnCollectionPreviewDownload = rootView.findViewById(R.id.more_sticker_download_button_preview);

        layoutCollectionInfo = rootView.findViewById(R.id.layout_collection_info);
        progressBar = rootView.findViewById(R.id.progress_loading);
        mLayoutRetry = rootView.findViewById(R.id.layout_retry);
        mLayoutRetry.setVisibility(View.GONE);
        mTvwMessage = rootView.findViewById(R.id.text_message);
        mBtnRetry = rootView.findViewById(R.id.btn_retry);
        mImgCollectionPreview.setVisibility(View.GONE);
        mToolBar = mActivity.getToolBarView();
//        mBtnMore = mToolBar.findViewById(R.id.ab_more_btn);
//        mBtnMore.setVisibility(View.GONE);
        if (mStickerCollection != null) {
            progressBar.setVisibility(View.GONE);
            layoutCollectionInfo.setVisibility(View.VISIBLE);
            setButtonByColection(mStickerCollection);
            if (!TextUtils.isEmpty(mStickerCollection.getCollectionPreviewPath())
                    && !"null".equals(mStickerCollection.getCollectionPreviewPath())) {
                String preview = UrlConfigHelper.getInstance(mActivity).getDomainFile()
                        + mStickerCollection.getCollectionPreviewPath();
                Log.i(TAG, "preview " + preview);
                progressBar.setVisibility(View.VISIBLE);
                mImgCollectionPreview.setVisibility(View.VISIBLE);
                Glide.with(mActivity).load(preview).transition(DrawableTransitionOptions.withCrossFade()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                }).into(mImgCollectionPreview);
            }
        } else {
//            getCollectionInfo(mCollectionId);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = (StickerActivity) activity;
        mRes = mActivity.getResources();
        mApplicationController = (ApplicationController) mActivity.getApplication();
        mStickerBusiness = mApplicationController.getStickerBusiness();
        super.onAttach(activity);
    }

    public void launchBarDialog(String message) {
        barProgressDialog = new ProgressDialog(mActivity);
        barProgressDialog.setTitle(mRes.getString(R.string.loading));
        barProgressDialog.setMessage(message);
        barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        Drawable drawable = ContextCompat.getDrawable(mActivity, R.drawable.bg_voice_send_progress_bar);
        barProgressDialog.setProgressDrawable(drawable);
        barProgressDialog.setProgress(0);
        barProgressDialog.setMax(100);
        barProgressDialog.setCancelable(true);
        barProgressDialog.setCanceledOnTouchOutside(false);
        barProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                mRes.getString(R.string.cancel), (dialog, which) -> {
                    barProgressDialog.dismiss();
                    if (downloadStickerAsynctask != null) {
                        downloadStickerAsynctask.cancel(true);
                        downloadStickerAsynctask = null;
                    }
                    isDownloadCancel = true;
                });
        barProgressDialog.setOnDismissListener(dialogInterface -> {
            if (downloadStickerAsynctask != null) {
                downloadStickerAsynctask.cancel(true);
                downloadStickerAsynctask = null;
            }
            isDownloadCancel = true;
        });
        isDownloadCancel = false;
        barProgressDialog.show();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_DELETE_STICKER_COLLECTION:
                mStickerBusiness.deleteStickerCollection(mStickerCollection);
                if (mStickerCollection.getCollectionState() != StickerConstant.COLLECTION_STATE_ENABLE) {
                    mActivity.onBackPressed();
                } else {
                    setButtonByColection(mStickerCollection);
                }
                break;
            default:
                break;
        }
    }

    private void setButtonByColection(StickerCollection stickerCollection) {
        if (stickerCollection.isDownloaded()) {
            mBtnCollectionDownload.setBackgroundResource(R.drawable.bg_button_chat);
            mBtnCollectionDownload.setText(mActivity.getString(R.string.delete));
            mBtnCollectionDownload.setTextColor(ContextCompat.getColor(mActivity, R.color.white));
        } else {
            mBtnCollectionDownload.setBackgroundResource(R.drawable.bg_button_chat);
            mBtnCollectionDownload.setText(mActivity.getString(R.string.free_download));
            mBtnCollectionDownload.setTextColor(ContextCompat.getColor(mActivity, R.color.white));
        }
    }

    @Override
    public void onPreExecute(StickerCollection stickerCollection) {
        launchBarDialog(mStickerCollection.getCollectionName());
    }

    @Override
    public void onPostExecute(Boolean result, StickerCollection stickerCollection) {
        if (isDownloadCancel) return;
        if (mHandler != null) {
            if (result) { // download ok
                barProgressDialog.dismiss();
                mStickerCollection.setDownloaded(true);
                isUpdateSticker = false;
                mActivity.showToast(mRes.getString(R.string.sticker_download_done), Toast.LENGTH_LONG);
                setButtonByColection(mStickerCollection);
            } else { // download fail
                barProgressDialog.dismiss();
                mStickerCollection.setDownloaded(false);
                setButtonByColection(mStickerCollection);
                if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                    mActivity.showToast(mRes.getString(R.string.prepare_photo_fail), Toast.LENGTH_LONG);
                } else {
                    mActivity.showToast(mRes.getString(R.string.sticker_download_fail), Toast.LENGTH_LONG);
                }
            }
        }
    }

    @Override
    public void onProgressUpdate(int progress) {
        if (mHandler != null) {
            if (progress < 0) {
                barProgressDialog.setIndeterminate(true);
            } else {
                barProgressDialog.setIndeterminate(false);
                barProgressDialog.setProgress(progress);
            }
        }
    }
}

package com.metfone.selfcare.fragment.musickeeng;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.NearYouAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.contact.StrangerDetailFragmentNew;
import com.metfone.selfcare.fragment.setting.BaseSettingFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.helper.httprequest.NearYouRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayoutDirection;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.utils.LinearItemDecoration;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/3/2017.
 */

public class NearYouFragment extends BaseSettingFragment implements NearYouRequestHelper.OnLoadDataResponseListener
        , SwipyRefreshLayout.OnRefreshListener, NearYouAdapter.NearYouInterface, ClickListener.IconListener
        , InitDataListener, PermissionHelper.RequestPermissionsResult, View.OnClickListener, StrangerFilterHelper.FilterChangeListener {
    private static String TAG = NearYouFragment.class.getSimpleName();
    private BaseSlidingFragmentActivity mParentActivity;
    private ApplicationController mApplication;
    private NearYouRequestHelper mRequestHelper;
    private Resources mRes;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private RecyclerView mRecyclerView;
    private TextView mTvwNoteEmpty;//mTvwAbTitle,
    private ProgressLoading mProgressLoading;
    private View mLayoutProgressPreLoad;
    private View mLayoutFooter;
    private LinearLayout mLoadmoreFooterView;
    //
    private Handler mHandler;
    private NearYouAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private LinearLayoutManager mLayoutManager;

    private ArrayList<StrangerMusic> mStrangerMusics;
    private boolean isReadyLoadMore = false;
    private boolean isLoadMore = false;
    private ArrayList<ItemContextMenu> mOverFlowItems;
    private boolean isDeclinedLocation = false;
    private boolean isAroundProcessed = false;
    private View mLayoutHeader;
    private View viewFilter;
    private AppCompatTextView tvFilter, tvHeaderFilter;
    private AppCompatImageView ivFilter, ivHeaderFilter, ivHeaderRefresh, ivRefresh;
    private StrangerFilterHelper filterHelper;
    private boolean filterRecentChanged;

    private View viewAppbar;
    private AppCompatTextView tvTitle;
    private AppCompatImageView icBack;
    private Handler handlerLoadmore = new Handler() {
        @Override
        public void handleMessage(@androidx.annotation.NonNull Message msg) {
            super.handleMessage(msg);
            onLoadMore();
        }
    };
    private StrangerDetailFragmentNew.OnStrangerFragmentInteractionListener mListener;
    private int type;


    public static NearYouFragment newInstance() {
        return new NearYouFragment();
    }

    public static NearYouFragment newInstance(boolean isShowToolbar) {
        Bundle args = new Bundle();
        args.putBoolean("is_show_toolbar", isShowToolbar);
        NearYouFragment fragment = new NearYouFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public NearYouFragment() {
    }

    @Override
    protected void initView(View view) {
        mHandler = new Handler();
        findComponentViews(view);
//        view.findViewById(R.id.appBar).setVisibility(View.GONE);
//        setTitle(mRes.getString(R.string.menu_near_you));
//        if (icOptionToolbar != null) {
//            icOptionToolbar.setVisibility(View.VISIBLE);
//            icOptionToolbar.setImageResource(R.drawable.ic_v5_more);
//            icOptionToolbar.setOnClickListener(new OnSingleClickListener() {
//                @Override
//                public void onSingleClick(View view) {
//                    initOverFlowMenu();
//                    PopupHelper.getInstance().showOrHideOverFlowMenu(icOptionToolbar, NearYouFragment.this, mOverFlowItems);
//                }
//            });
//        }
        if (mApplication.isDataReady()) {
            getData();
        }
        drawTabFilter();
        ListenerHelper.getInstance().addInitDataListener(this);
        setGridViewListener();
        filterHelper.addFilterChange(this);

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {

        mParentActivity = (BaseSlidingFragmentActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mRequestHelper = NearYouRequestHelper.getInstance(mApplication);
        mRes = mParentActivity.getResources();
        filterHelper = StrangerFilterHelper.getInstance(mApplication);
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SettingActivity) {
            mListener = (SettingActivity) context;
        }
        type = NumberConstant.TYPE_STRANGER_MOCHA;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.i(TAG, "setUserVisibleHint: " + isVisibleToUser);
        if (isVisibleToUser) {
            if (filterRecentChanged) {
                filterRecentChanged = false;
                refreshAfterChooseFilter();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_near_you;
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        LocationHelper.getInstant(mApplication).disconnectGoogleApiClient();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        mHandler = null;
        ListenerHelper.getInstance().removeInitDataListener(this);
        if (filterHelper != null) {
            filterHelper.removeFilterChangeListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        PermissionHelper.removeCallBack(this);
        super.onDetach();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
        PopupHelper.getInstance().destroyOverFlowMenu();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) return;
        mHandler.post(this::getData);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isDeclinedLocation) {
            isDeclinedLocation = false;
            mParentActivity.finish();
        } else {
            if (!isAroundProcessed && hasPermissionLocation()) {
                processShowTabAround();
            }
        }
    }

    private boolean hasPermissionLocation() {
        return !(PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @Override
    public void onClickPoster(StrangerMusic strangerMusic) {
        mParentActivity.trackingEvent(R.string.ga_category_stranger_music,
                R.string.ga_action_interaction, R.string.ga_label_stranger_music_click_avatar);
        if (strangerMusic.isStarRoom()) {
            processFollowStarRoom(strangerMusic);
        } else {
            processClickAvatarUser(strangerMusic.getPosterJid(),
                    strangerMusic.getPosterName(),
                    strangerMusic.getPosterLastAvatar(),
                    strangerMusic.getPosterStatus(),
                    strangerMusic.getPosterBirthDay(),
                    strangerMusic.getPosterGender());
        }
    }

    @Override
    public void onClickMessage(StrangerMusic entry) {
        if (mApplication != null && mApplication.getStrangerBusiness() != null) {
            ThreadMessage threadMessage = mApplication.getStrangerBusiness().createMochaStrangerAndThread(entry.getPosterJid(),
                    entry.getPosterName(), entry.getPosterLastAvatar(), Constants.CONTACT.STRANGER_MOCHA_ID, true);
            mListener.navigateToThreadDetail(threadMessage);
        }
    }


    @Override
    public void onClickListen(final StrangerMusic strangerAround) {
        NavigateActivityHelper.navigateToSelectSongAndListenr(mApplication, mParentActivity, strangerAround.getPosterJid(),
                strangerAround.getPosterName(), strangerAround.getPosterLastAvatar());
        mApplication.trackingEvent(R.string.ga_category_stranger_music, R.string.ga_action_interaction, R.string
                .ga_label_around_click_listen);
    }

    private void getData() {
        if (!hasPermissionLocation()) {
            PermissionHelper.setCallBack(this);
            PermissionHelper.requestPermissionWithGuide(mParentActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
        } else {
            processShowTabAround();
        }
    }

    public void initOverFlowMenu() {
        mOverFlowItems = new ArrayList<>();
        int currentOptionFilter = mRequestHelper.getOptionFilter();
        for (int menuId : Constants.STRANGER_MUSIC.getOptionFilterList()) {
            int iconResource = -1;
            if (menuId == currentOptionFilter) {
                iconResource = R.drawable.white_dot;
            }
            ItemContextMenu item = new ItemContextMenu(
                    getStringMenuFilter(menuId), iconResource, null, menuId);
            mOverFlowItems.add(item);
        }
        ItemContextMenu deleteHistory = new ItemContextMenu(mRes.getString(R.string.delete_stranger)
                , -1,
                null, Constants.MENU.MENU_DELETE_STRANGER);
        mOverFlowItems.add(deleteHistory);
    }

    private void findComponentViews(View rootView) {
        viewAppbar = rootView.findViewById(R.id.layoutAppbar);
        if (getArguments() != null) {
            if (getArguments().getBoolean("is_show_toolbar", false)) {
                viewAppbar.setVisibility(View.VISIBLE);
                tvTitle = viewAppbar.findViewById(R.id.txtTitleToolbar);
                tvTitle.setText(R.string.nearby_friends);
                icBack = viewAppbar.findViewById(R.id.icBackToolbar);
                icBack.setOnClickListener(view -> getActivity().onBackPressed());
            }
        }
        mLayoutHeader = LayoutInflater.from(mParentActivity).inflate(R.layout.layout_filter_stranger_v5, (ViewGroup) rootView, false);
        tvHeaderFilter = mLayoutHeader.findViewById(R.id.tvFilter);
        ivHeaderFilter = mLayoutHeader.findViewById(R.id.ivFilter);
        ivHeaderRefresh = mLayoutHeader.findViewById(R.id.ivRefresh);
        mSwipyRefreshLayout = rootView.findViewById(R.id.stranger_music_swipy_layout);
        mRecyclerView = rootView.findViewById(R.id.stranger_music_recycleview);
        mProgressLoading = rootView.findViewById(R.id.stranger_music_loading_progressbar);
        mLayoutProgressPreLoad = rootView.findViewById(R.id.layout_progress_music);
        mLayoutProgressPreLoad.setVisibility(View.VISIBLE);
        mTvwNoteEmpty = rootView.findViewById(R.id.stranger_music_note_text);

        mLayoutFooter = getLayoutInflater().inflate(R.layout.item_onmedia_loading_footer, (ViewGroup) rootView, false);
        mLoadmoreFooterView = mLayoutFooter.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);
        mProgressLoading.setVisibility(View.GONE);
        mTvwNoteEmpty.setVisibility(View.GONE);
        viewFilter = rootView.findViewById(R.id.layoutFilter);
        tvFilter = viewFilter.findViewById(R.id.tvFilter);
        ivFilter = viewFilter.findViewById(R.id.ivFilter);
        ivRefresh = viewFilter.findViewById(R.id.ivRefresh);
        ivFilter.setOnClickListener(this);
        ivHeaderFilter.setOnClickListener(this);
        ivHeaderRefresh.setOnClickListener(this);
        ivRefresh.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ivFilter) {
            showDialogFilter();
        } else if (view.getId() == R.id.ivRefresh) {
            doRefreshData();
        }
    }

    private void showDialogFilter() {
//        new BottomFilterStranger(mParentActivity, true).setListener(result -> {
//            drawTabFilter();
//            refreshAfterChooseFilter();
//        }).show();
        FilterFragment filterFragment = FilterFragment.newInstance(true);
        filterFragment.setListener(result -> {
            drawTabFilter();
            if (result) {
                mParentActivity.showLoadingDialog(null, R.string.waiting);
                mApplication.getMusicBusiness().clearStrangerHistory(new MusicBusiness.onCancelStrangeRoomListener() {
                    @Override
                    public void onResponse() {
//                        mParentActivity.hideLoadingDialog();
                        refreshAfterChooseFilter();
                        mParentActivity.showToast(R.string.delete_stranger_success);
                        doRefreshData();
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }, true);
            } else {
                refreshAfterChooseFilter();
            }
        });
        filterFragment.show(getChildFragmentManager(), "");
    }

    private void drawTabFilter() {
        String filter = filterHelper.getFormatFilterText(true);
        tvFilter.setText(filter);
        tvHeaderFilter.setText(filter);
    }


    @Override
    public void onPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onPermissionsResult: " + requestCode);
        if (mHandler != null && requestCode == Constants.PERMISSION.PERMISSION_REQUEST_LOCATION &&
                PermissionHelper.verifyPermissions(grantResults)) {
            processShowTabAround();
        } else {
            isDeclinedLocation = true;
        }
    }

    @Override
    public String getName() {
        return "NearYouFragment";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult");
        if (requestCode == Constants.ACTION.ACTION_SETTING_LOCATION) {
            if (LocationHelper.getInstant(mApplication).isNetworkLocationEnabled()) {
                if (mHandler != null) {
                    mHandler.post(this::initDataAndDrawDetail);
                }
            } else {
                isDeclinedLocation = true;
                Log.d(TAG, "onRequestActivityResult: " + LocationHelper.getInstant(mApplication)
                        .checkLocationProviders());
            }
        }
    }

    private void doRefreshData() {
        // dang load hoạc chua du lieu
        if (mRequestHelper == null || mSwipyRefreshLayout == null || mSwipyRefreshLayout.isRefreshing()) {
            return;
        }
        mSwipyRefreshLayout.setRefreshing(true);
        mRequestHelper.loadListStrangerAround("-1", false, false, this);
    }

    private String getStringMenuFilter(int optionId) {
        String msg;
        switch (optionId) {
            case Constants.MENU.STRANGER_FILTER_MALE_FEMALE:
                msg = mRes.getString(R.string.tab_contact);
                break;
            case Constants.MENU.STRANGER_FILTER_MALE:
                msg = mRes.getString(R.string.menu_man_wait);
                break;
            case Constants.MENU.STRANGER_FILTER_FEMALE:
                msg = mRes.getString(R.string.menu_woman_wait);
                break;
            case Constants.MENU.STRANGER_FILTER_STAR:
                msg = mRes.getString(R.string.menu_filter_star);
                break;
            default:
                msg = mRes.getString(R.string.tab_contact);
                break;
        }
        return msg;
    }

    private int firstVisibleItem;
    private int lastVisibleItem;
    private int totalItemCount;

    private void setGridViewListener() {
//        mSwipyRefreshLayout.setDirection(SwipyRefreshLayoutDirection.TOP);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(new RecyclerView
                .OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mLayoutManager == null) return;
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                totalItemCount = mLayoutManager.getItemCount();
                if (firstVisibleItem > 0) {
                    if (viewFilter.getVisibility() == View.GONE) {
                        Animation slide_down = AnimationUtils.loadAnimation(mApplication,
                                R.anim.slide_down);
                        viewFilter.startAnimation(slide_down);
                        viewFilter.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (viewFilter.getVisibility() == View.VISIBLE) {
                        viewFilter.setVisibility(View.GONE);
                    }
                }
                if (isReadyLoadMore) {
                    boolean needToLoad = (lastVisibleItem >= totalItemCount - 8) && (firstVisibleItem > 0);
                    if (!isLoadMore && needToLoad) {// dang ko load more
                        mLoadmoreFooterView.setVisibility(View.VISIBLE);
                        isLoadMore = true;
                        if (!handlerLoadmore.hasMessages(100)) {
                            handlerLoadmore.sendEmptyMessageDelayed(100, 1200);
                        }
                    }
                }
            }
        }));
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "onRefresh: direction --> " + direction.toString());
        if (!mApplication.isDataReady()) {
            mSwipyRefreshLayout.setRefreshing(false);
            return;
        }
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            mRequestHelper.loadListStrangerAround("-1", false, false, this);
        } else {
            onLoadMore();
        }
    }

    private void onLoadMore() {
        Log.d(TAG, "onLoadMore");
        // cua lay dc du lieu khong cho load more
        if (mStrangerMusics == null || mStrangerMusics.isEmpty()) {
            resetLoadMore();
        } else {
            StrangerMusic lastItem = mStrangerMusics.get(mStrangerMusics.size() - 1);
            mRequestHelper.loadListStrangerAround(lastItem.getLastGeoHash(), false, true, this);
        }
    }

    private void resetLoadMore() {
        mLoadmoreFooterView.setVisibility(View.GONE);
        isLoadMore = false;
    }

    @Override
    public void onRefreshDataResponse(ArrayList<StrangerMusic> strangerMusics) {
        Log.d(TAG, "onRefreshDataResponse");
        mParentActivity.hideLoadingDialog();
        mStrangerMusics = strangerMusics;
        mProgressLoading.setVisibility(View.GONE);
        mLayoutProgressPreLoad.setVisibility(View.GONE);
        notifyChangeAdapter();
        //mPullToRefreshFrameLayout.refreshComplete();
        mSwipyRefreshLayout.setRefreshing(false);
        isReadyLoadMore = true;
    }

    @Override
    public void onFilterDataError(int errorCode) {
        if (errorCode == -2) {
            mParentActivity.showToast(R.string.error_internet_disconnect);
        } else {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
        }
        onRefreshDataResponse(new ArrayList<>());
    }

    @Override
    public void onLoadMoreDataResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic> dataLoadMore) {
        Log.d(TAG, "onLoadMoreDataResponse");
        mSwipyRefreshLayout.setRefreshing(false);
        mStrangerMusics = strangerMusics;
        mProgressLoading.setVisibility(View.GONE);
        notifyChangeAdapter();
        resetLoadMore();
        /*if (dataLoadMore == null || dataLoadMore.isEmpty()) {
            mParentActivity.showToast(R.string.msg_loadmore_empty);
        }*/
    }

    @Override
    public void onError(int errorCode) {
        Log.d(TAG, "onResponseError");
        mParentActivity.hideLoadingDialog();
        mProgressLoading.setVisibility(View.GONE);
        mLayoutProgressPreLoad.setVisibility(View.GONE);
        mSwipyRefreshLayout.setRefreshing(false);
        checkShowNoteEmpty();
        resetLoadMore();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.STRANGER_FILTER_MALE:
                mRequestHelper.updateOptionFilter(Constants.MENU.STRANGER_FILTER_MALE);
                refreshAfterChooseFilter();
                break;
            case Constants.MENU.STRANGER_FILTER_FEMALE:
                mRequestHelper.updateOptionFilter(Constants.MENU.STRANGER_FILTER_FEMALE);
                refreshAfterChooseFilter();
                break;
            case Constants.MENU.STRANGER_FILTER_MALE_FEMALE:
                mRequestHelper.updateOptionFilter(Constants.MENU.STRANGER_FILTER_MALE_FEMALE);
                refreshAfterChooseFilter();
                break;
            case Constants.MENU.MENU_DELETE_STRANGER:
                mParentActivity.showLoadingDialog(null, R.string.waiting);
                mApplication.getMusicBusiness().clearStrangerHistory(new MusicBusiness.onCancelStrangeRoomListener() {
                    @Override
                    public void onResponse() {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(R.string.delete_stranger_success);
                        doRefreshData();
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }, true);
                break;
            case Constants.MENU.POPUP_OK_GG_PLAY_SERVICE:
                NavigateActivityHelper.navigateToPlayStore(mParentActivity, Constants.PACKET_NAME.GG_PLAY_SERVICE);
                break;
            case Constants.ACTION.ACTION_SETTING_LOCATION:
                try {
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                            Constants.ACTION.ACTION_SETTING_LOCATION);
                } catch (ActivityNotFoundException ex) {
                    Log.e(TAG, "Exception" + ex);
                }

                break;
            default:
                break;
        }
    }

    private void initDataAndDrawDetail() {
        isReadyLoadMore = false;
        mStrangerMusics = mRequestHelper.getStrangerArounds();
        if (mStrangerMusics != null && !mStrangerMusics.isEmpty()) {
            mProgressLoading.setVisibility(View.GONE);
            mLayoutProgressPreLoad.setVisibility(View.GONE);
            initAdapter();
        } else {
            mProgressLoading.setVisibility(View.VISIBLE);
        }
        mRequestHelper.loadListStrangerAround("-1", true, false, this);
    }

    @SuppressWarnings("deprecation")
    private void initAdapter() {
        if (mStrangerMusics == null) {
            mStrangerMusics = new ArrayList<>();
        }
        mLayoutManager = new LinearLayoutManager(mParentActivity, StaggeredGridLayoutManager.VERTICAL, false);
        mLayoutManager.setAutoMeasureEnabled(false);
        mAdapter = new NearYouAdapter(mParentActivity, mStrangerMusics, this);
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new LinearItemDecoration(ViewUtils.dpToPx(15), 1));
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        RecyclerViewUtils.setHeaderView(mRecyclerView, mLayoutHeader);
        RecyclerViewUtils.setFooterView(mRecyclerView, mLayoutFooter);
        checkShowNoteEmpty();
    }

    private void notifyChangeAdapter() {
        if (mAdapter == null) {
            initAdapter();
        } else {
            if (mStrangerMusics == null) {
                mStrangerMusics = new ArrayList<>();
            }
            mAdapter.setDatas(mStrangerMusics);
            mHeaderAndFooterAdapter.notifyDataSetChanged();
            checkShowNoteEmpty();
        }
    }

    private void checkShowNoteEmpty() {
        if (mProgressLoading == null) {
            mTvwNoteEmpty.setVisibility(View.GONE);
        } else if (mStrangerMusics == null || mStrangerMusics.isEmpty()) {
            if (mProgressLoading.getVisibility() == View.VISIBLE) {
                //chua load xong thi an di
                mTvwNoteEmpty.setVisibility(View.GONE);
            } else {
                mProgressLoading.setVisibility(View.INVISIBLE);
                mTvwNoteEmpty.setVisibility(View.VISIBLE);
                mTvwNoteEmpty.setText(mRes.getString(R.string.around_empty));
            }
        } else {
            mTvwNoteEmpty.setVisibility(View.GONE);
        }
    }

    private void processClickAvatarUser(String friendJid, String friendName, String lastAvatar,
                                        String status, String birthdayStr, int gender) {
        String myJid = mApplication.getReengAccountBusiness().getJidNumber();
        if (TextUtils.isEmpty(friendJid)) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
        } else if (myJid.equals(friendJid)) {
            NavigateActivityHelper.navigateToMyProfile(mParentActivity);
        } else {
            NavigateActivityHelper.navigateToFriendProfile(mApplication, mParentActivity,
                    friendJid, friendName, lastAvatar, status, gender);
        }
    }

    /**
     * follow room chat
     *
     * @param strangerMusic
     */
    private void processFollowStarRoom(StrangerMusic strangerMusic) {
        String starRoomId = strangerMusic.getPosterJid();
        if (TextUtils.isEmpty(starRoomId)) {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        String roomName = strangerMusic.getPosterName();
        String roomAvatar = strangerMusic.getPosterLastAvatar();
//        DeepLinkHelper.getInstance().handleFollowRoom(mApplication, mParentActivity,
//                starRoomId, roomName, roomAvatar);
    }

    private void refreshAfterChooseFilter() {
        mSwipyRefreshLayout.setRefreshing(true);
        mRequestHelper.loadListStrangerAround("-1", true, false, this);
    }

    /**
     * quanh day
     */
    private void processShowTabAround() {
        if (LocationHelper.getInstant(mApplication).checkGooglePlayService()) {
            if (!LocationHelper.getInstant(mApplication).isLocationServiceEnabled() &&
                    !LocationHelper.getInstant(mApplication).isExistCacheLocation()) {
                LocationHelper.getInstant(mApplication).showDialogSettingLocationProviders(mParentActivity, this);
            } else if (!LocationHelper.getInstant(mApplication).isNetworkLocationEnabled() &&
                    !LocationHelper.getInstant(mApplication).isExistCacheLocation()) {
                LocationHelper.getInstant(mApplication).showDialogSettingHighLocation(mParentActivity, this);
            } else {
                initDataAndDrawDetail();
            }
        } else {
            LocationHelper.getInstant(mApplication).showDialogGGPlayService(mParentActivity, this);
        }
        isAroundProcessed = true;
    }


    @Override
    public void filterChanged() {
        filterRecentChanged = true;
        if (getView() != null) {
            drawTabFilter();
        }
    }
}
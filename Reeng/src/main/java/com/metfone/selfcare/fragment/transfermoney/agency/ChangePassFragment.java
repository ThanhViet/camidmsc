package com.metfone.selfcare.fragment.transfermoney.agency;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 9/19/2019.
 */

public class ChangePassFragment extends Fragment {

    @BindView(R.id.ab_title)
    EllipsisTextView abTitle;
    @BindView(R.id.tilNewPass)
    TextInputLayout tilNewPass;
    @BindView(R.id.tilRePass)
    TextInputLayout tilRePass;
    Unbinder unbinder;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApp;
    private Resources mRes;

    EditText etNewPass, etRePass;


    public static ChangePassFragment newInstance() {
        ChangePassFragment fragment = new ChangePassFragment();
        /*Bundle args = new Bundle();
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        mApp = (ApplicationController) activity.getApplication();
        mRes = mApp.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_agency_password, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        abTitle.setText(mRes.getString(R.string.agency_change_pass));

        etNewPass = tilNewPass.getEditText();
        etRePass = tilRePass.getEditText();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ab_back_btn, R.id.btnVerify})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                activity.onBackPressed();
                break;
            case R.id.btnVerify:
                if (checkValid()) {
                    activity.showLoadingDialog("", R.string.loading);
                    AVNOHelper.getInstance(mApp).createSessionAgency("", AVNOHelper.TypeActionAgency.RESET_PASS,
                            etNewPass.getText().toString().trim(), "", "", new AVNOHelper.AgencyApiLister() {

                                @Override
                                public void onCreateSessionDone(String session) {
                                    super.onCreateSessionDone(session);
                                    activity.hideLoadingDialog();
                                    showDialogInputOtp(session);
                                }

                                @Override
                                public void onFail(int code, String msg) {
                                    activity.hideLoadingDialog();
                                    activity.showToast(msg);
                                }
                            });
                }
                break;
        }
    }

    private boolean checkValid() {
        String newPass = etNewPass.getText().toString().trim();
        String rePass = etRePass.getText().toString().trim();

        if(TextUtils.isEmpty(newPass)){
            activity.showToast(R.string.agency_pass_not_match);
            etNewPass.requestFocus();
            return false;
        }
        if(TextUtils.isEmpty(rePass)){
            activity.showToast(R.string.agency_pass_not_match);
            etRePass.requestFocus();
            return false;
        }

        if(!newPass.equals(rePass)){
            activity.showToast(R.string.agency_pass_not_match);
            etRePass.requestFocus();
            return false;
        }

        return true;
    }

    private void showDialogInputOtp(final String session) {
        DialogEditText dialog = new DialogEditText(activity, true)
                .setCheckEnable(true)
                .setSelectAll(true)
                .setLabel(mRes.getString(R.string.agency_reset_pass))
                .setMessage(mRes.getString(R.string.agency_reset_pass_msg))
                .setTextHint(mRes.getString(R.string.agency_input_otp))
                .setMaxLength(12)
                .setContentTop(true)
                .setNegativeLabel(mRes.getString(R.string.cancel))
                .setPositiveLabel(mRes.getString(R.string.agency_reset_pass_btn))
                .setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String result) {
                        result = result.trim();
                        activity.showLoadingDialog("", R.string.loading);
                        AVNOHelper.getInstance(mApp).changeOrResetPassAgency(session, result, new AVNOHelper.AgencyApiLister() {

                            @Override
                            public void onChangeResetPassDone(String msg) {
                                super.onChangeResetPassDone(msg);
                                activity.hideLoadingDialog();
                                activity.showToast(msg);
                                activity.onBackPressed();
                            }

                            @Override
                            public void onFail(int code, String msg) {
                                activity.showToast(msg);
                                activity.hideLoadingDialog();
                            }
                        });
                    }
                }).setDismissListener(new DismissListener() {
                    @Override
                    public void onDismiss() {
                        activity.hideKeyboard();
                    }
                });
        dialog.show();
    }
}

package com.metfone.selfcare.fragment.setting;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.BlockContactBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.BlockContactModel;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.ui.recyclerview.DividerItemDecoration;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.adapter.SettingListAdapter;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by nickgun on 8/12/14.
 */
public class BlockListFragment extends BaseSettingFragment implements
        ClickListener.IconListener, InitDataListener {
    private static final String TAG = BlockListFragment.class.getSimpleName();
    private ArrayList<String> mBlockNumberList;
    private ArrayList<PhoneNumber> mBlockPhoneNumberList;
    private BlockContactBusiness mBlockContactBusiness;
    private ContactBusiness contactBusiness;
    @BindView(R.id.recycler_view_setting)
    RecyclerView mRecyclerView;
    private SettingListAdapter settingListAdapter;
    private Resources mRes;
    private View viewEmpty;
    private boolean isUpdate = false;
    private Handler mHandler;

    public static BlockListFragment newInstance() {
        BlockListFragment fragment = new BlockListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mBlockContactBusiness = mApplication.getBlockContactBusiness();
        contactBusiness = mApplication.getContactBusiness();
        mRes = mApplication.getResources();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    protected void initView(View view) {
        setTitle(R.string.title_block_list);
        findComponentViews(view);
        drawBlockList();
        setListeners();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!isUpdate) mBlockNumberList = mBlockContactBusiness.getListBlock();
        if (mApplication.isDataReady() && mBlockNumberList != null) {
            mBlockPhoneNumberList = contactBusiness.getListPhoneNumberFormListNumber(mBlockNumberList);
            swapData(mBlockPhoneNumberList);
            isUpdate = false;
        }
    }

    private ArrayList<ItemContextMenu> initContextMenu(int pos) {
        ItemContextMenu deleteItem = new ItemContextMenu(
                mApplication.getResources().getString(R.string.delete), -1, pos, Constants.MENU.DELETE);
        ArrayList<ItemContextMenu> list = new ArrayList<>();
        list.add(deleteItem);
        return list;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        ListenerHelper.getInstance().addInitDataListener(this);
        mHandler = new Handler();
        Log.d(TAG, "OnResume");
        reDrawBlockList();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        ListenerHelper.getInstance().removeInitDataListener(this);
        super.onPause();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_recycler_view_setting_v5;
    }

    @Override
    public void onDataReady() {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mBlockNumberList = mBlockContactBusiness.getListBlock();
                    if (mBlockNumberList != null) {
                        mBlockPhoneNumberList = contactBusiness.getListPhoneNumberFormListNumber(mBlockNumberList);
                        swapData(mBlockPhoneNumberList);
                    }
                }
            });
        }
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        mHandler = null;
        super.onStop();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.DELETE:
                int pos = (int) entry;
                removeData(pos);
                break;
        }
    }

    public void removeData(final int position) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }

        final PhoneNumber phoneNumber = mBlockPhoneNumberList.get(position);
        mParentActivity.showLoadingDialog("", mRes.getString(R.string.processing), true);
        ArrayList<BlockContactModel> blockList = new ArrayList<>();
        blockList.add(new BlockContactModel(phoneNumber.getJidNumber(), BlockContactModel.STATE_UNBLOCK));
        ContactRequestHelper.getInstance(mApplication).handleBlockNumbers(blockList, new
                ContactRequestHelper.onResponseBlockContact() {
                    @Override
                    public void onResponse(String response) {
                        mParentActivity.hideLoadingDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                mBlockContactBusiness.removeBlockNumber(phoneNumber.getJidNumber());
                                mBlockNumberList.remove(phoneNumber.getJidNumber());
                                mBlockPhoneNumberList.remove(position);
                                settingListAdapter.removeItem(position);
                                if (mBlockPhoneNumberList.isEmpty()) {
                                    addViewEmpty();
                                }
                            } else {
                                String msg = mParentActivity.getResources().getString(R.string
                                        .e601_error_but_undefined);
                                mParentActivity.showError(msg, null);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception", e);
                            String msg = mParentActivity.getResources().getString(R.string.e601_error_but_undefined);
                            mParentActivity.showError(msg, null);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        String msg;
                        if (errorCode == -2) {
                            msg = mRes.getString(R.string.error_internet_disconnect);
                        } else {
                            msg = mRes.getString(R.string.e601_error_but_undefined);
                        }
                        mParentActivity.showError(msg, null);
                    }
                });
    }

    private void findComponentViews(View view) {
        InputMethodUtils.hideKeyboardWhenTouch(view, mParentActivity);
    }

    private void drawBlockList() {
        mBlockPhoneNumberList = new ArrayList<>();
        settingListAdapter = new SettingListAdapter(getActivity(), SettingListAdapter.SCREEN_BLOCK_USER);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mParentActivity, LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(settingListAdapter);
        settingListAdapter.setItemListener(new SettingListAdapter.ItemListener() {
            @Override
            public void removeItem(final int position) {
                DialogConfirm dialogConfirm = DialogConfirm.newInstance(
                        getString(R.string.unblock_user, mBlockPhoneNumberList.get(position).getName())
                        , getString(R.string.unblock_user_request, mBlockPhoneNumberList.get(position).getName())
                        , DialogConfirm.CONFIRM_TYPE, R.string.unblock, R.string.official_action_back);
                dialogConfirm.setSelectListener(
                        new BaseDialogFragment.DialogListener() {
                            @Override
                            public void dialogRightClick(int value) {
                            }

                            @Override
                            public void dialogLeftClick() {
                                removeData(position);
                            }
                        });
                dialogConfirm.show(getChildFragmentManager(), "");
            }

            @Override
            public void itemClick(int position) {
                openThreadDetail(mBlockPhoneNumberList.get(position).getJidNumber());
            }

            @Override
            public void avatarClickListener(int position) {

            }
        });

    }

    private void reDrawBlockList() {
        if (mBlockPhoneNumberList == null || !mBlockPhoneNumberList.isEmpty()) {
//            mTvwNoteView.setVisibility(View.GONE);
        } else {
//            mTvwNoteView.setVisibility(View.VISIBLE);
        }
    }

    private void setListeners() {
        setBlockButtonListener();
    }

    private void setBlockButtonListener() {
//        mButtonBlock.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mParentActivity.navigateToNumberList();
//            }
//        });
    }

    private void swapData(List<PhoneNumber> list) {
        settingListAdapter.swapList(list);
        if (list == null || list.isEmpty()) {
            addViewEmpty();
        } else {
            hideViewEmpty();
        }
    }

    private void setDataRecyclerView() {
//        mAdapter.setListPhoneNumbers(mBlockPhoneNumberList);
//        if (mBlockPhoneNumberList == null || !mBlockPhoneNumberList.isEmpty()) {
//            mTvwNoteView.setVisibility(View.GONE);
//        } else {
//            mTvwNoteView.setVisibility(View.VISIBLE);
//        }
    }

    public void updateBlockList(final ArrayList<BlockContactModel> phoneNumbers) {
        if (phoneNumbers == null || phoneNumbers.isEmpty()) return;
        isUpdate = true;
        mParentActivity.showLoadingDialog("", R.string.processing);
        ContactRequestHelper.getInstance(mApplication).handleBlockNumbers(phoneNumbers,
                new ContactRequestHelper.onResponseBlockContact() {
                    @Override
                    public void onResponse(String response) {
                        mParentActivity.hideLoadingDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                ArrayList<String> listBlockNew = new ArrayList<String>();
                                for (BlockContactModel blockContactModel : phoneNumbers) {
                                    if (blockContactModel.getStatus() == BlockContactModel.STATE_BLOCK) {
                                        listBlockNew.add(blockContactModel.getMsisdn());
                                    } else {
                                        mBlockContactBusiness.removeBlockNumber(blockContactModel.getMsisdn());
                                    }
                                }
                                mBlockContactBusiness.updateListAfterBlockNumbers(listBlockNew, false);
                                mBlockNumberList = mBlockContactBusiness.getListBlock();
                                mBlockPhoneNumberList = contactBusiness.getListPhoneNumberFormListNumber
                                        (mBlockNumberList);
                                if (settingListAdapter == null) {
                                    drawBlockList();
                                }
                                swapData(mBlockPhoneNumberList);
                            } else {
                                String msg = mParentActivity.getResources().getString(R.string
                                        .e601_error_but_undefined);
                                mParentActivity.showError(msg, null);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            String msg = mParentActivity.getResources().getString(R.string.e601_error_but_undefined);
                            mParentActivity.showError(msg, null);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        String msg;
                        if (errorCode == -2) {
                            msg = mParentActivity.getResources().getString(R.string.error_internet_disconnect);
                        } else {
                            msg = mParentActivity.getResources().getString(R.string.e601_error_but_undefined);
                        }
                        mParentActivity.showError(msg, null);
                    }
                });
    }

    private void openThreadDetail(String phoneNumber) {
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(phoneNumber);
        NavigateActivityHelper.navigateToChatDetail(mParentActivity, thread);
    }

    public ArrayList<String> getListPhoneNumberContactBlock() {
        ArrayList<String> listPhoneNumer = new ArrayList<>();
        for (PhoneNumber phoneNumber : mBlockPhoneNumberList) {
            if (!TextUtils.isEmpty(phoneNumber.getContactId()) && !TextUtils.isEmpty(phoneNumber.getId())) {
                listPhoneNumer.add(phoneNumber.getJidNumber());
            }
        }
        return listPhoneNumer;
    }

    private void addViewEmpty() {
        if (viewEmpty == null) {
            viewEmpty = LayoutInflater.from(getContext()).inflate(R.layout.layout_view_empty_setting_v5, null, false);
            CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            viewEmpty.setLayoutParams(layoutParams);
            // set image, text and description
            //
            AppCompatTextView tvTitleEmpty = viewEmpty.findViewById(R.id.tvEmptyTitle);
            tvTitleEmpty.setText(R.string.block_list_empty);
            AppCompatTextView tvDescription = viewEmpty.findViewById(R.id.tvEmptyDescription);
            tvDescription.setText(R.string.block_list_empty_des);
            ((ViewGroup) (getView())).addView(viewEmpty);
        }
//        AnimationUtil.animationScale((ViewGroup) getView(), viewEmpty);
        viewEmpty.setVisibility(View.VISIBLE);

    }

    private void hideViewEmpty() {
        if (viewEmpty != null) {
            viewEmpty.setVisibility(View.GONE);
        }
    }
}
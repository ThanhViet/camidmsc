package com.metfone.selfcare.fragment.changenumber;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.metfone.selfcare.activity.ChangeNumberActivity;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 11/14/2018.
 */

public class InputNumberFragment extends Fragment {

    @BindView(R.id.spCountryCode)
    Spinner spCountryCode;
    @BindView(R.id.edit_phone_number)
    EditText editPhoneNumber;
    Unbinder unbinder;
    @BindView(R.id.tvCurrentNumber)
    TextView tvCurrentNumber;
    private String inputNumber;
    private String regionCode;

    private ApplicationController mApp;
    private ChangeNumberActivity mActivity;
    private RegionSpinnerAdapter mRegionAdapter;
    private ArrayList<Region> mRegions;

    public static InputNumberFragment newInstance() {
        InputNumberFragment fragment = new InputNumberFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ChangeNumberActivity) getActivity();
        mApp = (ApplicationController) mActivity.getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_change_number, container, false);
        unbinder = ButterKnife.bind(this, view);
        setSpinnerView();
        setSpinnerItemSelectedListener();
        setListener();
        tvCurrentNumber.setText(mApp.getReengAccountBusiness().getJidNumber());
        return view;
    }

    private void setListener() {
        editPhoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    showDialogConfirmChangeNumber();
                }
                return false;
            }
        });
    }

    private void showDialogConfirmChangeNumber() {
        Resources mRes = mActivity.getResources();
        DialogConfirm dialogConfirm = new DialogConfirm(mActivity, true);
        dialogConfirm.setLabel(mRes.getString(R.string.title_popup_change_number));
        dialogConfirm.setMessage(mRes.getString(R.string.msg_popup_change_number));
        dialogConfirm.setUseHtml(true);
        dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
        dialogConfirm.setPositiveLabel(mRes.getString(R.string.confirm));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                mActivity.generateNewAuthCodeByServer(true);
            }
        });
        dialogConfirm.show();
    }

    private void setSpinnerView() {
        // set current default = vn
        mRegions = mApp.getLoginBusiness().getAllRegions();
        regionCode = mApp.getReengAccountBusiness().getRegionCode();
        int position = mApp.getLoginBusiness().getPositionRegion(regionCode);
        mRegionAdapter = new RegionSpinnerAdapter(mApp, mRegions);
        spCountryCode.setAdapter(mRegionAdapter);
        spCountryCode.setSelection(position);
    }

    private void setSpinnerItemSelectedListener() {
        spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                regionCode = mRegions.get(position).getRegionCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String getInputNumber() {
        inputNumber = editPhoneNumber.getText().toString().trim();
        return inputNumber;
    }

    public String getRegionCode() {
        return regionCode;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

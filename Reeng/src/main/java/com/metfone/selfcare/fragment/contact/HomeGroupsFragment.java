package com.metfone.selfcare.fragment.contact;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.ThreadListAdapterRecyclerView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.chat.GroupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactsTabInterface;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.listeners.ThreadListChangeListener;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.view.View.OVER_SCROLL_NEVER;

/**
 * Created by toanvk2 on 8/31/2017.
 */

public class HomeGroupsFragment extends BaseLoginAnonymousFragment implements InitDataListener,
        ConfigGroupListener,
        ThreadListChangeListener,
        ContactsTabInterface,
        BaseLoginAnonymousFragment.EmptyViewListener, ClickListener.IconListener,
        ReengMessageListener {
    private static final String TAG = HomeGroupsFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private HomeActivity mParentActivity;
    private Resources mRes;
    private RecyclerView mRecyclerView;
//    private HomeContactsAdapter mAdapter;

    //    private ArrayList<Object> mListGroups;
    private String contentEmpty;
    //    private InitListAsyncTask mInitListAsyncTask;
    //private SearchAsyncTask mSearchAsyncTask;
    private boolean isPageVisible = false;
//    private String searchContent;

    private ThreadListAdapterRecyclerView mThreadListAdapter;
    private GroupHelper groupHelper;
    private MessageBusiness mMessageBusiness;
    private ImageView mImgButtonShowKeyBoard;
    public static HomeGroupsFragment newInstance() {
        Log.i(TAG, "HomeContactsFragment newInstance ...");
        HomeGroupsFragment fragment = new HomeGroupsFragment();
        return fragment;
    }

    public HomeGroupsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (HomeActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mRes = mParentActivity.getResources();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            isPageVisible = savedInstanceState.getBoolean("PageStateVisible", false);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        findComponentViews(rootView, inflater);
//        showProgressLoading();
        hideEmptyView();
        mMessageBusiness = mApplication.getMessageBusiness();
        mImgButtonShowKeyBoard = rootView.findViewById(R.id.img_show_keyboard);
        mImgButtonShowKeyBoard.setVisibility(View.GONE);
        mThreadListAdapter = new ThreadListAdapterRecyclerView(mParentActivity,
                Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mRecyclerView.setOverScrollMode(OVER_SCROLL_NEVER);
        //mRecyclerViewMessage.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mThreadListAdapter);

        groupHelper = new GroupHelper(mApplication);

        mThreadListAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                ThreadMessage threadMessage = (ThreadMessage) object;
                NavigateActivityHelper.navigateToChatDetail(mParentActivity, threadMessage);
                /*ThreadGroup threadMessage = (ThreadGroup) object;
                ThreadMessage correctThread = mMessageBusiness.findGroupThreadByServerId(threadMessage.getServerId());
                if (correctThread != null) {
                    NavigateActivityHelper.navigateToChatDetail(mParentActivity, correctThread);
                } else {
                    if (NetworkHelper.isConnectInternet(mParentActivity)) {
                        if (mApplication.getXmppManager().isAuthenticated()) {
                            correctThread = mMessageBusiness.createNewGroupThread(threadMessage.getServerId());
                            NavigateActivityHelper.navigateToChatDetail(mParentActivity, correctThread);
                        } else {
                            mParentActivity.showToast(mRes.getString(R.string.e604_error_connect_server), Toast.LENGTH_LONG);
                        }
                    } else {
                        mParentActivity.showToast(mRes.getString(R.string.error_internet_disconnect), Toast.LENGTH_LONG);
                    }
                }*/

            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });

        if (mApplication.isDataReady()) {
//            mApplication.getMessageBusiness().addConfigGroupListener(this);
//            ListenerHelper.getInstance().addThreadsChangedListener(this);
            if (isPageVisible) {
                reloadDataAsyncTask();
            }
        }

        // [Start] CamID
//        initViewLogin(inflater, container, rootView);
        // [End] CamID
        // TODO: 5/22/2020 doMission  SmsOut
        return rootView;
    }

    @Override
    public void onStart() {
        ListenerHelper.getInstance().addInitDataListener(this);
        super.onStart();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
        mApplication.getMessageBusiness().removeReengMessageListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mApplication.isDataReady()) {
            mApplication.getMessageBusiness().addReengMessageListener(this);
            onLoadDataGroupDone();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("PageStateVisible", isPageVisible);
    }

    @Override
    public void onStop() {
        super.onStop();
        ListenerHelper.getInstance().removeInitDataListener(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /*if (mInitListAsyncTask != null) {
            mInitListAsyncTask.cancel(true);
            mInitListAsyncTask = null;
        }*/
        /*mApplication.getMessageBusiness().removeConfigGroupListener(this);
        ListenerHelper.getInstance().removeThreadsChangedListener(this);*/
    }

    @Override
    protected String getSourceClassName() {
        return "Groups";
    }

    @Override
    public void onDataReady() {
        /*mApplication.getMessageBusiness().addConfigGroupListener(this);
        ListenerHelper.getInstance().addThreadsChangedListener(this);*/
        mMessageBusiness.addReengMessageListener(this);
        if (isPageVisible) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    reloadDataAsyncTask();
                }
            });
        }
    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
        Log.d(TAG, "onConfigGroupChange");
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reloadDataAsyncTask();
            }
        });
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @Override
    public void onThreadsChanged() {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reloadDataAsyncTask();
            }
        });
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onPageStateVisible(boolean isVisible) {
        Log.d(TAG, "onPageStateVisible: " + isVisible);
        isPageVisible = isVisible;
        if (isPageVisible && mApplication != null && mApplication.isDataReady() && mParentActivity != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    reloadDataAsyncTask();
                }
            });
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        onPageStateVisible(isVisibleToUser);

        if (isVisibleToUser) {
            checkFirstLastVisible();
        }
    }

    private void checkFirstLastVisible() {
        if (mRecyclerView != null && mThreadListAdapter != null) {
            LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
            if (layoutManager == null) return;
            int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
            int findLastCompletelyVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition();

            if (findFirstCompletelyVisibleItemPosition == 0 && findLastCompletelyVisibleItemPosition
                    == (mThreadListAdapter.getItemCount() - 1)) {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(true));
                mRecyclerView.setNestedScrollingEnabled(false);
            } else {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(false));
                mRecyclerView.setNestedScrollingEnabled(true);
            }
        }
    }

    @Override
    public void onTabReselected() {
        if (mRecyclerView != null) {
            mRecyclerView.scrollToPosition(0);
        }
    }

    @Override
    public void onSearchChange(String textSearch) {
//        searchContent = textSearch;
    }

    private void findComponentViews(View rootView, LayoutInflater inflater) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        contentEmpty = mRes.getString(R.string.list_empty);
    }

    private void reloadDataAsyncTask() {
//        long lastTimeGetListGroup = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_LIST_GROUP, 0);
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) return;
        boolean getListGroupDone = mApplication.getPref().getBoolean(Constants.PREFERENCE.PREF_GET_LIST_GROUP_SUCCESS, false);
        if (!getListGroupDone) {
            Log.d(TAG, "reloadDataAsyncTask");
//            showProgressLoading();
            mParentActivity.showLoadingDialog("", R.string.loading);
            groupHelper.getListGroup(new GroupHelper.GetListGroupListener() {
                @Override
                public void onGetSuccess(ArrayList<ThreadMessage> listGroup) {
                    mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_GET_LIST_GROUP_SUCCESS, true).apply();
//                    hideEmptyView();
                    mParentActivity.hideLoadingDialog();
                    onLoadDataGroupDone();
                }

                @Override
                public void onGetError(int code, String msg) {
//                    showEmptyNote(mRes.getString(R.string.e601_error_but_undefined));
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(msg);
                    checkFirstLastVisible();
                }
            }, 0, 0);
        } else {
            onLoadDataGroupDone();
        }
    }

    private void onLoadDataGroupDone() {
        ArrayList<ThreadMessage> listThreads = mApplication.getMessageBusiness().getListThreadGroup(false);
        if (!listThreads.isEmpty()) {
            Collections.sort(listThreads, ComparatorHelper.getComparatorThreadMessageByLastTime());
        }
        mThreadListAdapter.setThreadList(new ArrayList<Object>(listThreads));
        mThreadListAdapter.notifyDataSetChanged();
        checkFirstLastVisible();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        //TODO onclick
    }

    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
//        mMessageBusiness.insertNewMessageToMemory(thread, message);
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                onLoadDataGroupDone();
            }
        });

    }

    @Override
    public void onRefreshMessage(int threadId) {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                onLoadDataGroupDone();
            }
        });
    }

    @Override
    public void notifyNewOutgoingMessage() {

    }

    @Override
    public void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {

    }

    @Override
    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {

    }

    @Override
    public void notifyMessageSentSuccessfully(int threadId) {

    }

    @Override
    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {

    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {

    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {

    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {

    }

    @Override
    public void onUpdateStateRoom() {

    }

    @Override
    public void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread) {

    }
}
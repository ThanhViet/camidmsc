package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.MainAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.ui.recyclerview.headerfooter.StrangerGridLayoutManager;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookMainFragment extends BaseRecyclerViewFragment implements
        View.OnClickListener,
        BaseRecyclerViewFragment.EmptyViewListener,
        ClickListener.IconListener {
    private static final String TAG = GuestBookMainFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private GuestBookHelper mGuestBookHelper;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private EllipsisTextView mAbTitle;
    private ImageView mAbAddOption;
    private OnFragmentInteractionListener mListener;
    private View mViewHeader;
    private LinearLayout mViewHeaderAddPage;
    private View mViewFooter;
    private RecyclerView mRecyclerView;
    private StrangerGridLayoutManager mLayoutManager;
    private MainAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private ArrayList<Object> listItems = null;
    private boolean requestBook;
    private boolean requestPage;
    private boolean showHeader;
    private boolean showFooter;
    private String myNumber;

    public GuestBookMainFragment() {

    }

    public static GuestBookMainFragment newInstance() {
        GuestBookMainFragment fragment = new GuestBookMainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        requestBook = true;
        requestPage = true;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        getDataAndDrawDetail();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        mGuestBookHelper.cancelRequest(GuestBookHelper.TAG_GET_BOOKS);
        mGuestBookHelper.cancelRequest(GuestBookHelper.TAG_GET_PAGE_ASSIGN);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        if (mGuestBookHelper.isNeedChangedPageData()) {
            mGuestBookHelper.setNeedChangedPageData(false);
            drawDataAfterRequest();
        }
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRetryClick() {

    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_guestbook_main, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        mAbAddOption = (ImageView) abView.findViewById(R.id.ab_add_action);
        mAbTitle.setText(mRes.getString(R.string.guest_book_title));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbAddOption.setVisibility(View.VISIBLE);
        mAbAddOption.setImageResource(R.drawable.ic_add_vote);
        mAbAddOption.setEnabled(false);
        mAbBack.setOnClickListener(this);
        mAbAddOption.setOnClickListener(this);
    }

    private void getData(Bundle savedInstanceState) {
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewHeader = inflater.inflate(R.layout.header_guest_book_main, container, false);
        mViewFooter = inflater.inflate(R.layout.footer_guest_book_main, container, false);
        mViewHeaderAddPage = (LinearLayout) mViewHeader.findViewById(R.id.header_guest_book_main_add_page);
        Button mBtnFooterInvite = (Button) mViewFooter.findViewById(R.id.footer_guest_book_invite_button);
        TextView mTvwFooterInvite = (TextView) mViewFooter.findViewById(R.id.footer_guest_book_invite_label);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        mBtnFooterInvite.setOnClickListener(this);
        mViewHeaderAddPage.setOnClickListener(this);
        mTvwFooterInvite.setText(mRes.getString(R.string.guest_book_footer_main));
        hideEmptyView();
        drawHeaderLayoutParams();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.ab_add_action:
                mListener.navigateToListTemplate();
                break;
            case R.id.footer_guest_book_invite_button:
                handleChooseFriendInvite();
                break;
            case R.id.header_guest_book_main_add_page:
                mListener.navigateToListTemplate();
                break;
        }
    }

    private void drawHeaderLayoutParams() {
        int screenWidth = mApplication.getWidthPixels();
        int padding = mRes.getDimensionPixelSize(R.dimen.space_height_item_source);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mViewHeaderAddPage.getLayoutParams();
        params.width = screenWidth / 3 - padding;
        params.height = (int) (params.width * com.android.editor.sticker.utils.Constants.SCALE_HEIGHT);
    }

    private void getDataAndDrawDetail() {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_HOME);
        Log.d(TAG, "getDataAndDrawDetail");
        listItems = new ArrayList<>();
        ArrayList<Book> myBooks = mGuestBookHelper.getMyBooks();
        ArrayList<Page> myPagesAssigned = mGuestBookHelper.getMyPagesAssigned();
        if ((myBooks == null || myBooks.isEmpty()) && (myPagesAssigned == null || myPagesAssigned.isEmpty())) {
            showHeader = true;
            showFooter = false;
            //listItems.add(mRes.getString(R.string.guest_book_main_divider));
        } else if (myBooks == null || myBooks.isEmpty()) {
            showHeader = true;
            showFooter = false;
            listItems.add(mRes.getString(R.string.guest_book_main_divider));
            listItems.addAll(myPagesAssigned);
        } else if (myPagesAssigned == null || myPagesAssigned.isEmpty()) {
            showHeader = false;
            showFooter = false;
            listItems.addAll(myBooks);
            listItems.add(new Book());// holder add
            //listItems.add(mRes.getString(R.string.guest_book_main_divider));
        } else {
            showHeader = false;
            showFooter = false;
            listItems.addAll(myBooks);
            listItems.add(new Book());// holder add
            listItems.add(mRes.getString(R.string.guest_book_main_divider));
            listItems.addAll(myPagesAssigned);
        }
        // vừa thêm mới quyển thì force hide header, chờ request get my books rồi hiển thị sau
        if (mGuestBookHelper.isNeedLoadMyBooks())
            showHeader = false;
        if (myBooks == null && myPagesAssigned == null) {
            showProgressLoading();
            mAbAddOption.setEnabled(false);
        } else if (mAdapter == null || mRecyclerView.getAdapter() == null) {
            mAbAddOption.setEnabled(true);
            hideEmptyView();
            setAdapter();
            showOrHideHeaderFooter();
        } else {
            mAbAddOption.setEnabled(true);
            hideEmptyView();
            changeAdapter();
            showOrHideHeaderFooter();
        }
        requestData();
    }

    private void drawDataAfterRequest() {
        Log.d(TAG, "drawDataAfterRequest");
        ArrayList<Object> items = new ArrayList<>();
        ArrayList<Book> myBooks = mGuestBookHelper.getMyBooks();
        ArrayList<Page> myPagesAssigned = mGuestBookHelper.getMyPagesAssigned();
        mAbAddOption.setEnabled(true);
        if ((myBooks == null || myBooks.isEmpty()) && (myPagesAssigned == null || myPagesAssigned.isEmpty())) {
            showHeader = true;
            showFooter = false;
            //items.add(mRes.getString(R.string.guest_book_main_divider));
        } else if (myBooks == null || myBooks.isEmpty()) {
            showHeader = true;
            showFooter = false;
            items.add(mRes.getString(R.string.guest_book_main_divider));
            items.addAll(myPagesAssigned);
        } else if (myPagesAssigned == null || myPagesAssigned.isEmpty()) {
            showHeader = false;
            showFooter = false;
            items.addAll(myBooks);
            items.add(new Book());// holder add
            //items.add(mRes.getString(R.string.guest_book_main_divider));
        } else {
            showHeader = false;
            showFooter = false;
            items.addAll(myBooks);
            items.add(new Book());// holder add
            items.add(mRes.getString(R.string.guest_book_main_divider));
            items.addAll(myPagesAssigned);
        }
        hideEmptyView();
        listItems = items;
        if (mAdapter == null || mHeaderAndFooterAdapter == null || mRecyclerView.getAdapter() == null) {
            setAdapter();
        } else {
            changeAdapter();
        }
        showOrHideHeaderFooter();
    }

    private void showOrHideHeaderFooter() {
        Log.d(TAG, "showOrHideHeaderFooter: " + showHeader + "----" + showFooter);
        if (showFooter) {
            RecyclerViewUtils.setFooterView(mRecyclerView, mViewFooter);
        } else {
            RecyclerViewUtils.removeFooterView(mRecyclerView);
        }
        if (showHeader) {
            RecyclerViewUtils.setHeaderView(mRecyclerView, mViewHeader);
        } else {
            RecyclerViewUtils.removeHeaderView(mRecyclerView);
        }
        mHeaderAndFooterAdapter.notifyDataSetChanged();
    }

    private void requestData() {
        if (mGuestBookHelper.isNeedLoadMyBooks() || requestBook) {
            requestBook = false;
            mGuestBookHelper.setNeedLoadMyBooks(false);
            mGuestBookHelper.requestGetListBooks(myNumber, true, getMyBooksListener);
        }
    }

    private void requestPageAssignedData() {
        if (requestPage) {
            mGuestBookHelper.requestGetMyPagesAssigned(getMyPagesAssignedListener);
            requestPage = false;
        }
    }

    private void setAdapter() {
        mLayoutManager = new StrangerGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setAutoMeasureEnabled(false);
        mAdapter = new MainAdapter(mApplication, listItems);
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        setItemListViewListener();
    }

    private void changeAdapter() {
        mAdapter.setListItems(listItems);
        mHeaderAndFooterAdapter.notifyDataSetChanged();
    }

    private void setItemListViewListener() {
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof Book) {
                    Book book = (Book) object;
                    if (book.isEmpty()) {
                        mListener.navigateToListTemplate();
                    } else {
                        getBookDetail(book);
                    }
                } else if (object instanceof Page) {
                    Page page = (Page) object;
                    page.setPosition(-2);
                    mListener.navigateToEditPage(page);
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {
                Log.d(TAG, "onLongClick: " + pos);
                if (object instanceof Book) {
                    Book book = (Book) object;
                    if (!book.isEmpty()) {
                        showContextMenu(book);
                    }
                }
            }
        });
        // long click
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        if (menuId == Constants.MENU.DELETE) {
            String title = mRes.getString(R.string.guest_book_title_delete);
            String btnYes = mRes.getString(R.string.ok);
            String btnCancel = mRes.getString(R.string.cancel);
            String msg = mRes.getString(R.string.guest_book_confirm_delete_book);
            PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                    title, msg, btnYes, btnCancel, this, entry, Constants.MENU.POPUP_GUEST_BOOK_DELETE_BOOK);
        } else if (menuId == Constants.MENU.POPUP_GUEST_BOOK_DELETE_BOOK) {
            handleDeleteBook((Book) entry);
        }
    }

    private void showContextMenu(Book book) {
        String title = null;
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu deleteItem = new ItemContextMenu(mRes.getString(R.string
                .guest_book_menu_delete), -1,
                book, Constants.MENU.DELETE);
        listMenu.add(deleteItem);
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mParentActivity,
                    title, listMenu, this);
        }
    }

    private void handleDeleteBook(final Book book) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestProcess(book.getId(), 0, Constants.GUEST_BOOK_STATUS.STATE_DELETE,
                Constants.GUEST_BOOK_SAVE.UPDATE_BOOK, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                        if (mGuestBookHelper.getMyBooks() != null) {
                            mGuestBookHelper.getMyBooks().remove(book);
                        }
                        drawDataAfterRequest();
                    }

                    @Override
                    public void onError(String error) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(error, Toast.LENGTH_LONG);
                    }
                });
    }

    private void getBookDetail(Book book) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        mGuestBookHelper.requestGetBookDetail(book.getId(), new GuestBookHelper.GetBookDetailListener() {
            @Override
            public void onSuccess(Book bookDetail) {
                // book.setPages(bookDetail.getPages());
                mParentActivity.hideLoadingDialog();
                mListener.navigateToBookDetail(bookDetail, false);
            }

            @Override
            public void onError(int error) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(R.string.request_send_error);
            }
        });
    }

    private void handleChooseFriendInvite() {
        Intent chooseFriend = new Intent(mParentActivity, ChooseContactActivity.class);
        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN);
        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN, Constants.GUEST_BOOK
                .INVITE_GUEST_BOOK_CAMPAIGN_ID);
        mParentActivity.startActivity(chooseFriend, true);
    }

    private GuestBookHelper.GetBooksListener getMyBooksListener = new GuestBookHelper.GetBooksListener() {
        @Override
        public void onSuccess(ArrayList<Book> books) {
            drawDataAfterRequest();
            requestPageAssignedData();
        }

        @Override
        public void onError(int error) {
            drawDataAfterRequest();
            requestPageAssignedData();
        }
    };

    private GuestBookHelper.GetDataListener getMyPagesAssignedListener = new GuestBookHelper.GetDataListener() {
        @Override
        public void onSuccess() {
            drawDataAfterRequest();
        }

        @Override
        public void onError(int error) {
            drawDataAfterRequest();
        }
    };

    public interface OnFragmentInteractionListener {
        void navigateToListTemplate();

        void navigateToBookDetail(Book book, boolean isNew);

        void navigateToEditPage(Page page);
    }
}
package com.metfone.selfcare.fragment.changenumber;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.ChangeNumberActivity;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 11/14/2018.
 */

public class IntroChangeNumberFragment extends Fragment {
    private static final String TAG = IntroChangeNumberFragment.class.getSimpleName();
    @BindView(R.id.tvBackUpChangeNumber)
    TextView tvBackUpChangeNumber;
    Unbinder unbinder;

    private ChangeNumberActivity mActivity;
    private Resources mRes;

    public static IntroChangeNumberFragment newInstance() {
        IntroChangeNumberFragment fragment = new IntroChangeNumberFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ChangeNumberActivity) getActivity();
        mRes = mActivity.getResources();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro_change_number, container, false);
        unbinder = ButterKnife.bind(this, view);
//        setTextBackup();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*private void setTextBackup() {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                ds.setUnderlineText(true);    // this remove the underline
            }

            @Override
            public void onClick(View textView) {
                DeepLinkHelper.getInstance().openSchemaLink(mActivity, "mocha://backup");
            }
        };
        String textNormal = mRes.getString(R.string.desc_change_number_3);
        String textToSpan = mRes.getString(R.string.desc_change_number_4);
        String textEnd = mRes.getString(R.string.desc_change_number_5);
        String textToShow = textNormal + textToSpan + textEnd;
        SpannableString spannableString = new SpannableString(textToShow);
        int START_TEXT = textNormal.length();
        int END_TEXT = START_TEXT + textToSpan.length();
        spannableString.setSpan(clickableSpan, START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.bg_mocha)),
                START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvBackUpChangeNumber.setText(spannableString);
        tvBackUpChangeNumber.setMovementMethod(LinkMovementMethod.getInstance());

    }*/
}

package com.metfone.selfcare.fragment.musickeeng;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.SearchSongActivity;
import com.metfone.selfcare.adapter.SongAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.SearchQuery;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.MoreAppInteractHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.SearchHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.MusicRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.restful.Groups;
import com.metfone.selfcare.restful.ResfulSearchQueryObj;
import com.metfone.selfcare.restful.RestTopModel;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.TooltipShowcase.SimpleTooltip;
import com.metfone.selfcare.ui.dialog.PermissionDialog;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.LinearItemDecoration;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 1/7/15.
 */
public class TopSongFragment extends Fragment implements
        ClickListener.IconListener {
    private static final String TAG = TopSongFragment.class.getSimpleName();
    private SearchSongActivity mParentActivity;
    private ApplicationController mApplication;
    private OnFragmentInteractionListener mListener;
    private MusicBusiness mMusicBusiness;
    private Handler mHandler;
    private ClickListener.IconListener mCallBack;
    private Resources mRes;
    private View rootView, abView /*mViewHeader*/;
//    private ImageView mImgLogoKeeng, mImgAbMore, mImgAbBack;

//    private FloatingActionButton mFabUpload;

//    private ReengSearchView mSearchView;
    private ProgressLoading mProgressLoading;
    private TextView mTvwNoteEmpty, mTvwNotFound;
    private RecyclerView mListView;
    private SongAdapter mAdapter;

    private ArrayList<MediaModel> mListTopSong;
    private ArrayList<SearchQuery> mListSearch;
    private ArrayList<Object> mListObject;
    private GetTopSongFromDBAsyncTask getTopSongAsynctask;
    private SharedPreferences mPref;
    private int preloadPageTopSong = 1;
    private boolean isLoadMoreReady = false, isNoMore = true, isLoading = false;

    private SimpleTooltip simpleTooltip;
    private long checkShowGuide;
    private String currentQuery;

    public TopSongFragment() {
        // Required empty public constructor
    }

    public static TopSongFragment newInstance() {
        TopSongFragment fragment = new TopSongFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (SearchSongActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        mRes = mParentActivity.getResources();
        mMusicBusiness = mApplication.getMusicBusiness();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_top_song, container, false);
        findComponentViews(rootView, inflater);
        initDataAndView();
        setViewListeners();

        checkShowGuide = mPref.getLong(Constants.INTRO.MUSIC_OFFLINE, Constants.INTRO.INTRO_NOT_SHOW_YET);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged ");
        super.onConfigurationChanged(newConfig);
        if (simpleTooltip != null && simpleTooltip.isShowing()) {
            simpleTooltip.dismiss(mParentActivity);
        }
    }

    @Override
    public void onStart() {
        mHandler = new Handler();
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "OnResume");
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mCallBack = this;
//        mSearchView.requestFocus();

        if (isEnableUpload() && Utilities.needGuideShow(checkShowGuide) && simpleTooltip == null) {
            SimpleTooltip.OnDismissListener onDismissListener = new SimpleTooltip.OnDismissListener() {
                @Override
                public void onDismiss(SimpleTooltip tooltip) {
                    checkShowGuide = System.currentTimeMillis();
                    mPref.edit().putLong(Constants.INTRO.MUSIC_OFFLINE, checkShowGuide).apply();
                    Log.i(TAG, "putlong dismiss");
                }
            };

//            String title = mRes.getString(R.string.intro_title_music_offline);
//            String text = mRes.getString(R.string.intro_music_offline);
//            simpleTooltip = new SimpleTooltip.Builder(mParentActivity)
//                    .anchorView(mFabUpload)
//                    .text(text)
//                    .gravity(Gravity.TOP)
//                    .transparentOverlay(false)
//                    .dismissOnOutsideTouch(true)
//                    .dismissOnInsideTouch(true)
//                    .onDismissListener(onDismissListener)
//                    .arrowColor(ContextCompat.getColor(mParentActivity, R.color.white))
//                    .contentView(R.layout.layout_tooltip_title, R.id.tvw_tooltip, R.id.tvw_title_tooltip)
//                    .textTitle(title)
//                    .build();


            /*simpleTooltip = new SimpleTooltip.Builder(mParentActivity)
                    .anchorView(mFloatBtnPasteLink)
                    .text(mRes.getString(R.string.intro_click_to_paste_link))
                    .gravity(Gravity.START)
                    .transparentOverlay(false)
                    .dismissOnOutsideTouch(true)
                    .dismissOnInsideTouch(true)
                    .arrowColor(ContextCompat.getColor(mParentActivity, R.color.white))
                    .contentView(R.layout.layout_tooltip_title, R.id.tvw_tooltip, R.id.tvw_title_tooltip)
                    .textTitle("")
                    .onDismissListener(onDismissListener)
                    .build();*/
//            simpleTooltip.show();
        }
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        mHandler = null;
//        InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
        if (getTopSongAsynctask != null) {
            getTopSongAsynctask.cancel(true);
            getTopSongAsynctask = null;
        }
        mCallBack = null;
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        mHandler = null;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_SEARCH_SONG");
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_GET_TOP_SONG");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void findComponentViews(View rootView, LayoutInflater inflater) {
//        abView = mParentActivity.getToolBarView();
//        mImgAbBack = abView.findViewById(R.id.ab_back_btn);
//        mSearchView = abView.findViewById(R.id.ab_search_view);
//        mImgAbMore = abView.findViewById(R.id.ab_more_btn);

        mListView = rootView.findViewById(R.id.search_song_listview);
        mListView.setLayoutManager(new LinearLayoutManager(mApplication, LinearLayoutManager.VERTICAL,
                false));
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mApplication, DividerItemDecoration.VERTICAL_LIST);
        mListView.addItemDecoration(new LinearItemDecoration(ViewUtils.dpToPx(15)));
        mAdapter = new SongAdapter(mParentActivity, mListObject, mCallBack);
        mListView.setAdapter(mAdapter);

        mProgressLoading = rootView.findViewById(R.id.search_song_progressbar);
//        mViewHeader = rootView.findViewById(R.id.search_song_header);
//        mTvwHeader = rootView.findViewById(R.id.search_song_header_text);
        mTvwNoteEmpty = rootView.findViewById(R.id.search_song_note_empty);
        mTvwNotFound = rootView.findViewById(R.id.search_song_note_not_found);
//        mImgLogoKeeng = rootView.findViewById(R.id.search_song_header_logo_keeng);
        //mListView.setFastScrollEnabled(false);
//        mSearchView.setHint(mRes.getString(R.string.search_song_hint));
//        mSearchView.setText("");
//        mImgAbMore.setVisibility(View.GONE);
        mProgressLoading.setVisibility(View.VISIBLE);

//        mFabUpload = rootView.findViewById(R.id.fab_home);
//        if (isEnableUpload()) {
//            mFabUpload.setVisibility(View.VISIBLE);
//        }
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
        //mListView.onLoadMoreComplete();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void initDataAndView() {
        Log.d(TAG, "initDataAndView");
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
        mListSearch = new ArrayList<>();
        // get top list from db
        if (getTopSongAsynctask != null) {
            getTopSongAsynctask.cancel(true);
            getTopSongAsynctask = null;
        }
        getTopSongAsynctask = new GetTopSongFromDBAsyncTask();
        getTopSongAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        /*if (menuId == Constants.ACTION.SEARCH_TRY_PLAY_SONG) {
            if (entry instanceof MediaModel) {
                MediaModel mediaModel = (MediaModel) entry;
                if (!TextUtils.isEmpty(mediaModel.getMedia_url()) && !TextUtils.isEmpty(mediaModel.getIcon())) {
                    tryPlaySong(mediaModel);
                } else {
                    getDetailInfoOfSongWhenClick(mediaModel.getId(), true);
                }
            } else if (entry instanceof SearchQuery) {
                SearchQuery searchQueryItem = (SearchQuery) entry;
                getDetailInfoOfSongWhenClick(searchQueryItem.getId(), true);
            }
        }*/
    }

    private void tryPlaySong(MediaModel model) {
        MoreAppInteractHelper.onPlayMediaClick(mParentActivity, model, mApplication, true);
    }

    private void setViewListeners() {
        setItemListViewListener();
        setButtonBackListener();
        setReengSearchViewListener();
        setLogoKeengListener();
        setFabListener();
        setListViewLoadMoreListener();
        // setListViewLoadMoreListener();
    }

    private void setFabListener() {
//        mFabUpload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (PermissionHelper.fragmentCheckPermissionAndShowDialogPermission(TopSongFragment.this,
//                        Constants.PERMISSION.PERMISSION_REQUEST_FILE,
//                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
//                    navigateToUploadSong();
//                }
//            }
//        });
    }

    private void navigateToUploadSong() {
        mParentActivity.navigateToUploadSong(true);
        if (checkShowGuide != Constants.INTRO.INTRO_HAS_FINISH_SHOW) {
            checkShowGuide = Constants.INTRO.INTRO_HAS_FINISH_SHOW;
            mPref.edit().putLong(Constants.INTRO.MUSIC_OFFLINE, checkShowGuide).apply();
            Log.i(TAG, "putlong finish");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionDialog.dismissDialogPermission(getFragmentManager());
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_FILE) {
            if (PermissionHelper.allowedPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    && PermissionHelper.allowedPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                navigateToUploadSong();
                mApplication.initFolder();
            }
        }
    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void setListViewLoadMoreListener() {
        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mListView.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !isNoMore) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }
            }
        };
        mListView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));
    }

    private void onLoadMore() {
        if (mApplication.getReengAccountBusiness().isTimorLeste()) {
            if (isNoMore || !isLoadMoreReady
//                    || mSearchView == null ||
//                    !TextUtils.isEmpty(mSearchView.getText().toString())
            ) {
                isLoading = false;
            } else {
                int page = preloadPageTopSong + 1;
                getListTopSong(page);
            }
        } else {
            isLoading = false;
        }
    }

    private void setButtonBackListener() {
//        mImgAbBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
//                mParentActivity.onBackPressed();
//            }
//        });
    }

    /**
     * search Contact
     */
    private void setReengSearchViewListener() {
//        mSearchView.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String query = SearchHelper.convertQuery(s.toString());
//                actionSearchOnlineSuggest(query);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                //String content = s.toString();
////                searchContactAsynctask(content);
//            }
//        });
//
//        mSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
//                if (keyCode == EditorInfo.IME_ACTION_DONE) {
//                    InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
//                }
//                return false;
//            }
//        });
//        mSearchView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                // assus zenphone
//                //mSearchView.clearFocus();
//                InputMethodUtils.showSoftKeyboard(mParentActivity, mSearchView);
//                return false;
//            }
//        });
    }

    private void setItemListViewListener() {
        // on touch
        mListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodUtils.hideSoftKeyboard(mParentActivity);
                return false;
            }
        });
        // onclick

        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (mListener == null)
                    return;
                if (!mApplication.getXmppManager().isAuthenticated()) {
                    mParentActivity.showToast(R.string.connect_server_error);
                    return; //ko ket noi xmpp
                }
                Object item = mListObject.get(pos);
                if (item instanceof MediaModel) {
                    MediaModel mediaModel = (MediaModel) item;
                    if (!TextUtils.isEmpty(mediaModel.getMedia_url()) && !TextUtils.isEmpty(mediaModel.getImage())) {
                        Log.i(TAG, "found song --> don't need to request");
                        //mMusicBusiness.insertNewMediaModel(mediaModel);
                        processResultSong(mediaModel);
                        mParentActivity.trackingEvent(R.string.ga_category_top_together_listening, mediaModel.getName(),
                                String.format(mRes.getString(R.string.ga_label_song_item), mediaModel.getId()));
                    } else {
                        getDetailInfoOfSongWhenClick(mediaModel.getIdInt(), mediaModel.getIdentify(), false);
                    }
                } else if (item instanceof SearchQuery) {
                    SearchQuery searchQueryItem = (SearchQuery) item;
                    getDetailInfoOfSongWhenClick(searchQueryItem.getId(), searchQueryItem.getIdentify(), false);
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });

    }

    private void setLogoKeengListener() {
//        mImgLogoKeeng.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String keengPackageId = MoreAppInteractHelper.getKeengPacketName(mApplication);
//                Log.d(TAG, "keengPackageId: " + keengPackageId);
//                if (!"-".equals(keengPackageId)) {
//                    mParentActivity.trackingEvent(R.string.ga_category_keeng,
//                            R.string.ga_action_interaction,
//                            mRes.getString(R.string.ga_label_click_logo_keeng) + keengPackageId);
//                    MoreAppInteractHelper.onKeengLogoClick(mParentActivity, mApplication, keengPackageId);
//                } else if (mApplication.getReengAccountBusiness().isTimorLeste()) {
//                    mParentActivity.trackingEvent(R.string.ga_category_keeng,
//                            R.string.ga_action_interaction,
//                            mRes.getString(R.string.ga_label_click_logo_keeng_web) + "http://beta.muzika.tl");
//                    mListener.navigateToWebView("http://beta.muzika.tl");
//                }
//            }
//        });
    }

    public void actionSearchOnlineSuggest(final String query) {
        currentQuery = query;
        mTvwNotFound.setVisibility(View.GONE);
        mTvwNoteEmpty.setVisibility(View.GONE);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_SEARCH_SONG");
        if (!TextUtils.isEmpty(query)) {
            mProgressLoading.setVisibility(View.VISIBLE);
            new KeengApi().getSearchSong(query, new Response.Listener<ResfulSearchQueryObj>() {
                @Override
                public void onResponse(ResfulSearchQueryObj resfulSearchQueryObj) {
                    handleSearchRespose(resfulSearchQueryObj, query);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "VolleyError", volleyError);
                    mProgressLoading.setVisibility(View.GONE);
                }
            });

        } else {
            mProgressLoading.setVisibility(View.GONE);
            updateTopSong(mListTopSong);
        }
    }

    private void handleSearchRespose(ResfulSearchQueryObj resfulSearchQueryObj, String query) {
        mProgressLoading.setVisibility(View.GONE);
        if (resfulSearchQueryObj.getGrouped() == null) {
            return;
        }
        if (resfulSearchQueryObj.getGrouped().getType() == null) {
            return;
        }
        if (resfulSearchQueryObj.getGrouped().getType().getGroups() == null) {
            return;
        }
        if (resfulSearchQueryObj.getGrouped().getType().getGroups().isEmpty() ||
                resfulSearchQueryObj.getGrouped().getType().getGroups().get(0) == null) {
            notifyAdapterSearchList(new ArrayList<SearchQuery>());
            return;
        }
        if (resfulSearchQueryObj.getGrouped().getType().getGroups().get(0).getDoclists() == null) {
            notifyAdapterSearchList(new ArrayList<SearchQuery>());
            return;
        }
        if (resfulSearchQueryObj.getGrouped().getType().getGroups().get(0).getDoclists()
                .getResSearchQuerys() == null) {
            notifyAdapterSearchList(new ArrayList<SearchQuery>());
            return;
        }

        ArrayList<Groups> groups = resfulSearchQueryObj.getGrouped()
                .getType().getGroups();
        ArrayList<SearchQuery> mDatasSong = groups.get(0).getDoclists().getResSearchQuerys();
        if (mDatasSong.isEmpty()) {
            return;
        }
        mListSearch.clear();
        mListSearch = SearchHelper.filterSearchData(query, mDatasSong);
        notifyAdapterSearchList(mListSearch);
    }

    private final int CP = 0; // Ban quyen cua Nha cung cap dich vu (Content Provider)

    private void notifyAdapterSearchList(ArrayList<SearchQuery> listSearch) {
        if (mHandler == null) {
            return;
        }
        if (listSearch == null) {
            mListObject = new ArrayList<>();
        } else {
            mListObject = new ArrayList<Object>(listSearch);
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter == null) {
                    mAdapter = new SongAdapter(mParentActivity, mListObject, mCallBack);
                    mListView.setAdapter(mAdapter);
                } else {
                    mAdapter.setListObject(mListObject);
                    mAdapter.notifyDataSetChanged();
                }
                mTvwNoteEmpty.setVisibility(View.GONE);
//                mViewHeader.setVisibility(View.GONE);
                if (mListObject == null || mListObject.isEmpty()) {
//                    mTvwHeader.setVisibility(View.GONE);
                    mTvwNotFound.setVisibility(View.VISIBLE);
                    mTvwNotFound.setText(mRes.getString(R.string.search_song_not_found));
                } else {
//                    mTvwHeader.setVisibility(View.GONE);
                    mTvwNotFound.setVisibility(View.GONE);
                }
            }
        });
    }

    private void notifyAdapterListTop(ArrayList<MediaModel> listTop) {
        if (mHandler == null) {
            return;
        }
        Log.d(TAG, "Flag1");
        if (listTop == null) {
            Log.d(TAG, "Flag2");
            mListObject = new ArrayList<>();
        } else {
            Log.d(TAG, "Flag3");
            mListObject = new ArrayList<Object>(listTop);
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter == null) {
                    Log.d(TAG, "Flag4");
                    mAdapter = new SongAdapter(mParentActivity, mListObject, mCallBack);
                    mListView.setAdapter(mAdapter);
                } else {
                    Log.d(TAG, "Flag5");
                    mListView.setAdapter(mAdapter);
                    mAdapter.setListObject(mListObject);
                    mAdapter.notifyDataSetChanged();
                }
                mTvwNotFound.setVisibility(View.GONE);
                if (mListObject == null || mListObject.isEmpty()) {
                    Log.d(TAG, "Flag6");
//                    mViewHeader.setVisibility(View.VISIBLE);
//                    mTvwHeader.setVisibility(View.GONE);
                    mTvwNoteEmpty.setVisibility(View.VISIBLE);
                    mTvwNoteEmpty.setText(mRes.getString(R.string.search_song_nodata));
                } else {
                    Log.d(TAG, "Flag7");
//                    mViewHeader.setVisibility(View.VISIBLE);
//                    mTvwHeader.setVisibility(View.VISIBLE);
//                    mTvwHeader.setText(mRes.getString(R.string.search_song_top));
                    mTvwNoteEmpty.setVisibility(View.GONE);
                }
            }
        });
    }

    // music - top song
    private void getListTopSong(final int page) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_GET_TOP_SONG");//remove
        // request truoc do
        new KeengApi().getTopSong(Constants.KEENG_MUSIC.NUM_TOP_SONG, page, new Response.Listener<RestTopModel>() {
            @Override
            public void onResponse(RestTopModel restTopModel) {
                Log.d(TAG, "getListTopSong:repone");
                if (restTopModel != null && restTopModel.getData() != null && !restTopModel.getData().isEmpty()) {
                    ArrayList<MediaModel> listAllModel = restTopModel.getData();
                    preloadPageTopSong = page;
                    isLoadMoreReady = true;
                    isNoMore = false;
                    if (page == 1) {
                        updateTopSong(listAllModel);
                    } else {
                        addToTopSong(listAllModel);
                        isLoading = false;
                    }
                    // cap nhat thoi gian get top song
                } else {
                    isNoMore = true;
                    isLoading = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
    }

    private void updateTopSong(ArrayList<MediaModel> listSong) {
        mProgressLoading.setVisibility(View.GONE);
        mListTopSong = listSong;
//        String textSearch = mSearchView.getText().toString();
//        // lan dau vao get thong tin nhac top, khi get xong kiem tra xem co dang search khong
        if (currentQuery == null || currentQuery.isEmpty()) {
            notifyAdapterListTop(mListTopSong);
        }
        mMusicBusiness.deleteTopSongFromDb();
        mMusicBusiness.insertListTopSongToDb(mListTopSong);
    }

    private void addToTopSong(ArrayList<MediaModel> listSong) {
        mProgressLoading.setVisibility(View.GONE);
        mListTopSong.addAll(listSong);
//        String textSearch = mSearchView.getText().toString();
        // lan dau vao get thong tin nhac top, khi get xong kiem tra xem co dang search khong
        if (currentQuery == null || currentQuery.isEmpty()) {
            notifyAdapterListTop(mListTopSong);
        }
    }

    /**
     * * lay chi tiet bai hat khi nguoi dung click
     *
     * @ram songId
     */
    public void getDetailInfoOfSongWhenClick(final long id, final String identify, final boolean isTryPlay) {
        //TODO lan nao search cung lay lai thong tin bai hat, khong check tren cache nữa
        MusicRequestHelper.getInstance(mApplication).getMediaDetail(id, identify, new MusicRequestHelper.GetSongListener() {
            @Override
            public void onResponse(MediaModel object) {
                if (mProgressLoading != null) {
                    mProgressLoading.setVisibility(View.GONE);
                }
                if (!object.isValid()) {
                    if (mParentActivity != null) {
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                } else {
                    if (isTryPlay) {
                        tryPlaySong(object);
                    } else {
                        processResultSong(object);
                    }
                }
            }

            @Override
            public void onError(String err) {
                if (mProgressLoading != null) {
                    mProgressLoading.setVisibility(View.GONE);
                }
                if (mParentActivity != null) {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }
        }, MusicRequestHelper.TAG_SONG);
        mProgressLoading.setVisibility(View.VISIBLE);
    }

    private void processResultSong(MediaModel songModel) {
        mListener.processResultSong(songModel);
    }

    public interface OnFragmentInteractionListener {
        void processResultSong(MediaModel songModel);

        void navigateToWebView(String url);
    }

    private class GetTopSongFromDBAsyncTask extends AsyncTask<Void, Void, ArrayList<MediaModel>> {
        @Override
        protected void onPreExecute() {
            mProgressLoading.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MediaModel> doInBackground(Void... voids) {
            return mMusicBusiness.getListTopSongFromDb();
        }

        @Override
        protected void onPostExecute(ArrayList<MediaModel> objects) {
            // neu co danh sach trong db thi hide progress
            if (objects != null && !objects.isEmpty()) {
                mProgressLoading.setVisibility(View.GONE);
            }
            // lay dc thi hien thi
            mListTopSong = objects;
            updateTopSong(mListTopSong);
            getListTopSong(1);
            super.onPostExecute(objects);
        }
    }

    private boolean isEnableUpload() {
        String uploadUrl = UrlConfigHelper.getInstance(mApplication).getUrlKeengByKey(Config.UrlKeengEnum
                .MEDIA_UPLOAD_SONG);
        return !"-".equals(uploadUrl);
    }
}
package com.metfone.selfcare.fragment.call;

import android.Manifest;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.CallHistoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CallHistoryHelper;
import com.metfone.selfcare.business.CallHistoryHelper.CallHistoryChangeListener;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.MoreAppInteractHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import static android.view.View.OVER_SCROLL_NEVER;
import static com.metfone.selfcare.helper.PermissionHelper.PERMISSION_GRANTED;

/**
 * Created by toanvk2 on 10/01/2016.
 */
public class CallHistoryFragment extends BaseLoginAnonymousFragment implements CallHistoryChangeListener,
        View.OnClickListener,
        InitDataListener,
        ClickListener.IconListener {
    private static final String TAG = CallHistoryFragment.class.getSimpleName();
    private BaseSlidingFragmentActivity mHomeActivity;
    private ApplicationController mApplication;
    private CallHistoryHelper mCallHistoryHelper;
    private Resources mRes;
    private Handler mHandler;
    private CallHistoryAdapter mAdapter;
    private ArrayList<CallHistory> mCallHistories;
    private ArrayList<Object> listItems = new ArrayList<>();
    private View rootView;
    private RecyclerView mRecyclerView;
    private TextView mTvwNoteEmpty;
    private ProgressLoading mPrgLoading;
    private RelativeLayout rlNoThreadRoot;

    private View mViewNoHistory;
    private boolean isNeedClickedCallButton;
    private boolean isAllowPermissionContact = true;

    public CallHistoryFragment() {
        // Required empty public constructor
    }

    public static CallHistoryFragment newInstance() {
        CallHistoryFragment fragment = new CallHistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mHomeActivity = (BaseSlidingFragmentActivity) activity;
        mApplication = (ApplicationController) mHomeActivity.getApplicationContext();
        mRes = mHomeActivity.getResources();
        mCallHistoryHelper = CallHistoryHelper.getInstance();
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_call_history, container, false);
        mHandler = new Handler();
        findComponentViews();
        ListenerHelper.getInstance().addInitDataListener(this);
        mCallHistoryHelper.addCallHistoryListener(this);
        getData();
        setAdapter();
        setListener();
        mHomeActivity.changeWhiteStatusBar();
        // [Start] CamID
//        initViewLogin(inflater, container, rootView);
        // [End] CamID
        return rootView;
    }

    @Override
    public void onResume() {
        displayNoHistoryView();
        super.onResume();
        if (!isAllowPermissionContact) {
            if (checkPermissionContact()) {
                mApplication.reLoadContactAfterPermissionsResult();
                isAllowPermissionContact = true;

            }
        }
        // continue action navigate
        if (isNeedClickedCallButton) {
            isNeedClickedCallButton = false;
            NavigateActivityHelper.navigateToContactList(mHomeActivity, Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION);
        }
    }

    private boolean checkPermissionContact() {
        if (getActivity() != null) {
            int readContact = PermissionHelper.checkPermissionStatus(getActivity(), Manifest.permission.READ_CONTACTS);
            int writeContact = PermissionHelper.checkPermissionStatus(getActivity(), Manifest.permission.WRITE_CONTACTS);
            return readContact == PERMISSION_GRANTED && writeContact == PERMISSION_GRANTED;
        }
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        mHandler = null;
//        isPagerVisible = false;
        ListenerHelper.getInstance().removeInitDataListener(this);
        mCallHistoryHelper.removeCallHistoryListener(this);
        super.onDestroyView();
    }

    @Override
    protected String getSourceClassName() {
        return "Call";
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onReady() { // call history ready
        mHandler.post(this::getData);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mAdapter != null) {
            mAdapter.setNameWidth(CallHistoryHelper.getInstance().getNameHistoryWidth());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDataChanged() {
        if (mHandler == null) return;
        mHandler.post(() -> {
            getData();
            mAdapter.notifyDataSetChanged();
            checkFirstLastVisible();
        });
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) return;
        mHandler.post(this::getData);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            checkFirstLastVisible();
            if (getView() != null) {
                if (mAdapter != null)// refresh lại thời gian khi visible
                    mAdapter.notifyDataSetChanged();
            }
        }
        Log.i(TAG, "setUserVisibleHint: " + isVisibleToUser);
    }

    private void checkFirstLastVisible() {
        if (mRecyclerView != null && mAdapter != null) {
            if (mCallHistories == null || mCallHistories.isEmpty()) {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(true));
                mRecyclerView.setNestedScrollingEnabled(false);
                return;
            }

            LinearLayoutManager layoutManager = ((LinearLayoutManager) mRecyclerView.getLayoutManager());
            if (layoutManager == null) return;
            int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
            int findLastCompletelyVisibleItemPosition = layoutManager.findLastCompletelyVisibleItemPosition();

            if ((findFirstCompletelyVisibleItemPosition == 0
                    && findLastCompletelyVisibleItemPosition == (mAdapter.getItemCount() - 1)) ||
                    findFirstCompletelyVisibleItemPosition == -1) {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(true));
                mRecyclerView.setNestedScrollingEnabled(false);
            } else {
                EventBus.getDefault().post(new TabMobileFragment.SetExpandAppBarLayoutEvent(false));
                mRecyclerView.setNestedScrollingEnabled(true);
            }
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        if (menuId == Constants.MENU.DELETE) {
            CallHistory history = (CallHistory) entry;
            mCallHistoryHelper.deleteCallHistory(history);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlNoThreadRoot:
                NavigateActivityHelper.navigateToContactList(mHomeActivity, Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION);
                break;
            case R.id.call_history_open_keyboard:
                KeyBoardDialog dialog = new KeyBoardDialog(mHomeActivity, "");
                if (dialog.getWindow() != null) {
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogKeyBoardAnimation;
                }
                dialog.show();
                break;
            case R.id.img_search_back:
                InputMethodUtils.hideSoftKeyboard(mHomeActivity);
                mHomeActivity.onBackPressed();
                break;
        }
    }


    private void findComponentViews() {
        mViewNoHistory = rootView.findViewById(R.id.call_history_no_history_layout);
        mRecyclerView = rootView.findViewById(R.id.call_history_recycler);
        mTvwNoteEmpty = rootView.findViewById(R.id.call_history_note_empty);
        mPrgLoading = rootView.findViewById(R.id.call_history_loading_progressbar);
        rlNoThreadRoot = rootView.findViewById(R.id.rlNoThreadRoot);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mHomeActivity);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void setListener() {
        rlNoThreadRoot.setOnClickListener(this);
    }

    private void setItemAdapterListener() {
        mAdapter.setHolderHistorylistener(new CallHistoryAdapter.HolderHistoryListener() {
            @Override
            public void onActionCall(CallHistory history) {
                mApplication.getCallBusiness().checkAndStartCall(mHomeActivity, history.getFriendNumber());
            }

            @Override
            public void onActionVideo(CallHistory history) {
                ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(history.getFriendNumber());
                mApplication.getCallBusiness().handleStartCall(threadMessage, mHomeActivity, false, true);
            }
        });
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof CallHistory) {
                    NavigateActivityHelper.navigateToCallLogDetail(mHomeActivity, (CallHistory) object);
                } else if (object instanceof BannerMocha) {
                    BannerMocha banner = (BannerMocha) object;
                    processClickBanner(banner);
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {
                showPopupContextMenu(object);
            }
        });
    }

    @SuppressWarnings("AccessStaticViaInstance")
    private void processClickBanner(BannerMocha banner) {
        int bannerType = banner.getType();
        String link = banner.getLink();
        String fakeMoConfirm = banner.getConfirm();
        if (TextUtils.isEmpty(link)) return;
        mHomeActivity.trackingEvent(mRes.getString(R.string.ga_category_banner),
                banner.getTitle(), mRes.getString(R.string.ga_label_banner_click));
        changeAdapter();
        if (bannerType == Constants.BANNER.TYPE_WEB_VIEW) {
            UrlConfigHelper.getInstance(mApplication).gotoWebViewOnMedia(mApplication, mHomeActivity, link);
        } else if (bannerType == Constants.BANNER.TYPE_FEATURES) {
            DeepLinkHelper.getInstance().openSchemaLink(mHomeActivity, link);
        } else if (bannerType == Constants.BANNER.TYPE_OPEN_APP) {
            MoreAppInteractHelper.openMoreApp(mHomeActivity, link);
        } else if (bannerType == Constants.BANNER.TYPE_FAKE_MO) {
            ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, mHomeActivity, fakeMoConfirm, link,
                    "inbox_banner");
        }
    }

    private void getData() {
        if (mCallHistoryHelper.isReady() && mApplication.isDataReady()) {
            mPrgLoading.setVisibility(View.GONE);
            mCallHistories = mCallHistoryHelper.getCallHistories();
            listItems.clear();
            listItems.addAll(mCallHistories);
            displayNoHistoryView();
        } else {
            mPrgLoading.setVisibility(View.VISIBLE);
            mCallHistoryHelper.loadAllData();
            mViewNoHistory.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        if (listItems == null) listItems = new ArrayList<>();
        mAdapter = new CallHistoryAdapter(mApplication, listItems);
        mAdapter.setNameWidth(CallHistoryHelper.getInstance().getNameHistoryWidth());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mHomeActivity));
        mRecyclerView.setOverScrollMode(OVER_SCROLL_NEVER);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        setItemAdapterListener();
    }

    private void changeAdapter() {
        if (mAdapter == null) {
            setAdapter();
        } else {
            /*if (!listItems.isEmpty() && mBanners != null && !mBanners.isEmpty()) {
                listItems.addAll(0, mBanners);
            }*/
            mAdapter.setNameWidth(CallHistoryHelper.getInstance().getNameHistoryWidth());
            mAdapter.setListData(listItems);
            mAdapter.notifyDataSetChanged();
            displayNoHistoryView();
        }
        checkFirstLastVisible();
    }

    private void displayNoHistoryView() {
        if (mCallHistories != null && !mCallHistories.isEmpty()) {
            mViewNoHistory.setVisibility(View.GONE);
        } else if (mPrgLoading.getVisibility() == View.VISIBLE) {
            mViewNoHistory.setVisibility(View.GONE);
        } else {
            mViewNoHistory.setVisibility(View.VISIBLE);
        }
        mTvwNoteEmpty.setVisibility(View.GONE);
    }

    private void showPopupContextMenu(Object object) {
        String title = null;
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        if (object instanceof CallHistory) {
            title = mApplication.getMessageBusiness().getFriendName(((CallHistory) object).getFriendNumber());
            ItemContextMenu deleteItem = new ItemContextMenu(mRes.getString(R.string.delete), -1,
                    object, Constants.MENU.DELETE);
            listMenu.add(deleteItem);
        } else if (object instanceof PhoneNumber) {
            return;
        }
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mHomeActivity,
                    title, listMenu, this);
        }
    }

    public void onTabReselected() {
        if (mRecyclerView != null) {
            if (Config.Extras.SMOOTH_SCROLL)
                mRecyclerView.smoothScrollToPosition(0);
            else
                mRecyclerView.smoothScrollToPosition(0);
        }
    }
}
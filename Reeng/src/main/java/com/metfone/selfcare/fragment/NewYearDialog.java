package com.metfone.selfcare.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.blankj.utilcode.util.ScreenUtils;
import com.metfone.esport.base.BaseDialogFragment;
import com.metfone.esport.common.Common;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.WsGetGiftResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsRedeemGiftTetResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class NewYearDialog extends BaseDialogFragment {
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.tvGeGift)
    TextView tvGeGift;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    private String phone;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.clGroup)
    ConstraintLayout clGroup;
    // 0:getgift; 1:back, 2:close
    private int type = 0;

    private boolean isShowKeyboard = false;
    private int currentHeigh;
    private int oldHeight;

    public static NewYearDialog newInstance(String phone) {
        NewYearDialog dialog = new NewYearDialog();
        dialog.phone = phone;
//        dialog.message = message;
//        dialog.onClickGetGiftListener = onClickGetGiftListener;
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        setCancelable(true);
    }

    @Override
    protected void initView(View rootView) {
        edtPhone.setText(phone == null ? "" : phone);
        setupUI(clGroup);
        clGroup.setOnClickListener(v -> {
            if (isShowKeyboard) {
                Common.hideKeyBoard(requireActivity(), edtPhone);
            } else {
                dismiss();
            }
        });

        clGroup.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            if (oldHeight == 0 && currentHeigh == 0) {
                oldHeight = clGroup.getHeight();
                currentHeigh = clGroup.getHeight();
            } else {
                oldHeight = currentHeigh;
                currentHeigh = clGroup.getHeight();
                if (oldHeight - currentHeigh > 0) {
                    isShowKeyboard = true;
                } else if (oldHeight - currentHeigh < 0) {
                    isShowKeyboard = false;
                }
            }
        });
    }

    @Override
    protected int getDialogWidth() {
        return ScreenUtils.getScreenWidth();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_dialog_new_year;
    }

    private void setupUI(View view) {
        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener((v, event) -> {
                Common.hideKeyBoard(requireActivity(), edtPhone);
                return false;
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    @Override
    public String getTAG() {
        return DialogFragment.class.getSimpleName();
    }

    @OnClick({R.id.tvGeGift})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvGeGift:
                if (type == 0) {
                    boolean isValid = validatePhone(edtPhone.getText().toString().trim());
                    if (isValid) {
                        phone = edtPhone.getText().toString().trim();
                        checkShowGetGift(phone);
                    }
                } else if (type == 1) {
                    type = 0;
                    edtPhone.setVisibility(View.VISIBLE);
                    tvMessage.setText(getString(R.string.your_phone_number));
                    tvGeGift.setText(getString(R.string.get_gift));
                } else {
                    dismiss();
                }
                break;
        }
    }

    private boolean validatePhone(String phone) {
        if (phone.isEmpty()) {
            showButtonBack(getString(R.string.please_enter_your_metfone_number));
            return false;
        }
        if (!Utilities.isMetfoneNumber(phone)) {
            showButtonBack(getString(R.string.error_this_function_gift_tet));
            return false;
        }
        return true;
    }

    private void showButtonBack(String msg) {
        type = 1;
        edtPhone.setVisibility(View.GONE);
        tvMessage.setText(msg);
        tvGeGift.setText(getString(R.string.back));
    }

    private void checkShowGetGift(String phone) {
        pbLoading.setVisibility(View.VISIBLE);
        new KhHomeClient().checkGetGiftTet(phone, DeviceUtils.getAndroidID(requireContext()),
                new MPApiCallback<WsGetGiftResponse>() {
                    @Override
                    public void onResponse(Response<WsGetGiftResponse> response) {
                        pbLoading.setVisibility(View.GONE);
                        if (response != null && response.body() != null
                                && response.body().getResult() != null
                                && response.body().getResult().getErrorCode().equals("0")
                                && response.body().getResult().getWsResponse() != null
                        ) {
                            WsGetGiftResponse.Response wsResponse = response.body().getResult().getWsResponse();
                            if (!wsResponse.isGetGift()) {
                                redeemGift(phone);
                            } else {
                                showButtonBack(getString(R.string.error_already_get_gift));
                            }
                        } else {
                            showButtonBack(getString(R.string.service_error));
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        pbLoading.setVisibility(View.GONE);
                        showButtonBack(getString(R.string.service_error));
                    }
                });
    }

    private void redeemGift(String isdn) {
        pbLoading.setVisibility(View.VISIBLE);
        new KhHomeClient().redeemLunarGift(isdn, new MPApiCallback<WsRedeemGiftTetResponse>() {
            @Override
            public void onResponse(Response<WsRedeemGiftTetResponse> response) {
                pbLoading.setVisibility(View.GONE);
                if (response != null && response.body() != null
                        && response.body().getResult() != null
                        && response.body().getResult().getWsResponse() != null
                ) {
                    String message = response.body().getResult().getWsResponse().getMessageCode();
                    if (response.body().getResult().getWsResponse().getErrorCode().equals("0")) {
                        type = 2;
                        edtPhone.setVisibility(View.GONE);
                        tvGeGift.setText(getString(R.string.close));
                        tvMessage.setText(message);
                        tvGeGift.setEnabled(true);
                    } else {
                        showButtonBack(message);
                    }
                } else {
                    showButtonBack(getString(R.string.service_error));
                }
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
                showButtonBack(getString(R.string.service_error));
            }
        });
    }
}

package com.metfone.selfcare.fragment.message;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.PreviewImageActivity;
import com.metfone.selfcare.adapter.AlbumProfileAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by thanhnt72 on 12/27/2017.
 */

public class GridImageThreadFragment extends BaseRecyclerViewFragment {

    private static final String TAG = GridImageThreadFragment.class.getSimpleName();

    private ApplicationController mApp;
    private PreviewImageActivity mActivity;
    private Resources mRes;

    private int threadId;
    private ThreadMessage mCurrentThread;
    private MessageBusiness mMessageBusiness;
    private RecyclerView mRecyclerView;
    private AlbumProfileAdapter mAdapter;
    private ArrayList<String> mListImage;
    private ArrayList<ReengMessage> mListMessageImage;
    private Handler mHandler;

    public GridImageThreadFragment() {
    }

    public static GridImageThreadFragment newInstance(int threadId) {
        GridImageThreadFragment fragment = new GridImageThreadFragment();
        Bundle args = new Bundle();
        args.putInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID, threadId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseSlidingFragmentActivity) {
            mActivity = (PreviewImageActivity) context;
            mApp = (ApplicationController) mActivity.getApplication();
            mRes = mActivity.getResources();
            mMessageBusiness = mApp.getMessageBusiness();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        showStatusBar();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_lst_image_view, container, false);
        getData(savedInstanceState);
        mActivity.changeWhiteStatusBar();
        setToolBar(inflater);
        findComponentViews(rootView, inflater, container);
        mHandler = new Handler();
        return rootView;
    }

    private void setToolBar(LayoutInflater inflater) {
        View abView = mActivity.getToolBarView();
        mActivity.setCustomViewToolBar(inflater.inflate(
                R.layout.ab_detail_no_action, null));
        ImageView mImgBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        View divider = (View) abView.findViewById(R.id.divider);
        TextView mTvwTitle = (TextView) abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(getString(R.string.thread_shared_image));
        divider.setVisibility(View.GONE);
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
//        ImageView mImgMore = (ImageView) abView.findViewById(R.id.ab_more_btn);
//        mImgMore.setVisibility(View.GONE);
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, null);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mApp, 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mAdapter = new AlbumProfileAdapter(mApp, false);
        mAdapter.setListImageString(mListImage);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
//                showImageDetail(pos);
                mActivity.displayPreviewImageThreadFragment(threadId, mListImage.get(pos), true);
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        if (mListImage == null || mListImage.isEmpty()) {
            showEmptyNote(mRes.getString(R.string.shared_image_empty));
        } else {
            mPrbLoading.setVisibility(View.GONE);
        }
    }

    private void showStatusBar() {
        if (mHandler == null) mHandler = new Handler();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT < 16) {
                    mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                } else {
                    View decorView = mActivity.getWindow().getDecorView();
                    // show the status bar.
                    int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                    decorView.setSystemUiVisibility(uiOptions);
                }
                mActivity.getToolBarView().setVisibility(View.VISIBLE);
                mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
                mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            }
        });

    }

    private void getData(Bundle savedInstanceState) {
        if (getArguments() != null) {
            threadId = getArguments().getInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID);
        } else if (savedInstanceState != null) {
            threadId = savedInstanceState.getInt(PreviewImageActivity.PARAM_THREAD_MESSAGE_ID);
        }
        mCurrentThread = mMessageBusiness.getThreadById(threadId);
        if (mCurrentThread == null) {
            mActivity.showToast(R.string.e601_error_but_undefined);
            mActivity.finish();
        }
        mListMessageImage = mMessageBusiness.getImageMessageOfThread(String.valueOf(threadId), false);
        mListImage = new ArrayList<>();
        for (ReengMessage message : mListMessageImage) {
            if (new File(message.getFilePath()).exists()) {
                mListImage.add(message.getFilePath());
            } else if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                mListImage.add(UrlConfigHelper.getInstance(mApp).getDomainImage() + message.getDirectLinkMedia());
            }
        }
        // room chat thi add them image tren mem
        if (mCurrentThread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && mCurrentThread
                .getAllMessages() != null) {
            for (ReengMessage mes : mCurrentThread.getAllMessages()) {
                if (mes.getMessageType() == ReengMessageConstant.MessageType.image &&
                        mes.getFilePath() != null && !mListImage.contains(mes.getFilePath())) {
                    if (new File(mes.getFilePath()).exists()) {
                        mListImage.add(0, mes.getFilePath());
                    } else if (!TextUtils.isEmpty(mes.getDirectLinkMedia())) {
                        mListImage.add(0, UrlConfigHelper.getInstance(mApp).getDomainImage() + mes.getDirectLinkMedia());
                    }
                }
            }
        }
        Collections.reverse(mListImage);
    }
}

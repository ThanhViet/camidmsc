package com.metfone.selfcare.fragment.game;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.adapter.AccumulatePagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogConvertSpointToVtPlus;
import com.metfone.selfcare.ui.dialog.DialogInputOtp;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.DividerItemDecoration;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by toanvk2 on 11/30/2016.
 */
public class AccumulatePagerFragment extends BaseRecyclerViewFragment implements AccumulatePointHelper.AccumulateDataChange,
        AccumulatePagerAdapter.ItemListener,
        BaseRecyclerViewFragment.EmptyViewListener {
    private static final String TAG = AccumulatePagerFragment.class.getSimpleName();
    private ListGamesActivity mParentActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private RecyclerView mRecyclerView;
    private AccumulatePagerAdapter mAdapter;
    private ArrayList<AccumulatePointItem> pointItems = new ArrayList<>();
    private int mType = AccumulatePointHelper.TAB_HISTORY;

    public AccumulatePagerFragment() {
    }

    public static AccumulatePagerFragment newInstance(int type) {
        AccumulatePagerFragment fragment = new AccumulatePagerFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.SETTINGS.DATA_FRAGMENT, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (ListGamesActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        AccumulatePointHelper.getInstance(mApplication).addAccumulateListener(this);
       /* try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        findViewComponent(inflater, rootView);
        drawDetail();
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.SETTINGS.DATA_FRAGMENT, mType);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        AccumulatePointHelper.getInstance(mApplication).removeAccumulateListener(this);
        super.onDetach();
    }

    @Override
    public void onAccumulateDataChanged(final boolean changeList) {
        if (!changeList) return;
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setAdapter();
            }
        });
    }

    @Override
    public void onDeepLinkClick(AccumulatePointItem item) {
        String schemaLink = item.getDeepLink();
        DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, schemaLink);
    }

    @Override
    public void onExchangeClick(final AccumulatePointItem item) {
        if (mType == AccumulatePointHelper.TAB_EXCHANGE) {
            if (item.getType() == AccumulatePointItem.TYPE_CONVERT_POINT_VTPLUS) {
                String point = AccumulatePointHelper.getInstance(mApplication).getTotalPoint();
                if ("0".equals(point)) {
                    mParentActivity.showToast(R.string.accumulate_no_spoint);
                    return;
                }
                DialogConvertSpointToVtPlus dialogConvert = new DialogConvertSpointToVtPlus(mParentActivity, true);
                dialogConvert.setMessage(String.format(mParentActivity.getString(R.string.accumulate_enter_spoint_des)
                        , NumberFormat.getNumberInstance(Locale.UK).format(Integer.valueOf(point)).replaceAll(",", ".")));
                dialogConvert.setLabel(mParentActivity.getString(R.string.accumulate_enter_spoint));
                dialogConvert.setNegativeLabel(mParentActivity.getString(R.string.accumulate_cancel));
                dialogConvert.setPositiveLabel(mParentActivity.getString(R.string.accumulate_exchange));
                dialogConvert.setPositiveListener(new PositiveListener<String>() {
                    @Override
                    public void onPositive(String result) {
                        onConvertSpointToViettelPlus(item, result, "", ""); // lần đầu click đổi chưa có otp
                    }
                });
                dialogConvert.show();
            } else {
                DialogConfirm dialogConfirm = new DialogConfirm(mParentActivity, true);
                dialogConfirm.setMessage(mRes.getString(R.string.msg_content_convert_point_to_gift));
                dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
                dialogConfirm.setPositiveLabel(mRes.getString(R.string.ok));
                dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        onProcessExchange(item);
                    }
                });
                dialogConfirm.show();
            }
        } else
            onProcessExchange(item);
    }

    private void onConvertSpointToViettelPlus(AccumulatePointItem item, String point, String otp, String transId) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        AccumulatePointHelper.getInstance(mApplication).convertSpointToViettelPlus(point, item, otp, transId, new AccumulatePointHelper.GiftPointListener() {
            @Override
            public void onSuccess(String desc) {
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                mParentActivity.hideLoadingDialog();
                int currentPoint = Integer.valueOf(AccumulatePointHelper.getInstance(mApplication).getViettelPlusPoint());
                int convertPoint = Integer.valueOf(point);
                int afterPoint = currentPoint + convertPoint;
                AccumulatePointHelper.getInstance(mApplication).setViettelPlusPoint(String.valueOf(afterPoint));

            }

            @Override
            public void onError(int errorCode, String desc) {
            }

            @Override
            public void onError(int errorCode, String desc, String tranId) {
                mParentActivity.hideLoadingDialog();
                if (errorCode == 202) {
                    if (TextUtils.isEmpty(tranId)) {
                        if (TextUtils.isEmpty(desc)) {
                            mParentActivity.showToast(R.string.accumulate_exchange_vt_fail);
                        } else {
                            mParentActivity.showToast(desc);
                        }
                    } else {
                        DialogInputOtp dialogInputOtp = new DialogInputOtp(mParentActivity, false) {
                            @Override
                            public void onSubmitBuyOtp(String otp) {
                                mParentActivity.showLoadingDialog("", R.string.loading);
                                onConvertSpointToViettelPlus(item, point, otp, tranId);
                                this.dismiss();
                            }

                            @Override
                            public void onCancel() {
                                this.dismiss();
                            }

                            @Override
                            public void onResend() {

                            }
                        };
                        String title = getString(R.string.confirm_register_viettel_plus);
                        if(!TextUtils.isEmpty(title)){
                            dialogInputOtp.setTitle(title);
                        }
                        if (TextUtils.isEmpty(otp)) {
                            dialogInputOtp.showContentText(mRes.getString(R.string.text_content_dialog_otp_viettel_plus));
                        } else {
                            // không phải lần đầu setContent của server
                            dialogInputOtp.showContentText(desc);
                        }
                        dialogInputOtp.setCanceledOnTouchOutside(false);
                        dialogInputOtp.setCancelable(false);
                        dialogInputOtp.show();
                    }
                } else if (!TextUtils.isEmpty(desc)) {
                    mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                } else {
                    mParentActivity.showToast(R.string.accumulate_exchange_vt_fail);
                }
            }
        });
    }

    private void onProcessExchange(AccumulatePointItem item) {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        AccumulatePointHelper.getInstance(mApplication).convertGiftPoint(item, new AccumulatePointHelper.GiftPointListener() {
            @Override
            public void onSuccess(String desc) {
                mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(int errorCode, String desc) {
                mParentActivity.hideLoadingDialog();
                if (!TextUtils.isEmpty(desc)) {
                    mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                } else {
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onError(int errorCode, String desc, String tranId) {

            }
        });
    }

    @Override
    public void onInstallAppClick(final AccumulatePointItem item) {
        if (item.getItemPoint() == AccumulatePointItem.ITEM_VTPLUS) {
            Utilities.openApp(mParentActivity, item.getDeepLink());
            return;
        }

        final AccumulatePointItem.ACTION action;
        if (AccumulatePointItem.MISSION_STATUS_NEW.equals(item.getMissionStatus())) {
            action = AccumulatePointItem.ACTION.REGISTER;
        } else if (AccumulatePointItem.MISSION_STATUS_REGISTED.equals(item.getMissionStatus())) {
            action = AccumulatePointItem.ACTION.CONFIRM;
        } else {
            mParentActivity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        if (action == AccumulatePointItem.ACTION.CONFIRM && !Utilities.isAppInstalled(mApplication, item.getAppId())) {
            NavigateActivityHelper.navigateToPlayStore(mParentActivity, item.getAppId());
            return;
        }

        /*ArrayList<String> listApp = new ArrayList<>();
        listApp.add(item.getAppId());
        String info = new Gson().toJson(listApp);*/
        ArrayList<String> info = new ArrayList<>();
        info.add(item.getAppId());
        AccumulatePointHelper.getInstance(mApplication).logAccumulate(action, info, new AccumulatePointHelper.LogAccumulateListener() {
            @Override
            public void onLogSuccess(String desc) {
                mParentActivity.showToast(desc);
                if (action == AccumulatePointItem.ACTION.REGISTER) {
                    item.setMissionStatus(AccumulatePointItem.MISSION_STATUS_REGISTED);
                    item.setLabelButton("Nhận điểm");  //hardcode text chỗ này
                    mAdapter.notifyDataSetChanged();
                    NavigateActivityHelper.navigateToPlayStore(mParentActivity, item.getAppId());
                }
            }

            @Override
            public void onLogError(int errorCode, String desc) {
                mParentActivity.showToast(desc);
            }
        });
    }

    @Override
    public void onRetryClick() {

    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mType = savedInstanceState.getInt(Constants.SETTINGS.DATA_FRAGMENT);
        } else if (getArguments() != null) {
            mType = getArguments().getInt(Constants.SETTINGS.DATA_FRAGMENT);
        }
    }

    private void findViewComponent(LayoutInflater inflater, View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }

    private void drawDetail() {
        if (AccumulatePointHelper.getInstance(mApplication).isReady()) {
            setAdapter();
        }
    }

    private void setAdapter() {
        Log.i(TAG, "setAdapter ---------------> " + mType);
        pointItems = AccumulatePointHelper.getInstance(mApplication).getAccumulatePointItem(mType);
        if (pointItems == null) pointItems = new ArrayList<>();
        if (mAdapter == null) {
            mAdapter = new AccumulatePagerAdapter(mApplication, pointItems);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
            mRecyclerView.addItemDecoration(new DividerItemDecoration(mApplication, LinearLayoutManager.VERTICAL));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);
            setItemListener();
        } else {
            mAdapter.setListItem(pointItems);
            mAdapter.notifyDataSetChanged();
        }
        if (pointItems.isEmpty()) {
            showEmptyNote(mRes.getString(R.string.list_empty));
        } else {
            hideEmptyView();
        }
    }

    private void setItemListener() {
        mAdapter.setListener(this);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }
}
package com.metfone.selfcare.fragment.contact;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.PhoneNumberAdapter;
import com.metfone.selfcare.adapter.PhoneNumberRecyclerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CallHistoryHelper;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.SectionCharecter;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.avno.PackageAVNODetailFragment;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.SimpleResponseListener;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.module.selfcare.event.SCRechargeEvent;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.indexable.IndexableRecyclerView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 10/3/14.
 */
public class ChooseSingleNumberFragment extends Fragment implements
        ContactChangeListener, InitDataListener, ClickListener.IconListener {
    private final String TAG = ChooseSingleNumberFragment.class.getSimpleName();
    private ApplicationController mApplication;

    private BaseSlidingFragmentActivity mParentActivity;

    private LayoutInflater mLayoutInflater;
    private Handler mHandler;
    private ClickListener.IconListener mClickCallBack;
    private Resources mRes;
    private OnFragmentInteractionListener mListener;
    private int actionType, mThreadType;
    //variable
    private PhoneNumberRecyclerAdapter mHeaderAndFooterAdapter;
    private PhoneNumberAdapter mAdapter;
    private IndexableRecyclerView mRecyclerView;
    private TextView mTvNote;
    private FrameLayout mFramChatMoreAvatar;
    private EllipsisTextView mTvwChatMoreNumber;
    private ProgressLoading mPbLoading;
    private String myNumber;
    private ReengSearchView mSearchView;
    private ArrayList<PhoneNumber> mListPhoneNumbers;
    private GetListPhoneNumberAsynctask mGetListAsynctask;
    private SearchContactTask mSearchTask;
    private View mViewActionBar, rootView, mViewFooter, mViewChatMore;
    private ArrayList<ItemContextMenu> listItemOverFlow;
    private ImageView mImgOverFlow, mImgChatMoreIcon, mImgBack;
    private TextView mTvwMessage;
    private Button mBtnInvite;
    private ReengAccountBusiness mAccountBusiness;
    private ContactBusiness mContactBusiness;
    private ArrayList<SectionCharecter> mListSectionChar;
    private MessageBusiness mMessageBusiness;
    private int mThreadId;
    private String guestBookId;
    private int guestBookPageId;
    private String packageIdAVNO;
    private String msg;

    /**
     * em thêm view này để ẩn hiện
     */
    private View rootSearch;

    public ChooseSingleNumberFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ChooseSingleNumberFragment newInstance(int actionType, int threadType, int threadId) {
        ChooseSingleNumberFragment fragment = new ChooseSingleNumberFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, threadType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadId);
        fragment.mThreadId = threadId;
        fragment.setArguments(args);
        return fragment;
    }

    public static ChooseSingleNumberFragment newInstance(int actionType, int threadType) {
        ChooseSingleNumberFragment fragment = new ChooseSingleNumberFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, threadType);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChooseSingleNumberFragment newInstance(int actionType) {
        ChooseSingleNumberFragment fragment = new ChooseSingleNumberFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChooseSingleNumberFragment newInstance(int actionType, String bookId, int pageId) {
        ChooseSingleNumberFragment fragment = new ChooseSingleNumberFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putString(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID, bookId);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID, pageId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChooseSingleNumberFragment newInstance(int actionType, String packageId, String msg) {
        ChooseSingleNumberFragment fragment = new ChooseSingleNumberFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putString(Constants.CHOOSE_CONTACT.DATA_PACKAGE_ID, packageId);
        args.putString(Constants.CHOOSE_CONTACT.DATA_MSG, msg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach :");
        mParentActivity = (BaseSlidingFragmentActivity) activity;
        mRes = mParentActivity.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        initOverFlowMenu();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_choose_number, container, false);
        findComponentViews(rootView, container);
        getData();
        setActionbarView();
        setListener();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            actionType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mThreadType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE);
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        mHandler = new Handler();
        mClickCallBack = this;
        ListenerHelper.getInstance().addInitDataListener(this);
        if (mApplication.isDataReady()) {
            ListenerHelper.getInstance().addContactChangeListener(this);
            getListPhoneAsynctask();
            Log.d(TAG, "isDataReady");
        } else {
            mPbLoading.setVisibility(View.VISIBLE);
            mPbLoading.setEnabled(true);
        }
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        mHandler = null;
        if (mGetListAsynctask != null) {
            mGetListAsynctask.cancel(true);
            mGetListAsynctask = null;
        }
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadType);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        mClickCallBack = null;
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) {
            return;
        }
        ListenerHelper.getInstance().addContactChangeListener(this);
        mHandler.post(() -> getListPhoneAsynctask());
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
        mHandler.post(() -> getListPhoneAsynctask());
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumber) {
        mHandler.post(() -> {
            if (mAdapter != null)
                mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.MENU_PRESENT:
                if (mListener != null) {
                    mListener.inviteFriend();
                }
                break;
            case Constants.MENU.POPUP_YES:
                PhoneNumber phone = (PhoneNumber) entry;
                mListener.returnResultShareContact(phone);
                break;
            case Constants.MENU.POPUP_GUEST_BOOK_ASSIGN:
                PhoneNumber phoneAssign = (PhoneNumber) entry;
                handleRequestAssign(phoneAssign);
                break;
            default:
                break;
        }
    }

    private void setActionbarView() {
        // setcustom view
    }

    private void findComponentViews(View rootView, ViewGroup container) {
        mLayoutInflater = (LayoutInflater) mParentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewActionBar = mParentActivity.getToolBarView();
        View content = mLayoutInflater.inflate(R.layout.ab_search_box, null);
        mParentActivity.setCustomViewToolBar(content);

        mViewFooter = mLayoutInflater.inflate(R.layout.button_layout, container, false);
        mImgBack = mViewActionBar.findViewById(R.id.ab_back_btn);
        mImgOverFlow = mViewActionBar.findViewById(R.id.ab_more_btn);
        mSearchView = mViewActionBar.findViewById(R.id.ab_search_view);
        mRecyclerView = rootView.findViewById(R.id.fragment_choose_number_listview);
        mRecyclerView.setHideFastScroll(true);
        mTvNote = rootView.findViewById(R.id.fragment_choose_number_note_text);
        mPbLoading = rootView.findViewById(R.id.fragment_choose_number_loading_progressbar);
        mBtnInvite = mViewFooter.findViewById(R.id.button_invite);
        // row chat more
        mViewChatMore = mLayoutInflater.inflate(R.layout.header_chat_more, container, false);
        mFramChatMoreAvatar = mViewChatMore.findViewById(R.id.chat_more_avatar_frame);
        //mImgChatMoreAvatar = (CircleImageView) mViewChatMore.findViewById(R.id.chat_more_avatar_img);
        mTvwChatMoreNumber = mViewChatMore.findViewById(R.id.chat_more_number_content);
        mImgChatMoreIcon = mViewChatMore.findViewById(R.id.chat_more_number_arrow_icon);
        mTvwMessage = rootView.findViewById(R.id.choose_number_broadcast_note);
        // ẩn view search
        rootSearch = rootView.findViewById(R.id.root_search);
        if (rootSearch != null)
            rootSearch.setVisibility(View.GONE);

        setVisibleChatMore(View.GONE);
        mTvNote.setVisibility(View.GONE);
//        mTvNote.setGravity(Gravity.CENTER);
        mPbLoading.setEnabled(false);
        mPbLoading.setVisibility(View.GONE);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
        mSearchView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSearchView.requestFocus();
            }
        }, 150);
        // mSearchView.requestFocus();
    }

    private void notifiChangeAdapter(ArrayList<PhoneNumber> phoneNumbers, boolean isSetList) {
        if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            mTvNote.setVisibility(View.VISIBLE);
        } else {
            mTvNote.setVisibility(View.GONE);
        }
        if (mAdapter == null) {
            mAdapter = new PhoneNumberAdapter(mApplication, phoneNumbers, null, Constants.CONTACT.CONTACT_VIEW_SHARE);
//            mAdapter.setSizeHeader(1);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mHeaderAndFooterAdapter = new PhoneNumberRecyclerAdapter(mAdapter, mApplication);
            mHeaderAndFooterAdapter.setListSectionCharecter(mListSectionChar);
            mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
            mHeaderAndFooterAdapter.addHeaderView(mViewChatMore);
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO)
                mHeaderAndFooterAdapter.addFooterView(mViewFooter);
            setItemListViewListener();
            return;
        }
        if (!isSetList) {
            mAdapter.notifyDataSetChanged();
        } else {
            mAdapter.setListPhoneNumbers(phoneNumbers);
            mHeaderAndFooterAdapter.setListSectionCharecter(mListSectionChar);
            mHeaderAndFooterAdapter.notifyDataSetChanged();
        }
        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        if (isSetList) {
            mRecyclerView.changeAdapter(mAdapter);
        }
    }

    private void getData() {
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        // lay danh sach ban be da co trong thread
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        myNumber = mAccountBusiness.getJidNumber();
        //ArrayList<String> listMember = new ArrayList<String>();
        if (getArguments() != null) {
            actionType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mThreadType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE);
            guestBookId = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID);
            guestBookPageId = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID);
            packageIdAVNO = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_PACKAGE_ID);
            msg = getArguments().getString(Constants.CHOOSE_CONTACT.DATA_MSG);
        }
        /*if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO && mRecyclerView.getFooterViewsCount() == 0) {
            mRecyclerView.addFooterView(mViewFooter);
        }*/
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO) {
            mTvNote.setText(mRes.getString(R.string.list_choose_empty));
        } else {
            mTvNote.setText(mRes.getString(R.string.not_find));
        }
        mImgOverFlow.setVisibility(View.GONE);
    }

    private void setListener() {
        setSearchViewListener();
        setButtonBackListener();
//        setButtonOverFlowListener();
        setButtonInviteListener();
    }

    private void setButtonInviteListener() {
        mBtnInvite.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.inviteFriend();
            }
        });
    }

    private void setButtonBackListener() {
        mImgBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }

    private void initOverFlowMenu() {
        listItemOverFlow = new ArrayList<>();
//        ItemContextMenu update = new ItemContextMenu(mRes, mRes.getString(R.string.sync),
//                -1, null, MessageConstants.MENU.MENU_CONTACT_SYNC);
        ItemContextMenu present = new ItemContextMenu(mRes.getString(R.string.present),
                -1, null, Constants.MENU.MENU_PRESENT);
        listItemOverFlow.add(present);
//        listItemOverFlow.add(update);
    }

    private void setItemListViewListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) object;
                    if (phone.getContactId() == null) {
                        return;
                    }
                    if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO) {
                        String number = phone.getJidNumber();
                        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                        if (number.equals(myNumber)) {
                            mParentActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                        } else if (phone.isReeng() || (mApplication.getReengAccountBusiness().isViettel() && phone.isViettel())) {
                            mListener.createThreadSoloMessage(phone.getJidNumber());
                        } else {
                            InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication,
                                    mParentActivity,
                                    phone.getName(), phone.getJidNumber(), false);
                        }
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT) {
                        showDialogShareContact(phone);
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED) {
                        showDialogAssignFriend(phone);
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_CALL_OUT_FREE_USER) {
                        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                        if (myNumber != null && myNumber.equals(phone.getJidNumber())) {
                            mParentActivity.showToast(R.string.msg_not_select_me);
                        } else if (mListener != null) {
                            mListener.returnResultShareContact(phone);
                        }
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE) {
                        if (mListener != null) {
                            mListener.returnResultAddFavorite(phone);
                        }
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SELECT_NUMBER_CALL_FREE_CALLOUT_GUIDE) {
                        showDialogChooseNumberCallFreeCalloutGuide(phone);
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLED_LIMITED
                            || actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLOUT_FREE) {
                        showDialogRegisterFreePackage(phone, packageIdAVNO);
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_TRANSFER_MONEY) {
                        //chon so thanh cong
                        mListener.createThreadSoloMessage(phone.getJidNumber());
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SELFCARE_RECHARGE) {
                        //TODO
                        mParentActivity.finish();
                        EventBus.getDefault().postSticky(new SCRechargeEvent(phone.getJidNumber()));
                    }
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private void showDialogRegisterFreePackage(final PhoneNumber phone, final String packageId) {
        InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
        String localMsg = "";
        if (TextUtils.isEmpty(msg)) {
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLED_LIMITED) {
                localMsg = String.format(mRes.getString(R.string.register_free_num_call),
                        phone.getJidNumber());
            } else
                localMsg = String.format(mRes.getString(R.string.call_subscription_confirm_choose_user),
                        phone.getJidNumber());
        } else {
            if (msg.contains("[msisdn]")) localMsg = msg.replace("[msisdn]", phone.getJidNumber());
            if (msg.contains("[name]")) localMsg = msg.replace("[name]", phone.getName());
        }

        String btnYes = mRes.getString(R.string.ok);
        String btnCancel = mRes.getString(R.string.cancel);
        DialogConfirm dialogConfirm = new DialogConfirm(mParentActivity, true);
        dialogConfirm.setLabel(null);
        dialogConfirm.setMessage(localMsg);
        dialogConfirm.setPositiveLabel(btnYes);
        dialogConfirm.setNegativeLabel(btnCancel);
        dialogConfirm.setPositiveListener(result -> {
            mParentActivity.showLoadingDialog("", R.string.loading);
            SimpleResponseListener listener = new SimpleResponseListener() {

                @Override
                public void onSuccess(String msg) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(msg, Toast.LENGTH_LONG);
                    mParentActivity.setResult(Activity.RESULT_OK);
                    mParentActivity.finish();
                    EventBus.getDefault().post(new AVNOActivity.AVNOManagerMessage());
                    EventBus.getDefault().post(new PackageAVNODetailFragment.AddNumberMessage(phone.getJidNumber()));
                }

                @Override
                public void onError(int code, String msg) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(msg, Toast.LENGTH_LONG);
                }
            };

            if (actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLED_LIMITED) {
                AVNOHelper.getInstance(mApplication).setCalledLimited(
                        packageId, phone.getJidNumber(), phone.getOperatorPresence(), listener);
            } else {
                AVNOHelper.getInstance(mApplication).registerFreeNumberPackage(phone,
                        packageId, listener);
            }

        });
        dialogConfirm.show();
    }

    private void showDialogChooseNumberCallFreeCalloutGuide(final PhoneNumber phone) {
        String msg = String.format(mRes.getString(R.string.call_subscription_confirm_choose_user), phone.getJidNumber
                ());
        String btnYes = mRes.getString(R.string.ok);
        String btnCancel = mRes.getString(R.string.cancel);
        DialogConfirm dialogConfirm = new DialogConfirm(mParentActivity, true);
        dialogConfirm.setLabel(null);
        dialogConfirm.setMessage(msg);
        dialogConfirm.setPositiveLabel(btnYes);
        dialogConfirm.setNegativeLabel(btnCancel);
        dialogConfirm.setPositiveListener(result -> {
            mParentActivity.showLoadingDialog("", R.string.loading);
            CallHistoryHelper.getInstance().requestSetCallOutFromCalloutGuide(phone.getJidNumber(),
                    phone.getName(),
                    DeepLinkHelper.DEEP_LINK.HOST_CALLOUT_GUIDE, new CallHistoryHelper.SetCallOutFreeUserListener
                            () {
                        @Override
                        public void onSuccess(String desc) {
                            mParentActivity.hideLoadingDialog();
                            mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                            mApplication.getCallBusiness().createCallOutAfterSelectNumberFree(mParentActivity,
                                    phone.getJidNumber());
                            mParentActivity.finish();
                        }

                        @Override
                        public void onError(String desc) {
                            mParentActivity.hideLoadingDialog();
                            mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                        }
                    });
        });
        dialogConfirm.show();
    }

    private void getListPhoneAsynctask() {
        if (mGetListAsynctask != null) {
            mGetListAsynctask.cancel(true);
            mGetListAsynctask = null;
        }
        mGetListAsynctask = new GetListPhoneNumberAsynctask();
        mGetListAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * search Contact
     */
    private void setSearchViewListener() {
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String content = s.toString();
                mRecyclerView.setHideFastScroll(!TextUtils.isEmpty(content));
                searchContactAsynctask(content);
            }
        });

        mSearchView.setOnEditorActionListener((textView, keyCode, keyEvent) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
            }
            return false;
        });
        mSearchView.setOnTouchListener((view, motionEvent) -> {
            // assus zenphone
            //mSearchView.clearFocus();
            InputMethodUtils.showSoftKeyboard(mParentActivity, mSearchView);
            return false;
        });
    }

    // new searchAsynctask
    private void searchContactAsynctask(String textSearch) {
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        if (mListPhoneNumbers == null) {
            return;
        }
        mSearchTask = new SearchContactTask(mApplication);
        mSearchTask.setDatas(mListPhoneNumbers);
        mSearchTask.setComparatorKeySearch(SearchUtils.getComparatorKeySearchForSearch());
        mSearchTask.setComparatorName(SearchUtils.getComparatorNameForSearch());
        mSearchTask.setComparatorContact(SearchUtils.getComparatorContactForSearch());
        mSearchTask.setComparatorMessage(SearchUtils.getComparatorMessageForSearch());
        mSearchTask.setListener(new SearchContactsListener() {
            @Override
            public void onPrepareSearchContact() {

            }

            @Override
            public void onFinishedSearchContact(String keySearch, ArrayList<PhoneNumber> list) {
                notifiChangeAdapter(list, true);
                showOrHideItemMoreNumber(keySearch, list);
            }
        });
        mSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
    }

    private void showOrHideItemMoreNumber(String textSearch, ArrayList<PhoneNumber> result) {
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO) {
            int countSpace = TextHelper.countSpace(textSearch);
            if (result == null || result.isEmpty()) {
                if ((countSpace == 0) && PhoneNumberHelper.getInstant().isValidNumberNotRemoveChar(textSearch)) {
                    mTvwChatMoreNumber.setText(String.format(mRes.getString(R.string.chat_more), textSearch));
                    setVisibleChatMore(View.VISIBLE);
                    mTvNote.setVisibility(View.GONE);
                } else {
                    setVisibleChatMore(View.GONE);
                    mTvNote.setVisibility(View.VISIBLE);
                }
            } else {
                setVisibleChatMore(View.GONE);
            }
        }
    }

    private void setVisibleChatMore(int visible) {
        mViewChatMore.setVisibility(visible);
        mFramChatMoreAvatar.setVisibility(visible);
        mTvwChatMoreNumber.setVisibility(visible);
        mImgChatMoreIcon.setVisibility(visible);
    }

    private void setViewInviteFriend() {
        if (mListPhoneNumbers == null || mListPhoneNumbers.isEmpty()) {
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO) {
                mTvNote.setText(mRes.getString(R.string.list_choose_empty));
            } else {
                mTvNote.setText(mRes.getString(R.string.list_empty));
            }
            mViewFooter.setVisibility(View.GONE);
            mBtnInvite.setVisibility(View.GONE);
//            mImgOverFlow.setEnabled(false);
//            mImgOverFlow.setImageResource(R.drawable.ic_action_overflow_dis);
        } else {
            mViewFooter.setVisibility(View.VISIBLE);
            mTvNote.setText(mRes.getString(R.string.not_find));
            mBtnInvite.setVisibility(View.VISIBLE);
//            mImgOverFlow.setEnabled(true);
//            mImgOverFlow.setImageResource(R.drawable.ic_action_overflow);
        }
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_SELECT_NUMBER_CALL_FREE_CALLOUT_GUIDE ||
                actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLOUT_FREE) {
            mTvwMessage.setVisibility(View.VISIBLE);
            mTvwMessage.setText(mRes.getString(R.string.msg_select_number_call_free));
        } else {
            mTvwMessage.setVisibility(View.GONE);
        }
    }

    private void showDialogShareContact(PhoneNumber phone) {
        String contactName = phone.getName();
        String msg = getShareContactNotifyMessage(contactName, mThreadId);
        String btnYes = mRes.getString(R.string.ok);
        String btnCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_share_contact);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                title, msg, btnYes, btnCancel, mClickCallBack, phone, Constants.MENU.POPUP_YES);
    }

    private void showDialogAssignFriend(PhoneNumber phone) {
        String title = mRes.getString(R.string.guest_book_title_assign);
        String btnYes = mRes.getString(R.string.ok);
        String btnCancel = mRes.getString(R.string.cancel);
        String msg = String.format(mRes.getString(R.string.guest_book_confirm_assign), phone.getName());
        PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                title, msg, btnYes, btnCancel, mClickCallBack, phone, Constants.MENU.POPUP_GUEST_BOOK_ASSIGN);
    }

    private void handleRequestAssign(final PhoneNumber phone) {
        // th number null tam thoi cung show toast nay :(
        if (phone.getJidNumber() == null || phone.getJidNumber().equals(mApplication.getReengAccountBusiness()
                .getJidNumber())) {
            mParentActivity.showToast(R.string.guest_book_alert_assigned_me);
        } else {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            GuestBookHelper.getInstance(mApplication).requestAssignFriend(guestBookId, guestBookPageId,
                    phone.getJidNumber(), new GuestBookHelper.UpdateStateListener() {
                        @Override
                        public void onSuccess() {
                            mParentActivity.hideLoadingDialog();
                            mListener.returnResultAssignedPage(phone);
                        }

                        @Override
                        public void onError(int error) {
                            mParentActivity.hideLoadingDialog();
                            mParentActivity.showToast(R.string.request_send_error);
                            mListener.returnResultAssignedPage(null);
                        }
                    });
        }
    }

    private String getShareContactNotifyMessage(String contactName, int threadId) {
        Log.i(TAG, "contactName " + contactName + " ; threadId " + threadId);
        ThreadMessage threadMessage = mMessageBusiness.getThreadById(threadId);
        return String.format(mRes.getString(R.string.msg_share_contact)
                , contactName, mApplication.getMessageBusiness().getThreadName(threadMessage));
    }

    ///
    public interface OnFragmentInteractionListener {
        void returnResultShareContact(PhoneNumber phone);

        void returnResultAssignedPage(PhoneNumber phone);

        void createThreadSoloMessage(String number);

//        public void forwardMessageToContact(String number);

        void returnResultAddFavorite(PhoneNumber phone);

        void inviteFriend();
    }

    private interface SearchContactsListener {
        void onPrepareSearchContact();

        void onFinishedSearchContact(String keySearch, ArrayList<PhoneNumber> list);
    }

    private static class SearchContactTask extends AsyncTask<String, Void, ArrayList<PhoneNumber>> {
        private final String TAG = "SearchContactTask";
        private WeakReference<ApplicationController> application;
        private SearchContactsListener listener;
        private String keySearch;
        private CopyOnWriteArrayList<PhoneNumber> datas;
        private Comparator comparatorName;
        private Comparator comparatorKeySearch;
        private Comparator comparatorContact;
        private Comparator comparatorMessage;
        private long startTime;
        private int totalDatas;

        public SearchContactTask(ApplicationController application) {
            this.application = new WeakReference<>(application);
            this.datas = new CopyOnWriteArrayList<>();
        }

        public void setListener(SearchContactsListener listener) {
            this.listener = listener;
        }

        public void setDatas(ArrayList<PhoneNumber> datas) {
            if (this.datas != null && datas != null)
                this.datas.addAll(datas);
        }

        public void setComparatorName(Comparator comparatorName) {
            this.comparatorName = comparatorName;
        }

        public void setComparatorKeySearch(Comparator comparatorKeySearch) {
            this.comparatorKeySearch = comparatorKeySearch;
        }

        public void setComparatorContact(Comparator comparatorContact) {
            this.comparatorContact = comparatorContact;
        }

        public void setComparatorMessage(Comparator comparatorMessage) {
            this.comparatorMessage = comparatorMessage;
        }

        @Override
        protected void onPreExecute() {
            if (listener != null) listener.onPrepareSearchContact();
        }

        @Override
        protected void onPostExecute(ArrayList<PhoneNumber> result) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                        + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
            if (listener != null) listener.onFinishedSearchContact(keySearch, result);
        }

        @Override
        protected ArrayList<PhoneNumber> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            totalDatas = 0;
            ArrayList<PhoneNumber> list = new ArrayList<>();
            keySearch = params[0];
            if (Utilities.notNull(application) && Utilities.notEmpty(datas)) {
                totalDatas = datas.size();
                ResultSearchContact result = SearchUtils.searchMessagesAndContacts(application.get()
                        , keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage, comparatorContact);
                if (result != null) {
                    keySearch = result.getKeySearch();
                    if (Utilities.notEmpty(result.getResult())) {
                        for (Object obj : result.getResult()) {
                            if (obj instanceof PhoneNumber) {
                                list.add((PhoneNumber) obj);
                            }
                        }
                    }
                }
            }
            return list;
        }
    }

    private class GetListPhoneNumberAsynctask extends AsyncTask<Void, Void, ArrayList<PhoneNumber>> {
        @Override
        protected void onPreExecute() {
            mPbLoading.setVisibility(View.VISIBLE);
            mPbLoading.setEnabled(true);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<PhoneNumber> doInBackground(Void... params) {
            if (mContactBusiness.getListContacs().isEmpty() || mContactBusiness.getListNumbers().isEmpty()) {
                return new ArrayList<>();
            }
            // chon contact
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO
                    || actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT
                    || actionType == Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT
                    || actionType == Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED
                    || actionType == Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_TRANSFER_MONEY
                    || actionType == Constants.CHOOSE_CONTACT.TYPE_SELFCARE_RECHARGE) {
                mListSectionChar = mContactBusiness.getListSectionCharAlls();
                return mContactBusiness.getListNumberAlls();
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_CALL_OUT_FREE_USER) {
                mListSectionChar = mContactBusiness.getListSectionCharByListPhone(mContactBusiness
                        .getListNumberViettel());
                return mContactBusiness.getListNumberViettel();
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SELECT_NUMBER_CALL_FREE_CALLOUT_GUIDE ||
                    actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLOUT_FREE) {
                mListSectionChar = mContactBusiness.getListSectionCharByListPhone(mContactBusiness
                        .getListNumberViettel());
                return mContactBusiness.getListNumberViettel();
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE) {
                mListSectionChar = mContactBusiness.getListSectionCharByListPhone(mContactBusiness.getListNonFavorite
                        ());
                return mContactBusiness.getListNonFavorite();
            } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SET_CALLED_LIMITED) {
                mListSectionChar = mContactBusiness.getListSectionCharByListPhone(mContactBusiness
                        .getListNumberViettel());
                return mContactBusiness.getListNumberViettel();
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(ArrayList<PhoneNumber> params) {
            mPbLoading.setVisibility(View.GONE);
            mPbLoading.setEnabled(false);
            mListPhoneNumbers = params;
            //kiem tra search box xem da nhap text chua
            setViewInviteFriend();
            String contentSearch = mSearchView.getText().toString().trim();
            if (!TextUtils.isEmpty(contentSearch)) {
                searchContactAsynctask(contentSearch);
            } else {
                notifiChangeAdapter(mListPhoneNumbers, true);
            }
            super.onPostExecute(params);
        }
    }
}
package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.ListLanguageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 8/11/2015.
 */
public class TranslationFragment extends Fragment {
    private static final String TAG = TranslationFragment.class.getSimpleName();
    private SettingActivity mParentActivity;
    private ApplicationController mApp;
    private Resources mRes;
    private ArrayList<Region> listLanguage = new ArrayList<>();
    private ListLanguageAdapter mLanguageAdapter;
    private ListView mLvLanguage;
    private String mCurrentLanguageCode;
    private SharedPreferences mPref;
    private int mCurrentPosition;

    public static TranslationFragment newInstance() {
        TranslationFragment fragment = new TranslationFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mParentActivity = (SettingActivity) activity;
        mApp = (ApplicationController) mParentActivity.getApplicationContext();
        mRes = mApp.getResources();
        mPref = mParentActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        String deviceLanguage = mApp.getReengAccountBusiness().getDeviceLanguage();
        mCurrentLanguageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
        super.onAttach(activity);
    }

    private View btnBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_setting_translation, container, false);
        btnBack = root.findViewById(R.id.iv_back);
        initComponents(root);
        getListLanguage();
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mParentActivity != null) mParentActivity.onBackPressed();
            }
        });
    }

    private void handleClickLayout(int i) {
        if (i != mCurrentPosition) {
            Log.i(TAG, "setOnItemClickListener: " + i);
            listLanguage.get(mCurrentPosition).setSelected(false);
            mCurrentLanguageCode = listLanguage.get(i).getRegionCode();
            listLanguage.get(i).setSelected(true);
            mCurrentPosition = i;
            mPref.edit().putString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, mCurrentLanguageCode).apply();
            mLanguageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initComponents(View view) {
        mLvLanguage = view.findViewById(R.id.list_language);
        TextView tvDesTrans = view.findViewById(R.id.tvDesTranslate);
        tvDesTrans.setMovementMethod(LinkMovementMethod.getInstance());
        setTextViewHTML(tvDesTrans, mRes.getString(R.string.des_translate));
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                String deeplink = span.getURL();
                DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, deeplink);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mApp, R.color.bg_mocha)),
                start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
    }

    private int getPositionLanguageCode(String languageCode) {
        for (int i = 0; i < listLanguage.size(); i++) {
            if (listLanguage.get(i).getRegionCode().equals(languageCode)) {
                return i;
            }
        }
        return -1;
    }

    public void getListLanguage() {
        String dataFromResource = mRes.getString(R.string.list_language_translate);
        Log.d(TAG, "response : " + dataFromResource);
        try {
            JSONObject object = new JSONObject(dataFromResource);
            //int code = -1;
            if (object.has("data")) {
                JSONObject dataObject = object.getJSONObject("data");
                if (dataObject.has("languages")) {
                    JSONArray arrayLanguage = dataObject.getJSONArray("languages");
                    if (arrayLanguage != null && arrayLanguage.length() > 0) {
                        for (int i = 0; i < arrayLanguage.length(); i++) {
                            JSONObject obj = arrayLanguage.getJSONObject(i);
                            if (obj.has("language") && obj.has("name")) {
                                String language = obj.getString("language");
                                String name = obj.getString("name");
                                listLanguage.add(new Region(language, name));
                            }
                        }
                    }
                    initData();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "JSONException", e);
        }
    }

    private void initData() {
        mCurrentPosition = getPositionLanguageCode(mCurrentLanguageCode);
        if (mCurrentPosition >= 0) {
            listLanguage.get(mCurrentPosition).setSelected(true);
        }

        ListLanguageAdapter.ListlanguageListener listener = this::handleClickLayout;
        mLanguageAdapter = new ListLanguageAdapter(mParentActivity, listLanguage, listener);
        mLvLanguage.setAdapter(mLanguageAdapter);
    }
}
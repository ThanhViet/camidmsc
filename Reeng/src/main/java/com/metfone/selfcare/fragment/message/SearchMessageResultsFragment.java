package com.metfone.selfcare.fragment.message;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.message.SearchMessageResultsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.ui.CusRelativeLayout;
import com.metfone.selfcare.ui.dialog.SearchMessageDialog;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class SearchMessageResultsFragment extends Fragment implements SearchMessageDialog.DialogResultListener, SearchMessageResultsAdapter.SearchMessageClickListener, AdapterView.OnItemClickListener {
    private static final String TAG = SearchMessageResultsFragment.class.getSimpleName();
    private CusRelativeLayout rootView;
    private ListView mLvwContent;
    private ThreadMessage mThreadMessage;
    private SearchMessageResultsAdapter mSearchAdapter;
    private ArrayList<ReengMessage> datas;
    private MessageBusiness mMessageBusiness;
    private ApplicationController mApplication;
    private int mThreadId, mThreadType;
    private int gaCategoryId;
    private String searchTag = "";
    private TextView mTvDataEmpty;
    private View mSearchListLayout;
    private TextView mTvSearchDescription;
    private OnFragmentInteractionListener mListener;
    private ArrayList<Integer> mAllMessage;

    public static SearchMessageResultsFragment newInstance(int threadId, int threadType, OnFragmentInteractionListener listener, ArrayList<Integer> list) {
        SearchMessageResultsFragment fragment = new SearchMessageResultsFragment();
        Bundle args = new Bundle();
        args.putInt(ThreadMessageConstant.THREAD_ID, threadId);
        args.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadType);
        fragment.setArguments(args);
        fragment.mListener = listener;
        fragment.mAllMessage = list;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        mApplication = (ApplicationController) getContext().getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        if (getArguments() != null) {
            mThreadId = getArguments().getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = getArguments().getInt(ThreadMessageConstant.THREAD_IS_GROUP);
        }
        //getFriendPhoneNumber();
        gaCategoryId = R.string.ga_category_chat_screen;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (CusRelativeLayout) inflater.inflate(R.layout.fragment_search_message_results, container, false);
        mLvwContent = (ListView) rootView.findViewById(R.id.person_chat_search_results);
        mSearchListLayout = rootView.findViewById(R.id.search_layout);
        mTvSearchDescription = (TextView) rootView.findViewById(R.id.tv_search_description);
        mTvDataEmpty = rootView.findViewById(R.id.tvDataEmpty);

        mSearchListLayout.setVisibility(View.GONE);
        mTvDataEmpty.setVisibility(View.GONE);
        setUp();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //showSearchDialog(true);
    }

    public void showSearchDialog(boolean cancelToFinish) {
        new SearchMessageDialog((BaseSlidingFragmentActivity) getActivity(), cancelToFinish, this).show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*public void getFriendPhoneNumber() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && mThreadMessage != null) {
            friendPhoneNumber = mThreadMessage.getSoloNumber();
        }
    }*/

    public void setUp() {
        if (mLvwContent != null) {
            mLvwContent.setOnItemClickListener(this);
        }
        if (mThreadMessage == null) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        }
        if (mThreadMessage == null) return;
        mSearchAdapter = new SearchMessageResultsAdapter(mApplication, this);
        datas = new ArrayList<>();
        mSearchAdapter.setData(datas);
        mSearchAdapter.setThreadMessage(mThreadMessage);
        mLvwContent.setAdapter(mSearchAdapter);
    }

    public void findMessgeByTag(String tag) {
        if (mThreadMessage == null || tag == null) return;
        if (datas == null) {
            datas = new ArrayList<>();
        }
        datas.clear();

        CopyOnWriteArrayList<ReengMessage> list = mMessageBusiness.searchMessagesByTag(mThreadId, tag);
        if (list != null && !list.isEmpty()) {
            for (ReengMessage message : list) {
                if (message != null && !TextUtils.isEmpty(message.getContent())) {
                    if (message.getContent().toLowerCase().contains(tag.toLowerCase())) {
                        datas.add(message);
                    }
                }
            }
        }

        setSearchResultsVisibility(datas.isEmpty());
        if (mSearchAdapter != null) {
            mSearchAdapter.notifyDataSetChanged();
        }
    }

    public void setSearchResultsVisibility(boolean isEmpty) {
        if (mSearchListLayout != null) {
            mSearchListLayout.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        }

        if (mTvDataEmpty != null) {
            mTvDataEmpty.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
            mTvDataEmpty.setText(getString(R.string.search_message_no_results, searchTag));
        }

        if (!isEmpty) {
            mTvSearchDescription.setText(getString(R.string.search_message_results_description, searchTag));
        }
    }

    @Override
    public void onDestroyView() {
        if (mSearchAdapter != null) {
            mSearchAdapter.onDestroy();
        }
        mListener = null;
        super.onDestroyView();
    }

    @Override
    public void onDialogResult(String tag) {
        if (mSearchAdapter != null) {
            mSearchAdapter.setSearchTag(tag);
            searchTag = tag;
            findMessgeByTag(tag);
        }
    }

    public void finishActivity() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void onFriendAvatarClick(String numberJid, String friendName) {
        if (mListener == null) return;
        Log.d(TAG, "onFriendAvatarClick:---numberJid-> " + numberJid + " friendName: " + friendName);
        PhoneNumber mPhoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberJid);
        if (mPhoneNumber != null) {
            mListener.navigateToContactDetailActivity(mPhoneNumber.getId());
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().
                    getExistStrangerPhoneNumberFromNumber(numberJid);
            if (strangerPhoneNumber != null) {
                mListener.navigateToStrangerDetail(strangerPhoneNumber, null, null);
            } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                mListener.navigateToNonContactDetailActiviy(numberJid);
            } else if (!TextUtils.isEmpty(numberJid) && !TextUtils.isEmpty(friendName)) {
                mListener.navigateToStrangerDetail(null, numberJid, friendName);
            } else {
                mListener.navigateToNonContactDetailActiviy(numberJid);
            }
        }
    }

    public void onMessageResultsClick(ReengMessage message) {
        if (mListener != null) {
            mListener.navigateToSearchDetailsFragment(searchTag, message);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (datas != null && position >= 0 && position < datas.size()) {
            onMessageResultsClick(datas.get(position));
        }
    }

    public interface OnFragmentInteractionListener {

        void navigateToSearchDetailsFragment(String searchTag, ReengMessage message);

        void navigateToThreadDetail(ThreadMessage threadMessage);

        void navigateToContactDetailActivity(String phoneId);

        void navigateToStrangerDetail(StrangerPhoneNumber strangerPhoneNumber,
                                      String friendJid, String friendName);

        void navigateToNonContactDetailActiviy(String phoneNumber);

        void navigateToOfficialDetail(String officialId);

        void addNewContact(String number, String name);

        void navigateToSendEmail(String email);

        void navigateToStatusMessageFragment(int threadId, int messageId, String abStatus);

        void navigateToChooseFriendsActivity(ArrayList<String> listNumberString, ThreadMessage threadMessage);

        void navigateToCall(String number);

        void navigateToViewLocation(ReengMessage message);

        void navigateToPollActivity(ThreadMessage threadMessage, ReengMessage reengMessage, String pollId, int type);
    }
}

package com.metfone.selfcare.fragment.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.ogaclejapan.smarttablayout.utils.v4.Bundler;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.BaseLoginAnonymousFragment;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayoutDirection;
import com.metfone.selfcare.ui.view.HeaderFrameLayout;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by thanhnt72 on 5/17/2019.
 */

public class WapHomeFragment extends BaseLoginAnonymousFragment {

    private static final String TAG = WapHomeFragment.class.getSimpleName();
    @SuppressLint("StaticFieldLeak")
    private static WapHomeFragment mSelf;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.webview_swipy_layout)
    SwipyRefreshLayout webviewSwipyLayout;
    @BindView(R.id.layout_progress)
    RelativeLayout layoutProgress;
    @BindView(R.id.rlParent)
    RelativeLayout rlParent;
    @BindView(R.id.bg_header)
    View bgHeader;
    @BindView(R.id.headerTop)
    HeaderFrameLayout headerTop;
    @BindView(R.id.statusBarHeight)
    View statusBarHeight;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.home_ab_title)
    TextView homeAbTitle;
    @BindView(R.id.headerController)
    LinearLayout headerController;
    @BindView(R.id.viewDivider)
    View viewDivider;
    @BindView(R.id.radius)
    View radius;
    Unbinder unbinder;
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.rlContentView)
    RelativeLayout rlContentView;
    @BindView(R.id.custom_view_container)
    FrameLayout customViewContainer;
    @BindView(R.id.fabBack)
    ImageView fabBack;
    @BindView(R.id.fabForward)
    ImageView fabForward;
    boolean hasLoadFirstUrl;
    private String id;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController app;
    private ConfigTabHomeItem tabWap;
    private boolean isVisible = false;
    private String currentUrl;
    private boolean needLoadOnCreate = false;           //dung cho deeplink
    private MyWebChromeClient mWebChromeClient = null;
    private boolean isShowingDialog;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;

    public static Fragment newInstance(String id) {
        WapHomeFragment fragment = new WapHomeFragment();
        fragment.setArguments(arguments(id));
        return fragment;
    }

    public static Bundle arguments(String id) {
        return new Bundler()
                .putString("id", id)
                .get();
    }

    public static WapHomeFragment self() {
        return mSelf;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseSlidingFragmentActivity) getActivity();
        app = (ApplicationController) activity.getApplication();
        if (getActivity() instanceof ModuleActivity)
            needLoadOnCreate = true;
        //marginRight = (int) mRes.getDimension(R.dimen.vip_margin);
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        mSelf = this;
        if (isVisible)
            webview.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        hideCustomView();
        webview.onPause();
        mSelf = null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint: " + isVisibleToUser);
        this.isVisible = isVisibleToUser;
        if (isVisibleToUser && getView() == null && getActivity() == null) {
            needLoadOnCreate = true;
            return;
        }
        if (isVisibleToUser && activity instanceof HomeActivity) {
            if (getView() == null)
                needLoadOnCreate = true;
            else {
//                ChangeABColorHelper.changeTabColor(bgHeader, TabHomeHelper.HomeTab.tab_wap, tabWap.getColor(),
//                        ((HomeActivity) activity).getCurrentTab(), ((HomeActivity) activity).getCurrentColorTabWap());
//                ((HomeActivity) activity).setColorTabWap(tabWap.getColor());
                bgHeader.setBackgroundResource(R.color.white);
                if (!hasLoadFirstUrl) {
                    loadUrl();
                    hasLoadFirstUrl = true;
                }
            }
        }

        if (webview != null) {
            if (isVisibleToUser) {
                webview.onResume();
                logTabWap();
            } else webview.onPause();
        }
        mSelf = isVisibleToUser ? this : null;
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    protected String getSourceClassName() {
        return "TabWap";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_wap, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initViewLogin(inflater, container, rootView);
        Bundle bundle = getArguments();
        id = bundle != null ? bundle.getString("id") : "";
        Log.i(TAG, "id: " + id);
        if (TextUtils.isEmpty(id)) {
            activity.showToast(R.string.e601_error_but_undefined);
            if (activity instanceof ModuleActivity) activity.finish();
        } else {
            tabWap = app.getConfigBusiness().getConfigTabHomeWap(id);
            if (tabWap == null) {
                Log.i(TAG, "tabwap null");
                activity.showToast(R.string.e601_error_but_undefined);
                if (activity instanceof ModuleActivity) activity.finish();
            } else {
                if (!TextUtils.isEmpty(tabWap.getColor()))
                    bgHeader.setBackgroundColor(Color.parseColor(tabWap.getColor()));
                else
                    bgHeader.setBackgroundResource(R.color.home_tab_more);
                initWebView();
                currentUrl = tabWap.getLink();
                Log.i(TAG, "WebView.loadUrl: " + currentUrl);
                if (needLoadOnCreate) {
                    loadUrl();
                    needLoadOnCreate = false;
                    logTabWap();
                }
                homeAbTitle.setText(tabWap.getName());
                /*Glide.with(app).load(tabWap.getImg()).apply(new RequestOptions()
                        .placeholder(R.drawable.ic_home_more_default)
                        .error(R.drawable.ic_home_more_default)).into(ivLogo);*/
                setViewHeader();
                webviewSwipyLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh(SwipyRefreshLayoutDirection direction) {
                        loadUrl();
                    }
                });
            }
        }

        return rootView;
    }

    private void setViewHeader() {
        if (activity instanceof HomeActivity) {
            ivBack.setVisibility(View.GONE);
            Utilities.setMargins(homeAbTitle, Utilities.dpToPixel(R.dimen.v5_spacing_normal, activity.getResources()), homeAbTitle.getTop(), homeAbTitle.getRight(), homeAbTitle.getBottom());
            if (!TextUtils.isEmpty(tabWap.getColor()))
                homeAbTitle.setTextColor(Color.parseColor(tabWap.getColor()));
            else
                homeAbTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.home_tab_more));
        } else {
            isVisible = true;
            ivBack.setVisibility(View.VISIBLE);
            headerTop.setVisibility(View.GONE);
            radius.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
//            statusBarHeight.setVisibility(View.GONE);
            ivBack.setVisibility(View.VISIBLE);
           /* LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams) ivLogo.getLayoutParams();
            ll.leftMargin = 0;
            ivLogo.setLayoutParams(ll);*/

            homeAbTitle.setPadding(0, 0, 0, 0);
            homeAbTitle.setAllCaps(false);
            homeAbTitle.setTypeface(homeAbTitle.getTypeface(), Typeface.BOLD);
            homeAbTitle.setTextColor(ContextCompat.getColor(app, R.color.text_ab_title));
            headerController.setBackgroundColor(ContextCompat.getColor(app, R.color.white));
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onBackPressed();
                }
            });
        }
    }

    private void initWebView() {
        webview.setWebViewClient(new MyWebViewClient());
        mWebChromeClient = new MyWebChromeClient();
        webview.setWebChromeClient(mWebChromeClient);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);

        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        //settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        /*if (mWebView != null && Version.hasJellyBeanMR1()) {
            mWebView.addJavascriptInterface(this, "Android");
        }*/
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDisplayZoomControls(false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (tabWap != null && activity instanceof ModuleActivity) {

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ivBack, R.id.fabBack, R.id.fabForward})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                break;
            case R.id.fabBack:
                if (webview != null && webview.canGoBack())
                    webview.goBack();
                break;
            case R.id.fabForward:
                if (webview != null && webview.canGoForward())
                    webview.goForward();
                break;
        }
    }

    public boolean hideCustomView() {
        if (customViewContainer != null && mCustomView != null) {
            Log.i(TAG, "mCustomViewContainer !null");
            mWebChromeClient.onHideCustomView();
            return true;
        }
        return false;
    }

    private void loadUrl() {
        if (app != null && !app.getReengAccountBusiness().isAnonymousLogin()) {
            String formatUrl = Utilities.formatUrl(currentUrl);
            Log.i(TAG, "currentUrl: " + currentUrl);
            Log.d(TAG, "WebView.loadUrl: " + formatUrl);
            if (webview != null) webview.loadUrl(formatUrl);
        }
    }

    public void logTabWap() {
        if (app != null && tabWap != null)
            new WSOnMedia(app).logClickTabWap(tabWap.getId(), null);
    }

    @Override
    public void onLoginSuccess() {
        super.onLoginSuccess();
        loadUrl();
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i(TAG, "onPageStarted: " + url);
        }

        @Override
        public void onPageFinished(WebView web, String url) {
            Log.i(TAG, "onPageFinished currentUrl: " + currentUrl + "\nurl: " + url);
            if (webviewSwipyLayout != null && webviewSwipyLayout.isRefreshing()) {
                webviewSwipyLayout.setRefreshing(false);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            Log.i(TAG, "shouldOverrideUrlLoading currentUrl: " + currentUrl + "\nurl: " + url);
            if (Utilities.processOnShouldOverrideUrlLoading(activity, webView, url)) {
                return true;
            } else return !URLUtil.isNetworkUrl(url);
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            if (isShowingDialog) return;
            /*new AlertDialog.Builder(new ContextThemeWrapper(WebViewActivity.this, R.style.DialogWebView))
                    .setTitle(mRes.getString(R.string.warning))
                    .setCancelable(false)
                    .setMessage(mRes.getString(R.string.notification_error_ssl_cert_invalid))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            isShowingDialog = false;
                            handler.proceed();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                            isShowingDialog = false;
                            handler.cancel();
                            onBackPressed();
                        }
                    })
                    .show();*/
            isShowingDialog = true;

        }
    }

    private class MyWebChromeClient extends WebChromeClient {

        /*@Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            try {
                mUploadMessage = filePathCallback;

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image*//*");
                startActivityForResult(Intent.createChooser(i, "Image Browser"), FILECHOOSER_RESULTCODE, false);
                return true;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return false;
            }
        }*/

        @Override
        public void onProgressChanged(WebView webView, int newProgress) {
            super.onProgressChanged(webView, newProgress);
            if (newProgress >= 80) {
                if (!TextUtils.isEmpty(webView.getUrl())) {
                    currentUrl = Utilities.getOriginalUrl(TextHelper.replaceUrl(webView.getUrl()));
                }
                if (BuildConfig.DEBUG) Log.i(TAG, "onProgressChanged newProgress: " + newProgress
                        + "\ngetUrl: " + webView.getUrl() + "\ngetOriginalUrl: " + webView.getOriginalUrl()
                        + "\ncurrentUrl: " + currentUrl
                );
            }
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            Log.i(TAG, "onshowcustomview");
            mCustomView = view;
            rlContentView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.setKeepScreenOn(true);
            customViewContainer.addView(view);
            customViewCallback = callback;
            if (activity instanceof HomeActivity)
                ((HomeActivity) activity).setShowingCustomViewWap(true);
            else if (activity instanceof ModuleActivity)
                ((ModuleActivity) activity).setShowingCustomViewWap(true);
        }

        @Override
        public void onHideCustomView() {
            if (mCustomView != null) {
                rlContentView.setVisibility(View.VISIBLE);
                customViewContainer.setKeepScreenOn(false);
                customViewContainer.setVisibility(View.GONE);

                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);

                // Remove the custom view from its container.
                customViewContainer.removeView(mCustomView);
                customViewCallback.onCustomViewHidden();

                mCustomView = null;
                if (activity instanceof HomeActivity)
                    ((HomeActivity) activity).setShowingCustomViewWap(false);
                else if (activity instanceof ModuleActivity)
                    ((ModuleActivity) activity).setShowingCustomViewWap(false);

            }
        }
    }
}

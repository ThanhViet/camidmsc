package com.metfone.selfcare.fragment.musickeeng;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.ui.dialog.BottomSheetDialog;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by daipr on 4/15/2020
 */

public class TabStrangerFragment extends BaseHomeFragment implements MusicBusiness.onCreateStrangerRoomListener {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.ivAddFriend)
    AppCompatImageView ivAddFriend;
    @BindView(R.id.ivPhoneStranger)
    AppCompatImageView ivPhoneStranger;
    @BindView(R.id.iv_more)
    AppCompatImageView ivMore;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.icBackToolbar)
    AppCompatImageView icBack;
    @BindView(R.id.tv_tab_name)
    AppCompatTextView tvTabTitle;
    private BottomSheetDialog dialogMore;
    private ArrayList<ItemContextMenu> dialogMoreItems;

    private MusicBusiness mMusicBusiness;
    private ApplicationController applicationController;

    private FragmentStatePagerItemAdapter viewPagerAdapter;
    private int currentTab = 0;

    public static TabStrangerFragment newInstance() {
        Bundle args = new Bundle();
        TabStrangerFragment fragment = new TabStrangerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static TabStrangerFragment newInstance(boolean isShowBackIcon) {
        Bundle args = new Bundle();
        args.putBoolean("is_show_back_icon", isShowBackIcon);
        TabStrangerFragment fragment = new TabStrangerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static TabStrangerFragment newInstance(boolean isShowBackIcon, int currentTab) {
        Bundle args = new Bundle();
        args.putBoolean("is_show_back_icon", isShowBackIcon);
        args.putInt(Constants.KEY_POSITION, currentTab);
        TabStrangerFragment fragment = new TabStrangerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applicationController = ApplicationController.self();
        mMusicBusiness = applicationController.getMusicBusiness();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_stranger_v5;
    }

    @Override
    public void onCreateView() {
        setupAdapter();
        if (applicationController.getReengAccountBusiness().isAnonymousLogin()) {
            ivMore.setVisibility(View.GONE);
            ivAddFriend.setVisibility(View.GONE);
            ivPhoneStranger.setVisibility(View.GONE);
        }
        if (getArguments() != null) {
            if (getArguments().getBoolean("is_show_back_icon", false)) {
                icBack.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) tvTabTitle.getLayoutParams();
                layoutParams.leftMargin = 0;
                tvTabTitle.invalidate();
            }
            if (currentTab == 0)
                currentTab = getArguments().getInt(Constants.KEY_POSITION, 0);
        }
        ivMore.setOnClickListener(v -> {
            onClickMore();
        });
        icBack.setOnClickListener(view -> {
            getActivity().onBackPressed();
        });
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(viewPagerAdapter);
        // TODO: 5/21/2020 thêm mới
        if (currentTab <= viewPagerAdapter.getCount()) {
            viewPager.setCurrentItem(currentTab);
        }
        tabLayout.setupWithViewPager(viewPager);
        ivAddFriend.setOnClickListener(v -> btnMusicClicked());
//        ivPhoneStranger.setOnClickListener(v ->
//                DeepLinkHelper.getInstance().handlerPostStrangerConfide(applicationController,
//                        (BaseSlidingFragmentActivity) getActivity(),
//                        false, this));
    }

    private void btnMusicClicked() {
        mMusicBusiness.showConfirmOrNavigateToSelectSong((BaseSlidingFragmentActivity) getActivity(), Constants.TYPE_MUSIC.TYPE_STRANGER,
                null, getString(R.string.stranger), new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
//                        notifyChangeAdapter();
//                        Fragment fragment = getChildFragmentManager().getFragments().get(0);
//                        if (fragment != null && fragment instanceof StrangerMusicFragment) {
//                            ((StrangerMusicFragment) fragment).notifyChangeAdapter();
//                        }
                        NavigateActivityHelper.navigateToSelectSong((BaseSlidingFragmentActivity) getActivity(), null,
                                Constants.ACTION.STRANGER_MUSIC_SELECT_SONG, false);
                    }

                    @Override
                    public void onGotoChange() {
                        //khong vao day
                    }
                });
    }

    private void setupAdapter() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(activity);
        creator.add(FragmentPagerItem.of(getString(R.string.listen_together), StrangerMusicFragment.class));
        Bundle bundle = new Bundle();
        bundle.putBoolean(StrangerMusicFragment.CONFIDE_TAB_KEY, true);
        creator.add(FragmentPagerItem.of(getString(R.string.talk_together), StrangerMusicFragment.class, bundle));
//        creator.add(FragmentPagerItem.of(getString(R.string.near_you), NearYouFragment.class));
        viewPagerAdapter = new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_strangers);
    }

    private void onClickMore() {
        if (dialogMore != null && dialogMore.isShowing()) {
            dialogMore.dismiss();
            dialogMore = null;
        }
        dialogMore = new BottomSheetDialog(activity);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_more_tab_video, null, false);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        initDataDialogMore();
        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(dialogMoreItems);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu item = (ItemContextMenu) object;
                    switch (item.getActionTag()) {
                        case Constants.MENU.MENU_MUSIC_ROOM:
                            NavigateActivityHelper.navigateToSettingActivity(activity, Constants.SETTINGS.LIST_ROOM_MUSIC);
                            break;
                        /*case MENU.MENU_ADD_FRIEND:
                            break;*/
                        case Constants.MENU.MENU_SEARCH:
                            NavigateActivityHelper.navigateToSelectSong(activity, null, -1, true);
                            break;
                        case Constants.MENU.MENU_NEAR_YOU:
                            NavigateActivityHelper.navigateToSettingActivity(activity, Constants.SETTINGS.NEAR_YOU);
                            break;
                        default:
                            break;
                    }
                }
                dialogMore.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalMenuRecycler(activity, recyclerView, null, adapter, true);
        dialogMore.setContentView(sheetView);
        dialogMore.setOnShowListener(dialog -> {

        });
        dialogMore.show();
    }

    private void initDataDialogMore() {
        if (dialogMoreItems == null) dialogMoreItems = new ArrayList<>();
        else dialogMoreItems.clear();

        ItemContextMenu musicRoom = new ItemContextMenu(getString(R.string.music_room),
                R.drawable.ic_music_room_v5, null, Constants.MENU.MENU_MUSIC_ROOM);
        dialogMoreItems.add(musicRoom);

        ItemContextMenu nearYou = new ItemContextMenu(getString(R.string.nearby_friends),
                R.drawable.ic_v5_location, null, Constants.MENU.MENU_NEAR_YOU);
        dialogMoreItems.add(nearYou);
        ItemContextMenu searchUser = new ItemContextMenu(getString(R.string.search_friend_by_name),
                R.drawable.ic_find_friend_by_name_v5, null, Constants.MENU.MENU_SEARCH);
        dialogMoreItems.add(searchUser);
        ItemContextMenu item = new ItemContextMenu(getString(R.string.cancel_menu_stranger), R.drawable.ic_close_option, null, Constants.MENU.MENU_EXIT);
        dialogMoreItems.add(item);

    }

    public void setCurrentTab(int tabPosition) {
        viewPager.setCurrentItem(tabPosition);
//        Fragment fragment = getChildFragmentManager().getFragments().get(0);
//        if (fragment != null && fragment instanceof StrangerMusicFragment) {
//            ((StrangerMusicFragment) fragment).notifyChangeAdapter();
//        }
    }

    public void updateStrangerModels(ArrayList<StrangerMusic> list, int tabPosition) {
        viewPager.setCurrentItem(tabPosition);
        Fragment fragment = getChildFragmentManager().getFragments().get(tabPosition);
        if (fragment != null && fragment instanceof StrangerMusicFragment) {
            ((StrangerMusicFragment) fragment).onChangeStrangerList(list);
        }
    }

    @Override
    public void onResponse(ArrayList<StrangerMusic> strangerMusics, boolean isConfide) {
        if(isConfide)  LuckyWheelHelper.getInstance(app).doMission(Constants.LUCKY_WHEEL.ITEM_TALK_STRANGER);
        updateStrangerModels(strangerMusics, isConfide ? 1 : 0);
    }

    @Override
    public void onError(int errorCode) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventHelper.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventHelper.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(HomeActivity.SetCurrentTabStrangerEvent event) {
        if (event != null) {
            currentTab = event.getPos();
        }
        EventHelper.removeStickyEvent(event);
    }
}

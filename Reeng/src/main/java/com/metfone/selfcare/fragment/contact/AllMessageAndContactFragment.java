package com.metfone.selfcare.fragment.contact;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.ThreadListAdapterRecyclerView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ReengMessageWrapper;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.module.search.model.ResultSearchContact;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thanh on 3/17/2015.
 */
public class AllMessageAndContactFragment extends Fragment implements
        ContactChangeListener,
        ClickListener.IconListener,
        InitDataListener {
    private static final String TAG = AllMessageAndContactFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mParentActivity;
    private LayoutInflater mLayoutInflater;
    private Handler mHandler;
    private Resources mRes;
    private OnFragmentInteractionListener mListener;
    private int actionType, mThreadType;
    //variable
    private RecyclerView mRecyclerView;
    private String myNumber;
    private ReengSearchView mSearchView;
    private View mViewActionBar, rootView, mViewFooter;
    //private ArrayList<ItemContextMenu> listItemOverFlow;
    private TextView mTvwNoteEmpty;
    private ImageView mImgOverFlow, mImgBack;
    //    private Button mBtnInvite;
    private ReengAccountBusiness mAccountBusiness;
    private MusicBusiness mMusicBusiness;
    private MessageBusiness mMessageBusiness;

    private ArrayList<ThreadMessage> mListThreads;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private ThreadListAdapterRecyclerView mAdapter;
    private SearchThreadTask mSearchTask;
    private InitThreadList mInitThreadAsynctask;
    private ClickListener.IconListener mIconListener;
    private ReengMessage mForwardingMessage;
    private MediaModel songModel;
    //
    private ThreadMessage mThreadMessage;
    private ReengMessageWrapper arrayMessage;

    private boolean showShareOnMedia = false;
    private LinearLayout mLayoutShareOnMedia;
    private TextView mTvwShareOnMedia;
    private ImageView mImgShareOnMedia;
    private ProgressLoading mProgressLoading;

    private View mViewHeaderChatMore;
    private TextView mTvwChatMoreNumber;

    private EventOnMediaHelper eventOnMediaHelper;

    private String textSearch;
    private Comparator comparatorNameUnmark;
    private Comparator comparatorKeySearchUnmark;
    private Comparator comparatorContacts;
    private Comparator comparatorThreadMessages;

    public AllMessageAndContactFragment() {

    }

    public static AllMessageAndContactFragment newInstance(int actionType, int threadType) {
        AllMessageAndContactFragment fragment = new AllMessageAndContactFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, threadType);
        fragment.setArguments(args);
        return fragment;
    }

    public static AllMessageAndContactFragment newInstance(int actionType, int threadType, ReengMessage message) {
        AllMessageAndContactFragment fragment = new AllMessageAndContactFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, threadType);
        args.putSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, message);
        fragment.mForwardingMessage = message;
        fragment.setArguments(args);
        return fragment;
    }

    public static AllMessageAndContactFragment newInstance(int actionType, int threadType,
                                                           ReengMessageWrapper arrayMessage, boolean showShareOnMedia) {
        AllMessageAndContactFragment fragment = new AllMessageAndContactFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, threadType);
        args.putSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE, arrayMessage);
        args.putBoolean(Constants.CHOOSE_CONTACT.DATA_SHOW_SHARE_ONMEDIA, showShareOnMedia);
        fragment.arrayMessage = arrayMessage;
        fragment.setArguments(args);
        return fragment;
    }

    public static AllMessageAndContactFragment newInstance(int actionType, MediaModel songModel) {
        AllMessageAndContactFragment fragment = new AllMessageAndContactFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        args.putSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC, songModel);
        fragment.setArguments(args);
        return fragment;
    }

    public static boolean isExistThread(String number, ArrayList<Object> listObjects) {
        if (TextUtils.isEmpty(number) || listObjects == null || listObjects.isEmpty()) {
            return false;
        } else {
            for (Object item : listObjects) {
                if (item instanceof PhoneNumber) {
                    return true;
                } else if (item instanceof ThreadMessage) {
                    ThreadMessage thread = (ThreadMessage) item;
                    //&&(thread.getStringFromListPhone().contains(number)
                    if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach :");
        if (activity instanceof ChooseContactActivity) {
            mParentActivity = (ChooseContactActivity) activity;
        } else if (activity instanceof HomeActivity) {
            mParentActivity = (HomeActivity) activity;
        }
        mRes = mParentActivity.getResources();
        eventOnMediaHelper = new EventOnMediaHelper(mParentActivity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        // initOverFlowMenu();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_thread_all_message, container, false);
        findComponentViews(rootView, container);
        getData();
//        setActionbarView();
        setListener();
        if (showShareOnMedia) {
            mParentActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            actionType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mThreadType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE);
            if (mParentActivity instanceof ChooseContactActivity) {
                mForwardingMessage = (ReengMessage) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT
                        .DATA_REENG_MESSAGE);
                arrayMessage = (ReengMessageWrapper) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT
                        .DATA_ARRAY_MESSAGE);
            }
            songModel = (MediaModel) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC);
            mThreadMessage = (ThreadMessage) savedInstanceState.getSerializable("thread_message");
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        mHandler = new Handler();
        mIconListener = this;
//        mClickCallBack = this;
        ListenerHelper.getInstance().addInitDataListener(this);
        if (mApplication.isDataReady()) {
            ListenerHelper.getInstance().addContactChangeListener(this);
//            getListPhoneAsynctask();
            startAsynctaskInitThreads();
        }
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        mHandler = null;
        if (mInitThreadAsynctask != null) {
            mInitThreadAsynctask.cancel(true);
            mInitThreadAsynctask = null;
        }
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, actionType);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadType);
        outState.putSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, mForwardingMessage);
        outState.putSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC, songModel);
        outState.putSerializable("thread_message", mThreadMessage);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        //        mClickCallBack = null;
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        //        mLvListView.releaseListView();
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) {
            return;
        }
        ListenerHelper.getInstance().addContactChangeListener(this);
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
////                getListPhoneAsynctask();
//            }
//        });
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    @Override
    public void onContactChange() {
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
////                getListPhoneAsynctask();
//            }
//        });
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> listNumber) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onRosterChange() {

    }

    private void startAsynctaskInitThreads() {
        if (mInitThreadAsynctask != null) {
            mInitThreadAsynctask.cancel(true);
            mInitThreadAsynctask = null;
        }
        mInitThreadAsynctask = new InitThreadList();
        mInitThreadAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setActionbarView() {
        // setcustom view
//        mActionBar = mParentActivity.getSupportActionBar();
//        mActionBar.setCustomView(mViewActionBar, new ActionBar.LayoutParams(
//                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
    }

    private void findComponentViews(View rootView, ViewGroup container) {
        mLayoutInflater = (LayoutInflater) mParentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mViewActionBar = mLayoutInflater.inflate(R.layout.ab_search_box, null);
        mViewActionBar = mParentActivity.getToolBarView();
        mParentActivity.setCustomViewToolBar(mLayoutInflater.inflate(R.layout.ab_search_box, null));

        mViewFooter = mLayoutInflater.inflate(R.layout.button_layout, container, false);
        mImgBack = mViewActionBar.findViewById(R.id.ab_back_btn);
        mImgOverFlow = mViewActionBar.findViewById(R.id.ab_more_btn);
        mSearchView = mViewActionBar.findViewById(R.id.ab_search_view);
        mRecyclerView = rootView.findViewById(R.id.message_list_recycleview);
        mLayoutShareOnMedia = rootView.findViewById(R.id.layout_share_onmedia);
        mLayoutShareOnMedia.setVisibility(View.GONE);
        mTvwShareOnMedia = rootView.findViewById(R.id.tvw_share_to_onmedia);
        mImgShareOnMedia = rootView.findViewById(R.id.img_share_to_onmedia);
        mTvwNoteEmpty = rootView.findViewById(R.id.message_list_note_empty);
        mTvwNoteEmpty.setText(mRes.getString(R.string.not_find));
        mTvwNoteEmpty.setVisibility(View.GONE);
        mProgressLoading = rootView.findViewById(R.id.message_list_loading_progressbar);
        mProgressLoading.setVisibility(View.GONE);
        //mLvListView.setFastScrollEnabled(true);
        InputMethodUtils.hideKeyboardWhenTouch(rootView, mParentActivity);
//        mSearchView.requestFocus();

        mViewHeaderChatMore = rootView.findViewById(R.id.create_chat_header_chat_more);
        mTvwChatMoreNumber = rootView.findViewById(R.id.create_chat_header_chat_more_number);
        mViewHeaderChatMore.setVisibility(View.GONE);
    }

    private void setSearchHint() {
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC) {
            mSearchView.setHint(mRes.getString(R.string.search_hint_keeng_together_music));
        } else {
            mSearchView.setHint(mRes.getString(R.string.search));
        }
    }

    private void getData() {
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        // lay danh sach ban be da co trong thread
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        myNumber = mAccountBusiness.getJidNumber();
        //ArrayList<String> listMember = new ArrayList<String>();
        if (getArguments() != null) {
            actionType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mThreadType = getArguments().getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE);
            showShareOnMedia = getArguments().getBoolean(Constants.CHOOSE_CONTACT.DATA_SHOW_SHARE_ONMEDIA);
            if (showShareOnMedia && mApplication.getConfigBusiness().isEnableOnMedia()) {
                mLayoutShareOnMedia.setVisibility(View.VISIBLE);
            } else {
                mLayoutShareOnMedia.setVisibility(View.GONE);
            }
            if (mParentActivity instanceof ChooseContactActivity) {
                mForwardingMessage = (ReengMessage) getArguments().getSerializable(Constants.CHOOSE_CONTACT
                        .DATA_REENG_MESSAGE);
                arrayMessage = (ReengMessageWrapper) getArguments().getSerializable(Constants.CHOOSE_CONTACT
                        .DATA_ARRAY_MESSAGE);
            }
            songModel = (MediaModel) getArguments().getSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC);
        }
        mImgOverFlow.setVisibility(View.GONE);
        setSearchHint();
    }

    private void setListener() {
        setSearchViewListener();
        setButtonBackListener();
        setViewOnMediaListener();
        setViewMoreListener();
    }

    private void setViewMoreListener() {
        mViewHeaderChatMore.setOnClickListener(v -> {
            if (mListener == null) return;
            if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_FROM_OTHER_APP && arrayMessage != null
                    && !arrayMessage.getReengMessages().isEmpty()) {
                if (PhoneNumberHelper.getInstant().isValidNumberNotRemoveChar(textSearch)) {
                    ThreadMessage threadMessage = mMessageBusiness.createNewThreadNormal(textSearch);
                    mListener.sendMultiMsgToThread(threadMessage);
                }
            } else {
                mApplication.getMessageBusiness().checkAndStartForwardUnknownNumber(mParentActivity,
                        textSearch, mForwardingMessage);
            }

        });
    }

    private void setViewOnMediaListener() {
        mLayoutShareOnMedia.setOnClickListener(view -> navigateToShareOnMedia());

        mTvwShareOnMedia.setOnClickListener(view -> navigateToShareOnMedia());

        mImgShareOnMedia.setOnClickListener(view -> navigateToShareOnMedia());
    }

    private void navigateToShareOnMedia() {
        if (arrayMessage != null && !arrayMessage.getReengMessages().isEmpty()) {
            ReengMessage msg = arrayMessage.getReengMessages().get(0);
            if (msg != null && !TextUtils.isEmpty(msg.getFilePath())) {
                if (msg.getMessageType() == ReengMessageConstant.MessageType.text) {
                    eventOnMediaHelper.getPreviewUrl(msg.getFilePath(),
                            R.string.ga_category_onmedia, R.string.ga_onmedia_action_share_from_other_app,
                            FeedModelOnMedia.ActionFrom.onmedia, () -> goToHome());
                } else if (msg.getMessageType() == ReengMessageConstant.MessageType.image) {
                    ArrayList<String> listImg = new ArrayList<>();
                    for (ReengMessage reengmsg : arrayMessage.getReengMessages()) {
                        listImg.add(reengmsg.getFilePath());
                    }
                    eventOnMediaHelper.navigateToWriteStatusImage(listImg);
                }
            }
        }
    }

    protected void goToHome() {
        Intent intent = new Intent(mApplication, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mParentActivity.startActivity(intent);
        mParentActivity.finish();
    }

    private void setButtonBackListener() {
        mImgBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }

    private void setItemListViewListener() {
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
                if (object instanceof ThreadMessage) {
                    ThreadMessage threadMessage = (ThreadMessage) object;
                    if (actionType == Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT) {
                        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                            mParentActivity.showToast(R.string.cant_forward_msg_to_room);
                        } else {
                            //neu la tn text thi ko co popup xac nhan
                            if (mForwardingMessage != null &&
                                    mForwardingMessage.getMessageType() == ReengMessageConstant.MessageType.text) {
                                mListener.forwardMessageToThread(threadMessage);
                            } else {
                                String message;
                                String threadName = mApplication.getMessageBusiness().getThreadName(threadMessage);
                                if (mParentActivity instanceof ChooseContactActivity) {
                                    //message = String.format(mParentActivity.getString(R.string
                                    // .forward_message_confirm), "", threadMessage.getName());
                                    message = getForwardNotifyContent(threadName);
                                } else {
                                    message = String.format(mRes.getString(R.string.share_confirm), threadName);
                                }
                                PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                                        "", message, mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                                        mIconListener, threadMessage, Constants.MENU.FORWARD_TO_THREAD);
                            }
                        }
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC && songModel != null) {
                        processChooseFriendTogetherMusic(threadMessage, null, songModel);
                    } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_FROM_OTHER_APP && arrayMessage != null
                            && !arrayMessage.getReengMessages().isEmpty()) {
                        mListener.sendMultiMsgToThread(threadMessage);
                    }
                } else if (object instanceof PhoneNumber) {
                    PhoneNumber phone = (PhoneNumber) object;
                    if (phone.getContactId() == null) {
                        return;
                    }
                    // Forward: khong duoc forward cho chinh minh, và nhưng so ngoai mang chua cai mocha.
                    String number = phone.getJidNumber();
                    ReengAccount account = mAccountBusiness.getCurrentAccount();
                    if (number.equals(myNumber)) {
                        mParentActivity.showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
                    } else if (phone.isReeng() || (mAccountBusiness.isViettel() && phone.isViettel())) {
                        if (actionType == Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT) {
                            //neu la tn text thi ko co popup xac nhan
                            if (mForwardingMessage != null &&
                                    mForwardingMessage.getMessageType() == ReengMessageConstant.MessageType.text) {
                                mListener.forwardMessageToContact(number);
                            } else {
                                String message;
                                if (mParentActivity instanceof ChooseContactActivity) {
                                    message = getForwardNotifyContent(phone.getName());
                                } else {
                                    message = String.format(mRes.getString(R.string.share_confirm), phone.getName());
                                }
                                PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                                        "", message, mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                                        mIconListener, phone, Constants.MENU.FORWARD_TO_PHONE_NUMBER);
                            }
                        } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC && songModel != null) {
                            processChooseFriendTogetherMusic(null, phone, songModel);
                        } else if (actionType == Constants.CHOOSE_CONTACT.TYPE_SHARE_FROM_OTHER_APP && arrayMessage
                                != null
                                && !arrayMessage.getReengMessages().isEmpty()) {
                            ThreadMessage mThread = mMessageBusiness.findExistingOrCreateNewThread(number);
                            mListener.sendMultiMsgToThread(mThread);
                        }
                    } else {
                        InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, mParentActivity,
                                phone.getName(), phone.getJidNumber(), false);
                    }
                }
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    private String getForwardNotifyContent(String contactName) {
        ReengMessageConstant.MessageType messageType = mForwardingMessage.getMessageType();
        String msgType = mRes.getString(ReengMessageConstant.MessageType.toStringResource(messageType)).toLowerCase();
        return String.format(mRes.getString(R.string.forward_message_confirm),
                msgType, contactName);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setSearchViewListener() {
        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchThreadAsynctask(s.toString());
            }
        });

        mSearchView.setOnEditorActionListener((textView, keyCode, keyEvent) -> {
            if (keyCode == EditorInfo.IME_ACTION_DONE) {
                InputMethodUtils.hideSoftKeyboard(mSearchView, mParentActivity);
            }
            return false;
        });
        mSearchView.setOnTouchListener((view, motionEvent) -> {
            // assus zenphone
            //mSearchView.clearFocus();
            InputMethodUtils.showSoftKeyboard(mParentActivity, mSearchView);
            return false;
        });
    }

    private String getContentOfSearchView() {
        if (mSearchView == null) {
            return null;
        }
        if (mSearchView.getText() == null) {
            return null;
        }
        return mSearchView.getText().toString().trim();
    }

    private void initComparatorSearchContacts() {
        comparatorNameUnmark = SearchUtils.getComparatorNameForSearch();
        comparatorKeySearchUnmark = SearchUtils.getComparatorKeySearchForSearch();
        comparatorContacts = SearchUtils.getComparatorContactForSearch();
        comparatorThreadMessages = SearchUtils.getComparatorMessageForSearch();
    }

    private void searchThreadAsynctask(String content) {
        textSearch = content;
        if (mSearchTask != null) {
            mSearchTask.cancel(true);
            mSearchTask = null;
        }
        mSearchTask = new SearchThreadTask(mApplication);
        mSearchTask.setDatas(mListThreads);
        mSearchTask.setComparatorContact(comparatorContacts);
        mSearchTask.setComparatorMessage(comparatorThreadMessages);
        mSearchTask.setComparatorKeySearch(comparatorKeySearchUnmark);
        mSearchTask.setComparatorName(comparatorNameUnmark);
        mSearchTask.setListener(new SearchMessagesListener() {
            @Override
            public void onPrepareSearchThread() {

            }

            @Override
            public void onFinishedSearchThread(String keySearch, ArrayList<Object> list) {
                textSearch = keySearch;
                displaySearchResult(list);
            }
        });
        mSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, textSearch);
    }

    private void displaySearchResult(ArrayList<Object> listObjects) {
        if (mAdapter == null) {
            setAdapter();
        }
        mAdapter.setThreadList(listObjects);
        mAdapter.notifyDataSetChanged();
        //String contentSearch = getContentOfSearchView();
        // list all empty or result empty
        /*if (listObjects == null || listObjects.isEmpty()) {
            mTvwNoteEmpty.setVisibility(View.VISIBLE);
        } else {
            mTvwNoteEmpty.setVisibility(View.GONE);
        }*/

        showOrHideItemMoreNumber(textSearch, listObjects);
    }

    private void showOrHideItemMoreNumber(String textSearch, ArrayList<Object> result) {
        if (((arrayMessage != null && arrayMessage.getReengMessages().size() != 0) || mForwardingMessage != null)
                && !TextUtils.isEmpty(textSearch)) {
            mTvwNoteEmpty.setVisibility(View.GONE);
            if (result == null || result.isEmpty()) {
                // neu ko tim dc kq nao hien ngay option chat more
                if (!PhoneNumberHelper.getInstant().isValidNumberNotRemoveChar(textSearch)) {
                    mTvwNoteEmpty.setVisibility(View.VISIBLE);
                    mViewHeaderChatMore.setVisibility(View.GONE);
                } else {
                    mTvwNoteEmpty.setVisibility(View.GONE);
                    mTvwChatMoreNumber.setText(String.format(mRes.getString(R.string.chat_more), textSearch));
                    mViewHeaderChatMore.setVisibility(View.VISIBLE);
                }
            } else if (!isExistThread(textSearch, result)) {
                // neu tim thay phonenumber hoac thread 1-1 thi khong hien option
                if (!PhoneNumberHelper.getInstant().isValidNumberNotRemoveChar(textSearch)) {
                    mViewHeaderChatMore.setVisibility(View.GONE);
                } else {
                    mTvwChatMoreNumber.setText(String.format(mRes.getString(R.string.chat_more), textSearch));
                    mViewHeaderChatMore.setVisibility(View.VISIBLE);
                }
            } else {
                mViewHeaderChatMore.setVisibility(View.GONE);
            }
        } else {
            mViewHeaderChatMore.setVisibility(View.GONE);
            mTvwNoteEmpty.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        mAdapter = new ThreadListAdapterRecyclerView(mParentActivity, Constants.CONTACT.CONTACT_VIEW_FORWARD_MESSAGE,
                null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mParentActivity));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        if (actionType == Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO)
            RecyclerViewUtils.setFooterView(mRecyclerView, mViewFooter);
        setItemListViewListener();
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.FORWARD_TO_THREAD:
                ThreadMessage threadMessage = (ThreadMessage) entry;
                mListener.forwardMessageToThread(threadMessage);
                break;
            case Constants.MENU.FORWARD_TO_PHONE_NUMBER:
                PhoneNumber phoneNumber = (PhoneNumber) entry;
                mListener.forwardMessageToContact(phoneNumber.getJidNumber());
                break;
            default:
                break;
        }
    }

    private void processChooseFriendTogetherMusic(ThreadMessage threadMessage,
                                                  PhoneNumber phoneNumber, MediaModel songModel) {
        mMusicBusiness = mApplication.getMusicBusiness();
        int threadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        String friendName;
        String friendNumber = null;
        if (threadMessage != null) {
            mThreadMessage = threadMessage;
            threadType = threadMessage.getThreadType();
            friendName = mApplication.getMessageBusiness().getThreadName(mThreadMessage);
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                friendNumber = threadMessage.getSoloNumber();
            }
        } else if (phoneNumber != null) {
            friendName = phoneNumber.getName();
            friendNumber = phoneNumber.getJidNumber();
            threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread(phoneNumber.getJidNumber());
        } else {
            return;
        }
        goToChatDetailOrShowError(threadType, threadMessage, friendNumber, friendName, songModel);
    }

    /**
     * goToChatDetailOrShowError
     *
     * @param threadType
     * @param friendNumber
     * @param friendName
     */
    private void goToChatDetailOrShowError(int threadType, final ThreadMessage threadMessage,
                                           final String friendNumber, final String friendName, final MediaModel
                                                   songModel) {
        Log.i(TAG, "goToSelectSongOr ShowError " + mMusicBusiness.getCurrentMusicSessionId());
        //mMusicBusiness.insertNewMediaModel(songModel);
        mMusicBusiness.showConfirmOrNavigateToSelectSong(mParentActivity, threadType,
                friendNumber, friendName, new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
                        mApplication.getMessageBusiness().createAndSendMessageInviteMusic(threadMessage, songModel,
                                mParentActivity);
                        mListener.navigateToChatActivity(threadMessage);
                    }

                    @Override
                    public void onGotoChange() {
                        boolean isRequestChange = !threadMessage.isAdmin();
                        mApplication.getMessageBusiness().createAndSendMessageChangeMusic(threadMessage, songModel,
                                mParentActivity, isRequestChange);
                        mListener.navigateToChatActivity(threadMessage);
                    }
                });
    }

    public interface OnFragmentInteractionListener {
        void forwardMessageToContact(String number);

        void forwardMessageToThread(ThreadMessage thread);

        void navigateToChatActivity(ThreadMessage threadMessage);

        void sendMultiMsgToThread(ThreadMessage thread);
    }

    private interface SearchMessagesListener {
        void onPrepareSearchThread();

        void onFinishedSearchThread(String keySearch, ArrayList<Object> list);
    }

    private static class SearchThreadTask extends AsyncTask<String, Void, ArrayList<Object>> {
        private final String TAG = "SearchThreadTask";
        private WeakReference<ApplicationController> application;
        private SearchMessagesListener listener;
        private String keySearch;
        private CopyOnWriteArrayList<Object> datas;
        private Comparator comparatorName;
        private Comparator comparatorKeySearch;
        private Comparator comparatorContact;
        private Comparator comparatorMessage;
        private long startTime;
        private int totalDatas;

        public SearchThreadTask(ApplicationController application) {
            this.application = new WeakReference<>(application);
            this.datas = new CopyOnWriteArrayList<>();
        }

        public void setListener(SearchMessagesListener listener) {
            this.listener = listener;
        }

        public void setDatas(ArrayList<ThreadMessage> datas) {
            if (this.datas != null && datas != null)
                this.datas.addAll(datas);
        }

        public void setComparatorName(Comparator comparatorName) {
            this.comparatorName = comparatorName;
        }

        public void setComparatorKeySearch(Comparator comparatorKeySearch) {
            this.comparatorKeySearch = comparatorKeySearch;
        }

        public void setComparatorContact(Comparator comparatorContact) {
            this.comparatorContact = comparatorContact;
        }

        public void setComparatorMessage(Comparator comparatorMessage) {
            this.comparatorMessage = comparatorMessage;
        }

        @Override
        protected void onPreExecute() {
            if (listener != null) listener.onPrepareSearchThread();
        }

        @Override
        protected void onPostExecute(ArrayList<Object> result) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "search " + keySearch + " has " + result.size() + "/" + totalDatas
                        + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
            if (listener != null) listener.onFinishedSearchThread(keySearch, result);
        }

        @Override
        protected ArrayList<Object> doInBackground(String... params) {
            startTime = System.currentTimeMillis();
            totalDatas = 0;
            keySearch = params[0];
            ArrayList<Object> list = new ArrayList<>();
            if (Utilities.notNull(application) && this.datas != null) {
                datas.addAll(application.get().getContactBusiness().getListNumberAlls());
                if (Utilities.notEmpty(datas)) {
                    totalDatas = datas.size();
                    ResultSearchContact result = SearchUtils.searchMessagesAndContacts(application.get()
                            , keySearch, datas, comparatorKeySearch, comparatorName, comparatorMessage, comparatorContact);
                    if (result != null) {
                        keySearch = result.getKeySearch();
                        if (Utilities.notEmpty(result.getResult())) {
                            list.addAll(result.getResult());
                        }
                    }
                }
            }
            return list;
        }
    }

    private class InitThreadList extends AsyncTask<Void, Void, ArrayList<ThreadMessage>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<ThreadMessage> doInBackground(Void... params) {
            long beginTime = System.currentTimeMillis();
            try {
                ArrayList<ThreadMessage> listThreads = new ArrayList<>();
                if (mMessageBusiness.getThreadMessageArrayList() == null ||
                        mMessageBusiness.getThreadMessageArrayList().isEmpty()) {
                    return null;
                }
                //ContactBusiness contactBusiness = mApplication.getContactBusiness();
                for (ThreadMessage threadMessage : mMessageBusiness.getThreadMessageArrayList()) {
                    if (actionType == Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC) {
                        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                                threadMessage.isReadyShow(mMessageBusiness)) {
                            listThreads.add(threadMessage);
                        }
                    } else if (threadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                            threadMessage.isReadyShow(mMessageBusiness)) {
                        listThreads.add(threadMessage);
                    }
                }
                if (!listThreads.isEmpty()) {
                    Collections.sort(listThreads, ComparatorHelper.getComparatorThreadMessageByLastTime());
                }
                Log.i(TAG, "InitThreadList doinBackground take " + (System.currentTimeMillis() - beginTime) + " ms");
                return listThreads;
            } catch (Exception e) {// ArrayIndexOutOfBoundsException
                Log.e(TAG, "Exception", e);
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(ArrayList<ThreadMessage> params) {
            super.onPostExecute(params);
            mListThreads = params;
            textSearch = getContentOfSearchView();
            initComparatorSearchContacts();
            searchThreadAsynctask(textSearch);
        }
    }
}
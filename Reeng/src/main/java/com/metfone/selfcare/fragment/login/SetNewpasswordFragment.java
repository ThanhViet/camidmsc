package com.metfone.selfcare.fragment.login;

import android.app.AlertDialog;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.fragment.login.dialog.SuccessChangePasswordDialog;
import com.metfone.selfcare.model.account.ForgotPasswordResetRequest;
import com.metfone.selfcare.model.account.ResetPasswordInfor;
import com.metfone.selfcare.model.account.ResetPasswordResponse;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.PinView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;


public class SetNewpasswordFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {
    RoundTextView btnConfirm;
    AppCompatImageView ivBack;
    RelativeLayout mRltPassword;
    ProgressBar pbLoading;
    RoundLinearLayout rledtPassword, rledtConfirmPassword;
    FrameLayout flSecondPinView, flPinViewPassword;
    ImageView ivPassVisible, ivConfirmPassVisible;
    boolean isPasswordVisible = false;
    boolean isConfirmPasswordVisible = false;
    String phoneNumber;
    String otp;
    private PinView pinPassword, pinConfirmPassword;
    private PinView hintPinView, hintConfirmPinView;

    public static SetNewpasswordFragment newInstance(String phoneNumber, String otp) {
        SetNewpasswordFragment fragment = new SetNewpasswordFragment();
        Bundle args = new Bundle();
        args.putString(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        args.putString(EnumUtils.OTP_KEY, otp);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_newpassword, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initData();
        initViewListensers();
        new Handler().postDelayed(this::detectKeyboard, 500);
    }

    private void initViewListensers() {
        ivPassVisible.setOnClickListener(view1 -> {
            isPasswordVisible = !isPasswordVisible;
            pinPassword.setPasswordHidden(!isPasswordVisible);
            hintPinView.setVisibility(isPasswordVisible ? View.GONE : View.VISIBLE);
            if (isPasswordVisible) {
                ivPassVisible.setImageResource(R.drawable.ic_visibility_24px_outlined);
            } else {
                ivPassVisible.setImageResource(R.drawable.ic_visibility_off_24px_outlined);
            }
        });
        ivConfirmPassVisible.setOnClickListener(view2 -> {
            isConfirmPasswordVisible = !isConfirmPasswordVisible;
            pinConfirmPassword.setPasswordHidden(!isConfirmPasswordVisible);
            hintConfirmPinView.setVisibility(isConfirmPasswordVisible ? View.GONE : View.VISIBLE);
            if (isConfirmPasswordVisible) {
                ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_24px_outlined);
            } else {
                ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_off_24px_outlined);
            }
        });
        ivBack.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        pinPassword.setOnFocusChangeListener(this);
        pinPassword.setOnClickListener(this);
        pinConfirmPassword.setOnClickListener(this);
        pinConfirmPassword.setOnFocusChangeListener(this);
        flSecondPinView.setOnClickListener(this);
        flPinViewPassword.setOnClickListener(this);
    }

    private void initData() {
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
            otp = getArguments().getString(EnumUtils.OTP_KEY);
        }
    }

    private void initViews(@NonNull View view) {
        btnConfirm = view.findViewById(R.id.btnConfirm);
        ivBack = view.findViewById(R.id.ivBack);
        mRltPassword = view.findViewById(R.id.rlContainer);
        rledtPassword = view.findViewById(R.id.rledtPassword);
        rledtConfirmPassword = view.findViewById(R.id.rledtConfirmPassword);
        pinPassword = view.findViewById(R.id.pinPassword);
        hintPinView = view.findViewById(R.id.hintPinView);
        pinConfirmPassword = view.findViewById(R.id.pinConfirmPassword);
        hintConfirmPinView = view.findViewById(R.id.hintConfirmPinView);
        pbLoading = view.findViewById(R.id.pbLoading);
        flSecondPinView = view.findViewById(R.id.flSecondPinView);
        flPinViewPassword = view.findViewById(R.id.flPinViewPassword);
        pinPassword.setPasswordHidden(true);
        pinConfirmPassword.setPasswordHidden(true);
        ivPassVisible = view.findViewById(R.id.imgPassVisible);
        ivConfirmPassVisible = view.findViewById(R.id.imgConfirmPassVisible);
    }

    void setNewPassWord() {
        pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String password = pinPassword.getText().toString();
        String confirmpassword = pinConfirmPassword.getText().toString();

        ResetPasswordInfor resetPasswordInfor = new ResetPasswordInfor(otp, phoneNumber, password);

        ForgotPasswordResetRequest forgotPasswordResetRequest = new ForgotPasswordResetRequest(resetPasswordInfor, "", "", "", "");
        apiService.resetPassword(token, forgotPasswordResetRequest).enqueue(new Callback<ResetPasswordResponse>() {

            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                pbLoading.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboardPassword(getContext(), flPinViewPassword);
                        UIUtil.showKeyboardPassword(getContext(), flSecondPinView);
                    }
                }, 200);
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        SuccessChangePasswordDialog successChangePasswordDialog = new SuccessChangePasswordDialog(requireActivity(), phoneNumber);
                        successChangePasswordDialog.setCanceledOnTouchOutside(false);
                        successChangePasswordDialog.show();
                    } else {
                        ToastUtils.showToast(getContext(), response.body().getMessage());
                    }
                } else {
                    ToastUtils.showToast(getContext(), response.message());
                }

            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(getContext(), getString(R.string.service_error));
            }
        });

    }

    @Override
    public void onResume() {
        pinPassword.requestFocus();
        UIUtil.showKeyboard(getContext(),pinPassword);
        setupUI(mRltPassword);
        super.onResume();
    }

    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText || innerView instanceof ImageView || innerView instanceof RoundLinearLayout) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pinPassword:
            case R.id.flSecondPinView:
                flSecondPinView.requestFocus();
                flPinViewPassword.clearFocus();
                break;
            case R.id.pinConfirmPassword:
            case R.id.rledtConfirmPassword:
                flSecondPinView.clearFocus();
                flPinViewPassword.requestFocus();
                break;
            case R.id.btnConfirm:
                //Validate
                if (TextUtils.isEmpty(pinPassword.getText().toString()) || TextUtils.isEmpty(pinConfirmPassword.getText().toString())) {
                    showDialog(getString(R.string.please_fill_all_the_fields));
                } else if (!pinPassword.getText().toString().equals(pinConfirmPassword.getText().toString())) {
                    showDialog(getString(R.string.confirm_password));
                } else if (pinPassword.getText().toString().length() != 6) {
                    showDialog(getString(R.string.password_consists_of_6_characters));
                } else {
//                    checkPhone();
                    setNewPassWord();
                }
                break;
            case R.id.ivBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.error));
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (v.getId() == R.id.pinPassword) {
                rledtPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.resend), 1);
            } else if (v.getId() == R.id.pinConfirmPassword) {
                rledtConfirmPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.resend), 1);
            }
        } else {
            if (v.getId() == R.id.pinPassword) {
                rledtPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.color_edt_full_name_stroke), 1);
            } else if (v.getId() == R.id.pinConfirmPassword) {
                rledtConfirmPassword.setStroke(ContextCompat.getColor(requireContext(), R.color.color_edt_full_name_stroke), 1);
            }
        }
    }

    public void detectKeyboard() {
        mRltPassword.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                mRltPassword.getWindowVisibleDisplayFrame(r);
                int heightDiff = mRltPassword.getRootView().getHeight() - (r.bottom - r.top);
                int screenHeight = mRltPassword.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened

                } else {
                    // keyboard is closed
                    pinPassword.clearFocus();
                    pinConfirmPassword.clearFocus();

                }
            }
        });
    }
}
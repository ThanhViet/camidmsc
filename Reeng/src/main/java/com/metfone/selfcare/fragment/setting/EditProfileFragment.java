package com.metfone.selfcare.fragment.setting;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.bumptech.glide.Glide;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.CropImageActivity;
import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.IllegalFormatConversionException;

import androidx.annotation.NonNull;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;

/**
 * Created by toanvk2 on 10/20/14.
 */
public class EditProfileFragment extends BaseSlidingFragmentActivity implements ClickListener.IconListener,
        ProfileListener {

    private static final String TAG = EditProfileFragment.class.getSimpleName();
    private Resources mRes;
    private Handler mHandler;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private View mActionBarView;
    private DatePickerDialog mDatePickerDialog;
    private TextView mTvwTitle;
    private ImageView mImgBack, mTvwUpdate;
    //    private ImageView mImgOption;
    private FrameLayout mFramAvatar;
    private LinearLayout mLLAvatar;
    private RoundedImageView mImgAvatar;
    private EditText mEdtName;
    private TextView mEdtBirthday, mTvwAvatar, mTvwAvatarDefault;
    private RelativeLayout rlDate;
    private RadioGroup mRagGender;
    private TextView mMale, mFemale;
    private Button mBtnFinish;
    private View mBtnFromFacebook;
    private View rootView;
    private View mViewAVNO, mViewAVNOUnderLine;
    private TextView mTvwAVNOLabel, mTvwAVNOValue;
    private RoundTextView mTvwAVNORegister;

    private View mViewAVNOIdentifyCard;
    private ImageView ivArrow;
    private LinearLayout llImgIDCard;
    private ScrollView infoLayout;
    private ImageView mImgDeleteFrontCard, mImgDeleteBackCard;
    private ImageView mImgImageFront, mImgImageBack;
    private ProgressLoading mProgressUpFront, mProgressUpBack;
    private View mHolderImgFront, mHolderImgBack;
    private int uploadOption = ImageProfileConstant.IMAGE_AVATAR;
    private TransferFileBusiness transferFileBusiness;

    //    private FacebookHelper.OnFacebookListener facebookListener;
    private boolean isSaveInstanceState = false, isUpdateSuccess = false;
    private boolean dateSet = false;

    private String blockCharacterSet = "\\\"{}";
    private boolean fromLoginActivity;

    private String fileCropTmp;
    private boolean forceUpIdCard = false;
    private boolean needUploadAvatar = false;
    private int typeTakePhoto;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                if ("{".equals(source.toString()) || "}".equals(source.toString())
                        || "\"".equals(source.toString()) || "\\".equals(source.toString())) {
                    showToast(R.string.name_filter);
                }
                return "";
            }
            return null;
        }
    };

    private InputFilter filterLength = new InputFilter.LengthFilter(Constants.CONTACT.USER_NAME_MAX_LENGTH);


    public static void startActivity(BaseSlidingFragmentActivity activity, boolean forceUpIdCard) {
        Intent intent = new Intent(activity, EditProfileFragment.class);
        intent.putExtra("force_up_idcard", forceUpIdCard);
        activity.startActivity(intent);
    }
    /*public static EditProfileFragment newInstance(boolean fromLoginActivity) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(LoginActivity.class.getSimpleName(), fromLoginActivity);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static EditProfileFragment newInstance(String url) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString(URL, url);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_personal_info);
        mApplication = (ApplicationController) getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mRes = getResources();
        transferFileBusiness = new TransferFileBusiness(mApplication);

        if (savedInstanceState != null) {
            forceUpIdCard = savedInstanceState.getBoolean("force_up_idcard", false);
            fromLoginActivity = savedInstanceState.getBoolean(LoginActivity.class.getSimpleName());
        } else if (getIntent() != null) {
            forceUpIdCard = getIntent().getBooleanExtra("force_up_idcard", false);
            fromLoginActivity = getIntent().getBooleanExtra(LoginActivity.class.getSimpleName(), false);
        }

//        View fakeStatusBar = rootView.findViewById(R.id.layout_fake_statusbar);
//        fakeStatusBar.setVisibility(View.VISIBLE);
        findComponentViews();
        setListener();
        drawProfile();
        if (mAccountBusiness.getCurrentAccount() != null)
            if (TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getName())
                    || mAccountBusiness.isInProgressLoginFromAnonymous()) {
                hideAVNOView = true;
                getUserInfo(mAccountBusiness.getCurrentAccount());
            }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mParentActivity = (ProfileActivity) context;
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mRes = mParentActivity.getResources();
        transferFileBusiness = new TransferFileBusiness(mApplication);
        try {
            mListener = (OnEditProfileListener) context;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentSettingListener");
        }
    }*/

    /*@Override
    public void onDestroyView() {
        mImgAvatar.setImageBitmap(null);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    @Override
    protected void onDestroy() {
        mImgAvatar.setImageBitmap(null);
        super.onDestroy();
    }

    private boolean hideAVNOView;

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_personal_info, container, false);
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (getArguments() != null) {
            fromLoginActivity = getArguments().getBoolean(LoginActivity.class.getSimpleName());
        } else if (saveInstanceState != null) {
            fromLoginActivity = saveInstanceState.getBoolean(LoginActivity.class.getSimpleName(), false);
        }

//        View fakeStatusBar = rootView.findViewById(R.id.layout_fake_statusbar);
//        fakeStatusBar.setVisibility(View.VISIBLE);
        findComponentViews(rootView, inflater);
        setListener();
        drawProfile();
        if (mAccountBusiness.getCurrentAccount() != null)
            if (TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getName())
                    || mAccountBusiness.isInProgressLoginFromAnonymous()) {
                hideAVNOView = true;
                getUserInfo(mAccountBusiness.getCurrentAccount());
            }
        *//*ProfileHelper.drawPersonalInfoAVNO(mApplication, mParentActivity, mViewAVNO,
                mViewAVNOUnderLine, mTvwAVNOLabel, mTvwAVNOValue, mTvwAVNORegister);*//*
        return rootView;
    }*/

    /*@Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL);
            if (url == null || url.isEmpty()) return;
            drawAvatar(url);
        }
    }*/

    private void findComponentViews() {
//        mActionBar = mParentActivity.getSupportActionBar();
//        mActionBarView = inflater.inflate(R.layout.ab_personal_info, null);
//        mActionBar.setCustomView(mActionBarView);

//        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_personal_info, null));
        mActionBarView = findViewById(R.id.layout_fake_actionbar_info);
        mActionBarView.setVisibility(View.VISIBLE);

        mTvwTitle = mActionBarView.findViewById(R.id.txtTitleToolbar);
        mImgBack = mActionBarView.findViewById(R.id.icBackToolbar);
        mTvwUpdate = mActionBarView.findViewById(R.id.icOption);
        mTvwUpdate.setVisibility(View.INVISIBLE);
        mFramAvatar = findViewById(R.id.profile_avatar_frame);
        mLLAvatar = findViewById(R.id.ll_avatar);
        mImgAvatar = findViewById(R.id.profile_avatar_circle);
        mTvwAvatar = findViewById(R.id.contact_avatar_text);
        mTvwAvatarDefault = findViewById(R.id.text_avatar_default);
        mEdtName = findViewById(R.id.edit_name);
        mEdtBirthday = findViewById(R.id.tv_date);
        rlDate = findViewById(R.id.rl_date);
        mRagGender = findViewById(R.id.group_sex);
        mBtnFromFacebook = findViewById(R.id.btn_get_info_from_facebook);
        mBtnFinish = findViewById(R.id.btn_continue);
        // avno
        mViewAVNO = findViewById(R.id.info_avno_layout);
        mViewAVNOUnderLine = findViewById(R.id.info_avno_underline);
        mTvwAVNOLabel = findViewById(R.id.info_avno_label);
        mTvwAVNOValue = findViewById(R.id.info_avno_value);
        mTvwAVNORegister = findViewById(R.id.info_avno_register);
        mMale = findViewById(R.id.tv_male);
        mFemale = findViewById(R.id.tv_female);
//        mTvwUpdate.setEnabled(false);
//        mTvwUpdate.setText(mRes.getString(R.string.update));
        if (fromLoginActivity) {
            mImgBack.setVisibility(View.INVISIBLE);
            mTvwTitle.setText(mRes.getString(R.string.personal_information));
        } else {
            if (isForceUpIdCard()) {
                mImgBack.setVisibility(View.INVISIBLE);
            } else
                mImgBack.setVisibility(View.VISIBLE);
            mTvwTitle.setText(mRes.getString(R.string.setting_pro_update));
        }
        mBtnFinish.setEnabled(true);
        mEdtName.setFilters(new InputFilter[]{filterLength, filter});
//        InputMethodUtils.hideKeyboardWhenTouch(view, mParentActivity);
        /*if (Config.Server.FREE_15_DAYS) {
            mBtnFromFacebook.setVisibility(View.GONE);
        } else {*/
        mBtnFromFacebook.setVisibility(View.VISIBLE);
        //}

        mViewAVNOIdentifyCard = findViewById(R.id.view_identify_card);
//        ivArrow = view.findViewById(R.id.ivShowViewID);
        llImgIDCard = findViewById(R.id.llImgIDCard);
        infoLayout = findViewById(R.id.info_layout);
        mImgDeleteBackCard = findViewById(R.id.img_delete_back);
        mImgDeleteFrontCard = findViewById(R.id.img_delete_front);
        mImgImageBack = findViewById(R.id.img_back_card);
        mImgImageFront = findViewById(R.id.img_front_card);
        mProgressUpBack = findViewById(R.id.progress_upload_back);
        mProgressUpFront = findViewById(R.id.progress_upload_front);

        mHolderImgBack = findViewById(R.id.holder_not_upload_back);
        mHolderImgFront = findViewById(R.id.holder_not_upload_front);

        initLayoutParam();

        mViewAVNOIdentifyCard.setVisibility(View.GONE);
        mBtnFinish.setSelected(true);
    }

    int width;

    private void initLayoutParam() {
        width = 9 * mApplication.getWidthPixels() / 20;
        RelativeLayout.LayoutParams paramsFront = (RelativeLayout.LayoutParams) mImgImageFront
                .getLayoutParams();
        paramsFront.width = width;
        paramsFront.height = Math.round(5f * (((float) width) / 8f));
        mImgImageFront.setLayoutParams(paramsFront);
        Log.i(TAG, "width: " + paramsFront.width + " height: " + paramsFront.height);

        RelativeLayout.LayoutParams paramsBack = (RelativeLayout.LayoutParams) mImgImageBack.getLayoutParams();
        paramsBack.width = width;
        paramsBack.height = Math.round(5f * (((float) width) / 8f));
        mImgImageBack.setLayoutParams(paramsBack);

        RelativeLayout.LayoutParams paramsBack1 = (RelativeLayout.LayoutParams) mHolderImgFront
                .getLayoutParams();
        paramsBack1.width = width;
        paramsBack1.height = Math.round(5f * (((float) width) / 7f));
        mHolderImgFront.setLayoutParams(paramsBack1);

        RelativeLayout.LayoutParams paramsFront1 = (RelativeLayout.LayoutParams) mHolderImgBack
                .getLayoutParams();
        paramsFront1.width = width;
        paramsFront1.height = Math.round(5f * (((float) width) / 7f));
        mHolderImgBack.setLayoutParams(paramsFront1);
        Log.i(TAG, "width: " + paramsFront1.width + " height: " + paramsFront1.height);
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        //        facebookListener = this;
        ListenerHelper.getInstance().addProfileChangeListener(this);
        /*ProfileHelper.drawPersonalInfoAVNO(mApplication, mParentActivity, mViewAVNO,
                mViewAVNOUnderLine, mTvwAVNOLabel, mTvwAVNOValue, mTvwAVNORegister);*/
        drawAVNOIdentifyCard();
        if (isSaveInstanceState && isUpdateSuccess) {
            onEditSuccess(true);
        }
        isUpdateSuccess = false;
        isSaveInstanceState = false;
        super.onResume();
    }

    public void onEditSuccess(boolean isSuccess) {
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    @Override
    public void onStop() {
        mHandler = null;
        ListenerHelper.getInstance().removeProfileChangeListener(this);
        super.onStop();
    }

    private void setListener() {
        setBackListener();
        setButtonContinueListener();
        setImgAvatarListener();
        setEditNameListener();
        setEditDateListener();
        setButtonImportFromFBListener();
        setRadioGenderListener();
        mTvwAVNORegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigateActivityHelper.navigateToChangeAVNO(mApplication, EditProfileFragment.this, AVNOActivity
                        .FINISH_ACTIVITY);
            }
        });

        setImageIdentifyCardListener();
        mMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkGenderEnable(1);
            }
        });
        mFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkGenderEnable(0);
            }
        });
    }

    private void checkGenderEnable(int gender) {
        if (gender == Constants.CONTACT.GENDER_FEMALE) {
            mMale.setSelected(false);
            mFemale.setSelected(true);
            mFemale.setTextColor(mRes.getColor(R.color.colorWhite));
            mMale.setTextColor(mRes.getColor(R.color.v5_text));
        } else {
            mMale.setSelected(true);
            mFemale.setSelected(false);

            mMale.setTextColor(mRes.getColor(R.color.colorWhite));
            mFemale.setTextColor(mRes.getColor(R.color.v5_text));
        }
    }

    private void setImageIdentifyCardListener() {

        mImgDeleteFrontCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "delete front card");
                removeIdentifyCard(true);
            }
        });

        mImgDeleteBackCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "delete back card");
                removeIdentifyCard(false);
            }
        });

        mImgImageFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadOption = ImageProfileConstant.IMAGE_IC_FRONT;
                showPopupChangeAvatar();
            }
        });

        mImgImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadOption = ImageProfileConstant.IMAGE_IC_BACK;
                showPopupChangeAvatar();
            }
        });

        mHolderImgFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                uploadOption = ImageProfileConstant.IMAGE_IC_FRONT;
                showPopupChangeAvatar();
            }
        });

        mHolderImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadOption = ImageProfileConstant.IMAGE_IC_BACK;
                showPopupChangeAvatar();
            }
        });
    }

    private void setBackListener() {
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void removeIdentifyCard(final boolean isFront) {
        showLoadingDialog("", R.string.loading);
        AVNOHelper.getInstance(mApplication).removeIdentifyCard(isFront, new AVNOHelper.RemoveIdentifyCardListener() {
            @Override
            public void onRemoveSuccess() {
                hideLoadingDialog();
                if (isFront) {
                    mAccountBusiness.updateAVNOIdenfityCardFront("");
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            visibleHolderFront(true);
                        }
                    }, 100);
//                    mImgImageFront.setImageResource(R.color.white);
                } else {
                    mAccountBusiness.updateAVNOIdenfityCardBack("");
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            visibleHolderBack(true);
                        }
                    }, 100);

//                    mImgImageBack.setImageResource(R.color.white);
                }
            }

            @Override
            public void onRemoveError(int code, String msg) {
                hideLoadingDialog();
                showToast(msg);
            }
        });
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        Log.d(TAG, "onIconClickListener: " + menuId);
        switch (menuId) {
            /*case Constants.MENU.CAPTURE_IMAGE:
                mListener.takeAPhoto(uploadOption);
                break;
            case Constants.MENU.SELECT_GALLERY:
                mListener.openGallery(uploadOption);
                break;*/
            /*case Constants.MENU.POPUP_SAVE:
                String content = (String) entry;
                Log.d(TAG, "content: " + content);
                FacebookHelper.getInstance().shareLinkOnFbDirect(mParentActivity, content, "Mocha, đong đầy cảm xúc",
                        getString(R.string.link_web_lucky_code_mocha), "");
                break;*/
           /* case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                FacebookHelper.getInstance().shareLinkOnFbDirect(mParentActivity, mRes.getString(R.string
                .fb_share_title), mRes.getString(R.string.fb_share_des),
                        mRes.getString(R.string.fb_share_url), mRes.getString(R.string.fb_share_img_url));
                break;*/
            default:
                break;
        }
    }

    @Override
    public void onProfileChange() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                drawProfile();
            }
        });
    }

    @Override
    public void onRequestFacebookChange(final String userName, final String birthDay, final int gender) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (userName != null && !userName.isEmpty()) {
                    mEdtName.setText(TextHelper.fromHtml(userName));
                    mEdtName.setSelection(mEdtName.getText().toString().length());
                }
                if (birthDay != null && !birthDay.isEmpty()) {
                    mEdtBirthday.setText(birthDay);
                }
                if (gender == Constants.CONTACT.GENDER_FEMALE) {
                    checkGenderEnable(Constants.CONTACT.GENDER_FEMALE);
                } else {
                    checkGenderEnable(1);
                }
                //setEnableContinueButton();
                Log.d(TAG, "onRequestFacebookChange name: " + userName + " birthday: " + birthDay + " gender: " +
                        gender);
            }
        });
    }

    @Override
    public void onAvatarChange(final String avatarPath) {
/*        mHandler.post(new Runnable() {
            @Override
            public void run() {
                drawAvatar(avatarPath);
            }
        });*/
    }

    @Override
    public void onCoverChange(String coverPath) {
        Log.d(TAG, "onCoverChange=" + coverPath);
    }

    private void showDialogBanana() {
        ClickListener.IconListener listener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                // khong lam gi ca
            }
        };
        String title = mRes.getString(R.string.note_title);
        String labelOk = mRes.getString(R.string.close);
        String msg = mRes.getString(R.string.msg_note_banana);
        PopupHelper.getInstance().showDialogConfirm(this, title,
                msg, labelOk, null, listener, null, -1);
    }

    private void drawProfile() {
        ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
        if (mReengAccount != null) {
            String userName = mReengAccount.getName();
            int gender = mReengAccount.getGender();
            if (userName != null && userName.length() > 0) {
                mEdtName.setText(userName);
                mEdtName.setSelection(mEdtName.getText().toString().length());
            } else {
                mEdtName.setText("");
            }
            if (gender == Constants.CONTACT.GENDER_MALE) {
                checkGenderEnable(1);
            } else {
                checkGenderEnable(Constants.CONTACT.GENDER_FEMALE);
            }
            if (!TextUtils.isEmpty(mReengAccount.getBirthdayString())) {
                mEdtBirthday.setText(TimeHelper.getStringBirthday(mReengAccount.getBirthdayString()));
            } else {
                if (mReengAccount.getBirthdayLong() != TimeHelper.BIRTHDAY_DEFAULT) {
                    mEdtBirthday.setText(TimeHelper.formatTimeBirthday(mReengAccount.getBirthdayLong()));
                    mReengAccount.setBirthdayString(TimeHelper.formatTimeBirthdayString(mReengAccount.getBirthdayLong
                            ()));
                }
            }
            //setEnableContinueButton();
            if (!TextUtils.isEmpty(mReengAccount.getLastChangeAvatar())) {
                mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatarDefault,
                        mReengAccount, null);
            }

//            drawAVNOIdentifyCard();

        }
    }

    private void drawAVNOIdentifyCard() {
        if (hideAVNOView)
            mViewAVNOIdentifyCard.setVisibility(View.GONE);
        else {
            mViewAVNOIdentifyCard.setVisibility(View.VISIBLE);

            ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
            if (mReengAccount != null) {
                if (mAccountBusiness.isAvnoEnable() && !TextUtils.isEmpty(mReengAccount.getAvnoNumber())) {
                    llImgIDCard.setVisibility(View.VISIBLE);
//                    ivArrow.setVisibility(View.GONE);
                    String icFront = mAccountBusiness.getCurrentAccount().getAvnoICFront();
                    String icBack = mAccountBusiness.getCurrentAccount().getAvnoICBack();
                    Log.i(TAG, "img: " + icFront + " back: " + icBack);
                    if (!TextUtils.isEmpty(icFront)) {
                        mHolderImgFront.setVisibility(View.INVISIBLE);
                        Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                                + icFront).into(mImgImageFront);
                    } else {
                        mHolderImgFront.setVisibility(View.VISIBLE);
                        infoLayout.requestLayout();
                    }
                    if (!TextUtils.isEmpty(icBack)) {
                        mHolderImgBack.setVisibility(View.INVISIBLE);
                        Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                                + icBack).into(mImgImageBack);
                    } else {
                        infoLayout.requestLayout();
                        mHolderImgBack.setVisibility(View.VISIBLE);
                    }
                } else {
//            mViewAVNOIdentifyCard.setVisibility(View.GONE);
                    boolean enableUploadIdcard = "1".equals(mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.UPLOAD_IDCARD_ENABLE));

            /*if (!enableUploadIdcard || !mApplication.getReengAccountBusiness().isVietnam()) {
                mViewAVNOIdentifyCard.setVisibility(View.GONE);
                ivArrow.setVisibility(View.GONE);
            } else {*/
                    mViewAVNOIdentifyCard.setVisibility(View.VISIBLE);
                    llImgIDCard.setVisibility(View.VISIBLE);
//                    ivArrow.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(icFront)) {
                        mHolderImgFront.setVisibility(View.INVISIBLE);
                    } else {
                        mHolderImgFront.setVisibility(View.VISIBLE);
                    }
                    if (!TextUtils.isEmpty(icBack)) {
                        mHolderImgBack.setVisibility(View.INVISIBLE);
                    } else {
                        mHolderImgBack.setVisibility(View.VISIBLE);
                    }

                    icFront = mReengAccount.getAvnoICFront();
                    icBack = mReengAccount.getAvnoICBack();
                    Log.i(TAG, "img: " + icFront + " back: " + icBack);
                    if (!TextUtils.isEmpty(icFront)) {
                        mHolderImgFront.setVisibility(View.INVISIBLE);
                        Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                                + icFront).into(mImgImageFront);
                    } else {
                        mHolderImgFront.setVisibility(View.VISIBLE);
                        infoLayout.requestLayout();
                        llImgIDCard.requestLayout();

                    }
                    if (!TextUtils.isEmpty(icBack)) {
                        mHolderImgBack.setVisibility(View.INVISIBLE);
                        Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                                + icBack).into(mImgImageBack);
                    } else {
                        mHolderImgBack.setVisibility(View.VISIBLE);
                        infoLayout.requestLayout();
                        llImgIDCard.requestLayout();

                    }
//            }
                }
            }
        }
    }

    String icFront;
    String icBack;

    public void drawAvatar(String filePath) {
        ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
        if (mReengAccount != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatarDefault, mReengAccount,
                            filePath);
                }
            });

        }
    }

    private void setEditDateListener() {
        rlDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (mDatePickerDialog != null && mDatePickerDialog.isShowing()) {
                    return;
                }
                showDateDialog();*/
                showDialogTimePicker();
            }
        });
    }

    private void showDialogTimePicker() {
        String dateChoseDefault;
        if (TextUtils.isEmpty(mEdtBirthday.getText().toString())) {
            dateChoseDefault = TimeHelper.formatTimeBirthday(TimeHelper.BIRTHDAY_DEFAULT_PICKER);
        } else
            dateChoseDefault = mEdtBirthday.getText().toString();

        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);

        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(this, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                mEdtBirthday.setText(dateDesc);
//                Toast.makeText(MainActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
            }
        }).textConfirm("CONFIRM") //text of confirm button
                .textCancel("CANCEL") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(mYear - 100) //min year in loop
                .maxYear(mYear - 12) // max year in loop
                .dateChose(dateChoseDefault) // date chose when init popwindow
                .build();
        pickerPopWin.showPopWin(this);
    }

    private void showDateDialog() {

        Calendar c = Calendar.getInstance();
        long dateLong = TimeHelper.convertBirthdayToTime(mEdtBirthday.getText().toString());
        if (dateLong <= 0) {
            c.setTimeInMillis(883644281000l);
        } else
            c.setTimeInMillis(dateLong);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        //
        Calendar minCalender = Calendar.getInstance();

        Context context = new ContextWrapper(this) {

            private Resources wrappedResources;

            @Override
            public Resources getResources() {
                Resources r = super.getResources();
                if (wrappedResources == null) {
                    wrappedResources = new Resources(r.getAssets(), r.getDisplayMetrics(), r.getConfiguration()) {
                        @NonNull
                        @Override
                        public String getString(int id, Object... formatArgs) throws NotFoundException {
                            try {
                                return super.getString(id, formatArgs);
                            } catch (IllegalFormatConversionException ifce) {
                                Log.e("DatePickerDialogFix", "IllegalFormatConversionException Fixed!", ifce);
                                String template = super.getString(id);
                                template = template.replaceAll("%" + ifce.getConversion(), "%s");
                                return String.format(getConfiguration().locale, template, formatArgs);
                            }
                        }
                    };
                }
                return wrappedResources;
            }
        };

        if (Build.VERSION.SDK_INT >= 18) {
            int minYear = minCalender.get(Calendar.YEAR) - 100;
            int maxYear = minCalender.get(Calendar.YEAR) - 12;
            com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener listener = new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    onMyDateSet(selectedYear, selectedMonth, selectedDay);
                }
            };

            dateSet = false;
            new SpinnerDatePickerDialogBuilder()
                    .context(context)
                    .callback(listener)
                    .spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true)
                    .showDaySpinner(true)
                    .defaultDate(mYear, mMonth, mDay)
                    .maxDate(maxYear, 11, 31)
                    .minDate(minYear, 0, 1)
                    .build()
                    .show();
        } else {
            minCalender.add(Calendar.YEAR, -100);

            Calendar maxCalender = Calendar.getInstance();
            maxCalender.add(Calendar.YEAR, -12);
            maxCalender.set(Calendar.MONTH, Calendar.DECEMBER);
            maxCalender.set(Calendar.DAY_OF_MONTH, 31);

            mDatePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            mDatePickerDialog.dismiss();
                            onMyDateSet(year, month, dayOfMonth);
                        }
                    }, mYear, mMonth, mDay);
            DatePicker datePicker = mDatePickerDialog.getDatePicker();
            datePicker.setMinDate(minCalender.getTimeInMillis());
            datePicker.setMaxDate(maxCalender.getTimeInMillis());
            dateSet = false;
            mDatePickerDialog.show();
        }

    }

    private void onMyDateSet(int selectedYear, int selectedMonth, int selectedDay) {
        Log.d(TAG, "onDateSet");
        if (!dateSet) {
            dateSet = true;
            Calendar cal = Calendar.getInstance();
            cal.set(selectedYear, selectedMonth, selectedDay);
            int old = TimeHelper.getOldFromBirthday(cal.getTimeInMillis());
            if (old > 90) {
                showDialogBanana();
            } else {
                StringBuilder dateString = new StringBuilder().append(selectedDay)
                        .append("/").append(selectedMonth + 1).append("/").append(selectedYear);
                mEdtBirthday.setText(dateString.toString());
                //setEnableContinueButton();
            }
        }
    }

    private void setRadioGenderListener() {
//        mRagGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int id) {
//                ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
//                if (mReengAccount == null || mReengAccount.getLastChangeAvatar() == null) {
//                    switch (id) {
//                        case R.id.radio_sex_male:
//                            // mImgAvatar.setImageResource(R.drawable.ic_male);
//                            break;
//                        case R.id.radio_sex_female:
//                            // mImgAvatar.setImageResource(R.drawable.ic_female);
//                            break;
//                    }
//                }
//                //setEnableContinueButton();
//            }
//        });
    }

    public void setButtonContinueListener() {
        mBtnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mEdtName.getText().toString().trim();
                long birthday = TimeHelper.convertBirthdayToTime(mEdtBirthday.getText().toString());
                Log.d(TAG, "birthday: " + birthday);
                if (!TextUtils.isEmpty(name) && birthday != TimeHelper.BIRTHDAY_DEFAULT) {
                    sendInfoToServerAndUpdateInfo(name, birthday);
                } else if (TextUtils.isEmpty(name)) {
                    showToast(mRes.getString(R.string.msg_validate_name), Toast.LENGTH_LONG);
                } else {// birthday trong
                    showToast(mRes.getString(R.string.msg_validate_birthday), Toast.LENGTH_LONG);
                }
            }
        });
    }

    public void setButtonDoneEnable(boolean enable) {
        mBtnFinish.setEnabled(enable);
    }

    public void setImgAvatarListener() {
        mLLAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadOption = ImageProfileConstant.IMAGE_AVATAR;
                showPopupChangeAvatar();
            }
        });
    }

    public void setEditNameListener() {
        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                //setEnableContinueButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /*mEdtName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mEdtName.clearFocus();
                InputMethodUtils.showSoftKeyboard(EditProfileFragment.this, mEdtName);
                return false;
            }
        });*/
//        mEdtName.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                mEdtName.setSelection(mEdtName.getText().length());
//                return false;
//            }
//        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    /*public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }*/

    public void setButtonImportFromFBListener() {
        mBtnFromFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (mListener != null) {
                    mListener.getInfoFacebook();
                }*/
                FacebookHelper facebookHelper = new FacebookHelper(EditProfileFragment.this);
                facebookHelper.getProfile(EditProfileFragment.this.getCallbackManager(), new FacebookHelper.OnFacebookListener
                        () {
                    @Override
                    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
                        try {
//                            mApplication.getReengAccountBusiness().getCurrentAccount().setFacebookId(userId);
                            // gender
                            ListenerHelper.getInstance().onRequestFacebookChange(name, birthDayStr, gender);
                            mApplication.getAvatarBusiness().downloadAndSaveAvatar(mApplication, EditProfileFragment.this,
                                    userId);
                            Log.d(TAG, "requestGetProfile fullName: " + name + " userId: " + userId + " birthDay: " +
                                    birthDayStr + " gender: " + gender + "=====:");
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            showError(R.string.facebook_get_error, "");
                        }
                    }
                }, FacebookHelper.PendingAction.GET_USER_INFO);
            }
        });
    }

    private void showPopupChangeAvatar() {
        /*ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu selectGallery = new ItemContextMenu(mParentActivity, mRes.getString(R.string
                .select_from_gallery),
                -1, null, Constants.MENU.SELECT_GALLERY);
        ItemContextMenu capture = new ItemContextMenu(mParentActivity, mRes.getString(R.string.capture_image),
                -1, null, Constants.MENU.CAPTURE_IMAGE);
        listItem.add(capture);
        listItem.add(selectGallery);
        PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(), null, listItem, this);*/

        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu openCamera = new ItemContextMenu(mRes.getString(R.string.camera),
                R.drawable.ic_camera_profile_v5, null, Constants.MENU.CAPTURE_IMAGE);
        ItemContextMenu openGallery = new ItemContextMenu(mRes.getString(R.string.select_from_gallery),
                R.drawable.ic_library_image_v5, null, Constants.MENU.SELECT_GALLERY);
        listItem.add(openCamera);
        listItem.add(openGallery);
        new BottomSheetMenu(this, true)
                .setListItem(listItem)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onItemClick(int itemId, Object entry) {
                        switch (itemId) {
                            case Constants.MENU.CAPTURE_IMAGE:
                                takeAPhoto(uploadOption);
                                break;
                            case Constants.MENU.SELECT_GALLERY:
                                openGallery(uploadOption);
                                break;
                        }
                    }
                }).show();
    }

    private void sendInfoToServerAndUpdateInfo(String name, long birthday) {
        //name = TextHelper.getInstant().escapeXml(name);
        String titleFail = mRes.getString(R.string.error);
        if (!NetworkHelper.isConnectInternet(this)) {
            showError(mRes.getString(R.string.error_internet_disconnect), titleFail);
            return;
        }
        showLoadingDialog("", R.string.processing);
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        if (account != null) {
            int gender = 0;
//            if (mRagGender.getCheckedRadioButtonId() == R.id.radio_sex_male) {
//                gender = 1;
//            }
            if (mMale.isSelected() && !mFemale.isSelected()) {
                gender = 1;
            }
            account.setName(name);
            account.setGender(gender);
            account.setBirthday(String.valueOf(birthday));
            account.setBirthdayString(TimeHelper.formatTimeBirthdayString(birthday));
            setUserInfo(account);
        } else {
            hideLoadingDialog();
        }
        if (isNeedUploadAvatar() && !TextUtils.isEmpty(getFileCropTmp())) {
            setNeedUploadAvatar(false);
            mAccountBusiness.processUploadAvatarTask(getFileCropTmp());
        }

        if (fromLoginActivity) {
            if (mAccountBusiness.isInProgressLoginFromAnonymous()) {
                finish();
                mAccountBusiness.setInProgressLoginFromAnonymous(false);
                ListenerHelper.getInstance().onLoginFromAnonymous();
            } else
                goToHome();
        }
    }

    private void setUserInfo(final ReengAccount account) {
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                hideLoadingDialog();
                showToast(R.string.update_info_ok);
                if (!TextUtils.isEmpty(account.getLastChangeAvatar())) {
                    mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatarDefault, account,
                            null);
                }
                if (isSaveInstanceState) {
                    isUpdateSuccess = true;
                } else {
                    onEditSuccess(true);
                }
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "onError: " + errorCode);
                hideLoadingDialog();
                showToast(R.string.update_infor_fail);
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setUserInfo(account, listener);
    }

    /*private void finishFragment() {
        mParentActivity.getSupportFragmentManager().beginTransaction().remove(this).commit();
    }*/

    public void uploadImageIdentifyCard(String filepath, int uploadOption) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            showToast(R.string.no_connectivity);
            return;
        }
        if (uploadOption == ImageProfileConstant.IMAGE_IC_FRONT) {
            mProgressUpFront.setVisibility(View.VISIBLE);
            transferFileBusiness.uploadIdentityCard(filepath, true, new UploadIdentifyCardListener() {
                @Override
                public void onUploadSuccess(String link) {
                    String newlink = link + "?t=" + System.currentTimeMillis();
                    Log.i(TAG, "newlink: " + newlink);
                    mProgressUpFront.setVisibility(View.GONE);
                    visibleHolderFront(false);
                    Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                            + newlink).into(mImgImageFront);
                    mAccountBusiness.updateAVNOIdenfityCardFront(newlink);
                }

                @Override
                public void onError(int code, String msg) {
                    showToast(msg);
                    mProgressUpFront.setVisibility(View.GONE);
                }
            });
        } else {
            mProgressUpBack.setVisibility(View.VISIBLE);
            transferFileBusiness.uploadIdentityCard(filepath, false, new UploadIdentifyCardListener() {
                @Override
                public void onUploadSuccess(String link) {
                    String newlink = link + "?t=" + System.currentTimeMillis();
                    Log.i(TAG, "newlink: " + newlink);
                    mProgressUpBack.setVisibility(View.GONE);
                    visibleHolderBack(false);
                    Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                            + newlink).into(mImgImageBack);
                    mAccountBusiness.updateAVNOIdenfityCardBack(newlink);
                }

                @Override
                public void onError(int code, String msg) {
                    showToast(msg);
                    mProgressUpBack.setVisibility(View.GONE);
                }
            });
        }
    }

    private void visibleHolderBack(boolean isVisible) {
        if (isVisible) {
            mHolderImgBack.setVisibility(View.VISIBLE);
            mImgImageBack.setVisibility(View.GONE);
            mImgDeleteBackCard.setVisibility(View.GONE);
        } else {
            mHolderImgBack.setVisibility(View.INVISIBLE);
            mImgImageBack.setVisibility(View.VISIBLE);
            mImgDeleteBackCard.setVisibility(View.VISIBLE);
        }
        mImgDeleteBackCard.invalidate();
        mHolderImgBack.invalidate();
    }

    private void visibleHolderFront(boolean isVisible) {
        if (isVisible) {
            mHolderImgFront.setVisibility(View.VISIBLE);
            mImgImageFront.setVisibility(View.GONE);
            mImgDeleteFrontCard.setVisibility(View.GONE);
        } else {
            mHolderImgFront.setVisibility(View.INVISIBLE);
            mImgImageFront.setVisibility(View.VISIBLE);
            mImgDeleteFrontCard.setVisibility(View.VISIBLE);
        }
    }

    public void getUserInfo(ReengAccount account) {
        showLoadingDialog("", R.string.loading);
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                hideLoadingDialog();
                mApplication.getAvatarBusiness().setMyAvatarFromGetUserInfo(mImgAvatar,
                        mTvwAvatar, mTvwAvatarDefault, account);
                drawProfile();
                drawAVNOIdentifyCard();
            }

            @Override
            public void onError(int errorCode) {
                hideLoadingDialog();
            }
        };
        ProfileRequestHelper.getInstance(mApplication).getUserInfo(account, listener);
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressedđ");
        if (!needUploadAvatar) {
            if (forceUpIdCard) {
                if (!TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getAvnoICBack()) &&
                        !TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getAvnoICFront())) {
                    super.onBackPressed();
                } else {
                    showToast(R.string.must_upload_id_card);
                }
            } else {
                super.onBackPressed();
            }
        } else {
            showDialogConfirmUploadAvatar();
        }
    }

    public interface UploadIdentifyCardListener {
        void onUploadSuccess(String filePath);

        void onError(int code, String msg);
    }

    public boolean isForceUpIdCard() {
        return forceUpIdCard;
    }

    public String getFileCropTmp() {
        return fileCropTmp;
    }

    public boolean isNeedUploadAvatar() {
        return needUploadAvatar;
    }

    public void setNeedUploadAvatar(boolean needUploadAvatar) {
        this.needUploadAvatar = needUploadAvatar;
    }

    public void showDialogConfirmUploadAvatar() {
        String msg = getResources().getString(R.string.avatar_update_notify);
        DialogConfirm dialog = new DialogConfirm(this, true)
                .setLabel(null)
                .setMessage(msg)
                .setNegativeLabel(getResources().getString(R.string.cancel))
                .setPositiveLabel(getResources().getString(R.string.ok))
                .setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        needUploadAvatar = false;
                        finish();
                    }
                });
        dialog.show();
    }

    public void setAvatarEditProfile(String filePath) {
        Log.i(TAG, "setAvatarEditProfile");
        fileCropTmp = filePath;
        needUploadAvatar = true;
        drawAvatar(filePath);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean isHD = SettingBusiness.getInstance(getApplicationContext()).getPrefEnableHDImage();
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                // get information from facebook
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        Log.d(TAG, "onActivityResult ACTION_PICK_PICTURE");
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                                String pathImage = picturePath.get(0);
                                cropAvatarImage(pathImage);
                            } else if (typeUploadImage == ImageProfileConstant.IMAGE_IC_FRONT ||
                                    typeUploadImage == ImageProfileConstant.IMAGE_IC_BACK) {
                                uploadImageIdentifyCard(picturePath.get(0), typeUploadImage);
                            } /*else {
                                showLoadingDialog("", R.string.text_uploading);
                                mApplication.getTransferFileBusiness().uploadAlbumImage(picturePath, this);
                            }*/
                        }
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    //Uri takeImageUri;
                    File imageFile = new File(mApplication.getPref().getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                        cropAvatarImage(imageFile.getPath());
                    } else if (typeUploadImage == ImageProfileConstant.IMAGE_IC_FRONT ||
                            typeUploadImage == ImageProfileConstant.IMAGE_IC_BACK) {
                        uploadImageIdentifyCard(imageFile.getPath(), typeUploadImage);
                    } /*else {
                        ArrayList<String> picturePath = new ArrayList<>();
                        picturePath.add(imageFile.getPath());
                        mApplication.getTransferFileBusiness().uploadAlbumImage(picturePath, this);
                        showLoadingDialog("", R.string.text_uploading);
                    }*/
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mApplication.getPref().getString(PREF_AVATAR_FILE_CROP, "");
                        setAvatarEditProfile(fileCrop);
                        if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                            Log.i(TAG, "setAvatarEditProfile");
                            setButtonDoneEnable(true);
//                            mAccountBusiness.processUploadAvatarTask(fileCrop);
                            Log.i(TAG, "upload anh IMAGE_AVATAR sau khi crop");
                        } /*else if (typeUploadImage == ImageProfileConstant.IMAGE_COVER) {
                            Log.i(TAG, "upload anh IMAGE_COVER sau khi crop path=" + fileCrop);
                            fileCrop = MessageHelper.getPathOfCompressedFile(fileCrop,
                                    Config.Storage.PROFILE_PATH, "", isHD);
                            mApplication.getTransferFileBusiness().uploadCover(fileCrop, uploadCoverListener);
                            showLoadingDialog("", R.string.text_uploading);
                        }*/ /*else if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL) {
                            Log.i(TAG, "upload anh IMAGE_NORMAL sau khi crop");
                            ArrayList<String> picturePath = new ArrayList<>();
                            picturePath.add(fileCrop);
                            mApplication.getTransferFileBusiness().uploadAlbumImage(picturePath, this);
                            showLoadingDialog("", R.string.text_uploading);
                        }*/
                    }
                    break;


                default:
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void takeAPhoto(int typeUpload) {
        typeTakePhoto = typeUpload;
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto(typeUpload);
        }
    }

    private void takePhoto(int typeUpload) {
        try {
            typeUploadImage = typeUpload;
            mAccountBusiness.removeFileFromProfileDir();
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            intent.putExtra("return-data", true);
            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, takePhotoFile.toString()).apply();
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    public void openGallery(int typeUpload) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    public void openGallery(int typeUpload, int maxImages) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, maxImages);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    private void cropAvatarImage(String inputFilePath) {
        try {
            String time = String.valueOf(System.currentTimeMillis());
            File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "/avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            mApplication.getPref().edit().putString(PREF_AVATAR_FILE_CROP, cropImageFile.toString()).apply();
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                intent.putExtra(CropView.MASK_OVAL, true);
            }
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);

        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO &&
                PermissionHelper.verifyPermissions(grantResults)) {
            takePhoto(typeTakePhoto);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
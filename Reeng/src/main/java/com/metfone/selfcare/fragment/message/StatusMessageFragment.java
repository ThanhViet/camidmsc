package com.metfone.selfcare.fragment.message;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.metfone.selfcare.activity.MessageDetailActivity;
import com.metfone.selfcare.adapter.EventMessageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.ConfigGroupListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by toanvk2 on 8/25/14.
 */
public class StatusMessageFragment extends Fragment implements
        ConfigGroupListener,
        InitDataListener,
        ContactChangeListener {
    private static final String TAG = StatusMessageFragment.class.getSimpleName();
    private MessageDetailActivity mParentActivity;
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ContactBusiness mContactBusiness;
    private Handler mHandler;
    private Resources mRes;
    private ThreadMessage mThreadMessage;
    private ReengMessage mReengMessage;
    private int mThreadId, mMessageId, mThreadType;
    private EllipsisTextView mTvName;
    private TextView mTvListFriends, mTvwStatusSent;
    private View mViewAbProfile, mViewBackLayout;
    private ProgressLoading mPgrProgressWait;
    private String mActionbarStatus;
    private EventMessageAdapter mAdapter;
    private ListView mListView;
    private ArrayList<EventMessage> mListEventMessages;

    public StatusMessageFragment() {
        // Required empty public constructor
    }

    public static StatusMessageFragment newInstance(int threadId, int messageId, String actionbarStatus) {
        StatusMessageFragment fragment = new StatusMessageFragment();
        Bundle args = new Bundle();
        args.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        args.putInt(ReengMessageConstant.MESSAGE_ID, messageId);
        args.putString("ab_status", actionbarStatus);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
        Log.i(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_status_message, container, false);
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        mRes = mParentActivity.getResources();
        findComponentViews(rootView, inflater, container);
        setListener();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
            mMessageId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_ID);
            mActionbarStatus = savedInstanceState.getString("ab_status");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (MessageDetailActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        ListenerHelper.getInstance().addInitDataListener(this);
        ListenerHelper.getInstance().addContactChangeListener(this);
        Log.i(TAG, "onResume");
        if (mHandler == null) {
            mHandler = new Handler();
        }
        if (mApplication.isDataReady()) {
            mPgrProgressWait.setVisibility(View.GONE);
            mMessageBusiness.addConfigGroupListener(this);
            getDataAndDraw();
        } else {
            mPgrProgressWait.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        mMessageBusiness.removeConfigGroupListener(this);
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, mThreadId);
        outState.putInt(ReengMessageConstant.MESSAGE_ID, mMessageId);
        outState.putString("ab_status", mActionbarStatus);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
//        mParentActivity.getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        mHandler = null;
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) {
            return;
        }
        mMessageBusiness.addConfigGroupListener(this);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mPgrProgressWait.setVisibility(View.GONE);
                getDataAndDraw();
            }
        });
    }

    @Override
    public void onConfigGroupChange(ThreadMessage threadMessage, int actionChange) {
        if (mThreadMessage == null)
            return;
        if (threadMessage.getId() != mThreadMessage.getId())
            return;
        if (actionChange == Constants.MESSAGE.CHANGE_GROUP_MEMBER) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTvListFriends.setText(mContactBusiness.getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
                }
            });
        } else if (actionChange == Constants.MESSAGE.CHANGE_GROUP_NAME) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTvName.setText(mApplication.getMessageBusiness().getThreadName(mThreadMessage));
                }
            });
        }
    }

    @Override
    public void onConfigRoomChange(String roomId, boolean unFollow) {

    }

    @Override
    public void onContactChange() {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    drawActionbar();
                }
            });
        }
    }

    @Override
    public void onPresenceChange(ArrayList<PhoneNumber> phoneNumbers) {
        if (mHandler != null && mThreadMessage != null &&
                mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String sender = mThreadMessage.getSoloNumber();
            for (PhoneNumber phoneNumber : phoneNumbers)
                if (sender != null && sender.equals(phoneNumber.getJidNumber())) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            drawActionbar();
                        }
                    });
                    return;
                }
        }
    }

    @Override
    public void onRosterChange() {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    drawActionbar();
                }
            });
        }
    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        //mParentActivity.getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        mListView = (ListView) rootView.findViewById(R.id.status_message_listview);
        mPgrProgressWait = (ProgressLoading) rootView.findViewById(R.id.fragment_loading_progressbar);
        mTvwStatusSent = (TextView) rootView.findViewById(R.id.status_message_sent_txt);
        View abView = mParentActivity.getToolBarView();
        mTvName = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        mTvListFriends = (TextView) abView.findViewById(R.id.ab_status_text);
        ImageButton mIbnCall = (ImageButton) abView.findViewById(R.id.message_menu_call);
        ImageButton mIbnAttach = (ImageButton) abView.findViewById(R.id.message_menu_attach);
        ImageButton mIbnVideoCall = (ImageButton) abView.findViewById(R.id.message_menu_video_call);
        mViewAbProfile = abView.findViewById(R.id.ab_profile_chat_layout);
        mViewBackLayout = abView.findViewById(R.id.ab_back_layout);
        mIbnCall.setVisibility(View.GONE);
        mPgrProgressWait.setVisibility(View.GONE);
        mIbnAttach.setVisibility(View.GONE);
        mIbnVideoCall.setVisibility(View.GONE);
    }

    private void setListener() {
        setButtonBackListener();
        mViewAbProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void setButtonBackListener() {
        mViewBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParentActivity.onBackPressed();
            }
        });
    }

    private void getDataAndDraw() {
        if (getArguments() != null) {
            mThreadId = getArguments().getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
            mMessageId = getArguments().getInt(ReengMessageConstant.MESSAGE_ID);
            mActionbarStatus = getArguments().getString("ab_status");
        }
        mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        mReengMessage = mMessageBusiness.getMessageByMessageIdInThread(mMessageId, mThreadMessage);
        if (mThreadMessage == null || mReengMessage == null) {
            return;
        }
        mThreadType = mThreadMessage.getThreadType();
        drawActionbar();
        // get data from database
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT ||
                mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT ||
                mReengMessage.getStatus() == ReengMessageConstant.STATUS_SENT) {
            mListView.setVisibility(View.GONE);
            mTvwStatusSent.setVisibility(View.VISIBLE);
            if (mReengMessage.getStatus() == ReengMessageConstant.STATUS_SENT) {
                mTvwStatusSent.setText(mRes.getString(R.string.sent) + " " +
                        TimeHelper.formatTimeEventMessage(mReengMessage.getTime()));
            } else if (mReengMessage.getStatus() == ReengMessageConstant.STATUS_DELIVERED) {
                mTvwStatusSent.setText(mRes.getString(R.string.delivered) + " " +
                        TimeHelper.formatTimeEventMessage(mReengMessage.getTime()));
            } else {
                mTvwStatusSent.setText(mRes.getString(R.string.seen) + " " +
                        TimeHelper.formatTimeEventMessage(mReengMessage.getTime()));
            }
        } else {
            mListView.setVisibility(View.VISIBLE);
            mTvwStatusSent.setVisibility(View.GONE);
            getListEventMessages();
        }
    }

    private void drawActionbar() {
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            //draw ab of group
            mTvListFriends.setVisibility(View.VISIBLE);
            mTvName.setText(mApplication.getMessageBusiness().getThreadName(mThreadMessage));
            mTvListFriends.setText(mContactBusiness.getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String sender = mThreadMessage.getSoloNumber();
            String senderName = mMessageBusiness.getFriendName(sender);
            mTvName.setText(senderName);
            if (TextUtils.isEmpty(mActionbarStatus)) {
                mTvListFriends.setVisibility(View.GONE);
            } else {
                mTvListFriends.setVisibility(View.VISIBLE);
                mTvListFriends.setText(mActionbarStatus);
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            //draw ab of group
            mTvListFriends.setVisibility(View.VISIBLE);
            mTvName.setText(mApplication.getMessageBusiness().getThreadName(mThreadMessage));
            mTvListFriends.setText(mContactBusiness.getListNameOfListNumber(mThreadMessage.getPhoneNumbers()));
            mTvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_ab_broadcast, 0);
        }
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new EventMessageAdapter(mParentActivity, mListEventMessages, mThreadType);
            mListView.setAdapter(mAdapter);
        } else {
            mAdapter.setListEventMessages(mListEventMessages);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void getListEventMessages() {
        ArrayList<EventMessage> listEventMessages = mMessageBusiness.getListEventMessageFromDBByReengMessage
                (mReengMessage);
        HashSet<EventMessage> removeDuplicateListEvents = new HashSet<>(listEventMessages);
        ArrayList<EventMessage> listEventMessageSeen = getListEventMessageSeen(removeDuplicateListEvents);
        ArrayList<EventMessage> listEventMessageDelivered = getListEventMessageDeliver(removeDuplicateListEvents,
                listEventMessageSeen);
        mListEventMessages = new ArrayList<>();
        if (listEventMessageSeen != null && !listEventMessageSeen.isEmpty()) {
            mListEventMessages.addAll(listEventMessageSeen);
        }
        if (listEventMessageDelivered != null && !listEventMessageDelivered.isEmpty()) {
            mListEventMessages.addAll(listEventMessageDelivered);
        }
        if (listEventMessages.isEmpty()) {   // khong lay duoc tu db
            mListEventMessages.add(createEventMessagesIfDBNull());
        }

        setAdapter();
    }

    private EventMessage createEventMessagesIfDBNull() {
        int status = mReengMessage.getStatus();
        long time = mReengMessage.getTime();
        String member;
        if (mThreadMessage.getPhoneNumbers() != null && mThreadMessage.getPhoneNumbers().size() > 0) {
            member = mThreadMessage.getPhoneNumbers().get(0);
        } else {
            member = "";
        }
        EventMessage eventMessage = new EventMessage();
        eventMessage.setSender(member);
        eventMessage.setStatus(status);
        eventMessage.setTime(time);
        return eventMessage;
    }

    private ArrayList<EventMessage> getListEventMessageSeen(HashSet<EventMessage> listEventMessages) {
        if (listEventMessages == null || listEventMessages.isEmpty()) {
            return null;
        } else {
            ArrayList<EventMessage> listEventSeens = new ArrayList<>();
            for (EventMessage eventMessage : listEventMessages) {
                if (eventMessage.getStatus() == ReengMessageConstant.STATUS_SEEN) {
                    listEventSeens.add(eventMessage);
                }
            }
            if (!listEventSeens.isEmpty())
                Collections.sort(listEventSeens, ComparatorHelper.getComparatorEventMessageByLastTime());
            return listEventSeens;
        }
    }

    private ArrayList<EventMessage> getListEventMessageDeliver(HashSet<EventMessage> listEventMessages,
                                                               ArrayList<EventMessage> listEventSeens) {
        if (listEventMessages == null || listEventMessages.isEmpty()) {
            return null;
        } else if (listEventSeens == null || listEventSeens.isEmpty()) {
            ArrayList<EventMessage> list = new ArrayList<>(listEventMessages);
            if (!list.isEmpty())
                Collections.sort(list, ComparatorHelper.getComparatorEventMessageByLastTime());
            return list;
        } else {
            ArrayList<EventMessage> listEventDelivered = new ArrayList<>();
            for (EventMessage eventMessage : listEventMessages) {
                // neu da nhan dc seen thi ko hien deliver nua
                if (eventMessage.getStatus() == ReengMessageConstant.STATUS_DELIVERED &&
                        !checkDuplicateSender(eventMessage, listEventSeens)) {
                    listEventDelivered.add(eventMessage);
                }
            }
            if (!listEventDelivered.isEmpty())
                Collections.sort(listEventDelivered, ComparatorHelper.getComparatorEventMessageByLastTime());
            return listEventDelivered;
        }
    }

    private boolean checkDuplicateSender(EventMessage eventMessage, ArrayList<EventMessage> listEventMessages) {
        if (listEventMessages == null || listEventMessages.isEmpty()) {
            return false;
        }
        for (EventMessage item : listEventMessages) {
            if (item.getSender().equals(eventMessage.getSender())) {
                return true;
            }
        }
        return false;
    }
}
package com.metfone.selfcare.fragment.message;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PollSettingFragmentV2 extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.ll_allows_multiple_options)
    LinearLayout llAllowsMultipleOptions;
    @BindView(R.id.tb_allows_multiple_options)
    ToggleButton tbAllowsMultipleOptions;
    @BindView(R.id.tb_ballot)
    ToggleButton tbBallot;
    @BindView(R.id.ll_allow_more_options)
    LinearLayout llAllowMoreOptions;
    @BindView(R.id.tb_allow_more_options)
    ToggleButton tbAllowMoreOptions;
    @BindView(R.id.tv_info_allow_more_options)
    TextView tvInfoAllowMoreOptions;
    @BindView(R.id.tb_survey_time_limits)
    ToggleButton tbSurveyTimeLimits;
    @BindView(R.id.tv_info_allows_multiple_options)
    TextView tvInfoAllowsMultipleOptions;
    @BindView(R.id.tv_info_ballot)
    TextView tvInfoBallot;
    @BindView(R.id.tv_info_survey_time_limits)
    TextView tvInfoSurveyTimeLimits;
    @BindView(R.id.btnCreateSurvey)
    Button btnCreateSurvey;
    Unbinder unbinder;

    @SuppressLint("StaticFieldLeak")
    private static PollSettingFragmentV2 self;
    @BindView(R.id.tb_pin_survey)
    ToggleButton tbPinSurvey;
    @BindView(R.id.ll_pin_survey)
    LinearLayout llPinSurvey;
    @BindView(R.id.tv_info_pin_survey)
    TextView tvInfoPinSurvey;

    private static PollSettingFragmentV2 self() {
        return self;
    }

    public static PollSettingFragmentV2 newInstance(int threadId, String name, ArrayList<String> options) {
        Bundle args = new Bundle();
        args.putStringArrayList(Constants.MESSAGE.POLL_OPTIONS, options);
        args.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        args.putString(Constants.MESSAGE.POLL_NAME, name);
        PollSettingFragmentV2 fragment = new PollSettingFragmentV2();
        fragment.setArguments(args);
        return fragment;
    }

    private View btnBack;
    private TextView btnSend;

    private ThreadMessage mThreadMessage;
    private ArrayList<String> options;
    private String myNumber;
    private String name;
    private int mThreadId;

    private int minute;
    private int hourOfDay;
    private int year;
    private int monthOfYear;
    private int dayOfMonth;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        options = getArguments() != null ? getArguments().getStringArrayList(Constants.MESSAGE.POLL_OPTIONS) : new ArrayList<String>();
        mThreadId = getArguments() != null ? getArguments().getInt(ReengMessageConstant.MESSAGE_THREAD_ID) : 0;
        name = getArguments().getString(Constants.MESSAGE.POLL_NAME);
        mThreadMessage = application.getMessageBusiness().getThreadById(mThreadId);
        myNumber = application.getReengAccountBusiness().getJidNumber();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_poll_setting_v2, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    boolean pinVote;

    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            activity.onBackPressed();
        } else if (v == btnCreateSurvey) {
            pinVote = tbPinSurvey.isChecked();
            if (pinVote) {
                if (mThreadMessage.isPrivateThread() && !mThreadMessage.isAdmin()) {
                    activity.showToast(R.string.poll_pin_in_private_group);
                    pinVote = false;
                    onCreatePoll(false);
                    return;
                }
                if (mThreadMessage.getPinMessage() != null
                        && !TextUtils.isEmpty(mThreadMessage.getPinMessage().getContent())) {
                    DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
                    dialogConfirm.setMessage(activity.getResources().getString(R.string.poll_change_pin));
                    dialogConfirm.setPositiveLabel(activity.getResources().getString(R.string.poll_pin_this_survey));
                    dialogConfirm.setNegativeLabel(activity.getResources().getString(R.string.no));
                    dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            application.getMessageBusiness().sendPinMessage(mThreadMessage, null, activity, true);
                            mThreadMessage.setPinMessage("");
                            application.getMessageBusiness().updateThreadMessage(mThreadMessage);
                            onCreatePoll(true);
                        }
                    });
                    dialogConfirm.setNegativeListener(new NegativeListener() {
                        @Override
                        public void onNegative(Object result) {
                            onCreatePoll(false);
                        }
                    });
                    dialogConfirm.show();
                    return;
                }
            }
            onCreatePoll(pinVote);

        } else if (v == tbSurveyTimeLimits) {
            if (tbSurveyTimeLimits.isChecked()) {
                openTimePickerDialog();
            } else {
                updateUiSetting();
            }
        } else if (v == tbAllowsMultipleOptions) {
            if (tbAllowsMultipleOptions.isChecked()) {
                tbBallot.setChecked(false);
            }
            updateUiSetting();
        } else if (v == tbAllowMoreOptions) {
            if (tbAllowMoreOptions.isChecked()) {
                tbBallot.setChecked(false);
            }
            updateUiSetting();
        } else if (v == tbBallot) {
            if (tbBallot.isChecked()) {
                tbAllowsMultipleOptions.setChecked(false);
                tbAllowMoreOptions.setChecked(false);
            }
            updateUiSetting();
        }
    }

    private void onCreatePoll(boolean pinVote) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth, hourOfDay, minute);

        boolean anonymous = tbBallot.isChecked();
        boolean choice = !anonymous && tbAllowsMultipleOptions.isChecked();
        boolean enableAddVote = !anonymous && tbAllowMoreOptions.isChecked();

        long expire = calendar.getTimeInMillis();

        if (year == 0 && monthOfYear == 0 && dayOfMonth == 0 && hourOfDay == 0 && minute == 0) {
            expire = -1;
        }
        if (!tbSurveyTimeLimits.isChecked()) {
            expire = -1;
        }

        activity.showLoadingDialog(null, R.string.waiting);
        PollRequestHelper.getInstance(application).createPollV3(mThreadMessage,
                name,
                options,
                choice ? String.valueOf(options.size()) : "1",
                anonymous ? "1" : "0",
                enableAddVote ? "1" : "0",
                expire <= 0 ? "" : expire + "",
                pinVote ? PollObject.STATUS_PIN : PollObject.STATUS_UNPIN,
                new ApiCallbackV2<PollObject>() {
                    @Override
                    public void onSuccess(String lastId, PollObject pollObject) throws JSONException {
                        activity.hideLoadingDialog();
                        MessageBusiness mMessageBusiness = application.getMessageBusiness();
                        ReengMessage reengMessage = mMessageBusiness.createMessagePollAfterRequest(mThreadMessage, pollObject, true, false);
                        HashMap<String, Integer> hashMap = mThreadMessage.getPollLastId();
                        if (hashMap.containsKey(reengMessage.getFileId())) {
                            int lastIdPoll = hashMap.get(reengMessage.getFileId());
                            if (lastIdPoll < reengMessage.getId())
                                hashMap.put(reengMessage.getFileId(), reengMessage.getId());
                        } else {
                            hashMap.put(reengMessage.getFileId(), reengMessage.getId());
                        }

                        mMessageBusiness.notifyNewMessage(reengMessage, mThreadMessage);
                        // TODO: 5/29/2020 Group Servey
                        LuckyWheelHelper.getInstance(ApplicationController.self()).doMission(Constants.LUCKY_WHEEL.ITEM_GROUP_SURVEY);
                        activity.onBackPressed();
                    }

                    @Override
                    public void onError(String s) {
                        activity.hideLoadingDialog();
                        try {
                            int errorCode = Integer.valueOf(s);
                            if (errorCode == 418) {
                                Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.poll_create_expire), Toast.LENGTH_LONG).show();
//                                    activity.showToast(R.string.poll_create_expire);
                            } else {
                                Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
//                                    activity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Toast.makeText(ApplicationController.self(), ApplicationController.self().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG).show();
//                                activity.showToast(R.string.e601_error_but_undefined);
                        }
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void openTimePickerDialog() {
        Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                PollSettingFragmentV2.this.minute = minute;
                PollSettingFragmentV2.this.hourOfDay = hourOfDay;
                openDatePickerDialog();
            }
        }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void openDatePickerDialog() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                PollSettingFragmentV2.this.year = year;
                PollSettingFragmentV2.this.monthOfYear = monthOfYear;
                PollSettingFragmentV2.this.dayOfMonth = dayOfMonth;
                updateUiSetting();
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void initView() {
        initToolBar();
        tbAllowsMultipleOptions.setOnClickListener(this);
        tbBallot.setOnClickListener(this);
        tbAllowMoreOptions.setOnClickListener(this);
        tbSurveyTimeLimits.setOnClickListener(this);

        tbAllowsMultipleOptions.setChecked(true);
        tbBallot.setChecked(false);
        tbAllowMoreOptions.setChecked(true);
        tbPinSurvey.setChecked(Config.Features.FLAG_SUPPORT_PIN_VOTE ? true : false);
        tbSurveyTimeLimits.setChecked(false);

        updateUiSetting();
        if (Config.Features.FLAG_SUPPORT_PIN_VOTE) {
            llPinSurvey.setVisibility(View.VISIBLE);
            tvInfoPinSurvey.setVisibility(View.VISIBLE);
        } else {
            llPinSurvey.setVisibility(View.GONE);
            tvInfoPinSurvey.setVisibility(View.GONE);
        }
    }

    @SuppressLint("InflateParams")
    private void initToolBar() {
        activity.setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_vote_detail, null));
        TextView tvTitle = activity.getToolBarView().findViewById(R.id.ab_title);
        TextView tvStatus = activity.getToolBarView().findViewById(R.id.ab_status_text);
        btnBack = activity.getToolBarView().findViewById(R.id.ab_back_btn);
        btnSend = activity.getToolBarView().findViewById(R.id.ab_agree_text);

        tvTitle.setText(R.string.poll_create_title);
        btnSend.setTextColor(ContextCompat.getColor(application, R.color.videoColorSelect));
        tvStatus.setVisibility(View.GONE);

        btnBack.setOnClickListener(this);
        btnCreateSurvey.setOnClickListener(this);
    }

    private void updateUiSetting() {
        if (tbAllowsMultipleOptions.isChecked()) {
            tvInfoAllowsMultipleOptions.setText(R.string.poll_setting_info_chose_allows_multiple_options);
        } else {
            tvInfoAllowsMultipleOptions.setText(R.string.poll_setting_info_allows_multiple_options);
        }
        if (tbBallot.isChecked()) {
//            llAllowsMultipleOptions.setVisibility(View.GONE);
//            tvInfoAllowsMultipleOptions.setVisibility(View.GONE);
//
//            llAllowMoreOptions.setVisibility(View.GONE);
//            tvInfoAllowMoreOptions.setVisibility(View.GONE);

            tvInfoBallot.setText(R.string.poll_setting_info_chose_ballot);
        } else {
//            llAllowsMultipleOptions.setVisibility(View.VISIBLE);
//            tvInfoAllowsMultipleOptions.setVisibility(View.VISIBLE);
//
//            llAllowMoreOptions.setVisibility(View.VISIBLE);
//            tvInfoAllowMoreOptions.setVisibility(View.VISIBLE);

            tvInfoBallot.setText(R.string.poll_setting_info_ballot);
        }
        if (tbAllowMoreOptions.isChecked()) {
            tvInfoAllowMoreOptions.setText(R.string.poll_setting_info_chose_allow_more_options);
        } else {
            tvInfoAllowMoreOptions.setText(R.string.poll_setting_info_allow_more_options);
        }
        if (tbSurveyTimeLimits.isChecked()) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth, hourOfDay, minute);
            tvInfoSurveyTimeLimits.setText(String.format(getString(R.string.poll_setting_info_chose_survey_time_limits), TimeHelper.formatSeparatorTimeOfMessage(calendar.getTimeInMillis(), getResources())));
        } else {
            tvInfoSurveyTimeLimits.setText(R.string.poll_setting_info_survey_time_limits);
        }
    }
}

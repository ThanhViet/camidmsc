package com.metfone.selfcare.fragment.login;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;

public class OTPForgotPasswordFragment extends Fragment implements ClickListener.IconListener,
        MySMSBroadcastReceiver.OTPReceiveListener {

    private static final String TAG = OTPLoginFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PHONE_NUMBER_PARAM = "param1";
    private static final String REGION_CODE_PARAM = "param2";
    private static final String CODE_PARAM = "code";
    //    private CountDownTimer countDownTimer;
    private static final int TIME_DEFAULT = 60000;
    private  static final int REQ_USER_CONSENT = 6;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.scr_password)
    ScrollView scr_password;
    @BindView(R.id.tvPhoneNumber)
    CamIdTextView tvPhoneNumber;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.tvNotReceivedOtp)
    CamIdTextView tvNotReceivedOtp;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.tv_error)
    AppCompatTextView tvError;
    @BindView(R.id.tvEnterNumber)
    CamIdTextView tvEnterNumber;
    CamIdTextView btnContinue;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    Unbinder unbinder;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private int codeGenOtp;
    private String password = "";
    private String phoneNumber = "";
    private OTPLoginFragment.OnFragmentInteractionListener mListener;
    private LoginPassWordFragment.OnFragmentInteractionListener fragmentInteractionListener;
    private LoginActivity mLoginActivity;
    private ApplicationController mApplication;
    private Resources mRes;
    private boolean isPhoneExisted = false;
    private ClickListener.IconListener mClickHandler;
    //------------Variable Views-------------------
    private Handler mHandler;
    private int START_TEXT = 85;
    private int END_TEXT = 0;
    private boolean isSaveInstanceState = false;
    private boolean isLoginDone = false;
    private long countDownTimer;
    private int remain;
    private MySMSBroadcastReceiver mySMSBroadcastReceiver;

    public static OTPForgotPasswordFragment newInstance(String phoneNumber) {
        OTPForgotPasswordFragment fragment = new OTPForgotPasswordFragment();
        Bundle args = new Bundle();
        args.putString(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        mLoginActivity.hideLoadingDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);

        unbinder = ButterKnife.bind(this, rootView);
        btnContinue = rootView.findViewById(R.id.btnContinue);
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        mLoginActivity.registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
        mySMSBroadcastReceiver.initOTPListener(this);
        fragmentInteractionListener = (LoginPassWordFragment.OnFragmentInteractionListener) getActivity();
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
            tvPhoneNumber.setText(phoneNumber.substring(0,3)+" "+phoneNumber.substring(3));

        }

        setViewListeners();

        findComponentViews();
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etOtp.getText().toString())) {
                    ToastUtils.showToast(getContext(), getString(R.string.txt_otp_empty));
                } else if (etOtp.getText().toString().length() < 6) {
                    ToastUtils.showToast(getContext(), getString(R.string.txt_otp_invalid));
                } else {
                    fragmentInteractionListener.displaySetNewPasswordScreen(phoneNumber, etOtp.getText().toString());
                }
            }
        });
        mApplication.registerSmsOTPObserver();
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.BLACK);
        return rootView;
    }

    private void initView() {
        tvEnterNumberDes.setText(fomatString());
        btnContinue.setVisibility(View.VISIBLE);
        btnContinue.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
        tvEnterNumber.setText(getResources().getString(R.string.forgot_your_password_title));
        setupUI(scr_password);
    }


    @Override
    public void onAttach(Activity activity) {
        mLoginActivity = (LoginActivity) activity;
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mRes = mLoginActivity.getResources();
        mLoginActivity.setTitle(getResources().getString(R.string.enter_code).toUpperCase());
        super.onAttach(activity);
        try {
            mListener = (OTPLoginFragment.OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        etOtp.requestFocus();
        UIUtil.showKeyboard(getContext(),etOtp);
        mClickHandler = this;
    //    MySMSBroadcastReceiver.addSMSReceivedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mClickHandler = null;
//        SmsReceiver.removeSMSReceivedListener(this);
   //     MySMSBroadcastReceiver.removeSMSReceivedListener(this);
        InputMethodUtils.hideSoftKeyboard(etOtp, mLoginActivity);
    }


    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, mCurrentNumberJid);
        outState.putString(REGION_CODE_PARAM, mCurrentRegionCode);
        outState.putInt(CODE_PARAM, codeGenOtp);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }
    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if(innerView instanceof RoundTextView){
                    continue;
                }
                setupUI(innerView);
            }
        }
    }
    private void findComponentViews() {
        etOtp.requestFocus();
        /*if (codeGenOtp == 200) {
            tvDescInfo.setText(mRes.getString(R.string.login_header_notice_2));
        } else if (codeGenOtp == 201) {
            tvDescInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_201), mCurrentNumberJid));
        } else if (codeGenOtp == 202) {
            tvDescInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_202), mCurrentNumberJid));
        }*/

    }
    private String fomatString() {
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        if (!TextUtils.isEmpty(phoneNumber)){
            String mPhoneNumber = phoneNumber.substring(0,3)+" "+ phoneNumber.substring(3);
            fmt.format(getString(R.string.text_otp_has_been_sent_to), mPhoneNumber);
            return sbuf.toString();
        }
        return "";
    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        generateOtpByServer();
//        etOtp.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
//            @Override
//            public void onPinEntered(CharSequence str) {
//                if (str != null && str.length() == 6){
//                    fragmentInteractionListener.displaySetNewPasswordScreen(phoneNumber, str.toString());
//                }
//
//            }
//        });
    }

    private void setResendPassListener() {
        tvResendOtp.setText(mLoginActivity.getString(R.string.mc_resend_otp));
        tvResendOtp.setTextColor(ContextCompat.getColor(mLoginActivity, R.color.resend));
        tvResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogConfirmResendCode();
            }
        });

    }

    private void showDialogConfirmResendCode() {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.title_confirm_resend_code);
        String msg = String.format(mRes.getString(R.string.msg_confirm_resend_code), tvPhoneNumber.getText()
                .toString());
        PopupHelper.getInstance().showDialogConfirm(mLoginActivity, title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    private void setPasswordEditText(String pass) {
        etOtp.setText(pass);
        Selection.setSelection(etOtp.getText(), etOtp.getText().length());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mLoginActivity.unregisterReceiver(mySMSBroadcastReceiver);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initViewListeners();

    }

    @OnClick({R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                mLoginActivity.onBackPressed();
                break;
        }
    }

    private void startCountDown(int time) {
        tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    setResendPassListener();
                } else if(remain > 0){
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                }else{remain=0;}
                String countDownString = getString(R.string.mc_resend_otp);
                tvResendOtp.setText(countDownString);
                tvTimeResendOtp.setText(remain + " " +getString(R.string.text_seconds));
            }
        }, 1000);
    }

    public void generateOtpByServer() {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.generateOtp(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                startSmsUserConsent();
                android.util.Log.d("TAG", "onResponse: " + response.body());
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.d("TAG", "onError: " + error.getMessage());
            }
        });


    }

    //XU ly phan sms otp
//    @Override
//    public boolean onOTPReceived(String otp) {
//        this.password = otp;
//        mLoginActivity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (etOtp != null) {
//                     setPasswordEditText(password);
////                    countDownTimer.cancel();
//                    mHandler.removeCallbacksAndMessages(null);
////                    doLoginAction(password);
//                }
//            }
//        });
//        return true;
//    }
    @Override
    public void onOTPTimeOut() {

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == RESULT_OK && data != null) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (message != null) {
                    getOtpFromMessage(message);
                }

            }

        }
    }
    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                Log.d(TAG,"Success");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,"Fail");

            }
        });
    }
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            etOtp.setText(matcher.group(0));
        }
    }
    @Override
    public void onOTPReceived(Intent intent) {
        startActivityForResult(intent, REQ_USER_CONSENT);
    }

    public interface OnFragmentInteractionListener {
        void displayPersonalInfo();

        void navigateToNextScreen();
    }

    private void initViewListeners() {
        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
//                    signIn();
                    btnContinue.setEnabled(true);
                    btnContinue.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_confirm));
                }else {
                    btnContinue.setEnabled(false);
                    btnContinue.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bgr_btn_gery));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

}
package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.GuestBookPreviewActivity;
import com.metfone.selfcare.adapter.guestbook.BookPreviewAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.ReengViewPager;
import com.metfone.selfcare.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by toanvk2 on 4/24/2017.
 */
public class GuestBookPreviewFragment extends Fragment implements
        View.OnClickListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener {
    private static final String TAG = GuestBookPreviewFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private Handler mHandler = new Handler();
    private GuestBookPreviewActivity mParentActivity;
    private GuestBookHelper mGuestBookHelper;
    private Book currentBook;
    private String bookId;
    private ImageView mImgMute;
    private View mViewFooterBack, mViewFooterSave, mViewFooterVote, mViewFooterShare;
    private TextView mTvwFooterVote;
    private ImageView mImgFooterVote;
    private ReengViewPager mViewPager;
    private BookPreviewAdapter mAdapter;
    private ArrayList<Page> listPages;
    //
    private int mCurrentPos, mListSize;
    private boolean pageSwipeManual = false;
    private ChangePageTimerTask mChangePageTask;
    private Timer mTimer;
    private SavePageAsyncTask savePageAsyncTask;
    private SharePageAsyncTask sharePageAsyncTask;
    //
    private AudioManager audioManager;
    private MediaPlayer mediaPlayer;
    private boolean isMute = false;
    //
    private int isVoted = -1;
    private int totalVote = -1;

    public GuestBookPreviewFragment() {

    }

    public static GuestBookPreviewFragment newInstance() {
        GuestBookPreviewFragment fragment = new GuestBookPreviewFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.GUEST_BOOK.VOTE_DETAIL_ISVOTED, -1);
        args.putInt(Constants.GUEST_BOOK.VOTE_DETAIL_TOTAL_VOTE, -1);
        fragment.setArguments(args);
        return fragment;
    }

    public static GuestBookPreviewFragment newInstance(int isVoted, int totalVote) {
        GuestBookPreviewFragment fragment = new GuestBookPreviewFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.GUEST_BOOK.VOTE_DETAIL_ISVOTED, isVoted);
        args.putInt(Constants.GUEST_BOOK.VOTE_DETAIL_TOTAL_VOTE, totalVote);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (GuestBookPreviewActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        audioManager = (AudioManager) mApplication.getSystemService(Context.AUDIO_SERVICE);
        mRes = mApplication.getResources();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_guest_book_preview, container, false);
        findComponentViews(rootView, container, inflater);
        mParentActivity.getToolBarView().setVisibility(View.GONE);
        getDataAnDraw(savedInstanceState);
        setViewListener();
        return rootView;
    }

    @Override
    public void onResume() {
        startTimerChangePage();
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.GUEST_BOOK.BOOK_ID, bookId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        stopTimerChangePage();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mGuestBookHelper.cancelRequest(GuestBookHelper.TAG_GET_VOTE_DETAL);
        mGuestBookHelper.cancelRequest(GuestBookHelper.TAG_VOTE);
        releaseMusic();
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        if (savePageAsyncTask != null) {
            savePageAsyncTask.cancel(true);
            savePageAsyncTask = null;
        }
        if (sharePageAsyncTask != null) {
            sharePageAsyncTask.cancel(true);
            sharePageAsyncTask = null;
        }
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.guest_book_footer_preview_back:
                mParentActivity.onBackPressed();
                break;
            case R.id.guest_book_footer_preview_save:
                Log.d(TAG, "click save preview: " + mViewPager.getCurrentItem());
                Page page = listPages.get(mViewPager.getCurrentItem());
                handleSavePage(page);
                break;
            case R.id.guest_book_footer_preview_vote:
                handleVote();
                break;
            case R.id.guest_book_footer_preview_share:
                handleSharePage(listPages.get(mViewPager.getCurrentItem()));
                break;
            case R.id.guest_book_preview_image_mute:
                if (isMute) {
                    unMute();
                } else {
                    mute();
                }
                break;
        }
    }


    private void getDataAnDraw(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            bookId = savedInstanceState.getString(Constants.GUEST_BOOK.BOOK_ID);
            isVoted = savedInstanceState.getInt(Constants.GUEST_BOOK.VOTE_DETAIL_ISVOTED, -1);
            totalVote = savedInstanceState.getInt(Constants.GUEST_BOOK.VOTE_DETAIL_TOTAL_VOTE, -1);
        } else if (getArguments() != null) {
            isVoted = getArguments().getInt(Constants.GUEST_BOOK.VOTE_DETAIL_ISVOTED, -1);
            totalVote = getArguments().getInt(Constants.GUEST_BOOK.VOTE_DETAIL_TOTAL_VOTE, -1);
        }
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        currentBook = mGuestBookHelper.getCurrentBookEditor();
        if (currentBook != null) {
            bookId = currentBook.getId();
            playMusic();
            drawDetail();
        } else {
            mParentActivity.showToast(R.string.request_send_error);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mParentActivity.onBackPressed();
                }
            }, 1000);
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mImgMute = (ImageView) rootView.findViewById(R.id.guest_book_preview_image_mute);
        mViewPager = (ReengViewPager) rootView.findViewById(R.id.guest_book_preview_image_viewpager);
        View footerView = rootView.findViewById(R.id.guest_book_preview_image_footer);
        mViewFooterBack = footerView.findViewById(R.id.guest_book_footer_preview_back);
        mViewFooterSave = footerView.findViewById(R.id.guest_book_footer_preview_save);
        mViewFooterVote = footerView.findViewById(R.id.guest_book_footer_preview_vote);
        mViewFooterShare = footerView.findViewById(R.id.guest_book_footer_preview_share);
        mTvwFooterVote = (TextView) footerView.findViewById(R.id.guest_book_footer_preview_vote_text);
        mImgFooterVote = (ImageView) footerView.findViewById(R.id.guest_book_footer_preview_vote_icon);
    }

    private void setViewListener() {
        mViewFooterBack.setOnClickListener(this);
        mViewFooterSave.setOnClickListener(this);
        mViewFooterVote.setOnClickListener(this);
        mViewFooterShare.setOnClickListener(this);
        mImgMute.setOnClickListener(this);
        //
        /*int i = mViewPager.getCurrentItem();
        String path = mListImage.get(i);*/
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (pageSwipeManual) {
                    Log.d(TAG, "onPageSelected");
                    pageSwipeManual = false;
                    mCurrentPos = position;
                    startTimerChangePage();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    pageSwipeManual = true;
                }
            }
        });
    }

    private void drawDetail() {
        setAdapter();
        drawStateMute();
        if (totalVote < 0) {// vào từ màn hình quyển thì chưa có thông tin vote sẽ get lại
            mViewFooterVote.setEnabled(false);
            mImgFooterVote.setColorFilter(ContextCompat.getColor(mApplication, R.color.onmedia_bg_button));
            //mImgFooterVote.setImageResource(R.drawable.ic_guest_book_footer_vote_non);
            //mTvwFooterVote.setVisibility(View.VISIBLE);
            mTvwFooterVote.setText("0");
            mGuestBookHelper.requestVoteDetail(bookId, getVoteDetailListener);
        } else {// vào từ màn hình danh sách vote đã có thông tin thì không get lại
            drawVoteDetail();
        }
    }

    private void drawVoteDetail() {
        mViewFooterVote.setEnabled(true);
        if (totalVote >= 0) {
            //mTvwFooterVote.setVisibility(View.VISIBLE);
            mTvwFooterVote.setText(String.valueOf(totalVote));
        } else {
            mTvwFooterVote.setText("0");
            //mTvwFooterVote.setVisibility(View.GONE);
        }
        if (isVoted == 1) {
            mImgFooterVote.setColorFilter(ContextCompat.getColor(mApplication, R.color.guest_book_vote));
            //mImgFooterVote.setImageResource(R.drawable.ic_guest_book_footer_vote);
        } else {
            //mImgFooterVote.setImageResource(R.drawable.ic_guest_book_footer_vote_non);
            mImgFooterVote.setColorFilter(ContextCompat.getColor(mApplication, R.color.onmedia_bg_button));
        }
    }

    private void setAdapter() {
        listPages = currentBook.getPages();
        mListSize = listPages.size();
        mAdapter = new BookPreviewAdapter(mApplication, listPages);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(2);
        startTimerChangePage();
    }

    private void drawStateMute() {
        if (isMute) {
            mImgMute.setImageResource(R.drawable.ic_music_mute);
        } else {
            mImgMute.setImageResource(R.drawable.ic_music_unmute);
        }
    }

    private class ChangePageTimerTask extends TimerTask {
        @Override
        public void run() {
            Log.d(TAG, "ChangePageTimerTask");
            if (mCurrentPos < mListSize) {
                mCurrentPos += 1;
            }
            if (mCurrentPos == mListSize) {
                mCurrentPos = 0;
            }
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                mViewPager.setCurrentItem(mCurrentPos, true);
            } else {
                mParentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mViewPager.setCurrentItem(mCurrentPos, true);
                    }
                });
            }
        }
    }

    private void startTimerChangePage() {
        stopTimerChangePage();
        if (mListSize < 2) return;
        if (mChangePageTask == null) {
            mChangePageTask = new ChangePageTimerTask();
        }
        mTimer = new Timer();
        mCurrentPos = mViewPager.getCurrentItem();
        mTimer.schedule(mChangePageTask,
                Constants.GUEST_BOOK.TIMER_CHANGE_PAGE,
                Constants.GUEST_BOOK.TIMER_CHANGE_PAGE);
    }

    private void stopTimerChangePage() {
        if (mChangePageTask != null) {
            mChangePageTask.cancel();
            mChangePageTask = null;
        }
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void handleSharePage(Page page) {
        if (sharePageAsyncTask != null) {
            sharePageAsyncTask.cancel(true);
            sharePageAsyncTask = null;
        }
        sharePageAsyncTask = new SharePageAsyncTask(page);
        sharePageAsyncTask.execute();
    }

    private class SharePageAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private Page page;

        public SharePageAsyncTask(Page page) {
            this.page = page;
        }

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (!TextUtils.isEmpty(page.getPreview())) {
                String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(page.getPreview());
                Bitmap bitmap = ImageLoaderManager.getInstance(mApplication).loadGuestBookPreviewSync(url);
                if (bitmap != null) {
                    mParentActivity.shareImageFacebook(bitmap, mRes.getString(R.string.guest_book_share_facebook_title));
                    bitmap.recycle();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            mParentActivity.hideLoadingDialog();
            if (!aVoid) {
                mParentActivity.showToast(R.string.request_send_error);
            }
        }
    }

    private void handleSavePage(Page page) {
        if (savePageAsyncTask != null) {
            savePageAsyncTask.cancel(true);
            savePageAsyncTask = null;
        }
        savePageAsyncTask = new SavePageAsyncTask(page);
        savePageAsyncTask.execute();
    }

    private class SavePageAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private Page page;

        public SavePageAsyncTask(Page page) {
            this.page = page;
        }

        @Override
        protected void onPreExecute() {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (!TextUtils.isEmpty(page.getPreview())) {
                String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(page.getPreview());
                Bitmap bitmap = ImageLoaderManager.getInstance(mApplication).loadGuestBookPreviewSync(url);
                if (bitmap != null) {
                    String fileName = ImageHelper.IMAGE_GUEST_BOOK + System.currentTimeMillis() + Constants.FILE.JPEG_FILE_SUFFIX;
                    ImageHelper.saveBitmapToGallery(mParentActivity, fileName, bitmap, mApplication);
                    bitmap.recycle();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            mParentActivity.hideLoadingDialog();
            if (!aVoid) {
                mParentActivity.showToast(R.string.request_send_error);
            }
        }
    }

    private void handleVote() {
        if (isVoted == 1) {
            mParentActivity.showToast(R.string.guest_book_you_voted);
        } else {
            mParentActivity.showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestVoteBook(bookId, new GuestBookHelper.UpdateStateListener() {
                @Override
                public void onSuccess() {
                    mParentActivity.hideLoadingDialog();
                    isVoted = 1;
                    totalVote++;
                    drawVoteDetail();
                    // update thông tin ra tab list vote
                    /*BookVote bookVote = new BookVote();
                    bookVote.setId(bookId);
                    bookVote.setVoteTotal(totalVote);
                    mParentActivity.setVoteChanged(bookVote);*/
                }

                @Override
                public void onError(int error) {
                    mParentActivity.hideLoadingDialog();
                    mParentActivity.showToast(R.string.request_send_error);
                }
            });
        }
    }

    private GuestBookHelper.GetVoteDetailListener getVoteDetailListener = new GuestBookHelper.GetVoteDetailListener() {
        @Override
        public void onSuccess(int total, int isVote) {
            isVoted = isVote;
            totalVote = total;
            drawVoteDetail();
        }

        @Override
        public void onError(int error) {
            mParentActivity.showToast(R.string.request_send_error);
        }
    };

    /////////////////////////// media player /////////////////////////////


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onCompletion");
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPrepared");
        // pause nhạc #
        /*if (mApplication.getPlayMusicController() != null) {
            if (mApplication.getPlayMusicController().isPlaying()) {
                mApplication.getPlayMusicController().setStateResumePlaying(true);
                mApplication.getPlayMusicController().toggleMusic();
            }
            mApplication.getPlayMusicController().setOtherAudioPlaying(true);
        }*/
        requestAudioFocus();
        mediaPlayer.start();
        mImgMute.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.e(TAG, "media player error " + i + " -i1 " + i1);
        mImgMute.setVisibility(View.GONE);
        return false;
    }

    private void mute() {
        if (Version.hasM()) {
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
        } else {
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        }
        isMute = true;
        drawStateMute();
    }

    public void unMute() {
        if (Version.hasM()) {
            audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, 0);
        } else {
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        }
        isMute = false;
        drawStateMute();
    }

    private void playMusic() {
        mImgMute.setVisibility(View.GONE);
        if (currentBook.getSong() == null || TextUtils.isEmpty(currentBook.getSong().getMediaUrl())) {
            Log.d(TAG, "song not found");
            return;
        }
        String mediaUrl = currentBook.getSong().getMediaUrl();
        //String mediaUrl = "http://vip.hot6.medias.keeng.vn/sata11/songv3/2017/04/10/8aSFhT8JUbznsegyuMB058eb57d90229c.mp3?type=song&id=2868142";
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setLooping(true);
        try {
            mediaPlayer.setDataSource(mediaUrl);
            mediaPlayer.prepareAsync();
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "IllegalArgumentException", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "IllegalStateException", e);
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }
    }

    private void releaseMusic() {
        if (isMute) {
            unMute();
        }
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mImgMute.setVisibility(View.GONE);
        releaseAudioFocus();
    }

    private void requestAudioFocus() {
        audioManager.requestAudioFocus(afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);
    }

    private void releaseAudioFocus() {
        mApplication.getPlayMusicController().setIsHuman(true);
        audioManager.abandonAudioFocus(afChangeListener);
    }

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            Log.d(TAG, "OnAudioFocusChangeListener: " + focusChange);
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {// Pause playback
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                }
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {   // Resume playback
                if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                //audioManager.abandonAudioFocus(afChangeListener);
                // Stop playback
            }
        }
    };
}
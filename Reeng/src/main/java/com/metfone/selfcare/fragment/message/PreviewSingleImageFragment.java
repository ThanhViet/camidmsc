package com.metfone.selfcare.fragment.message;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.PreviewImageActivity;
import com.metfone.selfcare.adapter.PreviewImageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/17/2018.
 */

public class PreviewSingleImageFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = PreviewSingleImageFragment.class.getSimpleName();
    private ApplicationController mApp;
    private PreviewImageActivity mActivity;
    private Resources mRes;

    private PreviewImageAdapter mPreviewImageAdapter;
    private ViewPager mViewPager;
    private View mViewActionBar, rootView;
    private TextView mTvwThreadName;
    private TextView mTvwThreadCounter;
    private View mBackBtn, mBtnDownload;
    private View btnShare;
    private boolean isHideStatus = false;
    //    private Handler mHandler;
    private Animation animFadeIn, animFadeOut;

    private String title, filePath, linkImage;
    private ArrayList<String> mListImage;

    public PreviewSingleImageFragment() {
    }

    public static PreviewSingleImageFragment newInstance(String name, String filePath, String linkImage) {
        PreviewSingleImageFragment fragment = new PreviewSingleImageFragment();
        Bundle args = new Bundle();
        args.putString(PreviewImageActivity.PARAM_TITLE, name);
        args.putString(PreviewImageActivity.PARAM_FILE_PATH, filePath);
        args.putString(PreviewImageActivity.PARAM_LINK, linkImage);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseSlidingFragmentActivity) {
            mActivity = (PreviewImageActivity) context;
            mApp = (ApplicationController) mActivity.getApplication();
            mRes = mActivity.getResources();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_preview_image, container, false);
        mActivity.setWhiteStatusBar();
        setComponentViews(rootView);
        initData(savedInstanceState);
        setListener();
        return rootView;
    }

    private void setComponentViews(View rootView) {
        mViewActionBar = rootView.findViewById(R.id.ab_preview_image);
        mTvwThreadName = rootView.findViewById(R.id.ab_preview_image_title);
        mTvwThreadCounter = rootView.findViewById(R.id.ab_preview_image_counter);
        mViewPager = rootView.findViewById(R.id.preview_image_view_pager);
        mBackBtn = rootView.findViewById(R.id.ab_preview_image_back_btn);
        mBtnDownload = rootView.findViewById(R.id.ab_download_btn);
        btnShare = rootView.findViewById(R.id.img_ab_share);
        mBtnDownload.setVisibility(View.GONE);
        btnShare.setVisibility(View.GONE);
        animFadeIn = AnimationUtils.loadAnimation(mApp, R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(mApp, R.anim.fade_out);

        View layoutView = rootView.findViewById(R.id.layout_preview_image);
        layoutView.setBackgroundColor(ContextCompat.getColor(mApp, R.color.black));
    }

    private void initData(Bundle savedInstanceState) {
        if (getArguments() != null) {
            title = getArguments().getString(PreviewImageActivity.PARAM_TITLE);
            filePath = getArguments().getString(PreviewImageActivity.PARAM_FILE_PATH);
            linkImage = getArguments().getString(PreviewImageActivity.PARAM_LINK);
        } else if (savedInstanceState != null) {
            title = savedInstanceState.getString(PreviewImageActivity.PARAM_TITLE);
            filePath = savedInstanceState.getString(PreviewImageActivity.PARAM_FILE_PATH);
            linkImage = savedInstanceState.getString(PreviewImageActivity.PARAM_LINK);
        }
        mListImage = new ArrayList<>();

        if (!TextUtils.isEmpty(filePath) && (new File(filePath)).exists())
            mListImage.add(filePath);
        else
            mListImage.add(linkImage);
        // load data
        //mThreadMessage.setListImage(listImage);
        mTvwThreadName.setText(title);
        drawDetail();
    }

    private void drawCounter() {
        String counter = "1/1" + " " + getResources().getString(R.string.message_image);
        mTvwThreadCounter.setText(counter);
    }

    private void drawDetail() {
        drawCounter();
        if (mPreviewImageAdapter == null) {
            mPreviewImageAdapter = new PreviewImageAdapter(mApp, mListImage);
            mPreviewImageAdapter.setLoadLocal(!TextUtils.isEmpty(filePath));
            mViewPager.setAdapter(mPreviewImageAdapter);
            mViewPager.setCurrentItem(0);
        } else {
            mPreviewImageAdapter.setListImage(mListImage);
            mPreviewImageAdapter.notifyDataSetChanged();
            mViewPager.setCurrentItem(0);
        }
        mPreviewImageAdapter.setImageClickListener(new PreviewImageAdapter.ImageClickListener() {
            @Override
            public void onClickImage() {
                if (isHideStatus) {
                    showStatusBarPreviewImage();
                } else {
                    hideStatusBar(true);
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void hideStatusBar(boolean animation) {
        Log.i(TAG, "hide");
        isHideStatus = true;
        /*if (Build.VERSION.SDK_INT < 16) {
            mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = mActivity.getWindow().getDecorView();
            // hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
        if (animation) {
            mViewActionBar.clearAnimation();
            mViewActionBar.startAnimation(animFadeOut);
        } else {
            mViewActionBar.setVisibility(View.GONE);
        }
    }

    public void showStatusBarPreviewImage() {
        Log.i(TAG, "show");
        isHideStatus = false;
        /*if (Build.VERSION.SDK_INT < 16) {
            mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = mActivity.getWindow().getDecorView();
            // show the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
        mViewActionBar.clearAnimation();
        mViewActionBar.startAnimation(animFadeIn);
        /*if (mHandler == null) {
            mHandler = new Handler();
        } else {
            mHandler.removeCallbacks(runnable);
        }
        mHandler.postDelayed(runnable, TIME_DELAY_SHOW);*/
    }

    private void setListener() {
        mBackBtn.setOnClickListener(this);
        setAnimationListener();
    }

    private void setAnimationListener() {
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                mViewActionBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewActionBar.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_preview_image_back_btn:
                mActivity.onBackPressed();
                break;
            default:
                break;
        }
    }
}

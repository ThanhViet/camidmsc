package com.metfone.selfcare.fragment.browser;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.adapter.feedback.FeedbackItemDecoration;
import com.metfone.selfcare.adapter.image.ImageViewerDirectoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.images.ImageDirectory;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.listeners.IImageBrowser.SelectImageListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by huybq on 11/5/2014.
 */
public class ImageViewerFragment extends Fragment implements
        SelectImageListener {
    private static String TAG = ImageViewerFragment.class.getSimpleName();

    private ImageBrowserActivity mImageBrowserActivity;

    private SelectImageListener mSelectImageListener;
    private ImageViewerDirectoryAdapter mAdapter;

    private ImageView imgNoMedia;
    private RecyclerView mRecyclerView;

    private ApplicationController mApp;

    //
    public ImageViewerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ImageViewerFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_directory_viewer, container, false);
        mApp = (ApplicationController) mImageBrowserActivity.getApplicationContext();
        findComponentViews(rootView);
        checkImageStatus();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        mImageBrowserActivity = (ImageBrowserActivity) activity;
        super.onAttach(activity);
    }

    private void findComponentViews(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_directory);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mApp, 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.addItemDecoration(new FeedbackItemDecoration(10, false, 3));
        mAdapter = new ImageViewerDirectoryAdapter(mImageBrowserActivity, this);
        mRecyclerView.setAdapter(mAdapter);

        imgNoMedia = (ImageView) rootView.findViewById(R.id.imgNoMedia);
    }


    private void checkImageStatus() {
        if (mAdapter.getItemCount() == 0) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }


    @Override
    public void onSelectImage(ImageDirectory director, ImageInfo image, int position) {
        if (mSelectImageListener != null) {
            mSelectImageListener.onSelectImage(mImageBrowserActivity.getCurrentImageDirectory(), image, position);
        }
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        checkImageStatus();
    }

    public void setSelectImageListener(SelectImageListener selectImageListener) {
        this.mSelectImageListener = selectImageListener;
    }

    public void notifySelectImage(int positionSelected){
        GridLayoutManager gridLayoutManager = (GridLayoutManager) mRecyclerView.getLayoutManager();
        int firstIndexVisibleItem  =  gridLayoutManager.findFirstVisibleItemPosition();
        int lastIndexVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
        if(positionSelected >= firstIndexVisibleItem && positionSelected <= lastIndexVisibleItem) {
            for (int i = firstIndexVisibleItem; i <= lastIndexVisibleItem; i++) {
                RecyclerView.ViewHolder viewHolder = mRecyclerView.findViewHolderForAdapterPosition(i);
                if (viewHolder != null && viewHolder instanceof ImageViewerDirectoryAdapter.ImageViewerDirectoryHolder) {
                    ImageViewerDirectoryAdapter.ImageViewerDirectoryHolder imageViewerDirectoryHolder = (ImageViewerDirectoryAdapter.ImageViewerDirectoryHolder) viewHolder;
                    if(mImageBrowserActivity.maxImages == 1) {
                        if (i == positionSelected) {
                            imageViewerDirectoryHolder.getImgQueue().setSelected(true);
                        } else {
                            imageViewerDirectoryHolder.getImgQueue().setSelected(false);
                        }
                    }else {

                    }
                }
            }

        }
    }

}
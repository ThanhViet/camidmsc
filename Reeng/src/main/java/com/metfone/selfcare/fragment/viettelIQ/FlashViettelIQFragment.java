package com.metfone.selfcare.fragment.viettelIQ;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.format.DateFormat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.activity.ViettelIQActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ViettelIQApi;
import com.metfone.selfcare.helper.MusicBackgroundHelper;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FlashViettelIQFragment extends Fragment {

    private static final String TAG = FlashViettelIQFragment.class.getSimpleName();

    private static final String START_TIME = "startTime";
    private static final String NUM_PLAYER = "numPlayer";
    private static final String NUM_VIEWER = "numViewer";

    @BindView(R.id.tv_title_count_down)
    TextView tvTitleCountDown;
    @BindView(R.id.tv_time_count_down)
    TextView tvTimeCountDown;
    @BindView(R.id.tv_how_to_player)
    RoundTextView tvHowToPlayer;
    Unbinder unbinder;

    boolean increaseTimeGetSub = false;

    private MusicBackgroundHelper musicBgHelper;

    public static FlashViettelIQFragment newInstance(long startTime, long numPlayer, long numViewer) {
        Bundle args = new Bundle();
        args.putLong(START_TIME, startTime);
        args.putLong(NUM_PLAYER, numPlayer);
        args.putLong(NUM_VIEWER, numViewer);
        FlashViettelIQFragment fragment = new FlashViettelIQFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private long startTime;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startTime = getArguments().getLong(START_TIME);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viettel_iq_flash, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        musicBgHelper = new MusicBackgroundHelper((ApplicationController) getActivity().getApplication());
        musicBgHelper.startMusic(R.raw.iq_background_new);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        musicBgHelper.pauseMusic();
    }

    @Override
    public void onResume() {
        super.onResume();
        musicBgHelper.resumeMusic();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        musicBgHelper.stopMusic();
        musicBgHelper.onDestroyMusic();
        if (tvTimeCountDown == null) return;
        tvTimeCountDown.removeCallbacks(scheduleRunnable);
        tvTitleCountDown.removeCallbacks(getSubRunnable);
    }

    @OnClick(R.id.tv_how_to_player)
    public void onViewClicked() {
    }

    private void initView() {
        if (tvTimeCountDown == null) return;
        tvTimeCountDown.removeCallbacks(scheduleRunnable);
        tvTimeCountDown.post(scheduleRunnable);


        tvTitleCountDown.removeCallbacks(getSubRunnable);
        tvTitleCountDown.postDelayed(getSubRunnable, 15 * 1000);
    }

    private Runnable scheduleRunnable = new Runnable() {
        @Override
        public void run() {
            Activity activity = getActivity();
            if (tvTimeCountDown == null || activity == null) return;
            if (startTime - ((ViettelIQActivity) activity).getCurrentTime() <= ((ViettelIQActivity) activity).getDisplayTime() * 1000) {
                ((ViettelIQActivity) activity).openCountFirstQuestion();
            } else {
                setTime();
                tvTimeCountDown.postDelayed(this, 200);
            }
        }
    };

    private Runnable getSubRunnable = new Runnable() {
        @Override
        public void run() {
            Activity activity = getActivity();
            if (activity != null) {
                ViettelIQApi.getInstance().getSubWaiting(((ViettelIQActivity) activity).getCurrentIdGame());
                tvTitleCountDown.postDelayed(this, increaseTimeGetSub ? 30 * 1000 : 60 * 1000);
            }
        }
    };

    @SuppressLint({"SetTextI18n", "SimpleDateFormat"})
    private void setTime() {
        Activity activity = getActivity();
        if (tvTimeCountDown == null || activity == null) return;
        long currentTimeMillis = ((ViettelIQActivity) activity).getCurrentTime();
        String startTimeSrt = ((ViettelIQActivity) activity).getStartTimeSrt();
        if (startTime - ((ViettelIQActivity) activity).getDisplayTime() * 1000 - currentTimeMillis > 15 * 60 * 1000/*thời gian bắt đầu game so với thời gian hiện tại lớn hơn 15 phút*/) {
            tvTitleCountDown.setText("Game tiếp theo");
            tvTimeCountDown.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
            tvTimeCountDown.setText(DateFormat.format("HH:mm", new Date(startTime))
                    + "\n"
                    + startTimeSrt);

        } else {
            tvTitleCountDown.setText("Game sẽ bắt đầu sau");
            tvTimeCountDown.setTextSize(TypedValue.COMPLEX_UNIT_SP, 37);
            long remainTime = startTime - currentTimeMillis - ((ViettelIQActivity) activity).getDisplayTime() * 1000;
            tvTimeCountDown.setText(DateFormat.format("mm:ss", new Date(remainTime)));
            if (remainTime >= 5 * 60 * 1000)
                increaseTimeGetSub = false;
            else
                increaseTimeGetSub = true;
        }
    }
}

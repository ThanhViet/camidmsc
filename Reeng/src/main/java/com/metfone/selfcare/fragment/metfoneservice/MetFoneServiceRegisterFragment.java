package com.metfone.selfcare.fragment.metfoneservice;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.MetFoneServiceActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Response;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;

public class MetFoneServiceRegisterFragment extends Fragment implements View.OnClickListener {
    String TAG = MetFoneServiceRegisterFragment.class.getName();
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.tvCancel)
    CamIdTextView tvCancel;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    @BindView(R.id.etNumber)
    AppCompatEditText etNumber;
    @BindView(R.id.rll_add_service)
    RelativeLayout mRllAddService;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private OnFragmentInteractionListener onFragmentInteractionListener;
    private MetFoneServiceActivity metFoneServiceActivity;
    private BaseMPConfirmDialog baseMPConfirmDialog;

    public static MetFoneServiceRegisterFragment newInstance() {
        MetFoneServiceRegisterFragment fragment = new MetFoneServiceRegisterFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        metFoneServiceActivity = (MetFoneServiceActivity) context;
        onFragmentInteractionListener = (OnFragmentInteractionListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_metfone_service, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setupUI(mRllAddService);
        metFoneServiceActivity.setColorStatusBar(R.color.color_tvdelete);
        if (getActivity() != null) {
            setSystemBarTheme((getActivity()), true);
        }
    }

    @Override
    @Optional
    @OnClick({R.id.ivBack, R.id.btnContinue, R.id.tvCancel})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvCancel:
            case R.id.ivBack:
                metFoneServiceActivity.onBackPressed();
                break;
            case R.id.btnContinue:

                String phone = etNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                } else {
                    if (etNumber.getText().toString().startsWith("0")) {
                        checkPhone(etNumber.getText().toString());
                    } else {
                        checkPhone("0" + etNumber.getText().toString());
                    }
                }
//                changeMetfone();
//                onFragmentInteractionListener.displayOtpProfileFragment(etNumber.getText().toString());
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        etNumber.requestFocus();
        UIUtil.showKeyboard(getContext(), etNumber);
    }

    private void checkPhone(String phoneNumber) {
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.checkPhone(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                Log.e(TAG, "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(), etNumber);
                    }
                }, 200);
                if (response.body() != null) {
                    String code = response.body().getCode();
                    Log.d(TAG, "onResponse: " + response.body().getMessage());
                    if ("00".equals(code) || "42".equals(code) || "68".equals(code)) {
                        baseMPConfirmDialog = new BaseMPConfirmDialog("lottie/img_dialog_man_thinking.json", R.string.m_p_dialog_combine_phone_title,
                            R.string.m_p_dialog_combine_phone_content, R.string.m_p_dialog_combine_phone_top_btn, R.string.m_p_dialog_combine_phone_bottom_btn,
                            view -> {
                                //letdoit
                                onFragmentInteractionListener.displayOtpProfileFragment(phoneNumber, 42);
                                if (baseMPConfirmDialog != null) {
                                    baseMPConfirmDialog.dismiss();
                                }
                            }, view -> {
                            //back
                            if (baseMPConfirmDialog != null) {
                                baseMPConfirmDialog.dismiss();
                            }
                        });
                        baseMPConfirmDialog.show(getChildFragmentManager(), null);
                    } else {
                        onFragmentInteractionListener.displayOtpProfileFragment(phoneNumber, 0);
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "onError: ", error);
            }
        });
    }

    public void setupUI(View view) {
        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);

            }
        }
    }

    public interface OnFragmentInteractionListener {
        void displayOtpProfileFragment(String phoneNumber, int checkPhoneCode);
    }
}
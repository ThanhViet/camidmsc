package com.metfone.selfcare.fragment.message;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metfone.selfcare.activity.ChooseDocumentActivity;
import com.metfone.selfcare.adapter.DocumentClassAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.holder.DocumentClassHolder;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by Toanvk2 on 5/6/2017.
 */

public class ChooseDocumentFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener,
        View.OnClickListener,
        DocumentClassHolder.OnDocumentListener,
        ClickListener.IconListener {
    private static final String TAG = ChooseDocumentFragment.class.getSimpleName();
    private static final String TAG_GET_DOCUMENT = "TAG_GET_DOCUMENT";
    private ApplicationController mApplication;
    private Resources mRes;
    private ChooseDocumentActivity mParentActivity;
    private TextView mAbTitle;
    private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;
    private DocumentClassAdapter mAdapter;
    private ArrayList<DocumentClass> listItems = null;
    private String groupId;

    public ChooseDocumentFragment() {

    }

    public static ChooseDocumentFragment newInstance(String groupId) {
        ChooseDocumentFragment fragment = new ChooseDocumentFragment();
        Bundle args = new Bundle();
        args.putString(Constants.DOCUMENT.GROUP_ID, groupId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mParentActivity = (ChooseDocumentActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        getDataAndDrawDetail();
        return rootView;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_DOCUMENT);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.DOCUMENT.GROUP_ID, groupId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onSendDocument(DocumentClass document) {
        showConfirmSendDocument(document);
    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (TextView) abView.findViewById(R.id.ab_title);
        mAbTitle.setText(mRes.getString(R.string.title_document_group));
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mAbBack.setOnClickListener(this);
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            groupId = savedInstanceState.getString(Constants.DOCUMENT.GROUP_ID);
        } else if (getArguments() != null) {
            groupId = getArguments().getString(Constants.DOCUMENT.GROUP_ID);
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_OK_SEND_DOCUMENT:
                mListener.onReturnData((DocumentClass) entry);
                break;
        }
    }

    private void showConfirmSendDocument(DocumentClass document) {
        String btnYes = mRes.getString(R.string.ok);
        String btnCancel = mRes.getString(R.string.cancel);
        String msg = String.format(mRes.getString(R.string.document_confirm_send), document.getName());
        PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                null, msg, btnYes, btnCancel, this, document, Constants.MENU.POPUP_OK_SEND_DOCUMENT);
    }

    private void getDataAndDrawDetail() {
        if (listItems == null) {
            showProgressLoading();
            requestData(groupId, getDocumentListener);
        } else if (listItems.isEmpty()) {
            hideEmptyView();
            showEmptyNote(mRes.getString(R.string.document_emtpy));
        } else {
            hideEmptyView();
            setAdapter();
        }
    }

    private void requestData(String groupId, final GetDocumentListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(groupId).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.GET_GROUP_DOCUMENT),
                numberEncode, HttpHelper.EncoderUrl(groupId),
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestData response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<DocumentClass> documents = new ArrayList<>();
                                if (object.has("objects_info")) {
                                    JsonArray jsonArray = object.getAsJsonArray("objects_info");
                                    if (jsonArray != null) {
                                        int size = jsonArray.size();
                                        Gson gson = new Gson();
                                        for (int i = 0; i < size; i++) {
                                            DocumentClass document = gson.fromJson(jsonArray.get(i), DocumentClass.class);
                                            documents.add(document);
                                        }
                                    }
                                }
                                listener.onSuccess(documents);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_DOCUMENT, false);
    }

    private GetDocumentListener getDocumentListener = new GetDocumentListener() {
        @Override
        public void onSuccess(ArrayList<DocumentClass> documents) {
            hideEmptyView();
            listItems = documents;
            setAdapter();
        }

        @Override
        public void onError(int code) {
            hideEmptyView();
            mParentActivity.showToast(R.string.e601_error_but_undefined);
        }
    };

    private void setAdapter() {
        if (mAdapter == null || mRecyclerView.getAdapter() == null) {
            mAdapter = new DocumentClassAdapter(mApplication, listItems, this);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
            mRecyclerView.setAdapter(mAdapter);
            setItemListViewListener();
        } else {
            mAdapter.setListItems(listItems);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setItemListViewListener() {
        // onclick
        mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {

            }

            @Override
            public void onLongClick(View v, int pos, Object object) {
                Log.d(TAG, "onLongClick: " + pos);

            }
        });
        // long click
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    public interface OnFragmentInteractionListener {
        void onReturnData(DocumentClass document);
    }

    public interface GetDocumentListener {
        void onSuccess(ArrayList<DocumentClass> documents);

        void onError(int code);
    }
}

package com.metfone.selfcare.fragment.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.api.response.PaymentHistoryItem;

import java.util.ArrayList;
import java.util.List;

class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private List<Object> itemList;
    private Context mContext;
    private final int TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    public static final boolean IS_USE_ITEM_LOADING = false;

    @Override
    public void onClick(View v) {

    }

    public HistoryAdapter(Context context, ArrayList<Object> itemList) {
        mContext = context;
        this.itemList = itemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view;
        if (viewType == TYPE_ITEM) {
            view = layoutInflater.inflate(R.layout.item_history, parent, false);
            return new ItemHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading_kh, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == TYPE_ITEM && !IS_USE_ITEM_LOADING) {
            ((ItemHolder) holder).bindData((PaymentHistoryItem) itemList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (itemList.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else {
            return TYPE_ITEM;
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int position);

        void onExpandClick(int position);
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        public TextView txtName, txPhoneNumber, txtMoney, txtDate;

        public ItemHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.tv_name);
            txPhoneNumber = view.findViewById(R.id.tv_phone_number);
            txtDate = view.findViewById(R.id.tv_date);
            txtMoney = view.findViewById(R.id.tv_money);
        }

        public void bindData(final PaymentHistoryItem paymentHistory) {
            txtName.setText(paymentHistory.getFtthName());
            txPhoneNumber.setText(paymentHistory.getFtthAccount());
            txtDate.setText(paymentHistory.getPaymentDate());
            txtMoney.setText("$" + paymentHistory.getAmount());
        }

    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}

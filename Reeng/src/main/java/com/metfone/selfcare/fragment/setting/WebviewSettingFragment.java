package com.metfone.selfcare.fragment.setting;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.metfone.selfcare.R;

import butterknife.BindView;

public class WebviewSettingFragment extends BaseSettingFragment {

    private static final String URL_KEY = "url key";
    private static final String TITLE_KEY = "title key";

    @BindView(R.id.webView)
    WebView webView;

    public static WebviewSettingFragment newInstance(String url, int title) {

        Bundle args = new Bundle();
        args.putString(URL_KEY, url);
        args.putInt(TITLE_KEY, title);
        WebviewSettingFragment fragment = new WebviewSettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView(View view) {
        setTitle(getArguments().getInt(TITLE_KEY));
        // setting webview
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        if(!TextUtils.isEmpty(getArguments().getString(URL_KEY))) {
            webView.loadUrl(getArguments().getString(URL_KEY));
        }else {
            if(mParentActivity != null){
                mParentActivity.onBackPressed();
            }
        }
    }

    @Override
    public String getName() {
        return "WebviewSettingFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_webview_setting_v5;
    }
}

package com.metfone.selfcare.fragment.login;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.ProfileHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.IllegalFormatConversionException;

/**
 * Created by nickgun on 8/15/14.
 */
public class PersonalInfoFragment extends Fragment implements ClickListener.IconListener, ProfileListener {
    private final String TAG = PersonalInfoFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private LoginActivity mLoginActivity;
    private OnFragmentPersonalInfoListener mListener;
    private EditText etName;
    private TextView etDate, mTvwAvatar, mTvwAvatarDefault;
    private Button btnContinue;
    private View btnImportFromFacebook;
    private CircleImageView mImgAvatar;
    private Handler mHandler;
    private RadioGroup mRadioGroup;
    private DatePickerDialog mDatePickerDialog;
    private Resources mRes;
    //    private EllipsisTextView mTvwSkip;
    private EllipsisTextView mTvwTitle;
    private View rootView;
    private View mViewAVNO, mViewAVNOUnderLine;
    private TextView mTvwAVNOLabel, mTvwAVNOValue;
    private RoundTextView mTvwAVNORegister;

    private View mViewAVNOIdentifyCard;
    // for faceboook
    //    private FacebookHelper mFacebookHelper;
    private ClickListener.IconListener mIconListener;
    private String blockCharacterSet = "\\\"{}";
    private boolean dateSet = false;

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                if ("{".equals(source.toString()) || "}".equals(source.toString())
                        || "\"".equals(source.toString()) || "\\".equals(source.toString())) {
                    mLoginActivity.showToast(R.string.name_filter);
                }
                return "";
            }
            return null;
        }
    };

    private InputFilter filterLength = new InputFilter.LengthFilter(Constants.CONTACT.USER_NAME_MAX_LENGTH);

    public static PersonalInfoFragment newInstance(String param1, String param2) {
        PersonalInfoFragment fragment = new PersonalInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_PARAM1, param1);
        bundle.putString(ARG_PARAM2, param2);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        mIconListener = this;
        ListenerHelper.getInstance().addProfileChangeListener(this);
        ProfileHelper.drawPersonalInfoAVNO(mApplication, mLoginActivity, mViewAVNO,
                mViewAVNOUnderLine, mTvwAVNOLabel, mTvwAVNOValue, mTvwAVNORegister);
        super.onResume();
    }

    @Override
    public void onPause() {
        ListenerHelper.getInstance().removeProfileChangeListener(this);
        mIconListener = null;
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mHandler = new Handler();
        rootView = inflater.inflate(R.layout.fragment_personal_info, container, false);
        findComponentViews(rootView);
        setViewListeners();
        drawProfile();
        setActionBar();
        /*ProfileHelper.drawPersonalInfoAVNO(mApplication, mLoginActivity, mViewAVNO,
                mViewAVNOUnderLine, mTvwAVNOLabel, mTvwAVNOValue, mTvwAVNORegister);*/
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        mLoginActivity = (LoginActivity) activity;
        mRes = mLoginActivity.getResources();
        mLoginActivity.setTitle(mRes.getString(R.string.enter_code).toUpperCase());
        mApplication = (ApplicationController) mLoginActivity.getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        super.onAttach(activity);
        try {
            mListener = (OnFragmentPersonalInfoListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentPersonalInfoListener");
        }
    }

    @Override
    public void onDestroyView() {
        mImgAvatar.setImageBitmap(null);
        mHandler = null;
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        Log.d(TAG, "onIconClickListener: " + menuId);
        switch (menuId) {
//            case MessageConstants.MENU.REMOVE_AVATAR:
//                removeAvatar();
//                break;
            case Constants.MENU.CAPTURE_IMAGE:
                mListener.takeAPhoto();
                break;
            case Constants.MENU.SELECT_GALLERY:
                mListener.openGallery();
                break;
            default:
                break;
        }
    }

    @Override
    public void onProfileChange() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mRadioGroup.setOnCheckedChangeListener(null);
            }
        });
    }

    @Override
    public void onRequestFacebookChange(final String userName, final String birthDay, final int gender) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (birthDay != null && !birthDay.isEmpty()) {
                    etDate.setText(birthDay);
                }
                if (gender == Constants.CONTACT.GENDER_FEMALE) {
                    mRadioGroup.check(R.id.radio_sex_female);
                } else {
                    mRadioGroup.check(R.id.radio_sex_male);
                }
                if (userName != null) {
                    etName.setText(TextHelper.fromHtml(userName));
                    etName.setSelection(etName.getText().toString().length());
                }
                //setEnableContinueButton();
            }
        });
    }

    @Override
    public void onAvatarChange(final String avatarPath) {
/*        mHandler.post(new Runnable() {
            @Override
            public void run() {
                updateAvatar(avatarPath);
            }
        });*/
    }

    @Override
    public void onCoverChange(String avatarPath) {
        Log.d(TAG, "onCoverChange avatarPath=" + avatarPath);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mRadioGroup.setOnCheckedChangeListener(null);
    }

    public void updateAvatar(String filePath) {
        ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
        if (mReengAccount != null) {
//            mImgAvatar.setBorderWidth(0);
//            mTvwAvatarDefault.setVisibility(View.GONE);
            mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatarDefault, mReengAccount,
                    filePath);
        }
    }

    private void drawProfile() {
        if (mHandler == null) mHandler = new Handler();
        mLoginActivity.showLoadingDialog("", R.string.processing);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
                if (mReengAccount != null) {
                    getUserInfo(mReengAccount);
                } else {
                    mLoginActivity.hideLoadingDialog();
                }
            }
        }, 1500);
    }

    private void findComponentViews(View view) {
        mImgAvatar = view.findViewById(R.id.profile_avatar_circle);
        mTvwAvatar = view.findViewById(R.id.contact_avatar_text);
        mTvwAvatarDefault = view.findViewById(R.id.text_avatar_default);
        etName = view.findViewById(R.id.edit_name);
        etName.setFilters(new InputFilter[]{filterLength, filter});
        etDate = view.findViewById(R.id.tv_date);
        btnImportFromFacebook = view.findViewById(R.id.btn_get_info_from_facebook);
        btnContinue = view.findViewById(R.id.btn_continue);
        mRadioGroup = view.findViewById(R.id.group_sex);
        // avno
        mViewAVNO = view.findViewById(R.id.info_avno_layout);
        mViewAVNOUnderLine = view.findViewById(R.id.info_avno_underline);
        mTvwAVNOLabel = view.findViewById(R.id.info_avno_label);
        mTvwAVNOValue = view.findViewById(R.id.info_avno_value);
        mTvwAVNORegister = view.findViewById(R.id.info_avno_register);
        InputMethodUtils.hideKeyboardWhenTouch(view, mLoginActivity);
        btnContinue.setEnabled(true);
        etName.requestFocus();
        /*if (Config.Server.FREE_15_DAYS) {
            btnImportFromFacebook.setVisibility(View.GONE);
        } else {*/
        btnImportFromFacebook.setVisibility(View.VISIBLE);
        //}
        View skip = view.findViewById(R.id.ab_skip);
        skip.setVisibility(View.GONE);

        mViewAVNOIdentifyCard = view.findViewById(R.id.view_identify_card);
        mViewAVNOIdentifyCard.setVisibility(View.GONE);
    }

    private void setViewListeners() {
        btnContinueListener();
        imgAvatarListener();
        editNameListener();
        editDateListener();
        btnImportFromFBListener();
        mTvwAVNORegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigateActivityHelper.navigateToChangeAVNO(mApplication, mLoginActivity, AVNOActivity.FINISH_ACTIVITY);
            }
        });
//        setKeyboardChangeListener();
//        buttonSkipListener();
//        radioGroupListener();
    }

    private void editDateListener() {
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDatePickerDialog != null && mDatePickerDialog.isShowing()) {
                    return;
                }
                showDateDialog();
            }
        });
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            Log.d(TAG, "onDateSet");
            mDatePickerDialog.dismiss();
            if (!dateSet) {
                dateSet = true;
                Calendar cal = Calendar.getInstance();
                cal.set(selectedYear, selectedMonth, selectedDay);
                int old = TimeHelper.getOldFromBirthday(cal.getTimeInMillis());
                if (old > 90) {
                    showDialogBanana();
                } else {
                    StringBuilder dateString = new StringBuilder().append(selectedDay)
                            .append("/").append(selectedMonth + 1).append("/").append(selectedYear);
                    /*Long birthDay = TimeHelper.convertBirthdayToTime(dateString.toString());
                    String mDate = TimeHelper.formatTimeBirthday(birthDay);*/
                    etDate.setText(dateString.toString());
                    //setEnableContinueButton();
                }
            }
        }
    };

    private void showDialogBanana() {
        ClickListener.IconListener listener = new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                // khong lam gi ca
            }
        };
        String title = mRes.getString(R.string.note_title);
        String labelOk = mRes.getString(R.string.close);
        String msg = mRes.getString(R.string.msg_note_banana);
        PopupHelper.getInstance().showDialogConfirm(mLoginActivity, title,
                msg, labelOk, null, listener, null, -1);
    }

    private void showDateDialog() {
        Calendar c = Calendar.getInstance();
        if (etDate.getText().toString().trim().length() > 0) {
            c.setTimeInMillis(TimeHelper.convertBirthdayToTime(etDate.getText().toString().trim()));
        } else {
            c.setTimeInMillis(TimeHelper.BIRTHDAY_DEFAULT_PICKER);
        }
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        //
        Calendar minCalender = Calendar.getInstance();
        minCalender.add(Calendar.YEAR, -100);
        Calendar maxCalender = Calendar.getInstance();
        maxCalender.add(Calendar.YEAR, -12);
        maxCalender.set(Calendar.MONTH, Calendar.JANUARY);
        maxCalender.set(Calendar.DAY_OF_MONTH, 1);
        Context context = new ContextWrapper(getActivity()) {
            private Resources wrappedResources;

            @Override
            public Resources getResources() {
                Resources r = super.getResources();
                if (wrappedResources == null) {
                    wrappedResources = new Resources(r.getAssets(), r.getDisplayMetrics(), r.getConfiguration()) {
                        @NonNull
                        @Override
                        public String getString(int id, Object... formatArgs) throws NotFoundException {
                            try {
                                return super.getString(id, formatArgs);
                            } catch (IllegalFormatConversionException ifce) {
                                Log.e("DatePickerDialogFix", "IllegalFormatConversionException Fixed!", ifce);
                                String template = super.getString(id);
                                template = template.replaceAll("%" + ifce.getConversion(), "%s");
                                return String.format(getConfiguration().locale, template, formatArgs);
                            }
                        }
                    };
                }
                return wrappedResources;
            }
        };
        mDatePickerDialog = new DatePickerDialog(context,
                datePickerListener, mYear, mMonth, mDay);
        DatePicker datePicker = mDatePickerDialog.getDatePicker();
        datePicker.setMinDate(minCalender.getTimeInMillis());
        datePicker.setMaxDate(maxCalender.getTimeInMillis());
        dateSet = false;
        mDatePickerDialog.show();
    }

    public void btnContinueListener() {
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString().trim();
                long birthday = TimeHelper.convertBirthdayToTime(etDate.getText().toString());
                if (!TextUtils.isEmpty(name) && birthday != TimeHelper.BIRTHDAY_DEFAULT) {
                    sendInfoToServerAndUpdateInfo(name, birthday);
                } else if (TextUtils.isEmpty(name)) {
                    mLoginActivity.showToast(mRes.getString(R.string.msg_validate_name), Toast.LENGTH_LONG);
                } else {// birthday trong
                    mLoginActivity.showToast(mRes.getString(R.string.msg_validate_birthday), Toast.LENGTH_LONG);
                }
            }
        });
    }

    public void imgAvatarListener() {
        mImgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupChangeAvatar();
            }
        });
    }

    public void editNameListener() {
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                //setEnableContinueButton();
//                if(TextHelper.hasSpecialCharacter(etName.getText().toString())){
//                    Toast.makeText(mLoginActivity,R.string.name_special_char, Toast.LENGTH_SHORT).show();
//                    String text = etName.getText().toString();
//                    text = text.substring(0, text.length()-1);
//                    etName.setText(text);
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                etName.clearFocus();
                InputMethodUtils.showSoftKeyboard(mLoginActivity, etName);
                return false;
            }
        });
    }

    public void btnImportFromFBListener() {
        btnImportFromFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.getInfoFacebook();
            }
        });
    }

    /*public void buttonSkipListener() {
        mTvwSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.goToHome();
            }
        });
    }*/

    /*private void setEnableContinueButton() {
        if (etName.getText().toString().trim().length() > 0
                && etDate.getText().toString().trim().length() > 0)
            btnContinue.setEnabled(true);
        else
            btnContinue.setEnabled(false);
    }*/

    private void showPopupChangeAvatar() {
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
//        ItemContextMenu removeAvatar = new ItemContextMenu(res, res.getString(R.string.remove_avatar),
//                -1, null, MessageConstants.MENU.REMOVE_AVATAR);
        ItemContextMenu selectGallery = new ItemContextMenu(mRes.getString(R.string
                .select_from_gallery),
                -1, null, Constants.MENU.SELECT_GALLERY);
        ItemContextMenu capture = new ItemContextMenu(mRes.getString(R.string.capture_image),
                -1, null, Constants.MENU.CAPTURE_IMAGE);
        ReengAccount reengAccount = mAccountBusiness.getCurrentAccount();
        String lastChangeAvatar = reengAccount.getLastChangeAvatar();
        if (lastChangeAvatar != null && lastChangeAvatar.length() > 0) {
            listItem.add(capture);
            listItem.add(selectGallery);
//            listItem.add(removeAvatar);
        } else {
            listItem.add(capture);
            listItem.add(selectGallery);
        }
        PopupHelper.getInstance().showContextMenu(mLoginActivity, null, listItem, this);
    }

    private void sendInfoToServerAndUpdateInfo(String name, long birthday) {
        String titleFail = mRes.getString(R.string.error);
        if (!NetworkHelper.isConnectInternet(mLoginActivity)) {
            mLoginActivity.showError(mRes.getString(R.string.error_internet_disconnect), titleFail);
            return;
        }
        try {
            // name = TextHelper.getInstant().escapeXml(name);
            // mLoginActivity.showLoadingDialog("", R.string.processing);
            ReengAccount account = mAccountBusiness.getCurrentAccount();
            ApplicationController applicationController = (ApplicationController) mLoginActivity.getApplication();
            if (account != null) {
                int gender = 0;
                if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_sex_male) {
                    gender = 1;
                }
                //    name = StringUtils.escapeForXML(name);
                account.setName(name);
                Log.i(TAG, "update name" + name);
                String birthDay = String.valueOf(birthday);
                account.setBirthday(birthDay);
                account.setGender(gender);
                account.setBirthdayString(TimeHelper.formatTimeBirthdayString(birthday));
                mAccountBusiness.updateReengAccount(account);
                setUserInfo(applicationController, account);
                mListener.goToHome();
            }
            /* mAccountBusiness.changeUserName(etName.getText().toString());
            //neu send thanh cong thi update vao database
            ReengAccount account = mAccountBusiness.getCurrentAccount(mLoginActivity);
            if (account != null) {
                account.setName(etName.getText().toString());
                mAccountBusiness.updateReengAccount(account);
                //va vao home activity
                mListener.goToHome();*/
            //            }
        } catch (Exception e) {
            Log.e(TAG, "sendInfoToServer", e);
        }
    }

    private void setActionBar() {
        LayoutInflater mLayoutInflater = (LayoutInflater) mLoginActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        View abView = mLoginActivity.getToolBarView();
        mLoginActivity.setCustomViewToolBar(mLayoutInflater.inflate(
                R.layout.ab_personal_info, null));
        abView.setVisibility(View.GONE);
        /*mTvwContinue = (EllipsisTextView) abView.findViewById(R.id.ab_skip);
        mTvwContinue.setVisibility(View.GONE);
        mTvwContinue.setText(mRes.getString(R.string.update));
        mTvwContinue.setEnabled(true);
        mTvwContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "---setActionBar---------------------------------mTvwContinue---");
                mListener.goToHome();
            }
        });*/
        /*ImageView mImgBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mImgBack.setVisibility(View.GONE);
        mTvwTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        int dpAsPixels = (int) (mApplication.getResources().getDimension(R.dimen.margin_more_content_15));
        mTvwTitle.setPadding(dpAsPixels, 0, 0, 0);*/
    }

    // tam thoi comment show popup error khi ko lay duoc thong tin tu sv
    // thong bao loi lam nguoi dung hieu lam ko dang nhap duoc ung dung
    public void getUserInfo(ReengAccount account) {
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                mLoginActivity.hideLoadingDialog();
                if (!TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getLastChangeAvatar())) {
                    //mImgAvatar.setBorderWidth(0);
                    //mTvwAvatarDefault.setVisibility(View.GONE);
                    mApplication.getAvatarBusiness().setMyAvatarFromGetUserInfo(mImgAvatar,
                            mTvwAvatar, mTvwAvatarDefault, account);
                }
                String birthDay = "";
                if (!TextUtils.isEmpty(account.getBirthdayString())) {
                    birthDay = TimeHelper.getStringBirthday(account.getBirthdayString());
                }
                etDate.setText(birthDay);
                if (account.getGender() == 1) {
                    mRadioGroup.check(R.id.radio_sex_male);
                } else {
                    mRadioGroup.check(R.id.radio_sex_female);
                }
                if (account.getName() != null) {
                    if (etName.getText().toString().length() <= 0) {
                        etName.setText(account.getName());
                        etName.setSelection(etName.getText().toString().length());
                    }
                }
                if (!mLoginActivity.isFinishing()) {
                    ProfileHelper.drawPersonalInfoAVNO(mApplication, mLoginActivity, mViewAVNO,
                            mViewAVNOUnderLine, mTvwAVNOLabel, mTvwAVNOValue, mTvwAVNORegister);
                }
            }

            @Override
            public void onError(int errorCode) {
                mLoginActivity.hideLoadingDialog();
            }
        };
        ProfileRequestHelper.getInstance(mApplication).getUserInfo(account, listener);
    }

    public void setUserInfo(final ApplicationController ctx, final ReengAccount account) {
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                mLoginActivity.showToast(R.string.update_info_ok);
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "onError: " + errorCode);
                mLoginActivity.showToast(R.string.update_infor_fail);
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setUserInfo(account, listener);
    }

    public interface OnFragmentPersonalInfoListener {
        void goToHome();

        void takeAPhoto();

        void openGallery();

        void getInfoFacebook();
    }
}

package com.metfone.selfcare.fragment.contact;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.home.HomeContactsAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.home.HomeContactsHelper;
import com.metfone.selfcare.listeners.SimpleContactsHolderListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 7/17/2018.
 */

public class ListFriendMochaFragment extends BaseRecyclerViewFragment implements BaseRecyclerViewFragment.EmptyViewListener, StrangerBusiness.onSocialFriendsRequestListener {

    private static final String TAG = ListFriendMochaFragment.class.getSimpleName();
    private ArrayList<SocialFriendInfo> listSocialFriend;
    private View mViewFooter;
    private LinearLayout mLoadMoreFooterView;
    private RecyclerView mRecyclerView;
    private boolean isFriendLoaded;
    private HomeContactsAdapter mAdapter;
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mActivity;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAdapter;
    private boolean isLoading;
    private boolean noMore;

    public static ListFriendMochaFragment create() {
        return new ListFriendMochaFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseSlidingFragmentActivity) getActivity();
        if (mActivity != null) {
            mApplication = (ApplicationController) mActivity.getApplication();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        findComponentViews(rootView,container, inflater);
        showProgressLoading();
        loadData();
        return rootView;
    }

    private void loadData() {
        isLoading = true;
        mApplication.getStrangerBusiness().getSocialFriendRequest(StrangerBusiness.TYPE_SOCIAL_FRIEND, false, this);
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewFooter = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mLoadMoreFooterView = mViewFooter.findViewById(R.id.layout_loadmore);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        mViewFooter.setVisibility(View.GONE);
        mLoadMoreFooterView.setVisibility(View.GONE);
        showOrHideLoadingView();
        initAdapter();

        mActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail_no_action, null));
        View abView = mActivity.getToolBarView();
        ImageView mImgBack = abView.findViewById(R.id.ab_back_btn);
        TextView mTvTitle = abView.findViewById(R.id.ab_title);
        mTvTitle.setText(mActivity.getResources().getString(R.string.social_tab_friend));
        mImgBack.setOnClickListener(v -> mActivity.onBackPressed());
    }

    private void showOrHideLoadingView() {
        if (listSocialFriend == null) {
            if (isFriendLoaded) {
                showRetryView();
            } else {
                showProgressLoading();
            }
        } else {
            hideEmptyView();
        }
    }

    private void initAdapter() {
        if (mAdapter == null) {
            mAdapter = new HomeContactsAdapter(mApplication, new ArrayList<>());
            mHeaderAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            mRecyclerView.setAdapter(mHeaderAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mHeaderAdapter.addFooterView(mViewFooter);
            setAdapterListener();
            setLoadMoreListener();
        }
    }

    private void setLoadMoreListener() {
        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {//check for scroll down
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
                    if (layoutManager == null) return;
                    if (((layoutManager.getChildCount() + layoutManager.findFirstVisibleItemPosition()) >= layoutManager.getItemCount())
                            && !isLoading && listSocialFriend != null && !listSocialFriend.isEmpty()) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }
            }
        };
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));
    }

    private void onLoadMore() {
        if (!noMore) {
            Log.i(TAG, "loaddata onLoadMore ");
            isLoading = true;
            mApplication.getStrangerBusiness().getSocialFriendRequest(StrangerBusiness.TYPE_SOCIAL_FRIEND, true, this);
        } else {
            Log.i(TAG, "loaddata onLoadMore nomore");
        }
    }


    private void setAdapterListener() {
        mAdapter.setClickListener(new SimpleContactsHolderListener() {
            @Override
            public void onActionLabel(Object entry) {
                super.onActionLabel(entry);
            }

            @Override
            public void onAvatarClick(Object entry) {
                super.onAvatarClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mActivity, entry);
            }

            @Override
            public void onItemClick(Object entry) {
                super.onItemClick(entry);
                HomeContactsHelper.getInstance(mApplication).handleItemContactsClick(mActivity, entry);
            }

            @Override
            public void onSocialCancel(Object entry) {
                super.onSocialCancel(entry);
            }

            @Override
            public void onSectionClick(Object entry) {
                super.onSectionClick(entry);
            }
        });
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onResponse(int type, ArrayList<SocialFriendInfo> list) {
        isLoading = false;
        isFriendLoaded = true;
        listSocialFriend = list;
        initAndDrawSocial();
    }

    @Override
    public void onLoadMoreResponse(int type, ArrayList<SocialFriendInfo> list, boolean isNoMore) {
        isLoading = false;
        noMore = isNoMore;
        listSocialFriend = mApplication.getStrangerBusiness().getSocialFriends(StrangerBusiness.TYPE_SOCIAL_FRIEND);
        //listSocialFriend.addAll(list);
        initAndDrawSocial();
    }

    private void initAndDrawSocial() {
        ArrayList<Object> list = new ArrayList<>();
        if (listSocialFriend == null) {
            listSocialFriend = new ArrayList<>();
        }
        list.addAll(listSocialFriend);
        showOrHideLoadingView();
        notifyChangeAdapter(list);
    }

    private void notifyChangeAdapter(ArrayList<Object> list) {
        if (mAdapter == null) {
            initAdapter();
        }
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onError(int type, int code, boolean isLoadMore) {
        isLoading = false;
        isFriendLoaded = true;
    }
}



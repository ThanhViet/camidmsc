package com.metfone.selfcare.fragment;

import static com.metfone.selfcare.activity.BaseSlidingFragmentActivity.fromRestartApp;
import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;
import static com.metfone.selfcare.fragment.addAccount.AddAccountFragment.isNeedRefreshAccount;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIRST_TIME_OPEN_TAB_METFONE;
import static com.metfone.selfcare.module.metfoneplus.fragment.MPRootFragment.statusLogin;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_PHONE_NUMBER;
import static com.metfone.selfcare.util.EnumUtils.LoginVia.NOT;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.ashokvarma.bottomnavigation.TextBadgeItem;
import com.blankj.utilcode.util.StringUtils;
import com.metfone.esport.entity.event.ChangeAccountEvent;
import com.metfone.esport.homes.HomeFragment;
import com.metfone.esport.ui.live.LiveVideoFragment;
import com.metfone.register_payment.ui.fragment.mp_tab_internet.MPTabInternetFragment;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.fragment.musickeeng.TabStrangerFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper.HomeTab;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ChangeUnreadMessageListener;
import com.metfone.selfcare.listeners.InitConfigDataListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.model.account.PhoneLinkedInfo;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.module.games.activity.ContainerActivity;
import com.metfone.selfcare.module.games.fragment.GameHomePageFragment;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGetAccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsCheckForceUpdateAppResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsPopupTetResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.game.GameFragment;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.module.home_kh.notification.model.FragmentNotification;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.metfoneplus.billpayment.MPBillPaymentDetailFragment;
import com.metfone.selfcare.module.metfoneplus.billpayment.MPBillPaymentFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddFeedbackFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddPhoneMetfoneFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPBuyServiceDetailFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailAccountFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailSmsCallDataFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPExchangeGuideLineFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPMapsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPMyServiceFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPRootFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPServiceFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPStoreFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPSupportFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPTabEnterAddPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPTabMobileFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPTabMobileLoginSignUpFragment;
import com.metfone.selfcare.module.metfoneplus.ishare.fragment.IShareFragment;
import com.metfone.selfcare.module.metfoneplus.search.fragment.BuyPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.topup.QrCodeTopUpFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.movienew.model.evenbus.MessageStopVideo;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.view.NonSwipeableViewPager;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.viettel.util.LogDebugHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by toanvk2 on 7/27/2017.
 */

public class HomePagerFragment extends Fragment implements BottomNavigationBar.OnTabSelectedListener
        , ChangeUnreadMessageListener, ContentConfigBusiness.OnConfigChangeListener
        , InitDataListener, InitConfigDataListener, IOnBackPressed, OnInternetChangedListener {
    private static final String TAG = HomePagerFragment.class.getSimpleName();
    @SuppressLint("StaticFieldLeak")
    private static HomePagerFragment mSelf;
    private final int tabStrangerIndex = -1;
    private final boolean isSelectedTab = false;
    // Store instance variables
    private ApplicationController mApplication;
    private HomeActivity mParentActivity;
    private Resources mRes;
    private NonSwipeableViewPager mViewPager;
    private BottomNavigationBar bottomNavigationBar;
    private PagerAdapter mHomePagerAdapter;
    private TextBadgeItem mBadgeHome, mBadgeChat, mBadgeOnmedia, mBadgeVideo, mBadgeMusic, mBadgeMovie, mBadgeNews, mBadgeSecurity, mBadgeTiins;
    private int currentPos = 0;
    private int unreadMsg = 0;
    private boolean isSelectMode = false;
    private ArrayList<ConfigTabHomeItem> listTabHome;
    private boolean mIsHaveMetfonePlusEqual1 = false;
    private CamIdUserBusiness mCamIdUserBusiness;
    private int mBottomNavigationBarBackgroundCurrent = R.color.m_home_tab_background;
    private UserInfoBusiness userInfoBusiness;
    private boolean isLoadingAvatar = false;
    private ListenerUtils mListenerUtils;
    private AccountRankDTO accountRankDTO;
    private KhHomeClient homeKhClient;
    private NewYearDialog dialogNewYear;


    // newInstance constructor for creating fragment with arguments
    public static HomePagerFragment newInstance() {
        HomePagerFragment fragmentFirst = new HomePagerFragment();
        Bundle args = new Bundle();
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    public static HomePagerFragment self() {
        if (mSelf != null) {
            return mSelf;
        }
        return newInstance();
    }

    public static String AssetJSONFile(String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }

    public static int heightBottomMenu = 0;

    @Override
    public void onAttach(Activity activity) {
        this.mParentActivity = (HomeActivity) activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        this.mCamIdUserBusiness = mApplication.getCamIdUserBusiness();
        this.mRes = mApplication.getResources();
        super.onAttach(activity);
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoBusiness = new UserInfoBusiness(getContext());
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mListenerUtils = ApplicationController.self().getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);
    }


    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        long time = System.currentTimeMillis();
        View view = inflater.inflate(R.layout.fragment_home_main, container, false);
        mViewPager = view.findViewById(R.id.home_fragment_view_pager);

        mViewPager.post(() -> mViewPager.setPagingEnabled(false));
        // mToolbar = (Toolbar) view.findViewById(R.id.tool_bar);
        //activity.setSupportActionBar(mToolbar);
        bottomNavigationBar = view.findViewById(R.id.home_fragment_bottom_navigation_bar);
        mParentActivity.initHomePagerAndActionBarView(mViewPager, bottomNavigationBar);

        if (heightBottomMenu == 0) {
            bottomNavigationBar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    bottomNavigationBar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    heightBottomMenu = bottomNavigationBar.getHeight(); //height is ready
                }
            });
        }

        //Get duoc config xong moi notify adapter
        if (mApplication.getConfigBusiness().isReady()) {
            setAdapter();
        } else {
            mParentActivity.showLoadingDialog("", R.string.loadding_data);
            ListenerHelper.getInstance().addInitConfigDataListener(this);

            LogDebugHelper.getInstance().logDebugContent("Draw item home pager init config bussiness not ready");
            ReportHelper.reportError(mApplication, ReportHelper.DATA_TYPE_TAB_CONFIG, "Draw item home pager init config bussiness not ready");
        }
        ListenerHelper.getInstance().addInitDataListener(this);
        mApplication.setChangeUnreadMessageListener(this);
        ListenerHelper.getInstance().addConfigChangeListener(this);
//        Utilities.adaptViewForInsertBottom(bottomNavigationBar.getBottomNavigationBarItemContainer());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).setIOnBackPressed(this);
        }
    }

    public static boolean isClickTabGame = false;

    @Override
    public void onResume() {
        super.onResume();
        mSelf = this;
        // TODO số tin chưa đọc
        if (mApplication.isDataReady()) {
            unreadMsg = mApplication.getMessageBusiness().getNumberOfUnreadMessage();
            Log.i(TAG, "onResume unreadMsg: " + unreadMsg);
            changeUnreadMessageBadge();
        }

        if (isLogin() != NOT) {
            Log.d(TAG, "Da login");
            if (mCamIdUserBusiness.isProcessingLoginSignUpGame()) {
                if (isClickTabGame) {
                    isClickTabGame = false;
                    addGameHomeFragment();
                }
            }
            if (accountRankDTO == null) {
                getWSGetAccountRank();
            }
        } else {
            Log.d(TAG, "Chua login");
            EventBus.getDefault().postSticky(new LoadInfoUser(LoadInfoUser.LoadType.NOT_LOGIN));
        }

        showPopupGiftTet();
    }
    int mTabMetfonePosition = -1;
    public void gotoMetfoneTab() {
        if (mTabMetfonePosition > -1) {
            bottomNavigationBar.selectTab(mTabMetfonePosition);
        }
    }

    private void showPopupGiftTet() {
        String isdn = SharedPrefs.getInstance().get(PREF_PHONE_NUMBER, String.class);

        if (isdn == null || isdn.isEmpty() || !Utilities.isMetfoneNumber(isdn)) {
            List<Services> services = UserInfoBusiness.getInstance(requireContext()).getServiceList();
            for (Services s : services) {
                if (s.getServiceName().equals("Mobile")) {
                    for (PhoneLinkedInfo info : s.getPhoneLinkedList()) {
                        if (info.getMetfonePlus() == 1) {
                            isdn = info.getPhoneNumber();
                            break;
                        }
                    }
                    break;
                }
            }
        }

        if (Boolean.TRUE.equals(SharedPrefs.getInstance().get("SHOW_NOTIFY", Boolean.class))) {
            android.util.Log.e("ttt", "onResume: show poup form function nay");
            SharedPrefs.getInstance().put("SHOW_NOTIFY", false);
            showLunarGift(isdn);
        } else {
            checkShowPopup(isdn);
        }
    }

    public void addGameHomeFragment() {
        new Handler().postDelayed(() -> {
            if (mTabGamePosition > -1) bottomNavigationBar.selectTab(mTabGamePosition);
            else {
                Intent intent = new Intent(getActivity(), ContainerActivity.class);
                startActivity(intent);
            }
        }, 1000);

    }

    @Override
    public void onPause() {
        super.onPause();
        mSelf = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ListenerHelper.getInstance().removeInitDataListener(this);
        ListenerHelper.getInstance().removeInitConfigDataListener(this);
        ListenerHelper.getInstance().removeConfigChangeListener(this);
        mApplication.setChangeUnreadMessageListener(null);
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint: " + isVisibleToUser);
        mSelf = isVisibleToUser ? this : null;
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mViewPager != null) {
            mParentActivity.setTabSelected(mViewPager.getCurrentItem());
            //drawStatusBar(mViewPager.getCurrentItem(), false);
        }
    }

    @Override
    public void onDataReady() {
        unreadMsg = mApplication.getMessageBusiness().getNumberOfUnreadMessage();
        Log.i(TAG, "onDataReady unreadMsg: " + unreadMsg);
        changeUnreadMessageBadge();
        checkAndShowAlertTabHot();
        checkAndShowAlertTabVideo();
        checkAndShowAlertTabMovie();
        checkAndShowAlertTabMusic();
        checkAndShowAlertTabNews();
        checkAndShowAlertTabSecurity();
    }

    @Override
    public void onChangeUnreadMessage(int numberUnreadMsg) {
        unreadMsg = numberUnreadMsg;
        Log.i(TAG, "onChangeUnreadMessage unreadMsg: " + unreadMsg);
        changeUnreadMessageBadge();
    }

    public static int mTabGamePosition = -1;

    public void openTabGameFromIconHome() {
        if (mTabGamePosition > -1) bottomNavigationBar.selectTab(mTabGamePosition);
    }

    private void setAdapter() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        initItemHomeAdapter();
        bottomNavigationBar.setTabSelectedListener(this);
        mViewPager.setAdapter(mHomePagerAdapter);
        mViewPager.setOffscreenPageLimit(6);

        /*mViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                page.setAlpha(position <= -1f || position >= 1f ? 0f : 1f);
            }
        });*/

        if (mCamIdUserBusiness.isProcessingLoginSignUpGame()
                && !"".equals(mCamIdUserBusiness.getCamIdToken())) {
            TabHomeHelper.TAB_CURRENT = mCamIdUserBusiness.getPositionGameTab() == -1 ? TabHomeHelper.TAB_CURRENT : mCamIdUserBusiness.getPositionGameTab();
        }

        if (fromRestartApp) {
            TabHomeHelper.TAB_CURRENT = 0;
            fromRestartApp = false;
        }
        mParentActivity.setCurrentTabPosition(TabHomeHelper.TAB_CURRENT);
        mViewPager.setCurrentItem(TabHomeHelper.TAB_CURRENT);
        bottomNavigationBar.selectTab(TabHomeHelper.TAB_CURRENT);
        //drawStatusBar(currentPos, false);
        mParentActivity.setTabSelected(mViewPager.getCurrentItem());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            float sum;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (Float.compare(positionOffset, 0) == 0) {
                    currentPos = position;
                }
                sum = position + positionOffset;
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    List<Fragment> fragments = getChildFragmentManager().getFragments();
                    if (fragments != null) {
                        for (Fragment fragment : fragments) {
                            if (fragment instanceof MPExchangeGuideLineFragment) {
                                EventBus.getDefault().postSticky(new MPExchangeGuideLineFragment());
                                break;
                            }
                        }
                    }
                }
                if (position == 5) {
                    //todo nếu là tab esport thì resume auto play
                    try {
                        Fragment fragment = getChildFragmentManager().getFragments().get(1);
                        if (fragment instanceof HomeFragment) {
                            new Handler().postDelayed(() -> ((HomeFragment) fragment).resumeAutoPlay(), 100L);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //todo nếu khác tab esport thì pause auto play
                    try {
                        Fragment fragment = getChildFragmentManager().getFragments().get(1);
                        if (fragment instanceof HomeFragment) {
                            new Handler().postDelayed(() -> ((HomeFragment) fragment).pauseAutoPlay(true), 100L);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                mParentActivity.setTabSelected(position);
                if (mBadgeChat != null) {
                    if (unreadMsg > 0) {
                        Log.i(TAG, "showBadgeChat");
                        String textBadgeChat = unreadMsg > 5 ? "+5" : String.valueOf(unreadMsg);
                        mBadgeChat.setText(textBadgeChat);
                        mBadgeChat.show(false);
                    } else {
                        Log.i(TAG, "hideBadgeChat");
                        mBadgeChat.setText("0");
                        mBadgeChat.hide(true);
                    }
                }
                // stop available video when change tab
                if (position != 2) {
                    EventBus.getDefault().postSticky(new MessageStopVideo("Change tab, so need stop video"));
                }
//                if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_home, null)) {
                if (position == 0) {
                    onTabHomeSelected();
//                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_chat, null)) {
                } else if (position == 4) {
                    onTabChatSelected();
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_hot, null) && mApplication.getConfigBusiness().isEnableOnMedia()) {
                    onTabHotSelected();// change badge
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_SOCIAL, "");
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_video, null)) { //tab video
                    onTabVideoSelected();
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_VIDEOS, "");
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_movie, null)) {
                    onTabMovieSelected();
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_MOVIES, "");
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_music, null)) {
                    onTabMusicSelected();
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_MUSIC, "");
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_news, null)) {
                    onTabNewsSelected();
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_NEWS, "");
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_security, null)) {
                    onTabSecuritySelected();
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_stranger, null)) {
                    onTabStrangerSelected();
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_DATING, "");
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_tiins, null)) {
                    Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_TIIN, "");
                    onTabTiinSelected();
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_selfcare, null)) {
//                   onTabSelfCareSelected();
//                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_cinema, null)) {
                } else if (position == 2) {
                    onTabCinemaSelected();
//                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_esport, null)) {
                } else if (position == 5) {
                    onTabESortSelected();
//                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_metfone, null)) {
                } else if (position == 3) {
                    onTabMetfoneSelected();
                } else if (position == TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(HomeTab.tab_contact, null)) {
                    onTabContactSelected();
                } else {
                    if (listTabHome != null && listTabHome.size() > position && position >= 0) {
                        ConfigTabHomeItem item = listTabHome.get(position);
                        if (item != null && item.getHomeTab() == HomeTab.tab_wap) {
                            onTabWapSelected(item);
                            Utilities.showAdsMainTab(mApplication, mParentActivity, Constants.MAIN_TAB_WAP, item.getId());
                        }
                    }
                }

                //Firebase log event
                String tabName = "";
                if (position == 0) {
                    tabName = "Home";
                } else if (position == 1) {
                    tabName = "Game";
                } else if (position == 2) {
                    tabName = "Cinema";
                } else if (position == 3) {
                    tabName = "MetfonePlus";
                } else if (position == 4) {
                    tabName = "Chat";
                } else if (position == 5) {
                    tabName = "eSport";
                }
                ApplicationController.self().getFirebaseEventBusiness().logSelectTab(tabName);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.i("Animation: state", state + "");
            }
        });
    }

    private void onTabWapSelected(ConfigTabHomeItem item) {
        if (item.getId().equals("1") && item.getName().equalsIgnoreCase(mParentActivity.getString(R.string.c_comic))) {
            mApplication.logEventFacebookSDKAndFirebase(mParentActivity.getString(R.string.c_comic));
        } else if (item.getId().equals("2") && item.getName().equalsIgnoreCase("Horasas")) {
            mApplication.logEventFacebookSDKAndFirebase(mParentActivity.getString(R.string.c_xemboi));
        }
    }

    private void onTabStrangerSelected() {
        mApplication.logEventFacebookSDKAndFirebase(mParentActivity.getString(R.string.c_lamquen));
    }

    private void onTabTiinSelected() {
        mParentActivity.trackingEvent(R.string.click_tab_tiin, R.string.action_tab_tiin, "");
    }

    private void onTabChatSelected() {
        TabMobileFragment.TabMobileMessageEvent event = new TabMobileFragment.TabMobileMessageEvent();
        event.setSelectedTabMobile(true);
        EventBus.getDefault().postSticky(event);
    }

    private void onTabVideoSelected() {
        if (mBadgeVideo != null && !mBadgeVideo.isHidden()) {
            Log.i(TAG, "save time: " + System.currentTimeMillis());
            mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_VIDEO, System
                    .currentTimeMillis()).apply();
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBadgeVideo.hide(false);
                }
            });
        }
        mApplication.logEventFacebookSDKAndFirebase(mParentActivity.getString(R.string.c_video));
    }

    private void onTabMovieSelected() {
        if (mBadgeMovie != null && !mBadgeMovie.isHidden()) {
            Log.i(TAG, "save time: " + System.currentTimeMillis());
            mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_MOVIE, System
                    .currentTimeMillis()).apply();
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBadgeMovie.hide(false);
                }
            });
        }
        mApplication.logEventFacebookSDKAndFirebase(mParentActivity.getString(R.string.c_5dmax));
    }

    private void onTabMusicSelected() {
        if (mBadgeMusic != null && !mBadgeMusic.isHidden()) {
            Log.i(TAG, "save time: " + System.currentTimeMillis());
            mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_MUSIC, System
                    .currentTimeMillis()).apply();
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBadgeMusic.hide(false);
                }
            });
        }
        mApplication.logEventFacebookSDKAndFirebase(mParentActivity.getString(R.string.c_nhac));
    }

    private void onTabNewsSelected() {
        if (mBadgeNews != null && !mBadgeNews.isHidden()) {
            Log.i(TAG, "save time: " + System.currentTimeMillis());
            mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_NEWS, System
                    .currentTimeMillis()).apply();
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBadgeNews.hide(false);
                }
            });
        }
        mParentActivity.trackingEvent(R.string.click_tab_new, R.string.action_tab_new, "");
    }

    private void onTabSecuritySelected() {
        if (mBadgeSecurity != null && !mBadgeSecurity.isHidden()) {
            Log.i(TAG, "save time: " + System.currentTimeMillis());
            mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_SECURITY, System
                    .currentTimeMillis()).apply();
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBadgeSecurity.hide(false);
                }
            });
        }
    }

    // TODO CamID: home_tab_config
    // [start] CamID
    private void onTabCinemaSelected() {
        Log.d(TAG, "onTabCinemaSelected()");
    }

    private void onTabESortSelected() {
        Log.d(TAG, "onTabESortSelected()");
    }

    private void onTabMetfoneSelected() {
        if (isNeedRefreshAccount) {
            isNeedRefreshAccount = false;
            for (int i = 0; i < getChildFragmentManager().getFragments().size(); i++) {
                if (getChildFragmentManager().getFragments().get(i) instanceof MPRootFragment) {
                    ((MPRootFragment) getChildFragmentManager().getFragments().get(i)).reloadAccount();
                    break;
                }
            }
        }
        Log.d(TAG, "onTabMetfoneSelected()");
    }
    // TODO CamID: home_tab_config
    // [start] CamID

    private void onTabContactSelected() {
        Log.d(TAG, "onTabContactSelected()");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onTabSelected(int position) {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        int fragSize = fragments.size();
        if (fragSize > 0) {
            Fragment f = fragments.get(fragSize - 1);
            setSystemBarTheme(Objects.requireNonNull(getActivity()), !MPTabMobileLoginSignUpFragment.class.getSimpleName().equals(f.getClass().getSimpleName()));
        }
        if (listTabHome.get(position).getHomeTab() != HomeTab.tab_metfone) {
            setSystemBarTheme(Objects.requireNonNull(getActivity()), true);
        }

        if (listTabHome.get(position).getHomeTab() == HomeTab.tab_game) {
            isClickTabGame = true;
            if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
                logApp(Constants.LOG_APP.TAB_GAME);
                mCamIdUserBusiness.setProcessingLoginSignUpGame(true);
                mCamIdUserBusiness.setPositionGameTab(position);
                TabHomeHelper.TAB_CURRENT = 0;
            } else {
                if (mCamIdUserBusiness.getPositionGameTab() != -1) {
                    position = mCamIdUserBusiness.getPositionGameTab();
                    mCamIdUserBusiness.setPositionGameTab(-1);
                }
                TabHomeHelper.TAB_CURRENT = position;
            }
        } else {
            isClickTabGame = false;
        }

        if (listTabHome.get(position).getHomeTab() == HomeTab.tab_game) {
            if (mCamIdUserBusiness.isProcessingLoginSignUpGame()) {
                if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
//                    mParentActivity.setCurrentTabPosition(TabHomeHelper.TAB_CURRENT);
                    mViewPager.setCurrentItem(TabHomeHelper.TAB_CURRENT);
                    reInitialBottomNavigationBarWithBackground(R.color.m_home_tab_background);
                    mParentActivity.displayRegisterScreenActivity(true);
                    bottomNavigationBar.selectTab(TabHomeHelper.TAB_CURRENT, false);
                    return;
                }
                mCamIdUserBusiness.setProcessingLoginSignUpGame(false);
            }
//            bottomNavigationBar.selectTab(TabHomeHelper.TAB_CURRENT, false);
        }

        TabHomeHelper.TAB_CURRENT = position;
        mParentActivity.setCurrentTabPosition(position);

        int color = -1;
        if (listTabHome.get(position).getHomeTab() == HomeTab.tab_metfone) {
            color = R.color.m_home_tab_background_2;
            if (getChildFragmentManager().getFragments().size() > 0) {
                List<Fragment> fragmentss = getChildFragmentManager().getFragments();
                for (Fragment fragment : fragmentss) {
                    if (MPTabEnterAddPhoneNumberFragment.class.getSimpleName().equals(fragment.getClass().getSimpleName())) {
                        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );
                    }
                }
                for (Fragment fragment : fragmentss) {
                    if (fragment instanceof BuyPhoneNumberFragment || fragment instanceof IShareFragment) {
                        color = R.color.m_home_tab_background;
                        break;
                    }
                }
            }
        } else if (listTabHome.get(position).getHomeTab() == HomeTab.tab_chat) {
            color = R.color.m_home_tab_chat_background;
            if (getChildFragmentManager().getFragments().size() > 0) {
                Fragment fragment = getChildFragmentManager().getFragments().get(position);
                if (fragment != null && fragment instanceof TabMobileFragment) {
                    ((TabMobileFragment) fragment).checkReloadData();
                }
            }
        } else {
            color = R.color.m_home_tab_background;
        }

        String logCode = "";
        switch (position) {
            case 0:
                checkForceUpdate();
                logCode = Constants.LOG_APP.TAB_HOME;
                break;
            case 1:
                logCode = Constants.LOG_APP.TAB_GAME;
                break;
            case 5:
                logCode = Constants.LOG_APP.TAB_ESPORT;
                break;
            case 2:
                logCode = Constants.LOG_APP.TAB_CENIMA;
                break;
            case 4:
                logCode = Constants.LOG_APP.TAB_CHAT;
                break;
            case 3:
                logCode = Constants.LOG_APP.TAB_METFONEP;
                break;
        }
        logApp(logCode);

        reInitialBottomNavigationBarWithBackground(color);
        mViewPager.setCurrentItem(TabHomeHelper.TAB_CURRENT);
    }

    private void logApp(final String wsCode) {
        getHomeKhClient().wsAppLog(new MPApiCallback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    logT(String.format("wsLogApp => succeed [%s]", wsCode));
                }
            }

            @Override
            public void onError(Throwable error) {
                logT("wsLogApp => failed");
            }
        }, wsCode);

    }

    @Override
    public void onTabUnselected(int position) {
        Log.d(TAG, "onTabUnselected: " + position);
    }

    @Override
    public void onTabReselected(int position) {
        Log.d(TAG, "onTabReselected: " + position);
        // [Start] CamID
//        mParentActivity.onTabReselectedListener(position);
        // [End] CamID
    }

    private void onTabHotSelected() {
        if (mBadgeOnmedia != null && !mBadgeOnmedia.isHidden()) {
            Log.i(TAG, "save time: " + System.currentTimeMillis());
            mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_HOT, System
                    .currentTimeMillis()).apply();
            /*mPref.edit().putBoolean(Constants.PREFERENCE.PREF_HAD_SHOW_ALERT_TAB_HOT_TODAY, true).apply();
            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_HAD_NOTIFY_TAB_HOT, false).apply();*/
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBadgeOnmedia.hide(false);
                }
            });
        }
    }

    private void changeUnreadMessageBadge() {
        mParentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBadgeChat != null) {
                    if (unreadMsg > 0) {
                        Log.i(TAG, "showBadgeChat");
                        String textBadgeChat = unreadMsg > 5 ? "+5" : String.valueOf(unreadMsg);
                        mBadgeChat.setText(textBadgeChat);
                        mBadgeChat.show(false);
                    } else {
                        Log.i(TAG, "hideBadgeChat");
                        mBadgeChat.setText("0");
                        mBadgeChat.hide(true);
                    }
                }
            }
        });
    }

    private void checkAndShowAlertTabHot() {
        if (mBadgeOnmedia != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_HOT,
                            0L);
                    if (TimeHelper.checkTimeInDay(lastShow)) {
                        mBadgeOnmedia.hide(false);
                    } else {
                        mBadgeOnmedia.show(false);
                    }
                }
            });
        }
    }

    private void checkAndShowAlertTabVideo() {
        if (mBadgeVideo != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_VIDEO,
                            0L);
                    if (TimeHelper.checkTimeInDay(lastShow)) {
                        mBadgeVideo.hide(false);
                    } else {
                        mBadgeVideo.show(false);
                    }
                }
            });
        }
    }

    private void checkAndShowAlertTabMusic() {
        if (mBadgeMusic != null) {
            mParentActivity.runOnUiThread(() -> {
                long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_MUSIC,
                        0L);
                if (TimeHelper.checkTimeInDay(lastShow)) {
                    mBadgeMusic.hide(false);
                } else {
                    mBadgeMusic.show(false);
                }
            });
        }
    }

    private void checkAndShowAlertTabMovie() {
        if (mBadgeMovie != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_MOVIE,
                            0L);
                    if (TimeHelper.checkTimeInDay(lastShow)) {
                        mBadgeMovie.hide(false);
                    } else {
                        mBadgeMovie.show(false);
                    }
                }
            });
        }
    }

    private void checkAndShowAlertTabNews() {
        if (mBadgeNews != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_NEWS,
                            0L);
                    if (TimeHelper.checkTimeInDay(lastShow)) {
                        mBadgeNews.hide(false);
                    } else {
                        mBadgeNews.show(false);
                    }
                }
            });
        }
    }

    private void checkAndShowAlertTabSecurity() {
        if (mBadgeSecurity != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    long lastShow = mApplication.getPref().getLong(Constants.PREFERENCE.PREF_LAST_SHOW_ALERT_TAB_SECURITY,
                            0L);
                    if (TimeHelper.checkTimeInDay(lastShow)) {
                        mBadgeSecurity.hide(false);
                    } else {
                        mBadgeSecurity.show(false);
                    }
                }
            });
        }
    }

    private void initItemHomeAdapter() {
        bottomNavigationBar.setMode(BottomNavigationBar.MODE_FIXED);
        bottomNavigationBar.setBackgroundStyle(BottomNavigationBar.BACKGROUND_STYLE_STATIC);
        bottomNavigationBar.clearAll();
        mBadgeHome = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("0")
                .setHideOnSelect(false).hide(false);
        mBadgeChat = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setHideOnSelect(false).hide(true);
        mBadgeOnmedia = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);

        mBadgeVideo = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);

        mBadgeMovie = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);

        mBadgeMusic = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);

        mBadgeNews = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);
        mBadgeTiins = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);

        mBadgeSecurity = new TextBadgeItem()
                .setBorderWidth(1)
                .setBackgroundColorResource(R.color.red)
                .setText("N")
                .setHideOnSelect(false).hide(false);

        if (listTabHome == null) listTabHome = new ArrayList<>();
        else listTabHome.clear();
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(mParentActivity);

//        CopyOnWriteArrayList<ConfigTabHomeItem> listItem = TabHomeHelper.getInstant(mApplication).getListActiveItem();
        // TODO: [START] Cambodia version
        CopyOnWriteArrayList<ConfigTabHomeItem> listItem = TabHomeHelper.getInstant(mApplication).getKhListActiveItem();
        // TODO: [END] Cambodia version

        // khong co case tab_contact, vi hien tai chua co icon cho tab nay va cung khong co design cho man hinh nay
        // [end] CamID

        mTabGamePosition = -1;
        HashMap<Integer, ConfigTabHomeItem> listWapActive = new HashMap<>();
        for (int i = 0; i < listItem.size(); i++) {
            ConfigTabHomeItem item = listItem.get(i);
            HomeTab homeTab = item.getHomeTab();
            switch (homeTab) {
                // TODO: [START] Cambodia version
                case tab_home:
                    creator.add(FragmentPagerItem.of(mRes.getString(R.string.tab_m_home), FragmentTabHomeKh.class));
                    listTabHome.add(new ConfigTabHomeItem(HomeTab.tab_home, ConfigTabHomeItem.STATE_ACTIVE));
                    bottomNavigationBar.addItem(new BottomNavigationItem(R.drawable.ic_m_tab_home_active, R.string.tab_m_home)
                            .setInactiveIconResource(R.drawable.ic_m_tab_home_inactive)
                            .setActiveColorResource(R.color.m_home_tab_item)
                            .setLottieFile("lottie/tab_home.json")
                            .setColorDot(R.color.m_home_tab_dot));
                    break;
                case tab_chat:
                    creator.add(FragmentPagerItem.of(mRes.getString(R.string.tab_m_chat), TabMobileFragment.class));
                    listTabHome.add(new ConfigTabHomeItem(HomeTab.tab_chat, ConfigTabHomeItem.STATE_ACTIVE));
                    bottomNavigationBar.addItem(new BottomNavigationItem(R.drawable.ic_m_tab_chat_active, R.string.tab_m_chat)
                            .setInactiveIconResource(R.drawable.ic_m_tab_chat_inactive)
                            .setBadgeItem(mBadgeChat)
                            .setActiveColorResource(R.color.m_home_tab_item).setLottieFile("lottie/tab_chat.json")
                            .setColorDot(R.color.m_home_tab_chat_dot));

                    break;
                case tab_esport:
                    creator.add(FragmentPagerItem.of(mRes.getString(R.string.tab_m_esport), HomeFragment.class));
                    listTabHome.add(new ConfigTabHomeItem(HomeTab.tab_esport, ConfigTabHomeItem.STATE_ACTIVE));
                    bottomNavigationBar.addItem(new BottomNavigationItem(R.drawable.ic_m_tab_esport_active, R.string.tab_m_esport)
                            .setInactiveIconResource(R.drawable.ic_m_tab_esport_inactive)
                            .setActiveColorResource(R.color.m_home_tab_item).setLottieFile("lottie/tab_esport.json")
                            .setColorDot(R.color.m_home_tab_dot));
                    break;

                case tab_cinema:
                    // Cinema Tab :Dunglv
                    creator.add(FragmentPagerItem.of(mRes.getString(R.string.tab_m_cinema), MoviePagerFragment.class));
                    listTabHome.add(new ConfigTabHomeItem(HomeTab.tab_cinema, ConfigTabHomeItem.STATE_ACTIVE));
                    bottomNavigationBar.addItem(new BottomNavigationItem(R.drawable.ic_m_tab_cinema_active, R.string.tab_m_cinema)
                            .setInactiveIconResource(R.drawable.ic_m_tab_cinema_inactive)
                            .setActiveColorResource(R.color.m_home_tab_item).setLottieFile("lottie/tab_cinema.json")
                            .setColorDot(R.color.m_home_tab_dot));
                    break;

                case tab_metfone:
                    mTabMetfonePosition = i;
                    creator.add(FragmentPagerItem.of(mRes.getString(R.string.tab_m_metfone), MPRootFragment.class));
                    listTabHome.add(new ConfigTabHomeItem(HomeTab.tab_metfone, ConfigTabHomeItem.STATE_ACTIVE));
                    bottomNavigationBar.addItem(new BottomNavigationItem(R.drawable.ic_m_tab_metfone_active, R.string.tab_m_metfone)
                            .setInactiveIconResource(R.drawable.ic_m_tab_metfone_inactive)
                            .setActiveColorResource(R.color.m_home_tab_item).setLottieFile("lottie/tab_metfone.json")
                            .setColorDot(R.color.m_home_tab_dot));
                    break;

                case tab_game:
                    mTabGamePosition = i;
                    creator.add(FragmentPagerItem.of(mRes.getString(R.string.tab_m_game), GameHomePageFragment.class));
                    listTabHome.add(new ConfigTabHomeItem(HomeTab.tab_game, ConfigTabHomeItem.STATE_ACTIVE));
                    bottomNavigationBar.addItem(new BottomNavigationItem(R.drawable.ic_m_tab_games_active, R.string.tab_m_game)
                            .setInactiveIconResource(R.drawable.ic_m_tab_games_inactive)
                            .setActiveColorResource(R.color.m_home_tab_item).setLottieFile("lottie/tab_games.json")
                            .setColorDot(R.color.m_home_tab_dot));
                    break;
                // TODO: [END] Cambodia version
                // khong co case tab_contact, vi hien tai chua co icon cho tab nay va cung khong co design cho man hinh nay
            }
        }

        mHomePagerAdapter = new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());

        if (mApplication.isDataReady()) {
            unreadMsg = mApplication.getMessageBusiness().getNumberOfUnreadMessage();
            Log.i(TAG, "initItemHomeAdapter unreadMsg: " + unreadMsg);
            changeUnreadMessageBadge();
            checkAndShowAlertTabHot();
            checkAndShowAlertTabVideo();
            checkAndShowAlertTabMovie();
            checkAndShowAlertTabMusic();
            checkAndShowAlertTabNews();
            checkAndShowAlertTabSecurity();
        }
        bottomNavigationBar.initialise();
    }

    @Override
    public void onConfigStickyBannerChanged() {

    }

    @Override
    public void onConfigTabChange() {
        Log.i(TAG, "onConfigTabChange");
    }

    @Override
    public void onConfigBackgroundHeaderHomeChange() {
    }

    public BottomNavigationBar getBottomNavigationBar() {
        return bottomNavigationBar;
    }


    public boolean isSelectMode() {
        return isSelectMode;
    }

    public void setSelectMode(boolean selectMode) {
        isSelectMode = selectMode;
    }

    @Override
    public void onConfigDataReady() {
        if (mParentActivity != null) {
            mParentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mParentActivity.hideLoadingDialog();
                    setAdapter();
                }
            });
        }
    }

    private void onTabHomeSelected() {
        Log.d(TAG, "onTabHomeSelected");
    }

    public void selectTabOfStranger(int tabPosition) {
        if (tabStrangerIndex > 0) {
            Fragment fragment = getChildFragmentManager().getFragments().get(tabStrangerIndex);
            if (fragment != null && fragment instanceof TabStrangerFragment) {
                ((TabStrangerFragment) fragment).setCurrentTab(tabPosition);
            }
        }

    }

    public void selectMetfoneTab() {
        for (int i = 0; i < listTabHome.size(); i++) {
            if (listTabHome.get(i).getHomeTab() == HomeTab.tab_metfone) {
                mCamIdUserBusiness.setProcessingLoginSignUpMetfone(false);
                mCamIdUserBusiness.setAddOrSelectPhoneCalled(false);
                mCamIdUserBusiness.setPositionMetfoneTab(-1);
                TabHomeHelper.TAB_CURRENT = i;
                reInitialBottomNavigationBarWithBackground(R.color.m_home_tab_background_2);
                mViewPager.setCurrentItem(i);
            }
        }
    }

    public void reInitialBottomNavigationBarWithBackground(int colorIdRes) {
        android.util.Log.e(TAG, "reInitialBottomNavigationBarWithBackground: ");
        if (mBottomNavigationBarBackgroundCurrent != colorIdRes) {
            ArrayList<BottomNavigationItem> items = new ArrayList<>(bottomNavigationBar.getBottomNavigationItems());
            bottomNavigationBar.clearAll();
            for (BottomNavigationItem item : items) {
                bottomNavigationBar.addItem(item);
            }
            bottomNavigationBar.setBarBackgroundColor(colorIdRes);
            bottomNavigationBar.initialise();
            bottomNavigationBar.selectTab(TabHomeHelper.TAB_CURRENT, false);
            mBottomNavigationBarBackgroundCurrent = colorIdRes;
        }
    }

    public void setBottomNavigationBarVisibility(int visibility) {
        bottomNavigationBar.setVisibility(visibility);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void popBackStackFragment() {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        int fragSize = fragments.size();
        Fragment f = fragments.get(fragSize - 1);
        android.util.Log.e(TAG, "popBackStackFragment: " + f.getClass().getSimpleName());

        if (MPTabMobileLoginSignUpFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            setSystemBarTheme(Objects.requireNonNull(getActivity()), true);
        }

        if (MPBuyServiceDetailFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((MPBuyServiceDetailFragment) f).onBack();
        } else if (MPServiceFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((MPServiceFragment) f).onBack();
        } else if (MPMyServiceFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            Fragment fragment = fragments.get(fragSize - 3);
            ((MPServiceFragment) fragment).onBack();
        } else if (MPMapsFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            Fragment fragmentMain = fragments.get(fragSize - 2);
            if (MPStoreFragment.class.getSimpleName().equals(fragmentMain.getClass().getSimpleName())) {
                ((MPStoreFragment) fragmentMain).onBack();
            } else if (MPAddFeedbackFragment.class.getSimpleName().equals(fragmentMain.getClass().getSimpleName())) {
                ((MPAddFeedbackFragment) fragmentMain).onBack();
            }
        } else if (MPDetailSmsCallDataFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((MPDetailSmsCallDataFragment) f).popBackStackFragment();
        } else if (MPDetailAccountFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((MPDetailAccountFragment) f).popBackStackFragment();
        } else if (MPSupportFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((MPSupportFragment) f).onBack();
        } else if (MPExchangeFragment.class.getSimpleName().equals(f.getClass().getSimpleName())
            || MPTabInternetFragment.class.getSimpleName().equals(f.getClass().getSimpleName())
            || MPTabMobileLoginSignUpFragment.class.getSimpleName().equals(f.getClass().getSimpleName())
            || MPTabMobileFragment.class.getSimpleName().equals(f.getClass().getSimpleName())
            || MPAddPhoneMetfoneFragment.class.getSimpleName().equals(f.getClass().getSimpleName())
            || MPTabEnterAddPhoneNumberFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            mParentActivity.setCurrentTab(TabHomeHelper.HomeTab.tab_home, null, -1);
            reInitialBottomNavigationBarWithBackground(R.color.m_home_tab_background);
            checkForceUpdate();
        } else if (TopupMetfoneFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((TopupMetfoneFragment) f).onBack();
        } else if (QrCodeTopUpFragment.class.getSimpleName().equals(f.getClass().getSimpleName())) {
            ((QrCodeTopUpFragment) f).onBack();
        } else if (f instanceof BuyPhoneNumberFragment) {
            ((BuyPhoneNumberFragment) f).onBack();
        } else if (f instanceof IShareFragment) {
            ((IShareFragment) f).onBack();
        } else if (f instanceof MPBillPaymentFragment) {
            ((MPBillPaymentFragment) f).onBack();
        } else if (f instanceof MPBillPaymentDetailFragment) {
            ((MPBillPaymentDetailFragment) f).onBack();
        } else if (f instanceof HomeFragment) {
            mParentActivity.setCurrentTab(TabHomeHelper.HomeTab.tab_home, null, -1);
            reInitialBottomNavigationBarWithBackground(R.color.m_home_tab_background);
            checkForceUpdate();
        } else {
            getChildFragmentManager().popBackStack();
        }
    }

    public boolean popBackStackMoviePagerFragment() {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        int fragSize = fragments.size();
        Fragment f = fragments.get(fragSize - 1);
        android.util.Log.e(TAG, "popBackStackFragment: " + fragments.size());
        android.util.Log.e(TAG, "popBackStackFragment: " + f.getClass().getSimpleName());
        if (f instanceof BaseFragment) {
            ((BaseFragment) f).popBackStackFragment();
            return true;
        }
        return false;
    }

    public boolean popBackStackTabHome() {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        int fragSize = fragments.size();
        android.util.Log.e(TAG, "popBackStackFragment: " + fragments.size());
        if (fragSize == 0) {
            return false;
        }
//        Fragment f = fragments.get(fragSize - 1);
        Fragment f = getChildFragmentManager().findFragmentById(R.id.tab_home_parent);
        if (f == null) {
            return false;
        }
        android.util.Log.e(TAG, "popBackStackFragment: " + f.getClass().getSimpleName());
        if (f instanceof RewardBaseFragment) {
            ((RewardBaseFragment) f).popBackStackFragment();
            return true;
        } else if (f instanceof HistoryPointContainerFragment) {
            getChildFragmentManager().popBackStack();
        } else if (f instanceof FragmentNotification) {
            getChildFragmentManager().popBackStack();
            return true;
        } else if (f instanceof GameFragment) {
            if (((GameFragment) f).forceToClose) {
                getChildFragmentManager().popBackStack();
            } else {
                ((GameFragment) f).onBackPressed();
            }
            return true;
        } else if (f instanceof LiveVideoFragment) {
            getChildFragmentManager().popBackStack();
        } else if (f instanceof HomeFragment) {
            getChildFragmentManager().popBackStack();
        }
        return false;
    }

    @Override
    public boolean onBackPressed() {
        System.out.println("TuanHM getCurrentPos " + currentPos);
        if (currentPos == 0) {
            return popBackStackTabHome();
        } else if (currentPos == 2) {
            boolean back = popBackStackMoviePagerFragment();
            if (!back) {
                bottomNavigationBar.selectTab(TabHomeHelper.TAB_DEFAULT);
            }
        } else {
            bottomNavigationBar.selectTab(TabHomeHelper.TAB_DEFAULT);
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshUserInfo(final RequestRefreshInfo event) {
        if (!isLoadingAvatar && isLogin() != NOT) {
            getWSGetAccountRank();
        }
    }

    private void getWSGetAccountRank() {
        Log.d(TAG, "Request info user");
        isLoadingAvatar = true;
        EventBus.getDefault().postSticky(new LoadInfoUser(LoadInfoUser.LoadType.LOADING));
        KhHomeClient.getInstance().wsGetAccountRankInfo(new MPApiCallback<WsGetAccountRankInfoResponse>() {
            @Override
            public void onResponse(retrofit2.Response<WsGetAccountRankInfoResponse> response) {
                boolean isSetAvatar = false;
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    Log.d(TAG, "getWSGetAccountRank - onResponse: OK ");
                    if (response.body().getResult().accountRankDTO != null) {
                        isSetAvatar = true;
                        accountRankDTO = response.body().getResult().accountRankDTO;
                        EventBus.getDefault().postSticky(new LoadInfoUser(response.body().getResult().accountRankDTO, userInfoBusiness.getUser()));
                        EventBus.getDefault().post(new ChangeAccountEvent());
                    }
                } else {
                    Log.d(TAG, "getWSGetAccountRank - onResponse: error");
                }
                if (!isSetAvatar) {
                    if (ApplicationController.self().getAccountRankDTO() != null) {
                        accountRankDTO = ApplicationController.self().getAccountRankDTO();
                        EventBus.getDefault().postSticky(new LoadInfoUser(accountRankDTO, userInfoBusiness.getUser()));
                    } else {
                        AccountRankDTO rankDTO = new AccountRankDTO();
                        rankDTO.rankId = KhUserRank.RewardRank.MEMBER.id;
                        rankDTO.rankName = KhUserRank.RewardRank.MEMBER.name();
                        EventBus.getDefault().postSticky(new LoadInfoUser(rankDTO, userInfoBusiness.getUser()));
                    }
                }
                isLoadingAvatar = false;
            }

            @Override
            public void onError(Throwable error) {
                if (ApplicationController.self().getAccountRankDTO() != null) {
                    accountRankDTO = ApplicationController.self().getAccountRankDTO();
                    EventBus.getDefault().postSticky(new LoadInfoUser(accountRankDTO, userInfoBusiness.getUser()));
                } else {
                    AccountRankDTO rankDTO = new AccountRankDTO();
                    rankDTO.rankId = KhUserRank.RewardRank.MEMBER.id;
                    rankDTO.rankName = KhUserRank.RewardRank.MEMBER.name();
                    EventBus.getDefault().postSticky(new LoadInfoUser(rankDTO, userInfoBusiness.getUser()));
                }
                isLoadingAvatar = false;
            }
        });
    }

    @Override
    public void onInternetChanged() {
        if (!NetworkHelper.isConnectInternet(getContext())) {
            EventBus.getDefault().postSticky(new LoadInfoUser(LoadInfoUser.LoadType.LOADING));
            Log.d(TAG, "Lost internet");
        } else {
            EventBus.getDefault().post(new RequestRefreshInfo());
            Log.d(TAG, "Connecting internet");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    private void checkForceUpdate() {
        getHomeKhClient().wsCheckForceUpdateApp(new KhApiCallback<KHBaseResponse<WsCheckForceUpdateAppResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsCheckForceUpdateAppResponse> body) {
                if (body.getData() != null && body.getData().getWsResponse() != null) {
                    WsCheckForceUpdateAppResponse.WsResponse response = body.getData().getWsResponse();
                    EventBus.getDefault().post(response);
                }
            }

            @Override
            public void onFailed(String status, String message) {
                logT("wsCheckForceUpdateApp > onFailed");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logT("wsCheckForceUpdateApp > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsCheckForceUpdateAppResponse>>> call, Throwable t) {
                t.printStackTrace();
                logT("wsCheckForceUpdateApp > onFailure");
            }
        }, BuildConfig.VERSION_NAME);
    }

    private void checkShowPopup(String isdn) {
        getHomeKhClient().checkShowPopup(isdn,
                DeviceUtils.getAndroidID(requireContext()),
                new MPApiCallback<WsPopupTetResponse>() {
                    @Override
                    public void onResponse(Response<WsPopupTetResponse> response) {
                        if (response != null && response.body() != null
                                && response.body().getResult() != null
                                && response.body().getResult().getErrorCode().equals("0")
                                && response.body().getResult().getWsResponse() != null
                        ) {
                            if (response.body().getResult().getWsResponse().isShowPopupTet()) {
                                android.util.Log.e("ttt", "checkShowPopup: show poup from api");
                                showLunarGift(isdn);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        //no-op
                    }
                });
    }

    private void showLunarGift(String isdn) {
        if (dialogNewYear != null && dialogNewYear.getDialog() != null && dialogNewYear.getDialog().isShowing()) {
            dialogNewYear.dismiss();
        }
//        String value = isdn;
//        if (isdn == null || isdn.isEmpty()) {
//            value = mCamIdUserBusiness.getMetfoneUsernameIsdn();
//        }
        dialogNewYear = NewYearDialog.newInstance(
                isdn
        );
        dialogNewYear.show(getParentFragmentManager());
    }

    private KhHomeClient getHomeKhClient() {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        return homeKhClient;
    }

    private void logT(String message) {
        android.util.Log.e(TAG, message);
    }

    public NonSwipeableViewPager getViewPager() {
        return mViewPager;
    }

    public void selectTabDefault() {
        bottomNavigationBar.selectTab(TabHomeHelper.TAB_DEFAULT);
    }
}

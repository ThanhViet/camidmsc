package com.metfone.selfcare.fragment.contact;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.AlbumViewActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.adapter.onmedia.OMFeedAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.listener.Listener;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha.OnClickTag;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.ProfileHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.PopupZodiac;
import com.metfone.selfcare.ui.autoplay.AspectRelativeLayout;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/11/2015.
 */
@SuppressWarnings({"StatementWithEmptyBody", "unused"})
public class StrangerDetailFragmentNew extends Fragment implements ClickListener.IconListener
        , XMPPConnectivityChangeListener, ContactChangeListener, OnMediaHolderListener, OnClickTag
        , Listener, OnChannelChangedDataListener {
    private static final String TAG = StrangerDetailFragmentNew.class.getSimpleName();
    private OnStrangerFragmentInteractionListener mListener;
    private ContactDetailActivity mParentActivity;
    private Resources mRes;
    private ApplicationController mApplication;
    private StrangerBusiness mStrangerBusiness;
    private ContactBusiness mContactBusiness;
    private Handler mHandler;
    private ImageProfileBusiness mImageProfileBusiness;

    //variable

    private StrangerPhoneNumber mStrangerPhoneNumber;
    private NonContact mNonContact;
    private String mFriendJidNumber;
    private String mFriendName;// dung cho chat nguoi la tu mocha
    private String mFriendChangeAvatar;
    private String mFriendStatus;
    private String mFriendBirthDay;
    private int mFriendGender;
    private boolean isStrangerOtherApp;

    private OMFeedAdapter mFeedAdapter;
    private ArrayList<FeedModelOnMedia> listFeed = new ArrayList<>();
    protected int type;
    private ClickListener.IconListener iconListener;

    private boolean isLoading = false;
    private String urlAvatarTmp = "";

    private ArrayList<ImageProfile> mImageProfiles;
    private ImageProfile mImageCover;

    private WSOnMedia rest;
    private boolean noMoreFeed = false;
    private FeedBusiness mFeedBusiness;

    private int gaCategoryId, gaActionId;
    private OnMediaInterfaceListener feedInterface;
    private boolean getContact = false, isRequesting = false;

    private boolean enableClickTag = false;
    private int statusFollow = Constants.CONTACT.FOLLOW_STATE_UNKNOW;
    private String rowIdRequestSocial = null;

    private EventOnMediaHelper eventOnMediaHelper;

    private PhoneNumber responseNumber = null;

    private RecyclerView mRecyclerViewFeed;
    private View mFooterView, mHeaderView;
    private LinearLayout mLoadmoreFooterView;

    //    private View mViewFakeToolbar, mViewFakeStatusBar, mViewBottomToolbar;
    private ImageView mImgBack;
    private ImageView mImgHeaderBack;
    private ImageView icOption;
    private ImageView icHeaderOption;
//    private TextView mTvwProfileNameToolbar;
//    private Drawable mDrawableAb, mDrawableStatus;

    private ImageView mImgAvatar;
    private TextView mTvwAvatar;
    private ImageView mImgToolBarAvatar;
    private TextView mTvwToolBarAvatar;
    //    private AspectImageView mImgProfileAvatar;
    private EllipsisTextView mTvwProfileStatus;
    private TextView mTvwProfileName;
    private ImageView mImgSex;
    private TextView mTvwAge,tvPhone;
//    private int widthBigAvatar, heightBigAvatar;

    //    private View mLayoutInfo;
    private RoundTextView mTvwButtonProfile;
//    private View mLayoutInfoAgePoint;
//    private TextView mTvwPoint;
//    private View mLayoutAction1, mLayoutAction2, mLayoutAction3, mLayoutAction4, mLayoutAction5;
//    private View mViewPoint;

    //    private View mView3Image, mView2Image, mBtnShowAll;
//    private SquareImageView[] mImageViewList = new SquareImageView[5];
    private int maxHeight = 500;
    private int maxHeightDefaut = 500;

//    private TextView mTvwAction;
//    private View mLayoutMyAlbum;

    private int old;
    private String birthdayStr;
    private boolean isShowBirthday;
    private ListenerUtils listenerUtils;

    private View vMessage, vListen, vPicProfile;
    private AspectRelativeLayout vPic1, vPic2, vPic3;
    private NestedScrollView nScrollView;
    private RelativeLayout rlToolbar;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactListFragment.
     */
    public static StrangerDetailFragmentNew newInstance(String strangerJid, String friendName) {
        StrangerDetailFragmentNew fragment = new StrangerDetailFragmentNew();
        Bundle args = new Bundle();
        args.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerJid);
        args.putString(NumberConstant.NAME, friendName);
        fragment.setArguments(args);
        fragment.type = NumberConstant.TYPE_STRANGER_EXIST;
        return fragment;
    }

    public static StrangerDetailFragmentNew newInstance(String friendJid,
                                                        String friendName, String friendLAvatar,
                                                        String status, String birthdayStr, int gender) {
        StrangerDetailFragmentNew fragment = new StrangerDetailFragmentNew();
        Bundle args = new Bundle();
        args.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
        args.putString(NumberConstant.NAME, friendName);
        args.putString(NumberConstant.LAST_CHANGE_AVATAR, friendLAvatar);
        args.putString(NumberConstant.STATUS, status);
        args.putString(NumberConstant.BIRTHDAY_STRING, birthdayStr);
        args.putInt(NumberConstant.GENDER, gender);
        fragment.setArguments(args);
        fragment.type = NumberConstant.TYPE_STRANGER_MOCHA;
        return fragment;
    }

    public StrangerDetailFragmentNew() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_stranger_profile;
        iconListener = this;
        eventOnMediaHelper = new EventOnMediaHelper(mParentActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        findComponentViews(rootView, inflater, container);
        setViewListeners();
        mParentActivity.setColorIconStatusBar();
        ListenerHelper.getInstance().addContactChangeListener(this);
        getData();
        if (isStrangerOtherApp) {
            drawStrangerKeengDetail();
        } else {
            drawStrangerMochaDetail();
        }
        listenerUtils = mApplication.getApplicationComponent().providerListenerUtils();
        listenerUtils.addListener(this);
        getDetailFollow();
        mFeedAdapter = new OMFeedAdapter(mApplication, listFeed, this, this);
        mFeedAdapter.setActivity(mParentActivity);
        mRecyclerViewFeed.setLayoutManager(new LinearLayoutManager(mRecyclerViewFeed.getContext()));
        HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter = new
                HeaderAndFooterRecyclerViewAdapter(mFeedAdapter);
        mRecyclerViewFeed.setAdapter(mHeaderAndFooterRecyclerViewAdapter);
        mHeaderAndFooterRecyclerViewAdapter.addFooterView(mFooterView);
        mHeaderAndFooterRecyclerViewAdapter.addHeaderView(mHeaderView);
        if (mApplication.getConfigBusiness().isEnableOnMedia()) {
            loadData("");
        }
        /*if (!TextUtils.isEmpty(mFriendJidNumber)) {
            AccumulatePointHelper.getInstance(mApplication).getCurrentTotalPoint(mFriendJidNumber,
                    new AccumulatePointHelper.AccumulateListener() {
                        @Override
                        public void onSuccess(int point) {
                            mViewPoint.setVisibility(View.VISIBLE);
                            String pointShort = Utilities.shortenLongNumber(point);
                            mTvwPoint.setText(String.format(mRes.getString(R.string.profile_friend_point), pointShort));
                        }
                    });
        } else {
            mViewPoint.setVisibility(View.GONE);
        }*/
        return rootView;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        enableClickTag = true;
        XMPPManager.addXMPPConnectivityChangeListener(this);
//        ListenerHelper.getInstance().addNotifyFeedOnMediaListener(this);
        if (mHandler == null) {
            mHandler = new Handler();
        }
        if (mFeedAdapter != null && mFeedAdapter.getItemCount() != 0) {
            mFeedAdapter.notifyDataSetChanged();
            Log.i(TAG, "notify when resume");
        }
        super.onResume();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (ContactDetailActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mContactBusiness = mApplication.getContactBusiness();
        mStrangerBusiness = mApplication.getStrangerBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        mImageProfileBusiness = mApplication.getImageProfileBusiness();
        mHandler = new Handler();
        mRes = mParentActivity.getResources();
        rest = new WSOnMedia(mApplication);
        try {
            mListener = (OnStrangerFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            feedInterface = (OnMediaInterfaceListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectionFeedInterface");
        }
    }

    @Override
    public void onPause() {
        XMPPManager.removeXMPPConnectivityChangeListener(this);
//        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        PopupHelper.getInstance().destroyOverFlowMenu();
        super.onStop();
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        if (mImgAvatar != null) mImgAvatar.setImageBitmap(null);
        if (mImgToolBarAvatar != null) mImgToolBarAvatar.setImageBitmap(null);
        ListenerHelper.getInstance().removeContactChangeListener(this);
        if (listenerUtils != null) listenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mHandler = null;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int arg) {
        if (mListener == null) {
            return;
        }
        try {
            switch (arg) {
                case Constants.ACTION.REENG_CHAT:
                    PhoneNumber phoneNumber = (PhoneNumber) entry;
                    String number = phoneNumber.getJidNumber();
                    mListener.navigateToReengChatActivity(number);
                    break;
                //======onmedia
                case Constants.MENU.MENU_SHARE_LINK:
                    FeedModelOnMedia feed = (FeedModelOnMedia) entry;
                    handleShareLink(feed);
                    mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share_now);
                    break;

                case Constants.MENU.MENU_WRITE_STATUS:
                    FeedModelOnMedia feedWrite = (FeedModelOnMedia) entry;
                    handleWriteStatus(feedWrite);
                    mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string
                            .ga_onmedia_label_click_write_status);
                    break;
                case Constants.MENU.COPY:
                    FeedModelOnMedia feedCopy = (FeedModelOnMedia) entry;
                    TextHelper.copyToClipboard(mParentActivity, feedCopy.getFeedContent().getLink());
                    mParentActivity.showToast(R.string.copy_to_clipboard);
                    mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy);
                    break;
                case Constants.MENU.UNFOLLOW:
                    FeedModelOnMedia feedUnfollow = (FeedModelOnMedia) entry;
                    confirmUnfollow(feedUnfollow);
                    break;

                case Constants.MENU.REPORT:
                    FeedModelOnMedia feedReport = (FeedModelOnMedia) entry;
                    confirmReport(feedReport);
                    mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_report);
                    break;
                case Constants.MENU.POPUP_UNFOLLOW:
                    FeedModelOnMedia feedPopupUnfollow = (FeedModelOnMedia) entry;
                    handleUnfollow(feedPopupUnfollow);
                    break;

                case Constants.MENU.POPUP_REPORT:
                    FeedModelOnMedia feedPopupReport = (FeedModelOnMedia) entry;
                    handleReport(feedPopupReport);
                    break;

                case Constants.MENU.MENU_LIST_SHARE:
                    FeedModelOnMedia feedListShare = (FeedModelOnMedia) entry;
                    if (feedInterface != null) {
                        if (!TextUtils.isEmpty(feedListShare.getFeedContent().getUrl())) {
                            feedInterface.navigateToListShare(feedListShare.getFeedContent().getUrl());
                        }
                    }
                    mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_share);
                    break;
                case Constants.MENU.POPUP_EXIT:
                    FeedModelOnMedia item = (FeedModelOnMedia) entry;
                    eventOnMediaHelper.handleClickPopupExitListenTogether(item, feedInterface);
                    break;
                case Constants.MENU.MENU_COPY_TEXT:
                    FeedModelOnMedia feedCopyText = (FeedModelOnMedia) entry;
                    String textCopy = mFeedBusiness.getTextTagCopy(feedCopyText.getUserStatus(), feedCopyText
                            .getListTag());
                    TextHelper.copyToClipboard(mParentActivity, textCopy);
                    mParentActivity.showToast(R.string.copy_to_clipboard);
                    mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy_text);
                    break;
                case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                    FeedModelOnMedia feedShareFb = (FeedModelOnMedia) entry;
                    OnMediaHelper.shareFacebookOnMedia(feedShareFb, mParentActivity);
                    break;
                case Constants.MENU.SEND_MESSAGE:
                    if (entry instanceof FeedModelOnMedia) {
                        ShareUtils.sendToFriend(mParentActivity, (FeedModelOnMedia) entry);
                    }
                    break;
                case Constants.MENU.MORE:
                    if (entry instanceof FeedModelOnMedia) {
                        ShareUtils.shareWithIntent(mParentActivity, (FeedModelOnMedia) entry);
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void handleWriteStatus(final FeedModelOnMedia feedWrite) {
        eventOnMediaHelper.navigateToPostOnMedia(mParentActivity,
                feedWrite.getFeedContent(), "", feedWrite.getBase64RowId(), false,
                FeedModelOnMedia.ActionFrom.onmedia);
    }

    @Override
    public void onContactChange() {

    }

    @Override
    public void onRosterChange() {

    }

    @Override
    public void initListContactComplete(int sizeSupport) {

    }

    private void onPresenceChange(final PhoneNumber phoneNumber) {
        Log.d(TAG, "onPresenceChange");
        if (mFriendJidNumber != null && mFriendJidNumber.equals(phoneNumber.getJidNumber())) {
            if (!TextUtils.isEmpty(phoneNumber.getNickName())) {
                mFriendName = phoneNumber.getNickName();
            }
            if (isStrangerOtherApp) {
                drawStrangerKeengDetail();
            } else {
                mNonContact = mContactBusiness.getExistNonContact(mFriendJidNumber);
                drawStrangerMochaDetail();
            }
        }
    }

    @Override
    public void onPresenceChange(final ArrayList<PhoneNumber> phoneNumbers) {
        Log.d(TAG, "onPresenceChange");
        if (mHandler != null) {
            mHandler.post(() -> {
                for (PhoneNumber phoneNumber : phoneNumbers) {
                    onPresenceChange(phoneNumber);
                }
            });
        }
    }

    @Override
    public void onXMPPConnected() {
        Log.d(TAG, "onXMPPConnected");
    }

    @Override
    public void onXMPPDisconnected() {
        Log.d(TAG, "onXMPPDisconnected");
    }

    @Override
    public void onXMPPConnecting() {

    }

    private void getData() {
        if (getArguments() != null) {
            //param1 - contactId
            mFriendJidNumber = getArguments().getString(StrangerConstant.STRANGER_JID_NUMBER);
            mFriendName = getArguments().getString(NumberConstant.NAME);
            mFriendChangeAvatar = getArguments().getString(NumberConstant.LAST_CHANGE_AVATAR);
            mFriendStatus = getArguments().getString(NumberConstant.STATUS);
            mFriendBirthDay = getArguments().getString(NumberConstant.BIRTHDAY_STRING);
            //get gender default tra ve -1 de ko tu set gioi tinh nu
            mFriendGender = getArguments().getInt(NumberConstant.GENDER, -1);
            Log.i(TAG, "getData mFriendGender =" + mFriendGender);
            //stranger detail
            mStrangerPhoneNumber = mStrangerBusiness.getExistStrangerPhoneNumberFromNumber(mFriendJidNumber);
            mNonContact = mContactBusiness.getExistNonContact(mFriendJidNumber);
            /*if (type == NumberConstant.TYPE_STRANGER_MOCHA || mNonContact == null) {// chua co noncontact thi goi
            request len sv de lay thong tin
                mContactBusiness.getInfoContactFromNumber(mFriendJidNumber);
            }*/
            /* if (mNonContact == null)
                mContactBusiness.getInfoNumber(mFriendJidNumber);*/
            if (TextUtils.isEmpty(mFriendName)) {
                mFriendName = Utilities.hidenPhoneNumber(mFriendJidNumber);
            }
            isStrangerOtherApp = mStrangerPhoneNumber != null && type == NumberConstant.TYPE_STRANGER_EXIST &&
                    mStrangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger;
        }
    }

    private void findComponentViews(View rootView, LayoutInflater inflater, ViewGroup container) {
        nScrollView = rootView.findViewById(R.id.nScrollView);
        rlToolbar = rootView.findViewById(R.id.rlToolbar);
        mRecyclerViewFeed = rootView.findViewById(R.id.recycler_view);
        mRecyclerViewFeed.setHasFixedSize(true);
        mRecyclerViewFeed.setNestedScrollingEnabled(false);
        mImgToolBarAvatar = rootView.findViewById(R.id.ivToolbarAvatar);
        mTvwToolBarAvatar = rootView.findViewById(R.id.tvToolbarAvatar);
        mFooterView = inflater.inflate(R.layout.item_onmedia_loading_footer, container, false);
        mHeaderView = inflater.inflate(R.layout.header_friend_profile_v2, container, false);
        mLoadmoreFooterView = mFooterView.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);

//        mViewFakeToolbar = rootView.findViewById(R.id.layout_ab_profile);
//        mViewFakeStatusBar = rootView.findViewById(R.id.view_fake_status_bar);
//        mViewBottomToolbar = rootView.findViewById(R.id.view_bottom_toolbar);
//        mViewBottomToolbar.setVisibility(View.GONE);
//        mDrawableAb = new ColorDrawable(ContextCompat.getColor(mApplication, R.color.white));
//        mDrawableStatus = new ColorDrawable(ContextCompat.getColor(mApplication, R.color.status_bar));
//        mDrawableAb.setAlpha(0);
//        mDrawableStatus.setAlpha(0);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            mViewFakeToolbar.setBackground(mDrawableAb);
//            mViewFakeStatusBar.setBackground(mDrawableStatus);
//        } else {
//            mViewFakeToolbar.setBackgroundDrawable(mDrawableAb);
//            mViewFakeStatusBar.setBackground(mDrawableStatus);
//        }

        mImgBack = rootView.findViewById(R.id.ivBack);
//        mTvwProfileNameToolbar = rootView.findViewById(R.id.ab_title);
        icOption = rootView.findViewById(R.id.icOption);

//        mImgProfileAvatar = mHeaderView.findViewById(R.id.img_profile_avatar);
//        mLayoutInfo = mHeaderView.findViewById(R.id.layout_info);
        mImgAvatar = mHeaderView.findViewById(R.id.ivAvatar);
        mTvwAvatar = mHeaderView.findViewById(R.id.tvAvatar);
        mImgHeaderBack = mHeaderView.findViewById(R.id.ivHeaderBack);
        icHeaderOption = mHeaderView.findViewById(R.id.icHeaderOption);
        mTvwButtonProfile = mHeaderView.findViewById(R.id.btnFriend);
        mTvwProfileName = mHeaderView.findViewById(R.id.tvw_profile_name);
//        mLayoutInfoAgePoint = mHeaderView.findViewById(R.id.layout_info_age_point);
        mImgSex = mHeaderView.findViewById(R.id.img_profile_sex);
//        mViewPoint = mHeaderView.findViewById(R.id.layout_info_point);
//        mViewPoint.setVisibility(View.GONE);
        mTvwAge = mHeaderView.findViewById(R.id.tvw_profile_age);
//        mTvwPoint = mHeaderView.findViewById(R.id.tvw_point_friend);
        mTvwProfileStatus = mHeaderView.findViewById(R.id.tvw_profile_status);
        View vCall = mHeaderView.findViewById(R.id.rlCall);
        vCall.setVisibility(View.GONE);
        vMessage = mHeaderView.findViewById(R.id.rlMsg);
        vListen = mHeaderView.findViewById(R.id.rlListen);

        vPicProfile = mHeaderView.findViewById(R.id.vPicProfile);
        vPic1 = mHeaderView.findViewById(R.id.rlPic1);
        vPic2 = mHeaderView.findViewById(R.id.rlPic2);
        vPic3 = mHeaderView.findViewById(R.id.rlPic3);

        int screenWidth = ScreenManager.getWidth(mParentActivity);
        int widthParent = screenWidth - com.metfone.selfcare.module.keeng.utils.Utilities.dpToPx(mParentActivity, 28);
        int widthChild = widthParent / 3;
        ViewGroup.LayoutParams lpPic1 = vPic1.getLayoutParams();
        lpPic1.width = widthChild;
        lpPic1.height = widthChild * 100 / 114;
        vPic1.setLayoutParams(lpPic1);

        ViewGroup.LayoutParams lpPic2 = vPic2.getLayoutParams();
        lpPic2.width = widthChild;
        lpPic2.height = widthChild * 100 / 114;
        vPic2.setLayoutParams(lpPic2);

        ViewGroup.LayoutParams lpPic3 = vPic1.getLayoutParams();
        lpPic3.width = widthChild;
        lpPic3.height = widthChild * 100 / 114;
        vPic3.setLayoutParams(lpPic3);

//        Typeface face = Typeface.createFromAsset(mApplication.getAssets(), "fonts/utm_Helve.ttf");
//        mTvwProfileStatus.setTypeface(face);
//        mLayoutAction1 = mHeaderView.findViewById(R.id.layout_profile_action_1);
//        mLayoutAction2 = mHeaderView.findViewById(R.id.layout_profile_action_2);
//        mLayoutAction3 = mHeaderView.findViewById(R.id.layout_profile_action_3);
//        mLayoutAction4 = mHeaderView.findViewById(R.id.layout_profile_action_4);
//        mLayoutAction5 = mHeaderView.findViewById(R.id.layout_profile_action_5);
//        mLayoutAction3.setVisibility(View.GONE);
//        mLayoutAction4.setVisibility(View.GONE);
//        if (mApplication.getReengAccountBusiness().isVietnam())
//        mLayoutAction5.setVisibility(View.VISIBLE);
//        else
//            mLayoutAction5.setVisibility(View.GONE);

        //image album
//        View mImgUpload = mHeaderView.findViewById(R.id.btn_upload_image);
//        mImgUpload.setVisibility(View.GONE);
//        mBtnShowAll = mHeaderView.findViewById(R.id.tvw_view_all_album);
//        mBtnShowAll.setVisibility(View.VISIBLE);
//
//        mTvwAction = mHeaderView.findViewById(R.id.tvw_title_action);
//        mTvwAction.setVisibility(View.GONE);
//        mLayoutMyAlbum = mHeaderView.findViewById(R.id.view_my_album);
//
//        mView3Image = mHeaderView.findViewById(R.id.ll_image_view_small);
//        mView2Image = mHeaderView.findViewById(R.id.ll_image_view_big);
//
//        mImageViewList = new SquareImageView[5];
//        mImageViewList[0] = mView3Image.findViewById(R.id.img_0);
//        mImageViewList[1] = mView3Image.findViewById(R.id.img_1);
//        mImageViewList[2] = mView3Image.findViewById(R.id.img_2);
//
//        mImageViewList[3] = mView2Image.findViewById(R.id.img_3);
//        mImageViewList[4] = mView2Image.findViewById(R.id.img_4);
//
//        mView3Image.setVisibility(View.GONE);
//        mView2Image.setVisibility(View.GONE);
//
//        mBtnShowAll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gotoAlbumPreview();
//            }
//        });
//        for (int i = 0; i < 5; i++) {
//            mImageViewList[i].setVisibility(View.GONE);
//        }
//
//        DisplayMetrics displayMetrics = mApplication.getResources().getDisplayMetrics();
//        int widthScreen = displayMetrics.widthPixels;
//        TypedValue outValue = new TypedValue();
//        getResources().getValue(R.dimen.ratio_profile_avatar_friend, outValue, true);
//        float ratio = outValue.getFloat();
//        widthBigAvatar = widthScreen;
//        maxHeight = Math.round((float) widthBigAvatar / ratio);
//        maxHeightDefaut = maxHeight;
//        if (widthBigAvatar > 800) {
//            widthBigAvatar = 800;
//        }
//        heightBigAvatar = Math.round((float) widthBigAvatar / ratio);
//        Log.i(TAG, "width avatar: " + widthBigAvatar + " height avatar: " + heightBigAvatar);
    }

//    boolean colorWhite = true;

//    private void setColorItemFakeToolbar(boolean isWhite) {
//        if (isWhite) {
//            if (!colorWhite) {
//                mTvwProfileNameToolbar.setVisibility(View.INVISIBLE);
//                mViewBottomToolbar.setVisibility(View.GONE);
//                mImgBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.white));
//                mTvwSaveContact.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
//                colorWhite = true;
//            }
//        } else {
//            colorWhite = false;
//            mTvwProfileNameToolbar.setVisibility(View.VISIBLE);
//            mViewBottomToolbar.setVisibility(View.VISIBLE);
//            mImgBack.setColorFilter(ContextCompat.getColor(getContext(), R.color.bg_ab_icon));
//            mTvwSaveContact.setTextColor(ContextCompat.getColor(getContext(), R.color.bg_ab_icon));
//        }
//    }

    private void setViewListeners() {
        setImageAvatarListener();
        setBackListener();
        setAbEditProfileListener();
        setListviewListener();
        setHeaderButtonListener();
        setOldClickListener();
//        setSpointClicklistener();
    }

    private void setOldClickListener() {
        mTvwAge.setOnClickListener(view -> {
            if (mTvwAge.getText().equals(birthdayStr)) {
                mTvwAge.setText(String.format(mParentActivity.getString(R.string.mc_profile_age), old));
            } else {
                mTvwAge.setText(birthdayStr);
            }
        });
//        mImgSex.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isShowBirthday) {
//                    if (mTvwAge.getText().equals(birthdayStr)) {
//                        mTvwAge.setText(String.valueOf(old));
//                    } else {
//                        mTvwAge.setText(birthdayStr);
//                    }
//                }
//            }
//        });
    }

//    private void setSpointClicklistener() {
//        mViewPoint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AccumulatePointHelper.getInstance(mApplication).navigateToTotalPoint(mParentActivity);
//            }
//        });
//    }

    private void setHeaderButtonListener() {
        mTvwButtonProfile.setOnClickListener(v -> {
            mParentActivity.trackingEvent(gaCategoryId, R.string.ga_action_interaction, R.string.ga_label_follow);
            if (!NetworkHelper.isConnectInternet(mApplication)) {
                mParentActivity.showToast(R.string.error_internet_disconnect);
                return;
            }
            if (statusFollow == Constants.CONTACT.FOLLOW_STATE_FRIEND) {
                showMenuUnFollow();
            } else {
                processFollow();
            }
        });

        vMessage.setOnClickListener(v -> handleSendMsg());

        vListen.setOnClickListener(v -> {
            String lAvatar = null;
            if (mStrangerPhoneNumber != null) {
                if (mStrangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                } else if (mNonContact != null && mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                    lAvatar = mNonContact.getLAvatar();
                } else {
                    lAvatar = mStrangerPhoneNumber.getFriendAvatarUrl();
                }
            } else if (mNonContact != null) {
                if (mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                    lAvatar = mNonContact.getLAvatar();
                }
            } else {
                lAvatar = mFriendChangeAvatar;
            }
            NavigateActivityHelper.navigateToSelectSongAndListenr(mApplication, mParentActivity, mFriendJidNumber,
                    mFriendName, lAvatar);
        });
        //boi doi
//        mLayoutAction5.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mParentActivity.trackingEvent(gaCategoryId, R.string.ga_action_interaction, R.string.ga_label_divine);
//                if (!NetworkHelper.isConnectInternet(mApplication)) {
//                    mParentActivity.showToast(R.string.error_internet_disconnect);
//                    return;
//                }
//                mParentActivity.showLoadingDialog(null, R.string.waiting);
//                ContactRequestHelper.getInstance(mApplication).getDivineDetail(mFriendJidNumber, birthdayStr, new
//                        ContactRequestHelper.onDivineResponse() {
//                            @Override
//                            public void onResponse(String percent, String content) {
//                                mParentActivity.hideLoadingDialog();
//                                showPopupZodiac(percent, content);
//                            }
//
//                            @Override
//                            public void onError(int errorCode) {
//                                mParentActivity.hideLoadingDialog();
//                                mParentActivity.showToast(R.string.e601_error_but_undefined);
//                            }
//                        });
//            }
//        });
    }

    private void setImageClickListener() {
//        for (int i = 0; i < 5; i++) {
//            final int position = i;
//            mImageViewList[i].setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mImageProfileBusiness == null
//                            || mImageProfiles == null
//                            || mImageProfiles.isEmpty()) {
//                        Log.d(TAG, "NULL CMNR");
//                        return;
//                    }
//                    int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfiles, position);
//                    Log.i(TAG, "pos: " + pos);
//                    eventOnMediaHelper.showImageDetail(mFriendName, mFriendJidNumber, mImageProfiles, pos,
//                            FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
//
//                    /*mParentActivity.showImageDetail(mFriendName, mFriendJidNumber, false, mImageProfiles,
//                            Constants.CONTACT_TYPE.NONCONTACT,
//                            mImageProfileBusiness.getImageIndex(Math.min(5, mImageProfiles.size()), position));*/
//                }
//            });
//        }

        vPic1.setOnClickListener(v -> {
            if (mImageProfileBusiness == null
                    || mImageProfiles == null
                    || mImageProfiles.isEmpty()) {
                Log.d(TAG, "NULL CMNR");
                return;
            }

//                int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfiles, 0);
//                Log.i(TAG, "pos: " + pos);
            eventOnMediaHelper.showImageDetail(mFriendName, mFriendJidNumber, mImageProfiles, 0,
                    FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });

        vPic2.setOnClickListener(v -> {
            if (mImageProfileBusiness == null
                    || mImageProfiles == null
                    || mImageProfiles.isEmpty()) {
                Log.d(TAG, "NULL CMNR");
                return;
            }

//                int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfiles, 1);
//                Log.i(TAG, "pos: " + pos);
            eventOnMediaHelper.showImageDetail(mFriendName, mFriendJidNumber, mImageProfiles, 1,
                    FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });

        vPic3.setOnClickListener(v -> {
            if (mImageProfileBusiness == null
                    || mImageProfiles == null
                    || mImageProfiles.isEmpty()) {
                Log.d(TAG, "NULL CMNR");
                return;
            }

//                int pos = ProfileHelper.getIndexImage(mImageProfileBusiness, mImageProfiles, 2);
//                Log.i(TAG, "pos: " + pos);
            if (mImageProfiles != null && mImageProfiles.size() > 3) {
                // show activity preview list image
                if (mListener != null) {
                    mListener.openListImage(mImageProfiles, mFriendName, mFriendJidNumber);
                }
            } else
                eventOnMediaHelper.showImageDetail(mFriendName, mFriendJidNumber, mImageProfiles, 2,
                        FeedContent.ITEM_TYPE_PROFILE_ALBUM, -1);
        });
    }

    private void gotoAlbumPreview() {
        mParentActivity.trackingEvent(gaCategoryId, R.string.ga_action_interaction, R.string.ga_label_show_all_album);
        if (mImageProfiles == null) {
            mImageProfiles = new ArrayList<>();
        }
        /*String name;
        if (mNonContact != null && !TextUtils.isEmpty(mNonContact.getNickName())) {
            name = mNonContact.getNickName();
        } else {
            name = mFriendName;
        }*/

        Intent intent = new Intent(mApplication, AlbumViewActivity.class);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, mImageProfiles);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME, mFriendName);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, mFriendJidNumber);
        mParentActivity.startActivity(intent);
    }

    private int getAlphaforActionBar(int scrollY) {
        int minDist = 0, maxDist = maxHeight;
        if (scrollY > maxDist) {
            return 255;
        } else if (scrollY < minDist) {
            return Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR;
        } else {
            int alpha;
            maxHeight = maxHeightDefaut;
            alpha = (int) ((255.0 / maxDist) * scrollY);
            return Math.max(alpha, Constants.CONTACT.DEFAULT_ALPHA_ACTIONBAR);
        }
    }

    private void setTextButtonSticky() {
        if (isStrangerOtherApp) {
            if (mStrangerPhoneNumber.getState() == StrangerPhoneNumber.StateAccept.not_accept) {
//                mBtnStickyHeader.setText(mRes.getString(R.string.accept_stranger));
//                mBtnSticky.setText(mRes.getString(R.string.accept_stranger));
            } else {
//                mBtnStickyHeader.setText(mRes.getString(R.string.free_chat));
//                mBtnSticky.setText(mRes.getString(R.string.free_chat));
            }
        } else {
//            mBtnStickyHeader.setText(mRes.getString(R.string.free_chat));
//            mBtnSticky.setText(mRes.getString(R.string.free_chat));
        }
    }

    private void drawStrangerMochaDetail() {
        Log.i(TAG, "drawStrangerMochaDetail");
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        setTextButtonSticky();
        mTvwProfileName.setText(mFriendName);
//        mTvwProfileNameToolbar.setText(mFriendName);
        if (mNonContact != null) {
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar, mTvwAvatar, null,
                    mFriendJidNumber, mFriendName, mNonContact.getLAvatar(), size);
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgToolBarAvatar, mTvwToolBarAvatar, null,
                    mFriendJidNumber, mFriendName, mNonContact.getLAvatar(), size);
            urlAvatarTmp = mApplication.getAvatarBusiness().getAvatarUrl(mNonContact.getLAvatar(),
                    mFriendJidNumber, size);
        } else {
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar, mTvwAvatar,
                    mStrangerPhoneNumber, mFriendJidNumber, mFriendName, mFriendChangeAvatar, size);
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgToolBarAvatar, mTvwToolBarAvatar,
                    mStrangerPhoneNumber, mFriendJidNumber, mFriendName, mFriendChangeAvatar, size);
            urlAvatarTmp = mApplication.getAvatarBusiness().getAvatarUrl(mFriendChangeAvatar,
                    mFriendJidNumber, size);
        }
        drawStatusAndGender();
        drawCoverAndAlbums();
        if (!getContact && !isRequesting) {
            getInfoContactFromNumber(mFriendJidNumber);
        }
    }

//    SimpleImageLoadingListener listener = new SimpleImageLoadingListener() {
//        @Override
//        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//            if (loadedImage == null) {
//                ProfileHelper.blurHeaderProfile(mApplication, mFriendJidNumber, mImgProfileAvatar);
//            } else {
//                ImageHelper.blurAvatar(mApplication, loadedImage, null, mImgProfileAvatar);
////                ProfileHelper.blurHeaderProfileWithBitmap(mApplication, loadedImage, mImgProfileAvatar);
//            }
//            super.onLoadingComplete(imageUri, view, loadedImage);
//        }
//
//        @Override
//        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//            ProfileHelper.blurHeaderProfile(mApplication, mFriendJidNumber, mImgProfileAvatar);
//            super.onLoadingFailed(imageUri, view, failReason);
//        }
//    };

    private void setListImageSmall() {
        // disable album TODO
//        if (mImageProfiles == null || mImageProfiles.isEmpty()) {
//            Log.d(TAG, "ff1");
//            mImageProfiles = new ArrayList<>();
//            mLayoutMyAlbum.setVisibility(View.GONE);
//        } else {
//            Log.d(TAG, "ff2");
//            mLayoutMyAlbum.setVisibility(View.VISIBLE);
//            setImageClickListener();
//            ProfileHelper.drawAlbum(mApplication, mImageProfiles, mImageViewList, mView3Image,
//                    mView2Image, mBtnShowAll, false, true, null);
////            drawAlbum(mImageViewList, mView3Image, mView2Image, false);
//            /*mImageProfileBusiness.drawAlbum(
//                    mImageProfiles,
//                    mImageViewList,
//                    mBtnShowAll,
//                    mImageViewLine1,
//                    mImageViewLine2,
//                    false);*/
//        }

        if (mImageProfiles == null || mImageProfiles.isEmpty()) {
            vPicProfile.setVisibility(View.GONE);
        } else {
            setImageClickListener();
            vPicProfile.setVisibility(View.VISIBLE);
            ProfileHelper.drawAlbum(mApplication, mImageProfiles, vPic1,
                    vPic2, vPic3, false);
        }
    }

    private void setImageCover() {
        /*if (null == mImageCover) {
            mImageCover = new ImageProfile();
            mImageCover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
        }

        mImageProfileBusiness.displayImageProfile(mImgCover, mImageCover, false);
        mImgCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                if (mImageCover == null || !mImageCover.hasCover()) {
                if (mImageCover == null) {
                    Log.d(TAG, "Image Cover NULL CMNR");
                    return;
                }
                ArrayList<ImageProfile> imageCover = new ArrayList<>();
                imageCover.add(mImageCover);
                eventOnMediaHelper.showImageDetail(mFriendName, mFriendJidNumber, imageCover, 0,
                        FeedContent.ITEM_TYPE_PROFILE_COVER, -1);
                *//*mParentActivity.showImageDetail(mFriendName, mFriendJidNumber, true, imageCover,
                        Constants.CONTACT_TYPE.NONCONTACT, 0);*//*
            }
        });*/
    }

    /*public void drawAlbum(SquareImageView[] mImageViewList,
                          View mView3Image,
                          View mView2Image,
                          boolean isShowUploadImage) {
        if (!mImageProfiles.isEmpty()) {
            int sizeImageList = Math.min(mImageProfiles.size(), Constants.MAX_IMAGE_PROFILE);
            Log.d(TAG, "sizeImageList: " + sizeImageList);

            if (sizeImageList > 0 && sizeImageList <= 3) {
                mView3Image.setVisibility(View.VISIBLE);
                mView2Image.setVisibility(View.GONE);
            } else {
                mView3Image.setVisibility(View.VISIBLE);
                mView2Image.setVisibility(View.VISIBLE);
            }

            if (sizeImageList != 4 && sizeImageList != 5) { // 1, 2, 3 images
                for (int i = 0; i < sizeImageList; i++) {
                    mImageViewList[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mImageViewList[i].setVisibility(View.VISIBLE);
                    mImageProfileBusiness.displayImageProfile(mImageViewList[i], mImageProfiles.get(i), true);
                }

                if (isShowUploadImage) {
                    if (sizeImageList == 2) {
                        mImageViewList[2].setVisibility(View.VISIBLE);
                        mImageViewList[2].setClickable(true);
                        mImageViewList[2].setScaleType(ImageView.ScaleType.CENTER);
                        mImageViewList[2].setImageResource(R.drawable.ic_upload_image);
                        mImageViewList[2].setBackgroundColor(ContextCompat.getColor(mParentActivity, R.color
                                .gray_light));
                    } else if (sizeImageList == 1) {
                        mImageViewList[1].setVisibility(View.VISIBLE);
                        mImageViewList[1].setClickable(true);
                        mImageViewList[1].setScaleType(ImageView.ScaleType.CENTER);
                        mImageViewList[1].setImageResource(R.drawable.ic_upload_image);
                        mImageViewList[1].setBackgroundColor(ContextCompat.getColor(mParentActivity, R.color
                                .gray_light));
                        mImageViewList[2].setVisibility(View.GONE);
                    }
                }
            } else if (sizeImageList == 4) { //4 images
                mImageViewList[4].setVisibility(View.VISIBLE);
                mImageViewList[3].setVisibility(View.VISIBLE);
                mImageViewList[0].setVisibility(View.VISIBLE);
                mImageViewList[1].setVisibility(View.VISIBLE);

                for (int i = 0; i < sizeImageList; i++) {
                    mImageViewList[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mImageViewList[i].setVisibility(View.VISIBLE);
//                    mImageProfileBusiness.displayImageProfile(mImageViewList[i], mImageProfiles.get(getImageIndex
// (sizeImageList, i)), false, true);

                }
                mImageProfileBusiness.displayImageProfile(mImageViewList[0], mImageProfiles.get(2), true);
                mImageProfileBusiness.displayImageProfile(mImageViewList[1], mImageProfiles.get(3), true);
                mImageProfileBusiness.displayImageProfile(mImageViewList[3], mImageProfiles.get(0), true);
                mImageProfileBusiness.displayImageProfile(mImageViewList[4], mImageProfiles.get(1), true);

                //show btn upload
                if (isShowUploadImage) {
                    mImageViewList[2].setVisibility(View.VISIBLE);
                    mImageViewList[2].setClickable(true);
                    mImageViewList[2].setScaleType(ImageView.ScaleType.CENTER);
                    mImageViewList[2].setImageResource(R.drawable.ic_upload_image);
                    mImageViewList[2].setBackgroundColor(ContextCompat.getColor(mParentActivity, R.color.gray_light));
                }
            } else { // 5 images
                for (int i = 0; i < sizeImageList; i++) {
                    mImageViewList[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mImageViewList[i].setVisibility(View.VISIBLE);
                    mImageProfileBusiness.displayImageProfile(mImageViewList[i],
                            mImageProfiles.get(mImageProfileBusiness.getImageIndex(sizeImageList, i)), true);
                }
                mBtnShowAll.setVisibility(View.VISIBLE);
            }
        } else {
            mView3Image.setVisibility(View.GONE);
            mView2Image.setVisibility(View.GONE);
            mBtnShowAll.setVisibility(View.GONE);
        }
    }*/

    private void drawStrangerKeengDetail() {
        Log.i(TAG, "drawStrangerKeengDetail");
        if (mStrangerPhoneNumber == null) {
            mParentActivity.finish();
            return;
        }
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        setTextButtonSticky();
        mTvwProfileName.setText(mStrangerPhoneNumber.getFriendName());
//        mTvwProfileNameToolbar.setText(mStrangerPhoneNumber.getFriendName());
        mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar, mTvwAvatar, mStrangerPhoneNumber,
                mStrangerPhoneNumber.getPhoneNumber(), null, null, size);
        mApplication.getAvatarBusiness().setStrangerAvatar(mImgToolBarAvatar, mTvwToolBarAvatar, mStrangerPhoneNumber,
                mStrangerPhoneNumber.getPhoneNumber(), null, null, size);
        urlAvatarTmp = mStrangerPhoneNumber.getFriendAvatarUrl();
        Log.i(TAG, "drawStrangerKeengDetail urlAvatarTmp=" + urlAvatarTmp);
        // TODO de an het da, lay dc tu keeng thi hien sau
        drawStatusAndGender();
        drawCoverAndAlbums();
        if (!getContact && !isRequesting) {
            getInfoContactFromNumber(mFriendJidNumber);
        }
//        mLayoutMyAlbum.setVisibility(View.GONE);
    }

    public void drawCoverAndAlbums() {
//        mImageProfiles = mImageLoaderManager.getImageProfileOfPhoneNumber(mFriendJidNumber);
//        mImageCover = mImageLoaderManager.getImageCoverOfPhoneNumber(mFriendJidNumber);

        if (null == mImageCover) {
//            getInfoContactFromNumber(mFriendJidNumber);
        } else {
            setImageCover();
        }
        setListImageSmall();
//        setImageClickListener();
    }

    private void drawStatusAndGender() {
        if (mNonContact != null && mNonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
            String status = mNonContact.getStatus();
            if (!TextUtils.isEmpty(status)) {
                mTvwProfileStatus.setEmoticonStatus(mParentActivity, status, status.hashCode(), status);
                mTvwProfileStatus.setVisibility(View.VISIBLE);
            } else {
                mTvwProfileStatus.setVisibility(View.GONE);
            }

            mTvwButtonProfile.setVisibility(View.INVISIBLE);
//            mLayoutInfoAgePoint.setVisibility(View.INVISIBLE);
        } else if (mNonContact != null && mNonContact.getState() == Constants.CONTACT.ACTIVE) {
//            mLayoutInfoAgePoint.setVisibility(View.VISIBLE);
            String status = mNonContact.getStatus();
            if (!TextUtils.isEmpty(status)) {
                mTvwProfileStatus.setEmoticonStatus(mParentActivity, status, status.hashCode(), status);
                mTvwProfileStatus.setVisibility(View.VISIBLE);
            } else {
                mTvwProfileStatus.setVisibility(View.GONE);
            }
            if (mNonContact.getGender() == Constants.CONTACT.GENDER_FEMALE) {
                mImgSex.setImageResource(R.drawable.ic_v5_profile_female);
                mImgSex.setVisibility(View.VISIBLE);
            } else if (mNonContact.getGender() == Constants.CONTACT.GENDER_MALE) {
                mImgSex.setImageResource(R.drawable.ic_v5_profile_male);
                mImgSex.setVisibility(View.VISIBLE);
            } else {
                mImgSex.setVisibility(View.GONE);
            }
            if (mNonContact.isShowBirthday()) {
                long birthDay = TimeHelper.convertBirthdayStringToLong(mNonContact.getBirthDayString());
                old = TimeHelper.getOldFromBirthday(birthDay);
                birthdayStr = TimeHelper.formatTimeBirthday(birthDay);
                if (old <= 0) {
                    mTvwAge.setVisibility(View.GONE);
                } else {
                    mTvwAge.setText(String.format(mParentActivity.getString(R.string.mc_profile_age), old));
                    mTvwAge.setVisibility(View.VISIBLE);
                }
                isShowBirthday = true;
            } else {
                isShowBirthday = false;
                mTvwAge.setVisibility(View.GONE);
            }
        } else if (mNonContact == null) {
//            mLayoutInfoAgePoint.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(mFriendStatus)) {
                mTvwProfileStatus.setEmoticonStatus(mParentActivity, mFriendStatus, mFriendStatus.hashCode(),
                        mFriendStatus);
                mTvwProfileStatus.setVisibility(View.VISIBLE);
            } else {
                mTvwProfileStatus.setVisibility(View.GONE);
            }
            if (mFriendGender == Constants.CONTACT.GENDER_FEMALE) {
                mImgSex.setImageResource(R.drawable.ic_sex_female);
                mImgSex.setVisibility(View.VISIBLE);
            } else if (mFriendGender == Constants.CONTACT.GENDER_MALE) {
                mImgSex.setImageResource(R.drawable.ic_sex_male);
                mImgSex.setVisibility(View.VISIBLE);
            } else {
                mImgSex.setVisibility(View.GONE);
            }
            if (mStrangerPhoneNumber != null) {
                if (!TextUtils.isEmpty(mFriendBirthDay)) {
                    long birthDay = TimeHelper.convertBirthdayStringToLong(mFriendBirthDay);
                    old = TimeHelper.getOldFromBirthday(birthDay);
                    birthdayStr = TimeHelper.formatTimeBirthday(birthDay);
                    if (old <= 0) {
                        mTvwAge.setVisibility(View.GONE);
                    } else {
                        mTvwAge.setText(String.format(mParentActivity.getString(R.string.mc_profile_age), old));
                        mTvwAge.setVisibility(View.VISIBLE);
                    }
                    isShowBirthday = true;
                   /* mTvwGender.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable
                            (mApplication, R.drawable.ic_dot), null);
                    mTvwBirthday.setVisibility(View.VISIBLE);*/
                } else {
                    /*mTvwGender.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mTvwBirthday.setVisibility(View.GONE);*/
                    isShowBirthday = false;
                    mTvwAge.setVisibility(View.GONE);
                }
            } else {
                if (!TextUtils.isEmpty(mFriendBirthDay)) {
                    long birthDay = TimeHelper.convertBirthdayStringToLong(mFriendBirthDay);
                    old = TimeHelper.getOldFromBirthday(birthDay);
                    birthdayStr = TimeHelper.formatTimeBirthday(birthDay);
                    if (old <= 0) {
                        mTvwAge.setVisibility(View.GONE);
                    } else {
                        mTvwAge.setText(String.format(mParentActivity.getString(R.string.mc_profile_age), old));
                        mTvwAge.setVisibility(View.VISIBLE);
                    }
                    isShowBirthday = true;
                    /*mTvwGender.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable
                            (mParentActivity, R.drawable.ic_dot), null);
                    mTvwBirthday.setVisibility(View.VISIBLE);*/
                } else {
                    /*mTvwGender.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    mTvwBirthday.setVisibility(View.GONE);*/
                    isShowBirthday = false;
                    mTvwAge.setVisibility(View.GONE);
                }
            }

        } else {
            mTvwProfileStatus.setVisibility(View.INVISIBLE);
//            mLayoutInfoAgePoint.setVisibility(View.GONE);
        }
    }

    private void handleSendMsg() {
        saveNonContactBeforeChat();
        if (mStrangerPhoneNumber != null && isStrangerOtherApp) {
            if (mStrangerPhoneNumber.getState() == StrangerPhoneNumber.StateAccept.not_accept) {
                mParentActivity.showLoadingDialog(null, mRes.getString(R.string.waiting));
                StrangerBusiness.onAcceptStrangeKeengListener listener = new StrangerBusiness.
                        onAcceptStrangeKeengListener() {
                    @Override
                    public void onResponse() {
                        mParentActivity.hideLoadingDialog();
                        mStrangerPhoneNumber = mStrangerBusiness.getExistStrangerPhoneNumberFromNumber
                                (mFriendJidNumber);
                        setTextButtonSticky();
                        Log.i(TAG, "onResponse");
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                    }
                };
                mStrangerBusiness.requestAcceptStranger(mStrangerPhoneNumber.getPhoneNumber(),
                        mStrangerPhoneNumber.getMyName(), listener);
            } else {    // profile lam quen qua keeng va da dong y thi vao chat chi tiet
                mListener.navigateToReengChatActivity(mStrangerPhoneNumber.getPhoneNumber());
            }
        } else if (mStrangerPhoneNumber != null) {
            mListener.navigateToReengChatActivity(mStrangerPhoneNumber.getPhoneNumber());
        } else {// tao moi stranger number va tao thread message
            ThreadMessage threadMessage = mApplication.getStrangerBusiness().
                    createMochaStrangerAndThread(mFriendJidNumber, mFriendName, mFriendChangeAvatar, Constants
                            .CONTACT.STRANGER_MOCHA_ID, true);
            mListener.navigateToThreadDetail(threadMessage);
        }
    }

    private void saveNonContactBeforeChat() {
        if (responseNumber != null) {
            Log.d(TAG, "saveNonContactBeforeChat");
            mContactBusiness.insertOrUpdateNonContact(responseNumber, true, true);
        }
    }

    private void setImageAvatarListener() {
        mImgAvatar.setOnClickListener(view -> processWhenClickAvatar());
        mImgToolBarAvatar.setOnClickListener(view -> processWhenClickAvatar());
    }

    private void processWhenClickAvatar() {
        String lAvatar = null;
        String strangerAvatarUrl = null;
        if (mStrangerPhoneNumber != null) {
            if (mStrangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                strangerAvatarUrl = mStrangerPhoneNumber.getFriendAvatarUrl();
            } else if (mNonContact != null && mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                lAvatar = mNonContact.getLAvatar();
            } else {
                lAvatar = mStrangerPhoneNumber.getFriendAvatarUrl();
            }
        } else if (mNonContact != null) {
            if (mNonContact.getState() == Constants.CONTACT.ACTIVE) {
                lAvatar = mNonContact.getLAvatar();
            }
        } else {
            lAvatar = mFriendChangeAvatar;
        }
        if (!TextUtils.isEmpty(lAvatar) || !TextUtils.isEmpty(strangerAvatarUrl)) {
            mApplication.getAvatarBusiness().displayFullAvatar(mFriendJidNumber, lAvatar,
                    mFriendName, strangerAvatarUrl, urlAvatarTmp);
        }
    }

    private void setAbEditProfileListener() {
        icOption.setOnClickListener(v -> processWhenClickOption(icOption));
        icHeaderOption.setOnClickListener(v -> processWhenClickOption(icHeaderOption));
    }

    private void processWhenClickOption(View menuOption) {
        ArrayList<ItemContextMenu> overFlowItems = new ArrayList<>();
        ItemContextMenu editContact = new ItemContextMenu(mRes.getString(R.string.save)
                , -1,
                null, Constants.MENU.MENU_CONTACTS);
        overFlowItems.add(editContact);
        PopupHelper.getInstance().showOrHideOverFlowMenu(menuOption,
                (view, entry, menuId) -> {
                    if (menuId == Constants.MENU.MENU_CONTACTS && mListener != null) {
                        mListener.saveContact("", mFriendName);
                    }
                }, overFlowItems);
    }

    private void setBackListener() {
        mImgBack.setOnClickListener(view -> mParentActivity.onBackPressed());
        mImgHeaderBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private void setListviewListener() {
        RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.d(TAG, "onScrollStateChanged " + newState);
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                checkAndSetAlphaActionBar();
                if (dy > 0) //check for scroll down
                {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
                    if (layoutManager == null) return;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    if (((visibleItemCount + pastVisiblesItems) >= totalItemCount) && !isLoading && !listFeed.isEmpty
                            () && !noMoreFeed) {
                        Log.i(TAG, "needToLoad");
                        onLoadMore();
                    }
                }

            }
        };
        mRecyclerViewFeed.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(mOnScrollListener));
        nScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) ->
                rlToolbar.setVisibility(scrollY > ViewUtils.dpToPx(60) ? View.VISIBLE : View.GONE));
    }

//    private void checkAndSetAlphaActionBar() {
//        View c = mRecyclerViewFeed.getChildAt(0);
//        if (c != null) {
//            if (mHeaderView == c) {
////                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
//                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerViewFeed.getLayoutManager();
//                if (layoutManager == null) return;
//                int scrolly = -c.getTop() + layoutManager.findFirstVisibleItemPosition() * c.getHeight();
//                int alpha = getAlphaforActionBar(scrolly);
//                mDrawableAb.setAlpha(alpha);
//                mDrawableStatus.setAlpha(alpha);
//                if (alpha < Constants.SETTINGS.ALPHA_SHOW_ACTIONBAR) {
//                    setColorItemFakeToolbar(true);
//                } else {
//                    setColorItemFakeToolbar(false);
//                }
//            }
//        }
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
                || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (mFeedAdapter != null && mFeedAdapter.getItemCount() > 0) {
                mFeedAdapter.notifyDataSetChanged();
            }
        }
        Log.i(TAG, "newConfig=" + newConfig.orientation);
    }

    //=======onmedia
    @Override
    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        final boolean isLiked = feed.getIsLike() == 1;
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        mFeedBusiness.addUrlActionLike(feed.getFeedContent().getUrl(), feed.getBase64RowId(), action, feed.getFeedContent());
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);

    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        if (feed.getFeedContent() == null) return;
        String urlKey = feed.getFeedContent().getUrl();
        if (TextUtils.isEmpty(urlKey)) return;
        for (int i = 0; i < listFeed.size(); i++) {
            if (listFeed.get(i).getFeedContent() != null && urlKey.equals(listFeed.get(i).getFeedContent().getUrl())) {
                int delta;
                if (isLike) {
                    delta = 1;
                    listFeed.get(i).setIsLike(1);
                } else {
                    delta = -1;
                    listFeed.get(i).setIsLike(0);
                }
                long countLike = listFeed.get(i).getFeedContent().getCountLike();
                listFeed.get(i).getFeedContent().setCountLike(countLike + delta);
            }
        }
        mFeedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickCommentFeed(FeedModelOnMedia feed) {
        displayCommentStatusFragment(feed);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_comment);
    }

    public void displayCommentStatusFragment(FeedModelOnMedia mItem) {
        if (feedInterface != null) {
            feedInterface.navigateToComment(mItem);
        }
    }

    private void addAllStatusOnProfile(String urlKey, int countCmt) {
        for (FeedModelOnMedia feed : listFeed) {
            if (feed.getFeedContent().getUrl().equals(urlKey)) {
                long countComment = feed.getFeedContent().getCountComment();
                feed.getFeedContent().setCountComment(countComment + countCmt);
            }
        }
    }

    @Override
    public void onClickShareFeed(FeedModelOnMedia feed) {
        eventOnMediaHelper.showPopupContextMenuShare(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share);
    }

    @Override
    public void onClickUser(UserInfo userInfo) {
        if (userInfo != null) {
            if (!userInfo.getMsisdn().equals(mFriendJidNumber)) {
                eventOnMediaHelper.processUserClick(userInfo);
            }
        }
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_avatar);
    }

    @Override
    public void onClickMediaItem(FeedModelOnMedia feed) {
        eventOnMediaHelper.handleClickMediaItem(feed, gaCategoryId, gaActionId, feedInterface, iconListener);
    }

    @Override
    public void onClickImageItem(FeedModelOnMedia feed, int positionImage) {
        eventOnMediaHelper.handleClickImage(feed, positionImage, Constants.ONMEDIA.PARAM_IMAGE
                .FEED_FROM_FRIEND_PROFILE);
    }

    @Override
    public void onClickMoreOption(FeedModelOnMedia feed) {
//        showPopupContextMenu(feed);
        eventOnMediaHelper.handleClickMoreOption(feed, this);
        mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_option_feed);
    }

    @Override
    public void onClickButtonTotal(View rowView, FeedModelOnMedia feed) {
        eventOnMediaHelper.handleShareTotal(rowView, feed, gaCategoryId, gaActionId);
    }

    @Override
    public void onLongClickStatus(FeedModelOnMedia feed) {
        if (feed != null) {
            eventOnMediaHelper.showPopupContextMenuLongClick(feed, this);
        }
    }

    @Override
    public void onDeepLinkClick(FeedModelOnMedia feed, String link) {
        eventOnMediaHelper.handleDeeplink(feed, link);
    }

    @Override
    public void onClickSuggestFriend(UserInfo userInfo) {

    }

    @Override
    public void openChannelInfo(FeedModelOnMedia feed) {
        Channel channelVideo = Channel.convertFromChannelOnMedia(feed.getFeedContent().getChannel());
        if (channelVideo == null) return;
        mApplication.getApplicationComponent().providesUtils().openChannelInfo(mParentActivity, channelVideo);
    }

    @Override
    public void openPlayStore(FeedModelOnMedia feed, String packageName) {
        NavigateActivityHelper.navigateToPlayStore(mParentActivity, packageName);
    }

    @Override
    public void onSubscribeChannel(FeedModelOnMedia feed, Channel channel) {
        if (channel != null) {
            ApplicationController.self().getListenerUtils().notifyChannelSubscriptionsData(channel);
            ApplicationController.self().getApplicationComponent().providerChannelApi().callApiSubOrUnsubChannel(channel.getId(), channel.isFollow());
        }
    }

    /*private void showPopupContextMenuLongClick(FeedModelOnMedia feed) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(mParentActivity, mRes.getString(R.string.onmedia_copy_text), -1,
                feed, Constants.MENU.MENU_COPY_TEXT);
        listMenu.add(copyItem);
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(),
                    null, listMenu, this);
        }
    }*/

    public void handleShareLink(final FeedModelOnMedia feed) {
        if (feed.getIsShare() == 1) {
            mParentActivity.showToast(R.string.onmedia_already_shared);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        setShareFeed(feed, 1);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE, "",
                feed.getBase64RowId(), "", null,
                response -> {
                    Log.i(TAG, "actionShare: onresponse: " + response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                            int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                mParentActivity.showToast(R.string.onmedia_share_success);
                            } else {
                                setShareFeed(feed, -1);
                                mParentActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } else {
                            setShareFeed(feed, -1);
                            mParentActivity.showToast(R.string.e601_error_but_undefined);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        setShareFeed(feed, -1);
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }, volleyError -> {
                    setShareFeed(feed, -1);
                    mParentActivity.showToast(R.string.e601_error_but_undefined);
                });
    }

    private void setShareFeed(FeedModelOnMedia feed, int delta) {
        String urlKey = feed.getFeedContent().getUrl();
        for (int i = 0; i < listFeed.size(); i++) {
            if (listFeed.get(i).getFeedContent().getUrl().equals(urlKey)) {
                long countShare = listFeed.get(i).getFeedContent().getCountShare();
//        long countComment = feed.getFeedContent().getCountComment();
                listFeed.get(i).getFeedContent().setCountShare(countShare + delta);
//        feed.getFeedContent().setCountComment(countComment + delta);
                if (delta == 1) {
                    listFeed.get(i).setIsShare(1);
                } else {
                    listFeed.get(i).setIsShare(0);
                }
            }
        }
        mFeedAdapter.notifyDataSetChanged();
    }


    /*private void playSongAlbum(FeedModelOnMedia feed) {
        mApplication.getPlayMusicController().pauseMusic();
        mMusicBusiness.resetSessionMusic();
        MediaModel mediaModel = feed.getFeedContent().getMediaMode();
        mApplication.getPlayMusicController().getPlayingListDetail(mediaModel);
        // log feed
        mApplication.getKeengProfileBusiness().logListenTogether(mediaModel);
        Log.i(TAG, "mediamode: mediaurl: " + mediaModel.getMedia_url() + " itemid: " + mediaModel.getId() + " url: "
                + mediaModel.getUrl());
        mApplication.getPlayMusicController().setIsPlayFromFeed(true);
        if (feedInterface != null) {
            feedInterface.navigateToAlbum(feed);
        }
    }

    private void handleClickPopupExitListenTogether(FeedModelOnMedia item) {
        FeedContent feedContent = item.getFeedContent();
        if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_VIDEO)) {
            mMusicBusiness.closeMusic();
            mApplication.getPlayMusicController().pauseMusic();
            if (!TextUtils.isEmpty(feedContent.getMediaUrl())) {
                if (feedInterface != null) {
                    feedInterface.navigateToVideoView(item);
                }
            }
        } else {
            mMusicBusiness.closeMusic();
            mApplication.getPlayMusicController().pauseMusic();
            mApplication.getPlayMusicController().getPlayingListDetail(feedContent.getMediaMode());
            mApplication.getPlayMusicController().setIsPlayFromFeed(true);
            if (feedInterface != null) {
                feedInterface.navigateToAlbum(item);
            }
            // log feed
            mApplication.getKeengProfileBusiness().logListenTogether(feedContent.getMediaMode());
        }
    }

    private void showPopupContextMenu(FeedModelOnMedia feed) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<ItemContextMenu>();
        ItemContextMenu reportItem = new ItemContextMenu(mApplication, mRes.getString(R.string
                .onmedia_setting_report), -1,
                feed, Constants.MENU.REPORT);
        listMenu.add(reportItem);
        if (!mFeedBusiness.isFeedFromProfile(feed)) {
            ItemContextMenu copyItem = new ItemContextMenu(mApplication, mRes.getString(R.string.web_pop_copylink), -1,
                    feed, Constants.MENU.COPY);
            listMenu.add(copyItem);
        }
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance(mParentActivity).showContextMenu(getFragmentManager(),
                    null, listMenu, this);
        }
    }*/

    private void confirmUnfollow(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.onmedia_setting_unfollow_text1);
        String msg = mRes.getString(R.string.onmedia_message_unfollow);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_UNFOLLOW);
    }

    private void confirmReport(FeedModelOnMedia feed) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.onmedia_setting_report);
        String msg = mRes.getString(R.string.onmedia_message_report);
        PopupHelper.getInstance().showDialogConfirm(mParentActivity, title,
                msg, labelOK, labelCancel, this, feed, Constants.MENU.POPUP_REPORT);
    }

    private void handleUnfollow(FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.unFollowFriend(feed.getUserInfo().getMsisdn(), s -> {
            try {
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                    int code = jsonObject.optInt(Constants.HTTP.REST_CODE, 0);
                    if (code == HTTPCode.E200_OK) {
                        mParentActivity.showToast(R.string.onmedia_unfollow_success);
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

        }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));
    }

    private void handleReport(FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(R.string.no_connectivity_check_again);
            return;
        }
        rest.reportViolation(feed, s -> {
            try {
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                    int code = jsonObject.optInt(Constants.HTTP.REST_CODE, 0);
                    if (code == HTTPCode.E200_OK) {
                        mParentActivity.showToast(R.string.onmedia_action_success);
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mParentActivity.showToast(R.string.e601_error_but_undefined);
            }

        }, volleyError -> mParentActivity.showToast(R.string.e601_error_but_undefined));
    }

    /*@Override
    public void notifyFeedOnMedia(boolean needNotify, boolean needToSetSelection) {
        Log.i(TAG, "notifyFeedOnMedia: " + needNotify + " selection: " + needToSetSelection);
        notifyNewFeed(needToSetSelection);
    }

    @Override
    public void onPostFeed(FeedModelOnMedia feed) {
        mFeedBusiness.getListFeed().add(0, feed);
        mFeedBusiness.getListPost().add(feed);
        mFeedBusiness.getListFeedProfile().add(0, feed);

        mFeedAdapter.notifyDataSetChanged();
    }*/

    public void notifyNewFeed(final boolean needToSetSelection) {
        mParentActivity.runOnUiThread(() -> {
            Log.i(TAG, "notify nowwwwwwwwwwwwww");
            FeedModelOnMedia feedProcess = mFeedBusiness.getFeedProfileProcess();
            if (feedProcess == null || feedProcess.getFeedContent() == null ||
                    TextUtils.isEmpty(feedProcess.getFeedContent().getUrl())) {
                return;
            }
            for (int i = 0; i < listFeed.size(); i++) {
                FeedModelOnMedia feedTmp = listFeed.get(i);
                if (feedTmp == null || feedTmp.getFeedContent() == null ||
                        TextUtils.isEmpty(feedTmp.getFeedContent().getUrl())) {
                    break;
                }
                if (feedTmp.getFeedContent().getUrl().equals(feedProcess.getFeedContent().getUrl())) {
                    feedTmp.getFeedContent().setCountShare(feedProcess.getFeedContent().getCountShare());
                    feedTmp.getFeedContent().setCountComment((feedProcess.getFeedContent().getCountComment()));
                    feedTmp.getFeedContent().setCountLike(feedProcess.getFeedContent().getCountLike());
                    feedTmp.setIsLike(feedProcess.getIsLike());
                }
            }
            mFeedAdapter.notifyDataSetChanged();
            if (needToSetSelection) {
                mRecyclerViewFeed.scrollToPosition(1);
            }
        });

    }

    @Override
    public void OnClickUser(String msisdn, String name) {
        if (enableClickTag) {
            //ko hieu sao cai clickableSpan bi nhay vao 2 lan lien @@
            enableClickTag = false;
            Log.i(TAG, "msisdn: " + msisdn);
            if (!msisdn.equals(mFriendJidNumber)) {
                UserInfo userInfo = new UserInfo();
                userInfo.setMsisdn(msisdn);
                userInfo.setName(name);
                userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                userInfo.setStateMocha(1);
                eventOnMediaHelper.processUserClick(userInfo);
            }
        } else {
            enableClickTag = true;
        }
    }

    public interface OnStrangerFragmentInteractionListener {
        void navigateToReengChatActivity(String number);

        void navigateToThreadDetail(ThreadMessage threadMessage);

        void saveContact(String number, String name);

        void openListImage(ArrayList<ImageProfile> imageProfiles, String name, String jIdNumber);
    }

    /*public void onClickImageProfile(int position) {

        if (mImageProfiles == null) {
            Log.d(TAG, "mImageProfiles NULL");
            mImageProfiles = new ArrayList<ImageProfile>();
        }

        if (mImageCover == null) {
            Log.d(TAG, "mImageCover NULL");
            return;
        }

        ArrayList<ImageProfile> imageList = new ArrayList<ImageProfile>();
        if (position >= 0) {
            imageList = mImageProfiles;
        } else {
            imageList.add(mImageCover);
            position = 0;
        }

        Intent intent = new Intent(mApplication, PreviewImageProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PreviewImageProfileActivity.PARAM_LIST_IMAGE, imageList);
        intent.putExtra(PreviewImageProfileActivity.PARAM_CURRENT_IMAGE, position);
        intent.putExtra(PreviewImageProfileActivity.PARAM_AVATAR_COVER, true);
        intent.putExtra(PreviewImageProfileActivity.PARAM_THREAD_NAME, mFriendName);
        intent.putExtra(PreviewImageProfileActivity.PARAM_PHONE_NUMBER, mFriendJidNumber);
        mApplication.startActivity(intent);
    }*/

    public void getInfoContactFromNumber(String number) {
        if (TextUtils.isEmpty(number)) {
            setImageCover();
            return;
        }
        isRequesting = true;
        ContactRequestHelper
                .getInstance(mApplication)
                .getProfileFromNumber(number,
                        new ContactRequestHelper.onResponseProfileInfoListener() {
                            @Override
                            public void onResponse(PhoneNumber phone) {
                                if (phone != null) {
                                    if (null == mImageProfiles) {
                                        mImageProfiles = new ArrayList<>();
                                    } else {
                                        mImageProfiles.clear();
                                    }
                                    mImageProfiles.addAll(phone.getAlbums());
                                    mContactBusiness.insertOrUpdateNonContact(phone, true, true);
                                    if (mNonContact == null) {
                                        getContact = true;
                                        isRequesting = false;
                                        responseNumber = phone;
                                    }
                                    onPresenceChange(phone);
                                } else {
                                    PhoneNumber phoneNumber = new PhoneNumber();
                                    phoneNumber.setJidNumber(mFriendJidNumber);
                                    phoneNumber.setState(Constants.CONTACT.NONE);
                                    mContactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                                }
                            }

                            @Override
                            public void onError(int errorCode) {
//                                setImageCover();
                                Log.e(TAG, "Error getInfoContactFromNumber: " + errorCode);
                            }
                        });
    }

    //============onmedia
    private void loadData(final String lastRowId) {
        if (mParentActivity != null) {
            if (!NetworkHelper.isConnectInternet(mParentActivity)) {
                mParentActivity.showToast(R.string.no_connectivity_check_again);
                mLoadmoreFooterView.setVisibility(View.GONE);
                return;
            }
            isLoading = true;
            rest.getUserTimeLine(mFriendJidNumber, lastRowId, restAllFeedsModel -> {
                Log.i(TAG, "onResponse: getUserTimeLine code" + restAllFeedsModel.getCode());
                isLoading = false;
                mLoadmoreFooterView.setVisibility(View.GONE);
                if (restAllFeedsModel.getCode() == HTTPCode.E200_OK) {
                    mFeedBusiness.setDeltaTimeServer(
                            System.currentTimeMillis() - restAllFeedsModel.getCurrentTimeServer());
                    onLoadDataDone(restAllFeedsModel.getData(), lastRowId);
                }
            }, volleyError -> {
                isLoading = false;
                mLoadmoreFooterView.setVisibility(View.GONE);
                Log.i(TAG, "error");
            });
        }
    }

    private void onLoadDataDone(ArrayList<FeedModelOnMedia> result, String lastRowId) {
        if (result != null && !result.isEmpty()) {
            noMoreFeed = false;
            result = mFeedBusiness.preProcessListFeedModelListTag(result);
            if (TextUtils.isEmpty(lastRowId)) {
                listFeed = result;
//                mTvwAction.setVisibility(View.VISIBLE);
            } else {
                listFeed.addAll(result);
            }
            Log.i(TAG, "load data done: " + listFeed.size());
            mFeedAdapter.setListFeed(listFeed);
            mFeedAdapter.notifyDataSetChanged();
        } else {
            Log.i(TAG, "nomore feed");
            noMoreFeed = true;
            /*if (TextUtils.isEmpty(lastRowId)) {
            } else {
                mParentActivity.showToast(R.string.msg_loadmore_empty);
            }*/
        }
    }

    private String getLastRowId() {
        if (listFeed == null || listFeed.isEmpty()) {
            return "";
        } else {
            return listFeed.get(listFeed.size() - 1).getBase64RowId();
        }
    }

    private void onLoadMore() {
        if (isLoading) {
            Log.i(TAG, "loaddata onLoadMore isLoading");
        } else {
            if (!noMoreFeed) {
                mLoadmoreFooterView.setVisibility(View.VISIBLE);
                loadData(getLastRowId());
            } else {
                Log.i(TAG, "loaddata onLoadMore nomorefeed");
//                listView.onLoadMoreComplete();
            }
        }
    }

    private void getDetailFollow() {
        //lay state follow
        ContactRequestHelper.getInstance(mApplication).getSocialDetail(mFriendJidNumber, new ContactRequestHelper
                .onFollowResponse() {
            @Override
            public void onResponse(int status, String rowId) {
                statusFollow = status;
                rowIdRequestSocial = rowId;
                drawButtonFollow();
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "getDetailFollow onError: " + errorCode);
            }
        });
    }

    private void showMenuUnFollow() {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu unFollowItem = new ItemContextMenu(mRes.getString(R.string
                .contact_follow_menu_un_follow), -1,
                null, Constants.MENU.MENU_CONTACT_UNFOLLOW);
        listMenu.add(unFollowItem);
        PopupHelper.getInstance().showContextMenu(mParentActivity,
                null, listMenu, (view, entry, menuId) -> {
                    String friendName = mTvwProfileName.getText().toString();
                    String msg = String.format(mRes.getString(R.string.msg_confirm_social_unfriend), friendName);
                    new DialogConfirm(mParentActivity, true).setLabel(null).setMessage(msg).setNegativeLabel(mRes
                            .getString(R.string.cancel)).
                            setPositiveLabel(mRes.getString(R.string.ok)).setPositiveListener(result -> processFollow()).show();
                });
    }

    private void processFollow() {
        mParentActivity.showLoadingDialog(null, R.string.waiting);
        String friendName = mTvwProfileName.getText().toString();
        ContactRequestHelper.getInstance(mApplication).requestSocialFriend(mFriendJidNumber, friendName,
                Constants.CONTACT.SOCIAL_SOURCE_MOCHA, statusFollow,
                rowIdRequestSocial, new ContactRequestHelper.onFollowResponse() {
                    @Override
                    public void onResponse(int status, String rowId) {
                        mParentActivity.hideLoadingDialog();
                        statusFollow = status;
                        rowIdRequestSocial = rowId;
                        drawButtonFollow();
                    }

                    @Override
                    public void onError(int errorCode) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void drawButtonFollow() {
        switch (statusFollow) {
            case Constants.CONTACT.FOLLOW_STATE_NONE:
                mTvwButtonProfile.setVisibility(View.VISIBLE);
                mTvwButtonProfile.setText(R.string.contact_follow_none);
//                setBackgroundButtonProfile();
                break;
            case Constants.CONTACT.FOLLOW_STATE_FOLLOWED:
                mTvwButtonProfile.setVisibility(View.VISIBLE);
                mTvwButtonProfile.setText(R.string.contact_follow_followed);
//                setBackgroundButtonProfile();
                break;
            case Constants.CONTACT.FOLLOW_STATE_BE_FOLLOWED:
                mTvwButtonProfile.setVisibility(View.VISIBLE);
                mTvwButtonProfile.setText(R.string.contact_follow_be_followed);
                setBackgroundButtonProfile();
                break;
            case Constants.CONTACT.FOLLOW_STATE_FRIEND:
                mTvwButtonProfile.setVisibility(View.VISIBLE);
                mTvwButtonProfile.setText(R.string.contact_follow_friend);
                setBackgroundButtonProfileFriend();
                break;
            default:
                mTvwButtonProfile.setVisibility(View.GONE);
                break;
        }
    }

    private void setBackgroundButtonProfile() {
        mTvwButtonProfile.setTextColor(ContextCompat.getColor(mApplication, R.color.white));
        mTvwButtonProfile.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 0);
        mTvwButtonProfile.setBackgroundColorAndPress(
                ContextCompat.getColor(mApplication, R.color.bg_mocha),
                ContextCompat.getColor(mApplication, R.color.bg_toast));
        mTvwButtonProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
    }

    private void setBackgroundButtonProfileFriend() {
        mTvwButtonProfile.setTextColor(ContextCompat.getColor(mApplication, R.color.v5_text));
        mTvwButtonProfile.setStroke(ContextCompat.getColor(mApplication, R.color.bg_mocha), 0);
        mTvwButtonProfile.setBackgroundColorAndPress(
                ContextCompat.getColor(mApplication, R.color.v5_pre_select),
                ContextCompat.getColor(mApplication, R.color.bg_toast));
        mTvwButtonProfile.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_v5_check), null, null, null);
        mTvwButtonProfile.setCompoundDrawablePadding(Utilities.dpToPx(5));
    }

    private void showPopupZodiac(String percent, String content) {
        Object obj;
        if (mNonContact != null) {
            obj = mNonContact;
        } else if (mStrangerPhoneNumber != null) {
            obj = mStrangerPhoneNumber;
        } else {
            obj = mFriendJidNumber;
        }
        PopupZodiac popupZodiac = new PopupZodiac(mParentActivity, obj, percent, content, new PopupZodiac.ZodiacListener() {
            @Override
            public void onShare(Bitmap bitmap) {
                Log.d(TAG, "onShare-");
                mParentActivity.shareImageFacebook(bitmap, mRes.getString(R.string.ga_facebook_label_share_zodiac));
            }

            @Override
            public void onSend(String filePath) {
                Log.d(TAG, "onSend-");
                ArrayList<String> filePaths = new ArrayList<>();
                filePaths.add(filePath);
                ThreadMessage threadMessage;
                if (mStrangerPhoneNumber != null) {
                    threadMessage = mApplication.getMessageBusiness().findExistingOrCreateNewThread
                            (mStrangerPhoneNumber.getPhoneNumber());
                } else {// tao moi stranger number va tao thread message
                    threadMessage = mApplication.getStrangerBusiness().createMochaStrangerAndThread(mFriendJidNumber,
                            mFriendName, mFriendChangeAvatar, Constants.CONTACT.STRANGER_MOCHA_ID, true);
                }
                mApplication.getMessageBusiness().createAndSendMessageImage(mParentActivity, threadMessage,
                        filePaths, true);
                mListener.navigateToThreadDetail(threadMessage);
            }

            @Override
            public void onError() {
                mParentActivity.showToast(R.string.file_not_found_exception);
            }
        });

        if (!popupZodiac.isShowing())
            popupZodiac.show();
    }

    @Override
    public void onChannelCreate(Channel channel) {

    }

    @Override
    public void onChannelUpdate(Channel channel) {

    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
        if (channel != null && mFeedAdapter != null) {
            boolean notifyAdapter = false;
            int size = mFeedAdapter.getItemCount();
            for (int i = 0; i < size; i++) {
                if (mFeedAdapter.getItem(i) instanceof FeedModelOnMedia) {
                    FeedModelOnMedia item = (FeedModelOnMedia) mFeedAdapter.getItem(i);
                    FeedContent feedContent = item.getFeedContent();
                    if (feedContent != null && feedContent.getChannel() != null
                            && !TextUtils.isEmpty(feedContent.getChannel().getId())
                            && feedContent.getChannel().getId().equals(channel.getId())) {
                        feedContent.getChannel().setFollow(channel.isFollow());
                        feedContent.getChannel().setNumFollow(channel.getNumFollow());
                        notifyAdapter = true;
                    }
                }
            }
            if (notifyAdapter) mFeedAdapter.notifyDataSetChanged();
        }
    }

}
package com.metfone.selfcare.fragment.guestbook;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.adapter.guestbook.TemplateDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.TemplateDetail;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookTemplateDetailFragment extends BaseRecyclerViewFragment implements
        BaseRecyclerViewFragment.EmptyViewListener,
        GuestBookHelper.GetTemplateDetailListener,
        View.OnClickListener {
    private static final String TAG = GuestBookMainFragment.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private GuestBookActivity mParentActivity;
    private GuestBookHelper mGuestBookHelper;
    private EllipsisTextView mAbTitle;
    private OnFragmentInteractionListener mListener;
    private View mViewHeader;
    private TextView mTvwHeader;
    private int templateId;
    private String templateName;
    private RecyclerView mRecyclerView;
    private TemplateDetailAdapter mAdapter;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private TemplateDetail templateDetail;
    private Book newBook;

    public GuestBookTemplateDetailFragment() {

    }

    public static GuestBookTemplateDetailFragment newInstance(int templateId, String templateName) {
        GuestBookTemplateDetailFragment fragment = new GuestBookTemplateDetailFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.GUEST_BOOK.TEMPLATE_ID, templateId);
        args.putString(Constants.GUEST_BOOK.TEMPLATE_NAME, templateName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (GuestBookActivity) activity;
        mApplication = (ApplicationController) activity.getApplicationContext();
        mRes = mApplication.getResources();
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            Log.e(TAG, "ClassCastException", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycle_view, container, false);
        getData(savedInstanceState);
        setToolbar(inflater);
        findComponentViews(rootView, container, inflater);
        drawDetail();
        setViewListener();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.GUEST_BOOK.TEMPLATE_ID, templateId);
        outState.putString(Constants.GUEST_BOOK.TEMPLATE_NAME, templateName);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                mParentActivity.onBackPressed();
                break;
            case R.id.ab_agree_text:
                mParentActivity.showLoadingDialog(null, R.string.waiting);
                newBook = mGuestBookHelper.createNewBook(templateDetail);
                mGuestBookHelper.requestSaveNewBook(newBook, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mGuestBookHelper.setNeedLoadMyBooks(true);
                        mListener.navigateToBookDetail(newBook, true);
                    }

                    @Override
                    public void onError(String desc) {
                        mParentActivity.hideLoadingDialog();
                        mParentActivity.showToast(desc, Toast.LENGTH_LONG);
                    }
                });
                break;
        }
    }

    @Override
    public void onSuccess(TemplateDetail templateDetail) {
        hideEmptyView();
        this.templateDetail = templateDetail;
        setAdapter();
    }

    @Override
    public void onError(int error) {
        showEmptyNote(mRes.getString(R.string.request_send_error));
    }

    private void setToolbar(LayoutInflater inflater) {
        mParentActivity.setCustomViewToolBar(inflater.inflate(R.layout.ab_detail, null));
        View abView = mParentActivity.getToolBarView();
        mAbTitle = (EllipsisTextView) abView.findViewById(R.id.ab_title);
        ImageView mAbBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        TextView mAbCreate = (TextView) abView.findViewById(R.id.ab_agree_text);
        View moreOption = abView.findViewById(R.id.ab_more_btn);
        mAbCreate.setText(mRes.getString(R.string.guest_book_create));
        mAbBack.setOnClickListener(this);
        mAbCreate.setOnClickListener(this);
        mAbCreate.setVisibility(View.VISIBLE);
        moreOption.setVisibility(View.GONE);
    }

    private void getData(Bundle savedInstanceState) {
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        if (savedInstanceState != null) {
            templateId = savedInstanceState.getInt(Constants.GUEST_BOOK.TEMPLATE_ID);
            templateName = savedInstanceState.getString(Constants.GUEST_BOOK.TEMPLATE_NAME);
        } else if (getArguments() != null) {
            templateId = getArguments().getInt(Constants.GUEST_BOOK.TEMPLATE_ID, 0);
            templateName = getArguments().getString(Constants.GUEST_BOOK.TEMPLATE_NAME);
        }
    }

    private void findComponentViews(View rootView, ViewGroup container, LayoutInflater inflater) {
        mViewHeader = inflater.inflate(R.layout.header_guest_book_label, container, false);
        mTvwHeader = (TextView) mViewHeader.findViewById(R.id.guest_book_header_label);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        createView(inflater, mRecyclerView, this);
        hideEmptyView();
    }

    private void drawDetail() {
        mParentActivity.setBannerType(Constants.GUEST_BOOK.BANNER_TYPE_NON);
        mAbTitle.setText(templateName);
        mTvwHeader.setText(mRes.getString(R.string.guest_book_header_temp));
        if (templateDetail == null) {
            showProgressLoading();
            mGuestBookHelper.requestTemplateDetail(templateId, this);
        } else {
            hideEmptyView();
            setAdapter();
        }
    }

    private void setViewListener() {
    }

    private void setAdapter() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mApplication));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new TemplateDetailAdapter(mApplication, templateDetail.getPages());
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        mRecyclerView.setAdapter(mHeaderAndFooterAdapter);
        RecyclerViewUtils.setHeaderView(mRecyclerView, mViewHeader);
        setItemListViewListener();
    }

    private void setItemListViewListener() {
        // onclick
       /* mAdapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                //mListener.navigateToTemplateDetail();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });*/
        // long click
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
    }

    public interface OnFragmentInteractionListener {
        void navigateToBookDetail(Book book, boolean isNew);
    }
}

package com.metfone.selfcare.fragment.contact;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.metfone.selfcare.business.StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED;
import static com.metfone.selfcare.business.StrangerBusiness.TYPE_SOCIAL_SUGGEST;

/**
 * Created by thanhnt72 on 12/3/2019.
 */

public class SocialFriendFragment extends Fragment {

    private static final String TAG = SocialFriendFragment.class.getSimpleName();
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    Unbinder unbinder;
    private Resources mRes;
    private ContactListActivity activity;


    public static SocialFriendFragment newInstance() {
        SocialFriendFragment fragment = new SocialFriendFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        activity = (ContactListActivity) getActivity();
        if (activity == null) return;
        mRes = activity.getResources();
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_social_friend, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initAdapter();
        activity.getToolBarView().setVisibility(View.GONE);
        return rootView;
    }

    private void initAdapter() {
        viewPager.setAdapter(createAdapterPage());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(viewPager);
    }

    private PagerAdapter createAdapterPage() {
        FragmentPagerItems.Creator creator = new FragmentPagerItems.Creator(activity);
        creator.add(FragmentPagerItem.of(mRes.getString(R.string.social_tab_be_followed),
                SocialFriendRequestFragment.class, SocialFriendRequestFragment.arguments(TYPE_SOCIAL_BE_FOLLOWED)));
        creator.add(FragmentPagerItem.of(mRes.getString(R.string.title_suggest_friend),
                SocialFriendRequestFragment.class, SocialFriendRequestFragment.arguments(TYPE_SOCIAL_SUGGEST)));
        return new FragmentStatePagerItemAdapter(getChildFragmentManager(), creator.create());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ivBack, R.id.ivSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                activity.onBackPressed();
                break;
            case R.id.ivSearch:
                //TODO search
                break;
        }
    }
}

package com.metfone.selfcare.fragment.setting;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.adapter.EmoticonsGridAdapter;
import com.metfone.selfcare.adapter.EmoticonsPagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonManager;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.viewpagerindicator.TabPageIndicator;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 10/20/14.
 */
public class EditStatusFragment extends Fragment implements EmoticonsGridAdapter.KeyClickListener {
    private static final String TAG = EditStatusFragment.class.getSimpleName();
    private SettingActivity mParentActivity;
    private Resources mRes;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private View rootView;
//    private ImageView mImgEmoticon;
    private TextView mTvwPost, mTvwStatusCounter, tvBack, tvName;
    private EditText mEdtContent;
    private RoundedImageView mAvatar;
    //    private Button mBtnUpdate;
//    private String statusChanged;
//    private int statusPosition;
    private PopupWindow mEmoPopupWindow;
    private View mEmoPopupView;
    private int keyboardHeight;
    private int keyboardHeightTmp;
    private int previousHeightDiffrence = 0;
    private ImageView emoticonDelete;
    private ImageButton stickerStore;
    private boolean isSaveInstanceState = false;
    private boolean isUpdateSuccess = false;
    private DisplayMetrics displayMetrics;

    public static EditStatusFragment newInstance() {
        EditStatusFragment fragment = new EditStatusFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mParentActivity = (SettingActivity) activity;
        mRes = mParentActivity.getResources();
        mApplication = (ApplicationController) mParentActivity.getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        keyboardHeightTmp = (int) mRes.getDimension(R.dimen.keyboard_height);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_edit_status, container, false);
        findComponentViews(rootView, inflater, container);
        drawDetail();
        setListener();
        return rootView;
    }

    @Override
    public void onResume() {
        //show keyboad
        mEdtContent.clearFocus();
        super.onResume();
        if (isSaveInstanceState && isUpdateSuccess) {
            isUpdateSuccess = false;
            isSaveInstanceState = false;
            mParentActivity.showToast(mRes.getString(R.string.update_info_ok), Toast.LENGTH_SHORT);
            InputMethodUtils.hideSoftKeyboard(mEdtContent, mParentActivity);
            mParentActivity.finish();
        } else {
            InputMethodUtils.showSoftKeyboard(mParentActivity, mEdtContent);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        isSaveInstanceState = true;
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        if (mEmoPopupWindow != null && mEmoPopupWindow.isShowing()) {
            mEmoPopupWindow.dismiss();
        }
        super.onStop();
    }

    @Override
    public void emoClicked(String emoPath, String emoText, int emoIndex) {
        Spanned s = EmoticonUtils.getSpanned(mParentActivity.getApplicationContext(),
                emoText, mEdtContent.getTextSize());
        int cursorPosition = mEdtContent.getSelectionStart();
        mEdtContent.getText().insert(cursorPosition, s);
    }

    @Override
    public void stickerClicked(StickerItem stickerItem) {

    }

    private void findComponentViews(View view, LayoutInflater inflater, ViewGroup container) {
//        mActionBar = mParentActivity.getSupportActionBar();
//        mActionBarView = inflater.inflate(R.layout.ab_detail, null);
//        mActionBar.setCustomView(mActionBarView);

        tvBack = view.findViewById(R.id.tvBack);
        mTvwPost = view.findViewById(R.id.tvPost);

        tvBack.setPaintFlags(tvBack.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvwPost.setPaintFlags(mTvwPost.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mEdtContent = view.findViewById(R.id.edit_status_content);
//        mImgEmoticon = view.findViewById(R.id.edit_status_emoticon_img);
        mTvwStatusCounter = view.findViewById(R.id.edit_status_counter);
//        mBtnUpdate = (Button) view.findViewById(R.id.edit_status_update);
        mEmoPopupView = inflater.inflate(R.layout.popup_chat_detail_emoticons, container, false);
        emoticonDelete = mEmoPopupView.findViewById(R.id.emoticon_delete);
        stickerStore = mEmoPopupView.findViewById(R.id.sticker_more);
        mAvatar = view.findViewById(R.id.profile_avatar_circle);
        tvName = view.findViewById(R.id.tv_name);
//        emoticonDelete.setVisibility(View.VISIBLE);
//        emoticonDelete.setVisibility(View.GONE);
        stickerStore.setVisibility(View.GONE);
        mEmoPopupView.findViewById(R.id.layout_emoticon_control).setVisibility(View.GONE);
        //
        mTvwPost.setVisibility(View.VISIBLE);
        mTvwPost.setTextColor(ContextCompat.getColorStateList(mApplication, R.color.text_color_purple));
        mTvwPost.setAllCaps(true);
        mTvwPost.setTypeface(mTvwPost.getTypeface(), Typeface.BOLD);
        setHideKeboadWhenTouch(view);
        keyboardHeight = InputMethodUtils.getIntPreferenceHeightPref(mParentActivity,
                Constants.PREFERENCE.PREF_KEYBOARD_HEIGHT, keyboardHeightTmp);
    }

    private void setListener() {
        setEmoticonFooterContent();
        checkKeyboardHeight();
        setBackListener();
        setButtonPostListener();
        setStatusContentListener();
        setButtonEmoticonListener();
    }

    private void drawDetail() {
        mTvwPost.setText(mRes.getString(R.string.post));
//        mBtnUpdate.setEnabled(false);
        mTvwPost.setEnabled(false);
        ReengAccount mAccount = mAccountBusiness.getCurrentAccount();
        if (mAccount != null) {
            String status = mAccount.getStatus();
            if (status != null) {
                setInputEmoTextView(status, 0);
                mEdtContent.requestFocus();
            }
            drawStatusCounter(status);
        }
        mApplication.getAvatarBusiness().setMyAvatar(mAvatar,
                null, null, mApplication.getReengAccountBusiness().getCurrentAccount(), null);
        tvName.setText(mAccount.getName());
    }

    private void drawStatusCounter(String status) {
        int current;
        if (TextUtils.isEmpty(status)) {
            current = 0;
        } else {
            current = status.length();
        }
        mTvwStatusCounter.setText(String.format(mRes.getString(R.string.status_counter), current, Constants.CONTACT
                .STATUS_LIMIT));
    }

    private void setBackListener() {
        tvBack.setOnClickListener(view -> mParentActivity.onBackPressed());
    }

    private void setButtonPostListener() {
        mTvwPost.setOnClickListener(view -> updateStatus());
    }

    private void setStatusContentListener() {
        mEdtContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //String status = EmoticonUtils.getRawTextFromSpan(mEdtContent.getText()).trim();
                //if (status.length() > Constants.CONTACT.STATUS_LIMIT) {
                //setInputEmoTextView(statusChanged, statusPosition);
                //}
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                String status = EmoticonUtils.getRawTextFromSpan(mEdtContent.getText());
//                statusChanged = status;
//                statusPosition = start + count;
            }

            @Override
            public void afterTextChanged(Editable s) {
                String status = EmoticonUtils.getRawTextFromSpan(mEdtContent.getText()).trim();
                ReengAccount mReengAccount = mAccountBusiness.getCurrentAccount();
                if (mReengAccount == null) return;
                String oldStr = mReengAccount.getStatus();
                // if ((status == null && oldStr == null) || (status != null && oldStr != null && status.equals
                // (oldStr))) {
                if ((TextUtils.isEmpty(status) && TextUtils.isEmpty(oldStr)) ||
                        (status.length() > Constants.CONTACT.STATUS_LIMIT) ||
                        (status.equals(oldStr))) {
                    mTvwPost.setEnabled(false);
                } else {
                    mTvwPost.setEnabled(true);
                }
                drawStatusCounter(status);
            }
        });
//        mEdtContent.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                mEdtContent.clearFocus();
//                return false;
//            }
//        });
        // lan dau tien click vao edit text thi ko nhay vao onclick ma vao
        mEdtContent.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (mEmoPopupWindow.isShowing()) {
                    mEmoPopupWindow.dismiss();
                }
                InputMethodUtils.showSoftKeyboard(mParentActivity, mEdtContent);
//                mImgEmoticon.setImageResource(R.drawable.ic_emoticon);
            }
        });

        mEdtContent.setOnClickListener(v -> {
            if (mEmoPopupWindow.isShowing()) {
                mEmoPopupWindow.dismiss();
            }
            InputMethodUtils.showSoftKeyboard(mParentActivity, mEdtContent);
//            mImgEmoticon.setImageResource(R.drawable.ic_emoticon);
        });
    }

    private void setButtonEmoticonListener() {
//        mImgEmoticon.setOnClickListener(view -> {
//            if (!mEmoPopupWindow.isShowing()) {
//                mEdtContent.requestFocus();
//                mEmoPopupWindow.setHeight(keyboardHeight);
//                mEmoPopupWindow.showAtLocation(rootView, Gravity.BOTTOM, 0, 0);
//                // showing emo -> set icon to text
//                mImgEmoticon.setImageResource(R.drawable.ic_keyboard);
//            } else {
//                mEdtContent.requestFocus();
//                InputMethodUtils.showSoftKeyboard(mParentActivity, mEdtContent);
//                // neu click vao emoticon button ma dang show emoticon grid
//                // thi show ban phim
//                mEmoPopupWindow.dismiss();
//                mImgEmoticon.setImageResource(R.drawable.ic_emoticon);
//            }
//        });
    }

    private void setInputEmoTextView(String status, int pos) {
        Html.ImageGetter imageGetter = EmoticonManager.getInstance(mParentActivity)
                .getImageGetter();
        String content = TextHelper.getInstant().escapeXml(status);
        content = EmoticonUtils.emoTextToTag(content);
        Spanned spanned = TextHelper.fromHtml(content, imageGetter, null);
        mEdtContent.setText(spanned);
        int length = mEdtContent.getText().length();
        if (pos <= 0 || pos > length) {
            mEdtContent.setSelection(length);
        } else {
            mEdtContent.setSelection(pos);
        }
    }

    private void updateStatus() {
        // XMPPManager mXMPPManager = XMPPManager.getInstance(mApplication);
        String content = EmoticonUtils.getRawTextFromSpan(mEdtContent.getText()).trim();
        content = TextHelper.trimTextOnMedia(content);
        if (!NetworkHelper.isConnectInternet(mParentActivity)) {
            mParentActivity.showToast(mRes.getString(R.string.error_internet_disconnect),
                    Toast.LENGTH_SHORT);
            return;
        }
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        mParentActivity.showLoadingDialog("", R.string.processing);
        ProfileRequestHelper.onResponseChangeStatusListener listener = new ProfileRequestHelper
                .onResponseChangeStatusListener() {
            @Override
            public void onResponse() {
                mParentActivity.hideLoadingDialog();
                if (isSaveInstanceState) {
                    isUpdateSuccess = true;
                } else {
                    mParentActivity.showToast(mRes.getString(R.string.update_info_ok), Toast.LENGTH_SHORT);
                    InputMethodUtils.hideSoftKeyboard(mEdtContent, mParentActivity);
                    mParentActivity.onBackPressed();
                }
                LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL.ITEM_STATUS);
            }

            @Override
            public void onError(int errorCode) {
                Log.d(TAG, "onError: " + errorCode);
                mParentActivity.hideLoadingDialog();
                mParentActivity.showToast(mRes.getString(R.string.e490_illegal_state_exception),
                        Toast.LENGTH_SHORT);
            }
        };
        ProfileRequestHelper.getInstance(mApplication).setStatus(account, content, listener);
    }

    private void setEmoticonFooterContent() {
        ViewPager pager = mEmoPopupView
                .findViewById(R.id.chat_detail_emoticons_pager);
        TabPageIndicator mIndicator = mEmoPopupView.findViewById(R.id.indicator);
        mIndicator.setVisibility(View.GONE);
        /*TextView emoticonDone = (TextView) mEmoPopupView
                .findViewById(R.id.emoticon_done);*/
        /*ArrayList<String> paths = new ArrayList<String>();
        for (short i = 1; i <= EmoticonUtils.NO_OF_EMOTICONS; i++) {
            paths.add(EmoticonUtils.EMOTICON_KEYS[i - 1] + ".png");
        }*/
        /*ArrayList<Integer> icons = new ArrayList<Integer>();
        icons.add(R.drawable.ic_emoticon);*/
        EmoticonsPagerAdapter emoticonAdapter = new EmoticonsPagerAdapter(
                mParentActivity, this, null, false, null, mEdtContent);
        pager.setAdapter(emoticonAdapter);
//        mIndicator.setActivity(mParentActivity);
//        mIndicator.setViewPager(pager);
        // Creating a pop window for emoticons keyboard
        if (mEmoPopupWindow == null)
            mEmoPopupWindow = new PopupWindow(mEmoPopupView,
                    ViewGroup.LayoutParams.MATCH_PARENT, keyboardHeight, false);
        mEmoPopupWindow.dismiss();
//        emoticonDone.setVisibility(View.GONE);
        emoticonDelete.setOnClickListener(v -> {
            KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                    0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
            mEdtContent.dispatchKeyEvent(event);
        });
        emoticonDelete.setOnLongClickListener(v -> {
            KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                    0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
            mEdtContent.dispatchKeyEvent(event);
            return true;
        });
    }

    private void checkKeyboardHeight() {
        displayMetrics = new DisplayMetrics();
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(
                () -> {
                    Rect r = new Rect();
                    rootView.getWindowVisibleDisplayFrame(r);
                    WindowManager wm = (WindowManager) mParentActivity.getSystemService(Context.WINDOW_SERVICE);
                    wm.getDefaultDisplay().getMetrics(displayMetrics);
                    int screenHeight = displayMetrics.heightPixels;
                    int heightDifference = screenHeight - (r.bottom);
                    if (previousHeightDiffrence - heightDifference > 50) {
                        mEmoPopupWindow.dismiss();
                    }
                    previousHeightDiffrence = heightDifference;
                    if (heightDifference > 100) {
                        keyboardHeight = heightDifference;
                        InputMethodUtils.setIntPreferenceHeightPref(mParentActivity,
                                Constants.PREFERENCE.PREF_KEYBOARD_HEIGHT,
                                keyboardHeight);
                    }
                }
        );
    }

    private void setHideKeboadWhenTouch(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                InputMethodUtils.hideSoftKeyboard(mParentActivity);
                if (mEmoPopupWindow != null
                        && mEmoPopupWindow.isShowing()) {
                    mEmoPopupWindow.dismiss();
                }
                return false;
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setHideKeboadWhenTouch(innerView);
            }
        }
    }

}
package com.metfone.selfcare.helper.ads;

import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.vtm.adslib.template.TemplateView;

public class AdsNativeHolder extends BaseAdapter.ViewHolder {
    private TemplateView adContainer;

    public AdsNativeHolder(View itemView) {
        super(itemView);
        adContainer = itemView.findViewById(R.id.adContainer);
    }

    @Override
    public void bindData(Object item, int position) {
        if (adContainer != null) {
            AdsManager.getInstance().showAdsNative(adContainer);
        }
    }
}

package com.metfone.selfcare.helper.message;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.holder.message.BaseMessageHolder;
import com.metfone.selfcare.util.Log;

import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by vtsoft on 12/3/2014.
 */
public class CountDownInviteManager {
    public static final int COUNT_DOWN_DURATION = 119 * 1000;
    private static final String TAG = CountDownInviteManager.class.getSimpleName();
    private static final int COUNT_DOWN_PERIOD = 1000;
    private static CountDownInviteManager mInstance;
    private ApplicationController mApplication;
    private Timer mCountDownTimer;
    private TimerTask mTimeoutTimerTask;
    private ConcurrentHashMap<BaseMessageHolder, Integer> mapHolderMessage = new ConcurrentHashMap<>();
    private LinkedList<ReengMessage> listMessageCounting = new LinkedList<>();
    private Handler mHandler;


   /* private static Handler handler;
    private static ConcurrentHashMap<Integer, View> mapMessageIdWithConvertView = new ConcurrentHashMap<Integer, View>();
    private static ConcurrentHashMap<View, Integer> mapConvertViewWithMessageId = new ConcurrentHashMap<View, Integer>();*/

    public synchronized static CountDownInviteManager getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new CountDownInviteManager(application);
        }
        return mInstance;
    }

    private CountDownInviteManager(ApplicationController application) {
        this.mApplication = application;
        this.mapHolderMessage = new ConcurrentHashMap<>();
        this.listMessageCounting = new LinkedList<>();
    }

    public void destroyFragment() {
        this.mapHolderMessage = new ConcurrentHashMap<>();
        //this.listMessageCounting = new LinkedList<>();
        //this.mHandler = null;
    }

    // save current holder (listenerTogether) visible
    public void addHolderMessage(BaseMessageHolder holder, int messageId) {
        mapHolderMessage.put(holder, messageId);
    }

    public void startCountDownMessage(ReengMessage message) {
        Log.d(TAG, "startCountDownMessage");
        message.setCounting(true);
        listMessageCounting.add(message);
        if (mCountDownTimer == null) {
            startTimer();
        }
    }

    public void stopCountDownMessage(ReengMessage message) {
        Log.d(TAG, "stopCountDownMessage");
        message.setCounting(false);
        listMessageCounting.remove(message);
        if (listMessageCounting.isEmpty()) {
            stopTimer();
        }
    }

    /*    public static synchronized void initHandler() {
        handler = new Handler(Looper.getMainLooper());
    }*/

    private void processTimerTask() {
        long t = System.currentTimeMillis();
        if (listMessageCounting.isEmpty()) {
            Log.d(TAG, "listMessageCounting empty ->>  force stop timer");
            stopTimer();
        } else {
            LinkedList<ReengMessage> listTimeout = new LinkedList<>();
            for (ReengMessage message : listMessageCounting) {
                int duration = message.getDuration() - COUNT_DOWN_PERIOD;
                if (duration <= 0) {
                    mApplication.getMessageBusiness().updateSendInviteMessageTimeout(message);
                    listTimeout.add(message);
                } else {
                    message.setDuration(duration);
                    updateHolderMessage(message);
                }
            }
            if (!listTimeout.isEmpty()) {
                listMessageCounting.removeAll(listTimeout);
            }
        }
        Log.d(TAG, "processTimerTask take: " + (System.currentTimeMillis() - t));
    }

    private void updateHolderMessage(final ReengMessage message) {
        Log.d(TAG, "updateHolderMessage : " + message.getId());
        if (mapHolderMessage.isEmpty()) {
            Log.d(TAG, "holderMessages empty");
            return;
        }
        for (Map.Entry<BaseMessageHolder, Integer> entry : mapHolderMessage.entrySet()) {
            if (entry.getValue() == message.getId()) {
                BaseMessageHolder messageHolder = entry.getKey();
                if (mHandler == null)
                    mHandler = new Handler(Looper.getMainLooper());
                final View convertView = messageHolder.getConvertView();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (convertView != null) {
                            TextView elapsedTime = (TextView) convertView.findViewById(R.id.count_down_time);
                            RelativeLayout actionLayout = (RelativeLayout) convertView.findViewById(R.id.layout_user_action_music);
                            RelativeLayout inviteLayout = (RelativeLayout) convertView.findViewById(R.id.layout_reinvite_music);
                            elapsedTime.setText(StopWatch.convertSecondInMMSSForm(true, message.getDuration() / 1000));
                            if (message.getDirection() == ReengMessageConstant.Direction.send) {
                                elapsedTime.setVisibility(View.INVISIBLE);
                            } else {
                                elapsedTime.setVisibility(View.VISIBLE);
                            }
                            if (message.getDuration() <= 0) {
                                actionLayout.setVisibility(View.GONE);
                                inviteLayout.setVisibility(View.VISIBLE);
                            } else {
                                actionLayout.setVisibility(View.VISIBLE);
                                inviteLayout.setVisibility(View.GONE);
                            }
                        }
                    }
                });
                break;
            }
        }
    }

    private void startTimer() {
        stopTimer();
        mCountDownTimer = new Timer();
        mTimeoutTimerTask = new TimerTask() {
            @Override
            public void run() {
                processTimerTask();
            }
        };
        mCountDownTimer.schedule(mTimeoutTimerTask, 0, COUNT_DOWN_PERIOD);
    }

    private void stopTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }
}
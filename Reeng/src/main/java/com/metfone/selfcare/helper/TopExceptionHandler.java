package com.metfone.selfcare.helper;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by thanhnt72 on 5/16/2018.
 */

public class TopExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final String TAG = TopExceptionHandler.class.getSimpleName();

    private Thread.UncaughtExceptionHandler defaultUEH;
    private ApplicationController app;

    public TopExceptionHandler(ApplicationController app) {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        this.app = app;
    }

    public void uncaughtException(Thread t, Throwable e) {
        StackTraceElement[] arr = e.getStackTrace();
        String report = e.toString() + "\n";
        report += "--------- Stack trace ---------\n";
        for (int i = 0; i < arr.length; i++) {
            report += "    " + arr[i].toString() + "\n";
        }
        report += "-------------------------------\n";

        // If the exception was thrown in a background thread inside
        // AsyncTask, then the actual exception can be found with getCause

        report += "--------- Cause ---------\n";
        Throwable cause = e.getCause();
        if (cause != null) {
            report += cause.toString() + "\n";
            arr = cause.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                report += "    " + arr[i].toString() + "\n";
            }
        }
        report += "-------------------------------\n";
        if (app != null) {
            Log.e(TAG, "report: " + report);
            app.logDebugContent(report);
        }
        defaultUEH.uncaughtException(t, e);
    }

    public String getAppVersion(Context con) {
        PackageManager manager = con.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(con.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("" + con.getPackageName(), "Name not found Exception");
        }
        return info.versionName;
    }

    public boolean isTablet(Context con) {
        boolean xlarge = ((con.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((con.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return xlarge || large;
    }

    @SuppressWarnings("deprecation")
    public boolean isConnectingToInternet(Context act) {
        boolean isthere = false;
        TelephonyManager tm = (TelephonyManager) act.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getSimState() != TelephonyManager.SIM_STATE_UNKNOWN) {
            ConnectivityManager connectivityManager = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
            if ((connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED))
                isthere = true;
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
            if ((connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED))
                isthere = true;
        }
        return isthere;
    }

    @SuppressWarnings("deprecation")
    public String getScreenOrientation(Context act) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) act.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        if (displaymetrics.widthPixels == displaymetrics.heightPixels) {
            return "Square";
        } else {
            if (displaymetrics.widthPixels < displaymetrics.heightPixels) {
                return "Portrait";
            } else {
                return "Landscape";
            }
        }
    }

    public String getScreenLayout(Context act) {
        int screenSize = act.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "Large Screen";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "Normal Screen";
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "Small Screen";
            default:
                return "Screen size is neither large, normal or small";
        }
    }

    public String ConvertSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public String getBatteryStatus(Context act) {
        int status = act.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED)).getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        if (status == BatteryManager.BATTERY_STATUS_CHARGING)
            return "Charging";
        else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING)
            return "Discharging";
        else if (status == BatteryManager.BATTERY_STATUS_FULL)
            return "Full";
        return "NULL";
    }

    public String getBatteryChargingMode(Context act) {
        int plugged = act.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED)).getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        if (plugged == BatteryManager.BATTERY_PLUGGED_AC)
            return "AC";
        else if (plugged == BatteryManager.BATTERY_PLUGGED_USB)
            return "USB";
        else if (plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS)
            return "WireLess";
        return "NULL";
    }

    public String getSDCardStatus(Context act) {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (isSDPresent)
            return "Mounted";
        else
            return "Not mounted";
    }

    @SuppressWarnings("deprecation")
    public String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return ConvertSize(availableBlocks * blockSize);
    }

    @SuppressWarnings("deprecation")
    public String getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return ConvertSize(totalBlocks * blockSize);
    }

    @SuppressWarnings("deprecation")
    public String getAvailableExternalMemorySize() {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return ConvertSize(availableBlocks * blockSize);
        } else {
            return "SDCard not present";
        }
    }

    @SuppressWarnings("deprecation")
    public String getTotalExternalMemorySize() {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return ConvertSize(totalBlocks * blockSize);
        } else {
            return "SDCard not present";
        }
    }

    public boolean isRooted() {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
                    "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
            for (String where : places) {
                if (new File(where + "su").exists()) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    @SuppressWarnings("deprecation")
    public String getNetworkMode(Context act) {
        ConnectivityManager connMgr = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            return "Wifi";
        } else if (mobile.isConnectedOrConnecting()) {
            return mobile.getSubtypeName();
        } else {
            return "No Network";
        }
    }

    public boolean isSimSupport(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT);

    }
}

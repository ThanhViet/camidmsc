package com.metfone.selfcare.helper.message;

/**
 * Created by toanvk2 on 5/21/2016.
 */
public class SmsReceivedObject {
    private String from;
    private String content;

    public SmsReceivedObject(String from, String content) {
        this.content = content;
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public String getContent() {
        return content;
    }
}

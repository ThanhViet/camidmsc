package com.metfone.selfcare.helper.images;

/**
 * Created by huybq on 11/10/2014.
 */
public class ImageInfo implements Comparable<ImageInfo> {
    private String imagePath;
    private boolean selected = false;
    private long fileSize = 0L;
    private int durationInSecond = 0;
    private String videoContentURI;
    private long dateCreate = 0L;
    private boolean isVideo = false;

    private int indexSelect;

    public void setIsVideo(boolean isVideo) {
        this.isVideo = isVideo;
    }

    private String resolution;

    public ImageInfo() {
    }

    public ImageInfo(String imagePath) {
        this.imagePath = imagePath;
    }

    public ImageInfo(String imagePath, String videoContentURI) {
        this.imagePath = imagePath;
        this.videoContentURI = videoContentURI;
    }

    public ImageInfo(String imagePath, long dateCreate) {
        this.imagePath = imagePath;
        this.dateCreate = dateCreate;
    }

    public ImageInfo(String imagePath, long fileSize, int durationInSecond, String videoContentURI, long dateCreate) {
        this.imagePath = imagePath;
        this.fileSize = fileSize;
        this.durationInSecond = durationInSecond;
        this.videoContentURI = videoContentURI;
        this.dateCreate = dateCreate;
        isVideo = true;
    }

    public ImageInfo(String imagePath, long fileSize, int durationInSecond, String videoContentURI, long dateCreate, String resolution) {
        this.imagePath = imagePath;
        this.fileSize = fileSize;
        this.durationInSecond = durationInSecond;
        this.videoContentURI = videoContentURI;
        this.dateCreate = dateCreate;
        this.resolution = resolution;
        isVideo = true;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setVideoContentURI(String videoContentURI) {
        this.videoContentURI = videoContentURI;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getDurationInSecond() {
        return durationInSecond;
    }

    public void setDurationInSecond(int durationInSecond) {
        this.durationInSecond = durationInSecond;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getVideoContentURI() {
        return videoContentURI;
    }

    public long getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(long dateCreate) {
        this.dateCreate = dateCreate;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public int getIndexSelect() {
        return indexSelect;
    }

    public void setIndexSelect(int indexSelect) {
        this.indexSelect = indexSelect;
    }

    @Override
    public String toString() {
        return "ImageInfo{" +
                "imagePath='" + imagePath + '\'' +
                ", selected=" + selected +
                ", fileSize=" + fileSize +
                ", durationInSecond=" + durationInSecond +
                ", videoContentURI='" + videoContentURI + '\'' +
                ", dateCreate=" + dateCreate +
                '}';
    }

    @Override
    public int compareTo(ImageInfo mediaFile) {
        return Long.valueOf(mediaFile.dateCreate).compareTo(Long.valueOf(this.dateCreate));
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
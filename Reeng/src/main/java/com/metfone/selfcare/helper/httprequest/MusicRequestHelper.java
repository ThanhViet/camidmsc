package com.metfone.selfcare.helper.httprequest;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 9/30/2016.
 */

public class MusicRequestHelper {
    private static final String TAG = MusicRequestHelper.class.getSimpleName();
    public static final String TAG_SONG = "GET_SONG";
    public static final String TAG_ALBUM = "GET_ALBUM";
    private static MusicRequestHelper mInstance;
    private ApplicationController mApplication;
//    private Resources mRes;

    public static synchronized MusicRequestHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new MusicRequestHelper(application);
        }
        return mInstance;
    }

    private MusicRequestHelper(ApplicationController application) {
        this.mApplication = application;
//        this.mRes = mApplication.getResources();
    }

    public void getMediaDetail(long id, String identify, final GetSongListener listener, final String tag) {
        String url = "";
        if (TAG_SONG.equals(tag)) {
            if (id > 0)
                url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfServiceKeeng(Config.UrlKeengEnum.SERVICE_GET_SONG) + "?id=" + id;
            else if (!TextUtils.isEmpty(identify))
                url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfServiceKeeng(Config.UrlKeengEnum.SERVICE_GET_SONG) + "?identify=" + HttpHelper.EncoderUrl(identify);

        } else {
            if (id > 0)
                url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfServiceKeeng(Config.UrlKeengEnum.SERVICE_GET_ALBUM) + "?id=" + id;
            else if (!TextUtils.isEmpty(identify))
                url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfServiceKeeng(Config.UrlKeengEnum.SERVICE_GET_ALBUM) + "?identify=" + HttpHelper.EncoderUrl(identify);
        }
        if (TextUtils.isEmpty(url)) {
            if (listener != null) listener.onError("model error");
            return;
        }
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getDetail: " + tag + " : " + response);
                if (TextUtils.isEmpty(response)) {
                    listener.onError(response);
                    return;
                }
                try {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject object = jsonElement.getAsJsonObject();
                    if (object.has("data")) {
                        Gson gson = new Gson();
                        MediaModel model = gson.fromJson(object.getAsJsonObject("data"), MediaModel.class);
                        if (model != null) {
                            listener.onResponse(model);
                        } else {
                            listener.onError("model error");
                        }
                    } else {
                        listener.onError("no media");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onError("Parser error" + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
                listener.onError("VolleyError" + volleyError.getMessage());
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, tag, false);
    }

    public interface GetSongListener {
        void onResponse(MediaModel object);

        void onError(String err);
    }
}
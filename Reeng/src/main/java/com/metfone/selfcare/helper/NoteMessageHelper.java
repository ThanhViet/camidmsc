package com.metfone.selfcare.helper;

import android.content.Context;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.datasource.NoteMessageDataSource;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.NoteMessageItem;
import com.metfone.selfcare.helper.images.ImageHelper;

import java.util.ArrayList;

/**
 * Created by sonnn00 on 7/19/2017.
 */

public class NoteMessageHelper {
    private static final String TAG = NoteMessageHelper.class.getSimpleName();

    private static NoteMessageHelper mInstance;
    private ApplicationController mApplication;
    private ArrayList<NoteMessageItem> listNoteMessageItems = new ArrayList<>();

    public static synchronized NoteMessageHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new NoteMessageHelper(application);
        }
        return mInstance;
    }

    private NoteMessageHelper(ApplicationController application) {
        this.mApplication = application;

//        listNoteMessageItems = getAllNoteMessage();
    }

    public ArrayList<NoteMessageItem> getListNoteMessageItems() {

        return listNoteMessageItems;
    }

//    public void saveNoteMessage(NoteMessageItem message) {
////        NoteMessageItem messageItem = new NoteMessageItem();
////        messageItem.setId(message.getId());
////        messageItem.setTimestamp(message.getTime());
////        messageItem.setThread_avatar(message.getSenderAvatar());
////        messageItem.setContent(message.getContent());
//        NoteMessageDataSource.getInstance(mApplication).insertNoteMessageItem(message);
//        listNoteMessageItems.add(message);
//    }

    public void updateNoteMessage(NoteMessageItem message) {

        NoteMessageDataSource.getInstance(mApplication).updateNoteMessageItem(message);
    }

    public ArrayList<NoteMessageItem> getAllNoteMessage() {

        return NoteMessageDataSource.getInstance(mApplication).getAllNoteMessageItem();

    }

    public ArrayList<NoteMessageItem> getListNote(int page) {

        return NoteMessageDataSource.getInstance(mApplication).getListNote(page);

    }

    public void deleteNoteMessage(Object message) {
        NoteMessageItem pos = (NoteMessageItem) message;
        NoteMessageDataSource.getInstance(mApplication).deleteNoteMessage(pos);
    }

    public void saveNoteMessage(Context context, ReengMessage entry, ThreadMessage mThreadMessage, ContactBusiness
            mContactBusiness, AvatarBusiness mAvatarBusiness, int mThreadType,
                                String friendPhoneNumber, String userNumber, OfficerAccount mOfficerAccountRoomChat) {
        NoteMessageItem messageItem = new NoteMessageItem();
//        messageItem.setId(entry.getId());
        messageItem.setThread_type(mThreadType);
        messageItem.setThread_jid(friendPhoneNumber);
        messageItem.setThread_name(userNumber);
        String threadAvatar = null;
        int sizeAvatar = (int) context.getResources().getDimension(R.dimen.avatar_small_size);
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String numberFriend = mThreadMessage.getSoloNumber();
            PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(numberFriend);
            StrangerPhoneNumber stranger = mThreadMessage.getStrangerPhoneNumber();
            //avatar
            if (phoneNumber != null) {

                threadAvatar = mAvatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), numberFriend,
                        sizeAvatar);

            } else {
                if (mThreadMessage.isStranger()) {
                    NonContact nonContact = mContactBusiness.getExistNonContact(numberFriend);

                    if (stranger != null) {

                        if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                            threadAvatar = stranger.getFriendAvatarUrl();
                        } else {
                            String lastAvatar = null;
                            if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                                lastAvatar = nonContact.getLAvatar();
                            } else if (nonContact == null) {
                                lastAvatar = stranger.getFriendAvatarUrl();// chat voi nguoi la thi truong nay luu
                                // lastchange
                            }
                            threadAvatar = mAvatarBusiness.getAvatarUrl(lastAvatar, friendPhoneNumber, sizeAvatar);
                        }
                    } else {
                        if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                            threadAvatar = mAvatarBusiness.getAvatarUrl(nonContact.getLAvatar(), friendPhoneNumber,
                                    sizeAvatar);
                        } else {
                            threadAvatar = null;
                        }
                    }
                } else {
                    NonContact nonContact = mContactBusiness.getExistNonContact(friendPhoneNumber);
                    if (nonContact != null && nonContact.getState() == Constants.CONTACT.SYSTEM_BLOCK) {
                        threadAvatar = null;
                    } else {
                        String lAvatar;
                        if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                            lAvatar = nonContact.getLAvatar();
                            threadAvatar = mAvatarBusiness.getAvatarUrl(lAvatar, friendPhoneNumber, sizeAvatar);
                        } else {
                            threadAvatar = null;
                        }
                    }
                }
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
            messageItem.setThread_jid(mThreadMessage.getServerId());
            if (TextUtils.isEmpty(mThreadMessage.getGroupAvatar())) {

                int threadId = mThreadMessage.getId();

                threadAvatar = ImageHelper.getAvatarGroupFilePath(threadId);

            } else {
                threadAvatar = mAvatarBusiness.getGroupAvatarUrl(mThreadMessage, mThreadMessage.getGroupAvatar());

            }


        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room

            if (mOfficerAccountRoomChat != null) {
                threadAvatar = mOfficerAccountRoomChat.getAvatarUrl();
            } else {
                threadAvatar = null;
            }

        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {

        } else {    // official
            messageItem.setThread_jid(mThreadMessage.getServerId());

            if (mOfficerAccountRoomChat != null) {
                threadAvatar = mOfficerAccountRoomChat.getAvatarUrl();
            } else {
                OfficerBusiness officerBusiness = mApplication.getOfficerBusiness();
                threadAvatar = officerBusiness.getOfficerAvatarByServerId(mThreadMessage.getServerId());
            }
        }

        messageItem.setThread_avatar(threadAvatar);
        messageItem.setContent(TagHelper.getTextTagCopy(entry.getContent(), entry.getListTagContent(), mApplication));
        messageItem.setTimestamp(TimeHelper.getCurrentTime());
//        return messageItem;
        NoteMessageDataSource.getInstance(mApplication).insertNoteMessageItem(messageItem);
//        listNoteMessageItems.add(messageItem);
    }
}

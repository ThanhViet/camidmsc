package com.metfone.selfcare.helper.images;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.DrawableRes;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.sticker.StickerHelper;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import static com.bumptech.glide.load.DecodeFormat.DEFAULT;

/**
 * Created by toanvk2 on 1/27/15.
 */
@SuppressWarnings("SuspiciousNameCombination")
public class ImageLoaderManager {
    private static final String TAG = ImageLoaderManager.class.getSimpleName();
    public static final String PATH_BACKGROUND_DEFAULT = "assets:bgfull/bg_default_white.jpg";
    private static final String FILE_PATH = "file://";
    private static final String ASSET_PATH = "file:///android_asset/";
    private static final int THUMBLE_MAX_SIZE = 700;// gioi hanj anh thumble 400px
    private static final int SCREEN_MAX_WIDTH = 1080;//720
    private static final int SCREEN_MAX_HEIGHT = 1920;//1280
    private static final int LOCATION_MAX_WIDTH = 640;
    private static final int LOCATION_MAX_HEIGHT = 370;
    private static final double scaleBackground = 1;
    private static ImageLoaderManager instance;
    private ApplicationController mApplication;
    private Resources mRes;
    //    private ImageLoader universalImageLoader;
    private RequestOptions imageGalleryOptions;
    private RequestOptions imageOptions;
    private RequestOptions imageGuestBookOptions;
    private RequestOptions stickerGuestBookOptions;
    private RequestOptions thumbnailMapsOptions;
    private RequestOptions stickerOptions;
    private RequestOptions imageBackgroundOptions;
    private RequestOptions imageBannerOptions;
    private RequestOptions imageGalleryVideoOptions;
    private RequestOptions videoOptions;
    private RequestOptions stickerIndicatorOptions;
    private RequestOptions luckyWheelOptions;
    private RequestOptions displayFeedsOptions;
    private RequestOptions displayFeedsSongOptions;
    private RequestOptions discoveryOptions;

    private int imageThumbleSize, stickerMaxSize, gallerySize, largeEmoSize;
    private int imageDetailWidth, imageDetailHeight, backgroundWidth, backgroundHeight;
    private int notificationBarHeight = 0;
    private int locationHeightThumbSize, locationWidthThumbSize;
    private int guestBookImageWidth, guestBookImageHeight;
    public HashMap<String, ArrayList<ImageProfile>> mImageProfilesOfContact = new HashMap<>();

    public static synchronized ImageLoaderManager getInstance(Context context) {
        if (instance == null) {
            instance = new ImageLoaderManager(context);
        }
        return instance;
    }

    private ImageLoaderManager(Context context) {
        mApplication = (ApplicationController) context.getApplicationContext();
        mRes = mApplication.getResources();
        initImageSize();
        initUniversalLoader();
    }

    /**
     * khoi tao option cho image loader
     */
    private void initUniversalLoader() {
//        universalImageLoader = mApplication.getUniversalImageLoader();
        // gallery option// khong luu cache vao diskcache
        imageGalleryOptions = new RequestOptions()
                .placeholder(R.drawable.ic_images_default)
                .error(R.drawable.ic_images_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        // thumble option
        imageOptions = new RequestOptions()
                .placeholder(R.drawable.ic_images_default)
                .error(R.drawable.ic_images_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        imageGuestBookOptions = new RequestOptions()
                .placeholder(R.drawable.ic_images_defaul_portrait)
                .error(R.drawable.ic_images_defaul_portrait)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        stickerGuestBookOptions = new RequestOptions()
                .placeholder(R.color.bg_guest_book_item)
                .error(R.color.bg_guest_book_item)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        // thumble maps
        thumbnailMapsOptions = new RequestOptions()
                .placeholder(R.drawable.ic_thumbnail_maps_default)
                .error(R.drawable.ic_thumbnail_maps_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        // background option
        imageBackgroundOptions = new RequestOptions()
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        //sticker option
        stickerOptions = new RequestOptions()
                .placeholder(R.drawable.ic_sticker_default)
                .error(R.drawable.ic_sticker_default)
                .dontAnimate()
                .dontTransform()
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        //banner option
        imageBannerOptions = new RequestOptions()
                .dontAnimate()
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        //browser video
        imageGalleryVideoOptions = new RequestOptions()
                .placeholder(R.drawable.ic_videos_default)
                .error(R.drawable.ic_videos_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        //show video thumbnail
        videoOptions = new RequestOptions()
                .placeholder(R.drawable.ic_videos_default)
                .error(R.drawable.ic_videos_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        stickerIndicatorOptions = new RequestOptions()
                .placeholder(R.drawable.ic_voice_sticker)
                .error(R.drawable.ic_voice_sticker)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        luckyWheelOptions = new RequestOptions()
                .placeholder(R.drawable.ic_lw_default)
                .error(R.drawable.ic_lw_default)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
        displayFeedsOptions = new RequestOptions()
                .placeholder(R.color.bg_onmedia_content_item)
                .error(R.color.bg_onmedia_content_item)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        displayFeedsSongOptions = new RequestOptions()
                .placeholder(R.drawable.bg_song_disc)
                .error(R.drawable.bg_song_disc)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);

        discoveryOptions = new RequestOptions()
                .placeholder(R.color.bg_onmedia_content_item)
                .error(R.color.bg_onmedia_content_item)
                .dontAnimate()
                .dontTransform()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .format(DEFAULT)
                .skipMemoryCache(false);
    }

    private void initImageSize() {
        // lay width, height screen
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) mApplication.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            wm.getDefaultDisplay().getMetrics(displaymetrics);
        }
        imageDetailWidth = displaymetrics.widthPixels;
        imageDetailHeight = displaymetrics.heightPixels;
        //  lay actionbar height
        /*TypedValue tv = new TypedValue();
        if (mApplication.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, displaymetrics);
        }*/
        // lay size thumble, sticker theo dimen
        stickerMaxSize = (int) mRes.getDimension(R.dimen.voice_sticker_size);
        largeEmoSize = (int) mRes.getDimension(R.dimen.emoticon_large_size);
        imageThumbleSize = (int) mRes.getDimension(R.dimen.image_thumbnail_size);
        gallerySize = (int) mRes.getDimension(R.dimen.photo_width);
        locationWidthThumbSize = (int) mRes.getDimension(R.dimen.image_thumbnail_size);
        locationHeightThumbSize = (int) mRes.getDimension(R.dimen.holder_thumbnail_location_height);
        // neu get tu dimen ma lon hon 400px thi set = 400
        stickerMaxSize = Math.min(stickerMaxSize, THUMBLE_MAX_SIZE);
        largeEmoSize = Math.min(largeEmoSize, THUMBLE_MAX_SIZE);
        imageThumbleSize = Math.min(imageThumbleSize, THUMBLE_MAX_SIZE);
        gallerySize = Math.min(gallerySize, THUMBLE_MAX_SIZE);
        // neu screen >720*1280 thi set ve 720*1280
        imageDetailWidth = Math.min(imageDetailWidth, SCREEN_MAX_WIDTH);
        imageDetailHeight = Math.min(imageDetailHeight, SCREEN_MAX_HEIGHT);
        locationWidthThumbSize = Math.min(locationWidthThumbSize, LOCATION_MAX_WIDTH);
        locationHeightThumbSize = Math.min(locationHeightThumbSize, LOCATION_MAX_HEIGHT);
        initBackgroundSize();
        initGuestBookSize();
    }

    private void initBackgroundSize() {
        double height = scaleBackground * (imageDetailHeight - notificationBarHeight);
        double width = scaleBackground * (imageDetailWidth);
        backgroundHeight = (int) height;
        backgroundWidth = (int) width;
    }

    private void initGuestBookSize() {
        guestBookImageWidth = (mApplication.getWidthPixels() * 3) / 5;
        guestBookImageWidth = Math.min(guestBookImageWidth, THUMBLE_MAX_SIZE);
        guestBookImageHeight = (int) (guestBookImageWidth * com.android.editor.sticker.utils.Constants.SCALE_HEIGHT);
    }

    public void setHeightNotificationBar(Resources res) {
        // set lai chieu cao notification khi ==0
        if (notificationBarHeight == 0) {
            int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                notificationBarHeight = res.getDimensionPixelSize(resourceId);
                initBackgroundSize();
            }
        }
    }

    private String getStickerPath(int collectionId, int itemId) {
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            StickerItem defaultStickerByItemId = EmoticonUtils.getDefaultStickerByItemId(itemId);
            if (defaultStickerByItemId != null) {
                //item da co trong bo default
                return ASSET_PATH + defaultStickerByItemId.getImagePath();
            } else {
                //item chua co trong bo default
                return "";
            }
        } else {
            StickerItem stickerItem = mApplication.getStickerBusiness().getStickerItem(collectionId, itemId);
            if (stickerItem != null && stickerItem.isDownloadImg()
                    && !TextUtils.isEmpty(stickerItem.getImagePath())) {
                return FILE_PATH + stickerItem.getImagePath();
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(FILE_PATH);
                sb.append(Config.Storage.REENG_STORAGE_FOLDER).append(Config.Storage.STICKER_FOLDER).append
                        (collectionId).append("/");
                sb.append(itemId).append(".png");
                return sb.toString();
            }
        }
    }

//    public void displayThumbnailOfMessage(ReengMessage message, RoundedImageView content) {
//        if (message.getStatus() == ReengMessageConstant.STATUS_RECEIVED
//                || message.getDirection() == ReengMessageConstant.Direction.send) {
//            String filePath = null;
//            if (!TextUtils.isEmpty(message.getFilePath())) {
//                filePath = FILE_PATH + message.getFilePath();
//            }
//            if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
////                universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(content, imageThumbleSize,
////                        imageThumbleSize), videoOptions);
//                RequestOptions options = videoOptions.override(imageThumbleSize, imageThumbleSize);
//                Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
//            } else {
//                Log.d(TAG, "displayThumbnailOfMessage: " + filePath);
////                universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(content, imageThumbleSize,
////                        imageThumbleSize), imageOptions);
//                RequestOptions options = imageOptions.override(imageThumbleSize, imageThumbleSize);
//                Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
//            }
//        } else {
//            Log.d(TAG, "displayThumbnailOfMessage: cancelDisplayTask");
////            universalImageLoader.cancelDisplayTask(content);
//            Glide.with(mApplication).clear(content);
//            if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
//                content.setImageResource(R.drawable.ic_videos_default);
//            } else {
//                content.setImageResource(R.drawable.ic_images_default);
//            }
//        }
//    }

    public void displayThumbnailOfImagePath(String imagePath, ImageView content) {
//        universalImageLoader.displayImage(imagePath,
//                new ImageViewAwareTargetSize(content, imageThumbleSize, imageThumbleSize), imageOptions);
        RequestOptions options = imageOptions.override(imageThumbleSize, imageThumbleSize);
        Glide.with(ApplicationController.self()).load(imagePath).apply(options).into(content);
    }

    public void displayDetailOfMessage(String filePath, ImageView content, boolean isVideo) {
        displayDetailOfMessage(filePath, content, isVideo, false);
    }

    public void displayDetailOfMessage(String filePath, ImageView content, boolean isVideo, boolean isFull) {
        if (!TextUtils.isEmpty(filePath)) {
            if (new File(FILE_PATH + filePath).exists()) {
                filePath = FILE_PATH + filePath;
            }
        }
        if (isVideo) {
//            universalImageLoader.displayImage(filePath,
//                    new ImageViewAwareTargetSize(content, imageThumbleSize, imageThumbleSize), videoOptions);
            RequestOptions options = videoOptions.override(imageThumbleSize, imageThumbleSize);
            Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
        } else {
            if (isFull) {
//                universalImageLoader.displayImage(filePath,
//                        new ImageViewAwareTargetSize(content, imageDetailWidth, imageDetailHeight), imageOptions);
                RequestOptions options = imageOptions.override(imageDetailWidth, imageDetailHeight);
                Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
            } else {
//                universalImageLoader.displayImage(filePath,
//                        new ImageViewAwareTargetSize(content, imageThumbleSize, imageThumbleSize), imageOptions);
                RequestOptions options = imageOptions.override(imageThumbleSize, imageThumbleSize);
                Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
            }
        }
    }

    public void displayDetailOfMessageImageLink(ImageView image, String url) {
//        universalImageLoader.displayImage(url,
//                new ImageViewAwareTargetSize(image, imageDetailWidth, imageDetailWidth), imageOptions);
        RequestOptions options = imageOptions.override(imageDetailWidth, imageDetailWidth);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(image);
    }

    public void displayThumbnailLocation(ImageView imageView) {
        /*String url = LocationHelper.getInstant(mApplication).getThumbnailLocation(latitude,
                longitude, 17, locationWidthThumbSize, locationHeightThumbSize);*/
//        universalImageLoader.displayImage("drawable://" + R.drawable.ic_thumb_map, imageView, thumbnailMapsOptions);
        Glide.with(ApplicationController.self()).load(R.drawable.ic_thumb_map).apply(thumbnailMapsOptions).into(imageView);
    }

    public void displayThumbleOfGallery(String filePath, ImageView content) {
        // Log.d(TAG, "displayThumbleOfGallery: " + filePath);
        filePath = getThumbGalleryPath(filePath);
//        universalImageLoader.displayImage(filePath,
//                new ImageViewAwareTargetSize(content, gallerySize, gallerySize), imageGalleryOptions);
        RequestOptions options = imageGalleryOptions.override(gallerySize, gallerySize);
        Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
    }

    public void displayThumbnailVideo(String filePath, ImageView content) {
//        universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(content, gallerySize, gallerySize),
//                imageGalleryVideoOptions);
        RequestOptions options = imageGalleryVideoOptions.override(gallerySize, gallerySize);
        Glide.with(ApplicationController.self()).load(filePath).apply(options).into(content);
    }

    public void displayStickerImage(final ImageView imageView, final int collectionId, final int itemId) {
        // lay duong dan sticker
        String stickerPath = getStickerPath(collectionId, itemId);
        Log.i(TAG, "displayStickerImage collectionId " + collectionId + " itemId " + itemId + " stickerPath " +
                stickerPath);
        if (TextUtils.isEmpty(stickerPath)) {
            //chua co trong bo default
            displayStickerOnNetwork(imageView, collectionId, itemId + 1);
        } else {
            RequestOptions options = stickerOptions.override(stickerMaxSize, stickerMaxSize).centerInside();
            new GlideImageLoader(imageView, new GlideImageLoader.SimpleImageLoadingListener() {
                @Override
                public void onLoadingStarted() {

                }

                @Override
                public void onLoadingFailed(GlideException e) {
                    // FailType.IO_ERROR file not fopund
//                    if (failReason.getType() == FailReason.FailType.DECODING_ERROR) {
                    reloadStickerItemWhenExtraError(imageView, collectionId, itemId);
//                    }
                }

                @Override
                public void onLoadingComplete() {

                }
            }).load(stickerPath, options);
        }
    }

    public void displayLargeEmoticon(ImageView imageView, String content) {
        String emoticonPath = EmoticonUtils.getAssetPathEmoticon(content);
//        universalImageLoader.displayImage(emoticonPath, new ImageViewAwareTargetSize(imageView,
//                largeEmoSize, largeEmoSize), stickerOptions);
        RequestOptions options = stickerOptions.override(largeEmoSize, largeEmoSize);
        Glide.with(ApplicationController.self()).load(Uri.parse(emoticonPath)).apply(options).into(imageView);
    }

    public void displayStickerImageFromSdcard(final ImageView imageView, StickerItem stickerItem) {
        String imagePath = stickerItem.getImagePath();
        if (!TextUtils.isEmpty(imagePath)) {
            if (new File(FILE_PATH + imagePath).exists()) {
                imagePath = FILE_PATH + imagePath;
            }
        }
//        universalImageLoader.displayImage(imagePath, new ImageViewAwareTargetSize(imageView,
//                stickerMaxSize, stickerMaxSize), stickerOptions);
        RequestOptions options = stickerOptions.override(stickerMaxSize, stickerMaxSize);
        Glide.with(ApplicationController.self()).load(imagePath).apply(options).into(imageView);
    }

//    public void displayPreviewSticker(String url, ImageView imageView, GlideImageLoader.SimpleImageLoadingListener listener) {
//        universalImageLoader.displayImage(url, imageView, imageOptions, listener);
//    }

    /**
     * loaf 1 sicker tu sv ve
     */
    public void displayStickerOnNetwork(ImageView imageView, int collectionId, int itemId) {
       /* StringBuilder sb = new StringBuilder();
        String domailFile = UrlConfigHelper.getInstance(mApplication).getConfigDomainFile();
        sb.append(domailFile).append(Config.PREFIX.STICKER).append("/").
                append(collectionId).append("/").append(itemId).append(".png");*/
        String url = StickerHelper.getUrlStickerItem(mApplication, collectionId, itemId);
        Log.i(TAG, "displayStickerOnNetwork collectionId " + collectionId + " itemId " + itemId + " stickerPath " +
                url);
//        universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView,
//                stickerMaxSize, stickerMaxSize), stickerOptions);
        RequestOptions options = stickerOptions.override(stickerMaxSize, stickerMaxSize);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(imageView);
    }

    public void displayGifStickerNetwork(ImageView imageView, String url) {
        url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + url;
        Log.i(TAG, "URL GIF sticker: " + url);
//        universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView,
//                stickerMaxSize, stickerMaxSize), stickerOptions);
        RequestOptions options = stickerOptions.override(stickerMaxSize, stickerMaxSize);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(imageView);
    }

    public void displayStickerNetwork(ImageView imageView, String url) {
//        universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView,
//                imageThumbleSize, imageThumbleSize), stickerOptions);
        RequestOptions options = stickerOptions.override(imageThumbleSize, imageThumbleSize);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(imageView);
    }

    private void reloadStickerItemWhenExtraError(final ImageView imageView,
                                                 final int collectionId,
                                                 final int itemId) {
        String stickerItemUrl = StickerHelper.getUrlStickerItem(mApplication, collectionId, itemId);
        Log.d(TAG, "stickerItemUrl: " + stickerItemUrl);
        if (TextUtils.isEmpty(stickerItemUrl)) {
            return;
        }
        new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {
            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {

            }

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
                if (loadedImage != null) { // load thanh cong thi bitmap != null
                    FileOutputStream out = null;
                    try {
                        // save bitmap to file
                        File stickerCollectionFile = StickerHelper.findOrCreateSubFileSticker(collectionId);
                        File outFile = new File(stickerCollectionFile + "/" + itemId + ".png");
                        out = new FileOutputStream(outFile);
                        loadedImage.compress(Bitmap.CompressFormat.PNG, 80, out);
                        if (imageView != null) {// refresh lai neu image van con
//                            universalImageLoader.displayImage(getStickerPath(collectionId, itemId), new
//                                    ImageViewAwareTargetSize(imageView,
//                                    stickerMaxSize, stickerMaxSize), stickerOptions);
                            RequestOptions options = stickerOptions.override(stickerMaxSize, stickerMaxSize);
                            Glide.with(ApplicationController.self()).load(getStickerPath(collectionId, itemId)).apply(options).into(imageView);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }
            }
        }).loadBitmap(stickerItemUrl, stickerOptions);

//        universalImageLoader.loadImage(stickerItemUrl, stickerOptions, );
    }

    public void displayGifThumb(ImageView imageView, String filePath) {
        if (!TextUtils.isEmpty(filePath)) {
            if (new File(FILE_PATH + filePath).exists()) {
                filePath = FILE_PATH + filePath;
            }
        }
//        universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(imageView,
//                stickerMaxSize, stickerMaxSize), stickerOptions);
        RequestOptions options = stickerOptions.override(stickerMaxSize, stickerMaxSize);
        Glide.with(ApplicationController.self()).load(filePath).apply(options).into(imageView);
    }

    public void displayGuestBookPreview(ImageView imageView, String url) {
        displayGuestBookPreview(imageView, url, null);
    }

    public void displayGuestBookPreview(ImageView imageView, String url, GlideImageLoader.SimpleImageLoadingListener listener) {
        RequestOptions options = imageGuestBookOptions.override(guestBookImageWidth, guestBookImageHeight);
        new GlideImageLoader(imageView, listener).load(url, options);
//        universalImageLoader.displayImage(url,
//                new ImageViewAwareTargetSize(imageView, guestBookImageWidth, guestBookImageHeight),
//                imageGuestBookOptions, listener);
    }

    @SuppressWarnings("deprecation")
    public Bitmap loadGuestBookPreviewSync(String url) {
//        return universalImageLoader.loadImageSync(url, new ImageSize(guestBookImageWidth, guestBookImageWidth),
//                imageGuestBookOptions);
        try {
            return Glide.with(mApplication).asBitmap().load(url).into(guestBookImageWidth, guestBookImageWidth).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void displayGuestBookEmo(ImageView imageView, String url, int width, int height) {
        Log.i(TAG, "displayGuestBookEmo url: " + url);
//        universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView, width, height),
//                stickerOptions);
        RequestOptions options = stickerOptions.override(width, height);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(imageView);
    }

    public void loadGuestBookBitmap(String url, int width, int height, GlideImageLoader.ImageLoadingListener listener) {
//        universalImageLoader.loadImage(url, new ImageSize(width, height), stickerGuestBookOptions, listener);
        RequestOptions options = stickerGuestBookOptions.override(width, height);
        new GlideImageLoader(listener).loadBitmap(url, options);
    }

    public void loadGuestBookFileBitmap(String filePath, int width, int height, GlideImageLoader.ImageLoadingListener listener) {
//        universalImageLoader.loadImage(FILE_PATH + filePath, new ImageSize(width, height), stickerGuestBookOptions,
//                listener);
        RequestOptions options = stickerGuestBookOptions.override(width, height);
        if (new File(FILE_PATH + filePath).exists()) {
            filePath = FILE_PATH + filePath;
        }
        new GlideImageLoader(listener).loadBitmap(filePath, options);
    }

    public void clearDiskCacheSticker(int collectionId) {
//        while (!universalImageLoader.isInited()) {
//            try {
//                Log.i(TAG, "sleep wait clearDiskCacheSticker");
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                Log.e(TAG, "InterruptedException", e);
//            }
//        }
//        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
//            int sizeItem = EmoticonUtils.getStickerImagePaths().length;
//            for (int i = 0; i < sizeItem; i++) {
//                String path = ASSET_PATH + EmoticonUtils.getStickerImagePaths()[i];
//                universalImageLoader.clearDiskCache(path);
//                Log.d(TAG, "clearDiskCacheSticker: " + path);
//            }
//        }
        new Thread(() -> Glide.get(ApplicationController.self()).clearDiskCache()).start();
    }

    public void clearDiskCacheBackgroundDefault() {
//        while (!universalImageLoader.isInited()) {
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                Log.e(TAG, "InterruptedException", e);
//            }
//        }
//        String path = getThumbGalleryPath(PATH_BACKGROUND_DEFAULT);
//        universalImageLoader.clearDiskCache(path);
        new Thread(() -> Glide.get(ApplicationController.self()).clearDiskCache()).start();
    }

//    public void clearDiskCacheAllBackgroundAsset() {
//        while (!universalImageLoader.isInited()) {
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                Log.e(TAG, "InterruptedException", e);
//            }
//        }
//        try {
//            String[] listBg = mApplication.getAssets().list("bgfull");
//            String prePath = "assets:bgfull/";
//            for (int i = 0; i < listBg.length; i++) {
//                String path = getThumbleGalleryPath(prePath + listBg[i]);
//                universalImageLoader.clearDiskCache(path);
//                GlideApp.get(ApplicationController.self()).clearDiskCache();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    /**
     * anh tu assets thi them asset://
     * anh tu sdcard thi them file://
     */
    private String getThumbGalleryPath(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return null;
        }
        String path;
        if (filePath.startsWith("assets:")) {
            path = filePath.replaceAll("assets:", "");
            return ASSET_PATH + path;
        } else {
            if (new File(FILE_PATH + filePath).exists()) {
                path = FILE_PATH + filePath;
            } else {
                path = filePath;
            }
        }
        return path;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void displayBackgroundOfThreadDetail(String filePath, final ImageView imageView) {
        if (TextUtils.isEmpty(filePath)) {
            String configBg = mApplication.getConfigBusiness().getBackgroundDefault();
            filePath = (configBg != null & !"-".equals(configBg)) ? configBg : PATH_BACKGROUND_DEFAULT;
        }
        //filePath = mApplication.getConfigBusiness().getBackgroundDefault();
        Log.d(TAG, "displayBackgroundOfThreadDetail: " + filePath);
        if (!filePath.startsWith("http:") && !filePath.startsWith("https:")) {
            filePath = getThumbGalleryPath(filePath);
        }
        final int height = mApplication.getHeightPixels() - mApplication.getStatusBarHeight() - mRes
                .getDimensionPixelSize(R.dimen.action_bar_height);
        //final int height = mApplication.getHeightPixels();
        Log.d(TAG, "displayBackgroundOfThreadDetail - height: " + height);
//        universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(imageView, mApplication
//                        .getWidthPixels(), height),
//                imageBackgroundOptions,
//                new GlideImageLoader.SimpleImageLoadingListener() {
//                    @Override
//                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                        super.onLoadingFailed(imageUri, view, failReason);
//                        Log.d(TAG, "displayBackgroundOfThreadDetail: onLoadingFailed");
//                        // load anh loi thi set mac dinh
//                        universalImageLoader.displayImage(getThumbleGalleryPath(PATH_BACKGROUND_DEFAULT),
//                                new ImageViewAwareTargetSize(imageView, mApplication.getWidthPixels(), height),
//                                imageBackgroundOptions);
//                    }
//                });
        RequestOptions options = imageBackgroundOptions.override(mApplication.getWidthPixels(), height);
        new GlideImageLoader(imageView, new GlideImageLoader.SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(GlideException e) {
                new Handler().post(() -> Glide.with(ApplicationController.self())
                        .load(getThumbGalleryPath(PATH_BACKGROUND_DEFAULT))
                        .apply(options).into(imageView));
//                universalImageLoader.displayImage(getThumbleGalleryPath(PATH_BACKGROUND_DEFAULT),
//                        new ImageViewAwareTargetSize(imageView, mApplication.getWidthPixels(), height),
//                        imageBackgroundOptions);
            }

            @Override
            public void onLoadingComplete() {

            }
        }).load(filePath, options);
    }

//    public void displayThumbBackgroundOfThreadDetail(String filePath, final ImageView imageView) {
//        if (TextUtils.isEmpty(filePath)) {
//            filePath = PATH_BACKGROUND_DEFAULT;
//        }
//        if (!filePath.startsWith("http:") && !filePath.startsWith("https:")) {
//            filePath = getThumbleGalleryPath(filePath);
//        }
//        Log.d(TAG, "displayThumbBackgroundOfThreadDetail: " + filePath);
//        new GlideImageLoader(imageView, new GlideImageLoader.SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingStarted() {
//
//            }
//
//            @Override
//            public void onLoadingFailed(GlideException e) {
//                Glide.with(ApplicationController.self()).load(getThumbleGalleryPath(PATH_BACKGROUND_DEFAULT)).apply(imageGalleryOptions).into(imageView);
////                universalImageLoader.displayImage(getThumbleGalleryPath(PATH_BACKGROUND_DEFAULT),
////                        imageView, imageGalleryOptions);
//            }
//
//            @Override
//            public void onLoadingComplete() {
//
//            }
//        }).load(filePath, imageGalleryOptions);
//    }

    public void displayStickerIndicator(ImageView imageView, String url) {
//        universalImageLoader.displayImage(url, imageView, stickerIndicatorOptions);
        Glide.with(ApplicationController.self()).load(url).apply(stickerIndicatorOptions).into(imageView);
    }

    public void displayLuckyWheelIcon(ImageView imageView, String url) {
//        universalImageLoader.displayImage(url, imageView, luckyWheelOptions);
        Glide.with(ApplicationController.self()).load(url).apply(luckyWheelOptions).into(imageView);
    }

    public void cancelDisplayTag(ImageView imageView) {
//        universalImageLoader.cancelDisplayTask(imageView);
        Glide.with(mApplication).clear(imageView);
    }

    /**
     * load bitmap from file_path
     */
    public void loadCacheBackgroundOfThreadDetail(String filePath, GlideImageLoader.ImageLoadingListener listener) {
        Log.d(TAG, "displayBackgroundOfThreadDetail: " + filePath);
        filePath = getThumbGalleryPath(filePath);
//        universalImageLoader.loadImage(filePath, new ImageSize(backgroundWidth, backgroundHeight),
//                imageBackgroundOptions, listener);
        RequestOptions options = imageBackgroundOptions.override(backgroundWidth, backgroundHeight);
        new GlideImageLoader(listener).loadBitmap(filePath, options);
    }

    public void displayImageBanner(String url, final ImageView imageView, final @DrawableRes int resDef) {
        if (TextUtils.isEmpty(url)) {
            imageView.setImageResource(resDef);
//            universalImageLoader.cancelDisplayTask(imageView);
            Glide.with(mApplication).clear(imageView);
        } else {
            new GlideImageLoader(imageView, new GlideImageLoader.SimpleImageLoadingListener() {
                @Override
                public void onLoadingStarted() {
                    imageView.setImageResource(resDef);
                }

                @Override
                public void onLoadingFailed(GlideException e) {
                    imageView.setImageResource(resDef);
                }

                @Override
                public void onLoadingComplete() {

                }
            }).load(url, imageBannerOptions);

//            universalImageLoader.displayImage(url, imageView, imageBannerOptions, new GlideImageLoader.SimpleImageLoadingListener() {
//                @Override
//                public void onLoadingStarted(String imageUri, View view) {
//                    super.onLoadingStarted(imageUri, view);
//                    imageView.setImageResource(resDef);
//                }
//
//                @Override
//                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    super.onLoadingFailed(imageUri, view, failReason);
//                    imageView.setImageResource(resDef);
//                }
//            });
        }
    }

//    public void addImagesOfPhoneNumber(String phoneNumber, ArrayList<ImageProfile> imageProfiles) {
//        if (TextUtils.isEmpty(phoneNumber))
//            return;
//
//        mImageProfilesOfContact.put(phoneNumber, imageProfiles);
//    }

//    public ArrayList<ImageProfile> getImageProfileOfPhoneNumber(String phoneNumber) {
//        if (TextUtils.isEmpty(phoneNumber))
//            return null;
//
//        ArrayList<ImageProfile> list = mImageProfilesOfContact.get(phoneNumber);
//        if (list != null && !list.isEmpty()) {
//            ArrayList<ImageProfile> imageProfiles = new ArrayList<>();
//            for (ImageProfile image : list) {
//                if (null != image && image.getTypeImage() == ImageProfileConstant.IMAGE_NORMAL) {
//                    imageProfiles.add(image);
//                }
//            }
//            return imageProfiles;
//        }
//        return null;
//    }

//    public ImageProfile getImageCoverOfPhoneNumber(String phoneNumber) {
//        if (TextUtils.isEmpty(phoneNumber))
//            return null;
//
//        ArrayList<ImageProfile> list = mImageProfilesOfContact.get(phoneNumber);
//        if (list != null && !list.isEmpty()) {
//            //ArrayList<ImageProfile> imageProfiles = new ArrayList<ImageProfile>();
//            for (ImageProfile image : list) {
//                if (null != image && image.getTypeImage() == ImageProfileConstant.IMAGE_COVER) {
//                    return image;
//                }
//            }
//        }
//        return null;
//    }

    public void loadAdsImage(final View adsView,
                             final ImageView imageView,
                             final WebView webView,
                             String urlImage,
                             final String url) {
        new GlideImageLoader(new GlideImageLoader.ImageLoadingListener() {

            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {

            }

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
                imageView.setImageBitmap(loadedImage);
                imageView.setVisibility(View.VISIBLE);
                imageView.setOnClickListener(v -> {
                    Log.i(TAG, "WebView.loadUrl: " + url);
                    webView.loadUrl(url);
                    adsView.setVisibility(View.GONE);
                });
                adsView.setVisibility(View.VISIBLE);
            }
        }).loadBitmap(urlImage, imageBannerOptions);
    }

    public void setImageFeeds(ImageView imageView, String url) {
//        universalImageLoader.displayImage(url, imageTargetSize, displayFeedsOptions);
        Glide.with(ApplicationController.self()).load(url).apply(discoveryOptions).into(imageView);
    }

    public void setImageFeeds(ImageView songImage, String url, int width, int height) {
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(songImage, width, height);
//        universalImageLoader.displayImage(url, imageViewAwareTargetSize, displayFeedsOptions);
        RequestOptions options = displayFeedsOptions.override(width, height);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(songImage);
    }

//    public void setImageFeeds(ImageViewAwareTargetSize imageTargetSize, String url) {
//        universalImageLoader.displayImage(url, imageTargetSize, displayFeedsOptions);
//        Glide.with(ApplicationController.self()).load(url).apply(displayFeedsOptions).into(songImage);
//    }

    public void setImageFeedsSong(ImageView songImage, String url, int width, int height) {
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(songImage, width, height);
//        universalImageLoader.displayImage(url, imageViewAwareTargetSize, displayFeedsSongOptions);
        RequestOptions options = displayFeedsSongOptions.override(width, height);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(songImage);
    }

    public void setImageDiscovery(ImageView imageView, String url, int width, int height) {
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(imageView, width, height);
//        universalImageLoader.displayImage(url, imageViewAwareTargetSize, discoveryOptions);
        RequestOptions options = discoveryOptions.override(width, height);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(imageView);
    }

//    public void setImageDiscovery(ImageView imageView, String url) {
//        Glide.with(ApplicationController.self()).load(url).apply(discoveryOptions).into(imageView);
////        universalImageLoader.displayImage(url, imageViewAwareTargetSize, discoveryOptions);
//    }

    public void displayImageFeedsWithListener(AspectImageView mImgBanner, String imageUrl, int width, int height) {
        Glide.with(ApplicationController.self()).load(imageUrl).apply(new RequestOptions().override(width, height)).into(mImgBanner);
    }

    public void setAvatarOfficialOnMedia(ImageView imageView, String url, int size) {
        RequestOptions options = displayFeedsOptions.override(size, size);
        Glide.with(ApplicationController.self()).load(url).apply(options).into(imageView);
//        universalImageLoader.displayImage(url, new ImageViewAwareTargetSize(imageView, size, size),
//                displayFeedsOptions);
    }

    /*public void setImageAdvertise(ImageViewAwareTargetSize targetSize, String url) {
        universalImageLoader.displayImage(url, targetSize, displayImageFirstAdvertiseOption);
    }*/

   /* private void blurImage(Bitmap loadedImage){
        //https://futurestud.io/tutorials/how-to-blur-images-efficiently-with-androids-renderscript
        //https://github.com/mordonez-me/instagram-filters-jhlabs-android/tree/master/ImageFilters/src/main/java/com
        /jabistudio/androidjhlabs/filter/util
        int width = loadedImage.getWidth();
        int height = loadedImage.getHeight();
        BoxBlurFilter filter = new BoxBlurFilter();
        filter.setRadius(8.5f);
        int[] src = AndroidUtils.bitmapToIntArray(loadedImage);
        src = filter.filter(src, width, height);
        loadedImage = Bitmap.createBitmap(src, width, height, Bitmap.Config.ARGB_8888);
    }*/
}
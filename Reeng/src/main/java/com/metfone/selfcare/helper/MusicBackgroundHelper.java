package com.metfone.selfcare.helper;

import android.media.MediaPlayer;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 8/31/2018.
 */

public class MusicBackgroundHelper implements MediaPlayer.OnErrorListener {


    private static final String TAG = MusicBackgroundHelper.class.getSimpleName();

    MediaPlayer mPlayer;
    private int length = 0;


    private ApplicationController mApp;

    public MusicBackgroundHelper(ApplicationController mApp) {
        this.mApp = mApp;
    }

    public void initMusic(int resRaw) {
        mPlayer = MediaPlayer.create(mApp, resRaw);
        mPlayer.setOnErrorListener(this);

        if (mPlayer != null) {
            mPlayer.setLooping(true);
            mPlayer.setVolume(100, 100);
        }


        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            public boolean onError(MediaPlayer mp, int what, int
                    extra) {

                onError(mPlayer, what, extra);
                return true;
            }
        });
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.e(TAG, "music player failed");
        if (mPlayer != null) {
            try {
                mPlayer.stop();
                mPlayer.release();
            } finally {
                mPlayer = null;
            }
        }
        return false;
    }

    public void startMusic(int resRaw) {
        initMusic(resRaw);
        mPlayer.start();
    }

    public void pauseMusic() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
            length = mPlayer.getCurrentPosition();

        }
    }

    public void resumeMusic() {
        if (mPlayer != null && !mPlayer.isPlaying()) {
            mPlayer.seekTo(length);
            mPlayer.start();
        }
    }

    public void stopMusic() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void onDestroyMusic()

    {
        if (mPlayer != null) {
            try {
                mPlayer.stop();
                mPlayer.release();
            } finally {
                mPlayer = null;
            }
        }

    }
}

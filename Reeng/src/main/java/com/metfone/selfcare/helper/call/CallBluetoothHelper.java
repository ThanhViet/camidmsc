package com.metfone.selfcare.helper.call;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.broadcast.BluetoothHeadsetReceiver;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/31/2018.
 */

public class CallBluetoothHelper {
    private static final String TAG = CallBluetoothHelper.class.getSimpleName();
    private static final int BLUETOOTH_SCO_TIMEOUT_MS = 4000;
    private static final int MAX_SCO_CONNECTION_ATTEMPTS = 2;
    private static CallBluetoothHelper mInstance;
    private ApplicationController mApplication;
    private AudioManager audioManager;
    private BluetoothHeadsetReceiver bluetoothReceiver;
    private BluetoothProfile.ServiceListener bluetoothServiceListener;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothHeadset bluetoothHeadset;
    private State bluetoothState = State.UNINITIALIZED;
    private Handler handler;
    private boolean isRegistedReceiver = false;
    private boolean currentDisableSpeaker = false;

    public enum State {
        UNINITIALIZED,      // Bluetooth is not available; no adapter or Bluetooth is off.
        NON,                // Bluetooth begin start
        SCO_DISCONNECTED,   // Bluetooth audio SCO connection with remote device is closing.
        SCO_CONNECTING,     // Bluetooth audio SCO connection with remote device is initiated.
        SCO_CONNECTED       // Bluetooth audio SCO connection with remote device is established.
    }

    private final Runnable bluetoothRunnable = new Runnable() {
        @Override
        public void run() {
            tryingConnectBluetooth();
        }
    };

    private CallBluetoothHelper(ApplicationController application) {
        this.mApplication = application;
        this.audioManager = (AudioManager) mApplication.getSystemService(Context.AUDIO_SERVICE);
        bluetoothReceiver = new BluetoothHeadsetReceiver();
        bluetoothServiceListener = new BluetoothServiceListener();
        handler = new Handler(Looper.getMainLooper());
    }

    public static synchronized CallBluetoothHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new CallBluetoothHelper(application);
        }
        return mInstance;
    }

    public boolean isCurrentDisableSpeaker() {
        return currentDisableSpeaker;
    }

    private class BluetoothServiceListener implements BluetoothProfile.ServiceListener {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            Log.d(TAG, "onServiceConnected: " + bluetoothState);
            if (profile != BluetoothProfile.HEADSET || bluetoothState == State.UNINITIALIZED) {
                return;
            }
            bluetoothState = State.SCO_CONNECTING;
            bluetoothHeadset = (BluetoothHeadset) proxy;
            tryingConnectBluetooth();
            updateState();
            //updateAudioDeviceState();
        }

        @Override
        public void onServiceDisconnected(int profile) {
            Log.d(TAG, "onServiceDisconnected: " + bluetoothState);
            if (profile != BluetoothProfile.HEADSET || bluetoothState == State.UNINITIALIZED) {
                return;
            }
            cancelTimer();
            bluetoothHeadset = null;
            bluetoothState = State.SCO_DISCONNECTED;
            updateState();
            /*
            Log.e(TAG, "BluetoothServiceListener.onServiceDisconnected: BT state=" + bluetoothState);
            stopScoAudio();

            bluetoothState = State.HEADSET_UNAVAILABLE;
            updateAudioDeviceState();*/
        }
    }

    public void onBluetoothStateChanged(String action, int state) {
        if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
            if (state == BluetoothHeadset.STATE_CONNECTED) {
                bluetoothState = State.SCO_CONNECTED;
                tryingConnectBluetooth();
            } else if (state == BluetoothHeadset.STATE_CONNECTING) {
                bluetoothState = State.SCO_CONNECTING;
            } else if (state == BluetoothHeadset.STATE_DISCONNECTED) {
                bluetoothState = State.SCO_DISCONNECTED;
            }
        } else {
            if (state == BluetoothHeadset.STATE_CONNECTED) {
                if (bluetoothState == State.SCO_CONNECTING) {
                    Log.e(TAG, "+++ Bluetooth audio SCO is now connected");
                    bluetoothState = State.SCO_CONNECTED;
                    tryingConnectBluetooth();
                }
            }
        }
        updateState();
    }

    public void start() {
        Log.d(TAG, "start");
        isRegistedReceiver = false;
        if (bluetoothState != State.UNINITIALIZED) {
            Log.d(TAG, "Invalid BT state");
            return;
        }
        if (!PermissionHelper.hasPermissionBluetooth(mApplication)) {
            Log.d(TAG, "lacks BLUETOOTH permission");
            return;
        }
        if (!audioManager.isBluetoothScoAvailableOffCall()) {
            Log.d(TAG, "SCO audio is not available off call");
            return;
        }
        bluetoothHeadset = null;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Log.d(TAG, "Device does not support Bluetooth");
            return;
        }
        if (!bluetoothAdapter.getProfileProxy(mApplication, bluetoothServiceListener, BluetoothProfile.HEADSET)) {
            Log.d(TAG, "BluetoothAdapter.getProfileProxy(HEADSET) failed");
            return;
        }
        currentDisableSpeaker = false;
        IntentFilter bluetoothHeadsetFilter = new IntentFilter();
        bluetoothHeadsetFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
        bluetoothHeadsetFilter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED);
        mApplication.registerReceiver(bluetoothReceiver, bluetoothHeadsetFilter);
        Log.d(TAG, "registerReceiver -------");
        //audioManager.startBluetoothSco();
        //audioManager.setBluetoothScoOn();
        isRegistedReceiver = true;
        bluetoothState = State.NON;
    }

    public void stop() {
        Log.d(TAG, "stop");
        bluetoothState = State.UNINITIALIZED;
        cancelTimer();
        if (!PermissionHelper.hasPermissionBluetooth(mApplication)) {
            Log.d(TAG, "BLUETOOTH permission");
            return;
        }
        try {
            if (isRegistedReceiver) {
                mApplication.unregisterReceiver(bluetoothReceiver);
                isRegistedReceiver = false;
            }
            Log.d(TAG, "stopBluetoothSco");
            audioManager.stopBluetoothSco();
            audioManager.setBluetoothScoOn(false);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        /*if (bluetoothHeadset != null && bluetoothAdapter != null) {
            bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);
            bluetoothHeadset = null;
        }*/

        if (bluetoothAdapter != null) {
            bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);
            bluetoothHeadset = null;
            bluetoothAdapter = null;
        }
        currentDisableSpeaker = false;
    }

    private void startTimer() {
        handler.postDelayed(bluetoothRunnable, BLUETOOTH_SCO_TIMEOUT_MS);
    }

    private void cancelTimer() {
        handler.removeCallbacks(bluetoothRunnable);
    }

    private boolean isBluetoothReady() {// uu tien tai nghe thuong
        return !(bluetoothHeadset == null || bluetoothHeadset.getConnectedDevices().isEmpty()) && !audioManager.isWiredHeadsetOn();
    }

    private void tryingConnectBluetooth() {
        if (bluetoothState != State.UNINITIALIZED && isBluetoothReady()) {
            Log.d(TAG, "tryingConnectBluetooth: " + audioManager.isBluetoothScoOn());
            try {
                if (!audioManager.isBluetoothScoOn())
                    audioManager.startBluetoothSco();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            //startTimer(); //định retry start bluetooth
        } else {
            Log.d(TAG, "tryingConnectBluetooth not ready");
        }
    }

    public void updateStateWhenWiredHeadsetChanged(boolean isPlug) {
        if (bluetoothState == State.UNINITIALIZED) {
            return;
        }
        if (isPlug) {
            cancelTimer();
            try {
                Log.d(TAG, "stopBluetoothSco");
                audioManager.stopBluetoothSco();
                //audioManager.setBluetoothScoOn(false);
                bluetoothState = State.SCO_DISCONNECTED;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        } else {
            tryingConnectBluetooth();
        }
        updateState();
    }

    private void updateState() {
        if (bluetoothState != State.NON && bluetoothState != State.UNINITIALIZED && isBluetoothReady()) {
            currentDisableSpeaker = true;
            mApplication.getCallBusiness().onBluetoothChange(true);
        } else {
            currentDisableSpeaker = false;
            mApplication.getCallBusiness().onBluetoothChange(false);
        }
    }
}
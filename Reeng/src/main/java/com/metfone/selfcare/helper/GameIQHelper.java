package com.metfone.selfcare.helper;

import com.metfone.selfcare.app.dev.ApplicationController;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.packet.Presence;

/**
 * Created by thanhnt72 on 8/29/2018.
 */

public class GameIQHelper {

    private static final String TAG = GameIQHelper.class.getSimpleName();

    public static final long DURATION_ANIM = 500;

    private static GameIQHelper mInstant;
    private ApplicationController mApp;

    public static synchronized GameIQHelper getInstant(ApplicationController mApp) {
        if (mInstant == null) {
            mInstant = new GameIQHelper(mApp);
        }
        return mInstant;
    }

    public GameIQHelper(ApplicationController mApp) {
        this.mApp = mApp;
    }


    public void processViewerPlayerIncoming(Presence presence) {
        String idGame = presence.getIdGameSub();
        long playNumb = presence.getPlayNumb();
        long playWatch = presence.getWatchNumb();
        EventBus.getDefault().post(new IQGameEvent(idGame, playNumb, playWatch));
    }


    public static class IQGameEvent {
        String idGame;
        long playNumb;
        long viewNumb;

        public IQGameEvent(String idGame, long playNumb, long playWatch) {
            this.idGame = idGame;
            this.playNumb = playNumb;
            this.viewNumb = playWatch;
        }

        public String getIdGame() {
            return idGame;
        }

        public long getPlayNumb() {
            return playNumb;
        }

        public long getViewNumb() {
            return viewNumb;
        }

        @Override
        public String toString() {
            return "IQGameEvent{" +
                    "idGame='" + idGame + '\'' +
                    ", playNumb=" + playNumb +
                    ", viewNumb=" + viewNumb +
                    '}';
        }
    }

}

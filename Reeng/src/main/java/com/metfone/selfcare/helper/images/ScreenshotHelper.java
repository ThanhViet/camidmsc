package com.metfone.selfcare.helper.images;

import android.graphics.Bitmap;
import android.view.View;

import com.metfone.selfcare.util.Log;

import java.io.ByteArrayOutputStream;

/**
 * Created by bigant on 4/17/2015.
 */
public class ScreenshotHelper {
    private static final String TAG = ScreenshotHelper.class.getSimpleName();

    public static Bitmap takeScreenShotForView(View v) {
        try {
            View v1 = v;
            //Manually generate a bitmap copy of the view
            v1.setDrawingCacheEnabled(true);
            //Measure Pass : For deciding dimension specifications for view
//            v1.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//            //Layout Pass :  During this pass, each parent is responsible for positioning all of its children using
// the sizes computed in the measure pass
//            v1.layout(0, 0, v1.getMeasuredWidth(), v1.getMeasuredHeight());
            //Force the drawing cache to be built
//            v1.buildDrawingCache(true);
            //Retrieve the view to be saved using :
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            //Compress the bitmap and use 100 as factor for maintaining the quality. If you can compromise with
            // quality, reduce the factor
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
            v1.destroyDrawingCache();
            return bitmap;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
    }
}
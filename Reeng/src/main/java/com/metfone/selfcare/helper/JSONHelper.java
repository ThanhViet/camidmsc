package com.metfone.selfcare.helper;


import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class JSONHelper {
    private static final String TAG = "JSONHelper";
    // constants for generate password code
    private static final String CODE = "code";
    private static final String DESC = "desc";
    // constants for auto detect phone number

    public static Map<String, Object> toMap(JSONObject object)
            throws JSONException {
        Map<String, Object> map = new HashMap<>();
        Iterator keys = object.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            map.put(key, fromJson(object.get(key)));
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            list.add(fromJson(array.get(i)));
        }
        return list;
    }

    private static Object fromJson(Object json) throws JSONException {
        if (json == JSONObject.NULL) {
            return null;
        } else if (json instanceof JSONObject) {
            return toMap((JSONObject) json);
        } else if (json instanceof JSONArray) {
            return toList((JSONArray) json);
        } else {
            return json;
        }
    }

    /**
     * get account info from json object
     *
     * @param jsonString
     * @return
     * @author thanhnt
     */
    public static int getResponseFromJSON(String jsonString) {
        int mResponseCode = -1;
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.has(CODE) && jsonObject.has(DESC)) {
                mResponseCode = jsonObject.getInt(CODE);
            }
        } catch (Exception e) {
            Log.e(TAG, "getResponseCodeFromJSON", e);
        }
        return mResponseCode;
    }


    public static String getResponseDescFromJSON(String jsonString) {
        String desc = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.has(DESC)) {
                desc = jsonObject.getString(DESC);
            }
        } catch (Exception e) {
            Log.e(TAG, "getResponseDescFromJSON", e);
        }
        return desc;
    }

    public static String getString(JSONObject object, String key, String defaultValue) {
        String ret = defaultValue;
        try {
            ret = object.getString(key);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return ret;
    }

    public static boolean getBoolean(JSONObject object, String key, boolean defaultValue) {
        boolean ret = defaultValue;
        try {
            ret = object.getBoolean(key);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return ret;
    }

    public static int getInt(JSONObject object, String key, int defaultValue) {
        int ret = defaultValue;
        try {
            ret = object.getInt(key);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return ret;
    }
}
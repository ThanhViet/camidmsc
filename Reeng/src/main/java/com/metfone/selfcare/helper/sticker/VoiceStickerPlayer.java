package com.metfone.selfcare.helper.sticker;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.util.Log;

import java.io.IOException;

/**
 * Created by ThanhNT on 12/31/2014.
 */
public class VoiceStickerPlayer implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener {
    private static final String TAG = VoiceStickerPlayer.class.getSimpleName();
    private ApplicationController application;
    //    private int stickerWeight;
    private MediaPlayer player;
    private AssetFileDescriptor mAssetFileDesc;

    public VoiceStickerPlayer(ApplicationController application) {
        this.application = application;
        //      stickerWeight = (int) (mContext.getResources().getDimensionPixelSize(R.dimen.sticker_grid_height));
        player = new MediaPlayer();
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
        //      player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
    }

    private void playVoice(String assetPath) {
        try {
            mAssetFileDesc = application.getAssets().openFd(assetPath);
            player.reset();
            player.setDataSource(mAssetFileDesc.getFileDescriptor(), mAssetFileDesc.getStartOffset(), mAssetFileDesc
                    .getLength());
            mAssetFileDesc.close();
            player.prepare();
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }
    }

    public void playVoiceSticker(int collectionId, int itemId) {
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            StickerItem defaultStickerByItemId = EmoticonUtils.getDefaultStickerByItemId(itemId);
            if (defaultStickerByItemId != null) {
                String soundFilePath = defaultStickerByItemId.getVoicePath();
                Log.i(TAG, "path: " + soundFilePath);
                playVoiceFromAsset(soundFilePath, "mp3");
            }
        } else {
            String filePath;
            StickerItem stickerItem = application.getStickerBusiness().
                    getStickerItem(collectionId, itemId);
            if (stickerItem != null && stickerItem.isDownloadVoice()
                    && !TextUtils.isEmpty(stickerItem.getVoicePath())) {
                filePath = stickerItem.getVoicePath();
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(Config.Storage.REENG_STORAGE_FOLDER).append(Config.Storage.STICKER_FOLDER)
                        .append(collectionId).append("/").append(itemId).append(".mp3");
                filePath = sb.toString();
            }
            playVoiceFromSdcard(filePath);
        }

    }

    private void playVoiceFromAsset(String assetPath, String typeFile) {
        try {
            mAssetFileDesc = application.getAssets().openFd(assetPath);
            player.reset();
            player.setDataSource(mAssetFileDesc.getFileDescriptor(), mAssetFileDesc.getStartOffset(), mAssetFileDesc
                    .getLength());
            player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mAssetFileDesc.close();
            player.prepare();
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void playVoiceFromSdcard(String voicePath) {
        try {
            player.reset();
            player.setDataSource(voicePath);
            player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            player.prepare();
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void stopVoice() {
        if (application.getPlayMusicController() != null) {
            application.getPlayMusicController().checkAndResumeMusic();
        }
        try {
            player.stop();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "onPrepared");
        /*if (mMusicService.isPaused()) {
            isMusicPlaying = false;
        } else*/
        if (application.getPlayMusicController() != null) {
            if (application.getPlayMusicController().isPlaying()) {
                application.getPlayMusicController().setStateResumePlaying(true);
                application.getPlayMusicController().toggleMusic();
            }
            application.getPlayMusicController().setOtherAudioPlaying(true);
        }
        player.start();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onCompletion");
        if (application.getPlayMusicController() != null) {
            application.getPlayMusicController().checkAndResumeMusic();
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.d(TAG, "onError");
        if (application.getPlayMusicController() != null) {
            application.getPlayMusicController().checkAndResumeMusic();
        }
        return false;
    }

    public void freeSource() {
        //        mHandler = null;
        player.release();
        player = null;
    }
}
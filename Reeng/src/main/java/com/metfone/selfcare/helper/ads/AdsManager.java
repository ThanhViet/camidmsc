package com.metfone.selfcare.helper.ads;

import android.app.Activity;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.vtm.adslib.AdsBannerHelper;
import com.vtm.adslib.AdsBannerMediumHelper;
import com.vtm.adslib.AdsInterstitialHelper;
import com.vtm.adslib.AdsListener;
import com.vtm.adslib.AdsNativeHelper;
import com.vtm.adslib.AdsRewardedHelper;
import com.vtm.adslib.template.TemplateView;

import java.util.Random;

public class AdsManager {
    private Activity context;

    private AdsManager() {
    }

    public static AdsManager getInstance() {
        return AdsManager.SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final AdsManager INSTANCE = new AdsManager();
    }

    public void initAds(Activity context, String adAppId) {
        this.context = context;
        if (!TextUtils.isEmpty(adAppId))
            MobileAds.initialize(context, adAppId);
    }

    public void initAdsBanner(String adUnit, AdSize adSize) {
        if (AdsUtils.checkShowAds())
            AdsBannerHelper.getInstance().init(context, adUnit, adSize);
    }

    public void initAdsBannerMedium(String adUnit, AdSize adSize) {
        if (AdsUtils.checkShowAds())
            AdsBannerMediumHelper.getInstance().init(context, adUnit, adSize);
    }

    public void initAdsFullScreen(String adUnit) {
        if (AdsUtils.checkShowAds())
            AdsInterstitialHelper.getInstance().init(context, adUnit);
    }

    public void initAdsReward(String adUnit) {
        if (AdsUtils.checkShowAds())
            AdsRewardedHelper.getInstance().init(context, adUnit);
    }

    public void initAdsNative(String adUnit) {
        if (AdsUtils.checkShowAds())
            AdsNativeHelper.getInstance().initAds(context, adUnit);
    }

    public void showAdsBanner(ViewGroup adContainer, AdsListener adsListener)
    {
        if (AdsUtils.checkShowAds())
            AdsBannerHelper.getInstance().showAd(adContainer, adsListener);
        else
        {
            if(adsListener != null)
                adsListener.onAdClosed();
        }
    }

    public void showAdsBannerMedium(ViewGroup adContainer, AdsListener adsListener)
    {
        if (AdsUtils.checkShowAds())
            AdsBannerMediumHelper.getInstance().showAd(adContainer, adsListener);
        else
        {
            if(adsListener != null)
                adsListener.onAdClosed();
        }
    }

    public void showAdsFullScreen(AdsListener adsListener)
    {
        if (AdsUtils.checkShowAds())
            AdsInterstitialHelper.getInstance().showInterstitialAd(adsListener);
        else
        {
            if(adsListener != null)
                adsListener.onAdClosed();
        }
    }

    public boolean showAdsReward(AdsRewardedHelper.AdsRewardListener adsListener) {
        if (AdsUtils.checkShowAds()) {
            AdsRewardedHelper.getInstance().showAd(adsListener);
            return true;
        }
        return false;
    }

    public void showAdsNative(TemplateView adContainer)
    {
        if (AdsUtils.checkShowAds())
            AdsNativeHelper.getInstance().showAd(adContainer);
    }

    public void reloadAdsNative()
    {
        if (AdsUtils.checkShowAds())
            AdsNativeHelper.getInstance().loadAd();
    }

    public void showAdsFullScreenHome() {
        try {
            boolean isAdShowHome = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_FULL_SHOW_HOME) == 1;
            if (isAdShowHome) {
                long timeDisplayAds = ApplicationController.self().getPref().getLong(AdsUtils.KEY_FIREBASE.AD_DISPLAY_TIME, 0L);
                if (!AdsUtils.checkTimeInDay(timeDisplayAds)) // Check moi ngay hien mot lan
                {
                    ApplicationController.self().getPref().edit().putBoolean(AdsUtils.KEY_FIREBASE.AD_DISPLAY_FIRST, false).apply();
                }
                boolean isShowAdFirst = ApplicationController.self().getPref().getBoolean(AdsUtils.KEY_FIREBASE.AD_DISPLAY_FIRST, false);
                long ranFirst = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_FULL_RAN_FIRST);
                long ranSecond = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_FULL_RAN_SECOND);
                if (ranFirst == 0) ranFirst = 2;
                if (ranSecond == 0) ranSecond = 4;

                Random ran = new Random();
                int ranIndex = ran.nextInt((int) ranFirst);
                if (isShowAdFirst) //Neu da show 1 lan trong ngay roi thi xac suat hien thap hon
                    ranIndex = ran.nextInt((int) ranSecond);

                if (ranIndex == 1) {
                    AdsInterstitialHelper.getInstance().showInterstitialAd(new AdsListener() {
                        @Override
                        public void onAdClosed() {

                        }

                        @Override
                        public void onAdShow() {
                            ApplicationController.self().getPref().edit().putBoolean(AdsUtils.KEY_FIREBASE.AD_DISPLAY_FIRST, true).apply();
                            ApplicationController.self().getPref().edit().putLong(AdsUtils.KEY_FIREBASE.AD_DISPLAY_TIME, System.currentTimeMillis()).apply();
                        }
                    });
                }
            }
        } catch (Exception ex) {
        }
    }


}

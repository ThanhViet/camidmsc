package com.metfone.selfcare.helper.sticker;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by toanvk2 on 2/9/15.
 */
public class StickerHelper {
    private static final String TAG = StickerHelper.class.getSimpleName();

    public static File createSubFileSticker(StickerCollection stickerCollection) {
        /*if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return null;
        }
        String stickerPath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.STICKER_FOLDER
                + String.valueOf(stickerCollection.getServerId()) + "/";
        File stickerFile = new File(stickerPath);
        if (!stickerFile.exists()) {
            stickerFile.mkdirs();
        }
        return stickerFile;*/
        return findOrCreateSubFileSticker(stickerCollection.getServerId());
    }

    public static File findOrCreateSubFileSticker(int collectionId) {
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return null;
        }
        String stickerPath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.STICKER_FOLDER
                + String.valueOf(collectionId) + "/";
        File stickerFile = new File(stickerPath);
        if (!stickerFile.exists()) {
            stickerFile.mkdirs();
        }
        return stickerFile;
    }


    public static String createStickerZipFilePath(StickerCollection stickerCollection) {
        File stickerSubFile = createSubFileSticker(stickerCollection);
        if (stickerSubFile == null) {
            return null;
        } else {
            return stickerSubFile.getPath() + "/" + String.valueOf(stickerCollection.getServerId())
                    + Constants.FILE.ZIP_FILE_SUFFIX;
        }
    }

    public static String getUrlDownloadStickerPacket(StickerCollection stickerCollection,
                                                     ApplicationController application) {
        ReengAccount account = application.getReengAccountBusiness().
                getCurrentAccount();
        if (account == null || !account.isActive() || TextUtils.isEmpty(account.getToken())) {
            return null;
        } else {
            long currentTime = TimeHelper.getCurrentTime();
            long stickerCollectionId = stickerCollection.getServerId();
            String myNumberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
            String token = account.getToken();
            StringBuilder sb = new StringBuilder().
                    append(account.getJidNumber()).
                    append(stickerCollectionId).
                    append(token).
                    append(currentTime);
            String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(application, sb.toString(), token));
            String url = UrlConfigHelper.getInstance(application).
                    getUrlConfigOfFile(Config.UrlEnum.DOWNLOAD_STICKER_COLLECTION) + "&modifyDate=%5$s&modifyItem=%6$s";
            return String.format(url,
                    myNumberEncode,
                    String.valueOf(stickerCollectionId),
                    String.valueOf(currentTime),
                    dataEncrypt,
                    stickerCollection.getLastInfoUpdate(),
                    stickerCollection.getLastServerUpdate());
            /*String url = UrlConfigHelper.getInstance(application).
                    getConfigDomainFile() + "/ReengBackendBiz/downloadSticker/v21?msisdn=%1$s&stickerid=%2$s&timestamp=%3$s&security=%4$s&modifyDate=%5$s&modifyItem=%6$s";
            return String.format(url,
                    myNumberEncode,
                    String.valueOf(stickerCollectionId),
                    String.valueOf(currentTime),
                    dataEncrypt,
                    stickerCollection.getLastInfoUpdate(),
                    stickerCollection.getLastServerUpdate());*/
        }
    }

    public static String getUrlStickerItem(Context context, int collectionId, int itemId) {
        StringBuilder sb = new StringBuilder();
        String domainFile = UrlConfigHelper.getInstance(context).getDomainFile();
        sb.append(domainFile).append(Config.PREFIX.STICKER).append("/").
                append(collectionId).append("/").append(itemId).append(".png");
        return sb.toString();
    }

    /**
     * unzip file
     */
    public static void extractZipFile(ApplicationController application, int stickerCollection,
                                      String zipFilePath, String extraFilePath) {
        if (TextUtils.isEmpty(zipFilePath) || TextUtils.isEmpty(extraFilePath)) {
            return;
        }
        // kiem tra neu the nho chua dc mounted thi ko unzip
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return;
        }
        InputStream inStream = null;
        ZipInputStream zipStream = null;
        BufferedInputStream bis = null;
        // find or create extraFolder
        File zipFile = new File(zipFilePath);
        File extraFile = new File(extraFilePath);
        try {
            if (!extraFile.isDirectory()) {
                extraFile.mkdirs();
            }
            if (!zipFile.exists()) {// file zip khong ton tai
                return;
            }
            // get input stream from zip file
            inStream = new FileInputStream(zipFile);
            // create zip stream from input stream
            bis = new BufferedInputStream(inStream);
            zipStream = new ZipInputStream(bis);
            // the zip entry instance is used to traverse all items in the zip_file
            ZipEntry entry;
            // work with a buffer to improve speed
            int size;
            byte[] buffer = new byte[Constants.FILE.BUFFER_SIZE_DEFAULT];
            String ePath = extraFile.getPath();
            // loop all zip entries here
            while ((entry = zipStream.getNextEntry()) != null) {
                FileOutputStream outStream = null;
                BufferedOutputStream bufferOut = null;
                // let's have a try/catch in the loop so we don't break all code
                // if one file fails
                try {
                    // prepare file name to be created
                    File outFile = new File(ePath + "/" + entry.getName());
                    // create dirs where needed
                    if (entry.isDirectory()) {
                        outFile.mkdirs();
                    } else { // unpack files
                        outStream = new FileOutputStream(outFile);
                        bufferOut = new BufferedOutputStream(outStream, buffer.length);
                        while ((size = zipStream.read(buffer, 0, buffer.length)) != -1) {
                            bufferOut.write(buffer, 0, size);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                } finally {
                    // clean up
                    try {
                        if (bufferOut != null) {
                            bufferOut.flush();
                            bufferOut.close();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                    try {
                        if (outStream != null) {
                            outStream.flush();
                            outStream.close();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
                // delete file zip after unzip
                // zipFile.delete();
            }
            // get info from json.txt
            application.getStickerBusiness().getStickerInfoFromFile(stickerCollection);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            try {
                if (zipStream != null) {
                    zipStream.close();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (bis != null) {
                    bis.close();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (inStream != null) {
                    inStream.close();
                }
                // delete file zip after unzip
                zipFile.delete();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }
}
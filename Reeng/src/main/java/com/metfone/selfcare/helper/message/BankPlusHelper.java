package com.metfone.selfcare.helper.message;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bplus.sdk.BpGift;
import com.bplus.sdk.BpTransfer;
import com.bplus.sdk.BpTransferResult;
import com.bplus.sdk.BplusSdk;
import com.bplus.sdk.ResultListener;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.BPlusResult;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogTransferBankPlus;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PopupReceiveLixi;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 10/23/2017.
 */

public class BankPlusHelper {
    private static final String TAG = BankPlusHelper.class.getSimpleName();
    private static final String TAG_LIXI_GET_LIST_IMAGE = "TAG_LIXI_GET_LIST_IMAGE";
    private static final String TAG_LIXI_OPEN = "TAG_LIXI_OPEN";
    private static final String TAG_LOG_BANKPLUS_FAIL = "TAG_LOG_BANKPLUS_FAIL";
    private static BankPlusHelper mInstance;
    private ApplicationController mApplication;
    private Resources mRes;

    public static synchronized BankPlusHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new BankPlusHelper(application);
        }
        return mInstance;
    }

    private BankPlusHelper(ApplicationController application) {
        this.mApplication = application;
        this.mRes = mApplication.getResources();
        BplusSdk.init(Config.Extras.B_PLUS_ID, false);
        BplusSdk.setMerchantSecureKey(Config.Extras.B_PLUS_MERCHANT);
        BplusSdk.setAccessCode(Config.Extras.B_PLUS_ACCESS_CODE);
    }

    public void checkAndTransferMoney(final BaseSlidingFragmentActivity activity, final String friendJid, final
    String friendName) {
        checkAndTransferMoney(activity, friendJid, friendName, -1, null, null);
    }

    public void checkAndTransferMoney(final BaseSlidingFragmentActivity activity, final String friendJid,
                                      final String friendName, final long amount, final String desc, final String
                                              transferId) {
        Log.d(TAG, "checkAndTransferMoney: " + friendJid);
        if (NetworkHelper.isConnectInternet(mApplication) && mApplication.getReengAccountBusiness().isValidAccount()) {
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            activity.showLoadingDialog(null, R.string.bplus_waiting);
            new RequestCheckUserBPlusAsyncTask(mApplication, activity, myNumber, new CheckAccountCallBack() {
                @Override
                public void onResult(boolean isUserBPlus) {
                    activity.hideLoadingDialog();
                    if (isUserBPlus) {
                        checkBPlusFriend(activity, friendJid, friendName, amount, desc, transferId);
                    } else {
                        showDialogRegisterAndTransferBPlus(activity, friendJid, friendName, amount, desc, transferId);
                    }
                }

                @Override
                public void onError(int errorCode) {
                    activity.hideLoadingDialog();
                    activity.showToast(R.string.e601_error_but_undefined);
                }
            }).execute();
        } else if (!NetworkHelper.isConnectInternet(mApplication)) {
            activity.showToast(R.string.error_internet_disconnect);
        } else {
            activity.showToast(R.string.e601_error_but_undefined);
        }
    }

    private void showDialogRegisterAndTransferBPlus(final BaseSlidingFragmentActivity activity, final String friendJid,
                                                    final String friendName, final long amount, final String desc,
                                                    final String transferId) {
        DialogConfirm dialog = new DialogConfirm(activity, true)
                .setLabel(null)
                .setMessage(mRes.getString(R.string.bplus_msg_register))
                .setNegativeLabel(mRes.getString(R.string.non_skip))
                .setPositiveLabel(mRes.getString(R.string.non_register))
                .setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        BplusSdk.register(activity, new ResultListener<String>() {
                            @Override
                            public void success(String s) {
                                Log.d(TAG, "BplusSdk.register success - s: " + s);
                                checkBPlusFriend(activity, friendJid, friendName, amount, desc, transferId);
                            }

                            @Override
                            public void failed(int i, String s) {// không thông báo gì, sdk b+ thông báo
                                Log.d(TAG, "BplusSdk.register failed: " + i + " - s: " + s);
                            }
                        });
                    }
                });
        dialog.show();
    }

    private void checkBPlusFriend(final BaseSlidingFragmentActivity activity, final String friendJid,
                                  final String friendName, final long amount, final String desc, final String
                                          transferId) {
        activity.showLoadingDialog(null, R.string.bplus_waiting);
        new RequestCheckUserBPlusAsyncTask(mApplication, activity, friendJid, new CheckAccountCallBack() {
            @Override
            public void onResult(boolean isUserBPlus) {
                activity.hideLoadingDialog();
                if (isUserBPlus) {
                    if (amount > 0 && !TextUtils.isEmpty(desc) && !TextUtils.isEmpty(transferId)) {
                        String amountStr = TextHelper.formatTextDecember(String.valueOf(amount));
                        BPlusResult bpItem = new BPlusResult(friendJid, friendName, amount, amountStr, desc,
                                transferId, DialogTransferBankPlus.TAB_PAY);
                        handlePayMoney(activity, bpItem);
                    } else {
                        showDialogInputTransferBPlus(activity, friendJid, friendName);
                    }
                } else {
                    String content = String.format(activity.getResources().getString(R.string
                            .bplus_msg_friend_not_used), friendName, friendName);
                    insertMessageNotify(friendJid, content);
                }
            }

            @Override
            public void onError(int errorCode) {
                activity.hideLoadingDialog();
                activity.showToast(R.string.e601_error_but_undefined);
            }
        }).execute();
    }

    private void showDialogInputTransferBPlus(final BaseSlidingFragmentActivity activity, final String friendJid,
                                              String friendName) {
        DialogTransferBankPlus dialog = new DialogTransferBankPlus(activity, true)
                .setFriendJid(friendJid).setFriendName(friendName)
                .setPositiveListener(new PositiveListener<BPlusResult>() {
                    @Override
                    public void onPositive(final BPlusResult result) {
                        if (result.getType() == DialogTransferBankPlus.TAB_PAY) {
                            handlePayMoney(activity, result);
                        } else {
                            sendMessageBPlusSuccess(result, ReengMessageConstant.BPLUS_CLAIM);
                        }
                    }
                }).setDismissListener(new DismissListener() {
                    @Override
                    public void onDismiss() {
                        activity.hideKeyboard();
                    }
                });
        dialog.show();
    }

    private class RequestCheckUserBPlusAsyncTask extends AsyncTask<Void, Void, Void> {
        private ApplicationController application;
        private BaseSlidingFragmentActivity mActivity;
        private String friendJid;
        private CheckAccountCallBack mCallBack;
        private int state = -1;

        private RequestCheckUserBPlusAsyncTask(ApplicationController application,
                                               BaseSlidingFragmentActivity activity,
                                               String friendJid,
                                               CheckAccountCallBack callBack) {
            this.application = application;
            this.mActivity = activity;
            this.friendJid = friendJid;
            this.mCallBack = callBack;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                BplusSdk.setUserPhoneNumber(application.getReengAccountBusiness().getJidNumber());
                boolean isBaPlus = BplusSdk.checkAccount(mActivity, friendJid);
                state = isBaPlus ? 1 : 0;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                state = -2;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.d(TAG, "RequestCheckUserBPlusAsyncTask state: " + state);
            if (state >= 0) {
                mCallBack.onResult(state == 1);
            } else {
                mCallBack.onError(-1);
            }
        }
    }

    /*public void checkUserBPlus(final BaseSlidingFragmentActivity activity, final String friendJid,
    CheckAccountCallBack callBack) {
        if (NetworkHelper.isConnectInternet(mApplication) && mApplication.getReengAccountBusiness().isValidAccount()) {
            new RequestCheckUserBPlusAsyncTask(mApplication, activity, friendJid, callBack).execute();
        } else {
            callBack.onError(-1);
        }
    }

    public void transferMoney(BaseSlidingFragmentActivity activity, BpTransfer item, ResultListener<BpTransferResult>
     listener) {
        BplusSdk.transfer(activity, item, listener);
    }

    private void testBankPlus(ReengAccountBusiness mAccountBusiness, final BaseSlidingFragmentActivity
    mParentActivity) {

        final String myNumber = mAccountBusiness.getJidNumber();
        BplusSdk.init(Config.Extras.B_PLUS_ID, true);
        BplusSdk.setMerchantSecureKey(Config.Extras.B_PLUS_MERCHANT);
        BplusSdk.setUserPhoneNumber(myNumber);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    boolean isBaPlus = BplusSdk.checkAccount(mParentActivity, myNumber);
                    Log.d(TAG, "BplusSdk. checkAccount: " + isBaPlus);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                return null;
            }
        }.execute();
    }*/

    private void handlePayMoney(BaseSlidingFragmentActivity activity, final BPlusResult result) {
        //TODO test
        try {
            BpTransfer bpTransfer = new BpTransfer(result.getDesc(), result.getAmount(), result.getFriendJid(),
                    result.getTransferId());
            BplusSdk.transfer(activity, bpTransfer, new ResultListener<BpTransferResult>() {
                @Override
                public void success(BpTransferResult bpTransferResult) {
                    Log.d(TAG, "BplusSdk.transfer success - s: " + bpTransferResult.toString());
                    sendMessageBPlusSuccess(result, ReengMessageConstant.BPLUS_PAY);
                    //sendMessageLogTransfer(friendJid, result);
                }

                @Override
                public void failed(int i, String s) {
                    Log.d(TAG, "BplusSdk.transfer failed - s: " + s);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            activity.showToast(R.string.e601_error_but_undefined);
        }
    }

    private void insertMessageNotify(String friendJid, String content) {
        String myJid = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingSoloThread(friendJid);
        ReengMessage messageNotify = mApplication.getMessageBusiness().
                insertMessageNotify(content, friendJid, myJid, thread, ReengMessageConstant.READ_STATE_READ,
                        TimeHelper.getCurrentTime());
        mApplication.getMessageBusiness().notifyNewMessage(messageNotify, thread);
    }

    private void sendMessageBPlusSuccess(BPlusResult result, String bPlusType) {
        String friendJid = result.getFriendJid();
        String myJid = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingSoloThread(friendJid);
        ReengMessage message = mApplication.getMessageBusiness().createMessageBankPlus(myJid, friendJid, thread,
                String.valueOf(result.getAmount()), result.getDesc(), result.getTransferId(),
                bPlusType, TimeHelper.getCurrentTime());
        String msg;
        if (ReengMessageConstant.BPLUS_PAY.equals(message.getFilePath())) {
            msg = mRes.getString(R.string.bplus_holder_pay_send);
        } else {
            msg = mRes.getString(R.string.bplus_holder_claim_send);
        }
        message.setContent(msg);
        mApplication.getMessageBusiness().insertNewMessageToDB(thread, message);
        mApplication.getMessageBusiness().notifyNewMessage(message, thread);
        mApplication.getMessageBusiness().sendXMPPMessage(message, thread);
    }

    private void sendMessageLogTransfer(String friendJid, BpTransfer entry) {
        String content = "Bạn đã chuyển thành công " + entry.getPrice() + " VNĐ cho số điện thoại: " + friendJid;
        String myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(friendJid);
        SoloSendTextMessage message = new SoloSendTextMessage(thread, myJidNumber, friendJid, content);
        message.setPacketId(entry.getOrderId());
        mApplication.getMessageBusiness().sendXMPPMessage(message, thread);
        mApplication.getMessageBusiness().insertNewMessageToDB(thread, message);
        mApplication.getMessageBusiness().notifyNewMessage(message, thread);
    }


    public void checkAndSendLixi(final ChatActivity activity, final ArrayList<String> listFriendJid,
                                 final int threadId) {
        if (mApplication.getReengAccountBusiness().isValidAccount()) {
            if (NetworkHelper.isConnectInternet(mApplication)) {
                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                activity.showLoadingDialog(null, R.string.bplus_waiting);
                new RequestCheckUserBPlusAsyncTask(mApplication, activity, myNumber, new CheckAccountCallBack() {
                    @Override
                    public void onResult(boolean isUserBPlus) {
                        activity.hideLoadingDialog();
                        if (isUserBPlus) {
                            activity.navigateToSendGiftLixi(listFriendJid, threadId);
                        } else {
                            showDialogRegisterAndSendGiftLixi(activity, listFriendJid, threadId);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        activity.hideLoadingDialog();
                        activity.showToast(R.string.e601_error_but_undefined);
                    }
                }).execute();
            } else if (!NetworkHelper.isConnectInternet(mApplication)) {
                activity.showToast(R.string.error_internet_disconnect);
            }
        } else {
            activity.showToast(R.string.e601_error_but_undefined);
        }
    }

    private void showDialogRegisterAndSendGiftLixi(final ChatActivity activity,
                                                   final ArrayList<String> listFriendJid, final int threadId) {
        DialogConfirm dialog = new DialogConfirm(activity, true)
                .setLabel(null)
                .setMessage(mRes.getString(R.string.bplus_msg_register))
                .setNegativeLabel(mRes.getString(R.string.non_skip))
                .setPositiveLabel(mRes.getString(R.string.non_register))
                .setPositiveListener(new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        BplusSdk.register(activity, new ResultListener<String>() {
                            @Override
                            public void success(String s) {
                                Log.d(TAG, "BplusSdk.register success - s: " + s);
                                activity.navigateToSendGiftLixi(listFriendJid, threadId);
//                                checkBPlusFriend(activity, friendJid, friendName, amount, desc, transferId);
                            }

                            @Override
                            public void failed(int i, String s) {// không thông báo gì, sdk b+ thông báo
                                Log.d(TAG, "BplusSdk.register failed: " + i + " - s: " + s);
                            }
                        });
                    }
                });
        dialog.show();
    }

    public void showDialogLixi(BaseSlidingFragmentActivity mActivity, ThreadMessage threadMessage,
                               ReengMessage message, PopupReceiveLixi.LixiListener listener) {
        new PopupReceiveLixi(mActivity, threadMessage, message, listener).show();
    }

    public void sendRequestLixi(BaseSlidingFragmentActivity activity, String content,
                                final long price, final String orderId,
                                final ArrayList<String> listMember, final RequestSendLixiCallBack callBack) {
        Log.i(TAG, "conetn: " + content + " price: " + price + " orderId: " + orderId);
        logRequestBankplusFail("", orderId, getListMemberLixi(listMember), String.valueOf(price),
                String.valueOf(-1), "");
        BplusSdk.setUserPhoneNumber(mApplication.getReengAccountBusiness().getJidNumber());
        BpGift bpGift = new BpGift(content, price, orderId);
        BplusSdk.gift(activity, bpGift, new ResultListener<BpTransferResult>() {
            @Override
            public void success(BpTransferResult result) {
                Log.i(TAG, "success: " + result.originalRequestId);
                if (callBack != null) callBack.onSuccess(result.originalRequestId);
            }

            @Override
            public void failed(int code, String message) {
                Log.e(TAG, "failed: " + code + " msg: " + message);
                logRequestBankplusFail("", orderId, getListMemberLixi(listMember), String.valueOf(price),
                        String.valueOf(code), message);
                if (callBack != null) callBack.onError(code, message);
            }
        });
    }

    private String getListMemberLixi(ArrayList<String> listMember) {
        StringBuilder listMemberStr = new StringBuilder();
        for (String s : listMember) {
            if (!TextUtils.isEmpty(listMemberStr.toString())) {
                listMemberStr.append(",");
            }
            listMemberStr.append(s);
        }
        return listMemberStr.toString();
    }

    public static String generateRequestIdLixi(String msisdn, long timeStamp) {
        Log.i(TAG, "input: " + msisdn + " " + timeStamp);
        String request = msisdn.substring(2) + String.valueOf(timeStamp).substring(2);
        Log.i(TAG, "request: " + request);
        return request;
    }

    public void openGiftLixi(final String requestid, final RequestOpenLixiCallBack callBack) {
        final ReengAccount myAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!mApplication.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_LIXI_OPEN);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .LIXI_OPEN);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber()).append(requestid).append(myAccount.getToken()).append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), myAccount.getToken());
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, url, new Response
                .Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse: " + s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    String code = jsonObject.optString("code");
                    String desc = jsonObject.optString("desc");
                    if ("200".equals(code)) {
                        long amountMoney = jsonObject.optLong("amount");
                        callBack.onSuccess(amountMoney);
                    } else {
                        callBack.onError(0, desc);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    callBack.onError(-1, "");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse: " + volleyError.toString());
                callBack.onError(-1, volleyError.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("requestid", requestid);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_LIXI_OPEN, false);


    }

    public void getListImageLixi(final GetListImageLixiCallBack callBack) {
        ReengAccount myAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!mApplication.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_LIXI_GET_LIST_IMAGE);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .LIXI_GET_LIST_IMAGE);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber()).append(myAccount.getToken()).append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), myAccount.getToken());
        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam("security", dataEncrypt);
        StringRequest request = new StringRequest(
                Request.Method.GET, params.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int code = jsonObject.optInt("code");
                    if (code == HTTPCode.E200_OK) {
                        JSONArray jsonArray = jsonObject.optJSONArray("lst_result");
                        ArrayList<String> listImg = new ArrayList<>();
                        if (jsonArray != null && jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject js = jsonArray.optJSONObject(i);
                                String img = js.optString("img_thume");
                                if (!TextUtils.isEmpty(img)) {
                                    listImg.add(img);
                                }
                            }
                        }
                        callBack.onGetListImage(listImg);
                    } else {
                        callBack.onError(-1);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    callBack.onError(-1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                callBack.onError(-1);
            }
        });
        Log.i(TAG, "getListImageLixi: " + params.toString());
        /* String url = "http://vip.service.keeng.vn/KeengBackendBiz/ws/common/getVideo4Mocha?num=20&page=1";
        StringRequest request = new StringRequest(Request.Method.GET, url, response, error);*/
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_LIXI_GET_LIST_IMAGE, false);
    }

    public void logRequestBankplusFail(final String requestid, final String order_id, final String receiver,
                                       final String amount, final String errCode, final String errMsg) {
        final ReengAccount myAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!mApplication.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_LOG_BANKPLUS_FAIL);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .LIXI_LOG_BPLUS_FAIL);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber()).append(requestid).append(order_id).append(receiver).append(amount)
                .append(errCode).append(errMsg).append(myAccount.getToken()).append(timeStamp);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), myAccount.getToken());
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, url, new Response
                .Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse: " + volleyError.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("msisdn", myAccount.getJidNumber());
                params.put("timestamp", timeStamp);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("requestid", requestid);
                params.put("order_id", order_id);
                params.put("receiver", receiver);
                params.put("amount", amount);
                params.put("error_code", errCode);
                params.put("msg_code", errMsg);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_LOG_BANKPLUS_FAIL, false);
    }

    public boolean isShowLixi(ThreadMessage threadMessage) {
        if (mApplication.getConfigBusiness().isEnableLixi()) {
            if (threadMessage == null) {
                return false;
            } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                return !threadMessage.isStranger();
            } else {
                return threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
            }
        }
        return false;
    }


    public void checkUserViettelPay(BaseSlidingFragmentActivity activity,  CheckAccountCallBack callBack){
        String msisdn = mApplication.getReengAccountBusiness().getJidNumber();
        activity.showLoadingDialog(null, R.string.bplus_waiting);
        new RequestCheckUserBPlusAsyncTask(mApplication, activity, msisdn, callBack).execute();
    }

    public interface CheckAccountCallBack {
        void onResult(boolean isUserBPlus);

        void onError(int errorCode);
    }

    public interface GetListImageLixiCallBack {
        void onGetListImage(ArrayList<String> listImage);

        void onError(int errorCode);
    }

    public interface RequestOpenLixiCallBack {
        void onSuccess(long amountMoney);

        void onError(int errorCode, String desc);
    }

    public interface RequestSendLixiCallBack {
        void onSuccess(String orderId);

        void onError(int errorCode, String desc);
    }
}

package com.metfone.selfcare.helper.httprequest;

import android.content.res.Resources;
import android.location.Location;
import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.StrangerFilterHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/10/2017.
 */

public class NearYouRequestHelper {
    private static final String TAG = MusicRequestHelper.class.getSimpleName();
    private static final String TAG_LOAD_AROUND_LIST = "TAG_LOAD_AROUND_LIST";
    private static NearYouRequestHelper mInstance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private ArrayList<StrangerMusic> strangerArounds = new ArrayList<>();
    private int currentPageAround;
    private int mOptionFilter = -1;

    public static synchronized NearYouRequestHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new NearYouRequestHelper(application);
        }
        return mInstance;
    }

    private NearYouRequestHelper(ApplicationController application) {
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        this.mRes = mApplication.getResources();
    }

    public ArrayList<StrangerMusic> getStrangerArounds() {
        return strangerArounds;
    }

    public void updateOptionFilter(int option) {
        this.mOptionFilter = option;
        mApplication.getPref().edit().putInt(Constants.STRANGER_MUSIC.OPTION_FILTER_NEAR_YOU, option).apply();
    }

    public int getOptionFilter() {
        if (mOptionFilter <= 0) {
            mOptionFilter = mApplication.getPref().getInt(Constants.STRANGER_MUSIC.OPTION_FILTER_NEAR_YOU, Constants.MENU.STRANGER_FILTER_MALE_FEMALE);
        }
        return mOptionFilter;
    }

    private String getUrlGetListStrangerAround(int optionFilter, String lastGeoHash, int page, Location location) {
        String token = mAccountBusiness.getToken();
        StringBuilder sb = new StringBuilder();
        String posterInfo = mApplication.getMusicBusiness().createUserInfoParams(mAccountBusiness.getCurrentAccount());
        String regionCode = mAccountBusiness.getRegionCode();
        StrangerFilterHelper strangerFilterHelper = StrangerFilterHelper.getInstance(mApplication);

        String gender = String.valueOf(strangerFilterHelper.getFilterSex());
//        if (optionFilter == Constants.MENU.STRANGER_FILTER_MALE) {
//            gender = String.valueOf(Constants.CONTACT.GENDER_MALE);
//        } else if (optionFilter == Constants.MENU.STRANGER_FILTER_FEMALE) {
//            gender = String.valueOf(Constants.CONTACT.GENDER_FEMALE);
//        }
        long currentTime = TimeHelper.getCurrentTime();
        sb.append(mAccountBusiness.getJidNumber()).
                append(posterInfo).
                append(regionCode).
                append(gender).
                append(page).
                append(lastGeoHash).
                append(location.getLongitude()).
                append(location.getLatitude()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(token).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), token));
        StringBuilder urlSb = new StringBuilder(UrlConfigHelper.getInstance(mApplication).getDomainFile() + "/ReengBackendBiz/musicRoom/getLisStrangerAround/v3");
        urlSb.append("?posterMsisdn=").append(HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber())).
                append("&posterInfo=").append(HttpHelper.EncoderUrl(posterInfo)).
                append("&countryCode=").append(regionCode).
                append("&filterGender=").append(gender).
                append("&page=").append(page).
                append("&lastGeoHash=").append(HttpHelper.EncoderUrl(lastGeoHash)).
                append("&longitude=").append(location.getLongitude()).
                append("&latitude=").append(location.getLatitude()).
                append("&timestamp=").append(currentTime).
                append("&clientType=").append(Constants.HTTP.CLIENT_TYPE_STRING).
                append("&revision=").append(Config.REVISION).
                append("&security=").append(dataEncrypt);
        return urlSb.toString();
    }

    public void loadListStrangerAround(final String lastGeoHash, final boolean isNewFilter,
                                       final boolean isLoadMore, @NonNull final OnLoadDataResponseListener listener) {
        LocationHelper.getInstant(mApplication).getCurrentLatLng(new LocationHelper.GetLocationListener() {
            @Override
            public void onResponse(Location location) {
                Log.d(TAG, "loadListStrangerAround getCurrentLatLng: onResponse: " + location.getLatitude() + "," +
                        location.getLongitude());
                LocationHelper.getInstant(mApplication).setGetLocationListener(null);
                requestStrangerAround(location, lastGeoHash, isNewFilter, isLoadMore, listener);
            }

            @Override
            public void onFail() {
                Log.d(TAG, "loadListStrangerAround: onFail");
                listener.onFilterDataError(-1);
                LocationHelper.getInstant(mApplication).setGetLocationListener(null);
            }
        });
    }

    private void requestStrangerAround(Location location, String lastGeoHash, final boolean isNewFilter,
                                       final boolean isLoadMore, @NonNull final OnLoadDataResponseListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication) || !mAccountBusiness.isValidAccount()) {
            if (isNewFilter) {
                listener.onFilterDataError(-1);
            } else {
                listener.onError(-1);
            }
            return;
        }
        if (!isLoadMore) {
            currentPageAround = 0;
        }
        Log.d(TAG, "loadListStrangerAround: currentPage:" + currentPageAround + " isLoadMore: " + isLoadMore);
        int optionFilter = getOptionFilter();
        String url = getUrlGetListStrangerAround(optionFilter, lastGeoHash, currentPageAround, location);
        Log.d(TAG, "url: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "[Cambodia version] [requestStrangerAround] response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<StrangerMusic> strangers = parserStrangerAround(responseObject);
                                if (isLoadMore) {
                                    if (!strangers.isEmpty()) {
                                        strangerArounds.addAll(strangers);
                                        currentPageAround++;
                                    }
                                    listener.onLoadMoreDataResponse(strangerArounds, strangers);
                                } else {
                                    currentPageAround++;
                                    strangerArounds = strangers;
                                    listener.onRefreshDataResponse(strangerArounds);
                                }
                            } else {
                                if (isNewFilter) {
                                    listener.onFilterDataError(errorCode);
                                } else {
                                    listener.onError(errorCode);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (isNewFilter) {
                                listener.onFilterDataError(errorCode);
                            } else {
                                listener.onError(errorCode);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (isNewFilter) {
                    listener.onFilterDataError(-1);
                } else {
                    listener.onError(-1);
                }
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_LOAD_AROUND_LIST, false);
    }

    private ArrayList<StrangerMusic> parserStrangerAround(JSONObject jsonObject) throws JSONException {
        ArrayList<StrangerMusic> strangerMusics = new ArrayList<>();
        if (jsonObject.has("listStrangerAround")) {
            JSONArray jsonArray = jsonObject.getJSONArray("listStrangerAround");
            int size = jsonArray.length();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    StrangerMusic strangerMusic = new StrangerMusic();
                    strangerMusic.setAroundJsonObject(item);
                    strangerMusics.add(strangerMusic);
                }
            }
        }
        return strangerMusics;
    }

    public interface OnLoadDataResponseListener {
        void onRefreshDataResponse(ArrayList<StrangerMusic> strangerMusics);

        void onLoadMoreDataResponse(ArrayList<StrangerMusic> strangerMusics, ArrayList<StrangerMusic> dataLoadMore);

        void onFilterDataError(int errorCode);

        void onError(int errorCode);
    }
}

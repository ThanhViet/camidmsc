package com.metfone.selfcare.helper;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.plattysoft.leonids.ParticleSystem;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.VideoMochaActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.ReplyMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static com.metfone.selfcare.helper.GlideHelper.FILE_PATH;

public class MessageHelper {
    //    private static final String formatTimePattern = "dd/MM/yyyy HH:mm";
    //    private static final SimpleDateFormat spf = new SimpleDateFormat(formatTimePattern);
    private final static String TAG = "MessageHelper";

    /**
     * @param fileName
     * @return
     * @author ThaoDV
     */
    public static String getFileType(String fileName) {
        String fileType = "";
        try {
            for (int i = fileName.length() - 1; i >= 0; i--) {
                if (fileName.charAt(i) == '.') {
                    break;
                }
                fileType = fileName.charAt(i) + fileType;
            }
        } catch (Exception e) {
            Log.e(TAG, "getFileType", e);
        }
        return fileType.toLowerCase();
    }

    /**
     * @param filePath
     * @return
     * @author ThaoDV
     */
    public static boolean isImage(String filePath) {
        String fileType = getFileType(filePath);
        if (TextUtils.isEmpty(fileType)) return false;
        return "jpg".equals(fileType) || "png".equals(fileType)
                || "gif".equals(fileType) || "jpeg".equals(fileType);
    }

    /**
     * Calculate an inSampleSize for use in a {@link android.graphics.BitmapFactory.Options}
     * object when decoding bitmaps using the decode* methods from
     * {@link android.graphics.BitmapFactory}. This implementation calculates the closest
     * inSampleSize that will result in the final decoded bitmap having a width
     * and height equal to or larger than the requested width and height. This
     * implementation does not ensure a power of 2 is returned for inSampleSize
     * which can be faster when decoding but results in a larger bitmap which
     * isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run
     *                  through a decode* method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (reqWidth <= 0 || reqHeight <= 0) {
            return inSampleSize;
        }
        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee a final image
            // with both dimensions larger than or equal to the requested height
            // and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger
            // inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down
            // further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    /**
     * @param filename  The full path of the file to decode
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect
     * ratio and dimensions that are equal to or greater than the
     * requested width and height
     * @author cngp_thaodv Decode and sample down a bitmap from a file to the
     * requested width and height.
     */
    public static Bitmap decodeSampledBitmapFromFile(String filename,
                                                     int reqWidth, int reqHeight) {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filename, options);
            options.inPreferredConfig = Config.RGB_565;
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // If we're running on Honeycomb or newer, try to use inBitmap
            /*
             * if (Utils.hasHoneycomb()) { addInBitmapOptions(options, cache); }
             */

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(filename, options);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            Log.e(TAG, "fileName = " + filename);
            return null;
        }
    }

    /**
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect
     * ratio and dimensions that are equal to or greater than the
     * requested width and height
     * @author cngp_thaodv Decode and sample down a bitmap from a file to the
     * requested width and height.
     */
    public static Bitmap decodeSampledBitmapFromInputStream(InputStream in,
                                                            int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(in, null, options);
        options.inPreferredConfig = Config.RGB_565;
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // If we're running on Honeycomb or newer, try to use inBitmap
        /*
         * if (Utils.hasHoneycomb()) { addInBitmapOptions(options, cache); }
         */
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(in, null, options);
    }

    public static Bitmap decodeEmoticonIcon(InputStream inputStream,
                                            int reqWidth) {
        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // SkImageDecoder::Factory returned null -> change from true to false
        BitmapFactory.decodeStream(inputStream, null, options);
        options.inPreferredConfig = Config.RGB_565;
        try {
            inputStream.reset();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqWidth);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    public static void checkDownloadOrOpenVideo(ApplicationController application, BaseSlidingFragmentActivity
            activity, ReengMessage message) {
        /*if (message.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD ||
                message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
            Intent i = new Intent(activity, VideoMochaActivity.class);
            i.putExtra(ReengMessageConstant.MESSAGE_THREAD_ID, message.getThreadId());
            i.putExtra(ReengMessageConstant.MESSAGE_ID, message.getId());
            activity.startActivity(i);
        } else*/
        if (message.getStatus() == ReengMessageConstant.STATUS_LOADING) { // khong lam gi
            Log.d(TAG, "checkDownloadOrOpenVideo: loading ");
        } else {// down xong thi view
            /*File file = new File(message.getFilePath());
            if (file.isFile()) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(FileHelper.fromFile(application, file), "video*//*");
                    if (Version.hasN()) {
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    activity.startActivity(intent);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    activity.showToast(R.string.cant_open_file);
                }
            } else {
                activity.showToast(R.string.file_not_found_exception);
            }*/
            Intent i = new Intent(activity, VideoMochaActivity.class);
            i.putExtra(ReengMessageConstant.MESSAGE_THREAD_ID, message.getThreadId());
            i.putExtra(ReengMessageConstant.MESSAGE_ID, message.getId());
            activity.startActivity(i);
        }
    }

    public static void checkDownloadOrOpenFile(ApplicationController application, BaseSlidingFragmentActivity
            activity, ReengMessage message) {
        if (message.getDirection() == ReengMessageConstant.Direction.send || !TextUtils.isEmpty(message.getFilePath()
        )) {
            openFileFromMessage(message, activity, message.getFilePath(), false, message.getFileName());
        } else if (message.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD || TextUtils.isEmpty(message.getFilePath())) {
            if (PermissionHelper.allowedPermission(application, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                application.initFolderFileDocument();
                application.getTransferFileBusiness().startDownloadMessageFile(message);
            } else {
                PermissionHelper.requestPermissionWithGuide(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Constants.PERMISSION.PERMISSION_REQUEST_FILE_DOWNLOAD);
            }
        } else {
            Log.d(TAG, "checkDownloadOrOpenFile: " + message.toString());
        }
    }

    private static void openFileFromMessage(ReengMessage message, BaseSlidingFragmentActivity activity, String filePath,
                                            boolean isImage, String fileName) {
        //https://stackoverflow.com/questions/4212861/what-is-a-correct-mime-type-for-docx-pptx-etc
        if (TextUtils.isEmpty(filePath)) {
            if (PermissionHelper.allowedPermission(ApplicationController.self(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ApplicationController.self().initFolderFileDocument();
                ApplicationController.self().getTransferFileBusiness().startDownloadMessageFile(message);
            } else {
                PermissionHelper.requestPermissionWithGuide(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Constants.PERMISSION.PERMISSION_REQUEST_FILE_DOWNLOAD);
            }
        } else {
            try {
                if (filePath == null || filePath.length() < 3) {
                    Log.e(TAG, "filePath error" + filePath);
                    activity.showToast(R.string.e601_error_but_undefined);
                    return;
                }
                Intent intent = new Intent();
                ApplicationController application = (ApplicationController) activity.getApplicationContext();
                Uri uri = FileHelper.fromFile(application, new File(filePath));
                //Uri uri = Uri.fromFile(new File(filePath));
                // display image
                if (isImage) {
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(uri, "image/*");
                } else { // open folder if file
                    intent = new Intent(Intent.ACTION_VIEW);
                    String extension = FileHelper.getExtensionFile(fileName);
                    if ("doc".equals(extension)) {
                        intent.setDataAndType(uri, "application/msword");
                    } else if ("docx".equals(extension)) {
                        intent.setDataAndType(uri, "application/vnd.openxmlformats-officedocument.wordprocessingml" +
                                ".document");
                    } else if ("xls".equals(extension)) {
                        intent.setDataAndType(uri, "application/vnd.ms-excel");
                    } else if ("xlsx".equals(extension)) {
                        intent.setDataAndType(uri, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    } else if ("ppt".equals(extension)) {
                        intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
                    } else if ("pptx".equals(extension)) {
                        intent.setDataAndType(uri, "application/vnd.openxmlformats-officedocument.presentationml" +
                                ".presentation");
                    } else if ("pdf".equals(extension)) {
                        intent.setDataAndType(uri, "application/pdf");
                    } else {
                        intent.setDataAndType(uri, "text/plain");
                    }
                }
                if (Version.hasN()) {
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                activity.showToast(R.string.e666_not_support_function);
            }
        }
    }

    /**
     * @param inputFileName dang nhu IMG.jgp
     * @return IMG(i).jpg
     */
    public static String getUniqueFileName(String inputFileName, String fileType) {
        int index = inputFileName.lastIndexOf(".");
        String fileExtension, tempFileName;
        if (index != -1) {
            fileExtension = inputFileName.substring(index);
            tempFileName = inputFileName.substring(0, index);
        } else {
            fileExtension = "";
            tempFileName = inputFileName;
        }
        tempFileName = String.valueOf(EncryptUtil.md5(tempFileName));
        return tempFileName + "_" + System.currentTimeMillis()
                + fileExtension;
    }

    public static String getUniqueDocFileName(String inputFileName, String fileId) {
        if (!TextUtils.isEmpty(inputFileName)) {
            int index = inputFileName.lastIndexOf(".");
            String fileExtension, tempFileName;
            if (index != -1) {
                fileExtension = inputFileName.substring(index);
                tempFileName = inputFileName.substring(0, index);
            } else {
                fileExtension = "";
                tempFileName = inputFileName;
            }
            if (TextUtils.isEmpty(fileId)) {
                fileId = String.valueOf(System.currentTimeMillis());
            }
            tempFileName = String.valueOf(EncryptUtil.md5(tempFileName));
            return tempFileName + "_" + fileId + fileExtension;
        } else if (!TextUtils.isEmpty(fileId)) {
            return fileId;
        }
        return String.valueOf(System.currentTimeMillis());
    }

    public static String getPathOfCompressedFile(String filePath, String folder, String fileName, boolean isHD) {
        // TODO crash (width and height must be > 0)
        Bitmap scaledBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp;
        BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        Log.d(TAG, "actualHeight = " + actualHeight + " , actualWidth = " + actualWidth);

        float maxSize = 1280.0f;
        boolean isLandscape = false;
        if (isHD) {
            maxSize = 2048.0f;
        }

        ExifInterface exif;
        int orientation = 0;
        try {
            exif = new ExifInterface(filePath);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            if (orientation == 6 || orientation == 8 || actualWidth > actualHeight) {
                isLandscape = true;
                Log.d("EXIF", "Exif: " + orientation);
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }

        if (actualWidth <= 0 || actualHeight <= 0) {
            return null;
        }
        float imgRatio = (float) actualWidth / (float) actualHeight;
//        float maxRatio = maxWidth / maxHeight;
        if (isLandscape) {
            if (actualWidth >= maxSize) {
                actualWidth = (int) maxSize;
                actualHeight = (int) (maxSize / imgRatio);
            }
        } else {
            if (actualHeight >= maxSize) {
                actualWidth = (int) (maxSize * imgRatio);
                actualHeight = (int) maxSize;
            }
        }
        Log.d(TAG, " resize actualHeight = " + actualHeight + " , actualWidth = " + actualWidth + " isLandscape: " +
                isLandscape);
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false; // hoaf sawcs
        options.inPurgeable = true; // Lam trong sach, lam tinh khiet
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            Log.e(TAG, "OutOfMemoryError", exception);
            return null;
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Constants.IMAGE_CACHE.BITMAP_CONFIG);
        } catch (OutOfMemoryError exception) {
            scaledBitmap = null;
            Log.e(TAG, "OutOfMemoryError", exception);
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        if (scaledBitmap == null || bmp == null) {
            return null;
        }
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG)
        );

        Log.d("EXIF", "Exif: " + orientation);
        Matrix matrix = new Matrix();
        if (orientation == 6) {
            matrix.postRotate(90);
            Log.d("EXIF", "Exif: " + orientation);
        } else if (orientation == 3) {
            matrix.postRotate(180);
            Log.d("EXIF", "Exif: " + orientation);
        } else if (orientation == 8) {
            matrix.postRotate(270);
            Log.d("EXIF", "Exif: " + orientation);
        }
        scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                true);
        // ByteArrayOutputStream out = null;
        String newFilePath;
        if (TextUtils.isEmpty(folder)) {
            newFilePath = filePath;
        } else {
            String currentTime = System.currentTimeMillis() + "";
            String prePath = com.metfone.selfcare.helper.Config.Storage.REENG_STORAGE_FOLDER
                    + folder;

            if (filePath.startsWith(prePath)) {
                newFilePath = filePath;
            } else {
                if (TextUtils.isEmpty(fileName))
                    fileName = ImageHelper.IMAGE_NAME + currentTime + ".jpg";
                newFilePath = prePath + "/" + fileName;
            }
        }

        FileOutputStream fos;
        try {
            // out = new ByteArrayOutputStream();
            fos = new FileOutputStream(newFilePath);
            int quality = 75;
            boolean scaled = scaledBitmap.compress(Bitmap.CompressFormat.JPEG, quality, fos);
            Log.d(TAG,
                    "scaled = " + scaled + " height = "
                            + scaledBitmap.getHeight() + " width = "
                            + scaledBitmap.getWidth()
            );
            fos.close();
            return newFilePath;
        } catch (Exception e) {
            Log.e(TAG, "ExceptionEnd", e);
        }
        return null;
    }

    public static void getPathOfCompressedFile(String filePath, String folder, boolean isHD, FileHelper.CompressImageListener listener) {
        // TODO crash (width and height must be > 0)
        Bitmap scaledBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = null;
        BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        Log.d(TAG, "actualHeight = " + actualHeight + " , actualWidth = " + actualWidth);

        float maxSize = 1280.0f;
        boolean isLandscape = false;
        if (isHD) {
            maxSize = 2048.0f;
        }

        ExifInterface exif;
        int orientation = 0;
        float realRatio = 1; //fix loi tren vai may bi;
        try {
            exif = new ExifInterface(filePath);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            if (orientation == 6 || orientation == 8 || actualWidth > actualHeight) {
                isLandscape = true;
                Log.d("EXIF", "Exif: " + orientation);
            }

            int w = actualWidth, h = actualHeight;

            if (orientation == 6 || orientation == 8) {
                int tmp = w;
                w = h;
                h = tmp;
            }
            realRatio = (float) w / (float) h;
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }

        if (actualWidth <= 0 || actualHeight <= 0) {
            if (listener != null)
                listener.onCompressFail();
        }
        float imgRatio = (float) actualWidth / (float) actualHeight;

        if (isLandscape) {
            if (actualWidth >= maxSize) {
                actualWidth = (int) maxSize;
                actualHeight = (int) (maxSize / imgRatio);
            }
        } else {
            if (actualHeight >= maxSize) {
                actualWidth = (int) (maxSize * imgRatio);
                actualHeight = (int) maxSize;
            }
        }
        Log.d(TAG, " resize actualHeight = " + actualHeight + " , actualWidth = " + actualWidth + " isLandscape: " +
                isLandscape);
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false; // hoaf sawcs
        options.inPurgeable = true; // Lam trong sach, lam tinh khiet
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            Log.e(TAG, "OutOfMemoryError", exception);
            if (listener != null)
                listener.onCompressFail();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Constants.IMAGE_CACHE.BITMAP_CONFIG);
        } catch (OutOfMemoryError exception) {
            scaledBitmap = null;
            Log.e(TAG, "OutOfMemoryError", exception);
        }

        String ratio = (new DecimalFormat("#.##").format(realRatio)).replace(",", ".");

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        if (scaledBitmap == null || bmp == null) {
            if (listener != null)
                listener.onCompressFail();
        }
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG)
        );

        Log.d("EXIF", "Exif: " + orientation);
        Matrix matrix = new Matrix();
        if (orientation == 6) {
            matrix.postRotate(90);
            Log.d("EXIF", "Exif: " + orientation);
        } else if (orientation == 3) {
            matrix.postRotate(180);
            Log.d("EXIF", "Exif: " + orientation);
        } else if (orientation == 8) {
            matrix.postRotate(270);
            Log.d("EXIF", "Exif: " + orientation);
        }
        scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                true);
        // ByteArrayOutputStream out = null;
        String newFilePath;
        if (TextUtils.isEmpty(folder)) {
            newFilePath = filePath;
        } else {
            String currentTime = System.currentTimeMillis() + "";
            String prePath = com.metfone.selfcare.helper.Config.Storage.REENG_STORAGE_FOLDER
                    + folder;

            if (filePath.startsWith(prePath)) {
                newFilePath = filePath;
            } else {
                newFilePath = prePath + "/" + ImageHelper.IMAGE_NAME + currentTime + ".jpg";
            }
        }

        FileOutputStream fos;
        try {
            // out = new ByteArrayOutputStream();
            fos = new FileOutputStream(newFilePath);
            int quality = 75;
            boolean scaled = scaledBitmap.compress(Bitmap.CompressFormat.JPEG, quality, fos);
            Log.d(TAG,
                    "scaled = " + scaled + " height = "
                            + scaledBitmap.getHeight() + " width = "
                            + scaledBitmap.getWidth()
            );
            fos.close();
            if (listener != null)
                listener.onCompressSuccess(newFilePath, ratio);
        } catch (Exception e) {
            Log.e(TAG, "ExceptionEnd", e);
            if (listener != null)
                listener.onCompressFail();
        }
    }

    public static void setRatioForMessageImage(ReengMessage messageImage, String filePath) {
        String ratio;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            if (orientation == 6) {
                int tmp = actualWidth;
                actualWidth = actualHeight;
                actualHeight = tmp;
//                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
//                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
//                matrix.postRotate(270);
                int tmp = actualWidth;
                actualWidth = actualHeight;
                actualHeight = tmp;
                Log.d("EXIF", "Exif: " + orientation);
            }
            float ratioFloat = (float) actualWidth / (float) actualHeight;
            ratio = (new DecimalFormat("#.##").format(ratioFloat)).replace(",", ".");
            Log.d(TAG, " resize actualHeight = " + actualHeight + " , actualWidth = " + actualWidth + " ratio: " +
                    ratio);
            messageImage.setVideoContentUri(ratio);
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
            messageImage.setVideoContentUri(String.valueOf(1));
        }
    }

    public static ArrayList<ItemContextMenu> getListItemReport(Context context, ReengMessage reengMessage) {
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        Resources res = context.getResources();
        ItemContextMenu reportSpam = new ItemContextMenu(res.getString(R.string.report_spam),
                -1, reengMessage, Constants.MENU.POPUP_REPORT_SPAM);
        ItemContextMenu reportBadWord = new ItemContextMenu(res.getString(R.string.report_badword),
                -1, reengMessage, Constants.MENU.POPUP_REPORT_BADWORD);
        ItemContextMenu reportCheat = new ItemContextMenu(res.getString(R.string.report_cheat),
                -1, reengMessage, Constants.MENU.POPUP_REPORT_CHEAT);
        listItem.add(reportSpam);
        listItem.add(reportBadWord);
        listItem.add(reportCheat);
        return listItem;
    }

    public static ArrayList<ItemContextMenu> getListItemContextMenu(ApplicationController application,
                                                                    ReengMessage reengMessage,
                                                                    ThreadMessage threadMessage,
                                                                    String languageCode,
                                                                    boolean fromSearchMessage,
                                                                    boolean hasImageResource) {
        int threadType = threadMessage.getThreadType();
        Resources res = application.getCurrentActivity().getResources();
        ReengMessageConstant.Direction messageDirection = reengMessage.getDirection();
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
//        ItemContextMenu itemTranslate = new ItemContextMenu(res.getString(R.string.menu_translate),
//                hasImageResource ? R.drawable.ic_translate_menu : -1, reengMessage, Constants.MENU.MESSAGE_MENU_TRANSLATE);
//        if (!fromSearchMessage && application.getReengAccountBusiness().isTranslatable() &&
//                reengMessage.getMessageType() == ReengMessageConstant.MessageType.text &&
//                reengMessage.getDirection() == ReengMessageConstant.Direction.received) {
//            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT ||
//                    threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
//                if (TextUtils.isEmpty(reengMessage.getTextTranslated()) ||
//                        (!TextUtils.isEmpty(reengMessage.getLanguageTarget()) && !languageCode.equals(reengMessage
//                                .getLanguageTarget()))) {
//                    if (!reengMessage.isLargeEmo())//TODO voi message large thì ko cho dịch
//                        listItem.add(itemTranslate);
//                }
//            }
//        }
        // tin nhan group da gui,da doc, da xem
//        if (!fromSearchMessage && messageDirection == ReengMessageConstant.Direction.send &&
//                (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT ||
//                        threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) &&
//                (reengMessage.getStatus() == ReengMessageConstant.STATUS_DELIVERED ||
//                        reengMessage.getStatus() == ReengMessageConstant.STATUS_SENT ||
//                        reengMessage.getStatus() == ReengMessageConstant.STATUS_SEEN) &&
//                reengMessage.getMessageType() != ReengMessageConstant.MessageType.restore) {
//            ItemContextMenu detailDeliver = new ItemContextMenu(res.getString(R.string
//                    .view_detail_deliver), -1, reengMessage, Constants.MENU.MESSAGE_DETAIL_DELIVER);
//            listItem.add(detailDeliver);
//        }
        // Sao chep
        if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.text ||
                reengMessage.getMessageType() == ReengMessageConstant.MessageType.deep_link) {
            ItemContextMenu copy = new ItemContextMenu(res.getString(R.string.copy),
                    hasImageResource ? R.drawable.ic_copy_menu : -1, reengMessage, Constants.MENU.MESSAGE_COPY);
            if (threadType != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT ||
                    !threadMessage.isPrivateThread())
                listItem.add(copy);
        }
        //TODO reply
        if (!fromSearchMessage && /*messageDirection == ReengMessageConstant.Direction.received &&*/
                (reengMessage.getMessageType() == ReengMessageConstant.MessageType.text ||
                        reengMessage.getMessageType() == ReengMessageConstant.MessageType.image) &&
                (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT ||
                        threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT)) {
            ItemContextMenu reply = new ItemContextMenu(res.getString(R.string.reply),
                    hasImageResource ? R.drawable.ic_reply_menu : -1, reengMessage, Constants.MENU.MESSAGE_REPLY);
            listItem.add(reply);
        }
        // Xoa
        ItemContextMenu delete = new ItemContextMenu(res.getString(R.string.delete),
                hasImageResource ? R.drawable.ic_delete_menu : -1, reengMessage, Constants.MENU.MESSAGE_DELETE);
        listItem.add(delete);
        if (!fromSearchMessage && isMessageCanShare(reengMessage)) {
            ItemContextMenu share = new ItemContextMenu(res.getString(R.string.share),
                    hasImageResource ? R.drawable.ic_share_menu_new : -1, reengMessage, Constants.MENU.MESSAGE_SHARE);
            listItem.add(share);
        }


        if (isMessageForward(reengMessage, threadMessage, threadType)) {
            // forward
            ItemContextMenu forward = new ItemContextMenu(res.getString(R.string.forward),
                    hasImageResource ? R.drawable.ic_forward_menu : -1, reengMessage, Constants.MENU.MESSAGE_FORWARD);
            listItem.add(forward);
        } else if (isMessageBlockForward(reengMessage, threadMessage, threadType)) {
            // block forward
            ItemContextMenu forward = new ItemContextMenu(res.getString(R.string.forward),
                    hasImageResource ? R.drawable.ic_block_forward_menu_new : -1, reengMessage, Constants.MENU.MESSAGE_BLOCK_FORWARD);
            listItem.add(forward);
        }

//        if (!fromSearchMessage && isMessageRestoreAble(reengMessage, threadType)) {
//            ItemContextMenu restore = new ItemContextMenu(res.getString(R.string.pin_msg), -1, reengMessage, Constants.MENU.MESSAGE_RESTORE);
//            listItem.add(restore);
//        }
//        if (!fromSearchMessage && isPinableMessage(reengMessage, threadMessage, threadType)) {
//            ItemContextMenu pinMsg = new ItemContextMenu(res.getString(R.string.pin_msg), -1, reengMessage, Constants.MENU.MESSAGE_PIN);
//            listItem.add(pinMsg);
//        }
        // note
        if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.text
                && threadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT
                && threadType != ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            ItemContextMenu note = new ItemContextMenu(res.getString(R.string.note),
                    hasImageResource ? R.drawable.ic_save_message_menu_new : -1, reengMessage, Constants.MENU.MESSAGE_DETAIL_NOTE);
            listItem.add(note);
        }
        if (!fromSearchMessage && messageDirection == ReengMessageConstant.Direction.send && isMessageToSms(reengMessage, threadMessage,
                threadType)) {
            ItemContextMenu mocha2Sms = new ItemContextMenu(res.getString(R.string.mocha_2_sms),
                    hasImageResource ? R.drawable.ic_sms_out_menu_new : -1, reengMessage, Constants.ACTION.ACTION_MOCHA_TO_SMS);
            listItem.add(mocha2Sms);
        }
        /*if (isMessageForward(reengMessage, threadType)
                && reengMessage.getMessageType() == ReengMessageConstant.MessageType.file) {
            // share FB
            ItemContextMenu shareViaFB = new ItemContextMenu(res, res.getString(R.string.share_via_fb),
                    -1, reengMessage, Constants.MENU.MESSAGE_SHARE_VIA_FB);
            listItem.add(shareViaFB);
        }*/
//        if (threadMessage.getDhVtt() == 1 && threadMessage.isAdmin()
//                && (reengMessage.getMessageType() == ReengMessageConstant.MessageType.image
//                || reengMessage.getMessageType() == ReengMessageConstant.MessageType.text)) {
//            ItemContextMenu dhVtt = new ItemContextMenu(res.getString(R.string.report_video),
//                    hasImageResource ? R.drawable.ic_report_dhvtt : -1, reengMessage, Constants.MENU.MENU_REPORT_DHVTT);
//            listItem.add(dhVtt);
//        }
        return listItem;
    }

    private static boolean isMessageForward(ReengMessage reengMessage, ThreadMessage thread, int threadType) {
        //        ReengMessageConstant.Direction messageDirection = reengMessage.getDirection();
        if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) return false;
        if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && thread.isPrivateThread())
            return false;
        ReengMessageConstant.MessageType messageType = reengMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.restore) {
            return false;
        }
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && thread.isEncryptThread())
            return false;
        if (messageType == ReengMessageConstant.MessageType.file
                //Neu file chua upload xong/error thi ko cho forward
                && !TextUtils.isEmpty(reengMessage.getFilePath())
                && !(reengMessage.getStatus() == ReengMessageConstant.STATUS_LOADING
                || reengMessage.getStatus() == ReengMessageConstant.STATUS_FAIL))
            return true;
        if (/*messageType == ReengMessageConstant.MessageType.file
                ||*/ messageType == ReengMessageConstant.MessageType.image
                || messageType == ReengMessageConstant.MessageType.voicemail
                || messageType == ReengMessageConstant.MessageType.watch_video
                /*|| messageType == ReengMessageConstant.MessageType.inviteShareMusic*/
                || messageType == ReengMessageConstant.MessageType.shareVideo) {
            return !(reengMessage.getStatus() == ReengMessageConstant.STATUS_LOADING
                    //Neu file chua upload xong/error thi ko cho forward
                    || reengMessage.getStatus() == ReengMessageConstant.STATUS_FAIL);
        } else return messageType == ReengMessageConstant.MessageType.text
                || messageType == ReengMessageConstant.MessageType.shareContact
                || messageType == ReengMessageConstant.MessageType.voiceSticker
                || messageType == ReengMessageConstant.MessageType.shareLocation;
    }

    private static boolean isMessageBlockForward(ReengMessage reengMessage, ThreadMessage thread, int threadType) {
        boolean check = threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && thread != null && thread.isPrivateThread();
        if (!check) return false;
        ReengMessageConstant.MessageType messageType = reengMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.file
                //Neu file chua upload xong/error thi ko cho forward
                && !TextUtils.isEmpty(reengMessage.getFilePath())
                && !(reengMessage.getStatus() == ReengMessageConstant.STATUS_LOADING
                || reengMessage.getStatus() == ReengMessageConstant.STATUS_FAIL))
            return true;
        if (/*messageType == ReengMessageConstant.MessageType.file
                ||*/ messageType == ReengMessageConstant.MessageType.image
                || messageType == ReengMessageConstant.MessageType.voicemail
                || messageType == ReengMessageConstant.MessageType.watch_video
                /*|| messageType == ReengMessageConstant.MessageType.inviteShareMusic*/
                || messageType == ReengMessageConstant.MessageType.shareVideo) {
            return !(reengMessage.getStatus() == ReengMessageConstant.STATUS_LOADING
                    //Neu file chua upload xong/error thi ko cho forward
                    || reengMessage.getStatus() == ReengMessageConstant.STATUS_FAIL);
        } else return messageType == ReengMessageConstant.MessageType.text
                || messageType == ReengMessageConstant.MessageType.shareContact
                || messageType == ReengMessageConstant.MessageType.voiceSticker
                || messageType == ReengMessageConstant.MessageType.shareLocation;
    }

    private static boolean isMessageCanShare(ReengMessage reengMessage) {
        return reengMessage.getMessageType() == ReengMessageConstant.MessageType.text ||
                reengMessage.getMessageType() == ReengMessageConstant.MessageType.image;
    }

    private static boolean isMessageToSms(ReengMessage reengMessage, ThreadMessage thread, int threadType) {
        ReengMessageConstant.MessageType messageType = reengMessage.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.restore) {
            return false;
        }
        if (thread.isStranger()) return false;
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            if (reengMessage.getChatMode() == ReengMessageConstant.MODE_GSM) {
                return false;
            } else if (messageType == ReengMessageConstant.MessageType.text ||
                    messageType == ReengMessageConstant.MessageType.voiceSticker) {
                // se chi cho manual send sms khi tin nhan loi hoac
                return reengMessage.getStatus() == ReengMessageConstant.STATUS_DELIVERED ||
                        reengMessage.getStatus() == ReengMessageConstant.STATUS_SENT ||
                        reengMessage.getStatus() == ReengMessageConstant.STATUS_LOADING || //TODO fake .......1012111
                        reengMessage.getStatus() == ReengMessageConstant.STATUS_FAIL;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static boolean isMessageRestoreAble(ReengMessage message, int threadType) {
        ReengMessageConstant.MessageType messageType = message.getMessageType();
        if (messageType == ReengMessageConstant.MessageType.restore) {
            return false;
        }
        if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT ||
                threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT ||
                threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return false;
        }
        if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
            return false;
        } else {
            if (message.getDirection() == ReengMessageConstant.Direction.send) {
                if (messageType == ReengMessageConstant.MessageType.text
                        || messageType == ReengMessageConstant.MessageType.shareContact
                        || messageType == ReengMessageConstant.MessageType.voiceSticker
                        || messageType == ReengMessageConstant.MessageType.shareLocation
                        || messageType == ReengMessageConstant.MessageType.image
                        || messageType == ReengMessageConstant.MessageType.shareVideo
                        || messageType == ReengMessageConstant.MessageType.voicemail
                        || messageType == ReengMessageConstant.MessageType.file) {
                    return message.getStatus() == ReengMessageConstant.STATUS_SENT
                            || message.getStatus() == ReengMessageConstant.STATUS_DELIVERED
                            || message.getStatus() == ReengMessageConstant.STATUS_SEEN
                            || message.getStatus() == ReengMessageConstant.STATUS_FAIL;
                }
            }
        }
        return false;
    }

    private static boolean isPinableMessage(ReengMessage message, ThreadMessage thread, int threadType) {
        if (message.getMessageType() == ReengMessageConstant.MessageType.text) {
            /*if (!thread.isPrivateThread()) {
                    return true;
                } else {
                    if (thread.isAdmin()) {
                        return true;
                    }
                }*/
            return threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT;
        }
        return false;
    }

    public static void drawReplyView(ApplicationController application, View replyView,
                                     String friendJid, String messageString, String link, String filePath,
                                     ThreadMessage thread) {
        if (TextUtils.isEmpty(friendJid)) {
            return;
        }
        EllipsisTextView mTvwReplyContent = replyView.findViewById(R.id.message_reply_content);
        EllipsisTextView mTvwReplyName = replyView.findViewById(R.id.message_reply_name);
        View mViwLeftLine = replyView.findViewById(R.id.message_reply_left_line);
        ImageView mImgReply = replyView.findViewById(R.id.message_reply_ic_reply);
        ImageView ivThumb = replyView.findViewById(R.id.ivThumbReply);
        ImageView ivReplyImg = replyView.findViewById(R.id.ivMsgImage);
        mViwLeftLine.setVisibility(View.GONE);
        mImgReply.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(filePath)) {
            if (TextUtils.isEmpty(messageString)) {
                mTvwReplyContent.setText(application.getResources().getString(R.string.image_message));
            } else
                mTvwReplyContent.setEmoticon(application, messageString,
                        TextUtils.isEmpty(messageString) ? mTvwReplyContent.getTextId() : messageString.hashCode(), messageString);
            ivReplyImg.setVisibility(View.VISIBLE);
            ivThumb.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(filePath)) {
                if (new File(FILE_PATH + filePath).exists()) {
                    filePath = FILE_PATH + filePath;
                }
            }
            Glide.with(application)
                    .load(Uri.fromFile(new File(filePath)))
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_images_default)
                            .error(R.drawable.ic_images_default)
                            .fitCenter())
                    .into(ivThumb);
        } else {
            ivReplyImg.setVisibility(View.GONE);
            ivThumb.setVisibility(View.GONE);
            mTvwReplyContent.setEmoticon(application, messageString,
                    TextUtils.isEmpty(messageString) ? mTvwReplyContent.getTextId() : messageString.hashCode(), messageString);
        }
        String myNumber = application.getReengAccountBusiness().getJidNumber();
        String friendName;
        if (friendJid.equals(myNumber)) {
            friendName = application.getReengAccountBusiness().getUserName();
        } else {
            PhoneNumber phoneNumber = application.getContactBusiness().getPhoneNumberFromNumber(friendJid);
            if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                friendName = phoneNumber.getName();
            } else {                      //hien thi so dien thoai neu la group chat
                if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    friendName = application.getMessageBusiness().getFriendName(friendJid);
                } else {
                    NonContact nonContact = application.getContactBusiness().getExistNonContact(friendJid);
                    if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                        friendName = nonContact.getNickName();
                    } else {
                        friendName = friendJid;
                    }
                }
            }
        }
        mTvwReplyName.setText(friendName);
    }

    public static void drawReplyViewBubble(final ApplicationController application, View replyView,
                                           final ReplyMessage replyMessage,
                                           final ThreadMessage thread,
                                           final boolean isSend,
                                           final MessageInteractionListener listener) {
        if (thread == null || replyMessage == null || TextUtils.isEmpty(replyMessage.getMember())) {
            return;
        }
        final EllipsisTextView mTvwReplyContent = replyView.findViewById(R.id.message_reply_content);
        EllipsisTextView mTvwReplyName = replyView.findViewById(R.id.message_reply_name);
        View mViwLeftLine = replyView.findViewById(R.id.message_reply_left_line);
        ImageView mImgReply = replyView.findViewById(R.id.message_reply_ic_reply);
        ImageView ivThumb = replyView.findViewById(R.id.ivThumbReply);
        ImageView ivReplyImg = replyView.findViewById(R.id.ivMsgImage);
        ivReplyImg.setVisibility(View.GONE);
        ivThumb.setVisibility(View.GONE);
        mTvwReplyContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(application,
                Constants.FONT_SIZE.LEVEL_1_5));
        mTvwReplyName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(application,
                Constants.FONT_SIZE.LEVEL_2));
        mViwLeftLine.setVisibility(View.GONE);
        mImgReply.setVisibility(View.VISIBLE);
        replyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick reply: line " + mTvwReplyContent.getMaxLines());
                if (mTvwReplyContent.getMaxLines() == 1) {
                    mTvwReplyContent.setMaxLines(Integer.MAX_VALUE);
                    mTvwReplyContent.setEllipsize(null);
                } else {
                    mTvwReplyContent.setMaxLines(1);
                    mTvwReplyContent.setEllipsize(TextUtils.TruncateAt.END);
                }
            }
        });
        String myNumber = application.getReengAccountBusiness().getJidNumber();
        String friendName;
        if (replyMessage.getMember().equals(myNumber)) {
            friendName = application.getReengAccountBusiness().getUserName();
        } else {
            PhoneNumber phoneNumber = application.getContactBusiness().getPhoneNumberFromNumber(replyMessage.getMember());
            if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                friendName = phoneNumber.getName();
            } else {                      //hien thi so dien thoai neu la group chat
                if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    friendName = application.getMessageBusiness().getFriendName(replyMessage.getMember());
                } else {
                    NonContact nonContact = application.getContactBusiness().getExistNonContact(replyMessage.getMember());
                    if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                        friendName = nonContact.getNickName();
                    } else {
                        friendName = replyMessage.getMember();
                    }
                }
            }
        }
        mTvwReplyName.setText(friendName);

        LinearLayout llReplyImage = replyView.findViewById(R.id.llReplyImage);
        LinearLayout llReplyText = replyView.findViewById(R.id.llReplyText);
        if (replyMessage.getSubType() == ReengMessagePacket.SubType.image) {
            llReplyImage.setVisibility(View.VISIBLE);
            llReplyText.setVisibility(View.GONE);

            EllipsisTextView tvReplyCaption = replyView.findViewById(R.id.tvReplyCaption);
            final RoundedImageView ivReplyImage = replyView.findViewById(R.id.ivReplyImage);
            final int width = application.getResources().getDimensionPixelOffset(R.dimen.width_reply_image);
            final int height = application.getResources().getDimensionPixelOffset(R.dimen.height_reply_image);
            if (TextUtils.isEmpty(replyMessage.getBody())) {
                tvReplyCaption.setVisibility(View.GONE);
            } else {
                tvReplyCaption.setVisibility(View.VISIBLE);
                if (replyMessage.isExpandedContent()) {
                    tvReplyCaption.setMaxHeight(1000);
                    tvReplyCaption.setMaxLines(100);
                } else {
                    tvReplyCaption.setMaxHeight(height);
                    tvReplyCaption.setMaxLines(5);
                }
                tvReplyCaption.setEmoticon(application, replyMessage.getBody(), replyMessage.getBody().hashCode(), replyMessage.getBody());
                tvReplyCaption.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (replyMessage.isExpandedContent()) {
                            replyMessage.setExpandedContent(false);
                        } else {
                            replyMessage.setExpandedContent(true);
                        }
                        application.getMessageBusiness().refreshThreadWithoutNewMessage(thread.getId());
                    }
                });
            }

            ivReplyImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClickImageReply(replyMessage);
                    }
                }
            });
            if (isSend && !TextUtils.isEmpty(replyMessage.getFilePath())) {
                Log.i(TAG, "mình đi reply ảnh người khác");
                loadThumbReplyImage(application, replyMessage, width, height, ivReplyImage);
            } else {
                StringBuilder fileSb = new StringBuilder(com.metfone.selfcare.helper.Config.Storage.REENG_STORAGE_FOLDER);
                fileSb.append(com.metfone.selfcare.helper.Config.Storage.IMAGE_FOLDER);
                if (!TextUtils.isEmpty(replyMessage.getMsgId())) {
                    fileSb.append("/").append(getNameFileImage(replyMessage.getMsgId()));
                    String filePath = fileSb.toString();
                    if ((new File(filePath)).exists()) {
                        Log.i(TAG, "tồn tại file filepath: " + filePath);
                        replyMessage.setFilePath(filePath);
                        loadThumbReplyImage(application, replyMessage, width, height, ivReplyImage);
                    } else {
                        Log.i(TAG, "ko tìm được file, load online");
                        Glide.with(application)
                                .load(UrlConfigHelper.getInstance(application).getDomainImage() + replyMessage.getImgLink())
                                .apply(new RequestOptions()
                                        .placeholder(R.drawable.ic_images_default)
                                        .error(R.drawable.ic_images_default)
                                        .override(width, height)
                                        .fitCenter())
                                .into(ivReplyImage);
                    }
                } else {
                    Log.i(TAG, "wtf ko có msgid");
                    llReplyText.setVisibility(View.VISIBLE);
                    llReplyImage.setVisibility(View.GONE);
                }

            }

        } else {
            llReplyText.setVisibility(View.VISIBLE);
            llReplyImage.setVisibility(View.GONE);
            mTvwReplyContent.setEmoticon(application, replyMessage.getBody(), replyMessage.getBody().hashCode(), replyMessage.getBody());
        }
    }

    private static void loadThumbReplyImage(final ApplicationController application, final ReplyMessage replyMessage, final int width, final int height, final ImageView ivReplyImage) {
        Log.i(TAG, "loadThumbReplyImage: " + replyMessage.toString());
        String filePath = FILE_PATH + replyMessage.getFilePath();
        if (new File(replyMessage.getFilePath()).exists()) {
            filePath = replyMessage.getFilePath();
        } else if (new File(FILE_PATH + replyMessage.getFilePath()).exists()) {
            filePath = FILE_PATH + replyMessage.getFilePath();
        }
        Glide.with(application)
                .load(Uri.fromFile(new File(filePath)))
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_images_default)
                        .error(R.drawable.ic_images_default)
                        .override(width, height)
                        .fitCenter())
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        ivReplyImage.setImageDrawable(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        Log.i(TAG, "loadThumbReplyImage ảnh đã bị xóa, load online");
                        Glide.with(application)
                                .load(UrlConfigHelper.getInstance(application).getDomainImage() + replyMessage.getImgLink())
                                .apply(new RequestOptions()
                                        .placeholder(R.drawable.ic_images_default)
                                        .error(R.drawable.ic_images_default)
                                        .override(width, height)
                                        .fitCenter())
                                .into(ivReplyImage);
                    }
                });
    }

    public static ArrayList<ReengMessage> getMessagesTextFromIntent(Intent intent, Context context) {
        String text = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (text == null) {
            if (intent.getCharSequenceExtra(Intent.EXTRA_TEXT) == null) {
                return null;
            } else {
                text = intent.getCharArrayExtra(Intent.EXTRA_TEXT).toString();
            }
        }
        String subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);
        if (!TextUtils.isEmpty(text)) {
            ArrayList<ReengMessage> messageToShare = new ArrayList<>();
            ReengMessage msg = new ReengMessage();
            if ((text.startsWith("http://") || text.startsWith("https://")) && subject != null && subject.length() !=
                    0) {
                msg.setFilePath(text);
                text = subject + "\n" + text;
            }
            msg.setMessageType(ReengMessageConstant.MessageType.text);
            msg.setContent(text);
            messageToShare.add(msg);
            return messageToShare;
        }
        return null;
    }

    public static ArrayList<ReengMessage> getMessageImageFromIntent(Intent intent, Context mContext) {
        Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        Log.d(TAG, "imageUri: " + imageUri);
        if (imageUri != null) {
            String imagePath = FileHelper.getRealPath(mContext, imageUri);
            /*if ("file".equals(imageUri.getScheme())) {
                imagePath = imageUri.getPath();
            } else {
                String imageFilePath = FileHelper.getRealPathImageFromURI(mContext, imageUri);
                if (imageFilePath != null) {
                    imagePath = imageFilePath;
                    Log.d(TAG, " Image share: " + imageFilePath);
                }
            }*/
            if (!TextUtils.isEmpty(imagePath)) {
                ReengMessage msg = new ReengMessage();
                msg.setFilePath(imagePath);
                msg.setMessageType(ReengMessageConstant.MessageType.image);
                ArrayList<ReengMessage> messageToShare = new ArrayList<>();
                messageToShare.add(msg);
                return messageToShare;
            }
        }
        return null;
    }

    public static ArrayList<ReengMessage> getListMessageImageFromIntent(Intent intent, Context mContext) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            ArrayList<String> arrayImagePath = new ArrayList<>();
            for (Uri imgUri : imageUris) {
                arrayImagePath.add(FileHelper.getRealPath(mContext, imgUri));
                /*if ("file".equals(imgUri.getScheme())) {
                    arrayImagePath.add(imgUri.getPath());
                } else {
                    imageFilePath = FileHelper.getRealPathImageFromURI(mContext, imgUri);
                    if (imageFilePath != null) {
                        arrayImagePath.add(imageFilePath);
                        Log.d(TAG, " List image: " + imageFilePath);
                    }
                }*/
            }
            ArrayList<ReengMessage> messageToShare = new ArrayList<>();
            if (arrayImagePath.size() > 0) {
                for (String imagePath : arrayImagePath) {
                    if (!TextUtils.isEmpty(imagePath)) {
                        ReengMessage msg = new ReengMessage();
                        msg.setFilePath(imagePath);
                        msg.setMessageType(ReengMessageConstant.MessageType.image);
                        messageToShare.add(msg);
                    }
                }
                return messageToShare;
            }
        }
        return null;
    }

    public static ArrayList<ReengMessage> getListMessageContactFromIntent(Intent intent, Context mContext) {
        try {
            Uri uri = (Uri) intent.getExtras().get(Intent.EXTRA_STREAM);
            if (uri != null) {
                ContentResolver cr = mContext.getContentResolver();
                InputStream stream = cr.openInputStream(uri);
                String name = null;
                String nameEncoding = null;
                String nameCharset = null;
                ArrayList<String> phones = new ArrayList<>();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] args = line.split(":");
                    if (args.length != 2) {
                        continue;
                    }
                    if (args[0].startsWith("FN")) {
                        String[] params = args[0].split(";");
                        for (String param : params) {
                            String[] args2 = param.split("=");
                            if (args2.length != 2) {
                                continue;
                            }
                            if ("CHARSET".equals(args2[0])) {
                                nameCharset = args2[1];
                            } else if ("ENCODING".equals(args2[0])) {
                                nameEncoding = args2[1];
                            }
                        }
                        name = args[1];
                        if (nameEncoding != null && "QUOTED-PRINTABLE".equalsIgnoreCase(nameEncoding)) {
                            while (name.endsWith("=") && nameEncoding != null) {
                                name = name.substring(0, name.length() - 1);
                                line = bufferedReader.readLine();
                                if (line == null) {
                                    break;
                                }
                                name += line;
                            }
                            byte[] bytes = decodeQuotedPrintable(name.getBytes());
                            if (bytes != null && bytes.length != 0) {
                                try {
                                    name = new String(bytes, nameCharset);
                                    Log.i(TAG, "name: " + name);
                                } catch (UnsupportedEncodingException e) {
                                    Log.e(TAG, "UnsupportedEncodingException", e);
                                }
                            }
                        }
                    } else if (args[0].startsWith("TEL")) {
                        String phone = stripExceptNumbers(args[1], true);
                        if (phone.length() > 0) {
                            phones.add(phone);
                        }
                    }
                }
                if (!phones.isEmpty()) {
                    ArrayList<ReengMessage> messageToShare = new ArrayList<>();
                    for (String phone : phones) {
                        ReengMessage msg = new ReengMessage();
                        msg.setMessageType(ReengMessageConstant.MessageType.shareContact);
                        msg.setFileName(phone);
                        if (name != null) {
                            msg.setContent(name);
                        } else {
                            msg.setContent(phone);
                        }
                        messageToShare.add(msg);
                    }
                    return messageToShare;
                }
            } else {
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    private static byte[] decodeQuotedPrintable(final byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        /*for (int i = 0; i < bytes.length; i++) {
            final int b = bytes[i];
            if (b == '=') {
                try {
                    final int u = Character.digit((char) bytes[++i], 16);
                    final int l = Character.digit((char) bytes[++i], 16);
                    buffer.write((char) ((u << 4) + l));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return null;
                }
            } else {
                buffer.write(b);
            }
        }*/
        //TODO fix loi thay doi bien dem trong vong lap for
        int j = 0;
        while (j < bytes.length) {
            final int b = bytes[j];
            if (b == '=') {
                try {
                    final int u = Character.digit((char) bytes[++j], 16);
                    final int l = Character.digit((char) bytes[++j], 16);
                    buffer.write((char) ((u << 4) + l));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return null;
                }
            } else {
                buffer.write(b);
            }
            j++;
        }

        return buffer.toByteArray();
    }

    private static String stripExceptNumbers(String str, boolean includePlus) {
        StringBuilder res = new StringBuilder(str);
        String phoneChars = "0123456789";
        if (includePlus) {
            phoneChars += "+";
        }
        for (int i = res.length() - 1; i >= 0; i--) {
            if (!phoneChars.contains(res.substring(i, i + 1))) {
                res.deleteCharAt(i);
            }
        }
        return res.toString();
    }

    public static void checkFirstUrlAndGetMetaData(ReengMessage message, ApplicationController mApp) {
        if (message.getMessageType() != ReengMessageConstant.MessageType.text ||
                message.isPlaying() || message.getSongId() == -1) return;
        String s = message.getContent();
        try {
            String[] splited = s.split("\\s+");
            if (splited.length > 0) {
                for (String aSplited : splited) {
                    if (Patterns.WEB_URL.matcher(aSplited).matches()) {
                        try {
                            URL link = new URL(aSplited);
                            link.getHost();
                            handleGetMetaData(aSplited, message, mApp);
                            return;
                        } catch (Exception ex) {
                            Log.e(TAG, "Exception checkFirstUrlAndGetMetaData", ex);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        Log.i(TAG, "content ko chua url");
        message.setSongId(-1);
        mApp.getMessageBusiness().updateAllFieldsOfMessage(message);
    }

    private static void handleGetMetaData(String url, final ReengMessage message, final ApplicationController mApp) {
        message.setPlaying(true);
        WSOnMedia wsOnMedia = new WSOnMedia(mApp);
        wsOnMedia.getMetaData(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "getMetaDataFromServer: " + s);
                message.setPlaying(false);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has("code")) {
                        int code = Integer.parseInt(jsonObject.getString("code"));
                        if (code == HTTPCode.E200_OK) {
                            if (jsonObject.has("content")) {
                                String jsonContent = jsonObject.getString("content");
                                Log.i(TAG, "content: " + jsonContent);
                                FeedContent feedContent = new Gson().fromJson(jsonContent, FeedContent.class);
                                if (feedContent != null) {
                                    Log.i(TAG, "feedcontent: ok");
                                    message.setFilePath(jsonContent);
                                    mApp.getMessageBusiness().updateAllFieldsOfMessage(message);
                                    mApp.getMessageBusiness().refreshThreadWithoutNewMessage(message.getThreadId());
                                } else {
//                                    mActivity.showToast(R.string.e601_error_but_undefined);
                                }
                            } else {
//                                mActivity.showToast(R.string.e601_error_but_undefined);
                            }
                        } else {
                            if (jsonObject.has("desc")) {
                                String desc = jsonObject.getString("desc");
                                Log.e(TAG, "Error " + desc);
                            } else {
                                Log.e(TAG, "Error 2");
                            }
                        }
                    } else {
                        Log.e(TAG, "Error 1");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getMetaDataFromServer parse error", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError : "+ volleyError.getMessage());
                message.setPlaying(false);
            }
        }, false);
    }

    public static float getTextSizeChat(Context context, int level) {
        Resources res = context.getResources();
        float ratio = Constants.FONT_SIZE.MEDIUM_RATIO;
        if (SettingBusiness.getInstance(context).getFontSize() == Constants.FONT_SIZE.SMALL) {
            ratio = Constants.FONT_SIZE.SMALL_RATIO;
        } else if (SettingBusiness.getInstance(context).getFontSize() == Constants.FONT_SIZE.LARGE) {
            ratio = Constants.FONT_SIZE.LARGE_RATIO;
        }
        if (level == Constants.FONT_SIZE.LEVEL_0) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_0);
        } else if (level == Constants.FONT_SIZE.LEVEL_1) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_1);
        } else if (level == Constants.FONT_SIZE.LEVEL_1_5) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_1_5);
        } else if (level == Constants.FONT_SIZE.LEVEL_2) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_2);
        } else if (level == Constants.FONT_SIZE.LEVEL_2_5) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_2_5);
        } else if (level == Constants.FONT_SIZE.LEVEL_3) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_3);
        } else if (level == Constants.FONT_SIZE.LEVEL_4) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_4);
        } else if (level == Constants.FONT_SIZE.LEVEL_6) {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_6);
        } else {
            return ratio * res.getDimension(R.dimen.mocha_text_size_level_3);
        }
    }

    public static File checkFile(String filePath, String fileName, Context context) {
        if (TextUtils.isEmpty(filePath)) return null;
        File file = new File(filePath);
        if (file.exists()) {
            if (isImage(filePath)) {
                if (file.length() > com.metfone.selfcare.helper.Constants.MESSAGE.MAX_TRANSFER_PHOTO_SIZE) {
                    boolean isHD = SettingBusiness.getInstance(context).getPrefEnableHDImage();
                    String compressedFilePath = MessageHelper.getPathOfCompressedFile(
                            filePath, com.metfone.selfcare.helper.Config.Storage.IMAGE_COMPRESSED_FOLDER, fileName, isHD);
                    if (compressedFilePath != null)
                        return new File(compressedFilePath);
                }
                String prePath = com.metfone.selfcare.helper.Config.Storage.REENG_STORAGE_FOLDER
                        + com.metfone.selfcare.helper.Config.Storage.IMAGE_COMPRESSED_FOLDER;
                String newFilePath = prePath + "/" + fileName;
                return FileHelper.copyFile(filePath, newFilePath);
            } else
                return file;
        }
        return null;
    }

    public static String getNameFileImage(String fileName) {
        if (MessageHelper.isImage(fileName)) {
            return fileName;
        } else {
            return fileName + ".jpg";
        }
    }

    public static void showAnimReation(BaseSlidingFragmentActivity activity, View anchor, int resId) {
        int newPx = activity.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_15);
        // Read your drawable from somewhere
        Drawable dr = activity.getResources().getDrawable(resId);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        // Scale it to 50 x 50
        Drawable d = new BitmapDrawable(activity.getResources(), Bitmap.createScaledBitmap(bitmap, newPx, newPx, true));
        // Set your new, scaled drawable "d"

        new ParticleSystem(activity, 20, d, 500)
                .setSpeedRange(0.1f, 0.25f)
                .oneShot(anchor, 20);
    }
}
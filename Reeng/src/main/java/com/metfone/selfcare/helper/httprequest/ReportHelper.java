package com.metfone.selfcare.helper.httprequest;

import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.avno.ConfirmFakeMO;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.model.home.AdsMainTab;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 11/14/2015.
 */
public class ReportHelper {
    private static final String TAG = ReportHelper.class.getSimpleName();
    private static final String TAG_SIEU_HAI = "SIEU_HAI";
    private static final String TAG_CALL_STATE = "CALL_STATE";
    private static final String TAG_CALL_QUALITY = "CALL_QUALITY";

    public static final String DATA_TYPE_TOGHETHER_MUSIC = "TOGHETHER_MUSIC";
    public static final String DATA_TYPE_TAB_CONFIG = "TAB_CONFIG";

    public static final String DATA_TYPE_NEWS_HOME = "NEWS_HOME";
    public static final String DATA_TYPE_NEWS_DETAIL = "NEWS_DETAIL";
    public static final String VIDEO_SERVICE_ERROR = "VIDEO_SERVICE_ERROR";

    public static final String DATA_TYPE_TIIN_HOME = "TIIN_HOME";
    public static final String DATA_TYPE_TIIN_DETAIL = "TIIN_DETAIL";
    public static final String DATA_TYPE_TIIN_CATEGORY = "TIIN_CATEGORY";

    public static final String DATA_TYPE_GAME_LIVE_MESSAGE = "GAME_LIVE_MESSAGE";
    public static final String DATA_TYPE_GAME_LIVE_SCREEN = "GAME_LIVE_SCREEN";

    public static final String DATA_TYPE_LIVE_STREAM_MESSAGE = "LIVE_STREAM_MESSAGE";
    public static final String DATA_TYPE_LIVE_STREAM_SCREEN = "LIVE_STREAM_SCREEN";

    public static final String DATA_TYPE_POPUP_VIP = "POPUP_VIP";

    /**
     * -
     *
     * @param app
     * @param phoneNumber
     * @param token
     * @param errorString
     */
    public static void reportLockAccount(ApplicationController app, final String phoneNumber, final String token, final String errorString) {
        String url = UrlConfigHelper.getInstance(app).getUrlConfigOfFile(Config.UrlEnum.REPORT_ERROR);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "reportLockAccount onResponse" + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "reportLockAccount onErrorResponse" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("username", phoneNumber);
                params.put("data", buildErrorString(token, errorString));
                return params;
            }
        };
        VolleyHelper.getInstance(app).addRequestToQueue(request, TAG, false);
    }

    private static String buildErrorString(String token, String errorString) {
        return "token: " + token + "; error: " + errorString + " CLIENT_TYPE: " + Constants.HTTP.CLIENT_TYPE_STRING;
    }

    public static void showConfirmAndReconfirmSubscription(final ApplicationController application,
                                                           final BaseSlidingFragmentActivity activity,
                                                           String confirm, final ConfirmFakeMO reConfirm, final String fakeMoId, final String chanel) {
        final Resources mRes = application.getResources();
        new DialogConfirm(activity, true).setLabel(null).setMessage(confirm).
                setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                setEntry(fakeMoId).setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                requestFakeMo(application, activity, fakeMoId, chanel, false, reConfirm);
            }
        }).show();
    }

    public static void checkShowConfirmOrRequestFakeMo(final ApplicationController application,
                                                       final BaseSlidingFragmentActivity activity,
                                                       String confirm, final String fakeMoId, final String chanel) {
        checkShowConfirmOrRequestFakeMo(application, activity, confirm, fakeMoId, chanel, false);
    }

    public static void checkShowConfirmOrRequestFakeMo(final ApplicationController application,
                                                       final BaseSlidingFragmentActivity activity,
                                                       String confirm, final String fakeMoId,
                                                       final String chanel, final boolean unVip) {
        Resources mRes = application.getResources();
        if (TextUtils.isEmpty(confirm)) {
            requestFakeMo(application, activity, fakeMoId, chanel, unVip, null);
        } else {
            new DialogConfirm(activity, true).setLabel(null).setMessage(confirm).
                    setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                    setEntry(fakeMoId).setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    requestFakeMo(application, activity, fakeMoId, chanel, unVip, null);
                }
            }).show();
        }
    }

    public static void showDialogFromTabVideo(final ApplicationController app,
                                              final BaseSlidingFragmentActivity activity,
                                              final AdsRegisterVip adsRegisterVip,
                                              final NegativeListener<Object> negativeListener) {
        final Resources mRes = app.getResources();
        PositiveListener<Object> listener;
        if (adsRegisterVip.isSMS()) {
            listener = new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    NavigateActivityHelper.openAppSMS(activity, adsRegisterVip.getSmsCodes(), adsRegisterVip.getSmsCommand());
                }
            };
        } else if (adsRegisterVip.isDeeplink()) {
            listener = new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, adsRegisterVip.getDeeplink());
                }
            };
        } else {
            listener = new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    new DialogConfirm(activity, true).setLabel(null).setMessage(adsRegisterVip.getConfirmString()).
                            setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                            setEntry(adsRegisterVip).setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            requestFakeMo(app, activity, adsRegisterVip.getCommand(), "tab_video", false, null);
                        }
                    }).setNegativeListener(negativeListener).show();
                }
            };
        }

        if (TextUtils.isEmpty(adsRegisterVip.getTitleButton())) {
            new DialogMessage(activity, true).setLabelButton(mRes.getString(R.string.close))
                    .setLabel(adsRegisterVip.getTitle()).setMessage(adsRegisterVip.getContent())
                    .setNegativeListener(negativeListener).show();
        } else
            new DialogConfirm(activity, true).setLabel(adsRegisterVip.getTitle()).setMessage(adsRegisterVip.getContent()).
                    setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(adsRegisterVip.getTitleButton()).
                    setEntry(adsRegisterVip).setPositiveListener(listener).setNegativeListener(negativeListener).show();
    }

    public static void requestFakeMo(final ApplicationController app, final BaseSlidingFragmentActivity activity
            , final String action, final String channel, final boolean unVip, final ConfirmFakeMO subsConfirm) {
        String domain = ApplicationController.self().getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_FAKE_MO);
        String url = domain + ConstantApi.Url.File.API_FAKE_MO;
        final Resources mRes = app.getResources();
        activity.showLoadingDialog(null, app.getResources().getString(R.string.waiting));
        com.metfone.selfcare.restful.StringRequest request = new com.metfone.selfcare.restful.StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int code = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                code = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (activity == null) return;
                            activity.hideLoadingDialog();
                            String desc;
                            if (responseObject.has("desc")) {
                                desc = responseObject.optString("desc");
                            } else {
                                if (unVip) {
                                    desc = activity.getResources().getString(R.string.msg_un_vip_successfully);
                                } else {
                                    desc = activity.getResources().getString(R.string.request_success);
                                }
                            }

                            if (code == HTTPCode.E200_OK) {
                                if (subsConfirm == null)
                                    activity.showToast(desc);
                                else {
                                    new DialogConfirm(activity, true).setLabel(null).setMessage(subsConfirm.getDesc()).
                                            setNegativeLabel(mRes.getString(R.string.close)).setPositiveLabel(subsConfirm.getLabel()).
                                            setEntry(subsConfirm).setPositiveListener(new PositiveListener<Object>() {
                                        @Override
                                        public void onPositive(Object result) {
                                            DeepLinkHelper.getInstance().openSchemaLink(activity, subsConfirm.getUrl());
                                        }
                                    }).show();
                                }
                            } else {
                                activity.showToast(app.getResources().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                            }
                        } catch (Exception e) {
                            if (activity != null) {
                                activity.hideLoadingDialog();
                                activity.showToast(app.getResources().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                            }
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (activity != null) {
                    activity.hideLoadingDialog();
                    activity.showToast(app.getResources().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(action).
                        append(channel).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(app.getReengAccountBusiness().getRegionCode()).
                        append(app.getReengAccountBusiness().getCurrentLanguage()).
                        append(account.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("languageCode", app.getReengAccountBusiness().getCurrentLanguage());
                params.put("countryCode", app.getReengAccountBusiness().getRegionCode());
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put("cmd", action);
                params.put("channel", channel);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY,
                        HttpHelper.encryptDataV2(app, sb.toString(), account.getToken()));
                return params;
            }
        };
        VolleyHelper.getInstance(app).addRequestToQueue(request, TAG, false);
    }

    public static void requestShareLinkSieuHai(final ApplicationController application, final String youtubeUrl, final onShareSieuHaiListener listener) {
        if (!application.getReengAccountBusiness().isValidAccount()) {
            listener.onError(application.getResources().getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_SIEU_HAI);
        //long currentTime = TimeHelper.getCurrentTime();
        String url = UrlConfigHelper.getInstance(application).getUrlConfigOfFile(Config.UrlEnum.SHARE_SIEU_HAI);
        Log.d(TAG, "url: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "requestShareLinkSieuHai onResponse : " + response);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E0_OK) {
                                listener.onSuccess(responseObject.optString("result", application.getResources().getString(R.string.request_success)));
                            } else {
                                listener.onError(responseObject.optString("result", application.getResources().getString(R.string.e601_error_but_undefined)));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(application.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        listener.onError(application.getResources().getString(R.string.e601_error_but_undefined));
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("link", youtubeUrl);
                params.put(Constants.HTTP.REST_MSISDN, application.getReengAccountBusiness().getJidNumber());
                params.put(Constants.HTTP.REST_USER_NAME, application.getReengAccountBusiness().getUserName());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(stringRequest, TAG_SIEU_HAI, false);
    }

    public static void requestCallConnecting(ApplicationController application, final String caller,
                                             final String callee, final String session,
                                             final int candidate, final long duration) {
        if (!application.getReengAccountBusiness().isValidAccount()) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_CALL_STATE);
        //long currentTime = TimeHelper.getCurrentTime();
        String url = UrlConfigHelper.getInstance(application).getUrlConfigOfFile(Config.UrlEnum.LOGGER_CALL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "requestCallConnecting : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                JSONObject object = new JSONObject();
                try {
                    object.put("caller", caller);
                    object.put("callee", callee);
                    object.put("session", session);
                    object.put("platform", Constants.HTTP.CLIENT_TYPE_STRING);
                    object.put("candidate", candidate);
                    object.put("duration", duration);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                params.put("data", object.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(stringRequest, TAG_CALL_STATE, false);
    }

    public static void reportCallQuality(ApplicationController application, final String caller,
                                         final String callee, final String session,
                                         final String type, final String bytesReceived,
                                         final boolean isCaller) {
        if (!application.getReengAccountBusiness().isValidAccount()) {
            return;
        }
        VolleyHelper.getInstance(application).cancelPendingRequests(TAG_CALL_QUALITY);
        String url = UrlConfigHelper.getInstance(application).getUrlConfigOfFile(Config.UrlEnum.LOGGER_CALL_QUALITY);
        Log.f(TAG, "--------------REPORT------------ start call api reportCallQuality");
        //String url = "http://171.255.193.141:80/ReengBackendBiz/call_logger/report_quality";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "reportCallQuality : " + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("caller", caller);
                params.put("callee", callee);
                params.put("session", session);
                params.put("platform", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("type", type);
                params.put("is_caller", String.valueOf(isCaller));
                params.put("bytesRecv", bytesReceived);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(application).addRequestToQueue(stringRequest, TAG_CALL_QUALITY, false);
    }

    public static void showDialogRegisterVipWC(final ApplicationController application,
                                               final BaseSlidingFragmentActivity activity,
                                               final String confirm) {
        /*Resources mRes = application.getResources();
        String labelOK = mRes.getString(R.string.register);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                .REGISTER_VIP_BUTTON);
        String msg = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                .REGISTER_VIP_WC_CONFIRM);
        PopupHelper.getInstance(application).showDialogConfirm(activity, title,
                msg, labelOK, labelCancel, new ClickListener.IconListener() {
                    @Override
                    public void onIconClickListener(View view, Object entry, int menuId) {
                        switch (menuId) {
                            case Constants.MENU.POPUP_CONFIRM_REGISTER_VIP:
                                ReportHelper.checkShowConfirmOrRequestFakeMo(application, activity, confirm,
                                        application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                                                .REGISTER_VIP_CMD), "tab_video");
                                break;
                            default:
                                break;
                        }
                    }
                }, null, Constants.MENU.POPUP_CONFIRM_REGISTER_VIP);*/
    }

    public interface onShareSieuHaiListener {
        void onSuccess(String desc);

        void onError(String desc);
    }

    public static void reportError(final ApplicationController app, final String dataType, final String data) {
        if (!app.getReengAccountBusiness().isValidAccount()) return;
        String url = UrlConfigHelper.getInstance(app).getUrlConfigOfFile(Config.UrlEnum.LOG_ERROR);
        app.logDebugContent("dataType: " + dataType + " data: " + data);
        Log.f(TAG, "report error: " + dataType + " data: " + data);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "reportError onResponse: " + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "reportError onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                final String timeStamp = String.valueOf(System.currentTimeMillis());
                StringBuilder sb = new StringBuilder();
                sb.append(app.getReengAccountBusiness().getJidNumber()).append(data).append(dataType)
                        .append(Constants.HTTP.CLIENT_TYPE_STRING).append(Config.REVISION)
                        .append(app.getReengAccountBusiness().getToken()).append(timeStamp);
                params.put("msisdn", app.getReengAccountBusiness().getJidNumber());
                params.put("data", data);
                params.put("dataType", dataType);
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put("uuid", CommonUtils.getUIID(app));
                params.put("timestamp", timeStamp);
                params.put("security", HttpHelper.encryptDataV2(app, sb.toString(),
                        app.getReengAccountBusiness().getToken()));
                return params;
            }
        };
        VolleyHelper.getInstance(app).addRequestToQueue(request, TAG, false);
    }

    public static void reportDhVtt(final ApplicationController app, final BaseSlidingFragmentActivity activity,
                                   ThreadMessage threadMessage, ReengMessage reengMessage) {
        String url = UrlConfigHelper.getInstance(activity).getDomainFile() + ConstantApi.Url.File.API_REPORT_DH_VTT;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("group_id", threadMessage.getServerId());
            jsonObject.put("group_name", threadMessage.getThreadName());
            jsonObject.put("msg", reengMessage.getContent());
            jsonObject.put("created_date", reengMessage.getTime());
            jsonObject.put("sender", reengMessage.getSender());
            jsonObject.put("dhvtt", "1");
            if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.image)
                jsonObject.put("media_link", reengMessage.getDirectLinkMedia());
        } catch (Exception e) {
            e.printStackTrace();
        }
        final String data = jsonObject.toString();
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "reportDhVtt onResponse: " + response);
                        try {
                            JSONObject js = new JSONObject(response);
                            String desc = js.optString("desc");
                            activity.showToast(desc);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "reportError onErrorResponse: " + error.toString());
                activity.showToast(R.string.e601_error_but_undefined);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                final String timeStamp = String.valueOf(System.currentTimeMillis());
                StringBuilder sb = new StringBuilder();
                sb.append(app.getReengAccountBusiness().getJidNumber()).append(data)
                        .append(Constants.HTTP.CLIENT_TYPE_STRING).append(Config.REVISION)
                        .append(app.getReengAccountBusiness().getCurrentLanguage())
                        .append(app.getReengAccountBusiness().getRegionCode())
                        .append(app.getReengAccountBusiness().getToken()).append(timeStamp);

                params.put("msisdn", app.getReengAccountBusiness().getJidNumber());
                params.put("data", data);
                params.put("languageCode", app.getReengAccountBusiness().getCurrentLanguage());
                params.put("countryCode", app.getReengAccountBusiness().getRegionCode());
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put("timestamp", timeStamp);
                params.put("security", HttpHelper.encryptDataV2(app, sb.toString(),
                        app.getReengAccountBusiness().getToken()));

                return params;
            }
        };
        VolleyHelper.getInstance(app).addRequestToQueue(request, TAG, false);
    }

    public static void showPopupOnMainTab(final ApplicationController app
            , final BaseSlidingFragmentActivity activity, final AdsMainTab adsMainTab, final String channel) {
        if (!TextUtils.isEmpty(adsMainTab.getContent()) && !TextUtils.isEmpty(adsMainTab.getButtonCancel())
                && !TextUtils.isEmpty(adsMainTab.getButtonOk())) {
            PositiveListener<Object> listener;
            if (adsMainTab.isSMS()) {
                listener = new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        NavigateActivityHelper.openAppSMS(activity, adsMainTab.getSmsCodes(), adsMainTab.getSmsCommand());
                    }
                };
            } else {
                listener = new PositiveListener<Object>() {
                    @Override
                    public void onPositive(Object result) {
                        if (!TextUtils.isEmpty(adsMainTab.getConfirmString())
                                && !TextUtils.isEmpty(adsMainTab.getButtonCancel()) && !TextUtils.isEmpty(adsMainTab.getButtonOk())) {
                            new DialogConfirm(activity, true).setLabel(null).setMessage(adsMainTab.getConfirmString()).
                                    setNegativeLabel(adsMainTab.getButtonConfirmCancel()).setPositiveLabel(adsMainTab.getButtonConfirmOk()).
                                    setEntry(adsMainTab).setPositiveListener(new PositiveListener<Object>() {
                                @Override
                                public void onPositive(Object result) {
                                    requestFakeMo(app, activity, adsMainTab.getCommand(), channel, false, null);
                                }
                            }).show();
                        } else {
                            requestFakeMo(app, activity, adsMainTab.getCommand(), channel, false, null);
                        }
                    }
                };
            }

            new DialogConfirm(activity, true).setLabel(adsMainTab.getTitle()).setMessage(adsMainTab.getContent()).
                    setNegativeLabel(adsMainTab.getButtonCancel()).setPositiveLabel(adsMainTab.getButtonOk()).
                    setEntry(adsMainTab).setPositiveListener(listener).show();
        }
    }
}
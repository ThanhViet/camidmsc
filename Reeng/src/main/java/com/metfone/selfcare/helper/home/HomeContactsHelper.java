package com.metfone.selfcare.helper.home;

import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.helper.httprequest.RoomChatRequestHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toanvk2 on 9/6/2017.
 */

public class HomeContactsHelper {
    private static final String TAG = HomeContactsHelper.class.getSimpleName();
    private static final String TAG_SUGGEST = "TAG_SUGGEST";
    private static final String TAG_SEARCH_USER = "TAG_SEARCH_USER";
    private static HomeContactsHelper mInstance;
    private ApplicationController mApplication;
    private List<OfficerAccount> officerAccounts;
    private ArrayList<UserInfo> suggestFriends;
    private List<SocialFriendInfo> socialsRequest;
    private boolean isForeUpdateHomeSocials = false;
    private boolean isForeUpdateBeRequest = false;

    //TODO loan het ca rồi, sau khi lên bản gd mới làm lại cơ chế này sau
    public static synchronized HomeContactsHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new HomeContactsHelper(application);
        }
        return mInstance;
    }

    private HomeContactsHelper(ApplicationController application) {
        this.mApplication = application;
    }

    public void handleItemContactsClick(BaseSlidingFragmentActivity activity, Object entry) {
        if (entry == null) return;
        if (entry instanceof ThreadMessage) {
            NavigateActivityHelper.navigateToChatDetail(activity, (ThreadMessage) entry);
        } else if (entry instanceof OfficerAccount) {
            OfficerAccount account = (OfficerAccount) entry;
            if (!TextUtils.isEmpty(account.getServerId())) {
                ThreadMessage threadMessage = mApplication.getMessageBusiness().findExistingOrCreateOfficerThread(account.getServerId(),
                        account.getName(), account.getAvatarUrl(), OfficerAccountConstant.ONMEDIA_TYPE_NONE);
                NavigateActivityHelper.navigateToChatDetail(activity, threadMessage);
            }
        } else if (entry instanceof PhoneNumber) {
            PhoneNumber phone = (PhoneNumber) entry;
            if (phone.getContactId() == null) {
                return;
            }
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
            if (myNumber != null && myNumber.equals(phone.getJidNumber())) {
                activity.showToast(activity.getResources().getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
            } else {
                // an search view khi click vao 1 item
                ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(phone.getJidNumber());
                NavigateActivityHelper.navigateToChatDetail(activity, thread);
            }
        } else if (entry instanceof SocialFriendInfo) {
            SocialFriendInfo social = (SocialFriendInfo) entry;
            PhoneNumber phoneNumber = social.getPhoneNumber();
            if (phoneNumber != null) {
                NavigateActivityHelper.navigateToContactDetail(activity, phoneNumber);
            } else if (social.getUserType() == UserInfo.USER_ONMEDIA_NORMAL) {
                String jidNumber = social.getUserFriendJid();
                StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness()
                        .getExistStrangerPhoneNumberFromNumber(jidNumber);
                NavigateActivityHelper.navigateToStrangerDetail(activity, strangerPhoneNumber, jidNumber,
                        social.getUserName(), social.getUserAvatar(),
                        social.getUserStatus(), -1);
            } else {
                NavigateActivityHelper.navigateToOMOfficialDetail(activity, social.getUserFriendJid(), social.getUserName(), social.getUserAvatar());
            }
        } else if (entry instanceof UserInfo) {
            UserInfo userInfo = (UserInfo) entry;
            /*ThreadMessage threadMessage = mApplication.getStrangerBusiness().
                    createMochaStrangerAndThread(userInfo.getMsisdn(), userInfo.getName(), userInfo.getAvatar(),
                            Constants
                                    .CONTACT.STRANGER_MOCHA_ID, true);
            NavigateActivityHelper.navigateToChatDetail(activity, threadMessage);*/

            EventOnMediaHelper.getInstance(activity).processUserClick(userInfo);
        }
    }

    public void handleAvatarContactsClick(BaseSlidingFragmentActivity activity, Object entry) {
        if (entry instanceof PhoneNumber) {
            PhoneNumber mPhone = (PhoneNumber) entry;
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (myNumber != null && myNumber.equals(mPhone.getJidNumber())) {
                NavigateActivityHelper.navigateToMyProfile(activity);
            } else {
                NavigateActivityHelper.navigateToContactDetail(activity, mPhone);
            }
        }
    }

    public void handleSectionContactsClick(BaseSlidingFragmentActivity activity, Object entry) {
        if (entry instanceof PhoneNumber) {
            PhoneNumber section = (PhoneNumber) entry;
            if (section.getSectionType() == NumberConstant.SECTION_TYPE_OFFICIAL) {
                NavigateActivityHelper.navigateToContactList(activity, Constants.CONTACT.FRAG_LIST_UTIL);
            } else if (section.getSectionType() == NumberConstant.SECTION_TYPE_SOCIAL_REQUEST) {
                NavigateActivityHelper.navigateToContactList(activity, Constants.CONTACT.FRAG_LIST_SOCIAL_REQUEST);
            } else if (section.getSectionType() == NumberConstant.SECTION_TYPE_NEW_FRIENDS) {
                NavigateActivityHelper.navigateToContactList(activity, Constants.CONTACT.FRAG_LIST_FRIEND_MOCHA);
            } else if (section.getSectionType() == NumberConstant.SECTION_TYPE_CONTACT_HISTORY) {
                NavigateActivityHelper.navigateToCallHistory(activity);
            } else if (section.getSectionType() == NumberConstant.SECTION_TYPE_CONTACT ||
                    activity.getResources().getString(R.string.menu_contacts).equals(section.getName())) {
                if (NetworkHelper.isConnectInternet(activity)) {
                    synContactToServer(activity);
                } else {
                    activity.showToast(activity.getResources().getString(R.string.no_connectivity_not_feature), Toast.LENGTH_LONG);
                }
            }
        }
    }

    // gui toan bo contact len lan dau dang nhap
    private void synContactToServer(final BaseSlidingFragmentActivity activity) {
        Log.d(TAG, "init info ");
        final ApplicationController mApp = (ApplicationController) activity.getApplication();
      //  activity.showLoadingDialog(activity.getResources().getString(R.string.setting_sync_contact_content), R.string.waiting);
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (!mApp.getContactBusiness().isContactReady()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "InterruptedException", e);
                    }
                }
                // dang nhap bang code hoac bang pass xong thi init lai list contact, (luc nay moi biet so dang nhap
                // dung viettel hay ko)
                mApp.getContactBusiness().initArrayListPhoneNumber();
                // gui contact len sv
                setInfoAllPhoneNumber(activity);
            }
        };
        thread.start();
    }

    public void setInfoAllPhoneNumber(final BaseSlidingFragmentActivity activity) {
        ContactRequestHelper.getInstance(mApplication).setInfoAllPhoneNumber(new ContactRequestHelper
                .onResponseSetContactListener() {
            @Override
            public void onResponse(ArrayList<PhoneNumber> responses) {
                activity.hideLoadingDialog();
                activity.showToast(activity.getResources().getString(R.string.msg_sync_contact_success), Toast.LENGTH_LONG);
                ((ApplicationController) activity.getApplication()).getContactBusiness().updateContactInfo(responses);
                ((ApplicationController) activity.getApplication()).getContactBusiness().syncContact();
                /*ArrayList<PhoneNumber> listPhone = ((ApplicationController) activity.getApplication()).getContactBusiness().getListNumberAlls();
                if (listPhone != null && !listPhone.isEmpty()) {
                    Log.i(TAG, "update list phonumber: " + listPhone.size());
                    ContactSyncHelper contactSyncHelper = new ContactSyncHelper(mApplication);
                    contactSyncHelper.setListPhone(listPhone);
                    contactSyncHelper.start();
                }*/
            }

            @Override
            public void onError(int errorCode) {
                activity.hideLoadingDialog();
                activity.showToast(activity.getResources().getString(R.string.msg_sync_contact_fail), Toast.LENGTH_LONG);
            }
        });
    }

    public void handleCallContactsClick(BaseSlidingFragmentActivity activity, Object entry) {
        if (entry instanceof PhoneNumber) {
            mApplication.getCallBusiness().checkAndStartCall(activity, ((PhoneNumber) entry).getJidNumber());
        }
    }

    public ArrayList<ThreadMessage> threadSearchList(String contentSearch, ArrayList<ThreadMessage> listThreads) {
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());
        if (contentSearch == null || contentSearch.length() <= 0) {
            if (listThreads != null) {
                return listThreads;
            } else {
                return new ArrayList<>();
            }
        } else {
            ArrayList<ThreadMessage> listSearchThreads;
            String[] listTexts = contentSearch.split("\\s+");
            if (listThreads != null) {
                ArrayList<ThreadMessage> list;
                listSearchThreads = new ArrayList<>(listThreads);
                for (String item : listTexts) {
                    if (item.trim().length() > 0) {
                        list = new ArrayList<>();
                        for (ThreadMessage thread : listSearchThreads) {
                            String name = TextHelper.getInstant().
                                    convertUnicodeToAscci(mApplication.getMessageBusiness().getThreadName(thread));
                            String numberString = thread.getNumberSearchGroup();
                            // thread lam quen thi ko search so
                            if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT && thread.isStranger()) {
                                if (name != null && name.contains(item)) {
                                    list.add(thread);
                                }
                            } else {
                                if (name != null && name.contains(item) || numberString.contains(item)) {
                                    list.add(thread);
                                }
                            }
                        }
                        listSearchThreads = list;
                    }
                }
            } else {
                listSearchThreads = new ArrayList<>();
            }
            return listSearchThreads;
        }
    }

    public ArrayList<PhoneNumber> contactSearchList(String contentSearch, ArrayList<PhoneNumber> listContacts) {
        contentSearch = TextHelper.getInstant().convertUnicodeToAscci(contentSearch.trim());
        if (contentSearch == null || contentSearch.length() <= 0) {
            return listContacts;
        }
        String[] listTexts = contentSearch.split("\\s+");
        ArrayList<PhoneNumber> list;
        ArrayList<PhoneNumber> listSearch = new ArrayList<>(listContacts);
        for (String item : listTexts) {
            if (item.trim().length() > 0) {
                list = new ArrayList<>();
                for (PhoneNumber phone : listSearch) {
                    if (phone.getContactId() != null) {
                        String name = phone.getNameUnicode();
                        String numberJid = phone.getJidNumber();
                        String rawNumber;
                        if (numberJid.startsWith("0")) {// so vn
                            rawNumber = "+84" + numberJid.substring(1, numberJid.length());
                        } else {
                            rawNumber = phone.getRawNumber();
                        }
                        if (name != null && name.contains(item) || numberJid.contains(item) ||
                                (rawNumber != null && rawNumber.contains(item))) {
                            list.add(phone);
                        }
                    }
                }
                listSearch = list;
            }
        }
        if (!listSearch.isEmpty()) {
            ArrayList<PhoneNumber> others = new ArrayList<>();
            list = new ArrayList<>();
            for (PhoneNumber object : listSearch) {
                String friendName = object.getNameUnicode();
                if (friendName != null && friendName.contains(contentSearch)) {
                    list.add(object);
                } else {
                    others.add(object);
                }
            }
            listSearch = list;
            listSearch.addAll(others);
        }
        return listSearch;
    }

    public void getOfficialAccounts(final GetSectionOfficialListener listener) {
        if (officerAccounts != null && !officerAccounts.isEmpty()) {// neu con cache thi tra luon ko goi api nua
            listener.onResponse(officerAccounts);
        } else {
            RoomChatRequestHelper.getInstance(mApplication).getOfficerAccountFromSv(new OfficerBusiness.RequestOfficerResponseListener() {
                @Override
                public void onResponse(ArrayList<OfficerAccount> listRoom) {
                    int size = listRoom.size();
                    if (size > 0) {
//                        int subIndex = size > 5 ? 5 : size;
//                        officerAccounts = listRoom.subList(0, subIndex);
                        officerAccounts = listRoom;
                        if (listener != null)
                            listener.onResponse(officerAccounts);
                    }
                }

                @Override
                public void onError(int errorCode, String msg) {
                    if (listener != null) listener.onError(msg);
                }
            }, true, true);
        }
    }

    public void getSuggestFriend(final GetSocialListener<ArrayList<UserInfo>> listener) {
        if (!mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError();
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SUGGEST);
        //String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.GET_SUGGEST_CONTACT);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        //msisdn + token + timestamp
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        String platform = "Android";
        StringBuilder sb = new StringBuilder();
        sb.append(account.getJidNumber())
                .append(platform)
                .append(Config.REVISION)
                .append(1)
                .append(account.getGender())
                .append(account.getBirthdayString())
                .append(account.getToken())
                .append(timeStamp);

        ResfulString params = new ResfulString(UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_GET_SUGGEST_CONTACT);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam("platform", platform);
        params.addParam("page", 1);
        params.addParam("revision", Config.REVISION);
        params.addParam("gender", account.getGender());
        params.addParam("birthdayStr", account.getBirthdayString());
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken()));
        StringRequest req = new StringRequest(com.android.volley.Request.Method.GET, params.toString(),
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                ArrayList<UserInfo> listUser = new ArrayList<>();
                                String decryptData = HttpHelper.decryptResponse(s, mApplication.getReengAccountBusiness().getToken());
                                if (!TextUtils.isEmpty(decryptData)) {
                                    JSONArray jsonArray = new JSONArray(decryptData);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jso = jsonArray.getJSONObject(i);
                                        UserInfo userInfo = new UserInfo();
                                        userInfo.setAvatar(jso.optString("avatar"));
                                        userInfo.setName(jso.optString("name"));
                                        userInfo.setStateMocha(jso.optInt("isMochaUser"));
                                        userInfo.setUser_type(jso.optInt("user_type"));
                                        userInfo.setMsisdn(jso.optString("msisdn"));
                                        userInfo.setStatus(jso.optString("status"));
                                        listUser.add(userInfo);
                                    }
                                    suggestFriends = listUser;
                                    listener.onResponse(listUser);
                                } else {
                                    listener.onError();
                                }
                            } else {
                                listener.onError();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        listener.onError();
                    }
                });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(req, TAG_SUGGEST, false);
    }

    public List<SocialFriendInfo> getNextSocialRequest() {
        if (socialsRequest != null) {
            int size = socialsRequest.size();
            if (size > 0) {
                int subIndex = size > 3 ? 3 : size;
                return socialsRequest.subList(0, subIndex);
            } else {
                return null;
            }
        }
        return null;
    }

    public void getSocialsRequest(final GetSocialListener<List<SocialFriendInfo>> listener) {
        mApplication.getStrangerBusiness().getSocialFriendRequest(StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED, false, new StrangerBusiness.onSocialFriendsRequestListener() {
            @Override
            public void onResponse(int type, ArrayList<SocialFriendInfo> list) {
                int size = list.size();
                socialsRequest = list;
                if (size > 0) {
                    listener.onResponse(list);
                } else {
                    listener.onEmptyList();
                }
            }

            @Override
            public void onLoadMoreResponse(int type, ArrayList<SocialFriendInfo> list, boolean isNoMore) {

            }

            @Override
            public void onError(int type, int code, boolean isLoadMore) {
                listener.onError();
            }
        });
    }

    public void cancelSearchUserMocha() {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SEARCH_USER);
    }

    public void searchUserMocha(String name, final GetSocialListener<ArrayList<SocialFriendInfo>> listener) {
        if (TextUtils.isEmpty(name) || !mApplication.getReengAccountBusiness().isValidAccount()) {
            listener.onError();
            return;
        }
        cancelSearchUserMocha();
        int page = 1;
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SEARCH_USER_MOCHA);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mApplication.getReengAccountBusiness().getJidNumber())
                .append(name)
                .append(page)
                .append(mApplication.getReengAccountBusiness().getToken())
                .append(currentTime);

        ResfulString params = new ResfulString(url);
        params.addParam(Constants.HTTP.REST_MSISDN, mApplication.getReengAccountBusiness().getJidNumber());
        params.addParam(Constants.HTTP.REST_CONTENT, name);
        params.addParam(Constants.HTTP.REST_PAGE, page);
        params.addParam(Constants.HTTP.TIME_STAMP, currentTime);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApplication,
                sb.toString(), mApplication.getReengAccountBusiness().getToken()));
        Log.i(TAG, "url: " + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Response
                .Listener<String>() {
            @Override
            public void onResponse(String response) {
                String s = HttpHelper.decryptResponse(response, mApplication.getReengAccountBusiness().getToken());
                Log.i(TAG, "response: " + s);
                int errorCode;
                try {
                    JSONObject responseObject = new JSONObject(s);
                    errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onResponse(parserSearchUser(responseObject));
                    } else {
                        listener.onError();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "error: " + volleyError.toString());
                listener.onError();
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SEARCH_USER, false);
    }

    private ArrayList<SocialFriendInfo> parserSearchUser(JSONObject object) throws Exception {
        ArrayList<SocialFriendInfo> socials = new ArrayList<>();
        JSONArray jsonArray = object.optJSONArray("listStrangerSearch");
        if (jsonArray != null && jsonArray.length() > 0) {
            int lengthArray = jsonArray.length();
            for (int i = 0; i < lengthArray; i++) {
                JSONObject objectContact = jsonArray.getJSONObject(i);
                SocialFriendInfo social = new SocialFriendInfo();
                if (objectContact.has("name")) {
                    social.setUserName(objectContact.getString("name"));
                    social.setUserNameUnicode(social.getUserName());
                }
                String jidNumber = objectContact.optString(Constants.HTTP.STRANGER_MUSIC.MSISDN);
                social.setUserFriendJid(jidNumber);
                if (!TextUtils.isEmpty(jidNumber)) {
                    PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(jidNumber);
                    social.setPhoneNumber(phoneNumber);
                }
                social.setUserStatus(objectContact.optString(Constants.HTTP.STRANGER_MUSIC.STATUS));
                social.setUserMocha(1);
                social.setUserType(UserInfo.USER_ONMEDIA_NORMAL);
                social.setUserAvatar(objectContact.optString(Constants.HTTP.STRANGER_MUSIC.LAST_AVATAR, null));
                social.setSocialType(StrangerBusiness.TYPE_SOCIAL_FRIEND);
                socials.add(social);
            }
        }
        return socials;
    }

    public boolean isForeUpdateHomeSocials() {
        return isForeUpdateHomeSocials;
    }

    public void setForeUpdateHomeSocials(boolean foreUpdateHomeSocials) {
        isForeUpdateHomeSocials = foreUpdateHomeSocials;
    }

    public boolean isForeUpdateBeRequest() {
        return isForeUpdateBeRequest;
    }

    public void setForeUpdateBeRequest(boolean foreUpdateBeRequest) {
        isForeUpdateBeRequest = foreUpdateBeRequest;
    }

    public interface GetSectionOfficialListener {
        void onResponse(List<OfficerAccount> oficials);

        void onError(String e);
    }

    public interface GetSocialListener<T> {
        void onResponse(T result);

        void onError();

        void onEmptyList();
    }
}
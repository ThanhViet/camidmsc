package com.metfone.selfcare.helper.httprequest;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.BlockContactBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.database.model.BlockContactModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.onmedia.OfficalAccountOnMedia;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by toanvk2 on 07/11/2015.
 */
public class ContactRequestHelper {
    private static final String TAG_SET_BLOCK = "TAG_SET_BLOCK";
    private static final String TAG_GET_BLOCK = "TAG_GET_BLOCK";
    private static final String TAG_GET_INFO_NUMBER = "TAG_GET_INFO_NUMBER";
    private static final String TAG = ContactRequestHelper.class.getSimpleName();
    private static ContactRequestHelper instance;
    private ContactBusiness mContactBusiness;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private SharedPreferences mPref;

    public static final String BLOCK_NON = "0";
    public static final String BLOCK_ALERT_STRANGER = "1";

    private ContactRequestHelper(ApplicationController application) {
        this.mApplication = application;
        this.mContactBusiness = mApplication.getContactBusiness();
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        mPref = application.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized ContactRequestHelper getInstance(ApplicationController application) {
        if (instance == null) {
            instance = new ContactRequestHelper(application);
        }
        return instance;
    }

    // parser json sang oj phone
    private ArrayList<PhoneNumber> parserJsonPhoneNumber(JSONObject object) throws JSONException {
        long durationTimeSV = 0;
        if (object.has(Constants.HTTP.CONTACT.TIME_STAMP)) {
            //            durationTimeSV = TimeHelper.getCurrentTime() - object.getLong(Constants.HTTP.CONTACT
            // .TIME_STAMP);
            durationTimeSV = TimeHelper.getSVDurationLastSeen(object.getLong(Constants.HTTP.CONTACT.TIME_STAMP));
        }
        JSONArray jsonArray = object.optJSONArray(Constants.HTTP.CONTACT.DATA);
        if (jsonArray != null && jsonArray.length() > 0) {
            ArrayList<PhoneNumber> listPhone = new ArrayList<>();
            int lengthArray = jsonArray.length();
            for (int i = 0; i < lengthArray; i++) {
                JSONObject objectContact = jsonArray.getJSONObject(i);
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setJsonObject(objectContact, durationTimeSV);
                phoneNumber.setAddRoster(1);
                listPhone.add(phoneNumber);
            }
            return listPhone;
        }
        return null;
    }

    // parser json sang oj phone
    private ArrayList<PhoneNumber> parserJsonPhoneNumberFromSearch(JSONObject object) throws JSONException {
        long durationTimeSV = 0;
        if (object.has(Constants.HTTP.CONTACT.TIME_STAMP)) {
            //            durationTimeSV = TimeHelper.getCurrentTime() - object.getLong(Constants.HTTP.CONTACT
            // .TIME_STAMP);
            durationTimeSV = TimeHelper.getSVDurationLastSeen(object.getLong(Constants.HTTP.CONTACT.TIME_STAMP));
        }
        JSONArray jsonArray = object.optJSONArray("listStrangerSearch");
        if (jsonArray != null && jsonArray.length() > 0) {
            ArrayList<PhoneNumber> listPhone = new ArrayList<>();
            int lengthArray = jsonArray.length();
            for (int i = 0; i < lengthArray; i++) {
                JSONObject objectContact = jsonArray.getJSONObject(i);
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setJsonObjectSearchUser(objectContact, durationTimeSV);
                phoneNumber.setAddRoster(1);
                listPhone.add(phoneNumber);
            }
            return listPhone;
        }
        return new ArrayList<>();
    }

    // conver number to json
    private JSONArray getJsonArrayFromListNumber(ArrayList<String> numbers) {
        if (numbers == null || numbers.isEmpty()) {
            return null;
        }
        JSONArray array = new JSONArray();
        HashSet<String> hashSetNumbers = new HashSet<>(numbers);
        try {
            for (String number : hashSetNumbers) {
                JSONObject obj = new JSONObject();
                obj.put(Constants.HTTP.CONTACT.MSISDN, number);
                array.put(obj);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
        return array;
    }

    /**
     * convert listNumber to json
     *
     * @param listPhones danh sach so dt
     * @param setName    co add ten hay khong
     */
    private JSONArray getJsonArrayFromListPhoneNumber(ArrayList<PhoneNumber> listPhones, boolean setName) {
        JSONArray array = new JSONArray();
        if (listPhones == null || listPhones.isEmpty()) {
            return array;
        }
        // bo nhung phone trung ten va so di
        HashSet<PhoneNumber> hashSetPhones = new HashSet<>(listPhones);
        Log.d(TAG, "request getJsonArrayFromListPhoneNumber size : " + hashSetPhones.size());
        for (PhoneNumber phoneNumber : hashSetPhones) {
            array.put(phoneNumber.getJsonObject(setName));
        }
        return array;
    }

    /**
     * ham lay thong tin profile (lay cover voi album
     *
     * @param number
     * @param listener
     */
    public void getProfileFromNumber(String number, final onResponseProfileInfoListener listener) {
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        //        JSONArray data = getJsonArrayFromListNumber(listNumbers);
        String data = number;
        if (TextUtils.isEmpty(data) || !mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        String encrypt = HttpHelper.encryptDataProfile(mApplication, account.getJidNumber(), account.getToken(),
                currentTime, data);
        String url = String.format(
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_PROFILE_URL),
                HttpHelper.EncoderUrl(account.getJidNumber()), HttpHelper.EncoderUrl(encrypt),
                String.valueOf(currentTime), HttpHelper.EncoderUrl(data));
        Log.d(TAG, "getInfoContactFromNumber url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            String decryptData = HttpHelper.decryptResponse(response, mApplication.getReengAccountBusiness().getToken());
                            if (!TextUtils.isEmpty(decryptData)) {
                                JSONObject responseObject = new JSONObject(decryptData);
                                errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                                if (errorCode == HTTPCode.E200_OK) {
                                    PhoneNumber phoneNumber = null;
                                    JSONArray jsonArray = responseObject.optJSONArray(Constants.HTTP.CONTACT.DATA);
                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        JSONObject object = jsonArray.getJSONObject(0);// lay thang dau tien
                                        phoneNumber = new PhoneNumber();
                                        phoneNumber.setJsonObject(object, 0);

                                        mContactBusiness.updateContactEdit(phoneNumber);
                                    }
                                    if (phoneNumber != null) {
                                        listener.onResponse(phoneNumber);
                                    } else {
                                        listener.onError(-1);
                                    }
                                } else {
                                    listener.onError(errorCode);
                                }
                            } else {
                                listener.onError(-1);
                            }

                        } catch (Exception e) {
                            Log.i(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error:", error);
                listener.onError(-1);
            }
        }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public NonContact getInfoNonContactSynchronous(String friendNumber) {
        ArrayList<String> listNumbers = new ArrayList<>();
        listNumbers.add(friendNumber);
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        JSONArray data = getJsonArrayFromListNumber(listNumbers);
        if (data == null || !mAccountBusiness.isValidAccount()) {
            return null;
        }
        RequestFuture<String> future = RequestFuture.newFuture();
        long currentTime = TimeHelper.getCurrentTime();
        String dataContact = data.toString();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(dataContact).
                append(account.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_CONTACT);
        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("contacts", dataContact);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("timestamp", currentTime);
        params.addParam("security", dataEncrypt);
        Log.d(TAG, "url:" + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), future, future);
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
        try {
            String response = future.get(30, TimeUnit.SECONDS); // this will block (forever)
            String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getCurrentAccount()
                    .getToken());
            int errorCode = -1;
            try {
                JSONObject responseObject = new JSONObject(decryptResponse);
                errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                if (errorCode == HTTPCode.E200_OK) {
                    ArrayList<PhoneNumber> listPhone = parserJsonPhoneNumber(responseObject);
                    if (listPhone != null && !listPhone.isEmpty()) {
                        PhoneNumber phoneNumber = listPhone.get(0);
                        mContactBusiness.insertOrUpdateNonContact(phoneNumber, true);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            Log.d(TAG, "CheckSpamStrangerQueue getInfoContactsFromNumberSynchronous: " + decryptResponse);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            Log.e(TAG, "Exception", e);
        }
        NonContact nonContact = mContactBusiness.getExistNonContact(friendNumber);
        return nonContact;
    }

    /**
     * api lay thong tin contacts
     *
     * @param listNumbers danh sach so can lay info
     * @param listener    callback
     */
    public void getInfoContactsFromNumbers(ArrayList<String> listNumbers, final onResponseInfoContactsListener
            listener) {
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        JSONArray data = getJsonArrayFromListNumber(listNumbers);
        if (data == null || !mAccountBusiness.isValidAccount() || mAccountBusiness.isAnonymousLogin()) {
            listener.onError(-1);
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_INFO_NUMBER);
        long currentTime = TimeHelper.getCurrentTime();
        String dataContact = data.toString();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(dataContact).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(account.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_CONTACT);
        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("contacts", dataContact);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("timestamp", currentTime);
        params.addParam("security", dataEncrypt);
        Log.d(TAG, "url:" + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness
                                .getCurrentAccount().getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<PhoneNumber> listPhone = parserJsonPhoneNumber(responseObject);
                                listener.onResponse(listPhone);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_INFO_NUMBER, false);
    }

    /**
     * set all contact to sv
     *
     * @param listener callBack
     */
    public void setInfoAllPhoneNumber(final onResponseSetContactListener listener) {
        ArrayList<PhoneNumber> listPhones = mContactBusiness.getListNumbers();
        if (!mAccountBusiness.isValidAccount() || listPhones == null || listPhones.isEmpty() || mAccountBusiness.isAnonymousLogin()) {
            if (listener != null)
                listener.onError(-1);
            return;
        }
        final JSONArray data = getJsonArrayFromListPhoneNumber(listPhones, true);
        /*if (data == null) {
            listener.onError(-1);
            return;
        }*/
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SET_CONTACT);
        Log.d(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] url: " + url );
        Log.d(TAG, "setInfoAllPhoneNumber url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness
                                .getCurrentAccount().getToken());
                        Log.d(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] decryptResponse: " + decryptResponse);
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<PhoneNumber> listPhone = parserJsonPhoneNumber(responseObject);
                                if (listener != null)
                                    listener.onResponse(listPhone);
                            } else {
                                if (listener != null)
                                    listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] Exception: " + e);
                            Log.e(TAG, "Exception", e);
                            if (listener != null)
                                listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "[Cambodia Version] - [setInfoAllPhoneNumber] Error: ", error);
                Log.i(TAG, "Error:", error);
                if (listener != null)
                    listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long timeStamp = TimeHelper.getCurrentTime();
                String contactData = data.toString();
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(contactData).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getUsingDesktop()).
                        append(account.getToken()).
                        append(timeStamp);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.CONTACT.DATA, contactData);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(timeStamp));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.DESKTOP, String.valueOf(mApplication.getReengAccountBusiness().getUsingDesktop()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * get all contact from sv
     *
     * @param listener callback get contact
     */
    /*public void getInfoAllPhoneNumber(final onResponseContact listener) {
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        String encrypt = HttpHelper.encryptData(mApplication, account.getJidNumber(), account.getToken(), currentTime);
        String url = String.format(
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_ALL_CONTACT),
                numberEncode, HttpHelper.EncoderUrl(encrypt), String.valueOf(currentTime));
        Log.d(TAG, "url: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<PhoneNumber> listPhone = parserJsonPhoneNumber(responseObject);
                                listener.onResponse(listPhone);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (JSONException e) {
                            Log.i(TAG, "JSONException:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error:", error);
                listener.onError(-1);
            }
        }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }*/

    /**
     * api them so len sv
     *
     * @param listPhone danh sach contact can add
     * @param listener  callback
     */
    public void addInfoListPhoneNumber(final ArrayList<PhoneNumber> listPhone, final onResponseContact listener) {
        final JSONArray data = getJsonArrayFromListPhoneNumber(listPhone, true);
        if (!mAccountBusiness.isValidAccount() || listPhone == null) {
            listener.onError(-1);
            return;
        }
//        Resources res = mApplication.getResources();
        //mApplication.trackingEvent(res.getString(R.string.ga_category_contacts), res.getString(R.string
        // .ga_action_interaction), res.getString(R.string.ga_label_add_contact));
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ADD_CONTACT);
        Log.d(TAG, "url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        if (TextUtils.isEmpty(decryptResponse)) {
                            listener.onError(-1);
                            return;
                        }
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<PhoneNumber> listParser = parserJsonPhoneNumber(responseObject);
                                listener.onResponse(listParser);
                                /*if (listParser != null && !listParser.isEmpty()) {
                                    PhoneNumber entry = listParser.get(0);
                                    String jid = entry.getJidNumber();
                                    ThreadMessage thread = mApplication.getMessageBusiness().findExistingSoloThread
                                    (jid);
                                    if (entry.isReeng() && thread == null) {
                                        mContactBusiness.notifyNewUser(jid, mApplication.getResources().getString(R
                                        .string.msg_fake_new_user), true);
                                    }
                                }*/
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String dataContact = data.toString();
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(dataContact).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getUsingDesktop()).
                        append(account.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.CONTACT.DATA, dataContact);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY,
                        HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken()));
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.DESKTOP, String.valueOf(mApplication.getReengAccountBusiness().getUsingDesktop()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * remove contact
     *
     * @param listNumbers list contact remove
     * @param listener    callback
     */
    public void removeInfoListPhoneNumber(final ArrayList<String> listNumbers, final onResponseContact listener) {
        final JSONArray data = getJsonArrayFromListNumber(listNumbers);
        if (!mAccountBusiness.isValidAccount() || listNumbers == null || data == null) {
            return;
        }
//        Resources res = mApplication.getResources();
        /*mApplication.trackingEvent(res.getString(R.string.ga_category_contacts),
                res.getString(R.string.ga_action_interaction), res.getString(R.string.ga_label_remove_contact));*/
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.REMOVE_CONTACT);
        Log.d(TAG, "url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness
                                .getCurrentAccount().getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onResponse(null);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String dataContact = data.toString();
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(dataContact).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getUsingDesktop()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.CONTACT.DATA, dataContact);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.DESKTOP, String.valueOf(mApplication.getReengAccountBusiness().getUsingDesktop()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void getBlockListV5() {
        if (!mAccountBusiness.isValidAccount() || mAccountBusiness.isAnonymousLogin()) {
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber())
                .append(Constants.HTTP.CLIENT_TYPE_STRING)
                .append(mAccountBusiness.getToken())
                .append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        ResfulString paramsUrl = new ResfulString(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config
                .UrlEnum.GET_BLOCKLIST_V5));
        paramsUrl.addParam(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
        paramsUrl.addParam(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
        paramsUrl.addParam(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
        paramsUrl.addParam(Constants.HTTP.DATA_SECURITY, dataEncrypt);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, paramsUrl.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.d(TAG, "getBlockListV5 onResponse : " + s);
                        String response = HttpHelper.decryptResponse(s, mAccountBusiness.getToken());
                        Log.i(TAG, "onresponse decrypt: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.optInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                mPref.edit().putBoolean(Constants.PREFERENCE.PREF_GET_LIST_BLOCK, true).apply();
                                JSONArray jsonArray = jsonObject.getJSONArray("blockChat");
                                if (jsonArray != null && jsonArray.length() != 0) {
                                    ArrayList<String> listBlock = new ArrayList<String>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        listBlock.add((String) jsonArray.get(i));
                                    }
                                    HashSet<String> hashSetBlocks = new HashSet<>(listBlock);
                                    BlockContactBusiness blockContactBusiness = mApplication.getBlockContactBusiness();
                                    blockContactBusiness.updateListAfterBlockNumbers(
                                            new ArrayList<>(hashSetBlocks), true);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception getListBlockUser", e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Error:", volleyError);
                    }
                }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG, false);
    }

    public void handleBlockNumbers(final ArrayList<BlockContactModel> listNumberBlock,
                                   final onResponseBlockContact listener) {
        if (listNumberBlock == null || listNumberBlock.isEmpty() || !mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SET_BLOCKLIST_V5);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SET_BLOCK);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onresponse " + response);
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Error:", volleyError);
                        listener.onError(-1);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String msisdn = mAccountBusiness.getJidNumber();
                String token = mAccountBusiness.getToken();
                String listBlock = getListBlockInString(listNumberBlock);
                String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
                StringBuilder sb = new StringBuilder();
                sb.append(msisdn)
                        .append(clientType)
                        .append(listBlock)
                        .append(Config.REVISION)
                        .append(mApplication.getReengAccountBusiness().getCurrentLanguage())
                        .append(mApplication.getReengAccountBusiness().getRegionCode())
                        .append(mApplication.getReengAccountBusiness().getUsingDesktop())
                        .append(token)
                        .append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), token);
                //create param
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, msisdn);
                params.put(Constants.HTTP.BLOCK_LIST, listBlock);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.REST_CLIENT_TYPE, clientType);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("languageCode", mApplication.getReengAccountBusiness().getCurrentLanguage());
                params.put("countryCode", mApplication.getReengAccountBusiness().getRegionCode());
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.DESKTOP, String.valueOf(mApplication.getReengAccountBusiness().getUsingDesktop()));
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG_SET_BLOCK, false);
    }

    private String getListBlockInString(ArrayList<BlockContactModel> listBlock) {
        if (listBlock == null) return "null";
        int iMax = listBlock.size() - 1;
        if (iMax == -1)
            return "[]";
        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(listBlock.get(i).toString());
            if (i == iMax)
                return b.append(']').toString();
            b.append(", ");
        }
    }

    /**
     * ham lay thong tin boi doi
     *
     * @param friendJid
     * @param listener
     */
    public void getDivineDetail(String friendJid, String friendBirthday, final onDivineResponse listener) {
        if (TextUtils.isEmpty(friendJid) || !mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        if (friendBirthday == null) friendBirthday = "";
        //listener.onResponse("Độ hợp: 99%", "Xử Nữ ham vui còn Song Ngư lại nhậy cảm. Khó hòa hợp phết nhưng chỉ cần
        // chiều nhau thêm chút nữa là được thôi");
        long currentTime = TimeHelper.getCurrentTime();
        String birthdayStr = mAccountBusiness.getCurrentAccount().getBirthdayString();
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber())
                .append(friendJid)
                .append(birthdayStr)
                .append(friendBirthday)
                .append(mAccountBusiness.getToken())
                .append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        ResfulString paramsUrl = new ResfulString(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config
                .UrlEnum.GET_FORTUNE));
        paramsUrl.addParam(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
        paramsUrl.addParam("msisdnFriend", friendJid);
        paramsUrl.addParam("birthday_msisdn", birthdayStr);
        paramsUrl.addParam("birthday_friend", friendBirthday);
        paramsUrl.addParam(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
        paramsUrl.addParam(Constants.HTTP.DATA_SECURITY, dataEncrypt);

        Log.d(TAG, "getDivineDetail url:" + paramsUrl.toString());
        StringRequest request = new StringRequest(Request.Method.GET, paramsUrl.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                String percent;
                                String content;
                                percent = responseObject.optString("percent");
                                content = responseObject.optString("content");
                                if (!TextUtils.isEmpty(percent) && !TextUtils.isEmpty(content)) {
                                    listener.onResponse(percent, content);
                                } else {
                                    listener.onError(-1);
                                }
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error:", error);
                listener.onError(-1);
            }
        }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void searchUserMocha(String name, int page, final onResponseContact listener) {
        if (TextUtils.isEmpty(name) || !mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SEARCH_USER_MOCHA);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber())
                .append(name)
                .append(page)
                .append(mAccountBusiness.getToken())
                .append(currentTime);

        ResfulString params = new ResfulString(url);
        params.addParam(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
        params.addParam(Constants.HTTP.REST_CONTENT, name);
        params.addParam(Constants.HTTP.REST_PAGE, page);
        params.addParam(Constants.HTTP.TIME_STAMP, currentTime);
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApplication,
                sb.toString(), mAccountBusiness.getToken()));
        Log.i(TAG, "url: " + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Response
                .Listener<String>() {
            @Override
            public void onResponse(String response) {
                String s = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                Log.i(TAG, "response: " + s);
                int errorCode = -1;
                try {
                    JSONObject responseObject = new JSONObject(s);
                    errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                    if (errorCode == HTTPCode.E200_OK) {
                        ArrayList<PhoneNumber> listParser = parserJsonPhoneNumberFromSearch(responseObject);
                        listener.onResponse(listParser);
                    } else {
                        listener.onError(errorCode);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onError(-1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "error: " + volleyError.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * @param friendJid
     * @param listener
     */
    public void getSocialDetail(final String friendJid, final onFollowResponse listener) {
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber())
                .append(friendJid)
                .append(mAccountBusiness.getToken())
                .append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        //String url = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_GET_DETAIL),
        String url = String.format(UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_GET_DETAIL,
                HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()),
                HttpHelper.EncoderUrl(friendJid),
                currentTime,
                dataEncrypt);

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "getSocialDetail onResponse : " + response);
                        int errorCode, status;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                JSONObject data = responseObject.optJSONObject("data");
                                if (data != null) {
                                    status = data.optInt("friendStatus", Constants.CONTACT.FOLLOW_STATE_UNKNOW);
                                    String rowId = data.optString("rowIdRequest", null);
                                    listener.onResponse(status, rowId);
                                } else {
                                    listener.onError(-1);
                                }
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        listener.onError(-1);
                    }
                }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * request social friend (add, remove)
     *
     * @param friendJid
     * @param friendName
     * @param source
     * @param listener
     */
    public void requestSocialFriend(final String friendJid, final String friendName,
                                    final int source, final int currentState,
                                    final String rowId, final onFollowResponse listener) {
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        String url = getUrlRequestSocial(currentState);
        if (TextUtils.isEmpty(url)) {
            listener.onError(-1);
            return;
        }
        Log.d(TAG, "url: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "requestSocialFriend : " + response);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                int status = Constants.CONTACT.FOLLOW_STATE_UNKNOW;
                                String rowId = null;
                                JSONObject data = responseObject.optJSONObject("data");
                                if (data != null) {
                                    status = data.optInt("friendStatus", Constants.CONTACT.FOLLOW_STATE_UNKNOW);
                                    rowId = data.optString("rowIdRequest", null);
                                }
                                listener.onResponse(status, rowId);
                                if (currentState == Constants.CONTACT.FOLLOW_STATE_NONE) { //request add friend
                                    LuckyWheelHelper.getInstance(mApplication).doMission(Constants.LUCKY_WHEEL
                                            .ITEM_MAKE_FRIEND);
                                }
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.i(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        listener.onError(-1);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder();
                sb.append(mAccountBusiness.getJidNumber())
                        .append(friendJid);
                if (currentState == Constants.CONTACT.FOLLOW_STATE_NONE) {
                    sb.append(mAccountBusiness.getUserName());
                    sb.append(friendName);
                    sb.append(source);
                }
                sb.append(mAccountBusiness.getToken())
                        .append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                //create param
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(Constants.HTTP.REST_OTHER_MSISDN, friendJid);
                if (currentState == Constants.CONTACT.FOLLOW_STATE_NONE) {
                    params.put("requestorName", mAccountBusiness.getUserName());
                    params.put("requestedName", friendName);
                    params.put("source", String.valueOf(source));
                }
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                if (!TextUtils.isEmpty(rowId) && !"_".equals(rowId)) {
                    params.put("rowIdRequest", rowId);
                }
                // bổ xung user info (không cần mã hóa) cho api requestFriend  và acceptRequest
                if (currentState == Constants.CONTACT.FOLLOW_STATE_NONE ||
                        currentState == Constants.CONTACT.FOLLOW_STATE_BE_FOLLOWED) {
                    params.put("userInfo", getMyUserInfo());
                }
                Log.d(TAG, "requestSocialFriend params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG, false);
    }

    private String getMyUserInfo() {
        try {
            JSONObject info = new JSONObject();
            info.put("client_type", "Android");
            info.put("revision", Config.REVISION);
            info.put("language", mApplication.getReengAccountBusiness().getDeviceLanguage());
            info.put("name", mApplication.getReengAccountBusiness().getUserName());
            return info.toString();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return "";
        }
    }

    private String getUrlRequestSocial(int currentState) {
        String url = "";
        if (currentState == Constants.CONTACT.FOLLOW_STATE_NONE) {
            //url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_SEND_REQUEST);
            url = UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_SEND_REQUEST;
        } else if (currentState == Constants.CONTACT.FOLLOW_STATE_FOLLOWED) {
            //url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_CANCEL_MY_REQUEST);
            url = UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_CANCEL_MY_REQUEST;
        } else if (currentState == Constants.CONTACT.FOLLOW_STATE_BE_FOLLOWED) {
            //url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_ACCEPT_REQUEST);
            url = UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_ACCEPT_REQUEST;
        } else if (currentState == Constants.CONTACT.FOLLOW_STATE_FRIEND) {
            //url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfOnMedia(Config.UrlEnum.SOCIAL_CANCEL_FRIEND);
            url = UrlConfigHelper.getInstance(mApplication).getDomainOnMedia() + ConstantApi.Url.OnMedia.API_SOCIAL_CANCEL_FRIEND;
        }
        return url;
    }

    public interface onResponseInfoContactsListener {
        void onResponse(ArrayList<PhoneNumber> responses);

        void onError(int errorCode);
    }

    public interface onResponseSetContactListener {
        void onResponse(ArrayList<PhoneNumber> responses);

        void onError(int errorCode);
    }

    public interface onResponseContact {
        void onResponse(ArrayList<PhoneNumber> responses);

        void onError(int errorCode);
    }

    public interface onResponseBlockContact {
        void onResponse(String response);

        void onError(int errorCode);
    }

    public interface onResponseProfileInfoListener {
        void onResponse(PhoneNumber phoneNumber);

        void onError(int errorCode);
    }

    public interface onResponse {
        void onResponse();
    }

    public interface onFollowResponse {
        void onResponse(int status, String rowId);

        void onError(int errorCode);
    }

    public interface onDivineResponse {
        void onResponse(String percent, String content);

        void onError(int errorCode);
    }

    public interface onResponseOfficialOnMediaInfoListener {
        void onResponse(OfficalAccountOnMedia officalAccountOnMedia);

        void onError(int errorCode);
    }
}
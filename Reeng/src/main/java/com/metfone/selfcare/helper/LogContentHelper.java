package com.metfone.selfcare.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.datasource.LogDataSource;
import com.metfone.selfcare.database.model.LogKQI;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/17/2017.
 */

public class LogContentHelper {

    private static final String TAG = LogContentHelper.class.getSimpleName();

    private ApplicationController mApplication;
    private SharedPreferences mPref;

    public LogContentHelper(ApplicationController application) {
        this.mApplication = application;
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    public void insertLogContent(LogKQI content) {
        int networkType = 0;
//        String networkSubType = "";
        int networkSubType=0;
        ConnectivityManager cm = (ConnectivityManager) mApplication.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                networkType = 2;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                networkType = 1;
                networkSubType = activeNetwork.getSubtype();
            }
        }
        StringBuilder info = new StringBuilder();
        info.append(System.currentTimeMillis());
        info.append("|").append(mApplication.getReengAccountBusiness().getJidNumber());
        info.append("|").append("ANDROID");
        info.append("|").append(Config.REVISION);
        info.append("|").append(networkType);
        info.append("|").append(networkSubType);
        info.append("|").append(NetworkHelper.getIPAddress(true));
        String longitude = "0";
        String latitude = "0";
        if (LocationHelper.getInstant(mApplication).isExistCacheLocation()) {
            longitude = String.valueOf(LocationHelper.getInstant(mApplication).
                    getCurrentLocation().getLongitude());
            latitude = String.valueOf(LocationHelper.getInstant(mApplication).
                    getCurrentLocation().getLatitude());
        }
        info.append("|").append(longitude);
        info.append("|").append(latitude);
        info.append("|").append(SignalStrengthHelper.getInstant(mApplication).getSignalStrength());

        content.setInfo(info + "|" + content.getInfo());
        Log.i(TAG, "log content: " + content.getInfo());

        LogDataSource.getInstance(mApplication).insertLog(content);
    }

    public void insertListLogContent(ArrayList<LogKQI> listContent) {
        LogDataSource.getInstance(mApplication).insertListLog(listContent);
    }

    public void dropTableLogContent() {
        LogDataSource.getInstance(mApplication).deleteTable();
    }

    public void uploadLogContent() {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }

        final ReengAccount myAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!mApplication.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }

        //Goi api day log 1 ngay 1 lan

//        ArrayList<LogKQI> listContent = LogDataSource.getInstance(mApplication).getAllLog();
//        if (listContent == null || listContent.isEmpty()) {
//            Log.i(TAG, "no message yesterday");
//            mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOG, System.currentTimeMillis
//                    ()).apply();
//            return;
//        }
//        String url = UrlConfigHelper.getInstance(mApplication).
//                getUrlConfigOfFile(Config.UrlEnum.UPLOAD_LOG);
//        Log.i(TAG, "url: " + url);
//        final StringBuilder data = new StringBuilder();
//        for (String s : listContent) {
//            data.append(s);
//            data.append("\n");
//        }
//        final String timeStamp = String.valueOf(System.currentTimeMillis());
//        StringBuilder sb = new StringBuilder();
//        sb.append(myAccount.getJidNumber()).append(data.toString()).append(myAccount.getToken()).append(timeStamp);
//
//        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), myAccount.getToken());
//        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, url, new Response
//                .Listener<String>() {
//
//
//            @Override
//            public void onResponse(String s) {
//                Log.i(TAG, "onResponse: " + s);
//                try {
//                    JSONObject jsonObject = new JSONObject(s);
//                    int code = jsonObject.optInt("code");
//                    if (code == 200) {
//                        mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOG, System.currentTimeMillis
//                                ()).apply();
//                        dropTableLogContent();
//                    }
//                } catch (JSONException e) {
//                    Log.e(TAG, "JSONException", e);
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.e(TAG, "onErrorResponse: " + volleyError.toString());
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<>();
//                params.put("msisdn", myAccount.getJidNumber());
//                params.put("data", data.toString());
//                params.put("timestamp", timeStamp);
//                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
//                Log.i(TAG, "params: " + params.toString());
//                return params;
//            }
//        };
//        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }


}

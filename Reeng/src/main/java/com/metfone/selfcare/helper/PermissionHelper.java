package com.metfone.selfcare.helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;

import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.PermissionSettingIntroActivity;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.ui.dialog.PermissionDialog;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by toanvk2 on 07/12/2015.
 */
public class PermissionHelper {
    private static RequestPermissionsResult mCallBack;

    public static boolean hasPermissionBluetooth(Context context) {
        return context.checkPermission(android.Manifest.permission.BLUETOOTH,
                Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean allowedPermission(Context context, String permission) {
        if (Version.hasM()) {
            int permissionCheck = ContextCompat.checkSelfPermission(context, permission);
            return permissionCheck == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public static boolean declinedPermission(Context context, String permission) {
        if (Version.hasM()) {
            int permissionCheck = ContextCompat.checkSelfPermission(context, permission);
            return permissionCheck != PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    public static void requestPermission(BaseSlidingFragmentActivity activity, String permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
        // app-defined int constant. The callback method gets the
        // result of the request.
    }

    public static void requestPermissions(BaseSlidingFragmentActivity activity, String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
        // app-defined int constant. The callback method gets the
        // result of the request.
    }

    public static boolean verifyPermissions(int[] grantResults) {
        if (grantResults.length < 1) {
            return false;
        }
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean isSystemAlertGranted(Context context) {
        return Settings.canDrawOverlays(context);
    }

    public ArrayList<String> declinedPermissions(Context context, String[] permissions) {
        ArrayList<String> permissionsNeededs = new ArrayList<>();
        for (String permission : permissions) {
            if (declinedPermission(context, permission)) {
                permissionsNeededs.add(permission);
            }
        }
        return permissionsNeededs;
    }

    public static void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (mCallBack != null) {
            mCallBack.onPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public static synchronized void setCallBack(RequestPermissionsResult callBack) {
        mCallBack = callBack;
    }

    public static synchronized void removeCallBack(RequestPermissionsResult callBack) {
        if (mCallBack == callBack) {
            mCallBack = null;
        }
    }

    public interface RequestPermissionsResult {
        void onPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
    }

    public static int checkPermissionStatus(Activity context, String permission) {
        int permissionCheck = ActivityCompat.checkSelfPermission(context, permission);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            return PERMISSION_GRANTED;
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
            return PERMISSION_DENIED_WITHOUT_ASK_NEVER_AGAIN;
        }
        return PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY;
    }

    public static final int PERMISSION_GRANTED = 1;
    public static final int PERMISSION_DENIED_WITHOUT_ASK_NEVER_AGAIN = 2;
    public static final int PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY = 3;

    public static void showDialogPermissionSettingIntro(final Activity activity, final ArrayList<String> array, final int permissionCode) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity != null) {
                    Intent intent = new Intent(activity, PermissionSettingIntroActivity.class);
                    intent.putExtra("permissionCode", permissionCode);
                    intent.putExtra("permissionList", array);
                    activity.startActivity(intent);
                }
            }
        }, 500);
    }

    public static void requestPermissionWithGuide(final BaseSlidingFragmentActivity activity, final String permission, final int requestCode) {
        if (activity == null || TextUtils.isEmpty(permission)) {
            return;
        }
        if (!Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE) {
            requestPermission(activity, permission, requestCode);
            return;
        }
        try {
            final SharedPreferences preferences = activity.getSharedPreferences("com.viettel.reeng.app", Context.MODE_PRIVATE);

            final ArrayList<String> permissions = new ArrayList<String>();
            permissions.add(permission);
            if (permission.equals(Manifest.permission.WRITE_CONTACTS) && declinedPermission(activity, Manifest.permission.READ_CONTACTS)) {
                permissions.add(Manifest.permission.READ_CONTACTS);
            }

            if (permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION) && declinedPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (permission.equals(Manifest.permission.READ_CONTACTS) && declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
                permissions.add(Manifest.permission.WRITE_CONTACTS);
            }

            if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION) && declinedPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            final String[] array_permissions = permissions.toArray(new String[permissions.size()]);
            if (checkPermissionStatus(activity, permission) == PERMISSION_DENIED_WITHOUT_ASK_NEVER_AGAIN) {
                ActivityCompat.requestPermissions(activity, array_permissions, requestCode);
                preferences.edit().putBoolean("FirstRequest_" + permission, false).apply();
            } else {
                PermissionDialog.newInstance(requestCode, true, permissions, new PermissionDialog.CallBack() {
                    @Override
                    public void onPermissionAllowClick(boolean allow, int clickCount) {
                        if (allow) {
                            boolean isFirstRequest = preferences.getBoolean("FirstRequest_" + permission, true);
                            if (permission.contains(Manifest.permission.WRITE_CONTACTS) || permission.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE) || permission.contains(Manifest.permission.RECEIVE_SMS) || permission.contains(Manifest.permission.READ_PHONE_STATE)) {
                                isFirstRequest = false;
                            }
                            if (checkPermissionStatus(activity, permission) == PERMISSION_DENIED_WITHOUT_ASK_NEVER_AGAIN || isFirstRequest) {
                                ActivityCompat.requestPermissions(activity, array_permissions, requestCode);
                                preferences.edit().putBoolean("FirstRequest_" + permission, false).apply();
                            } else {
                                activity.dismissDialogFragment(PermissionDialog.TAG);
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                                intent.setData(uri);
                                activity.startActivity(intent);
                                PermissionHelper.showDialogPermissionSettingIntro(activity, permissions, requestCode);
                            }
                        } else {
                            ActivityCompat.requestPermissions(activity, array_permissions, requestCode);
                        }
                    }
                }).show(activity.getSupportFragmentManager(), PermissionDialog.TAG);
            }
        } catch (Exception e) {
            Log.e(PermissionHelper.class.getSimpleName(), "Exception !!", e);
        }
        // app-defined int constant. The callback method gets the
        // result of the request.
    }

    public static void requestRxPermissions(BaseSlidingFragmentActivity activity, String[] permissions, final ResultPermissionCallback callback) {
        RxPermissions rxPermissions = new RxPermissions(activity);
        rxPermissions.setLogging(BuildConfig.DEBUG);
        rxPermissions.requestEachCombined(permissions).subscribe(new Consumer<Permission>() {
            @Override
            public void accept(Permission permission) throws Exception {
                if (callback != null) {
                    if (permission.granted) {
                        callback.onPermissionGranted(permission);
                    } else if (permission.shouldShowRequestPermissionRationale) {
                        callback.onShouldShowRequestPermissionRationale(permission);
                    } else {
                        callback.onPermissionPermanentlyDenied(permission);
                    }
                }
            }
        });
    }

    public interface ResultPermissionCallback {
        void onPermissionGranted(Permission permission);

        void onPermissionPermanentlyDenied(Permission permission);

        void onShouldShowRequestPermissionRationale(Permission permission);
    }

    public static boolean verifyPermissions(Context context, List<String> permissions) {
        if (permissions == null || permissions.size() < 1) {
            return false;
        }
        for (String permission : permissions) {
            if (declinedPermission(context, permission)) {
                return false;
            }
        }
        return true;
    }

    public static boolean allowedOrRequestPermissionMoviesDRM(BaseSlidingFragmentActivity activity) {
        List<String> array = new ArrayList<>();
        if (declinedPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (declinedPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            array.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (declinedPermission(activity, Manifest.permission.READ_PHONE_STATE)) {
            array.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (array.isEmpty()) {
            return true;
        } else {
            String[] permissions = new String[array.size()];
            permissions = array.toArray(permissions);
            requestPermissions(activity, permissions, Constants.PERMISSION.PERMISSION_REQUEST_MOVIE_DRM);
            return false;
        }
    }

    public static boolean allowedPermissionMoviesDRM(Context context) {
        if (declinedPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            return false;
        }
        if (declinedPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            return false;
        }
        if (declinedPermission(context, Manifest.permission.READ_PHONE_STATE)) {
            return false;
        }
        return true;
    }

    // return true if has all permission is allow
    public static boolean checkPermissionAndShowDialogPermission(final BaseSlidingFragmentActivity activity,
                                                                 final int requestCode,
                                                                 final String[] permissions) {
        return checkPermissionAndShowDialogPermission(activity, requestCode, permissions, null);
    }

    // Note: if never request permission, shouldShowRequestPermissionRationale will return false.
    public static boolean checkPermissionAndShowDialogPermission(final BaseSlidingFragmentActivity activity,
                                                                 final int requestCode,
                                                                 final String[] permissions, final ListenerGotoSetting listenerGotoSetting) {
        boolean goToSettings = false;
        final ArrayList<String> array = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            int permissionStatus = checkPermissionStatus(activity,
                    permissions[i]);
            if (permissionStatus != PERMISSION_GRANTED) {
                array.add(permissions[i]);
                if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[i])) { // user clicked don't show again
                    goToSettings = true;
                }
            }
        }
        if (array.size() > 0) {
            if (!Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE || !goToSettings) {
                PermissionHelper.requestPermissions(activity, permissions, requestCode);
            } else {
                PermissionDialog.newInstance(requestCode, true, array, new PermissionDialog.CallBack() {
                    @Override
                    public void onPermissionAllowClick(boolean allow, int clickCount) {
                        activity.dismissDialogFragment(PermissionDialog.TAG);
                        if (allow) {
                            //go to settings
                            if (listenerGotoSetting != null) {
                                listenerGotoSetting.gotoSetting();
                            }
                            gotoActivitySetting(activity);
                            showDialogPermissionSettingIntro(activity,
                                    array, requestCode);
                        } else {
                            PermissionHelper.requestPermissions(activity,
                                    permissions, requestCode);
                        }
                    }

                }).show(activity.getSupportFragmentManager(), PermissionDialog.TAG);
            }
            return false;
        } else {
            return true;
        }
    }

    // return true if has all permission is allow
    public static boolean fragmentCheckPermissionAndShowDialogPermission(final Fragment fragment
            , final int requestCode, final String[] permissions) {
        if (fragment != null) {
            FragmentActivity activity = fragment.getActivity();
            if (activity != null) {
                boolean goToSettings = false;
                final ArrayList<String> array = new ArrayList<>();
                for (int i = 0; i < permissions.length; i++) {
                    if (ContextCompat.checkSelfPermission(activity, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                        array.add(permissions[i]);
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permissions[i])) { // user click don't show again
                            goToSettings = true;
                        }
                    }
                }
                if (array.size() > 0) {
                    if (!Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE || !goToSettings) {
                        fragment.requestPermissions(permissions, requestCode);
                    } else {
                        FragmentManager fragmentManager = fragment.getFragmentManager();
                        if (fragmentManager != null) {
                            PermissionDialog.newInstance(requestCode, true, array, new PermissionDialog.CallBack() {
                                @Override
                                public void onPermissionAllowClick(boolean allow, int clickCount) {
                                    if (fragment != null && fragment.isAdded()) {
                                        PermissionDialog.dismissDialogPermission(fragment.getFragmentManager());
                                        if (allow) {
                                            //go to settings
                                            gotoActivitySetting(fragment.getActivity());
                                            showDialogPermissionSettingIntro(fragment.getActivity(), array, requestCode);
                                        } else {
                                            fragment.requestPermissions(permissions, requestCode);
                                        }
                                    }
                                }
                            }).show(fragmentManager, PermissionDialog.TAG);
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private static void gotoActivitySetting(Activity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivity(intent);
    }

    public static boolean checkPermissionContact(BaseSlidingFragmentActivity activity) {
        return checkPermissionAndShowDialogPermission(activity,
                Constants.PERMISSION.PERMISSION_CONTACT,
                new String[]{Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.READ_CONTACTS});
    }

    public static boolean checkPermissionContact(Fragment fragment) {
        return fragmentCheckPermissionAndShowDialogPermission(fragment,
                Constants.PERMISSION.PERMISSION_CONTACT,
                new String[]{Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.READ_CONTACTS});
    }

    public static boolean onlyCheckPermissionContact(Context context) {
        int readContact = ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
        int writeContact = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CONTACTS);
        if (readContact == PackageManager.PERMISSION_GRANTED && writeContact == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public static boolean checkPermissionStorage(BaseSlidingFragmentActivity activity) {
        return checkPermissionAndShowDialogPermission(activity,
                Constants.PERMISSION.PERMISSION_REQUEST_FILE,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE});
    }

    public static boolean onlyCheckPermissionStorage(Context context) {
        int read = ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int write = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (read == PackageManager.PERMISSION_GRANTED && write == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public interface ListenerGotoSetting {
        void gotoSetting();
    }
}
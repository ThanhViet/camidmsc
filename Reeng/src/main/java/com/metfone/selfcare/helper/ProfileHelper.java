package com.metfone.selfcare.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.SquareImageView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

//import jp.wasabeef.blurry.Blurry;

/**
 * Created by thanhnt72 on 9/15/2017.
 */
public class ProfileHelper {

    private static final String TAG = ProfileHelper.class.getSimpleName();


    public static void drawAlbum(ApplicationController mApp, ArrayList<ImageProfile> mImageProfiles,
                                 View vPic1, View vPic2, View vPic3, boolean profileFriend) {
        ImageProfileBusiness mImageProfileBusiness = mApp.getImageProfileBusiness();
        if (!mImageProfiles.isEmpty()) {
            if (mImageProfiles.size() >= 1) {
                vPic1.setVisibility(View.VISIBLE);
                RoundedImageView iv = vPic1.findViewById(R.id.ivPic1);
                if (profileFriend) {
                    mImageProfileBusiness.displayImageProfileFriend(iv,
                            mImageProfiles.get(mImageProfileBusiness.getImageIndex(mImageProfiles.size()
                                    , 0)), false);
                } else {
                    mImageProfileBusiness.displayImageProfile(iv, mImageProfiles.get(0), true);
                }
            } else {
                vPic1.setVisibility(View.INVISIBLE);
            }

            if (mImageProfiles.size() >= 2) {
                vPic2.setVisibility(View.VISIBLE);
                RoundedImageView iv = vPic2.findViewById(R.id.ivPic2);
                if (profileFriend) {
                    mImageProfileBusiness.displayImageProfileFriend(iv,
                            mImageProfiles.get(mImageProfileBusiness.getImageIndex(mImageProfiles.size()
                                    , 1)), false);
                } else {
                    mImageProfileBusiness.displayImageProfile(iv, mImageProfiles.get(1), true);
                }
            } else {
                vPic2.setVisibility(View.INVISIBLE);
            }

            if (mImageProfiles.size() >= 3) {
                vPic3.setVisibility(View.VISIBLE);
                RoundedImageView iv = vPic3.findViewById(R.id.ivPic3);
                if (profileFriend) {
                    mImageProfileBusiness.displayImageProfileFriend(iv,
                            mImageProfiles.get(mImageProfileBusiness.getImageIndex(mImageProfiles.size()
                                    , 2)), false);
                } else {
                    mImageProfileBusiness.displayImageProfile(iv, mImageProfiles.get(2), true);
                }

                View mask = vPic3.findViewById(R.id.rlMaskMultiPic);
                if (mImageProfiles.size() > 3) {
                    mask.setVisibility(View.VISIBLE);
                    AppCompatTextView tvCount = vPic3.findViewById(R.id.tvMultiPic);
                    tvCount.setText("+" + String.valueOf(mImageProfiles.size() - 3));
                } else {
                    mask.setVisibility(View.GONE);
                }

            } else {
                vPic3.setVisibility(View.INVISIBLE);
            }
        }
    }

    public static void drawAlbum(ApplicationController mApp, ArrayList<ImageProfile> mImageProfiles,
                                 SquareImageView[] mImageViewList, View mView3Image, View mView2Image, View showAll,
                                 boolean isShowUploadImage, boolean profileFriend,
                                 final ProfileHelperDrawAlbumListener listener) {
        ImageProfileBusiness mImageProfileBusiness = mApp.getImageProfileBusiness();
        if (!mImageProfiles.isEmpty()) {
            if (mImageProfiles.size() > 5) {
                showAll.setVisibility(View.VISIBLE);
            } else {
                showAll.setVisibility(View.INVISIBLE);
            }
            int sizeImageList = Math.min(mImageProfiles.size(), Constants.MAX_IMAGE_PROFILE);
            Log.d(TAG, "sizeImageList: " + sizeImageList);

            if (sizeImageList > 0 && sizeImageList <= 3) {
                mView3Image.setVisibility(View.VISIBLE);
                mView2Image.setVisibility(View.GONE);
            } else {
                mView3Image.setVisibility(View.VISIBLE);
                mView2Image.setVisibility(View.VISIBLE);
            }

            if (sizeImageList <= 3) { // 1, 2, 3 images
                for (int i = 0; i < sizeImageList; i++) {
                    mImageViewList[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mImageViewList[i].setVisibility(View.VISIBLE);
                    if (profileFriend) {
                        mImageProfileBusiness.displayImageProfileFriend(mImageViewList[i],
                                mImageProfiles.get(mImageProfileBusiness.getImageIndex(sizeImageList, i)), false);
                    } else {
                        mImageProfileBusiness.displayImageProfile(mImageViewList[i], mImageProfiles.get(i), true);
                    }
                }

                if (isShowUploadImage) {
                    if (sizeImageList == 2) {
                        mImageViewList[2].setVisibility(View.VISIBLE);
                        mImageViewList[2].setClickable(true);
                        mImageViewList[2].setScaleType(ImageView.ScaleType.CENTER);
                        mImageViewList[2].setImageResource(R.drawable.ic_upload_image);
                        mImageViewList[2].setBackgroundColor(ContextCompat.getColor(mApp, R.color
                                .gray_light));
                        mImageViewList[2].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.onBtnUploadClicked();
                            }
                        });
                    } else if (sizeImageList == 1) {
                        mImageViewList[1].setVisibility(View.VISIBLE);
                        mImageViewList[1].setClickable(true);
                        mImageViewList[1].setScaleType(ImageView.ScaleType.CENTER);
                        mImageViewList[1].setImageResource(R.drawable.ic_upload_image);
                        mImageViewList[1].setBackgroundColor(ContextCompat.getColor(mApp, R.color
                                .gray_light));
                        mImageViewList[1].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.onBtnUploadClicked();
                            }
                        });
                        mImageViewList[2].setVisibility(View.GONE);
                    }
                }
            } else if (sizeImageList == 4) { //4 images
                mImageViewList[4].setVisibility(View.VISIBLE);
                mImageViewList[3].setVisibility(View.VISIBLE);
                mImageViewList[0].setVisibility(View.VISIBLE);
                mImageViewList[1].setVisibility(View.VISIBLE);
                mImageViewList[2].setVisibility(View.GONE);

                if (profileFriend) {
                    mImageProfileBusiness.displayImageProfileFriend(mImageViewList[0], mImageProfiles.get(2), false);
                    mImageProfileBusiness.displayImageProfileFriend(mImageViewList[1], mImageProfiles.get(3), false);
                    mImageProfileBusiness.displayImageProfileFriend(mImageViewList[3], mImageProfiles.get(0), false);
                    mImageProfileBusiness.displayImageProfileFriend(mImageViewList[4], mImageProfiles.get(1), false);
                } else {
                    mImageProfileBusiness.displayImageProfile(mImageViewList[0], mImageProfiles.get(2), true);
                    mImageProfileBusiness.displayImageProfile(mImageViewList[1], mImageProfiles.get(3), true);
                    mImageProfileBusiness.displayImageProfile(mImageViewList[3], mImageProfiles.get(0), true);
                    mImageProfileBusiness.displayImageProfile(mImageViewList[4], mImageProfiles.get(1), true);
                }

                //show btn upload
                if (isShowUploadImage) {
                    mImageViewList[2].setVisibility(View.VISIBLE);
                    mImageViewList[2].setClickable(true);
                    mImageViewList[2].setScaleType(ImageView.ScaleType.CENTER);
                    mImageViewList[2].setImageResource(R.drawable.ic_upload_image);
                    mImageViewList[2].setBackgroundColor(ContextCompat.getColor(mApp, R.color.gray_light));
                    mImageViewList[2].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null)
                                listener.onBtnUploadClicked();
                        }
                    });
                }
            } else { // 5 images
                for (int i = 0; i < sizeImageList; i++) {
                    mImageViewList[i].setScaleType(ImageView.ScaleType.CENTER_CROP);
                    mImageViewList[i].setVisibility(View.VISIBLE);
                    if (profileFriend) {
                        mImageProfileBusiness.displayImageProfileFriend(mImageViewList[i],
                                mImageProfiles.get(mImageProfileBusiness.getImageIndex(sizeImageList, i)), false);
                    } else {
                        mImageProfileBusiness.displayImageProfile(mImageViewList[i],
                                mImageProfiles.get(mImageProfileBusiness.getImageIndex(sizeImageList, i)), true);
                    }
                }
            }
        } else {
            mView3Image.setVisibility(View.GONE);
            mView2Image.setVisibility(View.GONE);
            showAll.setVisibility(View.GONE);
        }

    }

    public static int getIndexImage(ImageProfileBusiness mImageProfileBusiness, ArrayList<ImageProfile> list, int pos) {
        int sizeImageList = list.size();
        if (sizeImageList > 5) {
            return mImageProfileBusiness.getImageIndex(5, pos);
        } else {
            return mImageProfileBusiness.getImageIndex(sizeImageList, pos);
        }

    }

    public static void blurHeaderProfile(ApplicationController mApp, String friendJid, ImageView into) {
        Bitmap icon = BitmapFactory.decodeResource(mApp.getResources(),
                R.drawable.ic_avatar_default);


        /*int color = mApp.getAvatarBusiness().getColorByPhoneNumber(friendJid);
        Bitmap b= mApp.getAvatarBusiness().getAvatarBitmaps()[color];*/
        ImageHelper.blurAvatar(mApp, icon, null, into);


        /*Blurry.with(context).
                radius(15).
                async().
                animate(100).
                sampling(4).
                color(Color.argb(44, 0, 0, 0)).
                capture(capture).
                into(into);*/
    }

    /*public static void blurHeaderProfileWithBitmap(Context context, Bitmap loadedImage, ImageView into) {
        Blurry.with(context).radius(15).async().animate(100).sampling(4).
                color(Color.argb(44, 0, 0, 0)).from(loadedImage).into(into);
    }*/

    public static void drawPersonalInfoAVNO(ApplicationController application, BaseSlidingFragmentActivity activity,
                                            View avnoView, View avnoUnderLine, TextView avnoLabel,
                                            TextView avnoValue, RoundTextView avnoRegister) {
        avnoView.setVisibility(View.GONE);
        avnoUnderLine.setVisibility(View.GONE);
        //TODO always off để up store
        Log.d(TAG, "avno: " + application.getReengAccountBusiness().isAvnoEnable());
        if (application.getReengAccountBusiness().isAvnoEnable()) {
            ReengAccount account = application.getReengAccountBusiness().getCurrentAccount();
            if (account == null || application.getReengAccountBusiness().isViettel()) {
                avnoView.setVisibility(View.GONE);
                avnoUnderLine.setVisibility(View.GONE);
            } else {
                avnoView.setVisibility(View.VISIBLE);
                avnoUnderLine.setVisibility(View.VISIBLE);
                String currentAVNONumber = account.getAvnoNumber();
                //currentAVNONumber = null;
                if (TextUtils.isEmpty(currentAVNONumber)) {
                    avnoRegister.setVisibility(View.GONE);
                    avnoValue.setVisibility(View.GONE);
                    avnoLabel.setText(R.string.avno_title);
                } else {
                    avnoRegister.setVisibility(View.GONE);
                    avnoValue.setVisibility(View.VISIBLE);
                    if (application.getReengAccountBusiness().isVietnam())
                        avnoLabel.setText(R.string.avno_info_label);
                    else
                        avnoLabel.setText(R.string.avno_info_label_not_vn);
                    avnoValue.setText(currentAVNONumber);
                }
            }
        }
    }

    public void drawIdentifyCard(View mViewAVNOIdentifyCard,
                                 ImageView mImgDeleteFrontCard, ImageView mImgDeleteBackCard,
                                 ImageView mImgImageFront, ImageView mImgImageBack,
                                 ProgressLoading mProgressUpFront, ProgressLoading mProgressUpBack) {


    }

    public interface ProfileHelperDrawAlbumListener {
        void onBtnUploadClicked();
    }
}

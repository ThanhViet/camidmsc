package com.metfone.selfcare.helper.message;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.notification.MessageNotificationManager;
import com.metfone.selfcare.util.Log;

import java.util.LinkedList;

/**
 * Created by toanvk2 on 5/18/2016.
 * check incoming message stranger spam link or image
 */
public class CheckSpamStrangerQueue {
    private final String TAG = CheckSpamStrangerQueue.class.getSimpleName();
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ContactBusiness mContactBusiness;
    private final LinkedList<Object> queue;
    private Thread thread;
    private boolean isThreadRunning = false;

    /**
     * @param application
     */
    public CheckSpamStrangerQueue(ApplicationController application) {
        queue = new LinkedList<>();
        this.mApplication = application;
        // neu thread chua chay thi chay thread
        start();

    }

    public void start() {
        if (thread == null) {
            thread = new Thread() {
                @Override
                public void run() {
                    isThreadRunning = true;
                    internalRun();
                    isThreadRunning = false;
                }
            };
            thread.setName("thread check spam stranger");
            thread.setDaemon(true);
        }
        if (!isThreadRunning) {
            thread.start();
        }
    }

    public void stop() {
        //        isThreadRunning = false;
        thread = null;
        queue.clear();
    }

    private void notifyTaskEmpty() {
        Log.d(TAG, "notifyTaskEmpty: ");
    }

    private void waitQueue() {
        Log.i(TAG, "waitQueue");
        try {
            queue.wait();
        } catch (InterruptedException e) {
            Log.e(TAG,"Exception",e);
            stop();
        }
    }

    public void addTask(Object task) {
        if (task == null) {
            return;
        }
        synchronized (queue) {
            queue.addLast(task);
            queue.notify(); // notify any waiting threads
        }
    }

    private Object getNextTask() {
        synchronized (queue) {
            if (queue.isEmpty()) {
                notifyTaskEmpty();
                waitQueue();
            }
            return queue.removeFirst();
        }
    }

    private void internalRun() {
        while (isThreadRunning) {
            Object task = getNextTask();
            processCheckSpamMessage(task);
        }
    }

    private void processCheckSpamMessage(Object object) {
        Log.d(TAG, "processCheckSpamMessage ---->>>>>>>>>>>>>>");
        if (object == null) {
            return;
        }
        ReengMessage message = (ReengMessage) object;
        if (mApplication == null) return;
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        String friendNumber = message.getSender();
        NonContact nonContact = mContactBusiness.getExistNonContact(friendNumber);
        if (nonContact != null && !nonContact.isTimeOutGetInfo() && nonContact.getStateFollow() != -2) {
            if (SpamRoomManager.getInstance(mApplication).isSpamInComingMessage(nonContact, message)) {
                deleteMessageSpam(message, friendNumber);
            }
        } else {
            NonContact nonContactRq = ContactRequestHelper.getInstance(mApplication).getInfoNonContactSynchronous
                    (friendNumber);
            if (nonContactRq != null && nonContactRq.getStateFollow() != -2) {
                if (SpamRoomManager.getInstance(mApplication).isSpamInComingMessage(nonContactRq, message)) {
                    deleteMessageSpam(message, friendNumber);
                }
            }
        }
    }

    private void deleteMessageSpam(ReengMessage message, String friendNumber) {
        Log.d(TAG, "deleteMessageSpam ---->>>>>>>>>>>>>> :" + message.toString());
        ThreadMessage threadMessage = mMessageBusiness.findExistingSoloThread(friendNumber);
        if (threadMessage != null) {
            if (message.getReadState() == ReengMessageConstant.READ_STATE_UNREAD) {
                int numberUnread = threadMessage.getNumOfUnreadMessage();
                Log.d(TAG, "deleteMessageSpam ---- message unread :" + numberUnread);
                if (numberUnread > 0) numberUnread--;
                threadMessage.setNumOfUnreadMessage(numberUnread);
                mMessageBusiness.updateThreadMessage(threadMessage);
            }
            threadMessage.getAllMessages().remove(message);
            mMessageBusiness.deleteAMessage(threadMessage, message);
            // notify change
            mMessageBusiness.refreshThreadWithoutNewMessage(threadMessage.getId());
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            ReengMessage fakeMessage = new SoloSendTextMessage(threadMessage, myNumber,
                    friendNumber, mApplication.getResources().getString(R.string.spam_link_stranger_incoming));
            mApplication.getXmppManager().sendReengMessage(fakeMessage, threadMessage);
            MessageNotificationManager.getInstance(mApplication).clearNotifyThreadMessage(threadMessage);
        }
    }
}
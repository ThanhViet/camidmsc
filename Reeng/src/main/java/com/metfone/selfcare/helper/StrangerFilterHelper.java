package com.metfone.selfcare.helper;

import android.content.SharedPreferences;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.StrangerLocation;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toanvk2 on 8/23/2017.
 */

public class StrangerFilterHelper {
    private static final String TAG = StrangerFilterHelper.class.getSimpleName();
    private static StrangerFilterHelper mInstance;
    public static final int AGE_MIN = 0;
    public static final int AGE_MAX = 80;
    private ApplicationController mApplication;
    private SharedPreferences mPref;
    private Resources mRes;
    private ArrayList<StrangerLocation> listLocations = new ArrayList<>();
    private String mFilterLocationId;
    private StrangerLocation mFilterLocation;
    private int mFilterSex;
    private int mFilterAgeMin;
    private int mFilterAgeMax;
    private boolean isHideLocation;
    private List<FilterChangeListener> filterChangeListeners;

    private static final String LOCATION = "{\"UNKNOWN\":[{\"name\":\"All\",\"short_name\":\"ALL\",\"id\":\"-1\"}]," +
            "\"VN\":[{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}," +
            "{\"name\":\"An Giang\",\"short_name\":\"AG\",\"id\":\"A076\"}," +
            "{\"name\":\"Bà Rịa-Vũng Tàu\",\"short_name\":\"BV\",\"id\":\"V064\"}," +
            "{\"name\":\"Bạc Liêu\",\"short_name\":\"BL\",\"id\":\"B781\"}," +
            "{\"name\":\"Bắc Kạn\",\"short_name\":\"BK\",\"id\":\"B281\"}," +
            "{\"name\":\"Bắc Giang\",\"short_name\":\"BG\",\"id\":\"B240\"}," +
            "{\"name\":\"Bắc Ninh\",\"short_name\":\"BN\",\"id\":\"B241\"}," +
            "{\"name\":\"Bến Tre\",\"short_name\":\"BT\",\"id\":\"B075\"}," +
            "{\"name\":\"Bình Dương\",\"short_name\":\"BD\",\"id\":\"B650\"}," +
            "{\"name\":\"Bình Định\",\"short_name\":\"BĐ\",\"id\":\"B056\"}," +
            "{\"name\":\"Bình Phước\",\"short_name\":\"BP\",\"id\":\"B651\"}," +
            "{\"name\":\"Bình Thuận\",\"short_name\":\"BTh\",\"id\":\"B062\"}," +
            "{\"name\":\"Cà Mau\",\"short_name\":\"CM\",\"id\":\"C780\"}," +
            "{\"name\":\"Cao Bằng\",\"short_name\":\"CB\",\"id\":\"C026\"}," +
            "{\"name\":\"Cần Thơ\",\"short_name\":\"CT\",\"id\":\"C710\"}," +
            "{\"name\":\"Đà Nẵng\",\"short_name\":\"ĐNa\",\"id\":\"D511\"}," +
            "{\"name\":\"Đắk Lắk\",\"short_name\":\"ĐL\",\"id\":\"D500\"}," +
            "{\"name\":\"Đắk Nông\",\"short_name\":\"ĐNo\",\"id\":\"D501\"}," +
            "{\"name\":\"Điện Biên\",\"short_name\":\"ĐB\",\"id\":\"D230\"}," +
            "{\"name\":\"Đồng Nai\",\"short_name\":\"ĐN\",\"id\":\"D061\"}," +
            "{\"name\":\"Đồng Tháp\",\"short_name\":\"ĐT\",\"id\":\"D067\"}," +
            "{\"name\":\"Gia Lai\",\"short_name\":\"GL\",\"id\":\"G059\"}," +
            "{\"name\":\"Hà Giang\",\"short_name\":\"HG\",\"id\":\"H019\"}," +
            "{\"name\":\"Hà Nam\",\"short_name\":\"HNA\",\"id\":\"N351\"}," +
            "{\"name\":\"Hà Nội\",\"short_name\":\"HAN\",\"id\":\"H004\"}," +
            "{\"name\":\"Hà Tĩnh\",\"short_name\":\"HT\",\"id\":\"H039\"}," +
            "{\"name\":\"Hải Dương\",\"short_name\":\"HD\",\"id\":\"H320\"}," +
            "{\"name\":\"Hải Phòng\",\"short_name\":\"HP\",\"id\":\"H031\"}," +
            "{\"name\":\"Hậu Giang\",\"short_name\":\"HG\",\"id\":\"H711\"}," +
            "{\"name\":\"Hòa Bình\",\"short_name\":\"HB\",\"id\":\"H018\"}," +
            "{\"name\":\"TP Hồ Chí Minh\",\"short_name\":\"SG\",\"id\":\"T008\"}," +
            "{\"name\":\"Hưng Yên\",\"short_name\":\"HY\",\"id\":\"H321\"}," +
            "{\"name\":\"Khánh Hòa\",\"short_name\":\"KH\",\"id\":\"K058\"}," +
            "{\"name\":\"Kiên Giang\",\"short_name\":\"KG\",\"id\":\"K077\"}," +
            "{\"name\":\"Kon Tum\",\"short_name\":\"KT\",\"id\":\"K060\"}," +
            "{\"name\":\"Lai Châu\",\"short_name\":\"LC\",\"id\":\"L231\"}," +
            "{\"name\":\"Lạng Sơn\",\"short_name\":\"LS\",\"id\":\"L025\"}," +
            "{\"name\":\"Lào Cai\",\"short_name\":\"LCa\",\"id\":\"L020\"}," +
            "{\"name\":\"Lâm Đồng\",\"short_name\":\"LĐ\",\"id\":\"L063\"}," +
            "{\"name\":\"Long An\",\"short_name\":\"LA\",\"id\":\"L072\"}," +
            "{\"name\":\"Nam Định\",\"short_name\":\"NĐ\",\"id\":\"N350\"}," +
            "{\"name\":\"Nghệ An\",\"short_name\":\"NA\",\"id\":\"N038\"}," +
            "{\"name\":\"Ninh Bình\",\"short_name\":\"NB\",\"id\":\"N030\"}," +
            "{\"name\":\"Ninh Thuận\",\"short_name\":\"NT\",\"id\":\"N068\"}," +
            "{\"name\":\"Phú Thọ\",\"short_name\":\"PT\",\"id\":\"P210\"}," +
            "{\"name\":\"Phú Yên\",\"short_name\":\"PY\",\"id\":\"P057\"}," +
            "{\"name\":\"Quảng Bình\",\"short_name\":\"QB\",\"id\":\"Q052\"}," +
            "{\"name\":\"Quảng Nam\",\"short_name\":\"QNa\",\"id\":\"Q510\"}," +
            "{\"name\":\"Quảng Ngãi\",\"short_name\":\"QNg\",\"id\":\"Q055\"}," +
            "{\"name\":\"Quảng Ninh\",\"short_name\":\"QN\",\"id\":\"Q033\"}," +
            "{\"name\":\"Quảng Trị\",\"short_name\":\"QT\",\"id\":\"Q053\"}," +
            "{\"name\":\"Sóc Trăng\",\"short_name\":\"ST\",\"id\":\"S079\"}," +
            "{\"name\":\"Sơn La\",\"short_name\":\"SL\",\"id\":\"S022\"}," +
            "{\"name\":\"Tây Ninh\",\"short_name\":\"TN\",\"id\":\"T066\"}," +
            "{\"name\":\"Thái Bình\",\"short_name\":\"TB\",\"id\":\"T036\"}," +
            "{\"name\":\"Thái Nguyên\",\"short_name\":\"TNg\",\"id\":\"T280\"}," +
            "{\"name\":\"Thanh Hóa\",\"short_name\":\"TH\",\"id\":\"T037\"}," +
            "{\"name\":\"Thừa Thiên Huế\",\"short_name\":\"TTH\",\"id\":\"T054\"}," +
            "{\"name\":\"Tiền Giang\",\"short_name\":\"TG\",\"id\":\"T073\"}," +
            "{\"name\":\"Trà Vinh\",\"short_name\":\"TV\",\"id\":\"T074\"}," +
            "{\"name\":\"Tuyên Quang\",\"short_name\":\"TQ\",\"id\":\"T027\"}," +
            "{\"name\":\"Vĩnh Long\",\"short_name\":\"VL\",\"id\":\"V070\"}," +
            "{\"name\":\"Vĩnh Phúc\",\"short_name\":\"VP\",\"id\":\"V211\"}," +
            "{\"name\":\"Yên Bái\",\"short_name\":\"YB\",\"id\":\"Y029\"}]," +
            "\"KH\":[{\"name\":\"Campuchia\",\"short_name\":\"KH\",\"id\":\"KH\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"LA\":[{\"name\":\"Lào\",\"short_name\":\"LA\",\"id\":\"LA\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"TL\":[{\"name\":\"Đông Timor\",\"short_name\":\"TL\",\"id\":\"TL\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"MZ\":[{\"name\":\"Mozambique\",\"short_name\":\"MZ\",\"id\":\"MZ\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"CM\":[{\"name\":\"Cameroon\",\"short_name\":\"CM\",\"id\":\"CM\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"BI\":[{\"name\":\"Burundi\",\"short_name\":\"BI\",\"id\":\"BI\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"TZ\":[{\"name\":\"Tanzania\",\"short_name\":\"TZ\",\"id\":\"TZ\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"HT\":[{\"name\":\"Haiti\",\"short_name\":\"HT\",\"id\":\"HT\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"PE\":[{\"name\":\"Peru\",\"short_name\":\"PE\",\"id\":\"PE\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]," +
            "\"MM\":[{\"name\":\"Myanmar\",\"short_name\":\"MM\",\"id\":\"MM\"},{\"name\":\"Việt Nam\",\"short_name\":\"VN\",\"id\":\"VN\"}]" +
            "}";

    public static synchronized StrangerFilterHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new StrangerFilterHelper(application);
        }
        return mInstance;
    }

    private StrangerFilterHelper(ApplicationController application) {
        this.mApplication = application;
        this.mPref = mApplication.getPref();
        this.mRes = mApplication.getResources();
        init();
        initLocation();
    }

    public void reinit(){
        mFilterLocation = null;
        init();
        initLocation();
    }

    public void addFilterChange(FilterChangeListener filterChangeListener){
        if(filterChangeListeners == null){
            filterChangeListeners = new ArrayList<>();
        }
        if(filterChangeListener != null && !filterChangeListeners.contains(filterChangeListener)){
            filterChangeListeners.add(filterChangeListener);
        }
    }
    public void removeFilterChangeListener(FilterChangeListener filterChangeListener){
        if(filterChangeListeners != null){
            filterChangeListeners.remove(filterChangeListener);
        }
    }


    public void setFilterLocation(StrangerLocation mFilterLocation) {
        this.mFilterLocation = mFilterLocation;
    }

    public void init() {
        mFilterLocationId = mPref.getString(Constants.STRANGER_MUSIC.FILTER_LOCATION, mApplication.getReengAccountBusiness().getRegionCode());
        mFilterSex = mPref.getInt(Constants.STRANGER_MUSIC.FILTER_SEX, -1);
        isHideLocation = mPref.getBoolean(Constants.STRANGER_MUSIC.HIDE_LOCATION, false);
        mFilterAgeMin = mPref.getInt(Constants.STRANGER_MUSIC.FILTER_AGE_MIN, AGE_MIN);
        mFilterAgeMax = mPref.getInt(Constants.STRANGER_MUSIC.FILTER_AGE_MAX, AGE_MAX);
    }

    private ArrayList<StrangerLocation> parserLocation(JsonArray array) {
        ArrayList<StrangerLocation> locations = new ArrayList<>();
        try {
            if (array != null) {
                int length = array.size();
                Gson gson = new Gson();
                for (int i = 0; i < length; i++) {
                    StrangerLocation location = gson.fromJson(array.get(i), StrangerLocation.class);
                    location.setNameUnicode(TextHelper.getInstant().convertUnicodeToAscci(location.getName()));
                    locations.add(location);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return locations;
    }

    private void initLocation() {
        String regionCode = mApplication.getReengAccountBusiness().getRegionCode();
        listLocations = new ArrayList<>();
        try {
            JsonObject jsonObject = new JsonParser().parse(LOCATION).getAsJsonObject();
            if (jsonObject.has(regionCode)) {
                listLocations = parserLocation(jsonObject.getAsJsonArray(regionCode));
            } else {
                listLocations = parserLocation(jsonObject.getAsJsonArray("UNKNOWN"));
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        initLocationSelected();
    }

    public void initLocationSelected() {
        for (StrangerLocation item : listLocations) {
            if (mFilterLocationId.equals(item.getLocationId())) {
                mFilterLocation = item;
                item.setSelected(true);
            } else {
                item.setSelected(false);
            }
        }
        if (mFilterLocation == null) {// not find
            mFilterLocation = listLocations.get(0);
            mFilterLocation.setSelected(true);
            //TODO nếu default ==-1 thì không gọi đoạn code này, nếu default = item đầu tiên thì dùng
            //mFilterLocationId = mFilterLocation.getLocationId();
        }
    }

    public ArrayList<StrangerLocation> getListLocations() {
        return listLocations;
    }

    public void updateFilter(StrangerLocation location, int sex, int minAge, int maxAge, boolean isHideLocation) {
        this.mFilterLocation = location;
        this.mFilterLocationId = mFilterLocation.getLocationId();
        this.mFilterSex = sex;
        this.mFilterAgeMin = minAge;
        this.mFilterAgeMax = maxAge;
        this.isHideLocation = isHideLocation;
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(Constants.STRANGER_MUSIC.FILTER_LOCATION, mFilterLocationId);
        editor.putInt(Constants.STRANGER_MUSIC.FILTER_SEX, mFilterSex);
        editor.putInt(Constants.STRANGER_MUSIC.FILTER_AGE_MIN, mFilterAgeMin);
        editor.putInt(Constants.STRANGER_MUSIC.FILTER_AGE_MAX, mFilterAgeMax);
        editor.putBoolean(Constants.STRANGER_MUSIC.HIDE_LOCATION, isHideLocation);
        editor.apply();
        notifyChangeFilter();
    }

    public void notifyChangeFilter(){
        if(filterChangeListeners != null){
            for(FilterChangeListener filterChangeListener: filterChangeListeners){
                filterChangeListener.filterChanged();
            }
        }
    }

    public String getFormatFilterText(boolean isAround){
        StringBuilder sb = new StringBuilder();
        if(isAround){
            sb.append(mRes.getString(R.string.nearby)).append(", ");
        }else {
            if (getFilterLocation() != null) {
                sb.append(getFilterLocation().getName()).append(", ");
            }
        }
        if (getFilterSex() == Constants.CONTACT.GENDER_MALE) {
            if(!isAround) {
                sb.append(mRes.getString(R.string.sex_male)).append(", ");
            }else {
                sb.append(mRes.getString(R.string.sex_male));
            }
        } else if (getFilterSex() == Constants.CONTACT.GENDER_FEMALE) {
            if(!isAround) {
                sb.append(mRes.getString(R.string.sex_female)).append(", ");
            }else {
                sb.append(mRes.getString(R.string.sex_female));
            }
        } else {
            sb.append(mRes.getString(R.string.sex_male)).append("/");
            if(!isAround) {
                sb.append(mRes.getString(R.string.sex_female)).append(", ");
            }else {
                sb.append(mRes.getString(R.string.sex_female));
            }
        }
        if(!isAround) {
            int filterAgeMin = mFilterAgeMin == 60 ? 61 : mFilterAgeMin;
            sb.append(mRes.getString(R.string.filter_format, filterAgeMin, getRealFilterAgeMax()));
        }
        return sb.toString();
    }

    public StrangerLocation getFilterLocation() {
        return mFilterLocation;
    }

    public boolean isHideLocation() {
        return isHideLocation;
    }

    public String getFilterLocationId() {
        return mFilterLocationId;
    }

    public int getFilterSex() {
        return mFilterSex;
    }

    public int getRealFilterAgeMin() {
        return mFilterAgeMin;
    }

    public int getFilterAgeMin() {
        if (mFilterAgeMin <= AGE_MIN) {
            return -1;
        }
        return mFilterAgeMin;
    }

    public int getRealFilterAgeMax() {
        return mFilterAgeMax;
    }

    public int getFilterAgeMax() {
        if (mFilterAgeMax >= AGE_MAX) {
            return -1;
        }
        return mFilterAgeMax;
    }

    public interface FilterChangeListener{
        void filterChanged();
    }
}
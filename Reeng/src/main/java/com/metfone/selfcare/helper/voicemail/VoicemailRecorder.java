package com.metfone.selfcare.helper.voicemail;

import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.Build;

import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.IOException;

public class VoicemailRecorder {

    private static final String TAG = VoicemailRecorder.class.getSimpleName();
    public static final String VOICEMAIL_EXTENSION = ".aac";
    public static final String VOICEMAIL_EXTENSION_AMR = ".amr";
    public static final String VOICEMAIL_PREFIX_NAME = "vpvm";
    private MediaRecorder recorder = null;
    private File recordedAudioFile = null;
    private boolean isRecordSuccess = true;
    private String filePath;

    public File getRecordedAudioFile() {
        return recordedAudioFile;
    }

    private String getFilePath() {
        return filePath;
    }

    public String getFileName() {
        return recordedAudioFile.getName();
    }

    public void setFilePath(String extension) {
        File recordDir = new File(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.VOICEMAIL_FOLDER);
        if (!recordDir.exists()) {
            recordDir.mkdirs();
        }
        recordedAudioFile = new File(Config.Storage.REENG_STORAGE_FOLDER
                + Config.Storage.VOICEMAIL_FOLDER + "/" + VOICEMAIL_PREFIX_NAME
                + System.currentTimeMillis() + extension);
        filePath = recordedAudioFile.getAbsolutePath();
    }

    public void startRecording() {
        try {
            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            recorder.setAudioChannels(1);
            if (Build.VERSION.SDK_INT >= 10) {
                recorder.setOutputFormat(AudioFormat.ENCODING_PCM_16BIT);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                recorder.setAudioSamplingRate(44100);
                recorder.setAudioEncodingBitRate(128000);
                this.setFilePath(VOICEMAIL_EXTENSION);
            } else {
                recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                recorder.setAudioSamplingRate(8000);                    // 8khz
                this.setFilePath(VOICEMAIL_EXTENSION_AMR);
            }
            recorder.setOutputFile(getFilePath());
            recorder.setOnErrorListener(errorListener);
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            isRecordSuccess = false;
            Log.e(TAG, "IllegalStateException", e);
        } catch (IOException e) {
            isRecordSuccess = false;
            Log.e(TAG, "IOException", e);
        } catch (Exception e) {
            isRecordSuccess = false;
            Log.e(TAG, "Exception", e);
        }

    }

    public void stopRecording() {
        if (null != recorder) {
            try {
                recorder.stop();
                //   recorder.reset();
                recorder.release();
                recorder = null;
            } catch (IllegalStateException e) {
                isRecordSuccess = false;
                Log.e(TAG, "IllegalStateException", e);
            } catch (Exception e) {
                isRecordSuccess = false;
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public boolean isRecordSuccess() {
        return isRecordSuccess;
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.e("VoicemailRecorder", "error recording voicemail");
            isRecordSuccess = false;
        }
    };
}

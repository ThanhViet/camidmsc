package com.metfone.selfcare.helper;

import android.text.TextUtils;

import com.metfone.selfcare.database.model.SearchQuery;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by namnh40 on 8/17/2016.
 */
public class SearchHelper {

    private static final String TAG = SearchHelper.class.getSimpleName();

    private static final int NUM_SUGGEST = 3;
    private static final int CP = 1; // Ban quyen cua Nha cung cap dich vu (Content Provider)
    //    private static final String RESULT_REGEX = "[/|:|.|-|_|+|&|||!|~|^|?|;|'|$|@]";
    private static final String RESULT_REGEX = "[-|+|:|.|_|+|;|'|$|@||/]";
    //    private static final String QUERY_REGEX = "[+|-|&||!|(|)|{|}|[|]|^|\"|~|*|?|:|\\|']";
    private static final String QUERY_REGEX = "[`||^|{|}|\\\\|\\/|~|!|@|#|$|%|&|*|(|)|\\-|_|=|+|\\[|\\]|;|:|'|\"|,|<|.|>|?]";

    private static Comparator<SearchQuery> sortSearchByListeno = new Comparator<SearchQuery>() {
        @Override
        public int compare(SearchQuery lhs, SearchQuery rhs) {
            return (int) (rhs.getListenNo() - lhs.getListenNo());
        }
    };

    public static String convertQuery(String keySearch) {
        Log.i(TAG, "convertQuery input: " + keySearch);
        if (TextUtils.isEmpty(keySearch))
            keySearch = "";
        else {
            keySearch = keySearch.replaceAll(QUERY_REGEX, " ");
            //Log.i(TAG, "convertQuery 1 keySearch: " + keySearch);
            keySearch = keySearch.replaceAll("\\s{2,}", " ");
            //Log.i(TAG, "convertQuery 2 keySearch: " + keySearch);
            keySearch = keySearch.trim();
        }
        Log.e(TAG, "convertQuery output: " + keySearch);
        return keySearch;
    }

    private static String convertText(String text) {
        Log.i(TAG, "convertText input: " + text);
        if (TextUtils.isEmpty(text))
            text = "";
        else {
            text = TextHelper.getInstant().convertUnicodeToAscci(text);
            try {
                Pattern pattern = Pattern.compile(RESULT_REGEX);
                Matcher matcher = pattern.matcher(text);
                if (matcher.find()) {
                    text = matcher.replaceAll(" ");
                    text = text.replaceAll("\\s{2,}", " ");
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        Log.e(TAG, "convertText output: " + text);
        return text.trim();
    }

    /**
     * Chuan hoa du lieu search data:<br>
     * 1. Search dung Text.<br>
     * 2. Trong danh sach ket qua tra ve co 2 bai hat trung nhau thi lay bai hat
     * ban quyen cua ca si.</br>
     */
    public static ArrayList<SearchQuery> filterSearchData(String keySearch, ArrayList<SearchQuery> data) {

        if (data != null) {
            ArrayList<SearchQuery> correctList = new ArrayList<>();
            ArrayList<SearchQuery> correctList2 = new ArrayList<>();
            ArrayList<SearchQuery> similarList1 = new ArrayList<>();
            ArrayList<SearchQuery> similarList2 = new ArrayList<>();
            ArrayList<SearchQuery> similarList3 = new ArrayList<>();
            ArrayList<SearchQuery> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            int size = data.size();
            for (int i = 0; i < size; i++) {
                SearchQuery item = data.get(i);
                if (item != null) {
                    String keyFullName = item.getFullName().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getSearchInfo().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            Collections.sort(filterCoincide(correctList), sortSearchByListeno);
            Collections.sort(filterCoincide(correctList2), sortSearchByListeno);
            Collections.sort(filterCoincide(similarList1), sortSearchByListeno);
            Collections.sort(filterCoincide(similarList2), sortSearchByListeno);
            Collections.sort(filterCoincide(similarList3), sortSearchByListeno);

            data.addAll(correctList);
            data.addAll(correctList2);
            data.addAll(similarList1);
            data.addAll(similarList2);
            data.addAll(similarList3);

            //Collections.sort(filterCoincide(otherList), sortSearchByListeno);
            data.addAll(otherList);
        } else {
            data = new ArrayList<>();
        }
        return data;
    }

    private static ArrayList<SearchQuery> filterCoincide(ArrayList<SearchQuery> data) {
        if (data == null)
            return new ArrayList<>();
        int size = data.size();
        try {
            Map<String, ArrayList<SearchQuery>> maps = new HashMap<>();
            for (int i = 0; i < size; i++) {
                SearchQuery item = data.get(i);
                if (!maps.containsKey(item.getKeyCoincide())) {
                    ArrayList<SearchQuery> tmp = new ArrayList<>();
                    tmp.add(item);
                    maps.put(item.getKeyCoincide(), tmp);
                } else {
                    maps.get(item.getKeyCoincide()).add(item);
                }
            }
            data.clear();
            for (String key : maps.keySet()) {
                ArrayList<SearchQuery> tmp = maps.get(key);
                if (tmp.size() <= 1)
                    data.addAll(tmp);
                else {
                    size = tmp.size();
                    boolean check = false;
                    for (int j = 0; j < size; j++) {
                        if (tmp.get(j).getIsSinger() == CP) {
                            check = true;
                            data.add(tmp.get(j));
                        }
                    }
                    if (!check)
                        data.addAll(tmp);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return data;
    }


    /*public static ArrayList<Movie> filterSearchMoviesSuggest(ArrayList<Movie> listSearch,
                                                             String keySearch) {
        if (listSearch == null || listSearch.isEmpty())
            return new ArrayList<>();
        return filterSearchDataMovies(keySearch, listSearch, false);
//        if (dataMovies == null)
//            dataMovies = new ArrayList<>();
//        keySearch = keySearch.toLowerCase(Locale.US);
//        String keySearchTmp = convertText(keySearch);
//        if (dataTopResult != null) {
//            SearchModel movies = null;
//            if (!dataMovies.isEmpty()) {
//                movies = dataMovies.get(0);
//            }
//            //todo giống tuyệt đối
//            if (movies != null && movies.getName().equalsIgnoreCase(keySearch)) {
//                dataTopResult.add(dataMovies.remove(0));
//            }
//            //todo tương đối có dấu
//            else if (movies != null && movies.getName().toLowerCase(Locale.US).contains(keySearch)) {
//                dataTopResult.add(dataMovies.remove(0));
//            }
//            //todo tương đối không có dấu
//            else if (movies != null && convertText(movies.getName().toLowerCase(Locale.US)).contains(keySearchTmp)) {
//                dataTopResult.add(dataMovies.remove(0));
//            }
//            //todo con lai
//            else if (dataMovies.size() == NUM_SUGGEST)
//                dataMovies.remove(dataMovies.size() - 1);
//        }
//        else if (dataMovies.size() == NUM_SUGGEST)
//            dataMovies.remove(dataMovies.size() - 1);
    }*/

    public static ArrayList<Movie> filterSearchDataMovies(String keySearch, ArrayList<Movie> data) {
        ArrayList<Movie> list = new ArrayList<>();
        if (data != null && !data.isEmpty()) {
            List<Movie> correctList = new ArrayList<>();
            List<Movie> correctList2 = new ArrayList<>();
            List<Movie> similarList1 = new ArrayList<>();
            List<Movie> similarList2 = new ArrayList<>();
            List<Movie> similarList3 = new ArrayList<>();
            List<Movie> otherList = new ArrayList<>();

            keySearch = keySearch.toLowerCase(Locale.US);
            String keySearchTemp = convertText(keySearch);
            int size = data.size();
            for (int i = 0; i < size; i++) {
                Movie item = data.get(i);
                if (item != null) {
                    String keyFullName = item.getName().toLowerCase(Locale.US);
                    String keyFullNameTemp = convertText(keyFullName);
                    String keyQueryInfo = item.getSearchInfo().toLowerCase(Locale.US);
                    String keyQueryInfoTemp = convertText(keyQueryInfo);

                    if (keyFullName.equalsIgnoreCase(keySearch)) {
                        correctList.add(item);
                    } else if (keyFullName.contains(keySearch)) {
                        correctList2.add(item);
                    } else if (keyFullNameTemp.equalsIgnoreCase(keySearchTemp)) {
                        similarList1.add(item);
                    } else if (keyFullNameTemp.contains(keySearchTemp)) {
                        similarList2.add(item);
                    } else if (keyQueryInfoTemp.contains(keySearchTemp)) {
                        similarList3.add(item);
                    } else {
                        otherList.add(item);
                    }
                }
            }
            data.clear();
            list.addAll(correctList);
            list.addAll(correctList2);
            list.addAll(similarList1);
            list.addAll(similarList2);
            list.addAll(similarList3);
            list.addAll(otherList);

            correctList.clear();
            correctList2.clear();
            similarList1.clear();
            similarList2.clear();
            similarList3.clear();
            otherList.clear();
        }
        return list;
    }
}

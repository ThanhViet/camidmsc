package com.metfone.selfcare.helper.httprequest;

import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.OfficerBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.OfficerAccountConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.HTTP.OFFICER;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.helper.encrypt.RSAEncrypt;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQInfo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 20/08/2015.
 */
public class RoomChatRequestHelper {
    private static final String TAG = RoomChatRequestHelper.class.getSimpleName();
    private static final String TAG_SEARCH_OA = "TAG_SEARCH_OA";
    public static final int TAB_TOPIC = 1;
    public static final int TAB_STAR = 2;
    public static final int TAB_LOCATION = 3;

    private static RoomChatRequestHelper instance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private OfficerBusiness mOfficerBusiness;

    private RoomChatRequestHelper(ApplicationController application) {
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        this.mOfficerBusiness = mApplication.getOfficerBusiness();
    }

    public static synchronized RoomChatRequestHelper getInstance(ApplicationController application) {
        if (instance == null) {
            instance = new RoomChatRequestHelper(application);
        }
        return instance;
    }


    /**
     * lay list officer tu sv
     *
     * @param listener
     */
    public void getOfficerAccountFromSv(final OfficerBusiness.RequestOfficerResponseListener listener) {
        getOfficerAccountFromSv(listener, false, false);
    }

    public void getOfficerAccountFromSv(final OfficerBusiness.RequestOfficerResponseListener listener,
                                        final boolean isSearch, final boolean getListHome) {
        if (!mAccountBusiness.isValidAccount()) {
            return;
        }
        ReengAccount reengAccount = mAccountBusiness.getCurrentAccount();
        String jidNumberEncode = HttpHelper.EncoderUrl(reengAccount.getJidNumber());
        String timestamp = String.valueOf(TimeHelper.getCurrentTime());
        String token = mAccountBusiness.getToken();
        StringBuilder sb = new StringBuilder();

        sb.append(reengAccount.getJidNumber()).
                append(token).
                append(timestamp);
        String security = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), reengAccount
                .getToken()));

        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .GET_LIST_OFFICER_ACCOUNT);
        url = String.format(url, jidNumberEncode, timestamp, security);
        Log.i(TAG, "url get list: " + url);
        /*String url = String.format(UrlConfigHelper.getInstance(mApplication).
                getUrlConfigOfFile(Config.UrlEnum.GET_OFFICER_LIST), jidNumberEncode, reengAccount.getToken());*/
        // request
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                String resDecrypt = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                                Log.i(TAG, "text decrypt: " + resDecrypt);
                                JSONObject resJsonDecrypt = new JSONObject(resDecrypt);
                                if (resJsonDecrypt.optInt(Constants.HTTP.REST_CODE) == 200) {
                                    ArrayList<OfficerAccount> listOfficerAccounts =
                                            parserOfficerAccountFromResponse(resJsonDecrypt, isSearch, getListHome);
                                    mOfficerBusiness.setOfficerAccountSv(listOfficerAccounts);
                                    listener.onResponse(listOfficerAccounts);
                                } else {
                                    listener.onError(errorCode, "");
                                }
                            } else {
                                listener.onError(errorCode, "");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(errorCode, "");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1, "");
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SEARCH_OA, false);
    }

    public void cancelSearchOfficial() {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SEARCH_OA);
    }

    public void searchOfficerAccountFromSv(String content, final OfficerBusiness.RequestOfficerResponseListener
            listener) {
        if (!mAccountBusiness.isValidAccount()) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SEARCH_OA);
        ReengAccount reengAccount = mAccountBusiness.getCurrentAccount();
        String jidNumberEncode = HttpHelper.EncoderUrl(reengAccount.getJidNumber());
        String timestamp = String.valueOf(TimeHelper.getCurrentTime());
        String token = mAccountBusiness.getToken();
        StringBuilder sb = new StringBuilder();

        sb.append(reengAccount.getJidNumber()).
                append(content).
                append(token).
                append(timestamp);
        String security = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), reengAccount
                .getToken()));

        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .SEARCH_OFFICER_ACCOUNT);
        url = String.format(url, jidNumberEncode, timestamp, security, content);
        Log.i(TAG, "url search: " + url);
        /*String url = String.format(UrlConfigHelper.getInstance(mApplication).
                getUrlConfigOfFile(Config.UrlEnum.GET_OFFICER_LIST), jidNumberEncode, reengAccount.getToken());*/
        // request
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                String resDecrypt = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                                Log.i(TAG, "text decrypt: " + resDecrypt);
                                JSONObject resJsonDecrypt = new JSONObject(resDecrypt);
                                if (resJsonDecrypt.optInt(Constants.HTTP.REST_CODE) == 200) {
                                    ArrayList<OfficerAccount> listOfficerAccounts =
                                            parserOfficerAccountFromResponse(resJsonDecrypt, true, false);
                                    listener.onResponse(listOfficerAccounts);
                                } else {
                                    listener.onError(errorCode, "");
                                }
                            } else {
                                listener.onError(errorCode, "");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "JSONException:", e);
                            listener.onError(errorCode, "");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1, "");
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SEARCH_OA, false);
    }

    public void getListStarRoomWithTab(final OfficerBusiness.RequestListRoomTAbResponseListener listener) {
        if (!mAccountBusiness.isValidAccount()) {
            return;
        }
        ReengAccount reengAccount = mAccountBusiness.getCurrentAccount();
        String jidNumberEncode = HttpHelper.EncoderUrl(reengAccount.getJidNumber());
        String timestamp = String.valueOf(TimeHelper.getCurrentTime());
        String languagecode = mAccountBusiness.getDeviceLanguage();
        String token = mAccountBusiness.getToken();
        StringBuilder sb = new StringBuilder();

        sb.append(reengAccount.getJidNumber()).
                append(mAccountBusiness.getRegionCode()).
                append(languagecode).
                append(token).
                append(timestamp);
        String security = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), reengAccount
                .getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.GET_STAR_ROOM_TAB),
                jidNumberEncode,
                mAccountBusiness.getRegionCode(),
                languagecode,
                timestamp,
                security);

        Log.d(TAG, url);
        StringRequest jsObjRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<OfficerAccount> listOfficerAccounts = parseRoomTabList(responseObject);
                                mOfficerBusiness.setmListRoomTab(listOfficerAccounts);
                                listener.onResponse(listOfficerAccounts);
                            } else {
                                listener.onError(errorCode, "");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, "Exception");
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(-1, error.toString());
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(jsObjRequest, "GET_ROOM_TAB", true);
    }

    /**
     * @param jsonObjectResponse
     * @return list room
     */
    private ArrayList<OfficerAccount> parseRoomTabList(JSONObject jsonObjectResponse) {
        if (jsonObjectResponse.has("listTab")) {
            OfficerBusiness officerBusiness = mApplication.getOfficerBusiness();
            JSONArray tabList = jsonObjectResponse.optJSONArray("listTab");
            ArrayList<OfficerAccount> listRoom = new ArrayList<>();
            if (tabList != null && tabList.length() > 0) {
                officerBusiness.getTabName().clear();
                for (int i = 0; i < tabList.length(); i++) {
                    JSONObject tab = tabList.optJSONObject(i);
                    int tabid = tab.optInt("tabId");
                    //get tab name
                    officerBusiness.getTabName().add(tab.optString("tabName"));
                    //Get topic list
                    JSONArray listTopic = tab.optJSONArray("listItem");
                    if (listTopic != null && listTopic.length() > 0) {
                        for (int j = 0; j < listTopic.length(); j++) {
                            JSONObject topic = listTopic.optJSONObject(j);
                            if (topic != null) {
                                OfficerAccount room = new OfficerAccount();
                                try {
                                    room.setJsonObject(topic, tabid);
                                    listRoom.add(room);
                                } catch (Exception e) {
                                    Log.e(TAG, "Exception", e);
                                }
                            }
                        }
                    }
                }
            }
            return listRoom;
        }
        return null;
    }

    public void followRoomChat(final String roomId, final String roomName, final String roomAvatar, final int
            joinState, final OfficerBusiness.RequestFollowRoomChatListener listener) {
        if (!mAccountBusiness.isValidAccount() ||
                !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onErrorFollow(-1, null);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ROOM_CHAT_FOLLOW);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: followRoomChat: " + response);
                        int errorCode;
                        String desc = null;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            desc = responseObject.optString(Constants.HTTP.REST_DESC, null);
                            if (errorCode == HTTPCode.E200_OK) {
                                ThreadMessage thread = mApplication.getMessageBusiness()
                                        .findExistingOrCreateRoomThread(roomId,
                                                roomName, roomAvatar, OfficerAccountConstant.ROOM_STATE_NONE,
                                                ThreadMessageConstant.STATE_SHOW);
                                //update
                                ArrayList<MediaModel> songList = new ArrayList<>();
                                if (responseObject.has(OFFICER.ROOM_PLAY_LIST)) {
                                    songList = parserListSongFromRoomChat(responseObject);
                                }
                                thread.setStateOnlineStar(responseObject.optInt(OFFICER.ROOM_STATE_ONLINE, 0));
                                thread.setDescSongStar(responseObject.optString(OFFICER.ROOM_DESC_SONG, null));
                                thread.setListSongStars(songList);
                                thread.setFollowStar(responseObject.optInt(OFFICER.ROOM_FOLLOW, -1));
                                //background
                                String background = responseObject.optString(OFFICER.ROOM_BACKGROUND, null);
                                mApplication.getMessageBusiness().checkAndUpdateBackground(thread, background);
                                mApplication.getMessageBusiness().notifyConfigRoomChange(roomId, false);
                                listener.onResponseFollowRoomChat(thread);
                            } else {
                                listener.onErrorFollow(errorCode, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onErrorFollow(-1, desc);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onErrorFollow(-1, null);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                long currentTime = TimeHelper.getCurrentTime();
                String encrypt = encryptDataRoomChat(account.getJidNumber(), account.getToken(), roomId, currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.DATA_ENCRYPT, encrypt);
                params.put(Constants.HTTP.REQ_TIME, String.valueOf(currentTime));
                params.put(Constants.HTTP.OFFICER.MEMBER_NAME, account.getName());
                params.put(Constants.HTTP.OFFICER.ROOM_ID, roomId);
                params.put(Constants.HTTP.OFFICER.JOIN_STATE, String.valueOf(joinState));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void leaveRoomChat(String id, final OfficerBusiness.RequestLeaveRoomChatListener listener) {
        if (!mAccountBusiness.isValidAccount() ||
                !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onErrorLeave(-1, null);
            return;
        }
        LeaveRoomAsyncTask asyncTask = new LeaveRoomAsyncTask(listener, id);
        asyncTask.execute();
    }

    private class LeaveRoomAsyncTask extends AsyncTask<Void, Void, IQ> {
        private OfficerBusiness.RequestLeaveRoomChatListener listener;
        private String roomId;

        public LeaveRoomAsyncTask(OfficerBusiness.RequestLeaveRoomChatListener listener, String id) {
            this.listener = listener;
            this.roomId = id;
        }

        @Override
        protected IQ doInBackground(Void... params) {
            try {
                IQInfo iqLeaveRoom = new IQInfo(IQInfo.NAME_SPACE_STAR_UN_FOLLOW);
                iqLeaveRoom.setType(IQ.Type.SET);
                iqLeaveRoom.setTo(roomId + Constants.XMPP.XMPP_ROOM_RESOUCE);
                return mApplication.getXmppManager().sendPacketThenWaitingResponse(iqLeaveRoom, false);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(IQ result) {
            super.onPostExecute(result);
            if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                Log.i(TAG, "leaveRoomChat --> success");
                mApplication.getMusicBusiness().processResponseLeaveRoom(roomId, false);
                listener.onResponseLeaveRoomChat();
            } else {
                Log.i(TAG, "leaveRoomChat --> noResponse");
                listener.onErrorLeave(-1, null);
            }
        }
    }

    /**
     * encryptData
     *
     * @param jidNumber
     * @param token
     * @param currentTime
     * @param roomId
     * @return
     */
    private String encryptDataRoomChat(String jidNumber, String token, String roomId, long currentTime) {
        String textToEncrypt = jidNumber + token + roomId + String.valueOf(currentTime);
        String md5Encrypt = EncryptUtil.encryptMD5(textToEncrypt);
        JSONObject data = new JSONObject();
        try {
            data.put(Constants.HTTP.REST_TOKEN, token);
            data.put(Constants.HTTP.MESSGE.MD5_STR, md5Encrypt);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
        return RSAEncrypt.getInStance(mApplication).encrypt(mApplication, data.toString());
    }

    /**
     * parser account
     *
     * @param jsonObject
     * @return
     * @throws JSONException
     */
    private ArrayList<OfficerAccount> parserOfficerAccountFromResponse(JSONObject jsonObject, boolean isSearch, boolean getListHome)
            throws JSONException {
        ArrayList<OfficerAccount> listOfficers = new ArrayList<>();
        ArrayList<OfficerAccount> listOfficerType0 = new ArrayList<>();
        ArrayList<OfficerAccount> listOfficerType1 = new ArrayList<>();
        if (jsonObject.has(Constants.HTTP.OFFICER.OFFICER_LIST)) {
            JSONArray jsonArray = jsonObject.getJSONArray(Constants.HTTP.OFFICER.OFFICER_LIST);
            if (jsonArray != null && jsonArray.length() > 0) {
                int lengthArray = jsonArray.length();
                for (int i = 0; i < lengthArray; i++) {
                    JSONObject objectOfficer = jsonArray.getJSONObject(i);
                    OfficerAccount officerAccount = new OfficerAccount();
                    officerAccount.setJsonObject(objectOfficer, OfficerAccountConstant.TYPE_OFFICER);
                    if (officerAccount.getOfficerType() == OfficerAccountConstant.OFFICER_TYPE_NORMAL) {
                        listOfficerType0.add(officerAccount);
                    } else if (officerAccount.getOfficerType() == OfficerAccountConstant.OFFICER_TYPE_BUSSINESS) {
                        listOfficerType1.add(officerAccount);
                    }

                }
            }
        }
        if (!listOfficerType0.isEmpty()) {
            listOfficers.addAll(listOfficerType0);
            if (!isSearch) {
                OfficerAccount officerAccount = new OfficerAccount();
                officerAccount.setOfficerType(OfficerAccountConstant.OFFICER_TYPE_DIVIDER);
                listOfficers.add(officerAccount);
            }
        }
        if (!listOfficerType1.isEmpty() && !getListHome) {
            listOfficers.addAll(listOfficerType1);
        }

        return listOfficers;
    }

    private ArrayList<MediaModel> parserListSongFromRoomChat(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = jsonObject.getJSONArray(Constants.HTTP.OFFICER.ROOM_PLAY_LIST);
        ArrayList<MediaModel> songList = new ArrayList<>();
        if (jsonArray != null && jsonArray.length() > 0) {
            int lengthArray = jsonArray.length();
            for (int i = 0; i < lengthArray; i++) {
                JSONObject songObject = jsonArray.getJSONObject(i);
                MediaModel songModel = new MediaModel();
                if (songObject.has(Constants.HTTP.STRANGER_MUSIC.SONG_ID)) {
                    songModel.setId(songObject.getString(Constants.HTTP.STRANGER_MUSIC.SONG_ID));
                }
                if (songObject.has(Constants.HTTP.STRANGER_MUSIC.SONG_NAME)) {
                    songModel.setName(songObject.getString(Constants.HTTP.STRANGER_MUSIC.SONG_NAME));
                }
                if (songObject.has(Constants.HTTP.STRANGER_MUSIC.SONG_SINGER)) {
                    songModel.setSinger(songObject.getString(Constants.HTTP.STRANGER_MUSIC.SONG_SINGER));
                }
                if (songObject.has(Constants.HTTP.STRANGER_MUSIC.SONG_IMAGE)) {
                    songModel.setImage(songObject.getString(Constants.HTTP.STRANGER_MUSIC.SONG_IMAGE));
                }
                if (songObject.has(Constants.HTTP.STRANGER_MUSIC.SONG_MEDIA_URL)) {
                    songModel.setMedia_url(songObject.getString(Constants.HTTP.STRANGER_MUSIC.SONG_MEDIA_URL));
                }
                if (songObject.has(Constants.HTTP.STRANGER_MUSIC.SONG_URL)) {
                    songModel.setUrl(songObject.getString(Constants.HTTP.STRANGER_MUSIC.SONG_URL));
                }
                songList.add(songModel);
            }
        }
        return songList;
    }

    public void reportRoomChat(ReengMessage message, final int reportType, final OfficerBusiness
            .RequestReportRoomListener listener) {
        if (!mAccountBusiness.isValidAccount() ||
                !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1);
            return;
        }
        final String reported = message.getSender();
        final String content = message.getPacketId() + "|" + message.getContent();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.REPORT_ROOM);
        Log.d(TAG, "url: " + url);
        Log.d(TAG, "content: " + content);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: reportRoomChat: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(reported).
                        append(reportType).
                        append(content).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.ROOM_CHAT.REPORTER, account.getJidNumber());
                params.put(Constants.HTTP.ROOM_CHAT.REPORTED, reported);
                params.put(Constants.HTTP.ROOM_CHAT.REPORTTYPE, String.valueOf(reportType));
                params.put(Constants.HTTP.ROOM_CHAT.REPORTCONTENT, content);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }
}
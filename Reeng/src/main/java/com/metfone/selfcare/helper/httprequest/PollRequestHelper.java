package com.metfone.selfcare.helper.httprequest;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.datasource.PollDataSource;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.PinMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.HTTP.POLL;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 8/25/2016.
 */
public class PollRequestHelper extends BaseApi {
    private static final String TAG = PollRequestHelper.class.getSimpleName();
    private static PollRequestHelper mInstance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private HashMap<String, PollObject> listPoll;
    private GetPollObjectLocalAsynctask pollAsynctask;

    public static synchronized PollRequestHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new PollRequestHelper(application);
        }
        return mInstance;
    }

    private PollRequestHelper(ApplicationController application) {
        super(application);
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        this.mRes = mApplication.getResources();
        listPoll = new HashMap<>();
    }

    private String getJsonFromArrayString(ArrayList<String> list) {
        if (list == null || list.isEmpty()) return "";
        JSONArray jsonArray = new JSONArray();
        for (String item : list) {
            jsonArray.put(item);
        }
        return jsonArray.toString();
    }

    private ArrayList<String> parserListSelected(JSONObject object) throws JSONException {
        JSONArray array = object.optJSONArray(Constants.HTTP.POLL.SELECTED);
        ArrayList<String> arrayList = new ArrayList<>();
        if (array != null && array.length() > 0) {
            int length = array.length();
            for (int i = 0; i < length; i++) {
                JSONObject item = array.getJSONObject(i);
                arrayList.add(item.optString(Constants.HTTP.POLL.POLL_OPTION_ID));
            }
        }
        return arrayList;
    }

    private PollObject parserPollObject(JSONObject dataObject) throws JSONException {
        PollObject pollObject = new PollObject();
        pollObject.setPollId(dataObject.optString(POLL.ID, ""));
        pollObject.setTitle(dataObject.optString(POLL.POLL_TITLE, ""));
        pollObject.setCreator(dataObject.optString(POLL.POLL_CREATOR, ""));
        pollObject.setChoice(dataObject.optInt(POLL.POLL_CHOICE, 1));
        pollObject.setAnonymous(dataObject.optInt(POLL.POLL_ANONYMOUS, 0));
        pollObject.setEnableAddVote(dataObject.optInt(POLL.POLL_ENABLE_ADD_VOTE, 0));
        pollObject.setCreatedAt(dataObject.optLong(POLL.POLL_CREATED_AT, System.currentTimeMillis()));
        pollObject.setExpireAt(dataObject.optLong(POLL.POLL_EXPIRE_AT, 0L));
        pollObject.setListSelected(parserListSelected(dataObject));
        int status = dataObject.optInt("pin", -1);
        pollObject.setPinStatus(status);
        int close = dataObject.optInt("close", -1);
        pollObject.setClose(close);
        JSONArray itemArray = dataObject.optJSONArray(POLL.POLL_OPTIONS);
        if (itemArray != null && itemArray.length() > 0) {
            int lengthArray = itemArray.length();
            for (int i = 0; i < lengthArray; i++) {
                JSONObject object = itemArray.getJSONObject(i);
                PollItem item = new PollItem();
                item.setJsonObject(object);
                pollObject.addPollItem(item);
            }
        }
        return pollObject;
    }

    public String convertPollObjectToString(PollObject pollObject) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(POLL.ID, pollObject.getPollId());
        jsonObject.put(POLL.POLL_TITLE, pollObject.getTitle());
        jsonObject.put(POLL.POLL_CREATOR, pollObject.getCreator());
        jsonObject.put(POLL.POLL_CHOICE, pollObject.getChoice());
        jsonObject.put(POLL.POLL_INCOGNITO, pollObject.getAnonymous());
        jsonObject.put(POLL.POLL_CREATED_AT, pollObject.getCreatedAt());
        jsonObject.put(POLL.POLL_EXPIRE_AT, pollObject.getExpireAt());

        ArrayList<PollItem> pollItems = pollObject.getListItems();
        if (pollItems.size() > 0) {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < pollItems.size(); i++) {
                jsonArray.put(pollItems.get(i).toJSonObject());
            }
            jsonObject.put(Constants.HTTP.POLL.POLL_OPTIONS, jsonArray);
        }

        ArrayList<String> listSelected = pollObject.getListSelected();
        if (listSelected.size() > 0) {
            JSONArray jsSelected = new JSONArray();
            for (int i = 0; i < listSelected.size(); i++) {
                JSONObject js = new JSONObject();
                js.put(Constants.HTTP.POLL.POLL_OPTION_ID, listSelected.get(i));
                jsSelected.put(listSelected.get(i));
            }
            jsonObject.put(Constants.HTTP.POLL.SELECTED, jsSelected);
        }
        return jsonObject.toString();
    }

    public void processPollObjectAfterRequestDetail(PollObject poll) {
        if (poll == null ||
                poll.getListItems() == null || poll.getListItems().isEmpty())
            return;
        boolean isCheckSelected = !(poll.getListSelected() == null || poll.getListSelected().isEmpty());
        for (PollItem item : poll.getListItems()) {
            if (isCheckSelected && poll.getListSelected().contains(item.getItemId())) {
                item.setSelected(true);
            } else {
                item.setSelected(false);
            }
        }
    }

    public PollObject parserPollObjectFromMessage(String pollDetail) {
        try {
            JSONObject responseObject = new JSONObject(pollDetail);
            return parserPollObject(responseObject);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public String getContentMessageCreatePoll(PollObject pollObject) {
        if (pollObject == null) return "";
        StringBuilder sb = new StringBuilder();
        sb.append(pollObject.getTitle()).append("\n\r");
        if (pollObject.getListItems() != null) {
            for (PollItem item : pollObject.getListItems()) {
                sb.append("\n\r").append("+ ").append(item.getTitle());
            }
        }
        return sb.toString();
    }

    public String getContentMessageActionPoll(PollObject pollObject, String member, boolean isSend, boolean isAdd) {
        Log.d(TAG, "getContentMessageActionPoll: " + pollObject.toString());
        String memberName;
        if (isSend) {
            memberName = mRes.getString(R.string.you);
        } else {
            memberName = getName(member, true);
        }
        if (pollObject.getListItems() != null && !pollObject.getListItems().isEmpty()) {
            if (isAdd) {
                boolean isMulti = false;
                ArrayList<PollItem> listAdd = pollObject.getListItems();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < listAdd.size(); i++) {
                    PollItem item = listAdd.get(i);
                    sb.append(item.getTitle());
                    if (listAdd.size() > 1 && i < (listAdd.size() - 1)) {
                        isMulti = true;
                        sb.append(", ");
                    }
                }
                if (isSend) {// minh vote thi insert "ban da them ..."
                    if (!isMulti)
                        return String.format(mRes.getString(R.string.poll_msg_add_item_vote), mRes.getString(R.string.you), sb.toString());
                    else
                        return String.format(mRes.getString(R.string.poll_msg_add_multi_item_vote), mRes.getString(R.string.you), sb.toString());
                } else {// ban be them vote
                    /*PollItem item = pollObject.getListItems().get(0);
                    ArrayList<String> memberVoted = item.getMemberVoted();
                    int numberVoted = item.getTotalVoted();*/
                    if (pollObject.getAnonymous() == 0) {
                        if (!isMulti)
                            return String.format(mRes.getString(R.string.poll_msg_add_item_vote), memberName, sb.toString());
                        else
                            return String.format(mRes.getString(R.string.poll_msg_add_multi_item_vote), memberName, sb.toString());
                    } else {
                        if (!isMulti)
                            return String.format(mRes.getString(R.string.poll_msg_add_item_vote), mRes.getString(R.string.poll_anonymous), sb.toString());
                        else
                            return String.format(mRes.getString(R.string.poll_msg_add_multi_item_vote), mRes.getString(R.string.poll_anonymous), sb.toString());
                    }
                }
            } else {
                if (pollObject.getChoice() == 1) {
                    PollItem item = pollObject.getListItems().get(0);
                    if (isSend) {// minh vote thi insert "ban da chon ..."
                        return String.format(mRes.getString(R.string.poll_msg_content), mRes.getString(R.string.you), item.getTitle());
                    } else {// ban be vote
                        ArrayList<String> memberVoted = item.getMemberVoted();
                        int numberVoted = item.getTotalVoted();
                        if (pollObject.getAnonymous() == 0)
                            return String.format(mRes.getString(R.string.poll_msg_content), getDetailMemberVoted(memberVoted, numberVoted), item.getTitle());
                        else
                            return String.format(mRes.getString(R.string.poll_msg_content), mRes.getString(R.string.poll_anonymous), item.getTitle());
                    }
                } else {
                    if (pollObject.getAnonymous() == 0)
                        return String.format(mRes.getString(R.string.poll_multi_msg_content), memberName, getDetailOptionVote(pollObject.getListItems()));
                    else
                        return String.format(mRes.getString(R.string.poll_multi_msg_content), mRes.getString(R.string.poll_anonymous), getDetailOptionVote(pollObject.getListItems()));
                }
            }
        } else {
            if (pollObject.getAnonymous() == 0)
                return String.format(mRes.getString(R.string.poll_msg_un_vote), memberName);
            else
                return String.format(mRes.getString(R.string.poll_msg_un_vote), mRes.getString(R.string.poll_anonymous));
        }
    }

    public String getContentPollActionFromStatus(int status, String member, String memberName, PollObject pollObject) {
        String content = "";
        if (status == PollObject.STATUS_PIN) {
            if (mApplication.getReengAccountBusiness().getJidNumber().equals(member)) {
                content = String.format(mApplication.getResources().getString(R.string.poll_pinned_survey)
                        , mApplication.getResources().getString(R.string.you));
            } else {
                String name;
                PhoneNumber p = mApplication.getContactBusiness().getPhoneNumberFromNumber(member);
                if (p != null) {
                    name = p.getName();
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(member);
                    if (nonContact != null)
                        name = nonContact.getNickName();
                    else
                        name = memberName;
                }
                if (TextUtils.isEmpty(name)) {
                    name = member;
                }
                content = String.format(mApplication.getResources().getString(R.string.poll_pinned_survey), name);
            }
        } else if (status == PollObject.STATUS_UNPIN) {
            if (mApplication.getReengAccountBusiness().getJidNumber().equals(member)) {
                content = String.format(mApplication.getResources().getString(R.string.poll_unpinned_survey)
                        , mApplication.getResources().getString(R.string.you));
            } else {
                String name;
                PhoneNumber p = mApplication.getContactBusiness().getPhoneNumberFromNumber(member);
                if (p != null) {
                    name = p.getName();
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(member);
                    if (nonContact != null)
                        name = nonContact.getNickName();
                    else
                        name = memberName;
                }
                if (TextUtils.isEmpty(name)) {
                    name = member;
                }
                content = String.format(mApplication.getResources().getString(R.string.poll_unpinned_survey), name);
            }
        } else if (status == PollObject.STATUS_PUSH_TOP) {
            if (mApplication.getReengAccountBusiness().getJidNumber().equals(member)) {
                content = String.format(mApplication.getResources().getString(R.string.poll_pushed_to_top_msg)
                        , mApplication.getResources().getString(R.string.you));
            } else {
                String name;
                PhoneNumber p = mApplication.getContactBusiness().getPhoneNumberFromNumber(member);
                if (p != null) {
                    name = p.getName();
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(member);
                    if (nonContact != null)
                        name = nonContact.getNickName();
                    else
                        name = memberName;
                }
                if (TextUtils.isEmpty(name)) {
                    name = member;
                }
                content = String.format(mApplication.getResources().getString(R.string.poll_pushed_to_top_msg), name);
            }
        } else if (status == PollObject.STATUS_CLOSE) {
            if (mApplication.getReengAccountBusiness().getJidNumber().equals(member)) {
                content = String.format(mApplication.getResources().getString(R.string.poll_closed_survey)
                        , mApplication.getResources().getString(R.string.you));
            } else {
                String name;
                PhoneNumber p = mApplication.getContactBusiness().getPhoneNumberFromNumber(member);
                if (p != null) {
                    name = p.getName();
                } else {
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(member);
                    if (nonContact != null)
                        name = nonContact.getNickName();
                    else
                        name = memberName;
                }
                if (TextUtils.isEmpty(name)) {
                    name = member;
                }
                content = String.format(mApplication.getResources().getString(R.string.poll_closed_survey), name);
            }
        }
        return content;
    }

    private String getDetailOptionVote(ArrayList<PollItem> pollOptions) {
        if (pollOptions == null || pollOptions.isEmpty()) return "";
        StringBuilder sb = new StringBuilder();
        sb.append(pollOptions.get(0).getTitle());
        int size = pollOptions.size();
        if (size > 1) {
            for (int i = 1; i < size; i++) {
                sb.append(", ").append(pollOptions.get(i).getTitle());
            }
        }
        return sb.toString();
    }

    private String getDetailMemberVoted(ArrayList<String> memberVoted, int numberVoted) {
        if (numberVoted <= 0 || memberVoted.size() <= 0) return "";
        StringBuilder sb = new StringBuilder();
        sb.append(getName(memberVoted.get(0), true));
        if (numberVoted >= 2) {
            int others;
            if (memberVoted.size() > 1) {
                String secondName = getName(memberVoted.get(1), false);
                if (numberVoted == 2) {
                    sb.append(" ").append(mRes.getString(R.string.text_and)).append(" ").append(secondName);
                } else {
                    sb.append(", ").append(secondName).append(" ").append(mRes.getString(R.string.text_and)).append(" ");
                    others = numberVoted - 2;
                    sb.append(others).append(" ");
                    if (others == 1) {
                        sb.append(mRes.getString(R.string.onmedia_other));
                    } else {
                        sb.append(mRes.getString(R.string.onmedia_others));
                    }
                }
            } else {
                others = numberVoted - 1;
                sb.append(" ").append(mRes.getString(R.string.text_and)).append(" ").append(others).append(" ");
                if (others == 1) {
                    sb.append(mRes.getString(R.string.onmedia_other));
                } else {
                    sb.append(mRes.getString(R.string.onmedia_others));
                }
            }
        }
        return sb.toString();
    }

    private String getName(String number, boolean first) {
        if (number.equals(mAccountBusiness.getJidNumber())) {
            if (first)
                return mRes.getString(R.string.you);
            else
                return mRes.getString(R.string.you).toLowerCase();
        } else {
            PhoneNumber numberFirst = mApplication.getContactBusiness().getPhoneNumberFromNumber(number);
            if (numberFirst != null) {
                return numberFirst.getName();
            } else {
                NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(number);
                if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                    return nonContact.getNickName();
                } else {
                    return number;
                }
            }
        }
    }

    //    http://hlvip2.mocha.com.vn:80/ReengBackendBiz/poll/v1/create

    public void createPollV3(final ThreadMessage threadMessage,
                             final String title,
                             final ArrayList<String> listOption,
                             final String choice,
                             final String anonymous,
                             final String enableAddVote,
                             final String expireAt,
                             final int pin,
                             final ApiCallbackV2<PollObject> apiCallback) {

        final String groupId = threadMessage.getServerId();
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

        String optionJson = application.getGson().toJson(listOption);
        String textPin = Config.Features.FLAG_SUPPORT_PIN_VOTE ? String.valueOf(pin) : "";

//        md5 = msisdn + groupId + title + anonymous + enableAddVote + expireTime + choice + optionJson + clientType + revision
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + groupId
                + title
                + anonymous
                + enableAddVote
                + expireAt
                + choice
                + optionJson
                + textPin
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        String url = "";
        if (Config.Features.FLAG_SUPPORT_PIN_VOTE)
            url = "/ReengBackendBiz/poll/create/v3";
        else
            url = "/ReengBackendBiz/poll/create/v2";

        post(domainFile + url)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("groupId", groupId)
                .putParameter("title", title)
                .putParameter("anonymous", anonymous)
                .putParameter("enableAddVote", enableAddVote)
                .putParameter("expireAt", expireAt)
                .putParameter("choice", choice)
                .putParameter("options", optionJson)
                .putParameter("pin", String.valueOf(pin))
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws JSONException {
                        Log.d(TAG, "onResponse: createPoll: " + response);
                        int errorCode = -1;
                        JSONObject responseObject = new JSONObject(response);
                        errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                        JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                        if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                            PollObject poll = parserPollObject(dataObject);
                            insertOrUpdatePollObject(poll.getPollId(), dataObject.toString(), -1);

                            if (pin == PollObject.STATUS_PIN) {
                                PinMessage pin = new PinMessage();
                                pin.setContent(poll.getTitle());
                                pin.setTitle(poll.getCreator());
                                pin.setTarget(poll.getPollId());
                                pin.setType(PinMessage.TypePin.TYPE_VOTE.VALUE);
                                threadMessage.setPinMessage(pin);
                                mApplication.getMessageBusiness().updateThreadMessage(threadMessage);
                            } else if (pin == PollObject.STATUS_UNPIN && threadMessage.getPinMessage() != null
                                    && poll.getPollId().equals(threadMessage.getPinMessage().getTarget())) {
                                threadMessage.setPinMessage("");
                                mApplication.getMessageBusiness().updateThreadMessage(threadMessage);
                            }

                            apiCallback.onSuccess("", poll);
                        } else if (errorCode != 0) {
                            if (apiCallback != null) apiCallback.onError(String.valueOf(errorCode));
                        } else {
                            if (apiCallback != null) apiCallback.onError("");
                        }

                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) apiCallback.onComplete();
                    }
                }).execute();
    }


    public void setStatusPoll(final ThreadMessage threadMessage,
                              final String pollId,
                              final int statusType,
                              final ApiCallbackV2<PollObject> apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();


//        md5 = msisdn + groupId + title + anonymous + enableAddVote + expireTime + choice + optionJson + clientType + revision
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + pollId
                + statusType
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        post(domainFile + "/ReengBackendBiz/poll/setStatusPoll/v1")
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("pollId", pollId)
                .putParameter("statusType", String.valueOf(statusType))
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws JSONException {
                        Log.d(TAG, "onResponse: createPoll: " + response);
                        int errorCode = -1;
                        JSONObject responseObject = new JSONObject(response);
                        errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                        JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                        if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                            PollObject poll = parserPollObject(dataObject);
                            insertOrUpdatePollObject(poll.getPollId(), dataObject.toString(), -1);

                            if (statusType == PollObject.STATUS_PIN) {
                                PinMessage pin = new PinMessage();
                                pin.setContent(poll.getTitle());
                                pin.setTitle(poll.getCreator());
                                pin.setTarget(poll.getPollId());
                                pin.setType(PinMessage.TypePin.TYPE_VOTE.VALUE);
                                threadMessage.setPinMessage(pin);
                                mApplication.getMessageBusiness().updateThreadMessage(threadMessage);
                            } else if (statusType == PollObject.STATUS_UNPIN && threadMessage.getPinMessage() != null
                                    && poll.getPollId().equals(threadMessage.getPinMessage().getTarget())) {
                                threadMessage.setPinMessage("");
                                mApplication.getMessageBusiness().updateThreadMessage(threadMessage);
                            }

                            apiCallback.onSuccess("", poll);
                        } else if (errorCode != 0) {
                            if (apiCallback != null) apiCallback.onError(String.valueOf(errorCode));
                        } else {
                            if (apiCallback != null) apiCallback.onError("");
                        }

                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) apiCallback.onComplete();
                    }
                }).execute();
    }

    public void addMoreV2(final String groupId,
                          final String pollId,
                          final ArrayList<String> listOption,
                          final ArrayList<String> newOption,
                          final ApiCallbackV2<PollObject> apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

        String optionJson = application.getGson().toJson(listOption);
        String newOptions = application.getGson().toJson(newOption);

//        md5 = msisdn + groupId + title + anonymous + enableAddVote + expireTime + choice + optionJson + clientType + revision
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + pollId
                + groupId
                + optionJson
                + newOptions
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        post(domainFile + "/ReengBackendBiz/poll/addMore/V2")
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("pollId", pollId)
                .putParameter("groupId", groupId)
                .putParameter("options", optionJson)
                .putParameter("newOptions", newOptions)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        int errorCode;
                        JSONObject responseObject = new JSONObject(data);
                        errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                        JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                        if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                            PollObject poll = parserPollObject(dataObject);
                            insertOrUpdatePartOrGetPollObject(poll.getPollId(), dataObject.toString(),
                                    -1, mApplication.getReengAccountBusiness().getJidNumber(), true);
                            apiCallback.onSuccess("", poll);
                        } else if (errorCode != 0) {
                            if (apiCallback != null) apiCallback.onError(String.valueOf(errorCode));
                        } else {
                            if (apiCallback != null) apiCallback.onError("");
                        }

                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) apiCallback.onComplete();
                    }
                }).execute();
    }

    public void findByIdV2(final String pollId,
                           final ApiCallbackV2<PollObject> apiCallback,
                           final int threadId) {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

        /*
         * -msisdn: String
         * -pollId: String
         * -clientType: String
         * -revision: String
         * -timestamp: long
         * -security: String
         * md5=msisdn + pollId + clientType + revision
         */
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + pollId
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        get(domainFile + "/ReengBackendBiz/poll/find/byId/v2")
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("pollId", pollId)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        int errorCode;
                        JSONObject responseObject = new JSONObject(data);
                        errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                        JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                        if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                            PollObject poll = parserPollObject(dataObject);
                            insertOrUpdatePollObject(poll.getPollId(), dataObject.toString(), threadId);
                            if (apiCallback != null)
                                apiCallback.onSuccess("", poll);
                        } else {
                            if (apiCallback != null) apiCallback.onError("");
                        }

                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) apiCallback.onComplete();
                    }
                }).execute();
    }

    public void voteV2(final String pollId,
                       final ArrayList<String> newOptionIds,
                       final ArrayList<String> oldOptionIds,
                       final ApiCallbackV2<PollObject> apiCallback,
                       final int threadId) {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

        String newOptionIdsJson = ApplicationController.self().getGson().toJson(newOptionIds);
        String oldOptionIdsJson = ApplicationController.self().getGson().toJson(oldOptionIds);
        /*
         * -msisdn: String
         * -pollId: String
         * -optionNewIds: String
         * -optionOldIds: String
         * //
         * -clientType: String
         * -revision: String
         * -timestamp: long
         * -security: String
         * md5=msisdn + pollId + optionNewIds + optionOldIds + clientType + revision
         */
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + pollId
                + newOptionIdsJson
                + oldOptionIdsJson
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        post(domainFile + "/ReengBackendBiz/poll/vote/v2")
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("pollId", pollId)
                .putParameter("optionNewIds", newOptionIdsJson)
                .putParameter("optionOldIds", oldOptionIdsJson)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        int errorCode;
                        JSONObject responseObject = new JSONObject(data);
                        errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                        JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                        if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                            PollObject poll = parserPollObject(dataObject);
                            insertOrUpdatePartOrGetPollObject(poll.getPollId(), dataObject.toString(),
                                    threadId, mApplication.getReengAccountBusiness().getJidNumber(), false);
                            apiCallback.onSuccess("", poll);
                        } else if (errorCode != 0) {
                            if (apiCallback != null) apiCallback.onError(String.valueOf(errorCode));
                        } else {
                            if (apiCallback != null) apiCallback.onError("");
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) apiCallback.onComplete();
                    }
                }).execute();
    }

    public void createPoll(final String groupId, final String title, final ArrayList<String> listOption,
                           final int choice, final PollRequestListener listener) {
        if (!mAccountBusiness.isValidAccount() || !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.POLL_CREATE);
        Log.d(TAG, "createPoll url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: createPoll: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                            JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                            if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                                PollObject poll = parserPollObject(dataObject);
                                listener.onSuccess(poll);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String options = getJsonFromArrayString(listOption);
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(title).
                        append(choice).
                        append(groupId).
                        append(options).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(POLL.POLL_TITLE, title);
                params.put(POLL.POLL_CHOICE, String.valueOf(choice));
                params.put(POLL.POLL_GROUP_ID, groupId);
                params.put(POLL.POLL_OPTIONS, options);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    //    http://hlvip2.mocha.com.vn:80/ReengBackendBiz/poll/v1/update
    public void updatePoll(final String groupId, final String pollId, final String title, final ArrayList<String> listOption,
                           final int choice, final PollRequestListener listener) {
        if (!mAccountBusiness.isValidAccount() || !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1);
            return;
        }
        String url = UrlConfigHelper.getInstance(ApplicationController.self()).getDomainFile() + "/ReengBackendBiz/poll/v1/update";
        Log.d(TAG, "updatePoll url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: updatePoll: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                            JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                            if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                                PollObject poll = parserPollObject(dataObject);
                                insertOrUpdatePartOrGetPollObject(pollId, dataObject.toString(),
                                        -1, mApplication.getReengAccountBusiness().getJidNumber(), false);
                                listener.onSuccess(poll);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String options = getJsonFromArrayString(listOption);
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(pollId).
                        append(title).
                        append(choice).
                        append(groupId).
                        append(options).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(POLL.POLL_ID, pollId);
                params.put(POLL.POLL_TITLE, title);
                params.put(POLL.POLL_CHOICE, String.valueOf(choice));
                params.put(POLL.POLL_GROUP_ID, groupId);
                params.put(POLL.POLL_OPTIONS, options);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void votePoll(final String pollId, final ArrayList<String> newOptionIds,
                         final ArrayList<String> oldOptionIds, final PollRequestListener listener, final int threadId) {
        if (!mAccountBusiness.isValidAccount() || !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.POLL_VOTE);
        Log.d(TAG, "votePoll url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: votePoll: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                            JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                            if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                                PollObject poll = parserPollObject(dataObject);
                                insertOrUpdatePartOrGetPollObject(poll.getPollId(), dataObject.toString(),
                                        threadId, mApplication.getReengAccountBusiness().getJidNumber(), false);
                                listener.onSuccess(poll);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String newIds = getJsonFromArrayString(newOptionIds);
                String oldIds = getJsonFromArrayString(oldOptionIds);
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(pollId).
                        append(newIds).
                        append(oldIds).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(POLL.POLL_ID, pollId);
                params.put(POLL.POLL_NEW_IDS, newIds);
                params.put(POLL.POLL_OLD_IDS, oldIds);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void getPollDetail(String pollId, final PollRequestListener listener, final int threadId) {
        if (!mAccountBusiness.isValidAccount() || !NetworkHelper.isConnectInternet(mApplication)) {
            if (listener != null)
                listener.onError(-1);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(pollId).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.POLL_GET_DETAIL),
                HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()),
                HttpHelper.EncoderUrl(pollId),
                String.valueOf(currentTime),
                encrypt);
        Log.d(TAG, "getPollDetail url: " + url);
        // request
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: getPollDetail: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                            JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                            if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                                PollObject poll = parserPollObject(dataObject);
                                insertOrUpdatePollObject(poll.getPollId(), dataObject.toString(), threadId);
                                if (listener != null)
                                    listener.onSuccess(poll);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            if (listener != null)
                                listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                if (listener != null)
                    listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void getPollItemDetail(String pollId, String itemId, final PollRequestListener listener) {
        if (!mAccountBusiness.isValidAccount() || !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        ArrayList<String> temp = new ArrayList<>();
        temp.add(itemId);
        String itemIds = getJsonFromArrayString(temp);
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(pollId).
                append(itemIds).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.POLL_GET_ITEM_DETAIL),
                HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()),
                HttpHelper.EncoderUrl(pollId),
                HttpHelper.EncoderUrl(itemIds),
                String.valueOf(currentTime),
                encrypt);
        Log.d(TAG, "getPollItemDetail url: " + url);
        // request
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: getPollItemDetail: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                            JSONObject dataObject = responseObject.optJSONObject(POLL.POLL_DATA);
                            if (errorCode == HTTPCode.E200_OK && dataObject != null) {
                                PollObject poll = parserPollObject(dataObject);
                                listener.onSuccess(poll);
                            } else {
                                listener.onError(-1);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public PollObject getPollObjectLocal(String id, int threadId) {
        if (TextUtils.isEmpty(id)) return null;
        if (!listPoll.isEmpty()) {
            if (listPoll.containsKey(id)) return listPoll.get(id);
        }

        if (pollAsynctask != null) {
            pollAsynctask.cancel(true);
        }
        pollAsynctask = new GetPollObjectLocalAsynctask(threadId);
        pollAsynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id);
        return null;
    }

    @SuppressLint("StaticFieldLeak")
    public void insertOrUpdatePollObject(String serverId, String content, final int threadId) {
        new AsyncTask<String, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(String... voids) {
                String id = voids[0];
                String contentSv = voids[1];
                if (TextUtils.isEmpty(id)) return false;
                if (!listPoll.isEmpty() && listPoll.containsKey(id)) {
                    boolean updateSuccess = PollDataSource.getInstance(mApplication).updatePollObject(id, contentSv);
                    if (updateSuccess) {
                        listPoll.put(id, parserPollObjectFromMessage(contentSv));
                    }
                    return updateSuccess;
                } else {
                    if (PollDataSource.getInstance(mApplication).isExistPollId(id)) {
                        boolean updateSuccess = PollDataSource.getInstance(mApplication).updatePollObject(id, contentSv);
                        if (updateSuccess) {
                            listPoll.put(id, parserPollObjectFromMessage(contentSv));
                        }
                        return updateSuccess;
                    } else {
                        boolean insertSuccess = PollDataSource.getInstance(mApplication).insertPollObject(id, contentSv);
                        if (insertSuccess) {
                            listPoll.put(id, parserPollObjectFromMessage(contentSv));
                        }
                        return insertSuccess;
                    }
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    mApplication.getMessageBusiness().refreshThreadWithoutNewMessage(threadId);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverId, content);

    }

    //update them 1 ph?n
    @SuppressLint("StaticFieldLeak")
    public void insertOrUpdatePartOrGetPollObject(final String serverId, String content,
                                                  final int threadId, final String from,
                                                  final boolean isAdd) {
        new AsyncTask<String, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(String... voids) {
                String id = voids[0];
                String contentSv = voids[1];
                PollObject pollObjServer = parserPollObjectFromMessage(contentSv);
                if (TextUtils.isEmpty(id) || pollObjServer == null) return false;
                PollObject afterMer = null;
                //l?y trong mem ra tru?c
                if (!listPoll.isEmpty() && listPoll.containsKey(id)) {
                    PollObject pMem = listPoll.get(id);
                    if (pMem != null)
                        afterMer = mergerPollObject(from, pollObjServer, pMem, isAdd);
                } else {    //l?y trong db ra
                    PollObject pDB = PollDataSource.getInstance(mApplication).findPollObject(id);
                    if (pDB != null)
                        afterMer = mergerPollObject(from, pollObjServer, pDB, isAdd);
                }
                if (afterMer != null) {
                    boolean updateSuccess = false;
                    try {
                        updateSuccess = PollDataSource.getInstance(mApplication).updatePollObject(afterMer.getPollId(), convertPollObjectToString(afterMer));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (updateSuccess) {
                        listPoll.put(id, afterMer);
                    }
                    return updateSuccess;
                } else {
                    return false;
                }


                /*if (!listPoll.isEmpty() && listPoll.containsKey(id)) {
                    boolean updateSuccess = PollDataSource.getInstance(mApplication).updatePollObject(id, contentSv);
                    if (updateSuccess) {
                        listPoll.put(id, parserPollObjectFromMessage(contentSv));
                    }
                    return updateSuccess;
                } else {
                    if (PollDataSource.getInstance(mApplication).isExistPollId(id)) {
                        boolean updateSuccess = PollDataSource.getInstance(mApplication).updatePollObject(id, contentSv);
                        if (updateSuccess) {
                            listPoll.put(id, parserPollObjectFromMessage(contentSv));
                        }
                        return updateSuccess;
                    } else {
                        boolean insertSuccess = PollDataSource.getInstance(mApplication).insertPollObject(id, contentSv);
                        if (insertSuccess) {
                            listPoll.put(id, parserPollObjectFromMessage(contentSv));
                        }
                        return insertSuccess;
                    }
                }*/
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    mApplication.getMessageBusiness().refreshThreadWithoutNewMessage(threadId);
                } else {
                    //TODO get info from sv
                    findByIdV2(serverId, null, threadId);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverId, content);

    }

    public PollObject mergerPollObject(String from, PollObject pollServer, PollObject pollLocal, boolean isAdd) {
        if (isAdd) {
            if (pollServer.getListItems() != null)
                pollLocal.addListPollItem(pollServer.getListItems());
        } else {
            CopyOnWriteArrayList<PollItem> listAdd = new CopyOnWriteArrayList<>();
            ArrayList<PollItem> listPollItemServer = pollServer.getListItems();

            for (PollItem pL : pollLocal.getListItems()) {
                boolean needRemove = true;
                for (PollItem pS : listPollItemServer) {
                    if (pS.getItemId().equals(pL.getItemId())) {
                        listAdd.add(pS);
                        needRemove = false;
                        break;
                    }
                }
                if (needRemove) {
                    ArrayList<String> listVoter = pL.getMemberVoted();
                    listVoter.remove(from);
                    listAdd.add(pL);
                }
            }


            /*for (PollItem pL : pollLocal.getListItems()) {
                boolean isExist = false;
                for (PollItem pS : listPollItemServer) {
                    if (pL.getItemId().equals(pS.getItemId())) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    boolean addItem = true;
                    for (String voter : pL.getMemberVoted()) {
                        if (voter.equals(from)) {
                            addItem = false;
                            break;
                        }
                    }
                    if (!addItem) {
                        pL.getMemberVoted().remove(from);
                    }
                    pL.setTotalVoted(pL.getMemberVoted().size());
                    listAdd.add(pL);
                }
            }
            listAdd.addAll(listPollItemServer);*/
            pollLocal.setListItems(new ArrayList<PollItem>(listAdd));
        }
        pollLocal.setPinStatus(pollServer.getPinStatus());
        pollLocal.setClose(pollServer.getClose());
        return pollLocal;
    }

    public void insertPollObject(String pollId, String voteDetail) {
        PollObject poll = parserPollObjectFromMessage(voteDetail);
        listPoll.put(pollId, poll);
        PollDataSource.getInstance(mApplication).insertPollObject(pollId, voteDetail);
    }

    @SuppressLint("StaticFieldLeak")
    private class GetPollObjectLocalAsynctask extends AsyncTask<String, Void, PollObject> {

        int threadId;
        String id;

        public GetPollObjectLocalAsynctask(int threadId) {
            this.threadId = threadId;
        }

        @Override
        protected PollObject doInBackground(String... strings) {
            id = strings[0];
            return PollDataSource.getInstance(mApplication).findPollObject(id);
        }

        @Override
        protected void onPostExecute(PollObject pollObject) {
            super.onPostExecute(pollObject);
            if (pollObject != null) {
                Log.i(TAG, "GetPollObjectLocalAsynctask success");
                listPoll.put(pollObject.getPollId(), pollObject);
                mApplication.getMessageBusiness().refreshThreadWithoutNewMessage(threadId);
            } else {
                Log.i(TAG, "GetPollObjectLocalAsynctask from network");
                findByIdV2(id, null, threadId);
            }
        }
    }

    public interface PollRequestListener {
        void onError(int errorCode);

        void onSuccess(PollObject poll);
    }
}
package com.metfone.selfcare.helper.workmanager;

import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.util.Log;

import java.util.concurrent.TimeUnit;

import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class SettingWorkManager {

    public static void startWorkOnTimeNotiSetting(long time) {
        if (!Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION) return;
        if (time < 0) return;
        WorkManager.getInstance().cancelAllWorkByTag(SettingNotiWork.TAG);
        OneTimeWorkRequest notiWork = new OneTimeWorkRequest.Builder(SettingNotiWork.class).setInitialDelay(time, TimeUnit.MILLISECONDS).addTag(SettingNotiWork.TAG).build();
        WorkManager.getInstance().enqueue(notiWork);
    }

    public static void cancelSettingNotiWork() {
        if (!Config.Features.FLAG_SUPPORT_ON_OFF_NOTIFICATION) return;
        try {
            WorkManager.getInstance().cancelAllWorkByTag(SettingNotiWork.TAG);
        } catch (Exception e) {
            Log.e("SettingWorkManager", "cancelSettingNotiWork", e);
        }
    }
}

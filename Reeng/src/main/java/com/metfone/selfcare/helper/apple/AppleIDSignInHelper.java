package com.metfone.selfcare.helper.apple;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.metfone.selfcare.helper.google.GoogleSignInHelper;
import com.metfone.selfcare.module.keeng.utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import lombok.val;

public class AppleIDSignInHelper {
    private SharedPref sharedPref;
    private static AppleIDSignInHelper appleIDSignInHelper;
    private AppCompatActivity mActivity;

    public static AppleIDSignInHelper newInstance(AppCompatActivity mActivity) {
        if (appleIDSignInHelper == null) {
            appleIDSignInHelper = new AppleIDSignInHelper(mActivity);
        }
        return appleIDSignInHelper;
    }

    public AppleIDSignInHelper(AppCompatActivity mActivity) {
        this.mActivity = mActivity;
        sharedPref = new SharedPref(mActivity);
    }

    public void requestForAccessToken(String code, String clientSecret) {
        String grantType = "authorization_code";

        String postParamsForAuth =
                "grant_type=" + grantType + "&code=" + code + "&redirect_uri=" + AppleConstants.REDIRECT_URI + "&client_id=" + AppleConstants.CLIENT_ID + "&client_secret=" + clientSecret;

        URL url = null;
        try {
            url = new URL(AppleConstants.TOKENURL);
            HttpsURLConnection httpsURLConnection =(HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setRequestProperty(
                    "Content-Type",
                    "application/x-www-form-urlencoded"
            );
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            outputStreamWriter.write(postParamsForAuth);
            outputStreamWriter.flush();
            BufferedReader br;
            StringBuilder sb = new StringBuilder();

            br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
            String strCurrentLine;
            while ((strCurrentLine = br.readLine()) != null) {
                sb.append(strCurrentLine);
            }

            String response = sb.toString();  // defaults to UTF-8

            JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();

            String accessToken = jsonObject.getString("access_token");//Here is the access token
            Log.i("Apple Access Token is: ", accessToken);

            int expiresIn = jsonObject.getInt("expires_in"); //When the access token expires
            Log.i("expires in: ", String.valueOf(expiresIn));

            String refreshToken =
                    jsonObject.getString("refresh_token"); // The refresh token used to regenerate new access tokens. Store this token securely on your server.
            Log.i("refresh token: ", refreshToken);
            // Save the RefreshToken Token (refreshToken) using SharedPreferences
            // This will allow us to verify if refresh Token is valid every time they open the app after cold start.


            String idToken =
                    jsonObject.getString("id_token"); // A JSON Web Token that contains the user’s identity information.
            Log.i("ID Token: ", idToken);
            sharedPref.putString("id_token", idToken);

            // Get encoded user id by splitting idToken and taking the 2nd piece
            String encodedUserID = idToken.split(".")[1];

            //Decode encodedUserID to JSON
            String decodedUserData = new String(Base64.decode(encodedUserID, Base64.DEFAULT));
            JSONObject userDataJsonObject = new JSONObject(decodedUserData);
            // Get User's ID
            String userId = userDataJsonObject.getString("sub");
            Log.i("Apple User ID :", userId);
//            listener.onGetInfoAppleIdFinish(userId);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


    }

    public boolean isLoggedIn(){
        val expireTime = sharedPref.getLong("verify_refresh_token_timer", 0);

        val currentTime = System.currentTimeMillis() / 1000L; // Check the current Unix Time

        if (currentTime >= expireTime) {
            // After 24 hours validate the Refresh Token and generate new Access Token
            long untilUnixTime =
                    currentTime + (60 * 60 * 24); // Execute the method after 24 hours again
            sharedPref.putLong("verify_refresh_token_timer", untilUnixTime);
            return verifyRefreshToken();
        } else {
            return true;
        }
    }

    private boolean verifyRefreshToken(){
        // Verify Refresh Token only once a day
        String refreshToken = sharedPref.getString("refresh_token", "");
        String clientSecret = sharedPref.getString("client_secret", "");

        String postParamsForAuth =
                "grant_type=refresh_token" + "&client_id=" + AppleConstants.CLIENT_ID + "&client_secret=" + clientSecret + "&refresh_token=" + refreshToken;

        URL url;
        try {
            url = new URL(AppleConstants.TOKENURL);
            HttpsURLConnection httpsURLConnection =  (HttpsURLConnection) url.openConnection() ;
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setRequestProperty(
                    "Content-Type",
                    "application/x-www-form-urlencoded"
            );
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            outputStreamWriter.write(postParamsForAuth);
            outputStreamWriter.flush();
            BufferedReader br;
            StringBuilder sb = new StringBuilder();

            br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
            String strCurrentLine;
            while ((strCurrentLine = br.readLine()) != null) {
                sb.append(strCurrentLine);
            }

            String response = sb.toString();  // defaults to UTF-8
            JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
            String newAccessToken = jsonObject.getString("access_token");
            //Replace the Access Token on your server with the new one
            Log.d("New Access Token: ", newAccessToken);
            return true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            Log.e(
                    "ERROR: ",
                    "Refresh Token has expired or user revoked app credentials"
            );
            return  false;
        }

    }

    public SharedPref getSharedPref() {
        return sharedPref;
    }

    public interface OnAppleIdListener {
        void onGetInfoAppleIdFinish(String firstName, String middleName, String lastName, String email);
    }

}

package com.metfone.selfcare.helper.ads;

import android.view.View;
import android.widget.FrameLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.vtm.adslib.AdsListener;

public class AdsHolder extends BaseViewHolder {
    private FrameLayout layout_ads;

    public AdsHolder(View itemView) {
        super(itemView);
        layout_ads = itemView.findViewById(R.id.layout_ads);
    }

    @Override
    public void setElement(Object obj) {
        if(layout_ads != null && layout_ads.getVisibility() == View.GONE)
        {
            AdsManager.getInstance().showAdsBanner(layout_ads, new AdsListener() {
                @Override
                public void onAdClosed() {

                }

                @Override
                public void onAdShow() {

                }
            });
        }
    }
}

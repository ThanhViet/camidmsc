package com.metfone.selfcare.helper;

import android.content.Context;

/**
 * Created by toanvk2 on 23/07/2015.
 */
public class ConfigProperties extends AssetsProperties {

    @Property("MCH")
    String distribution;

    public ConfigProperties(Context context) {
        super(context);
    }

    public ConfigProperties(Context context, String fileName) {
        super(context, fileName);
    }
}
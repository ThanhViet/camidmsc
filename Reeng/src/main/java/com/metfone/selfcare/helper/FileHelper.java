package com.metfone.selfcare.helper;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by thanhnt72 on 09/04/2015.
 */
public class FileHelper {

    public static final String TAG = FileHelper.class.getSimpleName();

    public static int getDurationMediaFile(Context mContext, String filePath) {
        int duration = 0;
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            int tmp = MediaPlayer.create(mContext, FileHelper.fromFile((ApplicationController) mContext
                    .getApplicationContext(), new File(filePath))).getDuration();
            duration = Math.round(tmp / 1000.0f);
            mediaPlayer.release();
            Log.i(TAG, " duration: " + duration);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return duration; //
    }

    public static int getSizeOfFile(String filePath) {
        File file = new File(filePath);
        if (file.isFile()) {
            return (int) file.length();
        }
        return 0;
    }

    public static float getSizeInMbFromByte(long sizeInByte) {
        return (float) (sizeInByte / (1024.0 * 1024.0));
    }

    public static String formatFileSize(long sizeInByte) {
        String strFileSizeFormat = "%.1f %s";
        if (sizeInByte < Constants.FILE.ONE_KILOBYTE) {
            return sizeInByte + " B";
        } else if (sizeInByte < Constants.FILE.ONE_MEGABYTE) {
            return String.format(strFileSizeFormat, (float) (sizeInByte / 1024.0), "KB");
        } else {
            return String.format(strFileSizeFormat, (float) (sizeInByte / (1024.0 * 1024.0)), "MB");
        }
    }

    public static String getVideoContentUri(Context context, String filePath) {
        Log.i(TAG, "filePath: " + filePath);
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Video.Media._ID},
                MediaStore.Video.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Log.i(TAG, "id: " + id);
            cursor.close();
            return "content://media/external/video/media/" + id;
        }
        return null;
    }

    public static ImageInfo getMediaInfo(Context context, String filePath) {
        final String[] columns = {
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.SIZE,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.DATE_ADDED,
                MediaStore.Video.Media.RESOLUTION,
                MediaStore.Video.Media._ID,};

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                columns,
                MediaStore.Video.Media.DATA + "=? ",
                new String[]{filePath},
                null);
        if (cursor != null && cursor.moveToFirst()) {
            try {
                final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                final int dateColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED);
                int sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE);
                int durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);
                int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
                int resColum = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.RESOLUTION);

                String data = cursor.getString(dataColumn);
                long date = cursor.getLong(dateColumn);
                long size = cursor.getLong(sizeColumn);
                long duration = cursor.getLong(durationColumn);
                int durationInSecond = Math.round(duration / 1000.0f);
                int id = cursor.getInt(idColumn);
                String videoContentURI = "content://media/external/video/media/" + id;
                String res = cursor.getString(resColum);

                return new ImageInfo(data, size, durationInSecond, videoContentURI, date, res);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return null;
            } finally {
                cursor.close();
            }
        }
        return null;
    }

    public static String addNameFileDuplicate(String filePath, int number) {
        int pos = filePath.lastIndexOf(".");
        if (pos > 0) {
            String fileName = filePath.substring(0, pos);
            String extension = filePath.substring(pos, filePath.length());
            if (filePath.charAt(pos - 1) == ')') { // File co dang abc(n).mp4
                int posNumb = filePath.lastIndexOf("(");
                String numberFile = filePath.substring(posNumb + 1, pos - 1);
                Log.i(TAG, "numberFile: " + numberFile);
                try {
                    fileName = filePath.substring(0, posNumb)
                            + "(" + String.valueOf(number) + ")";
                    Log.i(TAG, "fileName new: " + fileName);
                    return fileName + extension;
                } catch (NumberFormatException e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            return fileName + "(" + String.valueOf(number) + ")" + extension;
        }
        return filePath + "(" + String.valueOf(number) + ")";
    }

    public static String getRealPathVideoFromURI(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Video.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null); //Since manageQuery
        // is deprecated
        if (cursor != null && cursor.moveToFirst()) {
            try {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                return cursor.getString(column_index);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return null;
            } finally {
                cursor.close();
            }
        }
        return null;
    }

    public static String getExtensionFile(String fileName) {
        if (TextUtils.isEmpty(fileName)) return "";
        //String extension = MimeTypeMap.getFileExtensionFromUrl(url);//TODO lấy từ name chắc ko cần đến hàm này đâu
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static String getNameFileFromURL(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

    public static File createVideoFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String videoFileName = Constants.FILE.VIDEO_FILE_PREFIX + timeStamp + "_";
        String cacheDir = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.CACHE_FOLDER;
        File albumF = new File(cacheDir);
        return File.createTempFile(videoFileName, Constants.FILE.MP4_FILE_SUFFIX, albumF);
    }

    public static void createNoMediaFile(Context context) {
        String filePath = Config.Storage.REENG_STORAGE_FOLDER + "/";
        File file = new File(filePath, Constants.FILE.NO_MEDIA_FILE_NAME);
        if (!file.exists()) {
            FileOutputStream noMediaOutStream = null;
            try {
                noMediaOutStream = new FileOutputStream(file);
                noMediaOutStream.write(0);
                noMediaOutStream.flush();
            } catch (IOException e) {
                Log.e(TAG, "Exception", e);
            } finally {
                try {
                    if (noMediaOutStream != null) {
                        noMediaOutStream.close();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            refreshGallery(context, file);
        }

        filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.IMAGE_FOLDER + "/";
        file = new File(filePath, Constants.FILE.NO_MEDIA_FILE_NAME);
        if (!file.exists()) {
            FileOutputStream noMediaOutStream = null;
            try {
                noMediaOutStream = new FileOutputStream(file);
                noMediaOutStream.write(0);
            } catch (IOException e) {
                Log.e(TAG, "Exception", e);
            } finally {
                if (noMediaOutStream != null) {
                    try {
                        noMediaOutStream.close();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException", e);
                    }
                }
            }
            refreshGallery(context, file);
        }
    }

    public static void deleteNoMediaFile(Context context) {
        String filePath = Config.Storage.REENG_STORAGE_FOLDER + "/";
        File file = new File(filePath, Constants.FILE.NO_MEDIA_FILE_NAME);
        if (file.exists()) {
            file.delete();
            refreshGallery(context, file);
        }

        filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.IMAGE_FOLDER + "/";
        file = new File(filePath, Constants.FILE.NO_MEDIA_FILE_NAME);
        if (file.exists()) {
            file.delete();
            refreshGallery(context, file);
        }
    }

    public static void deleteListFile(HashSet<String> filePaths) {
        long t = System.currentTimeMillis();
        for (String path : filePaths) {
            try {
                File file = new File(path);
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        Log.d(TAG, "deleteListFile take: " + (System.currentTimeMillis() - t));
    }

    public static void deleteFile(Context context, String filePath) {
        Log.i(TAG, "deletefile: " + filePath);
        try {
            if (!TextUtils.isEmpty(filePath)) {
                File file = new File(filePath);
                if (file.exists()) {
                    file.delete();
                    refreshGallery(context, file);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public static void refreshGallery(Context context, String filePath) {
        if (filePath == null) {
            return;
        }
        refreshGallery(context, new File(filePath));
    }

    public static void refreshGallery(Context context, File file) {
        //Uri uri = FileHelper.fromFile((ApplicationController) context.getApplicationContext(), file);
        Uri uri = Uri.fromFile(file);// notify gallery thì ko check provider android 7.0
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(uri);
        context.sendBroadcast(mediaScanIntent);
    }

    public static void testZipLogFile() {
        String LOG_FOLDER_PATH = Environment.getExternalStorageDirectory().getPath() +
                File.separator + "Mocha" + File.separator + ".logs" + File.separator;
        File home = new File(LOG_FOLDER_PATH);
        File[] listFiles = home.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File file : listFiles) {
                Log.d(TAG, "zipFile: " + file.getAbsolutePath());
                File zipFile = new File(home, "zipFile" + System.currentTimeMillis() + ".zip");
                zipFile(file, zipFile);
            }
        }
    }

    public static void zipFile(File inputFile, File zipFile) {
        BufferedInputStream origin = null;
        ZipOutputStream out = null;
        FileInputStream in = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            fos = new FileOutputStream(zipFile);
            bos = new BufferedOutputStream(fos);
            out = new ZipOutputStream(bos);
            byte data[] = new byte[Constants.FILE.BUFFER_SIZE_DEFAULT];
            in = new FileInputStream(inputFile);
            origin = new BufferedInputStream(in, Constants.FILE.BUFFER_SIZE_DEFAULT);
            ZipEntry entry = new ZipEntry("11");
            out.putNextEntry(entry);
            int count;
            while ((count = origin.read(data, 0, Constants.FILE.BUFFER_SIZE_DEFAULT)) != -1) {
                out.write(data, 0, count);
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "FileNotFoundException", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            try {
                if (origin != null)
                    origin.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (in != null)
                    in.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (bos != null)
                    bos.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (fos != null)
                    fos.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public static void saveImageToGallery(BaseSlidingFragmentActivity activity, ApplicationController application,
                                          ReengMessage message) {
        File messageImageFile = new File(message.getFilePath());
        if (!messageImageFile.exists()) {
            if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                Glide.with(activity)
                        .downloadOnly()
                        .load(UrlConfigHelper.getInstance(application).getDomainImage() + message.getDirectLinkMedia())
                        .into(new SimpleTarget<File>() {

                            @Override
                            public void onResourceReady(@NonNull File resource, @Nullable Transition<? super File> transition) {
                                if (!resource.exists()) {
                                    activity.showToast(R.string.e601_error_but_undefined);
                                    return;
                                }
                                message.setFilePath(resource.getAbsolutePath());
                                saveImageToGallery(activity, application, message);
                            }
                        });
                return;
            }
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        File recordDir = new File(Config.Storage.GALLERY_MOCHA);
        if (!recordDir.exists()) {
            recordDir.mkdirs();
        }
        String saveFileName = "received_" + message.getFileId() + ".jpg";
        File saveImage = new File(recordDir, saveFileName);
        if (saveImage.exists()) {
            activity.showToast(R.string.file_exist);
        } else {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(messageImageFile);
                out = new FileOutputStream(saveImage);
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                activity.showToast(R.string.sticker_download_done);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                activity.showToast(R.string.e601_error_but_undefined);
            } finally {
                try {
                    if (in != null) in.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                try {
                    if (out != null) out.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            refreshGallery(application, saveImage);
        }
    }

    public static Uri fromFile(ApplicationController application, File file) {
        if (Version.hasN()) {
            return FileProvider.getUriForFile(application, application.getPackageName() + ".provider", file);
        } else {
            return Uri.fromFile(file);
        }
    }

    public static String saveFileShare(Uri uri, Context app) {

        try {
            InputStream inputStream = app.getContentResolver().openInputStream(uri);
            if (inputStream == null) return null;
            String currentTime = System.currentTimeMillis() + "";
            String path = com.metfone.selfcare.helper.Config.Storage.REENG_STORAGE_FOLDER
                    + com.metfone.selfcare.helper.Config.Storage.CACHE_FOLDER + "/" + ImageHelper.IMAGE_NAME + currentTime + ".jpg";
            FileOutputStream output = new FileOutputStream(path);
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int len = 0;
            while ((len = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, len);
            }
            return path;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void compressImage(String filePath, String folder, boolean isHD, CompressImageListener listener) {
        // TODO crash (width and height must be > 0)
        String ratio;
        Bitmap scaledBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp;
        BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        Log.d(TAG, "actualHeight = " + actualHeight + " , actualWidth = " + actualWidth);
        if (actualHeight == 0 || actualWidth == 0) {
            if (listener != null) {
                listener.onCompressFail();
            }
        }
        float maxSize = 1280.0f;
        boolean isLandscape = false;
        if (isHD) {
            maxSize = 2048.0f;
        }

        ExifInterface exif;
        int orientation = 0;
        try {
            exif = new ExifInterface(filePath);

            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            if (orientation == 6 || orientation == 8 || actualWidth > actualHeight) {
                isLandscape = true;
                Log.d("EXIF", "Exif: " + orientation);
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }
        float imgRatio = (float) actualWidth / (float) actualHeight;
//        float maxRatio = maxWidth / maxHeight;
        if (isLandscape) {
            if (actualWidth >= maxSize) {
                actualWidth = (int) maxSize;
                actualHeight = (int) (maxSize / imgRatio);
            }
        } else {
            if (actualHeight >= maxSize) {
                actualWidth = (int) (maxSize * imgRatio);
                actualHeight = (int) maxSize;
            }
        }

        options.inSampleSize = MessageHelper.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false; // hoaf sawcs
        options.inPurgeable = true; // Lam trong sach, lam tinh khiet
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            Log.e(TAG, "OutOfMemoryError", exception);
            bmp = null;
            if (listener != null) {
                listener.onCompressFail();
            }
        }
        try {
            // Returns a mutable bitmap with the specified width and height.
            // Its initial density is as per getDensity().
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Constants.IMAGE_CACHE.BITMAP_CONFIG);
        } catch (OutOfMemoryError exception) {
            Log.e(TAG, "OutOfMemoryError", exception);
            return;
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        if (scaledBitmap == null || bmp == null) {
            if (listener != null) {
                listener.onCompressFail();
            }
        } else {
            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                    middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG)
            );
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            // ByteArrayOutputStream out = null;
            String currentTime = System.currentTimeMillis() + "";
            String newFilePath = com.metfone.selfcare.helper.Config.Storage.REENG_STORAGE_FOLDER
                    + folder + "/" + ImageHelper.IMAGE_NAME + currentTime + ".jpg";

            FileOutputStream fos;
            try {
                fos = new FileOutputStream(newFilePath);
                int quality = 75;
                boolean scaled = scaledBitmap.compress(Bitmap.CompressFormat.JPEG,
                        quality, fos);
                Log.d(TAG,
                        "scaled = " + scaled + " height = "
                                + scaledBitmap.getHeight() + " width = "
                                + scaledBitmap.getWidth()
                );
                fos.close();
                if (listener != null) {
                    float ratioFloat = (float) actualWidth / (float) actualHeight;
                    ratio = (new DecimalFormat("#.##").format(ratioFloat)).replace(",", ".");
                    Log.d(TAG, " resize actualHeight = " + actualHeight + " , actualWidth = " + actualWidth + " " +
                            "ratio: " + ratio);
                    listener.onCompressSuccess(newFilePath, ratio);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                if (listener != null) {
                    listener.onCompressFail();
                }
            }
        }
    }

    public static String detectMimeType(File file) {
        String extension = null;
        String absolutePath = file.getAbsolutePath();
        int index = absolutePath.lastIndexOf(".") + 1;
        if (index >= 0 && index <= absolutePath.length()) {
            extension = absolutePath.substring(index);
        }
        if (extension == null || extension.isEmpty()) {
            return com.metfone.selfcare.network.file.Constants.APPLICATION_OCTET_STREAM;
        }
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        if (mimeType == null) {
            return com.metfone.selfcare.network.file.Constants.APPLICATION_OCTET_STREAM;
        }
        return mimeType;
    }

    public interface CompressImageListener {
        void onCompressSuccess(String filePath, String ratio);

        void onCompressFail();
    }

    public static ImageView caculateImageViewAware(ApplicationController mApp, ImageView img,
                                                   float ratio, boolean isThumb, int padding) {
        int screenWidth = mApp.getWidthPixels() - padding;
        int screenHeight = mApp.getHeightPixels() - padding;
        int maxSize = 1280;
        /*int maxSize;
        if (!isThumb) {
            maxSize = 1280;
        } else {
            maxSize = 640;
        }*/
        if (ratio >= 1) {   //width > height
            if (screenWidth >= maxSize) {
                screenWidth = maxSize;
            }
            screenHeight = Math.round(screenWidth / ratio);

        } else {        //width < height
            if (screenHeight >= maxSize) {
                screenHeight = maxSize;
            }
            screenWidth = Math.round(screenHeight * ratio);
        }
        Log.i(TAG, "caculateImageViewAware: width - " + screenWidth + " height - " + screenHeight);
        if (img != null) {
            img.setMaxWidth(screenWidth);
            img.setMaxHeight(screenHeight);
        }
        return img;
    }

    public static String getFileNameFromPath(String filePath) {
        if (TextUtils.isEmpty(filePath)) return "";
        int index = filePath.lastIndexOf("/");
        String fileName = filePath.substring(index + 1);
        // chan max length
        if (!TextUtils.isEmpty(fileName) && fileName.length() > Constants.MESSAGE.MAX_FILE_NAME) {
            fileName = fileName.substring(fileName.length() - Constants.MESSAGE.MAX_FILE_NAME - 1);
        }
        return fileName;
    }

    public static boolean isDriveFile(final Uri uri) {
        return Version.hasKitKat() && uri != null && "com.google.android.apps.docs.storage".equals(uri.getAuthority());
    }

    public static String getRealPath(final Context context, final Uri uri) {
        if (uri == null) return null;
        Log.d(TAG, "getRealPath: " + uri);
        String mochaPath = getPathFromMochaUri(uri);
        if (!TextUtils.isEmpty(mochaPath)) {
            return mochaPath;
        } else if (Version.hasKitKat() && DocumentsContract.isDocumentUri(context, uri)) {
            if ("com.android.externalstorage.documents".equals(uri.getAuthority())) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                final String id = DocumentsContract.getDocumentId(uri);
                if (id != null && id.startsWith("raw:"))
                    return id.replaceFirst("raw:", "");
                return getDownloadDocumentDataColumn(context, uri, null, null);
            } else if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
            return getDataColumn(context, uri, null, null);
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            /*if ("com.google.android.apps.photos.content".equals(uri.getAuthority())) {
                return uri.getLastPathSegment();
            } else if ("com.facebook.katana.securefileprovider".equals(uri.getAuthority())) {
                return getFilePathImageShareFacebook(uri.getPath());
            } else if ("com.viber.voip.provider.file".equals(uri.getAuthority())) {
                return getFilePathImageShareViber(uri.getPath());
            }*/

            return saveFileShare(uri, context);
//            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private static String getFilePathImageShareFacebook(String pathFb) {
        if (pathFb.startsWith("/sdcard_root/")) {
            pathFb = pathFb.split("/sdcard_root/")[1];
            return Environment.getExternalStorageDirectory().getPath() + "/" + pathFb;
        }
        return null;

    }

    private static String getFilePathImageShareViber(String pathViber) {
        if (pathViber.startsWith("/external_files/")) {
            pathViber = pathViber.split("/external_files/")[1];
            return Environment.getExternalStorageDirectory().getPath() + "/" + pathViber;
        }
        return null;

    }

    private static String getPathFromMochaUri(Uri uri) {
        if (Version.hasKitKat() && uri != null && ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            String authority = uri.getAuthority();
            if (authority != null && authority.contains(BuildConfig.APPLICATION_ID)) {
                final List<String> paths = uri.getPathSegments();
                int size = paths.size();
                if (size > 1) {
                    StringBuilder sb = new StringBuilder(Environment.getExternalStorageDirectory().getPath());
                    for (int i = 1; i < size; i++) {
                        sb.append(File.separator).append(paths.get(i));
                    }
                    return sb.toString();
                }
            }
        }
        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static String getDownloadDocumentDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = MediaStore.MediaColumns.DISPLAY_NAME;
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                //return cursor.getString(index);
                String fileName = cursor.getString(index);
                return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static String writeToFile(Context context, String fileName, String data) {
        /*String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Mocha" + File.separator + ".logs" + File.separator + "logkqi";
        try {
            File myFile = new File(filePath);
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.close();
            Log.i(TAG, "file size: " + myFile.length() + "B");
        } catch (Exception e) {
            return null;
        }
        return filePath;*/

        FileOutputStream outputStream;

        File file = new File(context.getFilesDir(), fileName);
        try {
            outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            return null;
        }
        return file.getAbsolutePath();

    }

    public static File copyFile(String oriFilePath, String newFilePath) {
        File messageImageFile = new File(oriFilePath);
        if (!messageImageFile.exists()) {
            return null;
        }
        File saveImage = new File(newFilePath);
        if (saveImage.exists()) {
            return saveImage;
        } else {
            InputStream in = null;
            OutputStream out = null;
            boolean exception = false;
            try {
                in = new FileInputStream(messageImageFile);
                out = new FileOutputStream(saveImage);
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                exception = true;
            } finally {
                try {
                    if (in != null) in.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                try {
                    if (out != null) out.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            if (exception) return null;
            else return saveImage;
        }
    }
}
package com.metfone.selfcare.helper;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by thanhnt72 on 1/9/2018.
 */

public class TagHelper {

    private static final String TAG = TagHelper.class.getSimpleName();

    public static ArrayList<TagMocha> getListTagFromListPhoneNumber(ArrayList<PhoneNumber> listPhoneNumbers,
                                                                    boolean useJid) {
        ArrayList<TagMocha> listTagToServer = new ArrayList<>();
        if (listPhoneNumbers != null && !listPhoneNumbers.isEmpty()) {
            for (PhoneNumber p : listPhoneNumbers) {
                String nameTag;
                if (useJid) {
                    nameTag = p.getJidNumber();
                } else {
                    nameTag = p.getNickName();
                    if (TextUtils.isEmpty(nameTag)) {
                        nameTag = p.getJidNumber();
                    }
                }

                if (!TextUtils.isEmpty(nameTag) && !TextUtils.isEmpty(p.getJidNumber())) {
                    TagMocha myTag = new TagMocha(TagsCompletionView.PREFIX_TAG + nameTag,
                            nameTag, p.getJidNumber());
                    myTag.setContactName(p.getName());
                    listTagToServer.add(myTag);
                }
            }
        }
        return listTagToServer;
    }

    public static String getTextTag(ArrayList<TagMocha> listTagToServer) {
        try {
            if (!listTagToServer.isEmpty()) {
                Gson gson = new GsonBuilder().create();
                JsonArray myCustomArray = gson.toJsonTree(listTagToServer).getAsJsonArray();
                Log.i(TAG, "jsonarray: " + myCustomArray.toString());
                /*JsonObject jsonObject = new JsonObject();
                jsonObject.add("tags", myCustomArray);
                Log.i(TAG, "jsonobject: " + jsonObject.toString());*/
                return myCustomArray.toString();
            } else {
                return "";
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
            return "";
        }
    }

    public static String getTextTagCopy(String text, ArrayList<TagMocha> listTag, ApplicationController mApp) {
        if (TextUtils.isEmpty(text) || listTag == null || listTag.isEmpty()) {
            return text;
        }
        ContactBusiness contactBusiness = mApp.getContactBusiness();
        String textCopy = text;
        String myJid = mApp.getReengAccountBusiness().getJidNumber();
        for (TagMocha tag : listTag) {
            if (TextUtils.isEmpty(tag.getContactName())) {
                if(tag.getMsisdn().equals(myJid)){
                    tag.setContactName(mApp.getReengAccountBusiness().getUserName());
                } else {
                    PhoneNumber p = contactBusiness.getPhoneNumberFromNumber(tag.getMsisdn());
                    if (p != null) {
                        tag.setContactName(p.getName());
                    } else {
                        tag.setContactName(tag.getMsisdn());
                    }
                }
            }
            if (!TextUtils.isEmpty(tag.getTagName()) && !TextUtils.isEmpty(tag.getContactName())) {
                textCopy = textCopy.replaceFirst(Pattern.quote(tag.getTagName()),
                        Matcher.quoteReplacement(tag.getContactName()));
                Log.i(TAG, "textCopy: " + textCopy);
            }
        }
        return textCopy;
    }

}

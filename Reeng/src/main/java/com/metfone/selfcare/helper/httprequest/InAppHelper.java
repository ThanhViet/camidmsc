package com.metfone.selfcare.helper.httprequest;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.PREFERENCE;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Kich ban in app
 * Created by toanvk2 on 5/11/2016.
 */
public class InAppHelper {
    private static final String TAG = InAppHelper.class.getSimpleName();
    private static InAppHelper instance;
    private static final String TAG_CHECK_SMS = "TAG_REQUEST_CHECK_SMS";
    private ApplicationController mApplication;
    private SharedPreferences mPref;

    private InAppHelper(ApplicationController application) {
        this.mApplication = application;
        this.mPref = mApplication.getSharedPreferences(PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized InAppHelper getInstance(ApplicationController application) {
        if (instance == null) {
            instance = new InAppHelper(application);
        }
        return instance;
    }

    public void cancelRequestCheckSms() {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_CHECK_SMS);
    }

    public void requestLogActionSmsSent(final String smsTo, final String content) {
        if (!mApplication.getReengAccountBusiness().isValidAccount() ||
                !mApplication.getReengAccountBusiness().isVietnam()) return;
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.INAPP_ACTION_SMS_SENT);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse" + response);
                        /*int code = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                code = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                        } catch (JSONException e) {
                            Log.e(TAG,"Exception",e);
                        }*/
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(smsTo).
                        append(mApplication.getReengAccountBusiness().getOperator()).
                        append(account.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("smsto", smsTo);
                params.put("currOperator", mApplication.getReengAccountBusiness().getOperator());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY,
                        HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    private void checkInAppFreeSms(String friendJid, String friendName, final OnResponseSmsFreeListener listener) {
        cancelRequestCheckSms();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String token = mApplication.getReengAccountBusiness().getToken();
        long currentTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder().
                append(myNumber).
                append(friendJid).
                append(friendName).
                append(token).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), token));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.INAPP_CHECK_FREE_SMS),
                HttpHelper.EncoderUrl(myNumber),
                HttpHelper.EncoderUrl(friendJid),
                HttpHelper.EncoderUrl(friendName),
                String.valueOf(currentTime),
                dataEncrypt);
        Log.d(TAG, "checkInAppFreeSms url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "response" + response);
                        try {
                            int errorCode = -1;
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE))
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            if (errorCode == HTTPCode.E200_OK) {
                                int smsRemain = responseObject.optInt("sms_remain", -1);
                                int type = responseObject.optInt("type", -1);
                                String content = responseObject.optString("popup_content", null);
                                String confirm = responseObject.optString("popup_confirm", null);
                                String fake_mo_cmd = responseObject.optString("fake_mo_cmd", null);
                                listener.onResponse(type, smsRemain, content, confirm, fake_mo_cmd);
                            } else {
                                Log.d(TAG, "checkInAppFreeSms error: " + errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }
                , new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_CHECK_SMS, false);
    }

    public void checkShowPopupOrSendRequest(final FragmentManager fragmentManager,
                                            final BaseSlidingFragmentActivity activity,
                                            String friendJid) {
        if (mApplication.getReengAccountBusiness().isVip() ||
                !mApplication.getReengAccountBusiness().isVietnam() ||
                !mApplication.getReengAccountBusiness().isViettel()) return;
        if (!mApplication.getConfigBusiness().isEnableInApp()) return;
        if (TextUtils.isEmpty(friendJid)) return;
        PhoneNumber friendNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendJid);
        if (friendNumber == null) return;
        String friendName = friendNumber.getName();
        if (friendNumber.isReeng()) {
            long lastTimeFakeOA = mPref.getLong(PREFERENCE.PREF_INAPP_LAST_FAKE_OA, -1);
            if (lastTimeFakeOA != -1 && !TimeHelper.checkTimeOutForDurationTime(lastTimeFakeOA, TimeHelper.ONE_HOUR_IN_MILISECOND)) {
                return;// 1 h goi api 1 lan voi so da dung mocha
            }
        }
        checkInAppFreeSms(friendJid, friendName, new OnResponseSmsFreeListener() {
            @Override
            public void onResponse(int type, int remain, String content, String confirm, String fake_mo_cmd) {
                if (type == 3) {
                    mPref.edit().putLong(PREFERENCE.PREF_INAPP_LAST_FAKE_OA, TimeHelper.getCurrentTime()).apply();
                } else if (type == 1) {
                    if (TextUtils.isEmpty(content) || TextUtils.isEmpty(confirm) || TextUtils.isEmpty(fake_mo_cmd)) {
                        Log.d(TAG, "checkInAppFreeSms error: TextUtils.isEmpty(content) || TextUtils.isEmpty(confirm) || TextUtils.isEmpty(fake_mo_cmd)");
                        return;
                    }
                    if (remain > 0) {
                        long lastTimeShowPopup = mPref.getLong(PREFERENCE.PREF_INAPP_LAST_SHOW_POPUP, -1);
                        if (lastTimeShowPopup == -1 || !TimeHelper.checkTimeInDay(lastTimeShowPopup)) {
                            mPref.edit().putLong(PREFERENCE.PREF_INAPP_LAST_SHOW_POPUP, TimeHelper.getCurrentTime()).apply();
                            showPopupSuggestInApp(fragmentManager, activity, content, confirm, fake_mo_cmd);
                        }
                    } else if (remain == 0) {
                        showPopupSuggestInApp(fragmentManager, activity, content, confirm, fake_mo_cmd);
                    }
                } else if (type == 2) {
                    if (TextUtils.isEmpty(content)) {
                        Log.d(TAG, "checkInAppFreeSms error: TextUtils.isEmpty(content)");
                        return;
                    }
                    if (remain > 0) {
                        long lastTimeShowPopup = mPref.getLong(PREFERENCE.PREF_INAPP_LAST_SHOW_POPUP, -1);
                        if (lastTimeShowPopup == -1 || !TimeHelper.checkTimeInDay(lastTimeShowPopup)) {
                            mPref.edit().putLong(PREFERENCE.PREF_INAPP_LAST_SHOW_POPUP, TimeHelper.getCurrentTime()).apply();
                            showPopupAlertInApp(activity, content);
                        }
                    } else if (remain == 0) {
                        showPopupAlertInApp(activity, content);
                    }
                }
            }
        });
    }

    private void showPopupSuggestInApp(final FragmentManager fragmentManager,
                                       final BaseSlidingFragmentActivity activity,
                                       String content, String confirm, String cmd) {
        mApplication.trackingEvent(R.string.ga_category_inapp, R.string.ga_action_interaction, R.string.ga_label_inapp_show_popup_register);
        ObjInApp obj = new ObjInApp(confirm, cmd);
        Resources res = mApplication.getResources();
        String btnOk = res.getString(R.string.register);
        String btnCancel = res.getString(R.string.cancel);
        PopupHelper.getInstance().showDialogConfirm(activity, null, content, btnOk, btnCancel, new ClickListener.IconListener() {
            @Override
            public void onIconClickListener(View view, Object entry, int menuId) {
                if (menuId == 100) {
                    ObjInApp objInApp = (ObjInApp) entry;
                    showPopupConfirmInApp(activity, objInApp.getConfirm(), objInApp.getCmd());
                }
            }
        }, obj, 100);
    }

    private void showPopupAlertInApp(final BaseSlidingFragmentActivity activity, String content) {
        mApplication.trackingEvent(R.string.ga_category_inapp, R.string.ga_action_interaction, R.string.ga_label_inapp_show_popup_close);
        activity.showError(content, null);
    }

    private void showPopupConfirmInApp(final BaseSlidingFragmentActivity activity, String confirm, String cmd) {
        Resources res = activity.getResources();
        new DialogConfirm(activity, true).setLabel(null).setMessage(confirm).
                setNegativeLabel(res.getString(R.string.cancel)).setPositiveLabel(res.getString(R.string.ok)).
                setEntry(cmd).setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                mApplication.trackingEvent(R.string.ga_category_inapp, R.string.ga_action_interaction, R.string.ga_label_inapp_click_ok);
                String cmd = (String) result;
                ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, activity, null, cmd, "popup_in_app");
            }
        }).show();
    }

    private class ObjInApp {
        private String confirm;
        private String cmd;

        public ObjInApp(String confirm, String cmd) {
            this.confirm = confirm;
            this.cmd = cmd;
        }

        public String getConfirm() {
            return confirm;
        }

        public String getCmd() {
            return cmd;
        }
    }

    public interface OnResponseSmsFreeListener {
        void onResponse(int type, int remain, String content, String confirm, String fake_mo_cmd);
    }
}

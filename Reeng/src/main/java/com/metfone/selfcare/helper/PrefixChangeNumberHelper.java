package com.metfone.selfcare.helper;

import android.Manifest;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.text.TextUtils;


import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.message.ThreadListFragmentRecycleView;
import com.metfone.selfcare.ui.dialog.DialogChangePrefixNumber;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_LIST_NUMBER_SEND_BROADCAST_CHANGE_PREFIX;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_SHOW_DIALOG_THREAD_CHAT_CHANGE_PREFIX;

/**
 * Created by thanhnt72 on 8/7/2018.
 */

public class PrefixChangeNumberHelper {

    private static final String TAG = PrefixChangeNumberHelper.class.getSimpleName();

    private static PrefixChangeNumberHelper mInstant;
    private ApplicationController mApp;
    private HashMap<Integer, ThreadShowDialog> listShow = new HashMap<>();
    private HashSet<String> listNumberSendBroadcast = new HashSet<>();

    public static final String[] PREFIX_OLD = {
            "0120", "0121", "0122", "0126", "0128"
            , "0123", "0124", "0125", "0127", "0129"
            , "0162", "0163", "0164", "0165", "0166", "0167", "0168", "0169"
            , "0186", "0188", "0199"};
    public static final String[] PREFIX_NEW = {
            "070", "079", "077", "076", "078"
            , "083", "084", "085", "081", "082"
            , "032", "033", "034", "035", "036", "037", "038", "039"
            , "056", "058", "059"};

    public static synchronized PrefixChangeNumberHelper getInstant(ApplicationController mApp) {
        if (mInstant == null) {
            mInstant = new PrefixChangeNumberHelper(mApp);
        }
        return mInstant;
    }

    public PrefixChangeNumberHelper(ApplicationController mApp) {
        this.mApp = mApp;
        String dataThreadShow = mApp.getPref().getString(PREF_SHOW_DIALOG_THREAD_CHAT_CHANGE_PREFIX, "");
        Log.i(TAG, "dataThreadShow: " + dataThreadShow);
        if (!TextUtils.isEmpty(dataThreadShow)) {
            try {
                JSONArray jsa = new JSONArray(dataThreadShow);
                for (int i = 0; i < jsa.length(); i++) {
                    JSONObject js = jsa.optJSONObject(i);
                    if (js != null) {
                        int id = js.optInt("idThread");
                        long time = js.optLong("time");
                        listShow.put(id, new ThreadShowDialog(id, time));
                        Log.i(TAG, "idThread: " + id + " time: " + time);
                    }
                }
            } catch (Exception e) {
                Log.i(TAG, "Exception", e);
            }
        }
        String dataListNumberSendBroadcast = mApp.getPref().getString(PREF_LIST_NUMBER_SEND_BROADCAST_CHANGE_PREFIX, "");
        Log.i(TAG, "dataListNumberSendBroadcast: " + dataListNumberSendBroadcast);
        if (!TextUtils.isEmpty(dataListNumberSendBroadcast)) {
            listNumberSendBroadcast = new Gson().fromJson(dataListNumberSendBroadcast,
                    new TypeToken<HashSet<String>>() {
                    }.getType());
        }
    }

    public String convertNewPrefix(String oldNumber) {
        if (TextUtils.isEmpty(oldNumber)) return null;
        if (mApp.getConfigBusiness().isDisableChangePrefix()) return null;
        if (mApp.getConfigBusiness().needChangePrefix(oldNumber)) {
            String prefix = oldNumber.substring(0, 4);
            String prefixNew = mApp.getConfigBusiness().getHashChangeNumberOld2New().get(prefix);
            oldNumber = oldNumber.replaceFirst(prefix, prefixNew);
            return oldNumber;
        } else
            return null;
    }

    public String getOldNumber(String newNumber) {
        if (TextUtils.isEmpty(newNumber)) return null;
        if (mApp.getConfigBusiness().isDisableChangePrefix()) return null;
        int length = newNumber.length();
        if (length == 10) {
            String prefix = newNumber.substring(0, 3);
            if (mApp.getConfigBusiness().getHashChangeNumberNew2Old().containsKey(prefix)) {
                String prefixOld = mApp.getConfigBusiness().getHashChangeNumberNew2Old().get(prefix);
                newNumber = newNumber.replaceFirst(prefix, prefixOld);
                if (mApp.getConfigBusiness().needChangePrefix(newNumber)) {
                    return newNumber;
                }
            }
        }
        return null;
    }

    public String getOldNumberWithoutConfig(String newNumber) {
        if (TextUtils.isEmpty(newNumber)) return null;
        if (mApp.getConfigBusiness().isDisableChangePrefix()) return null;
        int length = newNumber.length();
        if (length == 10) {
            String prefix = newNumber.substring(0, 3);
            if (mApp.getConfigBusiness().getHashChangeNumberNew2Old().containsKey(prefix)) {
                String prefixOld = mApp.getConfigBusiness().getHashChangeNumberNew2Old().get(prefix);
                newNumber = newNumber.replaceFirst(prefix, prefixOld);
                return newNumber;
            }
        }
        return null;
    }

    public void showDialogFriendChangePrefix(PhoneNumber phoneNumber, String newNumber,
                                             BaseSlidingFragmentActivity activity,
                                             EditContactSuccess listener,
                                             int threadId) {
        String msg = String.format(activity.getResources().getString(R.string.change_prefix_msg_contact_update_old),
                phoneNumber.getName());
        showDialogSoloNumber(msg, "", phoneNumber, newNumber, activity, listener, threadId);
    }

    public void showDialogFriendNewNumber(PhoneNumber phoneNumber, String newNumber,
                                          BaseSlidingFragmentActivity activity,
                                          EditContactSuccess listener,
                                          int threadId) {
        String msg = String.format(activity.getResources().getString(R.string.change_prefix_msg_contact_update_new),
                phoneNumber.getNewNumber(), phoneNumber.getName());
        showDialogSoloNumber(msg, "", phoneNumber, newNumber, activity, listener, threadId);
    }

    public void showDialogFriendNewNumberDeeplink(PhoneNumber phoneNumber, String newNumber,
                                                  BaseSlidingFragmentActivity activity,
                                                  EditContactSuccess listener,
                                                  int threadId) {
        String title = activity.getResources().getString(R.string.title_update_contact_new);
        showDialogSoloNumber("", title, phoneNumber, newNumber, activity, listener, threadId);
    }


    private void showDialogSoloNumber(String msg, String title, final PhoneNumber phoneNumber, final String newNumber,
                                      final BaseSlidingFragmentActivity activity,
                                      final EditContactSuccess listener,
                                      final int threadId) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(phoneNumber);
        final DialogChangePrefixNumber dialogConfirm = new DialogChangePrefixNumber(activity, true, list);
        dialogConfirm.setMessage(msg);
        dialogConfirm.setTitle(title);
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                if (PermissionHelper.declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
                    PermissionHelper.requestPermissionWithGuide(activity,
                            Manifest.permission.WRITE_CONTACTS,
                            Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
                } else {
                    activity.showLoadingDialog("", R.string.loading);
                    boolean isSuccess = updateContact(phoneNumber.getJidNumber(), phoneNumber.getContactId(), newNumber);
                    HashMap<String, String> listNumber = new HashMap<>();
                    listNumber.put(phoneNumber.getJidNumber(), newNumber);
                    listener.onEditContact(isSuccess, listNumber, null);
//                editContact(activity, phoneNumber.getContactId());
                }

            }
        });
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!activity.isFinishing()) {
                    activity.setCurrentPrefixDialog(dialogConfirm);
                    dialogConfirm.show();
                    saveShowInDay(threadId);
                }
            }
        });

    }

    public void showDialogNonContactChangePrefix(final NonContact nonContact,
                                                 final BaseSlidingFragmentActivity activity,
                                                 PositiveListener<Object> listener,
                                                 final int threadId) {


        Resources res = activity.getResources();
        String msg = String.format(res.getString(
                R.string.change_prefix_msg_contact_update_old), nonContact.getJidNumber());
        ArrayList<Object> listObj = new ArrayList<>();
        listObj.add(nonContact);
        final DialogChangePrefixNumber dialogConfirm = new DialogChangePrefixNumber(activity, true, listObj);
        dialogConfirm.setMessage(msg);
        dialogConfirm.setPositiveListener(listener);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!activity.isFinishing()) {
                    activity.setCurrentPrefixDialog(dialogConfirm);
                    dialogConfirm.show();
                    saveShowInDay(threadId);
                }
            }
        });
    }

    public void showDialogGroupChangePrefix(final HashMap<String, PhoneNumber> listChange,
                                            final HashMap<String, String> listChangeNonContact,
                                            final BaseSlidingFragmentActivity activity,
                                            final EditContactSuccess listener,
                                            final int threadId) {

        if (!listChange.isEmpty()) {
            ArrayList<Object> list = new ArrayList<>();
            for (Map.Entry<String, PhoneNumber> entry : listChange.entrySet()) {
//                String key = entry.getKey();
                PhoneNumber p = entry.getValue();
                list.add(p);
            }

            Resources res = activity.getResources();
            String msg;
            if (list.size() == 1)
                msg = res.getString(R.string.change_prefix_msg_group_single_change);
            else
                msg = String.format(res.getString(R.string.change_prefix_msg_group), list.size());
            final DialogChangePrefixNumber dialogConfirm = new DialogChangePrefixNumber(activity, true, list);
            dialogConfirm.setMessage(msg);
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    if (PermissionHelper.declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
                        PermissionHelper.requestPermissionWithGuide(activity,
                                Manifest.permission.WRITE_CONTACTS,
                                Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
                    } else {
                        activity.showLoadingDialog("", R.string.loading);
                        HashMap<String, String> listEditSuccess = processEditListContact(listChange, activity);
                        listener.onEditContact(!listEditSuccess.isEmpty(), listEditSuccess, listChangeNonContact);
//                editContact(activity, phoneNumber.getContactId());
                    }

                }
            });
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!activity.isFinishing()) {
                        activity.setCurrentPrefixDialog(dialogConfirm);
                        dialogConfirm.show();
                        saveShowInDay(threadId);
                    }
                }
            });
        }

        /*new AsyncTask<Void, Void, HashMap<String, String>>() {
            @Override
            protected HashMap<String, String> doInBackground(Void... voids) {
                HashMap<String, String> hashChangeUpdate = new HashMap<>();
                if (!listChangeNonContact.isEmpty()) {
                    ArrayList<NonContact> listChangeUpdate = new ArrayList<>();
                    for (Map.Entry<String, String> entry : listChangeNonContact.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        NonContact nonContact = mApp.getContactBusiness().getExistNonContact(key);
                        if (nonContact != null) {
                            nonContact.setJidNumber(value);
                            listChangeUpdate.add(nonContact);
                            hashChangeUpdate.put(key, value);
                        }
                    }
                    if (!listChangeUpdate.isEmpty()) {
                        mApp.getContactBusiness().updateListNonContact(listChangeUpdate);
                    }
                }
                return hashChangeUpdate;
            }

            @Override
            protected void onPostExecute(HashMap<String, String> hashMap) {
                super.onPostExecute(hashMap);
                if (listener != null) {
                    listener.onEditNonContact(true, hashMap);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);*/


    }

    private HashMap<String, String> processEditListContact(HashMap<String, PhoneNumber> listEdit, BaseSlidingFragmentActivity activity) {
        HashMap<String, String> listEditSuccess = new HashMap<>();

        for (Map.Entry<String, PhoneNumber> entry : listEdit.entrySet()) {
//                String key = entry.getKey();
            PhoneNumber p = entry.getValue();
            boolean isSuccess = updateContact(p.getJidNumber(), p.getContactId(), p.getNewNumber());
            if (isSuccess) listEditSuccess.put(p.getJidNumber(), p.getNewNumber());
        }
        return listEditSuccess;
    }

    /*public void editContact(ChatActivity activity, String contactID) {
        if (PermissionHelper.declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermission(activity,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
        } else {
            ContentObserverBusiness.getInstance(activity).setAction(Constants.ACTION.EDIT_CONTACT);
            Intent editContact = new Intent(Intent.ACTION_EDIT);
            editContact.setData(Uri.parse(ContactsContract.Contacts.CONTENT_URI + "/" + contactID));
            editContact.putExtra("finishActivityOnSaveCompleted", true);
            activity.setActivityForResult(true);
            activity.setContactIdEdit(contactID);
            activity.startActivityForResult(editContact, Constants.ACTION.EDIT_CONTACT, true);
        }
    }*/


    /*public boolean updateContact(String number, String oldPhoneNumber, String contactId,
                                 BaseSlidingFragmentActivity activity) {
        boolean success = true;
        String phnumexp = "^[0-9]*$";

        try {
            number = number.trim();

            if ((!number.equals("")) && (!match(number, phnumexp))) {
                success = false;
            } else {
                ContentResolver contentResolver = activity.getContentResolver();

//                String where = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";

                String where = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?";
                String[] numberParams = new String[]{contactId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, oldPhoneNumber};

//                String[] numberParams = new String[]{ContactId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE};

                ArrayList<ContentProviderOperation> ops = new ArrayList<android.content.ContentProviderOperation>();

                if (!number.equals("")) {

                    ops.add(android.content.ContentProviderOperation.newUpdate(android.provider.ContactsContract.Data.CONTENT_URI)
                            .withSelection(where, numberParams)
                            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
                            .build());
                }
                contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
            }
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    private boolean match(String stringToCompare, String regularExpression) {
        boolean success = false;
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(stringToCompare);
        if (matcher.matches())
            success = true;
        return success;
    }*/


    public void showDialogConfirmChangePrefixContact(final BaseSlidingFragmentActivity activity, final boolean showDialogEmpty) {
        new AsyncTask<Void, Void, ArrayList<PhoneNumber>>() {

            @Override
            protected ArrayList<PhoneNumber> doInBackground(Void... voids) {

                ArrayList<PhoneNumber> hashUpdate = new ArrayList<>();
                CopyOnWriteArrayList<PhoneNumber> allPhoneNumber = new CopyOnWriteArrayList<>(mApp.getContactBusiness().getListNumberAlls());
                Log.i(TAG, "----PREFIX------- all number: " + allPhoneNumber.size());
                for (PhoneNumber phoneNumber : allPhoneNumber) {
                    if (!TextUtils.isEmpty(phoneNumber.getId())) {
                        String newJid = convertNewPrefix(phoneNumber.getJidNumber());
                        if (newJid != null) {
                            phoneNumber.setNewNumber(newJid);
                            hashUpdate.add(phoneNumber);
                            Log.i(TAG, "------PREFIX-----" + newJid + " phone: " + phoneNumber.toString());
                        }
                    }
                }

                return hashUpdate;
            }

            @Override
            protected void onPostExecute(ArrayList<PhoneNumber> stringPhoneNumberHashMap) {
                super.onPostExecute(stringPhoneNumberHashMap);
                mApp.getPref().edit().putLong(Constants.PREFERENCE.PREF_SHOW_DIALOG_CHANGE_PREFIX, System.currentTimeMillis()).apply();
                if (stringPhoneNumberHashMap.isEmpty()) {
                    if (showDialogEmpty) {
                        showDialogUpdatedAllContact(activity);
                    }
                    return;
                }
                ArrayList<Object> list = new ArrayList<>();
                for (PhoneNumber p : stringPhoneNumberHashMap) {
                    list.add(p);
                }
                showDialogConfirmChangePrefixContact(activity, list, stringPhoneNumberHashMap);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showDialogUpdatedAllContact(BaseSlidingFragmentActivity activity) {
        DialogMessage dialogMessage = new DialogMessage(activity, true);
        dialogMessage.setMessage(activity.getResources().getString(R.string.updated_all_contact));
        dialogMessage.setLabel(activity.getResources().getString(R.string.change_prefix_title));
        dialogMessage.show();
    }

    public void showDialogConfirmChangePrefixContact(final BaseSlidingFragmentActivity activity, ArrayList<Object> list,
                                                     final ArrayList<PhoneNumber> stringPhoneNumberHashMap) {
        Resources res = activity.getResources();
        String msg;
        if (list.size() == 1)
            msg = res.getString(R.string.change_prefix_msg_group_single_change);
        else
            msg = String.format(res.getString(R.string.change_prefix_msg_group), list.size());
        final DialogChangePrefixNumber dialogConfirm = new DialogChangePrefixNumber(activity, true, list);
        dialogConfirm.setMessage(msg);
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                if (PermissionHelper.declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
                    PermissionHelper.requestPermissionWithGuide(activity,
                            Manifest.permission.WRITE_CONTACTS,
                            Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
                } else {
                    updateAllContactAndThreadChat(stringPhoneNumberHashMap, activity);
                }

            }
        });
        /*dialogRightClick.setDismissListener(new DismissListener() {
            @Override
            public void onDismiss() {
                activity.hideLoadingDialog();
            }
        });*/
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!activity.isFinishing()) {
                    activity.setCurrentPrefixDialog(dialogConfirm);
                    dialogConfirm.show();
                }
            }
        });
    }

    PhoneNumber p;
    String oldNumb;
    String newNumb;
    ArrayList<ThreadMessage> containThread;
    CopyOnWriteArrayList<ReengMessage> listMsg;
    CopyOnWriteArrayList<ReengMessage> listAllMsg;

    public void updateAllContactAndThreadChat(ArrayList<PhoneNumber> hashUpdate, final BaseSlidingFragmentActivity activity) {
        Log.i(TAG, "----PREFIX------- updateAllContactAndThreadChat : " + mApp.getConfigBusiness().getRegexPrefixChangeNumb());
        isUpdating = true;
        activity.showLoadingDialog("", R.string.loading_prefix);
        EventBus.getDefault().post(new ThreadListFragmentRecycleView.ChangePrefixContact(false));
        new AsyncTask<ArrayList<PhoneNumber>, Void, HashSet<ThreadMessage>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected HashSet<ThreadMessage> doInBackground(ArrayList<PhoneNumber>... voids) {
                ArrayList<PhoneNumber> hashUpdate = voids[0];

                HashSet<ThreadMessage> listUpdated = new HashSet<>();
                /*HashMap<String, PhoneNumber> hashUpdate = new HashMap<>();
                CopyOnWriteArrayList<PhoneNumber> allPhoneNumber = new CopyOnWriteArrayList<>(mApp.getContactBusiness().getListNumberAlls());
                Log.i(TAG, "----PREFIX------- all number: " + allPhoneNumber.size());
                for (PhoneNumber phoneNumber : allPhoneNumber) {
                    if (!TextUtils.isEmpty(phoneNumber.getId())) {
                        String newJid = convertNewPrefix(phoneNumber.getJidNumber());
                        if (newJid != null) {
                            phoneNumber.setNewNumber(newJid);
                            hashUpdate.put(newJid, phoneNumber);
                            Log.i(TAG, "------PREFIX-----" + newJid + " phone: " + phoneNumber.toString());
                        }
                    }
                }*/
                CopyOnWriteArrayList<ThreadMessage> allThread = new CopyOnWriteArrayList<>(mApp.getMessageBusiness().getAllThreadMessages(false, false));
                for (PhoneNumber p : hashUpdate) {
                    oldNumb = p.getJidNumber();
                    newNumb = p.getNewNumber();

                    boolean isSuccess = updateContact(oldNumb, p.getContactId(), newNumb);
                    p.setJidNumber(newNumb);
                    if (isSuccess) {
                        containThread = findAllThreadContainJid(oldNumb, allThread);
                        for (ThreadMessage thread : containThread) {
                            try {
                                listMsg = thread.getAllMessages();
//                    ArrayList<ReengMessage> listUpdate = new ArrayList<>();
                                listAllMsg = new CopyOnWriteArrayList<>();
                                for (ReengMessage message : listMsg) {
                                    String sender = message.getSender();
                                    if (oldNumb.equals(sender)) {
                                        message.setSender(newNumb);
//                            listUpdate.add(message);
                                    }
                                    listAllMsg.add(message);
                                }
                    /*if (!listUpdate.isEmpty()) {
                        mApp.getMessageBusiness().updateAllFieldsOfListMessage(listUpdate);
                    }*/
                                thread.setAllMessages(listAllMsg);
                                processEditMember(oldNumb, newNumb, thread);

                            } catch (Exception ex) {
                                Log.e(TAG, "-------PREFIX-----ex: " + ex.toString());

                            }
                            listUpdated.add(thread);
                        }
                        //update cac tin nhan con lai trong db
                        int update = mApp.getMessageBusiness().updateListMessageChangeNumberDB(oldNumb, newNumb);
                        Log.i(TAG, "-------PREFIX-----updated: " + update);
                    }

                }
                return listUpdated;
            }

            @Override
            protected void onPostExecute(HashSet<ThreadMessage> threadMessages) {
                super.onPostExecute(threadMessages);
                Log.i(TAG, "-------PREFIX-----onPostExecute: " + threadMessages.size());
                if (threadMessages != null && !threadMessages.isEmpty()) {
                    ListenerHelper.getInstance().onUpdateAllThread(threadMessages);
                }
                isUpdating = false;
                EventBus.getDefault().post(new ThreadListFragmentRecycleView.ChangePrefixContact(true));
                mApp.getContactBusiness().syncContact();
                if (activity != null) {
                    activity.hideLoadingDialog();
                    activity.showToast(activity.getResources().getString(R.string.change_prefix_update_success));
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, hashUpdate);
    }


    private boolean isUpdating;

    public boolean isUpdating() {
        return isUpdating;
    }

    private ArrayList<ThreadMessage> findAllThreadContainJid(String jidNumber, CopyOnWriteArrayList<ThreadMessage> allThread) {
        ArrayList<ThreadMessage> list = new ArrayList<>();
        if (allThread != null && !allThread.isEmpty()) {
            for (ThreadMessage thread : allThread) {
                String numberString = thread.getNumberSearchGroup();
                String soloNumb = thread.getSoloNumber();
                if (numberString.contains(jidNumber)) {
                    list.add(thread);
                } else if (soloNumb.equals(jidNumber)) {
                    list.add(thread);
                }
            }
        }
        return list;
    }

    private boolean updateContact(String oldNumber, String contactId, String newNumber) {
        Cursor phones = mApp.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{
                        ContactsContract.RawContacts._ID,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.TYPE}
                , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "='" + contactId + "'"
                , null, null);

        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        boolean isSuccess = false;
        if (phones != null) {
            while (phones.moveToNext()) {
                try {
                    String rawId = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String type = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    String rawContactId = phones.getString(phones.getColumnIndex(ContactsContract.RawContacts._ID));

                    String formatNumber = null;

                    ReengAccount account = mApp.getReengAccountBusiness().getCurrentAccount();
                    Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                            getPhoneNumberProtocol(mApp.getPhoneUtil(), number, account.getRegionCode());
                    if (phoneNumberProtocol != null) {
                        formatNumber = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                                mApp.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat
                                        .E164));
                    }

                    if (formatNumber == null || !formatNumber.equals(oldNumber))
                        continue;

                    updateContact(rawId, newNumber, type, rawContactId);
                /*ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                builder.withSelection(ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?"
                                + " AND " + ContactsContract.Data.MIMETYPE + "=?"
                                + " AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + "=?"
                                + " AND " + ContactsContract.CommonDataKinds.Phone.TYPE + "=?",
                        new String[]{rawId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, number, type});
                builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, newNumber);
                ops.add(builder.build());*/

                } catch (Exception e) {
                    Log.f(TAG, "Exception updateContact: " + oldNumber + " newNumb" + newNumber);
                }
            }

            try {
                ContentProviderResult[] array = mApp.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                if (array.length > 0) isSuccess = true;
            } catch (Exception e) {
                Log.f(TAG, "Exception ContentProviderResult updateContact: " + oldNumber + " newNumb" + newNumber);
            }
            phones.close();
        }

        return isSuccess;
    }


    void updateContact(String id, String phone, String TYPE, String _ID) {
        ContentResolver cr = mApp.getContentResolver();
        String where = "contact_id = ? AND mimetype = ? AND _id = ? AND " + String.valueOf("data2") + " = ? ";

        String[] params = /*TYPE == 1 ? new String[]{id, "vnd.android.cursor.item/phone_v2", _ID, String.valueOf(1)}
                : TYPE == 2 ? new String[]{id, "vnd.android.cursor.item/phone_v2", _ID, String.valueOf(2)}
                : TYPE == 3 ? new String[]{id, "vnd.android.cursor.item/phone_v2", _ID, String.valueOf(3)}
                : TYPE == 7 ? new String[]{id, "vnd.android.cursor.item/phone_v2", _ID, String.valueOf(7)}
                : */new String[]{id, "vnd.android.cursor.item/phone_v2", _ID, String.valueOf(TYPE)};
        if (params != null) {
            Cursor phoneCur = cr.query(ContactsContract.Data.CONTENT_URI, null, where, params, null);
            ArrayList<ContentProviderOperation> ops = new ArrayList();
            if (phoneCur != null) {
                ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI).withSelection(where, params).withValue("data1", phone).build());
            }
            phoneCur.close();
            try {
                cr.applyBatch("com.android.contacts", ops);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (OperationApplicationException e2) {
                e2.printStackTrace();
            }
        }
    }


    public void processEditMember(HashMap<String, String> list, HashMap<String, String> listChangeNonContact, ThreadMessage mThreadMessage) {
        Log.i(TAG, "-------PREFIX-----processEditMember");
        ArrayList<String> listMember = mThreadMessage.getPhoneNumbers();
        if (listMember != null && !listMember.isEmpty()) {
            for (int i = 0; i < listMember.size(); i++) {
                String phone = listMember.get(i);
                if (list != null && !list.isEmpty() && list.containsKey(phone)) {
                    listMember.set(i, list.get(phone));
                } else if (listChangeNonContact != null && !listChangeNonContact.isEmpty() && listChangeNonContact.containsKey(phone)) {
                    listMember.set(i, listChangeNonContact.get(phone));
                }
            }
        }

        ArrayList<String> listAdmin = mThreadMessage.getAdminNumbers();
        if (listAdmin != null && !listAdmin.isEmpty()) {
            for (int i = 0; i < listAdmin.size(); i++) {
                String phone = listAdmin.get(i);
                if (list != null && !list.isEmpty() && list.containsKey(phone)) {
                    listAdmin.set(i, list.get(phone));
                } else if (listChangeNonContact != null && !listChangeNonContact.isEmpty() && listChangeNonContact.containsKey(phone)) {
                    listAdmin.set(i, listChangeNonContact.get(phone));
                }
            }
        }


        if (listChangeNonContact != null && !listChangeNonContact.isEmpty()) {
            ArrayList<NonContact> listChangeUpdate = new ArrayList<>();
            for (Map.Entry<String, String> entry : listChangeNonContact.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                NonContact nonContact = mApp.getContactBusiness().getExistNonContact(key);
                if (nonContact != null) {
                    nonContact.setJidNumber(value);
                    listChangeUpdate.add(nonContact);
                }
            }
            if (!listChangeUpdate.isEmpty()) {
                mApp.getContactBusiness().updateListNonContact(listChangeUpdate);
            }
        }

        mThreadMessage.setSoloNumberAndNumberSearchGroup();

    }

    public void processEditMember(String oldNumb, String newNumb, ThreadMessage mThreadMessage) {
        Log.i(TAG, "-------PREFIX-----processEditMember");
        ArrayList<String> listMember = mThreadMessage.getPhoneNumbers();
        if (listMember != null && !listMember.isEmpty()) {
            for (int i = 0; i < listMember.size(); i++) {
                String phone = listMember.get(i);
                if (oldNumb.equals(phone)) {
                    listMember.set(i, newNumb);
                }
            }
        }

        ArrayList<String> listAdmin = mThreadMessage.getAdminNumbers();
        if (listAdmin != null && !listAdmin.isEmpty()) {
            for (int i = 0; i < listAdmin.size(); i++) {
                String phone = listAdmin.get(i);
                if (oldNumb.equals(phone)) {
                    listAdmin.set(i, newNumb);
                }
            }
        }

        NonContact nonContact = mApp.getContactBusiness().getExistNonContact(oldNumb);
        if (nonContact != null) {
            nonContact.setJidNumber(newNumb);
            mApp.getContactBusiness().updateNonContact(nonContact);
        }

        mThreadMessage.setSoloNumberAndNumberSearchGroup();

    }

    public boolean hasShowInDay(int threadId) {
        if (listShow.containsKey(threadId)
                && TimeHelper.checkTimeInDay(listShow.get(threadId).getTimeShow())) {
            return true;
        }
        return false;
    }

    private void saveShowInDay(int idThread) {
        if (idThread == -1) return;
        if (listShow.containsKey(idThread)) {
            listShow.remove(idThread);
        }
        listShow.put(idThread, new ThreadShowDialog(idThread, System.currentTimeMillis()));
        JSONArray jsa = new JSONArray();
        try {
            for (Map.Entry<Integer, ThreadShowDialog> entry : listShow.entrySet()) {
                ThreadShowDialog p = entry.getValue();
                JSONObject js = new JSONObject();
                js.put("idThread", p.getThreadId());
                js.put("time", p.getTimeShow());
                jsa.put(js);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(TAG, "jsa: " + jsa.toString());
        mApp.getPref().edit().putString(PREF_SHOW_DIALOG_THREAD_CHAT_CHANGE_PREFIX, jsa.toString()).apply();
    }

    public boolean isSendBroadcast(String number) {
        if (listNumberSendBroadcast.contains(number)) return true;
        return false;
    }

    public void saveListNumber(ArrayList<String> listNumber) {
        listNumberSendBroadcast.addAll(listNumber);
        String jsaString = new Gson().toJson(listNumberSendBroadcast);
        mApp.getPref().edit().putString(PREF_LIST_NUMBER_SEND_BROADCAST_CHANGE_PREFIX, jsaString).apply();
    }

    public interface EditContactSuccess {
        void onEditContact(boolean isSuccess, HashMap<String, String> newNumber, HashMap<String, String> listNonContact);

//        void onEditNonContact(boolean isSuccess, HashMap<String, String> listChangeSuccess);
    }

    public static class ChangeNumberObject {

        public static final int STATE_NON_CONTACT = 0;
        public static final int STATE_FRIEND_NEED_CHANGE = 1;
        public static final int STATE_FRIEND_CHANGED = 2;
        public static final int STATE_AUTO_UPDATE_VIEW = 3;

        int state = -1;
        String oldNumber;
        String newNumber;
        PhoneNumber phoneNumber;
        NonContact nonContact;

        public ChangeNumberObject(int state) {
            this.state = state;
        }

        public ChangeNumberObject(int state, String oldNumber, String newNumber, PhoneNumber phoneNumber) {
            this.state = state;
            this.oldNumber = oldNumber;
            this.newNumber = newNumber;
            this.phoneNumber = phoneNumber;
        }

        public int getState() {
            return state;
        }

        public String getOldNumber() {
            return oldNumber;
        }

        public String getNewNumber() {
            return newNumber;
        }

        public PhoneNumber getPhoneNumber() {
            return phoneNumber;
        }

        public NonContact getNonContact() {
            return nonContact;
        }

        public void setNonContact(NonContact nonContact) {
            this.nonContact = nonContact;
        }
    }

    public class ThreadShowDialog implements Serializable {
        @SerializedName("idThread")
        int threadId;
        @SerializedName("time")
        long timeShow;

        public ThreadShowDialog(int threadId, long timeShow) {
            this.threadId = threadId;
            this.timeShow = timeShow;
        }

        public int getThreadId() {
            return threadId;
        }

        public void setThreadId(int threadId) {
            this.threadId = threadId;
        }

        public long getTimeShow() {
            return timeShow;
        }

        public void setTimeShow(long timeShow) {
            this.timeShow = timeShow;
        }
    }

    public interface UpdateThreadChat {
        void onUpdateThreadChat(int threadId);
    }

    public interface UpdateListThread {
        void onUpdateListThread(HashSet<ThreadMessage> updatedThread);
    }

}

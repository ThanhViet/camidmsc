package com.metfone.selfcare.helper.facebook;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * A class that helps control the audience on Facebook that can see a post made
 * by an app on behalf of a user.
 *
 * @author ronlut
 *         // @see https://developers.facebook.com/docs/reference/api/privacy-parameter/
 */
public class Privacy {

    private static final String TAG = Privacy.class.getSimpleName();

    private static final String PRIVACY = "value";
    private static final String ALLOW = "allow";
    private static final String DENY = "deny";
    private static final String DESCRIPTION = "description";
    // private static final String FRIENDS = "friends";
    private static final String EMPTY = "";

    @SerializedName(PRIVACY)
    private PrivacySettings mPrivacySetting = null;

    @SerializedName(ALLOW)
    private ArrayList<String> mAllowedUsers = new ArrayList<>();

    @SerializedName(DENY)
    private ArrayList<String> mDeniedUsers = new ArrayList<>();

    @SerializedName(DESCRIPTION)
    private String mDescription;

    public enum PrivacySettings {
        EVERYONE,
        ALL_FRIENDS,
        FRIENDS_OF_FRIENDS,
        SELF,
        CUSTOM
    }

    private Privacy(Builder builder) {
        mPrivacySetting = builder.mPrivacySetting;
        mAllowedUsers = builder.mAllowedUsers;
        mDeniedUsers = builder.mDeniedUsers;
    }

    public static class Builder {
        private PrivacySettings mPrivacySetting = null;
        private ArrayList<String> mAllowedUsers = new ArrayList<>();
        private ArrayList<String> mDeniedUsers = new ArrayList<>();

        public Builder() {
        }

        /**
         * The privacy parameter only applies for posts to the user's own
         * timeline and is ultimately governed by the privacy ceiling a user has
         * configured for an app.<br>
         * <br>
         * , you must call either <code>addAllowed()</code> or
         * <code>addDenied()</code>.
         *
         * @param privacySettings The wanted privacy settings
         */
        public Builder setPrivacySettings(PrivacySettings privacySettings) {
            mPrivacySetting = privacySettings;
            return this;
        }

        /**
         * Add a user or a friend list to the allowed list.<br>
         * <br>
         * <p/>
         * <b>Note:</b><br>
         * If you defined other privacy setting than
         * method, then the privacy will be changed with warning to
         *
         * @param userOrListId user Id or friend list Id that <b>can</b> see the post.
         */
        public Builder addAllowed(String userOrListId) {
            validateListsAccessRequest();
            mAllowedUsers.add(userOrListId);
            return this;
        }

        /**
         * Add a predefined friend list to the allowed list This can <b>only</b>
         * include all members of those sets. <br>
         * <br>
         * <p/>
         * <b>Note:</b><br>
         * If you defined other privacy setting than
         * method, then the privacy will be changed with warning to
         *
         * @param privacySettings FRIENDS_OF_FRIENDS} to include all members of those sets.
         * @throws UnsupportedOperationException in case of using other values than
         *                                       FRIENDS_OF_FRIENDS}
         */
        public Builder addAllowed(PrivacySettings privacySettings) {
            validateListsAccessRequest();
            if (privacySettings != PrivacySettings.ALL_FRIENDS || privacySettings != PrivacySettings
                    .FRIENDS_OF_FRIENDS) {
                UnsupportedOperationException exception = new UnsupportedOperationException("Can't add this " +
                        "predefined friend list. Only allowed are: ALL_FRIENDS or FRIENDS_OF_FRIENDS");
                throw exception;
            }

            mAllowedUsers.add(privacySettings.name());
            return this;
        }

        /**
         * Add users and/or friend lists to the allowed list. <br>
         * <br>
         * <p/>
         * <b>Note:</b><br>
         * If you defined other privacy setting than
         * method, then the privacy will be changed with warning to
         *
         * @param userOrListIds mixed user Ids and friend list Ids that <b>can</b> see the
         *                      post.
         */
        public Builder addAllowed(Collection<? extends String> userOrListIds) {
            validateListsAccessRequest();
            mAllowedUsers.addAll(userOrListIds);
            return this;
        }

        /**
         * Add a user or a friend list to the denied list. <br>
         * <br>
         * <p/>
         * <b>Note:</b><br>
         * If you defined other privacy setting than
         * method, then the privacy will be changed with warning to
         *
         * @param userOrListId user Id or friend list Id that <b>cannot</b> see the post
         */
        public Builder addDenied(String userOrListId) {
            validateListsAccessRequest();
            mDeniedUsers.add(userOrListId);
            return this;
        }

        /**
         * Add users and/or friend lists to the denied list. <br>
         * <br>
         * <p/>
         * <b>Note:</b><br>
         * If you defined other privacy setting than
         * method, then the privacy will be changed with warning to
         *
         * @param userOrListIds mixed user Ids and friend list Ids that <b>can</b> see the
         *                      post.
         */
        public Builder addDenied(Collection<? extends String> userOrListIds) {
            validateListsAccessRequest();
            mDeniedUsers.addAll(userOrListIds);
            return this;
        }

        public Privacy build() {
            return new Privacy(this);
        }

        /**
         * Validates that the allowed / denied lists can be accessed.<br/>
         * In case you use a predefined privacy setting different than
         * custom lists.
         */
        private void validateListsAccessRequest() {
            if (mPrivacySetting != PrivacySettings.CUSTOM) {
                mPrivacySetting = PrivacySettings.CUSTOM;
            }
        }

    }

    /**
     * Returns the {@code JSON} representation that should be used as the value
     * of the privacy parameter<br/>
     * in the entities that have privacy settings.
     *
     * @return A {@code String} representing the value of the privacy parameter
     */
    public String getJSONString() {
        JSONObject jsonRepresentation = new JSONObject();
        try {
            jsonRepresentation.put(PRIVACY, mPrivacySetting.name());
            if (mPrivacySetting == PrivacySettings.CUSTOM) {
                if (!mAllowedUsers.isEmpty()) {
                    jsonRepresentation.put(ALLOW, join(mAllowedUsers.iterator(), ","));
                }
                if (!mDeniedUsers.isEmpty()) {
                    jsonRepresentation.put(DENY, join(mDeniedUsers.iterator(), ","));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "JSONException", e);
        }

        return jsonRepresentation.toString();
    }

    private String join(Iterator<?> iterator, String separator) {
        if (iterator == null) {
            return null;
        }
        if (!iterator.hasNext()) {
            return EMPTY;
        }
        Object first = iterator.next();
        if (!iterator.hasNext()) {
            return first == null ? EMPTY : first.toString();
        }
        StringBuilder buf = new StringBuilder(256);
        if (first != null) {
            buf.append(first);
        }
        while (iterator.hasNext()) {
            buf.append(separator);
            Object obj = iterator.next();
            if (obj != null) {
                buf.append(obj);
            }
        }
        return buf.toString();
    }
}
package com.metfone.selfcare.helper;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.pm.ShortcutManagerCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MochaShortcutManager {

    private static final String TAG = "MochaShortcutManager";
    private static final String VIDEO_SHORTCUT_ID = "video_shortcut_id";
    private static final String NEWS_SHORTCUT_ID = "news_shortcut_id";
    private static final String MUSIC_SHORTCUT_ID = "music_shortcut_id";
    private static final String MOVIES_SHORTCUT_ID = "movies_shortcut_id";
    private static final String DATING_SHORTCUT_ID = "dating_shortcut_id";
    private static final String ONMEDIA_SHORTCUT_ID = "onmedia_shortcut_id";
    private static final String TIIN_SHORTCUT_ID = "tiin_shortcut_id";

    private static final List<String> SHORTCUT_IDS = Arrays.asList(VIDEO_SHORTCUT_ID, NEWS_SHORTCUT_ID, MUSIC_SHORTCUT_ID, MOVIES_SHORTCUT_ID, DATING_SHORTCUT_ID, ONMEDIA_SHORTCUT_ID);

    @TargetApi(Build.VERSION_CODES.O)
    public static void enableAllDynamicShortcut(@NonNull Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) return;
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);

            ShortcutInfo videoShortcut = createShortcutInfo(VIDEO_SHORTCUT_ID, context);
            ShortcutInfo moviesShortcut = createShortcutInfo(MOVIES_SHORTCUT_ID, context);
            ShortcutInfo datingShortcut = createShortcutInfo(DATING_SHORTCUT_ID, context);
            ShortcutInfo newsShortcut = createShortcutInfo(NEWS_SHORTCUT_ID, context);
            ShortcutInfo musicShortcut = createShortcutInfo(MUSIC_SHORTCUT_ID, context);
            ShortcutInfo onmediaShortcut = createShortcutInfo(ONMEDIA_SHORTCUT_ID, context);

            if (shortcutManager != null)
                shortcutManager.addDynamicShortcuts(Arrays.asList(videoShortcut, newsShortcut, musicShortcut, moviesShortcut, datingShortcut));
        } catch (Exception e) {

        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static void addDynamicShortcut(String shortId, @NonNull Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) return;
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            ShortcutInfo shortcutInfo = createShortcutInfo(shortId, context);

            if (shortcutManager != null)
                shortcutManager.addDynamicShortcuts(Collections.singletonList(shortcutInfo));
        } catch (Exception e) {

        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static void addDynamicShortcutWrap(final ConfigTabHomeItem item, @NonNull final Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) return;
            final ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);

            Glide.with(context).asBitmap().load(item.getImgMore()).apply(
                    new RequestOptions().error(R.drawable.ic_home_more_default)
                            .placeholder(R.drawable.ic_home_more_default))
                    .into(new SimpleTarget<Bitmap>(200, 200) {

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            Log.d(TAG, "download Image onLoadFailed");
                        }

                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            ShortcutInfo shortcutInfo = createShortcutInfoWrap(item, context, resource);

                            if (shortcutManager != null)
                                shortcutManager.addDynamicShortcuts(Collections.singletonList(shortcutInfo));
                        }
                    });
        } catch (Exception ignored) {

        }
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    public static void disableShortcut(String shortId, @NonNull Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) return;
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            if (shortcutManager != null) {
                shortcutManager.disableShortcuts(Collections.singletonList(shortId));
                shortcutManager.removeDynamicShortcuts(Collections.singletonList(shortId));
            }
        } catch (Exception e) {

        }
    }

    @TargetApi(Build.VERSION_CODES.N_MR1)
    public static void enableShortcut(String shortId, @NonNull Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) return;
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            if (shortcutManager != null) {
                shortcutManager.enableShortcuts(Collections.singletonList(shortId));
            }
        } catch (Exception e) {

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    public static void addShortcutToHome(String shortId, Context context) {
        try {
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);

            if (ShortcutManagerCompat.isRequestPinShortcutSupported(context)) {
                ShortcutInfo pinShortcutInfo = getShortcutInfo(shortId, context);

                if (pinShortcutInfo == null) {
                    pinShortcutInfo = createShortcutInfo(shortId, context);

                    if (shortcutManager != null) {
                        Intent pinnedShortcutCallbackIntent = shortcutManager.createShortcutResultIntent(pinShortcutInfo);
                        PendingIntent successCallback = PendingIntent.getBroadcast(context, 0, pinnedShortcutCallbackIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        IntentSender intentSender = successCallback.getIntentSender();
                        shortcutManager.requestPinShortcut(pinShortcutInfo, intentSender);
                    }
                } else {
                    if (pinShortcutInfo.isEnabled()) {
                        ToastUtils.showToast(context, context.getString(R.string.add_short_cut_exists));
                    } else //Neu dang disable thi enable len
                    {
                        enableShortcut(shortId, context);
                    }
                }
            } else {
                ToastUtils.showToast(context, "Not support pin shortcut");
            }
        } catch (Exception e) {

        }
    }

    public static boolean checkShortcutAdded(String shortcutID, Context context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
            if (ShortcutManagerCompat.isRequestPinShortcutSupported(context)) {
                ShortcutInfo pinShortcutInfo = getShortcutInfo(shortcutID, context);
                if (pinShortcutInfo != null && pinShortcutInfo.isEnabled()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    public static void addShortcutToHomeWrap(final ConfigTabHomeItem item, final Context context) {
        try {
            final ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);

            if (ShortcutManagerCompat.isRequestPinShortcutSupported(context)) {
                ShortcutInfo pinShortcutInfo = getShortcutInfo(item.getId(), context);//shortId la id tab wrap

                if (pinShortcutInfo == null) {
                    Glide.with(context).asBitmap().load(item.getImgMore()).apply(
                            new RequestOptions().error(R.drawable.ic_home_more_default)
                                    .placeholder(R.drawable.ic_home_more_default))
                            .into(new SimpleTarget<Bitmap>(200, 200) {

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                    super.onLoadFailed(errorDrawable);
                                    Log.d(TAG, "download Image onLoadFailed");
                                }

                                @Override
                                public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                                    ShortcutInfo pinShortcutInfo = createShortcutInfoWrap(item, context, bitmap);//Khoi tao moi check lai

                                    if (shortcutManager != null) {
                                        Intent pinnedShortcutCallbackIntent = shortcutManager.createShortcutResultIntent(pinShortcutInfo);
                                        PendingIntent successCallback = PendingIntent.getBroadcast(context, 0, pinnedShortcutCallbackIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                                        IntentSender intentSender = successCallback.getIntentSender();

                                        shortcutManager.requestPinShortcut(pinShortcutInfo, intentSender);
                                    }
                                }
                            });
                } else {
                    if (pinShortcutInfo.isEnabled()) {
                        ToastUtils.showToast(context, context.getString(R.string.add_short_cut_exists));
                    } else //Neu dang disable thi enable len
                    {
                        enableShortcut(item.getId(), context);
                    }
                }
            } else {
                Toast.makeText(ApplicationController.self(), "Not support pin shortcut", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
    }

    @RequiresApi(Build.VERSION_CODES.N_MR1)
    private static ShortcutInfo getShortcutInfo(String shortcutId, Context context) {
        ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
        if (shortcutManager != null) {
            List<ShortcutInfo> shortcutInfoList = shortcutManager.getPinnedShortcuts();
            for (ShortcutInfo info : shortcutInfoList) {
                if (info.getId().equals(shortcutId)) {
                    return info;
                }
            }
        }
        return null;
    }

    @RequiresApi(Build.VERSION_CODES.N_MR1)
    public static boolean isExistShortcutInfo(String shortcutId, Context context) {
        ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
        if (shortcutManager != null) {
            List<ShortcutInfo> shortcutInfoList = shortcutManager.getPinnedShortcuts();

            for (ShortcutInfo info : shortcutInfoList) {
                if (info.getId().equals(shortcutId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void deleteShortcut(String shortcutId, Context context) {
        Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
        shortcutIntent.setClassName("com.example.androidapp", "SampleIntent");
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shortcutIntent.putExtra("someParameter", "HelloWorld");

        Intent removeIntent = new Intent();
        removeIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        removeIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "ShortcutName");
        removeIntent.putExtra("duplicate", false);

        removeIntent
                .setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        context.sendBroadcast(removeIntent);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private static ShortcutInfo createShortcutInfo(String shortcutId, Context context) {
        ShortcutInfo shortcutInfo = null;
        switch (shortcutId) {
            case VIDEO_SHORTCUT_ID:
                Intent newTaskIntentVideo = new Intent(context, HomeActivity.class);
                newTaskIntentVideo.setAction(Intent.ACTION_VIEW);
                newTaskIntentVideo.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentVideo.setData(Uri.parse("mocha://home/video"));

                shortcutInfo = new ShortcutInfo.Builder(context, VIDEO_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_video))
                        .setLongLabel(context.getString(R.string.tab_video))
//                        .setLongLabel(context.getString(R.string.tab_video_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_video_png))
                        .setIntent(newTaskIntentVideo)
                        .build();
                break;
            case MUSIC_SHORTCUT_ID:
                Intent newTaskIntentMusic = new Intent(context, HomeActivity.class);
                newTaskIntentMusic.setAction(Intent.ACTION_VIEW);
                newTaskIntentMusic.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentMusic.setData(Uri.parse("mocha://home/music"));

                shortcutInfo = new ShortcutInfo.Builder(context, MUSIC_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_music))
                        .setLongLabel(context.getString(R.string.tab_music))
//                        .setLongLabel(context.getString(R.string.tab_music_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_music_png))
                        .setIntent(newTaskIntentMusic)
                        .build();
                break;
            case MOVIES_SHORTCUT_ID:
                Intent newTaskIntentMovies = new Intent(context, HomeActivity.class);
                newTaskIntentMovies.setAction(Intent.ACTION_VIEW);
                newTaskIntentMovies.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentMovies.setData(Uri.parse("mocha://home/movie"));

                shortcutInfo = new ShortcutInfo.Builder(context, MOVIES_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_movie))
                        .setLongLabel(context.getString(R.string.tab_movie))
//                        .setLongLabel(context.getString(R.string.tab_movie_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_movie_pngl))
                        .setIntent(newTaskIntentMovies)
                        .build();
                break;
            case NEWS_SHORTCUT_ID:
                Intent newTaskIntentNews = new Intent(context, HomeActivity.class);
                newTaskIntentNews.setAction(Intent.ACTION_VIEW);
                newTaskIntentNews.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentNews.setData(Uri.parse("mocha://home/news"));

                shortcutInfo = new ShortcutInfo.Builder(context, NEWS_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_news))
                        .setLongLabel(context.getString(R.string.tab_news))
//                        .setLongLabel(context.getString(R.string.tab_news_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_netnews_png))
                        .setIntent(newTaskIntentNews)
                        .build();
                break;
            case DATING_SHORTCUT_ID:
                Intent newTaskIntentDating = new Intent(context, HomeActivity.class);
                newTaskIntentDating.setAction(Intent.ACTION_VIEW);
                newTaskIntentDating.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentDating.setData(Uri.parse("mocha://home/stranger"));

                shortcutInfo = new ShortcutInfo.Builder(context, DATING_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_stranger))
                        .setLongLabel(context.getString(R.string.tab_stranger))
//                        .setLongLabel(context.getString(R.string.tab_stranger_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_hot_stranger_png))
                        .setIntent(newTaskIntentDating)
                        .build();
                break;
            case ONMEDIA_SHORTCUT_ID:
                Intent newTaskIntentOnmedia = new Intent(context, HomeActivity.class);
                newTaskIntentOnmedia.setAction(Intent.ACTION_VIEW);
                newTaskIntentOnmedia.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentOnmedia.setData(Uri.parse("mocha://home/onmedia"));

                shortcutInfo = new ShortcutInfo.Builder(context, ONMEDIA_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_onmedia))
                        .setLongLabel(context.getString(R.string.tab_onmedia))
//                        .setLongLabel(context.getString(R.string.tab_social_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_social_png))
                        .setIntent(newTaskIntentOnmedia)
                        .build();
                break;
            case TIIN_SHORTCUT_ID:
                Intent newTaskIntentTiin = new Intent(context, HomeActivity.class);
                newTaskIntentTiin.setAction(Intent.ACTION_VIEW);
                newTaskIntentTiin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                newTaskIntentTiin.setData(Uri.parse("mocha://home/tiin"));

                shortcutInfo = new ShortcutInfo.Builder(context, TIIN_SHORTCUT_ID)
                        .setShortLabel(context.getString(R.string.tab_tiin))
                        .setLongLabel(context.getString(R.string.tab_tiin))
//                        .setLongLabel(context.getString(R.string.tab_tiin_des))
                        .setIcon(Icon.createWithResource(context, R.drawable.ic_fun_tiin_png))
                        .setIntent(newTaskIntentTiin)
                        .build();
                break;
        }

        return shortcutInfo;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private static ShortcutInfo createShortcutInfoWrap(final ConfigTabHomeItem item, final Context context, Bitmap bitmap) {
        Intent newTaskIntent = new Intent(context, HomeActivity.class);
        newTaskIntent.setAction(Intent.ACTION_VIEW);
        newTaskIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        try {
            newTaskIntent.setData(Uri.parse("mocha://wepapp?ref=" + URLEncoder.encode(item.getLink(), "UTF-8") + "&title=" + URLEncoder.encode(item.getName(), "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return new ShortcutInfo.Builder(context, item.getId())
                .setShortLabel(item.getName())
                .setLongLabel(item.getDesc())
//                        .setLongLabel(context.getString(R.string.tab_social_des))
                .setIcon(Icon.createWithBitmap(bitmap))
                .setIntent(newTaskIntent)
                .build();
    }

    public static String convertShortcutId(TabHomeHelper.HomeTab homeTab) {
        switch (homeTab) {
            case tab_video:
                return VIDEO_SHORTCUT_ID;
            case tab_movie:
                return MOVIES_SHORTCUT_ID;
            case tab_music:
                return MUSIC_SHORTCUT_ID;
            case tab_news:
                return NEWS_SHORTCUT_ID;
            case tab_stranger:
                return DATING_SHORTCUT_ID;
            case tab_hot:
                return ONMEDIA_SHORTCUT_ID;
            case tab_tiins:
                return TIIN_SHORTCUT_ID;
            case tab_wap:

        }
        return "";
    }
}

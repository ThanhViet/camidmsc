package com.metfone.selfcare.helper;

import android.content.res.Resources;
import android.text.TextUtils;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/10/2017.
 */

public class OnMediaHelper {

    private static final String TAG = OnMediaHelper.class.getSimpleName();

    //TODO rat la loang ngoang :(
    public static String getTitleLikeInHtml(long countLikeFeed, ArrayList<UserInfo> listUserLike,
                                            ApplicationController mApp, String colorInHtml) {
        Resources mRes = mApp.getResources();
        if (countLikeFeed < 0) countLikeFeed = 0;
        String titleLike;
        String textBold, textBold2;
        int likeFromFriend = listUserLike.size();
        if (countLikeFeed == 0) { //Be the first to like this
            titleLike = mRes.getString(R.string.onmedia_fisrt_like);
        } else if (countLikeFeed == 1) {
            if (likeFromFriend == 0) {
                Log.i(TAG, "1 person liked this");
                textBold = "1 " + mRes.getString(R.string.onmedia_person);
            } else {
                Log.i(TAG, "contact A liked this");
                UserInfo userInfo = listUserLike.get(0);
                if (userInfo != null) {
                    textBold = getNameTextFromUserInfo(userInfo, mApp);
                } else {
                    textBold = "1 " + mRes.getString(R.string.onmedia_person);
                }
            }
            titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                    .onmedia_like_this);
        } else if (countLikeFeed == 2) {
            if (likeFromFriend == 0) {
                Log.i(TAG, "2 people liked this");
                textBold = "2 " + mRes.getString(R.string.onmedia_people);
                titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                        .onmedia_like_this);
            } else if (likeFromFriend == 1) {
                Log.i(TAG, "contact A and 1 other liked this");
                UserInfo userInfo = listUserLike.get(0);
                if (userInfo != null) {
                    textBold = getNameTextFromUserInfo(userInfo, mApp);
                    textBold2 = "1 " + mRes.getString(R.string.onmedia_other);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and)
                            + " " + TextHelper.textColorWithHTML(textBold2, colorInHtml) + " "
                            + mRes.getString(R.string.onmedia_like_this);
                } else {
                    textBold = "2 " + mRes.getString(R.string.onmedia_others);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .onmedia_like_this);
                }
            } else {
                UserInfo userInfo1 = listUserLike.get(0);
                UserInfo userInfo2 = listUserLike.get(1);
                if (userInfo1 != null && userInfo2 != null) {
                    Log.i(TAG, "contact A and contact B liked this");
                    textBold = getNameTextFromUserInfo(userInfo1, mApp);
                    textBold2 = getNameTextFromUserInfo(userInfo2, mApp);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and)
                            + " " + TextHelper.textColorWithHTML(textBold2, colorInHtml)
                            + " " + mRes.getString(R.string.onmedia_like_this);
                } else if (userInfo1 == null && userInfo2 != null) {
                    Log.i(TAG, "contact B and 1 other liked this");
                    textBold = getNameTextFromUserInfo(userInfo2, mApp);
                    textBold2 = "1 " + mRes.getString(R.string.onmedia_other);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and)
                            + " " + TextHelper.textColorWithHTML(textBold2, colorInHtml)
                            + " " + mRes.getString(R.string.onmedia_like_this);
                } else if (userInfo1 != null && isObjectNull(userInfo2)) {
                    Log.i(TAG, "contact A and 1 other liked this");
                    textBold = getNameTextFromUserInfo(userInfo1, mApp);
                    textBold2 = "1 " + mRes.getString(R.string.onmedia_other);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and) +
                            " " + TextHelper.textColorWithHTML(textBold2, colorInHtml) + " " + mRes.getString(R
                            .string.onmedia_like_this);
                } else {
                    Log.i(TAG, "2 people liked this");
                    textBold = "2 " + mRes.getString(R.string.onmedia_people);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .onmedia_like_this);
                }
            }
        } else {
            if (likeFromFriend == 0) {
                Log.i(TAG, "3 people liked this");
                textBold = String.valueOf(countLikeFeed) + " " + mRes.getString(R.string.onmedia_people);
                titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                        .onmedia_like_this);
            } else if (likeFromFriend == 1) {
                Log.i(TAG, "contact A and 2 others liked this");
                UserInfo userInfo = listUserLike.get(0);
                if (userInfo != null) {
                    long likeTmp = countLikeFeed - 1;
                    textBold = getNameTextFromUserInfo(userInfo, mApp);
                    textBold2 = String.valueOf(likeTmp) + " " + mRes.getString(R.string.onmedia_others);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and) +
                            " " + TextHelper.textColorWithHTML(textBold2, colorInHtml) + " " + mRes.getString(R.string
                            .onmedia_like_this);
                } else {
                    textBold = String.valueOf(countLikeFeed) + " " + mRes.getString(R.string.onmedia_people);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .onmedia_like_this);
                }
            } else {
                UserInfo userInfo1 = listUserLike.get(0);
                UserInfo userInfo2 = listUserLike.get(1);
                if (userInfo1 != null && userInfo2 != null) {
                    Log.i(TAG, "contact A, contact B and 1 other liked this");
                    textBold = getNameTextFromUserInfo(userInfo1, mApp);
                    textBold2 = getNameTextFromUserInfo(userInfo2, mApp);
                    long likeTmp = countLikeFeed - 2;
                    String textBoldTmp;
                    if (likeTmp == 1) {
                        textBoldTmp = "1 " + mRes.getString(R.string.onmedia_other);
                    } else {
                        textBoldTmp = String.valueOf(likeTmp) + " " + mRes.getString(R.string.onmedia_others);
                    }
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + ", " +
                            TextHelper.textColorWithHTML(textBold2, colorInHtml) + " " + mRes.getString(R.string
                            .text_and) +
                            " " + TextHelper.textColorWithHTML(textBoldTmp, colorInHtml) + " " + mRes.getString(R
                            .string.onmedia_like_this);
                } else if (userInfo1 == null && userInfo2 != null) {
                    Log.i(TAG, "contact B and 2 other liked this");
                    long likeTmp = countLikeFeed - 1;
                    textBold = getNameTextFromUserInfo(userInfo2, mApp);
                    textBold2 = String.valueOf(likeTmp) + " " + mRes.getString(R.string.onmedia_others);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and) +
                            " " + TextHelper.textColorWithHTML(textBold2, colorInHtml) + " " + mRes.getString(R
                            .string.onmedia_like_this);
                } else if (userInfo1 != null && isObjectNull(userInfo2)) {
                    Log.i(TAG, "contact A and 2 other liked this");
                    long likeTmp = countLikeFeed - 1;
                    textBold = getNameTextFromUserInfo(userInfo1, mApp);
                    textBold2 = String.valueOf(likeTmp) + " " + mRes.getString(R.string.onmedia_others);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .text_and) +
                            " " + TextHelper.textColorWithHTML(textBold2, colorInHtml) + " " + mRes.getString(R
                            .string.onmedia_like_this);
                } else {
                    Log.i(TAG, "3 people liked this");
                    textBold = String.valueOf(countLikeFeed) + " " + mRes.getString(R.string.onmedia_people);
                    titleLike = TextHelper.textColorWithHTML(textBold, colorInHtml) + " " + mRes.getString(R.string
                            .onmedia_like_this);
                }
            }
        }
        return titleLike;
    }

    private static boolean isObjectNull(Object obj) {
        return (obj == null);
    }

    private static String getNameTextFromUserInfo(UserInfo userInfo, ApplicationController mApp) {
        ReengAccountBusiness mAccountBusiness = mApp.getReengAccountBusiness();
        ContactBusiness contactBusiness = mApp.getContactBusiness();
        Resources mRes = mApp.getResources();
        if (mAccountBusiness != null && userInfo.getMsisdn().equals(mAccountBusiness.getJidNumber())) {
            return mRes.getString(R.string.you);
        }
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(userInfo.getMsisdn());
        String textBold;
        if (phoneNumber != null) {
            textBold = phoneNumber.getName();
        } else {
            if (!TextUtils.isEmpty(userInfo.getName())) {
                textBold = userInfo.getName();
            } else {
                textBold = Utilities.hidenPhoneNumber(userInfo.getMsisdn());
            }
        }
        return textBold;
    }

    public static boolean isFeedViewNew(FeedModelOnMedia feed) {
        if (feed == null || feed.getFeedContent() == null) return false;
        String itemType = feed.getFeedContent().getItemType();
        String itemSubType = feed.getFeedContent().getItemSubType();
        if (FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) && FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType)))
            return true;
        return false;
    }

    public static boolean isFeedViewImage(FeedModelOnMedia feed) {
        if (feed == null || feed.getFeedContent() == null) return false;
        String itemType = feed.getFeedContent().getItemType();
        String itemSubType = feed.getFeedContent().getItemSubType();
        if (FeedContent.ITEM_TYPE_PROFILE_ALBUM.equals(itemType)
                || FeedContent.ITEM_TYPE_PROFILE_AVATAR.equals(itemType)
                || FeedContent.ITEM_TYPE_PROFILE_COVER.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) && FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(itemSubType)))
            return true;
        return false;
    }

    public static boolean isEnableWriteStatus(FeedModelOnMedia feed) {
        String itemType = feed.getFeedContent().getItemType();
        String itemSubType = feed.getFeedContent().getItemSubType();
        if (itemType.equals(FeedContent.ITEM_TYPE_NEWS)
                || itemType.equals(FeedContent.ITEM_TYPE_VIDEO)
                /*|| itemType.equals(FeedContent.ITEM_TYPE_SONG)
                || itemType.equals(FeedContent.ITEM_TYPE_ALBUM)*/
                || (itemType.equals(FeedContent.ITEM_TYPE_SOCIAL) &&
                (itemSubType.equals(FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK)
                        || itemSubType.equals(FeedContent.ITEM_SUB_TYPE_SOCIAL_VIDEO)
                        || itemSubType.equals(FeedContent.ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO)
                ))) {
            return true;
        }
        return false;
    }

    public static boolean isFeedFromProfile(FeedModelOnMedia feed) {
        String feedType = feed.getFeedContent().getItemType();
        if (feedType.equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_COVER) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_STATUS)) {
            return true;
        }
        return false;
    }

    public static boolean canMoveVideoDetail(FeedModelOnMedia feed) {
        FeedContent feedContent = feed.getFeedContent();
        String url = feedContent.getLink();
//        if (FeedContent.ITEM_TYPE_VIDEO.equals(feedContent.getItemType())) {
//            url = feedContent.getUrl();
//        } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(feedContent.getItemType())
//                && (FeedContent.ITEM_SUB_TYPE_SOCIAL_VIDEO.equals(feedContent.getItemSubType())
//                || FeedContent.ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO.equals(feedContent.getItemSubType()))) {
//            url = feedContent.getContentUrl();
//        }
        return TextHelper.getInstant().isLinkMochaVideo(url);
//        return !TextUtils.isEmpty(url) && (url.contains("video.mocha.com.vn") || url.contains("sieuhai.tv") || url.contains("keeng.vn"));
    }

    public static boolean canMoveMovieDetailWithVideoView(FeedModelOnMedia feed) {
        FeedContent feedContent = feed.getFeedContent();
        String url = feedContent.getLink();
        return (FeedContent.ITEM_TYPE_FILM.equals(feedContent.getItemType()) || (FeedContent.ITEM_TYPE_SOCIAL.equals(feedContent.getItemType())
                && FeedContent.ITEM_SUB_TYPE_SOCIAL_MOVIE.equals(feedContent.getItemSubType())))
                && TextHelper.getInstant().isLinkMochaVideo(url);
    }

    public static void shareFacebookOnMedia(FeedModelOnMedia feedShareFb, BaseSlidingFragmentActivity activity) {
        String link;
        if (FeedContent.ITEM_TYPE_VIDEO.equals(feedShareFb.getFeedContent().getItemType())) {
            link = feedShareFb.getFeedContent().getUrl();
        } else {
            link = feedShareFb.getFeedContent().getContentUrl();
        }
        new FacebookHelper(activity).shareContentToFacebook(activity, activity.getCallbackManager(), link,
                null, null, null, "onmedia");
    }

    public static boolean isFeedViewVideo(FeedModelOnMedia feed) {
        return feed != null && feed.getFeedContent() != null && isFeedViewVideo(feed.getFeedContent());
    }

    public static boolean isFeedViewMovie(FeedModelOnMedia feed) {
        return feed != null && isFeedViewMovie(feed.getFeedContent());
    }

    public static boolean isFeedViewAudio(FeedModelOnMedia feed) {
        return feed != null && isFeedViewAudio(feed.getFeedContent());
    }

    public static boolean isFeedViewChannel(FeedModelOnMedia feed) {
        return feed != null && isFeedViewChannel(feed.getFeedContent());
    }

    public static boolean isFeedViewVideo(FeedContent feedContent) {
        if (feedContent == null) return false;
        String itemType = feedContent.getItemType();
        String itemSubType = feedContent.getItemSubType();
        return FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) && (
                FeedContent.ITEM_SUB_TYPE_SOCIAL_VIDEO.equals(itemSubType) ||
                        FeedContent.ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO.equals(itemSubType))
        );
    }

    public static boolean isFeedViewMovie(FeedContent feedContent) {
        if (feedContent == null) return false;
        String itemType = feedContent.getItemType();
        String itemSubType = feedContent.getItemSubType();
        return FeedContent.ITEM_TYPE_FILM.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) &&
                FeedContent.ITEM_SUB_TYPE_SOCIAL_MOVIE.equals(itemSubType)
        );
    }

    public static boolean isFeedViewAudio(FeedContent feedContent) {
        if (feedContent == null) return false;
        String itemType = feedContent.getItemType();
        String itemSubType = feedContent.getItemSubType();
        return FeedContent.ITEM_TYPE_SONG.equals(itemType)
                || FeedContent.ITEM_TYPE_ALBUM.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) &&
                FeedContent.ITEM_SUB_TYPE_SOCIAL_AUDIO.equals(itemSubType)
        );
    }

    public static boolean isFeedViewChannel(FeedContent feedContent) {
        if (feedContent == null) return false;
        String itemType = feedContent.getItemType();
        String itemSubType = feedContent.getItemSubType();
        return (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) &&
                FeedContent.ITEM_SUB_TYPE_SOCIAL_CHANNEL.equals(itemSubType)
        );
    }
}

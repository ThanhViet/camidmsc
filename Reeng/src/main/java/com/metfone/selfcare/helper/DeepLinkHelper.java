package com.metfone.selfcare.helper;

import static com.metfone.esport.service.ServiceRetrofit.getDefaultParam;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.blankj.utilcode.util.StringUtils;
import com.google.android.gms.common.util.Strings;
import com.metfone.esport.AloneFragmentActivity;
import com.metfone.esport.base.ICallBackResponse;
import com.metfone.esport.entity.repsonse.DetailVideoResponse;
import com.metfone.esport.service.APIService;
import com.metfone.esport.service.ServiceRetrofit;
import com.metfone.esport.ui.channels.ChannelContainerFragment;
import com.metfone.esport.ui.games.tournamentdetails.TournamentDetailFragment;
import com.metfone.esport.ui.live.LiveVideoFragment;
import com.metfone.esport.util.RxUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.DeepLinkActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.RegisterScreenActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.activity.QRCodeKhActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGetAllAppsResponse;
import com.metfone.selfcare.module.home_kh.api.request.WsPushInfoPartnerId;
import com.metfone.selfcare.module.home_kh.fragment.menu.RewardCamIdActivity;
import com.metfone.selfcare.module.home_kh.model.App;
import com.metfone.selfcare.module.home_kh.notification.detail.WebViewActivity;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetExchangeServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;

import org.json.JSONException;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

/**
 * Created by toanvk2 on 12/6/2016.
 */
public class DeepLinkHelper {
    private static final String TAG = DeepLinkHelper.class.getSimpleName();
    private static DeepLinkHelper mInstance;
    public static String uriDeepLink = null;

    public DeepLinkHelper() {

    }

    public static synchronized DeepLinkHelper getInstance() {
        if (mInstance == null) {
            mInstance = new DeepLinkHelper();
        }
        return mInstance;
    }

    public void openSchemaLink(BaseSlidingFragmentActivity activity, String schemaLink, String serviceType,
                               ReengMessage message) {

    }

    private long mLastClickTime = 0;

    public boolean openSchemaLink(BaseSlidingFragmentActivity activity, String schemaLink, boolean showToast) {

        return false;
    }

    public boolean openSchemaLink(BaseSlidingFragmentActivity activity, String schemaLink) {
        return openSchemaLink(activity, schemaLink, true);
    }

//    public static String plusSchemeOneIfNeed(String deepLink) {
//        if (deepLink != null && !deepLink.isEmpty() &&
//                !(deepLink.startsWith("https://camids.page.link"))
//        ) {
//            return if (deepLink.startsWith("/")) {
//                PREFIX_DEEPLINK_ONEID.plus(deepLink)
//            } else {
//                "$PREFIX_DEEPLINK_ONEID/".plus(deepLink)
//            }
//        }
//        return deepLink;
//    }

    public static String getFullPathWithQueryFromDeepLink(String deeplink) {
        Uri uriDeepLink = Uri.parse(deeplink);
        String path = uriDeepLink.getPath();
        String query = uriDeepLink.getQuery();
        if (path == null || path.isEmpty()) {
            return null;
        }
        if (query == null || query.isEmpty()) {
            return path;
        }
        return path + "?" + query;
    }

    public String buildDeepLinkForDeepLinkBuilder(String deeplink) {
        return getFullPathWithQueryFromDeepLink(deeplink);
    }

    public Intent createIntentFromDynamicLink(String path) {
        String deeplink = buildDeepLinkForDeepLinkBuilder(path);
        Uri.Builder uriBuilder = new Uri.Builder().scheme(DEEP_LINK.SCHEME_HTTP);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uriBuilder.path(deeplink).build());
        return intent;
    }

    public void openSchemaLink(BaseSlidingFragmentActivity activity, Uri uri, String serviceType, ReengMessage
            message) {

        if (activity == null || uri == null) {
            return;
        }
        String path = uri.getPath();
        String query = uri.getQuery();
        Log.d(TAG, "Host : " + uri.getHost());
        Log.d(TAG, "Path : " + path);
        Log.d(TAG, "Query : " + query);
        for (String str : uri.getQueryParameterNames()) {
            Log.d(TAG, "Query parameter : " + str);
        }

        if (DEEP_LINK.HOST_CAM_ID.equals(uri.getHost()) || DEEP_LINK.HOST_CAM_DYNAMIC_LINK.equals(uri.getHost()) || DEEP_LINK.HOST_CAM_ID_NOTIFICATION.equals(uri.getHost())) {
            if (uri.getPath().endsWith(DEEP_LINK.PATH_TAB_CINEMA)) {
                handleSelectTab(activity, 2);
            } else if (uri.getPath().endsWith(DEEP_LINK.PATH_TAB_ESPORT)) {
                handleSelectTab(activity, 1);
            } else if (uri.getPath().endsWith(DEEP_LINK.PATH_TAB_METFONE)) {
                handleSelectTab(activity, 4);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_QR_CODE)) {
                handleDeepLinkQrCode(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_INVITE_FRIENDS)) {
                handleDeepLinkInviteFriends(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_REWARD_PROFILE)) {
                handleDeepLinkRewardProfile(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_REWARD_CATEGORY)) {
                handleDeepLinkRewardCategory(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_REWARDS)) {
                handleDeepLinkRewards(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_NEAR_BY)) {
                handleDeepLinkNearYou(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_GAME)) {
                handleDeepLinkGame(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_FILM_PROFILE)) {
                handleDeepLinkFilmProfile(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_FILM_PLAY)) {
                handleDeepLinkFilmPlay(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_FILM_CATEGORY)) {
                handleDeepLinkFilmCategory(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_FILM_ACTOR)) {
                handleDeepLinkActor(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_FILM_DIRECTORY)) {
                handleDeepLinkDirector(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_REWARD_PROFILE)) {
                handleDeepLinkRewardProfile(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_REWARD_CATEGORY)) {
                handleDeepLinkRewardCategory(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_ESPORT_CHANNEL)) {
                handleDeepLinkEsportChannel(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_ESPORT_TOURNAMENT)) {
                handleDeepLinkEsportTournament(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_ESPORT_PLAY)) {
                handleDeepLinkEsportPlay(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_ACCOUNT_INFO)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_ACCOUNT_INFO);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_TOPUP)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_TOPUP);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_SERVICE_PROFILE)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_SERVICES);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_SERVICE_DETAIL)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_DETAIL_SERVICE);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_BUY_PHONE)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_BUY_PHONE);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_I_SHARE)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_ISHARE);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_FEEDBACK)) {
                handleDeepLinkMetfone(activity, uri, DeepLinkActivity.TYPE_FEEDBACK);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_LINK)) {
                handleLink(activity, uri);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_SIGNUP)) {
                handleSignUp(activity);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_LOGIN)) {
                handleLogin(activity);
            } else if (uri.getPath().startsWith(DEEP_LINK.PATH_INSTALL)) {
                handleInstallDeeplink(activity, uri);
            }
        }
    }

    public static boolean needLoginToMetfone(Activity activity, Uri uri) {
        if (uri.getPath().startsWith(DEEP_LINK.PATH_REWARD_PROFILE)
                || uri.getPath().startsWith(DEEP_LINK.PATH_REWARD_CATEGORY)
                || uri.getPath().startsWith(DEEP_LINK.PATH_REWARDS)
                || uri.getPath().startsWith(DEEP_LINK.PATH_ACCOUNT_INFO)
                || uri.getPath().startsWith(DEEP_LINK.PATH_TOPUP)
                || uri.getPath().startsWith(DEEP_LINK.PATH_SERVICE_PROFILE)
                || uri.getPath().startsWith(DEEP_LINK.PATH_BUY_PHONE)
                || uri.getPath().startsWith(DEEP_LINK.PATH_I_SHARE)
                || uri.getPath().startsWith(DEEP_LINK.PATH_FEEDBACK)) {
            return true;
        }

        return false;
    }

    private void goToRegisterScreen(BaseSlidingFragmentActivity activity, Uri data) {
        uriDeepLink = data.toString();
        NavigateActivityHelper.navigateToRegisterScreenActivity(activity, true);
    }

    public void handleDeepLinkQrCode(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity != null) {
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                goToRegisterScreen(activity, uri);
                return;
            }
            Intent intent = new Intent(activity, QRCodeKhActivity.class);
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkNearYou(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity != null) {
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                goToRegisterScreen(activity, uri);
                return;
            }
            NavigateActivityHelper.navigateToSettingKhActivity(activity, Constants.SETTINGS.NEAR_YOU);
        }
    }

    public void handleDeepLinkRewards(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity != null) {
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                goToRegisterScreen(activity, uri);
                return;
            }
            Intent intent = new Intent(activity, RewardCamIdActivity.class);
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkInviteFriends(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity != null) {
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
                goToRegisterScreen(activity, uri);
                return;
            }
            NavigateActivityHelper.navigateToSettingKhActivity(activity, Constants.SETTINGS.SHARE_AND_GET_MORE_SCREEN);
        }
    }

    public void handleDeepLinkGame(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        if (ApplicationController.self().isLogin() == FragmentTabHomeKh.LoginVia.NOT) {
            goToRegisterScreen(activity, uri);
            return;
        } else {
            activity.showLoadingDialog("", "");
            KhHomeClient.getInstance().wsGetAllApps(new MPApiCallback<WsGetAllAppsResponse>() {
                @Override
                public void onResponse(retrofit2.Response<WsGetAllAppsResponse> response) {
                    activity.hideLoadingDialog();
                    if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                        List<App> appList = response.body().getResult().getWsResponse().apps;
                        if (appList != null) {
                            for (App app : appList) {
                                if (app.getName().equalsIgnoreCase("game") && app.getShortDes().equalsIgnoreCase("munny")) {
                                    if (app != null && !Strings.isEmptyOrWhitespace(app.getAndroidLink())) {
                                        Intent intent = new Intent(activity, DeepLinkActivity.class);
                                        intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_GAME);
                                        intent.putExtra(DeepLinkActivity.PARAM_URL_GAME, app.getAndroidLink());
                                        activity.startActivity(intent);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    activity.hideLoadingDialog();
                }
            });
        }
    }

    public void handleDeepLinkFilmProfile(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        if (!StringUtils.isEmpty(id)) {
            activity.showLoadingDialog(null, null);
            MovieApi.getInstance().getMovieDetail(id, new ApiCallbackV2<Movie>() {

                @Override
                public void onError(String s) {

                }

                @Override
                public void onComplete() {
                    activity.hideLoadingDialog();
                }

                @Override
                public void onSuccess(String msg, Movie result) throws JSONException {
                    VideoPlayerActivity.start(activity, result, "", true);
                }
            });
        }
    }

    public void handleDeepLinkFilmPlay(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        activity.showLoadingDialog("", "");
        MovieApi.getInstance().getMovieDetail(id, new ApiCallbackV2<Movie>() {

            @Override
            public void onError(String s) {
                activity.hideLoadingDialog();
            }

            @Override
            public void onComplete() {
                activity.hideLoadingDialog();
            }

            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                if (result != null) {
                    Video video = Movie.movie2Video(result);
                    VideoPlayerActivity.playNow(activity, result, video, "");
                }
            }
        });
    }

    public void handleDeepLinkFilmCategory(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        if (!StringUtils.isEmpty(id)) {
            Intent intent = new Intent(activity, DeepLinkActivity.class);
            intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_FILM_CATEGORY);
            intent.putExtra(DeepLinkActivity.PARAM_ID, id);
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkActor(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        if (!StringUtils.isEmpty(id)) {
            Intent intent = new Intent(activity, DeepLinkActivity.class);
            intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_ACTOR);
            intent.putExtra(DeepLinkActivity.PARAM_ID, id);
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkDirector(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        if (!StringUtils.isEmpty(id)) {
            Intent intent = new Intent(activity, DeepLinkActivity.class);
            intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_DIRECTOR);
            intent.putExtra(DeepLinkActivity.PARAM_ID, id);
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkRewardProfile(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
            goToRegisterScreen(activity, uri);
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        if (!StringUtils.isEmpty(id)) {
            Intent intent = new Intent(activity, DeepLinkActivity.class);
            intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_REWARD_DETAIL);
            intent.putExtra(DeepLinkActivity.PARAM_ID, id);
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkRewardCategory(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
            goToRegisterScreen(activity, uri);
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        if (!StringUtils.isEmpty(id)) {
            Intent intent = new Intent(activity, DeepLinkActivity.class);
            if ("metfone".equals(id)) {
                intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_CATEGORY_METFONE);
            } else {
                intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_REWARD_CATEGORY);
                intent.putExtra(DeepLinkActivity.PARAM_ID, id);
            }
            activity.startActivity(intent);
        }
    }

    public void handleDeepLinkEsportPlay(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        APIService apiService = ServiceRetrofit.getInstance();
        RxUtils.async(compositeDisposable, apiService.getVideoById(id, getDefaultParam())
                , new ICallBackResponse<DetailVideoResponse>() {
                    @Override
                    public void onSuccess(DetailVideoResponse response) {
                        activity.hideLoadingDialog();
                        activity.addFragment(LiveVideoFragment.newInstance(response, true), true, "");
                    }

                    @Override
                    public void onFail(String error) {
                        activity.hideLoadingDialog();
                    }
                });
    }

    public void handleDeepLinkEsportChannel(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        long channelId = 0;
        try {
            channelId = Long.parseLong(id);
        } catch (Exception e) {
            channelId = -1;
        }
        if (channelId != -1) {
            AloneFragmentActivity.with(activity)
                    .parameters(ChannelContainerFragment.newBundle(channelId))
                    .start(ChannelContainerFragment.class);
        }
    }

    public void handleDeepLinkEsportTournament(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String id = uri.getQueryParameter(DEEP_LINK.PARAM_ID);
        long tournamentId = 0;
        try {
            tournamentId = Long.parseLong(id);
        } catch (Exception e) {
            tournamentId = -1;
        }
        if (tournamentId != -1) {
            AloneFragmentActivity.with(activity).parameters(TournamentDetailFragment.newBundle(tournamentId))
                    .start(TournamentDetailFragment.class);
        }
    }

    private void handleServiceList(BaseSlidingFragmentActivity activity, CamIdUserBusiness mCamIdUserBusiness, int type) {
        List<Service> services = mCamIdUserBusiness.getServices();
        boolean mIsHaveMetfonePlusEqual1 = false;
        int sumServiceNumber = 0;
        int userServiceId = -1;
        for (Service s : services) {
            List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
            for (int i = 0; i < phoneLinkeds.size(); i++) {
                PhoneLinked phoneLinked = phoneLinkeds.get(i);
                sumServiceNumber++;
                userServiceId = phoneLinked.getUserServiceId();
                if (phoneLinked.getMetfonePlus() == 1) {
                    mIsHaveMetfonePlusEqual1 = true;
                    mCamIdUserBusiness.setPhoneService(phoneLinked.getPhoneNumber());
                }
            }
        }

        if (sumServiceNumber > 1 && !mIsHaveMetfonePlusEqual1) {
            if (!mCamIdUserBusiness.isAddOrSelectPhoneCalled()) {
                NavigateActivityHelper.navigateToSelectPhone(activity);
                mCamIdUserBusiness.setAddOrSelectPhoneCalled(true);
            }
        } else if (sumServiceNumber == 1) {
            activity.showLoadingDialog("", "");
            RetrofitInstance retrofitInstance = new RetrofitInstance();
            retrofitInstance.updateMetfone(null, null, userServiceId, new ApiCallback<AddServiceResponse>() {
                @Override
                public void onResponse(Response<AddServiceResponse> response) {
                    if (response.body() != null && response.body().getData() != null) {
                        mCamIdUserBusiness.setForceUpdateMetfone(true);
                        mCamIdUserBusiness.setPhoneService(response.body().getData().getServices());
                        Intent intent = new Intent(activity, DeepLinkActivity.class);
                        intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, type);
                        activity.startActivity(intent);
                    }
                    activity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    activity.hideLoadingDialog();
                    mCamIdUserBusiness.setPhoneService("");
                }
            });
        } else {
            if (!mCamIdUserBusiness.isAddOrSelectPhoneCalled()) {
                NavigateActivityHelper.navigateToMetfoneAddNumber(activity);
                mCamIdUserBusiness.setAddOrSelectPhoneCalled(true);
            }
        }
    }

    public void handleDeepLinkMetfone(BaseSlidingFragmentActivity activity, Uri uri, int type) {
        if (activity == null || uri == null) {
            return;
        }
        if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())
                || StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getToken())
                || StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getPhoneService())) {
            uriDeepLink = uri.toString();
            ((HomeActivity) activity).gotoMetfoneTab();
            return;
        }
//        CamIdUserBusiness mCamIdUserBusiness = ((ApplicationController) activity.getApplication()).getCamIdUserBusiness();
//        if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getCamIdToken())) {
//            uriDeepLink = uri.toString();
//            activity.displayRegisterScreenActivity(true);
//            return;
//        }
//        else if (StringUtils.isEmpty(ApplicationController.self().getReengAccountBusiness().getToken())) {
//            mCamIdUserBusiness.setProcessingLoginSignUpMetfone(false);
//            uriDeepLink = uri.toString();
//            NavigateActivityHelper.navigateToEnterAddPhoneNumber(activity, true);
//            return;
//        } else if (StringUtils.isEmpty(ApplicationController.self().getCamIdUserBusiness().getPhoneService())) {
//            mCamIdUserBusiness.setProcessingLoginSignUpMetfone(false);
//            uriDeepLink = uri.toString();
//            handleServiceList(activity, mCamIdUserBusiness, type);
//            return;
//        }

        if (type == DeepLinkActivity.TYPE_DETAIL_SERVICE) {
            String isExchange = uri.getQueryParameter("isExchange");
            String actionObjectId = uri.getQueryParameter("id");
            activity.showLoadingDialog("", "");
            if (isExchange.equals("0")) {
                new MetfonePlusClient().wsGetServiceDetail(actionObjectId, new MPApiCallback<WsGetServiceDetailResponse>() {
                    @Override
                    public void onResponse(Response<WsGetServiceDetailResponse> response) {
                        activity.hideLoadingDialog();
                        if (response.body() != null && response.body().getResult().getWsResponse() != null) {
                            Intent intent = new Intent(activity, DeepLinkActivity.class);
                            intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_DETAIL_SERVICE);
                            intent.putExtra(DeepLinkActivity.PARAM_SERVICE_DETAIL, response.body().getResult().getWsResponse());
                            activity.startActivity(intent);
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        activity.hideLoadingDialog();
                    }
                });
            } else if (isExchange.equals("1")) {
                new MetfonePlusClient().wsGetExchangeService(new MPApiCallback<WsGetExchangeServiceResponse>() {
                    @Override
                    public void onResponse(Response<WsGetExchangeServiceResponse> response) {
                        if (response.body().getResult() != null) {
                            ExchangeItem exchangeItem = null;
                            List<WsGetExchangeServiceResponse.Response> services = response.body().getResult().getData();
                            if (services != null && services.size() > 0) {
                                for (WsGetExchangeServiceResponse.Response service : services) {
                                    List<ExchangeItem> items = service.getListItem();
                                    for (ExchangeItem exchange : items) {
                                        if (exchange.getGroupCode().equals(actionObjectId)) {
                                            exchangeItem = exchange;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (exchangeItem != null) {
                                Intent intent = new Intent(activity, DeepLinkActivity.class);
                                intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, DeepLinkActivity.TYPE_DETAIL_SERVICE);
                                intent.putExtra(DeepLinkActivity.PARAM_IS_EXCHANGE, true);
                                intent.putExtra(DeepLinkActivity.PARAM_EXCHANGE_ITEM, exchangeItem);
                                activity.startActivity(intent);
                            }
                        }
                        activity.hideLoadingDialog();
                    }

                    @Override
                    public void onError(Throwable error) {
                        activity.hideLoadingDialog();
                    }
                });
            } else {
                activity.hideLoadingDialog();
            }
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity != null) {
                    Intent intent = new Intent(activity, DeepLinkActivity.class);
                    intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, type);
                    activity.startActivity(intent);
                }
            }
        }, 1000);

    }

    private void handleSignUp(BaseSlidingFragmentActivity activity) {
        if (activity == null) {
            return;
        }
        SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
        Intent intent = new Intent(activity, RegisterScreenActivity.class);
        activity.startActivity(intent, false);
    }

    private void handleLogin(BaseSlidingFragmentActivity activity) {
        if (activity == null) {
            return;
        }
        SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
        Intent intent = new Intent(activity, RegisterScreenActivity.class);
        activity.startActivity(intent, false);
    }

    private void handleInstallDeeplink(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null) {
            return;
        }
        String partnerID = uri.getQueryParameter("partner");
        Log.e(TAG, "handleInstallDeeplink: " + partnerID);
        pushPartnerIDApi(activity, partnerID);
    }

    private void pushPartnerIDApi(Context context, String partnerId) {
        UserInfoBusiness user = new UserInfoBusiness(context);
        WsPushInfoPartnerId.Request request = new WsPushInfoPartnerId.Request();
        request.setPartnerId(partnerId);
        request.setDeviceId(DeviceUtils.getAndroidID(context));
        request.setDeviceOS("Android");
        request.setModelDevice(Build.MODEL);
        request.setVersionDevice(Build.VERSION.RELEASE);
        request.setVersionApp(DeviceUtils.getVersionApp(context));
        request.setCamId(String.valueOf(user.getUser().getUser_id()));
        new MetfonePlusClient().wsPushInfoPartner(request);
    }

    private void handleLink(BaseSlidingFragmentActivity activity, Uri uri) {
        if (activity == null || uri == null) {
            return;
        }
        String link = uri.getQueryParameter("link");
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(WebViewActivity.URL_KEY, link);
        activity.startActivity(intent);
    }

    private void handleSelectTab(BaseSlidingFragmentActivity activity, int position) {
        if (activity instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) activity;
            new Thread(() -> {
                try {
                    Thread.sleep(1000);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (homeActivity.getNavigationBar() != null) {
                                homeActivity.getNavigationBar().selectTab(position);
                                homeActivity.getNavigationBar().setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    public static Uri createDeepLinkForNotification(Bundle bundle) {
        String notificationType = bundle.getString("notificationType");
        String actionType = bundle.getString("actionType");
        String objectId = bundle.getString("actionObjectId");
        if ("link".equals(notificationType)) {
            String link = bundle.getString("link");
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_LINK + "?link=" + link);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_PROFILE)) {
            return Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_PROFILE + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_ACTOR)) {
            return Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_FILM_ACTOR + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_DIRECTOR)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_FILM_DIRECTORY + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_CATEGORY)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_FILM_CATEGORY + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.MOVIE_PLAY_DIRECTLY)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_FILM_PLAY + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.ESPORT_CHANNEL_PROFILE)) {
            return Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_ESPORT_CHANNEL + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.ESPORT_PLAY_DIRECTLY)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_ESPORT_PLAY + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.ESPORT_TOURNAMENT_PROFILE)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_ESPORT_TOURNAMENT + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.METFONE_SERVICE)) {
            String isExchange = bundle.getString("isExchange");
            return Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_SERVICE_DETAIL + "?isExchange=" + isExchange + "&id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.METFONE_TOPUP)) {
            return Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_TOPUP);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.REWARD_DETAIL)) {
            return Uri.parse("camid://notification" + DeepLinkHelper.DEEP_LINK.PATH_REWARD_PROFILE + "?id=" + objectId);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.LOGIN)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_LOGIN);
        } else if (actionType.equals(Constants.NOTIFICATION_CAMID.SIGNUP)) {
            return Uri.parse("camid://notification" + DEEP_LINK.PATH_SIGNUP);
        }
        return null;
    }

    public static final class DEEP_LINK {
        public static final String SCHEME = "camid";
        public static final String SCHEME_HTTP = "http";
        public static final String SCHEME_HTTPS = "https";
        public static final String HOST_CALLOUT_GUIDE = "calloutguide";     //dung de truyen len sv
        public static final String HOST_CAM_ID = "camid.app";
        public static final String HOST_CAM_ID_NOTIFICATION = "notification";
        public static final String HOST_CAM_DYNAMIC_LINK = "camid.page.link";
        //          params
        public static final String PARAM_CAPM_ID = "campidpush";
        public static final String PARAM_CAPM_ID_DEEPLINK = "campidpushdeeplink";

        public static final String PARAM_ID = "id";

        //Deeplink and dynamic link
        public static final String PATH_FEEDBACK = "/feedback";
        public static final String PATH_I_SHARE = "/ishare";
        public static final String PATH_BUY_PHONE = "/buynumber";
        public static final String PATH_SERVICE_PROFILE = "/service";
        public static final String PATH_SERVICE_DETAIL = "/detail_service";
        public static final String PATH_TOPUP = "/topup";
        public static final String PATH_ACCOUNT_INFO = "/account_info";
        public static final String PATH_NEAR_BY = "/nearby";
        public static final String PATH_REWARDS = "/reward";
        public static final String PATH_INVITE_FRIENDS = "/invite";
        public static final String PATH_QR_CODE = "/qrcode";
        public static final String PATH_GAME = "/game_munny";

        // Only deeplink
        public static final String PATH_TAB_CINEMA = "/movie";
        public static final String PATH_TAB_ESPORT = "/esport";
        public static final String PATH_TAB_METFONE = "/metfoneplus";
        public static final String PATH_FILM_PROFILE = "/cinema/film";
        public static final String PATH_FILM_PLAY = "/cinema/play";
        public static final String PATH_FILM_CATEGORY = "/cinema/category";
        public static final String PATH_FILM_ACTOR = "/cinema/actor";
        public static final String PATH_FILM_DIRECTORY = "/cinema/directory";
        public static final String PATH_REWARD_PROFILE = "/reward/reward";
        public static final String PATH_REWARD_CATEGORY = "/reward/category";
        public static final String PATH_ESPORT_CHANNEL = "/esport/channel";
        public static final String PATH_ESPORT_TOURNAMENT = "/esport/tournament";
        public static final String PATH_ESPORT_PLAY = "/esport/play";
        public static final String PATH_LINK = "/link";
        public static final String PATH_SIGNUP = "/sign_up";
        public static final String PATH_LOGIN = "/login";
        public static final String PATH_INSTALL = "/install_camid";


    }

    public class DEEPLINK_INTRO_ANONYMOUS {
        public static final String UPLOAD_VIDEO = "mocha://intro?ref=21";
        public static final String CHANNEL_FOLLOW = "mocha://intro?ref=22";
        public static final String MY_CHANNEL = "mocha://intro?ref=23";
        public static final String VIDEO_LIBRARY = "mocha://intro?ref=24";
        public static final String NET_NEWS = "mocha://intro?ref=31";
        public static final String LISTENER_MUSIC = "mocha://intro?ref=61";
        public static final String MOVIE_KEENG = "mocha://intro?ref=41";
        public static final String MOCHA_IQ = "mocha://intro?ref=91";
        public static final String SPOINT = "mocha://intro?ref=92";
        public static final String VQMM = "mocha://intro?ref=93";
        public static final String CALLOUT = "mocha://intro?ref=94";
        public static final String INVITE = "mocha://intro?ref=95";
        public static final String NEAR_FRIEND = "mocha://intro?ref=96";
    }
}
package com.metfone.selfcare.helper;

public class ConfigLocalized {
    public static final String DOMAIN_FILE = "http://hlvip2.mocha.com.vn:80";
    public static final String DOMAIN_MSG = "172.255.193.162:5225";
    public static final String DOMAIN_ON_MEDIA = "http://hlvip2.mocha.com.vn:80";

    public static final String DOMAIN_IMAGE = "pv.mocha.com.vn";                    //default

    public static final String DOMAIN_GEN_OTP = DOMAIN_FILE;
    public static final String DOMAIN_FILE_V1 = DOMAIN_FILE;
    public static final String DOMAIN_IMAGE_V1 = DOMAIN_IMAGE;
    public static final String DOMAIN_ON_MEDIA_V1 = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_MC_VIDEO = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_KMUSIC = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_KMUSIC_SEARCH = "http://mckeeng.recvip.keeng.net:80";
    public static final String DOMAIN_KMOVIES = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_NETNEWS = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_TIIN = DOMAIN_ON_MEDIA;

    public static final String DOMAIN_GEN_OTP_TEST = DOMAIN_FILE;
    public static final String DOMAIN_FILE_V1_TEST = DOMAIN_FILE;
    public static final String DOMAIN_IMAGE_V1_TEST = DOMAIN_IMAGE;
    public static final String DOMAIN_ON_MEDIA_V1_TEST = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_MC_VIDEO_TEST = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_KMUSIC_TEST = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_KMUSIC_SEARCH_TEST = "http://mckeeng.recvip.keeng.net:80";
    public static final String DOMAIN_KMOVIES_TEST = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_NETNEWS_TEST = DOMAIN_ON_MEDIA;
    public static final String DOMAIN_TIIN_TEST = DOMAIN_ON_MEDIA;

    //public static final int PORT_SSL = 5228;
    public static final int PORT_NON_SSL = 5225;
    public static final boolean ENABLE_TLS = true;

    //keeng
    public static final String DOMAIN_SERVICE_KEENG = "vip.service.keeng.vn:8080";
    // free 15days
    public static final String DOMAIN_MEDIA2_KEENG = "vip.media2.keeng.vn:8081";        // free 15days
    public static final String DOMAIN_IMAGE_KEENG = "vip.image.keeng.vn";               // free 15days
    public static final String DOMAIN_LOG_DEVICE = "pvvip.mocha.com.vn:8080";

    public static final boolean PREF_DEF_SETUP_KEYBOARD_SEND = false;
    public static final boolean PREF_DEF_SHOW_MEDIA = false;
    public static final boolean PREF_DEF_REPLY_SMS = true;
}
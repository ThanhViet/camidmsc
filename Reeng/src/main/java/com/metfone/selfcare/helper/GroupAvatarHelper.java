package com.metfone.selfcare.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.LruCache;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.ui.JoinBitmaps;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by HUYPQ on 3/5/2015.
 */
public class GroupAvatarHelper implements Runnable {
    public static final String TAG = GroupAvatarHelper.class.getSimpleName();
    private final Queue<GroupAvatarInfo> mGroupAvatarQueue;
    private HashMap<String, String> mArrayGenAvatarRequest = new HashMap<>();
    private ApplicationController mApplication;
    private int size;
    private Bitmap bitmapDefault;
    //    private Bitmap bitmapGroupDefault;
    private SharedPreferences mPref;
    private Thread mThread = null;
    private boolean done;
    private static GroupAvatarHelper mInstance;

    private LruCache<Integer, Bitmap> bitmapLruCache;

    public static synchronized GroupAvatarHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new GroupAvatarHelper(application);
        }
        return mInstance;
    }

    private GroupAvatarHelper(ApplicationController application) {
        this.mApplication = application;
        this.mGroupAvatarQueue = new LinkedBlockingQueue<>();
        this.bitmapDefault = BitmapFactory.decodeResource(application.getResources(), R.drawable.ic_avatar_default);
//        this.bitmapGroupDefault = BitmapFactory.decodeResource(application.getResources(), R.drawable.ic_avatar_default);
        this.mPref = application.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        this.size = (int) application.getResources().getDimension(R.dimen.avatar_small_size);
        bitmapLruCache = new LruCache<>(2048);
    }

    public int getSize() {
        return size;
    }

    @Override
    public synchronized void run() {
        GroupAvatarInfo groupAvatarInfo;
        while (!done) {
            try {
                Log.i(TAG, "run requestGenGroupAvatar");
                groupAvatarInfo = nextGroupAvatarInfo();
                ThreadMessage threadMessage = groupAvatarInfo.getThreadMessage();
                String currentToken = threadMessage.genAvatarToken(mApplication);
                int threadId = threadMessage.getId();
                Log.d(TAG, "G-" + threadId + " Runnable ");
                // token thay doi hoac chua co token
                if (mApplication.getAvatarBusiness().checkTokenGenAvatarGroup(this, currentToken, threadMessage, size)
                        || groupAvatarInfo.isForceToGenerate()) {
                    Log.d(TAG, "G-" + threadId + " token changed ");
                    ArrayList<Bitmap> mListBitmap = new ArrayList<>();
                    List<PhoneNumber> phoneNumbers = getListPhoneNumber(threadMessage.getPhoneNumbers());
                    String lastChangeAvatar, number;
                    PhoneNumber phoneNumber;
                    // lay 4 phonenumber dau tien co avatar.
                    for (int i = 0; i < phoneNumbers.size() && i < 4; i++) {
                        phoneNumber = phoneNumbers.get(i);
                        if (phoneNumber.isReeng()) {
                            lastChangeAvatar = phoneNumber.getLastChangeAvatar();
                        } else {
                            lastChangeAvatar = null;
                        }
                        number = phoneNumber.getJidNumber();
                        Bitmap bitmap = loadBitmapForGroupAvatar(number, lastChangeAvatar, threadId);
                        mListBitmap.add(bitmap);
                    }
                    invalidListBitmap(groupAvatarInfo, mListBitmap);
                    genAndSaveAvatar(mListBitmap, groupAvatarInfo, threadId);
                }
                // saveRequestGen(threadId + "");
                groupAvatarInfo.dispose();
            } catch (Exception ex) {
                Log.e(TAG, "Exception", ex);
            }
        }
    }

    private void invalidListBitmap(GroupAvatarInfo groupAvatarInfo, ArrayList<Bitmap> mListBitmap) {
        // fill nhung member khong co avatar
        /*for (int i = mListBitmap.size(); i < groupAvatarInfo.getGroupSize(); i++) {
            mListBitmap.add(i, bitmapDefault);
        }*/
        // neu listBitmap.size = 0 hay. co 1 mem ber. thi de avatar mac dinh
        if (mListBitmap.size() <= 1) {
            mListBitmap.set(0, bitmapDefault);
        }
    }

    /**
     * @param groupAvatarInfo
     */
    private void genAndSaveAvatar(ArrayList<Bitmap> mListBitmap, GroupAvatarInfo groupAvatarInfo, int threadId) {
        final Bitmap bitmap_ = JoinBitmaps.join(null, groupAvatarInfo.getViewWith(), mListBitmap, groupAvatarInfo
                .getGroupSize(), 0.15f);
        if (bitmap_ != null) {
            Log.d(TAG, "G-" + threadId + " gen AndSaveAvatar: success ");
            ImageHelper imageHelper = ImageHelper.getInstance(mApplication);
            String pathFile = ImageHelper.getAvatarGroupFilePath(threadId);
            imageHelper.saveBitmapToPath(bitmap_, pathFile, Bitmap.CompressFormat.PNG);
            Log.d(TAG, "G-" + threadId + " gen AndSaveAvatar: clear cache: " + pathFile);
            clearImageCache("file://" + pathFile);
            /*universalImageLoader.clearDiskCache("file://" + pathFile);
            universalImageLoader.clearMemoryCache("file://" + pathFile, new ImageSize(size, size));*/
            //set image
            final ImageView imgAvatar = groupAvatarInfo.getImgGroupAvatar();
            if (imgAvatar.getTag() != null && imgAvatar.getTag().equals(String.valueOf(threadId))) {
                imgAvatar.postInvalidate();
                Handler refresh = new Handler(Looper.getMainLooper());
                refresh.post(new Runnable() {
                    public void run() {
                        imgAvatar.setImageBitmap(bitmap_);
                    }
                });
            }
        }
    }

    private Bitmap loadBitmapForGroupAvatar(String number, String lastChange, final int threadId) {
        Bitmap bitmap = null;
        try {
            if (number != null && lastChange != null && lastChange.length() > 0) {
                final String url = mApplication.getAvatarBusiness().getAvatarUrl(lastChange, number, size);
                if (url != null) {
                    bitmap = Glide.with(mApplication).asBitmap().load(url).into(size, size).get();
//                    universalImageLoader.loadImageSync(url, new ImageSize(size, size));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "setAvatar", e);
        }
        if (bitmap == null)
            bitmap = bitmapDefault;
        return bitmap;
    }

    private GroupAvatarInfo nextGroupAvatarInfo() {
        GroupAvatarInfo groupAvatarInfo;
        while ((groupAvatarInfo = mGroupAvatarQueue.poll()) == null) {
            synchronized (mGroupAvatarQueue) {
                try {
                    mGroupAvatarQueue.wait();
                } catch (InterruptedException ie) {
                    Log.e(TAG, "InterruptedException", ie);
                }
            }
        }
        return groupAvatarInfo;
    }

    public void requestGenGroupAvatar(GroupAvatarInfo groupAvatarInfo) {
        // da request tạo avatar roi.
       /* if (checkRequestGen(groupAvatarInfo.getThreadMessage().getId() + "")) {
            return;
        }*/
        Log.i(TAG, "requestGenGroupAvatar");
        mGroupAvatarQueue.offer(groupAvatarInfo);
        if (mThread == null) {
            done = false;
            mThread = new Thread(this);
            mThread.start();
        } else {
            synchronized (mGroupAvatarQueue) {
                mGroupAvatarQueue.notifyAll();
            }
        }
    }


    private void saveRequestGen(String threadId) {
        Log.d(TAG, "G-" + threadId + " -add RequestGen " + mGroupAvatarQueue.size());
        mArrayGenAvatarRequest.put(threadId, threadId);
    }

    /**
     * khong remove trong
     *
     * @param threadId
     */
    public void removeRequestGenHistory(String threadId) {
        mArrayGenAvatarRequest.remove(threadId);
    }

    private boolean checkRequestGen(String threadId) {
        if (mArrayGenAvatarRequest.get(threadId) != null) {
            Log.d(TAG, "G-" + threadId + " -check RequestGen return");
            return true;
        }
        return false;
    }

    /**
     * lay list PhoneNumber can de gen avatar (uu tien PhoneNumber co anh dai dien)
     *
     * @param listFriends
     * @return listPhoneNumber
     */
    public List<PhoneNumber> getListPhoneNumber(ArrayList<String> listFriends) {
        List<PhoneNumber> listPhoneNumbers = new ArrayList<>();
        int countPhoneNumberHasAvatar = 0;
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        for (String number : listFriends) {
            if (countPhoneNumberHasAvatar > 3) { // lay nhieu nhat la 4 so co avatar.(pos 0-3)
                break;
            }
            PhoneNumber phone = contactBusiness.getPhoneNumberFromNumber(number);
            if (phone != null) {
                if (phone.isReeng() && !TextUtils.isEmpty(phone.getLastChangeAvatar())) {
                    listPhoneNumbers.add(countPhoneNumberHasAvatar, phone);
                    countPhoneNumberHasAvatar++;
                } else {
                    listPhoneNumbers.add(phone);
                }
            } else {
                NonContact nonContact = contactBusiness.getExistNonContact(number);
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE &&
                        !TextUtils.isEmpty(nonContact.getLAvatar())) {
                    listPhoneNumbers.add(countPhoneNumberHasAvatar, contactBusiness.createPhoneNumberFromNonContact
                            (nonContact));
                    countPhoneNumberHasAvatar++;
                } else {
                    phone = new PhoneNumber();
                    phone.setJidNumber(number);
                    listPhoneNumbers.add(phone);
                }
            }
        }
        if (listPhoneNumbers.size() < 4) {// it hon 4 thanh vien thi add them minh vao cuoi
            ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
            PhoneNumber myPhone = new PhoneNumber();
            myPhone.setJidNumber(reengAccount.getJidNumber());
            myPhone.setState(Constants.CONTACT.ACTIVE);
            myPhone.setLastChangeAvatar(reengAccount.getLastChangeAvatar());
            listPhoneNumbers.add(myPhone);
        }
        return listPhoneNumbers;
    }


    /**
     * md5(lastchange avatar cua member) duoc lay ra tu Pref luu truoc do
     *
     * @param threadId
     * @return
     */
    public String getTokenChangeGroupAvatar(int threadId) {
        String prefName = Constants.PREFERENCE.PREF_AVATAR_FILE_GROUP + threadId;
        try {
            return mPref.getString(prefName, "");
        } catch (ClassCastException cce) {
            Log.e(TAG, "ClassCastException", cce);
            mPref.edit().remove(prefName);
            return "";
        }
    }

    /**
     * tam thoi save token chi de remove old avatar
     *
     * @param threadId
     * @param token
     */
    public void setTokenChangeGroupAvatar(int threadId, String token) {
        String prefName = Constants.PREFERENCE.PREF_AVATAR_FILE_GROUP + threadId;
        mPref.edit().putString(prefName, token).apply();
    }

    public void deleteAvatarGroup(ThreadMessage threadMessage) {
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            String pathFile = ImageHelper.getAvatarGroupFilePath(threadMessage.getId());
            if (!TextUtils.isEmpty(pathFile)) {
                File file = new File(pathFile);
                if (file.exists()) {
                    file.delete();
                }
                clearImageCache(pathFile);
            }
        }
    }

    private void clearImageCache(String fileUrl) {
//        File imageFile = universalImageLoader.getDiskCache().get(fileUrl);
        File imageFile = null;
        try {
            imageFile = Glide.with(mApplication).asFile().load(fileUrl).submit().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (imageFile != null && imageFile.exists()) {
            imageFile.delete();
        }
//        MemoryCacheUtils.removeFromCache(fileUrl, universalImageLoader.getMemoryCache());
    }

    @SuppressLint("StaticFieldLeak")
    public synchronized Bitmap genAvatarGroup(final ThreadMessage threadMessage, final GetBitmapAvatarGroupListener listener) {
        Bitmap bmAvatarGroup = bitmapLruCache.get(threadMessage.getId());
        if (bmAvatarGroup != null) {
            return bmAvatarGroup;
        } else {
            new AsyncTask<ThreadMessage, Void, Bitmap>() {

                @Override
                protected Bitmap doInBackground(ThreadMessage... voids) {
                    ThreadMessage thread = voids[0];
                    if (thread == null) return bitmapDefault;
                    String urlAvatar = thread.getGroupAvatar();
                    if (TextUtils.isEmpty(urlAvatar)) {
                        ArrayList<Bitmap> mListBitmap = new ArrayList<>();
                        ArrayList<Object> listAvatar = mApplication.getAvatarBusiness().getlistAvatarMember(thread);
                        for (int i = 0; i < listAvatar.size() && i < 3; i++) {
                            if (thread.getListMemberAvatar().size() > i) {
                                Bitmap bitmap = loadBitmapForGroupAvatar(thread.getListMemberAvatar().get(i));
                                mListBitmap.add(bitmap);
                            }
                        }
                        int viewWidth = (int) mApplication.getResources().getDimension(R.dimen.item_thread_avatar_height);
                        int size = thread.getListAllMemberIncludeAdmin(mApplication).size();
                        if (size == 1) {
                            return bitmapDefault;
                        }
                        if (size > 3) size = 3;
                        Log.d(TAG, "before size bitmap: " + mListBitmap.size() + " size group: " + size);
                        while (mListBitmap.size() < size) {
                            mListBitmap.add(bitmapDefault);
                        }
                        Log.d(TAG, "after size bitmap: " + mListBitmap.size() + " size group: " + size);
                        final Bitmap bitmap = JoinBitmaps.join(null, viewWidth, mListBitmap,
                                size, 0.15f);
                        return bitmap;
                    } else {
                        Bitmap avatarCache = mApplication.getAvatarBusiness().getBitmapFromCache(urlAvatar, thread.getId());
                        if (avatarCache == null) {
                            return loadBitmapForGroupAvatar(urlAvatar);
                        } else {
                            return avatarCache;
                        }
                    }
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    bitmapLruCache.put(threadMessage.getId(), bitmap);
                    listener.onGetBitmapSuccess(bitmap);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, threadMessage);
            return bitmapDefault;
        }
    }

    private Bitmap loadBitmapForGroupAvatar(Object avatar) {
        Bitmap bitmap = null;
        try {
            if (avatar != null) {
                if (avatar instanceof String) {
//                    bitmap = universalImageLoader.loadImageSync((String) avatar, new ImageSize(size, size));
                    bitmap = Glide.with(mApplication).asBitmap().load((String) avatar).into(size, size).get();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "setAvatar", e);
        }
        if (bitmap == null)
            bitmap = bitmapDefault;
        return bitmap;
    }

    public void deleteBitmapCache(int threadId) {
        bitmapLruCache.remove(threadId);
    }

    public Bitmap getBitmapCache(int threadId) {
        return bitmapLruCache.get(threadId);
    }

    public void addBitmapCache(int threadId, Bitmap bitmap) {
        bitmapLruCache.put(threadId, bitmap);
    }

    public void clearBitmapCache() {
        bitmapLruCache.evictAll();
    }

    public void changeInfoAvatarGroup(String jid) {
        Log.i(TAG, "changeInfoAvatarGroup: " + jid);
        CopyOnWriteArrayList<ThreadMessage> list = mApplication.getMessageBusiness().getThreadMessageArrayList();
        for (ThreadMessage threadMessage : list) {
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                String numberString = threadMessage.getNumberSearchGroup();
                if (numberString.contains(jid)) {
                    threadMessage.setForceCalculatorAllMember(true);
                }
            }
        }
    }

    public Bitmap getBitmapDefault() {
        return bitmapDefault;
    }

    public void loadBitmapThreadMessage(ThreadMessage threadMessage, GetBitmapAvatarGroupListener listener) {
        if (threadMessage == null) listener.onGetBitmapSuccess(bitmapDefault);
        else {
            String avatarUrl = null;
            int threadType = threadMessage.getThreadType();
            AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                String friendNumber = threadMessage.getSoloNumber();
                avatarUrl = getFriendAvatarUrl(avatarBusiness, friendNumber);
            } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                if (TextUtils.isEmpty(threadMessage.getGroupAvatar())) {
                    genAvatarGroup(threadMessage, listener);
                    return;
                } else {
                    avatarUrl = avatarBusiness.getGroupAvatarUrl(threadMessage, threadMessage.getGroupAvatar());
                }
            } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
                avatarUrl = mApplication.getOfficerBusiness().getOfficerAvatarByServerId(threadMessage.getServerId());
            }
            // get bitmap
            if (TextUtils.isEmpty(avatarUrl)) {
                listener.onGetBitmapSuccess(bitmapDefault);
            } else {
                int avatarSize = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
                avatarBusiness.loadAvatar(avatarUrl, new GlideImageLoader.ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, GlideException e) {

                    }

                    @Override
                    public void onLoadingComplete(Bitmap loadedImage) {
                        addBitmapCache(threadMessage.getId(), loadedImage);
                        listener.onGetBitmapSuccess(loadedImage);
                    }
                }, avatarSize);
            }
        }
    }

    private String getFriendAvatarUrl(AvatarBusiness avatarBusiness, String friendNumber) {
        String avatarUrl = null;
        int avatarSize = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
        if (phoneNumber != null && phoneNumber.getId() != null) {
            avatarUrl = avatarBusiness.getAvatarUrl(phoneNumber.getLastChangeAvatar(), friendNumber, avatarSize);
        } else {
            NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendNumber);
            if (nonContact != null) {
                avatarUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), friendNumber, avatarSize);
            } else {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.
                        getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
                if (strangerPhoneNumber != null && strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber
                        .StrangerType.other_app_stranger) {
                    avatarUrl = strangerPhoneNumber.getFriendAvatarUrl();
                } else if (strangerPhoneNumber != null) {
                    avatarUrl = avatarBusiness.getAvatarUrl(strangerPhoneNumber.getFriendAvatarUrl(), friendNumber,
                            avatarSize);
                }
            }
        }
        return avatarUrl;
    }

    public interface GetBitmapAvatarGroupListener {
        void onGetBitmapSuccess(Bitmap bitmap);
    }
}
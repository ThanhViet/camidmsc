package com.metfone.selfcare.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;

import com.metfone.esport.homes.HomeFragment;
import com.metfone.esport.ui.games.luckywheel.GameActivity;
import com.metfone.register_payment.ui.fragment.mp_tab_internet.MPTabInternetFragment;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.AVNOActivity;
import com.metfone.selfcare.activity.AddAccountActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.CallHistoryActivity;
import com.metfone.selfcare.activity.CallHistoryDetailActivity;
import com.metfone.selfcare.activity.ChatActivity;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.activity.ContactListActivity;
import com.metfone.selfcare.activity.EnterAddPhoneNumberActivity;
import com.metfone.selfcare.activity.FullFillInfoActivity;
import com.metfone.selfcare.activity.Gift83Activity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.ListDataPackagesActivity;
import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.activity.LoginActivity;
import com.metfone.selfcare.activity.MetFoneServiceActivity;
import com.metfone.selfcare.activity.NewEditProfileActivity;
import com.metfone.selfcare.activity.NewProfileActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.activity.OnMediaListLikeShareActivity;
import com.metfone.selfcare.activity.QRCodeActivity;
import com.metfone.selfcare.activity.RegisterScreenActivity;
import com.metfone.selfcare.activity.SearchSongActivity;
import com.metfone.selfcare.activity.SetUpProfileActivity;
import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.activity.ShakeGameActivity;
import com.metfone.selfcare.activity.ShareAndGetMoreActivity;
import com.metfone.selfcare.activity.TermsConditionActivity;
import com.metfone.selfcare.activity.VerifyInformationActivity;
import com.metfone.selfcare.activity.update_info.AnotherUserInfoActivity;
import com.metfone.selfcare.activity.update_info.ConfirmIdentityInfoActivity;
import com.metfone.selfcare.activity.update_info.FullInformationActivity;
import com.metfone.selfcare.activity.update_info.IDAndPictureInfoActivity;
import com.metfone.selfcare.activity.update_info.VerifyUserActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ReengMessageWrapper;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.model.DetectInfo;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.home_kh.account.AccountDetailActivity;
import com.metfone.selfcare.module.home_kh.activity.QRCodeKhActivity;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.module.home_kh.model.AuthKeyModel;
import com.metfone.selfcare.module.metfoneplus.activity.MPAddPhoneActivity;
import com.metfone.selfcare.module.metfoneplus.activity.MPSelectPhoneActivity;
import com.metfone.selfcare.module.movie.activity.CategoryActivity;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.saving.activity.SavingActivity;
import com.metfone.selfcare.module.security.activity.SecurityActivity;
import com.metfone.selfcare.module.security.model.LogModel;
import com.metfone.selfcare.module.security.model.NewsModel;
import com.metfone.selfcare.module.security.model.VulnerabilityModel;
import com.metfone.selfcare.module.video.model.TabModel;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/11/2017.
 */

public class NavigateActivityHelper {
    private static final String TAG = NavigateActivityHelper.class.getSimpleName();

    public static void navigateToActionView(BaseSlidingFragmentActivity activity, Uri uri) {
        try {
            if (uri != null) {
                Intent webViewIntent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(webViewIntent);
            } else {
                activity.showToast(R.string.e601_error_but_undefined);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            activity.showToast(R.string.e667_device_not_support_function);
        }
    }

    public static void navigateToPlayStore(BaseSlidingFragmentActivity activity, String packageName) {
        if (activity == null || TextUtils.isDigitsOnly(packageName)) return;
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(Constants.HTTP.LINK_MARKET + packageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            Log.e(TAG, "ActivityNotFoundException", anfe);
            try {
                activity.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(Constants.HTTP.LINK_GOOGLE_PLAY + packageName)));
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                activity.showToast(R.string.e666_not_support_function);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            activity.showToast(R.string.e666_not_support_function);
        }
    }

    public static void openAppSMS(BaseSlidingFragmentActivity activity, String number, String body) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", body);
        activity.startActivity(it);
    }

    public static void navigateToRegisterScreenActivity(BaseSlidingFragmentActivity activity, boolean fromHome) {
        Intent intent = new Intent(activity, RegisterScreenActivity.class);
        if (fromHome) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        activity.startActivity(intent, false);
//        activity.finish();
    }

    public static void navigateToRegisterScreenActivity(BaseSlidingFragmentActivity activity, boolean fromHome, EventListener.LoginCallBack loginCallBack) {
        loginCallBack.onActionBeforeGoToLogin();
        navigateToRegisterScreenActivity(activity, fromHome);
    }

    public static void navigateToShareAndGetMoreActivity(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, ShareAndGetMoreActivity.class);
        activity.startActivity(intent, false);
    }


    public static void navigateToHomeScreenActivity(BaseSlidingFragmentActivity activity, boolean fromHome, boolean fromLogin) {
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.putExtra("FROM_LOGIN", fromLogin);
        if (fromHome) {
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent, false);
        } else {
            if (activity instanceof LoginActivity) {
                intent.putExtra(MoviePagerFragment.FROM_LOGIN, true);
            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            activity.startActivity(intent, false);
            activity.finish();
        }
    }

    public static void navigateToLoginActivity(BaseSlidingFragmentActivity activity, boolean fromHome) {
        Intent intent = new Intent(activity, LoginActivity.class);
        if (fromHome) {
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        activity.startActivity(intent, false);
//        activity.finish();
    }

    public static void navigateToSetUpProfileClearTop(Activity activity) {
        Intent intent = new Intent(activity, SetUpProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    public static void navigateToSetUpProfile(Activity activity) {
        Intent intent = new Intent(activity, SetUpProfileActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToMyProfileNew(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, NewProfileActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToTermsAndConditions(Activity activity) {
        Intent intent = new Intent(activity, TermsConditionActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToEsport(Activity activity) {
        Intent intent = new Intent(activity, HomeFragment.class);
        activity.startActivity(intent);
    }

    public static void navigateToMyProfile(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, NewProfileActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToEditProfile(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, NewEditProfileActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToVerifyNewInformation(BaseSlidingFragmentActivity activity, String phone) {
        Intent intent = new Intent(activity, VerifyUserActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, phone);
        activity.startActivity(intent);
    }

    public static void navigateToVerifyInformation(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, VerifyInformationActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToFullFillInfo(BaseSlidingFragmentActivity activity, DetectInfo detectInfo) {
        Intent intent = new Intent(activity, FullFillInfoActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, detectInfo);
        activity.startActivity(intent);
    }

    public static void navigateToFullInfo(BaseSlidingFragmentActivity activity, UserIdentityInfo userIdentityInfo) {
        Intent intent = new Intent(activity, FullInformationActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, userIdentityInfo);
        activity.startActivity(intent);
    }

    public static void navigateToUpdateInfo(BaseSlidingFragmentActivity activity, UserIdentityInfo userIdentityInfo) {
        Intent intent = new Intent(activity, ConfirmIdentityInfoActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, userIdentityInfo);
        activity.startActivity(intent);
    }

    public static void navigateToAnotherUser(BaseSlidingFragmentActivity activity, UserIdentityInfo userIdentityInfo) {
        Intent intent = new Intent(activity, AnotherUserInfoActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, userIdentityInfo);
        activity.startActivity(intent);
    }

    public static void navigateToIDAndPicture(BaseSlidingFragmentActivity activity, UserIdentityInfo userIdentityInfo) {
        Intent intent = new Intent(activity, IDAndPictureInfoActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToServiceScreenActivity(BaseSlidingFragmentActivity activity, int serviceId) {
        Intent intent = new Intent(activity, MetFoneServiceActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, serviceId);
        activity.startActivity(intent, false);
        activity.finish();
    }

    public static void navigateAddLoginActivity(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, EnterAddPhoneNumberActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, EnumUtils.PhoneNumberTypeDef.ADD_LOGIN);
        activity.startActivity(intent, false);

    }

    public static void navigateToAddAccountActivity(BaseSlidingFragmentActivity activity, int serviceId) {
        Intent intent = new Intent(activity, AddAccountActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, serviceId);
        activity.startActivity(intent, false);
        activity.finish();
    }

    public static void navigateToAddAccountResultActivity(MPTabInternetFragment fragment, int serviceId) {
        Intent intent = new Intent(fragment.getActivity(), AddAccountActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, serviceId);
        intent.putExtra(EnumUtils.IS_FROM_METFONE_TAB, true);
        fragment.startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
    }


    /*public static void navigateToMyProfile(BaseSlidingFragmentActivity activity, boolean edit, boolean forceUpIdCard) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        intent.putExtra("is_edit", edit);
        intent.putExtra("force_up_idcard", forceUpIdCard);
        activity.startActivity(intent);

      *//*  Intent intent = new Intent(activity, NewProfileActivity.class);
        activity.startActivity(intent);*//*
    }*/

    public static void navigateToFriendProfile(ApplicationController application, BaseSlidingFragmentActivity activity,
                                               String friendJid, String friendName, String friendChangeAvatar,
                                               String status, int gender) {
        PhoneNumber phoneNumber = application.getContactBusiness().getPhoneNumberFromNumber(friendJid);
        if (phoneNumber != null && phoneNumber.getId() != null) {
            navigateToContactDetail(activity, phoneNumber);
        } else {
            StrangerPhoneNumber strangerPhoneNumber = application.getStrangerBusiness().
                    getExistStrangerPhoneNumberFromNumber(friendJid);
//            navigateToStrangerDetail(activity, strangerPhoneNumber, friendJid, friendName,
//                    friendChangeAvatar, status, gender);
            // TODO: Cambodia version TVV
            navigateToKhAccountDetail(activity, strangerPhoneNumber, friendJid, friendName,
                    friendChangeAvatar, status, gender);
        }
    }

    public static void navigateToContactDetail(BaseSlidingFragmentActivity activity, PhoneNumber phoneNumber) {
        if (phoneNumber != null) {
            String soloNumber = phoneNumber.getJidNumber();
            String jidNumber = ApplicationController.self().getReengAccountBusiness().getJidNumber();
            if (Utilities.notEmpty(soloNumber) && Utilities.notEmpty(jidNumber) && soloNumber.equals(jidNumber)) {
                navigateToMyProfile(activity);
                return;
            }
            Intent contactDetail = new Intent(activity, ContactDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
            bundle.putString(NumberConstant.ID, phoneNumber.getId());
            contactDetail.putExtras(bundle);
            activity.startActivity(contactDetail, true);
        } else {
            activity.showToast(R.string.e601_error_but_undefined);
        }
    }

    // TODO: Cambodia version [START]
    public static void navigateToKhAccountDetail(BaseSlidingFragmentActivity activity,
                                                 StrangerPhoneNumber strangerPhoneNumber, String friendJid, String
                                                         friendName,
                                                 String lcAvatar, String status, int gender) {
        Intent strangerDetail = new Intent(activity, AccountDetailActivity.class);
        Bundle bundle = new Bundle();
        if (strangerPhoneNumber != null) {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
            bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
        } else {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
            bundle.putString(NumberConstant.NAME, friendName);
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, lcAvatar);
            bundle.putString(NumberConstant.STATUS, status);
            bundle.putInt(NumberConstant.GENDER, gender);
        }
        strangerDetail.putExtras(bundle);
        activity.startActivity(strangerDetail, true);
    }
    // TODO: Cambodia version [END]

    public static void navigateToStrangerDetail(BaseSlidingFragmentActivity activity,
                                                StrangerPhoneNumber strangerPhoneNumber, String friendJid, String
                                                        friendName,
                                                String lcAvatar, String status, int gender) {
        Intent strangerDetail = new Intent(activity, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        if (strangerPhoneNumber != null) {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
            bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
        } else {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
            bundle.putString(NumberConstant.NAME, friendName);
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, lcAvatar);
            bundle.putString(NumberConstant.STATUS, status);
            bundle.putInt(NumberConstant.GENDER, gender);
        }
        strangerDetail.putExtras(bundle);
        activity.startActivity(strangerDetail, true);
    }

    public static void navigateToOMOfficialDetail(BaseSlidingFragmentActivity activity, String officialId, String
            officialName, String avatar) {
        Intent strangerDetail = new Intent(activity, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_OFFICIAL_ONMEDIA);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_ID, officialId);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_NAME, officialName);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_AVATAR, avatar);
        strangerDetail.putExtras(bundle);
        activity.startActivity(strangerDetail, true);
    }

    public static void navigateToContactList(BaseSlidingFragmentActivity activity, int type) {
        Intent intent = new Intent(activity, ContactListActivity.class);
        if (type != -1) {
            intent.putExtra(Constants.CONTACT.DATA_FRAGMENT, type);
        }
        activity.startActivity(intent, true);
    }

    /*TYPE_INVITE_MAKE_TURN_LW = 34;
    TYPE_SOS_MAKE_TURN_LW = 35;*/
    public static void navigateToInviteFriends(BaseSlidingFragmentActivity activity, int type) {
        Intent intent = new Intent(activity, ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, new ArrayList<String>());
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, type);
        intent.putExtras(bundle);
        activity.startActivity(intent, true);
    }

    public static void navigateToInviteFriends(BaseSlidingFragmentActivity activity, int type, String ref) {
        Intent intent = new Intent(activity, ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, new ArrayList<String>());
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, type);
        intent.putExtras(bundle);
        activity.startActivity(intent, true);
    }

    public static void navigateToCreateChat(BaseSlidingFragmentActivity activity, int type, int resultCode) {
        Intent chooseFriend = new Intent(activity, ChooseContactActivity.class);
        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, type);
        if (resultCode != -1) {
            activity.startActivityForResult(chooseFriend, resultCode, true);
        } else {
            activity.startActivity(chooseFriend, true);
        }
    }

    public static void navigateToNewFriend(BaseSlidingFragmentActivity activity) {
        Intent chooseFriend = new Intent(activity, ChooseContactActivity.class);
        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_ADD_FRIEND);
        activity.startActivity(chooseFriend, true);
    }

    public static void navigateToChooseContact(BaseSlidingFragmentActivity activity, int type, String selectedNumber,
                                               MediaModel songModel, ArrayList<ReengMessage> listMessage,
                                               boolean isShareOnMedia, boolean isAnimation, int resultCode,
                                               boolean clearTask) {
        ArrayList<String> listChecked = new ArrayList<>();
        Intent chooseFriend = new Intent(activity, ChooseContactActivity.class);
        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, type);
        switch (type) {
            case Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC:
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC, songModel);
                break;
            case Constants.CHOOSE_CONTACT.TYPE_SHARE_FROM_OTHER_APP:
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, 0);
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE, new ReengMessageWrapper
                        (listMessage));
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_SHOW_SHARE_ONMEDIA, isShareOnMedia);
                if (clearTask) {
                    chooseFriend.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_MEMBER, listChecked);
                break;
            case Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND:
                if (selectedNumber != null) {
                    listChecked.add(selectedNumber);
                }
                chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_MEMBER, listChecked);
                break;
            case Constants.CHOOSE_CONTACT.TYPE_SOS_MAKE_TURN_LW:
                break;
        }
        if (resultCode != -1) {
            activity.startActivityForResult(chooseFriend, resultCode, isAnimation);
        } else {
            activity.startActivity(chooseFriend, isAnimation);
        }
    }

    public static void navigateToCallSubscription(ApplicationController application,
                                                  final BaseSlidingFragmentActivity activity) {
        /*activity.showLoadingDialog(null, R.string.waiting);
        CallHistoryHelper.getInstance(application).requestGetCallOutRemain(new CallHistoryHelper
                .GetCallOutRemainListener() {
            @Override
            public void onSuccess(CallSubscription entry) {
                activity.hideLoadingDialog();
                Intent callSubscription = new Intent(activity, CallHistoryDetailActivity.class);
                callSubscription.putExtra(CallHistoryConstant.FRAGMENT_HISTORY_TYPE, CallHistoryConstant
                        .HISTORY_TYPE_SUBSCRIPTION);
                callSubscription.putExtra(CallHistoryConstant.FRAGMENT_CALLOUT_DATA, entry);
                activity.startActivity(callSubscription, true);
            }

            @Override
            public void onError(String desc) {
                activity.hideLoadingDialog();
                activity.showToast(desc, Toast.LENGTH_LONG);
            }
        });*/
        navigateToAVNOManager(activity);
    }

    public static void navigateToSendEmail(ApplicationController application, final BaseSlidingFragmentActivity
            activity) {
        String myNumber = application.getReengAccountBusiness().getJidNumber();
        if (TextUtils.isEmpty(myNumber)) {
            myNumber = "";
        }
        String device = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL;
        String content = String.format(application.getResources().getString(R.string.msg_content_email), myNumber,
                Build.VERSION.RELEASE, BuildConfig.VERSION_NAME, Config.REVISION, device);
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setType("message/rfc822");
        String emailStr;
        if (application.getReengAccountBusiness().isCambodia()) {
            emailStr = activity.getString(R.string.add_email_support_kh);
        } else
            emailStr = activity.getString(R.string.add_email_support);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{emailStr});
        email.putExtra(Intent.EXTRA_SUBJECT, application.getString(R.string.msg_title_email));
        email.putExtra(Intent.EXTRA_TEXT, content);
        activity.startActivity(Intent.createChooser(email, content));
    }

    public static void navigateToSettingActivity(BaseSlidingFragmentActivity activity, int fragmentType) {
        Intent intentSetting = new Intent(activity, SettingActivity.class);
        intentSetting.putExtra(Constants.SETTINGS.DATA_FRAGMENT, fragmentType);
        activity.startActivity(intentSetting, true);
    }

    public static void navigateToSettingKhActivity(BaseSlidingFragmentActivity activity, int fragmentType) {
        Intent intentSetting = new Intent(activity, SettingKhActivity.class);
        intentSetting.putExtra(Constants.SETTINGS.DATA_FRAGMENT, fragmentType);
        activity.startActivity(intentSetting, true);
    }

    public static void navigateToLuckyGameActivity(BaseSlidingFragmentActivity activity, AuthKeyModel model) {
        Intent intentSetting = new Intent(activity, GameActivity.class);
        intentSetting.putExtra(GameActivity.AUTHENTICATION_KEY, model.getAuthenkey());
        intentSetting.putExtra(GameActivity.LINKGAME_KEY, model.getLinkGame());
        intentSetting.putExtra(GameActivity.FBSHARE_KEY, model.getFbShare());
        activity.startActivity(intentSetting, true);
    }

    public static void navigateToChatDetail(BaseSlidingFragmentActivity activity, ThreadMessage thread) {
        if (thread == null) {
            return;
        }
        if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String soloNumber = thread.getSoloNumber();
            String jidNumber = ApplicationController.self().getReengAccountBusiness().getJidNumber();
            if (Utilities.notEmpty(soloNumber) && Utilities.notEmpty(jidNumber) && soloNumber.equals(jidNumber)) {
                navigateToMyProfile(activity);
                return;
            }
        }
        navigateToChatDetail(activity, thread.getId(), thread.getThreadType());
    }

    public static void navigateToChatDetail(BaseSlidingFragmentActivity activity, int threadId, int threadType) {
        navigateToChatDetail(activity, threadId, threadType, false);
    }

    public static void navigateToChatDetail(BaseSlidingFragmentActivity activity, int threadId, int threadType, boolean notshowPin) {
        Log.i(TAG, "-------------------------- navigateToChatDetail");
        Intent intent = new Intent(activity, ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, threadId);
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, threadType);
        bundle.putBoolean(ChatActivity.NOT_SHOW_PIN, notshowPin);
        intent.putExtras(bundle);
        activity.startActivity(intent, true);
    }

    public static void navigateToChatDetail(BaseSlidingFragmentActivity activity, ThreadMessage thread,
                                            ArrayList<ReengMessage> messageToShare) {
        if (thread != null && messageToShare != null && !messageToShare.isEmpty()) {
            Intent intentChat = new Intent(activity, ChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
            bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, thread.getThreadType());
            bundle.putSerializable(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE, thread);
            bundle.putSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE, new ReengMessageWrapper
                    (messageToShare));
            intentChat.putExtras(bundle);
            activity.startActivity(intentChat, true);
        }
    }

    public static void navigateToCallHistory(BaseSlidingFragmentActivity activity) {
        Intent historyIntent = new Intent(activity, CallHistoryActivity.class);
        activity.startActivity(historyIntent, true);
    }

    public static void navigateToCallLogDetail(BaseSlidingFragmentActivity activity, CallHistory history) {
        Intent historyIntent = new Intent(activity, CallHistoryDetailActivity.class);
        historyIntent.putExtra(CallHistoryConstant.FRAGMENT_HISTORY_TYPE, CallHistoryConstant.HISTORY_TYPE_DETAIL);
        historyIntent.putExtra(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID, history.getId());
        activity.startActivity(historyIntent, true);
    }

    public static void navigateToSelectSong(BaseSlidingFragmentActivity activity,
                                            StrangerMusic strangerMusic, int actionCode, boolean search) {
        Intent i = new Intent(activity, SearchSongActivity.class);
        i.putExtra("actionCode", actionCode);
        if (strangerMusic != null) {
            i.putExtra("strangerObj", strangerMusic);
        }
        if (search) {
            i.putExtra(Constants.MOCHA_INTENT.EXTRA_INTENT_SEARCH_USER, true);
        }
        if (actionCode > 0) {
            activity.startActivityForResult(i, actionCode);
        } else {
            activity.startActivity(i, true);
        }
    }

    public static void navigateToWebView(BaseSlidingFragmentActivity activity, FeedModelOnMedia feed, boolean feedFromProfile) {
        String url;
        if (feed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_TOTAL)) {
            url = feed.getFeedContent().getOpenLink();
        } else {
            url = TextUtils.isEmpty(feed.getFeedContent().getContentUrl()) ? feed.getFeedContent().getUrl() : feed
                    .getFeedContent().getContentUrl();
            //            url = feed.getFeedContent().getUrl();
        }
//        boolean isReadNewsFast = feed.getFeedContent().getIsFastNews() == 1;
//        Intent intent = new Intent(activity, isReadNewsFast ? NewsDetailActivity.class : WebViewActivity.class);
//        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
//        activity.startActivity(intent, true);
//        if (isReadNewsFast) {
//            Intent intent = new Intent(activity, NewsDetailActivity.class);
//            intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
//            if (feedFromProfile)
//                intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_PROFILE);
//            activity.startActivity(intent, true);
//        } else {
        Utilities.processOpenLink(ApplicationController.self(), activity, url);
//        }
    }

    public static void navigateToWebView(BaseSlidingFragmentActivity activity, String url) {
        Utilities.openWebView(ApplicationController.self(), activity, url);
    }

    public static void navigateToOnMediaDetail(BaseSlidingFragmentActivity activity, String url, int type, int
            feedFrom, boolean showMenuCopy) {
        Intent intent = new Intent(activity, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, type);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        if (feedFrom >= 0)
            intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);

        activity.startActivity(intent, true);
    }

    public static void navigateToOnMediaDetail(BaseSlidingFragmentActivity activity, String url, int fragment, boolean showMenuCopy, FeedModelOnMedia feed, int feedType) {
        Intent intent = new Intent(activity, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, fragment);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feed);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, feedType);

        activity.startActivity(intent, true);
    }

    public static void navigateToOnMediaLikeOrShare(BaseSlidingFragmentActivity activity, String url, boolean
            typeLike) {
        Intent intent = new Intent(activity, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        if (typeLike) {
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_LIKE);
        } else {
            intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_SHARE);
        }
        activity.startActivity(intent, true);
    }

    public static void navigateToQRScan(BaseSlidingFragmentActivity activity) {
        if (DeviceHelper.isBackCameraAvailable(activity)) {
            Intent qrCode = new Intent(activity, QRCodeActivity.class);
            activity.startActivity(qrCode);
        } else {
            activity.showToast(R.string.qr_err_camera_not_found);
        }
    }

    // TODO: [START] Cambodia version
    public static void navigateToQRScanKh(BaseSlidingFragmentActivity activity) {
        if (DeviceHelper.isBackCameraAvailable(activity)) {
            Intent qrCode = new Intent(activity, QRCodeKhActivity.class);
            activity.startActivity(qrCode);
        } else {
            activity.showToast(R.string.qr_err_camera_not_found);
        }
    }
    // TODO: [END] Cambodia version

    public static void navigateToQRScan(BaseSlidingFragmentActivity activity, int type) {
        if (DeviceHelper.isBackCameraAvailable(activity)) {
            Intent qrCode = new Intent(activity, QRCodeActivity.class);
            qrCode.putExtra("KEY_FROM_SOURCE", type);
            activity.startActivity(qrCode);
        } else {
            activity.showToast(R.string.qr_err_camera_not_found);
        }
    }

    public static void navigateToSelectSongAndListenr(final ApplicationController application,
                                                      final BaseSlidingFragmentActivity activity,
                                                      final String friendNumber, final String friendName, final
                                                      String friendAvatar) {
        application.getMusicBusiness().showConfirmOrNavigateToSelectSong(activity,
                Constants.TYPE_MUSIC.TYPE_PERSON_CHAT,
                friendNumber,
                friendName,
                new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {
                        navigateToSelectSong(activity, Constants.ACTION.SELECT_SONG_AND_CREATE_THREAD, friendNumber,
                                friendName, friendAvatar);
                    }

                    @Override
                    public void onGotoChange() {
                        navigateToSelectSong(activity, Constants.ACTION.CHANGE_SONG_AND_CREATE_THREAD, friendNumber,
                                friendName, friendAvatar);
                    }
                });
    }

    private static void navigateToSelectSong(BaseSlidingFragmentActivity activity, int actionCode,
                                             String friendNumber, String friendName, String friendAvatar) {
        Intent i = new Intent(activity, SearchSongActivity.class);
        i.putExtra("actionCode", actionCode);
        i.putExtra("friendNumber", friendNumber);
        i.putExtra("friendName", friendName);
        i.putExtra("friendAvatar", friendAvatar);
        activity.startActivity(i);
    }

    public static void navigateToMoreApps(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, ListGamesActivity.class);
        intent.putExtra(Constants.GAME.ID_GAME, Constants.GAME.LIST_GAME);
        activity.startActivity(intent);
    }

    public static void navigateToAutoPlayVideo(BaseSlidingFragmentActivity activity, FeedModelOnMedia feed) {
        // TODO: 3/7/2018 auto play video
//        Intent i = new Intent(activity, VideoPlayerActivity.class);
//        i.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
//        i.putExtra(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feed);
//        activity.startActivity(i);
    }

    public static void navigateToShakeGame(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, ShakeGameActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToChangeAVNO(ApplicationController application,
                                            final BaseSlidingFragmentActivity activity, int backToActivity) {
        Intent intent = new Intent(activity, AVNOActivity.class);
        intent.putExtra(AVNOActivity.CALL_BACK_SUCCESS, backToActivity);
        activity.startActivity(intent);
        /*ReengAccount account = application.getReengAccountBusiness().getCurrentAccount();
        Log.d(TAG, "số sim ảo hiện tại : " + account.getAvnoNumber());
        AVNOHelper.getInstance(application).registerAVNONumber("84869440002", new AVNOHelper.RegisterAVNOListener() {
            @Override
            public void onSuccess() {
                activity.showToast(R.string.request_success);
            }

            @Override
            public void onError(String msg) {
                activity.showToast(msg);
            }
        });*/
    }

    public static void navigateToAVNOManager(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, AVNOActivity.class);
        if (ApplicationController.self().getReengAccountBusiness().isAvnoEnable())
            intent.putExtra(AVNOActivity.FRAGMENT_DATA, AVNOActivity.FRAGMENT_AVNO);
        else
            intent.putExtra(AVNOActivity.FRAGMENT_DATA, AVNOActivity.FRAGMENT_MANAGER_CALLOUT);
        activity.startActivity(intent);
    }

    public static void navigateToGift83Activity(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, Gift83Activity.class);
        activity.startActivity(intent);
    }

    public static void navigateToEditContactWithNewNumber(BaseSlidingFragmentActivity activity, String contactID, String newNumber) {
        if (PermissionHelper.declinedPermission(activity, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(activity,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
        } else {
            ContentObserverBusiness.getInstance(activity).setAction(Constants.ACTION.EDIT_CONTACT);
            Intent editContact = new Intent(Intent.ACTION_EDIT);
            editContact.putExtra(ContactsContract.Intents.Insert.PHONE, newNumber);
            editContact.setData(Uri.parse(ContactsContract.Contacts.CONTENT_URI + "/" + contactID));
            editContact.putExtra("finishActivityOnSaveCompleted", true);
            activity.setActivityForResult(true);
            activity.startActivityForResult(editContact, Constants.ACTION.EDIT_CONTACT, true);
        }
    }

    public static void navigateToTabSaving(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, SavingActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.HOME.FUNCTION.TAB_SAVING_STATISTICS);
        activity.startActivity(intent);
    }

    public static void navigateToTabSavingSearch(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, SavingActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.HOME.FUNCTION.TAB_SAVING_SEARCH);
        activity.startActivity(intent);
    }

    public static void navigateToTabSavingHistoryDetail(BaseSlidingFragmentActivity activity, String title, String startDate, String endDate) {
        Log.i(TAG, "navigateToTabSavingHistoryDetail title: " + title + "\nstartDate: " + startDate + "\nendDate: " + endDate);
        Intent intent = new Intent(activity, SavingActivity.class);
        intent.putExtra(Constants.KEY_TITLE, title);
        intent.putExtra(Constants.KEY_START_DATE, startDate);
        intent.putExtra(Constants.KEY_END_DATE, endDate);
        intent.putExtra(Constants.KEY_TYPE, Constants.HOME.FUNCTION.TAB_SAVING_HISTORY_DETAIL);
        activity.startActivity(intent);
    }

    public static void navigateToTabSecurityCenter(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, SecurityActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_CENTER);
        activity.startActivity(intent);
    }

    public static void navigateToTabSecuritySpamSms(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, SecurityActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_SPAM);
        activity.startActivity(intent);
    }

    public static void navigateToTabSecurityFirewallSms(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, SecurityActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_FIREWALL);
        activity.startActivity(intent);
    }

    public static void navigateToTabSecurityVulnerability(BaseSlidingFragmentActivity activity, VulnerabilityModel data) {
        Intent intent = new Intent(activity, SecurityActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_VULNERABILITY);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, data);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void navigateToTabSecurityNews(BaseSlidingFragmentActivity activity, NewsModel data) {
        Intent intent = new Intent(activity, SecurityActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_NEWS);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, data);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void navigateToTabSecuritySmsDetail(BaseSlidingFragmentActivity activity, LogModel data) {
        Intent intent = new Intent(activity, SecurityActivity.class);
        Bundle bundle = new Bundle();
        if (data.isLogSpam()) {
            intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_SPAM_SMS_DETAIL);
            bundle.putString(Constants.KEY_DATA, data.getSrc());
        } else if (data.isLogFirewall()) {
            intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SECURITY_FIREWALL_SMS_DETAIL);
            bundle.putString(Constants.KEY_DATA, data.getDest());
        }
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void navigateToTabMoviesCategoryDetail(BaseSlidingFragmentActivity activity, SubtabInfo data) {
        if (activity == null || activity.isFinishing() || data == null) return;
        Intent intent = new Intent(activity, CategoryActivity.class);
        intent.putExtra(Constants.KEY_DATA, data);
        activity.startActivity(intent);
    }

    public static void navigateToListDataPackages(BaseSlidingFragmentActivity activity) {
        if (activity == null || activity.isFinishing()) return;
        Intent intent = new Intent(activity, ListDataPackagesActivity.class);
        activity.startActivity(intent);
    }

    public static void navigateToTabVideoCategoryDetail(BaseSlidingFragmentActivity activity, TabModel data) {
        if (activity == null || activity.isFinishing() || data == null) return;
        Intent intent = new Intent(activity, com.metfone.selfcare.module.video.activity.CategoryActivity.class);
        intent.putExtra(Constants.KEY_DATA, data);
        activity.startActivity(intent);
    }

    public static void navigateToSendEmail(Activity activity, String email) {
        if (activity == null || email == null) return;
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.setType("message/rfc822");
        sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        activity.startActivity(sendEmailIntent);
    }

    public static void navigateToChatDetail(BaseSlidingFragmentActivity activity, ThreadMessage thread, int srcScreenId) {
        if (thread == null || activity == null || activity.isFinishing()) {
            return;
        }
        if (thread.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String soloNumber = thread.getSoloNumber();
            String jidNumber = ApplicationController.self().getReengAccountBusiness().getJidNumber();
            if (Utilities.notEmpty(soloNumber) && Utilities.notEmpty(jidNumber) && soloNumber.equals(jidNumber)) {
                navigateToMyProfile(activity);
                return;
            }
        }
        Intent intent = new Intent(activity, ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, thread.getThreadType());
        bundle.putBoolean(ChatActivity.NOT_SHOW_PIN, false);
        bundle.putInt(Constants.SRC_SCREEN_ID, srcScreenId);
        intent.putExtras(bundle);
        activity.startActivity(intent, true);
    }

    public static void navigateToMetfoneAddNumber(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, MPAddPhoneActivity.class);
        activity.startActivityForResult(intent, Constants.ACTION.ACTION_ADD_METFONE_NUMBER);
    }

    public static void navigateToSelectPhone(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, MPSelectPhoneActivity.class);
        activity.startActivityForResult(intent, Constants.ACTION.ACTION_SELECT_METFONE_NUMBER);
    }

    public static void navigateToEnterAddPhoneNumber(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, EnterAddPhoneNumberActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, EnumUtils.PhoneNumberTypeDef.ADD_LOGIN);
        activity.startActivity(intent);
    }

    public static void navigateToEnterAddPhoneNumber(BaseSlidingFragmentActivity activity, boolean needChange) {
        Intent intent = new Intent(activity, EnterAddPhoneNumberActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, EnumUtils.PhoneNumberTypeDef.ADD_LOGIN);
        intent.putExtra(EnterAddPhoneNumberActivity.NEED_CHANGE_PARAM, needChange);
        activity.startActivity(intent);
    }

    public interface EventListener {
        interface LoginCallBack {
            void onActionBeforeGoToLogin();
        }
    }
}

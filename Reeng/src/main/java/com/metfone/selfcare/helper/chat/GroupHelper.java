package com.metfone.selfcare.helper.chat;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.metfone.selfcare.database.constant.ThreadMessageConstant.STATE_NEED_GET_INFO;

/**
 * Created by thanhnt72 on 3/23/2018.
 */

public class GroupHelper {

    private static final String TAG = GroupHelper.class.getSimpleName();
    private static final String TAG_GET_LIST_GROUP = "GET_LIST_GROUP";
    private ApplicationController mApp;

    public GroupHelper(ApplicationController mApp) {
        this.mApp = mApp;
    }


    public void getListGroup(final GetListGroupListener listener, int page, int offset) {
        if (listener == null) return;
        if (!NetworkHelper.isConnectInternet(mApp)) {
            listener.onGetError(-1, mApp.getResources().getString(R.string.no_connectivity_check_again));
            return;
        }
        VolleyHelper.getInstance(mApp).cancelPendingRequests(TAG_GET_LIST_GROUP);
        ReengAccount account = mApp.getReengAccountBusiness().getCurrentAccount();
        if (account == null || !mApp.getReengAccountBusiness().isValidAccount()) {
            listener.onGetError(-1, mApp.getResources().getString(R.string.e601_error_but_undefined));
            return;
        }

        String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(account.getJidNumber()).append(account.getToken()).append(timeStamp);

        String url = UrlConfigHelper.getInstance(mApp).getUrlConfigOfFile(Config.UrlEnum.GET_LIST_GROUP);
        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("timestamp", timeStamp);
        params.addParam(Constants.HTTP.DATA_SECURITY,
                HttpHelper.encryptDataV2(mApp, sb.toString(), account.getToken()));


        StringRequest request = new StringRequest(Request.Method.GET, params.toString(), new Response
                .Listener<String>() {
            @Override
            public void onResponse(String s) {
                String decryptData = HttpHelper.decryptResponse(s, mApp.getReengAccountBusiness().getToken());
                if (!TextUtils.isEmpty(decryptData)) {
                    processResponseListGroup(decryptData, listener);
                } else {
                    listener.onGetError(-1, mApp.getResources().getString(R.string.e601_error_but_undefined));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                listener.onGetError(-1, mApp.getResources().getString(R.string.e601_error_but_undefined));
            }
        });
        VolleyHelper.getInstance(mApp).addRequestToQueue(request, TAG_GET_LIST_GROUP, false);
    }

    private void processResponseListGroup(String response, GetListGroupListener listener) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String code = jsonObject.optString("code");
            if (!TextUtils.isEmpty(code) && "200".equals(code)) {
                JSONArray jsonArray = jsonObject.optJSONArray("lst_result");
                ArrayList<ThreadMessage> listGroup = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject js = jsonArray.getJSONObject(i);
                    String id = js.optString("id");
                    String name = js.optString("name");
                    String avatar = js.optString("avatar");
                    String lastActive = js.optString("last_active");
                    ThreadMessage threadMessage = new ThreadMessage();
                    threadMessage.setServerId(id);
                    threadMessage.setName(name);
                    threadMessage.setGroupAvatar(avatar);
                    threadMessage.setThreadType(ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT);
                    threadMessage.setJoined(true);
//                    threadMessage.setLastActive(lastActive);

                    JSONArray listMember = js.optJSONArray("lst_user");
                    ArrayList<String> members = new ArrayList<>();
                    for (int j = 0; j < listMember.length(); j++) {
                        Log.i(TAG, "id group: " + id);
                        JSONObject jsMember = listMember.getJSONObject(j);
                        String msisdn = jsMember.optString("msisdn");
                        members.add(msisdn);
                        /*String avtMember = jsMember.optString("avatar");
                        String nameMember = jsMember.optString("name");*/


                    }
                    long timeActive = TimeHelper.getLongTimeGroup(lastActive);
                    threadMessage.setTimeOfLast(timeActive);
                    threadMessage.setPhoneNumbers(members);
                    listGroup.add(threadMessage);
                }
                mergeListGroup(listGroup, listener);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
            listener.onGetError(-1, mApp.getResources().getString(R.string.e601_error_but_undefined));
        }
    }

    private void mergeListGroup(ArrayList<ThreadMessage> listGroupSv, final GetListGroupListener listener) {
        Log.i(TAG, "mergeListGroup");
        HashMap<String, ThreadMessage> hashMap = new HashMap<>();
        ArrayList<ThreadMessage> listGroupLocal = mApp.getMessageBusiness().getListThreadGroup();
        for (ThreadMessage threadMessage : listGroupLocal) {
            hashMap.put(threadMessage.getServerId(), threadMessage);
        }
        ArrayList<ThreadMessage> listInsertDb = new ArrayList<>();
        for (ThreadMessage threadMessage : listGroupSv) {
            if (!hashMap.containsKey(threadMessage.getServerId())) {
                threadMessage.setState(STATE_NEED_GET_INFO);
                listInsertDb.add(threadMessage);
                hashMap.put(threadMessage.getServerId(), threadMessage);
            }
        }
        if (!listInsertDb.isEmpty()) {
            new AsyncTask<ArrayList<ThreadMessage>, Void, Void>() {

                @Override
                protected Void doInBackground(ArrayList<ThreadMessage>[] arrayLists) {
                    ArrayList<ThreadMessage> threadMessages = arrayLists[0];
                    for (ThreadMessage threadMessage : threadMessages) {
                        mApp.getMessageBusiness().insertThreadMessage(threadMessage);
                        mApp.getMessageBusiness().addThreadMessageToMemory(threadMessage);
                    }
//                    mApp.getMessageBusiness().addAllThreadMessageToMemory(threadMessages);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    listener.onGetSuccess(null);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, listInsertDb);
        } else {
            listener.onGetSuccess(null);
        }
    }


    public interface GetListGroupListener {
        void onGetSuccess(ArrayList<ThreadMessage> listGroup);

        void onGetError(int code, String msg);
    }


}

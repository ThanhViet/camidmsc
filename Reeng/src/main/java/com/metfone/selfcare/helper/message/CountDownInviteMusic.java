package com.metfone.selfcare.helper.message;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengMessage;

import java.io.Serializable;
import java.util.Timer;

/**
 * Created by vtsoft on 12/1/2014.
 */

public class CountDownInviteMusic implements Serializable {

    private static final String TAG = CountDownInviteMusic.class.getSimpleName();
    private ReengMessage mReengMessage;
    private int duration;
    private transient Timer mCountDownTimer;
    private ApplicationController mApp;

    public CountDownInviteMusic(ReengMessage reengMessage, ApplicationController mApp) {
        mReengMessage = reengMessage;
        this.mApp = mApp;
        duration = mReengMessage.getDuration();
        //        mReengMessage.setDuration(duration);
    }

   /* public void startCountDown() {
        Log.d("------------", "startCountDown:");
        mCountDownTimer = new Timer();
        mCountDownTimer.schedule(new CountDownTask(), 0, CountDownInviteManager.COUNT_DOWN_PERIOD);
    }

    public void stopCountDown() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    class CountDownTask extends TimerTask {
        @Override
        public void run() {
            processCountDown();
        }
    }

    private void processCountDown() {
        try {
            duration = duration - CountDownInviteManager.COUNT_DOWN_PERIOD;
            if (duration <= 0) {
                stopCountDown();
                // update db
                if (mReengMessage != null && mApp != null) {
                    mApp.getMessageBusiness().updateSendInviteMessageTimeout(mReengMessage);
                }
                duration = 0;
            }
            if (mReengMessage == null) {
                return;
            }
            final View convertView = CountDownInviteManager.getConvertViewByMessageId(mReengMessage.getId());
            if (convertView != null) {
                int messageId = CountDownInviteManager.getMessageIdByConvertView(convertView);
                if (messageId == mReengMessage.getId()) {
                    //Log.i(TAG, " same messageId " + messageId);
                    final String time = StopWatch.convertSecondInMMSSForm(true, duration / 1000);
                    if (CountDownInviteManager.getHandler() != null) {
                        CountDownInviteManager.getHandler().post(new Runnable() {
                            @Override
                            public void run() {
                                TextView elapsedTimeView = (TextView) convertView.findViewById(R.id.count_down_time);
                                RelativeLayout userActionLayout = (RelativeLayout) convertView.findViewById(R.id
                                        .layout_user_action_music);
                                RelativeLayout reInviteLayout = (RelativeLayout) convertView.findViewById(R.id
                                        .layout_reinvite_music);
                                if (elapsedTimeView != null) {
                                    elapsedTimeView.setText(time);
                                    // tin nhan gui thi ko hien thoi gian
                                    if (mReengMessage.getDirection() == ReengMessageConstant.Direction.send) {
                                        elapsedTimeView.setVisibility(View.INVISIBLE);
                                    } else {
                                        elapsedTimeView.setVisibility(View.VISIBLE);
                                    }
                                }
                                // hien hoac an layout button
                                if (duration <= 0) {
                                    userActionLayout.setVisibility(View.GONE);
                                    reInviteLayout.setVisibility(View.VISIBLE);
                                } else {
                                    userActionLayout.setVisibility(View.VISIBLE);
                                    reInviteLayout.setVisibility(View.GONE);
                                }
                            }
                        });
                    }
                }
            }
            mReengMessage.setDuration(duration);
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }*/
}
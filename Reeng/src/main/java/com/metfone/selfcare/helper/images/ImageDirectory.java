package com.metfone.selfcare.helper.images;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by huybq on 11/10/2014.
 */
public class ImageDirectory implements Serializable {

    //
    public static final String PATH_ALL_IMAGE = "all_image";

    private String parentName = null;
    private String parentPath = null;
    private int countSelectedImage;

    private boolean currentSelect;
    private boolean fromAsset;


    private ArrayList<ImageInfo> listFile = new ArrayList<>();

    public ImageDirectory() {
    }

    public ImageDirectory(ImageInfo imageInfo) {
        listFile.add(imageInfo);
    }

    public ImageDirectory(ArrayList<ImageInfo> listFile) {
        this.listFile = listFile;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentName() {
        return parentName;
    }

    public ImageInfo getFirstImage() {
        if (!listFile.isEmpty()) {
            if (fromAsset && listFile.size() >= 2)
                return listFile.get(1);
            return listFile.get(0);
        }
        return null;
    }

    public void addListFile(ImageInfo imageInfo) {
        this.listFile.add(imageInfo);
    }

    public ArrayList<ImageInfo> getListFile() {
        return listFile;
    }

    public int getCountSelectedImage() {
        return countSelectedImage;
    }

    public void setCountSelectedImage(int countSelectedImage) {
        if (countSelectedImage < 0) {
            this.countSelectedImage = 0;
        } else
            this.countSelectedImage = countSelectedImage;
    }

    public boolean isCurrentSelect() {
        return currentSelect;
    }

    public void setCurrentSelect(boolean currentSelect) {
        this.currentSelect = currentSelect;
    }

    public void setFromAsset(boolean fromAsset) {
        this.fromAsset = fromAsset;
    }
}
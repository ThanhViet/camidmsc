package com.metfone.selfcare.helper.message;

import android.os.AsyncTask;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 12/13/2017.
 */

public class MarkReadMessageAsyncTask extends AsyncTask<Void, Void, Void> {
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mActivity;
    private CopyOnWriteArrayList<ThreadMessage> mThreadMessages;
    private boolean isbackAfterComplete = false;

    public MarkReadMessageAsyncTask(ApplicationController application,
                                    BaseSlidingFragmentActivity activity,
                                    CopyOnWriteArrayList<ThreadMessage> threadMessages, boolean isbackAfterComplete) {
        this.mActivity = activity;
        this.mApplication = application;
        this.mThreadMessages = threadMessages;
        this.isbackAfterComplete = isbackAfterComplete;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.showLoadingDialog(null, R.string.waiting);
    }

    @Override
    protected Void doInBackground(Void... params) {
        mApplication.getMessageBusiness().markReadMessage(mThreadMessages);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        mActivity.hideLoadingDialog();
        mApplication.getMessageBusiness().refreshThreadWithoutNewMessage(-1);
        mApplication.updateCountNotificationIcon();
        if (isbackAfterComplete) {
            mActivity.onBackPressed();
        }
    }
}

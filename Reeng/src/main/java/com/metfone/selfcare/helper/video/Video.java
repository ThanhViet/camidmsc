package com.metfone.selfcare.helper.video;

import java.io.Serializable;

/**
 * Created by tungt on 8/13/2015.
 */
public class Video implements Serializable {
    private static final long serialVersionUID = 1L;
    private int messId;
    private String filePath;
    private String fileOutputPath;
    private String userNumber;
    private String friendNumber;
    private int videoDuration;
    private int threadId;
    private int threadType;
    private int fileSize;
    private String videoResolution;

    public int getMessId() {
        return messId;
    }

    public void setMessId(int messId) {
        this.messId = messId;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getFriendNumber() {
        return friendNumber;
    }

    public void setFriendNumber(String friendNumber) {
        this.friendNumber = friendNumber;
    }

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public int getThreadType() {
        return threadType;
    }

    public void setThreadType(int threadType) {
        this.threadType = threadType;
    }

    public Video() {
    }

    public Video(String filePath, int duration, String resolution) {
        this.filePath = filePath;
        this.videoDuration = duration;
        this.videoResolution = resolution;
    }

    public Video(String filePath, String fileOutputPath, int videoDuration, int fileSize, String videoResolution) {
        this.filePath = filePath;
        this.fileOutputPath = fileOutputPath;
        this.videoDuration = videoDuration;
        this.fileSize = fileSize;
        this.videoResolution = videoResolution;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getDuration() {
        return videoDuration;
    }

    public void setDuration(int duration) {
        this.videoDuration = duration;
    }

    public String getResolution() {
        return videoResolution;
    }

    public void setResolution(String resolution) {
        this.videoResolution = resolution;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileOutputPath() {
        return fileOutputPath;
    }

    public void setFileOutputPath(String fileOutputPath) {
        this.fileOutputPath = fileOutputPath;
    }
}

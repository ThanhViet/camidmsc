package com.metfone.selfcare.helper;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Toast;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 6/10/2015.
 */
public class MoreAppInteractHelper {

    private static final String TAG = MoreAppInteractHelper.class.getSimpleName();

    public static String getKeengPacketName(ApplicationController application) {
        return application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.PREF_KEENG_PACKAGE);
    }

    public static void onKeengLogoClick(BaseSlidingFragmentActivity mParentActivity,
                                        ApplicationController application, String keengPackageId) {
        // String keengPacketId = getKeengPacketName(application);
        openMoreApp(mParentActivity, keengPackageId);
    }

    public static void onPlayMediaClick(BaseSlidingFragmentActivity activity,
                                        MediaModel mediaModel, ApplicationController application, boolean logGa) {
        String mediaUrl = mediaModel.getUrl();
        if (TextUtils.isEmpty(mediaUrl)) {
            activity.showToast(activity.getResources().getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
            return;
        }
        // log feed
        application.getKeengProfileBusiness().logListenTogether(mediaModel);
        if (logGa) {
            activity.trackingEvent(R.string.ga_category_keeng, R.string.ga_action_interaction, R.string.ga_label_listen_song);
        }
        Intent i;
        //PackageManager manager = activity.getPackageManager();
        try {
            //i = manager.getLaunchIntentForPackage(Constants.KEENG_MUSIC.KEENG_PACKAGE_NAME);
            i = new Intent(Intent.ACTION_VIEW, Uri.parse(mediaUrl));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //i.setPackage(Constants.PACKET_NAME.KEENG_VIETNAM);
            //i.setAction(Intent.ACTION_VIEW);
            //i.setData(Uri.parse(mediaUrl));
            //            i.addCategory(Intent.CATEGORY_LAUNCHER);
            activity.startActivity(i);
        } catch (Exception e) {
            Log.e(TAG, "PackageManager.NameNotFoundException", e);
            UrlConfigHelper.openBrowser(activity, mediaUrl);
        }
    }

    public static void openMoreApp(BaseSlidingFragmentActivity baseActivity, String appPacketId) {
        Intent i;
        PackageManager manager = baseActivity.getPackageManager();
        try {
            i = manager.getLaunchIntentForPackage(appPacketId);
            if (i == null)
                throw new PackageManager.NameNotFoundException();
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            baseActivity.startActivity(i);
        } catch (Exception e) {
            Log.e(TAG, "NameNotFoundException", e);
            try {
                baseActivity.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(Constants.HTTP.LINK_MARKET + appPacketId)));
            } catch (ActivityNotFoundException e1) {
                Log.e(TAG, "ActivityNotFoundException", e1);
                baseActivity.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(Constants.HTTP.LINK_GOOGLE_PLAY + appPacketId)));
            }
        }
    }

    public static boolean appInstalled(BaseSlidingFragmentActivity activity, String uri) {
        boolean app_installed;
        try {
            PackageManager pm = activity.getPackageManager();
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "PackageManager.NameNotFoundException", e);
            app_installed = false;
        }
        return app_installed;
    }
}
package com.metfone.selfcare.helper.httprequest;

import android.net.Uri;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.onmedia.SieuHaiModel;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sonnn00 on 8/22/2017.
 */

public class MediaRequestHelper {
    private static final String TAG = MediaRequestHelper.class.getSimpleName();
    private static final String TAG_GET_LIST_VIDEO = "TAG_GET_LIST_VIDEO";
    private static final String TAG_GET_DETAIL_VIDEO = "TAG_GET_DETAIL_VIDEO";
    private static MediaRequestHelper instance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;

    private MediaRequestHelper(ApplicationController application) {
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
    }

    public static synchronized MediaRequestHelper getInstance(ApplicationController application) {
        if (instance == null) {
            instance = new MediaRequestHelper(application);
        }
        return instance;
    }

    public void getListVideoNext(String keyword, final GetListIdNextVideoInterface listener, int page) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_LIST_VIDEO);
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        String nameVideo = Uri.encode(keyword);
        // keyword = keyword.replaceAll(" ", "%20");
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.SEARCH_VIDEO_NEXT),
                numberEncode, nameVideo);
        url = url + "&page=" + page;
        Log.d(TAG, "getListVideoNext url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse getListVideoNext: " + s);
                try {
                    JSONObject responseObject = new JSONObject(s);
                    JSONObject responseContent = new JSONObject(responseObject.optString("response"));
                    JSONArray jsonArray = responseContent.optJSONArray("docs");
                    if (jsonArray.length() == 0) throw new Exception();
                    ArrayList<SieuHaiModel> listVideo = new ArrayList<>();
                    ArrayList<String> listIdVideo = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject js = jsonArray.getJSONObject(i);
                        String idVideo = js.optString("id");
//                        String name = jsonObject.optString("name");
//                        String image = jsonObject.optString("image");
                        long id = js.optLong("id");
                        String title = js.optString("name");
                        String des = js.optString("description");
                        String videoUrl = js.optString("original_path");
                        String imageUrl = js.optString("image");
                        String timeCreate = js.optString("created_at");
                        String link = js.optString("link");
                        long countView = js.optInt("isView", -1);
                        if (!TextUtils.isEmpty(idVideo)) {
                            listIdVideo.add(idVideo);
                            SieuHaiModel sieuHaiModel = new SieuHaiModel(id, title, des, videoUrl, imageUrl,
                                    timeCreate, link, countView);
                            listVideo.add(sieuHaiModel);
                        }

//                        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(videoUrl)) {
//                            listener.onErrorGetDetailVideo(-2); //-2 thi phai get lai detail

//                        }

                    }
                    listener.onResponseGetListIdVideo(listIdVideo, listVideo);
                } catch (Exception e) {
                    Log.e(TAG, "Exception:", e);
                    listener.onErrorGetListIdVideo(-1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                listener.onErrorGetListIdVideo(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_LIST_VIDEO, false);
    }


    public void getDetailVideoSieuHai(String id, final GetDetailVideoListener listener) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_DETAIL_VIDEO);
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        String idEncode = HttpHelper.EncoderUrl(id);
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.DETAIL_VIDEO_NEXT),
                idEncode, numberEncode);
        Log.d(TAG, "getDetailVideoSieuHai url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.optJSONArray("result");
                    if (jsonArray.length() == 0) {
                        listener.onErrorGetDetailVideo(-1);
                        return;
                    }
                    JSONObject js = jsonArray.getJSONObject(0);
                    long id = js.optLong("id");
                    String title = js.optString("name");
                    String des = js.optString("description");
                    String videoUrl = js.optString("original_path");
                    String imageUrl = js.optString("image_path");
                    String timeCreate = js.optString("created_at");
                    String link = js.optString("link");
                    long countView = js.optInt("isView", -1);
                    if (TextUtils.isEmpty(title) || TextUtils.isEmpty(videoUrl)) {
                        listener.onErrorGetDetailVideo(-2); //-2 thi phai get lai detail
                        return;
                    }
                    SieuHaiModel sieuHaiModel = new SieuHaiModel(id, title, des, videoUrl, imageUrl, timeCreate, link, countView);
                    listener.onResponseGetDetailVideo(sieuHaiModel);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onErrorGetDetailVideo(-1);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                listener.onErrorGetDetailVideo(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_DETAIL_VIDEO, false);
    }

    public interface GetDetailVideoListener {
        void onResponseGetDetailVideo(SieuHaiModel sieuHaiModel);

        void onErrorGetDetailVideo(int errorCode);
    }

    public interface GetListIdNextVideoInterface {
        void onResponseGetListIdVideo(ArrayList<String> listIdVideo, ArrayList<SieuHaiModel> listFeedVideo);

        void onErrorGetListIdVideo(int errorCode);
    }

}

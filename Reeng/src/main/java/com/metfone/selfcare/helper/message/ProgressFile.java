package com.metfone.selfcare.helper.message;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.ui.DonutProgress;
import com.metfone.selfcare.util.Log;

import java.io.Serializable;

/**
 * Created by thanhnt72 on 11/30/2016.
 */
public class ProgressFile implements Serializable {

    private static final String TAG = ProgressFile.class.getSimpleName();

    private ReengMessage mReengMessage;
    private Context mContext;

    public ProgressFile(ReengMessage reengMessage, Context context) {
        mReengMessage = reengMessage;
        mContext = context;
    }

    public void showProgressSecond() {
        if (mReengMessage == null) {
            return;
        }
        final View convertView = ProgressFileManager.getConvertViewByMessageId(mReengMessage.getId());
        if (convertView != null) {
            int messageId = ProgressFileManager.getMessageIdByConvertView(convertView);
            if (messageId == mReengMessage.getId()) {
                if (ProgressFileManager.getHandler() == null) ProgressFileManager.initHandler();

                ProgressFileManager.getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        final View vLoading = convertView.findViewById(R.id.message_detail_file_loading);
                        DonutProgress mProgressFirst = (DonutProgress) convertView.findViewById(R.id.progress_bar_first);
                        DonutProgress mProgressSecond = (DonutProgress) convertView.findViewById(R.id.progress_bar_second);
                        vLoading.setVisibility(View.VISIBLE);
                        mProgressFirst.setVisibility(View.GONE);
                        mProgressSecond.setVisibility(View.VISIBLE);
                        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(mContext, R.animator.progress_anim);
                            /*AnimatorSet set = new AnimatorSet();
                            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(mProgressSecond, "progress", 10f, 100f);
                            ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(mProgressSecond, "alpha", 0.1f, 1.0f);
                            objectAnimator1.setDuration(700);
                            objectAnimator2.setDuration(700);
                            set.playTogether(objectAnimator1, objectAnimator2);*/


                        set.setInterpolator(new DecelerateInterpolator());
                        set.setTarget(mProgressSecond);
                        set.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                Log.i(TAG, "onAnimationStart");
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                Log.i(TAG, "onAnimationEnd");
                                vLoading.setVisibility(View.GONE);
                                mReengMessage.setPlaying(false);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                                mReengMessage.setPlaying(false);
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        set.start();
                    }
                });
            } else {
                Log.i(TAG, "wtf, messageId != mReengMessage.getId()");
                mReengMessage.setPlaying(false);
            }
        } else {
            mReengMessage.setPlaying(false);
            Log.i(TAG, "convertView null");
        }
    }
}

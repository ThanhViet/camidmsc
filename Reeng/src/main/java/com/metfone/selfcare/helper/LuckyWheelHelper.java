package com.metfone.selfcare.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.activity.ListGamesActivity;
import com.metfone.selfcare.activity.SettingActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.WheelSpin;
import com.metfone.selfcare.helper.Constants.LUCKY_WHEEL;
import com.metfone.selfcare.helper.home.TabHomeHelper.HomeTab;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.ui.dialog.PopupLuckyWheel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by toanvk2 on 9/6/2016.
 */
public class LuckyWheelHelper extends BaseApi {
    private static final String TAG = LuckyWheelHelper.class.getSimpleName();
    private static final String TAG_GET_VALUE_BUDGET = "TAG_GET_VALUE_BUDGET";
    private static LuckyWheelHelper mInstance;
    public static final int PAUSE_GAME = 410;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private SharedPreferences mPref;
    private Resources mRes;
    private WheelSpin currentWheelSpin;// lưu lại đối tượng wheel spin
    private int newItemId = -1;// lưu id lan quay gan nhat

    private int currentLottPoint = 0;
    private String domain;
    public static final String TIME_MISSION = "TIME_MISSION";

    private static final int[] WHEEL_SPIN = {
            // TODO: 5/20/2020 Thay the phan moi
//            LUCKY_WHEEL.ITEM_LISTEN_STRANGER,
//            LUCKY_WHEEL.ITEM_POINT_1000,
//            LUCKY_WHEEL.ITEM_UPLOAD_ALBUM,
//            LUCKY_WHEEL.ITEM_SECRET,
//            LUCKY_WHEEL.ITEM_MONEY_100,  // 0
//            LUCKY_WHEEL.ITEM_MOCHA_VIP_1,
//            LUCKY_WHEEL.ITEM_POINT_100,
//            LUCKY_WHEEL.ITEM_MOCHA_VIP_7,
//            LUCKY_WHEEL.ITEM_MONEY_10,
//            LUCKY_WHEEL.ITEM_STATUS,
//            LUCKY_WHEEL.ITEM_INVITE_FRIEND,
//            LUCKY_WHEEL.ITEM_SHARE_ONMEDIA,
//            LUCKY_WHEEL.ITEM_BUDGET_GOLD,
//            LUCKY_WHEEL.ITEM_MAKE_FRIEND,
//            LUCKY_WHEEL.ITEM_POINT_500,

            LUCKY_WHEEL.ITEM_POINT_1000,
            LUCKY_WHEEL.ITEM_VIDEO_CALL,
            LUCKY_WHEEL.ITEM_LISTEN_STRANGER,
            LUCKY_WHEEL.ITEM_POINT_100,
            LUCKY_WHEEL.ITEM_TALK_STRANGER,
            LUCKY_WHEEL.ITEM_GROUP_SURVEY,
            LUCKY_WHEEL.ITEM_POINT_500,
            LUCKY_WHEEL.ITEM_LISTEN_MUSIC_KEENG,
            LUCKY_WHEEL.ITEM_UPLOAD_ALBUM,
            LUCKY_WHEEL.ITEM_POINT_2000,
            LUCKY_WHEEL.ITEM_WATCH_VIDEO,
            LUCKY_WHEEL.ITEM_SECRET,
            LUCKY_WHEEL.ITEM_BUDGET_GOLD,
            LUCKY_WHEEL.ITEM_SMS_OUT,
            LUCKY_WHEEL.ITEM_CALL_OUT
    };/*id spin item*/

    public static synchronized LuckyWheelHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new LuckyWheelHelper(application);
        }
        return mInstance;
    }

    private LuckyWheelHelper(ApplicationController application) {
        super(application);
        this.mApplication = application;
        this.mRes = mApplication.getResources();
        this.mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        this.domain = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_GAME_LUCKYWHEEL); // TODO: 6/2/2020 bổ sung
        init();
    }

    private void init() {
        currentLottPoint = mPref.getInt(Constants.PREFERENCE.PREF_LUCKEY_WHEEL_LAST_LOTTPOINT, 0);
        String data = mPref.getString(Constants.PREFERENCE.PREF_LUCKEY_WHEEL_LAST_SPIN, null);
        if (TextUtils.isEmpty(data)) {
            currentWheelSpin = null;
        } else {
            try {
                JSONObject json = new JSONObject(data);
                currentWheelSpin = new WheelSpin(json);
            } catch (Exception e) {
                currentWheelSpin = null;
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public int getCurrentLottPoint() {
        return currentLottPoint;
    }

    public static int getPositionWheelSpin(int spinId) {
        for (int i = 0; i < 15; i++) {
            if (WHEEL_SPIN[i] == spinId) {
                return i;
            }
        }
        return -1;
    }

    public boolean isExistMission() {
        if (currentWheelSpin != null) {
            return Constants.LUCKY_WHEEL.containMission(currentWheelSpin.getId());
        }
        return false;
    }

    public void navigateToLuckyWheel(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, ListGamesActivity.class);
        intent.putExtra(Constants.GAME.ID_GAME, Constants.GAME.VQMM_ID);
        activity.startActivity(intent, true);
    }

    private void insertMission(WheelSpin spin, String json) {
        mApplication.logDebugContent("----LUCKYWHEEL---- insertMission: " + json);
        currentWheelSpin = spin;
        mPref.edit().putString(Constants.PREFERENCE.PREF_LUCKEY_WHEEL_LAST_SPIN, json).apply();
    }

    public void doMission(int actionId) {
        if (currentWheelSpin != null && currentWheelSpin.getId() == actionId) {
            mApplication.logDebugContent("----LUCKYWHEEL----doMission : " + actionId);
            logDoneMission();
            mApplication.trackingEvent(R.string.ga_category_lucky_wheel, R.string.ga_action_interaction, R.string
                    .ga_label_lucky_wheel_do_mission);
        }
    }

    public void showPopupLuckyWheel(final BaseSlidingFragmentActivity activity) {
        if (newItemId == -1 || currentWheelSpin == null) {
            showPopupError(activity, mRes.getString(R.string.request_send_error));
        } else {
            showPopupLuckyWheel(activity, currentWheelSpin.getIconUrl(),
                    currentWheelSpin.getTitle(), currentWheelSpin.getDesc(),
                    currentWheelSpin.getGuideLabel(), currentWheelSpin.getActionLabel(),
                    currentWheelSpin.getGuideDesc(), currentWheelSpin.getId());
        }
    }

    private void showPopupLuckyWheel(final BaseSlidingFragmentActivity activity, final String iconUrl, final String title,
                                     String desc, String guideLabel, final String missionLabel,
                                     final String guideDesc, final int missionId) {
        if (activity == null || activity.isFinishing()) return;
        PopupLuckyWheel popupLuckyWheel = new PopupLuckyWheel(activity)
                .setIconUrl(iconUrl)
                .setTitle(title)
                .setDesc(desc);
        if (Constants.LUCKY_WHEEL.containMission(missionId)) {
            popupLuckyWheel.setNegative(guideLabel, new PopupLuckyWheel.NegativeListener() {
                @Override
                public void onNegative() {
                    showPopupGuide(activity, guideDesc, missionLabel, missionId, iconUrl, title);
                }
            });
        } /* else {
            handleMessageAfterStopSpin(currentWheelSpin.getFakeOAMessageGift());
        }*/
        popupLuckyWheel.setPositive(missionLabel, new PopupLuckyWheel.PositiveListener() {
            @Override
            public void onPositive() {
                Log.d(TAG, "onPositive  - missionId: " + missionId);
                if (Constants.LUCKY_WHEEL.containMissionV2(missionId)) {
                    handleGuideMission(activity, missionId);
                }
            }
        });
        // TODO: 6/4/2020 Ban OA fake cho luong nhan điểm Spoint
        if (Constants.LUCKY_WHEEL.containItemShareFacebook(missionId)) {
            popupLuckyWheel.setClose(new PopupLuckyWheel.CloseListener() {
                @Override
                public void onClose() {
                    MessageBusiness messageBusiness = mApplication.getMessageBusiness();
                    ThreadMessage thread = messageBusiness.findExistingOrCreateOfficialThread();
                    String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
                    ReengMessage reengMessage = messageBusiness.createFakeMessageDeepLink(thread.getId(),
                            thread.getServerId(), myNumber, currentWheelSpin.getDescriptionClosePopup(),
                            currentWheelSpin.getActionClosePopup(),
                            currentWheelSpin.getDeepLink(),
                            "",
                            "");
                    messageBusiness.notifyReengMessage(mApplication, thread, reengMessage, ThreadMessageConstant
                            .TYPE_THREAD_OFFICER_CHAT);
                }
            });
        }
        popupLuckyWheel.show();
    }

    public void showPopupGuide(final BaseSlidingFragmentActivity activity, String guideDesc,
                               String missionLabel, final int missionId, String iconUrl, String title) {
        if (activity == null || activity.isFinishing()) return;
        PopupLuckyWheel popupLuckyWheel = new PopupLuckyWheel(activity)
                .setDesc(guideDesc)
                .setIconUrl(iconUrl)
                .setTitle(title)
                .setPositive(missionLabel, new PopupLuckyWheel.PositiveListener() {
                    @Override
                    public void onPositive() {
                        Log.d(TAG, "missionId: " + missionId);
                        handleGuideMission(activity, missionId);
                    }
                });
        popupLuckyWheel.show();
    }

    public void showPopupOutOfTurn(final BaseSlidingFragmentActivity activity) {
        if (activity == null || activity.isFinishing()) return;
        PopupLuckyWheel popupLuckyWheel = new PopupLuckyWheel(activity)
                .setIconResource(R.drawable.ic_lw_out_of_turn)
                .setDesc(mRes.getString(R.string.lucky_wheel_out_of_turn))
                .setNeutral(mRes.getString(R.string.lucky_wheel_invite_friends), new PopupLuckyWheel.NeutralListener() {
                    @Override
                    public void onNeutral() {
                        Log.d(TAG, "navigate to invite friends");
                        NavigateActivityHelper.navigateToInviteFriends(activity, Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND);
                    }
                }).setPositive(mRes.getString(R.string.lucky_wheel_sos_friends), new PopupLuckyWheel.PositiveListener
                        () {
                    @Override
                    public void onPositive() {
                        Log.d(TAG, "navigate to só friends");
                        NavigateActivityHelper.navigateToInviteFriends(activity, Constants.CHOOSE_CONTACT.TYPE_SOS_MAKE_TURN_LW);
                    }
                });
        popupLuckyWheel.show();
    }

    public void showPopupExistMission(final BaseSlidingFragmentActivity activity) {
        if (activity == null || activity.isFinishing()) return;
        PopupLuckyWheel popupLuckyWheel = new PopupLuckyWheel(activity)
                // .setIconUrl(currentWheelSpin.getIconUrl())
                .setIconUrl(null)
                .setTitle(mRes.getString(R.string.lucky_wheel_title_mission))
                .setDesc(currentWheelSpin.getAlertContent())
                .setPositive(currentWheelSpin.getActionLabel(), new PopupLuckyWheel.PositiveListener() {
                    @Override
                    public void onPositive() {
                        if (currentWheelSpin != null)
                            handleGuideMission(activity, currentWheelSpin.getId());
                    }
                });
        popupLuckyWheel.show();
        mApplication.logDebugContent("----LUCKYWHEEL---- showPopupExistMission: " + currentWheelSpin.getId());
    }

    public void showPopupError(BaseSlidingFragmentActivity activity, String msgError) {
        if (activity == null || activity.isFinishing()) return;
        PopupLuckyWheel popupLuckyWheel = new PopupLuckyWheel(activity)
                //.setIconResource(R.drawable.ic_lw_out_of_turn)
                .setDesc(msgError)
                .setPositive(mRes.getString(R.string.lucky_wheel_close), new PopupLuckyWheel.PositiveListener() {
                    @Override
                    public void onPositive() {
                        // TODO ko lam gi
                    }
                });
        popupLuckyWheel.show();
    }

    private void handleGuideMission(BaseSlidingFragmentActivity activity, int missionId) {
        switch (missionId) {
            case LUCKY_WHEEL.ITEM_UPLOAD_ALBUM:
                NavigateActivityHelper.navigateToMyProfile(activity);
                break;
            case LUCKY_WHEEL.ITEM_INVITE_FRIEND:
                NavigateActivityHelper.navigateToInviteFriends(activity, Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND);
                break;
            case LUCKY_WHEEL.ITEM_SHARE_ONMEDIA:
//                navigateToHomeActivity(activity, HomeTab.tab_hot);
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/onmedia");
                break;
            case LUCKY_WHEEL.ITEM_LISTEN_STRANGER:
//                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/stranger");
////                navigateToHomeActivity(activity, HomeTab.tab_stranger);
//                break;
            case LUCKY_WHEEL.ITEM_MAKE_FRIEND:
//                navigateToHomeActivity(activity, HomeTab.tab_stranger);
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/stranger");
                break;
            case LUCKY_WHEEL.ITEM_STATUS:
                navigateToEditStatus(activity);
                break;
            case LUCKY_WHEEL.ITEM_VIDEO_CALL:
            case LUCKY_WHEEL.ITEM_CALL_OUT:
            case LUCKY_WHEEL.ITEM_SMS_OUT:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://contact");
                break;
            case LUCKY_WHEEL.ITEM_GROUP_SURVEY:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/message");
                break;
            case LUCKY_WHEEL.ITEM_LISTEN_MUSIC_KEENG:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/music");
                break;
            case LUCKY_WHEEL.ITEM_TALK_STRANGER:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/stranger?ref=1");
                break;
            case LUCKY_WHEEL.ITEM_WATCH_VIDEO:
                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://home/video");
                break;
            case LUCKY_WHEEL.ITEM_SECRET:
                if (!TextUtils.isEmpty(currentWheelSpin.getLinkShareFb())) {
                    ShareUtils.shareFacebookLuckyWheel(activity, currentWheelSpin.getLinkShareFb(), currentWheelSpin.getServiceCode());
                } else if (!TextUtils.isEmpty(currentWheelSpin.getImageShareFb())) {
                    ShareUtils.shareImageLuckeyWheelToFacebook(activity, currentWheelSpin.getImageShareFb(), currentWheelSpin.getServiceCode());
                }
                break;
            case LUCKY_WHEEL.ITEM_POINT_100:
            case LUCKY_WHEEL.ITEM_POINT_500:
            case LUCKY_WHEEL.ITEM_POINT_1000:
            case LUCKY_WHEEL.ITEM_POINT_2000:
            case LUCKY_WHEEL.ITEM_BUDGET_GOLD:
                if (!TextUtils.isEmpty(currentWheelSpin.getImageShareFb())) {
                    ShareUtils.shareImageLuckeyWheelToFacebook(activity, currentWheelSpin.getImageShareFb(), currentWheelSpin.getServiceCode());
                }
                break;
            default:
                break;
        }
    }

    //Constants.HOME.STRANGER_TAB -----Constants.HOME.ONMEDIA_TAB
    private void navigateToHomeActivity(BaseSlidingFragmentActivity activity, HomeTab tab) {
        Intent intent = new Intent(activity, HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.HOME.TAB_ENUM, tab.toString());
        bundle.putBoolean(Constants.HOME.NEED_SHOW_NOTIFY, false);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    private void navigateToEditStatus(BaseSlidingFragmentActivity activity) {
        Intent intentSetting = new Intent(activity, SettingActivity.class);
        intentSetting.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.CHANGE_STATUS);
        activity.startActivity(intentSetting, true);
    }

    public void requestWheelSpin(final WheelSpinRequestListener listener) {
        newItemId = -1;
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1, null);
            return;
        }
        long timeStamp = TimeHelper.getCurrentTime();
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        String language = mAccountBusiness.getDeviceLanguage();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(language).
                append(account.getName()).
                append(account.getToken()).
                append(timeStamp);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
        post(domain, ConstantApi.Url.LuckyWheel.API_REQUEST_SPIN)
                .putParameter(Constants.HTTP.REST_MSISDN, account.getJidNumber())
                .putParameter(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Constants.HTTP.REST_REVISION, Config.REVISION)
                .putParameter(Constants.HTTP.REST_LANGUAGE_CODE, language)
                .putParameter(Constants.HTTP.REST_NAME, account.getName())
                .putParameter(Constants.HTTP.TIME_STAMP, String.valueOf(timeStamp))
                .putParameter(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        Log.d(TAG, "response: " + response);
                        String responseDecrypt = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        Log.d(TAG, "responseDecrypt: " + responseDecrypt);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(responseDecrypt);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                int lottPoint = responseObject.optInt("lottpoint", -1);
                                JSONObject dataObject = responseObject.optJSONObject("data");
                                if (dataObject != null) {
                                    WheelSpin spin = new WheelSpin(dataObject);

                                    newItemId = spin.getId();
                                    application.logDebugContent("----LUCKYWHEEL---- request wheel spin: " + newItemId);
                                    SharedPref.newInstance(mApplication).putLong(TIME_MISSION, System.currentTimeMillis());
                                    insertMission(spin, dataObject.toString());

                                    if (lottPoint >= 0) {
                                        currentLottPoint = lottPoint;
                                        mPref.edit().putInt(Constants.PREFERENCE.PREF_LUCKEY_WHEEL_LAST_LOTTPOINT, currentLottPoint).apply();
                                    }
                                    listener.onSuccess(spin, currentLottPoint);
                                } else {
                                    listener.onError(-1, null);
                                }
                            } else if (errorCode == 201) {// het luot
                                listener.onOutOfTurn();
                            } else if (errorCode == PAUSE_GAME) {// tạm dừng
                                listener.onError(errorCode, responseObject.optString("desc"));
                            } else {
                                listener.onError(errorCode, null);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, null);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (BuildConfig.DEBUG)
                            ToastUtils.showToast(mApplication, message);
                        listener.onError(-1, null);
                    }
                }).execute();
        mApplication.trackingEvent(R.string.ga_category_lucky_wheel, R.string.ga_action_interaction, R.string
                .ga_label_lucky_wheel_spin);
    }

    public void getValueBudgetLuckyWheel(final GetValueBudgetListener listener) {
        Http.cancel("ValueBudget");
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount()) {
            if (listener != null) {
                listener.onGetValueError(-1, null);
            }
            return;
        }
        long timestamp = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mApplication.getReengAccountBusiness().getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(mApplication.getReengAccountBusiness().getToken()).
                append(timestamp);
        String security = HttpHelper.encryptDataV2(mApplication, sb.toString(), mApplication.getReengAccountBusiness().getToken());
        get(domain, ConstantApi.Url.LuckyWheel.API_GET_VALUE_BUGGET)
                .putParameter(Constants.HTTP.REST_MSISDN, mApplication.getReengAccountBusiness().getJidNumber())
                .putParameter(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Constants.HTTP.REST_REVISION, Config.REVISION)
                .putParameter(Constants.HTTP.TIME_STAMP, String.valueOf(timestamp))
                .putParameter(Constants.HTTP.DATA_SECURITY, security)
                .setTag("ValueBudget")
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        Log.d(TAG, "response: " + response);
                        String responseDecrypt = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        Log.d(TAG, "responseDecrypt: " + responseDecrypt);
                        try {
                            JSONObject jsonObject = new JSONObject(responseDecrypt);
                            int lottPoint = jsonObject.optInt("lottpoint", -1);
                            if (listener != null) {
                                if (lottPoint >= 0) {
                                    currentLottPoint = lottPoint;
                                    mPref.edit().putInt(Constants.PREFERENCE.PREF_LUCKEY_WHEEL_LAST_LOTTPOINT, currentLottPoint).apply();
                                    listener.onGetValueBudgetSuccess(currentLottPoint);
                                } else {
                                    listener.onGetValueError(0, jsonObject.optString("desc"));
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (listener != null) {
                                listener.onGetValueError(-1, null);
                            }
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        Log.e(TAG, "VolleyError " + message);
                        if (listener != null) {
                            listener.onGetValueError(-1, null);
                        }
                    }
                }).execute();
    }

    public void requestSosFriends(final ArrayList<String> listFriends, final SosFriendRequestListener listener) {
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        long timeStamp = TimeHelper.getCurrentTime();
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        HashSet<String> hashSetFriends = new HashSet<>(listFriends);
        JSONArray jsonArray = new JSONArray();
        for (String number : hashSetFriends) {
            jsonArray.put(number);
        }
        String receiverString = jsonArray.toString();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(receiverString).
                append(account.getToken()).
                append(timeStamp);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());

        post(domain, ConstantApi.Url.LuckyWheel.API_REQUEST_SOS_FRIEND)
                .putParameter(Constants.HTTP.REST_MSISDN, account.getJidNumber())
                .putParameter(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Constants.HTTP.REST_REVISION, Config.REVISION)
                .putParameter("otherMsisdns", receiverString)
                .putParameter(Constants.HTTP.TIME_STAMP, String.valueOf(timeStamp))
                .putParameter(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        Log.d(TAG, "response: " + response);
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                String desc = responseObject.optString("desc");
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        listener.onError(-1);
                    }
                }).execute();
    }

    public void doRequestSosAccept(final BaseSlidingFragmentActivity activity, final String friendNumber, final
    SosFriendAcceptListener listener) {
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount()) {
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        long timeStamp = TimeHelper.getCurrentTime();
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(friendNumber).
                append(account.getToken()).
                append(timeStamp);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
        activity.showLoadingDialog(null, R.string.waiting);
        post(domain, ConstantApi.Url.LuckyWheel.API_ACCEPT_HELP)
                .putParameter(Constants.HTTP.REST_MSISDN, account.getJidNumber())
                .putParameter(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Constants.HTTP.REST_REVISION, Config.REVISION)
                .putParameter("otherMsisdn", friendNumber)
                .putParameter(Constants.HTTP.TIME_STAMP, String.valueOf(timeStamp))
                .putParameter(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        Log.d(TAG, "response: " + response);
                        activity.hideLoadingDialog();
                        int errorCode;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                activity.showToast(R.string.request_success);
                                listener.onSuccess();
                            } else {
                                activity.showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            activity.showToast(R.string.e601_error_but_undefined);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        activity.hideLoadingDialog();
                        activity.showToast(R.string.e601_error_but_undefined);
                    }
                }).execute();
        mApplication.trackingEvent(R.string.ga_category_lucky_wheel, R.string.ga_action_interaction, R.string
                .ga_label_lucky_wheel_help);
    }

    private void logDoneMission() {
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount() || currentWheelSpin == null) return;
        long timeStamp = TimeHelper.getCurrentTime();
        ReengAccount account = mAccountBusiness.getCurrentAccount();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(currentWheelSpin.getId()).
                append(account.getToken()).
                append(timeStamp);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
        post(domain, ConstantApi.Url.LuckyWheel.API_LOG_DONE_MISSION)
                .putParameter(Constants.HTTP.REST_MSISDN, account.getJidNumber())
                .putParameter(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Constants.HTTP.REST_REVISION, Config.REVISION)
                .putParameter(Constants.HTTP.TIME_STAMP, String.valueOf(timeStamp))
                .putParameter("taskId", String.valueOf(currentWheelSpin.getId()))
                .putParameter(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String s) throws Exception {
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String code = jsonObject.optString("code");
                            String data = jsonObject.optString("data");
                            Log.e(TAG, "onResponse: logDoneMission" + code);
                            if (code.equals("200")) {
                                MessageBusiness messageBusiness = mApplication.getMessageBusiness();
                                ThreadMessage thread = messageBusiness.findExistingOrCreateOfficialThread();
                                String myNumber = mApplication.getReengAccountBusiness().getJidNumber();

                                String contentFakeOA = currentWheelSpin.getContentSuccessExtend();
                                long timeMission = SharedPref.newInstance(mApplication).getLong(TIME_MISSION, System.currentTimeMillis());
                                boolean checkInDay = TimeHelper.checkTimeInDay(timeMission);
                                if (checkInDay) contentFakeOA = currentWheelSpin.getFakeOaContent();

                                ReengMessage reengMessage = messageBusiness.createFakeMessageDeepLink(thread.getId(),
                                        thread.getServerId(), myNumber, contentFakeOA,
                                        mRes.getString(R.string.lucky_wheel_continue),
                                        mRes.getString(R.string.deep_link_lucky_wheel),
                                        mRes.getString(R.string.lucky_wheel_accumulate),
                                        mRes.getString(R.string.deep_link_lucky_wheel_accumulate));
                                messageBusiness.notifyReengMessage(mApplication, thread, reengMessage, ThreadMessageConstant
                                        .TYPE_THREAD_OFFICER_CHAT);
                                insertMission(null, null);
                            } else {
                                ToastUtils.showToast(application, application.getString(R.string.lucky_wheel_mission_error));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            ToastUtils.showToast(application, application.getString(R.string.lucky_wheel_mission_error));
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        ToastUtils.showToast(application, application.getString(R.string.lucky_wheel_mission_error));
                    }
                }).execute();
    }

    /*private class ParserMessageAsyncTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            String input = params[0];
            try {
                XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(new StringReader(input));
                ReengMessagePacket packet = null;
                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        if (parser.getName().equals("message")) {
                            String subTypeString = parser.getAttributeValue("", "subtype");
                            ReengMessagePacket.SubType subType = ReengMessagePacket.SubType.fromString(subTypeString);
                            String ns = parser.getAttributeValue("", "ns");
                            String external = parser.getAttributeValue("", "external");
                            packet = (ReengMessagePacket) PacketParserUtils.parseReengMessage(parser, subType,
                            external, subTypeString);
                        }
                    }
                    eventType = parser.next();
                }
                ReengMessagePacket.Type type = packet.getType();
                MessageBusiness messageBusiness = mApplication.getMessageBusiness();
                if (type == ReengMessagePacket.Type.chat) {
                    messageBusiness.getIncomingMessageProcessor().
                            processIncomingReengMessage(mApplication, packet);
                } else if (type == ReengMessagePacket.Type.groupchat) {
                    messageBusiness.getIncomingMessageProcessor().
                            processIncomingGroupMessage(mApplication, packet);
                } else if (type == ReengMessagePacket.Type.offical) {
                    messageBusiness.processIncomingOfficerMessage(mApplication, packet);
                } else if (type == ReengMessagePacket.Type.roomchat) {
                    messageBusiness.getIncomingMessageProcessor().
                            processIncomingRoomMessage(mApplication, packet);
                }
            } catch (XmlPullParserException e) {
                Log.e(TAG,"Exception",e);
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            }
            return null;
        }
    }*/

    public interface SosFriendAcceptListener {
        void onSuccess();
    }

    public interface SosFriendRequestListener {
        void onError(int code);

        void onSuccess(String desc);
    }

    public interface WheelSpinRequestListener {
        void onSuccess(WheelSpin wheelSpin, int currentLottPoint);

        void onOutOfTurn();

        void onError(int code, String desc);
    }

    public interface GetValueBudgetListener {
        void onGetValueBudgetSuccess(int value);

        void onGetValueError(int code, String desc);
    }
}
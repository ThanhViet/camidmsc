package com.metfone.selfcare.helper.message;

/**
 * Created by vtsoft on 10/16/2014.
 */
public class MessageConstants {
    public static final int TYPE_MESSAGE_SEND_TEXT = 0; //text, file, image, voicemail, notification;
    public static final int TYPE_MESSAGE_RECEIVED_TEXT = 1;
    public static final int TYPE_MESSAGE_SEND_VOICEMAIL = 2;
    public static final int TYPE_MESSAGE_RECEIVED_VOICEMAIL = 3;
    public static final int TYPE_MESSAGE_SEND_IMAGE = 4;
    public static final int TYPE_MESSAGE_RECEIVED_IMAGE = 5;
    public static final int TYPE_MESSAGE_SEND_SHARE_CONTACT = 6;
    public static final int TYPE_MESSAGE_RECEIVED_SHARE_CONTACT = 7;
    public static final int TYPE_MESSAGE_SEND_SHARE_VIDEO = 8;
    public static final int TYPE_MESSAGE_RECEIVED_SHARE_VIDEO = 9;
    public static final int TYPE_MESSAGE_SEND_VOICE_STICKER = 10;
    public static final int TYPE_MESSAGE_RECEIVED_VOICE_STICKER = 11;
    public static final int TYPE_MESSAGE_NOTIFY = 12;
    public static final int TYPE_MESSAGE_SUGGEST_SHARE_MUSIC = 13;
    public static final int TYPE_MESSAGE_SEND_INVITE_MUSIC = 14;
    public static final int TYPE_MESSAGE_RECEIVED_INVITE_MUSIC_NEW = 15;
    public static final int TYPE_MESSAGE_SEND_ACTION_MUSIC = 16; // loai message chuyen bai, doi bai ...
    public static final int TYPE_MESSAGE_RECEIVED_ACTION_MUSIC = 17;
    public static final int TYPE_MESSAGE_RECEIVED_LOCATION = 18;
    public static final int TYPE_MESSAGE_SEND_LOCATION = 19;
    public static final int TYPE_MESSAGE_GREETING_VOICE_STICKER = 20;
    public static final int TYPE_MESSAGE_SEND_RESTORE = 21;
    public static final int TYPE_MESSAGE_RECEIVED_RESTORE = 22;
    public static final int TYPE_MESSAGE_SEND_TRANSFER_MONEY = 23;
    public static final int TYPE_MESSAGE_RECEIVED_TRANSFER_MONEY = 24;
    public static final int TYPE_MESSAGE_RECEIVED_SEARCH_RANK = 25;
    public static final int TYPE_MESSAGE_SEND_SEARCH_RANK = 26;
    public static final int TYPE_MESSAGE_RECEIVED_CRBT_GIFT = 27;
    public static final int TYPE_MESSAGE_RECEIVED_DEEP_LINK = 28;
    public static final int TYPE_MESSAGE_RECEIVED_FILE_LINK = 29;
    public static final int TYPE_MESSAGE_ADVERTISE = 30;
    public static final int TYPE_MESSAGE_SEND_CALL = 31;
    public static final int TYPE_MESSAGE_RECEIVED_CALL = 32;
    public static final int TYPE_MESSAGE_SEND_WATCH_VIDEO = 33;
    public static final int TYPE_MESSAGE_RECEIVED_WATCH_VIDEO = 34;
    public static final int TYPE_MESSAGE_SEND_BPLUS = 35;
    public static final int TYPE_MESSAGE_RECEIVED_BPLUS = 36;
    public static final int TYPE_MESSAGE_SEND_FILE = 37;
    public static final int TYPE_MESSAGE_RECEIVED_FILE = 38;
    public static final int TYPE_MESSAGE_RECEIVED_GIFT_LIXI = 39;
    public static final int TYPE_MESSAGE_BANNER = 40;
    public static final int TYPE_MESSAGE_SUGGEST_VOICESTICKER = 41;
    public static final int TYPE_MESSAGE_POLL = 42;
    public static final int TYPE_MESSAGE_MAX_COUNT = TYPE_MESSAGE_POLL + 1;



    public enum NOTIFY_TYPE {
        create,
        join,
        invite,
        rename,
        leave,
        kick,
        removeAdmin,
        makeAdmin,
        groupPrivate,
        groupPublic,
        changeAvatar,
        leaveMusic
    }
}
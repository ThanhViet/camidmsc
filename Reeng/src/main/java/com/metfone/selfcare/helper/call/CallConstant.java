package com.metfone.selfcare.helper.call;

/**
 * Created by thanhnt72 on 7/25/2016.
 */
public class CallConstant {
    public static final String JIDNUMBER = "jidnumber";
    public static final String IS_CALLVIDEO = "is_callvideo";
    public static final String TYPE_FRAGMENT = "type_fragment";
    public static final String SDP_CALL_DATA = "sdp_call_data";
    public static final String FIRST_START_ACTIVITY = "first_start_mocha_call_activity";
    public static final String IS_ACCEPT_CALL = "is_accept_call"; // accept from notification

    public static final long TIME_VIBRATOR = 300;
    protected static final long[] TIME_VIBRATOR_CALLING = {0, 1000, 1000};

    public static final long CALL_TIME_OUT = 60 * 1000;//60s
    public static final long CALL_TIME_OUT_INVITE = 30 * 1000;//30s

    public static final int TYPE_CALLER = 1;
    public static final int TYPE_CALLEE = 2;

    public class STATE {
        public static final int NON = -1;// dang khong co cuoc goi nao
        public static final int TRYING = 100;
        public static final int RINGING = 180;
        public static final int CALLEE_STARTED = 181;       // callee started
        public static final int CONNECT = 198;      // begin connect
        public static final int CONNECTING = 199;   // connecting
        public static final int CONNECTED = 200;    // connected
    }

    public class STATE_AUDIO_VIDEO {
        public static final int NON = -1;
        public static final int OUT_GOING = 1;
        public static final int IN_COMMING = 2;
        public static final int ACCEPTED = 3;
    }

    public class CALL_TYPE {
        public static final int CALL_NONE = -1;
        public static final int CALL_DATA = 0;
        public static final int CALL_OUT = 1;
        public static final int CALL_VIDEO = 2;
    }

    public static final int HOLDER_CONTACT = 1;
    public static final int HOLDER_HISTORY = 2;
    public static final int HOLDER_SECTION = 3;
//    public static final int HOLDER_CALL_BANNER = 4;

    public static long[] getTimeVibratorCalling() {
        return TIME_VIBRATOR_CALLING;
    }


    public class SETTING_CALL {
        public static final int ICE_TIME_OUT = 15000;
        public static final long RESTART_ICE_PERIOD = 5000;
        public static final int RESTART_ICE_LOOP = 1;
        public static final long ZERO_BW_END_CALL = 25000;
//        public static final long NETWORK_2_FAILED_TIME = 5000;
        public static final long TIME_DIS_2_RECON = 4000;
    }
}
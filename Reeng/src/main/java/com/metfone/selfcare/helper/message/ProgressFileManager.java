package com.metfone.selfcare.helper.message;

import android.os.Handler;
import android.os.Looper;
import android.view.View;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by thanhnt72 on 11/30/2016.
 */
public class ProgressFileManager {
    private static final String TAG = ProgressFileManager.class.getSimpleName();
    private static Handler handler;
    private static ConcurrentHashMap<Integer, View> mapMessageIdWithConvertView = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<View, Integer> mapConvertViewWithMessageId = new ConcurrentHashMap<>();

    public static void addToHashMap(int messageId, View convertView) {
        mapMessageIdWithConvertView.put(messageId, convertView);
        mapConvertViewWithMessageId.put(convertView, messageId);
    }

    public static View getConvertViewByMessageId(int messageId) {
        if (mapMessageIdWithConvertView.containsKey(messageId)) {
            return mapMessageIdWithConvertView.get(messageId);
        } else
            return null;
    }

    public static int getMessageIdByConvertView(View convertView) {
        if (mapConvertViewWithMessageId.containsKey(convertView)) {
            return mapConvertViewWithMessageId.get(convertView);
        } else
            return -1;
    }

    public static synchronized void fressSource() {
        mapMessageIdWithConvertView.clear();
        mapConvertViewWithMessageId.clear();
        handler = null;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static synchronized void initHandler() {
        handler = new Handler(Looper.getMainLooper());
    }
}

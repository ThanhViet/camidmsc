package com.metfone.selfcare.helper;

import android.content.Context;
import android.graphics.Typeface;

import com.metfone.selfcare.util.Log;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 10/11/2015.
 */
public class TypefaceUtil {
    private static final String TAG = TypefaceUtil.class.getSimpleName();

    /**
     * Using reflection to override default typeface
     * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
     *
     * @param context                    to work with assets
     * @param defaultFontNameToOverride  for example "monospace"
     * @param customFontFileNameInAssets file name of the font from assets
     */
    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface newTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);
            if (Version.hasLollipop()) {
                Map<String, Typeface> newMap = new HashMap<>();
                newMap.put("sans-serif", newTypeface);
                try {
                    final Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
                    staticField.setAccessible(true);
                    staticField.set(null, newMap);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    Log.e(TAG,"Exception",e);
                }
            } else {
                try {
                    final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
                    defaultFontTypefaceField.setAccessible(true);
                    defaultFontTypefaceField.set(null, newTypeface);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    Log.e(TAG,"Exception",e);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception overrideFont", e);
        }
    }
}

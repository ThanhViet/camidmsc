package com.metfone.selfcare.helper.apple;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.metfone.selfcare.helper.google.GoogleSignInHelper;
import com.metfone.selfcare.module.keeng.utils.SharedPref;

import lombok.val;

// A client to know about WebView navigations
// For API 21 and above
//@Suppress("OverridingDeprecatedMember")
public class AppleWebViewClient extends WebViewClient {
    AppleIDSignInHelper.OnAppleIdListener listener;
    private SharedPref sharedPref;
    private Dialog appleDialog;
    private AppCompatActivity activity;
    private AppleIDSignInHelper appleIDSignInHelper;

    public AppleWebViewClient(Dialog appleDialog, AppCompatActivity activity, AppleIDSignInHelper.OnAppleIdListener listener) {
        appleIDSignInHelper = AppleIDSignInHelper.newInstance(activity);
        this.appleDialog = appleDialog;
        this.activity = activity;
        this.listener = listener;
        sharedPref = appleIDSignInHelper.getSharedPref();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (request.getUrl().toString().startsWith(AppleConstants.REDIRECT_URI)) {
            handleUrl(request.getUrl().toString());
            // Close the dialog after getting the authorization code
            if (request.getUrl().toString().contains("success=")) {
                appleDialog.dismiss();
            }
            return true;
        }
        return true;
    }

    @Override
    // For API 19 and below
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith(AppleConstants.REDIRECT_URI)) {
            handleUrl(url);
            // Close the dialog after getting the authorization code
            if (url.contains("success=")) {
                appleDialog.dismiss();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        Window window = activity.getWindow();
                window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        // Set height of the Dialog to 90% of the screen
        ViewGroup.LayoutParams layoutparms = view.getLayoutParams();
        layoutparms.height = (int) (displayRectangle.height() * 0.9f);
        view.setLayoutParams(layoutparms);
    }

    // Check webview url for access token code or error
    @SuppressLint("LongLogTag")
    private void handleUrl(String url) {
        Uri uri = Uri.parse(url);

        String success = uri.getQueryParameter("success");
        if (success == "true") {

            // Get the Authorization Code from the URL
            String appleAuthCode = uri.getQueryParameter("code")!= null ?uri.getQueryParameter("code"): "";
            Log.i("Apple Code: ", appleAuthCode);

            // Get the Client Secret from the URL
            String appleClientSecret = uri.getQueryParameter("client_secret") != null ? uri.getQueryParameter("client_secret"): "";
            Log.i("Apple Client Secret: ", appleClientSecret);
            // Save the Client Secret (appleClientSecret) using SharedPreferences
            // This will allow us to verify if refresh Token is valid every time they open the app after cold start.
//            val sharedPref = getPreferences(Context.MODE_PRIVATE)
            sharedPref.putString("client_secret", appleClientSecret);

            //Check if user gave access to the app for the first time by checking if the url contains their email
            if (url.contains("email")) {
                //Get user's First Name
                String firstName = uri.getQueryParameter("first_name");
                Log.i("Apple User First Name: ", firstName != null ? firstName : "");
//                appleFirstName = firstName != null ? firstName: "Not exists";

                //Get user's Middle Name
                String middleName = uri.getQueryParameter("middle_name");
                Log.i("Apple User Middle Name: ", middleName !=null ? middleName : "");
//                appleMiddleName = middleName ?: "Not exists"

                //Get user's Last Name
                String lastName = uri.getQueryParameter("last_name");
                Log.i("Apple User Last Name: ", lastName != null ?lastName : "");
//                appleLastName = lastName ?: "Not exists"

                //Get user's email
                String email = uri.getQueryParameter("email");
                Log.i("Apple User Email: ", email != null ? email : "Not exists");
//                appleEmail = email ?: ""
                listener.onGetInfoAppleIdFinish(firstName, middleName, lastName, email);
            }

            // Exchange the Auth Code for Access Token
            appleIDSignInHelper.requestForAccessToken(appleAuthCode, appleClientSecret);
        } else if (success == "false") {
            Log.e("ERROR", "We couldn't get the Auth Code");
        }
    }
}

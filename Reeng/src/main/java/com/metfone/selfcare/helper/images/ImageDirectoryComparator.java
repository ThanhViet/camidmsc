package com.metfone.selfcare.helper.images;

import java.util.Comparator;

public class ImageDirectoryComparator implements Comparator<ImageDirectory> {
    public int compare(ImageDirectory left, ImageDirectory right) {
        return Integer.compare(right.getListFile().size(), left.getListFile().size());
    }
}
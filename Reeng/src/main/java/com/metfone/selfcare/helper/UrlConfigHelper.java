package com.metfone.selfcare.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.WebViewNewActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.encrypt.AESCrypt;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by toanvk  on 11/19/2014.
 */
public class UrlConfigHelper {
    private static final String TAG = UrlConfigHelper.class.getSimpleName();
    private static UrlConfigHelper mInstance;
    private SharedPreferences mPref;
    private Context mContext;

    private String fullDomainFile;
    private String fullDomainMsg;
    private String fullDomainOnMedia;
    private String fullDomainImage;
    private String fullDomainGenOtp;

    //private String domainContent;// domain cu ko dung nua
    // keeng
    private String fullDomainServiceKeeng;
    private String fullDomainMediaKeeng;
    private String fullDomainImageKeeng;

    private String domainFileV1;
    private String domainImageV1;
    private String domainOnMediaV1;
    private String domainMochaVideo;
    private String domainKeengMusic;
    private String domainKeengMovies;
    private String domainNetnews;
    private String domainTiin;
    private String domainKeengMusicSearch;

    public static final String KEY = "CQPkng4R1wL@CZT29YDE94A$*";
    public static final String RSA_KEY = "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgF3g82nB1ImzAwSN7JXeOC7wChDA4Nbzun" +
            "/2B60sB04LCxBt88yRQTK734ugqAJ9cnYYNjwYfzcoTmubiMygsdtoNf1HTmezAL+ppsJxZ" +
            "/TlfomXz6zUS2HxNUdNcgX0NdHpq5OR9713p6tiq5Z4TdYjja9P7FEG8p4xf8snDEjhAgMBAAE=";
    private AESCrypt mAESCrypt;
    private HashMap<String, String> hashMapUrl = new HashMap<>();
    private SharedPreferences.Editor editor;

    public static synchronized UrlConfigHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new UrlConfigHelper(context);
        }
        return mInstance;
    }

    private UrlConfigHelper(Context context) {
        mContext = context;
        mPref = ApplicationController.self().getPref();
        new Thread(this::initHashMapUrl);
        editor = mPref.edit();
        initDomain();
        initDomainKeeng();
    }

    // parser json domain
    public void detectSubscription(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int ssl = jsonObject.optInt("ssl", -1);
            ApplicationController app = (ApplicationController) mContext.getApplicationContext();
            app.getReengAccountBusiness().setSSL(ssl);
            app.getReengAccountBusiness().setEmulatorCam(jsonObject.optInt("sml", 0) == 1);
            if (jsonObject.has("subscription_package")) {
                JSONArray subsArray = jsonObject.getJSONArray("subscription_package");
                if (subsArray == null || subsArray.length() <= 0) {
                    Log.d(TAG, "subsArray: notfound");
                    return;
                }
                Log.e(TAG, "updateDomain from detectSubscription ----------");
                Log.i(TAG, "detectSubscription: " + response);
                JSONObject subsObject = subsArray.getJSONObject(0);
                if (subsObject != null) {
                    String jDomainFile = subsObject.optString("domain_file", null);
                    String jDomainMsg = subsObject.optString("domain_msg", null);
                    String jDomainOnMedia = subsObject.optString("domain_on_media", null);
                    String jDomainImage = subsObject.optString("domain_img", null);
                    // update domain
                    boolean isDomainMsgChange = updateDomainMocha(jDomainFile,
                            jDomainMsg, jDomainOnMedia, jDomainImage, ""); //detect thi ko co public rsa key

                    String jDomainKService = subsObject.optString("kservice", null);
                    String jDomainKMedia = subsObject.optString("kmedia", null);
                    String jDomainKImage = subsObject.optString("kimage", null);
                    updateDomainKeeng(jDomainKService, jDomainKMedia, jDomainKImage);

                    updateDomainFileV1(subsObject.optString("domain_file_v1", null));
                    updateDomainImageV1(subsObject.optString("domain_img_v1", null));
                    updateDomainOnMediaV1(subsObject.optString("domain_on_media_v1", null));
                    updateDomainMochaVideo(subsObject.optString("domain_mcvideo", null));
                    updateDomainKeengMusic(subsObject.optString("domain_kmusic", null));
                    updateDomainKeengMusicSearch(subsObject.optString("domain_kmusic_search", null));
                    updateDomainKeengMovies(subsObject.optString("domain_kmovies", null));
                    updateDomainNetNews(subsObject.optString("domain_netnews", null));
                    updateDomainTiin(subsObject.optString("domain_tiin", null));

                    if (isDomainMsgChange) {
                        XMPPManager xmppManager = app.getXmppManager();
                        if (xmppManager != null) {
                            xmppManager.changeConfigXmpp();
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "detecteSubscription", e);
        }
    }

    /**
     * luu lai domain vao sharepref
     * tra ve true neu domain msg co su thay doi
     *
     * @param domainFile
     * @param domainMsg
     * @param domainOnMedia
     * @param publicKey
     */
    public boolean updateDomainMocha(String domainFile, String domainMsg, String domainOnMedia, String domainImage
            , String publicKey) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "updateDomainMocha old domain"
                    + "\nfullDomainGenOtp: " + fullDomainGenOtp
                    + "\nfullDomainMsg: " + fullDomainMsg
                    + "\nfullDomainFile: " + fullDomainFile
                    + "\nfullDomainImage: " + fullDomainImage
            );
            Log.e(TAG, "updateDomainMocha new domain\ndomainFile: " + domainFile
                    + "\ndomainMsg: " + domainMsg
                    + "\ndomainImage: " + domainImage
                    + "\npublicKey: " + publicKey);
        }
        if (!TextUtils.isEmpty(publicKey)) {
            //neu ko rong thi put vao
            editor.putString(Constants.PREFERENCE.PREF_PUBLIC_RSA_KEY, publicKey).apply();
        }
        /*if (Config.Server.SERVER_TEST) {  // server test ko cap nhat domain
            return false;
        }*/
        boolean isChangeDomainMsg = false;
        // domain file
        if (!TextUtils.isEmpty(domainFile)) {
            String fDomainFile = Config.PREFIX.PROTOCOL_HTTP + domainFile;
            if (!fDomainFile.equals(fullDomainFile)) {//khac domain
                this.fullDomainFile = fDomainFile;
                editor.putString(Constants.PREFERENCE.PREF_DOMAIN_FILE, domainFile).apply();
            }
        }
        //domain msg
        if (!TextUtils.isEmpty(domainMsg)) {
            //            String fDomainMsg = Config.PREFIX.PROTOCOL_HTTP + domainMsg;
            String fDomainMsg = domainMsg;
            if (!fDomainMsg.equals(fullDomainMsg)) {
                this.fullDomainMsg = fDomainMsg;
                editor.putString(Constants.PREFERENCE.PREF_DOMAIN_MSG, domainMsg).apply();
                isChangeDomainMsg = true;
            }
        }
        // domain on media
        if (!TextUtils.isEmpty(domainOnMedia)) {
            String fDomainOnMedia = Config.PREFIX.PROTOCOL_HTTP + domainOnMedia;
            if (!fDomainOnMedia.equals(fullDomainOnMedia)) {
                this.fullDomainOnMedia = fDomainOnMedia;
                editor.putString(Constants.PREFERENCE.PREF_DOMAIN_ON_MEDIA, domainOnMedia).apply();
                isChangeDomainMsg = true;
            }
        }
        // domain image
        if (!TextUtils.isEmpty(domainImage)) {
            String fDomainImage = Config.PREFIX.PROTOCOL_HTTP + domainImage;
            if (!fDomainImage.equals(fullDomainImage)) {
                this.fullDomainImage = fDomainImage;
                editor.putString(Constants.PREFERENCE.PREF_DOMAIN_IMAGE, domainImage).apply();
            }
        }
        return isChangeDomainMsg;
    }

    /**
     * get api file domain
     *
     * @param urlEnum
     * @return api mocha
     */
    public String getUrlConfigOfFile(Config.UrlEnum urlEnum) {
        String urlApi = getUrlByKey(urlEnum);
        return getDomainFile() + urlApi;
    }

    //Dung cho genOTP, detech msisdn
    public String getUrlGenOTP(Config.UrlEnum urlEnum) {
        String urlApi = getUrlByKey(urlEnum);
        if (TextUtils.isEmpty(fullDomainGenOtp)) {
            initDomain();
        }
        return fullDomainGenOtp + urlApi;
    }

    /**
     * get api msg domain
     *
     * @return domain msg
     */
    public String getUrlConfigOfMsg() {
        if (TextUtils.isEmpty(fullDomainMsg)) {
            initDomain();
        }
        if (fullDomainMsg.contains("171.255.139.133")) {
            return fullDomainMsg.replace("171.255.139.133", "171.255.193.142");
        }
        return fullDomainMsg;
    }

    /**
     * get api onMedia
     *
     * @param urlEnum
     * @return api onMedia
     */
    public String getUrlConfigOfOnMedia(Config.UrlEnum urlEnum) {
        String urlApi = getUrlByKey(urlEnum);
        return getDomainOnMedia() + urlApi;
    }

    public String getUrlConfigOfImage(Config.UrlEnum urlEnum) {
        String urlApi = getUrlByKey(urlEnum);
        return getDomainImage() + urlApi;
    }

    public String getConfigGuestBookUrl(String path) {
        if (TextUtils.isEmpty(path)) return null;
        return getDomainFile() + "/" + path;
    }

    /**
     * get api service keeng domain
     *
     * @param urlEnum
     */
    public String getUrlConfigOfServiceKeeng(Config.UrlKeengEnum urlEnum) {
        String urlApi = getUrlKeengByKey(urlEnum);
        if (TextUtils.isEmpty(fullDomainServiceKeeng)) {
            initDomain();
        }
        if (BuildConfig.DEBUG)
            Log.i(TAG, "getUrlConfigOfServiceKeeng urlEnum: " + urlEnum + "\nAPI:" + fullDomainServiceKeeng + urlApi);
        return fullDomainServiceKeeng + urlApi;
    }

    /**
     * get api media2 domain
     *
     * @param urlEnum
     * @return
     */
    public String getUrlConfigOfMedia2Keeng(Config.UrlKeengEnum urlEnum) {
        String urlApi = getUrlKeengByKey(urlEnum);
        if (urlEnum == Config.UrlKeengEnum.MEDIA_UPLOAD_SONG) {
            return urlApi;
        } else {
            if (TextUtils.isEmpty(fullDomainServiceKeeng)) {
                initDomainKeeng();
            }
            return fullDomainServiceKeeng + urlApi;
        }
    }

    public String getUrlConfigOfMediaSearchKeeng(Config.UrlKeengEnum urlEnum) {
        String urlApi = getUrlKeengByKey(urlEnum);
        if (urlEnum == Config.UrlKeengEnum.MEDIA_UPLOAD_SONG) {
            return urlApi;
        } else {
            if (TextUtils.isEmpty(fullDomainMediaKeeng)) {
                initDomainKeeng();
            }
            return fullDomainMediaKeeng + urlApi;
        }
    }

    public String getDomainImageKeeng() {
        if (TextUtils.isEmpty(fullDomainImageKeeng)) {
            initDomainKeeng();
        }
        return fullDomainImageKeeng;
    }

    public String getDomainMediaKeeng() {
        if (TextUtils.isEmpty(fullDomainMediaKeeng)) {
            initDomainKeeng();
        }
        return fullDomainMediaKeeng;
    }

    public String getDomainServiceKeeng() {
        if (TextUtils.isEmpty(fullDomainServiceKeeng)) {
            initDomainKeeng();
        }
        return fullDomainServiceKeeng;
    }

    public String getUrlByKey(Config.UrlEnum urlEnum) {
        if (hashMapUrl.isEmpty()) {
            initHashMapUrl();
        }
        return hashMapUrl.get(urlEnum.name());
    }

    private void initHashMapUrl() {
        long t = System.currentTimeMillis();
        mAESCrypt = AESCrypt.getInStance();
        int size = HttpHelper.getPlmTye().length;
        hashMapUrl = new HashMap<>();
        for (int i = 0; i < size; i++) {
            try {
                String decrypt = mAESCrypt.decrypt(HttpHelper.getPlmTye()[i]);
                JSONObject object = new JSONObject(decrypt);
                Iterator iter = object.keys();
                while (iter.hasNext()) {
                    String key = (String) iter.next();
                    String value = null;
                    try {
                        value = object.getString(key);
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    } finally {
                        Log.i(TAG, key + " - " + value);
                        hashMapUrl.put(key, value);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        Log.d(TAG, "[perform] initHashMapUrl take: " + (System.currentTimeMillis() - t));
    }

    public static void openBrowser(BaseSlidingFragmentActivity activity, String url) {
        if (activity == null || activity.isFinishing()) return;
        if (TextUtils.isEmpty(url)) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else {
            url = Utilities.formatUrl(url);
            Uri uri = UrlConfigHelper.getInstance(activity).getUriWebView(url);
            NavigateActivityHelper.navigateToActionView(activity, uri);
        }
    }

    public static void gotoWebViewSponsor(ApplicationController application, BaseSlidingFragmentActivity activity
            , String url, String sponsorId, int timeOut, int amount) {
        if (application == null || activity == null || activity.isFinishing()) return;
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(sponsorId) || timeOut <= 0 || amount <= 0) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else {
            Intent intent = new Intent(application, WebViewNewActivity.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_SPONSOR, true);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_SPONSOR_ID, sponsorId);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_SPONSOR_TIME_OUT, timeOut);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_SPONSOR_AMOUNT, amount);
            activity.startActivity(intent, true);
        }
    }

    public static void gotoWebViewOnMedia(ApplicationController application, BaseSlidingFragmentActivity activity, String url) {
        Utilities.gotoWebView(application, activity, url, "", "", "", "1", "", "");
    }

    public static void gotoWebViewOnMocha(ApplicationController application, BaseSlidingFragmentActivity activity, String url) {
        Utilities.gotoWebView(application, activity, url, "", "", "", "", "", "");
    }

    public static void gotoWebViewFullScreenPayment(ApplicationController application
            , BaseSlidingFragmentActivity activity, String url) {
        if (application == null || activity == null || activity.isFinishing()) return;
        if (TextUtils.isEmpty(url)) {
            activity.showToast(R.string.e601_error_but_undefined);
        } else {
            Intent intent = new Intent(application, WebViewNewActivity.class);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_FULLSCREEN, true);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_CONFIRM_BACK, true);
            intent.putExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_PAYMENT, true);
            activity.startActivity(intent, true);
        }
    }

    public Uri getUriWebView(String input) {
        Uri uri;
        try {
            if (input.startsWith("http:") ||
                    input.startsWith("https:") ||
                    input.startsWith("mocha:")) {
                uri = Uri.parse(input);
            } else {
                String url = parserUrl(input);
                if (!TextUtils.isEmpty(url)) {
                    uri = Uri.parse(url);
                } else {
                    uri = Uri.parse("http://" + input);
                }
            }
            return uri;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }
    }

    private String parserUrl(String input) {
        try {
            URL url = new URL(input);
            return url.toString();
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException", e);
            return null;
        }
    }

    public String getUrlKeengByKey(Config.UrlKeengEnum urlEnum) {
        String url = "";
        switch (urlEnum) {
            case SERVICE_GET_SONG:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_SERVICE_GET_SONG,
                        Constants.PREF_DEFAULT.URL_SERVICE_GET_SONG_DEFAULT);
                break;
            case SERVICE_GET_TOP_SONG:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_SERVICE_GET_TOP_SONG,
                        Constants.PREF_DEFAULT.URL_SERVICE_GET_TOP_SONG_DEFAULT);
                break;
            /*case SERVICE_GET_FEEDS_KEENG:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_SERVICE_GET_FEEDS_KEENG,
                        Constants.PREF_DEFAULT.URL_SERVICE_GET_FEEDS_KEENG_DEFAULT);
                break;*/
            /*case SERVICE_SEARCH_SONG:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_SERVICE_SEARCH_SONG,
                        Constants.PREF_DEFAULT.URL_SERVICE_SEARCH_SONG_DEFAULT);
                break;*/
            case SERVICE_GET_ALBUM:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_SERVICE_GET_ALBUM,
                        Constants.PREF_DEFAULT.URL_SERVICE_GET_ALBUM_DEFAULT);
                break;
            case SERVICE_GET_SONG_UPLOAD:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_SERVICE_GET_SONG_UPLOAD,
                        Constants.PREF_DEFAULT.URL_SERVICE_GET_SONG_UPLOAD_DEFAULT);
                break;
            case MEDIA2_SEARCH_SUGGESTION:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_MEDIA2_SEARCH_SUGGESTION,
                        Constants.PREF_DEFAULT.URL_MEDIA2_SEARCH_SUGGESTION_DEFAULT);
                break;
            case MEDIA_UPLOAD_SONG:
                url = mPref.getString(Constants.PREFERENCE.CONFIG.PREF_URL_MEDIA_UPLOAD_SONG,
                        Constants.PREF_DEFAULT.URL_MEDIA_UPLOAD_SONG_DEFAULT);
                break;
            default:
                break;
        }
        return url;
    }

    public Uri formatUrlBeforeWatchVideo(ApplicationController application, String originUrl) {
        Log.i(TAG, "watch video originUrl: " + originUrl);
        if (TextUtils.isEmpty(originUrl)) {
            return null;
        }
        String myNumber = application.getReengAccountBusiness().getJidNumber();
        StringBuilder sb = new StringBuilder(originUrl);
        Uri uri = Uri.parse(originUrl);
        if (TextUtils.isEmpty(myNumber)) {
            return uri;
        }
        if (TextUtils.isEmpty(uri.getQuery())) {
            sb.append("?mocha_msisdn=").append(HttpHelper.EncoderUrl(myNumber))
                    .append("&mocha_type=").append("android");
            Log.i(TAG, "watch video after ?:" + sb.toString());
            return Uri.parse(sb.toString());
        } else if (TextUtils.isEmpty(uri.getQueryParameter("mocha_msisdn")) &&
                TextUtils.isEmpty(uri.getQueryParameter("mocha_type"))) {
            sb.append("&mocha_msisdn=").append(HttpHelper.EncoderUrl(myNumber))
                    .append("&mocha_type=").append("android");
            Log.i(TAG, "watch video after &:" + sb.toString());
            return Uri.parse(sb.toString());
        }
        return uri;
    }

    private void initDomain() {
        boolean isDev = false;
        if (ApplicationController.self() != null && ApplicationController.self().getReengAccountBusiness() != null)
            isDev = ApplicationController.self().getReengAccountBusiness().isDev();
        // them protocol
        this.fullDomainFile = Config.PREFIX.PROTOCOL_HTTP
                + mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_FILE, ConfigLocalized.DOMAIN_FILE);
        this.fullDomainOnMedia = Config.PREFIX.PROTOCOL_HTTP
                + mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_ON_MEDIA, ConfigLocalized.DOMAIN_ON_MEDIA);
        this.fullDomainImage = Config.PREFIX.PROTOCOL_HTTP
                + mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_IMAGE, ConfigLocalized.DOMAIN_IMAGE);
        this.fullDomainMsg = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_MSG, ConfigLocalized.DOMAIN_MSG);
        this.fullDomainGenOtp = getDomainDefault("OTP", isDev);

        this.domainFileV1 = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_FILE_V1, getDomainDefault("FILE", isDev));
        this.domainImageV1 = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_IMAGE_V1, getDomainDefault("IMAGE", isDev));
        this.domainOnMediaV1 = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_ON_MEDIA_V1, getDomainDefault("ON_MEDIA", isDev));
        this.domainMochaVideo = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_MC_VIDEO, getDomainDefault("MC_VIDEO", isDev));
        this.domainKeengMusic = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_KMUSIC, getDomainDefault("KMUSIC", isDev));
        this.domainKeengMusicSearch = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_KMUSIC_SEARCH, getDomainDefault("KMUSIC_SEARCH", isDev));
        this.domainKeengMovies = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_KMOVIES, getDomainDefault("KMOVIES", isDev));
        this.domainNetnews = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_NETNEWS, getDomainDefault("NETNEWS", isDev));
        this.domainTiin = mPref.getString(Constants.PREFERENCE.PREF_DOMAIN_TIIN, getDomainDefault("TIIN", isDev));

        Log.d(TAG, "initDomain"
                + "\nfullDomainFile: " + fullDomainFile
                + "\nfullDomainOnMedia: " + fullDomainOnMedia
                + "\nfullDomainImage: " + fullDomainImage
                + "\nfullDomainMsg: " + fullDomainMsg

                + "\n--------------------"
                + "\nfullDomainGenOtp: " + fullDomainGenOtp
                + "\ndomainFileV1: " + domainFileV1
                + "\ndomainImageV1: " + domainImageV1
                + "\ndomainOnMediaV1: " + domainOnMediaV1
                + "\ndomainMochaVideo: " + domainMochaVideo
                + "\ndomainKeengMusic: " + domainKeengMusic
                + "\ndomainKeengMusicSearch: " + domainKeengMusicSearch
                + "\ndomainKeengMovies: " + domainKeengMovies
                + "\ndomainNetnews: " + domainNetnews
                + "\ndomainTiin: " + domainTiin
        );
    }

    private void initDomainKeeng() {
        this.fullDomainServiceKeeng = Config.PREFIX.PROTOCOL_HTTP
                + mPref.getString(Constants.PREFERENCE.CONFIG.PREF_DOMAIN_SERVICE_KEENG_V2,
                ConfigLocalized.DOMAIN_SERVICE_KEENG);
        this.fullDomainMediaKeeng = Config.PREFIX.PROTOCOL_HTTP
                + mPref.getString(Constants.PREFERENCE.CONFIG.PREF_DOMAIN_MEDIA2_KEENG_V2,
                ConfigLocalized.DOMAIN_MEDIA2_KEENG);
        this.fullDomainImageKeeng = Config.PREFIX.PROTOCOL_HTTP
                + mPref.getString(Constants.PREFERENCE.CONFIG.PREF_DOMAIN_IMAGE_KEENG_V2,
                ConfigLocalized.DOMAIN_IMAGE_KEENG);

        Log.d(TAG, "initDomainKeeng"
                + "\nfullDomainServiceKeeng: " + fullDomainServiceKeeng
                + "\nfullDomainMediaKeeng: " + fullDomainMediaKeeng
                + "\nfullDomainImageKeeng: " + fullDomainImageKeeng
        );
    }

    /**
     * change domain keeng
     * khi lay api get config tu sv thi da put vao share pref.on
     *
     * @param domainService
     * @param domainMedia
     * @param domainImage
     */
    public void updateDomainKeeng(String domainService, String domainMedia, String domainImage) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "updateDomainKeeng old domain"
                    + "\nfullDomainServiceKeeng: " + fullDomainServiceKeeng
                    + "\nfullDomainMediaKeeng: " + fullDomainMediaKeeng
                    + "\nfullDomainImageKeeng: " + fullDomainImageKeeng
            );
            Log.e(TAG, "updateDomainKeeng new domain\ndomainService: " + domainService
                    + "\ndomainMedia: " + domainMedia
                    + "\ndomainImage: " + domainImage);
        }
        if (!TextUtils.isEmpty(domainService)) {
            String domain = Config.PREFIX.PROTOCOL_HTTP + domainService;
            if (!domain.equals(fullDomainServiceKeeng)) {//# moi cap nhat
                this.fullDomainServiceKeeng = domain;
                editor.putString(Constants.PREFERENCE.CONFIG.PREF_DOMAIN_SERVICE_KEENG_V2, domainService).apply();
            }
        }
        if (!TextUtils.isEmpty(domainMedia)) {
            String domain = Config.PREFIX.PROTOCOL_HTTP + domainMedia;
            if (!domain.equals(fullDomainMediaKeeng)) {//# moi cap nhat
                this.fullDomainMediaKeeng = domain;
                editor.putString(Constants.PREFERENCE.CONFIG.PREF_DOMAIN_MEDIA2_KEENG_V2, domainMedia).apply();
            }
        }
        if (!TextUtils.isEmpty(domainImage)) {
            String domain = Config.PREFIX.PROTOCOL_HTTP + domainImage;
            if (!domain.equals(fullDomainImageKeeng)) {//# moi cap nhat
                this.fullDomainImageKeeng = domain;
                editor.putString(Constants.PREFERENCE.CONFIG.PREF_DOMAIN_IMAGE_KEENG_V2, domainImage).apply();
            }
        }
    }

    public boolean updateDomainFileV1(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainFileV1 oldDomain: " + domainFileV1 + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainFileV1)) {
                this.domainFileV1 = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_FILE_V1, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainImageV1(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainImageV1 oldDomain: " + domainImageV1 + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainImageV1)) {
                this.domainImageV1 = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_IMAGE_V1, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainOnMediaV1(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainOnMediaV1 oldDomain: " + domainOnMediaV1 + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainOnMediaV1)) {
                this.domainOnMediaV1 = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_ON_MEDIA_V1, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainMochaVideo(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainMochaVideo oldDomain: " + domainMochaVideo + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainMochaVideo)) {
                this.domainMochaVideo = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_MC_VIDEO, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainKeengMusic(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainKeengMusic oldDomain: " + domainKeengMusic + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainKeengMusic)) {
                this.domainKeengMusic = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_KMUSIC, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainKeengMovies(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainKeengMovies oldDomain: " + domainKeengMovies + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainKeengMovies)) {
                this.domainKeengMovies = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_KMOVIES, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainNetNews(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainNetNews oldDomain: " + domainNetnews + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainNetnews)) {
                this.domainNetnews = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_NETNEWS, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainTiin(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainTiin oldDomain: " + domainTiin + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainTiin)) {
                this.domainTiin = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_TIIN, domain).apply();
                return true;
            }
        }
        return false;
    }

    public boolean updateDomainKeengMusicSearch(String newDomain) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "updateDomainKeengMusicService oldDomain: " + domainKeengMusicSearch + "\n->\tnewDomain: " + newDomain);
        if (!TextUtils.isEmpty(newDomain)) {
            String domain = newDomain;
            if (!domain.equals(this.domainKeengMusicSearch)) {
                this.domainKeengMusicSearch = domain;
                if (editor != null)
                    editor.putString(Constants.PREFERENCE.PREF_DOMAIN_KMUSIC_SEARCH, domain).apply();
                return true;
            }
        }
        return false;
    }

    public String getDomainFile() {
        if (TextUtils.isEmpty(domainFileV1)) {
            initDomain();
        }
        return domainFileV1;
    }

    public String getDomainMessage() {
        if (TextUtils.isEmpty(fullDomainMsg)) {
            initDomain();
        }
        if (fullDomainMsg.contains("171.255.139.133")) {
            return fullDomainMsg.replace("171.255.139.133", "171.255.193.142");
        }
        return fullDomainMsg;
    }

    public String getDomainImage() {
        if (TextUtils.isEmpty(domainImageV1)) {
            initDomain();
        }
        return domainImageV1;
    }

    public String getDomainOnMedia() {
        if (TextUtils.isEmpty(domainOnMediaV1)) {
            initDomain();
        }
        return domainOnMediaV1;
    }

    public String getDomainMochaVideo() {
        if (TextUtils.isEmpty(domainMochaVideo)) {
            initDomain();
        }
        return domainMochaVideo;
    }

    public String getDomainKeengMusic() {
        if (TextUtils.isEmpty(domainKeengMusic)) {
            initDomain();
        }
        return domainKeengMusic;
    }

    public String getDomainKeengMusicSearch() {
        if (TextUtils.isEmpty(domainKeengMusicSearch)) {
            initDomain();
        }
        return domainKeengMusicSearch;
    }

    public String getDomainKeengMovies() {
        if (TextUtils.isEmpty(domainKeengMovies)) {
            initDomain();
        }
        return domainKeengMovies;
    }

    public String getDomainNetnews() {
        if (TextUtils.isEmpty(domainNetnews)) {
            initDomain();
        }
        return domainNetnews;
    }

    public String getDomainTiin() {
        if (TextUtils.isEmpty(domainTiin)) {
            initDomain();
        }
        return domainTiin;
    }

    public String getDomainKeengMusicImage() {
        if (TextUtils.isEmpty(fullDomainImageKeeng)) {
            initDomainKeeng();
        }
        return fullDomainImageKeeng;
    }

    public String getDomainFileOld() {
        return fullDomainFile;
    }

    public String getDomainImageOld() {
        return fullDomainImage;
    }

    public String getDomainOnMediaOld() {
        return fullDomainOnMedia;
    }

    public String getDomainOTP() {
        return fullDomainGenOtp;
    }

    public String getDomainImageSearch() {
        return getDomainFile() + "/api/keeng_thumb/";
    }

    public String getDomainDefault(String key, boolean isDev) {
        if (key != null) {
            switch (key) {
                case "OTP":
                    return isDev ? ConfigLocalized.DOMAIN_GEN_OTP_TEST : ConfigLocalized.DOMAIN_GEN_OTP;
                case "FILE":
                    return isDev ? ConfigLocalized.DOMAIN_FILE_V1_TEST : ConfigLocalized.DOMAIN_FILE_V1;
                case "IMAGE":
                    return isDev ? ConfigLocalized.DOMAIN_IMAGE_V1_TEST : ConfigLocalized.DOMAIN_IMAGE_V1;
                case "MC_VIDEO":
                    return isDev ? ConfigLocalized.DOMAIN_MC_VIDEO_TEST : ConfigLocalized.DOMAIN_MC_VIDEO;
                case "KMUSIC":
                    return isDev ? ConfigLocalized.DOMAIN_KMUSIC_TEST : ConfigLocalized.DOMAIN_KMUSIC;
                case "KMUSIC_SEARCH":
                    return isDev ? ConfigLocalized.DOMAIN_KMUSIC_SEARCH_TEST : ConfigLocalized.DOMAIN_KMUSIC_SEARCH;
                case "KMOVIES":
                    return isDev ? ConfigLocalized.DOMAIN_KMOVIES_TEST : ConfigLocalized.DOMAIN_KMOVIES;
                case "NETNEWS":
                    return isDev ? ConfigLocalized.DOMAIN_NETNEWS_TEST : ConfigLocalized.DOMAIN_NETNEWS;
                case "TIIN":
                    return isDev ? ConfigLocalized.DOMAIN_TIIN_TEST : ConfigLocalized.DOMAIN_TIIN;
                case "ON_MEDIA":
                    return isDev ? ConfigLocalized.DOMAIN_ON_MEDIA_V1_TEST : ConfigLocalized.DOMAIN_ON_MEDIA_V1;
            }
        }
        return "";
    }
}
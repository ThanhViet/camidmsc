package com.metfone.selfcare.helper.message;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Patterns;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by toanvk2 on 05/11/2015.
 */
public class SpamRoomManager {
    private final String TAG = SpamRoomManager.class.getSimpleName();
    public static final int SPAM_DUPLICATE_TEXT = 3;
    public static final int SPAM_DUPLICATE_STICKER = 4;
    public static final int SPAM_MUSIC = 4;
    public static final int STATE_LOCK_1 = 1;// khoa lan 1
    public static final int STATE_LOCK_2 = 2;// khoa lan 2
    private static SpamRoomManager mInstance;
    private static final long TIME_CHECK = 10 * 1000;// 10s
    public static final long TIME_LOCK_1 = 60 * 1000;       // 1 phut
    public static final long TIME_LOCK_2 = 5 * 60 * 1000;   // 5 phut
    private static final int MESSAGE_MAX_SIZE = 5;
    private LinkedList<ReengMessage> listMessageSend;
    private ApplicationController mApplication;
    private Resources mRes;
    private SharedPreferences mPref;
    // luu trang thai bi lock
    private int lockState = 0;          // khong bi lock
    private long lockTimeStart = 0;     // thoi gian bat dau lock
    private long lockTime = 0;          // thoi gian bi lock
    // chan spam noi dung giong nhau, sticker giong nhau, nhac
    private String messageHistoryStr = "";
    private int lastCollectionId = -1, lastItemId = -1;

    private SpamRoomManager(ApplicationController app) {
        mApplication = app;
        this.mRes = mApplication.getResources();
        this.mPref = app.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        listMessageSend = new LinkedList<>();
        getStateLockFromPref();
    }

    public static synchronized SpamRoomManager getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new SpamRoomManager(application);
        }
        return mInstance;
    }

    /**
     * @param message
     * @return add thanh cong
     */
    public int addMessageAfterSend(ThreadMessage threadMessage, ReengMessage message) {
        int status = -1;
        //check spam message
        switch (message.getMessageType()) {
            case text:
                if (messageHistoryStr.equals(message.getContent())) {
                    status = SPAM_DUPLICATE_TEXT;
                    Log.d(TAG, "spam text duplicate");
                } else {
                    messageHistoryStr = message.getContent();
                }
                break;
            case voiceSticker:
                int collectionId = TextHelper.parserIntFromString(message.getFileName(), -1);
                int itemId = (int) message.getSongId();
                if (collectionId == lastCollectionId && itemId == lastItemId) {
                    status = SPAM_DUPLICATE_STICKER;
                    Log.d(TAG, "spam sticker duplicate");
                } else {
                    lastCollectionId = collectionId;
                    lastItemId = itemId;
                }
                break;
            /*case inviteShareMusic:
                long currentTime = TimeHelper.getCurrentTime();
                long lastTime = threadMessage.getLastTimeShareMusic();
                long diff = currentTime - lastTime;
                if (diff < Config.Message.SHARE_MUSIC_TIME_DELAY) {
                   *//* int time = Math.max((int) ((Config.Message.SHARE_MUSIC_TIME_DELAY - diff) / 60000L), 1);
                    Log.d(TAG, time + "");
                    String strTime = "";
                    if (time <= 1) {
                        strTime = "1" + " " + mChatActivity.getString(R.string.minute_share_music);
                    } else {
                        strTime = String.valueOf(time) + " " + mChatActivity.getString(R.string.minutes_share_music);
                    }*//*
                    //mChatActivity.showToast(String.format(mChatActivity.getResources().getString(R.string.err_share_music_too_close), strTime), Toast.LENGTH_LONG);
                    status = SPAM_MUSIC;
                    Log.d(TAG, "spam music ********");
                } else {
                    threadMessage.setLastTimeShareMusic(currentTime);
                    mApplication.getMessageBusiness().updateThreadMessage(threadMessage);
                }
                break;*/
            default:
                break;
        }
        if (status != -1)// tra lai loi luon
            return status;
        if (listMessageSend.size() >= MESSAGE_MAX_SIZE) {
            // lay message cuoi cung
            ReengMessage lastMessage = listMessageSend.getLast();
            if (isSpamRoom(lastMessage, message)) {// bi lock
                if (lockState == STATE_LOCK_1) {
                    lockState = STATE_LOCK_2;
                    lockTime = TIME_LOCK_2;
                } else if (lockState == STATE_LOCK_2) {
                    lockState = STATE_LOCK_2;
                    lockTime = TIME_LOCK_2;
                } else {
                    lockState = STATE_LOCK_1;
                    lockTime = TIME_LOCK_1;
                }
                fakeOfficialAlert();
                lockTimeStart = message.getTime();
                saveStateLockToPref();
                listMessageSend.clear();
                status = lockState;
            } else {
                // day cai cuoi cung khoi list
                listMessageSend.pollLast();
                listMessageSend.addFirst(message);
            }
        } else {
            // add vao dau
            listMessageSend.addFirst(message);
        }
        return status;
    }

    private boolean isSpamRoom(ReengMessage lastMessage, ReengMessage firstMessage) {
        long lastTime = lastMessage.getTime();
        long firstTime = firstMessage.getTime();
        long differentTime = Math.abs(firstTime - lastTime);
        // tin dau vao tin moi nho hon hoac bang time check
        return differentTime <= TIME_CHECK;
    }

    public long getTimeLock() {
        if (lockState == STATE_LOCK_1 || lockState == STATE_LOCK_2) {// state dang lock, check thoi gian
            if (lockTime == 0) {// het thoi gian lock
                return 0;
            } else {
                long currentTime = TimeHelper.getCurrentTime();
                long differentTime = Math.abs(currentTime - lockTimeStart);
                // thoi gian hien tai tru thoi gian lock chua qua thoi gian bi lock thi tra ve different time
                if (differentTime < lockTime) {
                    return (lockTime - differentTime);
                } else {// reset thanh ko bi lock nua
                    lockTimeStart = 0;
                    lockTime = 0;
                    saveStateLockToPref();
                    return 0;
                }
            }
        } else {
            return 0;
        }
    }

    private void saveStateLockToPref() {
        try {
            JSONObject objectLock = new JSONObject();
            objectLock.put("lockState", lockState);
            objectLock.put("lockTimeStart", lockTimeStart);
            objectLock.put("lockTime", lockTime);
            mPref.edit().putString(Constants.PREFERENCE.PREF_LOCK_SPAM_ROOM_CHAT, objectLock.toString()).apply();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            mPref.edit().putString(Constants.PREFERENCE.PREF_LOCK_SPAM_ROOM_CHAT, "").apply();
        }
    }

    private void getStateLockFromPref() {
        try {
            String statePref = mPref.getString(Constants.PREFERENCE.PREF_LOCK_SPAM_ROOM_CHAT, "");
            if (!TextUtils.isEmpty(statePref)) {
                JSONObject objectLock = new JSONObject(statePref);
                if (objectLock.has("lockState")) {
                    lockState = objectLock.getInt("lockState");
                }
                if (objectLock.has("lockTimeStart")) {
                    lockTimeStart = objectLock.getLong("lockTimeStart");
                }
                if (objectLock.has("lockTime")) {
                    lockTime = objectLock.getLong("lockTime");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "JSONException", e);
        }
    }

    public long getMaxTimeLock(int stateLock) {
        long t;
        switch (stateLock) {
            case STATE_LOCK_1:
                t = TIME_LOCK_1;
                break;
            case STATE_LOCK_2:
                t = TIME_LOCK_2;
                break;
            case SPAM_MUSIC:
                t = Config.Message.SHARE_MUSIC_TIME_DELAY;
                break;
            default:
                t = 0;
                break;
        }
        return t;
    }

    private void fakeOfficialAlert() {
        String content = "Hệ thống cho rằng bạn đang có dấu hiệu spam tin nhắn khi tham gia room nhạc. Tài khoản của bạn có thể bị cấm nhắn tin trong room nhạc nếu tiếp tục vi phạm.\n\nTham khảo thêm tại http://mocha.com.vn/provision.html. Nếu bạn có khiếu nại hoặc phản hồi, vui lòng liên hệ mocha@viettel.com.vn";
        String regionCode = mApplication.getReengAccountBusiness().getRegionCode();
        if ("HT".equals(regionCode)) {
            //TODO Text tieng phap
            //content = "Hệ thống cho rằng bạn đang có dấu hiệu spam tin nhắn khi tham gia room nhạc. Tài khoản của bạn có thể bị cấm nhắn tin trong room nhạc nếu tiếp tục vi phạm.\nTài khoản của bạn có thể bị cấm nhắn tin trong room nhạc vĩnh viễn nếu tiếp tục vi phạm. Tham khảo thêm tại http://mocha.com.vn/provision.html. Nếu bạn có khiếu nại hoặc phản hồi, vui lòng liên hệ mocha@viettel.com.vn";
        }
        MessageBusiness messageBusiness = mApplication.getMessageBusiness();
        ThreadMessage mThread = messageBusiness.findExistingOrCreateOfficialThread();
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        ReengMessage reengMessage = messageBusiness.createFakeMessageText(mThread.getId(), mThread.getServerId(), myNumber, content);
        messageBusiness.notifyReengMessage(mApplication, mThread, reengMessage, ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT);
    }

    public boolean checkSpamLinkTextThreadStranger(String friendNumber, ReengMessage message) {
        long t = System.currentTimeMillis();
        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendNumber);
        if (nonContact != null && nonContact.getStateFollow() != -2 &&
                nonContact.getStateFollow() != Constants.CONTACT.FOLLOW_STATE_FRIEND) {
            if (message.getMessageType() == ReengMessageConstant.MessageType.text) {
                return detectLink(Patterns.WEB_URL, message.getContent(), Constants.SMART_TEXT.MAX_LENGTH_URL);
            }
        }
        Log.d(TAG, "checkSpamLinkTextThreadStranger take []: " + (System.currentTimeMillis() - t));
        return false;
    }

    public boolean checkSpamImageThreadStranger(ThreadMessage threadMessage, String friendNumber) {
        long t = System.currentTimeMillis();
        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(friendNumber);
        if (nonContact != null && nonContact.getStateFollow() != -2 &&
                nonContact.getStateFollow() != Constants.CONTACT.FOLLOW_STATE_FRIEND) {
            return true;
        }
        Log.d(TAG, "checkSpamImageThreadStranger take []: " + (System.currentTimeMillis() - t));
        return false;
    }

    public boolean isSpamInComingMessage(NonContact nonContact, ReengMessage message) {
        if (message == null) return false;
        if (nonContact.getStateFollow() != Constants.CONTACT.FOLLOW_STATE_FRIEND) {
            if (message.getMessageType() == ReengMessageConstant.MessageType.text) {
                return detectLink(Patterns.WEB_URL, message.getContent(), Constants.SMART_TEXT.MAX_LENGTH_URL);
            } else if (message.getMessageType() == ReengMessageConstant.MessageType.image) {
                return true;
            }
        }
        return false;
    }

    private boolean detectLink(Pattern pattern, String linkableText, int minLength) {
        if (TextUtils.isEmpty(linkableText)) return false;
        Matcher m = pattern.matcher(linkableText);
        boolean isFind = false;
        while (m.find() && !isFind) {
            if ((m.end() - m.start()) >= minLength) {
                isFind = true;
            }
        }
        return isFind;
    }
}
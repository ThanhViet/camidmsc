package com.metfone.selfcare.helper;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.metfone.selfcare.app.dev.ApplicationController;

/**
 * Created by thanhnt72 on 4/4/2018.
 */

public class SignalStrengthHelper {

    private static final String TAG = SignalStrengthHelper.class.getSimpleName();

    private int signalSupport = 99;
    private ApplicationController mApp;

    TelephonyManager mTelephonyManager;
    MyPhoneStateListener mPhoneStatelistener;

    private static SignalStrengthHelper mInstant;


    public static synchronized SignalStrengthHelper getInstant(ApplicationController app) {
        if (mInstant == null) {
            mInstant = new SignalStrengthHelper(app);
        }
        return mInstant;
    }

    public SignalStrengthHelper(ApplicationController mApp) {
        this.mApp = mApp;
        mPhoneStatelistener = new MyPhoneStateListener();
        mTelephonyManager = (TelephonyManager) mApp.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public void startListener(){
        mTelephonyManager.listen(mPhoneStatelistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    public void stopListener() {
        mTelephonyManager.listen(mPhoneStatelistener, PhoneStateListener.LISTEN_NONE);
    }

    public int getSignalStrength(){
        return signalSupport;
    }

    class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            /*SIGNAL_STRENGTH_NONE_OR_UNKNOWN (99)
SIGNAL_STRENGTH_GREAT (16-32)
SIGNAL_STRENGTH_GOOD (8-15)
SIGNAL_STRENGTH_MODERATE (4-7)
SIGNAL_STRENGTH_POOR (0-3)*/

            signalSupport = signalStrength.getGsmSignalStrength();
            Log.d(TAG, "------ gsm signal --> " + signalSupport);

            /*if (signalSupport == 99) {
                Log.d(TAG, "Signal GSM : NONE_OR_UNKNOWN");
            }

            if (signalSupport >= 0 && signalSupport <= 3) {
                Log.d(TAG, "Signal GSM : Poor");


            } else if (signalSupport >= 4 && signalSupport <= 7) {
                Log.d(TAG, "Signal GSM : MODERATE");


            } else if (signalSupport >= 8 && signalSupport <= 15) {
                Log.d(TAG, "Signal GSM : GOOD");


            } else if (signalSupport >= 16 && signalSupport <= 32) {
                Log.d(TAG, "Signal GSM : GREAT");

            }*/
        }
    }
}

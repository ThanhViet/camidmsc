package com.metfone.selfcare.helper.httprequest;

import android.content.res.Resources;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 12/12/2015.
 */
public class CrbtRequestHelper {
    private static final String TAG = CrbtRequestHelper.class.getSimpleName();
    private static CrbtRequestHelper instance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private ReengMessage messageCrbt;

    private CrbtRequestHelper(ApplicationController application) {
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        mRes = mApplication.getResources();
    }

    public static synchronized CrbtRequestHelper getInstance(ApplicationController application) {
        if (instance == null) {
            instance = new CrbtRequestHelper(application);
        }
        return instance;
    }

    public void sendCrbtMusic(ThreadMessage threadMessage, final String giftReceiver, final MediaModel songModel, final onResponseGiftCrbt listener) {
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        final String fromNick;
        final String external;
        StrangerPhoneNumber strangerPhoneNumber = threadMessage.getStrangerPhoneNumber();
        if (strangerPhoneNumber != null && threadMessage.isStranger()) {
            external = strangerPhoneNumber.getAppId();
            if (TextUtils.isEmpty(strangerPhoneNumber.getMyName())) {
                fromNick = mApplication.getReengAccountBusiness().getUserName();
            } else {
                fromNick = strangerPhoneNumber.getMyName();
            }
        } else {
            external = "";
            fromNick = "";
        }
        // mApplication.trackingEvent(res.getString(R.string.ga_categorey_contacts), res.getString(R.string.ga_action_interaction), res.getString(R.string.ga_label_add_contact));
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CRBT_GIFT);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        int errorCode = -1;
                        String des = null;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (responseObject.has(Constants.HTTP.REST_DESC)) {
                                des = responseObject.getString(Constants.HTTP.REST_DESC);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                String session = "";
                                listener.onResponse(session);
                            } else {
                                if (TextUtils.isEmpty(des)) {
                                    des = mRes.getString(R.string.e601_error_but_undefined);
                                }
                                listener.onError(des);
                            }
                        } catch (Exception e) {
                            Log.e(TAG,"Exception",e);
                            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String songInfo = getSongInfo(songModel);
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(giftReceiver).
                        append(songInfo).
                        append(external).
                        append(fromNick).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.CRBT.FROM, mAccountBusiness.getJidNumber());
                params.put(Constants.HTTP.CRBT.TO, giftReceiver);
                params.put(Constants.HTTP.CRBT.CRBT_SONG_INFO, songInfo);
                params.put(Constants.HTTP.CRBT.EXTERNAL, external);
                params.put(Constants.HTTP.CRBT.FROM_NICK, fromNick);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY,
                        HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void acceptCrbtMusic(ThreadMessage threadMessage, ReengMessage message, final String giftSender, final String session,
                                final MediaModel songModel, final onResponseAcceptCrbt listener) {
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        final String fromNick;
        final String toNick;
        final String external;
        StrangerPhoneNumber strangerPhoneNumber = threadMessage.getStrangerPhoneNumber();
        if (strangerPhoneNumber != null && threadMessage.isStranger()) {
            external = strangerPhoneNumber.getAppId();
            if (TextUtils.isEmpty(strangerPhoneNumber.getMyName())) {
                toNick = mApplication.getReengAccountBusiness().getUserName();
            } else {
                toNick = strangerPhoneNumber.getMyName();
            }
            if (TextUtils.isEmpty(strangerPhoneNumber.getFriendName())) {
                fromNick = Utilities.hidenPhoneNumber(giftSender);
            } else {
                fromNick = strangerPhoneNumber.getFriendName();
            }
        } else {
            external = "";
            toNick = "";
            fromNick = "";
        }
        messageCrbt = message;
        //mApplication.trackingEvent(mRes.getString(R.string.ga_categorey_contacts), mRes.getString(R.string.ga_action_interaction), res.getString(R.string.ga_label_add_contact));
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CRBT_ACCEPT);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        Log.i(TAG, "onResponse: decrypt: " + decryptResponse);
                        int errorCode = -1;
                        String des = null;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (responseObject.has(Constants.HTTP.REST_DESC)) {
                                des = responseObject.getString(Constants.HTTP.REST_DESC);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                messageCrbt.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
                                mApplication.getMessageBusiness().updateAllFieldsOfMessage(messageCrbt);
                                listener.onResponse();
                            } else {
                                if (TextUtils.isEmpty(des)) {
                                    des = mRes.getString(R.string.e601_error_but_undefined);
                                }
                                listener.onError(des);
                            }
                        } catch (Exception e) {
                            Log.e(TAG,"Exception",e);
                            listener.onError(mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error:", error);
                listener.onError(mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                String songInfo = getSongInfo(songModel);
                StringBuilder sb = new StringBuilder().
                        append(giftSender).
                        append(mAccountBusiness.getJidNumber()).
                        append(session).
                        append(songInfo).
                        append(external).
                        append(fromNick).
                        append(toNick).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.CRBT.FROM, giftSender);
                params.put(Constants.HTTP.CRBT.TO, mAccountBusiness.getJidNumber());
                params.put(Constants.HTTP.CRBT.SESSION, session);
                params.put(Constants.HTTP.CRBT.CRBT_SONG_INFO, songInfo);
                params.put(Constants.HTTP.CRBT.EXTERNAL, external);
                params.put(Constants.HTTP.CRBT.FROM_NICK, fromNick);
                params.put(Constants.HTTP.CRBT.TO_NICK, toNick);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY,
                        HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }


    private String getSongInfo(MediaModel songModel) {
        try {
            JSONObject songObject = new JSONObject();
            songObject.put("songid", songModel.getId());
            songObject.put("songname", songModel.getName());
            songObject.put("singername", songModel.getSinger());
            songObject.put("songurl", songModel.getUrl());
            songObject.put("mediaurl", songModel.getMedia_url());
            songObject.put("songthumb", songModel.getImage());
            songObject.put("crbt_code", songModel.getCrbtCode());
            songObject.put("crbt_price", songModel.getCrbtPrice());
            return songObject.toString();
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
        return "";
    }

    public interface onResponseAcceptCrbt {
        void onResponse();

        void onError(String msgError);
    }

    public interface onResponseGiftCrbt {
        void onResponse(String session);

        void onError(String msgError);
    }
}
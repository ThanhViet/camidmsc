package com.metfone.selfcare.helper;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ConfigurationInfo;
import android.content.res.Resources;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.restful.StringRequest;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by thanh on 3/12/2015.
 */
public class LocationHelper {
    private static final String TAG = LocationHelper.class.getSimpleName();
    // The minimum distance to change Updates in meters
    private static final String THUMBNAIL_MAPS_URL = "https://maps.googleapis.com/maps/api/staticmap?center=%s,%s&zoom=%d&size=%dx%d&markers=color:purple|%s,%s";
    private static final String DIRECT_MAPS = "http://maps.google.com/maps?saddr=%1$s,%2$s&daddr=%3$s,%4$s";
    private static final String MAPS_VIEW = "http://maps.google.com/maps?q=loc:%1$s,%2$s(%3$s)&z=14";
    private static LocationHelper mInstant;
    private ApplicationController mApplication;
    private Context mContext;
    private Resources mRes;
    private LocationManager mLocationManager;
    private Location currentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private SharedPreferences mPref;
    private String currentLocationPrefString;
    private static RequestActivityResult mCallBack;

    public static synchronized LocationHelper getInstant(ApplicationController app) {
        if (mInstant == null) {
            mInstant = new LocationHelper(app);
        }
        return mInstant;
    }

    private LocationHelper(ApplicationController app) {
        this.mApplication = app;
        this.mContext = mApplication.getApplicationContext();
        this.mRes = mContext.getResources();
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        mLocationManager = (LocationManager) mApplication.getSystemService(Context.LOCATION_SERVICE);
        getLocationFromPref();
    }

    public boolean checkLocationProviders() {
        return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) &&
                mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isLocationServiceEnabled() {
        return isGPSLocationEnabled() || isNetworkLocationEnabled();
    }

    public boolean isNetworkLocationEnabled() {
        if (mLocationManager == null) return false;
        return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public boolean isGPSLocationEnabled() {
        if (mLocationManager == null) return false;
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isHighAccuracyLocationEnabled() {
        return isGPSLocationEnabled() && isNetworkLocationEnabled();
    }

    public boolean checkGooglePlayService() {
        return FireBaseHelper.getInstance(mApplication).checkPlayServices();
    }

    /**
     * kiem tra may co ho tro GLE v2 khong
     *
     * @return
     */
    public boolean checkSupportGLEV2() {
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        Log.d(TAG, "glEsVersionV2Available---getGlEsVersion : " + configurationInfo.getGlEsVersion() +
                " reqGlEsVersion: " + configurationInfo.reqGlEsVersion);
        return (configurationInfo.reqGlEsVersion >= 0x20000);
    }

    public void showDialogGGPlayService(BaseSlidingFragmentActivity activity, ClickListener.IconListener listener) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.note_title);
        String msg = mRes.getString(R.string.need_install_play_service);
        PopupHelper.getInstance().showDialogConfirm(activity, title,
                msg, labelOK, labelCancel, listener, null, Constants.MENU.POPUP_OK_GG_PLAY_SERVICE);
    }

    public void showDialogSettingLocationProviders(BaseSlidingFragmentActivity activity, ClickListener.IconListener listener) {
        String labelOK = mRes.getString(R.string.ok);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.note_title);
        String msg = mRes.getString(R.string.need_turn_on_location);
        PopupHelper.getInstance().showDialogConfirm(activity, title,
                msg, labelOK, labelCancel, listener, null, Constants.ACTION.ACTION_SETTING_LOCATION);
    }
    public void showDialogSettingLocationProvidersNews(BaseSlidingFragmentActivity activity, ClickListener.IconListener listener) {
        String labelOK = mRes.getString(R.string.off_location);
        String labelCancel = mRes.getString(R.string.return_location);
        String title = mRes.getString(R.string.title_location);
        String msg = mRes.getString(R.string.content_location);
        PopupHelper.getInstance().showDialogConfirm(activity, title,
                msg, labelOK, labelCancel, listener, null, Constants.ACTION.ACTION_SETTING_LOCATION);
    }


    public void showDialogSettingHighLocation(BaseSlidingFragmentActivity activity, ClickListener.IconListener listener) {
        if (mPref != null) {
            boolean notShowAgain = mPref.getBoolean(Constants.PREFERENCE.PREF_LOCATION_NOT_SHOW_AGAIN, false);
            if (notShowAgain) return;
        }
        String labelOK = mRes.getString(R.string.setting);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mRes.getString(R.string.location_improvement_title);
        String msg = mRes.getString(R.string.location_improvement_des);
        PopupHelper.getInstance().showDialogConfirm(activity, title,
                msg, labelOK, labelCancel, listener, null, Constants.ACTION.ACTION_SETTING_LOCATION, true, true, new ClickListener.CheckboxListener() {
                    @Override
                    public void onChecked(boolean isChecked) {
                        if (mPref != null) {
                            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_LOCATION_NOT_SHOW_AGAIN, isChecked).apply();
                        }
                    }
                });
    }

    /**
     * getBestAvailableLocationProvider
     *
     * @return
     */
    public String getBestAvailableLocationProvider(LocationManager locationManager) {
        // Specify Location Provider criteria
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(true);
        criteria.setBearingRequired(true);
        criteria.setSpeedRequired(true);
        criteria.setCostAllowed(true);
        return locationManager.getBestProvider(criteria, true);
    }

    /**
     * lay dia chi theo toa do
     *
     * @param lat
     * @param lng
     */
    public String getAddressesFromLocation(double lat, double lng) {
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                StringBuilder sb = new StringBuilder();
                Address address = addresses.get(0);
                int addressSize = address.getMaxAddressLineIndex();
                for (int i = 0; i <= addressSize; i++) {
                    if (!TextUtils.isEmpty(address.getAddressLine(i))) {
                        sb.append(address.getAddressLine(i)).append(", ");
                    }
                }
                int sbLenght = sb.length();
                if (sbLenght > 2) {
                    sb.delete(sbLenght - 2, sbLenght);
                }
                return sb.toString();
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public String getThumbnailLocation(String lat, String lng, int zoom, int width, int height) {
        return String.format(THUMBNAIL_MAPS_URL, lat, lng, zoom, width, height, lat, lng);
    }

    public String getUrlDirectIntent(LatLng from, LatLng to) {
        return String.format(DIRECT_MAPS, String.valueOf(from.latitude),
                String.valueOf(from.longitude),
                String.valueOf(to.latitude),
                String.valueOf(to.longitude));
    }

    public String getUrlMapsView(LatLng toLatLng, String address) {
        return String.format(MAPS_VIEW, String.valueOf(toLatLng.latitude),
                String.valueOf(toLatLng.longitude), address);
    }

    private GetLocationListener mGetLocationListener;

    public void getCurrentLatLng(GetLocationListener callback) {
        this.mGetLocationListener = callback;
        if (!checkLocationProviders() && currentLocation != null) {
            mGetLocationListener.onResponse(currentLocation);
        } else if (currentLocation != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (mGetLocationListener != null) {
                mGetLocationListener.onResponse(currentLocation);
            }
        } else {
            connectGoogleApiClient();
        }
    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mApplication)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(connectionFailedListener)
                .addApi(LocationServices.API)
                .build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    public void connectGoogleApiClient() {
        if (declinedPermissionLocation()) return;
        if (mGoogleApiClient == null || mLocationRequest == null) {
            initGoogleApiClient();
        }
        mGoogleApiClient.connect();
    }

    public void disconnectGoogleApiClient() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
            mGoogleApiClient.disconnect();
        }
        mGoogleApiClient = null;
        mLocationRequest = null;
    }

    @SuppressWarnings({"MissingPermission"})
    private ConnectionCallbacks connectionCallbacks = new ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            if (mGoogleApiClient == null || mLocationRequest == null || locationListener == null) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
            } else {
                currentLocation = location;
                saveLastLocationToPref(currentLocation);
                if (mGetLocationListener != null) {
                    mGetLocationListener.onResponse(location);
                }
                //disconnectGoogleApiClient();
                Log.d(TAG, "onConnected: +");
            }
        }

        @Override
        public void onConnectionSuspended(int i) {
            if (mGetLocationListener != null) {
                mGetLocationListener.onFail();
            }
        }
    };

    private OnConnectionFailedListener connectionFailedListener = new OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            if (mGetLocationListener != null) {
                mGetLocationListener.onFail();
            }
        }
    };

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "locationListener: onLocationChanged-: ");
            //handleNewLocation(location);
            if (location != null) {
                currentLocation = location;
                saveLastLocationToPref(currentLocation);
                if (mGetLocationListener != null) {
                    mGetLocationListener.onResponse(location);
                }
            }
            //disconnectGoogleApiClient();
        }
    };

    public void setGetLocationListener(GetLocationListener listener) {
        this.mGetLocationListener = listener;
    }

    public boolean isExistCacheLocation() {
        return !TextUtils.isEmpty(currentLocationPrefString) && currentLocation != null;
    }

    private boolean declinedPermissionLocation() {
        return (PermissionHelper.declinedPermission(mApplication, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                PermissionHelper.declinedPermission(mApplication, Manifest.permission.ACCESS_FINE_LOCATION));
    }

    public void saveLastLocationToPref(Location location) {
        // chưa lưu location thì save lại 1 lần duy nhất, không save khi update tránh đcọ ghi share pref
        if (location != null && TextUtils.isEmpty(currentLocationPrefString)) {
            String value = String.valueOf(location.getLatitude()) + ";" + String.valueOf(location.getLongitude());
            mPref.edit().putString(Constants.PREFERENCE.PREF_AROUND_LOCATION, value).apply();
            currentLocationPrefString = value;
        }
    }
    public void clearCurrentLocation(){
        if(mPref != null){
            mPref.edit().putString(Constants.PREFERENCE.PREF_AROUND_LOCATION, null).apply();
        }
    }

    public void getLocationFromPref() {
        currentLocationPrefString = mPref.getString(Constants.PREFERENCE.PREF_AROUND_LOCATION, null);
        if (!TextUtils.isEmpty(currentLocationPrefString)) {
            String[] temp = currentLocationPrefString.split(";");
            if (temp != null && temp.length == 2) {
                Location l = new Location(LocationManager.GPS_PROVIDER);
                l.setLatitude(TextHelper.parserDoubleFromString(temp[0], 0));
                l.setLongitude(TextHelper.parserDoubleFromString(temp[1], 0));
                currentLocation = l;
            }
        }
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public interface GetLocationListener {
        void onResponse(Location location);

        void onFail();
    }

    public static void onRequestActivityResult(final int requestCode, final int resultCode, final Intent data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mCallBack != null) {
                    mCallBack.onRequestActivityResult(requestCode, resultCode, data);
                }
            }
        }, 500);
    }

    public static synchronized void setCallBack(RequestActivityResult callBack) {
        mCallBack = callBack;
    }

    public interface RequestActivityResult {
        void onRequestActivityResult(int requestCode, int resultCode, Intent data);
    }

    private long DAY_30 = 30 * 24 * 60 * 60;

    public void getMyLocation(final int selectUrl) {
        Log.i(TAG, "getMyLocation: " + selectUrl);
        String listUrlGetLocation = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.GET_LINK_LOCATION);
        if (TextUtils.isEmpty(listUrlGetLocation) || "-".equals(listUrlGetLocation)) return;
        long lastUploadLocation = mPref.getLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOCATION, 0);
        if ((System.currentTimeMillis() / 1000 - lastUploadLocation / 1000) <= DAY_30) return;
        if (!NetworkHelper.isConnectInternet(mApplication)) return;

        String[] listUrl = listUrlGetLocation.split(",");
        if (listUrl.length > selectUrl) {
            String urlGetLocation = listUrl[selectUrl];
            StringRequest req = new StringRequest(com.android.volley.Request.Method.GET, urlGetLocation, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i(TAG, "response getMyLocation: " + s);
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.has("latitude") && jsonObject.has("longitude")) {
                            LocationObject location = new LocationObject(
                                    jsonObject.optString("country_code", ""),
                                    jsonObject.optString("country_name", ""),
                                    jsonObject.optString("city", ""),
                                    jsonObject.optString("postal", ""),
                                    jsonObject.optString("latitude", ""),
                                    jsonObject.optString("longitude", ""),
                                    jsonObject.optString("IPv4", ""),
                                    jsonObject.optString("state", ""));
                            sendMyLocation(location);
                        } else {
                            getMyLocation(selectUrl + 1);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        getMyLocation(selectUrl + 1);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "onError getMyLocation" + volleyError.toString());
                    getMyLocation(selectUrl + 1);
                }
            });
            Log.i(TAG, "getMyLocation: " + urlGetLocation);
            VolleyHelper.getInstance(mApplication).addRequestToQueue(req, TAG, false);
        }

    }

    private void sendMyLocation(final LocationObject locationObject) {
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.POST_LOCATION);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    Log.i(TAG, "response sendMyLocation: " + s);
                    if ("200".equals(jsonObject.optString("code"))) {
                        mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOCATION, System.currentTimeMillis()).apply();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onError sendMyLocation" + volleyError.toString());
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                long timeStamp = System.currentTimeMillis();
                //msisdn+ country_code, + country_name + city + postal + latitude + longitude+ ipv4 + state+ token+timestamp
                String createAt = TimeHelper.formatTimeLocation(timeStamp);
                StringBuilder sb = new StringBuilder();
                sb.append(mApplication.getReengAccountBusiness().getJidNumber())
                        .append(locationObject.countryCode)
                        .append(locationObject.countryName)
                        .append(locationObject.city)
                        .append(locationObject.postal)
                        .append(locationObject.latitude)
                        .append(locationObject.longitude)
                        .append(locationObject.IPv4)
                        .append(locationObject.state)
                        .append(createAt)
                        .append("Android")
                        .append(Config.REVISION)
                        .append(mApplication.getReengAccountBusiness().getToken())
                        .append(timeStamp);
                Log.i(TAG, sb.toString());
                final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mApplication.getReengAccountBusiness().getToken());

                params.put("msisdn", mApplication.getReengAccountBusiness().getJidNumber());
                params.put("country_code", locationObject.countryCode);
                params.put("country_name", locationObject.countryName);
                params.put("city", locationObject.city);
                params.put("postal", locationObject.postal);
                params.put("latitude", locationObject.latitude);
                params.put("longitude", locationObject.longitude);
                params.put("IPv4", locationObject.IPv4);
                params.put("state", locationObject.state);
                params.put("created_at", createAt);
                params.put("clientType", "Android");
                params.put("revision", Config.REVISION);
                params.put("timestamp", String.valueOf(timeStamp));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.i(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public static class LocationObject {
        String countryCode;
        String countryName;
        String city;
        String postal;
        String latitude;
        String longitude;
        String IPv4;
        String state;

        public LocationObject(String countryCode, String countryName, String city, String postal,
                              String latitude, String longitude, String IPv4, String state) {
            this.countryCode = countryCode;
            this.countryName = countryName;
            this.city = city;
            this.postal = postal;
            this.latitude = latitude;
            this.longitude = longitude;
            this.IPv4 = IPv4;
            this.state = state;
        }
    }
}
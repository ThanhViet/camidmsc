package com.metfone.selfcare.helper;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContentConfigBusiness.OnConfigBannerChangeListener;
import com.metfone.selfcare.business.ContentConfigBusiness.OnConfigChangeListener;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.listeners.ContactChangeListener;
import com.metfone.selfcare.listeners.FeedOnMediaListener;
import com.metfone.selfcare.listeners.GsmCallStateChange;
import com.metfone.selfcare.listeners.InitConfigDataListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.LoginFromAnonymousListener;
import com.metfone.selfcare.listeners.OfficerAccountChangeListener;
import com.metfone.selfcare.listeners.ProfileListener;
import com.metfone.selfcare.listeners.SyncContactListner;
import com.metfone.selfcare.listeners.ThreadListChangeListener;
import com.metfone.selfcare.listeners.UpdateMessageListener;
import com.metfone.selfcare.listeners.UploadImageListener;
import com.metfone.selfcare.network.file.DownloadListener;
import com.metfone.selfcare.network.file.DownloadRequest;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 7/5/14.
 */
public class ListenerHelper {
    private static final String TAG = ListenerHelper.class.getSimpleName();
    private static ListenerHelper mInstance;
    private CopyOnWriteArrayList<ContactChangeListener> mContactChangeListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<SyncContactListner> mSyncContactListners ;
    private CopyOnWriteArrayList<ProfileListener> mProfileChangeListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<InitDataListener> mInitDataListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<InitConfigDataListener> mInitConfigDataListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<OfficerAccountChangeListener> mOfficerListeners = new
            CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<OnConfigBannerChangeListener> mConfigBannerListeners = new
            CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<UploadImageListener> mUploadImageListeners = new
            CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<FeedOnMediaListener> mFeedOnMediaListeners = new
            CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<GsmCallStateChange> gsmCallStateListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<DownloadListener> mDownloadListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ThreadListChangeListener> mThreadsChangedListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<UpdateMessageListener> mUpdateMessageListener = new CopyOnWriteArrayList<>();

    private CopyOnWriteArrayList<OnConfigChangeListener> mConfigChangeList = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<PrefixChangeNumberHelper.UpdateListThread> mPrefixUpdateListThread = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<LoginFromAnonymousListener> mLoginAnonymousListener = new CopyOnWriteArrayList<>();


    public static synchronized ListenerHelper getInstance() {
        if (mInstance == null)
            mInstance = new ListenerHelper();
        return mInstance;
    }

    private ListenerHelper() {

    }

    public void addContactChangeListener(ContactChangeListener listener) {
        if (!mContactChangeListeners.contains(listener)) {
            mContactChangeListeners.add(listener);
        }
    }

    public void removeContactChangeListener(ContactChangeListener listener) {
        if (mContactChangeListeners.contains(listener)) {
            mContactChangeListeners.remove(listener);
        }
    }

    public void onContactChanged() {
        if (mContactChangeListeners != null && !mContactChangeListeners.isEmpty()) {
            for (ContactChangeListener listener : mContactChangeListeners) {
                listener.onContactChange();
            }
        } else {
            Log.i(TAG, "no listener for contacts changed");
        }
    }

    public void initContactListComplate(int sizeSupport) {
        if (mContactChangeListeners != null && !mContactChangeListeners.isEmpty()) {
            for (ContactChangeListener listener : mContactChangeListeners) {
                listener.initListContactComplete(sizeSupport);
            }
        } else {
            Log.i(TAG, "no listener for initContactListComplate");
        }
    }


    public void onPresenceChanged(ArrayList<PhoneNumber> listNumber) {
        if (mContactChangeListeners != null && !mContactChangeListeners.isEmpty()) {
            for (ContactChangeListener listener : mContactChangeListeners) {
                listener.onPresenceChange(listNumber);
            }
        } else {
            Log.i(TAG, "no listener for presence changed");
        }
    }

    public void onRosterChanged() {
        if (mContactChangeListeners != null && !mContactChangeListeners.isEmpty()) {
            for (ContactChangeListener listener : mContactChangeListeners) {
                listener.onRosterChange();
            }
        } else {
            Log.i(TAG, "no listener for roster changed");
        }
    }

    public void addProfileChangeListener(ProfileListener listener) {
        if (!mProfileChangeListeners.contains(listener)) {
            mProfileChangeListeners.add(listener);
        }
    }

    public void removeProfileChangeListener(ProfileListener listener) {
        if (mProfileChangeListeners.contains(listener))
            mProfileChangeListeners.remove(listener);
    }

    public void onProfileChange() {
        if (mProfileChangeListeners != null && !mProfileChangeListeners.isEmpty()) {
            for (ProfileListener listener : mProfileChangeListeners) {
                listener.onProfileChange();
            }
        } else {
            Log.i(TAG, "no listener for profile changed");
        }
    }

    public void onRequestFacebookChange(String fullName, String birthDay, int gender) {
        if (mProfileChangeListeners != null && !mProfileChangeListeners.isEmpty()) {
            for (ProfileListener listener : mProfileChangeListeners) {
                listener.onRequestFacebookChange(fullName, birthDay, gender);
            }
        } else {
            Log.i(TAG, "no listener for onRequestFacebook changed");
        }
    }

    public void onAvatarChange(String avatarPath) {
        if (mProfileChangeListeners != null && !mProfileChangeListeners.isEmpty()) {
            for (ProfileListener listener : mProfileChangeListeners) {
                listener.onAvatarChange(avatarPath);
            }
        } else {
            Log.i(TAG, "no listener for onAvatar changed");
        }
    }

    public void onCoverChange(String coverPath) {
        Log.i(TAG, "onCoverChange=" + coverPath);
        if (mProfileChangeListeners != null && !mProfileChangeListeners.isEmpty()) {
            for (ProfileListener listener : mProfileChangeListeners) {
                listener.onCoverChange(coverPath);
            }
        } else {
            Log.i(TAG, "no listener for onCoverChange");
        }
    }

    public void addUploadImageListener(UploadImageListener listener) {
        if (listener != null && !mUploadImageListeners.contains(listener)) {
            mUploadImageListeners.add(listener);
        }
    }

    public void removeUploadImageListener(UploadImageListener listener) {
        if (listener != null && mUploadImageListeners.contains(listener))
            mUploadImageListeners.remove(listener);
    }

    public void onUploadFail(ImageProfile image) {
        Log.i(TAG, "onUploadFail");
        if (mUploadImageListeners != null && !mUploadImageListeners.isEmpty()) {
            for (UploadImageListener listener : mUploadImageListeners) {
                listener.onUploadFail(image);
            }
        } else {
            Log.i(TAG, "no listener for onUploadFail");
        }
    }

    public void onUploadSuccess(ImageProfile image) {
        Log.i(TAG, "onUploadSuccess");
        if (mUploadImageListeners != null && !mUploadImageListeners.isEmpty()) {
            for (UploadImageListener listener : mUploadImageListeners) {
                listener.onUploadSuccess(image);
            }
        } else {
            Log.i(TAG, "no listener for onUploadSuccess");
        }
    }

    public void onStartUploadAlbumImage() {
        Log.i(TAG, "onStartUploadAlbumImage");
        if (mUploadImageListeners != null && !mUploadImageListeners.isEmpty()) {
            for (UploadImageListener listener : mUploadImageListeners) {
                listener.onStartUpload();
            }
        } else {
            Log.i(TAG, "no listener for onStartUploadAlbumImage");
        }
    }

    public void onUploadEnd(int type, ArrayList<ImageProfile> imageProfiles, String message) {
        Log.i(TAG, "onUploadEnd");
        if (mUploadImageListeners != null && !mUploadImageListeners.isEmpty()) {
            for (UploadImageListener listener : mUploadImageListeners) {
                listener.onUploadEnd(type, imageProfiles, message);
            }
        } else {
            Log.i(TAG, "no listener for onUploadEnd");
        }
    }

    public void addInitConfigDataListener(InitConfigDataListener listener) {
        if (!mInitConfigDataListeners.contains(listener))
            mInitConfigDataListeners.add(listener);
    }

    public void removeInitConfigDataListener(InitConfigDataListener listener) {
        if (mInitConfigDataListeners.contains(listener))
            mInitConfigDataListeners.remove(listener);
    }

    public void onConfigDataReadyChange() {
        if (mInitConfigDataListeners != null && !mInitConfigDataListeners.isEmpty()) {
            for (InitConfigDataListener listener : mInitConfigDataListeners) {
                listener.onConfigDataReady();
            }
        } else {
            Log.i(TAG, "no listener for load data complate");
        }
    }

    public void addInitDataListener(InitDataListener listener) {
        if (!mInitDataListeners.contains(listener))
            mInitDataListeners.add(listener);
    }

    public void removeInitDataListener(InitDataListener listener) {
        if (mInitDataListeners.contains(listener))
            mInitDataListeners.remove(listener);
    }

    public void onDataReadyChange() {
        if (mInitDataListeners != null && !mInitDataListeners.isEmpty()) {
            for (InitDataListener listener : mInitDataListeners) {
                listener.onDataReady();
            }
        } else {
            Log.i(TAG, "no listener for load data complate");
        }
    }

    public void addNotifyFeedOnMediaListener(FeedOnMediaListener listener) {
        if (!mFeedOnMediaListeners.contains(listener))
            mFeedOnMediaListeners.add(listener);
    }

    public void removeNotifyFeedOnMediaListener(FeedOnMediaListener listener) {
        if (mFeedOnMediaListeners.contains(listener))
            mFeedOnMediaListeners.remove(listener);
    }

    public void onNotifyFeedOnMedia(boolean needNotify, boolean needToSetSelection) {
        if (mFeedOnMediaListeners != null && !mFeedOnMediaListeners.isEmpty()) {
            for (FeedOnMediaListener listener : mFeedOnMediaListeners) {
                listener.notifyFeedOnMedia(needNotify, needToSetSelection);
            }
        } else {
            Log.i(TAG, "no listener for onNotifyFeedOnMedia");
        }
    }

    public void onPostFeedSuccess(FeedModelOnMedia feed) {
        if (mFeedOnMediaListeners != null && !mFeedOnMediaListeners.isEmpty()) {
            for (FeedOnMediaListener listener : mFeedOnMediaListeners) {
                listener.onPostFeed(feed);
            }
        } else {
            Log.i(TAG, "no listener for onPostFeedSuccess");
        }
    }

    /**
     * officer
     *
     * @param listener
     */
    public void addOfficerAccountListener(OfficerAccountChangeListener listener) {
        if (!mOfficerListeners.contains(listener))
            mOfficerListeners.add(listener);
    }

    public void removeOfficerAcountListener(OfficerAccountChangeListener listener) {
        if (mOfficerListeners.contains(listener))
            mOfficerListeners.remove(listener);
    }

    public void onStickyOfficerAccountChange() {
        if (mOfficerListeners != null && !mOfficerListeners.isEmpty()) {
            for (OfficerAccountChangeListener listener : mOfficerListeners) {
                listener.onStickyOfficerChange();
            }
        } else {
            Log.i(TAG, "no listener for onStickyOfficerAccountChange");
        }
    }

    /**
     * officer
     *
     * @param listener
     */
    public void addConfigBannerListener(OnConfigBannerChangeListener listener) {
        if (!mConfigBannerListeners.contains(listener))
            mConfigBannerListeners.add(listener);
    }

    public void removeConfigBannerListener(OnConfigBannerChangeListener listener) {
        if (mConfigBannerListeners.contains(listener))
            mConfigBannerListeners.remove(listener);
    }

    public void onConfigBannerChanged(ArrayList<BannerMocha> listBanners) {
        if (mConfigBannerListeners != null && !mConfigBannerListeners.isEmpty()) {
            for (OnConfigBannerChangeListener listener : mConfigBannerListeners) {
                listener.onConfigBannerChanged(listBanners);
            }
        } else {
            Log.i(TAG, "no listener for onConfigBannerChanged");
        }
    }

    public void onConfigBannerCallChanged(ArrayList<BannerMocha> listBannersCall) {
        if (mConfigBannerListeners != null && !mConfigBannerListeners.isEmpty()) {
            for (OnConfigBannerChangeListener listener : mConfigBannerListeners) {
                listener.onConfigBannerCallChanged(listBannersCall);
            }
        } else {
            Log.i(TAG, "no listener for onConfigBannerChanged");
        }
    }

    /**
     * gsm call listener
     *
     * @param listener
     */
    public void addGsmCallListener(GsmCallStateChange listener) {
        if (!gsmCallStateListeners.contains(listener))
            gsmCallStateListeners.add(listener);
    }

    public void removeGsmCallListener(GsmCallStateChange listener) {
        if (gsmCallStateListeners.contains(listener))
            gsmCallStateListeners.remove(listener);
    }

    public void onGsmCallStateChanged(int state, String incomingNumber) {
        if (gsmCallStateListeners != null && !gsmCallStateListeners.isEmpty()) {
            for (GsmCallStateChange listener : gsmCallStateListeners) {
                listener.onCallGsmStateChanged(state, incomingNumber);
            }
        } else {
            Log.i(TAG, "no listener for GsmCallStateChange");
        }
    }

    /**
     * download listener
     *
     * @param listener
     */
    public void addDownloadListener(DownloadListener listener) {
        if (!mDownloadListeners.contains(listener))
            mDownloadListeners.add(listener);
    }

    public void removeDownloadListener(DownloadListener listener) {
        if (mDownloadListeners.contains(listener))
            mDownloadListeners.remove(listener);
    }

    public void onDownloadStated(DownloadRequest request) {
        if (mDownloadListeners != null && !mDownloadListeners.isEmpty()) {
            for (DownloadListener listener : mDownloadListeners) {
                listener.onDownloadStarted(request);
            }
        }
    }

    public void onDownloadComplete(DownloadRequest request) {
        if (mDownloadListeners != null && !mDownloadListeners.isEmpty()) {
            for (DownloadListener listener : mDownloadListeners) {
                listener.onDownloadComplete(request);
            }
        }
    }

    public void onDownloadProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
        if (mDownloadListeners != null && !mDownloadListeners.isEmpty()) {
            for (DownloadListener listener : mDownloadListeners) {
                listener.onProgress(request, totalBytes, downloadedBytes, progress);
            }
        }
    }

    public void addThreadsChangedListener(ThreadListChangeListener listener) {
        if (!mThreadsChangedListeners.contains(listener))
            mThreadsChangedListeners.add(listener);
    }

    public void removeThreadsChangedListener(ThreadListChangeListener listener) {
        if (mThreadsChangedListeners.contains(listener))
            mThreadsChangedListeners.remove(listener);
    }

    public void onThreadsChanged() {
        if (mThreadsChangedListeners != null && !mThreadsChangedListeners.isEmpty()) {
            for (ThreadListChangeListener listener : mThreadsChangedListeners) {
                listener.onThreadsChanged();
            }
        }
    }

    public void addUpdateMessageListener(UpdateMessageListener listener) {
        if (!mUpdateMessageListener.contains(listener))
            mUpdateMessageListener.add(listener);
    }

    public void removeUpdateMessageListener(UpdateMessageListener listener) {
        if (mUpdateMessageListener.contains(listener))
            mUpdateMessageListener.remove(listener);
    }

    public void onUpdateMessageDone() {
        if (mUpdateMessageListener != null && !mUpdateMessageListener.isEmpty()) {
            for (UpdateMessageListener listener : mUpdateMessageListener) {
                listener.onUpdateMessageDone();
            }
        }
    }


    public void addConfigChangeListener(OnConfigChangeListener listener) {
        if (!mConfigChangeList.contains(listener))
            mConfigChangeList.add(listener);
    }

    public void removeConfigChangeListener(OnConfigChangeListener listener) {
        if (mConfigChangeList.contains(listener))
            mConfigChangeList.remove(listener);
    }

    public void onConfigStickyBannerChanged() {
        if (mConfigChangeList != null && !mConfigChangeList.isEmpty()) {
            for (OnConfigChangeListener listener : mConfigChangeList) {
                listener.onConfigStickyBannerChanged();
            }
        } else {
            Log.i(TAG, "no listener for onConfigStickyBannerChanged");
        }
    }

    public void onConfigTabChanged(ApplicationController mApp) {
        TabHomeHelper.getInstant(mApp).initData();

        if (mConfigChangeList != null && !mConfigChangeList.isEmpty()) {
            for (OnConfigChangeListener listener : mConfigChangeList) {
                listener.onConfigTabChange();
            }
        } else {
            Log.i(TAG, "no listener for onConfigTabChanged");
        }

        removeShortcut(mApp);
    }

    private void removeShortcut(ApplicationController mApp) {
        if (!mApp.getConfigBusiness().isEnableTabVideo())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_video), mApp);

        if (!mApp.getConfigBusiness().isEnableTabMovie())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_movie), mApp);

        if (!mApp.getConfigBusiness().isEnableTabMusic())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_music), mApp);

        if (!mApp.getConfigBusiness().isEnableTabNews())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_news), mApp);

        if (!mApp.getConfigBusiness().isEnableTabStranger())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_stranger), mApp);

        if (!mApp.getConfigBusiness().isEnableOnMedia())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_hot), mApp);

        if (!mApp.getConfigBusiness().isEnableTabTiin())
            MochaShortcutManager.disableShortcut(MochaShortcutManager.convertShortcutId(TabHomeHelper.HomeTab.tab_tiins), mApp);
    }

    public void addUpdateAllThreadListener(PrefixChangeNumberHelper.UpdateListThread listener) {
        if (!mPrefixUpdateListThread.contains(listener))
            mPrefixUpdateListThread.add(listener);
    }

    public void removeUpdateAllThreadListener(PrefixChangeNumberHelper.UpdateListThread listener) {
        if (mPrefixUpdateListThread.contains(listener))
            mPrefixUpdateListThread.remove(listener);
    }

    public void onUpdateAllThread(HashSet<ThreadMessage> hashSet) {
        if (mPrefixUpdateListThread != null && !mPrefixUpdateListThread.isEmpty()) {
            for (PrefixChangeNumberHelper.UpdateListThread listener : mPrefixUpdateListThread) {
                listener.onUpdateListThread(hashSet);
            }
        } else {
            Log.i(TAG, "no listener for mPrefixUpdateListThread");
        }
    }

    public void addLoginAnonymousListener(LoginFromAnonymousListener listener) {
        if (!mLoginAnonymousListener.contains(listener))
            mLoginAnonymousListener.add(listener);
    }

    public void removeLoginAnonymousListener(LoginFromAnonymousListener listener) {
        if (mLoginAnonymousListener.contains(listener))
            mLoginAnonymousListener.remove(listener);
    }

    public void onLoginFromAnonymous() {
        if (mLoginAnonymousListener != null && !mLoginAnonymousListener.isEmpty()) {
            for (LoginFromAnonymousListener listener : mLoginAnonymousListener) {
                listener.onLoginSuccess();
            }
        } else {
            Log.e(TAG, "no listener for mLoginAnonymousListener");
        }
    }

    public void onConfigBackgroundHeaderHomeChanged() {
        if (mConfigChangeList != null && !mConfigChangeList.isEmpty()) {
            for (OnConfigChangeListener listener : mConfigChangeList) {
                listener.onConfigBackgroundHeaderHomeChange();
            }
        } else {
            Log.i(TAG, "no listener for onConfigBackgroundHeaderHomeChange");
        }
    }

    // sync contact completed

    public void addSyncContactListner(SyncContactListner syncContactListner) {
        if (mSyncContactListners == null) {
            mSyncContactListners = new CopyOnWriteArrayList<>();
        }
        if (!mSyncContactListners.contains(syncContactListner)) {
            mSyncContactListners.add(syncContactListner);
        }
    }

    public void removeSyncContactListner(SyncContactListner syncContactListner){
        if(mSyncContactListners != null){
            mSyncContactListners.remove(syncContactListner);
        }
    }

    public void syncContactNotify() {
        if (mSyncContactListners == null) {
            return;
        }
        for (SyncContactListner syncContactListner : mSyncContactListners) {
            syncContactListner.onSyncCompleted();
        }
    }
}
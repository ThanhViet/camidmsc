package com.metfone.selfcare.helper.images;

import java.io.File;

public abstract class AlbumStorageDirFactory {
	public abstract File getAlbumStorageDir();
    public abstract File getAlbumVideoStorageDir();
}

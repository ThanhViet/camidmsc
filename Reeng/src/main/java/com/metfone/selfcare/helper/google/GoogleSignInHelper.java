package com.metfone.selfcare.helper.google;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Log;

public class GoogleSignInHelper {
    public static final int RC_SIGN_IN = 9001;
    private static final String TAG = GoogleSignInHelper.class.getSimpleName();
    private static GoogleSignInHelper googleSignInHelper;
    OnGoogleListener listener;
    private AppCompatActivity mActivity;
    private GoogleSignInClient mGoogleSignInClient;
    private boolean isLoggingOut = false;

    public GoogleSignInHelper(AppCompatActivity mActivity, OnGoogleListener listener) {
        this.mActivity = mActivity;
        this.listener = listener;

        initGoogleSignIn();
    }

    public static GoogleSignInHelper newInstance(AppCompatActivity mActivity, OnGoogleListener listener) {
        if (googleSignInHelper == null) {
            googleSignInHelper = new GoogleSignInHelper(mActivity, listener);
        }
        return googleSignInHelper;
    }

    private void initGoogleSignIn() {
        // [START config_sign_in]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mActivity.getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);
    }

    public void getProfile() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mActivity);
        if (account != null) {
//            Toast.makeText(mActivity, "Login success, Account is " + account.getDisplayName() + " avatar " + account.getPhotoUrl().toString(), Toast.LENGTH_SHORT).show();
            listener.onGetInfoGoogleFinish(account.getId(), account.getDisplayName(), account.getEmail(), account.getPhotoUrl());
        } else {
            if (mGoogleSignInClient != null) {
                Intent intent = mGoogleSignInClient.getSignInIntent();
//                mActivity.startActivityForResult(intent, RC_SIGN_IN);
                mActivity.startActivityForResult(intent, RC_SIGN_IN);
            }
        }
    }

    public void handleSignInIntent(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            Log.d(TAG, "User name is " + account.getDisplayName() + " email is " + account.getEmail() + " photoUrl " + account.getPhotoUrl() + "Token id is " + account.getIdToken());
            listener.onGetInfoGoogleFinish(account.getId(), account.getDisplayName(), account.getEmail(), account.getPhotoUrl());
        } catch (ApiException e) {
            Log.e(TAG, "Google error is " + e.getMessage());
            Log.d(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    public void getGoogleAccountDetails(GoogleSignInResult result) {
        // Google Sign In was successful, authenticate with FireBase
        GoogleSignInAccount account = result.getSignInAccount();
        // You are now logged into Google
    }

    public void signOut(OnGoogleSignOutListener listener) {
        mGoogleSignInClient.signOut().addOnCompleteListener(mActivity, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "Google Sign out success");
                mGoogleSignInClient = null;
                initGoogleSignIn();
                listener.onSignOutGoogleFinish();
            }
        });
    }

    public GoogleSignInClient getGoogleClient() {
        return mGoogleSignInClient;
    }

    public boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(mActivity) != null;
    }

    public interface OnGoogleListener {
        public void onGetInfoGoogleFinish(String id, String userName, String email, Uri avatarUri);
    }

    public interface OnGoogleSignOutListener {
        void onSignOutGoogleFinish();
    }
}
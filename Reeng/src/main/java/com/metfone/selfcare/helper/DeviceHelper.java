package com.metfone.selfcare.helper;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thaodv on 5/14/2015.
 */
public class DeviceHelper {
    private static final String TAG = DeviceHelper.class.getSimpleName();

    // private static final String DISTRIBUTION = "XNKOL";
    @SuppressLint({"MissingPermission", "HardwareIds"})
    public static String getDeviceId(Context ctx) {
        try {
            if (PermissionHelper.allowedPermission(ctx, Manifest.permission.READ_PHONE_STATE)) {
                TelephonyManager telephonyManager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
                return telephonyManager.getDeviceId();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return "";
    }

    @SuppressLint({"HardwareIds", "MissingPermission"})
    public static String getDeviceId(Context context, boolean hasCache) {
        String deviceId = "";
        try {
            if (hasCache) {
                deviceId = SharedPrefs.getInstance().get("PREF_DEVICE_ID", String.class);
            }
            if (TextUtils.isEmpty(deviceId) && PermissionHelper.allowedPermission(context, Manifest.permission.READ_PHONE_STATE)) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (telephonyManager != null) {
                    deviceId = telephonyManager.getDeviceId();
                    SharedPrefs.getInstance().put("PREF_DEVICE_ID", deviceId);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return deviceId;
    }

    public static void sendUtmSource(Context context, String utmSource) {
        ApplicationController application = (ApplicationController) context.getApplicationContext();
        sendDeviceId(application, getDeviceId(context), utmSource);
    }

    public static void checkToSendDeviceIdOnFirstTime(ApplicationController mApplication) {
        //if (Config.Server.BUILD_FOR_PLAY_STORE) return; //build cho play store thi ko gui id
        /*String distribution = mApplication.getConfigProperties().distribution;
        if (TextUtils.isEmpty(distribution)) {
            return;
        }
        long beginTime = System.currentTimeMillis();
        SharedPreferences mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
        boolean isSentDeviceId = mPref.getBoolean(Constants.PREFERENCE.PREF_SENT_DEVICE_ID, false);
        if (!isSentDeviceId) {
            //neu chua gui device id thi gui
            sendDeviceId(mApplication, getDeviceId(mApplication), distribution);
        }
        Log.i(TAG, "checkToSendDeviceIdOnFirstTime take " + (System.currentTimeMillis() - beginTime) + " ms");*/
    }

    public static void checkToSendDeviceIdAfterLogin(ApplicationController mApplication, String phoneNumber) {
        //if (Config.Server.BUILD_FOR_PLAY_STORE) return; //build cho play store thi ko gui id
        /*String distribution = mApplication.getConfigProperties().distribution;
        if (TextUtils.isEmpty(distribution)) {
            return;
        }
        long beginTime = System.currentTimeMillis();
        SharedPreferences mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                Context.MODE_PRIVATE);
        boolean isSentDeviceId = mPref.getBoolean(Constants.PREFERENCE.PREF_SENT_DEVICE_ID_AFTER_LOGIN, false);
        if (!isSentDeviceId) {
            //neu chua gui device id thi gui
            sendDeviceIdAndNumber(mApplication, getDeviceId(mApplication), phoneNumber, distribution);
        }
        Log.i(TAG, "checkToSendDeviceIdAfterLogin take " + (System.currentTimeMillis() - beginTime) + " ms");*/
    }

    private static void sendDeviceId(final ApplicationController mApp, final String deviceId, final String
            distributionChannel) {
        String url = Config.PREFIX.PROTOCOL_HTTP + ConfigLocalized.DOMAIN_LOG_DEVICE +
                UrlConfigHelper.getInstance(mApp).getUrlByKey(Config.UrlEnum.SET_DEVICE_ID);
        Log.i(TAG, "sendDeviceId = " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            int responseCode = -1;
//                            JSONObject jsonObject = new JSONObject(response);
                            /*if (jsonObject.has(Constants.HTTP.REST_ERROR_CODE)) {
                                responseCode = jsonObject.getInt(Constants.HTTP.REST_ERROR_CODE);
                            }*/
                            SharedPreferences mPref = mApp.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                                    Context.MODE_PRIVATE);
                            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SENT_DEVICE_ID, true).apply();
                            /*if (responseCode == HTTPCode.E200_OK && !TextUtils.isEmpty(mApp.getConfigProperties()
                            .distribution)) {
                                Log.i(TAG, "sendDeviceId success ");
                                Toast.makeText(mApp, R.string.register_device_success, Toast.LENGTH_LONG).show();
                            }*/
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("imei", deviceId);
                params.put("distributionChannel", distributionChannel);
                return params;
            }
        };
        VolleyHelper.getInstance(mApp).addRequestToQueue(request, TAG, false);
    }

    private static void sendDeviceIdAndNumber(final ApplicationController mApp, final String deviceId, final String
            msisdn, final String distributionChannel) {
        String url = Config.PREFIX.PROTOCOL_HTTP + ConfigLocalized.DOMAIN_LOG_DEVICE +
                UrlConfigHelper.getInstance(mApp).getUrlByKey(Config.UrlEnum.SET_DEVICE_ID);
        Log.i(TAG, "sendDeviceId = " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            int responseCode = -1;
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_ERROR_CODE)) {
                                responseCode = jsonObject.getInt(Constants.HTTP.REST_ERROR_CODE);
                            }
                            SharedPreferences mPref = mApp.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                                    Context.MODE_PRIVATE);
                            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_SENT_DEVICE_ID, true).apply();
                            if (responseCode == HTTPCode.E200_OK) {
                                Log.i(TAG, "sendDeviceId success ");
                            } else {
                                Log.i(TAG, "res = " + response);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "getResponseCodeFromJSON", e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("imei", deviceId);
                params.put("msisdn", msisdn);
                params.put("distributionChannel", distributionChannel);
                return params;
            }
        };
        VolleyHelper.getInstance(mApp).addRequestToQueue(request, TAG, false);
    }

    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
        /* return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE; */
    }

    public static String getGAID(Context context) {

        AdvertisingIdClient.Info adInfo = null;
        try {
            adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
            return adInfo.getId();
        } catch (IOException | GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, "Exception", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    public static boolean isEmulator() {
        return Build.BRAND.contains("generic")
                || Build.DEVICE.contains("generic")
                || Build.PRODUCT.contains("sdk")
                || Build.HARDWARE.contains("goldfish")
                || Build.MANUFACTURER.contains("Genymotion")
                || Build.PRODUCT.contains("vbox86p")
                || Build.DEVICE.contains("vbox86p")
                || Build.HARDWARE.contains("vbox86");
    }

    public static boolean checkDontKeepActivity(Context context) {
        int state;
        if (Version.hasJellyBeanMR1()) {
            state = Settings.System.getInt(context.getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);
        } else {
            state = Settings.System.getInt(context.getContentResolver(), Settings.System.ALWAYS_FINISH_ACTIVITIES, 0);
        }
        return state != 0;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static boolean isBackCameraAvailable(Context context) {
        if (Version.hasLollipop()) {
            String backCameraId = null;
            CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            if (manager != null)
                try {
                    for (String cameraId : manager.getCameraIdList()) {
                        CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(cameraId);
                        Integer facing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                        if (facing != null && facing == CameraMetadata.LENS_FACING_BACK) {
                            backCameraId = cameraId;
                            break;
                        }
                    }
                } catch (CameraAccessException e) {
                    Log.e(TAG, "Exception", e);
                }
            return !TextUtils.isEmpty(backCameraId);
        } else {
            int backCameraId = -1;
            for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                CameraInfo cameraInfo = new CameraInfo();
                Camera.getCameraInfo(i, cameraInfo);
                if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                    backCameraId = i;
                    break;
                }
            }
            return backCameraId != -1;
        }
    }

    public static void checkOptimizationMode(BaseSlidingFragmentActivity activity) {
        if (Version.hasM() && "samsung".equals(android.os.Build.MANUFACTURER.toLowerCase())) {
            try {
                Intent intent = new Intent();
                String packageName = activity.getPackageName();
                PowerManager pm = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
                if (!pm.isIgnoringBatteryOptimizations(packageName))
                    intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                activity.startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }
}
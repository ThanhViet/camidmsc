package com.metfone.selfcare.helper.home;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.model.setting.TabHomeDefault;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thanhnt72 on 6/29/2018.
 */

public class TabHomeHelper {

    private static final String TAG = TabHomeHelper.class.getSimpleName();

    /**
     * 01/11/2020
     * The tabs is displayed on BottomNavigationBar when app is installed: Home(position in list = 0), eSport(1), Cinema(2), Chat(3), Metfone+(4)
     * Customizable tabs show/hide on Settings: eSport, Cinema, Metfone+
     * Default tab is : Cinema tab
     * If the Cinema tab is hidden, Home is the default tab.
     *
     * The position of the default tab when user install app: 2 (Cinema)
     */
    // Tab default is Home tab
    public static int TAB_DEFAULT = 0;
    public static int TAB_CURRENT = 0;

    private static TabHomeHelper mInstant;
    private ApplicationController mApp;
    private CopyOnWriteArrayList<ConfigTabHomeItem> listAllItem = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigTabHomeItem> listAvailableItem = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigTabHomeItem> listActiveItem = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigTabHomeItem> listNotActiveItem = new CopyOnWriteArrayList<>();

    // TODO: [START] Cambodia version
    private CopyOnWriteArrayList<ConfigTabHomeItem> khListAllItem = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigTabHomeItem> khListActiveItem = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigTabHomeItem> khListAvailableItem = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ConfigTabHomeItem> khListNotActiveItem = new CopyOnWriteArrayList<>();
    // TODO: [END] Cambodia version

    public TabHomeHelper(ApplicationController mApplication) {
        mApp = mApplication;
        initData();
    }

    public static synchronized TabHomeHelper getInstant(ApplicationController mApp) {
        if (mInstant == null) {
            mInstant = new TabHomeHelper(mApp);
        }
        return mInstant;
    }

    public static String getNameTabHome(HomeTab tab, Context context) {
        Resources res = context.getResources();
        String title = res.getString(R.string.app_name);
        switch (tab) {
            case tab_hot:
                title = res.getString(R.string.tab_onmedia);
                break;

            case tab_news:
                title = res.getString(R.string.tab_news);
                break;

            case tab_video:
                title = res.getString(R.string.tab_video);
                break;
            case tab_movie:
                if (ApplicationController.self().getReengAccountBusiness().isCambodia())
                    title = res.getString(R.string.tab_5dmax);
                else
                    title = res.getString(R.string.tab_movie);
                break;

            case tab_stranger:
                title = res.getString(R.string.tab_stranger);
                break;

            case tab_music:
                title = res.getString(R.string.tab_music_keeng);
                break;

            case tab_security:
                title = res.getString(R.string.tab_security);
                break;
            case tab_tiins:
                title = res.getString(R.string.tab_tiin);
                break;
            case tab_selfcare:
                title = res.getString(R.string.sc_tab_name);
                break;

            // TODO CamID: home_tab_config
            case tab_home:
                title = res.getString(R.string.tab_m_home);
                break;
            case tab_game:
                title = res.getString(R.string.tab_m_game);
                break;
            case tab_chat:
                title = res.getString(R.string.tab_chat);
                break;
            // [start] CamID
            case tab_cinema:
                title = res.getString(R.string.tab_m_cinema);
                break;
            case tab_esport:
                title = res.getString(R.string.tab_m_esport);
                break;
            case tab_metfone:
                title = res.getString(R.string.tab_m_metfone);
                break;
            case tab_contact:
                title = res.getString(R.string.tab_m_contact);
                break;
            // [end] CamID
        }
        return title;
    }

    public static String getDescTabHome(HomeTab tab, Context context) {
        Resources res = context.getResources();
        String title = res.getString(R.string.app_name);
        switch (tab) {
            case tab_hot:
                title = res.getString(R.string.tab_social_des);
                break;

            case tab_news:
                title = res.getString(R.string.tab_news_des);
                break;
            case tab_tiins:
                title = res.getString(R.string.tab_tiin_des);
                break;

            case tab_video:
                title = res.getString(R.string.tab_video_des);
                break;
            case tab_movie:
                title = res.getString(R.string.tab_movie_des);
                break;

            case tab_stranger:
                title = res.getString(R.string.tab_stranger_des);
                break;

            case tab_music:
                title = res.getString(R.string.tab_music_des);
                break;

            case tab_security:
                title = res.getString(R.string.tab_security_des);
                break;

            case tab_selfcare:
                title = res.getString(R.string.sc_selfcare_des);
                break;

            // TODO CamID: home_tab_config
            // [start] CamID
            case tab_home:
                title = ResourceUtils.getString(R.string.kh_tab_home_des);
                break;
            case tab_game:
                title = ResourceUtils.getString(R.string.kh_tab_game_des);
                break;
            case tab_chat:
                title = ResourceUtils.getString(R.string.kh_tab_chat_des);
                break;
            case tab_cinema:
                title = ResourceUtils.getString(R.string.tab_m_cinema_des);
                break;
            case tab_esport:
                title = ResourceUtils.getString(R.string.kh_tab_esport_des);
                break;
            case tab_metfone:
                title = ResourceUtils.getString(R.string.kh_tab_metfone_des);
                break;
            case tab_contact:
                title = ResourceUtils.getString(R.string.tab_m_contact_des);
                break;
            // [end] CamID
        }
        return title;
    }

    public static int getResIdTabHome(HomeTab tab) {
        int resId = R.mipmap.ic_launcher;
        switch (tab) {
            case tab_hot:
                resId = R.drawable.ic_func_interactive;
                break;

            case tab_news:
                resId = R.drawable.ic_func_news;
                break;
            case tab_tiins:
                resId = R.drawable.ic_func_tiin;
                break;
            case tab_video:
                resId = R.drawable.ic_func_video;
                break;
            case tab_movie:
                if (ApplicationController.self().getReengAccountBusiness().isCambodia())
                    resId = R.drawable.ic_more_5dmax;
                else
                    resId = R.drawable.ic_func_movie;
                break;

            case tab_stranger:
                resId = R.drawable.ic_func_make_friend;
                break;

            case tab_music:
                resId = R.drawable.ic_func_music;
                break;

            case tab_security:
                resId = R.drawable.ic_more_security;
                break;

            case tab_selfcare:
                resId = R.drawable.ic_more_selfcare;
                break;

        }
        return resId;
    }

    public static ItemMoreHome getFunctionTabHome(Context context, ConfigTabHomeItem item) {
        ItemMoreHome itemMoreHome = null;
        switch (item.getHomeTab()) {
            case tab_hot:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_HOT
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_SOCIAL.VALUE);
                break;
            case tab_news:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_NEWS
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_NEWS.VALUE);
                break;
            case tab_video:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_VIDEO
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_VIDEO.VALUE);
                break;
            case tab_movie:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_MOVIE
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_MOVIE.VALUE);
                break;
            case tab_stranger:

                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_STRANGER
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_STRANGER.VALUE);
                break;
            case tab_music:

                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_MUSIC
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_MUSIC.VALUE);
                break;
            case tab_security:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_SECURITY
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_SECURITY.VALUE);
                break;
            case tab_tiins:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_TIIN
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_TIIN.VALUE);
                break;
            case tab_my_viettel:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_MY_VIETTEL
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_MY_VIETTEL.VALUE);
                break;
            case tab_selfcare:
                itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_SELFCARE
                        , TabHomeHelper.getNameTabHome(item.getHomeTab(), context)
                        , TabHomeHelper.getResIdTabHome(item.getHomeTab()), null);
                itemMoreHome.setFuncName(LogApi.FuncName.FUNC_SELFCARE.VALUE);
                break;
        }
        return itemMoreHome;
    }

    public static boolean isHomeTabEnable(HomeTab tab, ApplicationController mApplication) {
        switch (tab) {
            // TODO: [START] Cambodia version
            case tab_home:
            case tab_cinema:
            case tab_esport:
            case tab_metfone:
            case tab_chat:
            case tab_game:
                return true;
            // TODO: [END] Cambodia version
            case tab_call:
            case tab_wap:
//            case tab_chat:
            case tab_more:
                return true;

            case tab_hot:
                return mApplication.getConfigBusiness().isEnableOnMedia();

            case tab_news:
                return mApplication.getConfigBusiness().isEnableTabNews();

            case tab_video:
                return mApplication.getConfigBusiness().isEnableTabVideo();

            case tab_movie:
                return mApplication.getConfigBusiness().isEnableTabMovie();

            case tab_stranger:
                return mApplication.getConfigBusiness().isEnableTabStranger();

            case tab_music:
                return mApplication.getConfigBusiness().isEnableTabMusic();

            case tab_security:
                return mApplication.getConfigBusiness().isEnableTabSecurity() && mApplication.getReengAccountBusiness().isViettel();

            case tab_selfcare:
                return mApplication.getConfigBusiness().isEnableTabSelfCare();

            case tab_tiins:
                return mApplication.getConfigBusiness().isEnableTabTiin();

            case tab_my_viettel:
                return mApplication.getConfigBusiness().isEnableTabMyViettel();

            // TODO CamID: home_tab_config
            // [start] CamID
//            case tab_cinema:
//                return mApplication.getConfigBusiness().isEnableTabCinema();
//            case tab_esport:
//                return mApplication.getConfigBusiness().isEnableTabESport();
//            case tab_metfone:
//                return mApplication.getConfigBusiness().isEnableTabMetfone();
//            case tab_contact:
//                return mApplication.getConfigBusiness().isEnableTabContact();
            // [end] CamID
        }
        return false;
    }

    /**
     * // TODO: Cambodia version
     */
    public void initDataKhVersion() {
        khListAllItem.clear();
        khListActiveItem.clear();
        khListAvailableItem.clear();
        khListNotActiveItem.clear();
        String dataSaveConfigKh = mApp.getPref().getString(Constants.PREFERENCE.PREF_SAVE_LIST_CONFIG_TAB_HOME_KH, "");
        if (TextUtils.isEmpty(dataSaveConfigKh)) {
            khListActiveItem = new CopyOnWriteArrayList<>();
            khListAvailableItem = new CopyOnWriteArrayList<>();
            khListAllItem = new CopyOnWriteArrayList<>();

            ConfigTabHomeItem tabHome = new ConfigTabHomeItem(HomeTab.tab_home, ConfigTabHomeItem.STATE_ACTIVE);
            setActiveConfigTabHomeItem(tabHome);

            ConfigTabHomeItem tabGame = new ConfigTabHomeItem(HomeTab.tab_game, ConfigTabHomeItem.STATE_ACTIVE);
            setActiveConfigTabHomeItem(tabGame);

            ConfigTabHomeItem tabEsport = new ConfigTabHomeItem(HomeTab.tab_esport, ConfigTabHomeItem.STATE_ACTIVE);
            setActiveConfigTabHomeItem(tabEsport);

            ConfigTabHomeItem tabCinema = new ConfigTabHomeItem(HomeTab.tab_cinema, ConfigTabHomeItem.STATE_ACTIVE);
            setActiveConfigTabHomeItem(tabCinema);

            ConfigTabHomeItem tabChat = new ConfigTabHomeItem(HomeTab.tab_chat, ConfigTabHomeItem.STATE_ACTIVE);
            setActiveConfigTabHomeItem(tabChat);

            ConfigTabHomeItem tabMetfone = new ConfigTabHomeItem(HomeTab.tab_metfone, ConfigTabHomeItem.STATE_ACTIVE);
            setActiveConfigTabHomeItem(tabMetfone);

            // Tab Home never hide
            khListAllItem.add(tabHome);
            khListAvailableItem.add(tabHome);
            khListActiveItem.add(tabHome);

            khListAllItem.add(tabGame);
            khListAvailableItem.add(tabGame);
            khListActiveItem.add(tabGame);

//            if (mApp.getConfigBusiness().isEnableTabCinema()) {
//                listAvailableItem.add(tabCinema);
//            } else {
//                tabCinema.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
//            }
            khListAllItem.add(tabCinema);
            khListAvailableItem.add(tabCinema);
            khListActiveItem.add(tabCinema);

//            if (mApp.getConfigBusiness().isEnableTabMetfone()) {
//                listAvailableItem.add(tabMetfone);
//            } else {
//                tabMetfone.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
//            }
            khListAllItem.add(tabMetfone);
            khListAvailableItem.add(tabMetfone);
            khListActiveItem.add(tabMetfone);

            // Tab Chat Never hide
            khListAllItem.add(tabChat);
            khListAvailableItem.add(tabChat);
            khListActiveItem.add(tabChat);

//            if (mApp.getConfigBusiness().isEnableTabESport()) {
//                listAvailableItem.add(tabEsport);
//            } else {
//                tabEsport.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
//            }
            khListAllItem.add(tabEsport);
            khListAvailableItem.add(tabEsport);
            khListActiveItem.add(tabEsport);

            if (TAB_CURRENT != mApp.getPref().getInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, TAB_DEFAULT)) {
                return;
            }
            TAB_CURRENT = ApplicationController.self().getPref().getInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, TAB_DEFAULT);
        } else {
            try {
                JSONArray jsonArray = new JSONArray(dataSaveConfigKh);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject js = jsonArray.getJSONObject(i);
                    if (js != null) {
                        TabHomeHelper.HomeTab tabItem = TabHomeHelper.HomeTab.fromString(js.optString("tab"));
                        int state = js.optInt("state");
                        ConfigTabHomeItem item = new ConfigTabHomeItem(tabItem, state);
                        if (TabHomeHelper.isHomeTabEnable(tabItem, mApp)) {
                            if (state == ConfigTabHomeItem.STATE_ACTIVE) {
                                khListActiveItem.add(item);
                            } else {
                                item.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
                                khListNotActiveItem.add(item);
                            }
                            khListAvailableItem.add(item);
                        } else {
                            item.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
                        }
                        khListAllItem.add(item);

                        /*if (tabItem != TabHomeHelper.HomeTab.tab_chat
                                && tabItem != TabHomeHelper.HomeTab.tab_more) {
                            int state = js.optInt("state");
                            String idTabWap = js.optString("idwap");
                            ConfigTabHomeItem item = new ConfigTabHomeItem(tabItem, state);
                            if (tabItem == HomeTab.tab_wap) {
                                item = mApp.getConfigBusiness().getConfigTabHomeWap(idTabWap);
                                if (item == null) continue;
                                item.setState(state);
                                Log.i(TAG, "tab wap: " + item.toString());
                            }
                            if (TabHomeHelper.isHomeTabEnable(tabItem, mApp)) {
                                if (state == ConfigTabHomeItem.STATE_ACTIVE) {
                                    khListActiveItem.add(item);
                                } else {
                                    item.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
                                    khListNotActiveItem.add(item);
                                }
                                khListAvailableItem.add(item);
                            } else {
                                item.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
                            }
                            khListAllItem.add(item);
                        }*/
                    }
                }
//                checkItemConfigNew();

//                LogDebugHelper.getInstance().logDebugContent("init item Tab Home: " + listActiveItem.size() + " | " + listNotActiveItem.size() + " | " + listAvailableItem.size());
//                Log.i(TAG, "init item Tab Home: " + listActiveItem.size() + " | " + listNotActiveItem.size() + " | " + listAvailableItem.size());
            } catch (Exception e) {
                Log.i(TAG, "init item Tab Home exception:", e);
                LogDebugHelper.getInstance().logDebugContent("init item Tab Home exception: " + e.toString());
            }

            setTabHomeDefault(khListActiveItem, false);
        }
    }

    public void initData() {
        // TODO: [START] Cambodia version
        initDataKhVersion();
        // TODO: [END] Cambodia version
        listAllItem.clear();
        listAvailableItem.clear();
        listActiveItem.clear();
        listNotActiveItem.clear();
        String dataSaveConfig = mApp.getPref().getString(Constants.PREFERENCE.PREF_SAVE_LIST_CONFIG_TAB_HOME, "");
//        LogDebugHelper.getInstance().logDebugContent("dataSaveConfig: " + dataSaveConfig);
//        Log.i(TAG, "dataSaveConfig: " + dataSaveConfig);
        if (TextUtils.isEmpty(dataSaveConfig)) {
            listAllItem = new CopyOnWriteArrayList<>();
            listAvailableItem = new CopyOnWriteArrayList<>();
            listActiveItem = new CopyOnWriteArrayList<>();
            listNotActiveItem = new CopyOnWriteArrayList<>();
            initConfigTabHomeDefault();

//            LogDebugHelper.getInstance().logDebugContent("init item default Tab Home: " + listActiveItem.size() + " | " + listNotActiveItem.size() + " | " + listAvailableItem.size());
//            Log.i(TAG, "init item default Tab Home: " + listActiveItem.size() + " | " + listNotActiveItem.size() + " | " + listAvailableItem.size());
        } else {
            try {
                JSONArray jsonArray = new JSONArray(dataSaveConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject js = jsonArray.getJSONObject(i);
                    if (js != null) {
                        TabHomeHelper.HomeTab tabItem = TabHomeHelper.HomeTab.fromString(js.optString("tab"));
                        if (tabItem != TabHomeHelper.HomeTab.tab_chat
                                && tabItem != TabHomeHelper.HomeTab.tab_more) {
                            int state = js.optInt("state");
                            String idTabWap = js.optString("idwap");
                            ConfigTabHomeItem item = new ConfigTabHomeItem(tabItem, state);
                            if (tabItem == HomeTab.tab_wap) {
                                item = mApp.getConfigBusiness().getConfigTabHomeWap(idTabWap);
                                if (item == null) continue;
                                item.setState(state);
                                Log.i(TAG, "tab wap: " + item.toString());
                            }
                            if (TabHomeHelper.isHomeTabEnable(tabItem, mApp)) {
                                if (state == ConfigTabHomeItem.STATE_ACTIVE) {
                                    listActiveItem.add(item);
                                } else {
                                    item.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
                                    listNotActiveItem.add(item);
                                }
                                listAvailableItem.add(item);
                            } else {
                                item.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
                            }
                            listAllItem.add(item);
                        }
                    }
                }
                checkItemConfigNew();

//                LogDebugHelper.getInstance().logDebugContent("init item Tab Home: " + listActiveItem.size() + " | " + listNotActiveItem.size() + " | " + listAvailableItem.size());
//                Log.i(TAG, "init item Tab Home: " + listActiveItem.size() + " | " + listNotActiveItem.size() + " | " + listAvailableItem.size());
            } catch (Exception e) {
                Log.i(TAG, "init item Tab Home exception:", e);
                LogDebugHelper.getInstance().logDebugContent("init item Tab Home exception: " + e.toString());
            }
        }

    }

    private void checkItemConfigNew() throws Exception {
        ConfigTabHomeItem tabVideo = new ConfigTabHomeItem(HomeTab.tab_video, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabStranger = new ConfigTabHomeItem(HomeTab.tab_stranger, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabHot = new ConfigTabHomeItem(HomeTab.tab_hot, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabMusic = new ConfigTabHomeItem(HomeTab.tab_music, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabMovie = new ConfigTabHomeItem(HomeTab.tab_movie, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabNews = new ConfigTabHomeItem(HomeTab.tab_news, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabTiin = new ConfigTabHomeItem(HomeTab.tab_tiins, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabSecurity = new ConfigTabHomeItem(HomeTab.tab_security, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabSelfCare = new ConfigTabHomeItem(HomeTab.tab_selfcare, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        // TODO CamID: home_tab_config
        // [start] CamID
        ConfigTabHomeItem tabGame = new ConfigTabHomeItem(HomeTab.tab_game, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabCinema = new ConfigTabHomeItem(HomeTab.tab_cinema, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabEsport = new ConfigTabHomeItem(HomeTab.tab_esport, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabMetfone = new ConfigTabHomeItem(HomeTab.tab_metfone, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        ConfigTabHomeItem tabContact = new ConfigTabHomeItem(HomeTab.tab_contact, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        // [end] CamID
        ArrayList<ConfigTabHomeItem> listAllTmp = new ArrayList<>();
        listAllTmp.add(tabVideo);
        listAllTmp.add(tabStranger);
        listAllTmp.add(tabHot);
        listAllTmp.add(tabMusic);
        listAllTmp.add(tabMovie);
        listAllTmp.add(tabNews);
        listAllTmp.add(tabSecurity);
        listAllTmp.add(tabTiin);
        listAllTmp.add(tabSelfCare);
        // TODO CamID: home_tab_config
        // [start] CamID
        listAllTmp.add(tabGame);
        listAllTmp.add(tabCinema);
        listAllTmp.add(tabEsport);
        listAllTmp.add(tabMetfone);
        listAllTmp.add(tabContact);
        // [end] CamID

        ArrayList<ConfigTabHomeItem> listIndexRemove = new ArrayList<>();

        for (ConfigTabHomeItem item : listAllItem) {
            for (ConfigTabHomeItem itemTmp : listAllTmp) {
                if (item.getHomeTab() == itemTmp.getHomeTab()) {
                    Log.i(TAG, "item.getHomeTab(): " + item.getHomeTab());
                    listIndexRemove.add(itemTmp);
                    break;
                }
            }
        }

        if (!listIndexRemove.isEmpty())
            listAllTmp.removeAll(listIndexRemove);

        for (ConfigTabHomeItem itemTmp : listAllTmp) {
            if (isHomeTabEnable(itemTmp.getHomeTab(), mApp)) {
                listNotActiveItem.add(itemTmp);
                listAvailableItem.add(itemTmp);
                listAllItem.add(itemTmp);
            }
        }

        ArrayList<ConfigTabHomeItem> listAllWapConfig = mApp.getConfigBusiness().getListTabWap();
        for (ConfigTabHomeItem itemWap : listAllWapConfig) {
            boolean oldItem = false;
            for (ConfigTabHomeItem item : listAllItem) {
                if (item.getHomeTab() == HomeTab.tab_wap && item.getId().equals(itemWap.getId()))
                    oldItem = true;
            }
            if (!oldItem) {
                listNotActiveItem.add(itemWap);
                listAvailableItem.add(itemWap);
                listAllItem.add(itemWap);
                Log.i(TAG, "add item: " + itemWap.toString());
            }
        }
    }

    public CopyOnWriteArrayList<ConfigTabHomeItem> getListAllItem() {
        return listAllItem;
    }

    // TODO: [START] Cambodia version
    public CopyOnWriteArrayList<ConfigTabHomeItem> getKhListAllItem() {
        return khListAllItem;
    }

    public void setKhListAllItem(CopyOnWriteArrayList<ConfigTabHomeItem> items) {
        khListAllItem = items;
        khListAvailableItem.clear();
        khListActiveItem.clear();
        khListNotActiveItem.clear();
        for (ConfigTabHomeItem item : khListAllItem) {
            if (item.getState() != ConfigTabHomeItem.STATE_UNAVAILABLE) {
                khListAvailableItem.add(item);
                if (item.getState() == ConfigTabHomeItem.STATE_ACTIVE)
                    khListActiveItem.add(item);
                else if (item.getState() == ConfigTabHomeItem.STATE_NOT_ACTIVE)
                    khListNotActiveItem.add(item);
            }
        }
        setTabHomeDefault(khListActiveItem, true);
        String dataSave = listTabHomeToString(khListAllItem);
        Log.i(TAG, "[Cambodia version] Tabs dataSave: " + dataSave);
        mApp.getPref().edit().putString(Constants.PREFERENCE.PREF_SAVE_LIST_CONFIG_TAB_HOME_KH, dataSave).apply();
        logConfigTab();
    }

    private void setTabHomeDefault(CopyOnWriteArrayList<ConfigTabHomeItem> listItem, boolean isCustomizeTab) {
        if (khListActiveItem == null || khListActiveItem.size() == 0) {
            mApp.getPref().edit().putInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, TAB_DEFAULT).apply();
            TAB_CURRENT = TAB_DEFAULT;
            return;
        }

        boolean isContainCinema = false;
        // check is list active contain Cinema tab, set Cinema is default tab
        for (int i = 0; i < listItem.size(); i++) {
            HomeTab homeTab = listItem.get(i).getHomeTab();
            if (homeTab == HomeTab.tab_cinema) {
                isContainCinema = true;
                mApp.getPref().edit().putInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, i).apply();
                TAB_CURRENT = i;
            }

            if (homeTab == HomeTab.tab_home) {
                TAB_DEFAULT = i;
            }
        }

        // If list not contains tab Cinema, set default tab is Home
        if (!isContainCinema) {
            mApp.getPref().edit().putInt(Constants.PREFERENCE.PREF_CONFIG_TAB_DEFAULT, TAB_DEFAULT).apply();
            TAB_CURRENT = TAB_DEFAULT;
        }

        // if method call from initData(), not set TAB_CURRENT and TAB_DEFAULT is Home
        if (isCustomizeTab) {
            TAB_CURRENT = TAB_DEFAULT;
        }
    }
    // TODO: [END] Cambodia version

    public void setListAllItem(CopyOnWriteArrayList<ConfigTabHomeItem> listItem) {
        listAllItem = listItem;
        listAvailableItem.clear();
        listActiveItem.clear();
        listNotActiveItem.clear();
        for (ConfigTabHomeItem item : listAllItem) {
            if (item.getState() != ConfigTabHomeItem.STATE_UNAVAILABLE) {
                listAvailableItem.add(item);
                if (item.getState() == ConfigTabHomeItem.STATE_ACTIVE)
                    listActiveItem.add(item);
                else if (item.getState() == ConfigTabHomeItem.STATE_NOT_ACTIVE)
                    listNotActiveItem.add(item);
            }
        }
        String dataSave = listTabHomeToString(listAllItem);
        Log.i(TAG, "dataSave: " + dataSave);
        mApp.getPref().edit().putString(Constants.PREFERENCE.PREF_SAVE_LIST_CONFIG_TAB_HOME, dataSave).apply();
        logConfigTab();
    }

    // TODO: [START] Cambodia version
    public CopyOnWriteArrayList<ConfigTabHomeItem> getKhListActiveItem() {
        return khListActiveItem;
    }
    // TODO: [END] Cambodia version

    public CopyOnWriteArrayList<ConfigTabHomeItem> getListActiveItem() {
        return listActiveItem;
    }

    public CopyOnWriteArrayList<ConfigTabHomeItem> getListNotActiveItem() {
        return listNotActiveItem;
    }

    public String listTabHomeToString(CopyOnWriteArrayList<ConfigTabHomeItem> listAllItem) {
        JSONArray jsonArray = new JSONArray();
        ArrayList<ConfigTabHomeItem> listUnAvailable = new ArrayList<>();
        for (int i = 0; i < listAllItem.size(); i++) {
            ConfigTabHomeItem item = listAllItem.get(i);
            if (item.getState() != ConfigTabHomeItem.STATE_UNAVAILABLE) {
                try {
                    jsonArray.put(convertConfigTabHome(item));
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return "";
                }
            } else
                listUnAvailable.add(item);
        }
        for (int i = 0; i < listUnAvailable.size(); i++) {
            ConfigTabHomeItem item = listUnAvailable.get(i);
            try {
                jsonArray.put(convertConfigTabHome(item));
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        return jsonArray.toString();
    }

    private JSONObject convertConfigTabHome(ConfigTabHomeItem item) throws JSONException {
        JSONObject js = new JSONObject();
        js.put("tab", item.getHomeTab().name());
        js.put("state", item.getState());
        js.put("idwap", item.getId());
        return js;
    }

    public ConfigTabHomeItem getTabWapItem(String id) {
        for (int i = 0; i < listActiveItem.size(); i++) {
            ConfigTabHomeItem item = listActiveItem.get(i);
            if (item.getHomeTab() == HomeTab.tab_wap && id.equals(item.getId())) {
                return item;
            }
        }
        for (int i = 0; i < listNotActiveItem.size(); i++) {
            ConfigTabHomeItem item = listNotActiveItem.get(i);
            if (item.getHomeTab() == HomeTab.tab_wap && id.equals(item.getId())) {
                return item;
            }
        }
        return null;
    }

    public int findPositionFromDeepLink(HomeTab homeTab, String idTabWap) {
        if (homeTab == HomeTab.tab_chat) return 1;
        if (homeTab == HomeTab.tab_more)// bỏ tab more rồi
            return 0;//listActiveItem.size() + 1;  //do tabchat =0 nen tab more phai = size +1
        if (homeTab == HomeTab.tab_call) return 1;
        for (int i = 0; i < listActiveItem.size(); i++) {
            ConfigTabHomeItem item = listActiveItem.get(i);
            if (item.getHomeTab() == homeTab) {
                if (homeTab == HomeTab.tab_wap) {
                    if (!TextUtils.isEmpty(idTabWap) && idTabWap.equals(item.getId()))
                        return i + 2;
                } else
                    return i + 2;
            }
        }
        return 0;
    }

    public HomeTab findTabFromPosition(int pos) {
        if (pos <= 0) return HomeTab.tab_home;
        if (pos == 1) return HomeTab.tab_game;
        if (pos <= (listActiveItem.size() + 1)) {
            return listActiveItem.get(pos - 2).getHomeTab();
        }
        return HomeTab.tab_home;
    }

    public HomeTab findTabFromPositionKHListActiveItem(int pos) {
        return khListActiveItem.get(pos).getHomeTab();
    }

    public int findHomeTab() {
        int position = TAB_DEFAULT;
        if (khListActiveItem != null && khListActiveItem.size() != 0) {
            for (int i = 0; i < khListActiveItem.size(); i++) {
                HomeTab homeTab = khListActiveItem.get(i).getHomeTab();
                if (homeTab == HomeTab.tab_home) {
                    position = i;
                    break;
                }
            }
        }
        return position;
    }

    private void initConfigTabHomeDefault() {
        // CamID: home, esport, cinema, chat, metfone, contact
        //vn: video, news, movie, tiin
        // TODO CamID: home_tab_config
        // [start] CamID
        ConfigTabHomeItem tabGame = new ConfigTabHomeItem(HomeTab.tab_game, ConfigTabHomeItem.STATE_ACTIVE);
        setActiveConfigTabHomeItem(tabGame);

        ConfigTabHomeItem tabCinema = new ConfigTabHomeItem(HomeTab.tab_cinema, ConfigTabHomeItem.STATE_ACTIVE);
        setActiveConfigTabHomeItem(tabCinema);

        ConfigTabHomeItem tabEsport = new ConfigTabHomeItem(HomeTab.tab_esport, ConfigTabHomeItem.STATE_ACTIVE);
        setActiveConfigTabHomeItem(tabEsport);

        ConfigTabHomeItem tabMetfone = new ConfigTabHomeItem(HomeTab.tab_metfone, ConfigTabHomeItem.STATE_ACTIVE);
        setActiveConfigTabHomeItem(tabMetfone);

        ConfigTabHomeItem tabContact = new ConfigTabHomeItem(HomeTab.tab_contact, ConfigTabHomeItem.STATE_ACTIVE);
        setActiveConfigTabHomeItem(tabContact);
        // [end] CamID

        ConfigTabHomeItem tabVideo = new ConfigTabHomeItem(HomeTab.tab_video, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabVideo);
        ConfigTabHomeItem tabStranger = new ConfigTabHomeItem(HomeTab.tab_stranger, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabStranger);
        ConfigTabHomeItem tabHot = new ConfigTabHomeItem(HomeTab.tab_hot, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabHot);
        ConfigTabHomeItem tabMusic = new ConfigTabHomeItem(HomeTab.tab_music, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        if (mApp.getReengAccountBusiness().isCambodia())
            tabMusic.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabMusic);
        ConfigTabHomeItem tabMovie = new ConfigTabHomeItem(HomeTab.tab_movie, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        if (mApp.getReengAccountBusiness().isCambodia())
            tabMovie.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabMovie);
        ConfigTabHomeItem tabNews = new ConfigTabHomeItem(HomeTab.tab_news, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        if (mApp.getReengAccountBusiness().isCambodia())
            tabNews.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabNews);
        ConfigTabHomeItem tabSecurity = new ConfigTabHomeItem(HomeTab.tab_security, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabSecurity);
        //todo tiin config san tab tiin
        ConfigTabHomeItem tabTiin = new ConfigTabHomeItem(HomeTab.tab_tiins, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabTiin);

        ConfigTabHomeItem tabSelfCare = new ConfigTabHomeItem(HomeTab.tab_selfcare, ConfigTabHomeItem.STATE_NOT_ACTIVE);
        setActiveConfigTabHomeItem(tabSelfCare);

        if (mApp.getConfigBusiness().isEnableTabVideo())
            listAvailableItem.add(tabVideo);
        else
            tabVideo.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabVideo);

        if (mApp.getConfigBusiness().isEnableTabNews())
            listAvailableItem.add(tabNews);
        else
            tabNews.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabNews);

        if (mApp.getConfigBusiness().isEnableTabMovie())
            listAvailableItem.add(tabMovie);
        else
            tabMovie.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabMovie);

        if (mApp.getConfigBusiness().isEnableTabStranger())
            listAvailableItem.add(tabStranger);
        else
            tabStranger.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabStranger);

        if (mApp.getConfigBusiness().isEnableOnMedia())
            listAvailableItem.add(tabHot);
        else
            tabHot.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabHot);

        if (mApp.getConfigBusiness().isEnableTabMusic())
            listAvailableItem.add(tabMusic);
        else
            tabMusic.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabMusic);

        if (mApp.getConfigBusiness().isEnableTabSecurity() && ApplicationController.self().getReengAccountBusiness().isViettel())
            listAvailableItem.add(tabSecurity);
        else
            tabSecurity.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabSecurity);

        if (mApp.getConfigBusiness().isEnableTabTiin())
            listAvailableItem.add(tabTiin);
        else
            tabTiin.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabTiin);

        if (mApp.getConfigBusiness().isEnableTabSelfCare())
            listAvailableItem.add(tabSelfCare);
        else
            tabSelfCare.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabSelfCare);

        // TODO CamID: home_tab_config
        // [start] CamID
        if (mApp.getConfigBusiness().isEnableTabGame())
            listAvailableItem.add(tabGame);
        else
            tabGame.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabGame);

        if (mApp.getConfigBusiness().isEnableTabCinema())
            listAvailableItem.add(tabCinema);
        else
            tabCinema.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabCinema);

        if (mApp.getConfigBusiness().isEnableTabESport())
            listAvailableItem.add(tabEsport);
        else
            tabEsport.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabEsport);

        if (mApp.getConfigBusiness().isEnableTabMetfone())
            listAvailableItem.add(tabMetfone);
        else
            tabMetfone.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabMetfone);

        if (mApp.getConfigBusiness().isEnableTabContact())
            listAvailableItem.add(tabContact);
        else
            tabContact.setState(ConfigTabHomeItem.STATE_UNAVAILABLE);
        listAllItem.add(tabContact);
        // [end] CamID

        for (ConfigTabHomeItem item : listAvailableItem) {
            if (item.getState() == ConfigTabHomeItem.STATE_ACTIVE) {
                listActiveItem.add(item);
            } else if (item.getState() == ConfigTabHomeItem.STATE_NOT_ACTIVE)
                listNotActiveItem.add(item);
        }
        sortListActiveItem();

        // TODO CamID: home_tab_config
        // [start] CamID: Tạm thời sẽ không sử dụng
        // processListWap();
        // [end] CamID
    }

    private void processListWap() {
        ArrayList<ConfigTabHomeItem> listWap = mApp.getConfigBusiness().getListTabWap();
        if (listWap != null && !listWap.isEmpty()) {
            Log.i(TAG, "add all listwap: " + listWap.size());
            ArrayList<ConfigTabHomeItem> copy = new ArrayList<>();
            for (int i = 0; i < listWap.size(); i++) {
                boolean hasActive = false;
                ConfigTabHomeItem itemWap = listWap.get(i);
                for (int j = 0; j < listActiveItem.size(); j++) {
                    if (listActiveItem.get(j).getHomeTab() == HomeTab.tab_wap
                            && !TextUtils.isEmpty(itemWap.getId())
                            && itemWap.getId().equals(listActiveItem.get(j).getId())) {
                        hasActive = true;
                    }
                }
                if (!hasActive) {
                    itemWap.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
                    Log.i(TAG, "wap not active: " + itemWap.toString());
                    copy.add(itemWap);
                }
            }
            if (!copy.isEmpty()) {
                listAvailableItem.addAll(copy);
                listNotActiveItem.addAll(copy);
                listAllItem.addAll(copy);
            }
        }
    }

    private void sortListActiveItem() {
        ArrayList<TabHomeDefault> list = mApp.getConfigBusiness().getOrderTabHomeConfig();
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                int indexTab = list.get(i).getTabIndex();
                if (indexTab == 8 && !TextUtils.isEmpty(list.get(i).getIdTabWap())) {
                    ArrayList<ConfigTabHomeItem> listWap = mApp.getConfigBusiness().getListTabWap();
                    for (ConfigTabHomeItem item : listWap) {
                        if (list.get(i).getIdTabWap().equals(item.getId())) {
                            item.setState(ConfigTabHomeItem.STATE_ACTIVE);
                            listActiveItem.remove(item);
                            if (i > listActiveItem.size())
                                listActiveItem.add(item);
                            else
                                listActiveItem.add(i, item);
                            listAvailableItem.add(item);
                            listAllItem.add(item);
                        }
                    }
                } else {
                    ConfigTabHomeItem item = getConfigTabHomeItemFromIndex(indexTab);
                    if (item != null && isTabActive(item.getHomeTab(), item.getId())) {
                        listActiveItem.remove(item);
                        if (i > listActiveItem.size())
                            listActiveItem.add(item);
                        else
                            listActiveItem.add(i, item);
                    }
                }

            }
        }
    }

    private ConfigTabHomeItem getConfigTabHomeItemFromIndex(int index) {
        for (ConfigTabHomeItem item : listActiveItem) {
            switch (index) {
                case 0:
                    if (item.getHomeTab() == HomeTab.tab_hot)
                        return item;
                    break;
                case 1:
                    if (item.getHomeTab() == HomeTab.tab_stranger)
                        return item;
                    break;
                case 2:
                    if (item.getHomeTab() == HomeTab.tab_video)
                        return item;
                    break;
                case 3:
                    if (item.getHomeTab() == HomeTab.tab_movie)
                        return item;
                    break;
                case 4:
                    if (item.getHomeTab() == HomeTab.tab_music)
                        return item;
                    break;
                case 5:
                    if (item.getHomeTab() == HomeTab.tab_news)
                        return item;
                    break;
                case 6:
                    if (item.getHomeTab() == HomeTab.tab_security)
                        return item;
                    break;
                case 9:
                    if (item.getHomeTab() == HomeTab.tab_tiins)
                        return item;
                    break;
                case 7:
                    if (item.getHomeTab() == HomeTab.tab_selfcare)
                        return item;
                    break;
                // TODO CamID: home_tab_config
                // [start] CamID
                case 10:
                    if (item.getHomeTab() == HomeTab.tab_cinema)
                        return item;
                    break;
                case 11:
                    if (item.getHomeTab() == HomeTab.tab_esport)
                        return item;
                    break;
                case 12:
                    if (item.getHomeTab() == HomeTab.tab_metfone)
                        return item;
                    break;
                case 13:
                    if (item.getHomeTab() == HomeTab.tab_contact)
                        return item;
                    break;
                // [end] CamID

            }
        }
        return null;
    }

    public static HomeTab getHomeTabFromIndex(int index) {
        switch (index) {
            case 0:
                return HomeTab.tab_hot;
            case 1:
                return HomeTab.tab_stranger;
            case 2:
                return HomeTab.tab_video;
            case 3:
                return HomeTab.tab_movie;
            case 4:
                return HomeTab.tab_music;
            case 5:
                return HomeTab.tab_news;
            case 6:
                return HomeTab.tab_security;
            case 7:
                return HomeTab.tab_selfcare;
            case 8:
                return HomeTab.tab_wap;
            case 9:
                return HomeTab.tab_tiins;
            // TODO CamID: home_tab_config
            // [start] CamID
            case 10:
                return HomeTab.tab_cinema;
            case 11:
                return HomeTab.tab_esport;
            case 12:
                return HomeTab.tab_metfone;
            case 13:
                return HomeTab.tab_contact;
            // [end] CamID
        }
        return null;
    }

    private void setActiveConfigTabHomeItem(ConfigTabHomeItem tabHomeItem) {
        ArrayList<TabHomeDefault> orderList = mApp.getConfigBusiness().getOrderTabHomeConfig();
        if (orderList != null && !orderList.isEmpty()) {
            int index = -1;
            switch (tabHomeItem.getHomeTab()) {
                case tab_hot:
                    index = 0;
                    break;
                case tab_stranger:
                    index = 1;
                    break;

                case tab_video:
                    index = 2;
                    break;

                case tab_movie:
                    index = 3;
                    break;

                case tab_music:
                    index = 4;
                    break;

                case tab_news:
                    index = 5;
                    break;

                case tab_security:
                    index = -1;
                    break;
                case tab_selfcare:
                    index = 7;
                    break;

                case tab_wap:
                    index = 8;
                    break;
                case tab_tiins:
                    index = 9;
                    break;

                // TODO CamID: home_tab_config
                // [start] CamID
                case tab_cinema:
                    index = 10;
                    break;
                case tab_esport:
                    index = 11;
                    break;
                case tab_metfone:
                    index = 12;
                    break;
                case tab_contact:
                    index = 13;
                    break;
                case tab_game:
                    index = 14;
                    break;
                // [end] CamID

            }
            if (index != -1) {
                for (int i = 0; i < orderList.size(); i++) {
                    TabHomeDefault tabHomeDefault = orderList.get(i);
                    if (tabHomeDefault.getTabIndex() == index) {
                        if (index == 8) {
                            if (tabHomeItem.getId().equals(tabHomeDefault.getIdTabWap())) {
                                tabHomeItem.setState(ConfigTabHomeItem.STATE_ACTIVE);
                                return;
                            }
                        } else {
                            tabHomeItem.setState(ConfigTabHomeItem.STATE_ACTIVE);
                            return;
                        }

                    }
                }

                /*if (orderList.contains(index)) {
                    tabHomeItem.setState(ConfigTabHomeItem.STATE_ACTIVE);
                    return;
                }*/
            }
            tabHomeItem.setState(ConfigTabHomeItem.STATE_NOT_ACTIVE);
        }

    }

    public boolean isTabActive(HomeTab tab, String idTabWap) {
        if (tab == HomeTab.tab_chat || tab == HomeTab.tab_call || tab == HomeTab.tab_more || tab == HomeTab.tab_home)
            return true;
        for (ConfigTabHomeItem item : listActiveItem) {
            if (item.getHomeTab() == tab) {
                if (tab == HomeTab.tab_wap) {
                    if (!TextUtils.isEmpty(idTabWap) && idTabWap.equals(item.getId()))
                        return true;
                } else
                    return true;
            }

        }
        return false;
    }

    public static boolean isTabAvailable(HomeTab tab, ApplicationController mApp) {
        if (tab == HomeTab.tab_chat || tab == HomeTab.tab_call || tab == HomeTab.tab_more)
            return true;

        switch (tab) {
            case tab_hot:
                return mApp.getConfigBusiness().isEnableOnMedia();
            case tab_stranger:
                return mApp.getConfigBusiness().isEnableTabStranger();

            case tab_video:
                return mApp.getConfigBusiness().isEnableTabVideo();

            case tab_movie:
                return mApp.getConfigBusiness().isEnableTabMovie();

            case tab_music:
                return mApp.getConfigBusiness().isEnableTabMusic();

            case tab_news:
                return mApp.getConfigBusiness().isEnableTabNews();

            case tab_security:
                return mApp.getConfigBusiness().isEnableTabSecurity();

            case tab_wap:
                return true;
            case tab_tiins:
                return mApp.getConfigBusiness().isEnableTabTiin();
            case tab_selfcare:
                return mApp.getConfigBusiness().isEnableTabSelfCare();
            // TODO CamID: home_tab_config
            // [start] CamID
            case tab_cinema:
                return mApp.getConfigBusiness().isEnableTabCinema();
            case tab_esport:
                return mApp.getConfigBusiness().isEnableTabESport();
            case tab_metfone:
                return mApp.getConfigBusiness().isEnableTabMetfone();
            case tab_contact:
                return mApp.getConfigBusiness().isEnableTabContact();
            case tab_game:
                return mApp.getConfigBusiness().isEnableTabGame();
            // [end] CamID
        }

        return false;
    }

    public void logConfigTab() {
        String url = UrlConfigHelper.getInstance(mApp).getDomainFile() + ConstantApi.Url.File.API_SETTING_ON_OFF_TAB;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                String data = getDataLogConfigTab();
                Log.i(TAG, "data: " + data);
                String currentTime = String.valueOf(System.currentTimeMillis());
                StringBuilder sb = new StringBuilder().
                        append(mApp.getReengAccountBusiness().getJidNumber()).
                        append(data).append(mApp.getReengAccountBusiness().getToken()).append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApp, sb.toString(), mApp.getReengAccountBusiness().getToken());
                params.put(Constants.HTTP.REST_MSISDN, mApp.getReengAccountBusiness().getJidNumber());
                params.put("data", data);
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put(Constants.HTTP.TIME_STAMP, currentTime);
                return params;
            }
        };
        VolleyHelper.getInstance(mApp).addRequestToQueue(request, TAG, false);
    }

    private String getDataLogConfigTab() {
        if (listActiveItem.isEmpty()) return "notab";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < listActiveItem.size(); i++) {
            ConfigTabHomeItem item = listActiveItem.get(i);
            switch (item.getHomeTab()) {
                case tab_tiins:
                    sb.append("tiin");
                    break;
                case tab_news:
                    sb.append("news");
                    break;

                case tab_hot:
                    sb.append("social");
                    break;

                case tab_movie:
                    sb.append("movies");
                    break;

                case tab_music:
                    sb.append("music");
                    break;

                case tab_video:
                    sb.append("videos");
                    break;

                case tab_stranger:
                    sb.append("dating");
                    break;
                case tab_security:
                    sb.append("security");
                    break;
                case tab_selfcare:
                    sb.append("selfcare");
                    break;
                // TODO CamID: home_tab_config
                // [start] CamID
                case tab_cinema:
                    sb.append("cinema");
                    break;
                case tab_esport:
                    sb.append("esport");
                    break;
                case tab_metfone:
                    sb.append("metfone");
                    break;
                case tab_contact:
                    sb.append("contact");
                    break;
                case tab_game:
                    sb.append("game");
                    break;
                // [end] CamID
            }
            if (i != (listActiveItem.size() - 1))
                sb.append("|");
        }
        return sb.toString();
    }

    public enum HomeTab {
        tab_home,
        tab_chat,
        tab_call,   //cái này ngày xưa là tab to nhưng giờ đẩy thành subtab nên cứ để tạm
        tab_hot,
        tab_stranger,
        tab_more,
        tab_video,
        tab_movie,
        tab_music,
        tab_news,
        tab_security,
        tab_tiins,
        tab_wap,
        tab_my_viettel,
        tab_selfcare,
        tab_umoney,
        // TODO CamID: home_tab_config
        // [start] CamID
        tab_cinema,
        tab_esport,
        tab_metfone,
        tab_contact,
        tab_game;
        // [end] CamID

        public static HomeTab fromString(String name) {
            try {
                return HomeTab.valueOf(name);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return tab_chat;
            }
        }
    }

}

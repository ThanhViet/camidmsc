package com.metfone.selfcare.helper.ads;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.vtm.adslib.AdsCommon;

import java.util.Calendar;

public class AdsUtils {
    public static final class KEY_FIREBASE {
        public static final String AD_APP_ID = "AD_APP_ID";
        public static final String WHITE_LIST_PHONE = "WHITE_LIST_PHONE";
        public static final String AD_CHECK_VIP = "AD_CHECK_VIP";
        public static final String AD_FULL_SHOW_HOME = "AD_FULL_SHOW_HOME";
        public static final String AD_FULL_SHOW_FINISH = "AD_FULL_SHOW_FINISH";
        public static final String AD_DISPLAY_START = "AD_DISPLAY_START";
        public static final String AD_DISPLAY_COUNT = "AD_DISPLAY_COUNT";
        public static final String AD_DISPLAY_TIME = "AD_DISPLAY_TIME";
        public static final String AD_DISPLAY_FIRST = "AD_DISPLAY_FIRST";
        public static final String AD_NEWS = "AD_NEWS";
        public static final String AD_VIDEO_DETAIL = "AD_VIDEO_DETAIL";
        public static final String AD_PLAYER_MUSIC = "AD_PLAYER_MUSIC";
        public static final String AD_FULLSCREEN = "AD_FULLSCREEN";
        public static final String AD_REWARD = "AD_REWARD";
        public static final String AD_NATIVE = "AD_NATIVE";
        public static final String AD_FULL_RAN_FIRST = "AD_FULL_RAN_FIRST";
        public static final String AD_FULL_RAN_SECOND = "AD_FULL_RAN_SECOND";
    }

    public static boolean checkShowAds() {
        // Co enable ads ko? ko phai nam trong whitelist vs ad fullscreen khac null
        if (isEnableAds() == 0 || isWhiteList())
            return false;
        return true;
    }

    private static long isEnableAds() {
        return FirebaseRemoteConfig.getInstance().getLong(AdsCommon.KEY_FIREBASE.ENABLE_ADS);
    }

    private static boolean isWhiteList() {
        try {
            ReengAccountBusiness reengAccountBusiness = ApplicationController.self().getReengAccountBusiness();
            //Tk test
            if (reengAccountBusiness.getJidNumber().equals("0983121485"))
                return false;

            if (BuildConfig.DEBUG)
                return true;

            //Check thi truong
            if (reengAccountBusiness.isCambodia() || reengAccountBusiness.isLaos() || reengAccountBusiness.isTimorLeste() || reengAccountBusiness.isHaiti())
                return true;

            //La can bo nhan vien return luon
            if (reengAccountBusiness.isCBNV())
                return true;

            //Chua login return
            if (reengAccountBusiness.isAnonymousLogin())
                return true;

            //Neu config hien ca vip thi ko can check else nguoc lai
            if (!isShowVip()) {
                if (reengAccountBusiness.isVip())
                    return true;
            }

            String whiteList = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.WHITE_LIST_PHONE);
            String[] phoneList = whiteList.split("\\|");
            String msisdn = reengAccountBusiness.getJidNumber();
            for (String phone : phoneList) {
                if (msisdn.equals(phone)) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    private static boolean isShowVip() {
        return FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_CHECK_VIP) == 1;
    }

    public static boolean checkTimeInDay(long time) {
        Calendar currentCal = Calendar.getInstance();
        int currentDay = currentCal.get(Calendar.DAY_OF_YEAR);
        //        currentCal.setTimeInMillis(System.currentTimeMillis());
        Calendar timeCal = Calendar.getInstance();
        timeCal.setTimeInMillis(time);
        int timeDay = timeCal.get(Calendar.DAY_OF_YEAR);
        return currentDay == timeDay;
    }
}

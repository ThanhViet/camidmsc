package com.metfone.selfcare.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.ResultReceiver;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 7/9/2014.
 */
public class InputMethodUtils {
    private static final String TAG = InputMethodUtils.class.getSimpleName();

    public static int getIntPreferenceHeightPref(Context context, String key, int intDefault) {
        SharedPreferences mPref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        return mPref.getInt(key, intDefault);
    }

    public static void setIntPreferenceHeightPref(Context context, String key, int value) {
        SharedPreferences mPref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        mPref.edit().putInt(key, value).apply();
    }

    public static void hideSoftKeyboard(EditText editText, Context context) {
        if (editText == null || context == null) return;
        //Log.i(TAG, "hideSoftKeyboard(EditText: " + editText.getText() + ", Context: " + context.getClass().getName() + ")");
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(EditText editText, Context context, ResultReceiver resultReceiver) {
        Log.i(TAG, "hideSoftKeyboard(..)");
        if (editText == null || context == null) return;
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0, resultReceiver);
    }

    public static void showSoftKeyboard(Context context, EditText editText) {
        if (context == null || editText == null) return;
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        // imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        // imm.showSoftInput(this.editText,InputMethodManager.SHOW_FORCED);
        editText.requestFocus();
    }

    public static void hideKeyboardWhenTouch(View view, final Activity activity) {
        if (view == null || activity == null) {
            return;
        }
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboardWhenTouch(innerView, activity);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity
                        .getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public static void showSoftKeyboardNew(Activity activity, View view) {
        if (activity == null || view == null) {
            return;
        }
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Fix the leaks of soft input.
     * <p>Call the function in {@link Activity# onDestroy()}.</p>
     *
     * @param activity The activity.
     */
    public static void fixSoftInputLeaks(final Activity activity) {
        if (activity == null) return;
        InputMethodManager imm =
                (InputMethodManager) ApplicationController.self().getSystemService(android.content.Context.INPUT_METHOD_SERVICE);
        if (imm == null) return;
        String[] leakViews = new String[]{"mLastSrvView", "mCurRootView", "mServedView", "mNextServedView"};
        for (String leakView : leakViews) {
            try {
                java.lang.reflect.Field leakViewField = InputMethodManager.class.getDeclaredField(leakView);
                if (leakViewField == null) continue;
                if (!leakViewField.isAccessible()) {
                    leakViewField.setAccessible(true);
                }
                Object obj = leakViewField.get(imm);
                if (!(obj instanceof android.view.View)) continue;
                android.view.View view = (android.view.View) obj;
                if (view.getRootView() == activity.getWindow().getDecorView().getRootView()) {
                    leakViewField.set(imm, null);
                }
            } catch (Throwable e) {
                Log.e(TAG, e);
            }
        }
    }
}
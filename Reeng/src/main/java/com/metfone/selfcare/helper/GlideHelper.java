package com.metfone.selfcare.helper;

import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.util.Log;

import java.io.File;

/**
 * Created by thanhnt72 on 6/16/2018.
 */

public class GlideHelper {

    private static final String TAG = GlideHelper.class.getSimpleName();
    public static final String FILE_PATH = "file://";

    public static GlideHelper mInstance;
    private ApplicationController mApp;

    public GlideHelper(ApplicationController application) {
        mApp = application;
    }

    public static synchronized GlideHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new GlideHelper(application);
        }
        return mInstance;
    }

    /*if (message.getStatus() == ReengMessageConstant.STATUS_RECEIVED
                || message.getDirection() == ReengMessageConstant.Direction.send) {
            String filePath = null;
            if (!TextUtils.isEmpty(message.getFilePath())) {
                filePath = FILE_PATH + message.getFilePath();
            }
            if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(content, imageThumbleSize,
                        imageThumbleSize), videoOptions);
            } else {
                Log.d(TAG, "displayThumbnailOfMessage: " + filePath);
                universalImageLoader.displayImage(filePath, new ImageViewAwareTargetSize(content, imageThumbleSize,
                        imageThumbleSize), imageOptions);
            }
        } else {
            Log.d(TAG, "displayThumbnailOfMessage: cancelDisplayTask");
            universalImageLoader.cancelDisplayTask(content);
            if (message.getMessageType() == ReengMessageConstant.MessageType.shareVideo) {
                content.setImageResource(R.drawable.ic_videos_default);
            } else {
                content.setImageResource(R.drawable.ic_images_default);
            }
        }*/

    public void displayImageThreadChat(final ImageView imageView, ReengMessage message, int width, int height) {
        if (imageView == null || message == null) return;
        Log.i(TAG, "width: " + width + " height: " + height);
        boolean isVideo = message.getMessageType() == ReengMessageConstant.MessageType.shareVideo;
        if (width == 0 || height == 0) {
            Glide.with(mApp).load(isVideo ? R.drawable.ic_videos_default : R.drawable.ic_images_default).into(imageView);
            return;
        }
        if (!TextUtils.isEmpty(message.getFilePath()) && (message.getStatus() == ReengMessageConstant.STATUS_RECEIVED
                || message.getStatus() == ReengMessageConstant.STATUS_SEEN
                || message.getDirection() == ReengMessageConstant.Direction.send)) {
            String filePath = "";
            if (!TextUtils.isEmpty(message.getFilePath())) {
                if (new File(message.getFilePath()).exists()) {
                    filePath = message.getFilePath();
                } else if (new File(FILE_PATH + message.getFilePath()).exists()) {
                    filePath = FILE_PATH + message.getFilePath();
                } else if (!TextUtils.isEmpty(message.getDirectLinkMedia())) {
                    String downloadUrl = UrlConfigHelper.getInstance(mApp).getDomainImage() + message
                            .getDirectLinkMedia();
                    Glide.with(mApp)
                            .load(downloadUrl)
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.ic_images_default)
                                    .error(R.drawable.ic_images_default)
                                    .override(width, height)
                                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                    .fitCenter())
                            .into(imageView);
                    return;
                }
            }

            Glide.with(mApp)
                    .asBitmap()
                    .load(Uri.fromFile(new File(filePath)))
                    .apply(new RequestOptions()
                            .placeholder(isVideo ? R.drawable.ic_videos_default : R.drawable.ic_images_default)
                            .error(isVideo ? R.drawable.ic_videos_default : R.drawable.ic_images_default)
                            .override(width, height)
                            .fitCenter())
                    .into(imageView);
        } else {
            if (message.getMessageType() == ReengMessageConstant.MessageType.image
                    && message.getDirection() == ReengMessageConstant.Direction.send
                    && TextUtils.isEmpty(message.getFilePath())
                    && !TextUtils.isEmpty(message.getDirectLinkMedia())
                    ) {
                String downloadUrl = UrlConfigHelper.getInstance(mApp).getDomainImage() + message
                        .getDirectLinkMedia();
                Glide.with(mApp)
                        .load(downloadUrl)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_images_default)
                                .error(R.drawable.ic_images_default)
                                .override(width, height)
                                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                .fitCenter())
                        .into(imageView);
            } else {
                if (isVideo) {
                    imageView.setImageResource(R.drawable.ic_videos_default);
                } else {
                    imageView.setImageResource(R.drawable.ic_images_default);
                }
            }
        }

    }

    public void setSongAvatar(ImageView songImage, String url) {
        Glide.with(mApp).load(url).apply(new RequestOptions().placeholder(R.drawable.ic_keeng).error(R.drawable.ic_keeng)).into(songImage);
    }
}

package com.metfone.selfcare.helper.workmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.fragment.setting.SettingNotificationFragment;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Executor;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class SettingNotiWork extends Worker {
    public static final String TAG = SettingNotiWork.class.getSimpleName();

    public SettingNotiWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        new MainThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    SettingBusiness.getInstance(ApplicationController.self()).setPrefSettingOnOffNoti(true, -1);
                    EventBus.getDefault().post(new SettingNotificationFragment.SettingNotificationEvent(true));
                } catch (Exception e) {

                }
            }
        });
        return Result.success();
    }

    private static class MainThreadExecutor implements Executor {
        MainThreadExecutor() {
            mHandler = new Handler(Looper.getMainLooper());
        }

        private Handler mHandler;

        @Override
        public void execute(Runnable runnable) {
            if (mHandler != null) {
                mHandler.post(runnable);
            }
        }
    }
}

package com.metfone.selfcare.helper;

import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.database.model.LocationMusic;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.util.Log;

import java.text.Collator;
import java.util.Comparator;

/**
 * Created by toanvk2 on 6/27/14.
 */
public class ComparatorHelper {
    private static final String TAG = ComparatorHelper.class.getSimpleName();
    private static Collator collatorVi = Collator
            .getInstance(Constants.LOCATION.DEFAULE_LOCATION);

    /**
     * sort contact by name
     *
     * @return Object
     */
    public static Comparator<Object> getComparatorContactByName() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    String name1 = ((Contact) ob1).getNameUnicode();
                    String name2 = ((Contact) ob2).getNameUnicode();
                    return name1.compareTo(name2);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    /**
     * sort phone number by name
     *
     * @return Object
     */
    public static Comparator<Object> getComparatorNumberByName() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    String name1 = ((PhoneNumber) ob1).getNameUnicode();
                    String name2 = ((PhoneNumber) ob2).getNameUnicode();
                    return name1.compareTo(name2);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    /**
     * sort phone number by number
     *
     * @return object
     */
    public static Comparator<Object> getComparatorNumberByNumber() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    String number1 = ((PhoneNumber) ob1).getJidNumber();
                    String number2 = ((PhoneNumber) ob2).getJidNumber();
                    return number1.compareTo(number2);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    /**
     * sort thread message by time
     *
     * @return object
     */
    public static Comparator<Object> getComparatorThreadMessageByLastTime() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    Long time1 = ((ThreadMessage) ob1).getLastChangeThread();
                    Long time2 = ((ThreadMessage) ob2).getLastChangeThread();
                    return time2.compareTo(time1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    public static Comparator<Object> getComparatorThreadMessageByLastTimePin() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    Long time1 = ((ThreadMessage) ob1).getLastTimePinThread();
                    Long time2 = ((ThreadMessage) ob2).getLastTimePinThread();
                    return time2.compareTo(time1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }


    public static Comparator<Object> getComparatorMessageBySender() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    String sender1 = ((ReengMessage) ob1).getSender();
                    String sender2 = ((ReengMessage) ob2).getSender();
                    return sender1.compareTo(sender2);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    /**
     * sort list sticker recent by recent time
     *
     * @return
     */
    public static Comparator<StickerItem> getComparatorStickerRecent() {
        Comparator<StickerItem> comparator = new Comparator<StickerItem>() {
            @Override
            public int compare(StickerItem lhs, StickerItem rhs) {
                try {
                    Long time1 = lhs.getRecentTime();
                    Long time2 = rhs.getRecentTime();
                    return time2.compareTo(time1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    /**
     * sort list sticker item by item id
     *
     * @return
     */
    public static Comparator<StickerItem> getComparatorStickerItem() {
        Comparator<StickerItem> comparator = new Comparator<StickerItem>() {
            @Override
            public int compare(StickerItem lhs, StickerItem rhs) {
                try {
                    Integer id1 = lhs.getItemId();
                    Integer id2 = rhs.getItemId();
                    return id1.compareTo(id2);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    public static Comparator<Region> getComparatorRegionByCountryName() {
        Comparator<Region> comparator = new Comparator<Region>() {
            @Override
            public int compare(Region lhs, Region rhs) {
                try {
                    String countryName1 = lhs.getRegionName();
                    String countryName2 = rhs.getRegionName();
                    return countryName1.compareTo(countryName2);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    public static Comparator<StickerCollection> getComparatorStickerCollectionByOrder() {
        Comparator<StickerCollection> comparator = new Comparator<StickerCollection>() {
            @Override
            public int compare(StickerCollection lhs, StickerCollection rhs) {
                try {
                    Integer order1 = lhs.getOrder();
                    Integer order2 = rhs.getOrder();
                    //if (order1 > 0 && order2 > 0) {
                    return order2.compareTo(order1);
                    /*} else {
                        Long id1 = lhs.getLocalId();
                        Long id2 = rhs.getLocalId();
                        return id2.compareTo(id1);
                    }*/
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    public static Comparator<StickerCollection> getComparatorStickerCollectionByLastSticky() {
        Comparator<StickerCollection> comparator = new Comparator<StickerCollection>() {
            @Override
            public int compare(StickerCollection lhs, StickerCollection rhs) {
                try {
                    Long sticky1 = lhs.getLastSticky();
                    Long sticky2 = rhs.getLastSticky();
                    return sticky2.compareTo(sticky1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    public static Comparator<StrangerPhoneNumber> getComparatorStrangerByCreateDate() {
        Comparator<StrangerPhoneNumber> comparator = new Comparator<StrangerPhoneNumber>() {
            @Override
            public int compare(StrangerPhoneNumber lhs, StrangerPhoneNumber rhs) {
                try {
                    Long createDate1 = lhs.getCreateDate();
                    Long createDate2 = rhs.getCreateDate();
                    return createDate2.compareTo(createDate1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }

    public static Comparator<LocationMusic> getComparatorLocationMusic() {
        Comparator<LocationMusic> comparator = new Comparator<LocationMusic>() {
            @Override
            public int compare(LocationMusic lhs, LocationMusic rhs) {
                try {
                    Integer order1 = lhs.getOrder();
                    Integer order2 = rhs.getOrder();
                    return order2.compareTo(order1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }


    public static Comparator<Object> getComparatorEventMessageByLastTime() {
        Comparator<Object> comparator = new Comparator<Object>() {
            @Override
            public int compare(Object ob1, Object ob2) {
                try {
                    Long time1 = ((EventMessage) ob1).getTime();
                    Long time2 = ((EventMessage) ob2).getTime();
                    return time2.compareTo(time1);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    return 0;
                }
            }
        };
        return comparator;
    }
}

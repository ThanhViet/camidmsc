package com.metfone.selfcare.helper.voicemail;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Process;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.ThreadDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.network.AudioVoicemailManager;
import com.metfone.selfcare.ui.imageview.RevealView;
import com.metfone.selfcare.util.Log;

import java.io.IOException;

public class AudioVoicemailPlayerImpl2
        implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private MediaPlayer mp = null;
    private ReengMessage currentPlayingVoicemail;
    private StopWatch stopWatch;
    private Handler mHandler;
    private View convertView;
    private boolean needToPause = false;
    private String TAG = "AudioVoicemailPlayerImpl2";
    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
//            Log.d(TAG, "mUpdateTimeTask running");
            if (mp.isPlaying()) {
                long totalDuration = mp.getDuration();
                long currentDuration = mp.getCurrentPosition();

                if(convertView == null)
                    convertView = AudioVoicemailManager.getConvertViewByMessageId(currentPlayingVoicemail.getId());
                if (convertView != null) {
                    int messageId = AudioVoicemailManager
                            .getMessageIdByConvertView(convertView);
                    // Log.i(TAG, " convertView != null ");
                    // kiem tra neu dung la dang play voicemail tuong ung voi
                    // convertView
                    if (messageId == currentPlayingVoicemail.getId()) {
                        // Log.i(TAG, " same messageId " + messageId);
                        // Updating progress bar

                        TextView elapsedTimeView = (TextView) convertView.findViewById(R.id.voicemail_detail_length);
                        ImageView playButton = (ImageView) convertView.findViewById(R.id.voicemail_detail_button_play);
                        if (currentPlayingVoicemail.getDirection() == ReengMessageConstant.Direction.received){
                            playButton.setImageResource(R.drawable.pause_circle_filled_recived);
                        } else {
                            playButton.setImageResource(R.drawable.pause_circle_voice);
                        }

                        int progress = (int) (currentDuration * 100.0 / totalDuration);

//                        ProgressBar playingSeekBar = (ProgressBar) convertView.findViewById(R.id.voicemail_seekbar);
//                        playingSeekBar.setMax(100);
//                        playingSeekBar.setProgress(progress);
                        RevealView mreRevealView = (RevealView) convertView.findViewById(R.id.image_progress);
                        mreRevealView.setPercentage(progress);
                        Log.i(TAG, "progress: " + progress);

                        currentPlayingVoicemail.setPlayingProgressPercentage(progress);
                        currentPlayingVoicemail.setDuration(mp.getDuration() / 1000);

                        elapsedTimeView.setText(StopWatch.convertSecondInMMSSForm(false, (int) currentDuration / 1000));
                        // Running this thread after 100 milliseconds
                    }
                }
                mHandler.postDelayed(this, 200);
            }
        }
    };
    private Activity activity;
    private int voicemailDuration;
    private AudioManager audioManager;
    private ThreadDetailAdapter chatAdapter;
    private SharedPreferences mPref;
    private boolean externalSpeakerEnabled = true;
    private ApplicationController mApplicationController;

    public AudioVoicemailPlayerImpl2(ApplicationController mApp) {
        mp = new MediaPlayer();
        mp.setOnCompletionListener(this); // Important
        mp.setOnPreparedListener(this);
        mp.setVolume(1.0f, 1.0f);
        mHandler = new Handler();
        mApplicationController = mApp;
    }


    @Override
    public void onPrepared(MediaPlayer play) {
        Log.i(TAG, "onPrepared(MediaPlayer play)");
        if (mApplicationController.getPlayMusicController() != null) {
            if (mApplicationController.getPlayMusicController().isPlaying()) {
                mApplicationController.getPlayMusicController().setStateResumePlaying(true);
                mApplicationController.getPlayMusicController().toggleMusic();
            }
            mApplicationController.getPlayMusicController().setOtherAudioPlaying(true);
        }
        setSpeakerOn(externalSpeakerEnabled);
        play.start();
        currentPlayingVoicemail.setPlaying(true);
        updateProgressBar();
    }

    public void setSpeakerOn(boolean isOn) {
        externalSpeakerEnabled = isOn;
        if (audioManager == null) return;
        if (isOn) {
            audioManager.setMode(AudioManager.MODE_NORMAL);
            audioManager.setSpeakerphoneOn(true);
        } else {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setSpeakerphoneOn(false);
        }
    }


    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
//        Log.d(TAG, "updateProgressBar()");
        mHandler.postDelayed(mUpdateTimeTask, 200);
    }

    @Override
    public boolean onError(MediaPlayer arg0, int arg1, int arg2) {
//        Log.e(TAG, "onError");
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        stopVoicemail(true);
        try {
            resetUI();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }

    }


    public void playVoicemail(ReengMessage voicemail, ThreadDetailAdapter adapter,
                              Activity activity) {
        // Log.i(TAG, "playVoicemail clicked");
        chatAdapter = adapter;
        convertView = null;
        if (mp.isPlaying()) {
            // stop playing voicemail
            resetUI();
            stopVoicemail(true);
            // if click on current playing voicemail -> stop voicemail
            // else play voicemail
            if (voicemail != null
                    && voicemail.getId() == currentPlayingVoicemail.getId()) {
                needToPause = true;
                // Log.i(TAG, " stop playing");
                return;
            }
        }
        this.activity = activity;
        if (audioManager == null)
            audioManager = (AudioManager) activity
                    .getSystemService(Context.AUDIO_SERVICE);
        if (mPref == null) {
            mPref = activity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        Log.d(TAG, " playVoicemail externalSpeakerEnabled " + externalSpeakerEnabled);
        externalSpeakerEnabled = mPref.getBoolean(Constants.PREFERENCE.PREF_SPEAKER, true);
        needToPause = true;
        // reset input source
        setInputSource(voicemail);
        needToPause = false;
        playAudio();
    }

    public void stopVoicemail(boolean isToogleMusic) {
        if (mp.isPlaying()) {
            // Log.d(TAG, "stopvoicemail() playing");
            mp.stop();
            currentPlayingVoicemail.setPlaying(false);
            currentPlayingVoicemail.setDuration(voicemailDuration);
            // wating for playing thread stop
        }
        if (mApplicationController.getPlayMusicController() != null && isToogleMusic) {
            mApplicationController.getPlayMusicController().checkAndResumeMusic();
        }
    }

    public void resetUI(ThreadDetailAdapter adapter,
                        Activity activity) {
        Log.d(TAG, "resetUI()");
        chatAdapter = adapter;
        this.activity = activity;
        stopStopWatch();
        if (currentPlayingVoicemail != null) {
            currentPlayingVoicemail.setPlaying(false);
            currentPlayingVoicemail.setDuration(voicemailDuration);
        }
        if (activity != null) {
            activity.getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (chatAdapter != null) {
                    chatAdapter.notifyDataSetChanged();
                    Log.d(TAG, "chatAdapter.notifyDataSetChanged()");
                }
            }
        });
    }

    private void resetUI() {
        Log.d(TAG, "resetUI()");
        stopStopWatch();
        if (currentPlayingVoicemail != null) {
            currentPlayingVoicemail.setPlaying(false);
            currentPlayingVoicemail.setDuration(voicemailDuration);
        }
        if (activity != null) {
            activity.getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (chatAdapter != null) {
                    chatAdapter.notifyDataSetChanged();
                    Log.d(TAG, "chatAdapter.notifyDataSetChanged()");
                }
            }
        });
    }

    public void stopVoicemail(View cView, ReengMessage msg) {

        if (mp.isPlaying()) {
            mp.stop();
            currentPlayingVoicemail.setDuration(voicemailDuration);
            // wating for playing thread stop
            stopStopWatch();
        }
    }

    public void playAudio() {
        // Play song
        try {
            mp.reset();
            mp.setDataSource(currentPlayingVoicemail.getFilePath());
            mp.prepare();
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "IllegalArgumentException", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "IllegalStateException", e);
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }
    }

    private void setInputSource(ReengMessage m) {
        currentPlayingVoicemail = m;
        voicemailDuration = currentPlayingVoicemail.getDuration();
    }

    private void startStopWatch() {
        stopWatch = new StopWatch();
        stopWatch.start();
        mHandler = new Handler();
        final int miliSecondToSleep = 1000;

        new Thread(new Runnable() {
            public void run() {
                android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                while (mp.isPlaying() && !needToPause) {
                    try {
                        int passedSeconds = voicemailDuration
                                - (int) stopWatch.getElapsedTimeSecs();
                        if (passedSeconds < 0)
                            break;
                        currentPlayingVoicemail.setDuration(passedSeconds);
                        //final String s = StopWatch.convertSecondInMMSSForm(false, passedSeconds);
                        if (mHandler == null)
                            return;
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                chatAdapter.notifyDataSetChanged();
                            }
                        });
                        Thread.sleep(miliSecondToSleep);
                    } catch (InterruptedException e) {
                        Log.i(TAG, "" + e.toString());
                    }
                }
            }
        }).start();
    }

    private void stopStopWatch() {
        if (stopWatch != null) {
            stopWatch.stop();
        }
    }

    public void setMusicPlaying(boolean isMusicPlaying) {
        if (mApplicationController.getPlayMusicController() != null) {
            mApplicationController.getPlayMusicController().setStateResumePlaying(isMusicPlaying);
        }
    }
}

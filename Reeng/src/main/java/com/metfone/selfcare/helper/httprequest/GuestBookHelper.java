package com.metfone.selfcare.helper.httprequest;

import android.content.Intent;
import android.content.res.Resources;
import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.GuestBookActivity;
import com.metfone.selfcare.activity.GuestBookEditorActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.BookVote;
import com.metfone.selfcare.database.model.guestbook.EmoCollection;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.database.model.guestbook.Song;
import com.metfone.selfcare.database.model.guestbook.Template;
import com.metfone.selfcare.database.model.guestbook.TemplateDetail;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class GuestBookHelper {
    private static final String TAG = GuestBookHelper.class.getSimpleName();
    public static final float DEFAULT_HEIGHT = 2480.0f;
    public static final float DEFAULT_WIDTH = 1476.0f;
    public static final String TAG_GET_STICKERS = "getStickers";
    public static final String TAG_GET_TEMPLATES = "getTemplates";
    public static final String TAG_GET_BACKGROUNDS = "getBackgrounds";
    public static final String TAG_GET_MUSICS = "getMusics";
    public static final String TAG_SAVE = "saveDetail";
    public static final String TAG_GET_BOOKS = "getBooks";
    public static final String TAG_GET_BOOK_DETAIL = "getBookDetail";
    public static final String TAG_GET_PAGE_ASSIGN = "getPagesAssign";
    public static final String TAG_GET_LIST_VOTE = "getListVote";
    public static final String TAG_GET_VOTE_DETAL = "getVoteDetail";
    public static final String TAG_VOTE = "vote";
    public static final String TAG_PROCESS = "process";
    public static final String TAG_ASSIGN = "assign";

    private ApplicationController mApplication;
    private Resources mRes;
    private int pageWidth, pageHeight;
    private int centerX, centerY;
    private int itemDefaultWidth;
    private int minSize;

    private static GuestBookHelper mInstance;
    private ArrayList<EmoCollection> mEmoCollections = new ArrayList<>();
    private ArrayList<Template> mTemplates;
    private Book currentBookEditor;
    private ArrayList<Background> mBackgrounds = new ArrayList<>();
    private ArrayList<Song> mMusics = new ArrayList<>();
    private ArrayList<Book> myBooks = null;
    private ArrayList<Page> myPagesAssigned = null;
    private boolean needLoadMyBooks = false;
    //
    private Page currentPageEditor;
    private boolean needChangedPageData = false;
    // vote
    private ArrayList<BookVote> mListVotesWeek;
    private ArrayList<BookVote> mListVotesTotal;


    private GuestBookHelper(ApplicationController application) {
        this.mApplication = application;
        this.mRes = mApplication.getResources();
        itemDefaultWidth = mRes.getDimensionPixelOffset(R.dimen.guest_book_item_default);
        pageHeight = mApplication.getHeightPixels() - mApplication.getStatusBarHeight()
                - mRes.getDimensionPixelOffset(R.dimen.action_bar_height);
        pageWidth = (int) (pageHeight * com.android.editor.sticker.utils.Constants.SCALE_WIDTH);
        centerX = pageWidth / 2;
        centerY = pageHeight / 2;
        minSize = mRes.getDimensionPixelOffset(R.dimen.guest_min_size);
        Log.d(TAG, "pageWidth: " + pageWidth + "***" + pageHeight);
    }

    public static synchronized GuestBookHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new GuestBookHelper(application);
        }
        return mInstance;
    }

    public int getMinSize() {
        return minSize;
    }

    public int getPageWidth() {
        return pageWidth;
    }

    public int getPageHeight() {
        return pageHeight;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public int getItemDefaultWidth() {
        return itemDefaultWidth;
    }

    public ArrayList<EmoCollection> getEmoCollections() {
        if (mEmoCollections == null) mEmoCollections = new ArrayList<>();
        return mEmoCollections;
    }

    public ArrayList<Template> getTemplates() {
        return mTemplates;
    }

    public ArrayList<Background> getBackgrounds() {
        if (mBackgrounds == null) mBackgrounds = new ArrayList<>();
        return mBackgrounds;
    }

    public ArrayList<Song> getMusics() {
        if (mMusics == null) mMusics = new ArrayList<>();
        return mMusics;
    }

    public ArrayList<BookVote> getListVotes(int tab) {
        if (tab == Constants.GUEST_BOOK.VOTE_TAB_TOTAL) {
            return mListVotesTotal;
        } else {
            return mListVotesWeek;
        }
    }

    public int calculateItemDefaultHeight(int readWidth, int realHeight) {
        return (itemDefaultWidth * realHeight) / readWidth;
    }

    // quyển đang được chỉnh sửa,save lại trạng thái để chuyển giữa các fragment có thể cập nhật được dữ liệu mà ko
    // cần gọi lại api get detail
    public Book getCurrentBookEditor() {
        return currentBookEditor;
    }

    public boolean isCurrentBookEmpty() {
        if (currentBookEditor == null) return true;
        if (currentBookEditor.getPages() == null) return true;
        if (currentBookEditor.getPages().isEmpty()) return true;
        return false;
    }

    public void setCurrentBookEditor(Book currentBookEditor) {
        this.currentBookEditor = currentBookEditor;
        if (currentBookEditor.getPages() != null) {
            int i = 0;
            for (Page page : currentBookEditor.getPages()) {
                page.setPosition(i);
                i++;
            }
        }
    }

    public void updateCurrentBookInfo(Book currentBook, boolean updatePreview) {
        if (myBooks != null) {
            for (Book book : myBooks) {
                if (book.getId() != null && book.getId().equals(currentBook.getId())) {
                    //tạm thời chỉ cần update 2 thông tin này vào obj lưu trên mem
                    book.setBookClass(currentBook.getBookClass());
                    book.setBookName(currentBook.getBookName());
                    if (updatePreview) {
                        book.setPreview(currentBook.getPreview());
                    }
                    break;
                }
            }
        }
    }

    public ArrayList<Book> getMyBooks() {
        return myBooks;
    }

    public ArrayList<Page> getMyPagesAssigned() {
        return myPagesAssigned;
    }

    public boolean isNeedLoadMyBooks() {
        return needLoadMyBooks;
    }

    public void setNeedLoadMyBooks(boolean needLoadMyBooks) {
        this.needLoadMyBooks = needLoadMyBooks;
    }

    public Page getCurrentPageEditor() {
        return currentPageEditor;
    }

    public boolean isNeedChangedPageData() {
        return needChangedPageData;
    }

    public void setNeedChangedPageData(boolean needChangedPageData) {
        this.needChangedPageData = needChangedPageData;
    }

    public void setCurrentPageEditor(Page editorPage) {
        Log.d(TAG, "setCurrentPageEditor ownerName: " + editorPage.getOwnerName());
        this.currentPageEditor = editorPage;
        needChangedPageData = true;
        if (currentBookEditor != null && currentBookEditor.getId() != null &&
                currentBookEditor.getId().equals(editorPage.getBookId())) {
            if (currentBookEditor.getPages() != null) {
                int size = currentBookEditor.getPages().size();
                for (int i = 0; i < size; i++) {
                    if (currentBookEditor.getPages().get(i).getPage() == editorPage.getPage()) {
                        currentBookEditor.getPages().set(i, editorPage);
                        break;
                    }
                }
            }
            if (editorPage.getPosition() == 0) {
                currentBookEditor.setPreview(editorPage.getPreview());
                updateCurrentBookInfo(currentBookEditor, true);
            }
        }
        // update page assigned neu co
        if (myPagesAssigned != null) {
            int sizeAssigned = myPagesAssigned.size();
            for (int j = 0; j < sizeAssigned; j++) {
                if (myPagesAssigned.get(j).getBookId() != null && myPagesAssigned.get(j).getBookId().equals(editorPage.getBookId()) &&
                        myPagesAssigned.get(j).getPage() == editorPage.getPage()) {
                    myPagesAssigned.set(j, editorPage);
                    break;
                }
            }
        }
    }

    public void navigateToGuestBookMain(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, GuestBookActivity.class);
        intent.putExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_TYPE, Constants.GUEST_BOOK.MAIN_TYPE_BOOKS);
        activity.startActivity(intent);
    }

    public void navigateToGuestBookFriend(BaseSlidingFragmentActivity activity, String friendNumber, String friendName) {
        Intent intent = new Intent(activity, GuestBookActivity.class);
        intent.putExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_TYPE, Constants.GUEST_BOOK.MAIN_TYPE_BOOKS);
        intent.putExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER, friendNumber);
        intent.putExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME, friendName);
        activity.startActivity(intent);
    }

    public void navigateToGuestBookVotes(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, GuestBookActivity.class);
        intent.putExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_TYPE, Constants.GUEST_BOOK.MAIN_TYPE_VOTES);
        activity.startActivity(intent);
    }

    public void navigateToGuestBookEditor(BaseSlidingFragmentActivity activity, Page page) {
        this.currentPageEditor = page;
        Intent intent = new Intent(activity, GuestBookEditorActivity.class);
        activity.startActivity(intent);
    }

    public void requestEmoCollection(final GetDataListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_STICKERS), numberEncode,
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestEmoCollection response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                mEmoCollections = parserEmoCollection(object);
                                listener.onSuccess();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_STICKERS, false);
    }

    public void requestTemplates(final GetDataListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_TEMPLATES), numberEncode,
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestTemplates response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                mTemplates = parserTemplates(object);
                                listener.onSuccess();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_TEMPLATES, false);
    }

    public void requestTemplateDetail(int templateId, final GetTemplateDetailListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(templateId).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_TEMPLATE_DETAIL), numberEncode,
                String.valueOf(templateId), String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestTemplateDetail response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                TemplateDetail templateDetail = new Gson().fromJson(object, TemplateDetail.class);
                                listener.onSuccess(templateDetail);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_TEMPLATES, false);
    }

    public void requestBackgrounds(final GetDataListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_BACKGROUND), numberEncode,
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestBackgrounds response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                mBackgrounds = parserBackgrounds(object);
                                listener.onSuccess();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_BACKGROUNDS, false);
    }

    public void requestMusics(final GetDataListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_MUSIC), HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()),
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestMusics response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                mMusics = parserMusics(object);
                                listener.onSuccess();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_MUSICS, false);
    }

    public void requestGetListBooks(String owner, final boolean isRequestMe, final GetBooksListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(owner).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_LIST_BOOK),
                HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()), HttpHelper.EncoderUrl(owner),
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestGetListBooks response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<Book> books = parserBooks(object);
                                if (isRequestMe) {
                                    myBooks = books;
                                }
                                listener.onSuccess(books);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_BOOKS, false);
    }

    public void requestGetBookDetail(final String bookId, final GetBookDetailListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(bookId).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_BOOK_DETAIL), numberEncode,
                HttpHelper.EncoderUrl(bookId), String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestGetBookDetail response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK && object.has("lst_result")) {
                                JsonObject bookObject = object.getAsJsonObject("lst_result");
                                Gson gson = new Gson();
                                Book book = gson.fromJson(bookObject, Book.class);
                                if (book.getPages() == null || book.getPages().isEmpty()) {
                                    listener.onError(errorCode);
                                } else {
                                    listener.onSuccess(book);
                                }
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_BOOK_DETAIL, false);
    }

    public void requestGetMyPagesAssigned(final GetDataListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_GET_PAGES_ASSIGNED), numberEncode,
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestGetMyPagesAssigned response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                if (object.has("lst_result")) {
                                    JsonArray jsonArray = object.getAsJsonArray("lst_result");
                                    myPagesAssigned = parserPagers(jsonArray);
                                } else {
                                    myPagesAssigned = new ArrayList<>();
                                }
                                listener.onSuccess();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_PAGE_ASSIGN, false);
    }

    private void requestSave(final String jsonInfo, final String bookId, final int type,
                             final ProcessBookListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(mRes.getString(R.string.request_send_error));
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.BOOK_SAVE);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "requestSave response: " + response);
                int errorCode = -1;
                String desc = mRes.getString(R.string.request_send_error);
                try {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject object = jsonElement.getAsJsonObject();
                    if (object.has(Constants.HTTP.REST_CODE)) {
                        errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                    }
                    if (object.has(Constants.HTTP.REST_DESC)) {
                        desc = object.get(Constants.HTTP.REST_DESC).getAsString();
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onSuccess(desc);
                    } else {
                        listener.onError(desc);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception:", e);
                    listener.onError(desc);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(mRes.getString(R.string.request_send_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(accountBusiness.getJidNumber()).
                        append(bookId).
                        append(type).
                        append(jsonInfo).
                        append(accountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, accountBusiness.getJidNumber());
                params.put("id_memory", bookId);
                params.put("type", String.valueOf(type));
                params.put("json_string", jsonInfo);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SAVE, false);
    }

    public void requestAssignFriend(final String bookId, final int pageId, final String friendNumber, final UpdateStateListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.BOOK_ASSIGN_PAGE);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "requestAssignFriend response: " + response);
                int errorCode = -1;
                try {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject object = jsonElement.getAsJsonObject();
                    if (object.has(Constants.HTTP.REST_CODE)) {
                        errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onSuccess();
                    } else {
                        listener.onError(errorCode);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception:", e);
                    listener.onError(-1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(accountBusiness.getJidNumber()).
                        append(bookId).
                        append(pageId).
                        append(friendNumber).
                        append(accountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, accountBusiness.getJidNumber());
                params.put("id_memory", bookId);
                params.put("id_page", String.valueOf(pageId));
                params.put("assigned", String.valueOf(friendNumber));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                Log.d(TAG, "requestAssignFriend: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_ASSIGN, false);
    }

    public void requestProcess(final String bookId, final int pageId, final int status,
                               final int type, final ProcessBookListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(mRes.getString(R.string.request_send_error));
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.BOOK_PROCESS_BOOK);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "requestProcess response: " + response);
                int errorCode = -1;
                String desc = mRes.getString(R.string.request_send_error);
                try {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject object = jsonElement.getAsJsonObject();
                    if (object.has(Constants.HTTP.REST_CODE)) {
                        errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                    }
                    if (object.has(Constants.HTTP.REST_DESC)) {
                        desc = object.get(Constants.HTTP.REST_DESC).getAsString();
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onSuccess(desc);
                    } else {
                        listener.onError(desc);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception:", e);
                    listener.onError(desc);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(mRes.getString(R.string.request_send_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(accountBusiness.getJidNumber()).
                        append(bookId).
                        append(pageId).
                        append(status).
                        append(type).
                        append(accountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, accountBusiness.getJidNumber());
                params.put("id_memory", bookId);
                params.put("id_page", String.valueOf(pageId));
                params.put("status", String.valueOf(status));
                params.put("type", String.valueOf(type));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_PROCESS, false);
    }

    public String requestUploadFile(File file) {
        Log.d(TAG, "requestUploadFile: " + file.getName() + " *** " + file.getPath());
        String responsePath;
        try {
            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
            String uploadUrl = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfImage(Config.UrlEnum.BOOK_UPLOAD_IMAGE);
            // init
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(Constants.HTTP.REST_MSISDN, accountBusiness.getJidNumber());
            builder.addFormDataPart(Constants.HTTP.FILE.REST_FILE_TYPE, "image");
            builder.addFormDataPart(Constants.HTTP.REST_TOKEN, accountBusiness.getToken());
            builder.addFormDataPart(Constants.HTTP.FILE.REST_UP_FILE, file.getName(),
                    RequestBody.create(MediaType.parse(FileHelper.detectMimeType(file)), file));
            // request
            okhttp3.Request mRequest = new okhttp3.Request.Builder()
                    .url(uploadUrl)
                    .post(builder.build())
                    .build();
            OkHttpClient client = new OkHttpClient();
            okhttp3.Response mResponse = client.newCall(mRequest).execute();
            int responseCode = mResponse.code();
            String response = mResponse.body().string();
            Log.d(TAG, "responseCode: " + responseCode + " - " + response);
            if (responseCode == HttpURLConnection.HTTP_PARTIAL || responseCode == HttpURLConnection.HTTP_OK) {
                JSONObject object = new JSONObject(response);
                int errorCode = object.optInt("errorCode", -1);
                if (errorCode == 0) {
                    responsePath = object.optString("link", "");
                    //listener.onSuccess(object.optString("link", ""));
                } else {
                    responsePath = null;
                    //listener.onError(responseCode, response);
                }
            } else {
                responsePath = null;
                //listener.onError(responseCode, response);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            responsePath = null;
            //listener.onError(-1, "Exception");
        }
        return responsePath;
    }

    public void requestListVotes(final int type, final int page, final GetListVoteListener listener) {
        String tag = "requestListVotes" + type + page;
        cancelRequest(tag);
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(type).
                append(page).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_VOTE_GET_LIST), numberEncode,
                String.valueOf(type), String.valueOf(page),
                String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestListVotes response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                ArrayList<BookVote> votes = parserBookVotes(object);
                                if (page == 1) {// load page 1
                                    if (type == Constants.GUEST_BOOK.VOTE_TAB_WEEK) {
                                        mListVotesWeek = votes;
                                    } else {
                                        mListVotesTotal = votes;
                                    }
                                }
                                listener.onSuccess(votes);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, tag, false);
    }

    public void requestVoteDetail(String bookId, final GetVoteDetailListener listener) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String numberEncode = HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber());
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(bookId).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(),
                mAccountBusiness.getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.BOOK_VOTE_GET_DETAIL), numberEncode,
                bookId, String.valueOf(currentTime), dataEncrypt);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestVoteDetail response: " + response);
                        int errorCode = -1;
                        try {
                            JsonElement jsonElement = new JsonParser().parse(response);
                            JsonObject object = jsonElement.getAsJsonObject();
                            if (object.has(Constants.HTTP.REST_CODE)) {
                                errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                int totalVote = 0;
                                int isVoted = 0;
                                if (object.has("data")) {
                                    JsonObject dataObject = object.getAsJsonObject("data");
                                    if (dataObject.has("vote_total")) {
                                        totalVote = dataObject.get("vote_total").getAsInt();
                                    }
                                    if (dataObject.has("is_vote")) {
                                        isVoted = dataObject.get("is_vote").getAsInt();
                                    }
                                }
                                listener.onSuccess(totalVote, isVoted);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception:", e);
                            listener.onError(-1);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_VOTE_DETAL, false);
    }

    public void requestVoteBook(final String bookId, final UpdateStateListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.BOOK_VOTE);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "requestVoteBook response: " + response);
                int errorCode = -1;
                try {
                    JsonElement jsonElement = new JsonParser().parse(response);
                    JsonObject object = jsonElement.getAsJsonObject();
                    if (object.has(Constants.HTTP.REST_CODE)) {
                        errorCode = object.get(Constants.HTTP.REST_CODE).getAsInt();
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onSuccess();
                    } else {
                        listener.onError(errorCode);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception:", e);
                    listener.onError(-1);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse" + error.toString());
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(accountBusiness.getJidNumber()).
                        append(bookId).
                        append(accountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, accountBusiness.getJidNumber());
                params.put("id_memory", bookId);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_VOTE, false);
    }

    public void cancelRequest(String tag) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(tag);
    }

    private ArrayList<EmoCollection> parserEmoCollection(JsonObject object) throws JsonSyntaxException {
        ArrayList<EmoCollection> collections = new ArrayList<>();
        if (object.has("lst_sticker")) {
            JsonArray jsonArray = object.getAsJsonArray("lst_sticker");
            if (jsonArray != null && jsonArray.size() > 0) {
                int lengthArray = jsonArray.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    EmoCollection item = gson.fromJson(jsonArray.get(i), EmoCollection.class);
                    collections.add(item);
                }
            }
        }
        return collections;
    }

    private ArrayList<Template> parserTemplates(JsonObject object) throws JsonSyntaxException {
        ArrayList<Template> templates = new ArrayList<>();
        if (object.has("lst_template")) {
            JsonArray jsonArray = object.getAsJsonArray("lst_template");
            if (jsonArray != null && jsonArray.size() > 0) {
                int lengthArray = jsonArray.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    Template item = gson.fromJson(jsonArray.get(i), Template.class);
                    templates.add(item);
                }
            }
        }
        return templates;
    }

    private ArrayList<Background> parserBackgrounds(JsonObject object) throws JsonSyntaxException {
        ArrayList<Background> backgrounds = new ArrayList<>();
        if (object.has("lst_bg")) {
            JsonArray array = object.getAsJsonArray("lst_bg");
            if (array != null && array.size() > 0) {
                int lengthArray = array.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    Background background = gson.fromJson(array.get(i), Background.class);
                    backgrounds.add(background);
                }
            }
        }
        return backgrounds;
    }

    private ArrayList<Song> parserMusics(JsonObject object) throws JsonSyntaxException {
        ArrayList<Song> mediaModels = new ArrayList<>();
        if (object.has("lst_song")) {
            JsonArray array = object.getAsJsonArray("lst_song");
            if (array != null && array.size() > 0) {
                int lengthArray = array.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    Song song = gson.fromJson(array.get(i), Song.class);
                    mediaModels.add(song);
                }
            }
        }
        return mediaModels;
    }

    private ArrayList<Book> parserBooks(JsonObject object) throws JsonSyntaxException {
        ArrayList<Book> books = new ArrayList<>();
        if (object.has("lst_result")) {
            JsonArray array = object.getAsJsonArray("lst_result");
            if (array != null && array.size() > 0) {
                int lengthArray = array.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    Book book = gson.fromJson(array.get(i), Book.class);
                    books.add(book);
                }
            }
        }
        return books;
    }

    private ArrayList<Page> parserPagers(JsonArray jsonArray) throws JsonSyntaxException {
        ArrayList<Page> pages = new ArrayList<>();
        if (jsonArray != null && jsonArray.size() > 0) {
            int lengthArray = jsonArray.size();
            Gson gson = new Gson();
            for (int i = 0; i < lengthArray; i++) {
                Page item = gson.fromJson(jsonArray.get(i), Page.class);
                pages.add(item);
            }
        }
        return pages;
    }

    private ArrayList<BookVote> parserBookVotes(JsonObject object) throws JsonSyntaxException {
        ArrayList<BookVote> books = new ArrayList<>();
        if (object.has("lstVote")) {
            JsonArray array = object.getAsJsonArray("lstVote");
            if (array != null && array.size() > 0) {
                int lengthArray = array.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    BookVote book = gson.fromJson(array.get(i), BookVote.class);
                    books.add(book);
                }
            }
        }
        return books;
    }

    public void requestSavePage(Page page, final ProcessBookListener listener) {
        ArrayList<Page> listPage = new ArrayList<>();
        listPage.add(page);
        Book book = new Book();
        book.setPages(listPage);
        String jsonInfo = new Gson().toJson(book);
        Log.d(TAG, "requestSavePage info: " + jsonInfo);
        requestSave(jsonInfo, page.getBookId(), Constants.GUEST_BOOK_SAVE.UPDATE_PAGE, listener);
    }

    public void requestSaveNewBook(Book book, final ProcessBookListener listener) {
        //String jsonInfo=book.
        String jsonInfo = new Gson().toJson(book);
        Log.d(TAG, "requestSaveNewBook info: " + jsonInfo);
        requestSave(jsonInfo, book.getId(), Constants.GUEST_BOOK_SAVE.UPDATE_BOOK, listener);
    }

    public void requestSaveBookInfo(String bookId, final int status, final String bookName,
                                    final String bookClass, final Song song, final int statePublic,
                                    @NonNull final ProcessBookListener listener) {
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        Book book = new Book();
        book.setBookName(bookName);
        book.setBookClass(bookClass);
        book.setId(bookId);
        book.setOwner(accountBusiness.getJidNumber());
        book.setPages(new ArrayList<Page>());
        book.setSong(song);
        book.setPublicState(statePublic);
        book.setStatus(status);

        String jsonInfo = new Gson().toJson(book);
        Log.d(TAG, "requestSaveBookInfo info: " + jsonInfo);
        requestSave(jsonInfo, bookId, Constants.GUEST_BOOK_SAVE.UPDATE_BOOK_INFO, new ProcessBookListener() {
            @Override
            public void onSuccess(String desc) {
                if (currentBookEditor != null) {// update state in current book
                    currentBookEditor.setStatus(status);
                    currentBookEditor.setBookName(bookName);
                    currentBookEditor.setBookClass(bookClass);
                    currentBookEditor.setPublicState(statePublic);
                    currentBookEditor.setSong(song);
                }
                listener.onSuccess(desc);
            }

            @Override
            public void onError(String desc) {
                listener.onError(desc);
            }
        });
    }

    public Book createNewBook(TemplateDetail templateDetail) {
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        String bookId = makeBookId(accountBusiness.getJidNumber());
        String bookName = String.format(mRes.getString(R.string.guest_book_default_name),
                accountBusiness.getUserName());
        int i = 1;
        for (Page page : templateDetail.getPages()) {
            page.setBookId(bookId);
            page.setPage(i);
            i++;
        }
        Book book = new Book();
        book.setBookName(bookName);
        book.setBookClass("");
        book.setId(bookId);
        book.setPublicState(1);
        book.setLockState(0);
        book.setOwner(accountBusiness.getJidNumber());
        book.setPages(templateDetail.getPages());
        book.setSong(templateDetail.getSong());
        return book;
    }

    /*public void testUpload(final File file) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                requestUploadFile(file, file.getTitle());
            }
        });
        thread.start();
    }*/

    private String makeBookId(String jidNumber) {
        return "01_" + jidNumber + "_" + TimeHelper.getCurrentTime();
    }

    public interface GetBooksListener {
        void onSuccess(ArrayList<Book> books);

        void onError(int error);
    }

    public interface GetDataListener {
        void onSuccess();

        void onError(int error);
    }

    public interface GetTemplateDetailListener {
        void onSuccess(TemplateDetail templateDetail);

        void onError(int error);
    }

    public interface UpdateStateListener {
        void onSuccess();

        void onError(int error);
    }

    public interface ProcessBookListener {
        void onSuccess(String desc);

        void onError(String error);
    }

    public interface GetBookDetailListener {
        void onSuccess(Book bookDetail);

        void onError(int error);
    }

    public interface GetListVoteListener {
        void onSuccess(ArrayList<BookVote> bookVotes);

        void onError(int error);
    }

    public interface GetVoteDetailListener {
        void onSuccess(int totalVote, int isVote);

        void onError(int error);
    }
}
package com.metfone.selfcare.helper.images;

import com.metfone.selfcare.helper.Config;

import java.io.File;

public final class BaseAlbumDirFactory extends AlbumStorageDirFactory {

    // Standard storage location for digital camera files
    @Override
    public File getAlbumStorageDir() {
        return new File(
                Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.IMAGE_FOLDER);
    }

    @Override
    public File getAlbumVideoStorageDir() {
        return new File(
                Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.VIDEO_FOLDER);
    }
}

package com.metfone.selfcare.helper;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by thaodv on 6/13/2014.
 */
public interface Constants {
    /**
     * danh sách các nhà sản xuất hoặc các thiết bị đặc biệt.
     * Các thiết bị này thường xảy ra các lỗi đặc thù như không nhận thông báo, hay lỗi không play được video
     */
    public static final String WHITE_LIST = "INFINIX|VIETTEL|W505|YFND|XCCESS|LEIMIN|MEIZU|MICROMAX|OUKITEL|MASSTEL|HOTWAV|ALPS|ASUS|GRETEL|CETRIX|ASANZO|LENOVO|F3116|SM-G530H|F3216|XIAOMI|OPPO|VIVO|HUAWEI|MOBIISTAR|MOBISTAR|WIKO|CPH1609|MOBELL|ITEL|NOVA|E5663|COOLPAD|S-NICE S|TA-1032|LOTUS_VIVAS-LOTUS_S3_LTE|Q-mobile|GT-I8552|weixin|E5653|LEAGOO|G3116|SM-J320H|YuLong|Coolpad|iPhone|HOTWAV|FPT X58|GIGI|SM-T116NU|GIONEE|AllCall|Arbustus|ASTON|BAVAPEN|blackberry|Blackview|BLUBOO|Cherry|CutePad|DOOGEE|DOOV|Dream|Elephone|EPIC|FPT|G9|HOMTOM|HTC_D10i|HTC Desire 10 pro|HTC_M9pw|HTC_M9px|HTC One A9s|HTC One X10|HTC U Play|HTC_X10u|HTC_X9u|HYUNDAI|I7|InFocus|Intex|JTY|Karbonn|lanin|LeEco|MediaTek|MTK|Philips S327|R7|roco|STK|Summer|TCL|TECNO|Telego|ulefone|UMIDIGI|W2|ZOPO|ZTE|TA-1021|";

    public static final String OPERATOR_VIETTEL = "04";
    public static final int MAX_IMAGE_PROFILE = 5;
    public static final boolean STICKY_DRAGABLE = false;
    public static final long TOKEN_EXPIRE_TIME = 5 * 60; //5 minutes
    public static final int CHANGE_BANNER_TIME = 6000;
    public static final int MAX_IMAGE_CHOSEN = 10;
    public static final String FILE_PATH = "FILE_PATH";
    public static final String XXTEA_KEY = "Mocha@#$2017";
    public static final String MF_PHONE = "MF_PHONE";
    //Constants Keeng
    public static final boolean API_MUSIC_TEST = false;
    public static final String KEENG_PRE_APPLICATION = "Keeng_VN";
    public static final String DOMAIN_IMAGE_SEARCH_DEFAULT = "http://vip.img.cdn.keeng.vn/";
    public static final String DOMAIN_MOCHA_DEFAULT = "http://hlvip2.mocha.com.vn:80";

    /*public static final class REGISTER_VIP {
        public static final String ID_REGISTER_CONTACT = "APPMC";
        public static final String ID_UN_REGISTER_VIP = "app_huymc";
    }*/
    public static final String DOMAIN_REST_DEFAULT = "http://vip.service.keeng.vn:8080";
    public static final String DOMAIN_SEARCH_DEFAULT = "http://vip.rec.keeng.vn:8084";
    public static final String DOMAIN_TEST = "http://vip.service.keeng.vn";
    public static final int TYPE_FAKE_SOMETHING = 0;
    public static final int TYPE_SONG = 1;
    public static final int TYPE_SONG_UPLOAD = 415;
    public static final int TYPE_VIDEO_UPLOAD = 416;
    public static final int TYPE_SONG_OFFLINE = 11;
    public static final int TYPE_ALBUM = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_SINGER = 4;
    public static final int TYPE_AUTHOR = 5;
    public static final int TYPE_KEENG_LIVE_TV = 401;
    public static final int TYPE_NOTIFICATION = 6;
    public static final int TYPE_KARAOKE_OFFLINE = 7;
    public static final int TYPE_VIDEO_OFFLINE = 8;
    public static final int TYPE_HISTORY = 9;
    //public static final int TYPE_KARAOKE = 10;
    public static final int TYPE_CHANGE_AVATAR = 25;
    public static final int TYPE_POST_BLAST = 26;
    public static final int TYPE_PLAYLIST = 20;
    public static final int TYPE_MOVIE = 909;
    public static final int TYPE_DEEPLINK = 5;
    public static final int TYPE_IMAGE = 36;
    //public static final int TYPE_RADIO = 22;
    public static final int TYPE_TOPIC = 23;
    public static final int TYPE_TOP_HIT = 27;
    //int TYPE_REPORT = 600;
    public static final int TYPE_SONG_BXH_VN = 50;
    public static final int TYPE_SONG_BXH_CA = 51;
    public static final int TYPE_SONG_BXH_AM = 52;
    public static final int TYPE_VIDEO_BXH_VN = 53;
    public static final int TYPE_VIDEO_BXH_CA = 54;
    public static final int TYPE_VIDEO_BXH_AM = 55;
    public static final int TYPE_YOUTUBE = 911;
    public static final int TYPE_PLAYLIST_VIDEO = 100;
    public static final int TYPE_ALBUM_VIDEO = 101;
    public static final int TIME_DELAY_RETRY = 3000;
    public static final int TIME_DELAY_LOAD_DATA_FRAGMENT = 300;
    public static final int TAB_HOME_MUSIC = 1;
    public static final int TAB_VIDEO_HOT = 159;
    public static final int TAB_ALBUM_HOT = 160;
    public static final int TAB_SONG_HOT = 161;
    public static final int TAB_TOPIC_HOT = 162;
    public static final int TAB_PLAYLIST_HOT = 172;
    public static final int TAB_SINGER_HOT = 173;
    public static final int TAB_TOP_HIT = 191;
    public static final int TAB_YOUTUBE_HOT = 205;
    public static final int TAB_ALBUM_DETAIL = 171;
    public static final int TAB_CATEGORY_DETAIL = 103;
    public static final int TAB_PLAYLIST_DETAIL = 117;
    public static final int TAB_SINGLE_DETAIL = 118;
    public static final int TAB_TOPIC_DETAIL = 141;
    public static final int TAB_SEARCH_SUGGEST = 142;
    public static final int TAB_SEARCH_SUBMIT = 143;
    public static final int TAB_RANK_DETAIL = 168;
    public static final int TAB_TOP_HIT_DETAIL = 192;
    public static final int TAB_SEARCH_MOVIES = 219;
    public static final int TAB_SEARCH_MUSIC = 220;
    public static final int TAB_SECURITY_FIREWALL_SETTING = 221;
    public static final int TAB_SECURITY_FIREWALL_IGNORE = 222;
    public static final int TAB_SECURITY_FIREWALL_HISTORY = 223;
    public static final int TAB_SECURITY_SPAM_SMS = 224;
    public static final int TAB_SECURITY_SPAM_BLOCK = 225;
    public static final int TAB_SECURITY_SPAM_IGNORE = 226;
    public static final int TAB_SECURITY_CENTER_DETAIL = 227;
    public static final int TAB_SECURITY_VULNERABILITY = 228;
    public static final int TAB_SECURITY_CENTER = 229;
    public static final int TAB_SECURITY_SPAM = 230;
    public static final int TAB_SECURITY_FIREWALL = 231;
    public static final int TAB_SECURITY_NEWS = 232;
    public static final int TAB_SECURITY_SPAM_SMS_DETAIL = 233;
    public static final int TAB_SECURITY_FIREWALL_SMS_DETAIL = 234;
    public static final int TAB_SUBSCRIBE_CHANNEL = 235;
    public static final int TAB_CHANNEL_MANAGEMENT = 236;
    public static final int TAB_TOP_MONEY_UPLOAD = 237;
    public static final int TAB_VIDEO_HOME = 238;
    public static final int TAB_MOVIE_HOME = 239;
    public static final int TAB_NEWS_HOME = 240;
    public static final int TAB_TIIN_HOME = 268;
    public static final int TAB_MUSIC_HOME = 241;
    public static final int TAB_REWARD = 2419;
    public static final int TAB_THREAD_CHAT_HOME = 242;
    public static final int TAB_THREAD_CALL_HOME = 243;
    public static final int TAB_CONTACT_HOME = 244;
    public static final int TAB_GROUP_CHAT_HOME = 245;
    public static final int TAB_HOME = 246;
    public static final int TAB_MOBILE_HOME = 247;
    public static final int TAB_SEARCH_ALL = 248;

    public static final int MUSIC_HOME_TYPE_FLASH_HOT = 1;
    public static final int MUSIC_HOME_TYPE_MIX = 2;
    public static final int MUSIC_HOME_TYPE_MV_HOT = 3;
    public static final int MUSIC_HOME_TYPE_KEENG_TV = 4;
    public static final int MUSIC_HOME_TYPE_MV_YOUTUBE = 5;
    public static final int MUSIC_HOME_TYPE_PLAYLIST_HOT = 6;
    public static final int MUSIC_HOME_TYPE_ALBUM_HOT = 7;
    public static final int MUSIC_HOME_TYPE_TOP_HIT = 8;
    public static final int MUSIC_HOME_TYPE_CHART = 9;
    public static final int MUSIC_HOME_TYPE_TOPIC = 10;
    public static final int MUSIC_HOME_TYPE_SONG_HOT = 11;
    public static final int MUSIC_HOME_TYPE_SINGER_HOT = 12;
    public static final int MUSIC_HOME_TYPE_BANNER = -1;
    public static final int CLIENT_ANDROID = 1;
    public static final float ROUNDED_CORNERS = 5;
    public static final int NO_MIN_ITEMS = 10;
    public static final int NO_LISTENED = 100000;
    public static final int MAX_LISTEN_NO_DELETE_PLAYLIST = 100;
    public static final String KEENG_LOCAL_CODE = "84";
    public static final String KEY_TYPE = "TYPE";
    public static final String KEY_DATA = "DATA";
    public static final String KEY_TITLE = "TITLE";
    public static final String KEY_SEARCH = "KEY_SEARCH";
    public static final String KEY_START_DATE = "START_DATE";
    public static final String KEY_END_DATE = "END_DATE";
    public static final String KEY_ITEM_REWARD = "KEY_ITEM_REWARD";
    public static final String KEY_BUNDLE_REWARD = "KEY_BUNDLE_REWARD";
    public static final String KEY_POSITION = "POSITION";
    public static final String KEY_DATA_SEARCH_MUSIC = "KEY_SEARCH";
    public static final String KEY_FLOATING_VIEW_POS_X = "KEY_FLOATING_VIEW_POS_X";
    public static final String KEY_FLOATING_VIEW_POS_Y = "KEY_FLOATING_VIEW_POS_Y";
    public static final String MOVIE_TYPE_TVOD_DRM = "tvod_drm";
    public static final String MOVIE_TYPE_TVOD_ODD = "tvod_odd";
    public static final String MOVIE_TYPE_NORMAL = "normal";
    public static final String MOVIE_TYPE_BIGSIX = "bigsix";

    public static final float RATIO_POSTER_MOVIE = 0.67f;
    public static final float RATIO_COVER_MOVIE = 1.78f;

    public static final String KEY_REGISTER_E2E = "KEY_REGISTER_E2E";
    public static final String KEY_SIGNAL_STORE = "KEY_SIGNAL_STORE";

    public static final String PARAM_IS_LOGIN = "is_login";

    public static final double INVALID_LAT = 11.562108;
    public static final double INVALID_LNG = 104.888535;
    public static final String PROVINCE_ID = "PNP";
    public static final float MAP_ZOOM = 15.5f;
    public static final int CACHE_EXPIRATION = 24 * 60 * 60 * 1000;// 24h
    public static final String KEY_FINGER_PHONE = "KEY_FINGER_PHONE";
    public static final String KEY_FINGER_ENCODE_PW = "KEY_FINGER_ENCODE_PW";
    public static final String KEY_IS_NOT_FIRST_OPENNING = "KEY_IS_NOT_FIRST_OPENNING";
    public static final String KEY_IS_FIRST_OPEN_APP = "KEY_IS_NOT_ADD_CATEGORY";
    public static final String KEY_IS_FIRST_TAB_HOME = "KEY_IS_FIRST_TAB_HOME";
    public static final String KEY_IS_FIRST_TAB_CINEMA = "KEY_IS_FIRST_TAB_CINEMA";
    public static final int LIMIT_LOAD_HOME = 10;
    public static final boolean LOG_CONTENT_ENABLE = true;
    // main ads tab_type: videos, social, news, movies, dating, music
    public static final String MAIN_TAB_VIDEOS = "videos";
    public static final String MAIN_TAB_SOCIAL = "social";
    public static final String MAIN_TAB_NEWS = "news";
    public static final String MAIN_TAB_MOVIES = "movies";
    public static final String MAIN_TAB_DATING = "dating";
    public static final String MAIN_TAB_MUSIC = "music";
    public static final String MAIN_TAB_TIIN = "tiin";
    public static final String MAIN_TAB_WAP = "wap";
    public static final int[] COLOR_EPISODE = {0xFF653FD5, Color.TRANSPARENT, Color.WHITE, Color.YELLOW};
    public static final int[] COLOR_SUBTITLE = {0xFFFF7F00, Color.TRANSPARENT, Color.WHITE, Color.YELLOW};
    public static final int[] COLOR_NARRATIVE = {0xFF5297FE, Color.TRANSPARENT, Color.WHITE, Color.YELLOW};
    public static final int[] COLOR_FULL_SET = {0xFFFF7F00, Color.TRANSPARENT, Color.WHITE, Color.YELLOW};
    public static final String SRC_SCREEN_ID = "src_screen_id";

    public enum IndexItem {
        // index at tab profile
        ALBUM_IMAGE, HISTORY, FAVOURITE, OFFLINE, PLAYLIST, SONG_UPLOAD, REGISTER_VIP, FRIEND_FOLLOW, COLLECTION,
        // index on menu
        ALL, FILTER, SONG, VIDEO, ALBUM, MORE, NOTIFICATION, KEENG, MUSIC_FEELING, CATEGORY, FEEDBACK,
        INTRODUCTION, REGULATION, HELP, SETTING, SIGN_OUT, SINGER_HOT, SINGER_A_Z, RATE_APP, ACCUMULATE,
        // follow friend
        FRIEND_KEENG, FRIEND_CONTACT, FAMILIAR, SEND_MESSAGE, CLICK_BACK, PROMOTION, LISTEN_TOGETHER,
    }

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    public static final class VOLLEY {
        public static final int VOLLEY_TIMEOUT = 30000;//30s khong sua lai thong so nay
        public static final int VOLLEY_MAX_NUMBER_RETRY = 2;
        public static final int VOLLEY_BACK_OFF_MULTIPLER = 2;
    }

    public static final class CONNECTION_STATE {
        public static final int NOT_CONNECT = -1;
        public static final int CONNECTING = 1;
        public static final int CONNECTED = 2;
    }

    public static final class XMPP {
        public static final String XMPP_RESOUCE = "@reeng/reeng";
        public static final String XMPP_DOMAIN = "reeng";
        public static final String XMPP_SMS = "sms";
        public static final String XMPP_GROUP_RESOUCE = "@muc.reeng/reeng";
        public static final String XMPP_OFFICAL_RESOUCE = "@offical.reeng/reeng";
        public static final String XMPP_ROOM_RESOUCE = "@room.reeng/reeng";
        public static final String XMPP_BROADCAST_RESOUCE = "@broadcast.reeng/reeng";
    }

    public static final class LOCATION {
        public static final Locale DEFAULE_LOCATION = new Locale("vi");
        public static final String DATA_INPUT_TYPE = "data_location_input_type";
        public static final int TYPE_SEND = 1;
        public static final int TYPE_RECEIVED = 2;
        public static final String DATA_ADDRESS = "data_location_address";
        public static final String DATA_LATITUDE = "data_location_latitude";
        public static final String DATA_LONGITUDE = "data_location_longitude";
    }

    public static final class PERMISSION {
        public static final int PERMISSION_REQUEST_ALL = 1;
        public static final int PERMISSION_REQUEST_LOCATION = 2;
        public static final int PERMISSION_REQUEST_RECORD_AUDIO = 3;
        public static final int PERMISSION_REQUEST_TAKE_PHOTO = 4;
        public static final int PERMISSION_REQUEST_TAKE_VIDEO = 5;
        public static final int PERMISSION_REQUEST_EDIT_CONTACT = 6;
        public static final int PERMISSION_REQUEST_SAVE_CONTACT = 7;
        public static final int PERMISSION_REQUEST_DELETE_CONTACT = 8;
        public static final int PERMISSION_REQUEST_WRITE_STORAGE = 9;
        public static final int PERMISSION_REQUEST_WRITE_CONTACT = 10;
        public static final int PERMISSION_REQUEST_TAKE_PHOTO_GROUP_AVATAR = 14;
        public static final int PERMISSION_REQUEST_GALLERY = 15;
        public static final int PERMISSION_REQUEST_FILE = 16;
        public static final int PERMISSION_REQUEST_FILE_DOWNLOAD = 17;

        public static final int PERMISSION_REQUEST_RECEIVE_MESSAGE = 18;
        public static final int PERMISSION_REQUEST_PHONE = 19;

        public static final int PERMISSION_REQUEST_TAKE_PHOTO_AND_STORAGE = 100;
        public static final int PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO = 101;
        public static final int PERMISSION_REQUEST_STORAGE_AND_CONTACT = 102;
        public static final int PERMISSION_REQUEST_MOVIE_DRM = 103;

        public static final int PERMISSION_CONTACT = 104;
        public static final int PERMISSION_STORAGE = 105;
        public static final int PERMISSION_MICRO = 106;
        public static final int PERMISSION_READ_EXTERNAL_STORAGE = 107;

    }

    public static final class CONTACT {
        public static final int STATUS_LIMIT = 300;
        public static final int USER_NAME_MAX_LENGTH = 40;
        public static final int NONE = 0;
        public static final int ACTIVE = 1;
        public static final int DEACTIVE = 2;
        public static final int SYSTEM_BLOCK = 4;

        public static final int GROUP_NONE = 0;
        public static final int GROUP_FAVORITE = 1;
        public static final int GENDER_MALE = 1;    // nam
        public static final int GENDER_FEMALE = 0;  // nu
        public static final int CONTACT_VIEW_NOMAL = 1;
        public static final int CONTACT_VIEW_CHECKBOX = 2;
        public static final int CONTACT_VIEW_ICON_BLOCK = 3;
        public static final int CONTACT_VIEW_SHARE = 4;
        public static final int CONTACT_VIEW_THREAD_MESSAGE = 5;
        public static final int CONTACT_VIEW_FORWARD_MESSAGE = 6;
        public static final int CONTACT_VIEW_MEMBER_GROUP = 7;
        public static final int CONTACT_VIEW_AVATAR_AND_NAME = 8;
        //public static final int COLOR_AVATAR_SIZE = 20;
        public static final int NUM_COLORS_AVATAR_DEFAULT = 10;
        public static final int MIN_LENGTH_NUMBER = 1;
        // group,stranger,officer list
        public static final String TYPE_LIST = "display_type_list";
        public static final int SHOW_LIST_GROUP = 1;
        public static final int FRAG_LIST_UTIL = 3;
        public static final int FRAG_LIST_SOCIAL_REQUEST = 4;
        public static final int FRAG_LIST_CONTACT_WITH_ACTION = 5;
        public static final int FRAG_LIST_FRIEND_MOCHA = 6;
        public static final String DATA_FRAGMENT = "data_frag";
        //stranger
        public static final String STRANGER_MUSIC_ID = "music_stranger";
        public static final String STRANGER_CONFIDE_ID = "talk_stranger";
        public static final String STRANGER_MOCHA_ID = "mocha_stranger";
        public static final int MAX_MEMBER_GROUP_SHOW = 5;
        //follow
        public static final int FOLLOW_STATE_UNKNOW = -2;//chua lay dc state
        public static final int FOLLOW_STATE_NONE = 0;  // khong co quan he
        public static final int FOLLOW_STATE_FOLLOWED = 1;//dang follow B
        public static final int FOLLOW_STATE_BE_FOLLOWED = 2;// B follow minh
        public static final int FOLLOW_STATE_FRIEND = 3;// friend

        public static final int SOCIAL_SOURCE_MOCHA = 0;
        public static final int SOCIAL_SOURCE_NON = 1;

        public static final int DEFAULT_ALPHA_ACTIONBAR = 0;
    }

    public static final class MESSAGE {
        public static final int MESSAGE_LIMIT = 20, MESSAGE_INCRE_SIZE = 10;
        public static final int VOICEMAIL_MAX_LENGTH = 2 * 60 * 1000;
        public final static int INCALL_NOTIF_ID = 7472;
        public static final int MAX_TRANSFER_PHOTO_SIZE = 300 * 1024;
        public static final int TEXT_IP_MAX_LENGTH = 3000;
        public static final int TEXT_IP_MAX_LENGTH_CAM = 6000;
        public static final int TEXT_GSM_MAX_LENGTH = 960;
        public static final int TEXT_ROOM_CHAT_MAX_LENGTH = 250;
        public static final int FRIEND_NAME_INBOX_MAX_LENGTH = 12;
        public static final int GROUP_NAME_MAX_LENGTH = 30;
        public static final int CHANGE_GROUP_NAME = 200;
        public static final int CHANGE_GROUP_MEMBER = 201;
        public static final int CHANGE_GROUP_PRIVATE = 202;
        public static final int CHANGE_GROUP_AVATAR = 203;
        public static final int MAX_FILE_NAME = 100;
        public static final int IMAGE_LIMIT = 10;
        public static final String NUMBER = "number_new_user";
        public static final String NUMBER_FROM_OTHER_APP = "number_from_other_app";

        public static final int HOLDER_THREAD_MESSAGE = 0;
        public static final int HOLDER_THREAD_CONTACT = 1;
        public static final int HOLDER_THREAD_BANNER = 2;
        public static final int HOLDER_THREAD_SECTION = 3;
        public static final int HOLDER_THREAD_MEDIA_BOX = 4;
        public static final int HOLDER_THREAD_ADS = 10;

        public static final int ACTION_REPORT_SPAM = 1;
        public static final int ACTION_REPORT_BADWORD = 2;
        public static final int ACTION_REPORT_CHEAT = 3;

        public static final int TYPE_CREATE_POLL = 1;
        public static final int TYPE_DETAIL_POLL = 2;

        public static final String AB_DESC = "ab_status";
        public static final String TYPE = "data_type";
        public static final String POLL_ITEM_ID = "poll_detail_item_id";
        public static final String POLL_ID = "poll_detail_id";
        public static final String POLL_OPTIONS = "poll_options";
        public static final String POLL_NAME = "poll_name";
        public static final String POLL_UPDATE = "poll_update";

        public static final class CONFIG {
            //message config
            public static final String KEY_CONFIG = "CONFIG";
            public static final String KEY_STICKER = "STICKER";
            public static final String KEY_LISTGAME = "LISTGAME";
            public static final String KEY_REGID = "REGID";
            public static final String KEY_CONTACT = "CONTACT";
            public static final String KEY_DEVICE_INFO = "DEVICEINFO";
            /*public static final String KEY_ENABLE_CALLOUT = "ENABLE_CALLOUT";
            public static final String KEY_DISABLE_CALLOUT = "DISABLE_CALLOUT";*/

            public static final String KEY_ENABLE_UPLOAD = "ENABLE_UPLOG";
            public static final String KEY_DISABLE_UPLOAD = "DISABLE_UPLOG";
            public static final String KEY_UPLOG_KQI = "UPLOG_KQI";

            public static final String KEY_ENABLE_DEBUG = "ENABLE_DEBUG";
            public static final String KEY_DISABLE_DEBUG = "DISABLE_DEBUG";
            public static final String KEY_UPLOG_DEBUG = "UPLOG_DEBUG";

            public static final String KEY_GROUP_CFG = "GROUP_CFG";

            public static final String KEY_CHANGE_NUMBER = "CHANGENUMBER";
            public static final String KEY_GET_BLOCK_LIST = "GET_BLOCK_LIST";
            public static final String KEY_GET_USER_INFO = "GET_USER_INFO";
            public static final String KEY_BACKUP_MSG = "BACKUP_MSG";

            public static final String KEY_RESEND_MSG = "RESEND_MSG";
            public static final String KEY_PRODUCT = "PRODUCT";
            public static final String KEY_DEVELOPMENT = "DEVELOPMENT";

        }
    }

    public static final class SEARCH_MESSAGE {
        public static final String DATA_TYPE = "action_type";
        private static final int TYPE_SEARCH_MESSAGE_BASE = 1000;
        public static final int TYPE_CREATE_SEARCH_MESSAGE = TYPE_SEARCH_MESSAGE_BASE + 1;
    }

    public static final class NOTIFICATION {
        /*public static final int MUSIC_NOTIFICATION_ID = 2000;
        public static final int NOTIFY_MESSAGE = 4000;
        public static final int NOTIFY_ONMEDIA = 5000;
        public static final int NOTIFY_OTHER = 6000;
        public static final int NOTIFY_CALL = 7000;*/
        // set id notification 2^31-2 ----

        public static final int NOTIFY_FAKE_NEW_MSG = 2147483647;
        public static final int MUSIC_NOTIFICATION_ID = 2147483646;
        public static final int NOTIFY_MESSAGE = 2147483645;
        public static final int NOTIFY_ONMEDIA = 2147483644;
        public static final int NOTIFY_OTHER = 2147483643;
        public static final int NOTIFY_CALL = 2147483642;
        public static final int NOTIFY_MOCHA_VIDEO = 2147483641;
        public static final int NOTIFY_DEEPLINK = 2147483640;
        public static final int NOTIFY_REACTION = 2147483639;
        public static final int NOTIFY_HIDDEN_THREAD = 2147483638;
        public static final int NOTIFY_MUSIC = 2147483637;
        public static final int NOTIFY_MOVIE = 2147483636;
        public static final int NOTIFY_NEWS = 2147483635;
        public static final int NOTIFY_CALL_HEAD_UP = 2147483634;
    }

    public static final class NOTIFICATION_CAMID {
        public static final String MOVIE_PROFILE = "movie_profile";
        public static final String MOVIE_PLAY_DIRECTLY = "movie_play_directly";
        public static final String MOVIE_ACTOR = "movie_actor";
        public static final String MOVIE_DIRECTOR = "movie_director";
        public static final String MOVIE_CATEGORY = "movie_category";
        public static final String ESPORT_CHANNEL_PROFILE = "esport_channel_profile";
        public static final String ESPORT_PLAY_DIRECTLY = "esport_play_directly";
        public static final String ESPORT_TOURNAMENT_PROFILE = "esport_tournament_profile";
        public static final String METFONE_SERVICE = "metfone_service";
        public static final String METFONE_TOPUP = "metfone_topup";
        public static final String REWARD_DETAIL = "reward_detail";
        public static final String LINK = "link";
        public static final String LOGIN = "login";
        public static final String SIGNUP = "sign_up";
    }

    public static final class NOTIFICATION_CHANNEL_NAME {
        public static final String NOTIFY_FAKE_NEW_MSG = "Quick Message";
        public static final String MUSIC_NOTIFICATION_ID = "Music";
        public static final String NOTIFY_MESSAGE = "Message";
        //        public static final String NOTIFY_ONMEDIA = "Onmedia";
        public static final String NOTIFY_ONMEDIA = "Social";
        //        public static final String NOTIFY_OTHER = "Other";
        public static final String NOTIFY_OTHER = "Mocha";
        public static final String NOTIFY_CALL = "Call";
        public static final String NOTIFY_HEAD_UP_CALL = "Head Up Call";
        public static final String NOTIFY_MOCHA_VIDEO = "Video";
        public static final String NOTIFY_DEEPLINK = "Deeplink";
    }

    public static final class BANNER {
        public static final int TYPE_WEB_VIEW = 1;
        public static final int TYPE_FEATURES = 2;
        public static final int TYPE_OPEN_APP = 3;
        public static final int TYPE_FAKE_MO = 4;
    }

    public static final class PREFERENCE {
        public static final String PREF_FIRST_OPEN = "PREF_FIRST_OPEN";
        public static final String PREF_FIRST_OPEN_CAM = "PREF_FIRST_OPEN_CAM";
        public static final String PREF_DIR_NAME = "com.viettel.reeng.app";
        public static final String PREF_SPEAKER = "pref_speaker";
        public static final String PREF_APP_VERSION = "pref_app_version";// luu version code cu
        public static final String PREF_CLIENT_INFO_CODE_VERSION = "PREF_CLIENT_INFO_CODE_VERSION";
        public static final String PREF_CLIENT_INFO_DEVICE_LANGUAGE = "PREF_CLIENT_INFO_DEVICE_LANGUAGE";
        public static final String PREF_NO_NOTIF_LIST_ID = "pref_no_notif_list_id";
        public static final String PREF_THREAD_SMSOUT_LIST_ID = "PREF_THREAD_SMSOUT_LIST_ID";
        public static final String PREF_CONTACT_LIST_REMOVE_FAIL = "pref_contact_list_remove_fail";
        public static final String PREF_AVATAR_FILE_CAPTURE = "avatar_capture";
        public static final String PREF_IMAGE_FILE_CAPTURE = "image_capture";
        public static final String PREF_AVATAR_FILE_CROP = "avatar_crop";
        public static final String PREF_AVATAR_GROUP_FILE_CROP = "avatar_group_crop";
        public static final String PREF_IMAGE_FILE_CROP = "image_crop";
        public static final String PREF_AVATAR_FILE_FACEBOOK = "avatar_facebook";
        public static final String PREF_AVATAR_FILE_GROUP = "avatar_group_";
        public static final String PREF_CHAT_IMAGE_PATH = "pref_chat_image_path";
        public static final String PREF_AVATAR_GROUP_IMAGE_PATH = "pref_avatar_group_image_path";
        public static final String PREF_STRANGER_SHOW_ALERTS = "pref_stranger_show_alerts";
        public static final String PREF_GET_LIST_BLOCK = "pref_get_list_block";

        public static final String PREF_EXCHANGE_IMAGE_FILE_CAPTURE = "exchange_image_capture";
        //for module setting
        public static final String PREF_SETTING_VIBRATE = "pref_vibrate";//true
        public static final String PREF_SETTING_RINGTONE = "pref_ringtone";//true
        public static final String PREF_SETTING_MSG_IN_POPUP = "pref_msg_in_popup";//hien thi msg tren popup; true
        public static final String PREF_SETTING_ENABLE_SEEN = "pref_enable_seen";//true
        public static final String PREF_SETTING_ENABLE_UNLOCK = "pref_enable_unlock";//true
        public static final String PREF_SETTING_NOTIFY_NEW_FRIEND = "pref_setting_notify_new_friend"; //true
        public static final String PREF_SETTING_IMAGE_HD = "pref_setting_image_hd"; //false
        public static final String PREF_SETTING_OFF_MUSIC_IN_ROOM = "pref_setting_off_music"; //false
        public static final String PREF_SETTING_SYSTEM_SOUND = "pref_setting_system_sound";
        public static final String PREF_SETTING_NOTIFICATION_SOUND = "pref_setting_notification_sound";
        public static final String PREF_SETTING_NOTIFICATION_SOUND_DEFAULT = "pref_setting_notification_sound_default";
        public static final String PREF_SETTING_AUTO_SMSOUT = "pref_setting_auto_smsout";
        public static final String PREF_SETTING_RECEIVE_SMSOUT = "pref_setting_receive_smsout";
        public static final String PREF_SETTING_BIRTHDAY_REMINDER = "pref_birthday_reminder";//true
        public static final String PREF_SETTING_FONT_SIZE = "pref_setting_font_size";
        public static final String PREF_SETTING_ON_NOTI = "pref_setting_on_noti";
        public static final String PREF_SETTING_ON_NOTI_UTIL_TIME = "pref_setting_noti_until_time";

        // TODO: [START] Cambodia version
        public static final String PREF_SETTING_WATCHING_CINEMA_ACTIVITY_ENABLE = "pref_setting_watching_cinema_activity";
        // TODO: [END] Cambodia version

        // for prevate setting
        public static final String PREF_SETTING_PREVIEW_MSG = "pref_preview_msg";//xem trc noi dung tin nhan: true
        //
        public static final String PREF_DOMAIN_FILE = "pref_domain_file";
        public static final String PREF_DOMAIN_MSG = "pref_domain_message";
        public static final String PREF_DOMAIN_ON_MEDIA = "pref_domain_on_media";
        public static final String PREF_DOMAIN_IMAGE = "pref_domain_image";
        //
        public static final String PREF_AUTO_PLAY_STICKER = "PREF_AUTO_PLAY_STICKER";
        public static final String PREF_SHOW_MEDIA = "PREF_SHOW_MEDIA";
        public static final String PREF_REPLY_SMS = "PREF_REPLY_SMS";
        // keybroad
        public static final String PREF_KEYBOARD_HEIGHT = "pref_keyboard_height";
        public static final String PREF_KEYBOARD_OFFSET_HEIGHT = "pref_keyboard_offset_height";
        public static final String PREF_TAPLET_LANGSCAPE_KEYBOARD_OFFSET_HEIGHT =
                "pref_taplet_langscape_keyboard_offset_height";
        // music business
        public static final String PREF_RECEIVE_MUSIC_DIFFERENCE_TIME = "pref_receive_music_difference_time";
        public static final String PREF_SEND_MUSIC_LAST_THREAD_ID = "pref_send_music_last_thread_id";
        //niem vui lan toa
        public static final String PREF_SHOW_BUZZ_COUNT = "pref_show_buzz_count";
        //sticker store
        public static final String PREF_SENT_DEVICE_ID = "PREF_SENT_DEVICE_ID";
        public static final String PREF_PUBLIC_RSA_KEY = "PREF_PUBLIC_RSA_KEY";
        public static final String PREF_STICKER_NEW_FROM_SERVER = "new_stickers_from_server_v2";
        //More apps response
        public static final String PREF_MOREAPP_RESPONSE = "PREF_RESPONSE_MOREAPPS";
        //game list response
//        public static final String PREF_LISTGAME_RESPONSE = "PREF_LISTGAME_RESPONSE";
        //default back ground
        public static final String PREF_DEFAULT_BACKGROUND_PATH = "PREF_DEFAULT_BG_PATH";
        public static final String PREF_APPLY_BACKGROUND_ALL = "PREF_APPLY_BACKGROUND_ALL";
        //language translate
        public static final String PREF_LANGUAGE_TRANSLATE_SELECTED = "PREF_LANGUAGE_TRANSLATE_SELECTED";
        public static final String PREF_CONFIG_BANNER_CONTENT = "PREF_CONFIG_BANNER_CONTENT_V2";
        //        public static final String PREF_CONFIG_BANNER_CALL = "PREF_CONFIG_BANNER_CALL";
        //pref onmedia
        public static final String PREF_ONMEDIA_CHAT_WITH_STRANGER = "PREF_ONMEDIA_CHAT_WITH_STRANGER";
        public static final String PREF_ONMEDIA_NOTIFY_NEW_FEED = "PREF_ONMEDIA_NOTIFY_NEW_FEED";
        public static final String PREF_ONMEDIA_SECURE_WEB = "PREF_ONMEDIA_SECURE_WEB";
        //        public static final String PREF_ONMEDIA_SHARE_FACEBOOK = "PREF_ONMEDIA_SHARE_FACEBOOK";
        public static final String PREF_ONMEDIA_LAST_TIME_SHOW_HOROSCOPE = "PREF_ONMEDIA_LAST_TIME_SHOW_HOROSCOPE";
        //last tab
//        public static final String PREF_HOME_LAST_TAB = "PREF_HOME_LAST_TAB";
        public static final String PREF_LOCK_SPAM_ROOM_CHAT = "PREF_LOCK_SPAM_ROOM_CHAT";
        //
//        public static final String PREF_DOMAIN_DOMAIN_FILE_TEST = "PREF_DOMAIN_DOMAIN_FILE";
//        public static final String PREF_DOMAIN_DOMAIN_MSG_TEST = "PREF_DOMAIN_DOMAIN_MSG";
//        public static final String PREF_DOMAIN_DOMAIN_ONMEDIA_TEST = "PREF_DOMAIN_DOMAIN_ONMEDIA";
        public static final String PREF_SETTING_SETUP_KEYBOARD_SEND = "PREF_SETTING_SETUP_KEYBOARD_SEND";//default true
        public static final String PREF_SETTING_LANGUAGE_MOCHA = "PREF_SETTING_LANGUAGE_MOCHA";//default true
        public static final String PREF_MOCHA_USER_VIP_INFO = "PREF_MOCHA_USER_VIP_INFO";//default 0
        public static final String PREF_MOCHA_USER_CBNV = "PREF_MOCHA_USER_CBNV";//default false
        public static final String PREF_MOCHA_ENABLE_CALL = "PREF_MOCHA_ENABLE_CALL";//default -1
        public static final String PREF_MOCHA_ENABLE_SMS_IN = "PREF_MOCHA_ENABLE_SMS_IN";//default -1
        public static final String PREF_MOCHA_ENABLE_SSL = "PREF_MOCHA_ENABLE_SSL";//default -1
        public static final String PREF_KEENG_USER_ID = "PREF_KEENG_USER_ID";
        public static final String PREF_MOCHA_ENABLE_AVNO = "PREF_MOCHA_ENABLE_AVNO";
        public static final String PREF_MOCHA_ENABLE_TAB_CALL = "PREF_MOCHA_ENABLE_TAB_CALL";
        public static final String PREF_MOCHA_EMULATOR_CAM = "PREF_MOCHA_EMULATOR_CAM";
        public static final String PREF_MOCHA_ENABLE_INTERNATIONAL = "PREF_MOCHA_ENABLE_INTERNATIONAL";
        public static final String PREF_MOCHA_OPERATOR = "PREF_MOCHA_OPERATOR";
        public static final String PREF_LIST_GAME_IQ_PLAYED = "PREF_LIST_GAME_IQ_PLAYED";
        public static final String PREF_TIME_CALLOUT_REMAIN = "PREF_TIME_CALLOUT_REMAIN";
        public static final String PREF_MOCHA_ENABLE_CHANGE_NUMBER = "PREF_MOCHA_ENABLE_CHANGE_NUMBER";
        public static final String PREF_DONT_SHOW_TIP_CUSTOMIZE_TAB = "PREF_DONT_SHOW_TIP_CUSTOMIZE_TAB";
        public static final String PREF_MOCHA_USING_DESKTOP = "PREF_MOCHA_USING_DESKTOP";
        public static final String PREF_MOCHA_TRANSLATABLE = "PREF_MOCHA_TRANSLATABLE";
        public static final String PREF_MOCHA_CALL_VIA_FS = "PREF_MOCHA_CALL_VIA_FS";
        public static final String PREF_MOCHA_ENABLE_E2E = "PREF_MOCHA_ENABLE_E2E";

        public static final String PREF_LAST_SHOW_ALERT_TAB_HOT = "PREF_LAST_SHOW_ALERT_TAB_HOT";
        //        public static final String PREF_HAD_SHOW_ALERT_TAB_HOT_TODAY = "PREF_HAD_SHOW_ALERT_TAB_HOT_TODAY";
        public static final String PREF_HAD_NOTIFY_TAB_HOT = "PREF_HAD_NOTIFY_TAB_HOT";

        public static final String PREF_LAST_SHOW_ALERT_TAB_VIDEO = "PREF_LAST_SHOW_ALERT_TAB_VIDEO";
        public static final String PREF_LAST_SHOW_ALERT_TAB_MOVIE = "PREF_LAST_SHOW_ALERT_TAB_MOVIE";
        public static final String PREF_LAST_SHOW_ALERT_TAB_MUSIC = "PREF_LAST_SHOW_ALERT_TAB_MUSIC";
        public static final String PREF_LAST_SHOW_ALERT_TAB_NEWS = "PREF_LAST_SHOW_ALERT_TAB_NEWS";
        public static final String PREF_LAST_SHOW_ALERT_TAB_SECURITY = "PREF_LAST_SHOW_ALERT_TAB_SECURITY";
        public static final String PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET = "PREF_LAST_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET";
        public static final String PREF_FIRST_SHOW_POPUP_INTRO_SMS_NO_INTERNET = "PREF_FIRST_SHOW_POPUP_INTRO_SMS_NO_INTERNET";
        public static final String PREF_TIME_SHOW_BANNER_LIXI = "PREF_TIME_SHOW_BANNER_LIXI";
//        public static final String PREF_HAS_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET = "PREF_HAS_SHOW_DIALOG_SUGGEST_SEND_SMS_WITHOUT_INTERNET";
//        public static final String PREF_LAST_SHOW_INTRO_SEND_SMS_WITHOUT_INTERNET = "PREF_LAST_SHOW_INTRO_SEND_SMS_WITHOUT_INTERNET";
//        public static final String PREF_LAST_SHOW_ALERT_POPUP_REGISTER_VIP = "PREF_LAST_SHOW_ALERT_POPUP_REGISTER_VIP";

        //        public static final String PREF_LAST_SHOW_ALERT_AB_DISCOVER = "PREF_LAST_SHOW_ALERT_AB_DISCOVER";
        //app lock
        public static final String PREF_SETTING_LOCK_APP_ENABLE = "PREF_SETTING_LOCK_APP_ENABLE";
        public static final String PREF_SETTING_LOCK_APP_TIME = "PREF_SETTING_LOCK_APP_TIME";
        public static final String PREF_SETTING_LOCK_APP_PASS_ENCRYPTED = "PREF_SETTING_LOCK_APP_PASS_ENCRYPTED";
        public static final String PREF_SETTING_LOCK_APP_STATE_LOCKED = "PREF_SETTING_LOCK_APP_STATE_LOCKED";
        public static final String PREF_AROUND_LOCATION = "PREF_AROUND_LOCATION";
        public static final String PREF_LOCATION_NOT_SHOW_AGAIN = "PREF_LOCATION_NOT_SHOW_AGAIN";
        public static final String PREF_SETTING_LOCK_APP_FINGERPRINT_ENABLE = "PREF_SETTING_LOCK_FINGERPRINT_ENABLE";

        public static final String PREF_INAPP_LAST_SHOW_POPUP = "PREF_INAPP_LAST_SHOW_POPUP";
        public static final String PREF_INAPP_LAST_FAKE_OA = "PREF_INAPP_LAST_FAKE_OA";

        public static final String PREF_SAVE_SEARCH_USER = "PREF_SAVE_SEARCH_USER";
        public static final String PREF_LUCKEY_WHEEL_LAST_SPIN = "PREF_LUCKEY_WHEEL_LAST_SPIN";
        public static final String PREF_LUCKEY_WHEEL_LAST_TIME_CLICK = "PREF_LUCKEY_WHEEL_LAST_TIME_CLICK";
        public static final String PREF_LUCKEY_WHEEL_LAST_LOTTPOINT = "PREF_LUCKEY_WHEEL_LAST_LOTTPOINT";
        //        public static final String PREF_ITEM_DEEPLINK_LAST_TIME_CLICK = "PREF_ITEM_DEEPLINK_LAST_TIME_CLICK";
        public static final String PREF_LAST_TIME_SHOW_DIALOG_WEB = "PREF_LAST_TIME_SHOW_DIALOG_WEB";
        public static final String PREF_COUNT_SHOW_DIALOG_WEB = "PREF_COUNT_SHOW_DIALOG_WEB";
//        public static final String PREF_LIST_GAME_LAST_TIME_CLICK = "PREF_LIST_GAME_LAST_TIME_CLICK";

        public static final String PREF_CALL_OUT_ENABLE_STATE = "PREF_CALL_OUT_ENABLE_STATE";
        public static final String PREF_MY_STRANGER_LOCATION = "PREF_MY_STRANGER_LOCATION";
        public static final String PREF_LAST_UPDATE_STRANGER_LOCATION = "PREF_LAST_UPDATE_STRANGER_LOCATION";

        public static final String PREF_LIST_TOP_VIDEO = "PREF_LIST_TOP_VIDEO";
        //        public static final String PREF_CALL_HISTORY_BANNER_LAST_TIME_CLICK = "PREF_CALL_HISTORY_BANNER_LAST_TIME_CLICK";
        public static final String PREF_CONFIDE_LAST_TIME_CLICK = "PREF_CONFIDE_LAST_TIME_CLICK";
        public static final String PREF_QRCODE_URL = "QRCODE_URL";

        public static final String PREF_LAST_TIME_UPLOAD_LOG = "PREF_LAST_TIME_UPLOAD_LOG";
        public static final String PREF_ENABLE_UPLOAD_LOG = "PREF_ENABLE_UPLOAD_LOG";

        public static final String PREF_LIST_STICKY_BANNER = "LIST_STICKY_BANNER";
        public static final String PREF_LAST_TIME_UPLOAD_LOCATION = "PREF_LAST_TIME_UPLOAD_LOCATION";
        //        public static final String PREF_LAST_TIME_GET_LIST_GROUP = "PREF_LAST_TIME_GET_LIST_GROUP";
        public static final String PREF_GET_LIST_GROUP_SUCCESS = "PREF_GET_LIST_GROUP_SUCCESS";
        public static final String PREF_LIST_ADS_VIDEO_CATEGORY = "PREF_LIST_ADS_VIDEO_CATEGORY";
        public static final String PREF_SHOW_DIALOG_CHANGE_PREFIX = "PREF_SHOW_DIALOG_CHANGE_PREFIX";
        public static final String PREF_SHOW_DIALOG_THREAD_CHAT_CHANGE_PREFIX = "PREF_SHOW_DIALOG_THREAD_CHAT_CHANGE_PREFIX";
        public static final String PREF_LIST_NUMBER_SEND_BROADCAST_CHANGE_PREFIX = "PREF_LIST_NUMBER_SEND_BROADCAST_CHANGE_PREFIX";
        public static final String PREF_SAVE_LIST_CONFIG_TAB_HOME = "PREF_SAVE_LIST_CONFIG_TAB_HOME_V1";
        // TODO: [START] Cambodia version
        public static final String PREF_SAVE_LIST_CONFIG_TAB_HOME_KH = "PREF_SAVE_LIST_CONFIG_TAB_HOME_KH";
        public static final String PREF_CONFIG_TAB_DEFAULT = "PREF_CONFIG_TAB_DEFAULT";
        // TODO: [END] Cambodia version
        public static final String PREF_MONEY_SAVING = "PREF_MONEY_SAVING";
        public static final String PREF_PIN_HIDE_THREAD_CHAT = "PREF_PIN_HIDE_THREAD_CHAT";
        public static final String PREF_PIN_USE_FINGERPRINT = "PREF_PIN_USE_FINGERPRINT";

        public static final String PREF_SECURITY_FIREWALL_MODE = "PREF_SECURITY_FIREWALL_MODE";
        public static final String PREF_SECURITY_FIREWALL_WHITE_LIST = "PREF_SECURITY_FIREWALL_WHITE_LIST";
        public static final String PREF_SECURITY_SPAM_WHITE_LIST = "PREF_SECURITY_SPAM_WHITE_LIST";
        public static final String PREF_SECURITY_SPAM_BLACK_LIST = "PREF_SECURITY_SPAM_BLACK_LIST";
        public static final String PREF_SECURITY_SPAM_SMS_LIST = "PREF_SECURITY_SPAM_SMS_LIST";
        public static final String PREF_SECURITY_FIREWALL_SMS_LIST = "PREF_SECURITY_FIREWALL_SMS_LIST";
        public static final String PREF_SECURITY_VULNERABILITY_ON_DEVICE = "PREF_SECURITY_VULNERABILITY_ON_DEVICE";
        public static final String PREF_SECURITY_TIME_SCAN_VULNERABILITY = "PREF_SECURITY_TIME_SCAN_VULNERABILITY";
        public static final String PREF_SECURITY_NEWS_LIST = "PREF_SECURITY_NEWS_LIST";
        public static final String PREF_LAST_TIME_GET_SAVING_BUDGET = "PREF_LAST_TIME_GET_SAVING_BUDGET";
        public static final String PREF_LAST_TIME_GET_SECURITY_DATA = "PREF_LAST_TIME_GET_SECURITY_DATA";

        //        public static final String PREF_CURRENT_LIST_MEDIA_BOX = "PREF_CURRENT_LIST_MEDIA_BOX";
        public static final String PREF_HAS_MESSAGE_YET = "PREF_HAS_MESSAGE_YET";
        public static final String PREF_ENABLE_RECEIVE_MEDIA_BOX_NEWS = "PREF_ENABLE_RECEIVE_MEDIA_BOX_NEWS";
        public static final String PREF_ENABLE_RECEIVE_MEDIA_BOX_MOVIE = "PREF_ENABLE_RECEIVE_MEDIA_BOX_MOVIE";
        public static final String PREF_ENABLE_RECEIVE_MEDIA_BOX_MUSIC = "PREF_ENABLE_RECEIVE_MEDIA_BOX_MUSIC";
        public static final String PREF_ENABLE_RECEIVE_MEDIA_BOX_VIDEO = "PREF_ENABLE_RECEIVE_MEDIA_BOX_VIDEO";
        //        public static final String PREF_CLOSE_MEDIA_BOX_UNTIL_HAS_NEW = "PREF_CLOSE_MEDIA_BOX_UNTIL_HAS_NEW";
        public static final String PREF_MOVIES_SUBTAB_HOME_LIST = "PREF_MOVIES_SUBTAB_HOME_LIST";
        public static final String PREF_MOVIES_DATA_SUBTAB_HOME = "PREF_MOVIES_DATA_SUBTAB_HOME";
        public static final String PREF_LAST_TIME_GET_TOP_CHANNEL_BY_CATEGORY = "PREF_LAST_TIME_GET_TOP_CHANNEL_BY_CATEGORY";
        public static final String PREF_LOCATION_CLIENT_IP = "PREF_CLIENT_IP";
        public static final String PREF_LOCATION_CLIENT_COUNTRY_CODE = "PREF_CLIENT_COUNTRY_CODE";
        public static final String PREF_LAST_TIME_GET_ADS_MAIN_TAB_DATA = "PREF_LAST_TIME_GET_ADS_MAIN_TAB_DATA";
        public static final String PREF_LIST_ADS_MAIN_TAB = "PREF_LIST_ADS_MAIN_TAB";
        public static final String PREF_IS_LOGIN = "PREF_IS_LOGIN";
        public static final String PREF_PHONE_NUMBER = "PREF_PHONE_NUMBER";
        public static final String PREF_IS_FIRST_OPEN_METFONE_TAB = "PREF_IS_FIRST_METFONE_OPEN";
        public static final String PREF_OTP = "otp";
        public static final String PREF_INDEX_SHOW_ADS_MAIN_TAB = "PREF_INDEX_SHOW_ADS_MAIN_TAB";
        public static final String PREF_LAST_TIME_SHOW_ADS_MAIN_TAB = "PREF_HISTORY_SHOW_ADS_MAIN_TAB";
        public static final String PREF_IS_ANONYMOUS_LOGIN = "PREF_IS_ANONYMOUS_LOGIN";
        //        public static final String PREF_LAST_TIME_GET_MEDIA_BOX = "PREF_LAST_TIME_GET_MEDIA_BOX";
        public static final String PREF_COUNT_SHOW_DIALOG_FREE_DATA_NETNEWS = "PREF_COUNT_SHOW_DIALOG_FREE_DATA_NETNEWS";
        public static final String PREF_LAST_SHOW_DIALOG_FREE_DATA_NETNEWS = "PREF_LAST_SHOW_DIALOG_FREE_DATA_NETNEWS";
        public static final String PREF_COUNT_SHOW_DIALOG_FREE_DATA_MOVIES = "PREF_COUNT_SHOW_DIALOG_FREE_DATA_MOVIES";
        public static final String PREF_LAST_SHOW_DIALOG_FREE_DATA_MOVIES = "PREF_LAST_SHOW_DIALOG_FREE_DATA_MOVIES";
        public static final String PREF_DATA_EVENTS_NETNEWS = "PREF_DATA_EVENTS_NETNEWS";
        public static final String PREF_DATA_TAB_HOME_NEW = "PREF_DATA_TAB_HOME_NEW";
        public static final String PREF_BG_HEADER_HOME_FILE_PATH = "PREF_BG_HEADER_HOME_FILE_PATH";
        public static final String PREF_BG_HEADER_HOME_URL = "PREF_BG_HEADER_HOME_URL";
        public static final String PREF_SIGN_UP_INFO = "PREF_SIGN_UP_INFO";
        public static final String PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN";
        public static final String PREF_REFRESH_TOKEN = "PREF_REFRESH_TOKEN";
        public static final String PREF_PHONE_SERVICE = "PREF_PHONE_SERVICE";
        public static final String PREF_LIST_PHONE_SERVICE = "PREF_LIST_PHONE_SERVICE";
        public static final String PREF_IDENTIFY_PROVIDER = "PREF_IDENTIFY_PROVIDER";
        public static final String PREF_FIREBASE_REFRESHED_TOKEN = "PREF_FIREBASE_REFRESHED_TOKEN";
        public static final String PREF_SEND_TOKEN_SUCCESS = "PREF_SEND_TOKEN_SUCCESS";
        //        public static final String PREF_HAS_SHOW_INTRO = "PREF_HAS_SHOW_INTRO";
        public static final String PREF_LAST_TIME_DATA_TAB_HOME = "PREF_LAST_TIME_DATA_TAB_HOME";

        public static final String PREF_IS_SERVER_TEST = "PREF_IS_SERVER_TEST";
        public static final String PREF_NAME_BANNER = "PREF_NAME_BANNER";

        public static final String PREF_DID_SHOW_INTRO_NEW_REACTION = "PREF_SHOW_INTRO_NEW_REACTION";
        public static final String PREF_UUID_CONFIG = "PREF_UUID_CONFIG";
        public static final String PREF_SHOW_DIALOG_DATA_CHALLENGE = "PREF_SHOW_DIALOG_DATA_CHALLENGE";
        public static final String PREF_HAS_BACKUP = "PREF_HAS_BACKUP";
        public static final String PREF_DATA_ONLINE_SPOINT = "PREF_DATA_ONLINE_SPOINT";

        public static final String PREF_NAME_VIETTELPAY = "PREF_NAME_VIETTELPAY";
        public static final String PREF_LAST_CLICK_SUGGEST_CONTENT = "PREF_LAST_CLICK_SUGGEST_CONTENT";

        public static final String PREF_DOMAIN_FILE_V1 = "pref_domain_file_v1";
        public static final String PREF_DOMAIN_IMAGE_V1 = "pref_domain_image_v1";
        public static final String PREF_DOMAIN_ON_MEDIA_V1 = "pref_domain_on_media_v1";
        public static final String PREF_DOMAIN_MC_VIDEO = "pref_domain_mcvideo";
        public static final String PREF_DOMAIN_KMUSIC = "pref_domain_kmusic";
        public static final String PREF_DOMAIN_KMUSIC_SEARCH = "pref_domain_kmusic_search";
        public static final String PREF_DOMAIN_KMOVIES = "pref_domain_kmovies";
        public static final String PREF_DOMAIN_NETNEWS = "pref_domain_netnews";
        public static final String PREF_DOMAIN_TIIN = "pref_domain_tiin";
        public static final String PREF_EXTENSION_DECODERS_EXTRA = "pref_extension_decoders";

        public static final String PREF_WS_SESSION_ID = "PREF_WS_SESSION_ID";
        public static final String PREF_WS_TOKEN = "PREF_WS_TOKEN";
        public static final String PREF_WS_ISDN = "PREF_WS_ISDN";

        public static final String PREF_ON_BOARDING_SHOWED = "PREF_ON_BOARDING_SHOWED";
        public static final String PREF_KH_ACC_RANK = "PREF_KH_ACC_RANK";
        public static final String PREF_CACHE_ACCOUNT_RANK_KH = "PREF_CACHE_ACCOUNT_RANK_KH";
        public static final String PREF_CACHE_CHANGE_ACCOUNT_CHANNEL = "PREF_CACHE_CHANGE_ACCOUNT_CHANNEL";
        public static final String BACK_DEVICE = "BACK_DEVICE";

        public static final String PREF_FIRST_TIME_OPEN_TAB_METFONE = "PREF_FIRST_TIME_OPEN_TAB_METFONE";
        public static final String IS_REQUEST_PERMISSION_FOR_CALL = "is_request_for_call";

        /**
         * config content
         */
        public static final class CONFIG {
            public static final String TIMESTAMP_GET_CONFIG = "pref_timestamp_get_config_4";
            public static final String SMS2_NOTE_AVAILABLE = "smsout2.note.available"; //key o ban moi
            public static final String SMSOUT_PREFIX_AVAILABLE = "smsout.to.prefix";
            // domain keeng --> ko dung domain lay tu api config nua
            public static final String PREF_DOMAIN_SERVICE_KEENG = "domain.service.keeng";
            public static final String PREF_DOMAIN_MEDIA2_KEENG = "domain.media2.keeng";
            public static final String PREF_DOMAIN_IMAGE_KEENG = "domain.image.keeng";
            // domain keeng tu ban tin success xmpp
            public static final String PREF_DOMAIN_SERVICE_KEENG_V2 = "domain.service.keeng_v2";
            public static final String PREF_DOMAIN_MEDIA2_KEENG_V2 = "domain.media2.keeng_v2";
            public static final String PREF_DOMAIN_IMAGE_KEENG_V2 = "domain.image.keeng_v2";
            // config game
            public static final String TIMESTAMP_GET_LIST_STICKER = "TIMESTAMP_GET_LIST_STICKER";
            public static final String DEFAULT_STATUS_NOT_MOCHA = "default.status.notmocha";
            public static final String NVLT_ENABLE = "nvlt.enable";
            // version app
            public static final String VERSION_CODE_APP = "version.code.app";
            public static final String VERSION_NAME_APP = "version.name.app";
            public static final String PING_INTERVAL = "ping.interval";
            public static final String PING_INTERVAL_POWER_SAVE_MODE = "ping.interval.power";

            public static final String PREF_FORCE_GET_CONFIG_NOT_DONE = "PREF_FORCE_GET_CONFIG_NOT_DONE";
            public static final String PREF_FORCE_GET_STICKER_NOT_DONE = "PREF_FORCE_GET_STICKER_NOT_DONE";

            //url api keeng service
            public static final String PREF_URL_SERVICE_GET_SONG = "SERVICE_GET_SONG";
            public static final String PREF_URL_SERVICE_GET_TOP_SONG = "SERVICE_GET_TOP_SONG";
            public static final String PREF_URL_SERVICE_GET_FEEDS_KEENG = "SERVICE_GET_FEEDS_KEENG";
            public static final String PREF_URL_SERVICE_SEARCH_SONG = "SERVICE_SEARCH_SONG";
            public static final String PREF_URL_SERVICE_GET_ALBUM = "SERVICE_GET_ALBUM";
            public static final String PREF_URL_SERVICE_GET_SONG_UPLOAD = "SERVICE_GET_SONG_UPLOAD";
            public static final String PREF_URL_MEDIA2_SEARCH_SUGGESTION = "MEDIA2_SEARCH_SUGGESTION";
            public static final String PREF_URL_MEDIA_UPLOAD_SONG = "MEDIA_UPLOAD_SONG";

            //
            public static final String IMAGE_PROFILE_MAX_SIZE = "imageprofile.max.size";
            // crbt
            public static final String PREF_CRBT_ENABLE = "crbt.enable";
            public static final String PREF_CRBT_PRICE = "crbt.price";
            public static final String PREF_KEENG_PACKAGE = "android.keeng.package.name";
            //onmedia
            public static final String CONFIG_ONMEDIA_ON = "onmedia.on";

//            public static final String REGISTER_VIP_BANNER = "register.vip.banner.v2";
//            public static final String REGISTER_VIP_BUTTON = "register.vip.button.v2";
//            public static final String REGISTER_VIP_CONFIRM = "register.vip.confirm.v2";
//            public static final String REGISTER_VIP_CMD = "register.vip.cmd";
//            public static final String REGISTER_VIP_CMD_CANCEL = "register.vip.cmdcancel";
//            public static final String UNREGISTER_VIP_CONFIRM = "unregister.vip.confirm";
//            public static final String REGISTER_VIP_RECONFIRM = "register.vip.reconfirm";
//            public static final String REGISTER_VIP_WC_CONFIRM = "register.vip.wc.confirm";

            public static final String INAPP_ENABLE = "inapp.enable";
            public static final String LUCKY_WHEEL_ENABLE = "lucky.wheel.enable";
            public static final String QR_SCAN_ENABLE = "qr.scan.enable";
            public static final String GUEST_BOOK_ENABLE = "mocha.memory.on";
            public static final String CALL_OUT_LABEL = "call.out.label";
            public static final String LISTGAME_ENABLE = "listgame.enable";
            public static final String BANNER_CALL_HISTORY = "banner.callhistory";
            public static final String DISCOVERY_ENABLE = "discovery.enable";
            public static final String STRANGER_LOCATION_TIMEOUT = "stranger.location.update";
            public static final String BACKGROND_DEFAULT = "background.default";
            //public static final String MORE_ITEM_DEEPLINK = "more.item.deeplink";
            public static final String MORE_ITEM_DEEPLINK_V2 = "more.item.deeplink.v2";
            public static final String MORE_ITEM_DEEPLINK_V3 = "more.item.deeplink.v3";
            public static final String SHAKE_GAME_ON = "shake.game.on";
            public static final String LIXI_ENABLE = "lixi.v2.enable";
            public static final String SONTUNG83_ENABLE = "sontung83.enable";
            public static final String AVNO_PAYMENT_WAPSITE = "avno.payment.wapsite";
            public static final String VIDEO_UPLOAD_USER = "video.upload.user";
            public static final String VIDEO_UPLOAD_PASS = "video.upload.pass";
            public static final String TAB_VIDEO_ENABLE = "mocha.video.enable";
            public static final String SPOINT_ENABLE = "spoint.enable";
            public static final String BANKPLUS_ENABLE = "bankplus.enable";
            public static final String WATCH_VIDEO_TOGETHER_ENABLE = "mocha.watchtogether.enable";
            public static final String TAB_STRANGER_ENABLE = "home.stranger.enable";
            public static final String SUGGEST_CONTENT_ENABLE = "suggest.content";
            public static final String FIREBASE_WAKEUP_ENABLE = "firebase.wakeup.enable";
            public static final String GET_LINK_LOCATION = "getlink.location";
            public static final String WHITELIST_DEVICE = "whitelist.device";
            public static final String PREFIX_CHANGENUMBER = "changenum.prefix.v2";
            //            public static final String PREFIX_CHANGENUMBER_TEST = "changenum.prefix.v2.test";//
            public static final String UPLOAD_IDCARD_ENABLE = "upload.idcard.enable";
            //public static final String MORE_ITEM_EDIT_CONTACT = "more.item.editcontact"; //config on/off tính năng tính năng Chuyển đổi 11 số
            public static final String SMS_NO_INTERNET_ENABLE = "sms.nointernet.enable";
            public static final String HOME_MOVIE_ENABLE = "home.movie.enable";
            public static final String HOME_NEWS_ENABLE = "home.news.enable";
            public static final String HOME_TIIN_ENABLE = "home.tiin.enable";
            public static final String HOME_MUSIC_ENABLE = "home.music.enable";
            public static final String HOME_SELFCARE_ENABLE = "home.selfcare.enable";
            public static final String MORE_BANNER = "more.banner";
            public static final String FACEBOOK_HASHTAG = "facebook.hashtag";
            public static final String VIP_SUBSCRIPTION = "vip.subscription";
            public static final String DOMAIN_FREE_DATA = "domain.free.data";
            public static final String DOMAIN_VIDEO_UPLOAD = "video.upload.domain";
            public static final String DOMAIN_VIDEO_CAN_OPEN_PLAYER = "video.domain.open.mcplayer";
            public static final String SAVING_ENABLE = "savings.on";
            public static final String HOME_SECURITY_ENABLE = "home.security.enable";
            public static final String MOVIE_DRM_ENABLE = "movie.drm.enable";
            public static final String HOME_TAB_CONFIG = "home.tab.config.v3";
            public static final String VIDEO_INDEX_SHOW_ADS = "video.index.show.ads";
            public static final String CHANNEL_VIDEO_UPLOAD_MONEY_ENABLE = "channel.video.upload.money.enable";
            public static final String VIDEO_TIME_SKIP_AD = "video.time_skip_ad";
            public static final String API_LOCATION_GET = "api.location.get";
            public static final String SECURE_LOCATION_GET = "secure.location.get";
            public static final String REGEX_MUSIC_SONG = "regex.music.song";
            public static final String REGEX_MUSIC_ALBUM = "regex.music.album";
            public static final String REGEX_MUSIC_VIDEO = "regex.music.video";
            public static final String REGEX_MUSIC_PLAYLIST = "regex.music.playlist";
            public static final String REGEX_MOVIES_DETAIL = "regex.movies.detail";
            public static final String REGEX_MOCHA_VIDEO = "regex.mocha.video";
            public static final String REGEX_MOCHA_CHANNEL = "regex.mocha.channel";
            public static final String REGEX_NETNEWS_DETAIL = "regex.netnews.detail";
            public static final String REGEX_TIIN_DETAIL = "regex.tiin.detail";
            public static final String HOME_BANNER_THEME = "home.banner.theme";
            public static final String VIDEO_SHOW_ADS_ON_FIRST = "video.show.ads.on.first";
            public static final String FUNCTION_DATA_PACKAGES_TITLE = "function.data.packages.title";
            public static final String LOCATION_GOOGLE_MAP_URL = "location.google.map.url";
            public static final String DOMAIN_KMOVIES_SHARING_ORIGINAL = "domain.kmovies.sharing.original";
            public static final String DOMAIN_KMOVIES_SHARING_SWAP = "domain.kmovies.sharing.swap";
            public static final String DOMAIN_KMUSIC_SHARING_ORIGINAL = "domain.kmusic.sharing.original";
            public static final String DOMAIN_KMUSIC_SHARING_SWAP = "domain.kmusic.sharing.swap";
            public static final String HOME_MY_VIETTEL_ENABLE = "home.myviettel.enable";
            public static final String MY_VIETTEL_LINK_SHOP = "myviettel.link.shop";
            public static final String MY_VIETTEL_LINK_SWITCH_TO_VIETTEL = "myviettel.link.switch.to.viettel";
            public static final String MY_VIETTEL_MSG_SWITCH_TO_VIETTEL = "myviettel.msg.switch.to.viettel";
            public static final String MY_VIETTEL_MSG_NOT_REGISTERED_DATA = "myviettel.msg.not.registered.data";
            public static final String MY_VIETTEL_LINK_CHARGING_HISTORY = "myviettel.link.charging.history";
            public static final String MY_VIETTEL_LINK_VIETTEL_PLUS = "myviettel.link.viettel.plus";
            public static final String MY_VIETTEL_PACKAGE_NAME = "android.myviettel.package.name";
            public static final String DOMAIN_LOG_ACTION_MEDIA = "domain.log.action.media";
            public static final String MY_VIETTEL_DATA_CHALLENGE_ENABLE = "myviettel.data.challenge.enable";
            public static final String MY_VIETTEL_DATA_CHALLENGE_RULE = "myviettel.data.challenge.rule";
            public static final String DOMAIN_VIDEO_GAME_STREAMING_API = "domain.video.game.streaming.api";
            public static final String DOMAIN_VIDEO_GAME_STREAMING_WS = "domain.video.game.streaming.ws";
            public static final String PUBLIC_KEY_VIDEO_GAME_STREAMING = "public.key.video.game.streaming";
            public static final String DOMAIN_GAME_COUNT_DOWN = "domain.game.countdown";
            public static final String DOMAIN_GAME_IQ = "domain.game.iq";
            public static final String CONFIG_LIVE_COMMENT = "livecomment.config";
            public static final String BAD_WORD_CONFIG = "bad.word.config";
            public static final String SENSITIVE_WORD_CONFIG = "sensitive.word.config";
            public static final String DOMAIN_FAKE_MO = "domain.func.fakemo";
            public static final String HOME_SEACH_HINT = "home.search.hint";
            public static final String SPOINT_ONLINE_BONUS = "spoint.online.bonus";
            public static final String VIDEO_TEXT_RUN_CONFIG = "video.text.run.config";
            public static final String CONFIG_UMONEY = "umoney.on";
            public static final String DOMAIN_GAME_LUCKYWHEEL = "domain.game.luckywheel";
            // TODO CamID: home_tab_config
            // [start] CamID
            public static final String HOME_GAME_ENABLE = "home.game.enable";
            public static final String HOME_CINEMA_ENABLE = "home.cinema.enable";
            public static final String HOME_ESPORT_ENABLE = "home.esport.enable";
            public static final String HOME_METFONE_ENABLE = "home.metfone.enable";
            public static final String HOME_CONTACT_ENABLE = "home.contact.enable";
            // [end] CamID
        }
    }

    public static final class PREF_DEFAULT {
        public static final String URL_SERVICE_GET_SONG_DEFAULT = "/KeengWSRestful/ws/common/getSong";
        public static final String URL_SERVICE_GET_TOP_SONG_DEFAULT = "/KeengWSRestful/ws/internal/mocha/getSongMocha";
        public static final String URL_SERVICE_GET_FEEDS_KEENG_DEFAULT = "/KeengWSRestful/ws/social/user/feed";
        public static final String URL_SERVICE_SEARCH_SONG_DEFAULT = "/KeengWSRestful/ws/common/search";
        public static final String URL_SERVICE_GET_ALBUM_DEFAULT = "/KeengWSRestful/ws/common/getAlbum";
        public static final String URL_SERVICE_GET_SONG_UPLOAD_DEFAULT = "/KeengWSRestful/ws/internal/mocha/getSongUpload";
        public static final String URL_MEDIA2_SEARCH_SUGGESTION_DEFAULT = "/solr/mbartists/select/";
        public static final String URL_MEDIA_UPLOAD_SONG_DEFAULT = "http://vip.medias4.cdn.keeng.vn:8089/uploadv4.php";
        public static final String URL_BANNER_CALL_HISROTY = "assets://banner/banner_call_history.jpg";
        //for module setting
        public static final boolean PREF_DEF_VIBRATE = true;
        public static final boolean PREF_DEF_RINGTONE = true;
        public static final boolean PREF_DEF_MSG_IN_POPUP = false;// default disable quick reply
        public static final boolean PREF_DEF_ENABLE_SEEN = true;
        public static final boolean PREF_DEF_ENABLE_UNLOCK = true;
        public static final boolean PREF_DEF_NOTIFY_NEW_FRIEND = true;
        public static final boolean PREF_DEF_IMAGE_HD = true;
        public static final boolean PREF_DEF_PLAYMUSIC_IN_ROOM = false;
        public static final boolean PREF_DEF_BIRTHDAY_REMINDER = true;
        public static final boolean PREF_DEF_SETTING_ON_OFF_NOTI = true;

        // TODO: [START] Cambodia version
        public static final boolean PREF_DEF_ENABLE_WATCHING_CINEMA_ACTIVITY = true;
        // TODO: [START] Cambodia version

        public static final long PREF_DEF_SETTING_NOTIFY_TIME_DEFAULT = -1;
        public static final long PREF_DEF_SETTING_NOTIFY_TIME_15M = 15 * 60 * 1000;
        public static final long PREF_DEF_SETTING_NOTIFY_TIME_1H = 60 * 60 * 1000;
        public static final long PREF_DEF_SETTING_NOTIFY_TIME_8H = 8 * 60 * 60 * 1000;
        public static final long PREF_DEF_SETTING_NOTIFY_TIME_24H = 24 * 60 * 60 * 1000;
        // for prevate setting
        public static final boolean PREF_DEF_PREVIEW_MSG = true;
        // keyboard
        public static final int KEYBOARD_HEIGHT_DEFAULT = 233; // dp
        public static final int CHAT_BAR_HEIGHT_DEFAULT = 44; // dp
        public static final int SLIDE_MENU_MARGIN = 66;//dp

        //onmedia
        public static final boolean PREF_DEF_ONMEDIA_CHAT_WITH_STRANGER = true;
        public static final boolean PREF_DEF_ONMEDIA_NOTIFY_NEW_FEED = true;
        public static final boolean PREF_DEF_ONMEDIA_SECURE_WEB = true;
        //max images can upload
        public static final String PREF_IMAGE_PROFILE_MAX_SIZE = "20";
        //crbt
        public static final String PREF_DEF_CRBT_ENABLE = "0";
        public static final String PREF_DEF_CRBT_PRICE = "1000 VND";
        public static final String PREF_DEF_KEENG_PACKAGE = "com.vttm.keeng";
        public static final String PREF_DEF_NETNEWS_PACKAGE = "com.vttm.tinnganradio";
        //upload video
        public static final String PREF_DEF_VIDEO_UPLOAD_USER = "974802c5178ab63eba02d8035ad3f6ab";
        public static final String PREF_DEF_VIDEO_UPLOAD_PASS = "974802c5178ab63eba02d8035ad3f6ab";
        public static final String PREF_DEF_DOMAIN_VIDEO_GAME_STREAMING_API = "http://wslive.mocha.com.vn:80/";
        public static final String PREF_DEF_DOMAIN_VIDEO_GAME_STREAMING_WS = "ws://wslive.mocha.com.vn:8083/gameWS/websocket";
        public static final String PREF_DEF_PUBLIC_KEY_VIDEO_GAME_STREAMING = "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgF3g82nB1ImzAwSN7JXeOC7wChDA4Nbzun/2B60sB04LCxBt88yRQTK734ugqAJ9cnYYNjwYfzcoTmubiMygsdtoNf1HTmezAL+ppsJxZ/TlfomXz6zUS2HxNUdNcgX0NdHpq5OR9713p6tiq5Z4TdYjja9P7FEG8p4xf8snDEjhAgMBAAE=";

    }

    public static final class SMS {
        public static final String SMS_CENTER_NUMBER = "3304";
        public static final String SMS_CENTER_NUMBER_3 = "500";
        public static final String SMS_CENTER_NUMBER_4 = "MOCHA";
        // tong dai nhan tin mang ngoai
        public static final String SMS_CENTER_NUMBER_2 = "1667893234";
        public static final String SMS_CENTER_NUMBER_5 = "0981234888";
        public static final String SMS_CENTER_NUMBER_6 = "84981234888";
        public static final String SMS_CENTER_NUMBER_7 = "+84981234888";
        public static final String SMS_PASSWORD_NOTIFY_CONTENT = "ma xac nhan cua ban la:";
        public static final String SMS_PASSWORD_NOTIFY_CONTENT_2 = "your mocha code is:";
        // lixi
        public static final String SMS_LIXI_CENTER_NUMBER = "LIXI";
        public static final String SMS_LIXI_CODE_CONTENT = "ma xac thuc";
    }

    public static final class MENU {
        public static final int DELETE = 100;
        public static final int EDIT = 101;
        public static final int COPY = 102;
        public static final int SEND_EMAIL = 104;
        public static final int GO_URL = 105;
        public static final int VIEW_CONTATCT_DETAIL = 106;
        public static final int ADD_CONTACT = 107;
        public static final int REENG_CHAT = 109;
        public static final int SELECT_GALLERY = 112;
        public static final int REMOVE_AVATAR = 111;
        public static final int CAPTURE_IMAGE = 113;
        public static final int MESSAGE_COPY = 114;
        public static final int MESSAGE_DELETE = 115;
        public static final int MESSAGE_DETAIL_SEND_RECEIVE = 116;
        public static final int MESSAGE_DETAIL_DELIVER = 117;

        public static final int MENU_CONTACTS = 118;
        public static final int MENU_CREATE_GROUP = 119;
        public static final int MENU_SEARCH = 120;
        public static final int MENU_SETTING = 121;
        public static final int MENU_PROFILE = 122;
        public static final int MENU_ADD_FAV = 123;
        public static final int MENU_PRESENT = 124;
        public static final int ADD_FAV = 125;
        public static final int DEL_FAV = 126;
        public static final int SHARE_ONMEDIA = 127;
        public static final int INFO = 128;
        public static final int POPUP_YES = 129;
        public static final int POPUP_SAVE = 130;
        public static final int POPUP_CANCEL = 131;
        public static final int POPUP_GUEST_BOOK_ASSIGN = 132;
        public static final int POPUP_FORCE_UPDATE = 133;
        public static final int MENU_ADD_GROUP = 134;
        public static final int POPUP_EXIT = 135;
        public static final int POPUP_GUEST_BOOK_DELETE_PAGE = 136;
        public static final int POPUP_GUEST_BOOK_DELETE_BOOK = 137;
        public static final int MENU_GSM_CALL = 138;
        public static final int POPUP_OK_AUTO_PLAY_VOICE_STICKER = 139;
        public static final int MESSAGE_FORWARD = 140;
        public static final int POPUP_OK_SEND_DOCUMENT = 141;
        public static final int POPUP_OK_GG_PLAY_SERVICE = 142;
        public static final int FORWARD_TO_THREAD = 143;
        public static final int FORWARD_TO_PHONE_NUMBER = 144;
        public static final int SHARE_SIEU_HAI = 145;
        public static final int MESSAGE_REPLY = 147;
        public static final int MESSAGE_MENU_TRANSLATE = 148;
        // xoa bo sticker
        public static final int POPUP_DELETE_STICKER_COLLECTION = 150;
        public static final int MENU_DIRECT_MAPS = 151;
        public static final int MENU_OPEN_MAPS = 152;
        //confirm share facebook
        public static final int CONFIRM_SHARE_FACEBOOK = 153;
        public static final int CONFIRM_DEACTIVE_ACCOUNT = 165;
        public static final int MENU_ADD_FRIEND_GROUP = 154;
        public static final int MENU_BLOCK_PERSON = 166;
        public static final int MENU_MANAGE_GROUP = 167;
        public static final int MENU_SETTING_SOLO = 168;
        public static final int MENU_SETTING_GROUP = 169;
        //
        public static final int MESSAGE_RESTORE = 170;
        public static final int MENU_UNBLOCK_PERSON = 171;
        public static final int STRANGER_FILTER_MALE = 178;
        public static final int STRANGER_FILTER_FEMALE = 179;
        public static final int STRANGER_FILTER_MALE_FEMALE = 180;
        public static final int STRANGER_FILTER_STAR = 181;

        public static final int ACCEPT_RETRY_CHANGE_SONG = 183;
        public static final int INVITE_FRIEND = 184;
        public static final int POPUP_FOLLOW_ROOM = 185;
        public static final int POPUP_LEAVE_ROOM = 186;
        public static final int MENU_REFRESH = 188;
        public static final int MENU_DELETE_STRANGER = 189;
        public static final int DELETE_AND_LEAVE_GROUP = 190;
        public static final int LEAVE_GROUP = 191;

        public static final int MENU_SHARE_LINK = 192;
        public static final int MENU_WRITE_STATUS = 193;
        public static final int MENU_CREATE_BROADCAST = 194;
        public static final int MENU_MANAGE_BROADCAST = 195;
        public static final int REMOVE_CONTACT_BROADCAST = 197;
        public static final int DELETE_BROADCAST = 198;
        // group manager
        public static final int MAKE_ADMIN = 199;
        public static final int MAKE_MEMBER = 200;
        public static final int KICK_MEMBER = 201;
        public static final int VIEW_MEMBER_CONTACT = 202;
        public static final int ADD_MEMBER_CONTACT = 203;
        public static final int ACCEPT_REMOVE_IMAGE_PROFILE = 204;
        public static final int UNFOLLOW = 205;
        public static final int REPORT = 206;
        public static final int POPUP_UNFOLLOW = 207;
        public static final int POPUP_REPORT = 208;
        public static final int POPUP_DELETE = 209;
        // report room
        public static final int POPUP_REPORT_SPAM = 210;
        public static final int POPUP_REPORT_BADWORD = 211;
        public static final int POPUP_REPORT_CHEAT = 212;
        //onmedia
        public static final int NOW_PLAYING = 214;
        public static final int MENU_LIST_SHARE = 215;
        public static final int MENU_RETRY_GET_METADATA = 216;
        public static final int MENU_LIST_LIKE = 217;
        public static final int POPUP_CONFIRM_REGISTER_VIP = 219;
        public static final int MENU_PREVIEW_SHARE = 220;
        public static final int MENU_PREVIEW_AVATAR = 221;
        public static final int MENU_PREVIEW_BACKGROUND = 222;
        public static final int MENU_PREVIEW_DELETE = 223;
        public static final int MENU_THREADS_DELETE = 224;
        // contact
        public static final int MENU_CONTACT_UNFOLLOW = 225;
        public static final int MENU_COPY_TEXT = 226;
        public static final int MENU_REPLY = 227;
        public static final int MENU_CALL = 228;
        public static final int MENU_CREATE_POLL = 230;
        public static final int MENU_FORWARD = 231;
        public static final int GROUP_LIST = 232;
        public static final int MENU_WATCH_VIDEO = 233;
        public static final int MENU_MUSIC_ROOM = 234;
        public static final int MENU_SOCIAL_FRIENDS = 235;
        public static final int MENU_NEAR_YOU = 236;
        public static final int MENU_CALL_FREE = 237;
        public static final int MENU_CALL_OUT = 238;
        public static final int MENU_DELETE_ALL_MSG = 239;
        public static final int MENU_OPEN_IN_BROWSER = 240;
        public static final int MENU_DELETE_IMAGE_ONMEDIA = 241;
        public static final int MENU_SET_AS_AVATAR = 242;
        public static final int MESSAGE_PIN = 243;
        public static final int MESSAGE_SHARE = 244;
        public static final int MENU_DOWNLOAD = 245;
        public static final int SEND_IMAGE_TO_FRIEND = 246;
        public static final int POS_ON_SOCIAL = 247;
        public static final int LIST_PEOPLE_SHARE = 248;
        public static final int SHARE_EXTERNAL = 249;
        public static final int MESSAGE_BLOCK_FORWARD = 250;

        //call
        public static final int MENU_CALL_CANT_TALK = 601;
        public static final int MENU_CALL_WILL_CALL = 602;
        public static final int MENU_CALL_WILL_CALL_RIGHT_BACK = 603;
        public static final int MENU_CALL_ON_MY_WAY = 604;
        public static final int MENU_CALL_TYPE_YOURSELF = 605;
        public static final int MENU_CALL_CONFIDE_BUSY = 606;
        public static final int MENU_CALL_CONFIDE_SORRY = 607;
        // guest book
        public static final int MENU_GUEST_BOOK_ADD_PAGE = 608;
        public static final int MENU_GUEST_BOOK_CHANGE_INFO = 609;
        public static final int MENU_GUEST_BOOK_EXPORT = 6010;
        public static final int MENU_GUEST_BOOK_PREVIEW = 6011;
        public static final int MENU_GUEST_BOOK_LOCK = 612;
        public static final int MENU_GUEST_BOOK_UN_LOCK = 613;
        public static final int MENU_GUEST_BOOK_PRINT = 614;
        public static final int MENU_GUEST_BOOK_DELETE_BOOK = 615;

        public static final int MESSAGE_DETAIL_NOTE = 616;
        public static final int MENU_ADD_FRIEND = 617;
        public static final int MENU_QR_CODE = 618;
        public static final int MENU_MARK_READ = 619;
        public static final int MENU_CHATS_SETTING = 620;
        public static final int MENU_CHATS_CALL_DESCRIPTION = 621;
        public static final int MENU_CHANNEL_SUBSCRIBE = 622;
        public static final int MENU_DISCOVER = 633;
        public static final int SHARE_NOW = 634;
        public static final int MORE = 635;
        public static final int PEOPLE_SHARE = 636;
        public static final int MENU_MY_CHANNEL = 637;
        public static final int MENU_VIDEO_LIBRARY = 638;
        public static final int MENU_REPORT_VIDEO = 639;
        public static final int MENU_MINI_VIDEO = 640;
        public static final int MENU_VIP_VIDEO = 641;
        public static final int MENU_SPEED_VIDEO = 642;
        public static final int MENU_REPORT_DHVTT = 643;
        public static final int SEND_MESSAGE = 644;
        public static final int MENU_TOP_MONEY_UPLOAD = 645;
        public static final int MENU_POLL_PIN = 646;
        public static final int MENU_POLL_PUSH = 647;
        public static final int MENU_POLL_CLOSE = 648;
        public static final int MENU_MOVIE_LIKED = 649;
        public static final int MENU_MOVIE_WATCHED = 650;
        public static final int MENU_MOVIE_WATCH_LATER = 651;
        public static final int MENU_OPEN_KEENG_MOVIE = 652;
        public static final int MENU_POST_VIDEO = 653;
        public static final int MENU_EXIT = 654;
        public static final int MENU_ADD_FAVORITE = 655;
        public static final int MENU_ADD_LATER = 656;
        public static final int MENU_REMOVE_LATER = 657;
        public static final int MENU_REMOVE_WATCHED = 658;
        public static final int MENU_REMOVE_FAVORITE = 659;
        public static final int MENU_SAVE_VIDEO = 670;
        public static final int MENU_VIDEO_YOUTUBE = 671;
        public static final int MENU_REMOVE_SAVED = 672;
        public static final int MENU_HISTORY = 673;
        public static final int MENU_MESS = 674;
    }

    public static final class ACTION {
        public static final int REENG_CHAT = 1001;
        public static final int ADD_CONTACT = 1002;
        public static final int EDIT_CONTACT = 1003;
        public static final int DEL_CONTACT = 1004;
        public static final int BLOCK_CONTACT = 1005;
        public static final int CHOOSE_CONTACT = 1006;
        public static final int CHANGE_FAVORITE = 1008;
        public static final int EMAIL_CREATE = 1009;
        public static final int SET_BACKGROUND = 1010;
        public static final int SET_DOWNLOAD_STICKER = 1011;
        public static final int SHARE_LOCATION = 1012;
        public static final int HOLDER_INVITE_FRIEND = 1013;
        // photo action
        public static final int ACTION_TAKE_PHOTO = 1014;
        public static final int ACTION_TAKE_VIDEO = 1015;
        public static final int ACTION_PICK_PICTURE = 1016;
        public static final int ACTION_PICK_VIDEO = 1017;
        public static final int ACTION_CROP_IMAGE = 1018;
        public static final int ACTION_SETTING_LOCATION = 1019;

        public static final int ACTION_GOTO_GROUP_DETAIL = 1020;
        public static final int ACTION_MOCHA_TO_SMS = 1021;
        public static final int ACTION_PICK_FILE = 1022;

        public static final int SEARCH_TRY_PLAY_SONG = 1023;
        public static final int MONEY_AMOUNT_MIN = 1024;
        public static final int MONEY_AMOUNT_MAX = 1025;
        public static final int MONEY_AMOUNT_TRANSFER_SUCCESS = 1026;
        public static final int ACTION_WRITE_STATUS = 1027;
        public static final int ACTION_ONMEDIA_COMMENT = 1028;
        public static final int ACTION_TAKE_PHOTO_GROUP_AVATAR = 1029;
        public static final int ACTION_PICK_PICTURE_GROUP_AVATAR = 1030;
        public static final int ACTION_CROP_BACKGROUND = 1031;
        public static final int ACTION_CROP_AVATAR = 1032;

        public static final int SELECT_SONG = 1033;
        public static final int CHANGE_SONG = 1034;
        public static final int STRANGER_MUSIC_SELECT_SONG = 1035;
        public static final int SELECT_SONG_AND_CREATE_THREAD = 1036;
        public static final int CHANGE_SONG_AND_CREATE_THREAD = 1037;

        public static final int CHOOSE_VIDEO_WATCH_TOGETHER = 1038;
        public static final int CHOOSE_DOCUMENT_CLASS = 1039;
        public static final int SEND_GIFT_LIXI = 1040;
        public static final int ACTION_RC_HINT = 1041;
        public static final int ACTION_OPEN_HIDDEN_THREAD = 1041;
        public static final int ACTION_OPEN_HIDDEN_THREAD_SHARE = 1042;
        public static final int ACTION_SETTING_LOCATION_ON = 1043;
        public static final int ACTION_SETTING_LOCATION_OFF = 1044;

        // Metfone+
        public static final int ACTION_ADD_METFONE_NUMBER = 1045;
        public static final int ACTION_SELECT_METFONE_NUMBER = 1046;
    }

    public static final class FILE {
        public static final String[] FILE_DOC_TYPES = {"application/msword", "application/vnd" +
                ".openxmlformats-officedocument.wordprocessingml.document",
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml" +
                ".presentation",
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "text/plain",
                "application/pdf"};

        public static final int VOICE_MIN_DURATION = 1;
        public static final String JPEG_FILE_PREFIX = "IMG_";
        public static final String VIDEO_FILE_PREFIX = "VID_";

        public static final String JPEG_FILE_SUFFIX = ".jpg";
        public static final String PNG_FILE_SUFFIX = ".png";
        public static final String ZIP_FILE_SUFFIX = ".zip";
        public static final String MP4_FILE_SUFFIX = ".mp4";
        public static final String GP_FILE_SUFFIX = ".3gp";
        public static final String VIDEO_PREFIX_NAME = "V_MOCHA_";
        public static final String NO_MEDIA_FILE_NAME = ".nomedia";
        // avatar
        public static final int AVATAR_MAX_SIZE = 800; // pixel
        public static final int BUFFER_SIZE_DEFAULT = 4096; // 4 Kb
        // max video recorder 30mb
        public static final long VIDEO_MAX_SIZE = 25 * 1024 * 1024;
        public static final long DOCUMENT_MAX_SIZE = 100 * 1000 * 1000;

        public static final long ONE_MEGABYTE = 1024 * 1024;
        public static final long ONE_KILOBYTE = 1024;
        public static final long VIDEO_EXCHANGE_MAX_SIZE = 2 * 1024 * 1024;

        public static final short VIDEO_RECORD_MAX_DURATION = 15; //Giay
        public static final short VIDEO_EXCHANGE_MAX_DURATION = 10; //Giay
    }

    public static final class HTTP {
        public static final String LINK_GOOGLE_PLAY = "https://play.google.com/store/apps/details?id=";
        public static final String LINK_MARKET = "market://details?id=";
        public static final String BOUNDARY = "reeng";
        public static final int CONNECTION_TIMEOUT = 30 * 1000; //30s

        public static final String REST_CLIENT_TYPE = "clientType";
        public static final String REST_REVISION = "revision";
        public static final String REST_GROUP_ID = "groupid";
        public static final String REST_MSISDN = "msisdn";
        public static final String REST_OTHER_MSISDN = "otherMsisdn";
        public static final String REST_TOKEN = "token";
        public static final String REST_ERROR_CODE = "errorCode";// 1 so api dung code, 1 so dung errorCode :(((((((((
        public static final String REST_CODE = "code";
        public static final String INVALID_USER = "invalidUser";
        public static final String INVALID_USER_DESC = "invalidUserDesc";
        public static final String REST_DESC = "desc";
        public static final String REST_THUMB = "thumb";
        public static final String REST_LINK = "link";
        public static final int CODE_SUCCESS = 0;
        public static final String RECEIVERS = "receivers";
        public static final String REST_NAME = "name";
        public static final String REST_USER_NAME = "username";
        public static final String REST_COUNTRY_CODE = "countryCode";
        public static final int CLIENT_TYPE_ANDROID = 1;
        public static final String CLIENT_TYPE_STRING = "Android";
        public static final String LUCKY_CODE = "luckyCode"; //key lucky_code khi invite SMS tra ve
        public static final String REST_NOT_SUPPORT = "notsupport";
        public static final String USER_FACEBOOK_ID = "facebook_id";
        public static final String REQ_TIME = "reqTime";
        public static final String DATA_ENCRYPT = "dataEncrypt";
        public static final String BLOCK_LIST = "blocklist";
        public static final String ROOM_ID = "roomId";
        public static final String FRIENDS = "friends";
        public static final String REST_CONTENT = "content";
        public static final String REST_PAGE = "page";

        public static final String KEENG_USER_ID = "userid";
        public static final String KEENG_MEDIA_NAME = "media_name";
        public static final String KEENG_SINGER_NAME = "singer_name";
        public static final String KEENG_SESSION_TOKEN_UPLOAD = "sessiontoken";

        public static final String REST_LANGUAGE_CODE = "languageCode";
        public static final int CODE_UPLOAD_FAILED_EXCEEDED = 402;

        public static final String UPLOAD_TYPE_IMAGE = "image";
        //new params
        public static final String TIME_STAMP = "timestamp";
        public static final String DATA_SECURITY = "security";
        public static final String REST_MD5 = "md5";
        public static final String REST_RATIO = "ratio";

        public static final String SERVICE_TYPE = "serviceType";
        public static final String LINK_SHARE = "linkShare";
        public static final String BIRTHDAY_REMINDER = "birthdayReminder";
        public static final String DESKTOP = "desktop";
        public static final String PLATFORM = "Mocha";

        public static final class CRBT {
            public static final String FROM = "from";
            public static final String TO = "to";
            public static final String SESSION = "session";
            public static final String CRBT_SONG_INFO = "songinfo";
            public static final String FROM_NICK = "fromnick";
            public static final String TO_NICK = "tonick";
            public static final String EXTERNAL = "external";
        }

        public static final class MESSGE {
            public static final String FROM = "from";
            public static final String TO = "to";
            public static final String CONTENT = "content";
            public static final String TYPE = "type";
            public static final String MSG_ID = "msgid";
            public static final String MD5_STR = "md5Str";
        }

        public static final class FILE {
            public static final String REST_UP_FILE = "uploadfile";
            public static final String REST_FILE_TYPE = "type";
            public static final String REST_UP_MUSIC = "media";
            public static final String REST_FILE_ID = "id";
        }

        public static final class CONTACT {
            public static final String TIME_STAMP = "timestamps";
            public static final String DATA = "contacts";
            public static final String MSISDN = "cmsisdn";
            public static final String STATUS = "status";
            public static final String STATE = "state";
            public static final String LAST_AVATAR = "lavatar";
            public static final String GENDER = "gender";
            public static final String NAME = "cname";
            public static final String BIRTHDAY_STRING = "birthdayStr";
            public static final String COVER_IMAGE = "cover";
            public static final String COVER_ID = "coverSId";
            public static final String ALBUMS = "albums";
            public static final String PERMISSION = "permission";
            public static final String FOLLOWER = "follower";
            public static final String STATE_FOLLOWER = "sFollow";
            public static final String CHAT_DETAIL = "chatdetail";
            public static final String IS_COVER_DEFAULT = "isCover";
            public static final String OPERATOR = "currOperator";
            public static final String USING_DESKTOP = "desktop";
        }

        public static final class USER_INFOR {
            public static final String INFOR = "info";
            public static final String NAME = "name";
            public static final String GENDER = "gender";
            public static final String BIRTHDAY = "birthday";
            public static final String STATUS = "status";
            public static final String LAST_AVATAR = "lavatar";
            public static final String BIRTHDAY_STRING = "birthdayStr";
            public static final String AVATAR_URL = "avatar_url";
            public static final String COVER_IMAGE = "cover";
            public static final String ALBUMS = "albums";
            public static final String PERMISSION = "permission";
            public static final String HIDE_STRANGLE_HISTORY = "hideStrangleHistory";
            public static final String EMAILS = "emails";
            public static final String BIRTHDAY_REMINDER = "birthday_reminder";
            public static final String OPERATOR = "opr";
            public static final String PREKEY = "e2e_prekey";
        }

        public static final class STICKER {
            public static final String STICKER_COLLECTION_DATA = "stickerCollection";
            public static final String STICKER_COLLECTION_ID = "collectionId";
            public static final String STICKER_COLLECTION_NAME = "collectionName";
            public static final String STICKER_COLLECTION_NUMBER = "numberSticker";
            public static final String STICKER_COLLECTION_AVATAR = "collectionAvatar";
            public static final String STICKER_COLLECTION_TYPE = "type";
            public static final String STICKER_COLLECTION_PREVIEW = "collectionPreview";
            // sticker item
            public static final String STICKER_ITEM_DATA = "stickers";
            public static final String STICKER_ITEM_ID = "stickerId";
            public static final String STICKER_ITEM_TYPE = "type";
            public static final String STICKER_ITEM_IMAGE = "img";
            public static final String STICKER_ITEM_VOICE = "voice";
        }

        public static final class STRANGER_MUSIC {
            public static final String NAME = "name";
            public static final String LAST_AVATAR = "lastChangeAvatar";
            public static final String BIRTHDAY = "birthdayStr";
            public static final String GENDER = "gender";
            public static final String MSISDN = "msisdn";
            public static final String STATUS = "status";
            public static final String IS_STAR = "isStar";
            public static final String HIDE_STRANGER_HISTORY = "hideStrangleHistory";
            public static final String LOCATION_ID = "locationId";
            public static final String MESSAGE_STICKY = "messageSticky";
            public static final String GROUP_ID = "groupId";
            public static final String POSTER_INFO = "posterInfo";
            public static final String SONG_INFO = "songInfo";
            public static final String ACCEPTOR_INFO = "acceptorInfo";
            public static final String ROOM_ID = "roomId";
            public static final String SESSION_ID = "sessionId";
            public static final String CREATE_DATE = "createdDate";
            public static final String STATE = "state";
            public static final String SONG_ID = "id";
            public static final String SONG_NAME = "name";
            public static final String SONG_SINGER = "singer";
            public static final String SONG_IMAGE = "image";
            public static final String SONG_MEDIA_URL = "mediaurl";
            public static final String SONG_URL = "url";
            public static final String SONG_CRBT_CODE = "crbt_code";
            public static final String SONG_CRBT_PRICE = "crbt_price";
            // banner quang cao
            public static final String STRANGER_BANNER = "banner";
            //around
            public static final String STRANGER_DISTANCE_STR = "strDistance";
            public static final String STRANGER_GEO_HASH = "geoHash";
            public static final String STRANGER_AROUND_INFO = "info";
            public static final String STRANGER_AGE = "age";
            // cofide
            public static final String CONFIDE_STATUS = "status";
            public static final String STRANGER_OPERATER = "currOperator";
        }

        public static final class STRANGER_STICKY {
            public static final int TYPE_DEEP_LINK = 1;
            public static final int TYPE_FAKE_MO = 2;
            public static final int TYPE_WEB_VIEW = 3;
            public static final String STICKY_TITLE = "imageTitle";
            public static final String STICKY_IMAGE_URL = "imageUrl";
            public static final String STICKY_TYPE = "type";
            public static final String STICKY_CONFIRM = "confirm";
            public static final String STICKY_ACTION = "action";
        }

        public static final class OFFICER {
            public static final String OFFICER_LIST = "listOffical";
            public static final String SERVER_ID = "serviceId";
            public static final String ALIAS = "alias";
            public static final String AVATAR_URL = "avatar";
            public static final String DESCRIPTION = "description";
            public static final String OFFICIAL_TYPE = "offical_type";
            //room
            public static final String MEMBER_NAME = "name";
            public static final String ROOM_ID = "roomId";
            public static final String JOIN_STATE = "joinState";
            public static final String ROOM_PLAY_LIST = "playList";
            public static final String ROOM_STATE_ONLINE = "roomOnline";
            public static final String ROOM_DESC_SONG = "msgPoster";
            public static final String ROOM_FOLLOW = "follow";
            public static final String ROOM_BACKGROUND = "background";
            //
            public static final String ROOM_FOLLOWS = "follows";
            public static final String ROOM_ORDER = "iOrder";
            public static final String ROOM_GROUP_ID = "groupId";
        }

        public static final class ANONYMOUS {
            public static final String FROM_NAME = "from_nickname";
            public static final String TO_AVATAR = "to_lavatar";// nguoi can thong tin
            public static final String TO_NAME = "to_nickname";
        }

        public static final class LOG_LISTEN {
            public static final String ACTION_TYPE = "actionType"; // = 1
            public static final String ITEM_TYPE = "itemType"; //  1 : song  , 2 : album , 3 : video
            public static final String ITEM_ID = "itemId";
            public static final String CHANNEL = "channel";     //mocha
            public static final String USER_ID = "userId";
            public static final String PASSWORD = "password";
        }

        public static final class ROOM_CHAT {
            public static final String REPORTER = "reporter";
            public static final String REPORTED = "reported";
            public static final String REPORTTYPE = "reportType";
            public static final String REPORTCONTENT = "content";
        }

        public static final class POLL {
            public static final String ID = "id";
            public static final String POLL_DATA = "data";
            public static final String POLL_TITLE = "title";
            public static final String POLL_CHOICE = "choice";
            public static final String POLL_CREATOR = "creator";
            public static final String POLL_INCOGNITO = "anonymous";
            public static final String POLL_CREATED_AT = "createdAt";
            public static final String POLL_EXPIRE_AT = "expireAt";
            public static final String POLL_GROUP_ID = "groupId";
            public static final String POLL_OPTIONS = "options";
            public static final String POLL_ID = "pollId";
            public static final String POLL_OPTION_ID = "optionId";
            public static final String POLL_NEW_IDS = "optionNewIds";
            public static final String POLL_OLD_IDS = "optionOldIds";
            public static final String RESULT = "result";
            public static final String VOTERS = "voters";
            public static final String SELECTED = "selected";
            public static final String POLL_ANONYMOUS = "anonymous";
            public static final String POLL_ENABLE_ADD_VOTE = "enableAddVote";
        }

        public static final class BDSV {
            public static final String MATCH_ID = "match_id";
            public static final String TEAM_ID = "team_id";
            public static final String NUMBER = "number";
            public static final String VIDEO_ID = "id";
            public static final String VIDEO_TYPE = "type";
        }
    }

    public static final class SMART_TEXT {
        public static final int TYPE_NUMBER = 1;
        public static final int TYPE_EMAIL = 2;
        public static final int TYPE_URL = 3;
        public static final int TYPE_MOCHA = 4;
        public static final int MAX_LENGTH_NUMBER = 6;
        public static final int MAX_LENGTH_EMAIL = 6;
        public static final int MAX_LENGTH_URL = 6;
    }

    public static final class CHOOSE_CONTACT {
        public static final int TYPE_CREATE_SOLO = 20;
        public static final int TYPE_CREATE_GROUP = 21;
        public static final int TYPE_INVITE_GROUP = 22;
        public static final int TYPE_SHARE_CONTACT = 23;
        public static final int TYPE_ADD_FAVORITE = 24;
        public static final int TYPE_INVITE_FRIEND = 25;
        public static final int TYPE_FORWARD_CONTENT = 26;
        public static final int TYPE_INVITE_NOPHOSO = 27;
        public static final int TYPE_TOGETHER_MUSIC = 28;
        public static final int TYPE_SHARE_FROM_OTHER_APP = 29;
        public static final int TYPE_BLOCK_NUMBER = 30;
        public static final int TYPE_CREATE_BROADCAST = 31;
        public static final int TYPE_INVITE_BROADCAST = 32;
        public static final int TYPE_INVITE_TO_ROOM = 33;
        public static final int TYPE_INVITE_MAKE_TURN_LW = 34;
        public static final int TYPE_SOS_MAKE_TURN_LW = 35;
        public static final int TYPE_DEEPLINK_CAMPAIGN = 36;
        public static final int TYPE_GUEST_BOOK_ASSIGNED = 37;
        public static final int TYPE_CALL_OUT_FREE_USER = 38;
        public static final int TYPE_CREATE_CHAT = 39;
        public static final int TYPE_BROADCAST_CHANGE_NUMBER = 40;
        public static final int TYPE_SELECT_NUMBER_CALL_FREE_CALLOUT_GUIDE = 41;
        public static final int TYPE_ADD_FRIEND = 43;
        public static final int TYPE_BROADCAST_CHANGE_PREFIX_NUMBER = 44;
        public static final int TYPE_SET_CALLED_LIMITED = 45;
        public static final int TYPE_SET_CALLOUT_FREE = 46;
        public static final int TYPE_CHOOSE_CONTACT_TRANSFER_MONEY = 47;
        public static final int TYPE_CHOOSE_CONTACT_RECHARGE = 48;
        public static final int TYPE_SELFCARE_RECHARGE = 49;
        public static final int TYPE_CREATE_GROUP_SETTING = 50;
        public static final String DATA_TYPE = "action_type";
        public static final String TYPE_SCREEN = "type_screen";
        public static final String DATA_MEMBER = "list_member";
        public static final String DATA_THREAD_ID = "thread_id";
        public static final String DATA_GUEST_BOOK_ID = "guest_book_id";
        public static final String DATA_GUEST_BOOK_PAGE_ID = "guest_book_page_id";
        public static final String RESULT_PHONE_ID = "phone_id";
        public static final String DATA_THREAD_TYPE = "thread_type";
        public static final String DATA_REENG_MESSAGE = "reeng_message";
        public static final String DATA_ARRAY_MESSAGE = "array_message";
        public static final String RESULT_THREAD_ID = "thread_id";
        public static final String RESULT_THREAD_TYPE = "thread_type";
        public static final String RESULT_CONTACT_NAME = "contact_name";
        public static final String RESULT_CONTACT_NUMBER = "contact_number";
        public static final String DATA_THREAD_MESSAGE = "thread_message";
        public static final String DATA_TOGETHER_MUSIC = "data_together_music";
        public static final String DATA_SHOW_SHARE_ONMEDIA = "data_show_share_onmedia";
        public static final String DATA_ROOM_ID = "data_room_id";
        public static final String DATA_DEEPLINK_CAMPAIGN = "deeplink_campaign";
        public static final String DATA_PACKAGE_ID = "package_id";
        public static final String DATA_TYPE_CREATE_GROUP_SETTING = "data_type_create_group_setting";
        public static final String REF_PREFIX_CHANGE = "prefixchange";
        public static final String DATA_MSG = "msg";
        public static final String DATA_AUTO_FORWARD = "auto_forward";
        public static final String DATA_LIST_REENG_MESSAGE = "list_reeng_message";
        public static final String RESULT_LIST_CONTACT = "list_contact";
        public static final String RESULT_CREATE_GROUP = "result_create_group";
        public static final String RESULT_SEARCH_MESSAGER = "result_search_messager";
    }

    public static final class SETTINGS {
        public static final int TRANSLATE = 2;
        public static final int CHANGE_STATUS = 6;
        public static final int LOGOUT = 8;
        public static final int LIST_ROOM_MUSIC = 9;
        public static final int ABOUT = 12;
        public static final int PACKAGE_DETAIL = 13;
        public static final int NEAR_YOU = 14;

        public static final int SETTING_NOTIFICATION = 14;
        public static final int SETTING_PRIVATE = 15;
        public static final int SETTING_CALL_MESSAGE = 16;
        public static final int SETTING_IMAGE_SOUND = 17;
        public static final int NOTE_MESSAGE = 18;
        public static final int SETTING_ACCOUNT = 19;
        public static final int SETTING_CONFIG_TAG = 20;

        public static final int SETTING_CREATE_PIN_HIDE_THREAD = 21;
        public static final int SETTING_HIDE_THREAD = 22;
        public static final int SETTING_OPEN_HIDDEN_THREAD = 23;
        public static final int SETTING_HIDDEN_THREAD = 24;

        // TODO: [START] Cambodia version
        public static final int SETTING_ACCOUNT_SECURITY = 25;
        public static final int SETTING_KH_SCREEN = 26;
        public static final int SHARE_AND_GET_MORE_SCREEN = 27;
        // TODO: [END] Cambodia version

        public static final String DATA_FRAGMENT = "fragment";
        public static final String DATA_THREAD = "thread";
        public static final String RESET_PIN = "reset_pin";
        public static final String DATA_FINGERPRINT_ERROR = "fingerprint_error";
        public static final int ALPHA_SHOW_ACTIONBAR = 200;
    }

    public static final class LOCK_APP {
        public static final int LOCK_APP_SETTING = 1;
        public static final int LOCK_APP_LOCKED = 2;
        public static final int LOCK_APP_CHANGE_OLD_PASS = 3;
        public static final int LOCK_APP_CHANGE_PASS = 4;
        public static final int LOCK_APP_CHANGE_PASS_RE = 5;
        public static final int LOCK_APP_OPEN_SETTING = 6;
        public static final int LOCK_APP_LOCKED_SETTING = 7;

        public static final int LOCK_APP_TIME_NOW = 100;//now
        public static final int LOCK_APP_TIME_5_S = 5000;//10s
        public static final int LOCK_APP_TIME_10_S = 10000;
        public static final int LOCK_APP_TIME_30_S = 30000;
        public static final int LOCK_APP_TIME_1_M = 60000;//60s
    }

    public static final class MORE_APP {
        public static final int STATE_NEW = 0;
        public static final int STATE_PENDDING = 1;
        public static final int STATE_INSTALLED = 2;
        public static final int STATE_SENDING = 3;
        public static final int STATE_UNINSTALL = 4;

        public static final int INSTALL_FLAG = 5;
    }

    public static final class ALARM_MANAGER {
        public static final long PING_TIMER = 60 * 1000L; //90s
        public static final int PING_ID = 13579;
        // ping pong music
        public static final long MUSIC_PING_TIMER = 30000L;
        public static final int MUSIC_PING_ID = 11112;
        public static final int MUSIC_PONG_ID = 11113;
        // timout timer
        public static final int TIME_OUT_RESPONSE_ACTION_MUSIC = 30 * 1000;     // 30s
        public static final int TIME_OUT_RECEIVE_INVITE_MUSIC = 20 * 1000;      // 20s
        public static final long ENTER_BACKGROUND_MAX_TIME = 30 * 60 * 1000;    // 30 mins
        //public static final long ENTER_BACKGROUND_MAX_TIME = 120 * 60 * 1000;    // 30 mins
    }

    public static final class KEENG_MUSIC {
        public static final String SONG_OBJECT = "song_object";
        //
        public static final int NUM_TOP_SONG = 30;
        //cung nghe voi nguoiwf la
        public static final int STRANGE_MUSIC_STATE_WAIT = 5;
        public static final int STRANGE_MUSIC_STATE_ACCEPTED = 10;
        public static final int STRANGE_MUSIC_STATE_TIME_OUT = 15;
        public static final int STRANGE_MUSIC_STATE_STAR_OFF = 20;

    }

    public static final class PATTERN {

        public static final String PHIM_KEENG = "phim.keeng.vn";
        public static final String KEENG_VN = "keeng.vn";
        public static final String KEENG_NET = "keeng.net";
        public static final String NETNEWS = "netnews.vn";

        public static final String MV_KEENG_VN = "keeng.vn/video/";
        public static final String MV_KEENG_NET = "keeng.net/video/";
        public static final String AUDIO_KEENG_VN = "keeng.vn/audio/";
        public static final String AUDIO_KEENG_NET = "keeng.net/audio/";
        public static final String ALBUM_KEENG_VN = "keeng.vn/album/";
        public static final String ALBUM_KEENG_NET = "keeng.net/album/";
        public static final String ALBUM_VIDEO_KEENG_VN = "keeng.vn/album-video/";
        public static final String ALBUM_VIDEO_KEENG_NET = "keeng.net/album-video/";
        public static final String PLAYLIST_KEENG_VN = "keeng.vn/list-hot/";
        public static final String PLAYLIST_KEENG_NET = "keeng.net/list-hot/";

        public static final String PATTERN_SALES_MOVIES = "-[t|a|b|c|d|e|g|h|i|k|l][0-9]{6,7}.html|-tf[0-9]{6,7}.html|-ft[0-9]{6,7}.html";
        public static final String PATTERN_SALES_MUSIC = "\\/ga.html|\\/fb.html|\\/fb2.html";
        public static final String KEY_SEARCH_REPLACE = "[^\\dA-Za-z]";
    }

    public static final class PLAY_MUSIC {
        public static final int REPEAT_All = 0;
        public static final int REPEAT_ONE = 1;

        public static final int REPEAT_SUFF_OFF = 0;
        public static final int REPEAT_SUFF = 1;

        //
        public static final int PLAYING_NONE = 0;
        public static final int PLAYING_GET_INFO = 1;
        public static final int PLAYING_RETRIEVING = 2;
        public static final int PLAYING_PREPARING = 3;
        public static final int PLAYING_PREPARED = 4;
        public static final int PLAYING_PLAYING = 5;
        public static final int PLAYING_PAUSED = 6;
        public static final int PLAYING_CLOSED = 7;
        public static final int PLAYING_BUFFERING_START = 8;
        public static final int PLAYING_BUFFERING_END = 9;
        //
        public static final String ACTION_BROADCAST = "com.metfone.selfcare.keeng";// dung de xu ly play, next bai tu
        // widget
        public static final String ACTION_NAME = "com.metfone.selfcare.action";
        //
        public static final int ACTION_VALUE_DEFAULT = -10;//dung cho notification, tranh trung voi case cua tele
        public static final int ACTION_VALUE_NEXT = 1;//dung cho notification
        public static final int ACTION_VALUE_PAUSE_PLAY = 2;
        public static final int ACTION_VALUE_PREV = 3;
        public static final int ACTION_VALUE_CLOSE = 4;
        public static final int ACTION_VALUE_SWIPE_DISMISS = 5;
        //
        public static final int MUSIC_RETRY_MAX_COUNT = 5;
        public static final long MUSIC_RETRY_DELAY_TIME = 2000;//5s
    }

    // stranger music
    public static final class STRANGER_MUSIC {
        public static final int TYPE_MUSIC = 1;
        public static final int TYPE_AROUND = 2;
        public static final int TYPE_CONFIDE = 3;

        public static final String OPTION_FILTER_NEAR_YOU = "near_you_option_filter";
        public static final String FILTER_LOCATION = "stranger_filter_location";
        public static final String FILTER_SEX = "stranger_filter_sex";
        public static final String FILTER_AGE_MIN = "stranger_filter_age_min";
        public static final String FILTER_AGE_MAX = "stranger_filter_age_max";
        public static final String HIDE_LOCATION = "stranger_hide_location";
        public static final long TIMER_CHECK_REFRESH = 30 * 1000;// 30s
        public static final long MIM_NEW_ROOM = 3;
        public static final long TIME_OUT_UN_SUB_STRANGER = 2 * 60 * 1000;//2 phut
        public static final long TIME_OUT_UN_SUB_ROOM_CHAT = 1 * 60 * 1000;//1 phut
        public static final long TIME_OUT_UN_SUB_CONNECTION_FEEDS = 1 * 60 * 1000;//1 phut
        private static final int[] OPTION_FILTER_LIST = {
                MENU.STRANGER_FILTER_MALE_FEMALE,
                MENU.STRANGER_FILTER_MALE,
                MENU.STRANGER_FILTER_FEMALE
                // MENU.STRANGER_FILTER_STAR
        };

        public static int[] getOptionFilterList() {
            return OPTION_FILTER_LIST;
        }
    }

    public static final class IMAGE_CACHE {
        public static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.RGB_565;
        public static final int BITMAP_DISK_CACHE_DEFAULT_SIZE = 1024 * 1024 * 50;// 50Mb
        public static final int BITMAP_DISK_CACHE_EXTRA_SIZE = 1024 * 1024 * 200;// 200Mb. cho may co man hinh lon
    }

    public static final class GAME {
        public static final int LIST_GAME = -1;
        public static final int VQMM_ID = 0;
        public static final int ACCUMULATE_ID = 1;
        public static final String ID_GAME = "id_game";
        public static final int LOG_DOWNLOAD = 1;
        public static final int LOG_OPEN = 2;
        public static final int LOG_ALREADY_DOWNLOAD = 3;

    }

    public static final class BACKGROUND {
        public static final String KEY_CHAT_SCREEN = "CHANGE_BACKGROUND_IN_CHAT_SCREEN";
        public static final String KEY_IS_APPPLY_ALL = "CHANGE_BACKGROUND_APPLY_ALL";
        public static final String FROM_SERVER = "CHANGE_BACKGROUND_FROM_SERVER";
        public static final String FROM_DEVICE = "CHANGE_BACKGROUND_FROM_DEVICE";
    }

    public static final class VIP {
        protected static final String[] NUMBERS = {"01648697568", "0983100500", "0982198301",
                "01649726619", "01685456809", "0979218996"};
        protected static final List<String> LIST_NUMBER = Arrays.asList(NUMBERS);

        public static List<String> getListNumber() {
            return LIST_NUMBER;
        }
    }

    public static final class HOME {
        public static final String TAB_CONTACT = "HOME_TAB_CONTACT";
        public static final String TAB_ENUM = "HOME_TAB_ENUM";
        public static final String NEED_SHOW_NOTIFY = "NEED_SHOW_NOTIFY";
        public static final String ID_TAB_WAP = "ID_TAB_WAP";
        public static final String INDEX_SUB_TAB_VIDEO = "INDEX_SUB_TAB_VIDEO";

        public static final class FUNCTION {
            public static final int DEEPLINK = 0;
            public static final int CALLOUT = 1;
            public static final int AVNO = 2;
            public static final int QR = 3;
            public static final int INVITE_FRIEND = 4;
            public static final int LUCKY_WHEEL = 5;
            public static final int SPOINT = 6;
            public static final int GAME = 7;
            public static final int NEAR_FRIEND = 8;
            public static final int FEED_BACK = 9;
            public static final int REGISTER_VIP = 10;
            public static final int PERSONAL = 11;
            public static final int TAB_VIDEO = 12;
            public static final int TAB_STRANGER = 13;
            public static final int TAB_HOT = 14;
            public static final int TAB_MUSIC = 15;
            public static final int TAB_MOVIE = 16;
            public static final int TAB_NEWS = 17;
            public static final int TAB_SECURITY = 18;
            public static final int TAB_SAVING_STATISTICS = 19;
            public static final int TAB_SAVING_SEARCH = 20;
            public static final int TAB_SAVING_HISTORY_DETAIL = 21;
            public static final int TAB_WAPVIEW = 22;
            public static final int LIST_DATA_PACKAGES = 23;
            public static final int TAB_TIIN = 24;
            public static final int TAB_MY_VIETTEL = 25;
            public static final int TAB_SELFCARE = 26;
            public static final int TAB_DATA_CHALLENGE = 27;
            public static final int TAB_UMONEY = 28;
        }
    }

    public static final class ONMEDIA {
        public static final int ONE_SECOND = 1000;
        public static final int DELAY_UPDATE_SEEKBAR = 200;
        public static final int LIST_LIKE = 2;
        //May cai nay ko dung nua, xoa di cho de nhin
        /*public static final int SETTING = 3;
        public static final int SETTING_CONTENT = 4;
        public static final int SUGGEST_FOLLOW = 5;
        public static final int NOT_SHOW_CONTENT = 6;
        public static final int SETTING_PRIVATE = 7;*/
        public static final int ALBUM_DETAIL = 8;
        public static final int COMMENT = 9;
        public static final int WRITE_STATUS = 10; //ko dung nua
        public static final int LIST_SHARE = 11;
        public static final int DISCOVERY = 13;
        public static final int NOTIFICATION = 14;
        public static final int REPLY_COMMENT = 15;

        //action write status
        public static final int ACTION_ATTACH_IMAGE = 1;
        public static final int ACTION_ATTACH_LINK = 2;
        // extra key
        public static final String EXTRAS_DATA = "data";
        public static final String EXTRAS_FEEDS_DATA = "feeds_data";
        public static final String EXTRAS_CONTENT_DATA = "content_data";
        public static final String EXTRAS_NOTIFY_DATA = "notify_data";
        public static final String EXTRAS_TYPE_FRAGMENT = "type_fragment";
        public static final String EXTRAS_EDIT_FEED = "edit_feed";
        public static final String EXTRAS_ROW_ID = "row_id";
        public static final String EXTRAS_SHOW_PREVIEW = "show_preview";
        public static final String EXTRAS_SHOW_MENU_COPY = "show_menu_copy";
        public static final String EXTRAS_FEED_TYPE = "feed_type";
        public static final String EXTRAS_DELTA_TIME = "delta_time";
        public static final String EXTRAS_ACTION = "action_type";
        public static final String EXTRAS_GET_DETAIL_URL = "get_detail_url";
        public static final String EXTRAS_WEBVIEW_FULLSCREEN = "webview_fullscreen";
        public static final String EXTRAS_WEBVIEW_SPONSOR = "webview_sponsor";
        public static final String EXTRAS_WEBVIEW_CONFIRM_BACK = "webview_confirm_back";
        public static final String EXTRAS_WEBVIEW_ORIENTATION = "webview_orientation";
        public static final String EXTRAS_WEBVIEW_PAYMENT = "webview_payment";
        public static final String EXTRAS_WEBVIEW_TITLE = "webview_title";
        public static final String EXTRAS_WEBVIEW_ON_MEDIA = "webview_on_media";
        public static final String EXTRAS_WEBVIEW_CLOSE = "webview_close";
        public static final String EXTRAS_WEBVIEW_EXPAND = "webview_expand";

        public static final String EXTRAS_SPONSOR_ID = "sponsor_id";
        public static final String EXTRAS_SPONSOR_TIME_OUT = "sponsor_time_out";
        public static final String EXTRAS_SPONSOR_AMOUNT = "sponsor_amount";
        public static final String DATA_INDEX_VIDEO_DISCOVERY = "index_video_discovery";
        public static final String EXTRAS_LIST_IMAGE = "list_image";
        public static final String EXTRAS_FEED_ACTION = "feed_action";
        public static final String EXTRAS_IS_PLAY_SONG = "is_play_song";
        public static final String EXTRAS_URL_SUB_COMMENT = "url_sub_comment";
        public static final String EXTRAS_NEED_SHOW_KEYBOARD = "need_show_keyboard";

        public static final int FEED_HOME_TIMELINE = 0;
        public static final int FEED_PROFILE = 1;
        public static final int FEED_CONTACT_DETAIL = 2;
        public static final int FEED_IMAGE = 3;
        public static final int FEED_TAB_VIDEO = 4;
        public static final int FEED_SONG = 5;
        public static final int FEED_FROM_PUSH = 6;
        public static final int FEED_TAB_MUSIC = 7;
        public static final int FEED_TAB_NEWS = 8;

        public static final long TIME_DELAY_HIDE_PLAYER_VIDEO = 3 * 1000;

        public static final String OFFICIAL_ID = "official_id";
        public static final String OFFICIAL_NAME = "official_name";
        public static final String OFFICIAL_AVATAR = "official_avatar";
        public static final String OFFICIAL_USER_TYPE = "official_user_type";

        public static final String OFFICIAL_CHAT_ID = "id";
        public static final String OFFICIAL_CHAT_NAME = "name";
        public static final String OFFICIAL_CHAT_AVATAR = "avatar";

        public class PARAM_IMAGE {
            public static final int FEED_FROM_TAB_HOT = 0;
            public static final int FEED_FROM_MY_PROFILE = 1;
            public static final int FEED_FROM_FRIEND_PROFILE = 2;
            public static final int FEED_FROM_HASHMAP = 3;

            public static final String LIST_IMAGE = "list_image";
            public static final String POSITION = "position";
            public static final String MSISDN = "msisdn";
            public static final String NAME = "name";
            public static final String ITEM_TYPE = "item_type";
            public static final String FEED_FROM = "feed_from";
            public static final String IS_OPEN_PROFILE = "isOpenProfile";
        }

        public class LOG_CLICK {
            public static final String CLICK_MOCHA_VIDEO = "mochavideo";
            public static final String CLICK_WAP_VIEW = "wapview";
            public static final String CLICK_ONMEDIA = "onmedia";
            public static final String CLICK_TAB_KEENG = "MC_MUSIC";
        }

        public class TYPE_RESET_NOTIFY {
            public static final String TYPE_ONMEDIA = "onmedia";
            public static final String TYPE_SUGGEST = "req_friend";
        }
    }

    public static final class MOCHA_INTENT {
        public static final String INTENT_TYPE = "text/plain";
        public static final String ACTION_OPEN_FROM_FACEBOOK = "com.facebook.application";
        public static final String ACTION_OPEN_CHAT = "com.metfone.selfcare.OPEN_CHAT";
        public static final String EXTRA_INTENT_FRIEND_NUMBER = "friend_number"; //receiver phone
        public static final String EXTRA_INTENT_APP_ID = "app_id";
        public static final String EXTRA_INTENT_ACTION = "action";
        public static final String EXTRA_INTENT_SENDER_NUMBER = "sender_number"; //sender phone
        public static final String EXTRA_INTENT_SONG_MODEL = "song_model";
        //
        public static final String ACTION_CHAT = "chat";
        public static final String ACTION_TOGETHER_MUSIC = "together_music";
        // share account
        public static final String ACTION_SHARE_ACCOUNT = "com.metfone.selfcare.SHARE_ACCOUNT";
        public static final String EXTRA_INTENT_SHARE_ACCOUNT = "reeng_account";
        public static final String EXTRA_INTENT_APP_NAME = "app_name";
        public static final String EXTRA_INTENT_APP_ICON = "app_icon";

        public static final String EXTRA_INTENT_SEARCH_USER = "search_user";
    }

    public static final class PACKET_NAME {
        public static final String GG_PLAY_SERVICE = "com.google.android.gms";
    }

    public static final class UPLOAD {
        public static final int HTTP_STATE_UPLOAD_FAILED_EXCEEDED = 2;
        public static final int HTTP_STATE_UPLOAD_SUCCESS = 3;
    }

    public static final class TYPE_MUSIC {
        //type
        public static final int TYPE_PERSON_CHAT = 0;
        public static final int TYPE_GROUP_CHAT = 1;
        public static final int TYPE_ROOM_CHAT = 3;
        public static final int TYPE_STRANGER = 5;
    }

    public static final class STICKER {
        public static final String FRAGMENT_TYPE = "fragment_type";
        public static final int STORE = 0;
        public static final int COLLECTION = 1;
    }

    public static final class INTRO {
        public static final long INTRO_NOT_SHOW_YET = -2L;
        public static final long INTRO_HAS_FINISH_SHOW = -1L;
        public static final String CLICK_TO_PASTE_LINK = "CLICK_TO_PASTE_LINK";
        public static final String MUSIC_OFFLINE = "MUSIC_OFFLINE";
        public static final String INVITE_MUSIC = "INVITE_MUSIC";
        public static final String CLICK_TO_INVITE_STRANGER = "CLICK_TO_INVITE_STRANGER";
    }

    public static final class LUCKY_WHEEL {
        public static final int ITEM_MONEY_100 = 4;
        public static final int ITEM_MOCHA_VIP_1 = 5;
        //public static final int ITEM_KEENG_VIP = 6;
        public static final int ITEM_MONEY_10 = 7;
        public static final int ITEM_MOCHA_VIP_7 = 8;
        //public static final int ITEM_MORE_TURN = 9;
        public static final int ITEM_INVITE_FRIEND = 10;
        public static final int ITEM_SHARE_ONMEDIA = 11;
        //public static final int ITEM_MONEY_20 = 12;
        public static final int ITEM_MAKE_FRIEND = 13;
        //public static final int ITEM_TIIN_VIP = 14;
        public static final int ITEM_POINT_100 = 22;
        public static final int ITEM_POINT_500 = 21;
        public static final int ITEM_STATUS = 20;
        public static final int ITEM_BUDGET_GOLD = 25;

        public static final int ITEM_LISTEN_STRANGER = 0;
        public static final int ITEM_POINT_1000 = 1;
        public static final int ITEM_UPLOAD_ALBUM = 2;
        public static final int ITEM_SECRET = 3;

        public static final int ITEM_SMS_OUT = 15;
        public static final int ITEM_CALL_OUT = 26;
        public static final int ITEM_VIDEO_CALL = 27;
        public static final int ITEM_TALK_STRANGER = 28;
        public static final int ITEM_GROUP_SURVEY = 29;
        public static final int ITEM_LISTEN_MUSIC_KEENG = 30;
        public static final int ITEM_WATCH_VIDEO = 31;
        public static final int ITEM_POINT_2000 = 33;

        public static boolean containMission(int item) {
//            return item == ITEM_LISTEN_STRANGER || item == ITEM_UPLOAD_ALBUM ||
//                    item == ITEM_INVITE_FRIEND || item == ITEM_SHARE_ONMEDIA ||
//                    item == ITEM_MAKE_FRIEND || item == ITEM_STATUS;
            return item == ITEM_LISTEN_STRANGER || item == ITEM_UPLOAD_ALBUM ||
                    item == ITEM_INVITE_FRIEND || item == ITEM_SHARE_ONMEDIA ||
                    item == ITEM_MAKE_FRIEND || item == ITEM_STATUS ||
                    item == ITEM_SMS_OUT || item == ITEM_CALL_OUT || item == ITEM_VIDEO_CALL || item == ITEM_WATCH_VIDEO ||
                    item == ITEM_TALK_STRANGER || item == ITEM_GROUP_SURVEY || item == ITEM_LISTEN_MUSIC_KEENG;
        }

        public static boolean containMissionV2(int item) {
            return item == ITEM_LISTEN_STRANGER || item == ITEM_UPLOAD_ALBUM ||
                    item == ITEM_INVITE_FRIEND || item == ITEM_SHARE_ONMEDIA ||
                    item == ITEM_MAKE_FRIEND || item == ITEM_STATUS ||
                    item == ITEM_SMS_OUT || item == ITEM_CALL_OUT || item == ITEM_VIDEO_CALL || item == ITEM_WATCH_VIDEO ||
                    item == ITEM_TALK_STRANGER || item == ITEM_GROUP_SURVEY || item == ITEM_LISTEN_MUSIC_KEENG ||
                    item == ITEM_POINT_100 || item == ITEM_POINT_500 || item == ITEM_POINT_1000 || item == ITEM_POINT_2000 || item == ITEM_BUDGET_GOLD || item == ITEM_SECRET;
        }

        public static boolean containItemShareFacebook(int item) {
            return item == ITEM_POINT_100 ||
                    item == ITEM_POINT_500 ||
                    item == ITEM_POINT_1000 ||
                    item == ITEM_POINT_2000 ||
                    item == ITEM_BUDGET_GOLD || item == ITEM_SECRET;
        }

    }

    public static final class QR_CODE {
        public static final int TYPE_DISCOUNT = 1;
        public static final int TYPE_DEEPLINK = 2;
        public static final int TYPE_USER_INFO = 3;
        public static final int TYPE_RECHARGE = 4;

        public static final int FROM_NONE = 0;
        public static final int FROM_MVT_RECHARGE = 1;
        public static final int FROM_MVT_RECHARGE_HOME = 2;

        public static final String RESULT_SCAN_QR = "RESULT_SCAN_QR";
    }

    public static final class GUEST_BOOK {
        public static final int MIN_PAGE_PRINT = 10;
        public static final int MAX_PAGES = 50;
        public static final String TEMPLATE_ID = "template_id";
        public static final String TEMPLATE_NAME = "template_name";
        public static final String BOOK_ID = "book_id";
        public static final String MAIN_FRAGMENT_TYPE = "main_fragment_type";
        public static final String MAIN_FRAGMENT_NUMBER = "main_fragment_number";
        public static final String MAIN_FRAGMENT_NAME = "main_fragment_name";

        public static final String VOTE_DETAIL_ISVOTED = "vote_detail_voted";
        public static final String VOTE_DETAIL_TOTAL_VOTE = "vote_detail_total_vote";

        public static final int MAIN_TYPE_BOOKS = 1;
        public static final int MAIN_TYPE_VOTES = 2;

        public static final String TYPE_IMAGE = "image";
        public static final String TYPE_TEXT = "text";

        public static final int HOLDER_BACKGROUND = 0;
        public static final int HOLDER_TEMPLATE = 1;
        //
        public static final int HOLDER_MAIN_BOOK = 0;
        public static final int HOLDER_MAIN_ADD_BOOK = 1;
        public static final int HOLDER_MAIN_DIVIDER = 2;
        public static final int HOLDER_MAIN_PAGE = 3;

        public static final String INVITE_GUEST_BOOK_CAMPAIGN_ID = "12";

        public static final int TIMER_CHANGE_PAGE = 5 * 1000;   //5s
        public static final int BANNER_TYPE_HOME = 1;
        public static final int BANNER_TYPE_TAB_VOTE = 2;
        public static final int BANNER_TYPE_HOME_FRIEND = 3;
        public static final int BANNER_TYPE_NON = 0;
        public static final String LINK_BANNER_VOTE = "http://mocha.com.vn/luubut.html";
        public static final String LINK_GUIDE = "http://mocha.com.vn/hdlb.html";

        public static final String VOTE_TAB_TYPE = "vote_tab_type";
        public static final int VOTE_TAB_WEEK = 0;
        public static final int VOTE_TAB_TOTAL = 1;
    }

    public static final class GUEST_BOOK_STATUS {
        public static final int STATE_NEW = 0;
        public static final int STATE_LOCK = 1;
        public static final int STATE_PROCESSING = 2;
        public static final int STATE_DELETE = 3;
        public static final int STATE_PRINT = 4;
        public static final int STATE_UN_LOCK = 5;
    }

    public static final class GUEST_BOOK_SAVE {
        public static final int UPDATE_BOOK = 0;
        public static final int UPDATE_PAGE = 1;
        public static final int UPDATE_BOOK_INFO = 2;
    }

    public static final class DOCUMENT {
        public static final String DOCUMENT_DATA = "document_data";
        public static final String GROUP_ID = "group_id";
    }

    public static final class PROFILE {
        public static final String TYPE_PROFILE = "type_profile";
        public static final int TYPE_MY_PROFILE = 0;
        public static final int TYPE_PHONENUMBER_PROFILE = 1;
        public static final int TYPE_STRANGER_PROFILE = 2;
        public static final int TYPE_OFFICIAL_PROFILE = 3;
    }

    public static final class ADS_POS {
        public static final int FIRST = 0;
        public static final int MID = 1;
        public static final int END = 2;
    }

    public static final class FONT_SIZE {
        public static final float SMALL_RATIO = 0.85f;
        public static final float MEDIUM_RATIO = 1.0f;
        public static final float LARGE_RATIO = 1.2f;
        public static final int SMALL = 1;
        public static final int MEDIUM = 2;
        public static final int LARGE = 3;

        public static final int LEVEL_0 = 1;
        public static final int LEVEL_1 = 2;
        public static final int LEVEL_1_5 = 3;
        public static final int LEVEL_2 = 4;
        public static final int LEVEL_2_5 = 5;
        public static final int LEVEL_3 = 6;
        public static final int LEVEL_4 = 7;

        public static final int LEVEL_6 = 9;
    }

    public static final class SieuHai {
        public static final String SITE_SIEUHAI = "sieuhai.tv";

        public enum SieuHaiEnum {
            LogStreaming, log5s, log30s
        }
    }

    public static final class TabVideo {
        public static final int UPLOAD_YOUTUBE = 0;
        public static final int UPLOAD_LIBRARY = 1;
        public static final String CATEGORY_ID = "categoryId";
        public static final String POSITION = "position";
        public static final String OPEN_CHANNEL_SUBSCRIBE = "openChannelSubscribe";
        public static final String ACTION = "action";
        public static final String OPEN_DISCOVER = "openDiscover";
        public static final String CLASS_NAME = "className";
        public static final String INTERNET = "internet";
        public static final String STATUS = "status";
        public static final String TAB_HOME_FIRST_CLICK = "tabHomeFirstClick";
        public static final String TAB_HOME_REFRESH_CLICK = "tabHomeRefreshClick";
        public static final String TAB_CATEGORY = "tabCategory";
        public static final String LIKE = "like";
        public static final String SUBSCRIBE = "subscribe";
        public static final String SHARE = "share";
        public static final String TAB_CATEGORY_REFRESH = "TAB_CATEGORY_REFRESH";
        public static final String VIDEO = "VIDEO";
        public static final String MOVIE = "MOVIE";
        public static final String ARR_EPISODES_MOVIE = "ARR_EPISODES_MOVIE";
        public static final String CURRENT_POSITION = "CURRENT_POSITION";
        public static final String TYPE = "TYPE";
        public static final String PAUSE_VIDEO_SERVICE = "com.metfone.selfcare.ui.tabvideo.service.PAUSE_VIDEO_SERVICE";
        public static final String PLAYER_TAG = "PLAYER_TAG";
        public static final String SEARCH_VIDEO = "search";
        public static final String SEARCH_CHANNEL = "searchChannel";
        public static final String SEARCH_FILM = "searchFilm";
        public static final String TAG_FILM_GET_HOME_DATA = "getHomeData";
        public static final String TAG_FILM_GET_NATION = "getNation";
        public static final String TAG_FILM_GET_KIND = "getKind";
        public static final String VIDEO_SERIES = "videoSeries";
        public static final String VIDEO_DETAIL = "videoDetail";
        public static final String VIDEO_WORLD_CUP = "videoWorldCup";
        public static final String COMMENT = "comment";
        public static final String IMAGE = "image";
        public static final String WATCH_LATER = "watchLater";
        public static final String PLAYER_INIT = "initPlayer";
        public static final String PLAY_FROM_SERVICE = "play_from_service";
        public static final String MP4 = "mp4";
        public static final String MP4_DRM = "mp4_drm";
        public static final String LIVE_WC = "livewc_vtv";
        public static final String LIVE_WC_VAS = "livewc_vt";
        public static final String VIP = "VIP";
        public static final String NOVIP = "NOVIP";
        public static final String SAVE = "save";
        public static final String SUBSCRIPTION = "subscription";
        public static final String PLAY = "play";
        public static final String WATCH_VIDEO = "watchVideo";
        public static final String UPDATE = "update";
        public static final String WHITE_LIST_DEVICE = "WHITE_LIST_DEVICE";
        public static final String ENABLE_NOTIFY_UPLOAD_VIDEO_ON_MEDIA = "ENABLE_NOTIFY_UPLOAD_VIDEO_ON_MEDIA";
        public static final String GET_CHANNEL_INFO = "GET_CHANNEL_INFO";
        public static final String TIME_GET_CHANNEL_INFO = "TIME_GET_CHANNEL_INFO";
        public static final String MOVIE_DETAIL = "MovieDetail";

        public static final String CACHE_LAST_ID = "CACHE_LAST_ID";
        public static final String CACHE_CATEGORIES = "CACHE_CATEGORIES";
        public static final String CACHE_MY_CHANNEL_INFO = "CACHE_MY_CHANNEL_INFO";
        public static final String CACHE_ADS_REGISTER_VIPS = "CACHE_ADS_REGISTER_VIPS";
        public static final String CACHE_VIDEO_BANNER_ITEM = "CACHE_VIDEO_BANNER_ITEM";
        public static final String CACHE_GET_VIDEOS_BY_CATEGORY = "CACHE_GET_VIDEOS_BY_CATEGORY";
        public static final String CACHE_GET_MOVIES_BY_CATEGORY = "CACHE_GET_MOVIES_BY_CATEGORY";
        public static final String CACHE_IDS = "CACHE_IDS";
        public static final String CACHE_GET_TOP_CHANNEL_BY_CATEGORY = "CACHE_GET_TOP_CHANNEL_BY_CATEGORY";
        public static final String LIST_DATA_VIDEO = "LIST_DATA_VIDEO";
    }

    public static final class LOG_SOURCE_TYPE {

        public static final String TYPE_MOCHA_VIDEO = "MC_VIDEO";
        public static final String TYPE_KEENG_MOVIES = "MC_KEMOVICES";
        public static final String TYPE_KEENG_VIDEO = "MC_KEMUSIC";
        public static final String TYPE_NEWS_VIDEO = "MC_NEWS_VIDEO";
        public static final String TYPE_TIIN_VIDEO = "MC_TIIN_VIDEO";
        public static final String TYPE_NEWS = "MC_NEWS";
        public static final String TYPE_TIIN = "MC_TIIN";
        public static final String TYPE_PUSH = "MC_PUSH";
    }

    public static final class LogKQI {

        public static final String VIDEO_GET_CATEGORY = "VIDEO_GET_CATEGORY";
        public static final String VIDEO_LIST_BY_CATEGORY = "VIDEO_LIST_BY_CATEGORY";
        public static final String VIDEO_GET_DETAIL = "VIDEO_GET_DETAIL";
        public static final String VIDEO_GET_RELATE = "VIDEO_GET_RELATE";
        public static final String VIDEO_SEARCH = "VIDEO_SEARCH";
        public static final String VIDEO_LIST_BY_CHANNEL = "VIDEO_LIST_BY_CHANNEL";
        public static final String VIDEO_UPLOAD_FILE = "VIDEO_UPLOAD_FILE";
        public static final String VIDEO_UPLOAD_YOUTUBE = "VIDEO_UPLOAD_YOUTUBE";
        public static final String VIDEO_UPLOAD_INFO = "VIDEO_UPLOAD_INFO";
        public static final String MOCHA_VIDEO_BUFFER = "MOCHA_VIDEO_BUFFER";
        public static final String MOCHA_SONG_BUFFER = "MOCHA_SONG_BUFFER";

        public static final String ONMEDIA_GET_FEED = "ONMEDIA_GET_FEED";
        public static final String ONMEDIA_GET_USER_TIMELINE = "ONMEDIA_GET_USER_TIMELINE";
        public static final String ONMEDIA_GET_COMMENT = "ONMEDIA_GET_COMMENT";

        public static final String STRANGER_GET_LIST_STRAGER_MUSIC = "STRANGER_GET_LIST_STRAGER_MUSIC";
        public static final String STRANGER_GET_LIST_STRAGER_TALK = "STRANGER_GET_LIST_STRAGER_TALK";

        public static final String LOGIN_BY_TOKEN = "LOGIN_BY_TOKEN";
        public static final String LOGIN_BY_CODE = "LOGIN_BY_CODE";
        public static final String MESSAGE_SEND_RECEIVE = "MESSAGE_SEND_RECEIVE";
        public static final String UPLOAD_IMAGE_CHAT = "UPLOAD_IMAGE_CHAT";
        public static final String DOWNLOAD_IMAGE_CHAT = "DOWNLOAD_IMAGE_CHAT";
        public static final String CHAT_DRAW_VIEW = "CHAT_DRAW_VIEW";

        public static final String MUSIC_GET_HOME = "MUSIC_GET_HOME";
        public static final String MUSIC_GET_SONG_BUFFER = "MUSIC_GET_SONG_BUFFER";
        public static final String MUSIC_GET_VIDEO_BUFFER = "MUSIC_GET_VIDEO_BUFFER";
        public static final String MUSIC_SEARCH = "MUSIC_SEARCH";

        public static final String MOVIE_GET_HOME = "MOVIE_GET_HOME";
        public static final String MOVIE_GET_BUFFER = "MOVIE_GET_BUFFER";
        public static final String MOVIE_SEARCH = "MOVIE_SEARCH";
        public static final String MOVIE_SEARCH_CINEMA = "MOVIE_SEARCH_CINEMA";
        public static final String MOVIE_GET_DETAIL = "MOVIE_GET_DETAIL";
        public static final String MOVIE_GET_CATEGORY = "MOVIE_GET_CATEGORY";
        public static final String CINEMA_GET_COUNTRY = "CINEMA_GET_COUNTRY";
        public static final String CINEMA_GET_CATEGORY = "CINEMA_GET_CATEGORY";

        public static final String NEWS_GET_HOME = "NEWS_GET_HOME";
        public static final String NEWS_SEARCH = "NEWS_SEARCH";
        public static final String NEWS_GET_VIDEO_BUFFER = "NEWS_GET_VIDEO_BUFFER";
        public static final String NEWS_GET_DETAIL = "NEWS_GET_DETAIL";
        public static final String NEWS_GET_CATEGORY = "NEWS_GET_CATEGORY";
        public static final String NEWS_GET_NEWS_BY_CATEGORY = "NEWS_GET_NEWS_BY_CATEGORY";
        public static final String NEWS_GET_RELATE = "NEWS_GET_RELATE";
        public static final String NEWS_GET_RELATE_CATEGORY = "NEWS_GET_RELATE_MOST";

        public static final String TIIN_GET_HOME = "TIIN_GET_HOME";
        public static final String TIIN_GET_LIST_CATEGORY = "TIIN_GET_LIST_CATEGORY";
        public static final String TIIN_GET_CATEGORY = "TIIN_GET_CATEGORY";
        public static final String TIIN_SEARCH = "TIIN_SEARCH";
        public static final String TIIN_GET_VIDEO_BUFFER = "TIIN_GET_VIDEO_BUFFER";
        public static final String TIIN_GET_DETAIL = "TIIN_GET_DETAIL";
        public static final String TIIN_GET_TIIN_BY_CATEGORY = "TIIN_GET_TIIN_BY_CATEGORY";
        public static final String TIIN_GET_EVENT = "TIIN_GET_EVENT";
        public static final String TIIN_GET_MOST_VIEW = "TIIN_GET_MOST_VIEW";
        public static final String TIIN_GET_RELATE = "TIIN_GET_RELATE";
        public static final String TIIN_GET_SIBLING = "TIIN_GET_SIBLING";
        public static final String TIIN_GET_SIBLING_CATEGORY = "TIIN_GET_SIBLING_CATEGORY";

        public enum StateKQI {
            SUCCESS("200"),
            ERROR_TIMEOUT("408"),
            ERROR_EXCEPTION("-1"),
            VIDEO_DETAIL_NULL("-2"),
            UPLOAD_FILE_NOT_FOUND("-3"),
            UPLOAD_FILE_FAIL("-4");

            private String value;

            StateKQI(String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }

        }

    }

    public static class TabMovies {
        //public static final String TAG_GET_HOME = "MOVIES_GET_HOME";
        public static final String TAG_GET_NEW = "MOVIES_GET_NEW";
        public static final String TAG_GET_HOT = "MOVIES_GET_HOT";
        public static final String TAG_GET_LIST_GROUPS = "MOVIES_GET_LIST_GROUPS";
        //public static final String TAG_GET_COUNTRY = "MOVIES_GET_COUNTRY";
        //public static final String TAG_GET_TOP_COUNTRY = "MOVIES_GET_TOP_COUNTRY";
        //public static final String TAG_GET_CATEGORY = "MOVIES_GET_CATEGORY";
        public static final String TAG_GET_TOP_CATEGORY = "MOVIES_GET_TOP_CATEGORY";
        //public static final String TAG_GET_DETAIL_FILM = "MOVIES_GET_DETAIL_FILM";
        //public static final String TAG_GET_LIST_GROUPS_DETAIL = "MOVIES_GET_LIST_GROUPS_DETAIL";
        //public static final String TAG_GET_RELATE_FILM = "MOVIES_GET_RELATE_FILM";
        //public static final String TAG_GET_TOP_KEYWORD = "MOVIES_GET_TOP_KEYWORD";
        //public static final String TAG_INSERT_LOG_WATCHED = "MOVIES_INSERT_LOG_WATCHED";
        public static final String TAG_GET_LIST_LOG_WATCHED = "MOVIES_GET_LIST_LOG_WATCHED";
        //public static final String TAG_DELETE_LOG_WATCHED = "MOVIES_DELETE_LOG_WATCHED";
        public static final String TAG_GET_LIST_FILM_USER_LIKED = "TAG_GET_LIST_FILM_USER_LIKED";
        //public static final String TAG_DELETE_LIST_FILM_USER_LIKED = "MOVIES_DELETE_LIST_FILM_USER_LIKED";
        //public static final String TAG_GET_LIST_SUBTAB = "MOVIES_GET_LIST_SUBTAB";
        //public static final String TAG_GET_HOME_OF_SUBTAB = "MOVIES_GET_HOME_OF_SUBTAB";
        public static final String TAG_GET_LIST_FILM_OF_EVENT = "MOVIES_GET_LIST_FILM_OF_EVENT";
        public static final String TAG_GET_LIST_FILM_OF_PRIZE = "MOVIES_GET_LIST_FILM_OF_PRIZE";
        public static final String TAG_GET_LIST_FILM_OF_CATEGORY = "MOVIES_GET_LIST_FILM_OF_CATEGORY";

    }

    public class CacheSeen {
        public static final String CACHE_IDS_VIDEO = "cache_ids_video_seen";
        public static final String CACHE_IDS_NEWS = "cache_ids_news_seen";
        public static final String CACHE_IDS_TIIN = "cache_ids_tiin_seen";
    }

    public static final class QR_SOURCE {
        public static final int FROM_NONE = 0;
        public static final int FROM_SC_RECHARGE = 1;
        public static final int FROM_SC_RECHARGE_HOME = 2;
        public static final int SCAN_SERIAL = 3;
    }

    public static final class UMONEY {
        public static final String PHONE_NUMBER = "phoneNumber";
        public static final String CUSTOMER_NAME = "customerName";
        public static final String BALANCE = "balance";
        public static final String PAN = "pan";
        public static final String ROLE_ID = "roleID";
        public static final String ACCOUNT_ID = "accountID";
        public static final String AMOUNT = "amount";
        public static final String TRANSACTION_ID = "transactionID";
        public static final String TRANSACTION_FEE = "transactionFee";
        public static final String TRANSACTION_TYPE = "serviceCode";
        public static final String SERVICETYPE = "serviceType";
        public static final String SUBTYPE = "subType";

        public static final String UMONEY_COMPLETE = "UMONEY_COMPLETE";
    }

    public static final class SCREEN {
        //todo khai báo id của các màn hình ở đây để phục vụ cho việc log và định danh màn hình
        public static final int SRC_CREATE_CHAT = 1;
    }

    final class WSCODE {
        public static final String WS_DO_ACTION_SERVICE_RESULT_SUCCESS = "0";
        public static final String WS_DO_ACTION_SERVICE_RESULT_NOT_FOUND_DATA = "1";
        public static final String WS_DO_ACTION_SERVICE_RESULT_IS_MIU = "2";
        public static final String WS_DO_ACTION_SERVICE_RESULT_NOT_ENOUGH_MONEY = "3";
        public static final String WS_DO_ACTION_SERVICE_RESULT_ERR_PROVISIONING = "4";
        public static final String WS_DO_ACTION_SERVICE_RESULT_DEDUCT_MONEY_FAIL = "5";
        public static final String WS_DO_ACTION_SERVICE_RESULT_ADD_MONEY_FAIL = "6";
        public static final String WS_DO_ACTION_SERVICE_RESULT_NUMBER_NOT_EX = "10";
        public static final String WS_DO_ACTION_SERVICE_RESULT_ERR_DB = "99";

        public static final String SERVICE_PACKAGE_1 = "EXCHANGE-MONEY-150";
        public static final String SERVICE_PACKAGE_2 = "EXCHANGE-MONEY-300";
        public static final String SERVICE_PACKAGE_4 = "SU4";
        public static final String SERVICE_PACKAGE_5 = "SU5";
        public static final String SERVICE_PACKAGE_8 = "SU8";

        public static final int WS_DO_ACTION_SERVICE_ACTION_TYPE_REGISTER = 0;
        public static final int WS_DO_ACTION_SERVICE_ACTION_TYPE_CANCEL = 1;

        public static final String WS_ACCOUNT_INFO = "wsAccountInfo";
        public static final String WS_GET_SUB_ACCOUNT_INFO_NEW = "wsGetSubAccountInfoNew";
        public static final String WS_GET_ACCOUNTS_OCS_DETAIL = "wsGetAccountsOcsDetail";
        public static final String WS_GET_EXCHANGE_SERVICE = "wsGetExchangeService";
        public static final String WS_TOP_UP = "wsTopUp";
        public static final String WS_HISTORY_TOP_UP = "wsHistoryTopUp";
        public static final String WS_HISTORY_TOP_UP_NEW = "wsHistoryTopUpNew";
        public static final String WS_GET_SERVICES = "wsGetServices";
        public static final String WS_GET_SERVICES_BY_GROUP = "wsGetServicesByGroup";
        public static final String WS_GET_SERVICE_DETAIL = "wsGetServiceDetail";
        public static final String WS_DO_ACTION_SERVICE = "wsDoActionService";
        public static final String WS_HISTORY_CHARGE = "wsHistoryCharge";
        public static final String WS_HISTORY_CHARGE_DETAIL = "wsHistoryChargeDetail";
        public static final String WS_GET_PROCESS_LIST_CAMID = "wsGetProcessListCamID";
        public static final String WS_GET_COMPLAINT_HISTORY = "wsGetComplaintHistory";
        public static final String WS_CHECK_COMPLAINT_NOTIFICATION = "wsCheckComplaintNotification";
        public static final String WS_UPDATE_COMPLAINT = "wsUpdateComplaint";
        public static final String WS_GET_COM_TYPE_CAMID = "wsGetComTypeCamID";
        public static final String WS_SUBMIT_COMPLAINT_MY_METFONE = "wsSubmitComplaintMyMetfone";
        public static final String WS_GET_NEAREST_STORES = "wsGetNearestStores";
        public static final String WS_FIND_STORE_BY_ADDR = "wsFindStoreByAddr";
        public static final String WS_GET_PROVINCES = "wsGetProvinces";
        public static final String WS_GET_DISTRICTS = "wsGetDistricts";
        public static final String WS_GET_CURRENT_USED_SERVICES = "wsGetCurrentUsedServices";
        public static final String WS_GET_LIST_PREFIX_NUMBER = "wsGetListPrefixNumber";
        public static final String WS_SEARCH_NUMBER_TO_BUY = "wsSearchNumberToBuy";

        public static final String WS_GET_ACCOUNT_RANK_INFO = "wsGetAccountRankInfo";
        public static final String WS_GET_ALL_PARTNER_GIFT_SEARCH = "wsGtAllPartnerGiftSearch";
        public static final String WS_GET_RANK_DEFINE_INFO = "wsGetRankDefineInfo";
        public static final String WS_GET_ACCOUNT_POINT_INFO = "wsGetAccountPointInfo";
        public static final String WS_GET_PARTNER_GIFT_REDEEM_HISTORY = "wsGetPartnerGiftRedeemHistory";
        public static final String WS_GET_LIST_GIFT_RECEIVED = "wsGetListGiftReceived";
        public static final String WS_GET_LIST_REWARD = "uc_getListGift";
        public static final String WS_GET_POINT_TRANSFER_HISTORY = "wsGetPointTransferHistory";
        public static final String WS_GET_ALL_TE_LE_COM_GIFT = "wsGetAllTelecomGift";
        public static final String WS_GET_PARTNER_GIFT_DETAIL = "wsGetPartnerGiftDetail";
        public static final String WS_REDEEM_POINT = "wsRedeemPoint";
        public static final String WS_REDEEM_GIFT_AND_RETURN_EXCHANGE_CODE = "wsRedeemGiftAndReturnExchangeCode";
        public static final String WS_CHECK_FORCE_UPDATE_APP = "wsCheckForceUpdateApp";
        public static final String WS_SUB_GET_OTP_METFONT = "wsSubscriberGetOTP";
        public static final String WS_SUB_DETACH_IP = "wsDetachIP";
        public static final String WS_SUBSCRIBE_GET_INFO_BY_ISDN = "wsSubscriberGetInforCusByIsdn";
        public static final String WS_SUBSCRIBE_UPDATE_CUSTOMER_INFO = "wsSubscriberUpdateCustomerInfo";
        public static final String WS_DETECT_ORC_FROM_IMAGE = "wsDetectOCRFromImage";
        public static final String WS_GET_OTP_METFONT = "wsGetOTPByService";
        public static final String WS_CONFIRM_OTP = "wsConfirmOTP";
        public static final String WS_CHANGE_ISDN_KEEP_SIM = "wsChangeIsdnKeepSim";
        public static final String WS_LIST_HISTORY_ORDERED_SIM = "wsGetListOrderNumberHistory";
        public static final String WS_LOCK_ISDN_TO_BUY = "wsLockIsdnToBuy";
        public static final String WS_GET_RANKING_LIST_DETAIL = "uc_getRankingListDetail";
        public static final String WS_GET_OTP_MY_METFONT = "wsGetOtpIshare";
        public static final String WS_CONFIRM_OTP_ISHARE = "wsTransferIshare";
        public static final String WS_GET_ALL_APPS = "wsGetAllApps";
        public static final String WS_CLOSE_COMPLAINT = "wsCloseComplain";
        public static final String WS_RATE_COMPLAINT = "wsRateComplain";
        public static final String WS_SEARCH_COMPLAINT_HISTORY = "wsSearchComplaintHistory";
        /* list complaint type  for service internet */
        public static final String WS_GET_LIST_COMPLAINT_TYPE = "wsGetListComplaintType";
        /* list complaint type  for service mobile */
        public static final String WS_GET_COMPLAINT_TYPE = "wsGetComType";
        public static final String WS_GET_ALL_TE_LE_COM_GIFT_FOR_FRIEND = "wsGetAllTelecomGiftForFriend";
        public static final String WS_REDEEM_POINT_FOR_FRIEND = "wsRedeemPointAndGive";
        public static final String WS_GET_GIFT_INFO = "wsGiftInfo";
        public static final String WS_GIFT_SPIN = "wsSpin";
        public static final String WS_GIFT_BOX_LIST = "wsListGiftBox";
        public static final String WS_REDEEM_HISTORY_LIST = "wsListHistoryRedeem";
        public static final String WS_REDEEM_GIFT = "wsRedeem";
        public static final String WS_CHECK_USER_GAME = "wsCheckUseGame";
        public static final String WS_GET_AUTH_KEY_GAME = "wsGetAuthenkey";
        public static final String WS_LOG_APP = "wsLogApp";
        public static final String WS_GET_HOT_MOVIE = "wsMovieGetHot";
        public static final String WS_GET_TOP_MOVIE_CATEGORY = "wsMovieGetTopCategory";
        public static final String WS_GET_MOVIE_HOME_APP2 = "wsMovieGetHomeApp2";
        public static final String WS_GET_LIST_FILM_FOR_USER = "wsMovieGetListFilmForUser";
        public static final String WS_GET_MOVIES_LIST_LOG_WATCHED = "wsMovieGetListLogWatched";
        public static final String WS_GET_MOVIE_DETAIL = "wsMovieGetDetailFilm";
        public static final String WS_GET_MOVIE_CATEGORY_V2 = "wsMovieGetCategoryV2";
        public static final String WS_GET_MOVIE_COUNTRY = "wsMovieGetCountry";
        public static final String WS_GET_MOVIE_RELATED = "wsMovieGetRelateFilm";
        public static final String WS_GET_MOVIE_LIST_FILM_OF_CATEGORY = "wsMovieGetListFilmOfCategory";
        public static final String WS_GET_MOVIE_LIST_GROUP_DETAIL = "wsMovieGetListGroupsDetail";
        public static final String WS_GET_MOVIE_CATEGORY_BY_IDS = "wsMovieGetCategoryByIds";
        public static final String WS_ABA_MOBILE_PAYMENT_TOP_UP = "paymentRequestABAMobile";
        public static final String WS_ABA_MOBILE_PAYMENT_TOP_UP_CALLBACK = "callbackReponseABAMobile";
        public static final String WS_UPDATE_CAMID_DEVICE_TOKEN = "wsUpdateCamIdDeviceToken";
        public static final String WS_UPDATE_CAMID_DEVICE_STATUS = "wsUpdateCamIdAccessStatus";
        public static final String WS_SLIDER_FILM = "wsGetSliderFilm";
        public static final String WS_CHANGE_CARD_GET_LIST = "exChangeCardGetList";
        public static final String WS_ADD_CHANGE_CARD = "ChangeCardAddNew";

        public static final String WS_GET_SUMMARY_PACKAGE = "getSummaryPackage";
        public static final String WS_GET_LIST_PRECINCT = "getListPrecinct";
        public static final String WS_GET_LIST_DISTRICT = "getListDistrict";
        public static final String WS_GET_LIST_PROVINCE = "getListProvince";
        public static final String WS_CAMID_INTERNET_REGISTER = "camIdRegisterInternet";
        public static final String WS_GET_PACKAGE_INFO = "getPackageInfor";

        public static final String WS_PARTNER_ID = "wsInforPartner";
        public static final String WS_SEARCH_FTTH = "searchFTTHRequest";
        public static final String WS_FORGOT_REQUEST_ID = "forgetRequest";
        public static final String WS_PAYMENT_HISTORY = "wsGetPaymentHistory";
        public static final String WS_GET_ACCOUNT_PAYMENT_INFO = "wsGetAccountInfoForPayment";
        public static final String WS_ABA_MOBILE_BILL_PAYMENT = "paymentRequestABAMobile";
        public static final String WS_BILL_PAYMENT_HISTORY = "wsGetPaymentHistory";

        //Donate
        public static final String WS_GET_ALL_DONATE_PACKAGE_APP = "wsGetAllDonatePackageApp";
        public static final String WS_DONATE_GIFT_PACKAGE = "wsDonateGiftPackage";
        public static final String WS_UPDATE_CHANNEL_INFORMATION = "wsUpdateChannelInformation";

        public static final String WS_CHECK_SHOW_POPUP_TET = "wsCheckShowPopupTet";
        public static final String WS_CHECK_GET_GIFT_TET = "wsCheckGetGiftTet";
        public static final String WS_REDEEM_GIFT_TET = "wsRedeemGiftTet";

    }

    public static class LOG {
        public static final String ACTIVE_TAB_HOME = "active_tab_home";
        public static final String ACTIVE_TAB_REWARD = "active_tab_reward";
        public static final String ACTIVE_TAB_ESPORT = "active_tab_esport";
        public static final String ACTIVE_TAB_CINEMA = "active_tab_cinema";
        public static final String ACTIVE_TAB_CHAT = "active_tab_chat";
        public static final String ACTIVE_TAB_METFONE = "active_tab_esport";
    }

    interface LOG_APP{
        String TAB_HOME = "tabHome";
        String TAB_ESPORT = "tabEsport";
        String TAB_CENIMA = "tabCenima";
        String TAB_CHAT = "tabChat";
        String TAB_METFONEP = "tabMetfone";
        String GAME_CHANNEL = "tabClickChannel";
        String FEEDBACK_US = "tabClickFeedbackUs";
        String BALANCE_INFO = "tabClickInformation";
        String TAB_MY_SERVICE = "tabClickMyServices";
        String MAKE_NEW_CALL = "tabClickNewCall";
        String REWARD_SHOP = "tabClickRewardShop";
        String SERVICE_EXCHANGE = "tabClickExchange";
        String SERVICE_SOCIAL_MEDIA = "tabClickSocialNetwork";
        String SERVICE_MOBILE_INTERNET = "tabClickMobileInternet";
        String SERVICE_INTERNATIONAL = "tabClickInternational";
        String SERVICE_TOP_UP = "Metfone_topup";
        String SERVICE_BUY_NUMBER = "Metfone_buynumber";
        String SERVICE_ISHARE = "Metfone_ishare";
        String SERVICE_SUPPORT = "Metfone_support";
        String HOME_MUNNY = "Home_Munny";
        String HOME_QRCODE = "Home_qrcode";
        String HOME_LUCKY_WHEEL = "Home_luckywheel";
        String HOME_CATEGORY = "Home_catelory";
        String HOME_SHARE_GIFT = "Home_sharegift";
        String HOME_DETAIL_ACC = "Home_detail_account";
        String SIGN_UP = "signup";
        String LOG_IN = "login";
        String UPDATE_INFORMATION = "ws_udpdate_infortmation";

        String TAB_GAME = "tabGame";
    }


}
package com.metfone.selfcare.helper;

import android.content.Intent;
import android.content.res.Resources;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.spoint.SpointActivity;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by toanvk2 on 11/29/2016.
 */
public class AccumulatePointHelper {
    private static final String TAG = AccumulatePointHelper.class.getSimpleName();
    public static final String TAG_READ_SPONSOR_SUCCESS = "READ_SPONSOR_SUCCESS";
    public static final String TAG_GET_DETAIL = "ACCUMULATE_GET_DETAIL";
    public static final String TAG_GET_POINT = "ACCUMULATE_GET_POINT";
    public static final String TAG_LOG_ACCUMULATE = "TAG_LOG_ACCUMULATE";
    public static final int TAB_TASK = 0;
    public static final int TAB_EXCHANGE = 1;
    public static final int TAB_HISTORY = 2;
    private static AccumulatePointHelper mInstance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private String totalPoint;
    private String viettelPlusPoint;
    private ArrayList<AccumulatePointItem> accumulateTasks;
    private ArrayList<AccumulatePointItem> accumulateGiftChanges;
    private ArrayList<AccumulatePointItem> accumulateHistories;
    private CopyOnWriteArrayList<AccumulateDataChange> mAccumulateChangedListeners = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<AccumulateHistoryListener> mAccumulateHistoryListeners = new CopyOnWriteArrayList<>();

    public static synchronized AccumulatePointHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new AccumulatePointHelper(application);
        }
        return mInstance;
    }

    private AccumulatePointHelper(ApplicationController application) {
        this.mApplication = application;
        this.mRes = mApplication.getResources();
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
    }

    public void navigateToTotalPoint(BaseSlidingFragmentActivity activity) {
//        Intent intent = new Intent(activity, ListGamesActivity.class);
//        intent.putExtra(Constants.GAME.ID_GAME, Constants.GAME.ACCUMULATE_ID);
//        activity.startActivity(intent, true);
        Intent intent = new Intent(activity, SpointActivity.class);
//        intent.putExtra(Constants.GAME.ID_GAME, Constants.GAME.ACCUMULATE_ID);
        activity.startActivity(intent, true);
    }

    public boolean isReady() {
        return !(totalPoint == null || accumulateTasks == null || accumulateGiftChanges == null);
    }

    public boolean isHistoryReady() {
        return accumulateHistories != null;
    }

    public String getTotalPoint() {
        if (TextUtils.isEmpty(totalPoint)) return "0";
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }

    public int getTotalPointInt() {
        try {
            int result = Integer.parseInt(totalPoint);
            if (result > 0) return result;
        } catch (Exception e) {
        }
        return 0;
    }

    public String getViettelPlusPoint() {
        if (TextUtils.isEmpty(viettelPlusPoint)) return "0";
        return viettelPlusPoint;
    }

    public void setViettelPlusPoint(String viettelPlusPoint) {
        this.viettelPlusPoint = viettelPlusPoint;
    }

    public ArrayList<AccumulatePointItem> getAccumulatePointItem(int type) {
        if (type == TAB_TASK) {
            return accumulateTasks;
        } else if (type == TAB_EXCHANGE) {
            return accumulateGiftChanges;
        } else {
            return accumulateHistories;
        }
    }

    // yc ben kd la khong cache lai thong tin, nen khi fragment bị detect se reset toan bo thong tin de lan sau load lại
    public void resetData() {
        totalPoint = null;
        viettelPlusPoint = null;
        accumulateTasks = null;
        accumulateGiftChanges = null;
        accumulateHistories = null;
    }

    public void addAccumulateListener(AccumulateDataChange listener) {
        if (!mAccumulateChangedListeners.contains(listener)) {
            mAccumulateChangedListeners.add(listener);
        }
    }

    public void removeAccumulateListener(AccumulateDataChange listener) {
        if (mAccumulateChangedListeners.contains(listener)) {
            mAccumulateChangedListeners.remove(listener);
        }
    }

    public void onAccumulateChanged(boolean changeList) {
        if (mAccumulateChangedListeners != null && !mAccumulateChangedListeners.isEmpty()) {
            for (AccumulateDataChange listener : mAccumulateChangedListeners) {
                listener.onAccumulateDataChanged(changeList);
            }
        } else {
            Log.i(TAG, "no listener for AccumulateChanged");
        }
    }

    public void addAccumulateHistoryListener(AccumulateHistoryListener listener) {
        if (!mAccumulateHistoryListeners.contains(listener)) {
            mAccumulateHistoryListeners.add(listener);
        }
    }

    public void removeAccumulateHistoryListener(AccumulateHistoryListener listener) {
        if (mAccumulateHistoryListeners.contains(listener)) {
            mAccumulateHistoryListeners.remove(listener);
        }
    }

    public void onAccumulateHistoryChanged(boolean isSuccess, int errorCode, String errorMsg) {
        if (mAccumulateHistoryListeners != null && !mAccumulateHistoryListeners.isEmpty()) {
            for (AccumulateHistoryListener listener : mAccumulateHistoryListeners) {
                if (isSuccess) {
                    listener.onSuccess();
                } else {
                    listener.onError(errorCode, errorMsg);
                }
            }
        } else {
            Log.i(TAG, "no listener for AccumulateHistoryListener");
        }
    }

    public void getAccumulateDetail(final GetAccumulateListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2, null);
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_DETAIL);
        mAccountBusiness = mApplication.getReengAccountBusiness();
        StringBuilder sbUrl = new StringBuilder();
        long timeStamp = TimeHelper.getCurrentTime();
        String language = mAccountBusiness.getDeviceLanguage();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(language).
                append(mAccountBusiness.getToken()).
                append(timeStamp);
        String dataSecurity = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        sbUrl.append(UrlConfigHelper.getInstance(mApplication).getDomainFile()).append("/ReengBackendBiz/accumulate/getListAccumulate/v5");
//        sbUrl.append(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GET_LIST_ACCUMULATE_V4));
        sbUrl.append("?").append(Constants.HTTP.REST_MSISDN).append("=").append(HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()));
        sbUrl.append("&").append(Constants.HTTP.REST_CLIENT_TYPE).append("=").append(Constants.HTTP.CLIENT_TYPE_STRING);
        sbUrl.append("&").append(Constants.HTTP.REST_REVISION).append("=").append(Config.REVISION);
        sbUrl.append("&").append(Constants.HTTP.REST_LANGUAGE_CODE).append("=").append(language);
        sbUrl.append("&").append(Constants.HTTP.TIME_STAMP).append("=").append(timeStamp);
        sbUrl.append("&").append(Constants.HTTP.DATA_SECURITY).append("=").append(HttpHelper.EncoderUrl(dataSecurity));
        String url = sbUrl.toString();
        Log.i(TAG, "getAccumulateDetail: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "getAccumulateDetail onResponse: " + response);
                try {
                    JsonElement element = new JsonParser().parse(response);
                    JsonObject jsonObject = element.getAsJsonObject();
                    int code = -1;
                    if (jsonObject.has(Constants.HTTP.REST_CODE) && jsonObject.has("data")) { // khong co data thi loi
                        code = jsonObject.get(Constants.HTTP.REST_CODE).getAsInt();
                    }
                    if (code == HTTPCode.E200_OK) {
                        JsonObject dataObject = jsonObject.getAsJsonObject("data");
                        if (dataObject.has("point")) {
                            totalPoint = dataObject.get("point").getAsString();
                        } else {
                            totalPoint = "0";
                        }
                        if (dataObject.has("vtPoint")) {
                            viettelPlusPoint = dataObject.get("vtPoint").getAsString();
                        } else {
                            viettelPlusPoint = "0";
                        }
                        accumulateTasks = parseListTask(dataObject);
                        accumulateGiftChanges = parseListGift(dataObject);
                        accumulateHistories = parseListHistory(dataObject);
                        listener.onSuccess();
                    } else {
                        listener.onError(code, null);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onError(-1, null);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
                listener.onError(-1, null);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_DETAIL, false);
    }

    public void convertGiftPoint(final AccumulatePointItem item, final GiftPointListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2, null);
            return;
        }
        mAccountBusiness = mApplication.getReengAccountBusiness();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ACCUMULATE_CONVERT);
        Log.i(TAG, "convertGiftPoint: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "convertGiftPoint onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC);
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                                if (object.has("point")) {
                                    totalPoint = object.getString("point");
                                    onAccumulateChanged(false);// cap nhat thong tin my point
                                }
                                forceGetDetailAfterConvertPoint();
                            } else if (code == 201) {
                                listener.onError(code, desc);
                            } else {
                                listener.onError(code, null);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, null);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, null);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(item.getId()).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put("id", String.valueOf(item.getId()));
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };

        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void convertSpointToViettelPlus(String point, final AccumulatePointItem item, String otp, String transId, final GiftPointListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2, null);
            return;
        }
        mAccountBusiness = mApplication.getReengAccountBusiness();
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + "/ReengBackendBiz/accumulate/convertPointToViettelAdd";
        Log.i(TAG, "convertSpointToViettelPlus: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "convertGiftPoint onResponse: " + response);
                        try {
                            JsonElement element = new JsonParser().parse(response);
                            JsonObject jsonObject = element.getAsJsonObject();
                            int code = jsonObject.get(Constants.HTTP.REST_CODE).getAsInt();
                            String desc = jsonObject.get(Constants.HTTP.REST_DESC).getAsString();
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                                if (jsonObject.has("point")) {
                                    totalPoint = jsonObject.get("point").getAsString();
                                    onAccumulateChanged(false);// cap nhat thong tin my point
                                }
                                forceGetDetailAfterConvertPoint();
                            } else if (code == 201) {
                                listener.onError(code, desc, null);
                            } else if (code == 202) {
                                String transId = "";
                                if (jsonObject.has("data")) {
                                    JsonObject dataObject = jsonObject.getAsJsonObject("data");
                                    transId = dataObject.get("transId").getAsString();
                                }
                                if (TextUtils.isEmpty(transId)) {
                                    if (listener != null) listener.onError(202, desc, null);
                                } else {
                                    if (listener != null) listener.onError(202, desc, transId);
                                }
                            } else {
                                listener.onError(code, null, null);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, null, null);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, null, null);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(point).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put("vtPoint", point);
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                // todo bo sumg param otp
                if (!TextUtils.isEmpty(otp))
                    params.put("otp", otp);
                if (!TextUtils.isEmpty(transId)) {
                    params.put("transId", transId);
                }
                return params;
            }
        };

        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void requestReadSponsorSuccess(final String sponsorId, final ReadSponsorSuccessListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        mAccountBusiness = mApplication.getReengAccountBusiness();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SPONSOR_READ_SUCCESS);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "requestReadSponsorSuccess onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC);
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(code, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(sponsorId).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put("id", String.valueOf(sponsorId));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_READ_SPONSOR_SUCCESS, false);
    }

    public void getCurrentTotalPoint(String friendJid, final AccumulateListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }
        if (!mApplication.getConfigBusiness().isEnableSpoint()) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_GET_POINT);
        mAccountBusiness = mApplication.getReengAccountBusiness();
        StringBuilder sbUrl = new StringBuilder();
        long timeStamp = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(friendJid).
                append(mAccountBusiness.getToken()).
                append(timeStamp);
        String dataSecurity = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        sbUrl.append(UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.ACCUMULATE_GET_POINT));
        sbUrl.append("?").append(Constants.HTTP.REST_MSISDN).append("=").append(HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()));
        sbUrl.append("&").append("friend").append("=").append(HttpHelper.EncoderUrl(friendJid));
        sbUrl.append("&").append(Constants.HTTP.TIME_STAMP).append("=").append(timeStamp);
        sbUrl.append("&").append(Constants.HTTP.DATA_SECURITY).append("=").append(HttpHelper.EncoderUrl(dataSecurity));
        String url = sbUrl.toString();
        Log.i(TAG, "getTotalPoint: " + url);
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "getTotalPoint onResponse: " + response);
                try {
                    String decryptData = HttpHelper.decryptResponse(response, mApplication.getReengAccountBusiness().getToken());
                    if (!TextUtils.isEmpty(decryptData)) {
                        JSONObject object = new JSONObject(decryptData);
                        int code = object.optInt(Constants.HTTP.REST_CODE, -1);
                        if (code == HTTPCode.E200_OK) {
                            int totalPoint = object.optInt("point", -1);
                            if (totalPoint >= 0 && listener != null) {
                                listener.onSuccess(totalPoint);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_GET_POINT, false);
    }

    private void forceGetDetailAfterConvertPoint() {
        getAccumulateDetail(new GetAccumulateListener() {
            @Override
            public void onSuccess() {
                onAccumulateChanged(true);

            }

            @Override
            public void onError(int errorCode, String desc) {

            }
        });
    }

    private ArrayList<AccumulatePointItem> parseListTask(JsonObject object) throws Exception {
        ArrayList<AccumulatePointItem> list = new ArrayList<>();
        ArrayList<String> listAppInstalled = new ArrayList<>();
        if (object.has("listTask")) {
            JsonArray jsonArray = object.getAsJsonArray("listTask");
            if (jsonArray != null && jsonArray.size() > 0) {
                int lengthArray = jsonArray.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    AccumulatePointItem item = gson.fromJson(jsonArray.get(i), AccumulatePointItem.class);
                    item.setItemType(TAB_TASK);
                    if (AccumulatePointItem.MISSION_TYPE.equals(item.getMissionType()) &&
                            (AccumulatePointItem.MISSION_STATUS_NEW.equals(item.getMissionStatus()))) {
                        if (Utilities.isAppInstalled(mApplication, item.getAppId())) {
                            listAppInstalled.add(item.getAppId());
                        } else
                            list.add(item);
                    } else {
                        list.add(item);
                    }

                }
            }
        }
        if (!listAppInstalled.isEmpty()) {

            logAccumulate(AccumulatePointItem.ACTION.INSTALLED, listAppInstalled, new LogAccumulateListener() {
                @Override
                public void onLogSuccess(String desc) {
                    Log.i(TAG, "onLogSuccess: " + desc);
                }

                @Override
                public void onLogError(int errorCode, String desc) {
                    Log.i(TAG, "onLogError: " + errorCode + " desc: " + desc);
                }
            });
        }
        return list;
    }

    private ArrayList<AccumulatePointItem> parseListGift(JsonObject object) throws Exception {
        ArrayList<AccumulatePointItem> list = new ArrayList<>();
        if (object.has("listGifChange")) {
            JsonArray jsonArray = object.getAsJsonArray("listGifChange");
            if (jsonArray != null && jsonArray.size() > 0) {
                int lengthArray = jsonArray.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                    int giftType = jsonObject.get("gif_type").getAsInt();
                    String title = jsonObject.get("title").getAsString();
                    String balance = jsonObject.get("balance").getAsString();
                    String pointDesc = jsonObject.get("point_desc").getAsString();

                    AccumulatePointItem item = new AccumulatePointItem();
                    item.setGiftType(giftType);
                    item.setTitle(title);
                    item.setBalance(balance);
                    item.setDesc(pointDesc);
                    if (jsonObject.has("label")) {
                        String label = jsonObject.get("label").getAsString();
                        item.setLabelButton(label);
                    }
                    item.setItemType(TAB_EXCHANGE);
                    list.add(item);
                    if (jsonObject.has("lst_gif_card")) {
                        JsonArray jsonArrayGiftCard = jsonObject.getAsJsonArray("lst_gif_card");
                        if (jsonArrayGiftCard != null && jsonArrayGiftCard.size() > 0) {
                            int lengthArrayGiftCard = jsonArrayGiftCard.size();
                            for (int j = 0; j < lengthArrayGiftCard; j++) {
                                AccumulatePointItem itemGiftCard = gson.fromJson(jsonArrayGiftCard.get(j), AccumulatePointItem.class);
                                itemGiftCard.setItemType(TAB_EXCHANGE);
                                itemGiftCard.setItemPoint(AccumulatePointItem.ITEM_CPOINT);
                                list.add(itemGiftCard);
                            }
                        }
                    }

                    if (jsonObject.has("lst_gif_point")) {
                        JsonArray jsonArrayGiftPoint = jsonObject.getAsJsonArray("lst_gif_point");
                        if (jsonArrayGiftPoint != null && jsonArrayGiftPoint.size() > 0) {
                            int lengthArrayGiftPoint = jsonArrayGiftPoint.size();
                            for (int j = 0; j < lengthArrayGiftPoint; j++) {
                                AccumulatePointItem itemGiftCard = gson.fromJson(jsonArrayGiftPoint.get(j), AccumulatePointItem.class);
                                itemGiftCard.setItemType(TAB_EXCHANGE);
                                itemGiftCard.setItemPoint(AccumulatePointItem.ITEM_MPOINT);
                                list.add(itemGiftCard);
                            }
                        }
                    }

                    if (jsonObject.has("lst_gif_vtpoint")) {
                        JsonArray jsonArrayGiftPoint = jsonObject.getAsJsonArray("lst_gif_vtpoint");
                        if (jsonArrayGiftPoint != null && jsonArrayGiftPoint.size() > 0) {
                            int lengthArrayGiftPoint = jsonArrayGiftPoint.size();
                            for (int j = 0; j < lengthArrayGiftPoint; j++) {
                                AccumulatePointItem itemGiftCard = gson.fromJson(jsonArrayGiftPoint.get(j), AccumulatePointItem.class);
                                itemGiftCard.setItemType(TAB_EXCHANGE);
                                itemGiftCard.setItemPoint(AccumulatePointItem.ITEM_VTPLUS);
                                list.add(itemGiftCard);
                            }
                        }
                    }

                }
            }
        }
        return list;
    }

    private ArrayList<AccumulatePointItem> parseListHistory(JsonObject object) throws Exception {
        ArrayList<AccumulatePointItem> list = new ArrayList<>();
        if (object.has("listHis")) {
            JsonArray jsonArray = object.getAsJsonArray("listHis");
            if (jsonArray != null && jsonArray.size() > 0) {
                int lengthArray = jsonArray.size();
                Gson gson = new Gson();
                for (int i = 0; i < lengthArray; i++) {
                    AccumulatePointItem item = gson.fromJson(jsonArray.get(i), AccumulatePointItem.class);
                    item.setItemType(TAB_HISTORY);
                    list.add(item);

                }
            }
        }
        return list;
    }

    public void logAccumulate(final AccumulatePointItem.ACTION action, final ArrayList<String> listAppInstalled, final LogAccumulateListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onLogError(-2, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        mAccountBusiness = mApplication.getReengAccountBusiness();
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.LOG_ACCUMULATE);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "logAccumulate onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC);
                            if (code == HTTPCode.E200_OK) {
                                listener.onLogSuccess(desc);
                            } else {
                                listener.onLogError(code, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onLogError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onLogError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();

                StringBuilder sb1 = new StringBuilder("[");
                for (int i = 0; i < listAppInstalled.size(); i++) {
                    sb1.append("\"");
                    sb1.append(listAppInstalled.get(i));
                    sb1.append("\"");
                    if (i != (listAppInstalled.size() - 1)) {
                        sb1.append(",");
                    }
                }
                sb1.append("]");

                StringBuilder sbData = new StringBuilder("{\"");
                sbData.append("action\":\"");
                sbData.append(action.name());
                sbData.append("\",\"info\":");
                sbData.append(sb1.toString());
                sbData.append("}");

                /*JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("action", action.name());
                    jsonObject.put("info", info);
                } catch (JSONException e) {
                    Log.i(TAG, "JSONException", e);
                }*/

                String data = XXTEACrypt.encryptToBase64String(sbData.toString(), mAccountBusiness.getToken());

                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(data).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).append(Config.REVISION).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put("data", data);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_LOG_ACCUMULATE, false);
    }

    public interface ReadSponsorSuccessListener {
        void onSuccess(String desc);

        void onError(int errorCode, String desc);
    }

    public interface GetAccumulateListener {
        void onSuccess();

        void onError(int errorCode, String desc);
    }

    public interface GiftPointListener {

        void onError(int errorCode, String desc, String tranId);

        void onSuccess(String desc);

        void onError(int errorCode, String desc);
    }

    public interface AccumulateDataChange {
        void onAccumulateDataChanged(boolean changeList);
    }

    public interface AccumulateHistoryListener {
        void onSuccess();

        void onError(int errorCode, String desc);
    }

    public interface AccumulateListener {
        void onSuccess(int point);
    }

    public interface LogAccumulateListener {
        void onLogSuccess(String desc);

        void onLogError(int errorCode, String desc);
    }
}
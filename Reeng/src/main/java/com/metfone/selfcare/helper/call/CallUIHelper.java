package com.metfone.selfcare.helper.call;

import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.cardview.widget.CardView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.CallBusiness;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.ui.LoadingTextView;

/**
 * Created by toanvk2 on 9/19/2017.
 */

public class CallUIHelper {

    public static void drawStateMute(CallBusiness mCallBusiness, ImageView mImgMute) {
        if (mCallBusiness.isMute()) {
            mImgMute.setImageResource(R.drawable.ic_call_mic_on);
            //mImgMute.setColorFilter(ContextCompat.getColor(mParentActivity, R.color.bg_mocha));
        } else {
            mImgMute.setImageResource(R.drawable.ic_call_mic_off);
        }
    }

    public static void drawStateSpeaker(CallBusiness mCallBusiness, ImageView mImgSpeaker) {
        if (mCallBusiness.isSpeaker()) {
            mImgSpeaker.setImageResource(R.drawable.ic_call_speaker_on);
        } else {
            mImgSpeaker.setImageResource(R.drawable.ic_call_speaker_off);
        }
    }

    public static void drawStateAudioToVideo(CallBusiness callBusiness, ApplicationController application, LinearLayout mViewAudioToVideo, ImageView mImgAudioToVideo) {
        if (application.getCallBusiness().getCurrentCallState() < CallConstant.STATE.CONNECTED) {
            mViewAudioToVideo.setEnabled(false);
            mImgAudioToVideo.setImageResource(R.drawable.ic_call_video_disable);
        } else {
            mViewAudioToVideo.setEnabled(true);
            drawStateVideoEnable(callBusiness, mImgAudioToVideo);
        }
    }

    public static void drawStateVideoEnable(CallBusiness callBusiness, ImageView mImgVideoEnable) {
        if (callBusiness.isEnableMyVideoCall()) {
            mImgVideoEnable.setImageResource(R.drawable.ic_call_video_on);
        } else {
            mImgVideoEnable.setImageResource(R.drawable.ic_call_video_off);
        }
    }

    public static long timeDisConnect;

    public static void drawCallQuality(CardView viewQualityAndState, LoadingTextView textQuality,
                                       long bandwidth, boolean isShow, ApplicationController mContext) {
        Resources res = mContext.getResources();
        textQuality.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        if (isShow) {
            viewQualityAndState.setCardBackgroundColor(res.getColor(R.color.transparent));
            textQuality.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            textQuality.stopTimer();
            if (bandwidth == 0) {
                if (timeDisConnect == 0) {
                    textQuality.setText(res.getString(R.string.msg_no_internet));
                    timeDisConnect = System.currentTimeMillis();
                } else {
                    if (System.currentTimeMillis() - timeDisConnect >= mContext.getCallBusiness().getTimedis2recon()) {
                        textQuality.setText(res.getString(R.string.trying_connect));
                        //dainv: play media reconnecting here => not handle in callBusiness
                        mContext.getCallBusiness().startRingReconnecting();
                    } else {
                        textQuality.setText(res.getString(R.string.msg_no_internet));
                    }
                }
                viewQualityAndState.setCardBackgroundColor(res.getColor(R.color.white));
                textQuality.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            } /*else if (bandwidth < 5000) {
                timeDisConnect = 0;
                textQuality.setText(Html.fromHtml(res.getString(R.string.call_network_quality_1)));
            }*/ else if (bandwidth < 8000) {
                timeDisConnect = 0;
                textQuality.setText(Html.fromHtml(res.getString(R.string.call_network_quality_1)));
            } else if (bandwidth < 12000) {
                timeDisConnect = 0;
                textQuality.setText(Html.fromHtml(res.getString(R.string.call_network_quality_2)));
            } else {
                timeDisConnect = 0;
                textQuality.setText(Html.fromHtml(res.getString(R.string.call_network_quality_3)));
            }
        }
    }

    /*public static void blurAvatar(Context context, Bitmap source, Bitmap bmDefault, ImageView imgTarget) {
        if (source == null) {
            *//*Blurry.with(context).
                    radius(50).
                    async().
                    animate(100).
                    color(Color.argb(111, 51, 88, 74)).
                    capture(imgSource).
                    into(imgTarget);*//*
            Blurry.with(context).radius(10).async().animate(100).sampling(4).
                    color(Color.argb(111, 51, 88, 74)).from(bmDefault).into(imgTarget);
        } else {
            Blurry.with(context).radius(10).async().animate(100).sampling(4).
                    color(Color.argb(111, 51, 88, 74)).from(source).into(imgTarget);
        }
    }*/

    public static void resetOptionView(View viewQuality, View viewOption) {
        if (Version.hasLollipop()) {
            ViewPropertyAnimatorCompat animatorQuality = ViewCompat.animate(viewQuality);
            ViewPropertyAnimatorCompat animatorOption = ViewCompat.animate(viewOption);
            animatorQuality.cancel();
            animatorOption.cancel();
            if (viewQuality.getVisibility() == View.GONE || viewOption.getVisibility() == View.GONE) {
                viewOption.setTranslationY(0);
                viewQuality.setTranslationY(0);
                viewOption.setVisibility(View.VISIBLE);
                viewQuality.setVisibility(View.VISIBLE);
            }
        } else {
            viewOption.setVisibility(View.VISIBLE);
            viewQuality.setVisibility(View.VISIBLE);
        }
    }

    public static void showOrHideOptionVideoWithAnimation(View viewQuality, View viewOption, boolean isShow) {
        if (Version.hasLollipop()) {
            ViewPropertyAnimatorCompat animatorQuality = ViewCompat.animate(viewQuality);
            ViewPropertyAnimatorCompat animatorOption = ViewCompat.animate(viewOption);
            animatorQuality.cancel();
            animatorOption.cancel();
            if (isShow) {
                viewOption.setVisibility(View.VISIBLE);
                //viewOption.setTranslationY(viewOption.getHeight() + 200);
                animatorOption.setDuration(200);
                animatorOption.translationY(0);
                animatorOption.setListener(null);
                animatorOption.start();
                //quality
                viewQuality.setVisibility(View.VISIBLE);
                //viewQuality.setTranslationY(-viewQuality.getHeight());
                animatorQuality.setDuration(200);
                animatorQuality.translationY(0);
                animatorQuality.setListener(null);
                animatorQuality.start();
            } else {
                animatorOption.setDuration(200);
                animatorOption.translationY(viewOption.getHeight());
                animatorOption.setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(View view) {
                        view.setVisibility(View.GONE);
                    }
                });
                animatorOption.start();
                //quality
                animatorQuality.setDuration(200);
                animatorQuality.translationY(-viewOption.getHeight());
                animatorQuality.setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(View view) {
                        view.setVisibility(View.GONE);
                    }
                });
                animatorQuality.start();
            }
        } else {
            if (isShow) {
                viewOption.setVisibility(View.VISIBLE);
                viewQuality.setVisibility(View.VISIBLE);
            } else {
                viewOption.setVisibility(View.GONE);
                viewQuality.setVisibility(View.GONE);
            }
        }
    }
}

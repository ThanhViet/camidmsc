package com.metfone.selfcare.helper.message;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.message.TypingMessageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.SearchQuery;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.TypingItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.SearchHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.MusicRequestHelper;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.restful.Groups;
import com.metfone.selfcare.restful.ResfulSearchQueryObj;
import com.metfone.selfcare.restful.RestTopModel;
import com.metfone.selfcare.ui.MaterialProgressBar;
import com.metfone.selfcare.ui.chatviews.MultiLineEdittextTag;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/26/2017.
 */

public class TypingManager implements TypingMessageAdapter.TypingMessageListener {
    private static final String TAG = TypingManager.class.getSimpleName();
    public static final int STATE_VISIBLE = 1;
    public static final int STATE_GONE = 2;
    public static final int STATE_INSERT_MESAGE = 3;
    public static final int ITEM_TYPE_MUSIC = 1;
    public static final int ITEM_TYPE_VIDEO = 2;
    public static final String KEY_TYPE_MUSIC = "@music";
    public static final String KEY_TYPE_VIDEO = "@video";
    private ApplicationController mApplication;
    private BaseSlidingFragmentActivity mActivity;
    private TypingManagerInterface listener;
    private ThreadMessage currentThreadMessage;
    private View mViewFooterTyping;
    private MultiLineEdittextTag mEdtText;
    private RecyclerView mRecyclerView;
    private HeaderAndFooterRecyclerViewAdapter mWrapperAdapter;
    private View mHeader;
    private TypingMessageAdapter mAdapter;
    private MaterialProgressBar mProgressLoading;
    private TextView mTvwError;
    private LinearLayoutManager mVerticalLayoutManager;
    private LinearLayoutManager mHorizontalLayoutManager;
    //
    private String keyWord, searchKey;
    private ArrayList<Object> listData = new ArrayList<>();
    private ArrayList<TypingItem> typingItems = new ArrayList<>();
    private ArrayList<TypingItem> searchTypingItems = new ArrayList<>();
    private ArrayList<MediaModel> topSongs = new ArrayList<>();

    public TypingManager(ApplicationController application) {
        this.mApplication = application;
    }

    public void initViewController(BaseSlidingFragmentActivity activity, ThreadMessage thread, View view, MultiLineEdittextTag edt, TypingManagerInterface mListener) {
        this.mActivity = activity;
        this.currentThreadMessage = thread;
        initTypingItem();
        this.mViewFooterTyping = view;
        this.listener = mListener;
        this.mEdtText = edt;
        this.mRecyclerView = (RecyclerView) mViewFooterTyping.findViewById(R.id.typing_manager_recycler_view);
        this.mProgressLoading = (MaterialProgressBar) mViewFooterTyping.findViewById(R.id.typing_manager_progress);
        this.mTvwError = (TextView) mViewFooterTyping.findViewById(R.id.typing_manager_empty_text);
        mViewFooterTyping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "parent click listener");
            }
        });
        mVerticalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        mHorizontalLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);

        mAdapter = new TypingMessageAdapter(mApplication, this);
        mWrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
        View header = LayoutInflater.from(mApplication).inflate(R.layout.item_upload_song_typing, null);
        mHeader = header.findViewById(R.id.viewHeader);
        mHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onClickUploadSong();
            }
        });
        mHeader.setVisibility(View.GONE);
        mWrapperAdapter.addHeaderView(header);
        mAdapter.setListItems(new ArrayList<Object>());
        mRecyclerView.setAdapter(mWrapperAdapter);
        mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mHorizontalLayoutManager);
    }

    private void initTypingItem() {
        typingItems = new ArrayList<>();
        if (currentThreadMessage == null) {
            return;
        }
        if (currentThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {// room chat chi co cung nghe
            typingItems.add(new TypingItem(ITEM_TYPE_MUSIC, KEY_TYPE_MUSIC, R.drawable.ic_typing_music, R.string.typing_item_music));
        } else {
            typingItems.add(new TypingItem(ITEM_TYPE_MUSIC, KEY_TYPE_MUSIC, R.drawable.ic_typing_music, R.string.typing_item_music));
            if (mApplication.getConfigBusiness().isEnableWatchVideoTogether()) {
                typingItems.add(new TypingItem(ITEM_TYPE_VIDEO, KEY_TYPE_VIDEO, R.drawable.ic_typing_video, R.string.typing_item_video));
            }
        }
    }

    public void onTextChanged(String text) {
        long t = System.nanoTime();
        //TODO [perform] hiện đang xử lý trên main thread, sau này rảnh sẽ chuyển vào background thread sau
        if (currentThreadMessage == null ||
                currentThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT ||
                currentThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            return;
        }
        // room chat offline
        if (currentThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT && currentThreadMessage.getStateOnlineStar() != 1) {
            return;
        }
        if (text.startsWith("@")) {
            int firstSpace = text.indexOf(" ");
            if (firstSpace > 0) {
                keyWord = text.substring(0, firstSpace);
                TypingItem current = getCurrentTypingItem(keyWord.toLowerCase());
                if (current == null) {
                    closeTyingView();
                    if (mHeader != null)
                        mHeader.setVisibility(View.GONE);
                } else {
                    mViewFooterTyping.setVisibility(View.VISIBLE);
                    if (mHeader != null)
                        if (text.startsWith("@music "))
                            mHeader.setVisibility(View.VISIBLE);
                        else
                            mHeader.setVisibility(View.GONE);
                    searchKey = text.substring(firstSpace + 1).trim();
                    showSearchContent(current, searchKey);
                    listener.onStateChange(STATE_VISIBLE);
                }
            } else {
                keyWord = text;
                searchTypingItems = searchTypingItem(keyWord.toLowerCase());
                if (searchTypingItems.isEmpty()) {
                    closeTyingView();
                    if (mHeader != null)
                        mHeader.setVisibility(View.GONE);
                } else {
                    mViewFooterTyping.setVisibility(View.VISIBLE);
                    if (mProgressLoading != null) {
                        mProgressLoading.setStart(false);
                        mProgressLoading.setVisibility(View.GONE);
                    }
                    if (mHeader != null)
                        if (text.startsWith("@music "))
                            mHeader.setVisibility(View.VISIBLE);
                        else
                            mHeader.setVisibility(View.GONE);
                    mTvwError.setVisibility(View.GONE);
                    listData.clear();// doi type thi clear data
                    setAdapter(new ArrayList<Object>(searchTypingItems), true);
                    listener.onStateChange(STATE_VISIBLE);
                }
            }
            Log.d(TAG, "process input - index: " + firstSpace + " - " + keyWord + " - " + searchKey);
        } else {
            closeTyingView();
        }
        Log.d(TAG, "[perform] textChanged take: " + (System.nanoTime() - t));
    }

    private TypingItem getCurrentTypingItem(String keyword) {
        for (TypingItem item : typingItems) {
            if (item.getKey().equals(keyword)) {
                return item;
            }
        }
        return null;
    }

    private ArrayList<TypingItem> searchTypingItem(String keyword) {
        ArrayList<TypingItem> list = new ArrayList<>();
        for (TypingItem item : typingItems) {
            if (item.getKey().contains(keyword)) {
                list.add(item);
            }
        }
        return list;
    }

    public void releaseController() {
        listener = null;
        mProgressLoading = null;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicBusiness.TAG_GET_LIST_WATCH_VIDEO);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicBusiness.TAG_SEARCH_VIDEO);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_GET_TOP_SONG");
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_SEARCH_SONG");
    }

    private void closeTyingView() {
        if (mViewFooterTyping.getVisibility() == View.VISIBLE) {
            VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicBusiness.TAG_GET_LIST_WATCH_VIDEO);
            VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicBusiness.TAG_SEARCH_VIDEO);
            VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_GET_TOP_SONG");
            VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_SEARCH_SONG");
            mViewFooterTyping.setVisibility(View.GONE);
            listener.onStateChange(STATE_GONE);
        }
    }

    @Override
    public void onSongClick(Object item) {
        Log.d(TAG, "onSongClick: ");
        if (item instanceof MediaModel) {
            MediaModel mediaModel = (MediaModel) item;
            if (!TextUtils.isEmpty(mediaModel.getMedia_url()) && !TextUtils.isEmpty(mediaModel.getImage())) {
                Log.i(TAG, "found song --> don't need to request");
                showConfirmLeaveAndReInviteOrChangeSong(mediaModel);
            } else {
                getDetailInfoOfSongWhenClick(mediaModel.getIdInt(), mediaModel.getIdentify());
            }
        } else if (item instanceof SearchQuery) {
            SearchQuery searchQueryItem = (SearchQuery) item;
            getDetailInfoOfSongWhenClick(searchQueryItem.getId(), searchQueryItem.getIdentify());
        }
    }

    @Override
    public void onVideoClick(MediaModel model) {
        Log.d(TAG, "onVideoClick: ");
//        mActivity.showLoadingDialog(null, R.string.waiting);
//        mApplication.getMusicBusiness().getDetailWatchVideo(new MusicBusiness.OnGetDetailWatchVideoListener() {
//            @Override
//            public void onResponse(MediaModel video) {
//                mActivity.hideLoadingDialog();
//                listener.onSelectedVideo(video);
//                clearInputWhenSelectedItem();
//            }
//
//            @Override
//            public void onError(int errorCode, String desc) {
//                mActivity.hideLoadingDialog();
//                mActivity.showToast(R.string.e601_error_but_undefined);
//            }
//        }, model);
//        mActivity.hideLoadingDialog();
        listener.onSelectedVideo(model);
        clearInputWhenSelectedItem();
        InputMethodUtils.hideSoftKeyboard(mActivity);
    }

    @Override
    public void onItemTypeClick(TypingItem item) {
        Log.d(TAG, "onItemClick: ");
        mEdtText.setText(item.getKey() + " ");
        mEdtText.setSelection(item.getKey().length() + 1);
    }

    private void showSearchContent(TypingItem currentItem, String searchKey) {
        if (mProgressLoading != null) {
            mProgressLoading.setStart(true);
            mProgressLoading.setVisibility(View.VISIBLE);
        }
        setAdapter(listData, false);
        if (currentItem.getType() == ITEM_TYPE_MUSIC) {
            getDataAndDisplaySong(searchKey);
        } else {
            getDataAndDisplayVideo(searchKey);
        }
    }

    private void getDataAndDisplaySong(final String searchKey) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_GET_TOP_SONG");
        VolleyHelper.getInstance(mApplication).cancelPendingRequests("TAG_SEARCH_SONG");
        if (TextUtils.isEmpty(searchKey)) {
            if (topSongs.isEmpty()) {
                new KeengApi().getTopSong(Constants.KEENG_MUSIC.NUM_TOP_SONG, 1, new Response.Listener<RestTopModel>() {
                    @Override
                    public void onResponse(RestTopModel restTopModel) {
                        if (restTopModel != null && restTopModel.getData() != null && !restTopModel.getData().isEmpty()) {
                            topSongs = restTopModel.getData();
                            drawTopSong();
                        } else {
                            showError();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        showError();
                    }
                });
            } else {
                drawTopSong();
            }
        } else {
            new KeengApi().getSearchSong(searchKey, new Response.Listener<ResfulSearchQueryObj>() {
                @Override
                public void onResponse(ResfulSearchQueryObj resfulSearchQueryObj) {
                    drawSearchSong(handleSearchResponse(resfulSearchQueryObj, searchKey));
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "VolleyError", volleyError);
                    showError();
                }
            });
        }
    }

    private void getDataAndDisplayVideo(final String searchKey) {
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicBusiness.TAG_GET_LIST_WATCH_VIDEO);
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(MusicBusiness.TAG_SEARCH_VIDEO);
        if (TextUtils.isEmpty(searchKey)) {
            mApplication.getMusicBusiness().getListWatchVideo(new MusicBusiness.OnGetListWatchVideoListener() {
                @Override
                public void onResponse(ArrayList<MediaModel> list) {
                    Log.i(TAG, "getListVideo size: " + list.size());
                    if (list.isEmpty()) {
                        showError();
                    } else {
                        drawTopVideo(list);
                    }
                }

                @Override
                public void onError(int errorCode, String desc) {
                    showError();
                }
            }, 1);
        } else {
            mApplication.getMusicBusiness().getListSearchVideo(new MusicBusiness.OnGetListWatchVideoListener() {
                @Override
                public void onResponse(ArrayList<MediaModel> list) {
                    Log.i(TAG, "searchListVideo size: " + list.size());
                    if (list.isEmpty()) {
                        showError();
                    } else {
                        drawSearchVideo(list);
                    }
                }

                @Override
                public void onError(int errorCode, String desc) {
                    showError();
                }
            }, searchKey);
        }
    }

    private void clearInputWhenSelectedItem() {
        mEdtText.setText("");
        listener.onStateChange(STATE_INSERT_MESAGE);
    }

    private void showError() {
        if (mProgressLoading != null) {
            mProgressLoading.setStart(false);
            mProgressLoading.setVisibility(View.GONE);
        }
        mTvwError.setVisibility(View.VISIBLE);
        listData.clear();
        setAdapter(listData, false);
    }

    private void hideLoading() {
        if (mProgressLoading != null) {
            mProgressLoading.setStart(false);
            mProgressLoading.setVisibility(View.GONE);
        }
        mTvwError.setVisibility(View.GONE);
    }

    private void drawTopSong() {
        hideLoading();
        listData.clear();
        listData.addAll(topSongs);
        setAdapter(listData, false);
    }

    private void drawSearchSong(ArrayList<SearchQuery> searchItems) {
        if (searchItems == null || searchItems.isEmpty()) {
            showError();
        } else {
            hideLoading();
            listData.clear();
            listData.addAll(searchItems);
            setAdapter(listData, false);
        }
    }

    private void drawTopVideo(ArrayList<MediaModel> topVideos) {
        hideLoading();
        listData.clear();
        listData.addAll(topVideos);
        setAdapter(listData, false);
    }

    private void drawSearchVideo(ArrayList<MediaModel> searchItems) {
        if (searchItems == null || searchItems.isEmpty()) {
            showError();
        } else {
            hideLoading();
            listData.clear();
            listData.addAll(searchItems);
            setAdapter(listData, false);
            mHeader.setVisibility(View.GONE);
        }
    }

    private void setAdapter(final ArrayList<Object> listItem, boolean isVertical) {
        if (mAdapter == null) {
            mAdapter = new TypingMessageAdapter(mApplication, this);
            mWrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(mAdapter);
            View header = LayoutInflater.from(mApplication).inflate(R.layout.item_upload_song_typing, null);
            mHeader = header.findViewById(R.id.viewHeader);
            mHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) listener.onClickUploadSong();
                }
            });
            mHeader.setVisibility(View.GONE);
            mWrapperAdapter.addHeaderView(header);
            mAdapter.setListItems(listItem);
            mRecyclerView.setAdapter(mWrapperAdapter);
            mRecyclerView.addOnScrollListener(mApplication.getPauseOnScrollRecyclerViewListener(null));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setLayoutManager(isVertical ? mVerticalLayoutManager : mHorizontalLayoutManager);
        } else {
            mRecyclerView.setLayoutManager(isVertical ? mVerticalLayoutManager : mHorizontalLayoutManager);
            mAdapter.setListItems(listItem);
            mAdapter.notifyDataSetChanged();
            mRecyclerView.scrollToPosition(0);
        }
    }

    private ArrayList<SearchQuery> handleSearchResponse(ResfulSearchQueryObj resfulObj, String query) {
        if (mProgressLoading != null) {
            mProgressLoading.setStart(false);
            mProgressLoading.setVisibility(View.GONE);
        }
        if (resfulObj.getGrouped() == null ||
                resfulObj.getGrouped().getType() == null ||
                resfulObj.getGrouped().getType().getGroups() == null ||
                resfulObj.getGrouped().getType().getGroups().isEmpty() ||
                resfulObj.getGrouped().getType().getGroups().get(0) == null ||
                resfulObj.getGrouped().getType().getGroups().get(0).getDoclists() == null ||
                resfulObj.getGrouped().getType().getGroups().get(0).getDoclists().getResSearchQuerys() == null) {
            return null;
        }
        ArrayList<Groups> groups = resfulObj.getGrouped().getType().getGroups();
        ArrayList<SearchQuery> mDatasSong = groups.get(0).getDoclists().getResSearchQuerys();
        if (mDatasSong.isEmpty()) {
            return null;
        } else {
            return SearchHelper.filterSearchData(query, mDatasSong);
        }
    }

    private void getDetailInfoOfSongWhenClick(final long id, final String identify) {
        mActivity.showLoadingDialog(null, R.string.waiting);
        MusicRequestHelper.getInstance(mApplication).getMediaDetail(id, identify, new MusicRequestHelper.GetSongListener() {
            @Override
            public void onResponse(MediaModel object) {
                mActivity.hideLoadingDialog();
                if (!object.isValid()) {
                    mActivity.showToast(R.string.e601_error_but_undefined);
                } else {
                    showConfirmLeaveAndReInviteOrChangeSong(object);
                }
            }

            @Override
            public void onError(String err) {
                mActivity.hideLoadingDialog();
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        }, MusicRequestHelper.TAG_SONG);
        if (mProgressLoading != null) {
            mProgressLoading.setStart(true);
            mProgressLoading.setVisibility(View.VISIBLE);
        }
    }

    private void showConfirmLeaveAndReInviteOrChangeSong(final MediaModel songModel) {
        if (currentThreadMessage == null) {
            clearInputWhenSelectedItem();
            return;
        }
        if (currentThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mApplication.getMessageBusiness().createAndSendMessageInviteMusic(currentThreadMessage, songModel, mActivity);
            clearInputWhenSelectedItem();
        } else {
            int threadType = currentThreadMessage.getThreadType();
            String jid = threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT ?
                    currentThreadMessage.getSoloNumber() : currentThreadMessage.getServerId();
            String name;
            if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                name = mApplication.getMessageBusiness().getFriendName(currentThreadMessage.getSoloNumber());
            } else {
                name = currentThreadMessage.getThreadName();
            }
            mApplication.getMusicBusiness().showConfirmOrNavigateToSelectSong(mActivity, threadType, jid, name, new MusicBusiness.OnConfirmMusic() {
                @Override
                public void onGotoSelect() {
                    mApplication.getMessageBusiness().createAndSendMessageInviteMusic(currentThreadMessage, songModel, mActivity);
                    clearInputWhenSelectedItem();
                }

                @Override
                public void onGotoChange() {
                    mApplication.getMessageBusiness().createAndSendMessageChangeMusic(currentThreadMessage, songModel, mActivity, !currentThreadMessage.isAdmin());
                    clearInputWhenSelectedItem();
                }
            });
        }
    }

    public interface TypingManagerInterface {
        void onStateChange(int state);

        void onSelectedVideo(MediaModel mediaModel);

        void onClickUploadSong();
    }
}

package com.metfone.selfcare.helper;

import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.CommonApi;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.avno.AgencyHistory;
import com.metfone.selfcare.database.model.avno.ConfirmFakeMO;
import com.metfone.selfcare.database.model.avno.DeeplinkItem;
import com.metfone.selfcare.database.model.avno.FakeMOItem;
import com.metfone.selfcare.database.model.avno.InfoAVNO;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.database.model.avno.NumberAVNO;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.database.model.avno.ResultOtp;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.helper.encrypt.RSAEncrypt;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.listeners.SimpleResponseListener;
import com.metfone.selfcare.module.saving.model.SavingDetailModel;
import com.metfone.selfcare.module.saving.model.SavingStatisticsModel;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by toanvk2 on 2/25/2018.
 */

public class AVNOHelper {
    private static final String TAG = AVNOHelper.class.getSimpleName();
    private static final String TAG_REGISTER = "AVNO_REGISTER";
    private static final String TAG_AVNO_SUGGEST = "TAG_AVNO_SUGGEST";
    private static final String TAG_AVNO_SEARCH = "TAG_AVNO_SEARCH";
    private static final String TAG_AVNO_GET_INFO = "TAG_AVNO_GET_INFO";
    private static final String TAG_SAVING_STATISTIC = "TAG_SAVING_STATISTIC";
    private static final String TAG_SAVING_BUDGET_NUM = "TAG_SAVING_BUDGET_NUM";
    private static final String TAG_SAVING_DETAIL = "TAG_SAVING_DETAIL";
    private static final String TAG_AVNO_PAYMENT_SCRATCH_CARD = "TAG_AVNO_PAYMENT_SCRATCH_CARD";
    private static final String TAG_AVNO_PAYMENT_CONVERT_SPOINT = "TAG_AVNO_PAYMENT_CONVERT_SPOINT";
    private static final String TAG_AVNO_REMOVE_IC = "TAG_AVNO_REMOVE_IC";
    private static final String TAG_AVNO_ACTION_PACKAGE = "TAG_AVNO_ACTION_PACKAGE";
    private static final String TAG_ABORT = "AVNO_ABORT";
    private static final String TAG_SET_LIMITED = "TAG_SET_LIMITED";

    private static final String TAG_AGENCY_GET_HISTORY = "TAG_AGENCY_GET_HISTORY";
    private static final String TAG_AGENCY_CREATE_SESSION = "TAG_AGENCY_CREATE_SESSION";
    private static final String TAG_AGENCY_TRANSFER = "TAG_AGENCY_TRANSFER";
    private static final String TAG_AGENCY_CHANGE_PASS = "TAG_AGENCY_CHANGE_PASS";
    private static final String TAG_CREATE_OTP = "TAG_CREATE_OTP";
    private static final String TAG_BUY_PACKAGE_BY_OTP = "TAG_BUY_PACKAGE_BY_OTP";

    private static AVNOHelper mInstance;
    private ApplicationController mApplication;
    private Resources mRes;

    private AVNOHelper(ApplicationController application) {
        this.mApplication = application;
        this.mRes = mApplication.getResources();
    }

    public synchronized static AVNOHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new AVNOHelper(application);
        }
        return mInstance;
    }

    public void registerAVNONumber(final BaseSlidingFragmentActivity activity, final NumberAVNO avnoNumber,
                                   final RegisterAVNOListener listener) {

        if (!TextUtils.isEmpty(mApplication.getReengAccountBusiness().getAVNONumber())) {
            DialogMessage dialogMessage = new DialogMessage(activity, true)
                    .setLabel(mRes.getString(R.string.title_avno_change_number))
                    .setMessage(mRes.getString(R.string.msg_avno_change_number));
            dialogMessage.show();

        } else {
            DialogConfirm dialog = new DialogConfirm(activity, true)
                    .setLabel(null)
                    .setMessage(avnoNumber.getAlert())
                    .setNegativeLabel(mRes.getString(R.string.cancel))
                    .setPositiveLabel(mRes.getString(R.string.ok))
                    .setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            handleRegisterAVNO(activity, avnoNumber, listener);
                        }
                    });
            dialog.show();
        }

    }

    private void handleRegisterAVNO(final BaseSlidingFragmentActivity activity, final NumberAVNO avnoNumber, final RegisterAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_REGISTER);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.AVNO_REGISTER);
        Log.i(TAG, "registerAVNONumber: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "registerAVNONumber onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                .getReengAccountBusiness().getToken());
                        Log.i(TAG, "registerAVNONumber decryptResponse: " + decryptResponse);
                        try {
                            JSONObject object = new JSONObject(decryptResponse);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC);
                            if (code == HTTPCode.E200_OK) {
                                mApplication.getReengAccountBusiness().updateAVNONumber(avnoNumber.getNumber());
                                listener.onSuccess(desc);
                            } else if (code == 201) {
                                gotoWebViewCharge(activity);
                                /*mApplication.getReengAccountBusiness().updateAVNONumber(avnoNumber.getNumber());
                                String title = object.optString("title_dialog");
                                String des = object.optString("desc_dialog");
                                DialogConfirm dialog = new DialogConfirm(activity, true)
                                        .setLabel(title)
                                        .setMessage(des)
                                        .setNegativeLabel(mRes.getString(R.string.cancel))
                                        .setPositiveLabel(mRes.getString(R.string.ok))
                                        .setPositiveListener(new PositiveListener<Object>() {
                                            @Override
                                            public void onPositive(Object result) {
                                                if (activity instanceof AVNOActivity) {
                                                    ((AVNOActivity) activity).navigateToAVNOFragment();
                                                }
                                                gotoWebViewCharge(activity);
                                            }
                                        })
                                        .setNegativeListener(new NegativeListener() {
                                            @Override
                                            public void onNegative(Object result) {
                                                if (activity instanceof AVNOActivity) {
                                                    ((AVNOActivity) activity).navigateToAVNOFragment();
                                                }
                                            }
                                        });
                                dialog.show();*/
                            } else {
                                listener.onError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(avnoNumber.getNumber()).
                        append(mApplication.getReengAccountBusiness().getOperator()).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("virtual_number", avnoNumber.getNumber());
                params.put("currOperator", mApplication.getReengAccountBusiness().getOperator());
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void gotoWebViewCharge(BaseSlidingFragmentActivity activity) {
        String url = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                .AVNO_PAYMENT_WAPSITE);
        if (TextUtils.isEmpty(url) && activity != null) {
            activity.showToast(R.string.e601_error_but_undefined);
        }
        url = url + "?search=" + Uri.encode(RSAEncrypt.getInStance(mApplication).encrypt(mApplication, mApplication
                .getReengAccountBusiness().getJidNumber()));
        Log.i(TAG, "URL: " + url);
        UrlConfigHelper.gotoWebViewFullScreenPayment(mApplication, activity, url);
    }

    public void getSuggestNumberAVNO(final GetListNumberAVNOLIstener listener, int page) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_SUGGEST);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.AVNO_SUGGEST);
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(page).
                append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                append(mApplication.getReengAccountBusiness().getRegionCode()).
                append(account.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("page", String.valueOf(page));
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);

        Log.i(TAG, "getSuggestNumberAVNO: " + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "getSuggestNumberAVNO onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                .getReengAccountBusiness().getToken());
                        Log.i(TAG, "getSuggestNumberAVNO decryptResponse: " + decryptResponse);
                        try {
                            JSONObject object = new JSONObject(decryptResponse);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                ArrayList<NumberAVNO> list = parserListAVNO(object.optString("listSuggestion"));
                                if (list != null || !list.isEmpty()) {
                                    listener.onSuccess(list);
                                } else {
                                    listener.onError(-1, "");
                                }
                            } else {
                                listener.onError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_SUGGEST, false);
    }

    private ArrayList<NumberAVNO> parserListAVNO(String listSuggestion) {
        ArrayList<NumberAVNO> list = new ArrayList<>();
        if (TextUtils.isEmpty(listSuggestion)) {
            return list;
        }
        try {
            JSONArray jsonArray = new JSONArray(listSuggestion);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String avnoNumber = jsonObject.optString("virtualNumber");
                String price = jsonObject.optString("price");
                String alert = jsonObject.optString("alert");
                if (!TextUtils.isEmpty(avnoNumber)) {
                    list.add(new NumberAVNO(avnoNumber, price, alert));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "JSONException", e);
        }
        return list;
    }

    public void searchNumberAVNO(String numberSearch, final GetListNumberAVNOLIstener listener, int page) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_SEARCH);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.AVNO_SEARCH);
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(numberSearch).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(page).
                append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                append(mApplication.getReengAccountBusiness().getRegionCode()).
                append(account.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("virtual_number", numberSearch);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("page", String.valueOf(page));
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);

        Log.i(TAG, "searchNumberAVNO: " + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "searchNumberAVNO onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                .getReengAccountBusiness().getToken());
                        Log.i(TAG, "searchNumberAVNO decryptResponse: " + decryptResponse);
                        try {
                            JSONObject object = new JSONObject(decryptResponse);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                ArrayList<NumberAVNO> list = parserListAVNO(object.optString("listSearch"));
                                if (list != null) {
                                    listener.onSuccess(list);
                                } else {
                                    listener.onError(-1, "");
                                }
                            } else {
                                listener.onError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_SEARCH, false);
    }

    private InfoAVNO parserInfoAVNO(JSONObject data) throws JSONException {
        InfoAVNO infoAVNO = new InfoAVNO();
        String balance = data.optString("balance");
        infoAVNO.setBalanceAccount(balance);
        infoAVNO.setActiveState(data.optInt("active", 1));
        infoAVNO.setAvnoNumber(data.optString("virtualNumber"));
        infoAVNO.setTopUp(data.optInt("topup", 0));
        infoAVNO.setTransferMoney(data.optInt("is_tranfer", 0));

        JSONArray jsArrayPackage = data.optJSONArray("package");
        ArrayList<PackageAVNO> listPackageActive = null;
        if (jsArrayPackage != null && jsArrayPackage.length() != 0) {
            listPackageActive = parseListPackage(jsArrayPackage, 0);
            infoAVNO.setListPackageActive(listPackageActive);
        }

        JSONArray jsArrayPackageHot = data.optJSONArray("packageHot");
        if (jsArrayPackageHot != null && jsArrayPackageHot.length() != 0) {
            ArrayList<PackageAVNO> listPackageHot = parseListPackage(jsArrayPackageHot, infoAVNO.getListPackageActive().size());
            infoAVNO.setListPackageAvailable(listPackageHot);
        }

        JSONArray jsArrayGift = data.optJSONArray("gifts");
        ArrayList<ItemInfo> listGift = new ArrayList<>();
        if (jsArrayGift != null && jsArrayGift.length() != 0) {
            for (int i = 0; i < jsArrayGift.length(); i++) {
                JSONObject jsGift = jsArrayGift.getJSONObject(i);
                String title = jsGift.optString("title");
                String deepLinkLabel = jsGift.optString("deeplink_label");
                String deeplink = jsGift.optString("deeplink");
                ItemInfo gift = new ItemInfo(title, null, deepLinkLabel, deeplink);
                listGift.add(gift);
            }
            infoAVNO.setListGift(listGift);
        }

        if (!mApplication.getReengAccountBusiness().isAvnoEnable()) {
            JSONObject jsoInfo = data.optJSONObject("more_info");
            if (jsoInfo != null) {
                String title = jsoInfo.optString("title");
                String avt = jsoInfo.optString("avatar");
                String desc = jsoInfo.optString("desc");

                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(desc)) {
                    PackageAVNO packageInfo = new PackageAVNO(-1, title, desc, 0);
                    packageInfo.setImgUrl(avt);
                    listPackageActive.add(packageInfo);
                }
            }
        } else {
            JSONArray jaInfo = data.optJSONArray("more_info_avno");
            if (jaInfo != null && jaInfo.length() > 0) {
                ArrayList<ItemInfo> listInfo = new ArrayList<>();
                for (int i = 0; i < jaInfo.length(); i++) {
                    JSONObject jsoInfo = jaInfo.getJSONObject(i);
                    String title = jsoInfo.optString("title");
                    String desc = jsoInfo.optString("desc");

                    if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(desc)) {
                        ItemInfo itemInfo = new ItemInfo(title, desc, null, null);
                        listInfo.add(itemInfo);
                    }
                }
                infoAVNO.setAccountInfoList(listInfo);
            }

            String introPackage = data.optString("introPackage");
            JSONObject jsIntro = data.optJSONObject("intro");
            infoAVNO.setIntroPackage(introPackage);
            if (jsIntro != null) {
                ItemInfo intro = new ItemInfo(jsIntro.optString("title"), jsIntro.optString("desc"), jsIntro.optString("label"), null);
                infoAVNO.setIntro(intro);
            }
        }
        return infoAVNO;
    }

    private ArrayList<PackageAVNO> parseListPackage(JSONArray jsArrayPackage, int numbAddIndex) throws JSONException {
        ArrayList<PackageAVNO> listPackageActive = new ArrayList<>();

        for (int i = 0; i < jsArrayPackage.length(); i++) {
            JSONObject jsPackage = jsArrayPackage.getJSONObject(i);
            int id = jsPackage.optInt("id");
            String title = jsPackage.optString("title");
            String descPackage = jsPackage.optString("desc");
            String imgUrl = jsPackage.optString("avatar");
            int inUse = jsPackage.optInt("in_use", 0);

            String minute = jsPackage.optString("minute");
            String number = jsPackage.optString("number");
            String price = jsPackage.optString("price");
            int maxNumb = jsPackage.optInt("maxNumb");

            JSONArray jaNumb = jsPackage.optJSONArray("listNumber");
            ArrayList<String> listNumb = null;
            if (jaNumb != null && jaNumb.length() != 0) {
                listNumb = new ArrayList<>();
                for (int j = 0; j < jaNumb.length(); j++) {
                    String numb = jaNumb.optString(j);
                    listNumb.add(numb);
                }

            }

            PackageAVNO packageAVNO = new PackageAVNO();
            packageAVNO.setImgUrl(imgUrl);
            packageAVNO.setId(id);
            packageAVNO.setTitle(title);
            packageAVNO.setDesc(descPackage);
            packageAVNO.setInUse(inUse);

            packageAVNO.setMinutes(minute);
            packageAVNO.setNumbInString(number);
            packageAVNO.setPrice(price);
            packageAVNO.setMaxNumb(maxNumb);
            packageAVNO.setListNumber(listNumb);
            packageAVNO.setIndexColor(numbAddIndex + i % 9);

            JSONArray jsDeeplink = jsPackage.optJSONArray("deeplinks");
            if (jsDeeplink != null) {
                ArrayList<DeeplinkItem> deeplinkItems = new ArrayList<>();
                for (int j = 0; j < jsDeeplink.length(); j++) {
                    String dLabel = jsDeeplink.getJSONObject(j).optString("label");
                    String dUrl = jsDeeplink.getJSONObject(j).optString("url");
                    int stateButton = jsDeeplink.getJSONObject(j).optInt("state_button");
                    DeeplinkItem deeplinkItem = new DeeplinkItem(dLabel, dUrl, stateButton);
                    deeplinkItem.setNextLabel(jsDeeplink.getJSONObject(j).optString("next_label"));
                    deeplinkItems.add(deeplinkItem);
                }
                packageAVNO.setDeeplinkItems(deeplinkItems);
            }

            JSONArray jsFakeMO = jsPackage.optJSONArray("fake_mo");
            if (jsFakeMO != null) {
                ArrayList<FakeMOItem> fakeMOItems = new ArrayList<>();

                for (int k = 0; k < jsFakeMO.length(); k++) {
                    String fLabel = jsFakeMO.getJSONObject(k).optString("label");
                    String fDesc = jsFakeMO.getJSONObject(k).optString("desc");
                    String fContent = jsFakeMO.getJSONObject(k).optString("content");
                    String fCmd = jsFakeMO.getJSONObject(k).optString("cmd");
                    int stateButton = jsFakeMO.getJSONObject(k).optInt("state_button");
                    FakeMOItem fakeMOItem = new FakeMOItem(fLabel, fDesc, fCmd, fContent, stateButton);
                    JSONObject jsoConfirm = jsFakeMO.getJSONObject(k).optJSONObject("confirm");
                    if (jsoConfirm != null) {
                        String cDesc = jsoConfirm.optString("desc");
                        String cLabel = jsoConfirm.optString("label");
                        String cUrl = jsoConfirm.optString("url");
                        ConfirmFakeMO confirmFakeMO = new ConfirmFakeMO(cLabel, cDesc, cUrl);
                        fakeMOItem.setConfirmFakeMO(confirmFakeMO);
                    }
                    fakeMOItems.add(fakeMOItem);
                }
                packageAVNO.setFakeMOItems(fakeMOItems);
            }
            listPackageActive.add(packageAVNO);
        }
        return listPackageActive;

    }

    public void chargePaymentScratchCard(final BaseSlidingFragmentActivity activity, final String pinCode,
                                         final String serialCard, final ChargePaymentListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_PAYMENT_SCRATCH_CARD);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .AVNO_PAYMENT_SCRATCH_CARD);
        Log.i(TAG, "chargePaymentScratchCard: " + url);
        activity.showLoadingDialog("", R.string.loading);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        activity.hideLoadingDialog();
                        Log.i(TAG, "chargePaymentScratchCard onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(-1, desc);
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                activity.hideLoadingDialog();
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(serialCard).
                        append(pinCode).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("pincode", pinCode);
                params.put("serial", serialCard);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void convertPaymentSPoint(final long point, final ChargePaymentListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_PAYMENT_CONVERT_SPOINT);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                .AVNO_PAYMENT_CONVERT_SPOINT);
        Log.i(TAG, "convertPaymentSPoint: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "convertPaymentSPoint onResponse: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                .getReengAccountBusiness().getToken());
                        Log.i(TAG, "convertPaymentSPoint decryptResponse: " + decryptResponse);
                        try {
                            JSONObject object = new JSONObject(decryptResponse);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(point).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("point", String.valueOf(point));
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_PAYMENT_CONVERT_SPOINT, false);
    }

    public void removeIdentifyCard(final boolean isFront, final RemoveIdentifyCardListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onRemoveError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_REMOVE_IC);
        final String url = UrlConfigHelper.getInstance(mApplication)
                .getUrlConfigOfImage(Config.UrlEnum.AVNO_REMOVE_IC);
        Log.i(TAG, "removeIdentifyCard: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onRemoveSuccess();
                            } else {
                                listener.onRemoveError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onRemoveError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onRemoveError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(isFront ? 1 : 2).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("type", String.valueOf(isFront ? 1 : 2));
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_REMOVE_IC, false);
    }

    public void actionPackageAVNO(boolean isRegister, final String idPkg, final ActionPackageAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onActionPackageError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_ACTION_PACKAGE);
        String url;
        if (isRegister) {
            url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                    .CALLOUT_REGISTER_PACKAGE);
        } else {
            url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                    .CALLOUT_CANCEL_PACKAGE);
        }
        Log.i(TAG, "actionPackageAVNO: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onActionPackageSuccess(desc);
                            } else {
                                listener.onActionPackageError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onActionPackageError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onActionPackageError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                int calloutState = mApplication.getReengAccountBusiness().getCallOutState();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(idPkg).
                        append(calloutState).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("packageId", String.valueOf(idPkg));
                params.put("callout", String.valueOf(calloutState));
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_REMOVE_IC, false);
    }

    public void registerFreeNumberPackage(final PhoneNumber phoneNumber, final String packageId, final SimpleResponseListener
            listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_REGISTER);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CALLOUT_REGISTER_FREE);
        Log.i(TAG, "registerFreeNumberPackage: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "registerFreeNumberPackage onResponse: " + response);
                        /*String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                .getReengAccountBusiness().getToken());*/
//                        Log.i(TAG, "registerFreeNumberPackage decryptResponse: " + decryptResponse);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                int calloutState = mApplication.getReengAccountBusiness().getCallOutState();
                String friendOperator = phoneNumber.getOperatorPresence();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(packageId).
                        append(calloutState).
                        append(phoneNumber.getJidNumber()).
                        append(friendOperator).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("packageId", packageId);
                params.put("callout", String.valueOf(calloutState));
                params.put("contact", phoneNumber.getJidNumber());
                params.put("currOperator", friendOperator);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void requestBroadcastChangeNumberAVNO(final ApplicationController app,
                                                 final BaseSlidingFragmentActivity mParentActivity,
                                                 final ArrayList<String> listFriends,
                                                 final BroadcastChangeNumberAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(app)) {
            listener.onBroadcastError(-2, mRes.getString(R.string
                    .no_connectivity_check_again));
            return;
        }
        String msg = app.getString(R.string.waiting);
        mParentActivity.showLoadingDialog(null, msg);
        if (mRes == null) mRes = app.getResources();
        //
        String url = UrlConfigHelper.getInstance(app).getUrlConfigOfFile(Config.UrlEnum.BROADCAST_CHANGE_NUMBER_AVNO);
        //String url = " http://125.235.38.8:8080/ReengBackendBiz/campaign/submit";

        Log.d(TAG, "requestBroadcastChangeNumberAVNO url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse:" + response);
                int errorCode = -1;
                try {
                    mParentActivity.hideLoadingDialog();
                    JSONObject responseObject = new JSONObject(response);
                    errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                    String desc = responseObject.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                            .e601_error_but_undefined));
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onBroadcastSuccess(desc);
                    } else {
                        listener.onBroadcastError(errorCode, desc);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onBroadcastError(errorCode, mRes.getString(R.string
                            .e601_error_but_undefined));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mParentActivity.hideLoadingDialog();
                listener.onBroadcastError(-1, mRes.getString(R.string
                        .e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                HashSet<String> hashSetFriends = new HashSet<>(listFriends);
                long currentTime = TimeHelper.getCurrentTime();
                JSONArray jsonArray = new JSONArray();
                for (String number : hashSetFriends) {
                    jsonArray.put(number);
                }
                String receiverString = jsonArray.toString();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(account.getName()).
                        append(account.getAvnoNumber()).
                        append(receiverString).
                        append("Android").
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                //////////////////////////
                String dataEncrypt = HttpHelper.encryptDataV2(app, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("name", account.getName());
                params.put("virtual_number", account.getAvnoNumber());
                params.put("contacts", receiverString);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.REST_CLIENT_TYPE, "Android");
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "requestBroadcastChangeNumberAVNO params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance((ApplicationController) mParentActivity.getApplication()).addRequestToQueue(request,
                "DeepLinkCampaign", false);
    }

    public void deleteAVNONumber(final String avnoNumber, final SimpleResponseListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_ABORT);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.AVNO_ABORT);
        Log.i(TAG, "deleteAVNONumber: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "deleteAVNONumber onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onSuccess(desc);
                            } else {
                                listener.onError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(avnoNumber).
                        append(mApplication.getReengAccountBusiness().getOperator()).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("virtual_number", avnoNumber);
                params.put("currOperator", mApplication.getReengAccountBusiness().getOperator());
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void getInfoAVNOV4(final GetInfoAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onGetInfoAVNOError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_GET_INFO);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CALLOUT_GET_INFO);

        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(mApplication.getReengAccountBusiness().getCallOutState()).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                append(mApplication.getReengAccountBusiness().getRegionCode()).
                append(account.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("callout", mApplication.getReengAccountBusiness().getCallOutState());

        Log.i(TAG, "getInfoAVNO: " + params.toString());
        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "getInfoAVNOV3 onResponse: " + response);
                        try {
                            JSONObject js = new JSONObject(response);
                            if ("200".equals(js.optString("code"))) {
                                String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                        .getReengAccountBusiness().getToken());
                                Log.i(TAG, "getInfoAVNO decryptResponse: " + decryptResponse);
                                JSONObject object = new JSONObject(decryptResponse);
                                int code = object.optInt(Constants.HTTP.REST_CODE);
                                String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                        .e601_error_but_undefined));
                                if (code == HTTPCode.E200_OK) {
                                    JSONObject data = object.optJSONObject("data");

                                    if (data != null) {
                                        InfoAVNO infoAVNO = parserInfoAVNO(data);
                                        listener.onGetInfoAVNOSuccess(infoAVNO);
                                        String time = data.optString("balance_callout", "");
                                        if (!TextUtils.isEmpty(time)) {
                                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_TIME_CALLOUT_REMAIN, time).apply();
                                            TabMobileFragment.SubTabCallEvent event = new TabMobileFragment.SubTabCallEvent();
                                            event.setTimeRemain(time);
                                            EventBus.getDefault().post(event);
                                        }
                                    } else if (object.has("desc")) {
                                        desc = object.getString("desc");
                                        listener.onGetInfoAVNOError(code, desc);
                                    } else {
                                        listener.onGetInfoAVNOError(-1, desc);
                                    }
                                } else {
                                    listener.onGetInfoAVNOError(-1, desc);
                                }
                            } else {
                                listener.onGetInfoAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onGetInfoAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onGetInfoAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_GET_INFO, false);
    }

    public void actionPackageAVNOV2(boolean isRegister, final int idPkg, final int channelId,
                                    final ActionPackageAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onActionPackageError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AVNO_ACTION_PACKAGE);
        String url;
        if (isRegister) {
            url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                    .AVNO_REGISTER_PACKAGE_V2);
        } else {
            url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum
                    .AVNO_CANCEL_PACKAGE_V2);
        }
        Log.i(TAG, "actionPackageAVNO: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.optInt(Constants.HTTP.REST_CODE);
                            String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            if (code == HTTPCode.E200_OK) {
                                listener.onActionPackageSuccess(desc);
                            } else {
                                listener.onActionPackageError(-1, desc);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onActionPackageError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onActionPackageError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(idPkg).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(channelId).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("packageId", String.valueOf(idPkg));
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put("channelId", String.valueOf(channelId));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AVNO_REMOVE_IC, false);
    }

    public void setCalledLimited(final String packageId, final String contact, final String operator, final SimpleResponseListener
            listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SET_LIMITED);
        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.CALLOUT_SET_CALL_LIMITED);
        Log.i(TAG, "setCalledLimited: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        /*String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                .getReengAccountBusiness().getToken());*/
                        Log.i(TAG, "setCalledLimited onResponse: " + response);
                        if (!TextUtils.isEmpty(response)) {
                            try {
                                JSONObject object = new JSONObject(response);
                                int code = object.optInt(Constants.HTTP.REST_CODE);
                                String desc = object.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string
                                        .e601_error_but_undefined));
                                if (code == HTTPCode.E200_OK) {
                                    listener.onSuccess(desc);
                                } else {
                                    listener.onError(-1, desc);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                            }
                        } else {
                            listener.onError(-1, mRes.getString(R.string
                                    .e601_error_but_undefined));
                            Log.e(TAG, "error");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                int calloutState = mApplication.getReengAccountBusiness().getCallOutState();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(packageId).
                        append(calloutState).
                        append(contact).
                        append(operator).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("packageId", packageId);
                params.put("contact", contact);
                params.put("callout", String.valueOf(calloutState));
                params.put("currOperator", operator);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SET_LIMITED, false);
    }

    public void getSavingStatistic(final GetSavingStatisticListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onGetSavingStatisticError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SAVING_STATISTIC);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + ConstantApi.Url.File.API_AVNO_GET_SAVING_STATISTIC;
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        String accountToken = account.getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(account.getJidNumber());
        sb.append(mApplication.getReengAccountBusiness().getCallOutState());
        sb.append(mApplication.getReengAccountBusiness().getOperator());
        sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
        sb.append(Config.REVISION);
        sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
        sb.append(mApplication.getReengAccountBusiness().getRegionCode());
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("callout", mApplication.getReengAccountBusiness().getCallOutState());
        params.addParam("currOperator", mApplication.getReengAccountBusiness().getOperator());
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject js = new JSONObject(response);
                            if ("200".equals(js.optString("code"))) {
                                String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                        .getReengAccountBusiness().getToken());
                                Log.i(TAG, "getSavingStatistic decryptResponse: " + decryptResponse);
                                JSONObject object = new JSONObject(decryptResponse);
                                JSONObject data = object.optJSONObject("data");
                                if (data != null) {
                                    listener.onGetSavingStatisticSuccess(new Gson().fromJson(data.toString(), SavingStatisticsModel.class));
                                } else
                                    listener.onGetSavingStatisticSuccess(null);
                            } else {
                                listener.onGetSavingStatisticError(-1, mRes.getString(R.string.e601_error_but_undefined));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onGetSavingStatisticError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onGetSavingStatisticError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SAVING_STATISTIC, false);
    }

    public void getSavingDetail(String startDate, String endDate, int page, final GetSavingDetailListener listener) {
//        startDate: Định dạng 2019-03-10
//		endDate: Định dạng 2019-03-10
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            if (listener != null)
                listener.onGetSavingDetailError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SAVING_DETAIL);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + ConstantApi.Url.File.API_AVNO_GET_SAVING_DETAIL;
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        String msisdn = account.getJidNumber();
        String accountToken = account.getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn);
        sb.append(mApplication.getReengAccountBusiness().getCallOutState());
        sb.append(startDate);
        sb.append(endDate);
        sb.append(page);
        sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
        sb.append(Config.REVISION);
        sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
        sb.append(mApplication.getReengAccountBusiness().getRegionCode());
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", msisdn);
        params.addParam("callout", mApplication.getReengAccountBusiness().getCallOutState());
        params.addParam("startDate", startDate);
        params.addParam("endDate", endDate);
        params.addParam("page", page);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject js = new JSONObject(response);
                            if ("200".equals(js.optString("code"))) {
                                String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                        .getReengAccountBusiness().getToken());
                                Log.i(TAG, "getSavingDetail decryptResponse: " + decryptResponse);
                                JSONObject object = new JSONObject(decryptResponse);
                                JSONObject data = object.optJSONObject("data");
                                if (data != null) {
                                    if (listener != null)
                                        listener.onGetSavingDetailSuccess(new Gson().fromJson(data.toString(), SavingDetailModel.class));
                                } else if (listener != null)
                                    listener.onGetSavingDetailSuccess(null);
                            } else {
                                if (listener != null)
                                    listener.onGetSavingDetailError(-1, mRes.getString(R.string.e601_error_but_undefined));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            if (listener != null)
                                listener.onGetSavingDetailError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (listener != null)
                    listener.onGetSavingDetailError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SAVING_DETAIL, false);
    }

    public void getSavingBudgetNum() {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_SAVING_BUDGET_NUM);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + ConstantApi.Url.File.API_AVNO_GET_SAVING_BUDGET_NUM;
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        String accountToken = account.getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(account.getJidNumber());
        sb.append(mApplication.getReengAccountBusiness().getCallOutState());
        sb.append(mApplication.getReengAccountBusiness().getOperator());
        sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
        sb.append(Config.REVISION);
        sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
        sb.append(mApplication.getReengAccountBusiness().getRegionCode());
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("callout", mApplication.getReengAccountBusiness().getCallOutState());
        params.addParam("currOperator", mApplication.getReengAccountBusiness().getOperator());
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject js = new JSONObject(response);
                            if ("200".equals(js.optString("code"))) {
                                String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                        .getReengAccountBusiness().getToken());
                                Log.i(TAG, "getSavingBudgetNum decryptResponse: " + decryptResponse);
                                JSONObject object = new JSONObject(decryptResponse);
                                JSONObject data = object.optJSONObject("data");
                                String totalSaving = data.getString("total_saving_shortcut");
                                if (totalSaving != null) {
                                    mApplication.getPref().edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_GET_SAVING_BUDGET, System.currentTimeMillis()).apply();
                                    mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_MONEY_SAVING, totalSaving).apply();
//                                    TabMobileFragment.SubTabCallEvent event = new TabMobileFragment.SubTabCallEvent();
//                                    event.setTotalSavingShortcut(totalSaving);
//                                    EventBus.getDefault().post(event);
                                    EventBus.getDefault().postSticky(new TabHomeEvent().setUpdateFeature(true));
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_SAVING_BUDGET_NUM, false);
    }

    public void getHistoryAgency(int page, final AgencyApiLister lister) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            lister.onFail(-1, mRes.getString(R.string.no_connectivity_check_again));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AGENCY_GET_HISTORY);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + ConstantApi.Url.File.API_AVNO_GET_HISTORY;
        final ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        String accountToken = account.getToken();
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(account.getJidNumber());
        sb.append(mApplication.getReengAccountBusiness().getCallOutState());
        sb.append(page);
        sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
        sb.append(Config.REVISION);
        sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
        sb.append(mApplication.getReengAccountBusiness().getRegionCode());
        sb.append(accountToken);
        sb.append(currentTime);
        final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", account.getJidNumber());
        params.addParam("callout", mApplication.getReengAccountBusiness().getCallOutState());
        params.addParam("page", String.valueOf(page));
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
        params.addParam("timestamp", String.valueOf(currentTime));
        params.addParam("security", dataEncrypt);

        Log.i(TAG, "getHistoryAgency: " + params.toString());

        StringRequest request = new StringRequest(Request.Method.GET, params.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String decryptData = HttpHelper.decryptResponse(response, account.getToken());
                            if (!TextUtils.isEmpty(decryptData)) {
                                JSONObject jsonObject = new JSONObject(decryptData);
                                if (jsonObject.optInt("code") == 200) {
                                    JSONArray jsonArray = jsonObject.optJSONArray("lst_his");
                                    if (jsonArray != null) {
                                        ArrayList<AgencyHistory> list = new ArrayList<>();
                                        if (jsonArray.length() != 0) {
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject jsO = jsonArray.getJSONObject(i);
                                                AgencyHistory agencyHistory = new AgencyHistory();
                                                agencyHistory.setId(jsO.optString("id"));
                                                agencyHistory.setName(jsO.getString("name"));
                                                String msisdn = jsO.getString("msisdn");
                                                agencyHistory.setMsisdn(msisdn);
                                                PhoneNumber p = mApplication.getContactBusiness().getPhoneNumberFromNumber(msisdn);
                                                if (p != null && !TextUtils.isEmpty(p.getName()))
                                                    agencyHistory.setName(p.getName());
                                                agencyHistory.setAmount(jsO.getString("amount"));
                                                agencyHistory.setUnit(jsO.getString("unit"));
                                                agencyHistory.setLastAvt(jsO.getString("last_avatar"));
                                                agencyHistory.setTimeStamp(jsO.getString("created_date"));
                                                list.add(agencyHistory);
                                            }
                                        }
                                        lister.onGetHistoryDone(list);
                                        return;
                                    } else
                                        return;
                                }
                            }
                            lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));

                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AGENCY_GET_HISTORY, false);
    }

    public void createSessionAgency(final String jidFriend, final String type, final String pass,
                                    final String amount, final String note, final AgencyApiLister lister) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            lister.onFail(-1, mRes.getString(R.string.no_connectivity_check_again));
            return;
        }

        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AGENCY_CREATE_SESSION);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + CommonApi.Url.File.API_AVNO_CREATE_SESSION;
        final ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        Log.i(TAG, "createSessionAgency: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String desc = null;
                        try {
                            String decryptData = HttpHelper.decryptResponse(response, account.getToken());
                            if (!TextUtils.isEmpty(decryptData)) {
                                JSONObject jsonObject = new JSONObject(decryptData);
                                desc = jsonObject.optString("desc");
                                if (jsonObject.optInt("code") == 200) {
                                    String ssid = jsonObject.optString("ssid");
                                    if (!TextUtils.isEmpty(ssid)) {
                                        lister.onCreateSessionDone(ssid);
                                        return;
                                    }
                                }
                            }
                            lister.onFail(0, TextUtils.isEmpty(desc) ? mRes.getString(R.string.e601_error_but_undefined) : desc);

                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                String accountToken = account.getToken();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder();
                sb.append(account.getJidNumber());
                sb.append(jidFriend);
                sb.append(mApplication.getReengAccountBusiness().getCallOutState());
                sb.append(type);
                sb.append(pass);
                sb.append(amount);
                sb.append(note);
                sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
                sb.append(Config.REVISION);
                sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
                sb.append(mApplication.getReengAccountBusiness().getRegionCode());
                sb.append(accountToken);
                sb.append(currentTime);
                final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("friend", jidFriend);
                params.put("callout", String.valueOf(mApplication.getReengAccountBusiness().getCallOutState()));
                params.put("type", type);
                params.put("password", pass);
                params.put("amount", amount);
                params.put("note", note);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AGENCY_CREATE_SESSION, false);
    }

    public void transferMoneyAgency(final String ssid, final String otp, final AgencyApiLister lister) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            lister.onFail(-1, mRes.getString(R.string.no_connectivity_check_again));
            return;
        }

        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AGENCY_TRANSFER);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + ConstantApi.Url.File.API_AVNO_TRANSFER_MONEY;
        final ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        Log.i(TAG, "transferMoneyAgency: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String desc = null;
                        try {
                            String decryptData = HttpHelper.decryptResponse(response, account.getToken());
                            if (!TextUtils.isEmpty(decryptData)) {
                                JSONObject jsonObject = new JSONObject(decryptData);
                                desc = jsonObject.optString("desc");
                                if (jsonObject.optInt("code") == 200) {
                                    lister.onTransferMoneyDone(desc);
                                    return;
                                }
                            }
                            lister.onFail(0, TextUtils.isEmpty(desc) ? mRes.getString(R.string.e601_error_but_undefined) : desc);

                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                String accountToken = account.getToken();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder();
                sb.append(account.getJidNumber());
                sb.append(mApplication.getReengAccountBusiness().getCallOutState());
                sb.append(ssid);
                sb.append(otp);
                sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
                sb.append(Config.REVISION);
                sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
                sb.append(mApplication.getReengAccountBusiness().getRegionCode());
                sb.append(accountToken);
                sb.append(currentTime);
                final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("callout", String.valueOf(mApplication.getReengAccountBusiness().getCallOutState()));
                params.put("ssid", ssid);
                params.put("otp", otp);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AGENCY_TRANSFER, false);
    }

    public void changeOrResetPassAgency(final String ssid, final String otp, final AgencyApiLister lister) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            lister.onFail(-1, mRes.getString(R.string.no_connectivity_check_again));
            return;
        }

        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_AGENCY_CHANGE_PASS);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + CommonApi.Url.File.API_AVNO_CHANGE_PASS;
        final ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();

        Log.i(TAG, "changeOrResetPassAgency: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String desc = null;
                        try {
                            String decryptData = HttpHelper.decryptResponse(response, account.getToken());
                            if (!TextUtils.isEmpty(decryptData)) {
                                JSONObject jsonObject = new JSONObject(decryptData);
                                desc = jsonObject.optString("desc");
                                if (jsonObject.optInt("code") == 200) {
                                    lister.onChangeResetPassDone(desc);
                                    return;
                                }
                            }
                            lister.onFail(0, TextUtils.isEmpty(desc) ? mRes.getString(R.string.e601_error_but_undefined) : desc);

                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                lister.onFail(0, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                String accountToken = account.getToken();
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder();
                sb.append(account.getJidNumber());
                sb.append(mApplication.getReengAccountBusiness().getCallOutState());
                sb.append(ssid);
                sb.append(otp);
                sb.append(Constants.HTTP.CLIENT_TYPE_STRING);
                sb.append(Config.REVISION);
                sb.append(mApplication.getReengAccountBusiness().getCurrentLanguage());
                sb.append(mApplication.getReengAccountBusiness().getRegionCode());
                sb.append(accountToken);
                sb.append(currentTime);
                final String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("callout", String.valueOf(mApplication.getReengAccountBusiness().getCallOutState()));
                params.put("ssid", ssid);
                params.put("otp", otp);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_AGENCY_CHANGE_PASS, false);
    }

    public void createOtpBuyPackgeAVNO(String idPacakgeOrVitualNumber, final GetResultOtpAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onGetResultOtpAVNOError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        final ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_CREATE_OTP);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + CommonApi.Url.File.API_AVNO_CREATE_OTP;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.optInt("result");
                    String desc = object.optString("error_msg");
                    String sectionId = object.optString("session_id");
                    if (TextUtils.isEmpty(desc))
                        desc = mRes.getString(R.string.e601_error_but_undefined);
                    ResultOtp resultOtp = new ResultOtp(code, desc, sectionId);
                    if (code == HTTPCode.E200_OK) {
                        listener.onGetResultOtpAVNOSuccess(resultOtp);
                    } else {
                        listener.onGetResultOtpAVNOError(-1, desc);
                    }
                } catch (Exception error) {
                    Log.e(TAG, "VolleyError", error);
                    listener.onGetResultOtpAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onGetResultOtpAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                String accountToken = account.getToken();
                long currentTime = TimeHelper.getCurrentTime();
                String languageCode = mApplication.getReengAccountBusiness().getCurrentLanguage();
                String countryCode = mApplication.getReengAccountBusiness().getRegionCode();

                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(idPacakgeOrVitualNumber).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(languageCode).
                        append(countryCode).
                        append(accountToken).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("packageIdOrVirtualNumber", idPacakgeOrVitualNumber);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, "Android");
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, languageCode);
                params.put(Constants.HTTP.REST_COUNTRY_CODE, countryCode);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_CREATE_OTP, false);
    }

    public void buyPackageAVNOByOtp(String otp, String sectionId, String id,
                                    final GetResultOtpAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onGetResultOtpAVNOError(-2, mRes.getString(R.string.error_internet_disconnect));
            return;
        }
        final ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        int calloutState = mApplication.getReengAccountBusiness().getCallOutState();
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_BUY_PACKAGE_BY_OTP);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + CommonApi.Url.File.API_AVNO_BUY_PACKAGE_BY_OTP;
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);
                try {
                    JSONObject object = new JSONObject(response);
                    int code = object.optInt("code");
                    String desc = object.optString("desc");
                    if (TextUtils.isEmpty(desc)){
                        if(code == HTTPCode.E200_OK){
                            desc = mRes.getString(R.string.buy_package_success_otp_avno);
                        }else {
                            desc = mRes.getString(R.string.e601_error_but_undefined);
                        }
                    }
                    ResultOtp resultOtp = new ResultOtp(code, desc);
                    if (code == HTTPCode.E200_OK) {
                        listener.onGetResultOtpAVNOSuccess(resultOtp);
                    } else if (code == HTTPCode.E0_OK) {
                        listener.onGetResultOtpAVNOError(HTTPCode.E0_OK, desc);
                    } else {
                        listener.onGetResultOtpAVNOError(-1, desc);
                    }
                } catch (Exception error) {
                    Log.e(TAG, "VolleyError", error);
                    listener.onGetResultOtpAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onGetResultOtpAVNOError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                String accountToken = account.getToken();
                long currentTime = TimeHelper.getCurrentTime();

                String languageCode = mApplication.getReengAccountBusiness().getCurrentLanguage();
                String countryCode = mApplication.getReengAccountBusiness().getRegionCode();

                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(id).
                        append(otp).
                        append(sectionId).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(languageCode).
                        append(countryCode).
                        append(accountToken).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), accountToken);

                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("packageId", id);
                params.put("otp", otp);
                params.put("session_id", sectionId);
                params.put("callout", String.valueOf(calloutState));
                params.put(Constants.HTTP.REST_CLIENT_TYPE, "Android");
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, languageCode);
                params.put(Constants.HTTP.REST_COUNTRY_CODE, countryCode);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_BUY_PACKAGE_BY_OTP, false);
    }

    public void buyVituralNumberByOtpCode(final BaseSlidingFragmentActivity activity, final NumberAVNO avnoNumber,
                                          final String otp, final String sectionId, final RegisterAVNOListener listener) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            return;
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_REGISTER);
        String url = UrlConfigHelper.getInstance(mApplication).getDomainFile() + CommonApi.Url.File.API_AVNO_BUY_VIRTUAL_NUMBER_BY_OTP;
        Log.i(TAG, "registerAVNONumber: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "registerAVNONumber onResponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                            if (code == HTTPCode.E200_OK) {
                                String decryptResponse = HttpHelper.decryptResponse(response, mApplication
                                        .getReengAccountBusiness().getToken());
                                Log.i(TAG, "registerAVNONumber decryptResponse: " + decryptResponse);
                                try {
                                    JSONObject object = new JSONObject(decryptResponse);
                                    int code2 = object.optInt(Constants.HTTP.REST_CODE);
                                    String desc = object.optString(Constants.HTTP.REST_DESC);
                                    if (TextUtils.isEmpty(desc)){
                                        if(code2 == HTTPCode.E200_OK){
                                            desc = mRes.getString(R.string.buy_package_success_otp_avno);
                                        }else {
                                            desc = mRes.getString(R.string.e601_error_but_undefined);
                                        }
                                    }
                                    if (code2 == HTTPCode.E200_OK) {
                                        mApplication.getReengAccountBusiness().updateAVNONumber(avnoNumber.getNumber());
                                        listener.onSuccess(desc);
                                    } else if (code2 == 201) {
                                        gotoWebViewCharge(activity);
                                    } else {
                                        listener.onError(-1, desc);
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, "Exception", e);
                                    listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                                }
                            } else if (code == HTTPCode.E0_OK) {
                                listener.onError(HTTPCode.E0_OK, mRes.getString(R.string.e601_error_but_undefined));
                            } else {
                                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1, mRes.getString(R.string.e601_error_but_undefined));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                int calloutState = mApplication.getReengAccountBusiness().getCallOutState();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(otp).
                        append(sectionId).
                        append(avnoNumber.getNumber()).
                        append(mApplication.getReengAccountBusiness().getOperator()).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getCurrentLanguage()).
                        append(mApplication.getReengAccountBusiness().getRegionCode()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("otp", otp);
                params.put("virtual_number", avnoNumber.getNumber());
                params.put("currOperator", mApplication.getReengAccountBusiness().getOperator());
                params.put("session_id", sectionId);
                params.put("callout", String.valueOf(calloutState));
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.REST_LANGUAGE_CODE, mApplication.getReengAccountBusiness()
                        .getCurrentLanguage());
                params.put(Constants.HTTP.REST_COUNTRY_CODE, mApplication.getReengAccountBusiness().getRegionCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public static class TypeActionAgency {
        public static final String TRANSFER = "transfer";
        public static final String CHANGE_PASS = "change_pass";
        public static final String RESET_PASS = "reset_pass";
    }

    public abstract static class AgencyApiLister {

        public void onGetHistoryDone(ArrayList<AgencyHistory> list) {
        }

        public void onCreateSessionDone(String session) {
        }

        public void onTransferMoneyDone(String msg) {
        }

        public void onChangeResetPassDone(String msg) {
        }

        public abstract void onFail(int code, String msg);
    }

    public enum TypeAvno {
        AVNO(0),
        CALLOUT(1),
        INTERNATIONAL(2);
        private final int id;

        TypeAvno(int id) {
            this.id = id;
        }

        public int getValue() {
            return id;
        }
    }

    public interface RegisterAVNOListener {
        void onSuccess(String msg);

        void onError(int code, String msg);
    }

    public interface GetListNumberAVNOLIstener {
        void onSuccess(ArrayList<NumberAVNO> listNumber);

        void onError(int code, String msg);
    }

    public interface GetInfoAVNOListener {
        void onGetInfoAVNOSuccess(InfoAVNO infoAVNO);

        void onGetInfoAVNOError(int code, String msg);
    }

    public interface GetSavingStatisticListener {
        void onGetSavingStatisticSuccess(SavingStatisticsModel result);

        void onGetSavingStatisticError(int code, String msg);
    }

    public interface GetSavingDetailListener {
        void onGetSavingDetailSuccess(SavingDetailModel result);

        void onGetSavingDetailError(int code, String msg);
    }

    public interface ChargePaymentListener {
        void onSuccess(String msg);

        void onError(int code, String msg);
    }

    public interface RemoveIdentifyCardListener {
        void onRemoveSuccess();

        void onRemoveError(int code, String msg);
    }

    public interface OnClickActionPackage {
        void onClickActionPackage(PackageAVNO packageAVNO);

        void onClickRegisterFreeNumber(PackageAVNO packageAVNO);

        void onClickContentPackage(PackageAVNO packageAVNO);
    }

    public interface ActionPackageAVNOListener {
        void onActionPackageSuccess(String msg);

        void onActionPackageError(int code, String msg);
    }

    public interface BroadcastChangeNumberAVNOListener {
        void onBroadcastSuccess(String msg);

        void onBroadcastError(int error, String msg);
    }

    public interface PackageListener {
        void onClickDeeplink(DeeplinkItem deeplinkItem);

        void onClickFakeMO(FakeMOItem fakeMOItem);
    }

    public interface PackageAVNOListener {
        void onClickAction(PackageAVNO packageAVNO);

        void onClickGift(ItemInfo gift);
    }

    public interface GetResultOtpAVNOListener {
        void onGetResultOtpAVNOSuccess(ResultOtp resultOtp);

        void onGetResultOtpAVNOError(int code, String msg);

    }

}

package com.metfone.selfcare.helper.apple;

public class AppleConstants {
    public static final String CLIENT_ID = "MY_CLIENT_ID";
    public static final String REDIRECT_URI = "MY_REDIRECT_URI";
    public static final String SCOPE = "name%20email";

    public static final String AUTHURL = "https://appleid.apple.com/auth/authorize";
    public static final String TOKENURL = "https://appleid.apple.com/auth/token";
}

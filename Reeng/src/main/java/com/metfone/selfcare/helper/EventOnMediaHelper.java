package com.metfone.selfcare.helper;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ContactDetailActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.activity.OnMediaViewImageActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.module.home_kh.account.AccountDetailActivity;
import com.metfone.selfcare.module.keeng.widget.floatingView.MusicFloatingView;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

//import com.metfone.selfcare.activity.YoutubeActivity;

/**
 * Created by thanhnt72 on 5/17/2016.
 */
public class EventOnMediaHelper {

    private static final String TAG = EventOnMediaHelper.class.getSimpleName();
    private static final String PREFIX_HTTP = "http";
    private static final String ADD_PREFIX = "://";
    private static EventOnMediaHelper mInstance;

    public BaseSlidingFragmentActivity mParentActivity;
    public ApplicationController mApplication;
    public Resources mRes;
    public MusicBusiness mMusicBusiness;
    public FeedBusiness mFeedBusiness;

    public EventOnMediaHelper(BaseSlidingFragmentActivity activity) {
        mParentActivity = activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mRes = mApplication.getResources();
        mMusicBusiness = mApplication.getMusicBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
    }

    public static synchronized EventOnMediaHelper getInstance(BaseSlidingFragmentActivity activity) {
        if (mInstance == null) {
            mInstance = new EventOnMediaHelper(activity);
        }
        return mInstance;
    }

    public static void showImageDetail(ApplicationController mApp, String mName, String phoneNumber,
                                       ArrayList<ImageProfile> listImageProfile,
                                       int position, String itemType, int feedFrom, boolean isOpenProfile) {
        Log.i(TAG, "name: " + mName + " | jid: " + phoneNumber + " | pos: " + position + " | itemType: " + itemType);
        Intent intent = new Intent(mApp, OnMediaViewImageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, listImageProfile);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.POSITION, position);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME, mName);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, phoneNumber);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.ITEM_TYPE, itemType);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.IS_OPEN_PROFILE, isOpenProfile);
        mApp.startActivity(intent);
    }

    public static void showImageDetail(ApplicationController mApp, String mName, String phoneNumber,
                                       ArrayList<ImageProfile> listImageProfile,
                                       int position, String itemType, int feedFrom) {
        Log.i(TAG, "name: " + mName + " | jid: " + phoneNumber + " | pos: " + position + " | itemType: " + itemType);
        Intent intent = new Intent(mApp, OnMediaViewImageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, listImageProfile);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.POSITION, position);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME, mName);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, phoneNumber);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.ITEM_TYPE, itemType);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);
        mApp.startActivity(intent);
    }

    public void processUserClick(UserInfo userInfo) {
        String jidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (userInfo.getMsisdn().equals(jidNumber)) {
//            NavigateActivityHelper.navigateToMyProfile(mParentActivity);
            // TODO: TVV Redirect My Profile
            NavigateActivityHelper.navigateToMyProfileNew(mParentActivity);
        } else {
            if (userInfo.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL) {
                if (userInfo.isUserMocha()) {
                    PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(userInfo
                            .getMsisdn());
                    if (phoneNumber != null && phoneNumber.getId() != null) {
                        navigateToContactDetail(phoneNumber);
                    } else {
//                        navigateToStrangerDetail(userInfo.getMsisdn(), userInfo.getName(), userInfo.getAvatar(),
//                                "");
                        // TODO: TVV redirect Account detail screen
                        navigateToKhAccountDetail(userInfo.getMsisdn(), userInfo.getName(), userInfo.getAvatar(), "");
                    }
                } else {
                    /*ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                    if (!PhoneNumberHelper.getInstant().isViettelNumber(userInfo.getMsisdn()) ||
                            !mApplication.getReengAccountBusiness().isViettel()) {
                        *//*InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, activity,
                                activity.getSupportFragmentManager(), userInfo.getName(), userInfo.getMsisdn(),
                                false);*//*
                        mParentActivity.showToast(R.string.number_not_user_mocha);
                    } else {
                        PhoneNumber mPhone = mApplication.getContactBusiness().getPhoneNumberFromNumber(userInfo
                                .getMsisdn());
                        if (mPhone != null) {
                            navigateToContactDetail(mPhone);
                        } else {
                            mParentActivity.showToast(R.string.number_not_user_mocha);
                        }
                    }*/
                    ThreadMessage thread = mApplication.getStrangerBusiness().
                            createMochaStrangerAndThread(userInfo.getMsisdn(), userInfo.getName(), userInfo.getAvatar(), Constants.CONTACT.STRANGER_MOCHA_ID, true);
                    NavigateActivityHelper.navigateToChatDetail(mParentActivity, thread);
                }
            } else if (userInfo.getUser_type() == UserInfo.USER_ONMEDIA_OFFICAL ||
                    userInfo.getUser_type() == UserInfo.USER_ONMEDIA_VTF ||
                    userInfo.getUser_type() == UserInfo.USER_ONMEDIA_VERIFY) {
                navigateToOfficialProfile(userInfo.getMsisdn(), userInfo.getName(), userInfo.getAvatar(), userInfo
                        .getUser_type());
            }
        }
    }

    private void navigateToOfficialProfile(String officialId, String officialName, String avatar, int userType) {
        Intent strangerDetail = new Intent(mParentActivity, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_OFFICIAL_ONMEDIA);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_ID, officialId);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_NAME, officialName);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_AVATAR, avatar);
        bundle.putInt(Constants.ONMEDIA.OFFICIAL_USER_TYPE, userType);
        strangerDetail.putExtras(bundle);
        mParentActivity.startActivity(strangerDetail, true);
    }

    private void navigateToContactDetail(PhoneNumber phoneNumber) {
        Intent contactDetail = new Intent(mParentActivity, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
        bundle.putString(NumberConstant.ID, phoneNumber.getId());
        contactDetail.putExtras(bundle);
        mParentActivity.startActivity(contactDetail, true);
    }

    private void navigateToStrangerDetail(String friendJid, String friendName,
                                          String friendChangeAvatar, String status) {
        Intent contactDetail = new Intent(mParentActivity, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
        bundle.putString(NumberConstant.NAME, friendName);
        bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, friendChangeAvatar);
        bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
        bundle.putString(NumberConstant.STATUS, status);
        contactDetail.putExtras(bundle);
        mParentActivity.startActivity(contactDetail, true);
    }

    private void navigateToKhAccountDetail(String friendJid, String friendName,
                                          String friendChangeAvatar, String status) {
        Intent contactDetail = new Intent(mParentActivity, AccountDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
        bundle.putString(NumberConstant.NAME, friendName);
        bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, friendChangeAvatar);
        bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
        bundle.putString(NumberConstant.STATUS, status);
        contactDetail.putExtras(bundle);
        mParentActivity.startActivity(contactDetail, true);
    }

    public void handleClickMediaItem(FeedModelOnMedia feed,
                                     int gaCategoryId, int gaActionId,
                                     OnMediaInterfaceListener connectionFeedInterface,
                                     ClickListener.IconListener clickListener) {
        Log.i(TAG, "handleClickMediaItem: " + feed + "\ngaCategoryId: " + gaCategoryId + "\ngaActionId: " + gaActionId);
        FeedContent feedContent = feed.getFeedContent();
        String itemType = feedContent.getItemType();
        if (itemType.equals(FeedContent.ITEM_TYPE_SONG)) {
            handleFeedItemClick(feed, connectionFeedInterface, clickListener);
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_song);
        } else if (itemType.equals(FeedContent.ITEM_TYPE_ALBUM)) {
            handleFeedItemClick(feed, connectionFeedInterface, clickListener);
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_song);
        } else if (OnMediaHelper.isFeedViewMovie(feed)) {
            handleFeedItemClick(feed, connectionFeedInterface, clickListener);
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_movie);
        } else if (OnMediaHelper.isFeedViewVideo(feed)) {
            handleFeedItemClick(feed, connectionFeedInterface, clickListener);
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_video);
        } else if (itemType.equals(FeedContent.ITEM_TYPE_NEWS)) {
            if (!TextUtils.isEmpty(feedContent.getUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
            }
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_news);
        } else if (itemType.equals(FeedContent.ITEM_TYPE_AUDIO)) {
            if (!TextUtils.isEmpty(feedContent.getUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
            }
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_news);
        } else if (itemType.equals(FeedContent.ITEM_TYPE_IMAGE)) {
            if (!TextUtils.isEmpty(feedContent.getUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
            }
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_news);
        } else if (itemType.equals(FeedContent.ITEM_TYPE_TOTAL)) {
            if (!TextUtils.isEmpty(feedContent.getOpenLink())) {
                connectionFeedInterface.navigateToWebView(feed);
            }
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_total);
        } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType)
                && FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(feedContent.getItemSubType())) {
            if (!TextUtils.isEmpty(feedContent.getContentUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
            }
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_news);
        } else if (!TextUtils.isEmpty(feed.getFeedContent().getLink())) {
            connectionFeedInterface.navigateToProcessLink(feed);
            mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_url);
        } else {
            Log.i(TAG, "Loi roi@@@@");
        }
    }

    public void handleClickMediaItem(FeedModelOnMedia feed,
                                     OnMediaInterfaceListener connectionFeedInterface,
                                     ClickListener.IconListener clickListener) {
        Log.i(TAG, "handleClickMediaItem: " + feed);
        FeedContent feedContent = feed.getFeedContent();
        String itemType = feedContent.getItemType();
        if (OnMediaHelper.isFeedViewVideo(feed)
                || OnMediaHelper.isFeedViewMovie(feed)
                || OnMediaHelper.isFeedViewAudio(feed)) {
            handleFeedItemClick(feed, connectionFeedInterface, clickListener);
            return;
        } else if (itemType.equals(FeedContent.ITEM_TYPE_NEWS)) {
            if (!TextUtils.isEmpty(feedContent.getUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
                return;
            }
        } else if (itemType.equals(FeedContent.ITEM_TYPE_AUDIO)) {
            if (!TextUtils.isEmpty(feedContent.getUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
                return;
            }
        } else if (itemType.equals(FeedContent.ITEM_TYPE_IMAGE)) {
            if (!TextUtils.isEmpty(feedContent.getUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
                return;
            }
        } else if (itemType.equals(FeedContent.ITEM_TYPE_TOTAL)) {
            if (!TextUtils.isEmpty(feedContent.getOpenLink())) {
                connectionFeedInterface.navigateToWebView(feed);
                return;
            }
        } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType)
                && FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(feedContent.getItemSubType())) {
            if (!TextUtils.isEmpty(feedContent.getContentUrl())) {
                connectionFeedInterface.navigateToWebView(feed);
                return;
            }
        }
        if (!TextUtils.isEmpty(feedContent.getLink())) {
            connectionFeedInterface.navigateToProcessLink(feed);
        } else {
            Log.i(TAG, "Loi roi@@@@");
        }
    }

    private void handleFeedItemClick(FeedModelOnMedia feed,
                                     OnMediaInterfaceListener connectionFeedInterface,
                                     ClickListener.IconListener clickListener) {
        Log.i(TAG, "handleFeedItemClick: " + feed);
        FeedContent feedContent = feed.getFeedContent();
        if (OnMediaHelper.isFeedViewMovie(feed)) {
            //TODO choi phim nao
            if (mMusicBusiness.isExistListenMusic()) {
                checkMusicTogether(feed, clickListener, connectionFeedInterface);
            } else if (mApplication.getPlayMusicController().isPlaying()) {
                mApplication.getPlayMusicController().closeMusic();
                if (connectionFeedInterface != null) {
                    connectionFeedInterface.navigateToMovieView(feed);
                } else if (clickListener != null) {
                    clickListener.onIconClickListener(null, feed, Constants.MENU.POPUP_EXIT);
                }
            } else {
                if (connectionFeedInterface != null) {
                    connectionFeedInterface.navigateToMovieView(feed);
                } else if (clickListener != null) {
                    clickListener.onIconClickListener(null, feed, Constants.MENU.POPUP_EXIT);
                }
            }
        } else if (OnMediaHelper.isFeedViewVideo(feed)) {
            if (mMusicBusiness.isExistListenMusic()) {
                checkMusicTogether(feed, clickListener, connectionFeedInterface);
            } else if (mApplication.getPlayMusicController().isPlaying()) {
                mApplication.getPlayMusicController().closeMusic();
//                    checkPlayer();
                if (connectionFeedInterface != null) {
                    if (OnMediaHelper.canMoveVideoDetail(feed))
                        connectionFeedInterface.navigateToVideoView(feed);
                    else
                        connectionFeedInterface.navigateToWebView(feed);
                } else if (clickListener != null) {
                    clickListener.onIconClickListener(null, feed, Constants.MENU.POPUP_EXIT);
                }
            } else {
                if (connectionFeedInterface != null) {
                    if (OnMediaHelper.canMoveVideoDetail(feed))
                        connectionFeedInterface.navigateToVideoView(feed);
                    else
                        connectionFeedInterface.navigateToWebView(feed);
                } else if (clickListener != null) {
                    clickListener.onIconClickListener(null, feed, Constants.MENU.POPUP_EXIT);
                }
            }
        } else {
            if (mMusicBusiness.isExistListenMusic()) {
                checkMusicTogether(feed, clickListener, connectionFeedInterface);
            } else {
                if (mApplication.getPlayMusicController().isPlayFromFeed()
                        && mApplication.getFeedBusiness().getFeedPlayList() != null) {
                    FeedContent feedContentPlaying = mApplication.getFeedBusiness().getFeedPlayList().getFeedContent();
                    if (feedContent.getUrl().equals(feedContentPlaying.getUrl())) {
                        if (connectionFeedInterface != null) {
                            connectionFeedInterface.navigateToAlbum(feed);
                        }
                    } else {
                        playSongAlbum(feed, connectionFeedInterface);
                    }
                } else {
                    playSongAlbum(feed, connectionFeedInterface);
                }
            }
        }
    }

    private void checkMusicTogether(FeedModelOnMedia feed,
                                    ClickListener.IconListener clickListener,
                                    OnMediaInterfaceListener connectionFeedInterface) {
        if (mMusicBusiness.isPlayFromKeengMusic())
            MusicFloatingView.stop(mApplication);

//        FeedContent mediaItem = feed.getFeedContent();
        String currentNumberInRoom = mMusicBusiness.getCurrentNumberFriend();
        MessageBusiness mMessageBusiness = mApplication.getMessageBusiness();
        if (!TextUtils.isEmpty(currentNumberInRoom)) {
            String currentName = mMessageBusiness.getFriendName(currentNumberInRoom);
            PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                    "", String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                            currentName),
                    mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                    clickListener, feed, Constants.MENU.POPUP_EXIT);
        } else if (mMusicBusiness.isWaitingStrangerMusic()) {// cung nghe voi nguoi la
            PopupHelper.getInstance().showDialogConfirm(mParentActivity,
                    "", String.format(mRes.getString(R.string.warning_quit_music_and_play_onmedia),
                            mRes.getString(R.string.stranger)),
                    mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                    clickListener, feed, Constants.MENU.POPUP_EXIT);
        } else {
            if (connectionFeedInterface != null)
                handleClickPopupExitListenTogether(feed, connectionFeedInterface);
            else if (clickListener != null)
                clickListener.onIconClickListener(null, feed, Constants.MENU.POPUP_EXIT);
        }
    }

    public void handleClickPopupExitListenTogether(FeedModelOnMedia item,
                                                   OnMediaInterfaceListener connectionFeedInterface) {
        if (mMusicBusiness.isPlayFromKeengMusic())
            MusicFloatingView.stop(mApplication);

        FeedContent feedContent = item.getFeedContent();
        if (OnMediaHelper.isFeedViewMovie(item)) {
            mApplication.getPlayMusicController().pauseMusic();
            mMusicBusiness.closeMusic();
            if (!TextUtils.isEmpty(mMusicBusiness.getCurrentMusicSessionId())) {
                mMusicBusiness.cancelRoomStrangerRoom(mMusicBusiness.getCurrentMusicSessionId(), null, false);
            }
            if (OnMediaHelper.canMoveMovieDetailWithVideoView(item)) {
                if (connectionFeedInterface != null) {
                    connectionFeedInterface.navigateToVideoView(item);
                }
            } else {
                if (connectionFeedInterface != null) {
                    connectionFeedInterface.navigateToMovieView(item);
                }
            }
        } else if (OnMediaHelper.isFeedViewVideo(item)) {
            mApplication.getPlayMusicController().pauseMusic();
            mMusicBusiness.closeMusic();
            if (!TextUtils.isEmpty(mMusicBusiness.getCurrentMusicSessionId())) {
                mMusicBusiness.cancelRoomStrangerRoom(mMusicBusiness.getCurrentMusicSessionId(), null, false);
            }
            if (TextUtils.isEmpty(feedContent.getUrl())) {
                if (connectionFeedInterface != null) {
                    connectionFeedInterface.navigateToVideoView(item);
                }
            } else {
                if (!TextUtils.isEmpty(feedContent.getMediaUrl())
                        || TextHelper.getInstant().isLinkMochaVideo(feedContent.getUrl())
                        || OnMediaHelper.canMoveVideoDetail(item)) {
                    if (connectionFeedInterface != null) {
                        connectionFeedInterface.navigateToVideoView(item);
                    }
                } else {
                    if (connectionFeedInterface != null) {
                        connectionFeedInterface.navigateToWebView(item);
                    }
                }
            }
        } else {

            if (!mApplication.getPlayMusicController().isPlayFromFeed() ||
                    (mFeedBusiness.getFeedPlayList() != null && !feedContent.getUrl().
                            equals(mFeedBusiness.getFeedPlayList().getFeedContent().getUrl()))) {
                mApplication.getPlayMusicController().pauseMusic();
                mMusicBusiness.closeMusic();
                mApplication.getPlayMusicController().getPlayingListDetail(feedContent.getMediaMode());
                mApplication.getPlayMusicController().setIsPlayFromFeed(true);
            }
            if (connectionFeedInterface != null) {
                connectionFeedInterface.navigateToAlbum(item);
            }
            // log feed
            mApplication.getKeengProfileBusiness().logListenTogether(feedContent.getMediaMode());
        }
    }

    private void playSongAlbum(FeedModelOnMedia feed,
                               OnMediaInterfaceListener connectionFeedInterface) {
        if (mMusicBusiness.isPlayFromKeengMusic())
            MusicFloatingView.stop(mApplication);

        mApplication.getPlayMusicController().pauseMusic();
        mMusicBusiness.resetSessionMusic();
        MediaModel mediaModel = feed.getFeedContent().getMediaMode();
        mApplication.getPlayMusicController().getPlayingListDetail(mediaModel);
        // log feed
        mApplication.getKeengProfileBusiness().logListenTogether(mediaModel);
        Log.i(TAG, "mediamode: mediaurl: " + mediaModel.getMedia_url() + " itemid: " + mediaModel.getId() + " url: "
                + mediaModel.getUrl());
        mApplication.getPlayMusicController().setIsPlayFromFeed(true);
        if (connectionFeedInterface != null) {
            connectionFeedInterface.navigateToAlbum(feed);
        }
    }

    //===========================================
    public void getPreviewUrl(String url, final int gaCategoryId, final int gaActionId,
                              final FeedModelOnMedia.ActionFrom actionFrom) {
        getPreviewUrl(url, gaCategoryId, gaActionId, actionFrom, null);
    }

    public void getPreviewUrl(String url, final int gaCategoryId, final int gaActionId,
                              final FeedModelOnMedia.ActionFrom actionFrom, ShareLinkResponse listener) {
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_LINK);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        mParentActivity.startActivity(intent);

        /*url = Utilities.formatUrl(url);
        if (Patterns.WEB_URL.matcher(url).matches()) {
            FeedModelOnMedia feed = mFeedBusiness.getFeedModelFromUrl(url);
            // da co trong danh sach feed cua minh
            if (feed != null) {
                Log.i(TAG, "already has url in news feed");
                handleGetMetaData(feed.getFeedContent(), gaCategoryId, gaActionId, actionFrom, listener);
            } else {
                getMetaDataFromServer(url, gaCategoryId, gaActionId, actionFrom, listener);
            }
        } else {
            String msg = mRes.getString(R.string.you_must_copy_link);
            String titleBtnYes = mRes.getString(R.string.goto_browser);
            PopupHelper.getInstance(mParentActivity).showDialogConfirmOnMedia(mParentActivity, "",
                    msg, titleBtnYes, "", null,
                    new PopupHelper.DialogConfirmListener() {
                        @Override
                        public void onYesListener(Object entry) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"));
                            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mApplication.startActivity(browserIntent);
                        }

                        @Override
                        public void onNoListener(Object entry) {

                        }
                    });
//            mActivity.showToast(R.string.onmedia_toast_no_link);
        }*/
    }

    public void navigateToWriteStatusImage(ArrayList<String> listImage) {
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_IMAGE);
        intent.putStringArrayListExtra(Constants.ONMEDIA.EXTRAS_LIST_IMAGE, listImage);
        mParentActivity.startActivity(intent);
    }

    public void handleShareTotal(View rowView, FeedModelOnMedia feed, final int gaCategoryId, final int gaActionId) {
        if (feed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_TOTAL)) {
            if (feed.getFeedContent().getItemSubType().equals(FeedContent.ITEM_SUB_TYPE_HOROSCOPE)) {
                View viewBitmap = rowView.findViewById(R.id.layout_horoscope_share);
                Bitmap b = getBitmapFromView(viewBitmap);
                mParentActivity.shareImageFacebook(b, mRes.getString(R.string.ga_facebook_label_share_horoscope));
                //mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_share_fb_total);
            } else if (feed.getFeedContent().getItemSubType().equals(FeedContent.ITEM_SUB_TYPE_LUCKY_WHEEL)) {
                LuckyWheelHelper.getInstance(mApplication).navigateToLuckyWheel(mParentActivity);
            } else {
                Log.i(TAG, "type total but not define subtype " + feed.getFeedContent().getItemSubType());
            }
        } else {
            Log.i(TAG, "Item type must be total, not " + feed.getFeedContent().getItemType());
        }
    }

    public void handleDeeplink(FeedModelOnMedia feed, String link) {
        if (mApplication != null)
            mApplication.getLogApi().logBanner(link, LogApi.TypeService.ON_MEDIA, null);
        DeepLinkHelper.getInstance().openSchemaLink(mParentActivity, link);
    }

    private Bitmap getBitmapFromView(View holder) {
        holder.setBackgroundColor(ContextCompat.getColor(mApplication, R.color.white));
        Log.i(TAG, "width: " + holder.getWidth() + " height: " + holder.getHeight());
        Bitmap scaledBitmap = Bitmap.createBitmap(holder.getWidth(), holder.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        holder.draw(canvas);
        return scaledBitmap;
    }

    public void showPopupContextMenuShare(FeedModelOnMedia feed, ClickListener.IconListener listener) {
        ShareContentBusiness business = new ShareContentBusiness(mParentActivity, feed);
        business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_CONTENT);
        business.showPopupShareContent();
    }

    public void handleClickImage(FeedModelOnMedia feedModelOnMedia, int positionImage, int feedFrom) {
        Log.i(TAG, "Click position image: " + positionImage);
        UserInfo userInfo = feedModelOnMedia.getFeedContent().getUserInfo();
        if (userInfo == null) return;
        int userType = userInfo.getUser_type();
        String name;
        String msisdn = userInfo.getMsisdn();
        if (userType == UserInfo.USER_ONMEDIA_NORMAL) {
            String jidNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (msisdn.equals(jidNumber)) {
                name = mApplication.getReengAccountBusiness().getUserName();
            } else {
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(msisdn);
                if (phoneNumber != null) {
                    name = phoneNumber.getName();
                } else {
                    name = userInfo.getName();
                }
            }
        } else {
            name = userInfo.getName();
        }
        ArrayList<ImageProfile> mImageProfiles = new ArrayList<>();
        ArrayList<FeedContent.ImageContent> imageContents = feedModelOnMedia.getFeedContent().getListImage();
        for (FeedContent.ImageContent imageContent : imageContents) {
            ImageProfile img = new ImageProfile();
            if (feedModelOnMedia.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_COVER)) {
                img.setTypeImage(ImageProfileConstant.IMAGE_COVER);
            } else {
                img.setTypeImage(ImageProfileConstant.IMAGE_NORMAL);
            }
            img.setIdServerString(imageContent.getIdImage());
            img.setImageThumb(imageContent.getThumbUrl(mApplication));
            img.setImageUrl(imageContent.getImgUrl(mApplication));
            mImageProfiles.add(img);
        }
        if (feedFrom == Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_FRIEND_PROFILE) {
            mFeedBusiness.setFeedProfileProcess(feedModelOnMedia);
        }
        showImageDetail(name, msisdn, mImageProfiles, positionImage,
                feedModelOnMedia.getFeedContent().getItemType(), feedFrom);
    }

    public void showImageDetail(String mName, String phoneNumber,
                                ArrayList<ImageProfile> listImageProfile,
                                int position, String itemType, int feedFrom) {
        Log.i(TAG, "name: " + mName + " | jid: " + phoneNumber + " | pos: " + position + " | itemType: " + itemType);
        Intent intent = new Intent(mParentActivity, OnMediaViewImageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, listImageProfile);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.POSITION, position);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME, mName);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, phoneNumber);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.ITEM_TYPE, itemType);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);
        mParentActivity.startActivity(intent);
    }

    public void handleClickMoreOption(FeedModelOnMedia feed, ClickListener.IconListener listener) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        String numberActioner = feed.getUserInfo().getMsisdn();
        if (numberActioner.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            /*if (feed.getActionType() == FeedModelOnMedia.ActionLogApp.POST
                    && !mFeedBusiness.isFeedFromProfile(feed)
                    && !FeedContent.ITEM_TYPE_SOCIAL.equals(feed.getFeedContent().getItemType())) {
                ItemContextMenu editItem = new ItemContextMenu(mApplication, mRes.getString(R.string.edit), -1,
                        feed, Constants.MENU.EDIT);
                listMenu.add(editItem);
            }*/
            ItemContextMenu deleteItem = new ItemContextMenu(mRes.getString(R.string.delete), -1,
                    feed, Constants.MENU.DELETE);
            listMenu.add(deleteItem);
        } else {
            if (feed.getUserInfo().getUser_type() == UserInfo.USER_ONMEDIA_OFFICAL ||
                    feed.getUserInfo().getUser_type() == UserInfo.USER_ONMEDIA_VTF ||
                    feed.getUserInfo().getUser_type() == UserInfo.USER_ONMEDIA_VERIFY) {
                /*ItemContextMenu unfollowItem = new ItemContextMenu(mParentActivity, mRes.getString(R.string
                        .onmedia_setting_unfollow_text1), -1,
                        feed, Constants.MENU.UNFOLLOW);
                listMenu.add(unfollowItem);*/
            } else {
                ItemContextMenu reportItem = new ItemContextMenu(mRes.getString(R.string
                        .onmedia_setting_report), -1,
                        feed, Constants.MENU.REPORT);
                listMenu.add(reportItem);
            }

        }

        String itemType = feed.getFeedContent().getItemType();
        String itemSubType = feed.getFeedContent().getItemSubType();
        if (OnMediaHelper.isFeedViewAudio(feed)
                || OnMediaHelper.isFeedViewMovie(feed)
                || FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                || FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                || FeedContent.ITEM_TYPE_AUDIO.equals(itemType)
                || FeedContent.ITEM_TYPE_IMAGE.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) && FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType))
        ) {
            ItemContextMenu copyItem = new ItemContextMenu(mRes.getString(R.string
                    .web_pop_copylink), -1,
                    feed, Constants.MENU.COPY);
            listMenu.add(copyItem);
        }

        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mParentActivity,
                    null, listMenu, listener);
        }
    }

    public void navigateToPostOnMedia(BaseSlidingFragmentActivity activity, FeedContent feedContent,
                                      String status, String rowId, boolean isEdit,
                                      FeedModelOnMedia.ActionFrom actionFrom) {
        String url;
        if (!TextUtils.isEmpty(feedContent.getContentUrl())) {
            url = feedContent.getContentUrl();
        } else if (!TextUtils.isEmpty(feedContent.getUrl())) {
            url = feedContent.getUrl();
        } else {
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        Intent intent = new Intent(activity, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_EDIT_FEED, isEdit);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_CONTENT_DATA, feedContent);
        activity.startActivity(intent);
    }

    public void showPopupContextMenuLongClick(FeedModelOnMedia feed, ClickListener.IconListener listener) {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(mRes.getString(R.string.onmedia_copy_text), -1,
                feed, Constants.MENU.MENU_COPY_TEXT);
        listMenu.add(copyItem);
        if (!listMenu.isEmpty()) {
            PopupHelper.getInstance().showContextMenu(mParentActivity,
                    null, listMenu, listener);
        }
    }

    public interface ShareLinkResponse {
        void onSuccess();
    }
}

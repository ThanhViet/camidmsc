package com.metfone.selfcare.helper.httprequest;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 08/11/2015.
 */
public class ProfileRequestHelper {

    private static final String TAG = ProfileRequestHelper.class.getSimpleName();
    private static ProfileRequestHelper instance;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private ImageProfileBusiness mImageProfileBusiness;

    private ProfileRequestHelper(ApplicationController application) {
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        mImageProfileBusiness = mApplication.getImageProfileBusiness();
    }

    public static synchronized ProfileRequestHelper getInstance(ApplicationController application) {
        if (instance == null) {
            instance = new ProfileRequestHelper(application);
        }
        return instance;
    }

    public void getUserInfo(ReengAccount account, final onResponseUserInfoListener listener) {
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        long currentTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        Log.d(TAG, "[getUserInfo] JidNumber: " + mAccountBusiness.getJidNumber());
        Log.d(TAG, "[getUserInfo] Token: " + mAccountBusiness.getToken());
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), account
                .getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.GET_USER_INFO),
                numberEncode,
                String.valueOf(currentTime),
                dataEncrypt);
        Log.d(TAG, "[getUserInfo] url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                        int errorCode = -1;
                        Log.i(TAG, "[getUserInfo] decryptResponse" + decryptResponse);
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                ReengAccount reengAccount = mAccountBusiness.getCurrentAccount();
                                if (reengAccount != null) {
                                    reengAccount.setJsonObject(responseObject);
                                    // cap nhat du lieu(can sua lai)
                                    mAccountBusiness.updateReengAccount(reengAccount);
                                    // cap nhat albums vao db
                                    mImageProfileBusiness.deleteAllImageProfile();
                                    String coverUrl = reengAccount.getCoverUrl();
                                    if (!TextUtils.isEmpty(coverUrl)) {
                                        ImageProfile cover = new ImageProfile();
                                        cover.setUpload(true);
                                        cover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
                                        cover.setTime((new Date()).getTime());
                                        cover.setImageUrl(coverUrl);
                                        if (responseObject.has("coverSId")) {
                                            cover.setIdServerString(responseObject.getString("coverSId"));
                                        }
                                        if (responseObject.has("isCover")) {
                                            cover.setHasCover(responseObject.getInt("isCover") != 0);
                                        }
                                        mImageProfileBusiness.insertOrUpdateCover(cover);
                                    }
                                    ArrayList<ImageProfile> listImage = reengAccount.getAlbums();
                                    if (listImage != null && !listImage.isEmpty()) {
                                        mImageProfileBusiness.insertListImageProfile(listImage);
                                    }
                                }
                                if (listener != null)
                                    listener.onResponse(reengAccount);
                                boolean autoSms = responseObject.optInt("autoConvertSms", 1) == 1;
                                SettingBusiness.getInstance(mApplication).setPrefEnableAutoSmsOut(autoSms);
                                // receive sms out
                                boolean receiveSms = responseObject.optInt("allowReceiveSms", 1) == 1;
                                SettingBusiness.getInstance(mApplication).setPrefEnableReceiveSmsOut(receiveSms);

                                boolean reminder = responseObject.optInt("birthday_reminder", 1) == 1;
                                SettingBusiness.getInstance(mApplication).setPrefBirthdayReminder(reminder);
                            } else {
                                if (listener != null)
                                    listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "[getUserInfo] Exception", e);
                            if (listener != null)
                                listener.onError(-1);
                        }
                    }
                }
                , new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                if (listener != null)
                    listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * set user info
     *
     * @param account
     * @param listener
     */
    public void setUserInfo(final ReengAccount account, final onResponseUserInfoListener listener) {
        String url = UrlConfigHelper.getInstance(mApplication).
                getUrlConfigOfFile(Config.UrlEnum.SET_USER_INFO);
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response: " + response);
                        String decryptResponse = HttpHelper.decryptResponse(response, account.getToken());
                        Log.d(TAG, "decryptResponse: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                account.setName(responseObject.optString(Constants.HTTP.USER_INFOR.NAME, account
                                        .getName()));
                                account.setBirthdayString(responseObject.optString(Constants.HTTP.USER_INFOR
                                        .BIRTHDAY_STRING, account.getBirthdayString()));
                                account.setGender(responseObject.optInt(Constants.HTTP.USER_INFOR.GENDER, account
                                        .getGender()));
                                mAccountBusiness.updateReengAccount(account);
                                listener.onResponse(account);
                            } else {
                                mAccountBusiness.initAccountFromDatabase(); // lay lai thong tin trong db
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            mAccountBusiness.initAccountFromDatabase(); // lay lai thong tin trong db
                            listener.onError(errorCode);
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mAccountBusiness.initAccountFromDatabase(); // lay lai thong tin trong db
                listener.onError(-1);
                Log.e(TAG, "VolleyError: ", error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String myEmail = mAccountBusiness.getEmail(mApplication);
                String myJid = account.getJidNumber();
                long currentTime = System.currentTimeMillis();
                String userInfo = account.getJsonObject(myEmail).toString();
                Log.d(TAG, "userInfo: " + userInfo);
                StringBuilder sb = new StringBuilder().
                        append(myJid).
                        append(userInfo).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mApplication.getReengAccountBusiness().getUsingDesktop()).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, myJid);
                params.put(Constants.HTTP.USER_INFOR.INFOR, userInfo);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                params.put("revision", Config.REVISION);
                params.put(Constants.HTTP.DESKTOP, String.valueOf(mApplication.getReengAccountBusiness().getUsingDesktop()));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    /**
     * set status
     *
     * @param account
     * @param status
     * @param listener
     */
    public void setStatus(final ReengAccount account, final String status, final onResponseChangeStatusListener
            listener) {
        String url = UrlConfigHelper.getInstance(mApplication).
                getUrlConfigOfFile(Config.UrlEnum.CHANGE_STATUS);
        StringRequest request = new StringRequest(com.android.volley.Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "onResponse: " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE)) {
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            }
                            if (errorCode == HTTPCode.E200_OK) {
                                if (responseObject.has(Constants.HTTP.USER_INFOR.STATUS)) {// api moi co valid stt
                                    account.setStatus(responseObject.getString(Constants.HTTP.USER_INFOR.STATUS));
                                } else {
                                    account.setStatus(status);
                                }
                                mAccountBusiness.updateReengAccount(account);
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // lay lai thong tin trong db
                listener.onError(-1);
                Log.e(TAG, "VolleyError: ", error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String numberJid = account.getJidNumber();
                long currentTime = System.currentTimeMillis();
                String encrypt = HttpHelper.encryptData(mApplication, numberJid, account.getToken(), currentTime);
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, numberJid);
                params.put(Constants.HTTP.REQ_TIME, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_ENCRYPT, encrypt);
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.DESKTOP, String.valueOf(mApplication.getReengAccountBusiness().getUsingDesktop()));
                if (status == null) {
                    params.put(Constants.HTTP.USER_INFOR.STATUS, "");
                } else {
                    params.put(Constants.HTTP.USER_INFOR.STATUS, status);
                }
                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);

    }

    /*public void removeImageProfile(ReengAccount account, ImageProfile imageProfile,
                                   final onResponseRemoveImageProfileListener listener) {
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        long currentTime = System.currentTimeMillis();
        String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataObject(mApplication,
                account.getJidNumber(), account.getToken(), currentTime, imageProfile.getIdServerString()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.REMOVE_IMAGE_PROFILE),
                numberEncode, encrypt, currentTime, imageProfile.getIdServerString());
        Log.d(TAG, "removeImageProfile url:" + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int errorCode = -1;
                        Log.i(TAG, "response" + response);
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE))
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "JSONException:", e);
                            listener.onError(-1);
                        }
                    }
                }
                , new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();

                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(state).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(Constants.HTTP.BIRTHDAY_REMINDER, String.valueOf(state));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }*/

    public void setPermission(ReengAccount account, int permission,
                              final onResponseSettingPermissionListener listener) {
        if(account == null) return;
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        long currentTime = System.currentTimeMillis();
        String encrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataObject(mApplication,
                account.getJidNumber(), account.getToken(), currentTime, String.valueOf(permission)));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.SET_PERMISSION),
                numberEncode, encrypt, currentTime, permission);
        Log.d(TAG, "setPermission url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int errorCode = -1;
                        Log.i(TAG, "setPermission response" + response);
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE))
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                }
                , new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void setRemindFriendBirthday(ReengAccount account, final int state,
                                        final onResponseSettingPermissionListener listener) {

        String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SET_REMINDER);
        Log.d(TAG, "[setRemindFriendBirthday] url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "[setRemindFriendBirthday] onResponse: setRemindFriendBirthday : " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_ERROR_CODE);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "[setRemindFriendBirthday] Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "[setRemindFriendBirthday] VolleyError", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();

                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(state).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(Constants.HTTP.BIRTHDAY_REMINDER, String.valueOf(state));
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);

                Log.d(TAG, "[setRemindFriendBirthday] JidNumber: " + mAccountBusiness.getJidNumber());
                Log.d(TAG, "[setRemindFriendBirthday] Token: " + mAccountBusiness.getToken());

                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    public void setPermissionHideStrangerHistory(ReengAccount account, int hide,
                                                 final onResponseSettingPermissionListener listener) {
        String numberEncode = HttpHelper.EncoderUrl(account.getJidNumber());
        long currentTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder().
                append(account.getJidNumber()).
                append(hide).
                append(account.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.EncoderUrl(HttpHelper.encryptDataV2(mApplication, sb.toString(), account
                .getToken()));
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.SET_HIDE_STRANGER_HISTORY),
                numberEncode, hide, currentTime, dataEncrypt);
        Log.d(TAG, "setPermission url:" + url);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int errorCode = -1;
                        Log.i(TAG, "setPermission response" + response);
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            if (responseObject.has(Constants.HTTP.REST_CODE))
                                errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onResponse();
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                }
                , new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        });
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }


    public void setAutoSmsOut(boolean isEnableAutoSmsOut, final onResponseSetAutoSmsOutListener listener) {
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        int type = isEnableAutoSmsOut ? 1 : 0;
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber())
                .append(type)
                .append(mAccountBusiness.getToken())
                .append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());

        Log.d(TAG, "[setAutoSmsOut] token: " + mAccountBusiness.getToken());
        Log.d(TAG, "[setAutoSmsOut] JidNumber: " + mAccountBusiness.getJidNumber());

        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.AUTO_SMS_OUT),
                HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()), type, currentTime,
                HttpHelper.EncoderUrl(dataEncrypt));
        Log.d(TAG, "[setAutoSmsOut] url: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "[setAutoSmsOut] processSetAutoSmsOut onResponse : " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onRequestSuccess();
                            } else {
                                listener.onError(-1);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "[setAutoSmsOut] Exception", e);
                            listener.onError(-1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "[setAutoSmsOut] VolleyError", volleyError);
                        listener.onError(-1);
                    }
                }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG, false);
    }

    public void requestSettingReceiveSmsOut(boolean isEnableReceiverSmsOut, final onResponseReceiveSmsOutListener
            listener) {
        if (!mAccountBusiness.isValidAccount()) {
            listener.onError(-1);
            return;
        }
        int receiveSms = isEnableReceiverSmsOut ? 1 : 0;
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder();
        sb.append(mAccountBusiness.getJidNumber())
                .append(receiveSms)
                .append(mAccountBusiness.getToken())
                .append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        String url = String.format(UrlConfigHelper.getInstance(mApplication).
                        getUrlConfigOfFile(Config.UrlEnum.SETTING_RECEIVE_SMSOUT),
                HttpHelper.EncoderUrl(mAccountBusiness.getJidNumber()), receiveSms, currentTime,
                HttpHelper.EncoderUrl(dataEncrypt));
        Log.d(TAG, "url: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "settingReceiverSmsOut onResponse : " + response);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(response);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE, -1);
                            if (errorCode == HTTPCode.E200_OK) {
                                listener.onRequestSuccess();
                            } else {
                                listener.onError(-1);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(-1);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "VolleyError", volleyError);
                        listener.onError(-1);
                    }
                }
        );
        VolleyHelper.getInstance(mApplication).addRequestToQueue(stringRequest, TAG, false);
    }


    public interface onResponseSettingPermissionListener {
        void onResponse();

        void onError(int errorCode);
    }

    public interface onResponseUserInfoListener {
        void onResponse(ReengAccount account);

        void onError(int errorCode);
    }

    public interface onResponseChangeStatusListener {
        void onResponse();

        void onError(int errorCode);
    }

    public interface onResponseRemoveImageProfileListener {
        void onResponse();

        void onError(int errorCode);
    }

    public interface onResponseSetAutoSmsOutListener {
        void onRequestSuccess();

        void onError(int errorCode);
    }

    public interface onResponseReceiveSmsOutListener {
        void onRequestSuccess();

        void onError(int errorCode);
    }
}
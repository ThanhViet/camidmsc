package com.metfone.selfcare.helper;

/**
 * Created by ThaoDV on 6/6/14.
 */

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.OnMediaActivityNew;
import com.metfone.selfcare.activity.QuickMissCallActivity;
import com.metfone.selfcare.activity.QuickReplyActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.httprequest.ContactRequestHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.ui.PopupContextMenuFragment;
import com.metfone.selfcare.ui.PopupFragment;
import com.metfone.selfcare.ui.PopupRadioButtonFragment;
import com.metfone.selfcare.ui.PopupSingleChooseFragment;
import com.metfone.selfcare.ui.PopupWindowOverFlow;
import com.metfone.selfcare.ui.PopupWindowPlayGif;
import com.metfone.selfcare.ui.UploadSongPopupFragment;
import com.metfone.selfcare.ui.chatviews.BottomChatOption;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;


public class PopupHelper {
    private static final String TAG = PopupHelper.class.getSimpleName();
    private static PopupHelper mInstance = null;
    private Context mContext;
    private PopupWindowOverFlow mPopupOverFlow;
    private PopupWindowPlayGif mPopupWindowPlayGif;
    private BaseSlidingFragmentActivity mCurrentActivity;
    private BottomChatOption mBottomChatOption;

    public static PopupHelper getInstance() {
        if (mInstance == null) {
            synchronized (PopupHelper.class) {
                if (mInstance == null) {
                    mInstance = new PopupHelper();
                }
            }
        }
        return mInstance;
    }

    private PopupHelper() {
        //singleton constructor
        Log.i(TAG, "Creator!!");
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public UploadSongPopupFragment showUploadPopup(FragmentManager fragmentManager, String content, UploadSongPopupFragment.OnClick
            clickListener, LocalSongInfo song) {
        try {
            // show popup
            UploadSongPopupFragment uploadPopupFragment = UploadSongPopupFragment.newInstance(content, clickListener, song);
            uploadPopupFragment.show(fragmentManager, TAG);
            return uploadPopupFragment;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        }

    }

    /**
     * @param activity
     * @param title
     * @param msg
     * @param btnCancel
     * @param btnOk
     * @param clickHandler
     * @param entry
     * @param clickId
     * @author toanvk2
     */
    public void showDialogConfirm(BaseSlidingFragmentActivity activity, String title,
                                  String msg, String btnOk, String btnCancel,
                                  ClickListener.IconListener clickHandler, Object entry, int clickId) {
        try {
            PopupFragment popupFragment = new PopupFragment(activity, title, msg, btnOk,
                    btnCancel, clickHandler, entry, clickId,0, true);
            popupFragment.show();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void showDialogConfirm(BaseSlidingFragmentActivity activity, String title,
                                  String msg, String btnOk, String btnCancel,
                                  ClickListener.IconListener clickHandler, Object entry,
                                  int clickId, boolean cancelable) {
        try {
            PopupFragment popupFragment = new PopupFragment(activity, title, msg, btnOk,
                    btnCancel, clickHandler, entry, clickId, cancelable);
            popupFragment.show();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void showDialogConfirm(BaseSlidingFragmentActivity activity, String title,
                                  String msg, String btnOk, String btnCancel,
                                  ClickListener.IconListener clickHandler, Object entry,
                                  int clickId, boolean cancelable, boolean visibleDoNotShowAgain, ClickListener.CheckboxListener listener) {
        try {
            PopupFragment popupFragment = new PopupFragment(activity, title, msg, btnOk,
                    btnCancel, clickHandler, entry, clickId, cancelable, listener, visibleDoNotShowAgain);
            popupFragment.show();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void showDialogForceUpdate(BaseSlidingFragmentActivity activity, String msg, String link,
                                      ClickListener.IconListener clickHandler, int clickId) {
        try {
            String title = activity.getString(R.string.title_update_app);
            String btnOk = activity.getString(R.string.update);
            String btnCancel = activity.getString(R.string.cancel);
            // bat buoc cap nhat ban moi
            PopupFragment popupFragment = new PopupFragment(activity, title, msg, btnOk,
                    btnCancel, clickHandler, link, clickId, true);
            popupFragment.show();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void showChooseLanguage(FragmentManager fragmentManager, ArrayList<Region> regions,
                                   PopupSingleChooseFragment.SingleChooseListener callBack) {
        String title = mContext.getString(R.string.title_language);
        PopupSingleChooseFragment.newInstance(mContext, title, regions, callBack).show(fragmentManager, TAG);
    }

    public void navigateToEditPostOnMedia(BaseSlidingFragmentActivity activity, FeedModelOnMedia feed) {
        /*mPopupPostOnMedia = new PopupPostOnMediaFragment(activity, feedContent,
                status, rowId, isEdit, actionFrom, listener);
        mPopupPostOnMedia.show();*/
        String url;
        FeedContent feedContent = feed.getFeedContent();
        if (!TextUtils.isEmpty(feedContent.getContentUrl())) {
            url = feedContent.getContentUrl();
        } else if (!TextUtils.isEmpty(feedContent.getUrl())) {
            url = feedContent.getUrl();
        } else {
            activity.showToast(R.string.e601_error_but_undefined);
            return;
        }
        Intent intent = new Intent(activity, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_EDIT_FEED, true);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feed);
        activity.startActivity(intent);
    }

    //popup context menu
    public void showContextMenu(BaseSlidingFragmentActivity activity, String title,
                                ArrayList<ItemContextMenu> listItems, ClickListener.IconListener callBack) {
        PopupContextMenuFragment mPopupContextMenu = new PopupContextMenuFragment(activity, title, listItems, callBack);
        mPopupContextMenu.show();
    }


    public void showContextMenuSmartText(final BaseSlidingFragmentActivity activity, final FragmentManager fragmentManager,
                                         final String title, final String content, int type, final ClickListener.IconListener callBack) {
        final ApplicationController application = (ApplicationController) mContext.getApplicationContext();
        if (type == Constants.SMART_TEXT.TYPE_NUMBER) {
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(application.getPhoneUtil(), content, application.getReengAccountBusiness().getRegionCode());
            String myNumber = application.getReengAccountBusiness().getJidNumber();
            PhoneNumber phone = application.getContactBusiness().getPhoneNumberFromNumber(content);
            if (content.equals(myNumber) || phone != null ||
                    !PhoneNumberHelper.getInstant().isValidPhoneNumber(application.getPhoneUtil(), phoneNumberProtocol)) {
                ArrayList<ItemContextMenu> listItems = new ArrayList<>();
                if (content.equals(myNumber) || !PhoneNumberHelper.getInstant().
                        isValidPhoneNumber(application.getPhoneUtil(), phoneNumberProtocol)) {
                    ItemContextMenu copyItem = new ItemContextMenu(
                            activity.getString(R.string.copy), -1, content, Constants.MENU.COPY);
                    listItems.add(copyItem);
                } else {
                    listItems = getListItemMenuSmartPhone(application, content, phone, false, content);
                }
                PopupContextMenuFragment mPopupContextMenu = new PopupContextMenuFragment(activity, title, listItems, callBack);
                mPopupContextMenu.show();
            } else {// goi api
                final String numberJid = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        application.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
                activity.showLoadingDialog(null, R.string.waiting);
                ArrayList<String> listNumbers = new ArrayList<>();
                listNumbers.add(numberJid);
                ContactRequestHelper.getInstance(application).getInfoContactsFromNumbers(listNumbers, new ContactRequestHelper.onResponseInfoContactsListener() {
                    @Override
                    public void onResponse(ArrayList<PhoneNumber> responses) {
                        activity.hideLoadingDialog();
                        boolean isMocha = false;
                        if (responses != null && !responses.isEmpty()) {
                            isMocha = responses.get(0).isReeng();
                        }
                        ArrayList<ItemContextMenu> listItems = getListItemMenuSmartPhone(application, content, null, isMocha, numberJid);
                        PopupContextMenuFragment mPopupContextMenu = new PopupContextMenuFragment(activity, title, listItems, callBack);
                        mPopupContextMenu.show();
                    }

                    @Override
                    public void onError(int errorCode) {
                        try {
                            activity.hideLoadingDialog();
                            Log.d(TAG, "Error get info: " + errorCode);
                            ArrayList<ItemContextMenu> listItems = getListItemMenuSmartPhone(application, content, null, false, numberJid);
                            PopupContextMenuFragment mPopupContextMenu = new PopupContextMenuFragment(activity, title, listItems, callBack);
                            mPopupContextMenu.show();
                        } catch (IllegalStateException ignored) {
                            // There's no way to avoid getting this if saveInstanceState has already been called.
                        }
                    }
                });
            }
        } else {
            ArrayList<ItemContextMenu> listItems = getListItemMenuSmartText(application, content, type);
            PopupContextMenuFragment mPopupContextMenu = new PopupContextMenuFragment(activity, title, listItems, callBack);
            mPopupContextMenu.show();
        }
    }

    private ArrayList<ItemContextMenu> getListItemMenuSmartText(ApplicationController application, String content, int type) {
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(
                application.getString(R.string.copy), -1, content, Constants.MENU.COPY);
        listItem.add(copyItem);
        if (type == Constants.SMART_TEXT.TYPE_EMAIL) {
            ItemContextMenu sendEmail = new ItemContextMenu(application.getString(R.string.send_mail), -1,
                    content, Constants.MENU.SEND_EMAIL);
            listItem.add(sendEmail);
        } else if (type == Constants.SMART_TEXT.TYPE_URL) {
            ItemContextMenu viewURL = new ItemContextMenu(application.getString(R.string.view_url), -1, content,
                    Constants.MENU.GO_URL);
            listItem.add(viewURL);
            if (application.getConfigBusiness().isEnableOnMedia()) {
                ItemContextMenu shareOnMedia = new ItemContextMenu(application.getString(R.string
                        .onmedia_share_from_url), -1, content, Constants.MENU.SHARE_ONMEDIA);
                listItem.add(shareOnMedia);
            }
            if (TextHelper.getInstant().isYoutubeUrl(content)) {
                ItemContextMenu shareSieuHai = new ItemContextMenu(application.getString(R.string.share_sieu_hai),
                        -1, content, Constants.MENU.SHARE_SIEU_HAI);
                listItem.add(shareSieuHai);
            }
        }
        return listItem;
    }

    //get list item menu smart text
    private ArrayList<ItemContextMenu> getListItemMenuSmartPhone(ApplicationController application, String content,
                                                                 PhoneNumber phone, boolean isUserMocha, String numberJid) {
        boolean isEnableCallOut = application.getReengAccountBusiness().isEnableCallOut();
        ArrayList<ItemContextMenu> listItem = new ArrayList<>();
        ItemContextMenu copyItem = new ItemContextMenu(
                application.getString(R.string.copy), -1, content, Constants.MENU.COPY);
        ItemContextMenu addContact = new ItemContextMenu(application.getString(R.string.add_contact),
                -1, content, Constants.MENU.ADD_CONTACT);
        ItemContextMenu reengChat = new ItemContextMenu(application.getString(R.string.free_chat), -1, numberJid, Constants.MENU.REENG_CHAT);
        ItemContextMenu callItem = new ItemContextMenu(application.getString(R.string.call), -1, numberJid, Constants.MENU.MENU_CALL);
        ItemContextMenu callFree = new ItemContextMenu(application.getString(R.string.call_option_free), -1, numberJid, Constants.MENU.MENU_CALL_FREE);
        ItemContextMenu callOut = new ItemContextMenu(application.getString(R.string.call_option_out), -1, numberJid, Constants.MENU.MENU_CALL_OUT);
        if (isUserMocha || (phone != null && phone.isReeng())) {
            listItem.add(reengChat);// dùng mocha hoặc lưu danh bạ dùng mocha
            if (application.getReengAccountBusiness().isCallEnable()) {
                if (isEnableCallOut) {
                    listItem.add(0, callOut);
                    listItem.add(0, callFree);
                } else {
                    listItem.add(0, callFree);
                }
            } else if (isEnableCallOut) {
                listItem.add(0, callOut);
            } else {
                listItem.add(0, callItem);
            }
        } else {

            if (application.getReengAccountBusiness().isViettel()) {
                if (phone != null && phone.isViettel()) {
                    listItem.add(reengChat);
                } else {
                    NonContact nonContact = application.getContactBusiness().getExistNonContact(numberJid);
                    if (nonContact != null && nonContact.isViettel())
                        listItem.add(reengChat);
                }
            }
            if (isEnableCallOut) {
                listItem.add(0, callOut);
            } else {
                listItem.add(0, callItem);
            }
        }
        listItem.add(copyItem);
        if (phone != null) {
            ItemContextMenu viewDetail = new ItemContextMenu(application.getString(R.string
                    .view_contact_detail), -1, phone, Constants.MENU.VIEW_CONTATCT_DETAIL);
            listItem.add(viewDetail);
        } else {
            listItem.add(addContact);
        }
        return listItem;
    }

    public void showOrHideBottomChatOption(BaseSlidingFragmentActivity activity, View anchor, int threadType,
                                           int threadId, boolean stranger, int optionHeight, boolean isViettel,
                                           PositiveListener<ItemContextMenu> listener, DismissListener dismissListener) {
        if (mBottomChatOption == null) {
            mBottomChatOption = BottomChatOption.newInstance(activity, anchor, listener, dismissListener);
        }
        if (mBottomChatOption.isShowing()) {
            mBottomChatOption.dismiss();
        } else {
            mBottomChatOption.setThreadType(threadType);
            mBottomChatOption.setThreadId(threadId);
            mBottomChatOption.setStranger(stranger);
            mBottomChatOption.setOptionHeight(optionHeight);
            mBottomChatOption.showPopup(isViettel);
            if(anchor.getId() == R.id.bottom_chat_option_anchor) {
                anchor.getLayoutParams().height = Math.round(getCurrentActivity().getResources().getDimension(R.dimen.dimen_98dp));
                anchor.requestLayout();
            }
        }
    }

    public boolean isShowBottomChatOption() {
        return mBottomChatOption != null && mBottomChatOption.isShowing();
    }

    public void hideBottomChatOption() {
        if (mBottomChatOption != null && mBottomChatOption.isShowing()) {
            mBottomChatOption.dismiss();
        }
    }

    public void destroyBottomChatOption() {
        if (mBottomChatOption != null && mBottomChatOption.isShowing()) {
            mBottomChatOption.dismiss();
            mBottomChatOption = null;
        } else if (mBottomChatOption != null) {
            mBottomChatOption = null;
        }
    }

    public void showOrHideOverFlowMenu(View buttonView, ClickListener.IconListener callBack,
                                       ArrayList<ItemContextMenu> listItems) {
        if (mPopupOverFlow == null) {
            mPopupOverFlow = PopupWindowOverFlow.newInstance(mContext, buttonView, callBack, listItems);
        }
        if (mPopupOverFlow.isShowing()) {
            mPopupOverFlow.dismiss();
        } else {
            mPopupOverFlow.setCallBack(callBack);
            mPopupOverFlow.setListItem(listItems);
            mPopupOverFlow.showPopup();
        }
    }

    public void destroyOverFlowMenu() {
        if (mPopupOverFlow != null && mPopupOverFlow.isShowing()) {
            mPopupOverFlow.dismiss();
            mPopupOverFlow = null;
        } else if (mPopupOverFlow != null) {
            mPopupOverFlow = null;
        }
    }

    /**
     * play full gif
     *
     * @param parentView
     */
    public void showOrHidePopupPlayGif(View parentView, String filePath) {
        if (mPopupWindowPlayGif == null) {
            mPopupWindowPlayGif = PopupWindowPlayGif.newInstance(mContext, parentView);
        }
        if (mPopupWindowPlayGif.isShowing()) {
            mPopupWindowPlayGif.dismiss();
        } else {
            mPopupWindowPlayGif.showPopup(filePath);
        }
    }

    public void destroyPopupPlayGif() {
        if (mPopupWindowPlayGif != null && mPopupWindowPlayGif.isShowing()) {
            mPopupWindowPlayGif.dismiss();
            mPopupWindowPlayGif = null;
        } else if (mPopupWindowPlayGif != null) {
            mPopupWindowPlayGif = null;
        }
    }

    /**
     * show dialog confirm manager
     */
    public BaseSlidingFragmentActivity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(BaseSlidingFragmentActivity currentActivity) {
        this.mCurrentActivity = currentActivity;
    }

    public void releaseCurrentActivity(BaseSlidingFragmentActivity currentActivity) {
        if (mCurrentActivity == currentActivity) {
            mCurrentActivity = null;
        }
    }

    /**
     * PopupHelper.getInstance(application).checkAndShowDialogConfirmInApp("test", "msg test", null);
     *
     * @param title
     * @param msg
     * @param entry
     * @param listener
     * @return false if not show confirm
     */
    public boolean checkAndShowDialogConfirmAnyWhere(String title, String msg, Object entry, PositiveListener<Object> listener) {
        ApplicationController application = (ApplicationController) mContext.getApplicationContext();
        ApplicationStateManager applicationStateManager = application.getAppStateManager();
        String currentActivityName = applicationStateManager.getCurrentActivity();
        boolean isScreenOn = applicationStateManager.isScreenOn();
        boolean isLocked = applicationStateManager.isScreenLocker();
        if (mCurrentActivity == null || currentActivityName == null ||
                currentActivityName.equals(QuickReplyActivity.class.getCanonicalName()) ||
                currentActivityName.equals(QuickMissCallActivity.class.getCanonicalName())) {
            return false;
        } else if (!currentActivityName.equals(mCurrentActivity.getClass().getName())) {
            return false;
        }
        if (isLocked || !isScreenOn) {
            return false;
        }
        if (applicationStateManager.isActivityForResult() || applicationStateManager.isTakePhotoAndCrop()) {
            return false;// dang chụp anh, luu contact.
        }
        if (!applicationStateManager.isAppForeground()) return false;
        String titleOk = mContext.getString(R.string.ok);
        String titleCancel = mContext.getString(R.string.cancel);

        new DialogConfirm(mCurrentActivity, true)
                .setLabel(title)
                .setMessage(msg)
                .setNegativeLabel(titleCancel)
                .setPositiveLabel(titleOk)
                .setEntry(entry)
                .setPositiveListener(listener).show();
        return true;
    }

    public void showPopupRadioButton(FragmentManager fragmentManager, ArrayList<ItemContextMenu> list, ClickListener
            .IconListener listener) {
        PopupRadioButtonFragment.newInstance(mContext, null, list, listener).show(fragmentManager, TAG);
    }

    /*public boolean showDialogProgressChangeDomain() {
        ApplicationController application = (ApplicationController) mContext.getApplicationContext();
        ApplicationStateManager applicationStateManager = application.getAppStateManager();
        String currentActivityName = applicationStateManager.getCurrentActivity();
        boolean isScreenOn = applicationStateManager.isScreenOn();
        boolean isLocked = applicationStateManager.isScreenLocker();
        if (mCurrentActivity == null || currentActivityName == null ||
                currentActivityName.equals(QuickReplyActivity.class.getCanonicalName()) ||
                currentActivityName.equals(QuickMissCallActivity.class.getCanonicalName())) {
            return false;
        } else if (!currentActivityName.equals(mCurrentActivity.getClass().getName())) {
            return false;
        }
        if (isLocked || !isScreenOn) {
            return false;
        }
        if (applicationStateManager.isActivityForResult() || applicationStateManager.isTakePhotoAndCrop()) {
            return false;// dang chụp anh, luu contact.
        }
        if (!applicationStateManager.isAppForeground()) return false;
        mProgressChangeDomain = new ProgressDialog(mCurrentActivity);
        mProgressChangeDomain.setTitle(mRes.getString(R.string.app_name));
        mProgressChangeDomain.setMessage(mRes.getString(R.string.msg_waiting));
        mProgressChangeDomain.setIndeterminate(false);
        mProgressChangeDomain.setCancelable(false);
        mCurrentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressChangeDomain.show();
            }
        });
        return true;
    }*/

    /*public void hideDialogProgressChangeDomain() {
        if (mProgressChangeDomain != null) {
            if (mCurrentActivity != null) {
                mCurrentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //mProgressChangeDomain.dismiss();
                    }
                });
            } else {
                //mProgressChangeDomain.dismiss();
            }
        }
    }*/

    public void loadPopupIntro(ApplicationController mApp, String id, final HttpCallBack httpCallBack) {
        if (TextUtils.isEmpty(id)) {
            return;
        }
        ReengAccount myAccount = mApp.getReengAccountBusiness().getCurrentAccount();
        if (!mApp.getReengAccountBusiness().isValidAccount() || myAccount == null || httpCallBack == null) {
            return;
        }
        String url = UrlConfigHelper.getInstance(mApp).getUrlConfigOfFile(Config.UrlEnum.GET_POPUP_INTRO);
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(myAccount.getJidNumber()).append(id).append(Constants.HTTP.CLIENT_TYPE_STRING).append(Config.REVISION)
                .append(myAccount.getToken()).append(timeStamp);

        ResfulString params = new ResfulString(url);
        params.addParam("msisdn", myAccount.getJidNumber());
        params.addParam("ref", id);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("timestamp", timeStamp);
        params.addParam("appId", "mocha");
        params.addParam("countryCode", mApp.getReengAccountBusiness().getRegionCode());
        params.addParam("languageCode", mApp.getReengAccountBusiness().getCurrentLanguage());
        params.addParam(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(mApp, sb.toString(), myAccount
                .getToken()));

        StringRequest req = new StringRequest(Request.Method.GET, params.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    httpCallBack.onSuccess(s);
                } catch (Exception e) {
                    httpCallBack.onFailure("ERROR");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                httpCallBack.onFailure(volleyError.getMessage());
            }
        });
        Log.i(TAG, "loadPopupIntro: " + params.toString());
        VolleyHelper.getInstance(mApp).addRequestToQueue(req, TAG, false);
    }

    public interface DialogCallOptionListener {
        void onCallFree(ThreadMessage threadMessage);

        void onCallOut(ThreadMessage threadMessage);

        void onVideoCall(ThreadMessage threadMessage);
    }
}
package com.metfone.selfcare.helper;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

import com.metfone.selfcare.R;

/**
 * Created by huybq7 on 10/6/2014.
 */
public class LayoutHelper {

    public static void setBackgroundResourceAndKeepPadding(View view, int idResource) {
        int pL = view.getPaddingLeft();
        int pT = view.getPaddingTop();
        int pR = view.getPaddingRight();
        int pB = view.getPaddingBottom();

//        view.setBackgroundResource(idResource);

        view.setPadding(pL, pT, pR, pB);
    }


   /* public static void setBackgroundDrawableAndKeepPadding(View view, Drawable backgroundDrawable) {

        Rect drawablePadding = new Rect();
        backgroundDrawable.getPadding(drawablePadding);

        // Add background padding to view padding and subtract any previous background padding
        Rect prevBackgroundPadding = (Rect) view.getTag(R.id.prev_background_padding);
        int left = view.getPaddingLeft() + drawablePadding.left -
                (prevBackgroundPadding == null ? 0 : prevBackgroundPadding.left);
        int top = view.getPaddingTop() + drawablePadding.top -
                (prevBackgroundPadding == null ? 0 : prevBackgroundPadding.top);
        int right = view.getPaddingRight() + drawablePadding.right -
                (prevBackgroundPadding == null ? 0 : prevBackgroundPadding.right);
        int bottom = view.getPaddingBottom() + drawablePadding.bottom -
                (prevBackgroundPadding == null ? 0 : prevBackgroundPadding.bottom);
        view.setTag(R.id.prev_background_padding, drawablePadding);

        view.setBackgroundDrawable(backgroundDrawable);
        view.setPadding(left, top, right, bottom);
    }*/

    public static int convertDpToPx(Context context, float dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float density = displayMetrics.density;
        return (int) Math.ceil(dp * density);
    }

    public static int convertPxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float density = displayMetrics.density;
        return (int) Math.ceil(px / density);
    }

    public static int getWidthScreen(Context context) {
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static int getHeightScreen(Context context) {
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    public static int getHeightLayoutStrangerMusic(Resources res) {
        DisplayMetrics displayMetrics = res.getDisplayMetrics();
        int dividerSize = (int) res.getDimension(R.dimen.margin_more_content_5);
        int screenSize = displayMetrics.widthPixels;
        int size = (screenSize - dividerSize) / 4;
        return size;
    }

    public static int getHeightImageSingleViewHolder(Resources res) {
        DisplayMetrics displayMetrics = res.getDisplayMetrics();
        int screenSize = displayMetrics.widthPixels;
        return 4 * (screenSize / 7);
    }


}
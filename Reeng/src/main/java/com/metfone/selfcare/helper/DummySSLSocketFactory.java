package com.metfone.selfcare.helper;

import com.metfone.selfcare.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
//import javax.net.ssl.X509TrustManager;

/**
 * Created by DungNH on 6/13/14.
 */
public class DummySSLSocketFactory extends SSLSocketFactory {
    private static final String TAG = DummySSLSocketFactory.class.getSimpleName();
    private SSLSocketFactory factory;

    public DummySSLSocketFactory(InputStream ca) {

        try {
            X509Certificate cert;
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null);

            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            cert = (X509Certificate)certificateFactory.generateCertificate(ca);
            String alias = cert.getSubjectX500Principal().getName();
            trustStore.setCertificateEntry(alias, cert);


            TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
            tmf.init(trustStore);
            TrustManager[] trustManagers = tmf.getTrustManagers();
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, null);
            factory = sslContext.getSocketFactory();

//            SSLContext sslcontent = SSLContext.getInstance("TLS");
////            KeyStore trustStore = KeyStore.getInstance("BKS");
//////            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
////            trustStore.load(ca, null);
////            TrustManagerFactory trustMgt = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
////            trustMgt.init(trustStore);
////            sslcontent.init(null, trustMgt.getTrustManagers(), new SecureRandom());
//
//            sslcontent.init(
//                    null, // KeyManager not required
//                    new TrustManager[]{new DummyTrustManager()},
//                    new java.security.SecureRandom());
//            factory = sslcontent.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            Log.e(TAG, "DummySSLSocketFactory", e);
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "Exception", ex);
        }
    }

//    public static SocketFactory getDefault() {
//        return new DummySSLSocketFactory(null);
//    }

    public Socket createSocket(Socket socket, String s, int i, boolean flag)
            throws IOException {
        return factory.createSocket(socket, s, i, flag);
    }

    public Socket createSocket(InetAddress inaddr, int i, InetAddress inaddr2,
                               int j) throws IOException {
        return factory.createSocket(inaddr, i, inaddr2, j);
    }

    public Socket createSocket(InetAddress inaddr, int i) throws IOException {
        return factory.createSocket(inaddr, i);
    }

    public Socket createSocket(String s, int i, InetAddress inaddr, int j)
            throws IOException {
        return factory.createSocket(s, i, inaddr, j);
    }

    public Socket createSocket(String s, int i) throws IOException {
        return factory.createSocket(s, i);
    }

    public String[] getDefaultCipherSuites() {
        return factory.getSupportedCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return factory.getSupportedCipherSuites();
    }
}

/**
 * Trust manager which accepts certificates without any validation except date
 * validation.
 */
//class DummyTrustManager implements X509TrustManager {
//
//    private static final String TAG = DummyTrustManager.class.getSimpleName();
//
//    public boolean isClientTrusted(X509Certificate[] cert) {
//        return true;
//    }
//
//    public boolean isServerTrusted(X509Certificate[] cert) {
//        try {
//            cert[0].checkValidity();
//            return true;
//        } catch (CertificateExpiredException e) {
//            Log.e(TAG, "CertificateExpiredException", e);
//            return false;
//        } catch (CertificateNotYetValidException e) {
//            Log.e(TAG, "CertificateNotYetValidException", e);
//            return false;
//        }
//    }
//
//    public void checkClientTrusted(X509Certificate[] x509Certificates, String s)
//            throws CertificateException {
//        // Do nothing for now.
//    }
//
//    public void checkServerTrusted(X509Certificate[] x509Certificates, String s)
//            throws CertificateException {
//        // Do nothing for now.
//    }
//
//    public X509Certificate[] getAcceptedIssuers() {
//        return new X509Certificate[0];
//    }
//}

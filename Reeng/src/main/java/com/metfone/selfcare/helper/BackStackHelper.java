package com.metfone.selfcare.helper;

import android.app.ActivityManager;
import android.content.Context;

import com.metfone.selfcare.util.Log;

import java.util.List;

/**
 * Created by thaodv on 15-Nov-14.
 */
public class BackStackHelper {
    private static final String TAG = BackStackHelper.class.getSimpleName();
    private static BackStackHelper instance;

    public static synchronized BackStackHelper getInstance() {
        if (instance == null) {
            instance = new BackStackHelper();
        }
        return instance;
    }


    public boolean checkIsLastInStack(Context ctx, String activityName) {
        try {
            ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = am.getRunningTasks(10);
            Log.i(TAG, "last activity in stack = " + taskList.get(0).topActivity.getClassName());
            if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(activityName)) {
                Log.i(TAG, "this is last activity in the stack");
                return true;
            } else {
                Log.i(TAG, "this is not last activity in the stack");
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
        return false;
    }

    public boolean isBackToHome(Context ctx, String activityName) {
        try {
            Log.i(TAG, "activityName: " + activityName);
            ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = am
                    .getRunningTasks(10);
            if (taskList.get(0).topActivity.getClassName().equals(activityName)
                    && taskList.get(0).numRunning <= 2) {
                return true;
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
        return false;
    }

    public boolean isBackToChooseContact(Context ctx, String activityName) {
        try {
            Log.i(TAG, "activityName: " + activityName);
            ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = am
                    .getRunningTasks(10);
            if (taskList.get(0).baseActivity.getClassName().equals(activityName)) {
                return true;
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception",e);
        }
        return false;
    }
}

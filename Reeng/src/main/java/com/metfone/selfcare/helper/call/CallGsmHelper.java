package com.metfone.selfcare.helper.call;

import com.metfone.selfcare.app.dev.ApplicationController;

/**
 * Created by toanvk2 on 6/14/2017.
 */

public class CallGsmHelper {
    private ApplicationController mApplication;
    private static CallGsmHelper mInstance;
    private boolean ring = false;
    private boolean callReceived = false;
    private String incomingNumber = null;

    private CallGsmHelper(ApplicationController application) {
        this.mApplication = application;
    }

    public static synchronized CallGsmHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new CallGsmHelper(application);
        }
        return mInstance;
    }

    public boolean isRing() {
        return ring;
    }

    public void setRing(boolean ring) {
        this.ring = ring;
    }

    public boolean isCallReceived() {
        return callReceived;
    }

    public void setCallReceived(boolean callReceived) {
        this.callReceived = callReceived;
    }

    public String getIncomingNumber() {
        return incomingNumber;
    }

    public void setIncomingNumber(String incomingNumber) {
        this.incomingNumber = incomingNumber;
    }
}

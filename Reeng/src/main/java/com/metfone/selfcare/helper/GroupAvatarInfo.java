package com.metfone.selfcare.helper;

import android.widget.ImageView;

import com.metfone.selfcare.database.model.ThreadMessage;

/**
 * Created by HUYPQ on 3/5/2015.
 */
public class GroupAvatarInfo {
    //private List<PhoneNumber> listPhoneNumbers;
    private int viewWith;
    private int groupSize;
    private ThreadMessage threadMessage;
    private ImageView mImgGroupAvatar;
    private boolean isForceToGenerate;
    public GroupAvatarInfo(ThreadMessage thread) {
        this.threadMessage = thread;
    }

    public int getViewWith() {
        return viewWith;
    }

    public void setViewWith(int viewWith) {
        this.viewWith = viewWith;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    public ThreadMessage getThreadMessage() {
        return threadMessage;
    }

    public void setImgGroupAvatar(ImageView mImgGroupAvatar) {
        this.mImgGroupAvatar = mImgGroupAvatar;
    }

    public ImageView getImgGroupAvatar() {
        return mImgGroupAvatar;
    }

    public void dispose() {
        //listPhoneNumbers = null;
        mImgGroupAvatar = null;
    }

    public boolean isForceToGenerate() {
        return isForceToGenerate;
    }

    public void setIsForceToGenerate(boolean isForceToGenerate) {
        this.isForceToGenerate = isForceToGenerate;
    }
}

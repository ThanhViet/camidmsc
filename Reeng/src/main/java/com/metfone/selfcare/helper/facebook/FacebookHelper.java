package com.metfone.selfcare.helper.facebook;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.URLUtil;

import androidx.core.content.FileProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bigant on 4/7/2015.
 */
public class FacebookHelper {
    private static final String TAG = FacebookHelper.class.getSimpleName();
    private static final int ERROR_CODE_INVALID_OAUTH = 190;

    private static final String PROFILE_PERMISSION = "public_profile";
    private static final String BIRTHDAY_PERMISSION = "user_birthday";
    private static final String EMAIL_PERMISSION = "email";
    private static final boolean PREFER_AUTO_SHARE = false;
    private PendingAction pendingAction = PendingAction.NONE;
    private boolean canPresentShareDialog;
    private boolean canPresentShareDialogWithPhotos;
    private BaseSlidingFragmentActivity activity;
    private ApplicationController mApplication;
    private AccessToken accessToken;

    private String shareMsg, shareTitle, shareDesc, shareUrl, shareImgUrl;
    private FacebookCallback<Sharer.Result> shareCallback;

    public FacebookHelper(BaseSlidingFragmentActivity activity) {
        this.activity = activity;
        this.mApplication = (ApplicationController) activity.getApplicationContext();
        if (Config.Server.FREE_15_DAYS) {
            canPresentShareDialogWithPhotos = false;
            canPresentShareDialog = false;
        } else {
            canPresentShareDialogWithPhotos = ShareDialog.canShow(SharePhotoContent.class);
            canPresentShareDialog = ShareDialog.canShow(ShareLinkContent.class);
        }
    }

    public static boolean isLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            return false;
        }
        return !accessToken.isExpired();
    }

    public void getProfile(CallbackManager callbackManager,
                           final OnFacebookListener listener,
                           final PendingAction action) {
        if (Config.Server.FREE_15_DAYS) {
            if (activity != null && !activity.isFinishing()) {
                activity.showError(R.string.e601_error_but_undefined, "");
            }
            return;
        }
        accessToken = AccessToken.getCurrentAccessToken();
        pendingAction = action;
        Profile profile = Profile.getCurrentProfile();
        if (profile != null && accessToken != null) {
            handlePendingAction(listener);
        } else {
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            Log.i(TAG, "onSuccess " + loginResult);
                            // xin quyen share xong no lai chay vao day @@
                            handlePendingAction(listener);
                        }

                        @Override
                        public void onCancel() {
                            Log.i(TAG, "onCancel ");
                            if (pendingAction != PendingAction.NONE) {
                                pendingAction = PendingAction.NONE;
                            }
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            Log.e(TAG, "FacebookException", exception);
                            if (pendingAction != PendingAction.NONE) {
                                pendingAction = PendingAction.NONE;
                            }
                            if (action != null && !activity.isFinishing())
                                activity.showError(R.string.login_facebook_error, "");
                        }
                    });
            LoginManager.getInstance().logInWithReadPermissions(
                    activity,
                    Arrays.asList(PROFILE_PERMISSION, EMAIL_PERMISSION));
        }

    }

    public boolean isCanPresentShareDialogWithPhotos() {
        // Can we present the share dialog for photos?
        return canPresentShareDialogWithPhotos;
    }

    //TODO share image bitmap to facebook
    public void shareImageToFb(final Bitmap bitmap,
                               final CallbackManager callbackManager,
                               final BaseSlidingFragmentActivity activity, final String gaLabel) {
        if (Config.Server.FREE_15_DAYS) {
            if (activity != null && !activity.isFinishing()) {
                activity.showError(R.string.e601_error_but_undefined, "");
            }
            return;
        }

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(bitmap)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG, "shareImageToFb: onSuccess");
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "shareImageToFb: onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "shareImageToFb: onError");
            }
        });
    }

    private void handlePendingAction(OnFacebookListener listener) {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;
        switch (previouslyPendingAction) {
            case NONE:
                break;
            case POST_PHOTO:
                handlePendingActionPostPhoto();
                break;
            case GET_USER_INFO:
                handlePendingActionGetProfile(listener);
                break;
        }
    }

    private void handlePendingActionPostPhoto() {
        Bitmap image = null;
        SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(image).build();
        ArrayList<SharePhoto> photos = new ArrayList<>();
        photos.add(sharePhoto);

        SharePhotoContent sharePhotoContent =
                new SharePhotoContent.Builder().setPhotos(photos).build();
        if (canPresentShareDialogWithPhotos) {
            ShareDialog shareDialog = new ShareDialog(activity);
            //shareDialog.registerCallback(callbackManager, shareCallback);
            shareDialog.show(sharePhotoContent);
        } else if (hasPublishPermission()) {
            ShareApi.share(sharePhotoContent, shareCallback);
        } else {
            pendingAction = PendingAction.POST_PHOTO;
        }
    }

    public void handlePendingActionGetProfile(final OnFacebookListener listener) {
        if (Config.Server.FREE_15_DAYS) {
            if (activity != null && !activity.isFinishing()) {
                activity.showError(R.string.e601_error_but_undefined, "");
            }
            return;
        }

        if (activity != null && !activity.isFinishing()) {
            activity.showLoadingDialog("", R.string.loading_get_facebook_info);
        }
        accessToken = AccessToken.getCurrentAccessToken();
        GraphRequest graphRequest = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json, GraphResponse response) {
                        FacebookRequestError error = response.getError();
                        if (error != null) {
                            if (activity != null && !activity.isFinishing())
                                activity.showError(R.string.facebook_get_error, "");
                        } else {
                            try {
                                Log.i(TAG, "jsonObject: " + json.toString());
                                Log.i(TAG, "JSON Result" + String.valueOf(json));
                                Log.v(TAG, response.toString());
                                String name = null;
                                String fbId = null;
                                String birthDayStr = null;
                                int genderInt = Constants.CONTACT.GENDER_MALE;
                                name = json.optString("name");
                                fbId = json.optString("id");
                                String genderStr = json.optString("gender");
                                if (!TextUtils.isEmpty(genderStr)) {
                                    if (!"male".equals(genderStr.toLowerCase())) {
                                        genderInt = Constants.CONTACT.GENDER_FEMALE;
                                    }
                                }

                                birthDayStr = TimeHelper.convertBirthDayFacebookToString(
                                        json.optString("birthday"));

                                String email = "";
                                email = json.optString("email");
                                if (listener != null) {
                                    listener.onGetInfoFinish(fbId, name, email, birthDayStr, genderInt);
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Exception", e);
                                if (activity != null && !activity.isFinishing())
                                    activity.showError(R.string.facebook_get_error, "");
                            }
                        }
                        if (activity != null && !activity.isFinishing())
                            activity.hideLoadingDialog();
                    }
                }
        );
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    //======================================================================
    // TODO share content to facebook ShareDialog.Mode.AUTOMATIC
    public void shareContentToFacebook(final BaseSlidingFragmentActivity activity,
                                       final CallbackManager callbackManager,
                                       final String linkUrl,
                                       final String imageUrl,
                                       final String contentTitle,
                                       final String contentDesc,
                                       final String serviceType) {
        if (Config.Server.FREE_15_DAYS) {
            if (activity != null && !activity.isFinishing()) {
                activity.showError(R.string.e601_error_but_undefined, "");
            }
            return;
        }

        ShareLinkContent.Builder builder = new ShareLinkContent.Builder();
        if (URLUtil.isNetworkUrl(linkUrl)) {
            builder.setContentUrl(Uri.parse(linkUrl));
        } else {
            builder.setContentUrl(Uri.parse("http://mocha.com.vn/"));
            builder.setQuote(linkUrl);
        }
        String hashTag = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FACEBOOK_HASHTAG);
        if (!TextUtils.isEmpty(hashTag) && !"-".equals(hashTag)) {
            builder.setShareHashtag(new ShareHashtag.Builder()
                    .setHashtag(hashTag)
                    .build());
        }

        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG, "shareContentToFacebook: onSuccess");
                if (TextUtils.isEmpty(serviceType))
                    logShareFacebook(linkUrl, "OA");
                else
                    logShareFacebook(linkUrl, serviceType);
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "shareContentToFacebook: onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "shareContentToFacebook: onError");
                logShareFacebookFail(linkUrl);
            }
        });
        shareDialog.show(builder.build(), ShareDialog.Mode.AUTOMATIC);
    }

    //=================================================================================

    private boolean hasPublishPermission() {
        accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
//        return false;
    }

    /**
     * facebook
     */

    public void logShareFacebookFail(String shareUrl) {
        if (TextUtils.isEmpty(shareUrl)) return;
        Resources res = mApplication.getResources();
        mApplication.trackingEvent(res.getString(R.string.ga_category_onmedia), res.getString(R.string
                .ga_onmedia_action_share_fb_fail), shareUrl);
    }

    private void logShareFacebook(final String shareUrl, final String serviceType) {
        Log.d(TAG, "logShareFacebook");
        if (TextUtils.isEmpty(shareUrl)) return;
        Resources res = mApplication.getResources();
        mApplication.trackingEvent(res.getString(R.string.ga_category_onmedia), res.getString(R.string
                .ga_onmedia_action_share_fb_success), shareUrl);
        if (mApplication.getReengAccountBusiness().isValidAccount() && NetworkHelper.isConnectInternet(mApplication)) {
            StringRequest request = new StringRequest(Request.Method.POST, UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.LOG_SHARE_FB), new Response
                    .Listener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i(TAG, "logShareFacebook: " + s);
                    //reset
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "VolleyError", volleyError);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    long timeStamp = TimeHelper.getCurrentTime();
                    StringBuilder sb = new StringBuilder().
                            append(shareUrl).
                            append(serviceType).
                            append(mApplication.getReengAccountBusiness().getJidNumber()).
                            append(mApplication.getReengAccountBusiness().getToken()).
                            append(timeStamp);
                    String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mApplication.getReengAccountBusiness().getToken());
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.HTTP.REST_MSISDN, mApplication.getReengAccountBusiness().getJidNumber());
                    params.put(Constants.HTTP.LINK_SHARE, shareUrl);
                    params.put(Constants.HTTP.SERVICE_TYPE, serviceType);
                    params.put(Constants.HTTP.TIME_STAMP, String.valueOf(timeStamp));
                    params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                    return params;
                }
            };
            VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
        }
    }

    public void shareImageFileToFacebook(final BaseSlidingFragmentActivity activity,
                                         final CallbackManager callbackManager,
                                         final ArrayList<String> filePaths) {
        if (activity == null || activity.isFinishing() || Utilities.isEmpty(filePaths)) return;
        if (Config.Server.FREE_15_DAYS) {
            activity.showError(R.string.e601_error_but_undefined, "");
            return;
        }
        SharePhotoContent.Builder builder = new SharePhotoContent.Builder();
        for (int i = 0; i < filePaths.size(); i++) {
            String filePath = filePaths.get(i);
            if (Utilities.notEmpty(filePath)) {
                try {
                    File file = new File(filePath);
                    if (file.isFile()) {
                        Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
                        SharePhoto sharePhoto = new SharePhoto.Builder().setImageUrl(uri).build();
                        builder.addPhoto(sharePhoto);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String hashTag = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FACEBOOK_HASHTAG);
        if (!TextUtils.isEmpty(hashTag) && !"-".equals(hashTag)) {
            builder.setShareHashtag(new ShareHashtag.Builder()
                    .setHashtag(hashTag)
                    .build());
        }
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG, "shareContentToFacebook: onSuccess");
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "shareContentToFacebook: onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "shareContentToFacebook: onError");
            }
        });
        shareDialog.show(builder.build(), ShareDialog.Mode.AUTOMATIC);
    }

    public void shareImageBitmapToFacebook(final BaseSlidingFragmentActivity activity,
                                           final CallbackManager callbackManager,
                                           final ArrayList<Bitmap> list) {
        if (activity == null || activity.isFinishing() || Utilities.isEmpty(list)) return;
        if (Config.Server.FREE_15_DAYS) {
            activity.showError(R.string.e601_error_but_undefined, "");
            return;
        }
        SharePhotoContent.Builder builder = new SharePhotoContent.Builder();
        for (Bitmap item : list) {
            try {
                SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(item).build();
                builder.addPhoto(sharePhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String hashTag = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FACEBOOK_HASHTAG);
        if (!TextUtils.isEmpty(hashTag) && !"-".equals(hashTag)) {
            builder.setShareHashtag(new ShareHashtag.Builder()
                    .setHashtag(hashTag)
                    .build());
        }
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG, "shareContentToFacebook: onSuccess");
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "shareContentToFacebook: onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "shareContentToFacebook: onError");
            }
        });
        shareDialog.show(builder.build(), ShareDialog.Mode.AUTOMATIC);
    }

    public void shareContentToFacebookLuckyWheel(final BaseSlidingFragmentActivity activity,
                                                 final CallbackManager callbackManager,
                                                 final String linkUrl,
                                                 final String imageUrl,
                                                 final String contentTitle,
                                                 final String contentDesc,
                                                 final String serviceType) {
        if (Config.Server.FREE_15_DAYS) {
            if (activity != null && !activity.isFinishing()) {
                activity.showError(R.string.e601_error_but_undefined, "");
            }
            return;
        }

        ShareLinkContent.Builder builder = new ShareLinkContent.Builder();
        if (URLUtil.isNetworkUrl(linkUrl)) {
            builder.setContentUrl(Uri.parse(linkUrl));
        } else {
            builder.setContentUrl(Uri.parse("http://mocha.com.vn/"));
            builder.setQuote(linkUrl);
        }
//        String hashTag = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FACEBOOK_HASHTAG);
        String hashTag = "#mocha\n#VongQuayMayMan";
        if (!TextUtils.isEmpty(hashTag) && !"-".equals(hashTag)) {
            builder.setShareHashtag(new ShareHashtag.Builder()
                    .setHashtag(hashTag)
                    .build());
        }

        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG, "shareContentToFacebook: onSuccess");
                if (TextUtils.isEmpty(serviceType))
                    logShareFacebook(linkUrl, "OA");
                else
                    logShareFacebook(linkUrl, serviceType);
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "shareContentToFacebook: onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "shareContentToFacebook: onError");
                logShareFacebookFail(linkUrl);
            }
        });
        shareDialog.show(builder.build(), ShareDialog.Mode.AUTOMATIC);
    }

    public void shareImageBitmapToFacebookLuckyWheel(final BaseSlidingFragmentActivity activity,
                                                     final CallbackManager callbackManager,
                                                     final Bitmap bitmap, String url, String serviceCode) {
        if (activity == null || activity.isFinishing() || bitmap == null) return;
        SharePhotoContent.Builder builder = new SharePhotoContent.Builder();
        try {
            SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(bitmap).build();
            builder.addPhoto(sharePhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String hashTag = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FACEBOOK_HASHTAG);
        String hashTag = "#mocha\n#VongQuayMayMan";
        if (!TextUtils.isEmpty(hashTag) && !"-".equals(hashTag)) {
            builder.setShareHashtag(new ShareHashtag.Builder()
                    .setHashtag(hashTag)
                    .build());
        }
        ShareDialog shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG, "shareContentToFacebook: onSuccess");
                logShareFacebook(url, serviceCode);
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "shareContentToFacebook: onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i(TAG, "shareContentToFacebook: onError");
                logShareFacebookFail(url);
            }
        });
        shareDialog.show(builder.build(), ShareDialog.Mode.AUTOMATIC);
    }


    public enum PendingAction {
        NONE,
        POST_PHOTO,
        GET_USER_INFO,
    }

    public interface OnFacebookListener {
        void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender);
    }
}
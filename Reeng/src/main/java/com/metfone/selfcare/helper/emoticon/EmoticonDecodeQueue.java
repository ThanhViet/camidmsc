package com.metfone.selfcare.helper.emoticon;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.util.Log;

import java.util.LinkedList;

/**
 * Created by toanvk2 on 1/15/15.
 */
public class EmoticonDecodeQueue {
    /**
     * Source: http://google-ukdev.blogspot.com/2009/01/crimes-against-code-and-using-threads.html
     */
    private final String TAG = EmoticonDecodeQueue.class.getSimpleName();
    private final LinkedList<Object> queue;
    private Thread thread;
    private boolean isThreadRunning = false;
    private EmoticonManager mEmoticonManager;
    private Html.ImageGetter imageGetter;

    public EmoticonDecodeQueue(Context context) {
        queue = new LinkedList<>();
        mEmoticonManager = EmoticonManager.getInstance(context); //ham nay chiem nhieu thoi gian chay nhat
        imageGetter = EmoticonManager.getInstance(context).getImageGetter();
        // neu thread chua chay thi chay thread
        start();
    }

    public void start() {
        if (thread == null) {
            thread =  new Thread() {
                @Override
                public void run() {
                    isThreadRunning = true;
                    internalRun();
                    isThreadRunning = false;
                }
            };
            thread.setName("thread decode emoticon");
            thread.setDaemon(true);
        }
        if (!isThreadRunning) {
            thread.start();
        }
    }


    public void stop() {
//        isThreadRunning = false;
        thread = null;
        queue.clear();
    }

    public void addTask(Object task) {
        if (task == null) {
            return;
        }
        synchronized (queue) {
            queue.addLast(task);
            queue.notify(); // notify any waiting threads
        }
    }

    private Object getNextTask() {
        synchronized (queue) {
            if (queue.isEmpty()) {
                notifyTaskEmpty();
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    Log.e(TAG, "InterruptedException", e);
                    stop();
                }
            }
            return queue.removeFirst();
        }
    }

    private void internalRun() {
        while (isThreadRunning) {
            Object task = getNextTask();
            decodeEmoticon(task);
        }
    }

    private void decodeEmoticon(Object object) {
        if (object == null) {
            return;
        }
        String content = (String) object;
        if (TextUtils.isEmpty(content)) {
            return;
        }
        String contentToTag = TextHelper.getInstant().escapeXml(content);
        contentToTag = EmoticonUtils.emoTextToTag(contentToTag);
        Spanned spanned = TextHelper.fromHtml(contentToTag, imageGetter, null);
        mEmoticonManager.addSpannedToEmoticonCache(content, spanned);
    }

    private void notifyTaskEmpty() {
        Log.d(TAG, "notifyTaskEmpty: ");
    }
}

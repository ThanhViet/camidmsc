package com.metfone.selfcare.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.datasource.LogDataSource;
import com.metfone.selfcare.database.model.LogKQI;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/17/2017.
 */

public class LogKQIHelper {

    private static final String TAG = LogKQIHelper.class.getSimpleName();

    private ApplicationController mApplication;
    private SharedPreferences mPref;
    private LogDataSource logDataSource;

    private static LogKQIHelper mInstance;

    public static synchronized LogKQIHelper getInstance(ApplicationController context) {
        if (mInstance == null) {
            mInstance = new LogKQIHelper(context);
        }
        return mInstance;
    }

    public LogKQIHelper(ApplicationController application) {
        this.mApplication = application;
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        logDataSource = LogDataSource.getInstance(mApplication);
    }


    //Log KQI
    public String saveLogKQI(String type, String time, String timeStart, String comment) {
        return saveLogKQI(type, time, timeStart, comment, false);
    }

    public String saveLogKQI(String type, String time, String timeStart, String comment, boolean forceSave) {
        try {
            //Lưu log
            ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
            if (mAccountBusiness.isEnableUploadLog() || forceSave) {
                LogKQI logKQI = new LogKQI(TimeHelper.formatTimeLogKQI(Long.parseLong(timeStart)) + "|" + type, time, "", comment);
                return insertLogContent(logKQI);
            }
        } catch (Exception ex) {

        }
        return null;
    }

    public String saveLogKQI(String type, String time, String timeStart, String comment, String info) {
        return saveLogKQI(type, time, timeStart, comment, info, false);
    }

    public String saveLogKQI(String type, String time, String timeStart, String comment, String info, boolean forceSave) {
        try {
            //Lưu log
            ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
            if (mAccountBusiness.isEnableUploadLog() || forceSave) {
                LogKQI logKQI = new LogKQI(TimeHelper.formatTimeLogKQI(Long.parseLong(timeStart)) + "|" + type, time, info, comment);
                return insertLogContent(logKQI);
            }
        } catch (Exception ex) {

        }
        return null;
    }

    public String insertLogContent(LogKQI content) {
        try {
            StringBuilder info = new StringBuilder();
            info.append(mApplication.getReengAccountBusiness().getJidNumber());
            info.append("|").append("ANDROID");
            info.append("|").append(Config.REVISION);
            info.append("|").append(NetworkHelper.getNetworkSubType(mApplication));
            info.append("|").append(NetworkHelper.getIPAddress(true));
            try {
                info.append("|").append(SignalStrengthHelper.getInstant(mApplication).getSignalStrength());
            } catch (Exception ex) {
                info.append(-1);
            }
            String device = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL;
            info.append("|").append(device);

            if (!TextUtils.isEmpty(content.getInfo()))
                content.setInfo(info + "|" + content.getInfo());
            else
                content.setInfo(info.toString() + "|");
//            Log.i(TAG, "log content: " + content.toString());

            LogDataSource.getInstance(mApplication).insertLog(content);
            return content.toString();
        } catch (Exception ex) {
            mApplication.logDebugContent("insertLogContent exception: " + ex.toString());
        }
        return null;
    }

    public void insertListLogContent(ArrayList<LogKQI> listContent) {
        LogDataSource.getInstance(mApplication).insertListLog(listContent);
    }

    public void dropTableLogContent() {
        LogDataSource.getInstance(mApplication).deleteTable();
    }

    public void uploadLogContent() {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            return;
        }

        final ReengAccount myAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (!mApplication.getReengAccountBusiness().isValidAccount() || myAccount == null) {
            return;
        }

        ArrayList<LogKQI> listContent = LogDataSource.getInstance(mApplication).getAllLog();
        if (listContent == null || listContent.isEmpty()) {
            Log.i(TAG, "no log yesterday");
            mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOG, System.currentTimeMillis
                    ()).apply();
            return;
        }

        //Goi api day log 1 ngay 1 lan
        startUploadLog();


    }

    //=====================================================================
    public void startUploadLog() {
        Log.i(TAG, "startUploadLog");
        saveDataToFile(new SaveFileListener() {
            @Override
            public void onSaveFileSuccess(String filePath) {
                Log.i(TAG, "onSaveFileSuccess: " + filePath);
                uploadLog(filePath);
            }
        });
    }

    public UploadRequest uploadLog(final String filePath) {
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        if (mAccountBusiness == null) mAccountBusiness = mApplication.getReengAccountBusiness();
        String uploadUrl = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SEND_LOG_KPI);
        long currentTime = TimeHelper.getCurrentTime();
        StringBuilder sb = new StringBuilder().
                append(mAccountBusiness.getJidNumber()).
                append(mAccountBusiness.getToken()).
                append(currentTime);
        String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
        try {
//            UploadManager mUploadManager = new UploadManager(mApplication);
//            UploadRequest request = new UploadRequest(uploadUrl)
//                    .addParams(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
//                    .addParams(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime))
//                    .addParams(Constants.HTTP.DATA_SECURITY, dataEncrypt)
//                    .setFormData(Constants.HTTP.FILE.REST_UP_FILE)
//                    .setFilePath(filePath)
//                    .setContainer(filePath)
//                    .setUploadListener(uploadLogListener);
//            mUploadManager.add(request);
//            return request;

            Http.upload(uploadUrl)
                    .putFile(Constants.HTTP.FILE.REST_UP_FILE, filePath)
                    .putParameter(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber())
                    .putParameter(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime))
                    .putParameter(Constants.HTTP.DATA_SECURITY, dataEncrypt)
                    .withCallBack(new HttpCallBack() {
                        @Override
                        public void onSuccess(String response) throws Exception {
                            Log.i(TAG, "onUploadComplete: " + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                int errorCode = -1;
                                if (object.has(Constants.HTTP.REST_CODE)) {
                                    errorCode = object.getInt(Constants.HTTP.REST_CODE);
                                }
                                if (errorCode == 200) {
                                    mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOG, System.currentTimeMillis()).apply();
                                    dropTableLogContent();
                                    FileHelper.deleteFile(mApplication, filePath);
                                }
                            } catch (Exception ex) {
                            }
                        }
                    })
                    .execute();


        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

//    private UploadListener uploadLogListener = new UploadListener() {
//        @Override
//        public void onUploadStarted(UploadRequest uploadRequest) {
//
//        }
//
//        @Override
//        public void onUploadComplete(UploadRequest uploadRequest, String response) {
//            Log.i(TAG, "onUploadComplete: " + response);
//            try {
//                JSONObject object = new JSONObject(response);
//                int errorCode = -1;
//                if (object.has(Constants.HTTP.REST_CODE)) {
//                    errorCode = object.getInt(Constants.HTTP.REST_CODE);
//                }
//                if (errorCode == 200) {
//                    mPref.edit().putLong(Constants.PREFERENCE.PREF_LAST_TIME_UPLOAD_LOG, System.currentTimeMillis()).apply();
//                    dropTableLogContent();
//                    FileHelper.deleteFile(mApplication, uploadRequest.getFilePath());
//                }
//            } catch (Exception ex) {
//            }
//        }
//
//        @Override
//        public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
//        }
//
//        @Override
//        public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long speed) {
//
//        }
//    };

    public void saveDataToFile(SaveFileListener listener) {
        GetFileAsynctask asynctask = new GetFileAsynctask(listener);
        asynctask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class GetFileAsynctask extends AsyncTask<Void, Void, String> {

        SaveFileListener listener;

        public GetFileAsynctask(SaveFileListener listener) {
            this.listener = listener;
        }

        @Override
        protected String doInBackground(Void... voids) {
            ArrayList<LogKQI> listLog = logDataSource.getAllLog();
            if (listLog != null && !listLog.isEmpty()) {
                StringBuilder data = new StringBuilder();
                for (LogKQI logModel : listLog) {
                    data.append(logModel.toString());
                    data.append("\n");
                }
                String filePath = FileHelper.writeToFile(mApplication, "logkqi", data.toString());
                Log.i(TAG, "log content: " + data.toString());
                return filePath;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.onSaveFileSuccess(s);
        }
    }

    public interface SaveFileListener {
        void onSaveFileSuccess(String filePath);
    }
}

package com.metfone.selfcare.helper.images;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import androidx.collection.LruCache;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

/**
 * Created by thaodv on 7/11/2014.
 */
public class ImageHelper {
    private static final String TAG = ImageHelper.class.getSimpleName();
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private static ImageHelper mInstance;
    //    private ApplicationController mApplication;
    private LruCache<String, Bitmap> mImageBitmapCache = null;
    public static final String IMAGE_NAME = "image_mocha_";
    public static final String IMAGE_GUEST_BOOK = "memory_";

    public static synchronized ImageHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ImageHelper(context);
        }
        return mInstance;
    }

    public ImageHelper(Context context) {
//        this.mApplication = (ApplicationController) context.getApplicationContext();
        final int memClass = ((ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE))
                .getMemoryClass();
        final int cacheSize = 1024 * 1024 * memClass / 20;
        mImageBitmapCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in bytes rather than
                // number of items.
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (key == null || bitmap == null)
            return;
        if (getBitmapFromMemCache(key) == null) {
            mImageBitmapCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        if (key == null)
            return null;
        if (mImageBitmapCache == null) {
            return null;
        }
        return mImageBitmapCache.get(key);
    }

    public File createImageFile(Context ctx) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = Constants.FILE.JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir(ctx);
        return File.createTempFile(imageFileName, Constants.FILE.JPEG_FILE_SUFFIX, albumF);
    }

    private File getAlbumDir(Context ctx) {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            createAlbumDir();
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir();
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        return null;
                    }
                }
            }
        }
        return storageDir;
    }

    public File createVideoFile(Context ctx) throws IOException {
        // Create an video file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String videoFileName = Constants.FILE.VIDEO_PREFIX_NAME + timeStamp + "_";
        File albumF = getVideoAlbumDir(ctx);
        return File.createTempFile(videoFileName, Constants.FILE.MP4_FILE_SUFFIX, albumF);
    }

    private File getVideoAlbumDir(Context ctx) {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            createAlbumDir();
            storageDir = mAlbumStorageDirFactory.getAlbumVideoStorageDir();
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        return null;
                    }
                }
            }
        }
        return storageDir;
    }

    private void createAlbumDir() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
    }

    /**
     * get pathImage for background
     *
     * @return
     */
    public static String getBackgroundImagePath(String fileName) {
        fileName += String.valueOf(TimeHelper.getCurrentTime());
        String background = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.BACKGROUND_FOLDER;
        File file = new File(background);
        if (!file.exists()) {
            file.mkdirs();
        }
        return background + File.separator + "background_" + fileName + Constants.FILE.JPEG_FILE_SUFFIX;
    }

    public static String getAvatarGroupFilePath(int threadId) {
        String fileName = Constants.PREFERENCE.PREF_AVATAR_FILE_GROUP + threadId;
        String avatarFolder = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.LIST_AVATAR_PATH;
        File file = new File(avatarFolder);
        if (!file.exists()) {
            file.mkdirs();
        }
        return avatarFolder + fileName + Constants.FILE.PNG_FILE_SUFFIX;
    }

    public static boolean saveBitmapToPath(Bitmap bitmap, String pathImage, Bitmap.CompressFormat compressFormat) {
        Log.i(TAG, "saveBitmapToPath " + pathImage);
        try {
            File file = new File(pathImage);
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(compressFormat, 70, out);
            out.flush();
            out.close();
            return true;
        } catch (Exception ex) {
            Log.e(TAG, "saveBitmapToPath: ", ex);
        }
        return false;
    }

    public static File saveBitmapToFile(Bitmap bitmap, String pathImage, Bitmap.CompressFormat compressFormat) {
        Log.i(TAG, "saveBitmapToPath " + pathImage);
        File file = null;
        try {
             file = new File(pathImage);
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(compressFormat, 70, out);
            out.flush();
            out.close();
        } catch (Exception ex) {
            Log.e(TAG, "saveBitmapToPath: ", ex);
        }
        return file;
    }

    public Bitmap getImageFromAsset(Context context, String filePath) {
        Bitmap bitmap;
        String realFilePath = filePath.replaceAll("assets:", "");
        AssetManager assetManager = context.getAssets();
        //kiem tra xem da ton tai trong cache cua anh nay chua
        bitmap = getBitmapFromMemCache(filePath);
        if (bitmap == null) {
            InputStream istr = null;
            try {
                istr = assetManager.open(realFilePath);
                bitmap = BitmapFactory.decodeStream(istr);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                try {
                    if (istr != null) istr.close();
                } catch (Exception ex) {
                    Log.e(TAG, "Exception", ex);
                }
            }
        }
        if (bitmap != null) {
            addBitmapToMemoryCache(filePath, bitmap);
        }
        return bitmap;
    }

    public static void setImageAlpha(ImageView imageView, float alpha) {
        imageView.setAlpha(alpha);
       /* } else {
            imageView.setAlpha((int) alpha * 255);
        }*/
    }

    public static Bitmap getSquareBitmap(Bitmap srcBmp) {
        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            return Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );

        } else {
            return Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }
    }

    public String saveViewToImageFile(View view, int viewWidth, int viewHeight) {
        String filePath = Config.Storage.REENG_STORAGE_FOLDER
                + Config.Storage.IMAGE_COMPRESSED_FOLDER + "/" + ImageHelper.IMAGE_NAME + System.currentTimeMillis()
                + ".jpg";
        Bitmap scaledBitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        view.draw(canvas);
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            // Returns an immutable bitmap from subset of the source bitmap,
            // transformed by the optional matrix. The new bitmap may be the
            // same object as source, or a copy may have been made. It is
            // initialized with the same density as the original bitmap. If
            // the source bitmap is immutable and the requested subset is
            // the same as the source bitmap itself, then the source bitmap
            // is returned and no new bitmap is created.
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
        // ByteArrayOutputStream out = null;
        FileOutputStream fos;
        try {
            // out = new ByteArrayOutputStream();
            fos = new FileOutputStream(filePath);
            int quality = 72;
            boolean scaled = scaledBitmap.compress(Bitmap.CompressFormat.JPEG,
                    quality, fos);
            Log.d(TAG,
                    "scaled = " + scaled + " height = "
                            + scaledBitmap.getHeight() + " width = "
                            + scaledBitmap.getWidth()
            );
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return null;
        } finally {
            scaledBitmap.recycle();
        }
        return filePath;
    }

    public static void saveBitmapToGallery(BaseSlidingFragmentActivity activity, String fileName,
                                           Bitmap bitmap, ApplicationController application) {
        try {
            File pathFolder = new File(Config.Storage.GALLERY_MOCHA);
            if (!pathFolder.exists()) {
                pathFolder.mkdirs();
            }
            String filePath = Config.Storage.GALLERY_MOCHA + "/" + fileName;

            Log.i(TAG, "saveBitmapToPath " + filePath);
            File file = new File(filePath);
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            out.flush();
            out.close();
            FileHelper.refreshGallery(application, file);
            activity.showToast(R.string.sticker_download_done);
        } catch (Exception ex) {
            Log.e(TAG, "saveBitmapToPath: ", ex);
            activity.showToast(R.string.e601_error_but_undefined);
        }
    }

    public static void blurAvatar(ApplicationController context, Bitmap source, Bitmap bmDefault, ImageView imgTarget) {
        if (source == null) {
            Glide.with(context)
                    .asBitmap()
                    .load(bmDefault)
                    .transition(withCrossFade())
                    .apply(new RequestOptions()
                            .placeholder(context.getAvatarBusiness().getAvatarDrawable()[6])
                            .centerCrop()
                            .transform(new BlurTransformation(15))
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(imgTarget);

        } else {
            Glide.with(context)
                    .asBitmap()
                    .load(source)
                    .transition(withCrossFade())
                    .apply(new RequestOptions()
                            .placeholder(context.getAvatarBusiness().getAvatarDrawable()[6])
                            .centerCrop()
                            .transform(new BlurTransformation(15))
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                    .into(imgTarget);

        }

    }

    public static void setAvatar(ApplicationController context, Bitmap source, Bitmap bmDefault, ImageView imgTarget) {
        Glide.with(context)
                .asBitmap()
                .load(source == null ? bmDefault : source)
                .transition(withCrossFade())
                .apply(new RequestOptions()
                        .placeholder(context.getAvatarBusiness().getAvatarDrawable()[6])
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(imgTarget);
    }
}
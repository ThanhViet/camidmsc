package com.metfone.selfcare.helper.game;

import android.content.res.Resources;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 1/30/2018.
 */

public class ShakeGameHelper {
    private static final String TAG = ShakeGameHelper.class.getSimpleName();
    private static ShakeGameHelper mInstance;
    private ApplicationController mApplication;
    private Resources mRes;
    private ReengAccountBusiness mAccountBusiness;
    private ShakeGameListener mListener;
    private boolean isRequesting = false;

    private ShakeGameHelper(ApplicationController application) {
        this.mApplication = application;
        this.mRes = mApplication.getResources();
    }

    public synchronized static ShakeGameHelper getInstance(ApplicationController application) {
        if (mInstance == null) {
            mInstance = new ShakeGameHelper(application);
        }
        return mInstance;
    }

    public boolean isRequesting() {
        return isRequesting;
    }

    public void setShakeListener(ShakeGameListener listener) {
        this.mListener = listener;
    }

    private void notifyError(int code, String msg) {
        if (mListener != null) {
            mListener.onError(code, msg);
        }
    }

    private void notifyResult(int resultId, String resultStr) {
        if (mListener != null) {
            mListener.onResult(resultId, resultStr);
        } /*else {

        }*/
    }

    public void requestShake() {
        isRequesting = true;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG);
        mAccountBusiness = mApplication.getReengAccountBusiness();
        Log.i(TAG, "requestShake ");
        final String defaultError = mRes.getString(R.string.e601_error_but_undefined);
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.GAME_SHAKE), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "requestShake onResponse: " + response);
                String decrypt = HttpHelper.decryptResponse(response, mAccountBusiness.getToken());
                Log.i(TAG, "requestShake decryptResponse: " + decrypt);
                try {
                    JSONObject object = new JSONObject(decrypt);
                    int code = object.optInt(Constants.HTTP.REST_CODE, -1);
                    JSONObject dataObject = object.optJSONObject("data");
                    if (code == HTTPCode.E200_OK && dataObject != null) {
                        String resultStr = dataObject.optString("desc");
                        int resultId = dataObject.optInt("id");
                        notifyResult(resultId, resultStr);
                    } else {
                        notifyError(code, object.optString(Constants.HTTP.REST_DESC, defaultError));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    notifyError(-1, defaultError);
                }
                isRequesting = false;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
                notifyError(-1, defaultError);
                isRequesting = false;
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }


    public interface ShakeGameListener {
        void onResult(int id, String msg);

        void onError(int code, String msg);
    }
}
package com.metfone.selfcare.helper.message;

import android.text.TextUtils;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.broadcast.SmsReceiver;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.util.Log;

import java.util.LinkedList;

/**
 * Created by toanvk2 on 5/21/2016.
 */
public class ProcessSmsReceivedQueue {
    private final String TAG = ProcessSmsReceivedQueue.class.getSimpleName();
    private ApplicationController mApplication;
    private final LinkedList<Object> queue;
    private Thread thread;
    private boolean isThreadRunning = false;

    public ProcessSmsReceivedQueue(ApplicationController application) {
        queue = new LinkedList<>();
        this.mApplication = application;
        // neu thread chua chay thi chay thread
        start();
    }

    public void start() {
        if (thread == null) {
            thread = new Thread() {
                @Override
                public void run() {
                    isThreadRunning = true;
                    internalRun();
                    isThreadRunning = false;
                }
            };
            thread.setName("thread processSmsReceivedQueue");
            thread.setDaemon(true);
        }
        if (!isThreadRunning) {
            thread.start();
        }
    }

    public void stop() {
        //        isThreadRunning = false;
        thread = null;
        queue.clear();
    }

    private void notifyTaskEmpty() {
        Log.d(TAG, "notifyTaskEmpty: ");
    }

    public void addTask(Object task) {
        if (task == null) {
            return;
        }
        synchronized (queue) {
            queue.addLast(task);
            queue.notify(); // notify any waiting threads
        }
    }

    private Object getNextTask() {
        synchronized (queue) {
            if (queue.isEmpty()) {
                notifyTaskEmpty();
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    Log.e(TAG,"Exception",e);
                    stop();
                }
            }
            return queue.removeFirst();
        }
    }

    private void internalRun() {
        while (isThreadRunning) {
            Object task = getNextTask();
            processSmsReceived(task);
        }
    }

    private void processSmsReceived(Object object) {
        Log.d(TAG, "processSmsReceived ---->>>>>>>>>>>>>>");
        if (object == null) {
            return;
        }
        if (object instanceof SmsReceivedObject) {
            SmsReceivedObject smsObj = (SmsReceivedObject) object;
            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
            if (!accountBusiness.isValidAccount()) return;
            while (!mApplication.isDataReady()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    Log.e(TAG,"Exception",e);
                }
            }
            String from = smsObj.getFrom();
            if (TextUtils.isEmpty(from)) return;
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                    getPhoneNumberProtocol(mApplication.getPhoneUtil(), from, accountBusiness.getRegionCode());
            if (phoneNumberProtocol != null) {
                from = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat
                                .E164));
                if (PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                        phoneNumberProtocol)) {
                    SmsReceiver.notifySmsChatReceived(from, smsObj.getContent());
                }
            }
        }
    }
}
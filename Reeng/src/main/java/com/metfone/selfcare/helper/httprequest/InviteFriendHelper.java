package com.metfone.selfcare.helper.httprequest;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.ConstantApi;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by thaodv on 4/30/2015.
 */
public class InviteFriendHelper {
    private static final String TAG = InviteFriendHelper.class.getSimpleName();
    private static InviteFriendHelper instance;
    private ContactBusiness mContactBusiness;
    private Resources mRes;
    private ReengAccountBusiness mAccountBusiness;
    private SharedPreferences mPref;

    private InviteFriendHelper() {

    }

    public static synchronized InviteFriendHelper getInstance() {
        if (instance == null) {
            instance = new InviteFriendHelper();
        }
        return instance;
    }

    public void showRecommendInviteFriendPopup(final ApplicationController applicationController,
                                               final BaseSlidingFragmentActivity mParentActivity,
                                               final String contactName,
                                               final String phoneNumber,
                                               final boolean isBackWhenComplete) {
        //TODO uncheck invite
//        NavigateActivityHelper.navigateToChatDetail(mParentActivity,
//                applicationController.getMessageBusiness().findExistingOrCreateNewThread(phoneNumber));
//        if (AVNOHelper.AVNO) {
//            NavigateActivityHelper.navigateToChatDetail(mParentActivity,
//                    applicationController.getMessageBusiness().findExistingOrCreateNewThread(phoneNumber));
//        } else {
            mRes = mParentActivity.getResources();
            String title = mRes.getString(R.string.note_title);
            String message = String.format(mRes.getString(R.string.recommend_invite_friend), contactName);
            new DialogConfirm(mParentActivity, true).setLabel(title).setMessage(message).
                    setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                    setEntry(phoneNumber).setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    ArrayList<String> listPhoneNumber = new ArrayList<>();
                    listPhoneNumber.add((String) result);
                    inviteFriends(applicationController, mParentActivity,
                            listPhoneNumber, isBackWhenComplete);
                }
            }).show();
        //}
    }

    public void showInviteFriendPopup(final ApplicationController applicationController,
                                      final BaseSlidingFragmentActivity mParentActivity,
                                      final String contactName,
                                      final String phoneNumber,
                                      final boolean isBackWhenComplete) {
        mRes = mParentActivity.getResources();
        String title = mRes.getString(R.string.note_title);
        String message = String.format(mRes.getString(R.string.recommend_invite_friend), contactName);
        new DialogConfirm(mParentActivity, true).setLabel(title).setMessage(message).
                setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                setEntry(phoneNumber).setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                ArrayList<String> listPhoneNumber = new ArrayList<>();
                listPhoneNumber.add((String) result);
                inviteFriends(applicationController, mParentActivity,
                        listPhoneNumber, isBackWhenComplete);
            }
        }).show();
    }

    public void showInviteFriendPopup(final ApplicationController applicationController,
                                      final BaseSlidingFragmentActivity mParentActivity,
                                      Object entry,
                                      final boolean isBackWhenComplete) {
        PhoneNumber phone = (PhoneNumber) entry;
        mRes = mParentActivity.getResources();
        String title = mRes.getString(R.string.note_title);
        String message = String.format(mRes.getString(R.string.recommend_invite_friend), phone.getName());
        new DialogConfirm(mParentActivity, true).setLabel(title).setMessage(message).
                setNegativeLabel(mRes.getString(R.string.cancel)).setPositiveLabel(mRes.getString(R.string.ok)).
                setEntry(entry).setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                ThreadMessage thread = applicationController.getMessageBusiness().findExistingOrCreateNewThread(phone.getJidNumber());
                thread.setMessageInvite(mParentActivity.getString(R.string.message_invite) + " " + mParentActivity.getString(R.string.kh_share_and_get_more_link));
                thread.setActionInvite(true);
                NavigateActivityHelper.navigateToChatDetail(mParentActivity, thread);
            }
        }).show();
    }
    private void logT(String m) {
        Log.e("TVV-InviteFriendHelper", m);
    }

    public void inviteFriends(final ApplicationController app, final BaseSlidingFragmentActivity mParentActivity,
                              final ArrayList<String> listFriends, final boolean isBackWhenComplete) {
        logT("inviteFriends > start");
        String inviteUrl = UrlConfigHelper.getInstance(app).getDomainFile() + ConstantApi.Url.File.API_SEND_INVITE_SMS;
        logT("inviteFriends > inviteUrl: " + inviteUrl);
        doRequestInvite(app, mParentActivity, listFriends, isBackWhenComplete, inviteUrl);
        logT("inviteFriends > end");
    }

    public void inviteInRoom(final ApplicationController app,
                             final BaseSlidingFragmentActivity mParentActivity,
                             final ArrayList<String> listFriends,
                             final String roomId,
                             final InviteFromRoomCallBack listener) {
        String title = app.getString(R.string.title_invite_friend);
        String msg = app.getString(R.string.waiting);
        mParentActivity.showLoadingDialog(title, msg);
        //
        if (mContactBusiness == null) mContactBusiness = app.getContactBusiness();
        if (mAccountBusiness == null) mAccountBusiness = app.getReengAccountBusiness();
        if (mRes == null) mRes = mParentActivity.getResources();
        //
        final String inviteUrl = UrlConfigHelper.getInstance(app).getUrlConfigOfFile(Config.UrlEnum
                .REQUEST_INVITE_ROOM);
        StringRequest request = new StringRequest(Request.Method.POST, inviteUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse:" + response);
                int errorCode = -1;
                try {
                    mParentActivity.hideLoadingDialog();
                    JSONObject responseObject = new JSONObject(response);
                    if (responseObject.has(Constants.HTTP.REST_CODE)) {
                        errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        mParentActivity.showToast(mRes.getString(R.string.invite_friend_success), Toast.LENGTH_SHORT);
                        listener.onRequestSuccess();
                    } else {
                        mParentActivity.showToast(mRes.getString(R.string.e500_internal_server_error), Toast
                                .LENGTH_SHORT);
                        listener.onRequestFailed();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mParentActivity.showToast(mRes.getString(R.string.e500_internal_server_error), Toast.LENGTH_LONG);
                    listener.onRequestFailed();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showError(app.getString(R.string.e601_error_but_undefined), null);
                listener.onRequestFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                HashSet<String> hashSetFriends = new HashSet<>(listFriends);
                long currentTime = TimeHelper.getCurrentTime();

                JSONArray jsonArray = new JSONArray();
                for (String number : hashSetFriends) {
                    jsonArray.put(number);
                }
                String receiverString = jsonArray.toString();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(roomId).
                        append(receiverString).
                        append(account.getToken()).
                        append(currentTime);

                //////////////////////////
                String dataEncrypt = HttpHelper.encryptDataV2(app, sb.toString(), account.getToken());

                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.ROOM_ID, roomId);
                params.put(Constants.HTTP.FRIENDS, receiverString);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);

                return params;
            }
        };
        VolleyHelper.getInstance((ApplicationController) mParentActivity.getApplication()).addRequestToQueue(request,
                TAG, false);
    }

    private void doRequestInvite(final ApplicationController app,
                                 final BaseSlidingFragmentActivity mParentActivity,
                                 final ArrayList<String> listFriends,
                                 final boolean isBackWhenComplete,
                                 final String inviteUrl) {
        String title = app.getString(R.string.title_invite_friend);
        String msg = app.getString(R.string.waiting);

        mParentActivity.showLoadingDialog(title, msg);
        if (mContactBusiness == null) mContactBusiness = app.getContactBusiness();
        if (mAccountBusiness == null) mAccountBusiness = app.getReengAccountBusiness();
        if (mRes == null) mRes = mParentActivity.getResources();
        if (mPref == null)
            mPref = mParentActivity.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME,
                    Context.MODE_PRIVATE);
        StringRequest request = new StringRequest(Request.Method.POST, inviteUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse:" + response);
                int errorCode = -1;
                try {
                    mParentActivity.hideLoadingDialog();
                    JSONObject responseObject = new JSONObject(response);
                    if (responseObject.has(Constants.HTTP.REST_CODE)) {
                        errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        if (responseObject.has(Constants.HTTP.INVALID_USER)) {
                            JSONArray arrayInvalidUser = responseObject.getJSONArray("invalidUser");
                            if (arrayInvalidUser != null && arrayInvalidUser.length() > 0) {
                                ArrayList<String> listPhoneNumer = new ArrayList<>();
                                for (int i = 0; i < arrayInvalidUser.length(); i++) {
                                    listPhoneNumer.add(arrayInvalidUser.getString(i));
                                }
                                String listContact = mContactBusiness.getListNameOfListNumber(listPhoneNumer);
                                // Log.i(TAG, "listContact====" + listContact);
                                if (responseObject.has(Constants.HTTP.INVALID_USER_DESC)) {
                                    String toastInvalidUser = String.format(responseObject.getString(Constants.HTTP
                                            .INVALID_USER_DESC), listContact);
                                    mParentActivity.showToast(toastInvalidUser, Toast.LENGTH_LONG);
                                    // loi thi ko back lai
                                    // mParentActivity.onBackPressed();
                                }
                            }
                        } else if (responseObject.has(Constants.HTTP.LUCKY_CODE)) { // isLuckyCodeEnable == 1 de
                            // server check
                            String luckyCode = responseObject.getString(Constants.HTTP.LUCKY_CODE);
                            mPref.edit().putString(Constants.HTTP.LUCKY_CODE, luckyCode).apply();
                            mParentActivity.showToast(mRes.getString(R.string.msg_invite_complate), Toast.LENGTH_LONG);
                            /*if (!checkToShowShareLuckyCodeViaFacebook(app, mParentActivity)) {
                                mParentActivity.showToast(mRes.getString(R.string.msg_invite_complate), Toast
                                .LENGTH_LONG);
                                if (isBackWhenComplete) mParentActivity.onBackPressed();
                            }*/
                        } else {
                            mParentActivity.showToast(mRes.getString(R.string.msg_invite_complate), Toast.LENGTH_LONG);
                            if (isBackWhenComplete) mParentActivity.onBackPressed();
                        }
                        // invite friend thành công
                        LuckyWheelHelper.getInstance(app).doMission(Constants.LUCKY_WHEEL.ITEM_INVITE_FRIEND);
                    } else {
                        mParentActivity.showToast(mRes.getString(R.string.e500_internal_server_error), Toast
                                .LENGTH_LONG);
                        // loi thi ko back lai
                        // mParentActivity.onBackPressed();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mParentActivity.showToast(mRes.getString(R.string.e601_error_but_undefined), Toast.LENGTH_LONG);
                }
                navigateToSmsDefault(app, mParentActivity, listFriends);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mParentActivity.hideLoadingDialog();
                mParentActivity.showError(app.getResources().getString(R.string.e601_error_but_undefined), null);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                HashSet<String> hashSetFriends = new HashSet<>(listFriends);
                long currentTime = TimeHelper.getCurrentTime();
                JSONArray jsonArray = new JSONArray();
                for (String number : hashSetFriends) {
                    jsonArray.put(number);
                }
                String receiverString = jsonArray.toString();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(receiverString).
                        append("Android").
                        append(Config.REVISION).
                        append(account.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(app, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put(Constants.HTTP.RECEIVERS, receiverString);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put("clientType", "Android");
                params.put("revision", Config.REVISION);
                return params;
            }
        };
        VolleyHelper.getInstance((ApplicationController) mParentActivity.getApplication()).addRequestToQueue(request,
                TAG, false);
    }

    public void requestDeepLinkCampaign(final ApplicationController app,
                                        final BaseSlidingFragmentActivity mParentActivity,
                                        final ArrayList<String> listFriends,
                                        final String campaignId,
                                        final DeepLinkCampaignCallBack listener) {
        if (!NetworkHelper.isConnectInternet(app)) {
            listener.onError(-2);
            return;
        }
        String msg = app.getString(R.string.waiting);
        mParentActivity.showLoadingDialog(null, msg);
        if (mContactBusiness == null) mContactBusiness = app.getContactBusiness();
        if (mAccountBusiness == null) mAccountBusiness = app.getReengAccountBusiness();
        if (mRes == null) mRes = mParentActivity.getResources();
        //
        String url = UrlConfigHelper.getInstance(app).getUrlConfigOfFile(Config.UrlEnum.DEEPLINK_CAMPAIGN);

        Log.d(TAG, "requestDeepLinkCampaign url: " + url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse:" + response);
                int errorCode = -1;
                try {
                    mParentActivity.hideLoadingDialog();
                    JSONObject responseObject = new JSONObject(response);
                    if (responseObject.has(Constants.HTTP.REST_CODE)) {
                        errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                    }
                    if (errorCode == HTTPCode.E200_OK) {
                        listener.onSuccess();
                    } else {
                        listener.onError(errorCode);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    listener.onError(errorCode);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mParentActivity.hideLoadingDialog();
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                ReengAccount account = mAccountBusiness.getCurrentAccount();
                HashSet<String> hashSetFriends = new HashSet<>(listFriends);
                long currentTime = TimeHelper.getCurrentTime();
                ContactBusiness contactBusiness = app.getContactBusiness();
                JSONArray jsonArray = new JSONArray();
                for (String number : hashSetFriends) {
                    PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(number);
                    if (phoneNumber != null) {
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("contact", phoneNumber.getJidNumber());
                            jsonObject.put("t_opr", phoneNumber.getOperatorPresence());
                            jsonArray.put(jsonObject);
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }
                }
                String receiverString = jsonArray.toString();
                StringBuilder sb = new StringBuilder().
                        append(account.getJidNumber()).
                        append(mAccountBusiness.getOperator()).
                        append(campaignId).
                        append(receiverString).
                        append(account.getToken()).
                        append(currentTime);
                //////////////////////////
                String dataEncrypt = HttpHelper.encryptDataV2(app, sb.toString(), account.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
                params.put("campaign_id", campaignId);
                params.put("contacts", receiverString);
                params.put("f_opr", mAccountBusiness.getOperator());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                Log.d(TAG, "requestDeepLinkCampaign params: " + params.toString());
                return params;
            }
        };
        VolleyHelper.getInstance((ApplicationController) mParentActivity.getApplication()).addRequestToQueue(request,
                "DeepLinkCampaign", false);
    }


    private void navigateToSmsDefault(ApplicationController app, BaseSlidingFragmentActivity activity,
                                      ArrayList<String> numbers) {
        if (app.getReengAccountBusiness().isVietnam()) {
            StringBuilder sbNonViettel = new StringBuilder();
            for (String number : numbers) {
                PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(number);
                if (phoneNumber == null || !phoneNumber.isViettel()) {
                    sbNonViettel.append(number).append(";");
                }
            }
            if (sbNonViettel.length() > 0) {
                try {
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
                    sendIntent.putExtra("sms_body", activity.getResources().getString(R.string.invite_friend_content));
                    sendIntent.setData(Uri.parse("smsto:" + sbNonViettel.toString()));
                    //sendIntent.setType("vnd.android-dir/mms-sms");
                    activity.startActivity(sendIntent);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    activity.showToast(R.string.e666_not_support_function);
                }
            }
        }
    }

    public interface InviteFromRoomCallBack {
        void onRequestSuccess();

        void onRequestFailed();
    }

    public interface DeepLinkCampaignCallBack {
        void onSuccess();

        void onError(int errorCode);
    }

}

package com.metfone.selfcare.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.fragment.login.CreateAccountFragment;
import com.metfone.selfcare.fragment.login.EnterPhoneNumberFragment;
import com.metfone.selfcare.fragment.login.IntroduceFragment;
import com.metfone.selfcare.fragment.login.LoginPassWordFragment;
import com.metfone.selfcare.fragment.login.OTPForgotPasswordFragment;
import com.metfone.selfcare.fragment.login.OTPLoginFragment;
import com.metfone.selfcare.fragment.login.PersonalInfoFragment;
import com.metfone.selfcare.fragment.login.SetNewpasswordFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnEditProfileListener;
import com.metfone.selfcare.module.backup_restore.RestoreModel;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.dialog.PermissionDialog;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.login.DateOfBirthFragment;
import com.metfone.selfcare.v5.login.InfoNameFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThaoDV on 6/6/14.
 */
public class LoginActivity extends BaseSlidingFragmentActivity implements
        EnterPhoneNumberFragment.OnFragmentInteractionListener,
        OTPLoginFragment.OnFragmentInteractionListener,
        PersonalInfoFragment.OnFragmentPersonalInfoListener,
        IntroduceFragment.OnFragmentDetectPhoneNumberListener,
        LoginPassWordFragment.OnFragmentInteractionListener,
        CreateAccountFragment.OnFragmentInteractionListener,
        FacebookHelper.OnFacebookListener,
        OnEditProfileListener,
        ClickListener.IconListener {
    public static final int REQUEST_CODE_COUNTRY = 12321;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final String TAG_LOGIN = "Login";
    private static final String TAG_REGISTER = "Register";
    private static final String TAG_REGISTER_SECREEN = "LoginPassword";
    private static final String TAG_INFO = "Info";
    private static final String TAG_FORGOT_PASSWORD = "fogot";
    public static ApplicationController mApplication;
    ReengAccount reengAccountTmp;
    private OTPLoginFragment mOTPLoginFragment;
    private LoginPassWordFragment mLoginPassWordFragment;
    private OTPForgotPasswordFragment otpForgotPasswordFragment;
    private CreateAccountFragment createAccountFragment;
    private EnterPhoneNumberFragment mEnterPhoneNumberFragment;
    private PersonalInfoFragment mPersonaInfoFragment;
    private InfoNameFragment infoNameFragment;
    private ReengAccountBusiness mAccountBusiness;
    private SetNewpasswordFragment setnewPasswordScreen;
    private SharedPreferences mPref;
    private String detectPhoneNumberBySim = "";
    private String regionCode = "VN";
    //    private CallbackManager callbackManager;
    private ReengAccount currentAccount;
    private ClickListener.IconListener mIconListener;
    private FacebookHelper facebookHelper;
    private boolean needUploadAvatar = false;
    private String fileCropTmp;
    private boolean isDisplayFragment = false;
    private boolean isGotoSetting = false;
    private String fromSource;
    private String currentName;
    private int typeTakePhoto;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;
    private IOnBackPressed iOnBackPressed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, " onCreate ... ");
        super.onCreate(savedInstanceState);
        changeLightStatusBar();
        setContentView(R.layout.activity_detail_v2);
        mApplication = (ApplicationController) getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        currentAccount = mAccountBusiness.getCurrentAccount();
        facebookHelper = new FacebookHelper(this);
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
//        callbackManager = CallbackManager.Factory.create();
        trackingScreen(TAG);
        trackingDevideId(mApplication);
        // check permission
        checkPermissionOnFirstTime(savedInstanceState);
    }

    private void trackingDevideId(ApplicationController mApplication) {
        DeviceHelper.checkToSendDeviceIdOnFirstTime(mApplication);
    }

    @SuppressLint("MissingPermission")
    private void checkNetworkAndSetFragment(Bundle savedInstanceState) {
        isDisplayFragment = true;
        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            int phonePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_PHONE_STATE);
            if (phonePermissionStatus == PermissionHelper.PERMISSION_GRANTED) {
                String rc = PhoneNumberHelper.getInstant().detectRegionCodeFromDevice(mApplication);
                if (rc != null) {
                    regionCode = rc;
                }
                TelephonyManager tMgr = (TelephonyManager) mApplication.getSystemService(Context.TELEPHONY_SERVICE);
                if (tMgr != null)
                    detectPhoneNumberBySim = tMgr.getLine1Number();
            }

            /*Phonenumber.PhoneNumber phoneNumberDetect = PhoneNumberHelper.getInstant().detectPhoneNumberFromDevice
                    (mApplication);
            if (phoneNumberDetect != null) {
                PhoneNumberUtil phoneUtil = mApplication.getPhoneUtil();
                detectPhoneNumberBySim = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                        phoneUtil.format(phoneNumberDetect, PhoneNumberUtil.PhoneNumberFormat.E164));
                regionCode = phoneUtil.getRegionCodeForNumber(phoneNumberDetect);
            } else {
                String rc = PhoneNumberHelper.getInstant().detectRegionCodeFromDevice(mApplication);
                if (rc != null) {
                    regionCode = rc;
                }
            }*/
            //kiem tra xem da active chua
            //neu da active thi di den man hinh dat ten
            //neu khong thi hien thi man hinh register
//            if (NetworkHelper.isConnectInternet(this.getApplicationContext())) {
//                if (mAccountBusiness.isValidAccount() && !mAccountBusiness.isAnonymousLogin()) {
//                    Log.d(TAG, "Go to personal info");
//                    navigateToNextScreen();
//                } else {
//                    //neu da co thong tin dang nhap thi ko vao detect nua
//                    if (currentAccount != null && !TextUtils.isEmpty(currentAccount.getJidNumber())) {
//                        Log.i(TAG, "Go to login after deActive");
////                        displayRegisterFragment(currentAccount.getJidNumber(), currentAccount.getRegionCode());
//                        displayEnterPhoneNumberFragment(detectPhoneNumberBySim, currentAccount.getRegionCode());
//                    } /*else if (NetworkHelper.checkTypeConnection(this.getApplicationContext()) == NetworkHelper
//                            .TYPE_MOBILE) {
//                        Log.d(TAG, "Go to DetectPhoneFragment");
//                        *//*if (Config.Server.SERVER_TEST) {
//                            displayRegisterFragment(detectPhoneNumberBySim, regionCode);
//                        } else {
//                            displayIntroFragment();
//                        }*//*
////                        displayIntroFragment();
//                        displayRegisterFragment(detectPhoneNumberBySim, regionCode);
//                    } */ else {
//                        Log.d(TAG, "Go to RegisterFragment");// mac dinh de vn da, sau sua sau
////                        displayRegisterFragment(detectPhoneNumberBySim, regionCode);
////                        displayIntroFragment();
//                        displayEnterPhoneNumberFragment(detectPhoneNumberBySim, regionCode);
//                    }
//                }
//            } else {
//                Log.d(TAG, "Go to RegisterFragment");
////                if (Config.Server.SERVER_TEST) {
//                displayEnterPhoneNumberFragment(detectPhoneNumberBySim, regionCode);
////                } else {
////                    displayIntroFragment();
////                }
//
//                //                displayRegisterFragment(detectPhoneNumberBySim, regionCode);
//            }
            displayEnterPhoneNumberFragment(detectPhoneNumberBySim, regionCode);
        }
    }

    @Override
    protected void onPause() {
        this.hideLoadingDialog();
        mIconListener = null;
        super.onPause();
    }

    @Override
    public void onResume() {

        // resume khi ko chuyen sang home ngay
        // (nhieu khi chup anh de set avatar chayj thang vao resume nen loi ko cap nhat dc
//        if (mPersonaInfoFragment != null && mPersonaInfoFragment.isVisible()) {
//            goToHome();
//        }
        mIconListener = this;
        super.onResume();

        if (isGotoSetting && !isDisplayFragment) {
            checkNetworkAndSetFragment(null);
        }
    }


    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        this.hideLoadingDialog();
        super.onStop();
    }

    public void setiOnBackPressed(IOnBackPressed iOnBackPressed) {
        this.iOnBackPressed = iOnBackPressed;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "back to previous fragment when waiting code");
        if (iOnBackPressed != null){
            iOnBackPressed.onBackPressed();
            return;
        }
        //if (mLoginFragment != null && mLoginFragment.isVisible()) {
        this.hideLoadingDialog();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && (fragment instanceof PersonalInfoFragment
                || fragment instanceof InfoNameFragment || fragment instanceof DateOfBirthFragment || fragment instanceof  EnterPhoneNumberFragment)) {
            finish();
        } else {
            if (fragment instanceof OTPLoginFragment){
                displayEnterPhoneNumberFragment("", "+855");
            }else {
                int count = getSupportFragmentManager().getBackStackEntryCount();
                if(count > 0) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }

        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult");
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
//        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            String pathImage = picturePath.get(0);
                            cropAvatarImage(pathImage);
                        } else {
                            setActivityForResult(false);
                            showError(R.string.file_not_found_exception, null);
                        }
                    }
                    setActivityForResult(false);
                    setTakePhotoAndCrop(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    File imageFile = new File(mPref.getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    cropAvatarImage(imageFile.getPath());
                    setActivityForResult(false);
                    setTakePhotoAndCrop(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mPref.getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CROP, "");
//                        mAccountBusiness.processUploadAvatarTask(fileCrop);
                        setAvatarPersonalInfo(fileCrop);
                    }
                    break;
                case REQUEST_CODE_COUNTRY:
                    if (data != null) {
                        int index = data.getIntExtra("data", 0);
                        if (mEnterPhoneNumberFragment != null)
                            mEnterPhoneNumberFragment.updateRegionCode(index);
                    }

                default:
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: " + requestCode);
        dismissDialogFragment(PermissionDialog.TAG);
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_ALL) {
            mPref.edit().putBoolean(SharedPrefs.KEY.PERMISSION_EVER_REQUESTED, true).apply();
            mApplication.setFirstRequestPermission(false);
            if (PermissionHelper.allowedPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                mApplication.initFolder();
            }
            if (PermissionHelper.allowedPermission(this, Manifest.permission.WRITE_CONTACTS) && PermissionHelper.allowedPermission(this, Manifest.permission.READ_CONTACTS)) {
                mApplication.reLoadContactAfterPermissionsResult();
            }
//            if (PermissionHelper.allowedPermission(this, Manifest.permission.READ_SMS)) {
//                mApplication.registerSmsObserver();
//            }
        } else if (PermissionHelper.verifyPermissions(grantResults)) {
            if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO) {
                takePhoto();
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE) {
                mApplication.initFolder();
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_WRITE_CONTACT) {
                mApplication.reLoadContactAfterPermissionsResult();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (!checkCountryAfterAllowPermission())
            checkNetworkAndSetFragment(null);

    }

    @Override
    public void navigateToSetRegisterScreen(String numberInput, String region) {
        isDisplayFragment = true;
        displayEnterPhoneNumberFragment(numberInput, region);
    }

    @Override
    public void takeAPhoto(int typeUpload) {
        typeTakePhoto = typeUpload;
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto(typeUpload);
        }
    }

    private void takePhoto(int typeUpload) {
        try {
            typeUploadImage = typeUpload;
            mAccountBusiness.removeFileFromProfileDir();
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            intent.putExtra("return-data", true);
            mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, takePhotoFile.toString()).apply();
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    @Override
    public void openGallery(int typeUpload) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @Override
    public void onEditSuccess(boolean isSuccess) {

    }

    @Override
    public void goToHome() {
        if (needUploadAvatar) {
            mAccountBusiness.processUploadAvatarTask(fileCropTmp);
        }
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra("home_action", Constants.ACTION.VIEW_CREATE_CHAT);
        startActivity(intent, false);
        clearBackStack();
        finish();
    }

    @Override
    public void displayPersonalInfo() {
        isDisplayFragment = true;
        /*Fragment fragmentRegister = getSupportFragmentManager().findFragmentByTag(TAG_REGISTER);
        Fragment fragmentLogin = getSupportFragmentManager().findFragmentByTag(TAG_LOGIN);
        removeFragment(fragmentRegister);
        removeFragment(fragmentLogin);*/
        //clearBackStack();
        /*mPersonaInfoFragment = PersonalInfoFragment.newInstance("1", "2");
        executeFragmentTransaction(mPersonaInfoFragment, R.id.fragment_container, false, true);*/
        showLoadingDialog("", R.string.loading);
        ProfileRequestHelper.getInstance(mApplication).getUserInfo(
                mApplication.getReengAccountBusiness().getCurrentAccount(), new ProfileRequestHelper.onResponseUserInfoListener() {
                    @Override
                    public void onResponse(ReengAccount account) {
                        hideLoadingDialog();
                        Log.i(TAG, "name: " + account.getName());
//                        if (TextUtils.isEmpty(account.getName()))
                        goToPersonalInfo();
//                        else
//                            goToHome();

                    }

                    @Override
                    public void onError(int errorCode) {
                        hideLoadingDialog();
                        goToPersonalInfo();
                    }
                });
    }

    private void goToPersonalInfo() {
        /*Intent i = new Intent(this, ProfileActivity.class);
        i.putExtra("is_edit", true);
        i.putExtra(LoginActivity.class.getSimpleName(), true);
        startActivity(i);
        finish();*/
//        infoNameFragment = InfoNameFragment.newInstance();
//        executeFragmentTransactionAllowLoss(infoNameFragment, R.id.fragment_container, false, false, TAG_INFO);
//        if (SharedPrefs.getInstance().get(PREF_IS_LOGIN, Boolean.class)) {
//            NavigateActivityHelper.navigateToHomeScreenActivity((BaseSlidingFragmentActivity) this, true, true);
//        } else {
//            createAccountFragment = CreateAccountFragment.newInstance();
//            executeFragmentTransactionAllowLoss(createAccountFragment, R.id.fragment_container, false, false, "TAG_INFO");
//        }
        mApplication.getXmppManager().sendAvailable();
    }

    public void setNameAndGoToDateOfBirthFragment(String name) {
        currentName = name;
        DateOfBirthFragment fragment = DateOfBirthFragment.newInstance();
        executeFragmentTransactionAllowLoss(fragment, R.id.fragment_container, true, false, TAG_INFO);
    }

    @Override
    public void navigateToNextScreen() {
        if (Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES && mPref != null && !mPref.getBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, false)) {
            RestoreManager.getBackupFileInfo(new HttpCallBack() {
                @Override
                public void onSuccess(String data) throws Exception {
                    try {
                        Gson gson = new Gson();
                        RestoreModel restoreModel = gson.fromJson(data, RestoreModel.class);
                        if (restoreModel != null && restoreModel.getLstFile() != null && !restoreModel.getLstFile().isEmpty()) {
                            int lastIndex = 0;
                            RestoreModel.FileInfo fileInfo = restoreModel.getLstFile().get(lastIndex);
                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, data).apply();
                            displayRestoreInfo(fileInfo.getCapacity(), fileInfo.getPath(), fileInfo.getCreated_date(), fileInfo.getId());
                        } else {
                            displayPersonalInfo();
                        }
                    } catch (Exception e) {
                        displayPersonalInfo();
                    }
                }

                @Override
                public void onFailure(String message) {
                    super.onFailure(message);
                    Log.i(TAG, "getBackupFileInfo: fail " + message);
                    displayPersonalInfo();
                }
            });
        } else {
            displayPersonalInfo();
        }
    }

    @Override
    public void takeAPhoto() {
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(LoginActivity.this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto();
        }
    }

    @Override
    public void openGallery() {
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @Override
    public void getInfoFacebook() {
        facebookHelper.getProfile(getCallbackManager(), this, FacebookHelper.PendingAction.GET_USER_INFO);
    }

    private void takePhoto() {
        try {
            mAccountBusiness.removeFileFromProfileDir();
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.PROFILE_PATH, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            intent.putExtra("return-data", true);
            mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, takePhotoFile.toString()).apply();
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    /**
     * CROP IMAGE
     *
     * @param inputFilePath
     */
    private void cropAvatarImage(String inputFilePath) {
        // crop file
        try {
            String time = String.valueOf(System.currentTimeMillis());
            File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.PROFILE_PATH, "avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CROP, cropImageFile.toString()).apply();
            // intent crop
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            intent.putExtra(CropView.MASK_OVAL, true);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    private void displayIntroFragment() {
        executeFragmentTransaction(new IntroduceFragment(), R.id.fragment_container, false, false);
    }

    public void displayEnterPhoneNumberFragment(String numberInput, String region) {
        mEnterPhoneNumberFragment = EnterPhoneNumberFragment.newInstance(PhoneNumberHelper.getInstant().
                getNumberNational(mApplication.getPhoneUtil(), numberInput, region), region, fromSource);
        executeFragmentTransactionAllowLoss(mEnterPhoneNumberFragment, R.id.fragment_container, false, false, TAG_REGISTER);
    }

    @Override
    public void displayLoginOTPScreen(String phoneNumber, String regionCode, int code, String password) {
        mOTPLoginFragment = OTPLoginFragment.newInstance(phoneNumber, regionCode, code, password);
        executeFragmentTransactionAllowLoss(mOTPLoginFragment, R.id.fragment_container, false, true, TAG_LOGIN);
    }

    @Override
    public void displayOTPForgotScreen(String phoneNumber) {
        otpForgotPasswordFragment = OTPForgotPasswordFragment.newInstance(phoneNumber);
        executeFragmentTransactionAllowLoss(otpForgotPasswordFragment, R.id.fragment_container, true, true, TAG_FORGOT_PASSWORD);
    }


    @Override
    public void displayLoginOTPScreen(String numberJid, String regionCode, int code) {
        mOTPLoginFragment = OTPLoginFragment.newInstance(numberJid, regionCode, code, "");
        executeFragmentTransactionAllowLoss(mOTPLoginFragment, R.id.fragment_container, false, true, TAG_LOGIN);
    }

    @Override
    public void displayLoginPasswordScreen(String phoneNumber) {
        mLoginPassWordFragment = LoginPassWordFragment.newInstance(phoneNumber);
        executeFragmentTransactionAllowLoss(mLoginPassWordFragment, R.id.fragment_container, true, true, TAG_REGISTER_SECREEN);
    }

    @Override
    public void displayEnterPhoneNumberFragment(String phoneNumber) {
        mEnterPhoneNumberFragment = EnterPhoneNumberFragment.newInstance(PhoneNumberHelper.getInstant().
                getNumberNational(mApplication.getPhoneUtil(), phoneNumber, "KH"), "+855", fromSource);
        executeFragmentTransactionAllowLoss(mEnterPhoneNumberFragment, R.id.fragment_container, false, false, TAG_REGISTER);
    }

    @Override
    public void displayEnterPhoneNumberScreen(String phoneNumber) {
        mEnterPhoneNumberFragment = EnterPhoneNumberFragment.newInstance(PhoneNumberHelper.getInstant().
                getNumberNational(mApplication.getPhoneUtil(), phoneNumber, "KH"), "+855", fromSource);
        executeFragmentTransactionAllowLoss(mEnterPhoneNumberFragment, R.id.fragment_container, false, false, TAG_REGISTER);
    }

    @Override
    public void displaySetNewPasswordScreen(String phoneNumber, String otp) {
        setnewPasswordScreen = SetNewpasswordFragment.newInstance(phoneNumber, otp);
        executeFragmentTransactionAllowLoss(setnewPasswordScreen, R.id.fragment_container, true, true, "");
    }

    @Override
    public void displayCreateAccountScreen(String phoneNumber) {
        createAccountFragment = CreateAccountFragment.newInstance();
        executeFragmentTransactionAllowLoss(createAccountFragment, R.id.fragment_container, true, true, "");
    }

    @Override
    public void displayCreateAccountScreen(String phoneNumber, String otp, String code) {
        createAccountFragment = CreateAccountFragment.newInstance(phoneNumber, otp, code);
        executeFragmentTransactionAllowLoss(createAccountFragment, R.id.fragment_container, true, true, "");
    }


    @Override
    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
        try {
//            mApplication.getReengAccountBusiness().getCurrentAccount().setFacebookId(userId);
            ListenerHelper.getInstance().onRequestFacebookChange(name, birthDayStr, gender);
            mApplication.getAvatarBusiness().downloadAndSaveAvatar(mApplication, this, userId);
            Log.d(TAG, "requestGetProfile fullName: " + name + " userId: " + userId +
                    " birthDay: " + birthDayStr + " gender: " + gender + "=====:");
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            this.showError(R.string.facebook_get_error, "");
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                facebookHelper.shareContentToFacebook(this, getCallbackManager(), mApplication.getString(R.string.fb_share_url),
                        null, null, null, null);
                break;
            default:
                break;
        }
    }

    public void setAvatarPersonalInfo(String filePath) {
        if (infoNameFragment != null) {
            Log.i(TAG, "setAvatarPersonalInfo");
            fileCropTmp = filePath;
            needUploadAvatar = true;
            infoNameFragment.updateAvatar(filePath);

        }
    }

    public boolean isNeedUploadAvatar() {
        return needUploadAvatar;
    }

    public void setNeedUploadAvatar(boolean needUploadAvatar) {
        this.needUploadAvatar = needUploadAvatar;
    }

    public String getFileCropTmp() {
        return fileCropTmp;
    }

    private void checkPermissionOnFirstTime(Bundle saveInstance) {
        List<String> array = new ArrayList<>();
        boolean isDeniedPermanentOrNotRequested = false;
        //contact

        int contactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_CONTACTS);
        if (contactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.WRITE_CONTACTS);
        }
        /*int readcontactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_CONTACTS);

        if (readcontactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_CONTACTS);
        }*/

        int readContactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_CONTACTS);
        if (readContactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_CONTACTS);
        }

        //storage
        int storagePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (storagePermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        // received sms
//        int readSmsPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_SMS);
//        if (readSmsPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
//            array.add(Manifest.permission.READ_SMS);
//        }
//
//        int smsPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.RECEIVE_SMS);
//        if (smsPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
//            array.add(Manifest.permission.RECEIVE_SMS);
//        }

        // phone state
        int phonePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_PHONE_STATE);
        if (phonePermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!array.isEmpty()) {
            String[] permissions = new String[array.size()];
            permissions = array.toArray(permissions);
            if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE) {
                final String[] finalPermissions = permissions;
//                PermissionDialog.newInstance(Constants.PERMISSION.PERMISSION_REQUEST_ALL, true, array, new PermissionDialog.CallBack() {
//                    @Override
//                    public void onPermissionAllowClick(boolean allow, int clickCount) {
//                        if (allow) {
//                            checkAndRequestPermissionWhenStart();
//                        } else {
//                            PermissionHelper.requestPermissions(LoginActivity.this, finalPermissions, Constants.PERMISSION.PERMISSION_REQUEST_ALL);
//                        }
//                    }
//                }).show(getSupportFragmentManager(), PermissionDialog.TAG);
                checkAndRequestPermissionWhenStart();
            } else {
                PermissionHelper.requestPermissions(LoginActivity.this, permissions, Constants.PERMISSION.PERMISSION_REQUEST_ALL);
            }
            PermissionHelper.requestPermissions(LoginActivity.this, permissions, Constants.PERMISSION.PERMISSION_REQUEST_ALL);

        } else {
            checkNetworkAndSetFragment(saveInstance);
        }
    }

    private boolean checkCountryAfterAllowPermission() {
        int phonePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_PHONE_STATE);
//        if (phonePermissionStatus == PermissionHelper.PERMISSION_GRANTED) {
//            String rc = PhoneNumberHelper.getInstant().detectRegionCodeFromDevice(mApplication);
//            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
//            if ("KH".equalsIgnoreCase(rc) && (accountBusiness.getCurrentAccount() == null || !accountBusiness.isValidAccount())) {
//                Log.i(TAG, "KH not login, go anonymous");
//                showLoadingDialog("", R.string.loading);
//                mApplication.getApplicationComponent().provideUserApi().getInfoAnonymous(BuildConfig.KEY_XXTEA, new ApiCallbackV2<String>() {
//                    @Override
//                    public void onSuccess(String lastId, String s) throws JSONException {
//                        mApplication.getApplicationComponent().provideUserApi().unregisterRegid(0);
//                        mApplication.getConfigBusiness().getConfigFromServer(true, true);
//                        goToHome();
////                    processPermission();
//                    }
//
//                    @Override
//                    public void onError(String s) {
////                    showToast(s);
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        hideLoadingDialog();
//                    }
//                });
//                return true;
//            }
//        }
        return false;
    }

    private void checkAndRequestPermissionWhenStart() {
        ArrayList<String> array = new ArrayList<>();
        boolean isDeniedPermanentOrNotRequested = false;
        //contact

        int contactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_CONTACTS);
        if (contactPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            isDeniedPermanentOrNotRequested = true;
        }
        if (contactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.WRITE_CONTACTS);
        }

        int readContactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_CONTACTS);
        if (readContactPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            isDeniedPermanentOrNotRequested = true;
        }
        if (readContactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_CONTACTS);
        }

        /*int readcontactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_CONTACTS);
        if (readcontactPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            isDeniedPermanentOrNotRequested = true;
        }
        if (readcontactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_CONTACTS);
        }*/

        //storage
        int storagePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (storagePermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            isDeniedPermanentOrNotRequested = true;
        }
        if (storagePermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        // received sms
//        int smsPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.RECEIVE_SMS);
//        if (smsPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
//            isDeniedPermanentOrNotRequested = true;
//        }
//        if (smsPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
//            array.add(Manifest.permission.RECEIVE_SMS);
//        }
//
//        int readSmsPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_SMS);
//        if (readSmsPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
//            isDeniedPermanentOrNotRequested = true;
//        }
//        if (readSmsPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
//            array.add(Manifest.permission.READ_SMS);
//        }

        // phone state
        int phonePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_PHONE_STATE);
        if (phonePermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            isDeniedPermanentOrNotRequested = true;
        }
        if (phonePermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!array.isEmpty()) {
            boolean needGotoSetting = !mApplication.isFirstRequestPermission() && isDeniedPermanentOrNotRequested && mPref.getBoolean(SharedPrefs.KEY.PERMISSION_EVER_REQUESTED, false);
            String[] permissions = array.toArray(new String[array.size()]);

            if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE && needGotoSetting) {
                //go to settings
                isGotoSetting = true;
                dismissDialogFragment(PermissionDialog.TAG);
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                PermissionHelper.showDialogPermissionSettingIntro(LoginActivity.this, array, Constants.PERMISSION.PERMISSION_REQUEST_ALL);
            } else {
                PermissionHelper.requestPermissions(LoginActivity.this, permissions, Constants.PERMISSION.PERMISSION_REQUEST_ALL);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mApplication != null) {
            mApplication.setFirstRequestPermission(false);
        }
        EnumUtils.numberInput = null;
        super.onDestroy();
    }

    public String getCurrentName() {
        return currentName;
    }

    public void setCurrentName(String currentName) {
        this.currentName = currentName;
    }

    public ReengAccount getReengAccountTmp() {
        return reengAccountTmp;
    }

    public void getInfoFb() {
        FacebookHelper facebookHelper = new FacebookHelper(this);
        facebookHelper.getProfile(this.getCallbackManager(), new FacebookHelper.OnFacebookListener
                () {
            @Override
            public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
                try {
                    reengAccountTmp = new ReengAccount();
//                    mApplication.getReengAccountBusiness().getCurrentAccount().setFacebookId(userId);
                    // gender
                    ListenerHelper.getInstance().onRequestFacebookChange(name, birthDayStr, gender);
                    mApplication.getAvatarBusiness().downloadAndSaveAvatar(mApplication, LoginActivity.this,
                            userId);
                    reengAccountTmp.setName(name);
                    reengAccountTmp.setBirthdayString(birthDayStr);
                    reengAccountTmp.setGender(gender);
                    Log.d(TAG, "requestGetProfile fullName: " + name + " userId: " + userId + " birthDay: " +
                            birthDayStr + " gender: " + gender + "=====:");
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    showError(R.string.facebook_get_error, "");
                }
            }
        }, FacebookHelper.PendingAction.GET_USER_INFO);
    }
}
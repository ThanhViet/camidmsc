package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;

import com.metfone.selfcare.ui.dialog.PermissionSettingIntroDialog;

import java.util.ArrayList;

/*created by huongnd38 on 23/10/2018*/

public class PermissionSettingIntroActivity extends BaseSlidingFragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        int permissionCode = intent.getIntExtra("permissionCode", 1);
        ArrayList<String> permissionList = (ArrayList<String>) intent.getSerializableExtra("permissionList");
        PermissionSettingIntroDialog.showDialog(this, permissionList, permissionCode);
    }
}

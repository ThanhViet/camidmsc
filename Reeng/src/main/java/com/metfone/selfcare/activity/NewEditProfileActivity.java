package com.metfone.selfcare.activity;

import static com.metfone.selfcare.business.UserInfoBusiness.isEmailValid;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.SpinAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.view.CamIdButton;
import com.metfone.selfcare.ui.view.CamIdEditText;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.MappingUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;
import com.rd.utils.DensityUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewEditProfileActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = NewEditProfileActivity.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.clContainer)
    ConstraintLayout clContainer;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.edtFullName)
    CamIdEditText edtFullName;
    @BindView(R.id.edtEmail)
    CamIdEditText edtEmail;
    @BindView(R.id.edtContact)
    CamIdEditText edtContact;
    @BindView(R.id.edtIdNumber)
    CamIdButton edtIdNumber;
    @BindView(R.id.edtCurrentAddress)
    CamIdEditText edtCurrentAddress;
    @BindView(R.id.edtDob)
    CamIdTextView tvDob;
    @BindView(R.id.scr_edit_profile)
    NestedScrollView scr_edit_profile;
    @BindView(R.id.ivProfileAvatar)
    CircleImageView ivProfileAvatar;
    @BindView(R.id.btnEdit)
    CamIdTextView btnEdit;
    @BindView(R.id.spnSex)
    Spinner spnSex;
    @BindView(R.id.spnProvince)
    Spinner spnProvince;
    @BindView(R.id.spnDistrict)
    Spinner spnDistrict;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.btnCheckVerify)
    Button btnCheckVerify;
    ApplicationController mApplication;
    UserInfoBusiness userInfoBusiness;
    UserInfo currentUser;
    String[] provinceList;
    List<String> stringlist;
    ArrayAdapter<String> arrayadapter;
    boolean isFirstSelectDistrict = true;
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideKeyboard();
            return v.performClick();
        }
    };
    private ReengAccountBusiness mAccountBusiness;
    private boolean isNeedUploadAvatar = false;
    private String imagePath;
    private String fileCropTmp;
    private boolean isRemove;
    private int uploadOption = ImageProfileConstant.IMAGE_AVATAR;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;
    private int typeTakePhoto;
    private BottomSheetDialog bottomSheetDialog;
    private KhHomeClient homeClient;
    View.OnClickListener setBottomSheetItemClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvTakeAPhoto:
                    takeAPhoto(uploadOption);
                    break;
                case R.id.tvSelectFromGallery:
                    openGallery(uploadOption);
                    break;
                case R.id.tvRemovePhoto:
                    if (!TextUtils.isEmpty(fileCropTmp) || !TextUtils.isEmpty(currentUser.getAvatar())) {
                        isRemove = true;
                        fileCropTmp = "";
                        initRankInfo(userInfoBusiness.getAccountRankDTO());
                    }
                    break;
            }
            bottomSheetDialog.dismiss();
        }
    };
    private SpinAdapter districtAdapter;
    private TransferFileBusiness transferFileBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_edit_profile);
        unbinder = ButterKnife.bind(this);
        drawBackground();
        setListener();
        hideKeyboard();
        homeClient = new KhHomeClient();
        homeClient.logApp(Constants.LOG_APP.UPDATE_INFORMATION);
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        if (Utilities.hasNavBar(getResources())) {
            scr_edit_profile.setPadding(0, 0, 0, DensityUtils.dpToPx(32));
        }
        setupUI(scr_edit_profile);
        mApplication = (ApplicationController) getApplication();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        transferFileBusiness = new TransferFileBusiness(mApplication);
        userInfoBusiness = new UserInfoBusiness(this);
        currentUser = userInfoBusiness.getUser();
        if (currentUser != null) {
            drawProfile(currentUser);
        }

    }

    private void setListener() {
        districtAdapter = new SpinAdapter(this, android.R.layout.simple_spinner_item,
                new String[]{getString(R.string.hint_default_value)});
        districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnDistrict.setAdapter(districtAdapter);
        spnSex.setOnTouchListener(onTouchListener);
        spnProvince.setOnTouchListener(onTouchListener);
        stringlist = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.sex_your)));
        arrayadapter = new ArrayAdapter<String>(NewEditProfileActivity.this, R.layout.display_spinner, stringlist) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return stringlist.size() - 1;
            }
        };
        arrayadapter.setDropDownViewResource(R.layout.display_spinner);
        spnSex.setAdapter(arrayadapter);
        spnSex.setSelection(3);
        String[] provinceList = MappingUtils.getProvinceList(this);
        SpinAdapter provinceAdapter = new SpinAdapter(NewEditProfileActivity.this, android.R.layout.simple_spinner_item, provinceList) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return provinceList.length - 1;
            }
        };
        provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setSelection(provinceList.length - 1);
        customDropDownMenu(spnProvince);
        spnDistrict.setOnTouchListener(onTouchListener);
        spnDistrict.setSelection(0);
        customDropDownMenu(spnDistrict);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String province = spnProvince.getSelectedItem().toString();
                String code = MappingUtils.getProvinceCode(NewEditProfileActivity.this, province);
                districtAdapter.setValues(MappingUtils.getDistrictList(NewEditProfileActivity.this, code));
                if (!isFirstSelectDistrict) {
                    spnDistrict.setSelection(0, true);
                }
                hideKeyboard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void customDropDownMenu(Spinner spinner) {
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            PopupWindow popupWindow = (PopupWindow) popup.get(spinner);

            // Set popupWindow height to 500px
            if (popupWindow != null) {
                popupWindow.setHeight(50);
            }

//
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    void setUpSpinnerDown(Spinner spn) {
        spn.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                spn.setDropDownVerticalOffset(
                        spn.getDropDownVerticalOffset() + spn.getHeight());
                spn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void drawBackground() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            clContainer.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        } else {
            clContainer.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        }
    }

    private void drawProfile(UserInfo userInfo) {
        if (userInfo != null) {
            if (TextUtils.isEmpty(currentUser.getAvatar())) {
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    getRankAccount();
                }
            } else {
                Glide.with(this)
                        .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            }
            String userName = userInfo.getFull_name();
            String email = userInfo.getEmail();
            String currentAddress = userInfo.getAddress();
            String contact = userInfo.getContact();
            if (!TextUtils.isEmpty(currentUser.getVerified()) &&
                    EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(currentUser.getVerified())) {
                btnCheckVerify.setText(getString(R.string.verified_identity));
                btnCheckVerify.setEnabled(false);
                btnCheckVerify.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_verified_identity));
                edtIdNumber.setEnabled(false);
                edtIdNumber.setText(currentUser.getIdentity_number().trim());
                edtIdNumber.setTextColor(Color.parseColor("#000000"));
                edtFullName.setTextColor(Color.parseColor("#000000"));
                tvDob.setTextColor(Color.parseColor("#000000"));
                edtIdNumber.setGravity(Gravity.START);
                edtIdNumber.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_edittext));
                edtFullName.setEnabled(false);
                tvDob.setEnabled(false);
                spnSex.setEnabled(false);

                spnSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        ((TextView) adapterView.getChildAt(0)).setTextColor(Color.parseColor("#000000")); /* if you want your item to be white */
                        hideKeyboard();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            } else if (!TextUtils.isEmpty(currentUser.getVerified()) &&
                    EnumUtils.IdNumberVerifyStatusTypeDef.PENDING.equals(currentUser.getVerified())) {
                btnCheckVerify.setEnabled(false);
                edtIdNumber.setEnabled(false);
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    //call api to pass reject verify id number
                    getUserInformation();
                }
            }
            int gender = currentUser.getGender();
            if (gender > 0 && gender <= 3) {
                //Male
                spnSex.setSelection(gender - 1);
            }
            if (TextUtils.isEmpty(userName)) {
                edtFullName.setText(R.string.empty_string);
            } else {
                edtFullName.setText(userName);
            }
            if (TextUtils.isEmpty(email)) {
                edtEmail.setText(R.string.empty_string);
            } else {
                edtEmail.setText(email);
            }
            if (TextUtils.isEmpty(contact)) {
                edtContact.setText(R.string.empty_string);
            } else {
                edtContact.setText(contact);
            }
            if (TextUtils.isEmpty(currentAddress)) {
                edtCurrentAddress.setText(R.string.empty_string);
            } else {
                edtCurrentAddress.setText(currentAddress);
            }
            try {
                if (userInfo.getProvince() != null) {
                    int provincePos = MappingUtils.getProvinceIndexWithName(this,
                            userInfo.getProvince());
                    spnProvince.setSelection(provincePos);
                    if (isFirstSelectDistrict && userInfo.getDistrict() != null) {
                        String code = MappingUtils.getProvinceCode(NewEditProfileActivity.this, userInfo.getProvince());
                        int finalDistrictPos = MappingUtils.getDistrictIndex(this, code, userInfo.getDistrict());
                        isFirstSelectDistrict = false;
                        spnDistrict.postDelayed(() -> spnDistrict.setSelection(finalDistrictPos), 100);
                    }
                } else {
                    spnProvince.setSelection(provinceList.length - 1);
                }

            } catch (Exception ex) {

            }
            if (TextUtils.isEmpty(currentAddress)) {
                edtCurrentAddress.setText("");
            } else {
                edtCurrentAddress.setText(currentAddress);
            }

            if (userInfo.getDate_of_birth() != null) {
                try {
                    SharedPreferences mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                    String deviceLanguage = mApplication.getReengAccountBusiness().getDeviceLanguage();
                    String mCurrentLanguageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
                    String dob = TimeHelper.convertTimeFromAPi(userInfo.getDate_of_birth(), mCurrentLanguageCode);
                    if (dob.length() > 0) {
                        tvDob.setText(dob);
                    } else {
                        tvDob.setText("");
                    }

                } catch (Exception ignored) {
                    tvDob.setText("");
                }

            }
        }
    }

    @Optional
    @OnClick({R.id.btnBack, R.id.btnEdit, R.id.ivProfileAvatar, R.id.edtDob, R.id.edtIdNumber, R.id.btnCheckVerify})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCheckVerify:
            case R.id.edtIdNumber:
                String numberPhone = "";
                if (Utilities.isMetPhone(userInfoBusiness.getUser().getPhone_number())) {
                    numberPhone = userInfoBusiness.getUser().getPhone_number();
                }
                NavigateActivityHelper.navigateToVerifyNewInformation(this, numberPhone);
                break;
            case R.id.edtDob:
                InputMethodUtils.hideSoftKeyboard(this);
                showDialogTimePicker();
                break;
            case R.id.ivProfileAvatar:
                showPopupChangeAvatar();
                break;
            case R.id.btnEdit:
                updateUserInformation();
                break;
            case R.id.btnBack:
                finish();
                break;
        }
    }

    private void doUpdateUserInformation(UserInfo userInfo) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.updateUser(token, new BaseDataRequest<>(userInfo, "string", "string", "string", "string")).enqueue(new Callback<BaseResponse<BaseUserResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseUserResponseData>> call, Response<BaseResponse<BaseUserResponseData>> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    BaseResponse<BaseUserResponseData> baseResponse = response.body();
                    if ("00".equals(baseResponse.getCode())) {
                        if (baseResponse.getData() != null) {
                            currentUser = baseResponse.getData().getUser();
                            if (currentUser != null) {
                                userInfoBusiness.setUser(currentUser);
                            }
                            if (response.body().getData().getServices() != null) {
                                userInfoBusiness.setServiceList(baseResponse.getData().getServices());
                            }
                            userInfoBusiness.setIdentifyProviderList(baseResponse.getData().getIdentifyProviders());
                            finish();
                        }
                    } else {
                        ToastUtils.showToast(NewEditProfileActivity.this, baseResponse.getMessage());
                    }
                } else {
                    ToastUtils.showToast(NewEditProfileActivity.this, response.message());
                }
                EventBus.getDefault().post(new RequestRefreshInfo());
                android.util.Log.d(TAG, "onResponse: " + response.body());
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseUserResponseData>> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                android.util.Log.d(TAG, "onFailure: " + getString(R.string.service_error));
            }
        });

        if (isNeedUploadAvatar && !TextUtils.isEmpty(imagePath)) {
            isNeedUploadAvatar = false;
            mAccountBusiness.processUploadAvatarTask(imagePath);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
    }

    @SuppressLint("WrongConstant")
    private void showPopupChangeAvatar() {
        bottomSheetDialog = new BottomSheetDialog(this, R.style.NewBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.fragment_bottom_sheet_dialog);
        TextView tvTakeAPhoto = bottomSheetDialog.findViewById(R.id.tvTakeAPhoto);
        TextView tvSelectFromGallery = bottomSheetDialog.findViewById(R.id.tvSelectFromGallery);
        TextView tvRemovePhoto = bottomSheetDialog.findViewById(R.id.tvRemovePhoto);
        tvTakeAPhoto.setOnClickListener(setBottomSheetItemClick);
        tvSelectFromGallery.setOnClickListener(setBottomSheetItemClick);
        tvRemovePhoto.setOnClickListener(setBottomSheetItemClick);
        bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                CardView bottomSheet = (CardView) d.findViewById(R.id.cvContent);
                if (bottomSheet == null)
                    return;
                bottomSheet.setBackground(null);
            }
        });
        bottomSheetDialog.show();
    }

    public void takeAPhoto(int typeUpload) {
        typeTakePhoto = typeUpload;
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto(typeUpload);
        }
    }

    private void takePhoto(int typeUpload) {
        try {
            typeUploadImage = typeUpload;
            mAccountBusiness.removeFileFromProfileDir();
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            intent.putExtra("return-data", true);
            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, takePhotoFile.toString()).apply();
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    public void openGallery(int typeUpload) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    public void openGallery(int typeUpload, int maxImages) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, maxImages);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    private void cropAvatarImage(String inputFilePath) {
        try {
            String time = String.valueOf(System.currentTimeMillis());
            File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "/avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            mApplication.getPref().edit().putString(PREF_AVATAR_FILE_CROP, cropImageFile.toString()).apply();
            Intent intent = new Intent(this, SquareCropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                intent.putExtra(CropView.MASK_OVAL, true);
            }
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);

        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    public void getUserInfo(ReengAccount account) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ProfileRequestHelper.onResponseUserInfoListener listener = new ProfileRequestHelper
                .onResponseUserInfoListener() {
            @Override
            public void onResponse(ReengAccount account) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                mApplication.getAvatarBusiness().setMyAvatarFromGetUserInfo(ivProfileAvatar,
                        null, null, account);
            }

            @Override
            public void onError(int errorCode) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        };
        ProfileRequestHelper.getInstance(mApplication).getUserInfo(account, listener);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                // get information from facebook
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        Log.d(TAG, "onActivityResult ACTION_PICK_PICTURE");
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                                String pathImage = picturePath.get(0);
                                imagePath = pathImage;
                                cropAvatarImage(pathImage);
                            }
                        }
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    //Uri takeImageUri;
                    File imageFile = new File(mApplication.getPref().getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                        cropAvatarImage(imageFile.getPath());
                        imagePath = imageFile.getAbsolutePath();
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mApplication.getPref().getString(PREF_AVATAR_FILE_CROP, "");
                        setAvatarEditProfile(fileCrop);
                        isNeedUploadAvatar = true;
                    }
                    break;


                default:
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setAvatarEditProfile(String filePath) {
        Log.i(TAG, "setAvatarEditProfile");
        fileCropTmp = filePath;
        isRemove = false;
        File file = new File(filePath);
        Uri imageUri = Uri.fromFile(file);
        Glide.with(this)
                .load(imageUri).override(100, 100).
                error(R.drawable.ic_avatar_member).placeholder(R.drawable.ic_avatar_member)
                .into(ivProfileAvatar);
    }


    private void showDialogTimePicker() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        String dateChoseDefault;
//        if (TextUtils.isEmpty(mEdtBirthday.getText().toString())) {
//            dateChoseDefault = TimeHelper.formatTimeBirthday(TimeHelper.BIRTHDAY_DEFAULT_PICKER);
//        } else
//            dateChoseDefault = mEdtBirthday.getText().toString();

        if (TextUtils.isEmpty(tvDob.getText().toString()) || getString(R.string.sdf_oth_year_km).equals(tvDob.getText().toString())) {
            dateChoseDefault = UserInfoBusiness.getDefaultDate();

        } else {
            if (!"en".equals(UserInfoBusiness.getCurrentLanguage(this, mApplication))) {
                dateChoseDefault = tvDob.getText().toString();
            } else {
                dateChoseDefault = TimeHelper.convertTimeEnToKh(tvDob.getText().toString());
            }
        }
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(this, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                if (!"en".equals(UserInfoBusiness.getCurrentLanguage(NewEditProfileActivity.this, mApplication))) {
                    tvDob.setText(dateDesc);
                } else {
                    tvDob.setText(TimeHelper.convertKhToEn(dateDesc));
                }
                tvDob.setTextColor(getResources().getColor(R.color.color_tvphone));
//                Toast.makeText(MainActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
            }
        }).textConfirm(getResources().getString(R.string.confirm_button)) //text of confirm button
                .textCancel(getResources().getString(R.string.cancel_button)) //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .dateChose(dateChoseDefault) // date chose when init popwindow
                .minYear(1900)
                .maxYear(mYear + 1) // max year in loop
                .build();
        pickerPopWin.showPopWin(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO &&
                PermissionHelper.verifyPermissions(grantResults)) {
            takePhoto(typeTakePhoto);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void updateUserInformation() {
        if (TextUtils.isEmpty(edtFullName.getText().toString())) {
            ToastUtils.showToast(this, getResources().getString(R.string.enter_the_full_name));
        } else if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            ToastUtils.showToast(this, getResources().getString(R.string.enter_mail));
        } else if (TextUtils.isEmpty(edtCurrentAddress.getText().toString())) {
            ToastUtils.showToast(this, getResources().getString(R.string.enter_the_dress));
        } else if (TextUtils.isEmpty(tvDob.getText().toString())) {
            ToastUtils.showToast(this, getResources().getString(R.string.enter_dob));
        } else if (TextUtils.isEmpty(edtContact.getText().toString().trim())) {
            ToastUtils.showToast(this, getResources().getString(R.string.enter_your_contact));
        } else {
            String phoneNumber = currentUser.getPhone_number();
            String fullName = edtFullName.getText().toString().trim();
            String dateOfBirth = TimeHelper.convertTimeToAPi(tvDob.getText().toString(),
                    UserInfoBusiness.getCurrentLanguage(this, mApplication));
            android.util.Log.d(TAG, "onClick: " + dateOfBirth);
            String email = edtEmail.getText().toString().trim();
            String contact = edtContact.getText().toString().trim();
            int gender;
            if (getString(R.string.sex_male).equals(spnSex.getSelectedItem().toString())) {
                gender = 1;
            } else if (getString(R.string.sex_female).equals(spnSex.getSelectedItem().toString())) {
                gender = 2;
            } else {
                gender = 0;
            }
            if (edtEmail.getText().toString().isEmpty()) {
                ToastUtils.showToast(getApplicationContext(), getResources().getString(R.string.enter_email_address));
            } else {
                if (isEmailValid(edtEmail.getText().toString().trim())) {
                    String currentAddress = edtCurrentAddress.getText().toString().trim();
                    String province = spnProvince.getSelectedItem().toString();
                    String district = spnDistrict.getSelectedItem().toString();
                    if(province.toLowerCase(Locale.ROOT)
                            .equals(getString(R.string.hint_default_value).toLowerCase(Locale.ROOT))) {
                        province = getString(R.string.empty_string);
                        district = getString(R.string.empty_string);
                    }
                    String avatar = "";
                    if (!TextUtils.isEmpty(fileCropTmp)) {
                        File file = ImageUtils.saveBitmapToFile(new File(fileCropTmp));
                        if (file != null) {
                            avatar = ImageUtils.getFileToByte(file.getPath());
                        }
                    }

                    android.util.Log.d(TAG, "upDate: " + avatar);
                    UserInfo userInfo = userInfoBusiness.getUser();
                    userInfo.setEmail(email);
                    userInfo.setGender(gender);
                    userInfo.setAddress(currentAddress);
                    if (TextUtils.isEmpty(fileCropTmp) && isRemove) {
                        userInfo.setAvatar("");
                    } else {
                        userInfo.setAvatar(TextUtils.isEmpty(avatar) ? currentUser.getAvatar() : avatar);
                    }
                    userInfo.setProvince(province);
                    userInfo.setDistrict(district);
                    userInfo.setPhone_number(phoneNumber);
                    userInfo.setFull_name(fullName);
                    userInfo.setDate_of_birth(dateOfBirth);
                    userInfo.setContact(contact);
                    if (!NetworkHelper.isConnectInternet(this)) {
                        showError(getString(R.string.error_internet_disconnect), null);
                    } else {
                        doUpdateUserInformation(userInfo);
                    }
                } else {
                    ToastUtils.showToast(getApplicationContext(), getResources().getString(R.string.invalid_email_address));
                }
            }
        }

    }

    public void setupUI(View view) {

        if (!(view instanceof ScrollView)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(NewEditProfileActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount() {
        KhHomeClient homeKhClient = new KhHomeClient();
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                initRankInfo(body.getData().getAccountRankDTO());
                userInfoBusiness.setAccountRankDTO(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                ToastUtils.showToast(NewEditProfileActivity.this, message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                ToastUtils.showToast(NewEditProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void initRankInfo(AccountRankDTO account) {
        if (account != null) {
//            tvRankName.setText(account.rankName);
            int rankID = account.rankId;
            EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
            if (myRank != null) {
                Glide.with(this)
                        .load(myRank.drawable)
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            } else {
                Glide.with(this)
                        .load(ContextCompat.getDrawable(NewEditProfileActivity.this, R.drawable.ic_avatar_member))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            }
        } else {
            ivProfileAvatar.setImageDrawable(ContextCompat.getDrawable(NewEditProfileActivity.this, R.drawable.ic_avatar_member));
        }
    }

    private void getUserInformation() {
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (!TextUtils.isEmpty(userInfo.getVerified()) && EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(userInfo.getVerified())) {
                            btnCheckVerify.setText(getString(R.string.verified_identity));
                            btnCheckVerify.setBackground(ContextCompat.getDrawable(NewEditProfileActivity.this, R.drawable.bg_btn_verified_identity));
                            btnCheckVerify.setEnabled(false);
                        } else
                            btnCheckVerify.setEnabled(!TextUtils.isEmpty(userInfo.getVerified()) &&
                                    !EnumUtils.IdNumberVerifyStatusTypeDef.PENDING.equals(userInfo.getVerified()));
                    } else {
                        ToastUtils.showToast(NewEditProfileActivity.this, response.body().getMessage());
                    }

                }
//                else {
//                    ToastUtils.showToast(SetUpProfileActivity.this, "Get user information fail");
//                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                ToastUtils.showToast(NewEditProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

}
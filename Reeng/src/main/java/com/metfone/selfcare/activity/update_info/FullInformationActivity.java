package com.metfone.selfcare.activity.update_info;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.annotation.RequiresApi;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.NewEditProfileActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.ActivityFullInformationBinding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.metfoneplus.dialog.MPConfirmUpdateInformationToCamDialog;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.MappingUtils;

public class FullInformationActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = NewEditProfileActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private ActivityFullInformationBinding binding;
    private UserIdentityInfo userIdentityInfo;
    private UserInfoBusiness userInfoBusiness;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFullInformationBinding.inflate(getLayoutInflater());
        changeStatusBar(Color.parseColor("#1F1F1F"));
        setContentView(binding.getRoot());
        userInfoBusiness = new UserInfoBusiness(this);
        userIdentityInfo = (UserIdentityInfo) getIntent().getSerializableExtra(EnumUtils.OBJECT_KEY);
        if (userIdentityInfo != null) {
            drawProfile(userIdentityInfo);
        }
        mApplication = (ApplicationController) getApplication();
        SharedPrefs.getInstance().put(Constants.MF_PHONE, userIdentityInfo.getPhone());

        setAction();
    }

    private void setAction() {
        binding.btnConfirm.setOnClickListener(v -> {
            confirm();
        });
        binding.tvIDTypePicture.setOnClickListener(v -> {
            NavigateActivityHelper.navigateToIDAndPicture(this, userIdentityInfo);
        });
        binding.btnUpdate.setOnClickListener(v -> {
            NavigateActivityHelper.navigateToUpdateInfo(this, userIdentityInfo);
        });
        binding.btnBack.setOnClickListener(v -> {
            onBackPressed();
        });
        binding.tvAddAnother.setOnClickListener(v -> {
            NavigateActivityHelper.navigateToAnotherUser(this, userIdentityInfo);
        });
    }

    private void drawProfile(UserIdentityInfo userIdentityInfo) {
        if (userIdentityInfo != null) {
            UserIdentityInfo userMap = MappingUtils.mapBCCSToCamID(this, userIdentityInfo);
            String userName = userMap.getName();
            String currentAddress = userMap.getAddress();
            if (TextUtils.isEmpty(userName)) {
                binding.tvFullNameInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvFullNameInfo.setText(userName);
            }
            if (TextUtils.isEmpty(userMap.getIdIssueDate())) {
                binding.tvIssueDateInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvIssueDateInfo.setText(DateTimeUtils.dateStrToStringMP(userMap.getIdIssueDate()));
            }
            if (TextUtils.isEmpty(userMap.getIdExpireDate())) {
                binding.tvExpireInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvExpireInfo.setText(DateTimeUtils.dateStrToStringMP(userMap.getIdExpireDate()));
            }
            if (TextUtils.isEmpty(userMap.getBirthDate())) {
                binding.tvDobInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvDobInfo.setText(DateTimeUtils.dateStrToStringMP(userMap.getBirthDate()));
            }
            if (TextUtils.isEmpty(userMap.getIdNo())) {
                binding.tvIdNumberInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvIdNumberInfo.setText(userMap.getIdNo());
            }
            if (TextUtils.isEmpty(currentAddress)) {
                binding.tvFullAddressInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvFullAddressInfo.setText(currentAddress);
            }
            if (TextUtils.isEmpty(userMap.getStreetName())) {
                binding.tvStreetInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvStreetInfo.setText(userMap.getStreetName());
            }
            if (TextUtils.isEmpty(userMap.getHome())) {
                binding.tvHomeNoInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvHomeNoInfo.setText(userMap.getHome());
            }

            String gender = userMap.getSex();
            if (gender != null && !gender.isEmpty()) {
                String[] sex = getResources().getStringArray(R.array.sex);
                //Male
                if (gender.equals("0")) binding.tvGenderInfo.setText(sex[0]);
                    //Female
                else if (gender.equals("1")) binding.tvGenderInfo.setText(sex[1]);
                    //Other
                else binding.tvGenderInfo.setText(sex[2]);
            }
            if (TextUtils.isEmpty(userMap.getProvince())) {
                binding.tvProvinceInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvProvinceInfo.setText(userMap.getProvince());
            }
            if (TextUtils.isEmpty(userMap.getDistrict())) {
                binding.tvDistrictInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvDistrictInfo.setText(userMap.getDistrict());
            }
            if (TextUtils.isEmpty(userMap.getExpireVisa())) {
                binding.tvVisaExpireInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvVisaExpireInfo.setText(DateTimeUtils.dateStrToStringMP(userMap.getExpireVisa()));
            }
            if (TextUtils.isEmpty(userMap.getNationality())) {
                binding.tvNationalityInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvNationalityInfo.setText(userMap.getNationality());
            }
            if (TextUtils.isEmpty(userMap.getTelFax())) {
                binding.tvContactInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvContactInfo.setText(userMap.getTelFax());
            }
            if (TextUtils.isEmpty(userMap.getEmail())) {
                binding.tvEmailInfo.setText(R.string.three_sub_string);
            } else {
                binding.tvEmailInfo.setText(userMap.getEmail());
            }
        }
    }

    private void confirm() {
        MPConfirmUpdateInformationToCamDialog confirmInformationDialog = new MPConfirmUpdateInformationToCamDialog(this);
        confirmInformationDialog.setUserInfo(passDataDialog());
        confirmInformationDialog.show();
        confirmInformationDialog.setOnDismissListener(dialogInterface -> {
            if (!confirmInformationDialog.getBack()) {
                NavigateActivityHelper.navigateToSetUpProfile(this);
                finish();
            }
        });
    }

    private UserInfo passDataDialog() {
        String phoneNumber = userInfoBusiness.getUser().getPhone_number();
        String fullName = binding.tvFullNameInfo.getText().toString().trim();
        String idNumber = binding.tvIdNumberInfo.getText().toString().trim();
        String dob = binding.tvDobInfo.getText().toString().trim();
        int gender;

        if (getString(R.string.sex_male).equals(binding.tvGenderInfo.getText().toString())) {
            gender = 1;
        } else if (getString(R.string.sex_female).equals(binding.tvGenderInfo.getText().toString())) {
            gender = 2;
        } else {
            gender = 0;
        }

        String currentAddress = binding.tvFullAddressInfo.getText().toString().trim();
        String province = binding.tvProvinceInfo.getText().toString();
        String district = binding.tvDistrictInfo.getText().toString();
        String contact = binding.tvContactInfo.getText().toString();
        String email = binding.tvEmailInfo.getText().toString();
        String dateOfBirth = TimeHelper.convertTimeToAPi(dob,
                UserInfoBusiness.getCurrentLanguage(this, mApplication));
        android.util.Log.d(TAG, "onClick: " + dateOfBirth);

        if (phoneNumber.equals(getString(R.string.three_sub_string))) {
            phoneNumber = getString(R.string.empty_string);
        }
        if (fullName.equals(getString(R.string.three_sub_string))) {
            fullName = getString(R.string.empty_string);
        }
        if (idNumber.equals(getString(R.string.three_sub_string))) {
            idNumber = getString(R.string.empty_string);
        }
        if (province.equals(getString(R.string.three_sub_string))) {
            province = getString(R.string.empty_string);
        }
        if (district.equals(getString(R.string.three_sub_string))) {
            district = getString(R.string.empty_string);
        }
        if (currentAddress.equals(getString(R.string.three_sub_string))) {
            currentAddress = getString(R.string.empty_string);
        }
        if (contact.equals(getString(R.string.three_sub_string))) {
            contact = getString(R.string.empty_string);
        }
        if (email.equals(getString(R.string.three_sub_string))) {
            email = getString(R.string.empty_string);
        }
        boolean isValidateContact = Patterns.PHONE.matcher(contact).matches();
        if (!isValidateContact) {
            if (!TextUtils.isEmpty(userIdentityInfo.getPhone())) {
                contact = userIdentityInfo.getPhone();
            } else {
                contact = phoneNumber;
            }
        }

        UserInfo userInfo = new UserInfo();
        userInfo.setAddress(currentAddress);
        userInfo.setGender(gender);
        userInfo.setProvince(province);
        userInfo.setDistrict(district);
        userInfo.setPhone_number(phoneNumber);
        userInfo.setFull_name(fullName);
        userInfo.setIdentity_number(idNumber);
        userInfo.setDate_of_birth(dateOfBirth);
        userInfo.setContact(contact);
        userInfo.setEmail(email);
        return userInfo;
    }
}

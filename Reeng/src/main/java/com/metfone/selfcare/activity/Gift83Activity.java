package com.metfone.selfcare.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.LruCache;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.game.EventSonTungHelper;
import com.metfone.selfcare.ui.dialog.Gift83Dialog;


public class Gift83Activity extends BaseSlidingFragmentActivity implements View.OnClickListener, Gift83Dialog
        .OnGift83Listener, EventSonTungHelper.EventSonTungListener {

    private ImageView img00;
    private ImageView img01;
    private ImageView img02;
    private ImageView ivBackground;
    private LinearLayout background;
    private ImageView ivHear;
    private ImageView ivSonTung;
    private ImageView ivIntro;
    private ImageView ivBack;
    private TextView tvClickOnTheHeartToReceiveGifts;

    private LruCache<Integer, Bitmap> bitmapLruCache;
    private int s = 0;
    private int heightBackground = 0;
    private int wightBackground = 0;
    private ApplicationController mApp;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_83);
        mApp = (ApplicationController) getApplication();

        img00 = findViewById(R.id.img_00);
        img01 = findViewById(R.id.img_01);
        img02 = findViewById(R.id.img_02);
        ivBackground = findViewById(R.id.ivBackground);
        background = findViewById(R.id.background);
        ivHear = findViewById(R.id.ivHear);
        ivSonTung = findViewById(R.id.ivSonTung);
        ivIntro = findViewById(R.id.ivIntro);
        ivBack = findViewById(R.id.ivBack);
        tvClickOnTheHeartToReceiveGifts = findViewById(R.id.tvClickOnTheHeartToReceiveGifts);

        ivSonTung.setOnClickListener(this);
        ivIntro.setOnClickListener(this);
        ivBack.setOnClickListener(this);

        initView();
        EventSonTungHelper.getInstance(mApp).setEventSonTungListener(this);
    }

    @Override
    protected void onDestroy() {
        EventSonTungHelper.getInstance(mApp).setEventSonTungListener(null);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ivIntro) {
            // TODO: 3/5/2018
            showLoadingDialog("", getResources().getString(R.string.loading), true);
            EventSonTungHelper.getInstance(mApp).requestGift();
            tvClickOnTheHeartToReceiveGifts.setVisibility(View.GONE);
        } else if (view.getId() == R.id.ivBack) {
            finish();
        }
    }

    @Override
    public void onBtnAccessGift83Click() {
    }

    @Override
    public void onBtnShareGift83Click(Bitmap bitmap) {
        // TODO: 3/5/2018 l
        if (bitmap == null) {
            showToast(R.string.e601_error_but_undefined);
        } else {
            shareImageFacebook(bitmap, getResources().getString(R.string.ga_facebook_label_share_sontung83));
        }
    }

    private void initView() {
        Glide.with(this).load(R.drawable.img_83_hear).into(ivHear);
        Glide.with(this).load(R.drawable.img_83_son_tung_new).into(ivSonTung);
        Glide.with(this).load(R.drawable.img_83_background).into(ivBackground);
        background.post(new Runnable() {
            @Override
            public void run() {
                heightBackground = background.getHeight() - 20;
                wightBackground = background.getWidth();
                getBitmap();
                initBackground();
                initHear();
            }
        });
    }

    private void initHear() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) ivHear.getLayoutParams();
        layoutParams.leftMargin = 20;
        layoutParams.topMargin = 0;
        layoutParams.width = height * 900 / 506;
        layoutParams.height = height;
        ivHear.setLayoutParams(layoutParams);
        ivSonTung.setLayoutParams(layoutParams);
        ivHear.setVisibility(View.VISIBLE);
        ivSonTung.setVisibility(View.VISIBLE);

        int tran = height * 4000 / 26792;
        ivHear.setTranslationY(tran - 50);
        ivSonTung.setTranslationY(tran - 50);

        animationHear();
    }

    private void animationHear() {
        ivHear.setAlpha(1f);
        ivHear.setScaleX(0f);
        ivHear.setScaleY(0f);
        ivHear.animate()
                .scaleX(1.2f)
                .scaleY(1.2f)
                .setDuration(2 * 1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        animation.cancel();
                        ivHear.setScaleX(1.2f);
                        ivHear.setScaleY(1.2f);
                        ivHear.animate()
                                .scaleX(1.8f)
                                .scaleY(1.8f)
                                .alpha(0f)
                                .setDuration(1000)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        animation.cancel();
                                        animationHear();
                                    }
                                })
                                .start();
                    }
                })
                .start();
    }

    private void initBackground() {
        drawBitmapInView(img00, bitmapLruCache.get(R.drawable.img_83_background_first));
        drawBitmapInView(img01, bitmapLruCache.get(R.drawable.img_83_background_second));
        drawBitmapInView(img02, bitmapLruCache.get(R.drawable.img_83_background_first));

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) background.getLayoutParams();
        layoutParams.width = wightBackground;
        background.setLayoutParams(layoutParams);

        ValueAnimator moveAnimation = ValueAnimator.ofInt(0, -s);
        moveAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) background.getLayoutParams();
                layoutParams.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                background.setLayoutParams(layoutParams);
            }
        });
        moveAnimation.setDuration((long) (s / 0.2));
        moveAnimation.setInterpolator(new LinearInterpolator());
        moveAnimation.setRepeatMode(ValueAnimator.RESTART);
        moveAnimation.setRepeatCount(ValueAnimator.INFINITE);
        moveAnimation.start();
    }

    private void drawBitmapInView(ImageView view, Bitmap bitmap) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        layoutParams.width = bitmap.getWidth();
        layoutParams.height = bitmap.getHeight();
        wightBackground = wightBackground + bitmap.getWidth();
        view.setLayoutParams(layoutParams);
        view.setImageBitmap(bitmap);
    }

    private void getBitmap() {
        bitmapLruCache = new LruCache<>(2048);
        bitmapLruCache.put(R.drawable.img_83_background_first, resizeBitmap(BitmapFactory.decodeResource(getResources
                (), R.drawable.img_83_background_first)));
        bitmapLruCache.put(R.drawable.img_83_background_second, resizeBitmap(BitmapFactory.decodeResource
                (getResources(), R.drawable.img_83_background_second)));
    }

    private Bitmap resizeBitmap(Bitmap bitmap) {
        int wight = (int) ((float) (heightBackground * bitmap.getWidth()) / bitmap.getHeight());
        s = s + wight;
        return Bitmap.createScaledBitmap(bitmap, wight, heightBackground, false);
    }

    @Override
    public void onResult(int id, String msg) {
        hideLoadingDialog();
        Gift83Dialog.show(this, this, msg);
    }

    @Override
    public void onError(int code, String msg) {
        hideLoadingDialog();
        showToast(msg);
    }
}

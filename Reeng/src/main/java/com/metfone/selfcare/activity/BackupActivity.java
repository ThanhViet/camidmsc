package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.setting.BackupFragment;
import com.metfone.selfcare.helper.Constants;

public class BackupActivity extends BaseSlidingFragmentActivity {
    private final String TAG = BackupActivity.class.getSimpleName();
    public static final String SYNC_DESKTOP = "sync_desktop";
    SharedPreferences mPref;
    ApplicationController mApplication;
    private BackupFragment mBackUpFragment;
    int syncDesktop = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_v5);
        changeStatusBar(true);
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        mApplication = (ApplicationController) getApplicationContext();
        if (getIntent() != null)
            syncDesktop = getIntent().getIntExtra(SYNC_DESKTOP, 0);
//        setActionBar();
        if (savedInstanceState == null) {
            displayBackupFragment();
        }
        trackingScreen(TAG);
    }

    private void setActionBar() {
        setToolBar(findViewById(R.id.tool_bar));
    }

    public void displayBackupFragment() {
        mBackUpFragment = BackupFragment.newInstance(syncDesktop);
        executeFragmentTransaction(mBackUpFragment, R.id.fragment_container, false, true);
    }

    @Override
    public void onBackPressed() {
        if (mBackUpFragment != null && mBackUpFragment.isBackupInProgress()) {
            //backup is in progress
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mBackUpFragment = null;
        super.onDestroy();
    }
}

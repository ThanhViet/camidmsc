package com.metfone.selfcare.activity;

import static com.metfone.selfcare.business.UserInfoBusiness.setTextNotNull;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.SpinAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.DetectInfo;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.metfoneplus.dialog.MPConfirmInformationDialog;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDetachIPResponse;
import com.metfone.selfcare.ui.view.CamIdEditText;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.MappingUtils;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.Utils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Response;

public class FullFillInfoActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = NewEditProfileActivity.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.clContainer)
    ConstraintLayout clContainer;
    @BindView(R.id.edtFullName)
    CamIdEditText edtFullName;
    @BindView(R.id.tvFullName)
    CamIdTextView mTvFullName;
    @BindView(R.id.edtCurrentAddress)
    CamIdEditText edtCurrentAddress;
    @BindView(R.id.edtIdNumber)
    CamIdEditText edtIdNumber;
    @BindView(R.id.edtDob)
    CamIdTextView edtDob;
    @BindView(R.id.tvDob)
    CamIdTextView mTvDob;
    @BindView(R.id.tvSex)
    CamIdTextView mTvSex;
    @BindView(R.id.tvIdNumber)
    CamIdTextView mTvIDNumber;
    @BindView(R.id.scr_fullfill)
    NestedScrollView scr_fullfill;
    @BindView(R.id.tvCurrentAddress)
    CamIdTextView mTvCurrentAddress;
    @BindView(R.id.tvProvince)
    CamIdTextView mTvProvince;
    @BindView(R.id.tvDistrict)
    CamIdTextView mtvDistrict;
    @BindView(R.id.tvTitle)
    CamIdTextView tvTitle;
    @BindView(R.id.btnConfirm)
    CamIdTextView btnConfirm;
    @BindView(R.id.btnCancel)
    CamIdTextView btnCancel;
    @BindView(R.id.spnSex)
    Spinner spnSex;
    @BindView(R.id.spnProvince)
    Spinner spnProvince;
    @BindView(R.id.spnDistrict)
    Spinner spnDistrict;
    @BindView(R.id.edtContact)
    CamIdEditText edtContact;

    private UserInfoBusiness userInfoBusiness;
    private UserInfo currentUser;
    private String[] provinceList;
    private boolean isFirstSelectDistrict = true;
    private ApplicationController mApplication;
    private DetectInfo detectInfo;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideKeyboard();
            return v.performClick();
        }
    };
    private SpinAdapter districtAdapter;
    private HashMap<String, String[]> mapDistrictOfCity;
    private List<String> genders;
    private ArrayAdapter<String> arrayAdapter;
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#FFFFFF"));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_fill_info);
        unbinder = ButterKnife.bind(this);
        spanText();
        userInfoBusiness = new UserInfoBusiness(this);
        currentUser = userInfoBusiness.getUser();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        if (getIntent() != null) {
            detectInfo = getIntent().getParcelableExtra(EnumUtils.OBJECT_KEY);
            if (detectInfo != null) {
                currentUser.setFull_name(detectInfo.getFullname());
                currentUser.setDate_of_birth(DateTimeUtils.getDateFromOCRToInfo(detectInfo.getDob()));
                currentUser.setAddress(detectInfo.getAddress());
                currentUser.setGender(detectInfo.getGender());
                currentUser.setIdentity_number(detectInfo.getIdNumber());
            }
        }
        mApplication = (ApplicationController) getApplication();
        getData();
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setupUI(scr_fullfill);
        setAction();
    }

    private void setAction() {
        districtAdapter = new SpinAdapter(this, R.layout.spinner_custom_textcolor, new String[]{getString(R.string.hint_default_value)});
        districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        String[] provinceList = MappingUtils.getProvinceList(this);
        SpinAdapter provinceAdapter = new SpinAdapter(this, R.layout.spinner_custom_textcolor, provinceList) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return provinceList.length - 1;
            }
        };
        genders = Arrays.asList(getResources().getStringArray(R.array.sex_your));
        arrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_custom_textcolor, genders) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return genders.size() - 1;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSex.setAdapter(arrayAdapter);
        spnSex.setSelection(3);

        provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProvince.setAdapter(provinceAdapter);
        spnProvince.setSelection(provinceList.length - 1);
        spnDistrict.setAdapter(districtAdapter);
        spnDistrict.setSelection(0);
        spnSex.setOnTouchListener(onTouchListener);
        spnDistrict.setOnTouchListener(onTouchListener);
        spnSex.setOnItemSelectedListener(onItemSelectedListener);
        spnDistrict.setOnItemSelectedListener(onItemSelectedListener);
        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected: " + i);
                String province = spnProvince.getSelectedItem().toString();
                String code = MappingUtils.getProvinceCode(FullFillInfoActivity.this, province);
                districtAdapter.setValues(MappingUtils.getDistrictList(FullFillInfoActivity.this, code));
                if (!isFirstSelectDistrict) {
                    spnDistrict.setSelection(0, true);
                }
                hideKeyboard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getData() {
        int type = EnumUtils.VerifyIDNumberTypeDef.CREATE;
        if (getIntent().getExtras() != null) {
            type = getIntent().getIntExtra(EnumUtils.VERIFY_ID_NUMBER_TYPE, EnumUtils.VerifyIDNumberTypeDef.CREATE);
        }
        if (type != EnumUtils.VerifyIDNumberTypeDef.CREATE) {
            tvTitle.setText(getString(R.string.profile_update_information));
            btnCancel.setVisibility(View.VISIBLE);
            edtFullName.setEnabled(false);
            edtDob.setEnabled(false);
            spnSex.setEnabled(false);
            edtIdNumber.setEnabled(false);
            spnProvince.setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        drawProfile(currentUser);
        getIsdnFromIP(Utils.getIpOf3G());
    }

    public void getIsdnFromIP(String ip) {
        if (!NetworkHelper.isConnectInternet(this)) {
            if (edtContact.getText().toString().trim().isEmpty()) {
                edtContact.setText(currentUser.getPhone_number());
            }
            return;
        }
        new MetfonePlusClient().wsDetachIp(ip, new MPApiCallback<WsDetachIPResponse>() {
            @Override
            public void onResponse(Response<WsDetachIPResponse> response) {
                if (response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().getResult().getErrorCode().equals("0")) {
                    WsDetachIPResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    if (wsResponse != null && wsResponse.getIsdn() != null) {
                        if (edtContact.getText().toString().trim().isEmpty()) {
                            edtContact.setText(wsResponse.getIsdn());
                        } else {
                            edtContact.setText(currentUser.getPhone_number());
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
//                no-op

            }
        });
    }

    @Optional
    @OnClick({R.id.edtDob, R.id.btnConfirm, R.id.btnBack})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConfirm:
                upDate();
                break;
            //tvDob
            case R.id.edtDob:
                InputMethodUtils.hideSoftKeyboard(this);
                showDialogTimePicker();
                break;
            case R.id.btnBack:
                onBackPressed();
                break;
        }

    }


    private void drawProfile(UserInfo userInfo) {
        if (userInfo != null) {
            String userName = userInfo.getFull_name();
            String email = userInfo.getEmail();
            String currentAddress = userInfo.getAddress();
            String contact = userInfo.getContact();
            String idNumber = userInfo.getIdentity_number();

            if (TextUtils.isEmpty(userName)) {
                edtFullName.setText("");
            } else {
                edtFullName.setText(userName);
            }
            if (TextUtils.isEmpty(contact)) {
                edtContact.setText("");
            } else {
                edtContact.setText(contact);
            }
            if (TextUtils.isEmpty(currentAddress)) {
                edtCurrentAddress.setText("");
            } else {
                edtCurrentAddress.setText(currentAddress);
            }

            if (TextUtils.isEmpty(idNumber)) {
                edtIdNumber.setText("");
            } else {
                edtIdNumber.setText(idNumber);
            }

            if (!TextUtils.isEmpty(currentUser.getVerified()) &&
                    EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(currentUser.getVerified())) {
                edtFullName.setEnabled(false);
                edtIdNumber.setEnabled(false);
                edtDob.setEnabled(false);
                spnSex.setEnabled(false);
                spnProvince.setEnabled(false);
                spnDistrict.setEnabled(false);
            }
            int gender = currentUser.getGender();
            if (gender >= 0 && gender <= 2) {
                //Male
                if (gender == 1) spnSex.setSelection(0);
                    //Female
                else if (gender == 2) spnSex.setSelection(1);
                    //Other
                else spnSex.setSelection(2);
            }
            try {
                if (userInfo.getProvince() != null) {
                    int provincePos = MappingUtils.getProvinceIndexWithName(FullFillInfoActivity.this, userInfo.getProvince());
                    spnProvince.setSelection(provincePos);
                    if (isFirstSelectDistrict && userInfo.getDistrict() != null) {
                        String code = MappingUtils.getProvinceCode(FullFillInfoActivity.this, userInfo.getProvince());
                        int finalDistrictPos = MappingUtils.getDistrictIndex(this, code, userInfo.getDistrict());
                        isFirstSelectDistrict = false;
                        spnDistrict.postDelayed(() -> spnDistrict.setSelection(finalDistrictPos), 100);
                    }
                } else {
                    spnProvince.setSelection(provinceList.length - 1);
                }


            } catch (Exception ex) {

            }
            if (userInfo.getDate_of_birth() != null) {
                try {
                    SharedPreferences mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                    String deviceLanguage = mApplication.getReengAccountBusiness().getDeviceLanguage();
                    String mCurrentLanguageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
                    String dob = TimeHelper.convertTimeFromAPi(userInfo.getDate_of_birth(), mCurrentLanguageCode);
                    if (dob.length() > 0) {
                        edtDob.setText(dob);
                    } else {
                        edtDob.setText("");
                    }

                } catch (Exception ignored) {
                    edtDob.setText("");
                }

            }
        }
    }

    private void showDialogTimePicker() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        String dateChoseDefault;
        hideKeyboard();
//        if (TextUtils.isEmpty(mEdtBirthday.getText().toString())) {
//            dateChoseDefault = TimeHelper.formatTimeBirthday(TimeHelper.BIRTHDAY_DEFAULT_PICKER);
//        } else
//            dateChoseDefault = mEdtBirthday.getText().toString();
        if (TextUtils.isEmpty(edtDob.getText().toString())
                || "DD/MM/YYYY".equals(edtDob.getText().toString())) {
            dateChoseDefault = UserInfoBusiness.getDefaultDate();
        } else {
            if (!"en".equals(UserInfoBusiness.getCurrentLanguage(this, mApplication))) {
                dateChoseDefault = edtDob.getText().toString();
            } else {
                dateChoseDefault = TimeHelper.convertTimeEnToKh(edtDob.getText().toString());
            }
        }
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(this, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                if (!"en".equals(UserInfoBusiness.getCurrentLanguage(FullFillInfoActivity.this, mApplication))) {
                edtDob.setText(dateDesc);
                } else {
                    edtDob.setText(TimeHelper.convertKhToEn(dateDesc));
                }
//                edtDob.setTextColor(getResources().getColor(R.color.white));
            }
        }).textConfirm(getString(R.string.confirm_button)) //text of confirm button
                .textCancel(getString(R.string.cancel_button)) //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(getResources().getColor(R.color.color_of_cancel_button)) //color of cancel button
                .colorConfirm(getResources().getColor(R.color.color_of_confirm_button))//color of confirm button
                .dateChose(dateChoseDefault) // date chose when init popwindow
                .minYear(1900)
                .maxYear(mYear + 1) // max year in loop
                .build();
        pickerPopWin.showPopWin(this);
    }

    private void upDate() {
        if (TextUtils.isEmpty(edtFullName.getText().toString().trim())) {
            ToastUtils.showToast(this, getString(R.string.enter_the_full_name));
        } else if (TextUtils.isEmpty(edtDob.getText().toString())) {
            ToastUtils.showToast(this, getString(R.string.dob_is_required));
        } else if (TextUtils.isEmpty(edtIdNumber.getText().toString().trim())) {
            ToastUtils.showToast(this, getString(R.string.enter_id_number));
        } else if (TextUtils.isEmpty(edtCurrentAddress.getText().toString().trim())) {
            ToastUtils.showToast(this, getString(R.string.enter_the_dress));
        } else if (spnProvince.getSelectedItem().toString().equals(getString(R.string.hint_default_value))) {
            ToastUtils.showToast(this, getString(R.string.request_select_province));
        } else if (TextUtils.isEmpty(edtContact.getText().toString())) {
            ToastUtils.showToast(this, getString(R.string.request_select_contact));
        } else {
            MPConfirmInformationDialog confirmInformationDialog = new MPConfirmInformationDialog(this);
            confirmInformationDialog.setUserInfo(passDataDialog());
            confirmInformationDialog.show();
            confirmInformationDialog.setOnDismissListener(dialogInterface -> {
                if (!confirmInformationDialog.getBack()) {
                    NavigateActivityHelper.navigateToSetUpProfileClearTop(FullFillInfoActivity.this);
                    finish();
                }
            });
        }

    }

    private UserInfo passDataDialog() {
        String phoneNumber = currentUser.getPhone_number();
        String fullName = edtFullName.getText().toString().trim();
        String idNumber = edtIdNumber.getText().toString().trim();
        String contact = edtContact.getText().toString().trim();
        String dateOfBirth = TimeHelper.convertTimeToAPi(edtDob.getText().toString(),
                UserInfoBusiness.getCurrentLanguage(this, mApplication));
        android.util.Log.d(TAG, "onClick: " + dateOfBirth);
//            String email = edtEmail.getText().toString();
        int gender;

        if (getString(R.string.sex_male).equals(spnSex.getSelectedItem().toString())) {
            gender = 1;
        } else if (getString(R.string.sex_female).equals(spnSex.getSelectedItem().toString())) {
            gender = 2;
        } else {
            gender = 0;
        }

        String currentAddress = edtCurrentAddress.getText().toString().trim();
        String province = spnProvince.getSelectedItem().toString();
        String district = spnDistrict.getSelectedItem().toString();
        if (province.toLowerCase(Locale.ROOT)
                .equals(getString(R.string.hint_default_value).toLowerCase(Locale.ROOT))) {
            province = getString(R.string.empty_string);
            district = getString(R.string.empty_string);
        }

        UserInfo userInfo = userInfoBusiness.getUser();
        userInfo.setAddress(currentAddress);
        userInfo.setGender(gender);
        userInfo.setProvince(province);
        userInfo.setDistrict(district);
        userInfo.setPhone_number(phoneNumber);
        userInfo.setFull_name(fullName);
        userInfo.setIdentity_number(idNumber);
        userInfo.setDate_of_birth(dateOfBirth);
        userInfo.setContact(contact);
        userInfo.setAvatar(currentUser.getAvatar() == null ? "" : currentUser.getAvatar());
//        Gson gson = new Gson();
//        String json = gson.toJson(userInfo);
//        Bundle bundle = new Bundle();
//        bundle.putString(EnumUtils.OBJECT_KEY, json);
        return userInfo;
    }

    private void spanText() {
        setTextNotNull(mTvFullName, getString(R.string.full_name_information));
        setTextNotNull(mTvDob, getString(R.string.Date_of_Birth_information));
        setTextNotNull(mTvSex, getString(R.string.sex_infomation));
        setTextNotNull(mTvIDNumber, getString(R.string.id_number_information));
        setTextNotNull(mTvProvince, getString(R.string.province_city_infomation));
        setTextNotNull(mTvCurrentAddress, getString(R.string.current_address_information));
    }

    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener((v, event) -> {
                UserInfoBusiness.hideKeyboard(FullFillInfoActivity.this);
                return false;
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

}
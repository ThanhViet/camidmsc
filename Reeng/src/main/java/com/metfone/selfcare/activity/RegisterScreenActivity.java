package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.DeviceUtils;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.google.GoogleSignInHelper;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.SignIn;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.MethodLoginResponse;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.jivesoftware.smack.Connection;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.helper.google.GoogleSignInHelper.RC_SIGN_IN;
import static com.metfone.selfcare.util.EnumUtils.LOG_MODE;
import static com.metfone.selfcare.util.EnumUtils.PRE_LOG_MODE;
import static com.metfone.selfcare.util.EnumUtils.SINGIN;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

public class RegisterScreenActivity extends BaseSlidingFragmentActivity implements View.OnClickListener,
        FacebookHelper.OnFacebookListener, GoogleSignInHelper.OnGoogleListener, GoogleSignInHelper.OnGoogleSignOutListener {
    private static final String FULL_NAME = "fullname";
    private static final String GENDER = "gender";
    private static final String BIRTHDAY = "birthday";
    private static final String EMAIL = "email";
    private static final String METHOD = "method";
    private static final int FACEBOOK_METHOD = 1;
    private static final int GOOGLE_METHOD = 2;
    private static final String FACEBOOK_METHOD_CODE = "facebook";
    private static final String GOOGLE_METHOD_CODE = "google";
    private static final String TAG = RegisterScreenActivity.class.getSimpleName();
    CamIdTextView tvEnterPhone;
    CamIdTextView tvRegisterFacebook;
    CamIdTextView tvRegisterGoogle;
    CamIdTextView tvToRegisterAppleId;
    FrameLayout layoutFacebook;
    FrameLayout layoutGoogle;
    TextView tvOr;
    GoogleSignInHelper googleSignInHelper;
    ProgressBar pbLoading;
    SharedPref sharedPref;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    String userName, email, birthday;
    int gender;
    UserInfoBusiness userInfoBusiness;
    private ImageView ivBack;
    private CamIdTextView tvChangeScreenLogin, tvChangeScreenSighUp;
    private CamIdTextView tvTitleCamId, tvTitleFooterCamId;
    private FrameLayout llInputNumber;
    private ApplicationController mApplication;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;
    private KhHomeClient homeClient;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_register_screen);
        mApplication = (ApplicationController) getApplicationContext();
        llInputNumber = findViewById(R.id.llInputNumber);
        pbLoading = findViewById(R.id.pbLoading);
        tvRegisterFacebook = findViewById(R.id.tvToRegisterFacebook);
        tvChangeScreenLogin = findViewById(R.id.tvChangeScreenLogin);
        ivBack = findViewById(R.id.ivBack);
        tvChangeScreenSighUp = findViewById(R.id.tvChangeScreenSighUp);
        tvTitleCamId = findViewById(R.id.tvTitleCamId);
        tvTitleFooterCamId = findViewById(R.id.tvTitleFooterCamId);
        layoutFacebook = findViewById(R.id.layoutFacebook);
        layoutGoogle = findViewById(R.id.layoutGoogle);
        tvOr = findViewById(R.id.tv_Or);
        tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
        tvChangeScreenLogin.setText(getResources().getString(R.string.sign_up));
        tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
        llInputNumber.setOnClickListener(this);
        tvRegisterFacebook.setOnClickListener(this);
        tvChangeScreenLogin.setOnClickListener(this);
        tvChangeScreenSighUp.setOnClickListener(this);
        tvTitleCamId.setOnClickListener(this);
        tvRegisterGoogle = findViewById(R.id.tvToRegisterGoogle);
        googleSignInHelper = new GoogleSignInHelper(this, this);
        sharedPref = SharedPref.newInstance(this);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        tvRegisterGoogle.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        userInfoBusiness = new UserInfoBusiness(RegisterScreenActivity.this);
        homeClient = new KhHomeClient();
        if (SharedPrefs.getInstance().get(PREF_IS_LOGIN, Boolean.class)) {
            tvChangeScreenLogin.setVisibility(View.VISIBLE);
            tvChangeScreenSighUp.setVisibility(View.GONE);
            tvTitleCamId.setText(getResources().getString(R.string.title_camid_sign_up));
            tvTitleFooterCamId.setText(getResources().getString(R.string.already_have_an_account));
            tvChangeScreenLogin.setText(getResources().getString(R.string.log));
            homeClient.logApp(Constants.LOG_APP.SIGN_UP);
        } else {
            tvChangeScreenLogin.setVisibility(View.GONE);
            tvChangeScreenSighUp.setVisibility(View.VISIBLE);
            tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
            tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
            tvChangeScreenSighUp.setText(getResources().getString(R.string.sign_up));
            homeClient.logApp(Constants.LOG_APP.LOG_IN);
        }
        setColorStatusBar(R.color.white);
//        handleGetIntent();
        getMethodsLogin();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llInputNumber:
                userInfoBusiness.putLogInMode(EnumUtils.LoginModeTypeDef.PHONE_NUMBER);
                if (tvChangeScreenLogin.getVisibility() == View.GONE) {
                    LOG_MODE = EnumUtils.LogModeTypeDef.LOGIN;
                } else {
                    LOG_MODE = EnumUtils.LogModeTypeDef.SIGN_UP;
                }
                PRE_LOG_MODE = LOG_MODE;
                NavigateActivityHelper.navigateToLoginActivity(this, true);
                break;
            case R.id.tvToRegisterFacebook:
                userInfoBusiness.putLogInMode(EnumUtils.LoginModeTypeDef.FACEBOOK);
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    FacebookHelper facebookHelper = new FacebookHelper(this);
                    facebookHelper.getProfile(getCallbackManager(), this, FacebookHelper.PendingAction.GET_USER_INFO);
                }

                break;
            case R.id.tvChangeScreenLogin:
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                tvChangeScreenLogin.setVisibility(View.GONE);
                tvChangeScreenSighUp.setVisibility(View.VISIBLE);
                tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
                tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
                tvChangeScreenSighUp.setText(getResources().getString(R.string.sign_up));
                homeClient.logApp(Constants.LOG_APP.LOG_IN);
                break;
            case R.id.tvChangeScreenSighUp:
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
                tvChangeScreenLogin.setVisibility(View.VISIBLE);
                tvChangeScreenSighUp.setVisibility(View.GONE);
                tvTitleCamId.setText(getResources().getString(R.string.title_camid_sign_up));
                tvTitleFooterCamId.setText(getResources().getString(R.string.already_have_an_account));
                tvChangeScreenLogin.setText(getResources().getString(R.string.log));
                homeClient.logApp(Constants.LOG_APP.SIGN_UP);
                break;
            case R.id.tvToRegisterGoogle:
                userInfoBusiness.putLogInMode(EnumUtils.LoginModeTypeDef.GOOGLE);
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    if (googleSignInHelper.isSignedIn()) {
                        googleSignInHelper.signOut(this);
                        return;
                    }
                    signIn();
                }
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
        Log.d(TAG, "name is " + name);
//        mApplication.getReengAccountBusiness().getCurrentAccount().setFacebookId(userId);
        this.userName = name;
        this.email = email;
        this.birthday = birthDayStr;
        this.gender = gender;
        signIn(FACEBOOK_METHOD);
    }

    private void signIn() {
        googleSignInHelper.getProfile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            googleSignInHelper.handleSignInIntent(data);
        }
    }

    @Override
    public void onGetInfoGoogleFinish(String id, String userName, String email, Uri avatarUri) {
//        mApplication.getReengAccountBusiness().getCurrentAccount().setGoogleId(id);
        this.userName = userName;
        this.email = email;
        signIn(GOOGLE_METHOD);
    }

    /**
     * Xử lý trường hợp gọi từ màn hình MPPhoneExistFragment
     * isShowLogin = true khi người dùng tap "Login" button
     * false khi người dùng tap "Create new CamID account" button
     */
    private void handleGetIntent() {
        Intent intent = getIntent();
        boolean isShowLogin = intent.getBooleanExtra(Constants.PARAM_IS_LOGIN, true);
        if (isShowLogin) {
            tvChangeScreenLogin.setVisibility(View.GONE);
            tvChangeScreenSighUp.setVisibility(View.VISIBLE);
            tvTitleCamId.setText(getResources().getString(R.string.title_camid_log_in));
            tvTitleFooterCamId.setText(getResources().getString(R.string.new_to_cam_id));
            tvChangeScreenSighUp.setText(getResources().getString(R.string.sign_up));
        } else {
            tvChangeScreenLogin.setVisibility(View.VISIBLE);
            tvChangeScreenSighUp.setVisibility(View.GONE);
            tvTitleCamId.setText(getResources().getString(R.string.title_camid_sign_up));
            tvTitleFooterCamId.setText(getResources().getString(R.string.already_have_an_account));
            tvChangeScreenLogin.setText(getResources().getString(R.string.log));
        }
    }

    private void signIn(int currentMethod) {
        pbLoading.setVisibility(View.VISIBLE);
        SignIn signIn = new SignIn();
        switch (currentMethod) {
            case FACEBOOK_METHOD:
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                signIn.setSocialToken(accessToken.getToken());
                Log.d(TAG, "Facebook Access Token is " + accessToken.getToken());
                signIn.setType("facebook");
                break;
            case GOOGLE_METHOD:
                GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);
                signIn.setSocialToken(googleSignInAccount.getIdToken());
                Log.d(TAG, "Google token id is " + googleSignInAccount.getIdToken());
                signIn.setType("google");
                break;
        }

        SignInRequest signInRequest = new SignInRequest(signIn, "", "", "", "");
        apiService.signIn(DeviceUtils.getUniqueDeviceId(),signInRequest).enqueue(new Callback<SignInResponse>() {

            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                pbLoading.setVisibility(View.GONE);
                SignInResponse signInResponse = response.body();
                if (signInResponse != null) {
                    if (signInResponse.getCode().equals("00")) {
                        String token = "Bearer " + signInResponse.getData().getAccess_token();
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefresh_token());
                        if (signInResponse.getData() != null && signInResponse.getData().getAuth() != null
                                && SINGIN.equals(signInResponse.getData().getAuth().toUpperCase())) {
                            //case sign in social
                            getUserInformation(currentMethod);
                        } else {
                            //case sign up social
                            if (currentMethod == FACEBOOK_METHOD) {
                                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                                Log.d(TAG, "Facebook Access Token is " + accessToken.getToken());
                                Intent intent = new Intent(RegisterScreenActivity.this, CreateYourAccountActivity.class);
                                intent.putExtra(FULL_NAME, userName);
                                intent.putExtra(EMAIL, email);
                                intent.putExtra(GENDER, gender);
                                intent.putExtra(BIRTHDAY, birthday);
                                intent.putExtra(METHOD, FACEBOOK_METHOD);
                                startActivity(intent);
                            } else if (currentMethod == GOOGLE_METHOD) {
                                Intent intent = new Intent(RegisterScreenActivity.this, CreateYourAccountActivity.class);
                                intent.putExtra(FULL_NAME, userName);
                                intent.putExtra(EMAIL, email);
                                intent.putExtra(METHOD, GOOGLE_METHOD);
                                startActivity(intent);
                            }
                        }
                        //      NavigateActivityHelper.navigateToHomeScreenActivity(RegisterScreenActivity.this, true,true);
                    } else {
                        if (currentMethod == FACEBOOK_METHOD) {
                            ToastUtils.showToast(RegisterScreenActivity.this, "Sign Up Facebook Failed ");
                        } else if (currentMethod == GOOGLE_METHOD) {
                            ToastUtils.showToast(RegisterScreenActivity.this, "Sign Up Google Failed ");
                        } else {
                            ToastUtils.showToast(RegisterScreenActivity.this, "Sign Up Failed ");
                        }
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        ToastUtils.showToast(RegisterScreenActivity.this, "Sign In Failed");
//                        showDialog(jObjError.getJSONObject("error").getString("message"));
                    } catch (Exception e) {
                        ToastUtils.showToast(RegisterScreenActivity.this, e.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                ToastUtils.showToast(RegisterScreenActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void getUserInformation(int currentMethod) {
        pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                pbLoading.setVisibility(View.GONE);
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        FireBaseHelper.getInstance(ApplicationController.self()).checkServiceAndRegister(true);
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
                        mCurrentRegionCode = "KH";
                        //case have an account
                        if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                            if (userInfo.getPhone_number().startsWith("0")) {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number().substring(1);
                            } else {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number();
                            }
                            String originToken = token.substring(7);
                            generateOldMochaOTP(originToken);
                        } else {
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(RegisterScreenActivity.this, false, true);
                        }
                    } else {
                        ToastUtils.showToast(RegisterScreenActivity.this, response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(RegisterScreenActivity.this, "Get user information fail");
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(RegisterScreenActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void generateOldMochaOTP(String token) {
        pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(RegisterScreenActivity.this);
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(RegisterScreenActivity.this).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(RegisterScreenActivity.this, response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(RegisterScreenActivity.this, "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(RegisterScreenActivity.this, error.getMessage());
            }
        });

    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new RegisterScreenActivity.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    @Override
    public void onSignOutGoogleFinish() {
        googleSignInHelper = new GoogleSignInHelper(this, this);
        signIn();
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);
            pbLoading.setVisibility(View.GONE);

            try {
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
                    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(RegisterScreenActivity.this);
                    UserInfo userInfo = userInfoBusiness.getUser();
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    reengAccount.setName(userInfo.getFull_name());
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
//                    NavigateActivityHelper.navigateToHomeScreenActivity(RegisterScreenActivity.this, false, true);
                    RestoreManager.setRestoring(true);
                    userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                        @Override
                        public void onStartDownload() {

                        }

                        @Override
                        public void onDownloadProgress(int percent) {

                        }

                        @Override
                        public void onDownloadComplete() {

                        }

                        @Override
                        public void onDowloadFail(String message) {

                        }

                        @Override
                        public void onStartRestore() {

                        }

                        @Override
                        public void onRestoreProgress(int percent) {

                        }

                        @Override
                        public void onRestoreComplete(int messageCount, int threadMessageCount) {
                            try {
                                SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                                if (pref != null) {
                                    pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                                    com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                                }
                            } catch (Exception e) {
                            }
                            RestoreManager.setRestoring(false);
                            mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                            mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                            NavigateActivityHelper.navigateToHomeScreenActivity(RegisterScreenActivity.this, false, true);
                        }

                        @Override
                        public void onRestoreFail(String message) {
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(RegisterScreenActivity.this, false, true);
                        }
                    }, RegisterScreenActivity.this);
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    ToastUtils.showToast(RegisterScreenActivity.this, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
            }
        }
    }

    private void getMethodsLogin() {
        showLoadingDialog("", "");
        apiService.getMethodsLogin().enqueue(new Callback<MethodLoginResponse>() {
            @Override
            public void onResponse(Call<MethodLoginResponse> call, Response<MethodLoginResponse> response) {
                if (response != null && response.body() != null && response.body().getData() != null) {
                    List<MethodLoginResponse.MethodLogin> methods = response.body().getData().getMethodsLogin();
                    if (methods != null && methods.size() > 0) {
                        for (MethodLoginResponse.MethodLogin methodLogin : methods) {
                            if (methodLogin.getCode() != null && methodLogin.getCode().equals(FACEBOOK_METHOD_CODE)) {
                                if (methodLogin.getState().equals(MethodLoginResponse.MethodLogin.ACTIVE)) {
                                    layoutFacebook.setVisibility(View.VISIBLE);
                                    tvOr.setVisibility(View.VISIBLE);
                                } else {
                                    layoutFacebook.setVisibility(View.GONE);
                                }
                            } else if (methodLogin.getCode() != null && methodLogin.getCode().equals(GOOGLE_METHOD_CODE)) {
                                if (methodLogin.getState().equals(MethodLoginResponse.MethodLogin.ACTIVE)) {
                                    layoutGoogle.setVisibility(View.VISIBLE);
                                    tvOr.setVisibility(View.VISIBLE);
                                } else {
                                    layoutGoogle.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
                hideLoadingDialog();
            }

            @Override
            public void onFailure(Call<MethodLoginResponse> call, Throwable t) {
                hideLoadingDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DeepLinkHelper.uriDeepLink = null;
        HomePagerFragment.isClickTabGame = false;
        super.onBackPressed();
    }
}


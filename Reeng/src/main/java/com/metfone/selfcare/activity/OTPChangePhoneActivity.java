package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.icu.util.CurrencyAmount;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Selection;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.SPUtils;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.esport.common.Constant;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.fragment.login.EnterPhoneNumberFragment;
import com.metfone.selfcare.fragment.login.OTPLoginFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.UpdateLoginInfo;
import com.metfone.selfcare.model.account.UpdateLoginRequest;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.network.camid.response.RefreshTokenResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;

public class OTPChangePhoneActivity extends BaseSlidingFragmentActivity implements /*SmsReceiverListener,*/ ClickListener.IconListener, MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String TAG = OTPLoginFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PHONE_NUMBER_PARAM = "param1";
    private static final String REGION_CODE_PARAM = "param2";
    private static final String CODE_PARAM = "code";
    private static final String PASSWORD = "password";
    //    private CountDownTimer countDownTimer;
    private static final int TIME_DEFAULT = 60000;
    private  static final int REQ_USER_CONSENT = 6;
    private static boolean handlerflag = false;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvPhoneNumber)
    CamIdTextView tvPhoneNumber;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.tvNotReceivedOtp)
    CamIdTextView tvNotReceivedOtp;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.tv_error)
    AppCompatTextView tvError;
    @BindView(R.id.tvEnterNumber)
    CamIdTextView tvEnterNumber;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    @Nullable
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    TextView btnPassword;
    int remain = 0;

    /*@BindView(R.id.btnContinue)
    RoundTextView btnContinue;*/
    Unbinder unbinder;
    String countDownString;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private int codeGenOtp;
    private String password = "";
    private OTPLoginFragment.OnFragmentInteractionListener mListener;
    private EnterPhoneNumberFragment.OnFragmentInteractionListener fragmentInteractionListener;
    private ApplicationController mApplication;
    private Resources mRes;
    private boolean isPhoneExisted = false;
    private ClickListener.IconListener mClickHandler;
    //------------Variable Views-------------------
    private Handler mHandler;
    private int START_TEXT = 85;
    private int END_TEXT = 0;
    private boolean isSaveInstanceState = false;
    private boolean isLoginDone = false;
    private long countDownTimer;
    private String phoneNumber;
    UserInfoBusiness userInfoBusiness;
    UserInfo currentUser;
    BaseMPConfirmDialog baseMPConfirmDialog;
    BaseMPSuccessDialog baseMPSuccessDialog;
    int checkPhoneCode = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_change_phone);
        mHandler = new Handler();
        handlerflag = true;
        unbinder = ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplicationContext();
       changeStatusBar(getResources().getColor(R.color.color_status_fullfill));
        findComponentViews();
        if (!NetworkHelper.isConnectInternet(this)) {
            showError(getString(R.string.error_internet_disconnect), null);
        }else {
            setViewListeners();
        }
        getData();
        tvEnterNumberDes.setText(fomatString());
        mApplication.registerSmsOTPObserver();
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.WHITE);
    }

    private void getData() {
        userInfoBusiness = new UserInfoBusiness(this);
        currentUser = userInfoBusiness.getUser();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        phoneNumber = getIntent().getStringExtra(EnumUtils.PHONE_NUMBER_KEY);
        checkPhoneCode = getIntent().getIntExtra(EnumUtils.OBJECT_KEY, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        etOtp.requestFocus();
        UIUtil.showKeyboard(this,etOtp);
        mClickHandler = this;
        if (isSaveInstanceState) {          //Vao sau khi da saveinstance
            if (isLoginDone) {              //Da dang nhap thanh cong
                if (mListener != null) {    //Chuyen view

                    mListener.navigateToNextScreen();
                }
            }
        }

        isSaveInstanceState = false;
   //     etOtp.setText("");
        if (!handlerflag && remain > 0) {
            startCountDown(remain * 1000);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mClickHandler = null;
        handlerflag = false;
//        SmsReceiver.removeSMSReceivedListener(this);
        InputMethodUtils.hideSoftKeyboard(etOtp, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, mCurrentNumberJid);
        outState.putString(REGION_CODE_PARAM, mCurrentRegionCode);
        outState.putInt(CODE_PARAM, codeGenOtp);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }


    private void findComponentViews() {
        etOtp.requestFocus();
//            InputMethodUtils.hideKeyboardWhenTouch(rootView, mLoginActivity);
        //
        PhoneNumberUtil phoneUtil = mApplication.getPhoneUtil();
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(phoneUtil, mCurrentNumberJid, mCurrentRegionCode);
        StringBuilder sb = new StringBuilder();

        phoneNumber = getIntent().getStringExtra(EnumUtils.PHONE_NUMBER_KEY);
        if (PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil,
                phoneNumberProtocol)) {
            sb.append(" (+");
            sb.append(String.valueOf(phoneNumberProtocol.getCountryCode())).append(") ");
            sb.append(String.valueOf(phoneNumberProtocol.getNationalNumber()));
            tvPhoneNumber.setText(sb.toString());
        } else {
            tvPhoneNumber.setText(mCurrentNumberJid);
        }
        /*if (codeGenOtp == 200) {
            tvDescInfo.setText(mRes.getString(R.string.login_header_notice_2));
        } else if (codeGenOtp == 201) {
            tvDescInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_201), mCurrentNumberJid));
        } else if (codeGenOtp == 202) {
            tvDescInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_202), mCurrentNumberJid));
        }*/

    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        generateOtpByServer();
        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
//                checkPhone();
                updateLogin(phoneNumber, str.toString());
            }
        });
    }

//    private void checkPhone() {
//        showLoadingDialog("", R.string.processing);
//        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
//        CheckPhoneInfo checkPhoneInfo = new CheckPhoneInfo(phoneNumber);
//        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest(checkPhoneInfo, "", "", "", "");
//        apiService.checkPhone(checkPhoneRequest).enqueue(new Callback<BaseResponse<String>>() {
//            @Override
//            public void onResponse(Call<BaseResponse<String>> call, retrofit2.Response<BaseResponse<String>> response) {
//                 pbLoading.setVisibility(View.GONE);
//                if (response.body() != null) {
//                    if (response.body().getCode().equals("00") || response.body().getCode().equals("42")) {
//                        checkPhoneCode = 42;
//                        //case exist in camID
//                        baseMPConfirmDialog = new BaseMPConfirmDialog(R.drawable.img_dialog_2, R.string.m_p_dialog_combine_phone_title,
//                                R.string.m_p_dialog_combine_phone_content, R.string.m_p_dialog_combine_phone_top_btn, R.string.m_p_dialog_combine_phone_bottom_btn,
//                                view -> {
//                                    //letdoit
//                                    updateLogin(phoneNumber, etOtp.getText().toString());
//                                    if (baseMPConfirmDialog != null) {
//                                        baseMPConfirmDialog.dismiss();
//                                    }
//                                }, view -> {
//                            //back
//
//                            if (baseMPConfirmDialog != null) {
//                                Intent returnIntent = new Intent();
//                                returnIntent.putExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
//                                setResult(Activity.RESULT_OK, returnIntent);
//                                setResult(RESULT_OK);
//                                finish();
//                                baseMPConfirmDialog.dismiss();
//                            }
//                        });
//                        baseMPConfirmDialog.show(getSupportFragmentManager(), null);
//                        //case full service
//                    } else {
//                        updateLogin(phoneNumber, etOtp.getText().toString());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
//                 pbLoading.setVisibility(View.GONE);
//                ToastUtils.showToast(OTPChangePhoneActivity.this, t.getMessage());
//            }
//        });
//    }

    private void updateLogin(String phoneNumber, String otp) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        UpdateLoginInfo updateLoginInfo = new UpdateLoginInfo(phoneNumber, otp);
        UpdateLoginRequest updateLoginRequest = new UpdateLoginRequest(updateLoginInfo, "", "", "", "");
        apiService.updateLogin(token, updateLoginRequest).enqueue(new Callback<BaseResponse<BaseUserResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseUserResponseData>> call, Response<BaseResponse<BaseUserResponseData>> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    BaseResponse<BaseUserResponseData> dataBaseResponse = response.body();
                    if ("00".equals(dataBaseResponse.getCode())) {
                        UserInfo userInfo = dataBaseResponse.getData().getUser();
                        userInfoBusiness.setServiceList(dataBaseResponse.getData().getServices());
                        userInfoBusiness.setIdentifyProviderList(dataBaseResponse.getData().getIdentifyProviders());
                        mCurrentRegionCode = "KH";
                        if (userInfo != null) {
                            userInfoBusiness.setUser(userInfo);
                            ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                              reengAccount.setToken(token);
                            if (reengAccount != null){
                                reengAccount.setName(userInfo.getFull_name());
                                reengAccount.setEmail(userInfo.getEmail());
                                reengAccount.setBirthday(userInfo.getDate_of_birth());
                                reengAccount.setAddress(userInfo.getAddress());
                                mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                            }
                            if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                                 if (userInfo.getPhone_number().startsWith("0")) {
                            mCurrentNumberJid = "+855"+userInfo.getPhone_number().substring(1);
                        } else {
                            mCurrentNumberJid = "+855"+userInfo.getPhone_number();
                        }
                                SPUtils.getInstance().put(Constant.PREF_USERNAME_ACCOUNT_ESPORT, phoneNumber);
                                SPUtils.getInstance().put(Constant.PREF_PASSWORD_ACCOUNT_ESPORT, otp);
                            } else {
                                ToastUtils.showToast(OTPChangePhoneActivity.this, "Phone number null -> Login old mocha failed!");
                            }
                        }
                        String refreshToken = SharedPrefs.getInstance().get(PREF_REFRESH_TOKEN, String.class);
                        refreshToken(refreshToken);

                    } else if (response.body().getCode().equals("61")){
                        baseMPSuccessDialog = new BaseMPSuccessDialog(OTPChangePhoneActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry_full_service), getString(R.string.content_error_full_service_dialog), false);
                        baseMPSuccessDialog.show();
                        getSupportFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    OTPChangePhoneActivity.this.finish();
                                }
                            });
                        }
                    }
                    //case camid social
                 else if (response.body().getCode().equals("49")){
                    baseMPSuccessDialog = new BaseMPSuccessDialog(OTPChangePhoneActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), String.format(getString(R.string.content_error_social_combine), phoneNumber.substring(0, 2) + " " + phoneNumber.substring(2)), false);
                    if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
                        baseMPSuccessDialog.setBgWhite(true);
                    }
                    baseMPSuccessDialog.show();
                    getSupportFragmentManager().executePendingTransactions();
                    if (baseMPSuccessDialog != null) {
                        baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                OTPChangePhoneActivity.this.finish();
                            }
                        });
                    }

                    //21,24,41,43,45,64: Call api add login và hiển thị popup success
                }else{
                        baseMPSuccessDialog = new BaseMPSuccessDialog(OTPChangePhoneActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), dataBaseResponse.getMessage(), false);
                        baseMPSuccessDialog.show();
                        if (!"1".equals(response.body().getCode())){
                            getSupportFragmentManager().executePendingTransactions();
                            if (baseMPSuccessDialog != null) {
                                baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        OTPChangePhoneActivity.this.finish();
                                    }
                                });
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseUserResponseData>> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(OTPChangePhoneActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void setResendPassListener() {
        tvResendOtp.setText(getString(R.string.mc_resend_otp));
        tvResendOtp.setTextColor(ContextCompat.getColor(this, R.color.resend));
        tvResendOtp.setOnClickListener(view -> showDialogConfirmResendCode());
    }

    private void startCountDown(int time) {
        tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    setResendPassListener();
                } else if(remain > 0){
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                }else{remain=0;}
                String countDownString = getString(R.string.mc_resend_otp);
                tvResendOtp.setText(countDownString);
                tvTimeResendOtp.setText(remain +" "+ getString(R.string.text_seconds));
            }
        }, 1000);
    }

    public void generateOtpByServer() {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.generateOtp(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                startSmsUserConsent();
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                android.util.Log.d("TAG", "onResponse: " + response.body());
            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                android.util.Log.d("TAG", "onError: " + error.getMessage());
            }
        });


    }

    private String fomatString() {
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        String mPhoneNumber = phoneNumber.substring(0,3)+" "+ phoneNumber.substring(3);
        fmt.format(getString(R.string.text_otp_has_been_sent_to), mPhoneNumber);
        return sbuf.toString();
    }


    private void showDialogConfirmResendCode() {
        String labelOK = getString(R.string.ok);
        String labelCancel = getString(R.string.cancel);
        String title = getString(R.string.title_confirm_resend_code);
        String msg = String.format(getString(R.string.msg_confirm_resend_code), phoneNumber.substring(0,3)+" "+phoneNumber.substring(3));
        PopupHelper.getInstance().showDialogConfirm(this, title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    private void setPasswordEditText(String pass) {
        etOtp.setText(pass);
        Selection.setSelection(etOtp.getText(), etOtp.getText().length());
    }

    @OnClick({R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }

    //XU ly phan sms otp
    @Override
    public void onOTPReceived(Intent intent) {
        startActivityForResult(intent, REQ_USER_CONSENT);
    }

    @Override
    public void onOTPTimeOut() {
    }
    private void refreshToken(String token){
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.refreshToken(token, new ApiCallback<RefreshTokenResponse>() {
            @Override
            public void onResponse(Response<RefreshTokenResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() == null) {

                    Toast.makeText(OTPChangePhoneActivity.this, "Refresh Token Failed", Toast.LENGTH_LONG).show();
                } else {
                    if (response.body().getCode().equals("00")) {
                        String token = "Bearer " + response.body().getData().getAccessToken();
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefreshToken());
                        generateOldMochaOTP(response.body().getData().getAccessToken());

                    } else {
                        Toast.makeText(OTPChangePhoneActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Toast.makeText(OTPChangePhoneActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    private void generateOldMochaOTP(String token) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
         String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(OTPChangePhoneActivity.this);
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(OTPChangePhoneActivity.this).detectSubscription(json);
                   doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(OTPChangePhoneActivity.this, response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(OTPChangePhoneActivity.this, "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(OTPChangePhoneActivity.this, error.getMessage());
            }
        });

    }
    
    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new OTPChangePhoneActivity.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);

            try {
//                mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    //navigate next
                    String titleDialog = getString(R.string.successfully);
                    if (checkPhoneCode == 42){
                        titleDialog = getString(R.string.title_successfullly_combination);
                    }
                    baseMPSuccessDialog = new BaseMPSuccessDialog(OTPChangePhoneActivity.this, R.drawable.img_dialog_1, titleDialog, String.format(getString(R.string.content_success_change_), phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3)));
                    baseMPSuccessDialog.show();
                    getSupportFragmentManager().executePendingTransactions();
                    if (baseMPSuccessDialog != null) {
                        baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.DONE);
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                        });
                    }

//                    String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
//                    String otp = getArguments().getString(EnumUtils.OTP_KEY);
//                    String pass = edtPassWord.getEditText().getText().toString();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
//                    if (tvError != null) {
//                        tvError.setVisibility(View.GONE);
//                    }
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
//                    showError(responseCode.getDescription(), null);
//                    if (tvError != null) {
//                        tvError.setVisibility(View.VISIBLE);
//                    }
                    ToastUtils.showToast(OTPChangePhoneActivity.this, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == RESULT_OK && data != null) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (message != null) {
                    getOtpFromMessage(message);
                }

            }

        }
    }
    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(this);

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                Log.d(TAG,"Success");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,"Fail");

            }
        });
    }
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            etOtp.setText(matcher.group(0));
        }
    }
}
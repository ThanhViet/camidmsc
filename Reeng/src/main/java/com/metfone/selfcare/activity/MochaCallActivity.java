package com.metfone.selfcare.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.util.ImageUtils;
import com.spotxchange.internal.utility.Text;
import com.stringee.StringeeCallListener;
import com.stringee.StringeeStream;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.CallBusiness;
import com.metfone.selfcare.business.CallHistoryHelper;
import com.metfone.selfcare.business.CallRtcHelper;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.helper.call.CallUIHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.listeners.CallStateInterface;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.LoadingTextView;
import com.metfone.selfcare.ui.RippleActionCall;
import com.metfone.selfcare.ui.dialog.DialogRating;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.model.CallData;
import org.jivesoftware.smack.packet.ReengCallPacket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.IS_REQUEST_PERMISSION_FOR_CALL;

//import com.metfone.selfcare.helper.call.AppRTCAudioManager;

/**
 * Created by thanhnt72 on 7/25/2016.
 */
public class MochaCallActivity extends BaseSlidingFragmentActivity implements CallStateInterface,
        View.OnClickListener, SensorEventListener,
        ClickListener.IconListener, PermissionHelper.ListenerGotoSetting {
    private static final String TAG = MochaCallActivity.class.getSimpleName();
    private static final long AUTO_HIDE_TIME = 5000;
    private static final int ACTION_CHAT = 1;
    private int countBandwidthZero = 0;

    private ApplicationController mApplication;
    private Handler mHandler = new Handler();
    private ApplicationStateManager mAppStateManager;
    private boolean addFlagDim = false;
    private CallData sdpCallData;
    private String userNumber;

    private CallBusiness mCallBusiness;
    private ContactBusiness mContactBusiness;
    private ReengAccountBusiness mReengAccountBusiness;
    private MessageBusiness mMessageBusiness;
    private Resources mRes;
    private String jidFriend, rawNumber;
    private int actionClick = -1, callType;
    private int callTypeRating;
    private CardView mViewQualityAndState, mViewCallOutHeader;
    private View mViewAnswer, mViewMore, mViewAvatarName, mViewAnswerAudio2Video;
    private FrameLayout localRender, remoteRender;
    private ImageView mImgAvatar;
    private LoadingTextView mTvwCallState, mTvwCallStateNetwork, mTvwCallOutLabel;
    private TextView mTvwUserNumber, mTvwCallStrangerLabel, mTvwDuration, mTvCallOut;
    private EllipsisTextView mTvwName;
    private LinearLayout mLlActionAudio2Video, mLlActionVideoEnable, mLlActionSpeaker, mLlActionMute, mLlActionChat,
            mLlSwitchCamera, mLlOnlyAudio;
    private ImageButton mImgEndCall;
    private ImageView mImgMute, mImgSpeaker, mImgAvatarBlur, iconActionCall;
    private ImageView mImgAnswerIcon, mImgVideoEnable, mImgAudio2Video, mImgAnswerAduio2Video, mImgRejectAudio2Video;
    private RippleActionCall mRippleAnswer, mRippleReject;
    private View mViewBlack;
    private SensorManager sensorManager;
    private Sensor proximity;
    private PowerManager pm;
    private PowerManager.WakeLock proximityWakeLock;
    private PowerManager.WakeLock wakeLock;
    private RelativeLayout bg_call;
    private static final String WAKE_LOG_TAG = "Mochacall: wake log";

    private CountDownTimer mTimer;
    private Bitmap bmBlurDefault;

    private DialogRating dialogRating;
    private boolean isRequesting = false;
    private long countDownTimer;
    private static final long TIME_SHOW_RATING = 15000;
    private Handler mHandlerShowRating = new Handler();
    private long timeZeroBw2EndCall = 0;
    private boolean callVideo;

//    private AppRTCAudioManager audioManager = null;

    private boolean isAcceptFromNotification;
    private boolean isAllowPermissionCall = true;
    private boolean isRecentGotoSettingPermission = false;
    private boolean isShowFromNewIntent;
    private boolean isRecentRequestPermission;
    private boolean isAnsweredCall;
    private AccountRankDTO accountRankDTO;
    private UserInfo userInfo;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            );
        }
        sendBroadcast(new Intent(Constants.TabVideo.PAUSE_VIDEO_SERVICE));
        setContentView(R.layout.activity_mocha_call);
        initBusiness();
        setFlagWindow();
        initSensor();
        findComponentViews();
        CallBusiness.addCallStateListener(this);
        getData();
        setListener();
        drawDetail();
        trackingScreen(TAG);
        CallUIHelper.timeDisConnect = 0;
        /*audioManager = AppRTCAudioManager.create(getApplicationContext());
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        android.util.Log.d(TAG, "Starting the audio manager...");
        audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
            // This method will be called each time the number of available audio
            // devices has changed.
            @Override
            public void onAudioDeviceChanged(
                    AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
                onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
            }
        });*/
    }

    /*private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
        android.util.Log.d(TAG, "onAudioManagerDevicesChanged: " + availableDevices + ", "
                + "selected: " + device);
        // TODO(henrika): add callback handler.
    }*/

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent");
        if (intent != null && !isAnsweredCall) {
            isShowFromNewIntent = true;
            isAcceptFromNotification = intent.getExtras().getBoolean(CallConstant.IS_ACCEPT_CALL, false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !Settings.canDrawOverlays(mApplication.getApplicationContext())) {
                if (isAcceptFromNotification && mCallBusiness != null) {
                    if (!isRecentRequestPermission) {
                        if (PermissionHelper.checkPermissionAndShowDialogPermission(this, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO,
                                new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA}, this)) {
                            answerFromNotify();
                        }
                    } else {
                        if (checkPermissionCall()) {
                            answerFromNotify();
                        }
                    }
                }
            } else {
                if (isAcceptFromNotification && mCallBusiness != null) {
                    if (checkPermissionCall()) {
                        answerFromNotify();
                    }
                }
            }
        }
    }

    private void initBusiness() {
        this.mApplication = (ApplicationController) getApplicationContext();
        mApplication.getCallBusiness().setExistCallActivity(true);
        this.mAppStateManager = mApplication.getAppStateManager();
        this.mRes = mApplication.getResources();
        this.mCallBusiness = mApplication.getCallBusiness();
        this.mContactBusiness = mApplication.getContactBusiness();
        this.mReengAccountBusiness = mApplication.getReengAccountBusiness();
        this.mMessageBusiness = mApplication.getMessageBusiness();
        Bitmap defaultBitmap = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
        defaultBitmap.eraseColor(getResources().getColor(R.color.v5_background_call_default));
        bmBlurDefault = defaultBitmap;
    }

    @SuppressLint("InlinedApi")
    private void initSensor() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        proximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        proximityWakeLock = pm.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, WAKE_LOG_TAG);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                WAKE_LOG_TAG);
    }

    @SuppressWarnings("deprecation")
    private void setFlagWindow() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (mAppStateManager.isScreenLocker() || !mAppStateManager.isScreenOn()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND |
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            addFlagDim = true;
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            addFlagDim = false;
        }
        if (Version.hasKitKat()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @SuppressWarnings("deprecation")
    private void clearFlag() {
        if (addFlagDim) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        }
        if (Version.hasKitKat()) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void finishActivity() {
        Log.i(TAG, "finishActivity");
        remoteRender.setVisibility(View.GONE);
        localRender.setVisibility(View.GONE);
        mViewBlack.setVisibility(View.GONE);
        mViewBlack.setBackgroundColor(ContextCompat.getColor(mApplication, R.color.bg_profile_chat));
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        jidFriend = bundle.getString(CallConstant.JIDNUMBER);
        /*String friendPrefix = PrefixChangeNumberHelper.getInstant(mApplication).convertNewPrefix(jidFriend);
        if (friendPrefix != null) jidFriend = friendPrefix;*/
        callType = bundle.getInt(CallConstant.TYPE_FRAGMENT);
        callVideo = mCallBusiness.isVideoCall();

        callTypeRating = CallConstant.CALL_TYPE.CALL_NONE;
        sdpCallData = bundle.getParcelable(CallConstant.SDP_CALL_DATA);
        rawNumber = PhoneNumberHelper.getInstant().getRawNumber(mApplication, jidFriend);
        boolean isFirstStart = bundle.getBoolean(CallConstant.FIRST_START_ACTIVITY, false);
        isAcceptFromNotification = bundle.getBoolean(CallConstant.IS_ACCEPT_CALL, false);
        Log.i(TAG, "inten---- jid: " + jidFriend + " fragment: " + callType);
        String avatarSmallUrl = null;

        if (isFirstStart) {
            if (mCallBusiness != null) {
                if (callType == CallConstant.TYPE_CALLEE) {
                    mCallBusiness.dismissHeadUpNotification();
                }
            }
            if (!mApplication.getPref().getBoolean(IS_REQUEST_PERMISSION_FOR_CALL, false)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA},
                        Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO);
                isRecentRequestPermission = true;
            } else {
                isRecentRequestPermission = true;
                if (isAllowPermissionCall = PermissionHelper.checkPermissionAndShowDialogPermission(this, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO,
                        new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA}, this)) {
                    isRecentRequestPermission = true;
                    mApplication.getCallBusiness().handleAllowedPermissions(callType, jidFriend, sdpCallData);
                    answerFromNotify();
                } else {

                }
            }
            mApplication.getPref().edit().putBoolean(IS_REQUEST_PERMISSION_FOR_CALL, true).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, proximity, SensorManager.SENSOR_DELAY_NORMAL);
        if (!mApplication.getCallBusiness().isExistCall()) {
            runOnUiThread(() -> {
//                    ReengNotificationManager.clearMessagesNotification(mApplication, Constants.NOTIFICATION.NOTIFY_CALL);
                mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_CALL);
                finish();
            });
        } else {
            drawDetailCall();
            if (localRender.getVisibility() == View.VISIBLE) {
                localRender.removeAllViews();
                addLocalSurface();
            }
            if (!isAllowPermissionCall) {
                if (checkPermissionCall()) {
                    isAllowPermissionCall = true;
                    mApplication.getCallBusiness().handleAllowedPermissions(callType, jidFriend, sdpCallData);
                    answerFromNotify();
                } else {
                    if (isRecentGotoSettingPermission) {
                        isRecentGotoSettingPermission = false;
                        mApplication.getCallBusiness().handleDeclinedPermission();// khong cap quyen
                    }
                }
            } else {
                mApplication.getCallBusiness().onActivityResume();
            }
        }
    }

    private boolean checkPermissionCall() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
        mTvwCallState.stopTimer();
        mTvwCallStateNetwork.stopTimer();
        mTvwCallOutLabel.stopTimer();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        isRecentRequestPermission = false;
        if (PermissionHelper.verifyPermissions(grantResults)) {
            isAllowPermissionCall = true;
            mApplication.getCallBusiness().handleAllowedPermissions(callType, jidFriend, sdpCallData);
            answerFromNotify();
        } else {
            if (isShowFromNewIntent) {
                isShowFromNewIntent = false;
                PermissionHelper.checkPermissionAndShowDialogPermission(this, Constants.PERMISSION.PERMISSION_REQUEST_CAMERA_AND_RECORD_AUDIO,
                        new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA}, this);
                return;
            }
            mApplication.getCallBusiness().handleDeclinedPermission();// khong cap quyen
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void answerFromNotify() {
        if (isAcceptFromNotification && !isAnsweredCall) {
            isAcceptFromNotification = false;
            isAnsweredCall = true;
            mCallBusiness.handleAnswerCall(MochaCallActivity.this, false);
        }
    }

    @Override
    protected void onDestroy() {
        if (bmBlurDefault != null) bmBlurDefault.recycle();
        stopAutoHideControl();
        clearFlag();
        CallBusiness.removeCallStateListener(this);
        mApplication.getCallBusiness().setExistCallActivity(false);
        if (mHandlerShowRating != null)
            mHandlerShowRating.removeCallbacksAndMessages(null);
        EventBus.getDefault().unregister(this);


        /*if (audioManager != null) {
            audioManager.stop();
            audioManager = null;
        }*/
        mCallBusiness.resetDataSetting();

        if (sensorManager != null) {
            sensorManager = null;
        }
        super.onDestroy();
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
        if (proximityWakeLock.isHeld()) {
            proximityWakeLock.release();
        }
        wakeLock = null;
        proximityWakeLock = null;
    }

    @Override
    public void onBackPressed() {
        //return;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_HOME || keyCode == KeyEvent.KEYCODE_MENU)) {
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ||
                keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
            return mApplication.getCallBusiness().stopRingingWhenPressKeyVolumes();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call_end_image:
                mCallBusiness.handleDeclineCall(false);
                break;
            case R.id.call_mute_layout:
                mCallBusiness.toggleMute();
                CallUIHelper.drawStateMute(mCallBusiness, mImgMute);
                break;
            case R.id.call_speaker_layout:
                mCallBusiness.toggleSpeaker();
                CallUIHelper.drawStateSpeaker(mCallBusiness, mImgSpeaker);
                break;
            case R.id.call_chat_layout:
                showPopupContextMenu();
                break;
            case R.id.call_audio_to_video_layout:
                mCallBusiness.handleEnableVideo(true);
                break;
            case R.id.call_switch_camera_layout:
                CallRtcHelper.getInstance(mApplication).switchCamera(iconActionCall);
                break;
            case R.id.call_render_remote:
                if (mCallBusiness.isModeVideoCall() && mCallBusiness.getStateAudioVideo() == CallConstant
                        .STATE_AUDIO_VIDEO.ACCEPTED) {
                    boolean isShow = mViewQualityAndState.getVisibility() == View.GONE;
                    CallUIHelper.showOrHideOptionVideoWithAnimation(mViewQualityAndState, mViewMore, isShow);
                    if (isShow) {
                        startAutoHideControl();
                    } else {
                        stopAutoHideControl();
                    }
                }
                break;
            case R.id.call_video_enable_layout:
                if (mCallBusiness.getCurrentCallState() >= CallConstant.STATE.CONNECT) {
                    mCallBusiness.handleEnableVideo(!mCallBusiness.isEnableMyVideoCall());
                }
                break;
            case R.id.call_answer_audio_2_video:
                mCallBusiness.handleEnableVideo(true);
                break;
            case R.id.call_reject_audio_2_video:
                mCallBusiness.handleEnableVideo(false);
                break;
            case R.id.call_only_audio_layout:
                mAppStateManager.applicationEnterForeground(MochaCallActivity.this, true);
                // mApplication.getXmppManager().sendPresenceBackgroundOrForeground(false);//send foreground, no mark
                // state
                mCallBusiness.handleAnswerCall(MochaCallActivity.this, true);
                isAnsweredCall = true;
                break;
        }
    }

    @Override
    public void onRinging() {
        runOnUiThread(() -> {
            Log.d(TAG, " ringing ");
            mTvwCallState.setText(mRes.getString(R.string.call_state_ringing), 700);
        });
    }

    @Override
    public void onConnectChange() {
        runOnUiThread(this::drawDetailCall);
    }

    @Override
    public void onVideoStateChange() {
        runOnUiThread(this::drawDetailCall);
    }

    @Override
    public void onCallBusy() {// TODO chuan busy khoong vao case nay
        runOnUiThread(() -> {
            Log.i(TAG, "onCallBusy");
            mTvwCallState.stopTimer();
            mTvwCallStateNetwork.stopTimer();
            mTvwCallOutLabel.stopTimer();
            finishActivity();
            actionClick = -1;
        });
    }

    @Override
    public void onCallEnd() {
        runOnUiThread(() -> {
            mTvwCallState.stopTimer();
            mTvwCallStateNetwork.stopTimer();
            mTvwCallOutLabel.stopTimer();
            mTvwCallState.setText(R.string.call_state_end);
            mImgEndCall.setEnabled(false);
            if (mCallBusiness.getCurrentTimeCall() > 0) {
                Log.i(TAG, "timeZeroBw2EndCall: " + timeZeroBw2EndCall);
                if (timeZeroBw2EndCall > 0 && countBandwidthZero * 2000 >= timeZeroBw2EndCall) {
                    RecallActivity.start(mApplication, jidFriend, isCallVideo);
                    finish();
                } else {
                    //show popup rating call
                    if (callTypeRating == CallConstant.CALL_TYPE.CALL_OUT)
                        CallHistoryHelper.getInstance().getRemainTime();
                    actionCallEnd();
//                        showPopupRatingCall();
                }
            } else {
                actionCallEnd();
            }
        });
    }

    private void actionCallEnd() {
        Log.i(TAG, "onCallEnd");

        if (actionClick == -1) {
            finishActivity();
        } else if (actionClick == ACTION_CHAT) {
            navigateToChatActivity();
        }
        actionClick = -1;
    }

    @Override
    public void onNotSupport(final String msg, final boolean isToast) {
        runOnUiThread(() -> {
            if (callType == CallConstant.TYPE_CALLER) {// callee khong vao case nay
                if (isToast) showToast(msg, Toast.LENGTH_LONG);
                mTvwCallState.stopTimer();
                mTvwCallStateNetwork.stopTimer();
                mTvwCallOutLabel.stopTimer();
                mTvwCallState.setText(R.string.call_state_end);
                finishActivity();
            }
        });
    }

    boolean isCallVideo = false;

    @Override
    public void onTickTimeConnect(final int timeCall) {
        runOnUiThread(() -> {
            if (mViewAnswer.getVisibility() == View.VISIBLE) {// đến giây rồi mà vẫn visible view answer
                drawDetailCall();
            } else {
                drawDuration(timeCall);
            }

            if (timeCall == 1) {
                timeZeroBw2EndCall = mCallBusiness.getZeroBwEndCall();
                isCallVideo = mCallBusiness.isVideoCall();
                if (mCallBusiness.isVideoCall()) {
                    callTypeRating = CallConstant.CALL_TYPE.CALL_VIDEO;
                } else {
                    if (mCallBusiness.isCallOut())
                        callTypeRating = CallConstant.CALL_TYPE.CALL_OUT;
                    else
                        callTypeRating = CallConstant.CALL_TYPE.CALL_DATA;
                }
            }
        });
    }

    @Override
    public void onSpeakerChanged(final boolean isDisable) {
        runOnUiThread(() -> {
            Log.d(TAG, "onSpeakerChanged: " + isDisable);
            mLlActionSpeaker.setEnabled(!isDisable);
            if (isDisable) {
                mImgSpeaker.setImageResource(R.drawable.ic_call_speaker_disable);
                //mTvwSpeakLabel.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_ab_icon));
            } else {
                mImgSpeaker.clearColorFilter();
                //mTvwSpeakLabel.setTextColor(ContextCompat.getColor(mApplication, R.color.white));
                CallUIHelper.drawStateSpeaker(mCallBusiness, mImgSpeaker);
            }
        });
    }

    private boolean restartWhenBwZero = false;

    @Override
    public void onQualityReported(final long bandwidth) {
        runOnUiThread(() -> {
            Log.d(TAG, "onQualityReported: " + bandwidth);
            if (bandwidth == 0) countBandwidthZero++;
            else {
                countBandwidthZero = 0;
                restartWhenBwZero = false;
            }
            Log.i(TAG, "timeZeroBw2EndCall: " + timeZeroBw2EndCall + " countBandwidthZero: " + countBandwidthZero);
            if (timeZeroBw2EndCall > 0 && countBandwidthZero * 2000 >= timeZeroBw2EndCall) {
                Log.f(TAG, "---------end call timeZeroBw2EndCall");
                mCallBusiness.handleDeclineCall(false);
                return;
            }
            if (mCallBusiness.isEnableRestartICE() && !restartWhenBwZero && mCallBusiness.getTimeRestartBw() >= 0) {
                if (countBandwidthZero * 2000 >= mCallBusiness.getTimeRestartBw()) {
                    if (!mApplication.getCallBusiness().isStartedRestartICE()) {
                        Log.i(TAG, "countBandwidthZero * 2000 >= mCallBusiness.getTimeRestartBw(): " + mCallBusiness.getTimeRestartBw());
                        CallRtcHelper.getInstance(mApplication).setRestartReason(ReengCallPacket.RestartReason.BW_0);
                        CallRtcHelper.getInstance(mApplication).forceRestartICE();
                        restartWhenBwZero = true;
                    }

                }
            }
            drawDurationAndQuality(bandwidth);
        });
    }

    @Override
    public void onLocalStreamCreated(final StringeeStream stream) {
        runOnUiThread(() -> {
            Log.d(TAG, "onLocalStreamCreated: ");
            CallRtcHelper.getInstance(mApplication).setLocalSurfaceRenderer(stream.getSurfaceViewRenderer(getApplicationContext()));
            localRender.removeAllViews();
            addLocalSurface();
            CallRtcHelper.getInstance(mApplication).renderStream(MochaCallActivity.this, stream, true);
        });
    }

    @Override
    public void onAddStream(final StringeeStream stream) {
        runOnUiThread(() -> {
            Log.d(TAG, "onAddStream: ");
            CallRtcHelper.getInstance(mApplication).setRemoteSurfaceRenderer(
                    stream.getSurfaceViewRenderer(getApplicationContext()));
            remoteRender.removeAllViews();
            addRemoteSurface();
            CallRtcHelper.getInstance(mApplication).renderStream(MochaCallActivity.this, stream, false);
        });
    }

    @SuppressLint("WakelockTimeout")
    @Override
    public void onSensorChanged(SensorEvent event) {
//        float value = event.values[0];
//        if (Float.compare(value, 0) == 0) {
//            if (!mCallBusiness.isModeVideoCall()) {
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                mViewBlack.setVisibility(View.VISIBLE);
//            }
//        } else {
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            mViewBlack.setVisibility(View.GONE);
//        }
        if (!mCallBusiness.isModeVideoCall()) {
            float distance = event.values[0];
            if (distance <= 0.0) {
                proximityWakeLock.acquire();
            } else {
                wakeLock.acquire();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        String content = "";
        switch (menuId) {
            case Constants.MENU.MENU_CALL_CANT_TALK:
                content = mRes.getString(R.string.call_cant_talk_now);
                break;
            case Constants.MENU.MENU_CALL_WILL_CALL:
                content = mRes.getString(R.string.call_will_call_later);
                break;
            case Constants.MENU.MENU_CALL_WILL_CALL_RIGHT_BACK:
                content = mRes.getString(R.string.call_will_call_right_back);
                break;
            case Constants.MENU.MENU_CALL_ON_MY_WAY:
                content = mRes.getString(R.string.call_on_my_way);
                break;
            case Constants.MENU.MENU_CALL_CONFIDE_BUSY:
                content = mRes.getString(R.string.stranger_confide_call_busy);
                break;
            case Constants.MENU.MENU_CALL_CONFIDE_SORRY:
                content = mRes.getString(R.string.stranger_confide_call_sorry);
                break;
            case Constants.MENU.MENU_CALL_TYPE_YOURSELF:
                actionClick = ACTION_CHAT;
                mCallBusiness.handleDeclineCall(false);
                break;
            default:
                break;
        }
        if (!TextUtils.isEmpty(content)) {
            ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(jidFriend);
            SoloSendTextMessage textMessage = new SoloSendTextMessage(thread,
                    mReengAccountBusiness.getJidNumber(), jidFriend, "");
            textMessage.setCState(mContactBusiness.getCStateMessage(jidFriend));
            textMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
            textMessage.setContent(content);
            mMessageBusiness.insertNewMessageBeforeSend(thread, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT,
                    textMessage);
            mMessageBusiness.sendXMPPMessage(textMessage, thread);
            mCallBusiness.handleDeclineCall(false);
            mApplication.getXmppManager().sendPresenceBackgroundOrForeground(true);//TODO send background, no mark state
        }
    }

    private void findComponentViews() {
        localRender = findViewById(R.id.call_render_local);
        remoteRender = findViewById(R.id.call_render_remote);
        mViewQualityAndState = findViewById(R.id.call_header_layout);
        mViewCallOutHeader = findViewById(R.id.call_out_header_layout);
        mTvCallOut = findViewById(R.id.call_out_state);
        mViewAvatarName = findViewById(R.id.call_name_avatar_layout);
        mViewAnswerAudio2Video = findViewById(R.id.call_answer_audio_2_video_layout);
        mViewAnswer = findViewById(R.id.call_answer_layout);
        mViewMore = findViewById(R.id.call_more_action_layout);
        mTvwCallState = findViewById(R.id.call_state_text);
        mTvwCallStateNetwork = findViewById(R.id.call_state_network);
        mTvwDuration = findViewById(R.id.call_duration);
        mImgAvatar = findViewById(R.id.call_avatar_image);
        mTvwName = findViewById(R.id.call_user_name);
        mTvwUserNumber = findViewById(R.id.call_user_number);
        mTvwCallOutLabel = findViewById(R.id.call_out_label);
        mTvwCallStrangerLabel = findViewById(R.id.call_stranger_label);
        mImgAvatarBlur = findViewById(R.id.call_view_blur);
        iconActionCall = findViewById(R.id.IconActionCall);
        //action
        mLlActionVideoEnable = findViewById(R.id.call_video_enable_layout);
        mLlActionAudio2Video = findViewById(R.id.call_audio_to_video_layout);
        bg_call = findViewById(R.id.bg_call);
        mLlActionSpeaker = findViewById(R.id.call_speaker_layout);
        mLlActionMute = findViewById(R.id.call_mute_layout);
        mLlSwitchCamera = findViewById(R.id.call_switch_camera_layout);
        mImgSpeaker = findViewById(R.id.call_speaker_image);
        mImgMute = findViewById(R.id.call_mute_image);
        mImgEndCall = findViewById(R.id.call_end_image);
        mImgVideoEnable = findViewById(R.id.call_video_enable_image);
        mImgAudio2Video = findViewById(R.id.call_audio_to_video_image);
        mImgAnswerAduio2Video = findViewById(R.id.call_answer_audio_2_video);
        mImgRejectAudio2Video = findViewById(R.id.call_reject_audio_2_video);
        // answer
        mLlActionChat = findViewById(R.id.call_chat_layout);
        mLlOnlyAudio = findViewById(R.id.call_only_audio_layout);
        mRippleAnswer = findViewById(R.id.call_answer);
        mRippleReject = findViewById(R.id.call_reject);
        mImgAnswerIcon = findViewById(R.id.call_answer_icon);
        mViewBlack = findViewById(R.id.call_view_black);
    }

    private void drawDetailCall() {
        CallUIHelper.drawStateMute(mCallBusiness, mImgMute);
        CallUIHelper.drawStateSpeaker(mCallBusiness, mImgSpeaker);
        if (mCallBusiness.isCallOut() || mCallBusiness.isStrangerConfide()) {
            drawDetailCallOut();
            refreshSurfaceView(false, false);
        } else {
            drawDetailCallNormal();
            CallUIHelper.drawStateVideoEnable(mCallBusiness, mImgVideoEnable);
            CallUIHelper.drawStateAudioToVideo(mCallBusiness, mApplication, mLlActionAudio2Video, mImgAudio2Video);
        }
        drawDurationAndQuality(60000);//60kB
    }

    private void drawDurationAndQuality(long bandWidth) {
        if (mCallBusiness.getCurrentCallState() >= CallConstant.STATE.CONNECTED) {
            mViewCallOutHeader.setVisibility(View.GONE);
            mTvwCallOutLabel.setVisibility(View.GONE);
            mTvwUserNumber.setVisibility(View.GONE);
            mTvwCallState.setVisibility(View.GONE);
            drawDuration(mCallBusiness.getCurrentTimeCall());
            CallUIHelper.drawCallQuality(mViewQualityAndState, mTvwCallStateNetwork, bandWidth, true, mApplication);
        } else if (mCallBusiness.getCurrentCallState() == CallConstant.STATE.CONNECT ||
                mCallBusiness.getCurrentCallState() == CallConstant.STATE.CONNECTING) {
            mTvwCallState.setText(mRes.getString(R.string.call_state_connecting), 700);
            CallUIHelper.drawCallQuality(mViewQualityAndState, mTvwCallStateNetwork, bandWidth, false, mApplication);
            drawDuration(-1);
        } else if (mCallBusiness.getCurrentCallState() >= CallConstant.STATE.RINGING) {
            if (callType == CallConstant.TYPE_CALLEE) {
                if (TextUtils.isEmpty(userNumber)) {
                    mTvwCallState.setText(mRes.getString(R.string.call_state_calling), 700);
                } else {
                    mTvwCallState.setText("");
                }
            } else {
                mTvwCallState.setText(mRes.getString(R.string.call_state_ringing), 700);
            }
            CallUIHelper.drawCallQuality(mViewQualityAndState, mTvwCallStateNetwork, bandWidth, false, mApplication);
            drawDuration(-1);
        } else {
            if (callType == CallConstant.TYPE_CALLEE) {
                mTvwCallState.setText(mRes.getString(R.string.call_state_calling), 700);
            } else {
                mTvwCallState.setText(mRes.getString(R.string.call_state_dialing), 700);
            }
            CallUIHelper.drawCallQuality(mViewQualityAndState, mTvwCallStateNetwork, bandWidth, false, mApplication);
            drawDuration(-1);
        }
    }

    private void drawDetailCallOut() {
        mViewAnswerAudio2Video.setVisibility(View.GONE);
        mLlActionVideoEnable.setVisibility(View.GONE);
        mLlActionAudio2Video.setVisibility(View.GONE);
        mLlActionSpeaker.setVisibility(View.VISIBLE);
        mLlSwitchCamera.setVisibility(View.GONE);
        mViewAvatarName.setVisibility(View.VISIBLE);
        mViewQualityAndState.setVisibility(View.VISIBLE);
        if (callType == CallConstant.TYPE_CALLER || mCallBusiness.getCurrentCallState() >= CallConstant.STATE.CONNECT) {
            mViewAnswer.setVisibility(View.GONE);
            mViewMore.setVisibility(View.VISIBLE);
            mLlActionChat.setVisibility(View.GONE);
            mLlOnlyAudio.setVisibility(View.GONE);
        } else {
            mViewAnswer.setVisibility(View.VISIBLE);
            mLlOnlyAudio.setVisibility(View.GONE);
            mViewMore.setVisibility(View.GONE);
            mLlActionChat.setVisibility(View.GONE);
            mImgAnswerIcon.setImageResource(R.drawable.ic_call_accept);
        }
        drawUserNumber(null);
    }

    private void drawDetailCallNormal() {
        if (callType == CallConstant.TYPE_CALLER || mCallBusiness.getCurrentCallState() >= CallConstant.STATE.CONNECT) {
            mViewAnswer.setVisibility(View.GONE);
            mLlActionChat.setVisibility(View.GONE);
            mLlOnlyAudio.setVisibility(View.GONE);
            if (mCallBusiness.isModeVideoCall()) {
                drawDetailCallVideo();
            } else {
                drawDetailCallAudio();
                refreshSurfaceView(false, false);
            }
        } else {
            drawCalleeConfirming();
            refreshSurfaceView(false, false);
        }
    }

    private void drawDetailCallAudio() {
        drawDuration(mCallBusiness.getCurrentTimeCall());
        mViewAnswerAudio2Video.setVisibility(View.GONE);
        mViewAvatarName.setVisibility(View.VISIBLE);
        mLlActionSpeaker.setVisibility(View.VISIBLE);
        mLlSwitchCamera.setVisibility(View.GONE);
        mLlActionAudio2Video.setVisibility(View.VISIBLE);
        mLlActionVideoEnable.setVisibility(View.GONE);
        mViewAnswer.setVisibility(View.GONE);
        CallUIHelper.resetOptionView(mViewQualityAndState, mViewMore);
        drawUserNumber(null);
    }

    private void drawDetailCallVideo() {
        drawDuration(-1);
        String temp = null;
        mLlActionSpeaker.setVisibility(View.VISIBLE);
        mLlActionAudio2Video.setVisibility(View.GONE);
        if (mCallBusiness.getStateAudioVideo() == CallConstant.STATE_AUDIO_VIDEO.ACCEPTED) {
            if (mCallBusiness.getCurrentCallState() >= CallConstant.STATE.CONNECTED) {
                mViewAvatarName.setVisibility(View.INVISIBLE);
                refreshSurfaceView(true, true);
            } else {
                mViewAvatarName.setVisibility(View.VISIBLE);
                CallUIHelper.resetOptionView(mViewQualityAndState, mViewMore);
                refreshSurfaceView(true, false);
            }
            mLlSwitchCamera.setVisibility(View.VISIBLE);
            mLlActionVideoEnable.setVisibility(View.VISIBLE);
            mViewAnswerAudio2Video.setVisibility(View.GONE);
        } else if (mCallBusiness.getStateAudioVideo() == CallConstant.STATE_AUDIO_VIDEO.IN_COMMING) {
            mViewQualityAndState.setVisibility(View.VISIBLE);
            mViewMore.setVisibility(View.GONE);
            mViewAvatarName.setVisibility(View.VISIBLE);
            mViewAnswerAudio2Video.setVisibility(View.VISIBLE);
            mTvwUserNumber.setVisibility(View.VISIBLE);
            temp = mRes.getString(R.string.call_state_audio_video_incoming);
            refreshSurfaceView(false, true);
        } else {// out_going
            mViewAvatarName.setVisibility(View.VISIBLE);
            mLlSwitchCamera.setVisibility(View.VISIBLE);
            mLlActionVideoEnable.setVisibility(View.VISIBLE);
            mViewAnswerAudio2Video.setVisibility(View.GONE);
            mTvwUserNumber.setVisibility(View.VISIBLE);
            temp = mRes.getString(R.string.call_state_audio_video_outgoing);
            CallUIHelper.resetOptionView(mViewQualityAndState, mViewMore);
            refreshSurfaceView(true, false);
        }
        drawUserNumber(temp);
    }

    private void refreshSurfaceView(boolean isLocal, boolean isRemote) {
        // show full control video -> start timer auto hide control
        if (isLocal && isRemote && mViewQualityAndState.getVisibility() == View.VISIBLE) {
            mViewMore.setVisibility(View.VISIBLE);
            startAutoHideControl();
        } else {
            stopAutoHideControl();
        }
        // đang show video mà state change draw lại thi ko lam gì
        if (isLocal && isRemote && localRender.getVisibility() == View.VISIBLE && remoteRender.getVisibility() ==
                View.VISIBLE) {
            return;
        }
        localRender.removeAllViews();
        if (isLocal) {
            localRender.setVisibility(View.VISIBLE);
            addLocalSurface();
        } else {
            localRender.setVisibility(View.GONE);
        }
        remoteRender.removeAllViews();
        if (isRemote) {
            remoteRender.setVisibility(View.VISIBLE);
            addRemoteSurface();
        } else {
            remoteRender.setVisibility(View.GONE);
        }
    }

    private void addLocalSurface() {
        if (CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer() != null) {
            if (CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer().getParent() != null) {
                ((ViewGroup) CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer().getParent())
                        .removeView(CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer());
            }
            Log.e("debug", "add local surface");
            localRender.addView(CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer());
//        }else {
//            CallRtcHelper.getInstance(mApplication).recreateLocalSurfaceRenderer();
//            if(CallRtcHelper.getInstance(mApplication).getLocalSurfaceRenderer() != null) {
//                localRender.removeAllViews();
//                addLocalSurface();
//                CallRtcHelper.getInstance(mApplication).renderStream(MochaCallActivity.this, CallRtcHelper.getInstance(mApplication).getLocalStream(), true);
//            }
        }
    }

    private void addRemoteSurface() {
        if (CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer() != null) {
            if (CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer().getParent() != null) {
                ((ViewGroup) CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer().getParent())
                        .removeView(CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer());
            }
            Log.e("debug", "add remote surface");
            remoteRender.addView(CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer());
//        }else {
//            CallRtcHelper.getInstance(mApplication).recreateRemoteRenderer();
//            if (CallRtcHelper.getInstance(mApplication).getRemoteSurfaceRenderer() != null) {
//                remoteRender.removeAllViews();
//                addRemoteSurface();
//                CallRtcHelper.getInstance(mApplication).renderStream(MochaCallActivity.this, CallRtcHelper.getInstance(mApplication).getRemoteStream(), false);
//            }
        }
    }

    private void drawCalleeConfirming() {// callee chưa trả lời
        mViewAnswerAudio2Video.setVisibility(View.GONE);
        mViewAvatarName.setVisibility(View.VISIBLE);
        mViewAnswer.setVisibility(View.VISIBLE);
        mViewQualityAndState.setVisibility(View.VISIBLE);
        mViewMore.setVisibility(View.GONE);
        if (mCallBusiness.isVideoCall()) {
            mImgAnswerIcon.setImageResource(R.drawable.ic_video_call_accept);
            mLlOnlyAudio.setVisibility(View.VISIBLE);
            mLlActionChat.setVisibility(View.GONE);
        } else {
            mImgAnswerIcon.setImageResource(R.drawable.ic_call_accept);
            mLlOnlyAudio.setVisibility(View.GONE);
            if (mCallBusiness.isCallOut()) {
                mLlActionChat.setVisibility(View.GONE);
            } else {
                mLlActionChat.setVisibility(View.VISIBLE);
            }
        }
        drawUserNumber(null);
    }

    private void drawDuration(int timeCall) {
        if (mCallBusiness.isModeVideoCall()) {
            mTvwDuration.setVisibility(View.INVISIBLE);
            if (callType == CallConstant.TYPE_CALLEE) {
                mTvwCallState.setVisibility(View.INVISIBLE);
            }
        } else if (timeCall < 0) {
            mTvwDuration.setVisibility(View.INVISIBLE);
            if (callType == CallConstant.TYPE_CALLEE) {
                mTvwCallState.setVisibility(View.VISIBLE);
            }
        } else {
            mTvwDuration.setVisibility(View.VISIBLE);
            mTvwDuration.setText(Utilities.secondsToTimer(timeCall));
            if (callType == CallConstant.TYPE_CALLEE) {
                mTvwCallState.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void drawDetail() {
        drawAvatarAndName();
        drawRippleLayout();
    }

    private void drawAvatarAndName() {
        RelativeLayout.LayoutParams avatarParams = (RelativeLayout.LayoutParams) mImgAvatar.getLayoutParams();
//        int avatarH = (mApplication.getHeightPixels() * 15) / 100;
//        avatarParams.height = avatarH;
//        avatarParams.width = avatarH;
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        String avatarSmallUrl = null;
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(jidFriend);
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        if (mPhoneNumber != null) {
            mTvwName.setText(mPhoneNumber.getName());
            userNumber = mPhoneNumber.getJidNumber(); //String.format(mRes.getString(R.string.call_mobile), mPhoneNumber.getRawNumber());
            if (mPhoneNumber.isReeng()) {
                avatarSmallUrl = avatarBusiness.getAvatarUrl(mPhoneNumber.getLastChangeAvatar(), jidFriend, size);
            }
        } else {
            StrangerPhoneNumber stranger = mApplication.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(jidFriend);
            if (stranger != null) { // neu so co trong bang stranger thi lay ten o day
                if (TextUtils.isEmpty(stranger.getFriendName())) {
                    mTvwName.setText(Utilities.hidenPhoneNumber(rawNumber));
                } else {
                    mTvwName.setText(stranger.getFriendName());
                }
                userNumber = "";
                // other app link truc tiep
                if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    avatarSmallUrl = stranger.getFriendAvatarUrl();
                } else {
                    NonContact nonContactStr = mContactBusiness.getExistNonContact(jidFriend);
                    // co non contact lay lavatar cho moi nhat
                    if (nonContactStr != null && nonContactStr.getState() == Constants.CONTACT.ACTIVE) {
                        avatarSmallUrl = avatarBusiness.getAvatarUrl(nonContactStr.getLAvatar(), jidFriend, size);
                    } else {// chua co non contact thi lay tu stranger
                        avatarSmallUrl = avatarBusiness.getAvatarUrl(stranger.getFriendAvatarUrl(), jidFriend, size);
                    }
                }
            } else {
                mTvwName.setText(rawNumber);
                userNumber = rawNumber;//String.format(mRes.getString(R.string.call_mobile), rawNumber);
                NonContact nonContact = mContactBusiness.getExistNonContact(jidFriend);
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    avatarSmallUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), jidFriend, size);
                }
            }
        }
        Log.d(TAG, "displayAvatarCallImage: " + avatarSmallUrl);
        // avatar full
        mImgAvatar.setVisibility(View.VISIBLE);
        if (callVideo) {
            drawProfile(accountRankDTO, userInfo);
        }

//        mApplication.getAvatarBusiness().loadMyAvatar(bitmap -> {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    ImageHelper.setAvatar(mApplication, bitmap, bmBlurDefault, mImgAvatarBlur);
//                }
//            });
//
//        });
        if (!Text.isEmpty(avatarSmallUrl)) {
            mApplication.getImageProfileBusiness().displayAvatarCallImage(mImgAvatar, avatarSmallUrl, new
                GlideImageLoader.SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(GlideException e) {
                        mImgAvatar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingComplete() {
                        mImgAvatar.setVisibility(View.VISIBLE);
                    }
                });
        }

        if (mCallBusiness.isCallOut()) {
            mViewCallOutHeader.setVisibility(View.VISIBLE);
            mTvwCallStrangerLabel.setVisibility(View.GONE);
            if (mCallBusiness.getCurrentTypeCall() == CallConstant.TYPE_CALLEE) {
                String avnoNumber = "";
                ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
                if (account != null) {
                    if (TextUtils.isEmpty(account.getAvnoNumber())) {// chưa có số ảo thì hiển thị số thật, phòng TH
                        // sếp thích cho call in đến số thật của sếp
                        avnoNumber = account.getJidNumber();
                    } else {
                        avnoNumber = account.getAvnoNumber();
                    }
                }
                String label = String.format(mRes.getString(R.string.call_in_label), avnoNumber);
                mTvCallOut.setText(label);
            } else {
                String label;

                if (mApplication.getReengAccountBusiness().isCambodia()) {
                    label = mRes.getString(R.string.title_mocha_callout_to_2g);
                } else
                    label = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG
                            .CALL_OUT_LABEL);
                mTvCallOut.setText(label);
            }
        } else if (mCallBusiness.isModeVideoCall()) {
            if (callType == CallConstant.TYPE_CALLEE) {
                mTvwCallOutLabel.setVisibility(View.VISIBLE);
                mTvwCallOutLabel.setText(getString(R.string.mocha_video_call), 700);
            } else {
                mTvwCallOutLabel.setVisibility(View.GONE);
            }
            mTvwCallStrangerLabel.setVisibility(View.GONE);
        } else if (mCallBusiness.isStrangerConfide() && callType == CallConstant.TYPE_CALLEE) {
            mTvwCallOutLabel.setVisibility(View.GONE);
            mTvwCallStrangerLabel.setVisibility(View.VISIBLE);
        } else if (callType == CallConstant.TYPE_CALLEE) {
            mTvwCallOutLabel.setVisibility(View.VISIBLE);
            mTvwCallOutLabel.setText(getString(R.string.mocha_call), 700);
            mTvwCallStrangerLabel.setVisibility(View.GONE);
        } else {
            mTvwCallOutLabel.setVisibility(View.GONE);
            mTvwCallStrangerLabel.setVisibility(View.GONE);
        }
    }

    private void drawUserNumber(String audio2Video) {
        if (!TextUtils.isEmpty(audio2Video)) {
            mTvwUserNumber.setText(audio2Video);
            mTvwUserNumber.setVisibility(View.VISIBLE);
        } else if (TextUtils.isEmpty(userNumber) || callType == CallConstant.TYPE_CALLER) {
            mTvwUserNumber.setVisibility(View.GONE);
        } else {
            mTvwUserNumber.setText(userNumber);
            mTvwUserNumber.setVisibility(View.VISIBLE);
        }
    }

    private void drawRippleLayout() {
        int rippleSize = mApplication.getWidthPixels() / 2;
        int rippleMaxSize = ((mApplication.getHeightPixels() - mApplication.getStatusBarHeight()) * 7) / 29;
        if (rippleSize > rippleMaxSize) {
            rippleSize = rippleMaxSize;
        }
        LinearLayout.LayoutParams paramsAnswers = (LinearLayout.LayoutParams) mRippleAnswer.getLayoutParams();
        paramsAnswers.width = rippleSize;
        paramsAnswers.height = rippleSize;
        mRippleAnswer.changeParams(rippleSize, rippleSize);
        LinearLayout.LayoutParams paramsRejects = (LinearLayout.LayoutParams) mRippleReject.getLayoutParams();
        paramsRejects.width = rippleSize;
        paramsRejects.height = rippleSize;
        mRippleReject.changeParams(rippleSize, rippleSize);
    }

    private void setListener() {
        mImgEndCall.setOnClickListener(this);
        mLlActionChat.setOnClickListener(this);
        mLlActionSpeaker.setOnClickListener(this);
        mLlActionMute.setOnClickListener(this);
        mLlActionAudio2Video.setOnClickListener(this);
        mLlSwitchCamera.setOnClickListener(this);
        mLlActionVideoEnable.setOnClickListener(this);
        mImgAnswerAduio2Video.setOnClickListener(this);
        mImgRejectAudio2Video.setOnClickListener(this);
        mLlOnlyAudio.setOnClickListener(this);
        setRippleCallListener();
        setParentListener();
    }

    private void setParentListener() {
        remoteRender.setOnClickListener(this);
    }

    private void setRippleCallListener() {
        mRippleAnswer.setOnClickListener(v -> {
            mAppStateManager.applicationEnterForeground(MochaCallActivity.this, true);
            mCallBusiness.handleAnswerCall(MochaCallActivity.this, false);
            isAnsweredCall = true;
        });
        mRippleReject.setOnClickListener(v -> {
            mCallBusiness.handleDeclineCall(false);
            mApplication.getXmppManager().sendPresenceBackgroundOrForeground(true);
        });
        /*mRippleAnswer.setListener(new RippleActionCall.OnListener() {
            @Override
            public void onPress() {
                mRippleReject.setEnabled(false);
            }

            @Override
            public void onClick() {
                mAppStateManager.applicationEnterForeground(MochaCallActivity.this, true);
                mCallBusiness.handleAnswerCall(MochaCallActivity.this, false);
            }

            @Override
            public void onCancel() {
                mRippleReject.setEnabled(true);
            }
        });
        mRippleReject.setListener(new RippleActionCall.OnListener() {
            @Override
            public void onPress() {
                mRippleAnswer.setEnabled(false);
            }

            @Override
            public void onClick() {
                mCallBusiness.handleDeclineCall(false);
                mApplication.getXmppManager().sendPresenceBackgroundOrForeground(true);//TODO send background, no
                mark state
            }

            @Override
            public void onCancel() {
                mRippleAnswer.setEnabled(true);
            }
        });*/
    }

    private void showPopupContextMenu() {
        ArrayList<ItemContextMenu> listMenu = new ArrayList<>();
        ItemContextMenu cantTalk = new ItemContextMenu(mRes.getString(R.string.call_cant_talk_now), -1, null,
                Constants.MENU.MENU_CALL_CANT_TALK);
        ItemContextMenu callLater = new ItemContextMenu(mRes.getString(R.string.call_will_call_later), -1,
                null, Constants.MENU.MENU_CALL_WILL_CALL);
        ItemContextMenu callRightBack = new ItemContextMenu(mRes.getString(R.string.call_will_call_right_back),
                -1, null, Constants.MENU.MENU_CALL_WILL_CALL_RIGHT_BACK);
        ItemContextMenu callOnMyWay = new ItemContextMenu(mRes.getString(R.string.call_on_my_way), -1, null,
                Constants.MENU.MENU_CALL_ON_MY_WAY);
        ItemContextMenu callTypeYourSelf = new ItemContextMenu(mRes.getString(R.string.call_type_yourself), -1,
                null, Constants.MENU.MENU_CALL_TYPE_YOURSELF);
        ItemContextMenu callConfideBusy = new ItemContextMenu(mRes.getString(R.string
                .stranger_confide_call_busy), -1, null, Constants.MENU.MENU_CALL_CONFIDE_BUSY);
        ItemContextMenu callConfideSorry = new ItemContextMenu(mRes.getString(R.string
                .stranger_confide_call_sorry), -1, null, Constants.MENU.MENU_CALL_CONFIDE_SORRY);
        if (mCallBusiness.isStrangerConfide()) {
            listMenu.add(callConfideBusy);
            listMenu.add(callConfideSorry);
            listMenu.add(callTypeYourSelf);
        } else {
            listMenu.add(cantTalk);
            listMenu.add(callLater);
            listMenu.add(callRightBack);
            listMenu.add(callOnMyWay);
            listMenu.add(callTypeYourSelf);
        }
        PopupHelper.getInstance().showContextMenu(this, null, listMenu, this);
    }

    private void navigateToChatActivity() {
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(jidFriend);
        Intent chatIntent = new Intent(mApplication, ChatActivity.class);
        chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        chatIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, thread.getThreadType());
        chatIntent.putExtras(bundle);
        mApplication.startActivity(chatIntent);
        runOnUiThread(this::finish);
    }

    private void startAutoHideControl() {
        if (mTimer == null) {
            stopAutoHideControl();
            mTimer = new CountDownTimer(AUTO_HIDE_TIME, AUTO_HIDE_TIME) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "CountDownTimer onFinish");
                    runOnUiThread(() -> CallUIHelper.showOrHideOptionVideoWithAnimation(mViewQualityAndState, mViewMore, false));
                    mTimer = null;
                }
            };
            mTimer.start();
        }
    }

    private void stopAutoHideControl() {
        Log.d(TAG, "stopAutoHideControl");
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    //Call api rating call
    public void processRatingCall(final int ratingValue) {
        isRequesting = true;
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG);

        Log.i(TAG, "processRatingCall ");
        final String defaultError = mRes.getString(R.string.e601_error_but_undefined);
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.RATING_CALL), response -> {
            Log.i(TAG, "processRatingCall onResponse: " + response);
            isRequesting = false;
        }, volleyError -> {
            Log.e(TAG, "VolleyError", volleyError);
            showToast(defaultError);
            isRequesting = false;
        }) {
            @SuppressWarnings("deprecation")
            @Override
            protected Map<String, String> getParams() {
                long currentTime = TimeHelper.getCurrentTime();
                ReengAccount myAccount = mReengAccountBusiness.getCurrentAccount();

                String networkType = "";
                ConnectivityManager cm = (ConnectivityManager) mApplication.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (null != activeNetwork) {
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        networkType = "WIFI";
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        networkType = activeNetwork.getSubtypeName();
                    }
                }

                HashMap<String, String> params = new HashMap<>();
                StringBuilder sb = new StringBuilder().
                        append(myAccount.getJidNumber()).
                        append(callTypeRating).
                        append(ratingValue).
                        append(Constants.HTTP.CLIENT_TYPE_STRING).
                        append(Config.REVISION).
                        append(networkType).
                        append(myAccount.getToken()).
                        append(currentTime);
                String dataEncrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), myAccount.getToken());
                params.put(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber());
                params.put("type", callTypeRating + "");
                params.put("rank", ratingValue + "");
                params.put(Constants.HTTP.REST_CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
                params.put(Constants.HTTP.REST_REVISION, Config.REVISION);
                params.put("networkType", networkType + "");
                params.put(Constants.HTTP.DATA_SECURITY, dataEncrypt);
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG, false);
    }

    private void showPopupRatingCall() {
        mApplication.getCallBusiness().setExistCallActivity(false);
        dialogRating = new DialogRating(this) {
            @Override
            public void onSubmitRating(int value) {
                //Goi api log rating
                processRatingCall(value);
                actionCallEnd();
            }

            @Override
            public void onCancelRating() {
                actionCallEnd();
            }

            @Override
            public void show() {
                countDownTimer = TIME_SHOW_RATING;
                startCountDown();
                super.show();
            }
        };

        dialogRating.setOnCancelListener(dialog -> actionCallEnd());
        if (dialogRating != null && !dialogRating.isShowing())
            dialogRating.show();
    }

    private void startCountDown() {
        mHandlerShowRating.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                int remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    if (dialogRating != null && dialogRating.isShowing()) {
                        actionCallEnd();
                        dialogRating.dismiss();
                    }
                } else {
                    mHandlerShowRating.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventConnection(final EventConnection event) {
        if (BuildConfig.DEBUG)
//            showToast("EventConnection: " + event.state);
            Log.i(TAG, "onEventConnection: " + event.state.name());
    }

    @Override
    public void gotoSetting() {
        isRecentGotoSettingPermission = true;
    }

    public static class EventConnection {
        public StringeeCallListener.StringeeConnectionState state;

        public EventConnection(StringeeCallListener.StringeeConnectionState state) {
            this.state = state;
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        drawProfile(event.rankDTO, event.userInfo);
        accountRankDTO = event.rankDTO;
        userInfo = event.userInfo;
    }
    private void drawProfile(AccountRankDTO accountRankDTO, UserInfo userInfo) {
        KhUserRank myRank = null;
        if (accountRankDTO != null) {
            myRank = KhUserRank.getById(accountRankDTO.rankId);
        }
        if (callVideo) {
            if (!StringUtils.isEmpty(userInfo.getAvatar())) {
                Glide.with(this)
                        .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_default)
                        .error(R.drawable.ic_avatar_default)
                        .into(mImgAvatarBlur);
            } else {
                if (myRank != null) {
                    mImgAvatarBlur.setImageResource(myRank.resAvatar);
                }
            }
        }
    }
}
package com.metfone.selfcare.activity;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.editor.sticker.ImageSticker;
import com.android.editor.sticker.Sticker;
import com.android.editor.sticker.StickerView;
import com.android.editor.sticker.TextSticker;
import com.android.editor.sticker.textutils.DialogSelectColor;
import com.android.editor.sticker.textutils.DialogSelectFont;
import com.android.editor.sticker.textutils.DialogTextEditor;
import com.android.editor.sticker.textutils.OnEditorTextCallBack;
import com.android.editor.sticker.utils.StickerStack;
import com.android.editor.sticker.utils.StickerState;
import com.android.editor.sticker.utils.StickerUtils;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.database.model.guestbook.EmoItem;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.database.model.guestbook.PageItem;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.OnSelectSticker;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.ui.guestbook.CropActivity;
import com.metfone.selfcare.ui.guestbook.DialogPreview;
import com.metfone.selfcare.ui.guestbook.DialogSelectBackground;
import com.metfone.selfcare.ui.guestbook.DialogSelectEmoSticker;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public class GuestBookEditorActivity extends BaseSlidingFragmentActivity implements
        StickerView.OnStickerOperationListener,
        OnEditorTextCallBack,
        OnSelectSticker,
        View.OnClickListener {
    private static final String TAG = GuestBookEditorActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private GuestBookHelper mGuestBookHelper;
    private Handler mHandler = new Handler();
    private Resources mRes;
    private StickerView stickerView;
    private ImageView mImgBackground;
    private TextView mTvwPageNumber;
    private View mViewActionBar, mAbAvatarLayour;
    private ImageView mAbBack, mAbAddFriend, mAbSave, mAbPreview;
    private CircleImageView mAbAvatarImage;
    private TextView mAbTitle, mAbAvatarText;
    private LinearLayout mViewControlParent, mViewControlText;
    private View mControlChangeTheme, mControlAddImage, mControlAddText;
    private View mControlAddSticker, mControlUndo, mControlRedo;
    private View mControlTextEdit, mControlTextBold, mControlTextItalic;
    private View mControlTextUnderLine, mControlTextFont, mControlTextColor;
    private ImageView mControlUndoImage, mControlRedoImage;

    private Animation animSlideDown, animSlideUp;
    private boolean isShowActionBar = true;
    private boolean isStateChanged = false;
    private boolean isChangedBackground = false;
    private StickerState currentStickerState;
    private LinkedList<StickerStack> undoStacks = new LinkedList<>();
    private LinkedList<StickerStack> redoStacks = new LinkedList<>();
    //
    private Page currentPage;
    private Background currentBackground;
    //
    private float scaleToClient;
    private float scaleToServer;
    //
    private HashSet<String> tempFilePaths = new HashSet<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_book_editor);
        changeStatusBar(true);
        mApplication = (ApplicationController) getApplicationContext();
        mGuestBookHelper = GuestBookHelper.getInstance(mApplication);
        mRes = getResources();
        findComponentViews();
        getDataAndDraw(savedInstanceState);
        setListeners();
        //loadSticker();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (savePageAsyncTask != null) {
            savePageAsyncTask.cancel(true);
            savePageAsyncTask = null;
        }
        if (!tempFilePaths.isEmpty()) {
            FileHelper.deleteListFile(tempFilePaths);
        }
        // TODO comment when commit ibm
        System.gc();
        Runtime.getRuntime().gc();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                onBackPressed();
                break;
            case R.id.ab_add_friend_btn:
                handleAssignPage();
                break;
            case R.id.ab_thread_avatar_layout:
                handleClickAvatarAssigned();
                break;
            case R.id.ab_save_btn:
                handleSavePage(false);
                break;
            case R.id.ab_preview_btn:
                stickerView.clearFocus();
                DialogPreview preview = DialogPreview.newInstance(stickerView, currentPage.getBookId());
                preview.show(getSupportFragmentManager(), TAG);
                break;
            case R.id.editor_control_change_theme:
                handlePickBackground();
                break;
            case R.id.editor_control_add_image:
                if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionHelper.requestPermissionWithGuide(GuestBookEditorActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Constants.PERMISSION.PERMISSION_REQUEST_GALLERY);
                } else {
                    handlePickImage();
                }
                break;
            case R.id.editor_control_add_text:
                DialogTextEditor textEditor = DialogTextEditor.newInstance("", StickerStack.Action.ADD,
                        mRes.getString(R.string.ok), mRes.getString(R.string.cancel),
                        mRes.getString(R.string.guest_book_enter_content));
                textEditor.show(getSupportFragmentManager(), TAG);
                break;
            case R.id.editor_control_add_sticker:
                handlePickEmoSticker();
                break;
            case R.id.editor_control_undo:
                undoTask();
                break;
            case R.id.editor_control_redo:
                redoTask();
                break;
            case R.id.editor_control_edit:
                editTextSticker();
                break;
            case R.id.editor_control_bold:
                changeTextLayer(com.android.editor.sticker.utils.Constants.CHANGE_TEXT_BOLD);
                break;
            case R.id.editor_control_italic:
                changeTextLayer(com.android.editor.sticker.utils.Constants.CHANGE_TEXT_ITALIC);
                break;
            case R.id.editor_control_underline:
                changeTextLayer(com.android.editor.sticker.utils.Constants.CHANGE_TEXT_UNDERLINE);
                break;
            case R.id.editor_control_font:
                changeFontTextSticker();
                break;
            case R.id.editor_control_color:
                changeColorTextSticker();
                break;
        }
    }

    private void getDataAndDraw(Bundle savedInstanceState) {
        currentPage = mGuestBookHelper.getCurrentPageEditor();
        if (currentPage == null || currentPage.getBackground() == null) {
            showToast(R.string.e601_error_but_undefined);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                }
            }, 1000);
        } else {
            drawDetail();
        }
    }

    private void findComponentViews() {
        stickerView = (StickerView) findViewById(R.id.editor_sticker_view);
        mImgBackground = (ImageView) findViewById(R.id.editor_sticker_background);
        mTvwPageNumber = (TextView) findViewById(R.id.editor_sticker_page_number);
        //control
        mViewActionBar = findViewById(R.id.editor_action_bar);
        mViewControlParent = (LinearLayout) findViewById(R.id.editor_control_parent);
        mViewControlText = (LinearLayout) findViewById(R.id.editor_control_text);
        // actionbar
        mAbBack = (ImageView) mViewActionBar.findViewById(R.id.ab_back_btn);
        mAbAddFriend = (ImageView) mViewActionBar.findViewById(R.id.ab_add_friend_btn);
        mAbSave = (ImageView) mViewActionBar.findViewById(R.id.ab_save_btn);
        mAbPreview = (ImageView) mViewActionBar.findViewById(R.id.ab_preview_btn);
        mAbTitle = (TextView) mViewActionBar.findViewById(R.id.ab_title);
        mAbAvatarLayour = mViewActionBar.findViewById(R.id.ab_thread_avatar_layout);
        mAbAvatarImage = (CircleImageView) mViewActionBar.findViewById(R.id.ab_thread_avatar);
        mAbAvatarText = (TextView) mViewActionBar.findViewById(R.id.ab_thread_avatar_text);
        // parent
        mControlChangeTheme = mViewControlParent.findViewById(R.id.editor_control_change_theme);
        mControlAddImage = mViewControlParent.findViewById(R.id.editor_control_add_image);
        mControlAddText = mViewControlParent.findViewById(R.id.editor_control_add_text);
        mControlAddSticker = mViewControlParent.findViewById(R.id.editor_control_add_sticker);
        mControlUndo = mViewControlParent.findViewById(R.id.editor_control_undo);
        mControlRedo = mViewControlParent.findViewById(R.id.editor_control_redo);
        mControlUndoImage = (ImageView) mViewControlParent.findViewById(R.id.editor_control_undo_image);
        mControlRedoImage = (ImageView) mViewControlParent.findViewById(R.id.editor_control_redo_image);
        // editText
        mControlTextEdit = mViewControlText.findViewById(R.id.editor_control_edit);
        mControlTextBold = mViewControlText.findViewById(R.id.editor_control_bold);
        mControlTextItalic = mViewControlText.findViewById(R.id.editor_control_italic);
        mControlTextUnderLine = mViewControlText.findViewById(R.id.editor_control_underline);
        mControlTextFont = mViewControlText.findViewById(R.id.editor_control_font);
        mControlTextColor = mViewControlText.findViewById(R.id.editor_control_color);
        //animation
        animSlideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        animSlideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        //
        setLayoutParams();
    }

    private void setLayoutParams() {
        ViewGroup.LayoutParams params = stickerView.getLayoutParams();
        params.width = mGuestBookHelper.getPageWidth();
        params.height = mGuestBookHelper.getPageHeight();
    }

    private void setListeners() {
        mAbBack.setOnClickListener(this);
        mAbAddFriend.setOnClickListener(this);
        mAbAvatarLayour.setOnClickListener(this);
        mAbSave.setOnClickListener(this);
        mAbPreview.setOnClickListener(this);
        //sticker
        stickerView.setOnStickerOperationListener(this);
        mControlChangeTheme.setOnClickListener(this);
        mControlAddImage.setOnClickListener(this);
        mControlAddText.setOnClickListener(this);
        mControlAddSticker.setOnClickListener(this);
        mControlUndo.setOnClickListener(this);
        mControlRedo.setOnClickListener(this);
        mControlTextEdit.setOnClickListener(this);
        mControlTextBold.setOnClickListener(this);
        mControlTextItalic.setOnClickListener(this);
        mControlTextUnderLine.setOnClickListener(this);
        mControlTextFont.setOnClickListener(this);
        mControlTextColor.setOnClickListener(this);
        setAnimationListener();
    }

    //TODO
    @Override
    public void onStickerOutSideClicked() {
        Log.d(TAG, "onStickerOutSideClicked: ");
        showOrHideControlView(null);
        isShowActionBar = !isShowActionBar;
        showOrHideActionBar();
    }

    @Override
    public void onStickerLayoutBeginChange(Sticker sticker, int position) {
        Log.d(TAG, "onStickerBeginChange: " + position + " isShowActionBar: " + isShowActionBar);
        handleSaveCurrentState(sticker, position);
        showOrHideControlView(sticker);
        if (isShowActionBar) {
            isShowActionBar = false;
            showOrHideActionBar();
        }
    }

    @Override
    public void onStickerBringToFront(Sticker sticker) {
        Log.d(TAG, "onStickerBringToFront");
        if (currentStickerState != null) {
            StickerState newState = new StickerState();
            StickerStack stack = new StickerStack();
            stack.setSticker(sticker);
            stack.setAction(StickerStack.Action.BRING_TO_TOP);
            stack.setOldState(currentStickerState);
            stack.setNewState(newState);
            undoStacks.addLast(stack);
            redoStacks.clear();
            isStateChanged = true;
            setStateControlUndoAndRedo();
            // bring to top không clear current sticker state
        }
    }

    @Override
    public void onStickerClicked(Sticker sticker) {
        Log.d(TAG, "onStickerClicked");
    }

    @Override
    public void onStickerDeleted(Sticker sticker) {
        Log.d(TAG, "onStickerDeleted");
        handleAddOrRemoveStickerTask(sticker, StickerStack.Action.DELETE);
        showOrHideControlView(sticker);
    }

    @Override
    public void onStickerDragFinished(Sticker sticker) {
        Log.d(TAG, "onStickerDragFinished");
        handleAddUndoTask(sticker, StickerStack.Action.MOVE);
    }

    @Override
    public void onStickerZoomFinished(Sticker sticker) {
        Log.d(TAG, "onStickerZoomFinished");
        handleAddUndoTask(sticker, StickerStack.Action.SCALE);
    }

    @Override
    public void onStickerRotateFinished(Sticker sticker) {
        Log.d(TAG, "onStickerRotateFinished");
        handleAddUndoTask(sticker, StickerStack.Action.ROTATE);
    }

    @Override
    public void onStickerFlipped(Sticker sticker) {
        Log.d(TAG, "onStickerFlipped");
    }

    @Override
    public void onStickerDoubleTapped(Sticker sticker) {
        Log.d(TAG, "onDoubleTapped: double tap will be with two click");
    }

    @Override
    public void onEmoItemSelected(EmoItem item) {
        Log.d(TAG, "onEmoItemSelected: " + item.getPath());
        int width = mGuestBookHelper.getItemDefaultWidth();
        int height = mGuestBookHelper.calculateItemDefaultHeight(item.getWidth(), item.getHeight());
        final ImageSticker imageSticker = new ImageSticker(this, null, width, height);
        imageSticker.setOriginalXY(mGuestBookHelper.getCenterX() - width / 2,
                mGuestBookHelper.getCenterY() - height / 2);
        stickerView.addSticker(imageSticker);
        handleAddOrRemoveStickerTask(imageSticker, StickerStack.Action.ADD);
        String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(item.getPath());
        imageSticker.setServerPath(item.getPath());
        ImageLoaderManager.getInstance(this).loadGuestBookBitmap(url, item.getWidth(), item.getHeight(), new
                GlideImageLoader.ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, GlideException e) {

                    }

                    @Override
                    public void onLoadingComplete(Bitmap loadedImage) {
                        Log.d(TAG, "onLoadingComplete: ");
                        imageSticker.setBitmap(loadedImage);
                        stickerView.invalidate();
                    }
                });
    }

    @Override
    public void onBackgroundItemSelected(Background background) {
        Log.d(TAG, "onBackgroundItemSelected: " + background.getPath());
        currentBackground.setPath(background.getPath());
        isChangedBackground = true;
        drawBackground();
        setStateControlUndoAndRedo();
    }

    @Override
    public void onDismissEditor() {
        Log.d(TAG, "onDismissEditor");
    }

    @Override
    public void textContentChanged(@NonNull String text, StickerStack.Action action) {
        if (action == StickerStack.Action.ADD) {
            TextSticker sticker = new TextSticker(this);
            sticker.setText(text);
            sticker.resizeText();
            sticker.setOriginalXY(mGuestBookHelper.getCenterX() - sticker.getWidth() / 2,
                    mGuestBookHelper.getCenterY() - sticker.getHeight() / 2);
            stickerView.addSticker(sticker);
            handleAddOrRemoveStickerTask(sticker, StickerStack.Action.ADD);
            showOrHideControlView(stickerView.getCurrentSticker());
        } else {
            Sticker sticker = stickerView.getCurrentSticker();
            if (sticker != null && sticker instanceof TextSticker) {
                handleSaveCurrentState(sticker, -1);
                TextSticker textSticker = (TextSticker) sticker;
                textSticker.setText(text);
                textSticker.resizeText();
                stickerView.invalidate();
                handleAddUndoTask(sticker, StickerStack.Action.CHANGE_TEXT);
            }
        }
    }

    @Override
    public void textFontChanged(@NonNull String textFont) {
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker != null && sticker instanceof TextSticker) {
            handleSaveCurrentState(sticker, -1);
            TextSticker textSticker = (TextSticker) sticker;
            textSticker.setTextFont(textFont);
            textSticker.resizeText();
            stickerView.invalidate();
            handleAddUndoTask(sticker, StickerStack.Action.CHANGE_TEXT_LAYER);
        }
    }

    @Override
    public void textColorChanged(int textColor) {
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker != null && sticker instanceof TextSticker) {
            handleSaveCurrentState(sticker, -1);
            TextSticker textSticker = (TextSticker) sticker;
            textSticker.setTextColor(textColor);
            stickerView.invalidate();
            handleAddUndoTask(sticker, StickerStack.Action.CHANGE_TEXT_LAYER);
        }
    }

    /////////////////////////////////////////
    private void showOrHideControlView(Sticker currentSticker) {
        if (currentSticker == null || !(currentSticker instanceof TextSticker)) {
            mViewControlParent.setVisibility(View.VISIBLE);
            mViewControlText.setVisibility(View.GONE);
        } else {
            mViewControlParent.setVisibility(View.GONE);
            mViewControlText.setVisibility(View.VISIBLE);
        }
    }

    private void setStateControlUndoAndRedo() {
        if (isChangedBackground || (!undoStacks.isEmpty() && isStateChanged)) {
            mAbSave.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.white));
            mAbSave.setEnabled(true);
        } else {
            mAbSave.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.gray_light));
            mAbSave.setEnabled(false);
        }
        /*if (!isStateChanged || undoStacks.isEmpty()) {
            mAbSave.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.gray_light));
            mAbSave.setEnabled(false);
        } else {
            mAbSave.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.white));
            mAbSave.setEnabled(true);
        }*/
        //
        if (undoStacks.isEmpty()) {
            mControlUndoImage.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.undo_disable));
            mControlUndo.setEnabled(false);
        } else {
            mControlUndoImage.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.undo_enable));
            mControlUndo.setEnabled(true);
        }
        if (redoStacks.isEmpty()) {
            mControlRedoImage.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color
                    .undo_disable));
            mControlRedo.setEnabled(false);
        } else {
            mControlRedoImage.setColorFilter(ContextCompat.getColor(GuestBookEditorActivity.this, R.color.undo_enable));
            mControlRedo.setEnabled(true);
        }
    }

    private void showOrHideActionBar() {
        if (isShowActionBar) {
            mViewActionBar.startAnimation(animSlideDown);
        } else {
            mViewActionBar.startAnimation(animSlideUp);
        }
    }

    private void setAnimationListener() {
        animSlideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mViewActionBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewActionBar.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void undoTask() {
        if (!undoStacks.isEmpty()) {
            StickerStack stack = undoStacks.pollLast();
            redoStacks.addFirst(stack);
            stickerView.undoSticker(stack);
            isStateChanged = true;
            setStateControlUndoAndRedo();
            if (isShowActionBar) {
                isShowActionBar = false;
                showOrHideActionBar();
            }
            //showOrHideControlView(stickerView.getCurrentSticker());
        }
    }

    private void redoTask() {
        if (!redoStacks.isEmpty()) {
            StickerStack stack = redoStacks.pollFirst();
            undoStacks.addLast(stack);
            stickerView.redoSticker(stack);
            isStateChanged = true;
            setStateControlUndoAndRedo();
            if (isShowActionBar) {
                isShowActionBar = false;
                showOrHideActionBar();
            }
            //showOrHideControlView(stickerView.getCurrentSticker());
        }
    }

    private void editTextSticker() {
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker != null && sticker instanceof TextSticker) {
            DialogTextEditor fragment = DialogTextEditor.newInstance(((TextSticker) sticker).getText(),
                    StickerStack.Action.CHANGE_TEXT, mRes.getString(R.string.ok),
                    mRes.getString(R.string.cancel), mRes.getString(R.string.guest_book_enter_content));
            fragment.show(getSupportFragmentManager(), TAG);
        }
    }

    private void changeFontTextSticker() {
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker != null && sticker instanceof TextSticker) {
            TextSticker textSticker = (TextSticker) sticker;
            DialogSelectFont fragment = DialogSelectFont.newInstance(textSticker.getTextFont(),
                    mRes.getString(R.string.select_font), textSticker.getText());
            fragment.show(getSupportFragmentManager(), TAG);
        }
    }

    private void changeColorTextSticker() {
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker != null && sticker instanceof TextSticker) {
            DialogSelectColor fragment = DialogSelectColor.newInstance(((TextSticker) sticker).getTextColor(),
                    mRes.getString(R.string.select_color));
            fragment.show(getSupportFragmentManager(), TAG);
        }
    }

    private void changeTextLayer(int action) {
        Sticker sticker = stickerView.getCurrentSticker();
        if (sticker != null && sticker instanceof TextSticker) {
            handleSaveCurrentState(sticker, -1);
            TextSticker textSticker = (TextSticker) sticker;
            if (action == com.android.editor.sticker.utils.Constants.CHANGE_TEXT_BOLD) {
                textSticker.setTextBold(textSticker.getTextBold() == 1 ? 0 : 1);
            } else if (action == com.android.editor.sticker.utils.Constants.CHANGE_TEXT_ITALIC) {
                textSticker.setTextItalic(textSticker.getTextItalic() == 1 ? 0 : 1);
            } else {
                textSticker.setTextUnderLine(textSticker.getTextUnderLine() == 1 ? 0 : 1);
            }
            textSticker.resizeText();
            stickerView.invalidate();
            handleAddUndoTask(sticker, StickerStack.Action.CHANGE_TEXT_LAYER);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    public void testLock(View view) {
        stickerView.setLocked(!stickerView.isLocked());
    }

    public void testRemove(View view) {
        if (stickerView.removeCurrentSticker()) {
            Toast.makeText(GuestBookEditorActivity.this, "Remove current Sticker successfully!", Toast.LENGTH_SHORT)
                    .show();
        } else {
            Toast.makeText(GuestBookEditorActivity.this, "Remove current Sticker failed!", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void testRemoveAll(View view) {
        stickerView.removeAllStickers();
    }

    public void reset(View view) {
        stickerView.removeAllStickers();
        //loadSticker();
    }

    //////////////////////undo *** redo///////////////////
    private void handleSaveCurrentState(Sticker sticker, int position) {
        currentStickerState = new StickerState();
        currentStickerState.setWidth(sticker.getWidth());
        currentStickerState.setHeight(sticker.getHeight());
        currentStickerState.setPosition(position);
        currentStickerState.setStickerPoints(sticker.getMappedBoundPoints());
        currentStickerState.setRotate(sticker.getCurrentAngle());
        if (sticker instanceof TextSticker) {
            TextSticker textSticker = (TextSticker) sticker;
            currentStickerState.setText(textSticker.getText());
            currentStickerState.setTextStyle(textSticker.getTextFont(), textSticker.getTextColor(),
                    textSticker.getTextBold(), textSticker.getTextItalic(), textSticker.getTextUnderLine());
        }
    }

    private void handleAddOrRemoveStickerTask(Sticker sticker, StickerStack.Action action) {
        // add undo stack
        StickerState state = new StickerState();
        state.setRotate(sticker.getCurrentAngle());
        state.setWidth(sticker.getWidth());
        state.setHeight(sticker.getHeight());
        if (sticker instanceof TextSticker) {
            TextSticker textSticker = (TextSticker) sticker;
            state.setText(textSticker.getText());
            state.setTextStyle(textSticker.getTextFont(), textSticker.getTextColor(),
                    textSticker.getTextBold(), textSticker.getTextItalic(), textSticker.getTextUnderLine());
        }
        state.setStickerPoints(sticker.getMappedBoundPoints());
        StickerStack stack = new StickerStack();
        stack.setSticker(sticker);
        stack.setAction(action);
        stack.setOldState(state);
        stack.setNewState(state);
        undoStacks.addLast(stack);
        redoStacks.clear();
        isStateChanged = true;
        setStateControlUndoAndRedo();
    }

    private void handleAddUndoTask(Sticker sticker, StickerStack.Action action) {
        if (currentStickerState != null) {
            StickerState newState = new StickerState();
            switch (action) {
                case MOVE:
                    newState.setStickerPoints(sticker.getMappedBoundPoints());
                    break;
                case SCALE:
                    newState.setWidth(sticker.getWidth());
                    newState.setHeight(sticker.getHeight());
                    break;
                case ROTATE:
                    newState.setRotate(sticker.getCurrentAngle());
                    break;
                case CHANGE_TEXT:
                    if (sticker instanceof TextSticker) {
                        newState.setText(((TextSticker) sticker).getText());
                    }
                    break;
                case CHANGE_TEXT_LAYER:
                    if (sticker instanceof TextSticker) {
                        TextSticker textSticker = (TextSticker) sticker;
                        newState.setText(textSticker.getText());
                        newState.setTextStyle(textSticker.getTextFont(), textSticker.getTextColor(),
                                textSticker.getTextBold(), textSticker.getTextItalic(),
                                textSticker.getTextUnderLine());
                    }
                    break;
                default:
                    newState.setStickerPoints(sticker.getMappedBoundPoints());
                    break;
            }
            StickerStack stack = new StickerStack();
            stack.setSticker(sticker);
            stack.setAction(action);
            stack.setOldState(currentStickerState);
            stack.setNewState(newState);
            undoStacks.addLast(stack);
            redoStacks.clear();// có sự kiện add vào undo task thì clear redo stack
            currentStickerState = null;
            isStateChanged = true;
            setStateControlUndoAndRedo();
        }
    }

    @Override
    public void onBackPressed() {
        if (isChangedBackground || (!undoStacks.isEmpty() && isStateChanged)) {
            new DialogConfirm(GuestBookEditorActivity.this, true).setLabel(null).setMessage(mRes.getString(R.string.guest_book_confirm_close_editor)).
                    setNegativeLabel(mRes.getString(R.string.no)).setPositiveLabel(mRes.getString(R.string.yes)).setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    finish();
                }
            }).setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    handleSavePage(true);
                }
            }).show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (PermissionHelper.verifyPermissions(grantResults)) {
            if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_GALLERY) {
                handlePickImage();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setActivityForResult(false);
        setTakePhotoAndCrop(false);
        Log.d(TAG, "onActivityResult: " + requestCode + " -- " + resultCode);
        switch (requestCode) {
            case Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED:
                if (data != null) {
                    //String contactName = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME);
                    String jidNumber = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
                    currentPage.setAssigned(jidNumber);
                    drawActionBar();
                }
                break;
            case Constants.ACTION.ACTION_PICK_PICTURE:
                if (data != null) {
                    ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                    if (picturePath != null) {
                        handleCropImage(picturePath.get(0));
                    }
                }
                break;
            case Constants.ACTION.ACTION_CROP_IMAGE:
                if (data != null) {
                    handleCropResult(data);
                }
                break;
        }
    }

    private void handleCropResult(Intent data) {
        String filePath = data.getStringExtra(CropActivity.FILE_OUTPUT_PATH);
        tempFilePaths.add(filePath);
        int width = data.getIntExtra(CropActivity.FILE_OUTPUT_WIDTH, 100);
        int height = data.getIntExtra(CropActivity.FILE_OUTPUT_HEIGHT, 100);
        int maxW = (int) (GuestBookHelper.getInstance(mApplication).getPageWidth() * 0.85);
        int maxH = (int) (GuestBookHelper.getInstance(mApplication).getPageHeight() * 0.85);
        Log.d(TAG, "before width: " + width + " -- " + height);
        if (width > maxW) {
            float scale = (float) height / width;
            width = maxW;
            height = (int) (width * scale);
        }
        if (height > maxH) {//sau khi giới hạn widh thì giới hạn height
            float scale1 = (float) width / height;
            height = maxH;
            width = (int) (height * scale1);
        }
        Log.d(TAG, "after width: " + width + " -- " + height);

        final ImageSticker imageSticker = new ImageSticker(this, null, width, height);
        imageSticker.setOriginalXY(mGuestBookHelper.getCenterX() - width / 2, mGuestBookHelper.getCenterY() - height / 2);
        stickerView.addSticker(imageSticker);
        handleAddOrRemoveStickerTask(imageSticker, StickerStack.Action.ADD);
        imageSticker.setFilePath(filePath);
        //String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(item.getPath());
        ImageLoaderManager.getInstance(this).loadGuestBookFileBitmap(filePath, width, height, new
                GlideImageLoader.ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, GlideException e) {

                    }

                    @Override
                    public void onLoadingComplete(Bitmap loadedImage) {
                        Log.d(TAG, "handleCropResult onLoadingComplete: ");
                        imageSticker.setBitmap(loadedImage);
                        stickerView.invalidate();
                    }
                });
    }

    private void handleCropImage(String filePath) {
        File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                Config.Storage.IMAGE_COMPRESSED_FOLDER, "/guest_book_" +
                System.currentTimeMillis() + Constants.FILE.PNG_FILE_SUFFIX);
        Log.d(TAG, "filePath:" + filePath + " -- " + cropImageFile.getPath());
        Intent intent = new Intent(GuestBookEditorActivity.this, CropActivity.class);
        intent.putExtra(CropActivity.FILE_PATH, filePath);
        intent.putExtra(CropActivity.FILE_OUTPUT_PATH, cropImageFile.getPath());
        startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);
    }

    private void handlePickImage() {
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        setActivityForResult(true);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    private void handlePickEmoSticker() {
        if (mGuestBookHelper.getEmoCollections().isEmpty()) {
            showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestEmoCollection(new GuestBookHelper.GetDataListener() {
                @Override
                public void onSuccess() {
                    hideLoadingDialog();
                    DialogSelectEmoSticker selectSticker = DialogSelectEmoSticker.
                            newInstance(mGuestBookHelper.getEmoCollections());
                    selectSticker.show(getSupportFragmentManager(), TAG);
                }

                @Override
                public void onError(int error) {
                    hideLoadingDialog();
                    showToast(R.string.e601_error_but_undefined);
                }
            });
        } else {
            DialogSelectEmoSticker selectSticker = DialogSelectEmoSticker.newInstance(
                    mGuestBookHelper.getEmoCollections());
            selectSticker.show(getSupportFragmentManager(), TAG);
        }
    }

    private void handlePickBackground() {
        if (mGuestBookHelper.getBackgrounds().isEmpty()) {
            showLoadingDialog(null, R.string.waiting);
            mGuestBookHelper.requestBackgrounds(new GuestBookHelper.GetDataListener() {
                @Override
                public void onSuccess() {
                    hideLoadingDialog();
                    DialogSelectBackground selectBackground = DialogSelectBackground.newInstance(mGuestBookHelper
                            .getBackgrounds());
                    selectBackground.show(getSupportFragmentManager(), TAG);
                }

                @Override
                public void onError(int error) {
                    hideLoadingDialog();
                    showToast(R.string.e601_error_but_undefined);
                }
            });
        } else {
            DialogSelectBackground selectBackground = DialogSelectBackground.newInstance(mGuestBookHelper
                    .getBackgrounds());
            selectBackground.show(getSupportFragmentManager(), TAG);
        }
    }

    private void handleAssignPage() {
        String bookId = currentPage.getBookId();
        int pageId = currentPage.getPage();
        Intent assign = new Intent(GuestBookEditorActivity.this, ChooseContactActivity.class);
        assign.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED);
        assign.putExtra(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID, bookId);
        assign.putExtra(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID, pageId);
        startActivityForResult(assign, Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED, true);
    }

    private void handleClickAvatarAssigned() {
        String numberAssigned = currentPage.getAssigned();
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        if (numberAssigned != null && numberAssigned.equals(accountBusiness.getJidNumber())) {
            NavigateActivityHelper.navigateToMyProfile(GuestBookEditorActivity.this);
        } else {
            Intent profile = new Intent(GuestBookEditorActivity.this, ContactDetailActivity.class);
            Bundle bundle = new Bundle();
            PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(numberAssigned);
            if (phoneNumber != null && phoneNumber.getId() != null) {
                bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                bundle.putString(NumberConstant.ID, phoneNumber.getId());

            } else {
                StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(numberAssigned);
                if (strangerPhoneNumber != null) {
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
                    bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
                    bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
                    bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
                } else {
                    bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                    bundle.putString(NumberConstant.NUMBER, numberAssigned);
                }
            }
            profile.putExtras(bundle);
            startActivity(profile, true);
        }
    }

    private void drawLayoutParamPageNumber() {
        FrameLayout.LayoutParams pageParams = (FrameLayout.LayoutParams) mTvwPageNumber.getLayoutParams();
        int textWidth = (int) (mGuestBookHelper.getPageWidth() * 0.05);
        int textMargin = (int) (mGuestBookHelper.getPageHeight() * 0.015f);
        pageParams.width = textWidth;
        pageParams.height = textWidth;
        pageParams.setMargins(0, 0, textMargin, textMargin);
        mTvwPageNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, textWidth * 0.60f);
    }

    ////////////////////////////////////////*************************************//////////////////////////////
    private void drawDetail() {
        if (currentPage.getBackground() == null) {
            currentBackground = new Background();
            currentBackground.setWidth(GuestBookHelper.DEFAULT_WIDTH);
            currentBackground.setHeight(GuestBookHelper.DEFAULT_HEIGHT);
        } else {
            currentBackground = new Background(currentPage.getBackground());
        }
        initScaleValue();
        drawBackground();
        drawActionBar();
        drawSticker();
        drawPageNumber();
    }

    private void drawPageNumber() {
        if (currentPage.getPosition() < 0) {
            mTvwPageNumber.setVisibility(View.GONE);
            mAbTitle.setText("");
        } else if (currentPage.getPosition() == 0) {
            mTvwPageNumber.setVisibility(View.GONE);
            mAbTitle.setText(mRes.getString(R.string.guest_book_page_cover));
        } else {
            mTvwPageNumber.setVisibility(View.VISIBLE);
            mTvwPageNumber.setText(String.valueOf(currentPage.getPosition()));
            drawLayoutParamPageNumber();
            mAbTitle.setText(String.format(mRes.getString(R.string.guest_book_page_number), currentPage.getPosition()));
        }
    }

    private void drawBackground() {
        if (TextUtils.isEmpty(currentBackground.getPath())) {
            mImgBackground.setImageDrawable(ContextCompat.getDrawable(mApplication, R.drawable.transparent));
            ImageLoaderManager.getInstance(mApplication).cancelDisplayTag(mImgBackground);
        } else {
            String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(currentBackground.getPath());
            ImageLoaderManager.getInstance(this).displayGuestBookPreview(mImgBackground, url);
        }
    }

    private void drawActionBar() {
        //TODO không xử lý hiển thị assigned nữa
        /*String numberAssigned = currentPage.getAssigned();
        if (TextUtils.isEmpty(numberAssigned)) {
            mAbAvatarLayour.setVisibility(View.GONE);
            mAbAddFriend.setVisibility(View.VISIBLE);
        } else {
            mAbAvatarLayour.setVisibility(View.VISIBLE);
            mAbAddFriend.setVisibility(View.GONE);
            drawAvatarActionBar(numberAssigned);
        }*/
    }

    private void drawAvatarActionBar(@NonNull String jidNumber) {
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        if (jidNumber.equals(accountBusiness.getJidNumber())) {
            mApplication.getAvatarBusiness().setMyAvatar(mAbAvatarImage, accountBusiness.getCurrentAccount());
            mAbAvatarText.setVisibility(View.GONE);
        } else {
            int size = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(jidNumber);
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mAbAvatarImage, mAbAvatarText, phoneNumber, size);
            } else {
                mApplication.getAvatarBusiness().setUnknownNumberAvatar(mAbAvatarImage,
                        mAbAvatarText, jidNumber, size);
            }
        }
    }

    private void drawSticker() {
        int minSize = mGuestBookHelper.getMinSize();
        ArrayList<PageItem> pageItems = currentPage.getPageItems();
        String fontDefault = currentPage.getCommon().getDefaultFont();
        String colorDefault = currentPage.getCommon().getDefaultColor();
        if (pageItems != null) {
            for (PageItem item : pageItems) {
                float rotation = StickerUtils.convertRadianToDegrees(item.getRotate());
                int width = (int) (item.getWidth() * scaleToClient);
                int height = (int) (item.getHeight() * scaleToClient);
                width = width >= minSize ? width : minSize;
                height = height >= minSize ? height : minSize;
                Log.d(TAG, "drawSticker: " + width + " --- " + height);
                if ("text".equals(item.getType())) {
                    String fontName = item.getTextFont() == null ? fontDefault : item.getTextFont();
                    String fontColor = item.getTextColor() == null ? colorDefault : item.getTextColor();
                    TextSticker textSticker = new TextSticker(mApplication, width, height);
                    textSticker.setOriginalRotate(rotation);
                    textSticker.setOriginalXY((int) (item.getX() * scaleToClient), (int) (item.getY() * scaleToClient));
                    textSticker.setOriginalWidth(width);
                    textSticker.setOriginalHeight(height);
                    textSticker.setText(item.getText());
                    textSticker.setTextStyle(fontName, StickerUtils.convertColorStringToInt(fontColor),
                            item.getTextBold(), item.getTextItalic(), item.getTextUnderLine());
                    //textSticker.setTextColor(Color.BLUE);
                    textSticker.resizeText();
                    stickerView.addSticker(textSticker);
                } else {
                    final ImageSticker imageSticker = new ImageSticker(this, null, width, height);
                    imageSticker.setOriginalRotate(rotation);
                    imageSticker.setOriginalXY((int) (item.getX() * scaleToClient),
                            (int) (item.getY() * scaleToClient));
                    imageSticker.setOriginalWidth(width);
                    imageSticker.setOriginalHeight(height);
                    imageSticker.setServerPath(item.getPath());
                    stickerView.addSticker(imageSticker);
                    String url = UrlConfigHelper.getInstance(mApplication).getConfigGuestBookUrl(imageSticker
                            .getServerPath());
                    ImageLoaderManager.getInstance(this).loadGuestBookBitmap(url, width, height, new
                            GlideImageLoader.ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted() {

                                }

                                @Override
                                public void onLoadingFailed(String imageUri, GlideException e) {

                                }

                                @Override
                                public void onLoadingComplete(Bitmap loadedImage) {
                                    Log.d(TAG, "onLoadingComplete: " + stickerView.getWidth()
                                            + " --" + stickerView.getHeight());
                                    imageSticker.setBitmap(loadedImage);

                                    stickerView.invalidate();
                                }
                            });
                }
            }
        }
        showOrHideControlView(stickerView.getCurrentSticker());
        isStateChanged = false;
        setStateControlUndoAndRedo();
    }

    private void initScaleValue() {
        float deviceHeight = (float) mGuestBookHelper.getPageHeight();
        float height;
        if (currentBackground != null) {
            height = currentBackground.getHeight() > 0 ? currentBackground.getHeight() : GuestBookHelper.DEFAULT_HEIGHT;
        } else {
            height = GuestBookHelper.DEFAULT_HEIGHT;
        }
        scaleToClient = deviceHeight / height;
        scaleToServer = height / deviceHeight;
        Log.d(TAG, "initScaleValue: " + scaleToClient + " --- " + scaleToServer);
    }

    private void handleSavePage(boolean isBackPress) {
        stickerView.clearFocus();
        if (savePageAsyncTask != null) {
            savePageAsyncTask.cancel(true);
            savePageAsyncTask = null;
        }
        savePageAsyncTask = new SavePageAsyncTask(isBackPress);
        savePageAsyncTask.execute();
    }

    private SavePageAsyncTask savePageAsyncTask;

    private class SavePageAsyncTask extends AsyncTask<Void, Void, Void> {
        private boolean isBackPress;
        private boolean uploadFail;
        private String previewPath;
        private List<Sticker> stickers;
        private ArrayList<PageItem> pageItems;

        public SavePageAsyncTask(boolean isBackPress) {
            this.isBackPress = isBackPress;
            this.uploadFail = false;
        }

        @Override
        protected void onPreExecute() {
            showLoadingDialog(null, R.string.waiting);
            stickers = stickerView.getStickers();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            long t = System.currentTimeMillis();
            // upload preview
            previewPath = uploadMyPreview();
            if (TextUtils.isEmpty(previewPath)) {
                uploadFail = true;
                return null;
            }
            pageItems = new ArrayList<>();
            // upload
            int itemId = 1;
            for (Sticker sticker : stickers) {
                PageItem item = new PageItem();
                item.setId(itemId);
                item.setRotate(StickerUtils.roundAngle(sticker.getCurrentAngleRadian(), 3));
                /*int width = (int) (sticker.getWidth() * scaleToServer);
                int height = (int) (sticker.getHeight() * scaleToServer);*/
                Log.d(TAG, "with-height: " + sticker.getWidth() + "***" + sticker.getHeight());
                item.setWidth(sticker.getWidth() * scaleToServer);
                item.setHeight(sticker.getHeight() * scaleToServer);
                PointF centerPoint = sticker.getMappedCenterPoint();
                Log.d(TAG, "x,y: " + centerPoint.x + "***" + centerPoint.y);
                Log.d(TAG, "scale: " + scaleToServer + " screen height: " + mGuestBookHelper.getPageHeight());
                float x = (centerPoint.x - sticker.getWidth() / 2) * scaleToServer;
                float y = (centerPoint.y - sticker.getHeight() / 2) * scaleToServer;
                item.setX(x);
                item.setY(y);
                if (sticker instanceof ImageSticker) {
                    item.setType(Constants.GUEST_BOOK.TYPE_IMAGE);
                    ImageSticker imageSticker = (ImageSticker) sticker;
                    if (TextUtils.isEmpty(imageSticker.getServerPath()) && !TextUtils.isEmpty(imageSticker.getFilePath())) {
                        File file = new File(imageSticker.getFilePath());
                        String path = mGuestBookHelper.requestUploadFile(file);
                        if (TextUtils.isEmpty(path)) {
                            uploadFail = true;
                            Log.d(TAG, "upload Fail -> return ????");
                        } else {
                            imageSticker.setServerPath(path);
                        }
                    }
                    item.setPath(imageSticker.getServerPath());
                } else {
                    item.setType(Constants.GUEST_BOOK.TYPE_TEXT);
                    TextSticker textSticker = (TextSticker) sticker;
                    item.setText(textSticker.getText());
                    item.setTextFont(textSticker.getTextFont());
                    item.setTextColor(StickerUtils.convertColorIntToString(textSticker.getTextColor()));
                    item.setTextBold(textSticker.getTextBold());
                    item.setTextItalic(textSticker.getTextItalic());
                    item.setTextUnderLine(textSticker.getTextUnderLine());
                }
                itemId++;
                pageItems.add(item);
            }
            Log.d(TAG, "process save take: " + (System.currentTimeMillis() - t));
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (uploadFail || TextUtils.isEmpty(previewPath)) {
                hideLoadingDialog();
                showToast(R.string.e601_error_but_undefined);
            } else {
                final Page newPage = new Page(currentPage);
                newPage.setBackground(currentBackground);
                newPage.setPageItems(pageItems);
                newPage.setPreview(previewPath);
                mGuestBookHelper.requestSavePage(newPage, new GuestBookHelper.ProcessBookListener() {
                    @Override
                    public void onSuccess(String desc) {
                        isStateChanged = false;
                        isChangedBackground = false;
                        hideLoadingDialog();
                        showToast(desc, Toast.LENGTH_LONG);
                        setStateControlUndoAndRedo();
                        //currentPage = newPage;// gan lai cho current page để xử lý tiếp
                        mGuestBookHelper.setCurrentPageEditor(newPage);
                        //mGuestBookHelper.setNeedLoadMyBooks(true);
                        mGuestBookHelper.setNeedChangedPageData(true);
                        if (isBackPress) {
                            finish();
                        }
                    }

                    @Override
                    public void onError(String desc) {
                        hideLoadingDialog();
                        showToast(desc, Toast.LENGTH_LONG);
                    }
                });
            }
        }
    }

    private String uploadMyPreview() {
        File previewImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                Config.Storage.IMAGE_COMPRESSED_FOLDER, "/guest_book_preview_" + System.currentTimeMillis() + Constants.FILE.JPEG_FILE_SUFFIX);
        tempFilePaths.add(previewImageFile.getPath());
        Bitmap bitmap = Bitmap.createBitmap(stickerView.getWidth(), stickerView.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        stickerView.draw(canvas);
        ImageHelper.getInstance(mApplication).saveBitmapToPath(bitmap, previewImageFile.getPath(), Bitmap.CompressFormat.JPEG);
        String path = mGuestBookHelper.requestUploadFile(previewImageFile);
        bitmap.recycle();
        return path;
    }
}
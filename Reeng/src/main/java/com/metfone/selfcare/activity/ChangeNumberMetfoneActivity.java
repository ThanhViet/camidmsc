package com.metfone.selfcare.activity;

import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.metfone.selfcare.adapter.PhoneMetAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.model.account.AddServiceRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseServiceRequestInfo;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.account.UpdateLoginInfo;
import com.metfone.selfcare.model.account.UpdateLoginRequest;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_WS_ISDN;

public class ChangeNumberMetfoneActivity extends BaseSlidingFragmentActivity {
    Unbinder unbinder;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.btnDone)
    TextView btnDone;
    @BindView(R.id.rvChangeNumber)
    RecyclerView rvChangeNumber;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    ApplicationController mApplication;
    private PhoneMetAdapter phoneMetAdapter;
    UserInfoBusiness userInfoBusiness;
    UserInfo currentUser;
    ArrayList<ServicesModel> servicesModelList;
    BaseMPSuccessDialog baseMPSuccessDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number_metfone);
        unbinder = ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplication();
        userInfoBusiness = new UserInfoBusiness(this);
        currentUser = userInfoBusiness.getUser();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        initView();
    }

    private void initView() {
        phoneMetAdapter = new PhoneMetAdapter(this);
        rvChangeNumber.setAdapter(phoneMetAdapter);
        getData();
    }

    @Optional
    @OnClick({R.id.btnBack, R.id.btnDone})
    public void onClick(View view) {
        // ...
        switch (view.getId()) {
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnDone:
                ServicesModel model = servicesModelList.get(phoneMetAdapter.getLastSelectedPosition());
                updateMetfone("", model.getPhone_number(), model.getUser_service_id());
                break;
        }
    }
    /**
     * Change phone number
     * @param phoneNumber
     */
    private void updateMetfone(String otp, String phoneNumber, int userServiceId) {
        pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.updateMetfoneProfile(phoneNumber, otp, userServiceId, new ApiCallback<GetUserResponse>() {
            @Override
            public void onResponse(Response<GetUserResponse> response) {
                if (response.body() != null) {
//                    ToastUtils.showToast(ChangeNumberMetfoneActivity.this, response.body().getMessage());
                    if ("00".equals(response.body().getCode())){
                        mApplication.getCamIdUserBusiness().setForceUpdateMetfone(true);
                        currentUser = response.body().getData().getUser();
                        userInfoBusiness.setUser(currentUser);
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        baseMPSuccessDialog = new BaseMPSuccessDialog(ChangeNumberMetfoneActivity.this, R.drawable.img_dialog_1, getString(R.string.title_successfully_setting_up), String.format(getString(R.string.content_success_change_metfone), "0"+phoneNumber.substring(0, 2) + " " + phoneNumber.substring(2)), false);
                        baseMPSuccessDialog.show();
                        ApplicationController.self().getCamIdUserBusiness().setMetfoneUsernameIsdn(phoneNumber);
                        UserInfo userInfo = new UserInfo();
                        EventBus.getDefault().postSticky(userInfo);
                        getSupportFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    ChangeNumberMetfoneActivity.this.finish();
                                }
                            });
                        }
                    }
                }
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
            }
        });

    }

    private void getData() {
        List<Services> services = userInfoBusiness.getServiceList();
        servicesModelList = UserInfoBusiness.convertServicesToServiceModelList(services);
        phoneMetAdapter.setItems(servicesModelList);
        if (servicesModelList.size() > 0){
            phoneMetAdapter.setLastSelectedPosition(foundIndex());
            phoneMetAdapter.notifyDataSetChanged();
        }
        phoneMetAdapter.notifyDataSetChanged();
    }

    private int foundIndex() {
        for (int i = 0; i < servicesModelList.size(); i++) {
            if (servicesModelList.get(i).getMetfonePlus() == 1) {
                return i;
            }
        }
        return 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
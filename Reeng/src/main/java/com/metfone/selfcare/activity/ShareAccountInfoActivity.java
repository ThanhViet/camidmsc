package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import org.json.JSONObject;

/**
 * Created by toanvk2 on 08/09/2015.
 */
public class ShareAccountInfoActivity extends BaseSlidingFragmentActivity implements InitDataListener {
    private final String TAG = ShareAccountInfoActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private Handler mHandler;
    private ImageView mBtnBack;
    private Button mBtnSkip, mBtnAccept;
    private TextView mTvwContent;
    private CircleImageView mImgOtherApp, mImgUserAvatar;
    private TextView mTvwTitle;
    private ProgressLoading mPgrLoading;
    private LinearLayout mLlDetail, mLlBtn;
    private ReengAccount mReengAccount;
    private String appId = "", appName, appIconUrl;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_share_account);
        changeStatusBar(true);
        mApplication = (ApplicationController) getApplicationContext();
        mRes = this.getResources();
        findComponentViews();
        setViewListener();
        getDataFromIntent(saveInstanceState);
        trackingScreen(TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        ListenerHelper.getInstance().addInitDataListener(this);
        if (mHandler == null) {
            mHandler = new Handler();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ListenerHelper.getInstance().removeInitDataListener(this);
        mHandler = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDataReady() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                drawDetail();
            }
        });
    }

    private void findComponentViews() {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View toolCustomView = inflater.inflate(R.layout.ab_title_center, null);
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(toolCustomView);

        mBtnSkip = (Button) findViewById(R.id.btn_skip);
        mBtnAccept = (Button) findViewById(R.id.btn_accept);
        mTvwContent = (TextView) findViewById(R.id.share_account_content_txt);
        mImgOtherApp = (CircleImageView) findViewById(R.id.other_app_icon);
        mImgUserAvatar = (CircleImageView) findViewById(R.id.user_mocha_avatar);
        mPgrLoading = (ProgressLoading) findViewById(R.id.loading_progressbar);
        mLlDetail = (LinearLayout) findViewById(R.id.share_acc_detail_ll);
        mLlBtn = (LinearLayout) findViewById(R.id.share_acc_btn_ll);
        //tool bar
        mBtnBack = (ImageView) toolCustomView.findViewById(R.id.ab_back_btn);
        mBtnBack.setVisibility(View.GONE);
        mTvwTitle = (TextView) toolCustomView.findViewById(R.id.ab_title);
        mTvwTitle.setText(getString(R.string.title_share_account));
    }

    private void setViewListener() {
        mBtnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appId == null) {
                    appId = "";
                }
                trackingEvent(mRes.getString(R.string.ga_category_share_account), appId, mRes.getString(R.string.ga_label_skip));
                finish();
            }
        });
        mBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnData(mApplication.getReengAccountBusiness().getCurrentAccount());
            }
        });
    }

    private void showOrHideLoading(boolean isShow) {
        if (isShow) {
            mPgrLoading.setVisibility(View.VISIBLE);
            mLlDetail.setVisibility(View.GONE);
            mLlBtn.setVisibility(View.GONE);
        } else {
            mPgrLoading.setVisibility(View.GONE);
            mLlBtn.setVisibility(View.VISIBLE);
            mLlDetail.setVisibility(View.VISIBLE);
        }
    }

    private void getDataFromIntent(Bundle saveInstanceState) {
        /*ComponentName cm = getCallingActivity();
        if (cm != null) {
            String callingPackageName = cm.getPackageName();
            Log.d(TAG, "callingPackageName: " + callingPackageName);
        }*/
        Intent intent = getIntent();
        if (saveInstanceState != null) {
            appId = saveInstanceState.getString(Constants.MOCHA_INTENT.EXTRA_INTENT_APP_ID);
            appIconUrl = saveInstanceState.getString(Constants.MOCHA_INTENT.EXTRA_INTENT_APP_ICON);
            appName = saveInstanceState.getString(Constants.MOCHA_INTENT.EXTRA_INTENT_APP_NAME);
        } else if (intent != null) {
            if (Constants.MOCHA_INTENT.ACTION_SHARE_ACCOUNT.equals(intent.getAction()) &&
                    Constants.MOCHA_INTENT.INTENT_TYPE.equals(intent.getType())) {
                appId = intent.getStringExtra(Constants.MOCHA_INTENT.EXTRA_INTENT_APP_ID);
                appIconUrl = intent.getStringExtra(Constants.MOCHA_INTENT.EXTRA_INTENT_APP_ICON);
                appName = intent.getStringExtra(Constants.MOCHA_INTENT.EXTRA_INTENT_APP_NAME);
                Log.d(TAG, "getdata other appId: " + appId + " appName: " + appName + " appIcon: " + appIconUrl);
            } else {
                Log.d(TAG, "getdata other app: else----------");
                gotoHomeActivity();
                return;
            }
        } else {
            return;
        }
        if (mApplication.isDataReady()) {
            drawDetail();
        } else {
            showOrHideLoading(true);
        }
    }

    private void drawDetail() {
        showOrHideLoading(false);
        mReengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        if (mReengAccount == null || !mReengAccount.isActive()) {
            trackingEvent(mRes.getString(R.string.ga_category_share_account),
                    appId == null ? "" : appId, mRes.getString(R.string.ga_label_error));
            NavigateActivityHelper.navigateToLoginActivity(ShareAccountInfoActivity.this, false);
        } else {
            AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
            avatarBusiness.setMyAvatar(mImgUserAvatar, mReengAccount);
            avatarBusiness.setLogoOtherApp(mImgOtherApp, appIconUrl);
            mTvwContent.setText(String.format(mRes.getString(R.string.msg_share_account), appName));
        }
    }

    private void returnData(ReengAccount account) {
        Intent returnIntent = new Intent();
        JSONObject accountObject = new JSONObject();
        try {
            accountObject.put(Constants.HTTP.REST_MSISDN, account.getJidNumber());
            if (account.getName() != null)// ten
                accountObject.put(Constants.HTTP.USER_INFOR.NAME, account.getName());
            if (account.getBirthdayString() != null)// birthday "yyyy-MM-dd"
                accountObject.put(Constants.HTTP.USER_INFOR.BIRTHDAY_STRING, account.getBirthdayString());
            if (account.getStatus() != null)//status
                accountObject.put(Constants.HTTP.USER_INFOR.STATUS, account.getStatus());
            String avatarUrl = mApplication.getAvatarBusiness().
                    getAvatarUrl(account.getLastChangeAvatar(), account.getJidNumber(), 400, account.getAvatarVerify());

            if (!TextUtils.isEmpty(avatarUrl)) {//avatar_url
                accountObject.put(Constants.HTTP.USER_INFOR.AVATAR_URL, avatarUrl);
            }
            //kieu int MALE = 1,FEMALE = 0;
            accountObject.put(Constants.HTTP.USER_INFOR.GENDER, account.getGender());
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (appId == null) {
            appId = "";
        }
        trackingEvent(mRes.getString(R.string.ga_category_share_account), appId, mRes.getString(R.string.ga_label_success));
        returnIntent.putExtra(Constants.MOCHA_INTENT.EXTRA_INTENT_SHARE_ACCOUNT, accountObject.toString());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void gotoHomeActivity() {
        if (appId == null) {
            appId = "";
        }
        trackingEvent(mRes.getString(R.string.ga_category_share_account),
                appId, mRes.getString(R.string.ga_label_error));
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, false);
        finish();
    }
}

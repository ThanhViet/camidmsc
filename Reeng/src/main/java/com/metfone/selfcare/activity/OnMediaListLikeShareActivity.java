package com.metfone.selfcare.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.metfone.selfcare.adapter.UserActionAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.fragment.onmedia.ListUserLikeFragment;
import com.metfone.selfcare.fragment.onmedia.ListUserShareFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.httprequest.InviteFriendHelper;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 1/11/2016.
 */
public class OnMediaListLikeShareActivity extends BaseSlidingFragmentActivity implements
        UserActionAdapter.FeedUserInteractionListener {

    private static final String TAG = OnMediaListLikeShareActivity.class.getSimpleName();
    private int fragment = -1;
    private ApplicationController mApplication;
    private Resources mRes;
    private FeedBusiness mFeedBusiness;
    private String jidNumber;
    private EventOnMediaHelper eventOnMediaHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fragment = bundle.getInt(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT);
        }
        mApplication = (ApplicationController) getApplicationContext();
        jidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        mFeedBusiness = mApplication.getFeedBusiness();
        eventOnMediaHelper = new EventOnMediaHelper(this);
        mRes = getResources();
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        setActionBar();
        getDataAndDisplayFragment();
        trackingScreen(TAG);
    }

    public void setActionBar() {
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(getLayoutInflater().inflate(
                R.layout.ab_detail, null));
    }

    private void getDataAndDisplayFragment() {
        if (fragment == Constants.ONMEDIA.LIST_LIKE) {
            displayListLikeFragment();
        } else if (fragment == Constants.ONMEDIA.LIST_SHARE) {
            displayListShareFragment();
        } else {
            Log.i(TAG, "what the f*ck");
        }
    }

    private void displayListLikeFragment() {
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        ListUserLikeFragment listUserLikeFragment = ListUserLikeFragment.newInstance(url);
        executeFragmentTransaction(listUserLikeFragment, R.id.fragment_container, false, true);
    }

    private void displayListShareFragment() {
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        ListUserShareFragment listUserShareFragment = ListUserShareFragment.newInstance(url);
        executeFragmentTransaction(listUserShareFragment, R.id.fragment_container, false, true);
    }

    @Override
    public void onChatClick(UserInfo userInfo) {
        checkUserAndGotoChatDetail(userInfo);
    }

    @Override
    public void navigateToProfile(UserInfo userInfo) {
        if (userInfo != null) {
            eventOnMediaHelper.processUserClick(userInfo);
        }
    }

    private void checkUserAndGotoChatDetail(UserInfo userInfo) {
        String friendJid = userInfo.getMsisdn();
        String friendName = userInfo.getName();
        String avatar = userInfo.getAvatar();
        ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendJid);
        if (account == null || TextUtils.isEmpty(friendJid)) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (myNumber != null && myNumber.equals(friendJid)) {
            showToast(mRes.getString(R.string.msg_not_send_me), Toast.LENGTH_SHORT);
        } else if (phoneNumber != null) {
            if (phoneNumber.isReeng()) {
                ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(friendJid);
                gotoChatDetailActivity(thread);
            } else {
                if (!mApplication.getReengAccountBusiness().isViettel() || !phoneNumber.isViettel()) {
                    ArrayList<String> listInvite = new ArrayList<>();
                    listInvite.add(friendJid);
                    InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, this,
                            userInfo.getName(), userInfo.getMsisdn(), false);
//                    InviteFriendHelper.getInstance().inviteFriends(mApplication, OnMediaListLikeShareActivity.this, listInvite, false);
                } else {
                    ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(friendJid);
                    gotoChatDetailActivity(thread);
                }
            }
        } else {
            if (userInfo.isUserMocha()) {
                ThreadMessage threadMessage = mApplication.getStrangerBusiness().
                        createMochaStrangerAndThread(friendJid, friendName, avatar, Constants.CONTACT.STRANGER_MOCHA_ID, true);
                gotoChatDetailActivity(threadMessage);
            } else {
//                showToast(R.string.number_not_user_mocha);
                /*if (!PhoneNumberHelper.getInstant().isViettelNumber(userInfo.getMsisdn()) ||
                        !mApplication.getReengAccountBusiness().isViettel()) {
                    InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, this,
                            userInfo.getName(), userInfo.getMsisdn(), false);
                } else {*/
                    ThreadMessage thread = mApplication.getStrangerBusiness().
                            createMochaStrangerAndThread(friendJid, friendName, avatar, Constants.CONTACT.STRANGER_MOCHA_ID, true);
                    gotoChatDetailActivity(thread);
//                }
            }
        }
    }

    public void gotoChatDetailActivity(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(OnMediaListLikeShareActivity.this, threadMessage);
    }
}

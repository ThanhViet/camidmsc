package com.metfone.selfcare.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.manager.SupportRequestManagerFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.module.home_kh.fragment.game.GameFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewards.RewardsKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.RewardsShopKHFragment;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.metfoneplus.fragment.MPAddFeedbackFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPBuyServiceDetailFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPServiceFragment;
import com.metfone.selfcare.module.metfoneplus.ishare.fragment.IShareFragment;
import com.metfone.selfcare.module.metfoneplus.search.fragment.BuyPhoneNumberFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.module.movienew.fragment.ActionCategoryFragment;
import com.metfone.selfcare.module.movienew.fragment.MyListFragment;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeepLinkActivity extends BaseSlidingFragmentActivity {
    public static final String TYPE_DEEP_LINK = "type";
    public static final String PARAM_URL_GAME = "url_game";
    public static final String PARAM_ID = "id";
    public static final String PARAM_IS_EXCHANGE = "is_exchange";
    public static final String PARAM_SERVICE_DETAIL = "service_detail";
    public static final String PARAM_EXCHANGE_ITEM = "exchange_item";
    public static final int TYPE_GAME = 1;
    public static final int TYPE_FILM_CATEGORY = 2;
    public static final int TYPE_REWARD_DETAIL = 3;
    public static final int TYPE_REWARD_CATEGORY = 4;

    public static final int TYPE_ACCOUNT_INFO = 5;
    public static final int TYPE_SERVICES = 6;
    public static final int TYPE_TOPUP = 7;
    public static final int TYPE_BUY_PHONE = 8;
    public static final int TYPE_ISHARE = 9;
    public static final int TYPE_FEEDBACK = 10;
    public static final int TYPE_ACTOR = 11;
    public static final int TYPE_DIRECTOR = 12;
    public static final int TYPE_DETAIL_SERVICE = 13;
    public static final int TYPE_CATEGORY_METFONE = 14;

    private int type = 0;
    private String urlGame = "";
    private String id = "";
    private boolean isExchange = false;
    private WsGetServiceDetailResponse.Response serviceDetail;
    private ExchangeItem exchangeItem;

    @BindView(R.id.root_frame)
    FrameLayout root_frame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        type = getIntent().getIntExtra(TYPE_DEEP_LINK, 0);
        id = getIntent().getStringExtra(PARAM_ID);
        urlGame = getIntent().getStringExtra(PARAM_URL_GAME);
        isExchange = getIntent().getBooleanExtra(PARAM_IS_EXCHANGE, false);
        serviceDetail = (WsGetServiceDetailResponse.Response) getIntent().getSerializableExtra(PARAM_SERVICE_DETAIL);
        exchangeItem = (ExchangeItem) getIntent().getSerializableExtra(PARAM_EXCHANGE_ITEM);

        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);

        setContentView(R.layout.activity_deep_link);
        ButterKnife.bind(this);

        if (type != TYPE_CATEGORY_METFONE) {
            Utilities.adaptViewForInsertBottomKeepTop(root_frame);
        }
        handleDeepLink();

        DeepLinkHelper.uriDeepLink = null;
    }

    private void handleDeepLink() {
        Fragment fragment = null;
        switch (type) {
            case TYPE_GAME:
                addFragment(R.id.root_frame, GameFragment.newInstance(urlGame), false);
                break;
            case TYPE_FILM_CATEGORY:
                root_frame.setPadding(root_frame.getPaddingLeft(), Utilities.dpToPixel(R.dimen.dimen34dp, getResources()), root_frame.getPaddingRight(), root_frame.getPaddingBottom());
                String categoryNameId = id;
                fragment = ActionCategoryFragment.newInstance(categoryNameId, "", ActionCategoryFragment.CATEGORY);
                break;
            case TYPE_REWARD_DETAIL:
                GiftItems g = new GiftItems();
                g.setGiftId(id);
                fragment = RewardsDetailKHFragment.newInstance(g);
                break;
            case TYPE_REWARD_CATEGORY:
                fragment = RewardsShopKHFragment.newInstance(id, true);
                break;
            case TYPE_ACCOUNT_INFO:
                fragment = MPDetailsFragment.newInstance();
                ((MPDetailsFragment) fragment).setIsShowAnimation(false);
                break;
            case TYPE_TOPUP:
                fragment = TopupMetfoneFragment.newInstance();
                break;
            case TYPE_SERVICES:
                fragment = MPServiceFragment.newInstance(MPServiceFragment.TAB_BUY_SERVICE);
                break;
            case TYPE_BUY_PHONE:
                fragment = BuyPhoneNumberFragment.newInstance(false);
                break;
            case TYPE_ISHARE:
                fragment = IShareFragment.newInstance();
                break;
            case TYPE_FEEDBACK:
                fragment = MPAddFeedbackFragment.newInstance();
                break;
            case TYPE_ACTOR:
                fragment = MyListFragment.newInstance(getString(R.string.movie_info_actor), id);
                break;
            case TYPE_DIRECTOR:
                fragment = MyListFragment.newInstance(getString(R.string.director), id);
                break;
            case TYPE_DETAIL_SERVICE: {
                if (isExchange) {
                    fragment = MPBuyServiceDetailFragment.newInstance(exchangeItem);
                } else {
                    fragment = MPBuyServiceDetailFragment.newInstance(serviceDetail);
                }
                break;
            }
            case TYPE_CATEGORY_METFONE:
                fragment = RewardsKHFragment.newInstance();
                break;
            default:
                finish();
        }
        if (fragment != null) {
            addFragment(R.id.root_frame, fragment, false);
        }
    }

    @Override
    public void onBackPressed() {
        int fragmentSize = getSupportFragmentManager().getFragments().size();
        if (fragmentSize > 0) {
            Fragment fragment = getSupportFragmentManager().getFragments().get(fragmentSize - 1);
            if (type == TYPE_FEEDBACK
                    || fragment instanceof SupportRequestManagerFragment
                    || fragment instanceof RewardsShopKHFragment
                    || fragment instanceof MPDetailsFragment
                    || fragment instanceof TopupMetfoneFragment
                    || fragment instanceof BuyPhoneNumberFragment
                    || fragment instanceof IShareFragment
                    || fragment instanceof MPServiceFragment
                    || fragment instanceof ActionCategoryFragment
                    || fragment instanceof MPAddFeedbackFragment
                    || fragment instanceof GameFragment
                    || fragment instanceof RewardsDetailKHFragment
                    || fragment instanceof MyListFragment
                    || fragment instanceof MPBuyServiceDetailFragment
                    || fragment instanceof RewardsKHFragment
            ) {
                if (fragment instanceof MPServiceFragment) {
                    ((MPServiceFragment) fragment).handlePopBackStackFromDeepLink();
                } else
                    super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();
        }
    }
}

package com.metfone.selfcare.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import lombok.Getter;
import retrofit2.Response;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.metfone.esport.entity.event.OnWindowFocusEvent;
import com.metfone.esport.base.BaseFragment;
import com.metfone.esport.common.Common;
import com.metfone.esport.common.Navigator;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.ScreenStateReceiver;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.AVNOHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.ChargeMoneyAVNOListener;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.EventAppListener;
import com.metfone.selfcare.listeners.ScreenStateListener;
import com.metfone.selfcare.model.camid.PhoneLinked;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.games.activity.PlayGameActivity;
import com.metfone.selfcare.module.home_kh.activity.BackupKhActivity;
import com.metfone.selfcare.module.home_kh.activity.FeedbackKhActivity;
import com.metfone.selfcare.module.home_kh.activity.MyQRCodeKhActivity;
import com.metfone.selfcare.module.home_kh.activity.QRCodeKhActivity;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.module.keeng.KeengPlayerActivity;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.CategoryModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.DialogLoadData;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DialogMediaInfo;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetAdapter;
import com.metfone.selfcare.module.keeng.widget.buttonSheet.BottomSheetData;
import com.metfone.selfcare.module.newdetails.SlideImage.SlideImageActivity;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.search.activity.SearchAllActivity;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.restful.GsonRequest;
import com.metfone.selfcare.ui.dialog.BottomSheetChargerCard;
import com.metfone.selfcare.ui.dialog.BottomSheetDialog;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogLoading;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.ReportVideoDialog;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.widget.ColorUiUtil;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.metfone.selfcare.module.keeng.utils.ConvertHelper.convertKeengToVideoMocha;

/**
 * Created by toanvk2 on 7/8/14.
 */

public class BaseSlidingFragmentActivity extends AppCompatActivity implements EventAppListener,
        ClickListener.IconListener, ScreenStateListener/*, KeyboardVisibilityEventListener*/ {
    protected final String TAG = getClass().getSimpleName();
    //    protected static String TAG = "";
    protected DialogLoading mLoadingDialog;
    //Xu ly popup more Module Keeng
    BottomSheetDialog mBottomSheetFirst;
    BottomSheetDialog mBottomSheetSecond;
    GsonRequest lastRequest;
    @Getter
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Toolbar mToolBar;
    private ApplicationController applicationController;
    private MessageBusiness mMessageBusiness;
    private ApplicationStateManager mAppStateManager;
    private Handler mHandler;
    private ClickListener.IconListener mClickHandler;
    private ScreenStateReceiver mScreenStateReceiver;
    private CallbackManager callbackManager;
    private boolean isHideStatus = false;
    public  boolean isFromInternet = false;
    private BottomSheetChargerCard bottomSheetChargerCard;
    private Dialog currentPrefixDialog;
    private long lastClick;

    public BaseSlidingFragmentActivity() {
        // mTitleRes = titleRes;
    }





    @Override
    public void onCreate(Bundle savedInstanceState) {
        applicationController = (ApplicationController) getApplication();
        applicationController.setCurrentActivity(this);
        mMessageBusiness = applicationController.getMessageBusiness();
        mAppStateManager = applicationController.getAppStateManager();
        mFragmentManager = getSupportFragmentManager();
        super.onCreate(savedInstanceState);
        ImageLoaderManager.getInstance(this).setHeightNotificationBar(this.getResources());
        callbackManager = CallbackManager.Factory.create();
        //changeStatusBar(true);
        // set SystemUI & hide navigation
//        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
//        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setTransitionOnCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    public void changeStatusBar(boolean isShow) {
        //code works on android 4.4.4 & later
        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            //set color
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= 21) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                if (isShow) {
                    // window.setStatusBarColor(getResources().getColor(R.color.status_bar));
                    window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.status_bar));
                } else {
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
            }
        }
    }

    public void changeStatusBar(int color) {
        //code works on android 4.4.4 & later
        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            //set color
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= 21) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.status_bar));
                window.setStatusBarColor(color);
            }
        }
    }

    public void setWhiteStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
            }
        }


    }

    public void setColorStatusBar(@ColorRes int colorRes) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, colorRes));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, colorRes));
            }
        }
    }

    public Toolbar getToolBarView() {
        return mToolBar;
    }

    public void setToolBar(View mToolBar) {
        this.mToolBar = (Toolbar) mToolBar;
    }

    public void setCustomViewToolBar(View v) {
        if (mToolBar == null) return;
        this.mToolBar.removeAllViews();
        this.mToolBar.addView(v, new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        mToolBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        //unregistrar = KeyboardVisibilityEvent.registerEventListener(this, this);
        Log.d(TAG, " ---> onStart");
        mAppStateManager.applicationEnterForeground(this, false);
        super.onStart();
        mAppStateManager.notifyActivityStarted(this);
        initScreenStateReceiver();
        registerScreenReceiver();
    }

    public void onResume() {
        super.onResume();
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mClickHandler = this;
        PopupHelper.getInstance().setCurrentActivity(this);
        try {
            checkConnection();
        } catch (Exception ex) {
            android.util.Log.d(TAG, "onResume: " + ex.getMessage());
        }
        mMessageBusiness.addEventAppListener(this);
        applicationController.setCurrentActivity(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d(TAG, "[] onPostResume: ");
    }

    @Override
    protected void onPause() {
        InputMethodUtils.hideSoftKeyboard(this);
        mClickHandler = null;
        mHandler = null;
        Log.d(TAG, " ---> onPause: ");
        mMessageBusiness.removeEventAppListener();
        //clearReferences();
        super.onPause();
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, " ---> onBackPressed: ");
        InputMethodUtils.hideSoftKeyboard(this);
        if ((this instanceof HomeActivity || this instanceof QuickReplyActivity || this instanceof
                QuickMissCallActivity || this instanceof SearchAllActivity)) {
            mAppStateManager.setBackPressed(false);
        } else {
            mAppStateManager.setBackPressed(true);
        }
//        popBackStackFragment();
        super.onBackPressed();
        /*if ((this instanceof QuickReplyActivity) || (this instanceof QuickMissCallActivity)
                || (this instanceof HomeActivity) || this instanceof PreviewImageActivity) {
            return;
        }*/// TODO ko xu ly gi
        /*if (this instanceof OnMediaActivityNew) {
            int fragment = ((OnMediaActivityNew) this).getFragment();
            if (fragment == Constants.ONMEDIA.COMMENT) {
                Log.i(TAG, "-------------overridePendingTransition cmt");
                overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
            } else {
                Log.i(TAG, "-------------overridePendingTransition normal");
                overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            }
        } else {
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        }*/
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);

        if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
            ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
        }
        android.util.Log.e("BaseSlidingFragmentAct", "onBackPressed: isProcessing = " + ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        Log.d(TAG, "onWindowFocusChanged: " + hasFocus);
        if (mAppStateManager.isBackPressed() && !hasFocus) {
            mAppStateManager.setBackPressed(false);
            mAppStateManager.setWindowFocused(true);
        } else {
            mAppStateManager.setWindowFocused(hasFocus);
        }
        if (!mAppStateManager.isTakePhotoAndCrop() && mAppStateManager.isActivityForResult() && hasFocus) {
            setActivityForResult(false);
        }
        if (hasFocus && mAppStateManager.isAppWentToBg()) {
            mAppStateManager.applicationEnterForeground(this, false);
        }
        EventBus.getDefault().post(new OnWindowFocusEvent(hasFocus));
        super.onWindowFocusChanged(hasFocus);
    }

    public void setActivityForResult(boolean isActivityForResult) {
        mAppStateManager.setActivityForResult(isActivityForResult);
        Log.d(TAG, "setActivityForResult: " + isActivityForResult);
    }

    public void setTakePhotoAndCrop(boolean isTakePhotoAndCrop) {
        mAppStateManager.setTakePhotoAndCrop(isTakePhotoAndCrop);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, " ---> onStop: isScreenOn--> " + mAppStateManager.isScreenOn()
                + " windownfocus: " + mAppStateManager.isWindowFocused() + " isActivityForResult: " +
                mAppStateManager.isActivityForResult());
        if (!mAppStateManager.isScreenOn() || (!mAppStateManager.isWindowFocused() && !mAppStateManager
                .isActivityForResult())) {
            mAppStateManager.applicationEnterBackground(this, false);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        //InputMethodUtils.fixSoftInputLeaks(this);
        //if (unregistrar != null) unregistrar.unregister();
        if (mAppStateManager.isActivityForResult()) {
            setActivityForResult(false);
        }
        if (mAppStateManager.isTakePhotoAndCrop()) {
            setTakePhotoAndCrop(false);
        }
        Log.d(TAG, " ---> onDestroy");
        clearReferences();
        PopupHelper.getInstance().releaseCurrentActivity(this);
        super.onDestroy();
        hideLoadingDialog();// to avoid crash exception ViewAncestor is attached to null
        unRegisterScreenReceiver();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    public void startActivity(Intent intent, boolean animation) {
        startActivity(intent);
//        if (animation) {
//            overridePendingTransition(R.anim.decelerate_slide_in_right, R.anim.decelerate_slide_out_left);
//        }
    }

    public void fakeBackActivity(Intent intent) {
        startActivity(intent);
        //overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    public void startActivityForResult(Intent intent, int requestCode, boolean animation) {
        startActivityForResult(intent, requestCode);
        /*if (animation) {
            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }*/
    }

    public void hideLoadingDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

    public void showError(int resourceErrorId, String title) {
        showError(getResources().getString(resourceErrorId), title);
    }

    public void showError(String errorMessage, String title) {
        if (!isFinishing())
            new DialogConfirm(this, true).setLabel(title).setMessage(errorMessage).
                    setNegativeLabel(getResources().getString(R.string.close)).show();
    }

    /**
     * show loading dialog
     *
     * @param title             title of dialog
     * @param messageResourceId String resource id of message on dialog
     */
    public void showLoadingDialog(String title, int messageResourceId) {
        showLoadingDialog(title, getString(messageResourceId));
    }

    public void showLoadingDialog(final String title, final String message) {
        showLoadingDialog(title, message, false);
    }

    public void showLoadingDialog(final String title, final String message, final boolean cancelable) {
        if (isFinishing()) return;
        runOnUiThread(() -> {
            if (mLoadingDialog == null) {
                mLoadingDialog = new DialogLoading(BaseSlidingFragmentActivity.this, cancelable);
            }
            mLoadingDialog.show();
        });
    }

    public void showToast(final String msg, final int duration) {
        runOnUiThread(() -> {
            /*inflateToast();
            if (TextUtils.isEmpty(msg)) {
                mTvwToastContent.setText(R.string.e601_error_but_undefined);
            } else {
                mTvwToastContent.setText(msg);
            }
            mToast.setDuration(duration);
            mToast.show();*/
            com.metfone.selfcare.v5.utils.ToastUtils.showToast(BaseSlidingFragmentActivity.this, msg);
        });
    }

    public void showToast(final int stringResourceId) {
        runOnUiThread(() -> {
            /*inflateToast();
            mTvwToastContent.setText(stringResourceId);
            mToast.setDuration(Toast.LENGTH_LONG);
            mToast.show();*/
            com.metfone.selfcare.v5.utils.ToastUtils.showToast(BaseSlidingFragmentActivity.this, getResources().getString(stringResourceId));
        });
    }

    public void showToastDone(final String content) {
        runOnUiThread(() -> {
            /*inflateToast();
            mTvwToastContent.setText(stringResourceId);
            mToast.setDuration(Toast.LENGTH_LONG);
            mToast.show();*/
            com.metfone.selfcare.v5.utils.ToastUtils.showToast(BaseSlidingFragmentActivity.this, content, Toast.LENGTH_SHORT, 0, com.metfone.selfcare.v5.utils.ToastUtils.ToastType.done);
        });
    }

    public void showToast(final String msg) {
        showToast(msg, Toast.LENGTH_LONG);
    }

    /**
     * execute replacing fragment transaction
     *
     * @param fragment         Fragment to be display
     * @param isAddToBackStack true if could navigate
     */
    protected void executeFragmentTransaction(Fragment fragment, int fragmentParentId,
                                              boolean isAddToBackStack, boolean isAnimations) {
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
       /* if (isAnimations) {
            mFragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        }*/
            // Add the fragment to the 'fragment_container' FrameLayout
            mFragmentTransaction.replace(fragmentParentId, fragment); //change from raplace to add
//        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if (isAddToBackStack) {
                mFragmentTransaction.addToBackStack(null);
            }
            mFragmentTransaction.commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected void executeFragmentTransitionWithAnimation(Fragment fragment, int fragmentParentId,
                                                          boolean isAddToBackStack) {
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.setCustomAnimations(R.anim.decelerate_slide_in_right,
                    R.anim.decelerate_slide_out_left_fragment,
                    R.anim.decelerate_slide_in_left_fragment,
                    R.anim.decelerate_slide_out_right
            );
            // Add the fragment to the 'fragment_container' FrameLayout
            mFragmentTransaction.replace(fragmentParentId, fragment); //change from raplace to add
//        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if (isAddToBackStack) {
                mFragmentTransaction.addToBackStack(null);
            }
            mFragmentTransaction.commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected void executeAndRemoveCurrentFragmentTransaction(Fragment fragment, int fragmentParentId) {
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentManager.popBackStackImmediate();
            mFragmentTransaction.replace(fragmentParentId, fragment); //change from raplace to add
            mFragmentTransaction.addToBackStack(null);
            mFragmentTransaction.commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected void executeFragmentTransaction(Fragment fragment, int fragmentParentId,
                                              boolean isAddToBackStack, boolean isAnimations, String tag) {
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
        /*if (isAnimations) {
            mFragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        }*/
            // Add the fragment to the 'fragment_container' FrameLayout
            mFragmentTransaction.replace(fragmentParentId, fragment, tag); //change from raplace to add
            //        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if (isAddToBackStack) {
                mFragmentTransaction.addToBackStack(null);
            }
            mFragmentTransaction.commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void executeFragmentTransactionAllowLoss(Fragment fragment, int fragmentParentId,
                                                    boolean isAddToBackStack, boolean isAnimations, String tag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        /*if (isAnimations) {
            mFragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        }*/
        // Add the fragment to the 'fragment_container' FrameLayout
        mFragmentTransaction.replace(fragmentParentId, fragment, tag); //change from raplace to add
        //        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isAddToBackStack) {
            mFragmentTransaction.addToBackStack(null);
        }
        mFragmentTransaction.commitAllowingStateLoss();
    }

    protected void executeAddFragmentTransaction(Fragment fragment, int fragmentParentId,
                                                 boolean isAddToBackStack, boolean isAnimations) {
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            if (isAnimations) {
                mFragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            }
            mFragmentTransaction.add(fragmentParentId, fragment); //change from raplace to add
            if (isAddToBackStack) {
                mFragmentTransaction.addToBackStack(null);
            }
            mFragmentTransaction.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void executeFragmentTransactionAllowLossAnimation(Fragment fragment, int fragmentParentId,
                                                             boolean isAddToBackStack, String tag) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(
                R.anim.fif_slide_in,  // enter
                R.anim.fif_fade_out,  // exit
                R.anim.fif_fade_in,   // popEnter
                R.anim.fif_slide_out  // popExit
        );
        // Add the fragment to the 'fragment_container' FrameLayout
        mFragmentTransaction.replace(fragmentParentId, fragment, tag); //change from raplace to add
        //        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isAddToBackStack) {
            mFragmentTransaction.addToBackStack(null);
        }
        mFragmentTransaction.commitAllowingStateLoss();
    }

    protected void executeAddFragmentTransaction(Fragment fragment, int fragmentParentId, String tag) {
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.add(fragmentParentId, fragment, tag);
            mFragmentTransaction.commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected void removeFragment(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.remove(fragment).commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected void detachFragment(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        try {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.detach(fragment).commit();
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    protected void clearBackStack() {
        try {
            for (int i = 0; i < mFragmentManager.getBackStackEntryCount(); ++i) {
                mFragmentManager.popBackStack();
            }
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    /**
     * kiem tra ket noi XMPP khi vao mot so man hinh
     */
    public void checkConnection() {
        Log.i(TAG, " --->  checkConnection ...");
        if (this instanceof LoginActivity || this instanceof RestoreActivity || this instanceof SetUpProfileActivity) {
            return;
        }
        //DeviceHelper.checkOptimizationMode(this);
        ReengAccountBusiness accountBusiness = applicationController.getReengAccountBusiness();
        //neu chua co account nao dang nhap thi ra man hinh login
        //neu da co account dang nhap nhung token bi expire thi ra man hinh login voi thong bao "token expire"
        //neu da co account dang nhap va token hop le thi vao man hinh chinh
        //neu revision cu thi khong reconect
        if (accountBusiness.isValidRevision()) {
            if (!applicationController.getXmppManager().isAuthenticated() &&
                    accountBusiness.isValidAccount() &&
                    NetworkHelper.isConnectInternet(getApplicationContext())) {
                //neu co mang va chua ket noi toi XMPP thi thuc hien ket noi
//                if (IMService.isReady()) {
                if (applicationController.isReady()) {
                    Log.i(TAG, " ---> connectByToken when network is available");
//                    IMService.getInstance().connectByToken();
                    applicationController.connectByToken();
                } else {
//                    startService(new Intent(this, IMService.class));
                    applicationController.startIMService();
                }
            }
        } else {
            Log.i(TAG, "not valid revision");
            if (!applicationController.getReengAccountBusiness().isShowForceDialog()) {
                applicationController.getReengAccountBusiness().setShowForceDialog(true);
                PopupHelper.getInstance().
                        showDialogForceUpdate(this, accountBusiness.getMsgForceUpdate(),
                                accountBusiness.getUrlForceUpdate(), mClickHandler, Constants.MENU.POPUP_FORCE_UPDATE);
            }
        }
    }

    public static boolean fromRestartApp = false;
    public void restartApp() {
        if (this instanceof LoginActivity) {
            return;
        }
        ToastUtils.makeText(this, "Account is login by another user!");
        try {
            new ReengAccountBusiness(applicationController).deactivateAccount(getApplicationContext());
            new UserInfoBusiness(getApplicationContext()).clearCache();
        } catch (XMPPException e) {
            e.printStackTrace();
        }

        applicationController.cancelNotification(Constants.NOTIFICATION.NOTIFY_MESSAGE);
        applicationController.cancelNotification(Constants.NOTIFICATION.NOTIFY_ONMEDIA);
        applicationController.cancelNotification(Constants.NOTIFICATION.NOTIFY_OTHER);
        ((ApplicationController) getApplication()).getXmppManager().destroyXmpp();
        applicationController.removeCountNotificationIcon();
        applicationController.setRegisterSmsOtp(false);
        //logout facebook + reinit
        if (!Config.Server.FREE_15_DAYS) {
            LoginManager.getInstance().logOut();
        }
        // reset music
        applicationController.getMusicBusiness().clearSessionAndNotifyMusic();
        if (applicationController != null && applicationController.getReengAccountBusiness() != null && applicationController.getReengAccountBusiness().getCurrentAccount() != null){
            applicationController.getReengAccountBusiness().getCurrentAccount().setNumberJid("");
        }
//        doRestart(this);
//        triggerRebirth(this);
        fromRestartApp = true;
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent, false);
//        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void trackingScreen(String screenName) {
        //applicationController.trackingScreen(screenName);
    }

    /**
     * gui thong ke ga
     *
     * @param categoryId
     * @param actionId
     * @param labelId
     */
    public void trackingEvent(int categoryId, int actionId, int labelId) {
        applicationController.trackingEvent(categoryId, actionId, labelId);
    }

    /**
     * gui thong ke ga
     *
     * @param categoryId
     * @param action
     * @param label
     */
    public void trackingEvent(int categoryId, String action, String label) {
        applicationController.trackingEvent(getResources().getString(categoryId), action, label);
    }

    public void trackingEvent(int categoryId, int actionId, String label) {
        applicationController.trackingEvent(getResources().getString(categoryId), getResources().getString(actionId),
                label);
    }

    public void trackingEvent(String category, String action, String label) {
        applicationController.trackingEvent(category, action, label);
    }

    @Override
    public void notifyForceUpdate(final String msg, final String link, final boolean isForce) {
        if (!isForce)  // khong bat buoc cap nhat
            return;
        mHandler.post(() -> {
            applicationController.getReengAccountBusiness().setShowForceDialog(true);
            PopupHelper.getInstance().
                    showDialogForceUpdate(BaseSlidingFragmentActivity.this, msg, link,
                            mClickHandler, Constants.MENU.POPUP_FORCE_UPDATE);
        });
    }

    @Override
    public void notifyAuthenConflict() {
        if (mHandler == null)
            return;
        Log.d(TAG, " ---> notifyAuthenConflict");
        mHandler.post(this::restartApp);
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        if (menuId == Constants.MENU.POPUP_FORCE_UPDATE) {
            String url = (String) entry;
            UrlConfigHelper.openBrowser(this, url);
        }
    }

    @Override
    public void onScreenOn() {
        Log.i(TAG, " ---> Screen went ON");
        if (this instanceof QuickReplyActivity || this instanceof QuickMissCallActivity) {
            return;
        }
        mAppStateManager.applicationEnterForeground(this, false);
    }

    @Override
    public void onScreenOff() {
        if (!mAppStateManager.isActivityForResult()) {
            mAppStateManager.applicationEnterBackground(this, false);
        }
        if (this instanceof QuickReplyActivity || this instanceof QuickMissCallActivity) {
            finish();
        }
        Log.i(TAG, " ---> Screen went OFF");
    }

    private void initScreenStateReceiver() {
        Log.i(TAG, " ---> initScreenStateReceiver");
        if (mScreenStateReceiver == null) {
            mScreenStateReceiver = new ScreenStateReceiver();
            mScreenStateReceiver.setStateListener(this);
        }
    }

    //   screen on/off
    protected void registerScreenReceiver() {
        Log.i(TAG, " ---> registerScreenReceiver");
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, filter);
    }

    protected void unRegisterScreenReceiver() {
        Log.i(TAG, " ---> unRegisterScreenReceiver");
        try {
            if (mScreenStateReceiver != null) {
                unregisterReceiver(mScreenStateReceiver);
                mScreenStateReceiver = null;
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    public void goToHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        fakeBackActivity(intent);
        clearBackStack();
        finish();
    }

    public void shareImageFacebook(Bitmap bitmap, String gaLabel) {
        FacebookHelper facebookHelper = new FacebookHelper(this);
        facebookHelper.shareImageToFb(bitmap, callbackManager, this, gaLabel);
    }

    public void shareImageOnMedia(ArrayList<String> listFilePath) {
        Intent intent = new Intent(this, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_ACTION, Constants.ONMEDIA.ACTION_ATTACH_IMAGE);
        intent.putStringArrayListExtra(Constants.ONMEDIA.EXTRAS_LIST_IMAGE, listFilePath);
        startActivity(intent);
    }

    protected void goToContactDetail(PhoneNumber phoneNumber) {
        Intent contactDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
        bundle.putString(NumberConstant.ID, phoneNumber.getId());
        contactDetail.putExtras(bundle);
        startActivity(contactDetail, true);
    }

    protected void goToStrangerDetail(String friendJid, String friendName,
                                      String lcAvatar, String status, int gender) {
        StrangerPhoneNumber strangerPhoneNumber = applicationController.getStrangerBusiness()
                .getExistStrangerPhoneNumberFromNumber(friendJid);
        Intent strangerDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        if (strangerPhoneNumber != null) {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
            bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
        } else {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
            bundle.putString(NumberConstant.NAME, friendName);
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, lcAvatar);
            bundle.putString(NumberConstant.STATUS, status);
            bundle.putInt(NumberConstant.GENDER, gender);
        }
        strangerDetail.putExtras(bundle);
        startActivity(strangerDetail, true);
    }

    public CallbackManager getCallbackManager() {
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
            Log.i(TAG, "reinit callbackmangater");
        }
        return callbackManager;
    }

    public void hideKeyboard() {
        Handler handler = new Handler();
        handler.postDelayed(() -> InputMethodUtils.hideSoftKeyboard(BaseSlidingFragmentActivity.this), 200);
    }

    public void hideStatusBar() {
        if (isHideStatus) return;
        Log.i(TAG, "hide");
        isHideStatus = true;
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public void showStatusBar() {
        if (!isHideStatus) return;
        Log.i(TAG, "show");
        isHideStatus = false;
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // show the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    public void finish() {
        super.finish();
        setTransitionFinish();
    }

    protected void setTransitionOnCreate() {
        if (this instanceof HomeActivity || this instanceof VideoPlayerActivity
                || this instanceof QuickReplyActivity) {

        } else if (this instanceof SearchAllActivity) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (this instanceof ChatActivity) {
            overridePendingTransition(R.anim.fast_fade_in, R.anim.fast_fade_out);
        } else if (this instanceof RecallActivity) {
            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        } else if (this instanceof SettingActivity
                || this instanceof SettingKhActivity
                || this instanceof FeedbackKhActivity
                || this instanceof BackupKhActivity
                || this instanceof FeedbackActivity
                || this instanceof BackupActivity
                || this instanceof QRCodeKhActivity
                || this instanceof MyQRCodeKhActivity
                || this instanceof LockAppActivity) {
            overridePendingTransition(R.anim.decelerate_slide_in_right, R.anim.decelerate_slide_out_left);
//        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit);
//            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_down);
        }
    }

    protected void setTransitionFinish() {
        if (this instanceof HomeActivity || this instanceof VideoPlayerActivity
                || this instanceof QuickReplyActivity) {

        } else if (this instanceof SearchAllActivity) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (this instanceof ChatActivity) {
            overridePendingTransition(R.anim.fast_fade_in, R.anim.fast_fade_out);
        } else if (this instanceof RecallActivity || this instanceof OnMediaViewImageActivity ||
                this instanceof PreviewImageActivity || this instanceof SlideImageActivity) {
            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        } else if (this instanceof SettingActivity
                || this instanceof FeedbackActivity
                || this instanceof BackupActivity
                || this instanceof LockAppActivity) {
            overridePendingTransition(R.anim.decelerate_slide_in_left, R.anim.decelerate_slide_out_right);
        } else {
            overridePendingTransition(R.anim.activity_left_to_right_enter, R.anim.activity_left_to_right_exit);
//        overridePendingTransition(R.anim.popup_enter, R.anim.popup_exit);
//            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_down);
//            overridePendingTransition(R.anim.slide_bottom_in, R.anim.slide_top_out);
        }
    }

    public void showDialogChargeMoney(final ChargeMoneyAVNOListener listener) {
        bottomSheetChargerCard = new BottomSheetChargerCard(BaseSlidingFragmentActivity.this, true)
                .setListener((cardNumber, cardSeri) -> {
                    BaseSlidingFragmentActivity.this.showLoadingDialog("", R.string.loading);
                    AVNOHelper.getInstance((ApplicationController) getApplication()).chargePaymentScratchCard
                            (BaseSlidingFragmentActivity.this, cardNumber, cardSeri,
                                    new AVNOHelper.ChargePaymentListener() {

                                        @Override
                                        public void onSuccess(String msg) {
                                            BaseSlidingFragmentActivity.this.hideLoadingDialog();
                                            bottomSheetChargerCard.dismiss();
                                            showToast(msg);
                                            if (listener != null)
                                                listener.onChargeMoneySuccess();
                                        }

                                        @Override
                                        public void onError(int code, String msg) {
                                            showToast(msg);
                                            if (listener != null)
                                                listener.onChargeError(msg);
                                        }
                                    });
                });
        bottomSheetChargerCard.show();
    }

    private void clearReferences() {
        BaseSlidingFragmentActivity currActivity = applicationController.getCurrentActivity();
        if (currActivity == null) return;
        if (this.equals(currActivity))
            applicationController.setCurrentActivity(null);
    }

    public void dismissPrefixDialog() {
        if (currentPrefixDialog != null && currentPrefixDialog.isShowing())
            currentPrefixDialog.dismiss();
    }

    public void setCurrentPrefixDialog(Dialog currentPrefixDialog) {
        this.currentPrefixDialog = currentPrefixDialog;
    }

    public void dismissDialogFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            try {
                DialogFragment dialogFragment = (DialogFragment) fragmentManager.findFragmentByTag(tag);
                if (dialogFragment != null) {
                    dialogFragment.dismissAllowingStateLoss();
                }
            } catch (Exception e) {
                Log.e(TAG, "dismissDialogFragment: " + tag, e);
            }
        }
    }

    public boolean isDialogShowing(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            try {
                DialogFragment dialogFragment = (DialogFragment) fragmentManager.findFragmentByTag(tag);
                if (dialogFragment != null && dialogFragment.isAdded()) {
                    return true;
                }
            } catch (Exception e) {
                Log.e(TAG, "dismissDialogFragment: " + tag, e);
            }
        }
        return false;
    }

    public void showPopupMore(final AllModel item) {
        if (item == null)
            return;
        List<CategoryModel> datas;
        switch (item.getType()) {
            case Constants.TYPE_ALBUM:
                datas = BottomSheetData.getPopupOfAlbum(item);
                break;
            case Constants.TYPE_SONG:
                datas = BottomSheetData.getPopupOfSong(item, isPlayingMedia(), true, false, false, false);
                break;
            case Constants.TYPE_VIDEO:
                datas = BottomSheetData.getPopupOfVideo(item);
                break;
//            case Constants.TYPE_PLAYLIST:
//                datas = BottomSheetData.getPopupOfPlaylist(item);
//                break;
            default:
                datas = null;
                break;
        }
        if (datas == null || datas.isEmpty())
            return;
        showPopupMore(datas, item);
    }

    public void showPopupMore(final PlayListModel item) {
        dismissBottomSheetFirst();
        if (item == null)
            return;
        mBottomSheetFirst = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_playlist, null);
        TextView tvTitle = sheetView.findViewById(R.id.tv_title);
        TextView tvSinger = sheetView.findViewById(R.id.tv_description);
        ImageView ivTopLeft = sheetView.findViewById(R.id.iv_top_left);
        ImageView ivTopRight = sheetView.findViewById(R.id.iv_top_right);
        ImageView ivBottomLeft = sheetView.findViewById(R.id.iv_bottom_left);
        ImageView ivBottomRight = sheetView.findViewById(R.id.iv_bottom_right);
        View viewRight = sheetView.findViewById(R.id.right_layout);
        ImageBusiness.setImagePlaylist(item.getListAvatar(false), ivTopLeft, ivTopRight, ivBottomRight, ivBottomLeft, viewRight);
        tvTitle.setText(item.getName());
        tvSinger.setText(item.getSinger());
        String str = item.getNameUser();
        if (TextUtils.isEmpty(str)) {
            tvSinger.setVisibility(View.GONE);
        } else {
            tvSinger.setVisibility(View.VISIBLE);
            tvSinger.setText(getString(R.string.create_by, str));
        }
        RecyclerView mRecycler = sheetView.findViewById(R.id.recycler);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new CustomLinearLayoutManager(this));
        BottomSheetAdapter adapter = new BottomSheetAdapter(this, BottomSheetData.getPopupOfPlaylist(item), TAG, item1 -> {
            switch (item1.getType()) {
                case BottomSheetData.SHARE:
                    if (applicationController.getReengAccountBusiness().isAnonymousLogin())
                        showDialogLogin();
                    else {
                        dismissBottomSheetFirst();
                        ShareUtils.openShareMenu(BaseSlidingFragmentActivity.this, item1.getPlaylist());
                    }
                    break;
                default:
                    dismissBottomSheetFirst();
                    break;
            }
        });
        BaseAdapter.setupVerticalMenuRecycler(applicationController, mRecycler, null, adapter, true);
        mRecycler.setAdapter(adapter);
        mBottomSheetFirst.setContentView(sheetView);
        mBottomSheetFirst.show();
    }

    public boolean isPlayingMedia() {
        return true;
    }

    public void showPopupMore(List<CategoryModel> datas, final AllModel item) {
        dismissBottomSheetFirst();
        if (item == null || datas == null || datas.isEmpty())
            return;
        mBottomSheetFirst = new BottomSheetDialog(this);
        int resIdLayout;
        switch (item.getType()) {
            case Constants.TYPE_SONG:
                resIdLayout = R.layout.layout_bottom_sheet_song;
                break;
            case Constants.TYPE_ALBUM:
                resIdLayout = R.layout.layout_bottom_sheet_album;
                break;
            case Constants.TYPE_VIDEO:
            case Constants.TYPE_YOUTUBE:
                resIdLayout = R.layout.layout_bottom_sheet_video;
                break;
            case Constants.TYPE_PLAYLIST:
                resIdLayout = R.layout.layout_bottom_sheet_album;
                break;
            default:
                resIdLayout = R.layout.layout_bottom_sheet_category;
                break;
        }
        View sheetView = getLayoutInflater().inflate(resIdLayout, null);
        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
        TextView tvSinger = sheetView.findViewById(R.id.tvSinger);
        ImageView image = sheetView.findViewById(R.id.image);
        RecyclerView mRecycler = sheetView.findViewById(R.id.recycler);

        tvTitle.setText(item.getName());
        tvSinger.setText(item.getSinger());
        switch (item.getType()) {
            case Constants.TYPE_SONG:
                ImageBusiness.setSong(image, item.getImage(), item.getId(), ApplicationController.self().getRound());
                break;
            case Constants.TYPE_ALBUM:
                ImageBusiness.setAlbum(image, item.getImage(), item.getId(), ApplicationController.self().getRound());
                break;
            case Constants.TYPE_VIDEO:
                ImageBusiness.setVideo(image, item.getImage(), item.getId());
                break;
            default:
                ImageBusiness.setImage(item.getImage(), image, item.getId());
                break;
        }

        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new CustomLinearLayoutManager(this));
        BottomSheetAdapter adapter = new BottomSheetAdapter(this, datas, TAG, this::onClickBottomSheet);
        BaseAdapter.setupVerticalMenuRecycler(applicationController, mRecycler, null, adapter, true);
        mRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        mBottomSheetFirst.setContentView(sheetView);
        mBottomSheetFirst.show();
    }

    private void onClickBottomSheet(CategoryModel item) {
        if (item == null) {
            return;
        }
        switch (item.getType()) {

            case BottomSheetData.ADD_PLAYING:
                dismissBottomSheetFirst();
                addMediaSongToPlayingList(item.getMedia());
                break;

            case BottomSheetData.SHARE:
                if (applicationController.getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else
                    clickPopupShareMedia(item.getMedia());
                break;

            case BottomSheetData.REMOVE_PLAYING:
                dismissBottomSheetFirst();
//                removeMediaSongToPlayingList(item.getMedia());
                break;

            case BottomSheetData.SHOW_SONG_INFO:
            case BottomSheetData.SHOW_VIDEO_INFO:
                dismissBottomSheetFirst();
                DialogMediaInfo dialog = new DialogMediaInfo(this, item.getMedia());
                dialog.show();
                break;
            case Constants.MENU.MENU_REPORT_VIDEO:
                dismissBottomSheetFirst();
                new ReportVideoDialog(this)
                        .setCurrentVideo(convertKeengToVideoMocha(item.getMedia()))
                        .setTitleDialog(getString(R.string.report_video))
                        .show();
                break;
            default:
                dismissBottomSheetFirst();
                dismissBottomSheetSecond();
                break;
        }
    }

    private void addMediaSongToPlayingList(AllModel item) {
        if (applicationController.getPlayMusicController() != null) {
            if (applicationController.getPlayMusicController().getDataSong().size() == 0) {
                playMusicSong(item, false);
            } else {
                MediaModel mediaModel = ConvertHelper.convertKeengToAudioMocha(item);
                if (applicationController.getPlayMusicController().checkContainMediaModel(mediaModel)) {
                    ToastUtils.makeText(this, getString(R.string.add_playing_duplicate));
                } else {
                    applicationController.getPlayMusicController().getDataSong().add(mediaModel);
                    ToastUtils.makeText(this, getString(R.string.add_playing_done));
                }
            }
        }
    }

    private void dismissBottomSheetFirst() {
        if (mBottomSheetFirst != null)
            mBottomSheetFirst.dismiss();
    }

    private void dismissBottomSheetSecond() {
        if (mBottomSheetSecond != null)
            mBottomSheetSecond.dismiss();
    }

    public void clickPopupShareMedia(AllModel item) {
        dismissBottomSheetFirst();
        ShareUtils.openShareMenu(BaseSlidingFragmentActivity.this, item);
//        new FacebookHelper(this).shareContentToFacebook(this, getCallbackManager(), item.getUrl(),
//                null, null, null, null);
    }

    public void setMediaToPlayVideo(AllModel item) {
        Log.e(TAG, "vao play video");
        LuckyWheelHelper.getInstance(applicationController).doMission(Constants.LUCKY_WHEEL.ITEM_LISTEN_MUSIC_KEENG);
        if (lastRequest != null) lastRequest.cancel();
        final Video videoModel = ConvertHelper.convertKeengToVideoMocha(item);
        if (videoModel == null || TextUtils.isEmpty(videoModel.getId())) {
            ToastUtils.makeText(BaseSlidingFragmentActivity.this, getString(R.string.e500_internal_server_error));
            return;
        }
        if (TextUtils.isEmpty(videoModel.getOriginalPath()) && TextUtils.isEmpty(videoModel.getLink())) {
            final DialogLoadData dialog = new DialogLoadData(this, true);
            if (!dialog.isShowing())
                dialog.show();
            //Truong hop ko co link va ko co url thi goi api lay video chi tiet dua vao id va identify
            lastRequest = new KeengApi().getVideo(item.getId(), item.getIdentify(), result -> {
                dialog.dismiss();
                if (result != null && result.getData() != null) {
                    AllModel data = result.getData();
                    Video videoModel1 = ConvertHelper.convertKeengToVideoMocha(data);
                    applicationController.getApplicationComponent().providesUtils().openVideoDetail(BaseSlidingFragmentActivity.this, videoModel1);
                } else {
                    ToastUtils.makeText(BaseSlidingFragmentActivity.this, getString(R.string.e500_internal_server_error));
                }
            }, volleyError -> {
                dialog.dismiss();
                ToastUtils.makeText(BaseSlidingFragmentActivity.this, getString(R.string.e500_internal_server_error));
            });
        } else {
            applicationController.getApplicationComponent().providesUtils().openVideoDetail(this, videoModel);
        }
    }

    private void getMediaUrl(final AllModel model) {
        if (model.getId() > 0 || Utilities.notEmpty(model.getIdentify())) {
            final DialogLoadData dialog = new DialogLoadData(this, true);
            if (!dialog.isShowing())
                dialog.show();
            lastRequest = new KeengApi().getSong(model.getId(), model.getIdentify()
                    , result -> {
                        dialog.dismiss();
                        if (result != null && result.getData() != null) {
                            AllModel data = result.getData();
                            ArrayList<MediaModel> listSongs = new ArrayList<>();
                            listSongs.add(ConvertHelper.convertKeengToAudioMocha(data));
                            for (AllModel model1 : data.getSongList()) {
                                listSongs.add(ConvertHelper.convertKeengToAudioMocha(model1));
                            }

                            playSongWithPlaylist(data.getName(), listSongs, 0, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                        } else {
                            ToastUtils.makeText(BaseSlidingFragmentActivity.this, getString(R.string.e500_internal_server_error));
                        }
                    }, volleyError -> {
                        dialog.dismiss();
                        ToastUtils.makeText(BaseSlidingFragmentActivity.this, getString(R.string.e500_internal_server_error));
                    });
        } else {
            ToastUtils.makeText(BaseSlidingFragmentActivity.this, getString(R.string.e500_internal_server_error));
        }
    }

    private void playSongWithPlaylist(String playListName, ArrayList<MediaModel> listSongs, int position, int repeatType, int shuffleType) {
        applicationController.getPlayMusicController().pauseMusic();
        applicationController.getMusicBusiness().resetSessionMusic();
        applicationController.getPlayMusicController().setStateRepeat(repeatType);
        applicationController.getPlayMusicController().setStateShuffle(shuffleType);
        applicationController.getPlayMusicController().setPlayList(listSongs, position);
        applicationController.getPlayMusicController().setPlayListName(playListName);
        applicationController.getMusicBusiness().setPlayFromKeengMusic(true);

        Intent intent = new Intent(this, KeengPlayerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void setMediaToPlaySong(final AllModel item) {
        if (lastRequest != null) lastRequest.cancel();
        MediaModel mediaModel = ConvertHelper.convertKeengToAudioMocha(item);
        if (mediaModel == null) return;

        if (applicationController.getMusicBusiness().isExistListenMusic()) {
            new EventOnMediaHelper(this).handleClickMediaItem(FeedModelOnMedia.convertMediaToFeedModelOnMedia(mediaModel),
                    null, (view, entry, menuId) -> {
                        switch (menuId) {
                            case Constants.MENU.POPUP_EXIT:
                                new EventOnMediaHelper(BaseSlidingFragmentActivity.this).handleClickPopupExitListenTogether((FeedModelOnMedia) entry, null);

                                getMediaUrl(item);

                                break;
                        }
                    });
        } else {
            getMediaUrl(item);
        }
    }

    public void setMediaPlayingAudio(PlayingList playingList, final int position) {
        final ArrayList<MediaModel> listSongs = new ArrayList<>();
        if (!playingList.getSongList().isEmpty()) {
            for (AllModel model : playingList.getSongList()) {
                listSongs.add(ConvertHelper.convertKeengToAudioMocha(model));
            }
        }
        if (applicationController.getMusicBusiness().isExistListenMusic()) {
            MediaModel mediaModel = listSongs.get(position);
            new EventOnMediaHelper(this).handleClickMediaItem(FeedModelOnMedia.convertMediaToFeedModelOnMedia(mediaModel),
                    null, (view, entry, menuId) -> {
                        switch (menuId) {
                            case Constants.MENU.POPUP_EXIT:
                                new EventOnMediaHelper(BaseSlidingFragmentActivity.this).handleClickPopupExitListenTogether((FeedModelOnMedia) entry, null);

                                playSongWithPlaylist(playingList.getName(), listSongs, position, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);

                                break;
                        }
                    });
        } else {
            playSongWithPlaylist(playingList.getName(), listSongs, position, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
        }
    }

    public void setMediaPlayingAudioWithState(PlayingList playingList, final int position, final int repeatType, int shuffleType) {
        final ArrayList<MediaModel> listSongs = new ArrayList<>();
        if (!playingList.getSongList().isEmpty()) {
            for (AllModel model : playingList.getSongList()) {
                listSongs.add(ConvertHelper.convertKeengToAudioMocha(model));
            }
        }

        if (applicationController.getMusicBusiness().isExistListenMusic()) {
            MediaModel mediaModel = listSongs.get(position);
            new EventOnMediaHelper(this).handleClickMediaItem(FeedModelOnMedia.convertMediaToFeedModelOnMedia(mediaModel),
                    null, (view, entry, menuId) -> {
                        switch (menuId) {
                            case Constants.MENU.POPUP_EXIT:
                                new EventOnMediaHelper(BaseSlidingFragmentActivity.this).handleClickPopupExitListenTogether((FeedModelOnMedia) entry, null);

                                playSongWithPlaylist(playingList.getName(), listSongs, position, repeatType, shuffleType);

                                break;
                        }
                    });
        } else {
            playSongWithPlaylist(playingList.getName(), listSongs, position, repeatType, shuffleType);
        }
    }

    public void playMovies(Movie movie) {
        if (movie == null) return;
        ApplicationController.self().getApplicationComponent().providesUtils().openMovieDetail(this, movie);
    }

    public void playVideoTiin(TiinModel data) {
        Video videoModel = ConvertHelper.convertTiinToVideoMocha(data);
        ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(this, videoModel);
    }

    public void gotoAlbumDetail(AllModel model) {
        Intent intent = new Intent(this, TabKeengActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_ALBUM_DETAIL);
        intent.putExtra(Constants.KEY_DATA, model);
        startActivity(intent);
    }

    public void gotoPlaylistDetail(PlayListModel model) {
        Intent intent = new Intent(this, TabKeengActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_PLAYLIST_DETAIL);
        intent.putExtra(Constants.KEY_DATA, model);
        startActivity(intent);
    }

    public void gotoSingerDetail(Topic model) {
        Intent intent = new Intent(this, TabKeengActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_SINGLE_DETAIL);
        intent.putExtra(Constants.KEY_DATA, model);
        startActivity(intent);
    }

    public void gotoTopicDetail(final Topic model) {
        Intent intent = new Intent(this, TabKeengActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_TOPIC_DETAIL);
        intent.putExtra(Constants.KEY_DATA, model);
        startActivity(intent);
    }

    public void gotoCategoryDetail(final Topic model) {
        Intent intent = new Intent(this, TabKeengActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_CATEGORY_DETAIL);
        intent.putExtra(Constants.KEY_DATA, model);
        startActivity(intent);
    }

    public void gotoTopHitDetail(final Topic model) {
        Intent intent = new Intent(this, TabKeengActivity.class);
        intent.putExtra(Constants.KEY_TYPE, Constants.TAB_TOP_HIT_DETAIL);
        intent.putExtra(Constants.KEY_DATA, model);
        startActivity(intent);
    }

    public void loginFromAnonymous(String fromSource) {
        if (System.currentTimeMillis() - lastClick < 1000) {
            lastClick = System.currentTimeMillis();
            return;
        }

        lastClick = System.currentTimeMillis();
        ApplicationController app = (ApplicationController) getApplication();

        if (app.getReengAccountBusiness().isAnonymousLogin()) {
            app.getReengAccountBusiness().setInProgressLoginFromAnonymous(true);
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra(Constants.KEY_POSITION, fromSource);
            startActivity(intent, false);
        } else
            showToast(R.string.e601_error_but_undefined);
    }

    public void loginFromAnonymous() {
        loginFromAnonymous("");
    }

    public void showDialogLogin() {
        DialogMessage dialogMessage = new DialogMessage(this, true);
        dialogMessage.setMessage(getResources().getString(R.string.content_popup_login));
        dialogMessage.setNegativeListener(result -> loginFromAnonymous());
        dialogMessage.show();
    }

    public void exitFullscreenWap() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
    }

    public void setFullscreenWap() {
        int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(flags);

    }

    public void showStatusBarWap(boolean isShow) {
        Log.i(TAG, "showStatusBarWap start: " + isShow);
        if (isShow) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            exitFullscreenWap();
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setFullscreenWap();
        }
        Log.i(TAG, "showStatusBarWap finish");
    }

    public void playMusicSong(@NonNull final AllModel model, final boolean hasData) {
        if (lastRequest != null) lastRequest.cancel();
        if (applicationController.getMusicBusiness().isExistListenMusic()) {
            MediaModel mediaModel = ConvertHelper.convertKeengToAudioMocha(model);
            new EventOnMediaHelper(this).handleClickMediaItem(FeedModelOnMedia.convertMediaToFeedModelOnMedia(mediaModel),
                    null, (view, entry, menuId) -> {
                        if (menuId == Constants.MENU.POPUP_EXIT) {
                            new EventOnMediaHelper(BaseSlidingFragmentActivity.this).handleClickPopupExitListenTogether((FeedModelOnMedia) entry, null);
                            if (hasData) {
                                ArrayList<MediaModel> listSongs = new ArrayList<>();
                                listSongs.add(ConvertHelper.convertKeengToAudioMocha(model));
                                for (AllModel item : model.getSongList()) {
                                    listSongs.add(ConvertHelper.convertKeengToAudioMocha(item));
                                }
                                playSongWithPlaylist(model.getName(), listSongs, 0, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                            } else {
                                getMediaUrl(model);
                            }
                        }
                    });
        } else {
            if (hasData) {
                ArrayList<MediaModel> listSongs = new ArrayList<>();
                listSongs.add(ConvertHelper.convertKeengToAudioMocha(model));
                for (AllModel item : model.getSongList()) {
                    listSongs.add(ConvertHelper.convertKeengToAudioMocha(item));
                }
                playSongWithPlaylist(model.getName(), listSongs, 0, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
            } else {
                getMediaUrl(model);
            }
        }
    }

    protected void handleServiceList(BaseSlidingFragmentActivity activity, CamIdUserBusiness mCamIdUserBusiness, int type) {
        List<Service> services = mCamIdUserBusiness.getServices();
        boolean mIsHaveMetfonePlusEqual1 = false;
        int sumServiceNumber = 0;
        int userServiceId = -1;
        for (Service s : services) {
            List<PhoneLinked> phoneLinkeds = s.getPhoneLinked();
            for (int i = 0; i < phoneLinkeds.size(); i++) {
                PhoneLinked phoneLinked = phoneLinkeds.get(i);
                sumServiceNumber++;
                userServiceId = phoneLinked.getUserServiceId();
                if (phoneLinked.getMetfonePlus() == 1 && s.getServiceCode().trim().equals("MOBILE")) {
                    mIsHaveMetfonePlusEqual1 = true;
                    mCamIdUserBusiness.setPhoneService(phoneLinked.getPhoneNumber());
                }
            }
        }

        if (sumServiceNumber > 1 && !mIsHaveMetfonePlusEqual1) {
            if (!mCamIdUserBusiness.isAddOrSelectPhoneCalled()) {
                NavigateActivityHelper.navigateToSelectPhone(activity);
                mCamIdUserBusiness.setAddOrSelectPhoneCalled(true);
            }
        } else if (sumServiceNumber == 1) {
            activity.showLoadingDialog("", "");
            RetrofitInstance retrofitInstance = new RetrofitInstance();
            retrofitInstance.updateMetfone(null, null, userServiceId, new ApiCallback<AddServiceResponse>() {
                @Override
                public void onResponse(Response<AddServiceResponse> response) {
                    if (response.body() != null && response.body().getData() != null) {
                        mCamIdUserBusiness.setForceUpdateMetfone(true);
                        mCamIdUserBusiness.setPhoneService(response.body().getData().getServices());
                        Intent intent = new Intent(activity, DeepLinkActivity.class);
                        intent.putExtra(DeepLinkActivity.TYPE_DEEP_LINK, type);
                        activity.startActivity(intent);
                    }
                    activity.hideLoadingDialog();
                }

                @Override
                public void onError(Throwable error) {
                    activity.hideLoadingDialog();
                    mCamIdUserBusiness.setPhoneService("");
                }
            });
        } else {
            if (!mCamIdUserBusiness.isAddOrSelectPhoneCalled()) {
                NavigateActivityHelper.navigateToMetfoneAddNumber(activity);
                mCamIdUserBusiness.setAddOrSelectPhoneCalled(true);
            }
        }
    }

    public void changeLightStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.bg_mocha));
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setColorIconStatusBar()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.KH_qr_code_status_color));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.KH_qr_code_status_color));
            }
        }
    }

    public void changeWhiteStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
            }
        }
    }

    public void displayRestoreInfo(long size, String urlPath, long date, String idFile) {
        RestoreManager.setRestoring(true);
//        mApplication.getXmppManager().manualDisconnect();
        Intent intent = new Intent(this, RestoreActivity.class);
        intent.putExtra("backup_size", size);
        intent.putExtra("backup_url_path", urlPath);
        intent.putExtra("backup_time", date);
        intent.putExtra("backup_id_file", idFile);
        startActivity(intent);
        finish();
    }

    public void displayRegisterScreenActivity(boolean isLogin) {
        if (System.currentTimeMillis() - lastClick < 1000) {
            lastClick = System.currentTimeMillis();
            return;
        }
        lastClick = System.currentTimeMillis();

        Intent intent = new Intent(this, RegisterScreenActivity.class);
        intent.putExtra(Constants.PARAM_IS_LOGIN, isLogin);
        startActivity(intent, false);
    }

    //    @Override
//    public void onVisibilityChanged(boolean isOpen) {
//        Log.d(TAG, "onVisibilityChanged keyboard isOpen: " + isOpen);
//    }
    public void changeMode(boolean flag) {
        if (flag) {
            setTheme(R.style.ThemeNight);
        } else {
            setTheme(R.style.ThemeDay);
        }
        final View rootView = getWindow().getDecorView();
        ColorUiUtil.changeTheme(rootView, getTheme());
    }

    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull Boolean isAddtoBackStack) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(containerViewId, fragment, fragment.getClass().getName());
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isAddtoBackStack)
            mFragmentTransaction.addToBackStack(fragment.getClass().getName());
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void popBackStackFragment() {
        mFragmentManager.popBackStack();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK
//                && event.getRepeatCount() == 0) {
//            EventBus.getDefault().post(new OnBackDeviceEvent());
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    public void setupUI(View view, Activity activity) {

        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                return false;
            }
        });

//If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }
    }

    protected Navigator mNavigator;
    public void replaceFragment(BaseFragment baseFragment, String tag) {
        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, baseFragment, tag).commitAllowingStateLoss();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }

    /**
     * hàm chuy?n fragment
     * created by ntchung1 on 12/02/2020
     *
     * @param fragment fragment
     */
    public void addFragment(BaseFragment fragment, boolean isAddToBackStack, String TAG) {
        if (mNavigator == null) {
            synchronized (Navigator.class) {
                mNavigator = new Navigator(this);
            }
        }
        try {
            mNavigator.addFragment(R.id.fragment_container, fragment, isAddToBackStack, Navigator.NavigateAnim.RIGHT_LEFT, TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void popFragment() {
        try {
            popBackStackFragment();
        } catch (Exception e) {
            Common.handleException(e);
        }
    }


}
package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.adapter.QuickReplyAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.ReplyMessage;
import com.metfone.selfcare.database.model.message.SoloSendTextMessage;
import com.metfone.selfcare.database.model.message.SoloShareMusicMessage;
import com.metfone.selfcare.database.model.message.SoloVoiceStickerMessage;
import com.metfone.selfcare.fragment.message.ThreadDetailFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonManager;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.helper.sticker.VoiceStickerPlayer;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.ReengMessageListener;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.CusRelativeLayout;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.MultiLineEditText;
import com.metfone.selfcare.ui.WindowShaker;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.packet.ReengMusicPacket;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ThanhNT on 9/23/2014.
 */
public class QuickReplyActivity extends BaseSlidingFragmentActivity
        implements MessageInteractionListener,
        CusRelativeLayout.IKeyboardChanged,
        InitDataListener,
        ReengMessageListener,
        ClickListener.IconListener {

    public static final String TAG = QuickReplyActivity.class.getSimpleName();
    public static final String BG_TRANSPARENT = "bgTranparent";
    public static final String REENG_MESSAGE = "reengMessage";
    public static final String SCREEN_OFF = "screen_off";

    private ListView mLvListMessage;
    public QuickReplyAdapter mQuickReplyAdapter;
    private ApplicationController mApplication;
    private ArrayList<ReengMessage> messages = new ArrayList<>();
    private View mViewfooter;
    private ImageButton mBtnClose;
    private ImageButton mBtnExpand;
    private Button mBtnOpen;
    private CircleImageView mImgAvatar;
    private TextView mTvwAvatar;
    private EllipsisTextView mTwvName;
    private MultiLineEditText etMessageContent;
    private ImageButton mBtnSend;
    private View mViewAvatarNameLayout;
    private CusRelativeLayout mLayoutRect;
    private LinearLayout mSubLayoutQuickReply;
    private RelativeLayout mLayoutActionBar;
    private Handler mHandler;
    private CountDownTimer countDownTimer = null;
    private static final int TIME_DEFAULT = 15000;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mReengAccountBusiness;
    private SettingBusiness mSettingBusiness;
    private ThreadMessage mThreadMessage;
    private String friendJid;
    private ReengAccount currentAccount;
    private ReengMessage currentReengMessage;
    private String userNumber;
    private boolean mGsmMode = false;
    public int threadIDQuickReply, mThreadType;
    private ContactBusiness mContactBusiness;
    private boolean bgTransparent = true, isTouchQuickReply = false, isScreenOff = false;
    private boolean disableCountdown = false;
    private ApplicationStateManager mAppStateManager;
    private boolean isDraft = false;
    //  isSaved = false;
    private VoiceStickerPlayer mVoiceStickerPlayer;
    private boolean settingAutoPlaySticker = false; //co tu dong play sticker hay ko?
    private WindowShaker.Option option;
    // sticker
    private int stickerViewHeight = 50;
    private LinearLayout mLlStickerSuggest, mLlStickerOk, mLlStickerWhat, mLlStickerNo, mLlStickerLike;
    private RelativeLayout.LayoutParams layoutParamsSticker;
    private View popupViewPreview;
    private PopupWindow popupWindowPreview;
    private ImageView imgPreview;

    private int preCollectionIDSticker;
    private int prePossitionSticker;

    private boolean isStop = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        mApplication = (ApplicationController) getApplicationContext();
        mAppStateManager = mApplication.getAppStateManager();
        changeStatusBar(true);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0 ||
                !mApplication.isDataReady()) {
            goToHome();
        } else {
            setFlagWindow();
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.activity_quick_reply, null);
            setContentView(view);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            initQuickReply();
            initView(view);
            initAdapter();
            setViewListener();
            setChatMode();
            drawActionbar(currentReengMessage);
            setContentDraftMessage();
            initWindowShaker();
            initSticker();
        }
        trackingScreen(TAG);
        if (messages.isEmpty()) {
            mAppStateManager.setShowQuickReply(false);
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        mLvListMessage.setSelectionFromTop(mLvListMessage.getCount() - 1, -300000);
        ListenerHelper.getInstance().addInitDataListener(this);
        if (mApplication.isDataReady()) {
            mMessageBusiness.addReengMessageListener(this);
        }
        if (mHandler == null) {
            mHandler = new Handler();
        }
    }

    @Override
    protected void onPause() {
        dismissStickerPopupWindow();
        ListenerHelper.getInstance().removeInitDataListener(this);
        mMessageBusiness.removeReengMessageListener(this);
        if (mVoiceStickerPlayer != null && mVoiceStickerPlayer.isPlaying()) {
            mVoiceStickerPlayer.stopVoice();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "isStop: " + isStop);
        if (isStop) {
            Log.i(TAG, "onStop");
            cancelCountDown();
            mAppStateManager.setShowQuickReply(false);
            mAppStateManager.applicationEnterBackground(this, isTouchQuickReply);
            mHandler = null;
            saveContentDraftMessage();
            hideKeyboard();
            mApplication.wakeLockRelease();
            Window win = getWindow();
            win.clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                    | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            );
            if (mLayoutRect != null)
                mLayoutRect.removeKeyboardStateChangedListener(this);
        } else {
            isStop = true;
        }
    }

    private void setFlagWindow() {
        if (mAppStateManager.isScreenLocker() || !mAppStateManager.isScreenOn()) {
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        } else {
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
    }

    private void saveContentDraftMessage() {
        String draftMsg = EmoticonUtils.getRawTextFromSpan(etMessageContent.getText());
        mThreadMessage.setDraftMessage(draftMsg);
        mThreadMessage.setLastTimeSaveDraft(System.currentTimeMillis());
        mMessageBusiness.updateThreadMessage(mThreadMessage);
        Log.d(TAG, "saveContentDraftMessage: " + draftMsg);
    }

    private void setContentDraftMessage() {
        Log.d(TAG, "setContentDraftMessage");
        if (mThreadMessage == null) {
            return;
        }
        String draftMessage = mThreadMessage.getDraftMessage();
        if (draftMessage == null || draftMessage.length() <= 0) {
            return;
        }
        if (etMessageContent == null) {
            return;
        }
        Log.d(TAG, "setContentDraftMessage: " + draftMessage);
        new SetContentDraftAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, draftMessage);
    }

    private void initWindowShaker() {
        boolean isVibrate = mSettingBusiness.getPrefVibrate();
        option = new WindowShaker.Option.Builder(this).setOffsetY(0).setVibrate(isVibrate).build();
    }

    private class SetContentDraftAsyncTask extends AsyncTask<String, Void, Spanned> {

        @Override
        protected Spanned doInBackground(String... params) {
            String draftMessage = params[0];
            Html.ImageGetter imageGetter = EmoticonManager.getInstance(mApplication).getImageGetter();
            draftMessage = TextHelper.getInstant().escapeXml(draftMessage);
            draftMessage = EmoticonUtils.emoTextToTag(draftMessage);
            return TextHelper.fromHtml(draftMessage, imageGetter, null);
        }

        @Override
        protected void onPostExecute(Spanned content) {
            isDraft = true;
            etMessageContent.setText(content);
            etMessageContent.setSelection(content.length());
            etMessageContent.requestFocus();
            if (TextUtils.isEmpty(content)) {
                disableButtonSend();
            } else {
                enableButtonSend();
            }
            super.onPostExecute(content);
        }
    }

    @Override
    public void onDataReady() {
        mMessageBusiness.addReengMessageListener(this);
    }

    private void initQuickReply() {
        mMessageBusiness = mApplication.getMessageBusiness();
        mReengAccountBusiness = mApplication.getReengAccountBusiness();
        mSettingBusiness = SettingBusiness.getInstance(mApplication);
        mContactBusiness = mApplication.getContactBusiness();
        currentAccount = mReengAccountBusiness.getCurrentAccount();
        messages = new ArrayList<>();
        mThreadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            bgTransparent = extras.getBoolean(BG_TRANSPARENT);
            isScreenOff = extras.getBoolean(SCREEN_OFF);
            Log.i(TAG, "bgTransparent: " + bgTransparent + " screen off: " + isScreenOff);
            isStop = !isScreenOff && !mAppStateManager.isScreenLocker();
            currentReengMessage = (ReengMessage) extras.getSerializable(REENG_MESSAGE);
            //TODO crash neu khong xu ly
            if (currentReengMessage != null) {
                threadIDQuickReply = currentReengMessage.getThreadId();
                loadUnReadMessage();
                if (messages.isEmpty()) { // khong load duoc message nao
                    Log.i(TAG, "khong co message nao");
                }
            } else {
                Log.i(TAG, "currentReengMessage null");
            }
        }
        isTouchQuickReply = false;
        int threadId = 0;
        if (currentReengMessage != null) {
            threadId = currentReengMessage.getThreadId();
        }
        mThreadMessage = mMessageBusiness.getThreadById(threadId);
        userNumber = currentAccount.getJidNumber();
        mVoiceStickerPlayer = new VoiceStickerPlayer(mApplication);
        settingAutoPlaySticker = mSettingBusiness.getPrefAutoPlaySticker();
        //tu phat voice sticker
        if (!messages.isEmpty()) {
            ReengMessage lastMsg = messages.get(messages.size() - 1);
            if (lastMsg.getMessageType() == ReengMessageConstant.MessageType.voiceSticker) {
                playStickerMessage(lastMsg);
            }
        }
    }

    private void loadUnReadMessage() {
        messages = new ArrayList<>();
        CopyOnWriteArrayList<ReengMessage> listMessage = mMessageBusiness.getMessagesByThreadId(threadIDQuickReply);
        if (listMessage == null) return;
        Log.i(TAG, "listmsg: ");
        for (int i = 0; i < listMessage.size(); i++) {
            ReengMessage reengMessage = listMessage.get(i);
            if (reengMessage.getReadState() == ReengMessageConstant.READ_STATE_UNREAD) {
                Log.i(TAG, "msg: " + reengMessage);
                messages.add(reengMessage);
            }
        }
    }

    private void initSticker() {
        // da init o initQuickReply
        LayoutInflater layoutInflater
                = (LayoutInflater) mApplication.getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupViewPreview = layoutInflater.inflate(R.layout.popup_preview_sticker, null);
        popupWindowPreview = new PopupWindow(
                popupViewPreview,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        imgPreview = (ImageView) popupViewPreview.findViewById(R.id.img_sticker_preview);
        popupWindowPreview.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                resetDefaultStickerID();
                mVoiceStickerPlayer.stopVoice();
            }
        });
    }

    private void resetDefaultStickerID() {
        preCollectionIDSticker = ThreadDetailFragment.DEFAULT_PREVIOUS_STICKER;
        prePossitionSticker = ThreadDetailFragment.DEFAULT_PREVIOUS_STICKER;
    }

    private void dismissStickerPopupWindow() {
        if (popupWindowPreview != null && popupWindowPreview.isShowing()) {
            popupWindowPreview.dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        startCountDown();
        if (mLayoutRect != null)
            mLayoutRect.addKeyboardStateChangedListener(this);
        Log.i(TAG, "onStart");
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!disableCountdown) {
            disableCountdown = true;
            cancelCountDown();
        }
        return super.dispatchTouchEvent(ev);
    }

    public void goToHome() {
        mAppStateManager.setShowQuickReply(false);
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void initView(View view) {
        stickerViewHeight = (int) getResources().getDimension(R.dimen.quick_reply_sticker_height);
        mImgAvatar = (CircleImageView) view.findViewById(R.id.ab_thread_avatar);
        mBtnClose = (ImageButton) view.findViewById(R.id.message_menu_over_flow_ic);
        mBtnExpand = (ImageButton) view.findViewById(R.id.ic_expand_quick_reply);
        etMessageContent = (MultiLineEditText) view.findViewById(R.id.person_chat_detail_input_text);
        etMessageContent.requestFocus();
        etMessageContent.setEditerAction(true, EditorInfo.IME_ACTION_SEND);
        mBtnSend = (ImageButton) view.findViewById(R.id.person_chat_detail_send_reeng_text);
        mTwvName = (EllipsisTextView) view.findViewById(R.id.ab_title);
        mTvwAvatar = (TextView) view.findViewById(R.id.contact_avatar_text);
        mViewAvatarNameLayout = view.findViewById(R.id.ab_avt_name_layout);
        mLayoutRect = (CusRelativeLayout) view.findViewById(R.id.layout_quick_reply);
        mViewfooter = view.findViewById(R.id.person_chat_detail_footer);
        mBtnOpen = (Button) view.findViewById(R.id.open_button);
        mLvListMessage = (ListView) view.findViewById(R.id.list_message);
        //sticker
        mLlStickerSuggest = (LinearLayout) view.findViewById(R.id.quick_reply_sticker_suggest_layout);
        mLlStickerOk = (LinearLayout) view.findViewById(R.id.quick_reply_sticker_ok);
        mLlStickerWhat = (LinearLayout) view.findViewById(R.id.quick_reply_sticker_what);
        mLlStickerNo = (LinearLayout) view.findViewById(R.id.quick_reply_sticker_no);
        mLlStickerLike = (LinearLayout) view.findViewById(R.id.quick_reply_sticker_like);
        mLlStickerSuggest.setVisibility(View.GONE); //Cho an text tren sticker
        if (!bgTransparent) {
            mLayoutRect.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
        }
        mSubLayoutQuickReply = (LinearLayout) view.findViewById(R.id.sub_layout_quick_reply);
        mLayoutActionBar = (RelativeLayout) view.findViewById(R.id.layout_ab_quick_reply);
        disableButtonSend();
    }

    private void initAdapter() {
        mQuickReplyAdapter = new QuickReplyAdapter(mApplication, messages, this);
        mLvListMessage.setAdapter(mQuickReplyAdapter);
        mLvListMessage.smoothScrollToPosition(mLvListMessage.getCount() - 1);
    }

    private void setViewListener() {
        buttonCloseListener();
        buttonExpandListener();
        buttonOpenListener();
        setEditTextListenner();
        clickAvatarListener();
        showOrHideFooterView();
        setParentLayoutClick();
        setLayoutActionBarClick();
        setControlStickerListener();
        setClickOutSideClose();
        setButtonSendListener();
    }

    private void showOrHideFooterView() {
        mBtnOpen.setVisibility(View.GONE);
        mViewfooter.setVisibility(View.VISIBLE);
    }

    private void setClickOutSideClose() {
        mLayoutRect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAppStateManager.setShowQuickReply(false);
                finish();
            }
        });
    }

    public void clickAvatarListener() {
        mViewAvatarNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openThreadDetail();
            }
        });
    }

    public void enableButtonSend() {
        mBtnSend.setEnabled(true);
        mBtnSend.setColorFilter(ContextCompat.getColor(QuickReplyActivity.this, R.color.bg_mocha));
    }

    public void disableButtonSend() {
        mBtnSend.setEnabled(false);
        mBtnSend.setColorFilter(ContextCompat.getColor(QuickReplyActivity.this, R.color.gray));
    }

    private void setButtonSendListener() {
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sendText()) {
                    mMessageBusiness.setSendMsgQuickReply(true);
                    mMessageBusiness.markAllMessageIsReadAndCheckSendSeen(mThreadMessage, mThreadMessage
                            .getAllMessages());
                    mAppStateManager.setShowQuickReply(false);
                    finish();
                }
            }
        });
    }

    public void setEditTextListenner() {
        etMessageContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isDraft)
                    isDraft = false;
                if (s != null && !TextUtils.isEmpty(s.toString().trim())) {
                    enableButtonSend();
                } else {
                    disableButtonSend();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etMessageContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    isTouchQuickReply = true;
                    mAppStateManager.applicationEnterForeground(QuickReplyActivity.this, isTouchQuickReply);
                    dismissStickerPopupWindow();
                }
            }
        });

        etMessageContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (sendText()) {
                        mMessageBusiness.setSendMsgQuickReply(true);
                        mMessageBusiness.markAllMessageIsReadAndCheckSendSeen(mThreadMessage, mThreadMessage
                                .getAllMessages());
                        mAppStateManager.setShowQuickReply(false);
                        finish();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void buttonCloseListener() {
        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAppStateManager.setShowQuickReply(false);
                finish();
            }
        });
    }

    private void buttonExpandListener() {
        mBtnExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openThreadDetail();
            }
        });
    }

    private void buttonOpenListener() {
        mBtnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openThreadDetail();
            }
        });
    }

    private float mDownX;
    private float mDownY;
    private final float SCROLL_THRESHOLD = 10;
    private boolean isOnClick;

    private void setParentLayoutClick() {

        mLvListMessage.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent ev) {
                dismissStickerPopupWindow(); // tat popup
                switch (ev.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        mDownX = ev.getX();
                        mDownY = ev.getY();
                        isOnClick = true;
                        break;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        if (isOnClick) {
                            openThreadDetail();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (isOnClick && (Math.abs(mDownX - ev.getX()) > SCROLL_THRESHOLD || Math.abs(mDownY - ev
                                .getY()) > SCROLL_THRESHOLD)) {
                            isOnClick = false;
                            cancelCountDown();
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    private void setLayoutActionBarClick() {
        mLayoutActionBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                openThreadDetail();
                return false;
            }
        });
    }

    private void setControlStickerListener() {
        mLlStickerSuggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLlStickerSuggest.setVisibility(View.GONE);
            }
        });
        // item sticker
        mLlStickerOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleVoiceStickerClick(EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, EmoticonUtils
                        .getQuickReplySticker()[0]);
            }
        });
        // item sticker
        mLlStickerWhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleVoiceStickerClick(EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, EmoticonUtils
                        .getQuickReplySticker()[1]);
            }
        });
        // item sticker
        mLlStickerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleVoiceStickerClick(EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, EmoticonUtils
                        .getQuickReplySticker()[2]);
            }
        });
        // item sticker
        mLlStickerLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleVoiceStickerClick(EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, EmoticonUtils
                        .getQuickReplySticker()[3]);
            }
        });
    }

    private boolean sendText() {
        String mEdtContent = etMessageContent.getText().toString().trim();
        if (mEdtContent.length() > 0) {
            // add cache spanned
            Spannable contentSpannable = etMessageContent.getText();
            TextHelper.getInstant().removeUnderlines(contentSpannable);
            String messageContent = EmoticonUtils.getRawTextFromSpan(contentSpannable);
            int contentLength = messageContent.length();
            int MAX_LENGTH_DEFAULT = mApplication.getReengAccountBusiness().isCambodia()
                    ? Constants.MESSAGE.TEXT_IP_MAX_LENGTH_CAM : Constants.MESSAGE.TEXT_IP_MAX_LENGTH;
            if ((mGsmMode && contentLength > Constants.MESSAGE.TEXT_GSM_MAX_LENGTH) ||
                    (!mGsmMode && contentLength > MAX_LENGTH_DEFAULT)) {
                int maxLength = mGsmMode ? Constants.MESSAGE.TEXT_GSM_MAX_LENGTH : MAX_LENGTH_DEFAULT;
                String msgAlert = String.format(getResources().getString(R.string.alert_max_length_msg), maxLength);
                showToast(msgAlert, Toast.LENGTH_LONG);
                return false;
            } else {
                EmoticonManager.getInstance(QuickReplyActivity.this).addSpannedToEmoticonCache(messageContent,
                        contentSpannable);
                etMessageContent.setText("");
                sendTextMessage(messageContent);
                return true;
            }
        }
        return false;
    }

    private void setChatMode() {
        if (mThreadMessage == null) return;
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            //chat 1-1
            friendJid = mThreadMessage.getSoloNumber();
            if (mReengAccountBusiness.isViettel()) {
                String operatorFriend = mMessageBusiness.getOperatorFriendByJid(friendJid);
                Log.f(TAG, "operatorFiend: " + operatorFriend);
                if (PhoneNumberHelper.isViettel(operatorFriend)) {
                    PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendJid);
                    if (phoneNumber == null) {
                        //ko co trong danh ba
                        mGsmMode = false;
                    } else {
                        //co trong danh ba
                        mGsmMode = !phoneNumber.isReeng();
                    }
                }
            }
        }
    }

    private void drawActionbar(ReengMessage message) {
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(message.getSender());
        int size = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        if (phoneNumber != null) {
            mTwvName.setText(phoneNumber.getName());
            mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, size);
        } else {
            // so chua luu danh ba, thread lam quen, hien ten stranger
            String friendName;
            if (mThreadMessage != null && mThreadMessage.isStranger()) {
                friendName = mThreadMessage.getStrangerPhoneNumber().getFriendName();
                mApplication.getAvatarBusiness().
                        setStrangerAvatar(mImgAvatar, mTvwAvatar, mThreadMessage.getStrangerPhoneNumber(),
                                message.getSender(), friendName, null, size);
            } else {
                friendName = message.getSender();
                mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar,
                        mTvwAvatar, message.getSender(), size);
            }
            mTwvName.setText(friendName);
        }
    }

    private void startCountDown() {
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(TIME_DEFAULT, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    mAppStateManager.setShowQuickReply(false);
                    finish();
                }
            }.start();
        }
    }

    private void restartCountDown() {
        try {
            cancelCountDown();
            startCountDown();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void cancelCountDown() {
        try {
            if (countDownTimer != null) {
                countDownTimer.cancel();
                countDownTimer = null;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void onUpdateQuickMessageListener(final ReengMessage reengMessage) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mQuickReplyAdapter.addItem(reengMessage);
                mLvListMessage.smoothScrollToPosition(mLvListMessage.getCount() - 1);
                if (!disableCountdown) {
                    restartCountDown();
                }
                if (reengMessage.getMessageType() == ReengMessageConstant.MessageType.voiceSticker) {
                    playStickerMessage(reengMessage);
                }
            }
        });
    }

    private void sendTextMessage(String messageContent) {
        SoloSendTextMessage soloSendTextMessage = new SoloSendTextMessage(mThreadMessage,
                userNumber, currentReengMessage.getSender(), messageContent);
        if (mGsmMode) {
            soloSendTextMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);// khong tu dong gui sms phia client nen
            // ko set mod gsm
        } else {
            soloSendTextMessage.setChatMode(ReengMessageConstant.MODE_IP_IP);
        }
        mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT,
                soloSendTextMessage);
        sendMessage(soloSendTextMessage);
    }

    private void handleVoiceStickerClick(final int collectionId, final int itemId) {
        if (collectionId == preCollectionIDSticker && prePossitionSticker == itemId) {
            dismissStickerPopupWindow();
            sendVoiceSticker(collectionId, itemId);
        } else {
            preCollectionIDSticker = collectionId;
            prePossitionSticker = itemId;
            ImageLoaderManager.getInstance(mApplication).displayStickerImage(imgPreview, collectionId, itemId);
            if (popupWindowPreview != null) {
                if (!popupWindowPreview.isShowing()) {
                    popupWindowPreview.showAtLocation(etMessageContent, Gravity.CENTER, 0, 0);
                }
                mVoiceStickerPlayer.playVoiceSticker(collectionId, itemId);
                //shake when click buzz sticker
                if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID
                        && itemId == EmoticonUtils.getGeneralStickerDefault()[EmoticonUtils.BUZZ_STICKER_POSITION]
                        .getItemId()) {
                    if (mHandler == null) {
                        mHandler = new Handler();
                    }
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            WindowShaker.shake(mLayoutRect, option);
                        }
                    }, 200);
                }
                popupViewPreview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissStickerPopupWindow();
                        sendVoiceSticker(collectionId, itemId);
                    }
                });
            }
        }
    }

    private void sendVoiceSticker(int collectionId, int itemId) {
        SoloVoiceStickerMessage voiceStickerMessage = new SoloVoiceStickerMessage(mThreadMessage,
                userNumber, currentReengMessage.getSender(), collectionId, itemId);
        mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT,
                voiceStickerMessage);
        sendMessage(voiceStickerMessage);
        trackingEventSendVoiceSticker(voiceStickerMessage);
        mMessageBusiness.setSendMsgQuickReply(true);
        mMessageBusiness.markAllMessageIsReadAndCheckSendSeen(mThreadMessage, mThreadMessage.getAllMessages());
        mAppStateManager.setShowQuickReply(false);
        finish();
    }

    @Override
    public void onSystemKeyboardShown(int currentContentHeight, int keyBoardHeight) {
        Log.i(TAG, "keyboardshow");
        layoutParamsSticker = (RelativeLayout.LayoutParams) mSubLayoutQuickReply.getLayoutParams();
        layoutParamsSticker.setMargins(0, 0, 0, -stickerViewHeight);
        mSubLayoutQuickReply.setLayoutParams(layoutParamsSticker);
        mLayoutRect.setGravity(Gravity.BOTTOM);
    }

    @Override
    public void onChangeContentHeight(int currentContentHeight) {
        Log.i(TAG, "onChangeContentHeight");
    }

    @Override
    public void onSystemKeyboardHidden() {
        Log.i(TAG, "keyboardhidden");
        layoutParamsSticker = (RelativeLayout.LayoutParams) mSubLayoutQuickReply.getLayoutParams();
        if (layoutParamsSticker.bottomMargin < 0) {
            layoutParamsSticker.setMargins(0, 0, 0, 0);
            mSubLayoutQuickReply.setLayoutParams(layoutParamsSticker);
        }
        mLayoutRect.setGravity(Gravity.CENTER);
    }

    private void sendMessage(ReengMessage reengMessage) {
        reengMessage.setCState(mContactBusiness.getCStateMessage(friendJid));
        if (reengMessage instanceof SoloSendTextMessage) {
            mMessageBusiness.sendXMPPMessage(reengMessage, mThreadMessage);
        } else if (reengMessage instanceof SoloShareMusicMessage) {
            mApplication.getMusicBusiness().startTimerReceiveResponse(ReengMusicPacket.MusicAction.invite,
                    reengMessage.getPacketId(), Constants.ALARM_MANAGER.TIME_OUT_RECEIVE_INVITE_MUSIC);
            mMessageBusiness.sendInviteMusic(reengMessage, mThreadMessage);
        } else if (reengMessage instanceof SoloVoiceStickerMessage) {
            mMessageBusiness.sendXMPPMessage(reengMessage, mThreadMessage);
        } else {
            Log.i(TAG, "what's happen");
        }
        mApplication.getXmppManager().sendPresenceBackgroundOrForeground(true);//TODO send background, no mark state
    }

    @Override
    public void onUpdateStateRoom() {

    }

    @Override
    public void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread) {

    }

    @Override
    public void longClickBgrCallback(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void retryClickCallBack(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void replyClickCallBack(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void reportClickCallBack(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void onAcceptCrbtGift(ReengMessage message, MediaModel songModel) {
        openThreadDetail();
    }

    @Override
    public void onSendCrbtGift(ReengMessage message, MediaModel songModel) {
        openThreadDetail();
    }

    @Override
    public void onDeepLinkClick(ReengMessage message, String link) {
        openThreadDetail();
    }

    @Override
    public void onFakeMoClick(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void onPollDetail(ReengMessage message, boolean pollUpdate) {
        openThreadDetail();
    }

    @Override
    public void onCall(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void onBplus(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void textContentClickCallBack(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void fileContentClickCallBack(ReengMessage message, boolean isClickDownload) {
        openThreadDetail();
    }

    @Override
    public void imageContentClickCallBack(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void voicemailContentClickCallBack(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void shareContactClickCallBack(ReengMessage reengMessage) {
        openThreadDetail();
    }

    @Override
    public void voiceStickerClickCallBack(ReengMessage reengMessage, View convertView) {
        openThreadDetail();
    }

    @Override
    public void gifContentClickCallBack(ReengMessage reengMessage) {
        openThreadDetail();
    }

    @Override
    public void onMyAvatarClick() {
    }

    @Override
    public void onFriendAvatarClick(String friendJid, String friendName) {
    }

    @Override
    public void onAcceptInviteMusicClick(final ReengMessage reengMessage) {
        cancelCountDown();
        if (mThreadMessage.getThreadType() != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            return;
        }
        //neu dang trong cung nghe thi hien thi popup
        MusicBusiness musicBusiness = mApplication.getMusicBusiness();
        final String friendNumber = mThreadMessage.getSoloNumber();
        String friendName = mMessageBusiness.getFriendName(friendNumber);
        musicBusiness.showConfirmOrNavigateToSelectSong(QuickReplyActivity.this, Constants.TYPE_MUSIC.TYPE_PERSON_CHAT,
                friendNumber, friendName, new MusicBusiness.OnConfirmMusic() {
                    @Override
                    public void onGotoSelect() {


                        acceptMusicMessage(reengMessage, friendNumber);
                        mAppStateManager.setShowQuickReply(false);
                        finish();
                    }

                    @Override
                    public void onGotoChange() {
                        acceptMusicMessage(reengMessage, friendNumber);
                        mAppStateManager.setShowQuickReply(false);
                        finish();
                    }
                });
    }

    @Override
    public void onIconClickListener(View view, final Object entry, int menuId) {
        switch (menuId) {
            default:
                break;
        }
    }

    @Override
    public void onReinviteShareMusicClick(ReengMessage reengMessage) {
        MusicBusiness musicBusiness = mApplication.getMusicBusiness();
        String currentNumberInRoom = musicBusiness.getCurrentNumberFriend();
        Resources res = getResources();
        if (!musicBusiness.isExistListenMusic() ||
                musicBusiness.isExistListenerGroup() ||
                musicBusiness.isExistListenerRoom()) {// khong nghe, group, room
            if (musicBusiness.isExistListenMusic()) {
                musicBusiness.clearSessionAndNotifyMusic();
            }
            MediaModel songModel = reengMessage.getSongModel(musicBusiness);
            // xoa tren mem va db
            mThreadMessage.getAllMessages().remove(reengMessage);
            mMessageBusiness.deleteAMessage(mThreadMessage, reengMessage);
            String friendPhoneNumber = mThreadMessage.getSoloNumber();
            sendReinviteMusicMessage(ReengMessageConstant.MessageType.inviteShareMusic, friendPhoneNumber, songModel);
            mAppStateManager.setShowQuickReply(false);
            finish();
        } else if (!TextUtils.isEmpty(currentNumberInRoom) && !musicBusiness.isWaitingStrangerMusic()) {//1-1
            showToast(String.format(res.getString(R.string.already_in_room_music),
                    mMessageBusiness.getFriendName(currentNumberInRoom)), Toast.LENGTH_SHORT);
        } else {//stranger
            showToast(String.format(res.getString(R.string.already_in_room_music),
                    res.getString(R.string.stranger)), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onTryPlayMusicClick(ReengMessage reengMessage) {
        Log.i(TAG, "onTryPlayMusicClick");
        openThreadDetail();
    }

    @Override
    public void onAcceptMusicGroupClick(ReengMessage reengMessage) {
        Log.i(TAG, "onAcceptMusicGroupClick");
        openThreadDetail();
    }

    @Override
    public void onFollowRoom(ReengMessage reengMessage) {

    }

    @Override
    public void onInviteMusicViaFBClick(ReengMessage reengMessage) {

    }

    @Override
    public void videoContentClickCallBack(ReengMessage message, View convertView) {
        openThreadDetail();
    }

    @Override
    public void onGreetingStickerPreviewCallBack(StickerItem stickerItem) {
        openThreadDetail();
    }

    @Override
    public void shareLocationClickCallBack(ReengMessage reengMessage) {
        Log.i(TAG, "shareLocationClickCallBack");
        openThreadDetail();
    }

    @Override
    public void onInfoMessageCallBack() {
        openThreadDetail();
    }

    @Override
    public void onLuckyWheelHelpClick(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void onWatchVideoClick(ReengMessage message, boolean isUser) {
        openThreadDetail();
    }

    @Override
    public void onClickPreviewUrl(ReengMessage message, String url) {
        openThreadDetail();
    }

    @Override
    public void onClickOpenGiftLixi(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void onClickReplyLixi(ReengMessage message) {
        openThreadDetail();
    }

    @Override
    public void onSmartTextClick(String content, int type) {
        openThreadDetail();
    }

    @Override
    public void onClickImageReply(ReplyMessage replyMessage) {

    }

    @Override
    public void onOpenViewReaction(View anchorView, ReengMessage reengMessage) {

    }

    @Override
    public void onOpenListReaction(ReengMessage reengMessage, View view) {

    }


    @Override
    public void onClickActionChangeNumber(ReengMessage message, ReengMessageConstant.ActionChangeNumber actionChangeNumber) {

    }

    @Override
    public void onClickReaction(ReengMessage reengMessage, View viewReact) {

    }

    private void sendReinviteMusicMessage(ReengMessageConstant.MessageType type, String to, MediaModel songModel) {
        if (songModel == null) {
            return;
        }
        SoloShareMusicMessage soloShareMusicMessage = new SoloShareMusicMessage(mThreadMessage, userNumber, to);
        String packetId = PacketMessageId.getInstance().genShareMusicPacketId(mThreadType,
                ReengMessagePacket.SubType.music_invite);
        soloShareMusicMessage.setPacketId(packetId);
        soloShareMusicMessage.setSongId(ReengMessage.SONG_ID_DEFAULT_NEW);
        soloShareMusicMessage.setSongModel(songModel);
        // lay packet id lam session id
        soloShareMusicMessage.setImageUrl(soloShareMusicMessage.getPacketId());
        soloShareMusicMessage.setMessageType(ReengMessageConstant.MessageType.inviteShareMusic);
        String content = String.format(getResources().getString(R.string.invite_share_music_send),
                mMessageBusiness.getFriendName(to));
        String contentBold = String.format(getResources().getString(R.string.invite_share_music_send),
                TextHelper.textBoldWithHTML(mMessageBusiness.getFriendName(to)));
        soloShareMusicMessage.setContent(content);
        soloShareMusicMessage.setFileName(contentBold);
        Log.i(TAG, "soloShareMusicMessage: " + soloShareMusicMessage.toString());
        mMessageBusiness.insertNewMessageBeforeSend(mThreadMessage, mThreadType, soloShareMusicMessage);
        // tat media neu truoc do bi timeout
        if (mApplication.getPlayMusicController() != null) {
            mApplication.getPlayMusicController().closeNotifyMusicAndClearDataSong();
        }
        sendMessage(soloShareMusicMessage);
    }

    /**
     * dong y cung nghe
     *
     * @param message
     */
    private void acceptMusicMessage(ReengMessage message, String friendNumber) {
        if (message.getMusicState() != ReengMessageConstant.MUSIC_STATE_WAITING) {
            showToast(getResources().getString(R.string.invite_share_music_time_out),
                    Toast.LENGTH_LONG);
            return;
        }
        // huy ban tin moi truoc do neu co
        mApplication.getMusicBusiness().cancelLastMusicInviteMessage(mThreadMessage);
        message.setMusicState(ReengMessageConstant.MUSIC_STATE_ACCEPTED);
        message.setFileName(String.format(getResources().getString(R.string.invite_share_music_accepted),
                TextHelper.textBoldWithHTML(mMessageBusiness.getFriendName(friendNumber))));
        message.setContent(String.format(getResources().getString(R.string.invite_share_music_accepted),
                mMessageBusiness.getFriendName(friendNumber)));
        CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(message);
        //        notifyChangeAndSetSelection();
        mMessageBusiness.updateAllFieldsOfMessage(message);
        // ban invite nhan dc thi session id trung packet id
        mApplication.getMusicBusiness().onAcceptAndPlayMusic(message.getImageUrl(),
                friendNumber, message.getSongModel(mApplication.getMusicBusiness()));
        //Gui online
        isTouchQuickReply = true;
        mAppStateManager.applicationEnterForeground(QuickReplyActivity.this, isTouchQuickReply);
    }

    @Override
    public void onCancelInviteMusicClick(ReengMessage reengMessage) {
        //ko lam j ca
    }

    private void openThreadDetail() {
        mAppStateManager.setShowQuickReply(false);
        isTouchQuickReply = true;
        mAppStateManager.applicationEnterForeground(QuickReplyActivity.this, isTouchQuickReply);
        cancelCountDown();
        saveContentDraftMessage();
        Intent intent;
        intent = new Intent(getApplicationContext(), ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, currentReengMessage.getThreadId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (mAppStateManager.isScreenLocker()) {
            Window win = getWindow();
            win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            Log.i(TAG, "----------isScreenLocker: ");
        }
        mApplication.startActivity(intent);
        finish();
    }

    @Override
    public void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread) {
//        mMessageBusiness.insertNewMessageToMemory(thread, message);
        if (threadIDQuickReply == message.getThreadId()) {
            Log.i(TAG, "notifyNewIncomingMessage same thread id ");
            onUpdateQuickMessageListener(message);
            mMessageBusiness.updateAllFieldsOfMessage(message);
        }
    }

    private void getUnreadMessageAndDrawDetail() {
        loadUnReadMessage();
        // khong load duoc message nao
        if (messages.isEmpty()) {
            mAppStateManager.setShowQuickReply(false);
            finish();
        } else {
            if (mQuickReplyAdapter == null) {
                initAdapter();
            } else {
                mQuickReplyAdapter.setListItem(messages);
                mQuickReplyAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onRefreshMessage(int threadId) {
        Log.d("-------------", "onRefreshMessage");
        if (threadIDQuickReply == threadId) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    getUnreadMessageAndDrawDetail();
                }
            });
        }
    }

    public void onShowReceiverOfMessage(ArrayList<String> list, ReengMessage message) {
    }

    public void notifyNewOutgoingMessage() {
    }

    public void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode) {
    }

    public void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError) {
    }

    public void notifyMessageSentSuccessfully(int threadId) {
    }

    public void onUpdateStateTyping(String phoneNumber, ThreadMessage thread) {
    }

    @Override
    public void onUpdateMediaDetail(MediaModel mediaModel, int threadId) {
        Log.d(TAG, "onUpdateMediaDetail from sv");
        onRefreshMessage(threadId);
    }

    @Override
    public void onUpdateStateAcceptStranger(String friendJid) {

    }

    @Override
    public void onSendMessagesError(List<ReengMessage> reengMessageList) {

    }

    private void playStickerMessage(final ReengMessage reengMessage) {
        if (settingAutoPlaySticker) {
            try {
                final int collectionId = Integer.valueOf(reengMessage.getFileName());
                final int itemId = (int) reengMessage.getSongId();
                if (mHandler == null) {
                    mHandler = new Handler();
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mVoiceStickerPlayer.playVoiceSticker(collectionId, itemId);
                        reengMessage.setSize(1);    //Da play roi, ko play lai o trong ThreadDetail nua
                        mMessageBusiness.updateAllFieldsOfMessage(reengMessage);
                    }
                });
                if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID
                        && itemId == EmoticonUtils.getGeneralStickerDefault()[EmoticonUtils.BUZZ_STICKER_POSITION]
                        .getItemId()) {

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            WindowShaker.shake(mLayoutRect, option);
                        }
                    }, 200);
                }
            } catch (NumberFormatException nfe) {
                Log.e(TAG, "Exception", nfe);
            }
        }
    }

    /**
     * gui thong ke ga send voice sticker
     *
     * @param voiceStickerMessage
     */
    private void trackingEventSendVoiceSticker(SoloVoiceStickerMessage voiceStickerMessage) {
        int gaStickerCategory = R.string.ga_category_voice_sticker;
        // set action default
        int collectionId = Integer.valueOf(voiceStickerMessage.getFileName());
        int itemId = (int) voiceStickerMessage.getSongId();
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            trackingEvent(gaStickerCategory,
                    getResources().getString(R.string.ga_action_voice_sticker_defaule),
                    String.format(getResources().getString(R.string.ga_label_voice_sticker_item), itemId));
        } else {
            StickerCollection stickerCollection =
                    mApplication.getStickerBusiness().getStickerCollectionById(collectionId);
            if (stickerCollection != null) {
                String collectionName = stickerCollection.getCollectionName();
                trackingEvent(gaStickerCategory, collectionName,
                        String.format(getResources().getString(R.string.ga_label_voice_sticker_item), itemId));
            }
        }
    }
}
package com.metfone.selfcare.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.DeviceUtils;
import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.CheckPhoneInfo;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.SignIn;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.UserOldResponse;
import com.metfone.selfcare.module.keeng.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.utils.CustomBase64;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.view.CamIdEditText;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.business.UserInfoBusiness.isEmailValid;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.util.Utilities.isValidEmail;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

public class CreateYourAccountActivity extends BaseSlidingFragmentActivity {

    private static final String FULL_NAME = "fullname";
    private static final String GENDER = "gender";
    private static final String BIRTHDAY = "birthday";
    private static final String METHOD = "method";
    private static final String PASSWORD = "password";
    private static final String MASTER_OTP = "112233";
    private static final String CODE = "code";
    private static final int FACEBOOK_METHOD = 1;
    private static final int GOOGLE_METHOD = 2;
    private static final int PHONE_METHOD = 0;
    private static final String EMAIL = "email";
    private static final String SDF_OTH_YEAR = "DD/MM/YYYY";
    private static final int FEMALE_POSITION = 1;
    private static final int MALE_POSITION = 0;
    @BindView(R.id.contrainLayout)
    ConstraintLayout contrainLayout;
    @BindView(R.id.edt_full_name)
    CamIdEditText edtFullName;
    @BindView(R.id.edt_email)
    CamIdEditText edtEmail;
    @BindView(R.id.tv_full_name)
    CamIdTextView tvFullName;
    @BindView(R.id.tv_email)
    CamIdTextView tvEmail;
    @BindView(R.id.spinner_gender)
    Spinner mSpinnerGender;
    @BindView(R.id.tv_dob)
    CamIdTextView mEdtBirthday;
    @BindView(R.id.layout_date_of_birth)
    ConstraintLayout layoutDateOfBirth;
    @BindView(R.id.btn_confirm)
    AppCompatTextView btnConfirm;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.frame_by)
    LinearLayout mFrameLayout;
    @BindView(R.id.tvTermsAndConditions)
    CamIdTextView tvTermsAndConditions;
    ApiService apiService;
    ArrayAdapter<CharSequence> adapter;
    List<String> stringlist;
    ArrayAdapter<String> arrayadapter;
    boolean isSelect = false;
    View.OnTouchListener onTouchListener = (view, event) -> {
        hideKeyboard();
        return view.performClick();
    };
    private Intent intent;
    private String name = "";
    private String email = "";
    private String birthday = null;
    private int gender = -1;
    private int currentMethod = -1;
    private String phoneNumber = "";
    private String otp = "";
    private String password = "";
    private ApplicationController mApplication;
    private Resources mRes;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;
    private SharedPref mSharedPref;
    private String code;

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        ButterKnife.bind(this);
        setColorStatusBar(R.color.transparent);
        initData();
        initView();

    }

    private void initData() {
        mSharedPref = new SharedPref(this);
        mApplication = (ApplicationController) getApplicationContext();
        apiService = RetrofitInstance.getInstance().create(ApiService.class);
        mRes = getResources();
        intent = getIntent();
        if (intent != null && intent.getStringExtra(CODE) != null && intent.getStringExtra(CODE).equals("68")) {
            CheckPhoneInfo checkPhoneInfo = new CheckPhoneInfo(intent.getStringExtra(EnumUtils.PHONE_NUMBER_KEY));
            CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest(checkPhoneInfo, "", "", "", "");
            apiService.checkInfoOldUsed(checkPhoneRequest).enqueue(new Callback<UserOldResponse>() {
                @SneakyThrows
                @Override
                public void onResponse(Call<UserOldResponse> call, retrofit2.Response<UserOldResponse> response) {
                    if (response.body() != null && response.body().getData() != null) {
                        edtFullName.setText(response.body().getData().getFullName());
                        if (response.body().getData().getEmail() != null) {
                            edtEmail.setText(response.body().getData().getEmail());
                        }
                        mSpinnerGender.setSelection(response.body().getData().getGender());
                        if ("en".equals(UserInfoBusiness.getCurrentLanguage(CreateYourAccountActivity.this, mApplication))) {
                            if (response.body() != null && response.body().getData().getDateOfBirth() != null) {
                                String inputDateStr = response.body().getData().getDateOfBirth();
                                DateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
                                DateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
                                Date date1 = inputFormat.parse(inputDateStr);
                                String outputDateStr = outputFormat.format(date1);
                                mEdtBirthday.setText(outputDateStr);
                            }
                        } else {
                            if (response.body() != null && response.body().getData().getDateOfBirth() != null) {
                                DateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
                                DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                                String inputDateStr = response.body().getData().getDateOfBirth();
                                Date date1 = inputFormat.parse(inputDateStr);
                                String outputDateStr = outputFormat.format(date1);
                                mEdtBirthday.setText(outputDateStr);
                            }
                        }
                        mEdtBirthday.setTextColor(getResources().getColor(R.color.text_password));
                    }
                }

                @Override
                public void onFailure(Call<UserOldResponse> call, Throwable t) {
                }
            });
        }
        code = intent.getStringExtra(CODE);
        if (intent != null) {
            currentMethod = intent.getIntExtra(METHOD, -1);
            switch (currentMethod) {
                case FACEBOOK_METHOD:
                    name = intent.getStringExtra(FULL_NAME);
                    gender = intent.getIntExtra(GENDER, -1);
                    birthday = intent.getStringExtra(BIRTHDAY);
                    mFrameLayout.setVisibility(View.VISIBLE);
                    email = intent.getStringExtra(EMAIL);
                    break;
                case GOOGLE_METHOD:
                    name = intent.getStringExtra(FULL_NAME);
                    mFrameLayout.setVisibility(View.VISIBLE);
                    email = intent.getStringExtra(EMAIL);
                    break;
                case PHONE_METHOD:
                    phoneNumber = intent.getStringExtra(EnumUtils.PHONE_NUMBER_KEY);
                    password = intent.getStringExtra(PASSWORD);
                    otp = intent.getStringExtra(EnumUtils.OTP_KEY);
                    break;
            }
        }
//        getUserInformation();
    }

    private void initView() {
//        setWhiteStatusBar();
//        setColorStatusBar(R.color.white);
        edtFullName.setText(name);
        edtEmail.setText(email);
        detectKeyboard();
        if (gender == Constants.CONTACT.GENDER_FEMALE) {
            mSpinnerGender.setSelection(FEMALE_POSITION);
        } else {
            mSpinnerGender.setSelection(MALE_POSITION);
        }


        //
        String fullNameText = getString(R.string.text_full_name_create_account);
        String emailText = getString(R.string.text_email_create_account);
        Spannable span1 = new SpannableString(fullNameText);
        Spannable span2 = new SpannableString(emailText);

        span1.setSpan(new ForegroundColorSpan(Color.parseColor("#C41627")), fullNameText.indexOf("*"), fullNameText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        span2.setSpan(new ForegroundColorSpan(Color.parseColor("#C41627")), emailText.indexOf("*"), emailText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvFullName.setText(span1, TextView.BufferType.SPANNABLE);
        tvEmail.setText(span2, TextView.BufferType.SPANNABLE);


        if (!TextUtils.isEmpty(birthday) && birthday != null) {
            mEdtBirthday.setText(birthday);
        } else {
            mEdtBirthday.setText("");

        }
        //display date picker
        layoutDateOfBirth.setOnClickListener(view1 -> {
            showDialogTimePicker();
        });


        tvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigateActivityHelper.navigateToTermsAndConditions(CreateYourAccountActivity.this);
            }
        });

        btnConfirm.setOnClickListener(view3 -> {
            if (validInput()) {
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    if (currentMethod != PHONE_METHOD) {
                        signIn();
                    } else {
                        getUserInformation();
                    }
                }
            }
        });
        setupUI(contrainLayout);

        mEdtBirthday.setTextColor(getResources().getColor(R.color.textBlack));
        stringlist = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.sex_your)));
        arrayadapter = new ArrayAdapter<String>(CreateYourAccountActivity.this, R.layout.display_spinner, stringlist) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position + 0, convertView, parent);
                TextView textView = view.findViewById(R.id.textView1);
                if (position < 0) {
                    textView.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    textView.setTextColor(getResources().getColor(R.color.color_text_term_agreement));
                }
                return view;
            }

            public int getCount() {
                return stringlist.size() - 1;
            }
        };

        arrayadapter.setDropDownViewResource(R.layout.display_spinner);
        mSpinnerGender.setAdapter(arrayadapter);
        mSpinnerGender.setSelection(3);
        mEdtBirthday.setOnTouchListener(onTouchListener);
    }

    private boolean validInput() {
        if (TextUtils.isEmpty(edtFullName.getText().toString().trim())) {
            showDialog(getString(R.string.text_empty_fullname));
            return false;
        } else if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            showDialog(getString(R.string.text_empty_email));
            return false;
        } else if (!isValidEmail(edtEmail.getText().toString().trim())) {
            showDialog(getString(R.string.text_invalid_email));
            return false;
        } else {
            return true;
        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void handleUpdateUser(UserInfo currentUser) {
        String fullName = edtFullName.getText().toString().trim();
        String email = edtEmail.getText().toString();
        int gender;
        if (getString(R.string.sex_male).equals(mSpinnerGender.getSelectedItem().toString())) {
            gender = 1;
        } else if (getString(R.string.sex_female).equals(mSpinnerGender.getSelectedItem().toString())) {
            gender = 2;
        } else {
            gender = 0;
        }
        String dob = "";

        if (!TextUtils.isEmpty(mEdtBirthday.getText().toString().trim())) {
            dob = TimeHelper.convertTimeToAPi(mEdtBirthday.getText().toString().trim(), UserInfoBusiness.getCurrentLanguage(CreateYourAccountActivity.this, mApplication));
        }
        Gson gson = new Gson();
        String json = gson.toJson(currentUser);
        UserInfo userInfo = gson.fromJson(json, UserInfo.class);
        userInfo.setFull_name(fullName);
        userInfo.setEmail(email);
        userInfo.setAvatar(currentUser.getAvatar() == null ? "" : currentUser.getAvatar());
        if (gender >= 0 && gender <= 2) {
            userInfo.setGender(gender);
        }
        if (!TextUtils.isEmpty(dob)) {
            userInfo.setDate_of_birth(dob);
        }
        if (email.isEmpty()) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_email_address),
//                    Toast.LENGTH_SHORT).show();
        } else if (!isEmailValid(email.trim())) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_email_address),
//                    Toast.LENGTH_SHORT).show();
        } else if (fullName.isEmpty()) {
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_your_full_name),
//                    Toast.LENGTH_SHORT).show();
        } else {
            updateUser(userInfo);
        }
    }

    private void moveToAddMoreMethodActivity() {
        Intent intent = new Intent(this, AddMoreMethodActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(METHOD, currentMethod);
        startActivity(intent);
    }

    private void signIn() {
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        SignIn signIn = new SignIn();
        switch (currentMethod) {
            case FACEBOOK_METHOD:
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                signIn.setSocialToken(accessToken.getToken());
                Log.d(TAG, "Facebook Access Token is " + accessToken.getToken());
                signIn.setType("facebook");
                break;
            case GOOGLE_METHOD:
                GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);
                signIn.setSocialToken(googleSignInAccount.getIdToken());
                Log.d(TAG, "Google token id is " + googleSignInAccount.getIdToken());
                signIn.setType("google");
                break;
        }

        SignInRequest signInRequest = new SignInRequest(signIn, "", "", "", "");

        apiService.signIn(DeviceUtils.getUniqueDeviceId(),signInRequest).enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                SignInResponse signInResponse = response.body();
                progressBar.setVisibility(View.GONE);
                if (signInResponse != null) {
                    if (signInResponse.getCode().equals("00")) {
                        String token = "Bearer " + signInResponse.getData().getAccess_token();
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefresh_token());
                        getUserInformation();
                        //      NavigateActivityHelper.navigateToHomeScreenActivity(CreateYourAccountActivity.this, true,true);
                    } else {
                        if (currentMethod == FACEBOOK_METHOD) {
                            showDialog("Sign Up Facebook Failed " + signInResponse.getMessage());
                        } else if (currentMethod == GOOGLE_METHOD) {
                            showDialog("Sign Up Google Failed" + signInResponse.getMessage());
                        } else {
                            showDialog("Sign Up Failed" + signInResponse.getMessage());
                        }
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(CreateYourAccountActivity.this, "Sign In Failed", Toast.LENGTH_SHORT).show();
//                        showDialog(jObjError.getJSONObject("error").getString("message"));
                    } catch (Exception e) {
                        showDialog(e.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                ToastUtils.showToast(CreateYourAccountActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void showDialogTimePicker() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        String dateChoseDefault;
        if (TextUtils.isEmpty(mEdtBirthday.getText().toString()) || getString(R.string.sdf_oth_year_km).equals(mEdtBirthday.getText().toString())) {
            dateChoseDefault = UserInfoBusiness.getDefaultDate();

        } else {
            if (!getString(R.string.text_english_code).equals(UserInfoBusiness.getCurrentLanguage(this, mApplication))) {
                dateChoseDefault = mEdtBirthday.getText().toString();
            } else {
                dateChoseDefault = TimeHelper.convertTimeEnToKh(mEdtBirthday.getText().toString());
            }
        }
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(this, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                if (!"en".equals(UserInfoBusiness.getCurrentLanguage(CreateYourAccountActivity.this, mApplication))) {
                    mEdtBirthday.setText(dateDesc);
                } else {
                    mEdtBirthday.setText(TimeHelper.convertKhToEn(dateDesc));
                }
                mEdtBirthday.setTextColor(getResources().getColor(R.color.color_tvphone));
            }
        }).textConfirm(getString(R.string.confirm_button)) //text of confirm button
            .textCancel(getString(R.string.cancel_button)) //text of cancel button
            .btnTextSize(16) // button text size
            .viewTextSize(25) // pick view text size
            .colorCancel(getResources().getColor(R.color.color_of_cancel_button)) //color of cancel button
            .colorConfirm(getResources().getColor(R.color.color_of_confirm_button))//color of confirm button
            .dateChose(dateChoseDefault) // date chose when init popwindow
            .minYear(1900)
            .maxYear(mYear + 1) // max year in loop
            .build();
        pickerPopWin.showPopWin(this);
    }

    public void setupUI(View view) {
        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(CreateYourAccountActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof TextView) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    private void updateUser(UserInfo userInfo) {
        progressBar.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.updateUser(token, new BaseDataRequest<>(userInfo, getString(R.string.text_string), getString(R.string.text_string), getString(R.string.text_string), getString(R.string.text_string)))
            .enqueue(new Callback<BaseResponse<BaseUserResponseData>>() {
                @Override
                public void onResponse(Call<BaseResponse<BaseUserResponseData>> call, Response<BaseResponse<BaseUserResponseData>> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.code() == ERROR_UNAUTHORIZED) {
                        restartApp();
                        return;
                    }
                    if (response.body() != null) {
                        BaseResponse<BaseUserResponseData> baseResponse = response.body();
                        if ("00".equals(baseResponse.getCode())) {
                            if (baseResponse.getData() != null) {
                                DynamicSharePref.getInstance().put(Constants.KEY_FINGER_PHONE, phoneNumber);
                                DynamicSharePref.getInstance().put(Constants.KEY_FINGER_ENCODE_PW, CustomBase64.getEncoder().encodeToString(password.getBytes()));
                                UserInfoBusiness userInfoBusiness = new UserInfoBusiness(CreateYourAccountActivity.this);
                                UserInfo userInfo = response.body().getData().getUser();
                                userInfoBusiness.setUser(userInfo);
                                ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                              reengAccount.setToken(token);
                                if (reengAccount != null) {
                                    reengAccount.setName(userInfo.getFull_name());
                                    reengAccount.setEmail(userInfo.getEmail());
                                    reengAccount.setBirthday(userInfo.getDate_of_birth());
                                    reengAccount.setAddress(userInfo.getAddress());
                                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                                }

                                if (baseResponse.getData().getServices() != null) {
                                    userInfoBusiness.setServiceList(response.body().getData().getServices());
                                }
                                userInfoBusiness.setIdentifyProviderList(baseResponse.getData().getIdentifyProviders());
                                if (TextUtils.isEmpty(userInfo.getPhone_number()) || baseResponse.getData().getIdentifyProviders().size() < 2) {
                                    moveToAddMoreMethodActivity();
                                } else {
                                    NavigateActivityHelper.navigateToHomeScreenActivity(CreateYourAccountActivity.this, false, true);
                                }
                            }
                        } else {
                            ToastUtils.showToast(CreateYourAccountActivity.this, baseResponse.getMessage());
                        }
                    } else {

                        ToastUtils.showToast(CreateYourAccountActivity.this, getString(R.string.text_update_infomation_fail));
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<BaseUserResponseData>> call, Throwable t) {
                    ToastUtils.showToast(CreateYourAccountActivity.this, getString(R.string.service_error));
                }
            });
    }

    private void getUserInformation() {
        progressBar.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        FireBaseHelper.getInstance(ApplicationController.self()).checkServiceAndRegister(true);
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(CreateYourAccountActivity.this);
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getUser() != null) {
                            handleUpdateUser(response.body().getData().getUser());
                        } else {
                            ToastUtils.showToast(CreateYourAccountActivity.this, response.body().getMessage());
                        }

                    } else {
                        ToastUtils.showToast(CreateYourAccountActivity.this, response.body().getMessage());

                    }

                } else {
                    ToastUtils.showToast(CreateYourAccountActivity.this, getString(R.string.get_user_infor_fail));
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                ToastUtils.showToast(CreateYourAccountActivity.this, getString(R.string.service_error));

            }
        });
    }

    private void doBack() {
        if (currentMethod == PHONE_METHOD) {
            Intent intent = new Intent(CreateYourAccountActivity.this, LoginActivity.class);
            startActivity(intent, false);
        }
        finish();
    }

    public void detectKeyboard() {
        contrainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                contrainLayout.getWindowVisibleDisplayFrame(r);
                int heightDiff = contrainLayout.getRootView().getHeight() - (r.bottom - r.top);
                int screenHeight = contrainLayout.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d(TAG, "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    mFrameLayout.setVisibility(View.GONE);

                } else {
                    // keyboard is closed
                    mFrameLayout.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //Do nothing
    }
}


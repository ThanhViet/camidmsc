package com.metfone.selfcare.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.exifinterface.media.ExifInterface;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.util.Utilities;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.CropImageNew.MonitoredActivity;
import com.metfone.selfcare.ui.CropImageNew.Utils;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

public class SquareCropImageActivity extends MonitoredActivity {
    Unbinder unbinder;
    @BindView(R.id.btnCancel)
    TextView btnCancel;
    @BindView(R.id.btnScale)
    TextView btnScale;
    @BindView(R.id.cropImageView)
    CropImageView cropImageView;
    private Uri mSaveUri = null;
    private String mImageOutputPath;
    private ContentResolver mContentResolver;
    private String filePath = "";
    private ApplicationController mApp;
    private Bitmap.CompressFormat mOutputFormat = Bitmap.CompressFormat.JPEG;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square_crop_image);
        unbinder = ButterKnife.bind(this);
        mApp = (ApplicationController) getApplication();
        mContentResolver = getContentResolver();
        changeStatusBar(Color.parseColor("#161819"));


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            filePath = bundle.getString(CropView.IMAGE_PATH);
            Log.i(TAG, "file path: " + filePath);
            mImageOutputPath = bundle.getString(CropView.OUTPUT_PATH);
            mSaveUri = getImageUri(mImageOutputPath);
            File file = new File(filePath);
            Uri imageUri = Uri.fromFile(file);
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            try {
                ExifInterface exif = new ExifInterface(filePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                }
                else if (orientation == 3) {
                    matrix.postRotate(180);
                }
                else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap
            }
            catch (Exception e) {

            }
            cropImageView.setImageBitmap(bitmap);
            cropImageView.setAspectRatio(1,1);
//            Glide.with(this)
//                    .load(imageUri).override(100, 100).
//                    error(R.drawable.ic_avatar_default).placeholder(R.drawable.ic_avatar_default)
//                    .into(cropImageView);
        }
    }

    @Optional
    @OnClick({R.id.btnBack, R.id.btnCancel, R.id.btnScale})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnScale:
                try {
                    Bitmap croppedImage = cropImageView.getCroppedImage();
                    saveOutput(croppedImage);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    finish();
                }
                break;
            case R.id.btnBack:
            case R.id.btnCancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
    private void saveOutput(Bitmap croppedImage) {
        if (mSaveUri != null) {
            OutputStream outputStream = null;
            try {
                outputStream =  mContentResolver.openOutputStream(mSaveUri);
                if (outputStream != null) {
                    croppedImage.compress(mOutputFormat, 90, outputStream);
                }
            } catch (IOException ex) {
                Log.e(TAG, "Cannot open file: " + mSaveUri, ex);
                setResult(RESULT_CANCELED);
                finish();
                return;
            } finally {
                Utils.closeSilently(outputStream);
            }

            Bundle extras = new Bundle();
            Intent intent = new Intent(mSaveUri.toString());
            intent.putExtras(extras);
            intent.putExtra(CropView.OUTPUT_PATH, mImageOutputPath);
            intent.putExtra(CropView.ORIENTATION_IN_DEGREES, Utils.getOrientationInDegree(this));
            setResult(RESULT_OK, intent);
        } else {
            Log.e(TAG, "not defined image url");
        }
        croppedImage.recycle();
        finish();
    }

    private Uri getImageUri(String path) {
        return FileHelper.fromFile(mApp, new File(path));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}

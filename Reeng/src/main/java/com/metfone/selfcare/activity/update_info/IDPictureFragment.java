package com.metfone.selfcare.activity.update_info;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.ImageBrowserActivity;
import com.metfone.selfcare.activity.update_info.listener.IOCRDetect;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.databinding.FragmentIdPictureBinding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.account.VerifyInfo;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDetectORCResponse;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.MappingUtils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import retrofit2.Response;

/**
 * Created by toanns1 on 7/27/2017.
 */

public class IDPictureFragment extends BaseFragment {

    private int uploadOption = ImageProfileConstant.IMAGE_AVATAR;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;

    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private String filePathFS;
    private String filePathBS;
    private String filePathYS;
    private String typePhoto;

    private FragmentIdPictureBinding binding;
    private IOCRDetect mIOCRDetect;
    private BaseMPSuccessDialog baseMPSuccessDialog;

    private BottomSheetDialog bottomSheetDialog;
    View.OnClickListener setBottomSheetItemClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvTakeAPhoto:
                    takeAPhoto(uploadOption);
                    break;
                case R.id.tvSelectFromGallery:
                    openGallery(uploadOption);
                    break;
            }
            bottomSheetDialog.dismiss();

        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mIOCRDetect = (IOCRDetect) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentIdPictureBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApplication = (ApplicationController) requireActivity().getApplication();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        setAction();
    }

    private void setAction() {
        binding.clFrontSize.setOnClickListener(v -> {
            typePhoto = EnumUtils.VerifyImageTypeDef.FRONT_SIDE;
            showPopupChangPhoto();
        });

        binding.clBackSide.setOnClickListener(v -> {
            typePhoto = EnumUtils.VerifyImageTypeDef.BACK_SIDE;
            showPopupChangPhoto();
        });

        binding.clYourSelfie.setOnClickListener(v -> {
            typePhoto = EnumUtils.VerifyImageTypeDef.YOUR_SELFIE;
            showPopupChangPhoto();
        });

        binding.btnBack.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).handleBack(0);
        });
        binding.btnNext.setOnClickListener(v -> {
            if (checkPicture()) {
                ((ConfirmIdentityInfoActivity) requireActivity()).handleNext(0);
            }
        });
        binding.btnUpdate.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).handleUpdate();
        });
    }

    private void showDialogError() {
        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                R.drawable.image_error_dialog,
                getContext().getString(R.string.empty_string),
                getContext().getString(R.string.text_error_detect_ocr_fail),
                false);
        baseMPSuccessDialog.show();
    }

    private void detectOCRImage(String filePathFS) {
        binding.pbLoading.setVisibility(View.VISIBLE);
        String base64 = "";
        if (filePathFS != null && !filePathFS.isEmpty()) {
            base64 = ImageUtils.getFileToByte(filePathFS);
        }
        String type = ((ConfirmIdentityInfoActivity) requireActivity()).getTypeID();
        String id = MappingUtils.getIDType(requireContext(), type);
        new MetfonePlusClient().wsDetectORCImage(id, base64, new MPApiCallback<WsDetectORCResponse>() {
            @Override
            public void onResponse(Response<WsDetectORCResponse> response) {
                binding.pbLoading.setVisibility(View.GONE);
                if (response.body() != null && response.body().getResult() != null) {
                    if (response.body().getResult().getWsResponse() != null) {
                        if ("0".equals(response.body().getResult().getWsResponse().getErrorCode())) {
                            if (mIOCRDetect != null) {
                                mIOCRDetect.onSuccess(response.body().getResult().getWsResponse());
                            } else {
                                showDialogError();
                            }
                        } else {
                            showDialogError();
                        }
                    } else {
                        showDialogError();
                    }
                } else {
                    showDialogError();
                }
            }

            @Override
            public void onError(Throwable error) {
                binding.pbLoading.setVisibility(View.GONE);
                showDialogError();
            }
        });
    }

    public void takeAPhoto(int typeUpload) {
        if (PermissionHelper.declinedPermission(requireContext(), Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(((BaseSlidingFragmentActivity) requireActivity()),
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            dispatchTakePictureIntent(typeUpload);
        }
    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent(int typeUpload) {
        typeUploadImage = typeUpload;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, currentPhotoPath).apply();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(TAG, "Exception", ex);
                ToastUtils.showToast(requireContext(), getString(R.string.prepare_photo_fail));
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileHelper.fromFile(mApplication, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constants.ACTION.ACTION_TAKE_PHOTO);
            }
        }
    }

    public void openGallery(int typeUpload) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(requireContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    private void showPopupChangPhoto() {
        bottomSheetDialog = new BottomSheetDialog(requireContext(), R.style.NewBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.fragment_botton_sheet_dialog_verify);
        TextView tvTakeAPhoto = bottomSheetDialog.findViewById(R.id.tvTakeAPhoto);
        TextView tvSelectFromGallery = bottomSheetDialog.findViewById(R.id.tvSelectFromGallery);
        tvTakeAPhoto.setOnClickListener(setBottomSheetItemClick);
        tvSelectFromGallery.setOnClickListener(setBottomSheetItemClick);
        bottomSheetDialog.setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            CardView bottomSheet = (CardView) d.findViewById(R.id.cvContent);
            if (bottomSheet == null)
                return;
            bottomSheet.setBackground(null);
        });
        bottomSheetDialog.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                // get information from facebook
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        Log.d(TAG, "onActivityResult ACTION_PICK_PICTURE");
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && !picturePath.isEmpty()) {
                            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                                String pathImage = picturePath.get(0);
                                resizeImage(pathImage);
                                setAvatarEditProfile(pathImage);
                                if (typePhoto.equals(EnumUtils.VerifyImageTypeDef.FRONT_SIDE)) {
                                    String type = ((ConfirmIdentityInfoActivity) requireActivity()).getTypeID();
                                    String id = MappingUtils.getIDType(requireContext(), type);
                                    if (id.equals("1") || id.equals("3")) {
                                        detectOCRImage(pathImage);
                                    }
                                }
                            }
                        }
                    }
                    if (requireActivity() instanceof BaseSlidingFragmentActivity) {
                        ((BaseSlidingFragmentActivity) requireActivity()).setTakePhotoAndCrop(false);
                        ((BaseSlidingFragmentActivity) requireActivity()).setActivityForResult(false);
                    }
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    File imageFile = new File(mApplication.getPref().getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                        String imagePath = imageFile.getAbsolutePath();
                        resizeImage(imagePath);
                        setAvatarEditProfile(imagePath);
                        if (typePhoto.equals(EnumUtils.VerifyImageTypeDef.FRONT_SIDE)) {
                            String type = ((ConfirmIdentityInfoActivity) requireActivity()).getTypeID();
                            String id = MappingUtils.getIDType(requireContext(), type);
                            if (id.equals("1") || id.equals("3")) {
                                detectOCRImage(imagePath);
                            }
                        }
                    }
                    if (requireActivity() instanceof BaseSlidingFragmentActivity) {
                        ((BaseSlidingFragmentActivity) requireActivity()).setTakePhotoAndCrop(false);
                        ((BaseSlidingFragmentActivity) requireActivity()).setActivityForResult(false);
                    }
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mApplication.getPref().getString(PREF_AVATAR_FILE_CROP, "");
                        setAvatarEditProfile(fileCrop);
                    }
                    break;
                default:
                    break;
            }
        } else {
            if (requireActivity() instanceof BaseSlidingFragmentActivity) {
                ((BaseSlidingFragmentActivity) requireActivity()).setTakePhotoAndCrop(false);
                ((BaseSlidingFragmentActivity) requireActivity()).setActivityForResult(false);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void resizeImage(String pathImage) {
        File file = new File(pathImage);
        if (file.exists()) {
            int fileSize1 = Integer.parseInt(String.valueOf(file.length() / 1024));
            file = ImageUtils.saveBitmapToFile(file);
            int fileSize = Integer.parseInt(String.valueOf(file.length() / 1024));
            android.util.Log.e("ttt", "onActivityResult: \n path: " + pathImage + "\n size1: " + fileSize1 + "\n size2: " + fileSize);
        }
    }

    public void setAvatarEditProfile(String filePath) {
        Log.i(TAG, "setAvatarEditProfile");
        drawPhoto(filePath);
    }

    private void drawPhoto(String filePath) {
        if (Objects.equals(typePhoto, EnumUtils.VerifyImageTypeDef.FRONT_SIDE)) {
            filePathFS = filePath;
            ImageUtils.loadImageView(requireContext(), binding.ivPhotoFS, filePath);
            binding.ivPhotoFS.setVisibility(View.VISIBLE);
            binding.ivTakePhotoFS.setVisibility(View.GONE);
            binding.clFrontSize.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.color_rlContainer));
        } else if (Objects.equals(typePhoto, EnumUtils.VerifyImageTypeDef.BACK_SIDE)) {
            ImageUtils.loadImageView(requireContext(), binding.ivPhotoBS, filePath);
            filePathBS = filePath;
            binding.ivPhotoBS.setVisibility(View.VISIBLE);
            binding.ivTakePhotoBS.setVisibility(View.GONE);
            binding.clBackSide.setBackgroundColor(Color.parseColor("#1F1F1F"));
        } else {
            ImageUtils.loadImageView(requireContext(), binding.ivPhotoSelfie, filePath);
            filePathYS = filePath;
            binding.ivPhotoSelfie.setVisibility(View.VISIBLE);
            binding.ivPhotoSelfie.setBackgroundColor(Color.parseColor("#1F1F1F"));
        }
    }

    public boolean checkPicture() {
        if (TextUtils.isEmpty(filePathFS)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_upload_front_side));
            return false;
        }
        if (TextUtils.isEmpty(filePathBS)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_upload_back_side));
            return false;
        }
        if (TextUtils.isEmpty(filePathYS)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_upload_portrait_photo));
            return false;
        }
        return true;
    }

    public VerifyInfo getData(String idType) {
        String imageEncodeFS = "";
        String imageEncodeBS = "";
        String imageEncodeYS = "";

        imageEncodeFS = ImageUtils.getFileToByte(filePathFS);
        imageEncodeBS = ImageUtils.getFileToByte(filePathBS);
        imageEncodeYS = ImageUtils.getFileToByte(filePathYS);
        //mAccountBusiness.removeFileExternalDir(requireContext());

        if (!TextUtils.isEmpty(imageEncodeFS) && !TextUtils.isEmpty(imageEncodeBS)
                && !TextUtils.isEmpty(imageEncodeYS)) {
            return new VerifyInfo(imageEncodeFS, imageEncodeBS, imageEncodeYS, idType);
        } else {
            return null;
        }
    }
}

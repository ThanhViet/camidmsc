package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.fragment.changenumber.InputNumberFragment;
import com.metfone.selfcare.fragment.changenumber.InputOTPFragment;
import com.metfone.selfcare.fragment.changenumber.IntroChangeNumberFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.JSONHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.Connection;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by thanhnt72 on 11/14/2018.
 */

public class ChangeNumberActivity extends BaseSlidingFragmentActivity {

    private static final String TAG = ChangeNumberActivity.class.getSimpleName();
    @BindView(R.id.btnContinue)
    TextView btnContinue;

    private ApplicationController mApp;
    private Resources mRes;
    private int currentFragment;
    private String mCurrentRegionCode;
    private String mCurrentNumberJid;
    private String mPassword;
    private int otpCode;
    private String oldNumber;
    private String oldRegionCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number);
        ButterKnife.bind(this);
        mApp = (ApplicationController) getApplication();
        mRes = getResources();
        setToolBar(findViewById(R.id.tool_bar));
        navigateToIntroFragment();
        setToolbar();
        oldNumber = String.valueOf(mApp.getReengAccountBusiness().getJidNumber());
        oldRegionCode = mApp.getReengAccountBusiness().getRegionCode();
    }

    public void setToolbar() {
        View abView = getToolBarView();
        setCustomViewToolBar(LayoutInflater.from(mApp).inflate(R.layout.ab_detail, null));
        ImageView mBtnBack = abView.findViewById(R.id.ab_back_btn);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        View more = abView.findViewById(R.id.ab_more_btn);
        more.setVisibility(View.GONE);
        EllipsisTextView textToolbar = abView.findViewById(R.id.ab_title);
        textToolbar.setText(mRes.getString(R.string.title_change_number));

    }

    IntroChangeNumberFragment introFragment;

    private void navigateToIntroFragment() {
        currentFragment = 0;
        introFragment = IntroChangeNumberFragment.newInstance();
        executeFragmentTransaction(introFragment, R.id.fragment_container, false, false);
    }

    InputNumberFragment inputNumberFragment;

    private void navigateToInputNumberFragment() {
        currentFragment = 1;
        inputNumberFragment = InputNumberFragment.newInstance();
        executeFragmentTransaction(inputNumberFragment, R.id.fragment_container, false, false);
    }

    InputOTPFragment inputOtpFragment;

    private void navigateToInputOTPFragment(int code, boolean changeFragment) {
        currentFragment = 2;
        otpCode = code;
        if (changeFragment) {
            inputOtpFragment = InputOTPFragment.newInstance();
            executeFragmentTransaction(inputOtpFragment, R.id.fragment_container, false, false);
        }
    }

    @OnClick(R.id.btnContinue)
    public void onViewClicked() {
        switch (currentFragment) {
            case 0:
                navigateToInputNumberFragment();
                break;

            case 1:
                showDialogConfirmChangeNumber();
                break;

            case 2:
                processChangeNumber();
                break;
        }
    }


    public void processChangeNumber() {
        if (inputOtpFragment != null && inputOtpFragment.isVisible()) {
            mPassword = inputOtpFragment.getPassword();
        }
        if (!isValidInputOtp()) {
//            showToast("Nhap sai");
            return;
        }
        if (mApp.getReengAccountBusiness().isProcessingChangeNumber()) return;
        mApp.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApp.getXmppManager().manualDisconnect();
        LoginByCodeAsyncTask loginByCodeAsyncTask = new LoginByCodeAsyncTask();
        loginByCodeAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private boolean isValidInputOtp() {
        return true;
    }

    public void generateNewAuthCodeByServer(final boolean changeFragment) {
        if (inputNumberFragment != null && inputNumberFragment.isVisible()) {
            mCurrentNumberJid = inputNumberFragment.getInputNumber();
            mCurrentRegionCode = inputNumberFragment.getRegionCode();
        }
        if (!isValidInputNumber()) {
            showError(getString(R.string.phone_number_invalid), mRes.getString(R.string.note_title));
            return;
        }

        if (!NetworkHelper.isConnectInternet(mApp)) {
            showError(mRes.getString(R.string.error_internet_disconnect), null);
        } else {
            String url;
            if (Config.Server.FREE_15_DAYS) {
                url = UrlConfigHelper.getInstance(this).getUrlConfigOfFile(Config.UrlEnum.GEN_OTP_FREE);
            } else {
                url = UrlConfigHelper.getInstance(this).getUrlConfigOfFile(Config.UrlEnum.GEN_OTP_INTERNATIONAL);
            }
            Log.i(TAG, "url: " + url);
            showLoadingDialog("", R.string.loading);
            StringRequest request = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i(TAG, "response: " + response);
                            hideLoadingDialog();
                            int mResponseCode = JSONHelper.getResponseFromJSON(response);
                            String msisdn = "";
                            try {
                                JSONObject jso = new JSONObject(response);
                                msisdn = jso.optString("msisdn");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // detecte and set subcription
                            // mLoginActivity.hideLoadingDialog();
                            if (mResponseCode == HTTPCode.E200_OK) {
                                navigateToInputOTPFragment(mResponseCode, changeFragment);
                            } else if (mResponseCode == 201) {//11 số + viễn thông đã hỗ trợ 10 số -> trả về code: 201, msisdn: (sdt 10 số)
                                if (!TextUtils.isEmpty(msisdn)) mCurrentNumberJid = msisdn;
                                navigateToInputOTPFragment(mResponseCode, changeFragment);
                            } else if (mResponseCode == 202) {//10 số + viễn thông chưa hỗ trợ 10 số -> trả về code: 202, msisdn: (sdt 11 số)
                                if (!TextUtils.isEmpty(msisdn)) mCurrentNumberJid = msisdn;
                                navigateToInputOTPFragment(mResponseCode, changeFragment);
                            } else if (mResponseCode == HTTPCode.E501_INTERNAL_SERVER_ERROR ||
                                    mResponseCode == HTTPCode.E_SERVER_ERROR ||
                                    mResponseCode == HTTPCode.E405_USER_INVAID ||
                                    mResponseCode == HTTPCode.E550_GENERATE_PASSWORD_OVER_LIMIT) {
                                // show error
                                showError(mRes.getString(R.string.e601_error_but_undefined), null);
                            } else if (mResponseCode == HTTPCode.E403_GENOTP_OUT_OF_WHITELIST) {
                                showError(JSONHelper.getResponseDescFromJSON(response), null);
                            } else {
                                showError(mRes.getString(R.string.e601_error_but_undefined), null);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "VolleyError", error);
                    hideLoadingDialog();
                    showError(mRes.getString(R.string.e601_error_but_undefined), null);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    String version = BuildConfig.VERSION_NAME;
                    HashMap<String, String> params = new HashMap<>();
                    params.put(Constants.HTTP.REST_USER_NAME, mCurrentNumberJid);
                    params.put(Constants.HTTP.REST_COUNTRY_CODE, mCurrentRegionCode);
                    params.put("platform", "Android");
                    params.put("os_version", Build.VERSION.RELEASE);
                    params.put("device", Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL);
                    params.put("revision", Config.REVISION);
                    params.put("version", version);
                    params.put("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
                    return params;
                }
            };
            VolleyHelper.getInstance(mApp).addRequestToQueue(request, TAG, false);
        }
    }

    private boolean isValidInputNumber() {

        PhoneNumberUtil phoneUtil = mApp.getPhoneUtil();
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(phoneUtil,
                        mCurrentNumberJid, mCurrentRegionCode);
        if (PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil, phoneNumberProtocol)) {
            mCurrentRegionCode = mApp.getPhoneUtil().getRegionCodeForNumber(phoneNumberProtocol);
            mCurrentNumberJid = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    phoneUtil.format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat.E164));
            if (mCurrentNumberJid.equals(mApp.getReengAccountBusiness().getJidNumber()))
                return false;
            return true;
        } else {
            return false;
        }
    }

    public String getCurrentRegionCode() {
        return mCurrentRegionCode;
    }

    public String getCurrentNumberJid() {
        return mCurrentNumberJid;
    }

    public int getOtpCode() {
        return otpCode;
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {

        public LoginByCodeAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoadingDialog(mRes.getString(R.string.title_change_number), mRes.getString(R.string.title_msg_change_number));
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            LoginBusiness loginBusiness = mApp.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(mApp, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, false, Connection.CHANGE_NUMBER_AUTH_NON_SASL,
                    mApp.getReengAccountBusiness().getJidNumber(), mApp.getReengAccountBusiness().getToken());
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            Log.i(TAG, "LoginByCodeAsyncTask changenumber responseCode: " + responseCode);
            try {
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    Log.i(TAG, "E200_OK: " + responseCode);
                    ReengAccount reengAccount = mApp.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApp.getReengAccountBusiness().updateReengAccount(reengAccount);
                    if (!oldRegionCode.equals(mCurrentRegionCode)) {
                        mApp.getConfigBusiness().init();
                        mApp.getReengAccountBusiness().clearContactData(mApp);
                        mApp.getContactBusiness().clearMemoryDataContact();
                        mApp.getConfigBusiness().deleteAllKeyConfig();
                        mApp.getReengAccountBusiness().checkAndSendIqGetLocation();
                        mApp.loadDataAfterLogin();
                        mApp.getMusicBusiness().resetStrangerList();
                    }
                    doAfterChangeNumber();
                } else {
                    hideLoadingDialog();
                    showToast(R.string.e601_error_but_undefined);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                showToast(R.string.e601_error_but_undefined);
                hideLoadingDialog();
            }
            mApp.getReengAccountBusiness().setProcessingChangeNumber(false);
        }
    }

    private void doAfterChangeNumber() {
        String content = String.format(mApp.getResources().getString(R.string.title_msg_change_number_from_to),
                oldNumber, mApp.getReengAccountBusiness().getJidNumber());
        showToast(content);
        mApp.getMessageBusiness().processChangeNumberAllThread(oldNumber, mApp.getReengAccountBusiness().getJidNumber(), new ChangeNumberMemberListener() {
            @Override
            public void onChangeNumberDone(boolean isSuccess) {
                hideLoadingDialog();
                retartApp();
            }
        });
    }

    private void retartApp() {
        Intent intent = new Intent(mApp, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("change_number_from", oldNumber);
        mApp.startActivity(intent);
        /*int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(mApp, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) mApp.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);*/
    }

    private void showDialogConfirmChangeNumber() {
        DialogConfirm dialogConfirm = new DialogConfirm(this, true);
        dialogConfirm.setLabel(mRes.getString(R.string.title_popup_change_number));
        dialogConfirm.setMessage(mRes.getString(R.string.msg_popup_change_number));
        dialogConfirm.setUseHtml(true);
        dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
        dialogConfirm.setPositiveLabel(mRes.getString(R.string.confirm));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                generateNewAuthCodeByServer(true);
            }
        });
        dialogConfirm.show();
    }

    public interface ChangeNumberMemberListener {
        void onChangeNumberDone(boolean isSuccess);
    }
}

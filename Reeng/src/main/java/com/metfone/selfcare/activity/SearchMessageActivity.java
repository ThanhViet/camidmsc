package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.fragment.message.SearchMessageDetailsFragment;
import com.metfone.selfcare.fragment.message.SearchMessageResultsFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.module.search.activity.SearchAllActivity;
import com.metfone.selfcare.ui.ReengSearchView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by huongnd38 on 17/10/2018.
 */

public class SearchMessageActivity extends BaseSlidingFragmentActivity implements SearchMessageResultsFragment.OnFragmentInteractionListener {
    public static final String TAG = SearchMessageActivity.class.getSimpleName();
    public static final String PARAM_THREAD_MESSAGE_ID = "thread_message_id";
    private SearchMessageResultsFragment mSearchMessageResultFragment;
    private SearchMessageDetailsFragment mSearchMessageDetailFragment;

    private int mThreadId;
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ThreadMessage mCurrentThreadMessage;
    private int mThreadType;
    private ArrayList<Integer> mAllMessageIds;
    private  EditText mEdtInput;
    private ImageView closeImg;
    private LinearLayout bgViewSearch;
    private TextView txtMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, " onCreate ... ");
        changeStatusBar(true);
        setContentView(R.layout.activity_detail);
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(getLayoutInflater().inflate(R.layout.header_search_message, null));
        mApplication = (ApplicationController) getApplication();
        mMessageBusiness = mApplication.getMessageBusiness();

        if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(PARAM_THREAD_MESSAGE_ID);
        } else {
            Intent intent = getIntent();
            if (intent == null) {
                finish();
                return;
            }
            mThreadId = intent.getIntExtra(PARAM_THREAD_MESSAGE_ID, -1);
        }
        mAllMessageIds = mMessageBusiness.loadAllMessagesByThreadId(mThreadId);
        mCurrentThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        if (mCurrentThreadMessage == null) {
            finish();
            return;
        }
        mThreadType = mCurrentThreadMessage.getThreadType();
        setUpToolbar();
        displaySearchMessageResultsFragment(mThreadId);
        trackingScreen(TAG);
        changeWhiteStatusBar();

    }

    private void setUpToolbar() {
        View view = getToolBarView();

        if (view != null) {
            mEdtInput = view.findViewById(R.id.ab_search_view);
            closeImg = view.findViewById(R.id.close_img);
            closeImg.setVisibility(View.GONE);
            TextView textView = view.findViewById(R.id.ab_title);
            bgViewSearch = view.findViewById(R.id.bg_view_search);
            txtMessage = view.findViewById(R.id.tv_message);
            if (textView != null && mCurrentThreadMessage != null) {
                textView.setText(mCurrentThreadMessage.getThreadName());
            }

            if (closeImg != null) {
                closeImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEdtInput.setText(null);
                    }
                });
            }

            ImageView backBtn = view.findViewById(R.id.ab_back_btn);
            if (backBtn != null && mCurrentThreadMessage != null) {
                backBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
            if (mEdtInput == null) return;
            mEdtInput.postDelayed(() -> {
                InputMethodUtils.showSoftKeyboard(SearchMessageActivity.this, mEdtInput);
            }, 500);
            mEdtInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        InputMethodUtils.hideSoftKeyboard(mEdtInput, getBaseContext());

                    }
                }
            });
            mEdtInput.addTextChangedListener(new TextWatcher() {
                @Override
                public synchronized void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (mEdtInput == null) return;
                    String trimmedStr = mEdtInput.getText().toString().trim();
                    if (!trimmedStr.isEmpty()) {
                        closeImg.setVisibility(View.VISIBLE);
                    } else {
                        closeImg.setVisibility(View.GONE);
                    }

                    if (mSearchMessageResultFragment != null) {
                        mSearchMessageResultFragment.onDialogResult(trimmedStr);
                    }

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (mSearchMessageDetailFragment != null) {
            removeFragment(mSearchMessageDetailFragment);
            mSearchMessageDetailFragment = null;
            showSearchButton(true);
        } else {
            Intent intent = new Intent();
            intent.setAction(Constants.CHOOSE_CONTACT.RESULT_SEARCH_MESSAGER);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    void showSearchButton(boolean visibility) {
        View view = getToolBarView();
        if (view != null) {
            if (visibility) {
                mEdtInput.setVisibility(View.VISIBLE);
                closeImg.setVisibility(View.VISIBLE);
                bgViewSearch.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_search_box_transparent));
                txtMessage.setVisibility(View.GONE);
            } else {
                mEdtInput.setVisibility(View.GONE);
                closeImg.setVisibility(View.GONE);
                bgViewSearch.setBackgroundColor(getResources().getColor(R.color.transparent));
                txtMessage.setVisibility(View.VISIBLE);
                bgViewSearch.setPadding(10, 0, 0 , 0);
            }

        }
    }
    private void displaySearchMessageDetailFragment(String searchTag, ReengMessage message) {
        if (message.getDirection().equals("send")) {
            txtMessage.setText(message.getReceiver());
        } else {
            txtMessage.setText(message.getSender());
        }
        showSearchButton(false);
        mSearchMessageDetailFragment = SearchMessageDetailsFragment.newInstance(mThreadId, mCurrentThreadMessage.getThreadType(), searchTag, message, mAllMessageIds);
        executeAddFragmentTransaction(mSearchMessageDetailFragment, R.id.fragment_container, false, false);
    }

    private void displaySearchMessageResultsFragment(int threadId) {
        showSearchButton(true);
        mSearchMessageResultFragment = SearchMessageResultsFragment.newInstance(threadId, mThreadType, this, mAllMessageIds);
        executeAddFragmentTransaction(mSearchMessageResultFragment, R.id.fragment_container, false, false);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(PARAM_THREAD_MESSAGE_ID, mThreadId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mSearchMessageDetailFragment = null;
        mSearchMessageResultFragment = null;
        if (mAllMessageIds != null) {
            mAllMessageIds.clear();
        }
        super.onDestroy();
        Log.d(TAG, "on Destroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void navigateToNonContactDetailActiviy(String jidNumber) {
        NonContact existNonContact = mApplication.getContactBusiness().getExistNonContact(jidNumber);
        if (existNonContact == null) {
            PhoneNumber phone = new PhoneNumber(jidNumber, jidNumber);
            mApplication.getContactBusiness().insertNonContact(phone, false);
        }
        Intent contactDetail = new Intent(mApplication, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
        bundle.putString(NumberConstant.NUMBER, jidNumber);
        contactDetail.putExtras(bundle);
        startActivity(contactDetail, true);
    }

    @Override
    public void navigateToOfficialDetail(String officialId) {
        OfficerAccount official = mApplication.getOfficerBusiness().getOfficerAccountByServerId(officialId);
        if (official == null) {
            Log.d(TAG, "official==null");
            return;
        }
        Intent strangerDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_OFFICIAL_ONMEDIA);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_ID, officialId);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_NAME, official.getName());
        bundle.putString(Constants.ONMEDIA.OFFICIAL_AVATAR, official.getAvatarUrl());
        bundle.putInt(Constants.ONMEDIA.OFFICIAL_USER_TYPE, UserInfo.USER_ONMEDIA_OFFICAL);
        strangerDetail.putExtras(bundle);
        startActivity(strangerDetail, true);
    }

    @Override
    public void addNewContact(String number, String name) {
        mApplication.getContactBusiness().navigateToAddContact(this, number, name);
    }

    @Override
    public void navigateToSendEmail(String email) {
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.setType("message/rfc822");
        sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        startActivity(sendEmailIntent);
    }

    @Override
    public void navigateToStatusMessageFragment(int threadId, int messageId, String abStatus) {
        Intent intentDetail = new Intent(getApplicationContext(), MessageDetailActivity.class);
        intentDetail.putExtra(Constants.MESSAGE.AB_DESC, abStatus);
        intentDetail.putExtra(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        intentDetail.putExtra(ReengMessageConstant.MESSAGE_ID, messageId);
        startActivity(intentDetail, true);
    }

    @Override
    public void navigateToChooseFriendsActivity(ArrayList<String> listNumberString, ThreadMessage threadMessage) {
        Intent intent = new Intent(getApplicationContext(), ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listNumberString);
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP);
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadMessage.getId());
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP, true);
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST);
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadMessage.getId());
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST, true);
        } else {
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP);
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, -1);
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP, true);
        }
    }

    @Override
    public void navigateToCall(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        }
    }

    @Override
    public void navigateToViewLocation(ReengMessage message) {
        /*Intent i = new Intent(getApplicationContext(), ShareLocationActivity.class);
        i.putExtra(Constants.LOCATION.DATA_INPUT_TYPE, Constants.LOCATION.TYPE_RECEIVED);
        i.putExtra(Constants.LOCATION.DATA_ADDRESS, message.getContent());
        i.putExtra(Constants.LOCATION.DATA_LATITUDE, message.getFilePath());
        i.putExtra(Constants.LOCATION.DATA_LONGITUDE, message.getImageUrl());
        startActivityForResult(i, Constants.ACTION.SHARE_LOCATION, true);*/
    }

    @Override
    public void navigateToPollActivity(ThreadMessage thread, ReengMessage reengMessage, String pollId, int type) {
        Intent intent = new Intent(getApplicationContext(), PollMessageActivity.class);
        intent.putExtra(ReengMessageConstant.MESSAGE_THREAD_ID, thread.getId());
        intent.putExtra(Constants.MESSAGE.TYPE, type);
        if (type == Constants.MESSAGE.TYPE_DETAIL_POLL) {
            intent.putExtra(Constants.MESSAGE.POLL_ID, pollId);
        }
        startActivity(intent, true);
    }

    @Override
    public void navigateToSearchDetailsFragment(String searchTag, ReengMessage message) {
        displaySearchMessageDetailFragment(searchTag, message);
    }

    @Override
    public void navigateToThreadDetail(ThreadMessage threadMessage) {
        //
    }

    @Override
    public void navigateToContactDetailActivity(String phoneId) {
        Intent contactDetail = new Intent(mApplication, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
        bundle.putString(NumberConstant.ID, phoneId);
        contactDetail.putExtras(bundle);
        startActivity(contactDetail, true);
    }

    @Override
    public void navigateToStrangerDetail(StrangerPhoneNumber strangerPhoneNumber,
                                         String friendJid, String friendName) {
        Intent strangerDetail = new Intent(mApplication, ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        if (strangerPhoneNumber != null) {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
            bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
        } else {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
            bundle.putString(NumberConstant.NAME, friendName);
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, "1");
        }
        strangerDetail.putExtras(bundle);
        startActivity(strangerDetail, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT) {
            data.putExtra(Constants.SEARCH_MESSAGE.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT);
            setResult(RESULT_OK, data);
            finish();
        }
    }
}
package com.metfone.selfcare.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.database.model.GiftLixiModel;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ReengMessageWrapper;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.fragment.message.ThreadDetailFragment;
import com.metfone.selfcare.fragment.message.ThreadDetailInboxFragment;
import com.metfone.selfcare.fragment.message.ThreadDetailSettingFragment;
import com.metfone.selfcare.helper.BackStackHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.Version;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.helper.video.Video;
import com.metfone.selfcare.listeners.DownloadDriveResponse;
import com.metfone.selfcare.listeners.FilePickerListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.LockRoomListener;
import com.metfone.selfcare.listeners.ThreadDetailSettingListener;
import com.metfone.selfcare.network.fileTransfer.DownloadFileDriveAsyncTask;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.choosefile.FileChooserDialog;
import com.metfone.selfcare.ui.dialog.PermissionDialog;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

public class ChatActivity extends BaseSlidingFragmentActivity implements
    ThreadDetailFragment.OnFragmentInteractionListener,
    ThreadDetailSettingFragment.OnFragmentInteractionListener,
    ThreadDetailInboxFragment.OnFragmentInteractionListener,
    InitDataListener,
    LockRoomListener,
    DrawerLayout.DrawerListener {
    public static final String BUNDLE_BACK_NORMAL = "BUNDLE_BACK_NORMAL";
    public static final String NOT_SHOW_PIN = "not_show_pin";
    private static final String TAG = ChatActivity.class.getSimpleName();
    public static ReengMessage pollReengMessageFocus;
    int mThreadType, mThreadId;
    ApplicationController mApplication;
    MessageBusiness mMessageBusiness;
    ThreadMessage mCurrentThreadMessage;
    boolean isSearchMessageActivity = false;
    private FilePickerListener filePicker;
    private ThreadDetailSettingListener detailSettingCallBack;
    private Handler mHandler;
    private ProgressLoading mProgressLoading;
    private View mViewCustomActionBar;
    private String mCurrentVideoPath;
    private boolean mGsmMode = false, mIsReengUser;
    private ThreadDetailInboxFragment mThreadDetailInboxFragment;
    private ThreadDetailSettingFragment threadDetailSettingFragment;
    private ReengAccountBusiness mReengAccountBusiness;
    private ReengAccount currentAccount;
    private ThreadDetailFragment mThreadDetailFragment;
    private ContactBusiness mContactBusiness;
    private String mNumber = null;// so dien thoai notify nguoi dung moi
    private String mNumberOtherApp = null;
    private ReengMessageWrapper arrayMessage;
    private ReengMessage messageShare;
    private SharedPreferences mPref;
    private FrameLayout mLeftSlideMenu, mRightSlideMenu;
    //
    private DrawerLayout mDrawerLayout;
    private boolean isDrawerOpening = false, isForceOpen = false, isDrawerOpened = false;
    private boolean isNormalBack;
    private RequestPermissionResult permissionResult;
    private DownloadFileDriveAsyncTask downloadFileDriveAsyncTask;
    private boolean notShowPin = false;
    private int srcScreenId;

    public void setFilePickerListener(FilePickerListener f) {
        Log.i(TAG, "mThreadId setFilePickerListener " + f.hashCode());
        filePicker = f;
    }

    public void setDetailSettingCallBack(ThreadDetailSettingListener f) {
        detailSettingCallBack = f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        long beginTime = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        getBusiness();
        mApplication.getMessageBusiness().addLockRoomListener(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_chat_detail);
        changeStatusBar(true);
        changeWhiteStatusBar();
        Log.d(TAG, "--------------------------[perform] -  set contentView take " + (System.currentTimeMillis() - beginTime) + " ms");
        beginTime = System.currentTimeMillis();
        mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        findComponentViews();
        setActionbarDrawer();
        Log.d(TAG, "--------------------------[perform] -  findComponent take " + (System.currentTimeMillis() - beginTime) + " ms");
        beginTime = System.currentTimeMillis();
        getDataAndDisplayFragment(savedInstanceState, null);// take 1ms
        Log.d(TAG, "--------------------------[perform] - getDataAndDisplayFragment take " + (System.currentTimeMillis() - beginTime) + " ms");
        trackingScreen(TAG);
    }

    private void getBusiness() {
        mApplication = (ApplicationController) getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mReengAccountBusiness = mApplication.getReengAccountBusiness();
        mContactBusiness = mApplication.getContactBusiness();
    }

    @Override
    public void onStart() {
        mHandler = new Handler();
        mApplication.getMessageBusiness().addLockRoomListener(this);
        super.onStart();
    }

    @Override
    public void onResume() {
        mApplication.getMessageBusiness().addLockRoomListener(this);
        if (mHandler == null) {
            mHandler = new Handler();
        }
        ListenerHelper.getInstance().addInitDataListener(this);
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initLayoutParamSlideMenu();
    }

    @Override
    protected void onPause() {
        mApplication.getMessageBusiness().removeLockRoomListener();
        Log.i(TAG, "onPause");
        ListenerHelper.getInstance().removeInitDataListener(this);
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        mApplication.getMessageBusiness().removeLockRoomListener();
        super.onStop();
        checkLeftSlideMenuToClose();
        checkRightSlideMenuToClose();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy ");
        mDrawerLayout.removeDrawerListener(this);
        super.onDestroy();
        mApplication.getMessageBusiness().removeLockRoomListener();
        if (downloadFileDriveAsyncTask != null) {
            downloadFileDriveAsyncTask.cancel(true);
            downloadFileDriveAsyncTask = null;
        }
        // TODO comment when commit ibm
        System.gc();
        System.runFinalization();
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "onBackPressed");
        if (isSearchMessageActivity) {
            super.onBackPressed();
            return;
        }
        if (mThreadDetailFragment == null) {
            Log.d(TAG, "onBackPressed -> MessageDetailFragment == null");
            finish();
        } else if (mThreadDetailFragment.isCustomKeyboardOpened()) {
            Log.i(TAG, "onBackPressed hideCusKeyboard");
            mThreadDetailFragment.hideCusKeyboard();
        } else {
            if (mThreadDetailFragment.isFullScreen()) {
                mThreadDetailFragment.toggleScreen(true);
            } else {
                if (!mThreadDetailFragment.showDialogConfirmLeave()) {
                    goPreviousOrHomeWhenBackPress();
                }
            }
        }
    }

    public void goPreviousOrHomeWhenBackPress() {
        if (BackStackHelper.getInstance().checkIsLastInStack(getApplication(), ChatActivity.class.getName())) {
            goToHome();
        } else {
            if (!BackStackHelper.getInstance().isBackToHome(getApplication(), ChatActivity.class.getName())) {
                finish();
            } else if (BackStackHelper.getInstance().isBackToChooseContact(getApplication(), ChooseContactActivity
                .class.getName())) {
                goToHome();
            } else {
                finish();
            }
        }
        if (mCurrentThreadMessage != null && mCurrentThreadMessage.isHasNewMessage()) {
            mMessageBusiness.markAllMessageIsOld(mCurrentThreadMessage, mCurrentThreadMessage.getId());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ThreadMessageConstant.THREAD_ID, mThreadId);
        outState.putInt(ThreadMessageConstant.THREAD_IS_GROUP, mThreadType);
        outState.putString(Constants.MESSAGE.NUMBER, mNumber);
        outState.putBoolean(BUNDLE_BACK_NORMAL, isNormalBack);
        if (!TextUtils.isEmpty(mNumberOtherApp)) {
            outState.putString(Constants.MESSAGE.NUMBER_FROM_OTHER_APP, mNumberOtherApp);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i(TAG, "onNewIntent " + isDrawerOpened + " thread: " + mCurrentThreadMessage);
        Bundle bundle = intent.getExtras();
        getDataAndDisplayFragment(null, bundle);
        if (isDrawerOpened) {
            checkLeftSlideMenuToClose();
            checkRightSlideMenuToClose();
        }
        if (mCurrentThreadMessage != null)
            changeDataThreadSettingFragment(mCurrentThreadMessage);
    }

    @Override
    public void onDataReady() {
        if (isSearchMessageActivity) return;
        if (mHandler == null) {// activity stop roi ma moi vao ready
            return;
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mThreadDetailFragment == null) {// fragment da duoc attach. ko lam gi
                    // tat progress loading, load theread
                    findOrCreateAndDisplayFragment();
                }
            }
        });
    }

    @Override
    public void navigateToChatDetailActivity(ThreadMessage threadMessage) {
        int delay = 50;
        if (threadMessage.getId() != mThreadId) {
            mCurrentThreadMessage = threadMessage;
            mThreadId = mCurrentThreadMessage.getId();
            mThreadType = mCurrentThreadMessage.getThreadType();
            if (findViewById(R.id.fragment_container) != null) {
                displayMessageDetailFragment(mCurrentThreadMessage);
                changeDataThreadSettingFragment(mCurrentThreadMessage);
                delay = 650;
            }
        }
        if (mHandler == null) {
            mHandler = new Handler();
        }
        if (isSearchMessageActivity) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkLeftSlideMenuToClose();
            }
        }, delay);
    }

    @Override
    public void navigateToThreadDetail(ThreadMessage threadMessage) {
        mCurrentThreadMessage = threadMessage;
        mThreadId = mCurrentThreadMessage.getId();
        mThreadType = mCurrentThreadMessage.getThreadType();
        Log.i(TAG, "replace fragment " + mCurrentThreadMessage);
        if (findViewById(R.id.fragment_container) != null) {
            displayMessageDetailFragment(mCurrentThreadMessage);
            changeDataThreadSettingFragment(mCurrentThreadMessage);
        }
    }

    @Override
    public void navigateToContactDetailActivity(String phoneId) {
        Intent contactDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
        bundle.putString(NumberConstant.ID, phoneId);
        contactDetail.putExtras(bundle);
        startActivity(contactDetail, true);
    }

    @Override
    public void addNewContact(String number, String name) {
        mApplication.getContactBusiness().navigateToAddContact(ChatActivity.this, number, name);
    }

    @Override
    public void navigateToStrangerDetail(StrangerPhoneNumber strangerPhoneNumber,
                                         String friendJid, String friendName) {
        Intent strangerDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        if (strangerPhoneNumber != null) {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
            bundle.putString(NumberConstant.NAME, strangerPhoneNumber.getFriendName());
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, strangerPhoneNumber.getPhoneNumber());
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, strangerPhoneNumber.getFriendAvatarUrl());
        } else {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_MOCHA);
            bundle.putString(NumberConstant.NAME, friendName);
            bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, friendJid);
            bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, "1");
        }
        strangerDetail.putExtras(bundle);
        startActivity(strangerDetail, true);
    }

    @Override
    public void navigateToNonContactDetailActiviy(String jidNumber) {
        NonContact existNonContact = mContactBusiness.getExistNonContact(jidNumber);
        if (existNonContact == null) {
            PhoneNumber phone = new PhoneNumber(jidNumber, jidNumber);
            mContactBusiness.insertNonContact(phone, false);
        }
        Intent contactDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
        bundle.putString(NumberConstant.NUMBER, jidNumber);
        contactDetail.putExtras(bundle);
        startActivity(contactDetail, true);
    }

    @Override
    public void navigateToOfficialDetail(String officialId) {
        OfficerAccount official = mApplication.getOfficerBusiness().getOfficerAccountByServerId(officialId);
        if (official == null) {
            Log.d(TAG, "official==null");
            return;
        }
        Intent strangerDetail = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_OFFICIAL_ONMEDIA);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_ID, officialId);
        bundle.putString(Constants.ONMEDIA.OFFICIAL_NAME, official.getName());
        bundle.putString(Constants.ONMEDIA.OFFICIAL_AVATAR, official.getAvatarUrl());
        bundle.putInt(Constants.ONMEDIA.OFFICIAL_USER_TYPE, UserInfo.USER_ONMEDIA_OFFICAL);
        strangerDetail.putExtras(bundle);
        startActivity(strangerDetail, true);
    }

    @Override
    public void navigateToChooseFriendsActivity(ArrayList<String> listNumberString, ThreadMessage threadMessage, int typeScreen) {
        Intent intent = new Intent(getApplicationContext(), ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listNumberString);
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP);
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadMessage.getId());
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP, true);
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST);
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, threadMessage.getId());
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST, true);
        } else {
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP);
            if (typeScreen == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT_SETTTING) {
                bundle.putInt(Constants.CHOOSE_CONTACT.TYPE_SCREEN, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP_SETTING);
            }
            bundle.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, -1);
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP, true);
        }
    }

    @Override
    public void navigateToSendEmail(String email) {
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.setType("message/rfc822");
        sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        startActivity(sendEmailIntent);
    }

    @Override
    public void navigateToStatusMessageFragment(int threadId, int messageId, String abStatus) {
        // ban phim dang mo thi dong ban phim
        if (!isSearchMessageActivity && mThreadDetailFragment != null && mThreadDetailFragment.isCustomKeyboardOpened()) {
            mThreadDetailFragment.hideCusKeyboard();
        }

        Intent intentDetail = new Intent(getApplicationContext(), MessageDetailActivity.class);
        intentDetail.putExtra(Constants.MESSAGE.AB_DESC, abStatus);
        intentDetail.putExtra(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        intentDetail.putExtra(ReengMessageConstant.MESSAGE_ID, messageId);
        startActivity(intentDetail, true);
    }

    @Override
    public void navigateToCall(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + number));
            startActivity(callIntent);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        }
    }

    @Override
    public void navigateToViewLocation(ReengMessage message) {
        Intent i = new Intent(getApplicationContext(), ShareLocationActivity.class);
        i.putExtra(Constants.LOCATION.DATA_INPUT_TYPE, Constants.LOCATION.TYPE_RECEIVED);
        i.putExtra(Constants.LOCATION.DATA_ADDRESS, message.getContent());
        i.putExtra(Constants.LOCATION.DATA_LATITUDE, message.getFilePath());
        i.putExtra(Constants.LOCATION.DATA_LONGITUDE, message.getImageUrl());
        startActivityForResult(i, Constants.ACTION.SHARE_LOCATION, true);
    }

    @Override
    public void takePhotoChangeGroupAvatar() {
        dispatchTakePhotoIntent(Constants.ACTION.ACTION_TAKE_PHOTO_GROUP_AVATAR);
    }

    @Override
    public void openGalleryChangeGroupAvatar() {
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE_GROUP_AVATAR);
    }

    @Override
    public void navigateToInvite(String roomId) {
        Intent intent = new Intent(getApplicationContext(), ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM);
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, new ArrayList<String>());
        bundle.putString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID, mCurrentThreadMessage.getServerId());
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM, true);
    }

    @Override
    public void navigateToPollActivity(ThreadMessage thread, ReengMessage reengMessage, String pollId, int type, boolean pollUpdate) {
        pollReengMessageFocus = reengMessage;
        Intent intent = new Intent(getApplicationContext(), PollMessageActivity.class);
        intent.putExtra(ReengMessageConstant.MESSAGE_THREAD_ID, thread.getId());
        intent.putExtra(Constants.MESSAGE.TYPE, type);
        if (type == Constants.MESSAGE.TYPE_DETAIL_POLL) {
            intent.putExtra(Constants.MESSAGE.POLL_ID, pollId);
            intent.putExtra(Constants.MESSAGE.POLL_UPDATE, pollUpdate);
        }
        startActivity(intent, true);
    }

    @Override
    public void navigateToChooseDocument(ThreadMessage threadMessage) {
        Intent chooseDocument = new Intent(ChatActivity.this, ChooseDocumentActivity.class);
        chooseDocument.putExtra(Constants.DOCUMENT.GROUP_ID, threadMessage.getServerId());
        startActivityForResult(chooseDocument, Constants.ACTION.CHOOSE_DOCUMENT_CLASS, true);
    }

    @Override
    public void notifyReloadMessage(ThreadMessage threadMessage) {
        if (isSearchMessageActivity) return;
        if (mThreadDetailFragment != null) {
            mThreadDetailFragment.setDataAndDraw(false);
        }
    }

    @Override
    public void notifyGetListMemberThread(ArrayList<PhoneNumber> listPhone) {
        if (isSearchMessageActivity) return;
        if (mThreadDetailFragment != null) {
            mThreadDetailFragment.setListMemberTag();
        }
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState, Bundle newIntent) {
        // truong hop da vao man hinh chat, mem bi huy thi savedInstanceState!=null
        // luc do khong replace fragment lai, qua trinh cho dataUri load lai thuc hien o fragment
        Bundle bundle = getIntent().getExtras();
        Uri dataUri = getIntent().getData();
        String app_id = "";
        srcScreenId = 0;
        if (!TextUtils.isEmpty(app_id)) {
            mNumber = dataUri.getQueryParameter("to");
            mThreadType = ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;
        } else if (newIntent != null) {
            mThreadId = newIntent.getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = newIntent.getInt(ThreadMessageConstant.THREAD_IS_GROUP);
            mNumber = newIntent.getString(Constants.MESSAGE.NUMBER);
            mNumberOtherApp = newIntent.getString(Constants.MESSAGE.NUMBER_FROM_OTHER_APP);
            isNormalBack = newIntent.getBoolean(BUNDLE_BACK_NORMAL);
            notShowPin = newIntent.getBoolean(NOT_SHOW_PIN);
            srcScreenId = newIntent.getInt(Constants.SRC_SCREEN_ID);
            arrayMessage = null;
        } else if (bundle != null) {
            mThreadId = bundle.getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = bundle.getInt(ThreadMessageConstant.THREAD_IS_GROUP);
            mNumber = bundle.getString(Constants.MESSAGE.NUMBER);
            mNumberOtherApp = bundle.getString(Constants.MESSAGE.NUMBER_FROM_OTHER_APP);
            arrayMessage = (ReengMessageWrapper) bundle.getSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE);
            isNormalBack = bundle.getBoolean(BUNDLE_BACK_NORMAL);
            notShowPin = bundle.getBoolean(NOT_SHOW_PIN);
            srcScreenId = bundle.getInt(Constants.SRC_SCREEN_ID);
        } else if (savedInstanceState != null) {
            mThreadId = savedInstanceState.getInt(ThreadMessageConstant.THREAD_ID);
            mThreadType = savedInstanceState.getInt(ThreadMessageConstant.THREAD_IS_GROUP);
            mNumber = savedInstanceState.getString(Constants.MESSAGE.NUMBER);
            mNumberOtherApp = savedInstanceState.getString(Constants.MESSAGE.NUMBER_FROM_OTHER_APP);
            isNormalBack = savedInstanceState.getBoolean(BUNDLE_BACK_NORMAL);
            notShowPin = savedInstanceState.getBoolean(NOT_SHOW_PIN);
            srcScreenId = savedInstanceState.getInt(Constants.SRC_SCREEN_ID);
            arrayMessage = null;
        }
        // chua load xong du lieu
        if (!mApplication.isDataReady()) {
            mProgressLoading.setVisibility(View.VISIBLE);
            mProgressLoading.setEnabled(true);
            // remove fragment da attach cu
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            removeFragment(fragment);
            Fragment fragmentRight = getSupportFragmentManager().findFragmentById(R.id.activity_slide_menu_right_frame);
            removeFragment(fragmentRight);
            // remove left menu
            Fragment fragmentLeft = getSupportFragmentManager().findFragmentById(R.id.activity_slide_menu_left_frame);
            removeFragment(fragmentLeft);
        } else {// load xong du lieu
            findOrCreateAndDisplayFragment();
        }
    }

    private void findOrCreateAndDisplayFragment() {
        // tat progress loading, load thread
        mProgressLoading.setVisibility(View.GONE);
        mProgressLoading.setEnabled(false);
        currentAccount = mReengAccountBusiness.getCurrentAccount();
        if (mThreadId == -1 && mNumber != null) {
            //mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumber,false);
            mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumber);
            mThreadId = mCurrentThreadMessage.getId();
        } else if (mThreadId == -1 && mNumberOtherApp != null) {
            ReengAccount account = mReengAccountBusiness.getCurrentAccount();
            Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(mApplication.getPhoneUtil(), mNumberOtherApp, account.getRegionCode());
            if (phoneNumberProtocol != null) {
                // lay number jid
                mNumberOtherApp = PhoneNumberHelper.getInstant().getNumberJidFromNumberE164(
                    mApplication.getPhoneUtil().format(phoneNumberProtocol, PhoneNumberUtil.PhoneNumberFormat
                        .E164));
                if (!PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.getPhoneUtil(),
                    phoneNumberProtocol)) {
                    goToHome();// truyen vao khong phai so dien thoai
                } else {
                    if (PhoneNumberHelper.getInstant().isViettelNumber(mNumberOtherApp)) {
                        //mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp,false);
                        mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp);
                        mThreadId = mCurrentThreadMessage.getId();
                    } else {
                        PhoneNumber number = mContactBusiness.getPhoneNumberFromNumber(mNumberOtherApp);
                        if (number != null && number.isReeng()) {
                            //mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp,
                            // false);
                            mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp);
                            mThreadId = mCurrentThreadMessage.getId();
                        } else {
                            goToHome();// truyen vao so dien thoai khong co trong danh ba va ko phai so viettel
                        }
                    }

                    /*PhoneNumber number = mContactBusiness.getPhoneNumberFromNumber(mNumberOtherApp);
                    NonContact nonContact = mContactBusiness.getExistNonContact(mNumberOtherApp);
                    if (number != null && (number.isReeng() || number.isViettel())) {
                        mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp);
                        mThreadId = mCurrentThreadMessage.getId();
                    } else if (nonContact != null && (nonContact.isReeng() || nonContact.isViettel())) {
                        mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp);
                        mThreadId = mCurrentThreadMessage.getId();
                    } else {
                        showLoadingDialog("", R.string.loadding_data);
                        Log.f(TAG, "Lấy thông tin sdt từ sv: " + mNumberOtherApp);
                        ArrayList<String> listNumber = new ArrayList<>();
                        listNumber.add(mNumberOtherApp);
                        ContactRequestHelper.getInstance(mApplication).getInfoContactsFromNumbers(listNumber, new ContactRequestHelper.onResponseInfoContactsListener() {
                            @Override
                            public void onResponse(ArrayList<PhoneNumber> responses) {
                                hideLoadingDialog();
                                if (responses != null && !responses.isEmpty()) {
                                    PhoneNumber p = responses.get(0);
                                    if (p.isViettel()) {
                                        mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(mNumberOtherApp);
                                        mThreadId = mCurrentThreadMessage.getId();
                                    } else
                                        goToHome();
                                } else
                                    goToHome();
                            }

                            @Override
                            public void onError(int errorCode) {
                                hideLoadingDialog();
                                goToHome();
                            }
                        });
                    }*/
                }
            } else {
                goToHome();
            }
        } else if (mThreadId != -1) {
            mCurrentThreadMessage = mMessageBusiness.getThreadById(mThreadId);
        } else {
            goToHome();// truyen vao so dien thoai khong co trong danh ba va ko phai so viettel
        }
        if (mCurrentThreadMessage != null) {
            goToThreadDetail();
        }
    }

    private void goToThreadDetail() {
        if (arrayMessage != null) {
            displayMessageDetailAndSendMessage(mCurrentThreadMessage, arrayMessage);
        } else {
            displayMessageDetailFragment(mCurrentThreadMessage);
        }
    }

    private void setChatModeForFirstTime() {
        //neu ca 2 la so viettel thi moi enable button switch
        //neu so viettel-luu danh ba roi va chua dung reeng thi mac dinh la sms
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) { //chat 1-1
            String friendPhoneNumber = mCurrentThreadMessage.getSoloNumber();
            PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(friendPhoneNumber);
            if (mReengAccountBusiness.isViettel()) {
                String operatorFriend = mMessageBusiness.getOperatorFriendByJid(friendPhoneNumber);
                Log.f(TAG, "operatorFiend: " + operatorFriend);
                if (PhoneNumberHelper.isViettel(operatorFriend)) {
                    if (phoneNumber == null) {
                        //ko co trong danh ba
                        mGsmMode = false;
                    } else {
                        //co trong danh ba
                        mGsmMode = !phoneNumber.isReeng();
                    }
                }
            }
            //---------------------------------kiem tra so nay co dung reeng ko
            if (phoneNumber == null) {//ko co trong danh ba thi danh dau la ko dung reeng
                mIsReengUser = false;
            } else {
                mIsReengUser = phoneNumber.isReeng();
            }
        } else { //group
            mGsmMode = false;
            mIsReengUser = false;
        }
        setSwitchGsmButtonForFirstTime();
    }

    private void setSwitchGsmButtonForFirstTime() {
        if (isSearchMessageActivity) return;
        if (mThreadDetailFragment != null) {
            mThreadDetailFragment.onGsmModeChangeListener(mGsmMode, mIsReengUser);
        }
    }

    private void displayMessageDetailFragment(ThreadMessage threadMessage) {
        displayMessageDetailFragment(threadMessage, null, null, Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD);
    }

    private void displayMessageDetailAndSendMessage(ThreadMessage threadMessage, ReengMessageWrapper arrayMessage) {
        displayMessageDetailFragment(threadMessage, null, arrayMessage, Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD);
    }

    //neu msgForward null thi la sharecontact, nguoc lai
    private void displayMessageDetailFragment(ThreadMessage threadMessage, ReengMessage messageForward,
                                              ReengMessageWrapper arrayMessage, int code) {
        // cho load xong du lieu o fragment threadDetail
        // chac khong nhay vao nua dau
        if (isSearchMessageActivity) return;

        if (!mApplication.isDataReady() || threadMessage == null) {
            return;
        }

        if (Config.Features.FLAG_SUPPORT_HIDE_THREAD && mCurrentThreadMessage.getHiddenThread() == 1 && !notShowPin) {
            this.arrayMessage = arrayMessage;
            this.messageShare = messageForward;
            this.mCurrentThreadMessage = threadMessage;
            SettingActivity.startActivityForResultOpenHidden(this, threadMessage, code);
        } else {
            displayMessageDetailFragmentWithoutPin(threadMessage, messageForward, arrayMessage);
        }

    }

    private void displayMessageDetailFragmentWithoutPin(ThreadMessage threadMessage, ReengMessage messageForward,
                                                        ReengMessageWrapper arrayMessage) {
        long beginTime = System.currentTimeMillis();
        //mThreadName = mMessageBusiness.getThreadName(mCurrentThreadMessage);
        // Create an instance of newInstance
        mThreadId = threadMessage.getId();
        int threadType = threadMessage.getThreadType();
        Fragment oldFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        removeFragment(oldFragment);
        // ThreadDetailFragment fragment;
        if (arrayMessage != null) {
            mThreadDetailFragment = ThreadDetailFragment.newInstance(mThreadId, threadType, arrayMessage, srcScreenId);
        } else {
            mThreadDetailFragment = ThreadDetailFragment.newInstance(mThreadId, threadType, messageForward, srcScreenId);
        }
        executeFragmentTransaction(mThreadDetailFragment, R.id.fragment_container, false, false);
        setChatModeForFirstTime();
        //checkLeftSlideMenuToClose();
        Log.d(TAG, "Perform - displayMessageDetailFragment take " + (System.currentTimeMillis() - beginTime) + " ms");

    }

    private void changeDataThreadSettingFragment(ThreadMessage thread) {
        if (isSearchMessageActivity) return;
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_slide_menu_right_frame);
        if (fragment != null) {
            threadDetailSettingFragment = (ThreadDetailSettingFragment) fragment;
            threadDetailSettingFragment.changeThread(thread);
        }
    }

    private void findComponentViews() {
        mViewCustomActionBar = findViewById(R.id.tool_bar);
        setToolBar(mViewCustomActionBar);
        mProgressLoading = (ProgressLoading) findViewById(R.id.progress_loading);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mLeftSlideMenu = (FrameLayout) findViewById(R.id.activity_slide_menu_left_frame);
        mRightSlideMenu = (FrameLayout) findViewById(R.id.activity_slide_menu_right_frame);

        //mDrawerLayout.setScrimColor(Color.TRANSPARENT);
    }

    private void setActionbarDrawer() {
        initLayoutParamSlideMenu();
        mDrawerLayout.addDrawerListener(this);
    }

    @Override
    public void onDrawerSlide(View view, float slideOffset) {
        if (isSearchMessageActivity) return;
        if (!isDrawerOpening && !isForceOpen && !isDrawerOpened) {
            Log.i(TAG, "DrawerOpening");
            try {
                if (view.getId() == R.id.activity_slide_menu_right_frame) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id
                        .activity_slide_menu_right_frame);
                    if (fragment == null) {
                        Log.i(TAG, "replace fragment thread detail setting");
                        threadDetailSettingFragment = ThreadDetailSettingFragment.newInstance(mThreadId);
                        executeFragmentTransaction(threadDetailSettingFragment, R.id.activity_slide_menu_right_frame,
                            false, false);
                    } else {
                        threadDetailSettingFragment = (ThreadDetailSettingFragment) fragment;
                        threadDetailSettingFragment.setFragmentType();
                    }
                } else if (view.getId() == R.id.activity_slide_menu_left_frame) {
                    if (getSupportFragmentManager().findFragmentById(R.id.activity_slide_menu_left_frame) == null) {
                        Log.i(TAG, "replace fragment thread detail inbox");
//                        mThreadDetailInboxFragment = ThreadDetailInboxFragment.newInstance();
//                        executeFragmentTransaction(mThreadDetailInboxFragment, R.id.activity_slide_menu_left_frame,
//                                false, false);
                    }
                }
                isDrawerOpening = true;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                isDrawerOpening = false;
            }
        }
    }

    @Override
    public void onDrawerOpened(View view) {
        if (isSearchMessageActivity) return;

        isDrawerOpening = false;
        isDrawerOpened = true;
        isForceOpen = false;
        Log.i(TAG, "onDrawerOpened id = " + view.getId());
        if (view.getId() == R.id.activity_slide_menu_right_frame) {
            //disableLeftSlideMenu();
        } else if (view.getId() == R.id.activity_slide_menu_left_frame) {
            disableRightSlideMenu();
        }
        if (mThreadDetailFragment != null)
            mThreadDetailFragment.hideCusKeyboard();
    }

    @Override
    public void onDrawerClosed(View view) {
        if (isSearchMessageActivity) return;
        isDrawerOpening = false;
        isDrawerOpened = false;
        isForceOpen = false;
        Log.i(TAG, "onDrawerClosed id = " + view.getId());
        //neu cusKeyboard dang open thi disable tat ca
        //neu cusKeyboard dang close thi enable phia con lai
        if (mThreadDetailFragment != null && mThreadDetailFragment.isCustomKeyboardOpened()) {
            Log.i(TAG, "disable both side slide menu because cusKeyboard is opening");
            setEnableBothSideSlideMenu(false);
        } else if (view.getId() == R.id.activity_slide_menu_right_frame) {
            //enableLeftSlideMenu();
        } else if (view.getId() == R.id.activity_slide_menu_left_frame) {
            enableRightSlideMenu();
        }
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        if (isSearchMessageActivity) return;
      /*  if (newState == DrawerLayout.STATE_SETTLING && !isDrawerOpening) {
            isDrawerOpening = true;
        }*/
    }

    private void enableRightSlideMenu() {
        if (isSearchMessageActivity) return;
        //unlock right menu
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.END);
    }

    private void enableLeftSlideMenu() {
        if (isSearchMessageActivity) return;
        //unlock left menu
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, Gravity.LEFT);
    }

    private void disableRightSlideMenu() {
        if (isSearchMessageActivity) return;
        //lock right menu
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
    }

    private void disableLeftSlideMenu() {
        if (isSearchMessageActivity) return;
        //lock left menu
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT);
    }

    public void checkToOpenRightSlideMenu() {
        if (isSearchMessageActivity) return;
        isForceOpen = true;
        if (mDrawerLayout.isDrawerVisible(GravityCompat.END)) {
            Log.i(TAG, "right slide menu is visible");
        } else {
            Log.i(TAG, "right slide menu is invisible");
            mDrawerLayout.openDrawer(GravityCompat.END);
        }
    }

    public void checkRightSlideMenuToClose() {
        if (isSearchMessageActivity) return;
        if (mDrawerLayout.isDrawerVisible(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        }
    }

    public void checkLeftSlideMenuToClose() {
        if (isSearchMessageActivity) return;
        if (mDrawerLayout.isDrawerVisible(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    /**
     * enable hay ko both slide menu
     *
     * @param enable
     */
    public void setEnableBothSideSlideMenu(boolean enable) {
        if (isSearchMessageActivity) return;
        if (enable) {
            //enableLeftSlideMenu();
            enableRightSlideMenu();
        } else {
            //disableLeftSlideMenu();
            disableRightSlideMenu();
        }
    }

    /**
     * enable left side, disable right side
     */
    public void setOnlyLeftSlideMenuEnable() {
        if (isSearchMessageActivity) return;
        //enableLeftSlideMenu();
        disableRightSlideMenu();
    }

    public void dispatchUploadSongIntent(int actionCode, int threadId) {
        Intent i = new Intent(getApplicationContext(), SearchSongActivity.class);
        i.putExtra("actionCode", actionCode);
        i.putExtra("threadId", threadId);
        i.putExtra("uploadSong", true);
        startActivityForResult(i, actionCode);
    }

    public void dispatchSelectSongIntent(int actionCode, int threadId) {
        Intent i = new Intent(getApplicationContext(), SearchSongActivity.class);
        i.putExtra("actionCode", actionCode);
        i.putExtra("threadId", threadId);
        startActivityForResult(i, actionCode);
    }


    public void dispatchSetBackgroundIntent() {
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "assets:bgfull");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_PICK_BG, true);
        String bg_path;
        if (TextUtils.isEmpty(mCurrentThreadMessage.getBackground())) {
            bg_path = ImageLoaderManager.PATH_BACKGROUND_DEFAULT;
        } else {
            bg_path = mCurrentThreadMessage.getBackground();
        }
        i.putExtra(ImageBrowserActivity.PARAM_PATH_SELECTED, bg_path);
        i.putExtra(ImageBrowserActivity.PARAM_GEN_CACHE_FILE, true);
        i.putExtra(ImageBrowserActivity.PARAM_THREAD_ID, String.valueOf(mCurrentThreadMessage.getId()));
        setActivityForResult(true);
        startActivityForResult(i, Constants.ACTION.SET_BACKGROUND);
    }

    public void dispatchPickPictureIntent() {
        // gủi anh
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, Constants.MESSAGE.IMAGE_LIMIT);
        setActivityForResult(true);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    public void dispatchShareContactIntent() {
        Intent i = new Intent(getApplicationContext(), ChooseContactActivity.class);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadType);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, mThreadId);
        startActivityForResult(i, Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT, true);
    }

    public void dispatchShareFile() {
        if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            PermissionHelper.requestPermissionWithGuide(ChatActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Constants.PERMISSION.PERMISSION_REQUEST_FILE);
        } else {
            chooseFile();
        }
    }

    private void chooseFile() {
        if (Version.hasKitKat()) {
            try {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("*/*");
//                intent.putExtra(Intent.EXTRA_MIME_TYPES, Constants.FILE.FILE_DOC_TYPES);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                setActivityForResult(true);
                startActivityForResult(intent, Constants.ACTION.ACTION_PICK_FILE);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                showToast(R.string.e667_device_not_support_function);
            }
        } else {
            FileChooserDialog dialog = new FileChooserDialog(this);
            // Assign listener for the select event.
            dialog.addListener(new FileChooserDialog.OnFileSelectedListener() {
                @Override
                public void onFileSelected(Dialog source, File file) {
                    source.hide();
                    if (file.exists()) {
                        String filePath = file.getAbsolutePath();
                        String fileName = FileHelper.getFileNameFromPath(filePath);
                        transferFile(filePath, fileName);
                    } else {
                        showToast(R.string.e668_file_not_found);
                    }
                }

                @Override
                public void onFileSelected(Dialog source, File folder, String name) {

                }
            });
            dialog.setFilter(".*doc|.*docx|.*xls|.*xlsx|.*ppt|.*pptx|.*pdf|.*txt");
            dialog.setShowOnlySelectable(true);
            dialog.show();
        }
    }

    public void dispatchForwardContentIntent(ReengMessage reengMessage) {
        Intent i = new Intent(getApplicationContext(), ChooseContactActivity.class);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadType);
        i.putExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, reengMessage);
        startActivityForResult(i, Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT, true);
    }

    public void dispatchTakePhotoIntent(int actionCode) {
        int reqCode;
        if (actionCode == Constants.ACTION.ACTION_TAKE_PHOTO_GROUP_AVATAR) {
            reqCode = Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO_GROUP_AVATAR;
        } else {
            reqCode = Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO;
        }
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            final ArrayList<String> array = new ArrayList<>();
            //
            boolean needShowPermissionDialog = false;
            boolean goToSettings = false;
            //storage
            int storagePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (storagePermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
                needShowPermissionDialog = true;
                goToSettings = true;
            }
            if (storagePermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
                array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            //camera
            int cameraPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.CAMERA);
            if (cameraPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
                needShowPermissionDialog = true;
                if (mPref != null && !mPref.getBoolean("FirstRequest_" + Manifest.permission.CAMERA, true)) {
                    goToSettings = true;
                }
            }
            array.add(Manifest.permission.CAMERA);
            final boolean needGoToSettings = goToSettings;
            final String[] permissions = array.toArray(new String[array.size()]);
            if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE && needShowPermissionDialog) {
                final int permissionCode = reqCode;
                PermissionDialog.newInstance(reqCode, true, new PermissionDialog.CallBack() {
                    @Override
                    public void onPermissionAllowClick(boolean allow, int clickCount) {
                        if (allow && needGoToSettings) {
                            //go to settings
                            dismissDialogFragment(PermissionDialog.TAG);
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            PermissionHelper.showDialogPermissionSettingIntro(ChatActivity.this, array, permissionCode);
                        } else {
                            PermissionHelper.requestPermissions(ChatActivity.this, permissions, permissionCode);
                            if (mPref != null) {
                                mPref.edit().putBoolean("FirstRequest_" + Manifest.permission.CAMERA, false).apply();
                            }
                        }
                    }
                }).show(getSupportFragmentManager(), PermissionDialog.TAG);
            } else {
                PermissionHelper.requestPermissions(ChatActivity.this, permissions, reqCode);
            }
        } else if (PermissionHelper.declinedPermission(mApplication, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            PermissionHelper.requestPermissionWithGuide(ChatActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, reqCode);
        } else {
            takePhoto(actionCode);
        }
    }

    private void takePhoto(int actionCode) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = ImageHelper.getInstance(ChatActivity.this).createImageFile(getApplicationContext());
            if (actionCode == Constants.ACTION.ACTION_TAKE_PHOTO_GROUP_AVATAR) {
                mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_GROUP_IMAGE_PATH, f.getAbsolutePath()).apply();
            } else {
                mPref.edit().putString(Constants.PREFERENCE.PREF_CHAT_IMAGE_PATH, f.getAbsolutePath()).apply();
            }
            //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, f));
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(takePictureIntent, actionCode);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
            if (permissionResult != null) {
                permissionResult.declined();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
            if (permissionResult != null) {
                permissionResult.declined();
            }
        }
    }

    /**
     * CROP IMAGE
     *
     * @param inputFilePath
     */
    private void cropAvatarImage(String inputFilePath) {
        // crop file
        try {
            String time = String.valueOf(System.currentTimeMillis());
            File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                Config.Storage.IMAGE_COMPRESSED_FOLDER, "/avatar_group" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_GROUP_FILE_CROP, cropImageFile.toString()).apply();
            // intent crop
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            intent.putExtra(CropView.MASK_OVAL, true);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    public void dispatchShareLocation() {
        Intent i = new Intent(getApplicationContext(), ShareLocationActivity.class);
        i.putExtra(Constants.LOCATION.DATA_INPUT_TYPE, Constants.LOCATION.TYPE_SEND);
        startActivityForResult(i, Constants.ACTION.SHARE_LOCATION, true);
    }

    public void dispatchPickVideo() {
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_ALL_VIDEO, true);
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 15);
        setActivityForResult(true);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_VIDEO);
    }

    public void dispatchTakeVideo(int actionCode) {
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(ChatActivity.this,
                Manifest.permission.CAMERA,
                Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takeVideo(actionCode);
        }
    }

    private void takeVideo(int actionCode) {
        try {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            takeVideoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, Constants.FILE.VIDEO_MAX_SIZE);
            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Constants.FILE.VIDEO_RECORD_MAX_DURATION);
            takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            setActivityForResult(true);
            startActivityForResult(takeVideoIntent, actionCode);
            showToast(String.format(getString(R.string.video_size_limit),
                (int) FileHelper.getSizeInMbFromByte(Constants.FILE.VIDEO_MAX_SIZE)), Toast.LENGTH_SHORT);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
            if (permissionResult != null) {
                permissionResult.declined();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            mCurrentVideoPath = null;
            showToast(R.string.prepare_photo_fail);
            if (permissionResult != null) {
                permissionResult.declined();
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Log.i(TAG, "onActivityResult requestCode = " + requestCode);
        dismissDialogFragment(PermissionDialog.TAG);
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        super.onActivityResult(requestCode, resultCode, data);
        mApplication.getMessageBusiness().addLockRoomListener(this);
        if (!mApplication.isDataReady()) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    while (!mApplication.isDataReady()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            Log.e(TAG, "InterruptedException", e);
                        }
                    }
                    processOnActivityResult(requestCode, resultCode, data);
                }
            };
            thread.start();
        } else {
            processOnActivityResult(requestCode, resultCode, data);
        }
    }

    void processOnActivityResult(int requestCode, int resultCode, Intent data) {
        setActivityForResult(false);
        setTakePhotoAndCrop(false);
        if (resultCode != RESULT_OK) {
            if (requestCode == Constants.ACTION.ADD_CONTACT) {
                ContentObserverBusiness.getInstance(this).setAction(-1);
            } else if (requestCode == Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD || requestCode == Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD_SHARE) {
                finish();
            }

            return;
        }

        // Open ThreadDetailSettingFragment after back from create group - DungLV 29/11/2020
        if (data != null && data.getAction() != null && data.getAction().equals(Constants.CHOOSE_CONTACT.RESULT_CREATE_GROUP)) {
            mDrawerLayout.openDrawer(GravityCompat.END);
            return;
        }
        if (data != null && data.getAction() != null && data.getAction().equals(Constants.CHOOSE_CONTACT.RESULT_SEARCH_MESSAGER)) {
            mDrawerLayout.openDrawer(GravityCompat.END);
            return;
        }

        switch (requestCode) {
            case Constants.ACTION.ACTION_TAKE_PHOTO: {
                String currentPath = mPref.getString(Constants.PREFERENCE.PREF_CHAT_IMAGE_PATH, "");
                Log.i(TAG, "ACTION_TAKE_PHOTO OK " + currentPath);
                if (!TextUtils.isEmpty(currentPath)) {
                    FileHelper.refreshGallery(mApplication, currentPath);
                    filePicker.transferTakenPhoto(currentPath);
                } else {
                    showToast(R.string.prepare_photo_fail);
                }
                break;
            }
            case Constants.ACTION.ACTION_PICK_PICTURE: {
                if (data != null) {
                    ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                    if (picturePath != null) {
                        filePicker.transferPickedPhoto(picturePath);
                    }
                }
                break;
            }
            case Constants.ACTION.ACTION_TAKE_PHOTO_GROUP_AVATAR: {
                String currentPath = mPref.getString(Constants.PREFERENCE.PREF_AVATAR_GROUP_IMAGE_PATH, "");
                if (!TextUtils.isEmpty(currentPath)) {
                    cropAvatarImage(currentPath);
                } else {
                    setActivityForResult(false);
                    setTakePhotoAndCrop(false);
                    showToast(R.string.prepare_photo_fail);
                }
                break;
            }
            case Constants.ACTION.ACTION_PICK_PICTURE_GROUP_AVATAR: {
                if (data != null) {
                    ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                    if (picturePath != null && picturePath.size() > 0) {
                        String pathImage = picturePath.get(0);
                        cropAvatarImage(pathImage);
                    } else {
                        setActivityForResult(false);
                        setTakePhotoAndCrop(false);
                        showError(R.string.file_not_found_exception, null);
                    }
                }
                setTakePhotoAndCrop(false);
                setActivityForResult(false);
                break;
            }
            case Constants.ACTION.ACTION_CROP_IMAGE:
                if (data != null) {
                    String fileCrop = mPref.getString(Constants.PREFERENCE.PREF_AVATAR_GROUP_FILE_CROP, "");
                    MessageHelper.getPathOfCompressedFile(fileCrop, "", "", false);
                    detailSettingCallBack.transferGroupAvatar(fileCrop);
                }
                FileHelper.deleteFile(getApplicationContext(), mPref.getString(Constants.PREFERENCE
                    .PREF_AVATAR_GROUP_IMAGE_PATH, ""));
                break;
            case Constants.ACTION.SET_BACKGROUND: {
                if (data != null) {
                    ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                    boolean isSetAll = data.getBooleanExtra(Constants.BACKGROUND.KEY_IS_APPPLY_ALL, false);
                    Log.d(TAG, isSetAll + "");
                    if ((picturePath != null && picturePath.size() > 0)) {
                        String pathBackgroundImage = picturePath.get(0);
                        if (pathBackgroundImage != null) {
                            // update mem, DB
                            mCurrentThreadMessage.setBackground(pathBackgroundImage);
                            if (isSetAll) {
                                mPref.edit().putString(Constants.PREFERENCE.PREF_DEFAULT_BACKGROUND_PATH,
                                    pathBackgroundImage).apply();
                                ArrayList<ThreadMessage> listUpdateBackground = new ArrayList<>();
                                for (ThreadMessage thread : mMessageBusiness.getThreadMessageArrayList()) {
                                    // room chat khong cap background duoi client
                                    if (thread.getThreadType() != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                                        thread.setBackground(pathBackgroundImage);
                                        listUpdateBackground.add(thread);
                                    }
                                }
                                mMessageBusiness.updateListThreadMessageBackground(listUpdateBackground);
                            } else {
                                mMessageBusiness.updateThreadMessage(mCurrentThreadMessage);
                            }
                            mPref.edit().putBoolean(Constants.PREFERENCE.PREF_APPLY_BACKGROUND_ALL, isSetAll).apply();
                            // update UI
                            if (mThreadDetailFragment != null) {
                                mThreadDetailFragment.updateBackgroundUI(pathBackgroundImage);
                            }
                            //set pref default path
                            String image_source = data.getStringExtra(Constants.BACKGROUND.KEY_CHAT_SCREEN);
                            if (image_source != null && image_source.equals(Constants.BACKGROUND.FROM_SERVER))
                                trackingEvent(R.string.ga_category_chat_screen,
                                    getString(R.string.ga_action_change_background_from_server),
                                    pathBackgroundImage);
                            else if (image_source != null && image_source.equals(Constants.BACKGROUND.FROM_DEVICE))
                                trackingEvent(R.string.ga_category_chat_screen,
                                    getString(R.string.ga_action_change_background_from_device),
                                    pathBackgroundImage);
                            else
                                trackingEvent(R.string.ga_category_chat_screen,
                                    getString(R.string.ga_action_change_background), pathBackgroundImage);
                        }
                    }
                } else
                    Log.i(TAG, "data null");
                break;
            }
            case Constants.ACTION.ADD_CONTACT:
                ContentObserverBusiness.getInstance(this).setAction(-1);
                break;
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                if (data != null) {
                    getResultCreateGroup(data);
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                if (data != null) {
                    getResultCreateBroadcast(data);
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP:
                if (data != null) {
                    getResultCreateGroup(data);
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST:
                if (data != null) {
                    getResultCreateBroadcast(data);
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT:
                if (data != null) {
                    ArrayList<PhoneNumber> listContact = (ArrayList<PhoneNumber>) data.getSerializableExtra(Constants.CHOOSE_CONTACT.RESULT_LIST_CONTACT);
                    Log.i(TAG, "mThreadId transferSelectedContact " + filePicker.hashCode());
                    filePicker.transferSelectedContact(listContact);
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT:
                ReengMessage reengMessageForward = (ReengMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT
                    .DATA_REENG_MESSAGE);
                if (data.hasExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE)) {
                    mCurrentThreadMessage = (ThreadMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT
                        .DATA_THREAD_MESSAGE);
                    mThreadId = mCurrentThreadMessage.getId();
                    mThreadType = mCurrentThreadMessage.getThreadType();
                } else {
                    String number = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
                    Log.i(TAG, "number: " + number);
                    //mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(number,false);
                    mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(number);
                    mThreadId = mCurrentThreadMessage.getId();
                    mThreadType = mCurrentThreadMessage.getThreadType();
                }
                // Log.i(TAG, "Forward replace fragment " + mCurrentThreadMessage);
                if (findViewById(R.id.fragment_container) != null && reengMessageForward != null) {
                    reengMessageForward.setForwardingMessage(true);
                    displayMessageDetailFragment(mCurrentThreadMessage, reengMessageForward, null, Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD_SHARE);
                }
                break;
            case Constants.ACTION.SHARE_LOCATION:
                String address = data.getStringExtra(Constants.LOCATION.DATA_ADDRESS);
                String latitude = data.getStringExtra(Constants.LOCATION.DATA_LATITUDE);
                String longitude = data.getStringExtra(Constants.LOCATION.DATA_LONGITUDE);
                Log.i(TAG, "mThreadId transfer Share Location " + filePicker.hashCode());
                filePicker.transferShareLocation(address, latitude, longitude);
                break;
            case Constants.ACTION.ACTION_TAKE_VIDEO: {
                mCurrentVideoPath = FileHelper.getRealPathVideoFromURI(this, data.getData());
                Log.i(TAG, "ACTION_TAKE_VIDEO OK " + mCurrentVideoPath);
                if (mCurrentVideoPath != null && !TextUtils.isEmpty(mCurrentVideoPath)) {
                    FileHelper.refreshGallery(mApplication, mCurrentVideoPath);
                    ImageInfo mediaFile = FileHelper.getMediaInfo(mApplication, mCurrentVideoPath);
                    convertAndSendVideo(mediaFile);
                } else {
                    showToast(R.string.prepare_photo_fail);
                }
                break;
            }
            case Constants.ACTION.ACTION_PICK_VIDEO: {
                if (data != null) {
                    ArrayList<String> videoPath = data.getStringArrayListExtra("data");
                    if (videoPath != null) {
                        mCurrentVideoPath = videoPath.get(0);
                        int duration = (int) data.getLongExtra("duration", 0L);
                        //String res = data.getStringExtra("resolution");
                        Log.i(TAG, "ACTION_PICK_VIDEO OK " + mCurrentVideoPath + " duration: " + duration);
                        File file = new File(mCurrentVideoPath);
                        //String outPath = "";
                        int fileLen = (int) file.length();
                        Log.d("file size:", fileLen + "");
                    } else {
                        showToast(R.string.prepare_photo_fail);
                    }
                }
                break;
            }
            case Constants.ACTION.CHOOSE_VIDEO_WATCH_TOGETHER:
                if (data != null) {
                    MediaModel video = (MediaModel) data.getSerializableExtra("data");
                    if (video != null && mThreadDetailFragment != null) {
                        mThreadDetailFragment.sendVideoWatchTogether(video);
                    }
                }
                break;
            case Constants.ACTION.CHOOSE_DOCUMENT_CLASS:
                if (data != null) {
                    DocumentClass document = (DocumentClass) data.getSerializableExtra(Constants.DOCUMENT
                        .DOCUMENT_DATA);
                    filePicker.transferSelectedDocument(document);
                } else {
                    showToast(R.string.e601_error_but_undefined);
                }
                break;
            case Constants.ACTION.ACTION_PICK_FILE:
                handleSelectFile(data);
                break;
            case Constants.ACTION.SEND_GIFT_LIXI:
                if (data != null) {
                    GiftLixiModel giftLixi = (GiftLixiModel) data.getExtras().getSerializable("data");
                    if (giftLixi != null && mThreadDetailFragment != null) {
                        mThreadDetailFragment.sendMessageGiftLixi(giftLixi);
                    }
                }
                break;

            case Constants.SEARCH_MESSAGE.TYPE_CREATE_SEARCH_MESSAGE:
                if (data != null) {
                    int actionCode = data.getExtras().getInt(Constants.SEARCH_MESSAGE.DATA_TYPE, -1);
                    switch (actionCode) {
                        case Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT:
                            ReengMessage rmf = (ReengMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT
                                .DATA_REENG_MESSAGE);
                            if (data.hasExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE)) {
                                mCurrentThreadMessage = (ThreadMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT
                                    .DATA_THREAD_MESSAGE);
                                mThreadId = mCurrentThreadMessage.getId();
                                mThreadType = mCurrentThreadMessage.getThreadType();
                            } else {
                                String number = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
                                Log.i(TAG, "number: " + number);
                                //mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(number,false);
                                mCurrentThreadMessage = mMessageBusiness.findExistingOrCreateNewThread(number);
                                mThreadId = mCurrentThreadMessage.getId();
                                mThreadType = mCurrentThreadMessage.getThreadType();
                            }
                            // Log.i(TAG, "Forward replace fragment " + mCurrentThreadMessage);
                            if (findViewById(R.id.fragment_container) != null) {
                                rmf.setForwardingMessage(true);
                                displayMessageDetailFragment(mCurrentThreadMessage, rmf, null, Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD_SHARE);
                            }
                            break;
                    }
                }
                break;
            case Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD:
                notShowPin = true;
                goToThreadDetail();
                break;

            case Constants.ACTION.ACTION_OPEN_HIDDEN_THREAD_SHARE:
                notShowPin = true;
                displayMessageDetailFragmentWithoutPin(mCurrentThreadMessage, messageShare, arrayMessage);
                break;
        }
    }

    private void handleSelectFile(Intent data) {
        if (data != null) {
            if (FileHelper.isDriveFile(data.getData())) {
                //showToast(R.string.e666_not_support_function);
                showLoadingDialog(null, R.string.downloading);
                if (downloadFileDriveAsyncTask != null) {
                    downloadFileDriveAsyncTask.cancel(true);
                }
                downloadFileDriveAsyncTask = new DownloadFileDriveAsyncTask(mApplication, ChatActivity.this,
                    data.getData(), new DownloadDriveResponse() {
                    @Override
                    public void onResult(String fileName, String filePath) {
                        hideLoadingDialog();
                        transferFile(filePath, fileName);
                    }

                    @Override
                    public void onFail(boolean isMaxSize) {
                        hideLoadingDialog();
                        showToast(R.string.e601_error_but_undefined);
                    }
                });
                downloadFileDriveAsyncTask.execute();
            } else {
                String filePath = FileHelper.getRealPath(ChatActivity.this, data.getData());
                if (TextUtils.isEmpty(filePath)) {
                    showToast(R.string.e668_file_not_found);
                } else {
                    File file = new File(filePath);
                    if (file.exists()) {
                        if (file.length() >= Constants.FILE.DOCUMENT_MAX_SIZE) {
                            showToast(R.string.file_exceed_size);
                        } else {
                            String fileName = FileHelper.getFileNameFromPath(filePath);
                            transferFile(filePath, fileName);
                        }
                    } else {
                        showToast(R.string.e668_file_not_found);
                    }
                }
            }
        } else {
            showToast(R.string.e601_error_but_undefined);
        }
    }

    private void transferFile(String filePath, String fileName) {
        String extension = FileHelper.getExtensionFile(filePath);
        if (ReengMessageConstant.FileType.isSupported(extension)) {
            filePicker.transferPickedFile(filePath, fileName, extension);
        } else {
            showToast(R.string.e666_not_support_function);
        }
    }

    private void getResultCreateGroup(Intent data) {
        mThreadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
        mThreadType = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_TYPE, -1);
        Serializable serializable = data.getSerializableExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE);
        if (serializable instanceof ReengMessage)
            messageShare = (ReengMessage) serializable;
        mCurrentThreadMessage = mMessageBusiness.findThreadByThreadId(mThreadId);
        mCurrentThreadMessage.setForceCalculatorAllMember(true);
        Log.i(TAG, "replace fragment " + mCurrentThreadMessage);
        if (findViewById(R.id.fragment_container) != null) {
            displayMessageDetailFragment(mCurrentThreadMessage);
        }
        checkRightSlideMenuToClose();
        changeDataThreadSettingFragment(mCurrentThreadMessage);
    }

    private void getResultCreateBroadcast(Intent data) {
        mThreadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
        mThreadType = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_TYPE, -1);
        mCurrentThreadMessage = (ThreadMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE);
        Log.i(TAG, "getResultCreateBroadcast replace fragment " + mCurrentThreadMessage);
        if (findViewById(R.id.fragment_container) != null) {
            displayMessageDetailFragment(mCurrentThreadMessage);
        }
        checkRightSlideMenuToClose();
    }

    public void showViewSetting() {
        if (isSearchMessageActivity) return;
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_slide_menu_right_frame);
        if (fragment == null) {
            if (mThreadDetailFragment != null && !mThreadDetailFragment.isSaveInstance) {
                threadDetailSettingFragment = ThreadDetailSettingFragment.newInstance(mThreadId);
                executeFragmentTransaction(threadDetailSettingFragment, R.id.activity_slide_menu_right_frame, false,
                    false);
            }
        } else {
            threadDetailSettingFragment = (ThreadDetailSettingFragment) fragment;
            threadDetailSettingFragment.setFragmentType();
        }
        checkToOpenRightSlideMenu();
    }

    public void showViewManager() {
        if (isSearchMessageActivity) return;
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.activity_slide_menu_right_frame);
        if (fragment == null) {
            threadDetailSettingFragment = ThreadDetailSettingFragment.newInstance(mThreadId);
//            executeFragmentTransaction(threadDetailSettingFragment, R.id.activity_slide_menu_right_frame, false, false);
            executeFragmentTransactionAllowLoss(threadDetailSettingFragment,
                R.id.activity_slide_menu_right_frame, false, false, "ThreadDetailSettingFragment");
        } else {
            threadDetailSettingFragment = (ThreadDetailSettingFragment) fragment;
            threadDetailSettingFragment.setFragmentType();
        }
        checkToOpenRightSlideMenu();
    }

    private void initLayoutParamSlideMenu() {
        if (isSearchMessageActivity) return;
        double density = getResources().getDisplayMetrics().density;
        int marginSlideMenu = 100;
        if (density > 0) {
            marginSlideMenu = (int) (Constants.PREF_DEFAULT.SLIDE_MENU_MARGIN * density);
        }
        // get screen size
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(displaymetrics);
//        int slideWidth = displaymetrics.widthPixels - marginSlideMenu;
        int slideWidth = displaymetrics.widthPixels;
        DrawerLayout.LayoutParams leftParams = (DrawerLayout.LayoutParams) mLeftSlideMenu.getLayoutParams();
        DrawerLayout.LayoutParams rightParams = (DrawerLayout.LayoutParams) mRightSlideMenu.getLayoutParams();
//        leftParams.width = slideWidth;
        rightParams.width = slideWidth;
    }

    public void convertAndSendVideo(ImageInfo imageInfo) {
        String outPath = "";
        try {
            outPath = FileHelper.createVideoFile().getCanonicalPath();
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
        File file = new File(imageInfo.getImagePath());
        int fileLen = (int) file.length();
        Log.d(TAG, "onSendVideo" + imageInfo.toString() + " outPath: " + outPath);
        Video v = new Video(imageInfo.getImagePath().replace(" ", "\\ "), outPath, imageInfo.getDurationInSecond(),
            fileLen, imageInfo.getResolution());

        filePicker.transferTakenVideo1(imageInfo.getImagePath(),
            FileHelper.getNameFileFromURL(imageInfo.getImagePath()),
            imageInfo.getDurationInSecond(), 0, v);
    }

    public void navigateToSendGiftLixi(ArrayList<String> listFriendJid, int threadId) {
        Intent i = new Intent(this, GiftLixiActivity.class);
        i.putStringArrayListExtra(GiftLixiActivity.LIST_FRIEND_JID, listFriendJid);
        i.putExtra(GiftLixiActivity.THREAD_ID, threadId);
        startActivityForResult(i, Constants.ACTION.SEND_GIFT_LIXI);
    }

    @Override
    public void notifyLockRoom(int threadId, final int status, final long timeLock) {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Resources res = getResources();
                showToast(res.getString(R.string.notify_spam), Toast.LENGTH_LONG);
                /*if (status == SpamRoomManager.SPAM_DUPLICATE_TEXT ||
                        status == SpamRoomManager.SPAM_DUPLICATE_STICKER ||
                        status == SpamRoomManager.SPAM_MUSIC) {
                    showToast(res.getString(R.string.notify_spam), Toast.LENGTH_LONG);
                } else {
                    showToast(TimeHelper.formatTimeLocked(timeLock, res), Toast.LENGTH_LONG);
                }*/
            }
        });
    }

    @Override
    public void notifySpamStranger(int threadId, final String msg) {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(msg, Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: " + requestCode + " permissions " + permissions + " grantResults " +
            grantResults);
        if (PermissionHelper.verifyPermissions(grantResults)) {
            if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO) {
                takePhoto(Constants.ACTION.ACTION_TAKE_PHOTO);
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO_GROUP_AVATAR) {
                takePhoto(Constants.ACTION.ACTION_TAKE_PHOTO_GROUP_AVATAR);
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_VIDEO) {
                takeVideo(Constants.ACTION.ACTION_TAKE_VIDEO);
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE) {
                mApplication.initFolder();
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_GALLERY) {
                mApplication.initFolder();
                if (mThreadDetailFragment != null) {
                    mThreadDetailFragment.showMediaPreview();
                }
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_FILE) {
                mApplication.initFolder();
                chooseFile();
            } else if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_FILE_DOWNLOAD) {
                mApplication.initFolder();
            }
        } else if (permissionResult != null) {
            permissionResult.declined();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void setPermissionResult(RequestPermissionResult permissionResult) {
        this.permissionResult = permissionResult;
    }

    public void checkThreadEncrypt() {
        if (mThreadDetailFragment != null) mThreadDetailFragment.checkThreadEncrypt();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (threadDetailSettingFragment != null && threadDetailSettingFragment.isAdded() && threadDetailSettingFragment.isVisible()) {
                checkRightSlideMenuToClose();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public interface RequestPermissionResult {
        void declined();
    }
}
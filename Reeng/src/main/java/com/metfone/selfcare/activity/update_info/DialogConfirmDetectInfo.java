package com.metfone.selfcare.activity.update_info;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.CreateYourAccountActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.databinding.FragmentDialogConfirmDetectInfoBinding;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.DetectInfo;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DialogConfirmDetectInfo extends DialogFragment {

    private static final String DETECT_INFO = "DETECT_INFO";
    private IDialogConfirm iDialogConfirm;
    private DetectInfo detectInfo;
    private ApplicationController mApplication;

    private List<String> genders;
    private ArrayAdapter<String> arrayAdapter;

    private FragmentDialogConfirmDetectInfoBinding binding;

    public void setInterface(IDialogConfirm iDialogConfirm) {
        this.iDialogConfirm = iDialogConfirm;
    }

    public static DialogConfirmDetectInfo newInstance(DetectInfo detectInfo) {
        DialogConfirmDetectInfo dialog = new DialogConfirmDetectInfo();
        Bundle args = new Bundle();
        args.putParcelable(DETECT_INFO, detectInfo);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            if (dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mApplication = (ApplicationController) requireActivity().getApplication();
        binding = FragmentDialogConfirmDetectInfoBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAction();
        Bundle bundle = getArguments();
        if (bundle != null) {
            detectInfo = bundle.getParcelable(DETECT_INFO);
            setupView();
        }
    }

    private void setAction() {
        binding.btnYes.setOnClickListener(v -> {
            if (iDialogConfirm != null) {
                String fullName = binding.edtFullName.getText().toString();
                String dob = binding.edtDob.getText().toString();
                String idNo = binding.edtIdNumber.getText().toString();
                int sex = binding.spnSex.getSelectedItemPosition() + 1;
                String address = binding.edtCurrentAddress.getText().toString();

                if (!TextUtils.isEmpty(dob)) {
                    dob = TimeHelper.convertTimeToAPi(dob,
                            UserInfoBusiness.getCurrentLanguage((BaseSlidingFragmentActivity) requireActivity(), mApplication));
                }

                DetectInfo info= new DetectInfo();
                info.setFullname(fullName);
                info.setDob(dob);
                info.setIdNumber(idNo);
                info.setSex(String.valueOf(sex));
                info.setAddress(address);
                iDialogConfirm.confirm(info);
                dismiss();
            }
        });
        genders = Arrays.asList(getResources().getStringArray(R.array.sex));
        arrayAdapter = new ArrayAdapter<String>(requireContext(), R.layout.display_spinner_dark, genders) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return genders.size();
            }
        };
        arrayAdapter.setDropDownViewResource(R.layout.display_spinner_dark);
        binding.spnSex.setAdapter(arrayAdapter);
        binding.spnSex.setSelection(0);
    }

    private void setupView() {
        String sex = detectInfo.getSex();

        binding.edtDob.setText(detectInfo.getDob());
        binding.spnSex.setSelection(0);
        binding.edtIdNumber.setText(detectInfo.getIdNumber());
        binding.edtCurrentAddress.setText(detectInfo.getAddress());


        if (TextUtils.isEmpty(detectInfo.getFullname())) {
            binding.edtFullName.setText(R.string.empty_string);
        } else {
            binding.edtFullName.setText(detectInfo.getFullname());
        }

        if (TextUtils.isEmpty(sex)) {
            binding.spnSex.setSelection(2);
        } else {
            boolean equals = sex.toLowerCase(Locale.ROOT).equals("MALE".toLowerCase(Locale.ROOT));
            if (equals) {
                binding.spnSex.setSelection(0);
            } else {
                binding.spnSex.setSelection(1);
            }
        }

        if (TextUtils.isEmpty(detectInfo.getAddress())) {
            binding.edtCurrentAddress.setText(R.string.empty_string);
        } else {
            binding.edtCurrentAddress.setText(detectInfo.getAddress());
        }

        if (TextUtils.isEmpty(detectInfo.getDob())) {
            binding.edtDob.setText(R.string.empty_string);
        } else {
            binding.edtDob.setText(DateTimeUtils.getDateFromOCR(detectInfo.getDob()));
        }
        if (TextUtils.isEmpty(detectInfo.getIdNumber())) {
            binding.edtIdNumber.setText(R.string.empty_string);
        } else {
            binding.edtIdNumber.setText(detectInfo.getIdNumber());
        }
    }

    @Override
    public void onResume() {
        if (getDialog() != null) {
            ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        }
        super.onResume();
    }

    public interface IDialogConfirm {
        void confirm(DetectInfo detectInfo);
    }
}

package com.metfone.selfcare.activity;

import android.os.Bundle;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.fragment.message.PollCreateFragmentV2;
import com.metfone.selfcare.fragment.message.PollDetailFragment;
import com.metfone.selfcare.fragment.message.PollDetailFragmentV2;
import com.metfone.selfcare.fragment.message.PollItemDetailFragment;
import com.metfone.selfcare.fragment.message.PollSettingFragmentV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 8/22/2016.
 */
public class PollMessageActivity extends BaseSlidingFragmentActivity implements PollDetailFragment.OnFragmentInteractionListener {
    private static final String TAG = PollMessageActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private int threadId = -1, fragmentType = -1;
    private boolean pollUpdate;
    private String pollId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        changeWhiteStatusBar();
        mApplication = (ApplicationController) getApplicationContext();
        setToolBar(findViewById(R.id.tool_bar));
        getDataAndDisplayFragment(savedInstanceState);
    }

    @Override
    public void onResume() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodUtils.hideSoftKeyboard(PollMessageActivity.this);
            }
        });
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        outState.putInt(Constants.MESSAGE.TYPE, fragmentType);
        outState.putString(Constants.MESSAGE.POLL_ID, pollId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void navigateToPollItemDetail(String pollId, String pollItemId) {
        PollItemDetailFragment itemDetailFragment = PollItemDetailFragment.newInstance(pollId, pollItemId);
        executeFragmentTransaction(itemDetailFragment, R.id.fragment_container, true, false);
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            threadId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
            fragmentType = savedInstanceState.getInt(Constants.MESSAGE.TYPE);
            pollId = savedInstanceState.getString(Constants.MESSAGE.POLL_ID);
            pollUpdate = savedInstanceState.getBoolean(Constants.MESSAGE.POLL_UPDATE);
        } else if (getIntent() != null) {
            threadId = getIntent().getIntExtra(ReengMessageConstant.MESSAGE_THREAD_ID, -1);
            fragmentType = getIntent().getIntExtra(Constants.MESSAGE.TYPE, -1);
            pollId = getIntent().getStringExtra(Constants.MESSAGE.POLL_ID);
            pollUpdate = getIntent().getBooleanExtra(Constants.MESSAGE.POLL_UPDATE, false);
        }
        if (fragmentType == -1 || threadId == -1) return;
        if (fragmentType == Constants.MESSAGE.TYPE_CREATE_POLL) {
            PollCreateFragmentV2 pollCreateFragment = PollCreateFragmentV2.newInstance(threadId);
            executeFragmentTransaction(pollCreateFragment, R.id.fragment_container, false, false);
        } else {
            PollDetailFragmentV2 pollDetailFragment = PollDetailFragmentV2.newInstance(pollId, threadId, pollUpdate);
            executeFragmentTransaction(pollDetailFragment, R.id.fragment_container, false, false);
        }
    }

    public void openSetting(int threadId, String name, ArrayList<String> options) {
        PollSettingFragmentV2 pollDetailFragment = PollSettingFragmentV2.newInstance(threadId, name, options);
        executeFragmentTransaction(pollDetailFragment, R.id.fragment_container, false, false);
    }
}
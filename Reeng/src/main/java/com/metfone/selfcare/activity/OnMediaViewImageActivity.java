package com.metfone.selfcare.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.metfone.selfcare.adapter.PreviewImageProfileAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.UserApi;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedContent;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.listeners.ClickListener.IconListener;
import com.metfone.selfcare.listeners.FeedOnMediaListener;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.PreviewPager;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by thanhnt72 on 11/23/2016.
 */
public class OnMediaViewImageActivity extends BaseSlidingFragmentActivity implements IconListener,
        InitDataListener, FeedOnMediaListener, UserApi.AvatarCallback, UserApi.RemoveAvatarCallback {

    private static final String TAG = OnMediaViewImageActivity.class.getSimpleName();
    private static final long TIME_DELAY_SHOW = 2000;

    private ReengAccountBusiness mAccountBusiness;
    private ApplicationController mApplication;
    private ImageProfileBusiness imageProfileBusiness;
    private FeedBusiness mFeedBusiness;
    private Resources mRes;
    private WSOnMedia rest;
    private Handler mHandler;
    private Animation animFadeIn, animFadeOut;
    private ArrayList<UserInfo> currentListUserLike = new ArrayList<>();
    private EventOnMediaHelper eventOnMediaHelper;
    private FeedModelOnMedia mFeedModel;

    private ArrayList<ImageProfile> listImage = new ArrayList<>();
    private int currentPosition = 0;
    private String mName;
    private String mMsisdn;
    private PreviewImageProfileAdapter mPreviewImageProfileAdapter;
    private boolean isHideStatus = false;
    private boolean showRemoveButton = false;
    private int feedFrom = -1;
    private boolean isOpenProfile = false;
    private boolean needAddToHashMap = false;

    private PreviewPager mViewPager;
    private View mLayoutAb, mLayoutLikeShare;
    private ImageView mImgBack, mImgOverFlow, mImgDownload; //mImgRemove
    private TextView mTvwName, mTvwNumberImage, mTvwTime;
    private TextView mTvwPeopleLike, mTvwLike, mTvwComment, mTvwShare;
    private ImageView mImgLike, mImgComment, mImgShare;
//    private ElasticDragDismissRelativeLayout dragDismissLayout;

    private boolean isViewMyImageProfile;
    private String itemType;
    private FeedContent currentFeedContent;

    private ArrayList<ItemContextMenu> mOverFlowItems = new ArrayList<>();

    private CompositeDisposable compositeDisposable;
//    private ImageProfile img;

    @Override
    public void onResume() {
        super.onResume();
        ListenerHelper.getInstance().addNotifyFeedOnMediaListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (mHandler != null) mHandler.removeCallbacks(runnable);
        ListenerHelper.getInstance().removeNotifyFeedOnMediaListener(this);
    }

    @Override
    protected void onDestroy() {
        mFeedBusiness.clearHashMapFeed();
        if (compositeDisposable != null) compositeDisposable.dispose();
        Log.i(TAG, "clear hashmapfeed");
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
        setContentView(R.layout.activity_view_image_onmedia);
        changeStatusBar(true);
        initBusiness();
        getData(savedInstanceState);
        if (mApplication.isDataReady()) {
            initData();
        } else {
            ListenerHelper.getInstance().addInitDataListener(this);
            showLoadingDialog(null, R.string.loading);
        }

        setViewComponent();
        drawView();
        setViewListener();
        if (currentPosition < 0) currentPosition = 0;
        rest.getImageDetail(listImage.get(currentPosition).getIdServerString(), mMsisdn, itemType, new OnMediaInterfaceListener.GetListFeedContentListener() {

            @Override
            public void onGetListFeedContent(RestAllFeedContent restAllFeedContent) {
                Log.i(TAG, "onResponse");
                currentFeedContent = restAllFeedContent.getFeedContent();
                if (currentFeedContent == null) return;
                currentFeedContent.setItemType(itemType);
                currentListUserLike = restAllFeedContent.getUserLikes();
                getModelFeed(restAllFeedContent);
                drawFeedContent(currentListUserLike);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i(TAG, "onErrorResponse");
            }
        });

//        mHandler.postDelayed(runnable, TIME_DELAY_SHOW);
//        hideStatusBar(false);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
        }
    }

    /*private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hideStatusBar();
        }
    };*/

    private void getModelFeed(RestAllFeedContent restAllFeedContent) {
        if (needAddToHashMap) {
            FeedModelOnMedia feedTMP = new FeedModelOnMedia();
            feedTMP.setFeedContent(restAllFeedContent.getFeedContent());
            feedTMP.setIsLike(restAllFeedContent.getIsLike());
            feedTMP.setIsShare(restAllFeedContent.getIsShare());
            mFeedBusiness.addFeedToHashMap(feedTMP);
            mFeedModel = feedTMP;
        } else {
            switch (feedFrom) {
                case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_TAB_HOT:
                    mFeedModel = mFeedBusiness.getFeedModelFromUrl(restAllFeedContent.getFeedContent().getUrl());
                    break;
                case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_MY_PROFILE:
                    mFeedModel = mFeedBusiness.getFeedProfileFromUrl(restAllFeedContent.getFeedContent().getUrl());
                    break;
                case Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_FRIEND_PROFILE:
                    if (mFeedBusiness.getFeedProfileProcess() != null
                            && mFeedBusiness.getFeedProfileProcess().getFeedContent() != null
                            && restAllFeedContent.getFeedContent().getUrl()
                            .equals(mFeedBusiness.getFeedProfileProcess().getFeedContent().getUrl()))
                        mFeedModel = mFeedBusiness.getFeedProfileProcess();
                    else
                        mFeedModel = null;
                    break;
            }

            if (mFeedModel == null) {
                needAddToHashMap = true;
                FeedModelOnMedia feedTMP = new FeedModelOnMedia();
                feedTMP.setFeedContent(restAllFeedContent.getFeedContent());
                feedTMP.setIsLike(restAllFeedContent.getIsLike());
                feedTMP.setIsShare(restAllFeedContent.getIsShare());
                mFeedBusiness.addFeedToHashMap(feedTMP);
                mFeedModel = feedTMP;
            }
        }
    }

    private void drawFeedContent(ArrayList<UserInfo> listLike) {
        if (mFeedModel == null) return;
        FeedContent feedContent = mFeedModel.getFeedContent();
        if (feedContent == null) return;
//        mLayoutLikeShare.setVisibility(View.VISIBLE);
        setEnableImageAction(true);
        if (feedContent.getStamp() != 0L) {
            mTvwTime.setVisibility(View.VISIBLE);
            mTvwTime.setText(TimeHelper.caculateTimeFeed(mApplication, feedContent.getStamp(), mFeedBusiness
                    .getDeltaTimeServer()));
        } else {
            mTvwTime.setVisibility(View.GONE);
        }
        String countLike = Utilities.shortenLongNumber(feedContent.getCountLike());//    String.valueOf(feedModel
        // .getFeedContent().getCountLike());
        String countComment = Utilities.shortenLongNumber(feedContent.getCountComment());
        String countShare = Utilities.shortenLongNumber(feedContent.getCountShare());

        mTvwLike.setText(countLike);
        mTvwComment.setText(countComment);
        mTvwShare.setText(countShare);

        if (mFeedModel.getIsLike() == 1) {
            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like_press);
        } else {
            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like);
        }

        String titleLike = OnMediaHelper.getTitleLikeInHtml(feedContent.getCountLike(), listLike, mApplication,
                "#FFFFFF");
        mTvwPeopleLike.setText(TextHelper.fromHtml(titleLike));
    }

    private void setViewListener() {
        mViewPager.addOnPageChangeListener(onPageChangeListener);

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mImgOverFlow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else {
                    initBottomSheet();
                    ImageProfile img = listImage.get(currentPosition);
                    new BottomSheetMenu(OnMediaViewImageActivity.this, true)
                            .setListItem(mOverFlowItems)
                            .setListener(new BottomSheetListener() {
                                @Override
                                public void onItemClick(int itemId, Object entry) {
                                    switch (itemId) {
                                        case Constants.MENU.MENU_DELETE_IMAGE_ONMEDIA: {
                                            if (currentPosition > listImage.size() || listImage.get(currentPosition) ==
                                                    null) {
                                                return;
                                            }

                                            if (isOpenProfile && listImage.size() == 1) {
                                                showToast(R.string.must_keep_one_avatar);
                                                return;
                                            }
                                            String labelOK = getString(R.string.yes);
                                            String labelCancel = getString(R.string.no);
                                            String title = getString(R.string.remove_image_profile_title);
                                            String msg = getString(R.string.remove_image_profile_msg);
                                            PopupHelper.getInstance().showDialogConfirm
                                                    (OnMediaViewImageActivity.this,
                                                            title, msg, labelOK, labelCancel, OnMediaViewImageActivity.this,
                                                            null, Constants.MENU.ACCEPT_REMOVE_IMAGE_PROFILE);
                                            break;
                                        }

                                        case Constants.MENU.LIST_PEOPLE_SHARE: {
                                            Log.i(TAG, "onItemClick: list people share");
                                            NavigateActivityHelper.navigateToOnMediaLikeOrShare(OnMediaViewImageActivity.this, img.getIdServerString(), false);
                                            break;
                                        }
                                        default:
                                            break;
                                    }
                                }
                            }).show();
                }
            }
        });

        mPreviewImageProfileAdapter.setImageClickListener(new PreviewImageProfileAdapter.ImageClickListener() {
            @Override
            public void onClickImage() {
                Log.i(TAG, "isonclick");
                if (isHideStatus) {
                    showStatusBar();
                } else {
                    hideStatusBar(true);
                }
            }
        });

        mImgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "click like");
                onClickLikeFeed();
            }
        });

        mImgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "click commnt");
                onClickCommentFeed();
            }
        });

        mImgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    showDialogLogin();
                } else {
                    if (Utilities.notEmpty(listImage) && currentPosition >= 0 && currentPosition < listImage.size()) {
                        ImageProfile imageProfile = listImage.get(currentPosition);
                        if (imageProfile != null) {
                            if (Utilities.notEmpty(imageProfile.getImagePathLocal())) {
                                ReengMessage message = new ReengMessage();
                                message.setMessageType(ReengMessageConstant.MessageType.image);
                                message.setFilePath(imageProfile.getImagePathLocal());
                                //message.setDirectLinkMedia(imageProfile.getImageUrl());
                                //message.setFileId(imageProfile.getIdServerString());
                                message.setForwardingMessage(false);
                                ShareContentBusiness business = new ShareContentBusiness(OnMediaViewImageActivity.this, message);
                                business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_FROM_IMAGE_PREVIEW);
                                business.setTitleDialogChooseContact(getString(R.string.share));
                                business.showPopupShareContent();
                            } else {
                                downloadImageAndShare(imageProfile);
                            }
                        }
                    }
                }
            }
        });

        mTvwPeopleLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFeedContent == null || TextUtils.isEmpty(currentFeedContent.getUrl()))
                    return;
                NavigateActivityHelper.navigateToOnMediaLikeOrShare(OnMediaViewImageActivity.this, currentFeedContent
                        .getUrl(), true);
            }
        });

//        dragDismissLayout.addListener(new ElasticDragDismissCallback() {
//            @Override
//            public void onDrag(float elasticOffset, float elasticOffsetPixels, float rawOffset, float rawOffsetPixels) {
//                super.onDrag(elasticOffset, elasticOffsetPixels, rawOffset, rawOffsetPixels);
//            }
//
//            @Override
//            public void onDragDismissed() {
//                super.onDragDismissed();
//            }
//        });
//        if (Build.VERSION.SDK_INT >= 21) {
//            dragDismissLayout.addListener(new SystemChromeFader(this));
//        }

        mImgDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageProfile img = listImage.get(currentPosition);
                if (img == null || TextUtils.isEmpty(img.getImageUrl())) {
                    showToast(R.string.e601_error_but_undefined);
                    return;
                }
                if (!TextUtils.isEmpty(img.getImagePathLocal())) {
                    saveImageToGallery(img.getImagePathLocal());
                } else {
                    String imgUrl = listImage.get(currentPosition).getImageUrl();
                    if (!TextUtils.isEmpty(imgUrl)) {
                        ImageBusiness.downloadImagePreview(OnMediaViewImageActivity.this, imgUrl);
                    }
                }
            }
        });
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            mFeedModel = null;
            currentListUserLike = new ArrayList<>();
            currentPosition = position;
            String counter = (position + 1) + "/" + listImage.size() + " " + getResources().getString(R.string.message_image);
            mTvwNumberImage.setText(counter);
            setEnableImageAction(false);
            currentFeedContent = null;
            rest.getImageDetail(listImage.get(position).getIdServerString(), mMsisdn, itemType, new OnMediaInterfaceListener.GetListFeedContentListener() {

                @Override
                public void onGetListFeedContent(RestAllFeedContent restAllFeedContent) {
                    Log.i(TAG, "onResponse");
                    currentFeedContent = restAllFeedContent.getFeedContent();
                    if (currentFeedContent == null) return;
                    currentFeedContent.setItemType(itemType);
                    currentListUserLike = restAllFeedContent.getUserLikes();
                    getModelFeed(restAllFeedContent);
                    drawFeedContent(currentListUserLike);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.e(TAG, "VolleyError", volleyError);
                }
            });
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private void saveImageToGallery(String filePath) {
        File messageImageFile = new File(filePath);
        if (!messageImageFile.exists()) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        File recordDir = new File(Config.Storage.GALLERY_MOCHA);
        if (!recordDir.exists()) {
            recordDir.mkdirs();
        }
        String saveFileName = ImageHelper.IMAGE_NAME + "_" + System.currentTimeMillis() + Constants.FILE
                .JPEG_FILE_SUFFIX;
        File saveImage = new File(recordDir, saveFileName);
        if (saveImage.exists()) {
            showToast(R.string.file_exist);
        } else {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(messageImageFile);
                out = new FileOutputStream(saveImage);

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                showToast(R.string.sticker_download_done);
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                showToast(R.string.e601_error_but_undefined);
            } finally {
                try {
                    if (in != null) in.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                try {
                    if (out != null) out.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            FileHelper.refreshGallery(mApplication, saveImage);
        }
    }

    private void setEnableImageAction(boolean enable) {
        mImgLike.setEnabled(enable);
        mImgComment.setEnabled(enable);
        mImgShare.setEnabled(enable);
    }

    private void onClickCommentFeed() {
        if (mFeedModel == null || mFeedModel.getFeedContent() == null) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        FeedContent feedContent = mFeedModel.getFeedContent();
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_IMAGE);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feedContent.getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_CONTENT_DATA, feedContent);
        if (needAddToHashMap) {
            intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_HASHMAP);
        } else {
            intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, feedFrom);
        }
        startActivity(intent, true);
    }

    private void onClickLikeFeed() {
        if (mFeedModel == null || mFeedModel.getFeedContent() == null) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        FeedContent feedContent = mFeedModel.getFeedContent();
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }
        final boolean isLiked = mFeedModel.getIsLike() == 1;
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(!isLiked);
        mFeedBusiness.addUrlActionLike(feedContent.getUrl(), mFeedModel.getBase64RowId(), action, mFeedModel.getFeedContent());
    }

    private void setLikeFeed(boolean isLike) {
        FeedContent feedContent = mFeedModel.getFeedContent();
        //neu la like thi tang like them 1, neu la unlike thi tru 1.
        int delta;
        if (isLike) {
            delta = 1;
            mFeedModel.setIsLike(1);
            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like_press);
        } else {
            delta = -1;
            mFeedModel.setIsLike(0);
            mImgLike.setBackgroundResource(R.drawable.ic_onmedia_like);
        }
        long countLike = feedContent.getCountLike();
        feedContent.setCountLike(countLike + delta);

        if (isLike) {
            currentListUserLike.add(0, new UserInfo(mAccountBusiness.getJidNumber(), mAccountBusiness.getUserName()));
            if (currentListUserLike.size() > 2) {
                ArrayList<UserInfo> listTmp = new ArrayList<>();
                listTmp.add(0, currentListUserLike.get(0));
                listTmp.add(1, currentListUserLike.get(1));
                currentListUserLike = listTmp;
            }
        } else {
            if (currentListUserLike.size() == 0) {
                Log.i(TAG, "Loi roi, size phai khac 0");
            } else if (currentListUserLike.size() == 1) {   // unlike khi countlike = 1 thi la like cua minh
                currentListUserLike.clear();
            } else if (currentListUserLike.size() == 2) {
                int indexToDelete = -1;
                for (int i = 0; i < currentListUserLike.size(); i++) {
                    if (currentListUserLike.get(i).getMsisdn().equals(mAccountBusiness.getJidNumber())) {
                        indexToDelete = i;
                        break;
                    }
                }
                if (indexToDelete != -1) {
                    currentListUserLike.remove(indexToDelete);
                }
            } else {
                Log.i(TAG, "Loi roi, size phai < 3");
            }
        }
        String titleLike = OnMediaHelper.getTitleLikeInHtml(feedContent.getCountLike(), currentListUserLike,
                mApplication, "#FFFFFF");
        mTvwPeopleLike.setText(TextHelper.fromHtml(titleLike));
        mTvwLike.setText(Utilities.shortenLongNumber(feedContent.getCountLike()));
    }

    private void drawView() {
        mTvwName.setText(mName);
        String counter = (currentPosition + 1) + "/" + listImage.size() + " " + getResources().getString(R.string.message_image);
        mTvwNumberImage.setText(counter);

        mPreviewImageProfileAdapter = new PreviewImageProfileAdapter(mApplication, listImage);
        mViewPager.setAdapter(mPreviewImageProfileAdapter);
        mViewPager.setCurrentItem(currentPosition);
        if (feedFrom >= 0) {
            mPreviewImageProfileAdapter.setViewMyImage(false);
        } else {
            mPreviewImageProfileAdapter.setViewMyImage(isViewMyImageProfile);
        }
        //mImgOverFlow.setVisibility(isViewMyImageProfile ? View.VISIBLE : View.GONE);
        mImgOverFlow.setVisibility(showRemoveButton ? View.VISIBLE : View.GONE);
        mImgDownload.setVisibility(View.VISIBLE);
    }

    private void initBottomSheet() {
        mOverFlowItems.clear();
        ItemContextMenu listPeopleShare = new ItemContextMenu(mRes.getString(R.string.onmedia_title_user_share),
                R.drawable.ic_people_img, null, Constants.MENU.LIST_PEOPLE_SHARE);
        mOverFlowItems.add(listPeopleShare);
        if (showRemoveButton) {
            ItemContextMenu deleteImg = new ItemContextMenu(mRes.getString(R.string.delete),
                    R.drawable.ic_del_img, null, Constants.MENU.MENU_DELETE_IMAGE_ONMEDIA);
            mOverFlowItems.add(deleteImg);
        }
    }

    private void initBusiness() {
        mApplication = (ApplicationController) getApplication();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        imageProfileBusiness = mApplication.getImageProfileBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        mRes = getResources();
        rest = new WSOnMedia(mApplication);
        mHandler = new Handler();
        eventOnMediaHelper = new EventOnMediaHelper(this);

        animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mLayoutAb.setVisibility(View.VISIBLE);
                mLayoutLikeShare.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutAb.setVisibility(View.GONE);
                mLayoutLikeShare.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void setViewComponent() {
        mViewPager = findViewById(R.id.viewpager_preview_image);

        mLayoutAb = findViewById(R.id.layout_ab_view_img);
        mLayoutLikeShare = findViewById(R.id.layout_view_image_like_share);

        mImgBack = findViewById(R.id.img_ab_back);
//        mImgRemove = (ImageView) findViewById(R.id.img_ab_remove);
        mImgDownload = findViewById(R.id.img_ab_download);
        mImgOverFlow = findViewById(R.id.img_ab_overflow);

        mTvwName = findViewById(R.id.tvw_ab_title);
        mTvwNumberImage = findViewById(R.id.tvw_ab_number_image);
        mTvwTime = findViewById(R.id.tvw_ab_time);
        mTvwTime.setVisibility(View.GONE);

        mTvwPeopleLike = findViewById(R.id.tvw_title_like);
        mTvwLike = findViewById(R.id.tvw_number_like);
        mTvwComment = findViewById(R.id.tvw_number_comment);
        mTvwShare = findViewById(R.id.tvw_number_share);
        mImgLike = findViewById(R.id.img_like_feed);
        mImgComment = findViewById(R.id.img_comment_feed);
        mImgShare = findViewById(R.id.img_share_feed);

//        dragDismissLayout = findViewById(R.id.drag_dissmiss_layout);

//        mLayoutLikeShare.setVisibility(View.GONE);
        mTvwTime.setVisibility(View.GONE);

        /*RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLayoutAb.getLayoutParams();
        params.topMargin = mApplication.getStatusBarHeight();
        mLayoutAb.setLayoutParams(params);*/
    }

    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt(Constants.ONMEDIA.PARAM_IMAGE.POSITION);
            mName = savedInstanceState.getString(Constants.ONMEDIA.PARAM_IMAGE.NAME);
            mMsisdn = savedInstanceState.getString(Constants.ONMEDIA.PARAM_IMAGE.MSISDN);
            listImage = (ArrayList<ImageProfile>) savedInstanceState.getSerializable(Constants.ONMEDIA.PARAM_IMAGE
                    .LIST_IMAGE);
            itemType = savedInstanceState.getString(Constants.ONMEDIA.PARAM_IMAGE.ITEM_TYPE);
            feedFrom = savedInstanceState.getInt(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM);
            /*if (mMsisdn.equals(mAccountBusiness.getJidNumber())) {
                listImage = imageProfileBusiness.getImageProfileList();
            } else {
                listImage = (ArrayList<ImageProfile>) savedInstanceState.getSerializable(Constants.ONMEDIA
                .PARAM_IMAGE.LIST_IMAGE);
            }*/
        } else {
            currentPosition = getIntent().getIntExtra(Constants.ONMEDIA.PARAM_IMAGE.POSITION, 0);
            mName = getIntent().getStringExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME);
            mMsisdn = getIntent().getStringExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN);
            listImage = (ArrayList<ImageProfile>) getIntent().getSerializableExtra(Constants.ONMEDIA.PARAM_IMAGE
                    .LIST_IMAGE);
            itemType = getIntent().getStringExtra(Constants.ONMEDIA.PARAM_IMAGE.ITEM_TYPE);
            feedFrom = getIntent().getIntExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, 0);
            isOpenProfile = getIntent().getBooleanExtra(Constants.ONMEDIA.PARAM_IMAGE.IS_OPEN_PROFILE, false);
            if (itemType.equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR) && isOpenProfile) {
                getAvatars(mMsisdn);
            }
            /*if (mMsisdn.equals(mAccountBusiness.getJidNumber())) {
                listImage = imageProfileBusiness.getImageProfileList();
            } else {
                listImage = (ArrayList<ImageProfile>) getIntent().getSerializableExtra(Constants.ONMEDIA.PARAM_IMAGE
                .LIST_IMAGE);
            }*/
        }
        if (listImage == null) {
            listImage = imageProfileBusiness.getImageProfileList();
            showRemoveButton = true;
        }
        if (mMsisdn.equals(mAccountBusiness.getJidNumber())) {
            isViewMyImageProfile = true;
            if (listImage.get(0).getTypeImage() == ImageProfileConstant.IMAGE_COVER && !isOpenProfile) {
                showRemoveButton = false;
            } else if (listImage.get(0).getTypeImage() == ImageProfileConstant.IMAGE_AVATAR)
                showRemoveButton = true;
        }

        if (listImage.size() > 1) {
            needAddToHashMap = true;
        }
        Log.i(TAG, "name: " + mName + " | jid: " + mMsisdn + " | pos: " + currentPosition +
                " | itemType: " + itemType + " | showRemove: " + showRemoveButton + " | isViewMyProfile: " +
                isViewMyImageProfile);
    }

    /**
     * thực hiện lấy danh sách avatar
     *
     * @param contact số điện thoai của người muốn lấy
     */
    private void getAvatars(String contact) {
        UserApi userApi = mApplication.getApplicationComponent().provideUserApi();
        userApi.getAvatars(compositeDisposable, contact, this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.ONMEDIA.PARAM_IMAGE.POSITION, currentPosition);
        outState.putString(Constants.ONMEDIA.PARAM_IMAGE.NAME, mName);
        outState.putString(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, mMsisdn);
        outState.putSerializable(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, listImage);
        outState.putString(Constants.ONMEDIA.PARAM_IMAGE.ITEM_TYPE, itemType);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDataReady() {
        if (mHandler != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    hideLoadingDialog();
                    initData();
                }
            });
        }
    }

    private void initData() {

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.ACCEPT_REMOVE_IMAGE_PROFILE:
                if (isOpenProfile) {
                    ImageProfile imgProfile = listImage.get(currentPosition);
                    if (imgProfile != null && imgProfile.getTime() != 0) {
                        UserApi userApi = mApplication.getApplicationComponent().provideUserApi();
                        userApi.removeAvatar(compositeDisposable, String.valueOf(imgProfile.getTime()), this);
                    } else
                        onRemoveFail("");
                } else {
                    ImageProfile imageProfile = listImage.get(currentPosition);
                    removeImageProfile(imageProfile, currentPosition);
                }

                break;
            case Constants.MENU.MENU_SHARE_LINK:
                FeedModelOnMedia feed = (FeedModelOnMedia) entry;
//                handleShareLink(feed);
                handleShareNow(feed);
//                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share_now);
                break;

            case Constants.MENU.MENU_WRITE_STATUS:
                FeedModelOnMedia feedWrite = (FeedModelOnMedia) entry;
                handleWriteStatus(feedWrite);
//                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_write_status);
                break;
            case Constants.MENU.MENU_LIST_SHARE:
                FeedModelOnMedia feedListShare = (FeedModelOnMedia) entry;
                if (!TextUtils.isEmpty(feedListShare.getFeedContent().getUrl())) {
                    NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, feedListShare.getFeedContent().getUrl()
                            , false);
                    /*Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feedListShare.getFeedContent().getUrl());
                    intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_SHARE);
                    startActivity(intent, true);*/
                }
//                mParentActivity.trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_share);
                break;
        }
    }

    private void removeImageProfile(final ImageProfile imageProfile, final int index) {
        if (imageProfile == null) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(getString(R.string.error_internet_disconnect),
                    Toast.LENGTH_SHORT);
            return;
        }

        showLoadingDialog("", R.string.processing);
        ProfileRequestHelper.onResponseRemoveImageProfileListener listener =
                new ProfileRequestHelper.onResponseRemoveImageProfileListener() {
                    @Override
                    public void onResponse() {
                        showToast(R.string.remove_image_profile_response);
                        hideLoadingDialog();
                        imageProfileBusiness.deleteImageProfile(imageProfile, index);
                        listImage = imageProfileBusiness.getImageProfileList();
                        //                        listImage.remove(index);
                        if (listImage.size() <= 0) {
                            onBackPressed();
                            return;
                        }
                        if (currentPosition >= listImage.size() - 1 && currentPosition > 0)
                            currentPosition -= 1;

                        mPreviewImageProfileAdapter.setListImage(listImage);
                        mPreviewImageProfileAdapter.notifyDataSetChanged();
                        mViewPager.setAdapter(mPreviewImageProfileAdapter);
                        mViewPager.setCurrentItem(currentPosition);
                        String counter = (currentPosition + 1) + "/" + listImage.size() + " " + getResources().getString(R.string.message_image);
                        mTvwNumberImage.setText(counter);
                    }

                    @Override
                    public void onError(int errorCode) {
                        hideLoadingDialog();
                        Log.d(TAG, "onError: " + errorCode);
                        showToast(getString(R.string.e490_illegal_state_exception), Toast.LENGTH_SHORT);
                    }
                };

//        ProfileRequestHelper.getInstance(mApplication).removeImageProfile(account, imageProfile, listener);
        UserApi userApi = mApplication.getApplicationComponent().provideUserApi();
        userApi.removeImageAlbum(compositeDisposable, imageProfile, listener);
    }

    private void handleShareNow(final FeedModelOnMedia feed) {
        if (feed.getIsShare() == 1) {
            showToast(R.string.onmedia_already_shared);
            return;
        }
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }
        final String url = feed.getFeedContent().getUrl();
        rest.logAppV6(url, "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE, "", feed.getBase64RowId(), "", null,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "handleShareNow: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    showToast(R.string.onmedia_share_success);
                                    if (mFeedModel != null) {
                                        mFeedModel.setIsShare(1);
                                        long countShare = mFeedModel.getFeedContent().getCountShare();
                                        mFeedModel.getFeedContent().setCountShare(countShare + 1);
                                        drawFeedContent(currentListUserLike);
                                    }
                                } else {
                                    showToast(R.string.e601_error_but_undefined);
                                }
                            } else {
                                showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                            showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void handleWriteStatus(FeedModelOnMedia feedWrite) {
        eventOnMediaHelper.navigateToPostOnMedia(this,
                feedWrite.getFeedContent(), "", feedWrite.getBase64RowId(), false,
                FeedModelOnMedia.ActionFrom.onmedia);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void hideStatusBar(boolean animation) {
        Log.i(TAG, "hide");
        isHideStatus = true;
        /*if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
        /*if (!isLoadingFeedContent) {

        }*/
        if (animation) {
            mLayoutAb.clearAnimation();
            mLayoutLikeShare.clearAnimation();
            mLayoutAb.startAnimation(animFadeOut);
            mLayoutLikeShare.startAnimation(animFadeOut);
        } else {
            mLayoutAb.setVisibility(View.GONE);
            mLayoutLikeShare.setVisibility(View.GONE);
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showStatusBar() {
        Log.i(TAG, "show");
        isHideStatus = false;
        /*if (Build.VERSION.SDK_INT < 16) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // show the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
        /*if (!isLoadingFeedContent) {

        }*/
        mLayoutAb.clearAnimation();
        mLayoutLikeShare.clearAnimation();
        mLayoutAb.startAnimation(animFadeIn);
        mLayoutLikeShare.startAnimation(animFadeIn);
        /*if (mHandler == null) {
            mHandler = new Handler();
        } else {
            mHandler.removeCallbacks(runnable);
        }
        mHandler.postDelayed(runnable, TIME_DELAY_SHOW);*/
    }

    public void notifyNewFeed(final boolean needToSetSelection) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "notify nowwwwwwwwwwwwww");
                drawFeedContent(currentListUserLike);
            }
        });

    }

    @Override
    public void notifyFeedOnMedia(boolean needNotify, boolean needToSetSelection) {
        Log.i(TAG, "notifyFeedOnMedia: " + needNotify + " selection: " + needToSetSelection);
        notifyNewFeed(needToSetSelection);
    }

    @Override
    public void onPostFeed(FeedModelOnMedia feed) {

    }

    @Override
    public void onGetAvatarSuccess(ArrayList<ImageProfile> list) {
        if (mApplication == null || list == null || list.size() <= 1) {
            return;
        }
        listImage.clear();
        listImage.addAll(list);
        for (ImageProfile imageProfile : listImage) {
            imageProfile.setImageUrl(UrlConfigHelper.getInstance(mApplication).getDomainImage() + imageProfile.getImageUrl());
        }
        mPreviewImageProfileAdapter.setListImage(listImage);
        mPreviewImageProfileAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPreviewImageProfileAdapter);
        mViewPager.setCurrentItem(0);
        if (onPageChangeListener != null)
            onPageChangeListener.onPageSelected(mViewPager.getCurrentItem());
        String counter = 1 + "/" + listImage.size() + " " + getResources().getString(R.string.message_image);
        mTvwNumberImage.setText(counter);
    }

    @Override
    public void onGetAvatarError(String s) {
    }

    @Override
    public void onGetAvatarCompleted() {
    }

    @Override
    public void onRemoveSuccess(String lAvatar) {
        hideLoadingDialog();
        int posRemove = currentPosition;
        showToast(R.string.remove_image_profile_response);
        if (listImage.size() <= 0) {
            onBackPressed();
            return;
        }
        if (currentPosition >= listImage.size() - 1 && currentPosition > 0)
            currentPosition -= 1;
        listImage.remove(posRemove);

        mPreviewImageProfileAdapter.setListImage(listImage);
        mPreviewImageProfileAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPreviewImageProfileAdapter);
        mViewPager.setCurrentItem(currentPosition);
        String counter = (currentPosition + 1) + "/" + listImage.size() + " " + getResources().getString(R.string.message_image);
        mTvwNumberImage.setText(counter);
        if (posRemove == 0) {
            ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
            String currentAvatarPath = reengAccount.getAvatarPath();
            FileHelper.deleteFile(mApplication, currentAvatarPath);
//            mApplication.getUniversalImageLoader().clearDiskCache(currentAvatarPath);
            reengAccount.setLastChangeAvatar(lAvatar);
            reengAccount.setAvatarPath("");
            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
            saveNewAvatar(lAvatar);
        }

    }

    private void saveNewAvatar(final String lAvatar) {

        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();

        int size = (int) mRes.getDimension(R.dimen.avatar_home_menu_size);
        String userNumber = reengAccount.getJidNumber();
        String lastChange = reengAccount.getLastChangeAvatar();
        String urlAvatar = mApplication.getAvatarBusiness().getAvatarUrl(lastChange, userNumber, size, reengAccount.getAvatarVerify());
        Log.i(TAG, "urlAvatar: " + urlAvatar);

        Glide.with(mApplication)
                .asBitmap()
                .load(urlAvatar)
                .into(new SimpleTarget<Bitmap>(size, size) {
                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        Log.e(TAG, "loadBitmapBigPicture fail");
                    }

                    @Override
                    public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                        Log.i(TAG, "onResourceReady");
                        if (bitmap != null) {
                            mApplication.getAvatarBusiness().saveMyAvatar(bitmap, lAvatar, true);
                        }
                    }
                });
    }

    @Override
    public void onRemoveFail(String msg) {
        hideLoadingDialog();
        showToast(R.string.e601_error_but_undefined);
    }

    public void downloadImageAndShare(final ImageProfile imageProfile) {
        final ApplicationController application = ApplicationController.self();
        if (application.isDataReady()) {
            showLoadingDialog("", R.string.processing);
            try {
                Glide.with(application).asBitmap().load(imageProfile.getImageUrl()).into(new SimpleTarget<Bitmap>() {

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        hideLoadingDialog();
                        showToast(R.string.request_send_error);
                        Log.d(TAG, "download Image onLoadFailed");
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (application == null) return;
                        File recordDir = new File(Config.Storage.GALLERY_MOCHA);
                        if (!recordDir.exists()) {
                            recordDir.mkdirs();
                        }
                        String imagePath = application.getExternalCacheDir() + "/cache_img_" + System.currentTimeMillis() + ".jpg";
                        File file = ImageHelper.saveBitmapToFile(resource, imagePath, Bitmap.CompressFormat.JPEG);
                        hideLoadingDialog();
                        if (file != null && file.isFile()) {
                            ReengMessage message = new ReengMessage();
                            message.setMessageType(ReengMessageConstant.MessageType.image);
                            message.setFilePath(imagePath);
                            message.setForwardingMessage(false);
                            ShareContentBusiness business = new ShareContentBusiness(OnMediaViewImageActivity.this, message);
                            business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_FROM_IMAGE_PREVIEW);
                            business.setTitleDialogChooseContact(getString(R.string.share));
                            business.showPopupShareContent();
                        } else {
                            showToast(R.string.request_send_error);
                        }
                        Log.d(TAG, "download Image onResourceReady imagePath: " + imagePath);
                    }
                });
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                if (resultCode == RESULT_OK && intent != null) {
                    int threadId = intent.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                }
                break;

            default:
                break;
        }
    }
}


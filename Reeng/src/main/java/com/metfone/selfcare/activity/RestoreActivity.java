package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RestoreActivity extends BaseSlidingFragmentActivity implements View.OnClickListener, DBImporter.RestoreProgressListener {
    private static final String TAG = RestoreActivity.class.getSimpleName();
    private TextView mBtnSkip, mBtnRestore, mBtnRestoreContinue;
    View mRestoreBtnLayout, mRestoreProgressLayout;
    private ImageView mRestoreBackupImg;
    private ApplicationController mApplication;
    long mFileSize = 0;
    String mUrlFilePath = "";
    long mFileDate = 0l;
    String mFileId = "";
    private static final String SDF_IN_DAY = "HH:mm";
    private static final String SDF_IN_YEAR = "dd/MM";
    private static final String SDF_OTH_YEAR = "dd/MM/yyyy";

    private static final float ONE_KB = 1024f;
    private static final float ONE_MB = 1024f * 1024f;

    private TextView mTvRestoreDescription, mTvRestoreBackupLastTime, mTvRestoreSize, mTvRestoreProgress, mTvRestoreBackup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplication();
        setContentView(R.layout.activity_restore);
        mRestoreBackupImg = (ImageView) findViewById(R.id.img_restore_backup);
        mBtnSkip = findViewById(R.id.btn_restore_skip);
        mBtnRestore = findViewById(R.id.btn_restore);
        mBtnRestoreContinue = findViewById(R.id.btn_restore_continue);
        mTvRestoreDescription = findViewById(R.id.tv_restore_description);
        mTvRestoreBackupLastTime = findViewById(R.id.tv_restore_last_time);
        mTvRestoreSize = findViewById(R.id.tv_restore_size);
        mTvRestoreProgress = findViewById(R.id.tv_restore_progress);
        mTvRestoreBackup = findViewById(R.id.tv_restore_backup);
        mRestoreBtnLayout = findViewById(R.id.restore_button_layout);
        mRestoreProgressLayout = findViewById(R.id.restore_progress_layout);

        mBtnSkip.setOnClickListener(this);
        mBtnRestore.setOnClickListener(this);
        mBtnRestoreContinue.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent == null) {
            doContinue(true);
            return;
        }

        mFileSize = intent.getLongExtra("backup_size", 0l);
        mUrlFilePath = intent.getStringExtra("backup_url_path");
        mFileDate = intent.getLongExtra("backup_time", System.currentTimeMillis());
        mFileId = intent.getStringExtra("backup_id_file");

        updateFileSizeView(mFileSize);
        updateFileDateView();
        trackingScreen(TAG);
    }

    @Override
    public void onClick(View view) {
        if (view == null) return;

        switch (view.getId()) {
            case R.id.btn_restore_skip:
                doSkip();
                break;

            case R.id.btn_restore:
                if (NetworkHelper.isConnectInternet(this)) {
                    RestoreManager.restoreMessages(this, UrlConfigHelper.getInstance(mApplication).getDomainFile() + "/" + mUrlFilePath);
                } else {
                    Toast.makeText(ApplicationController.self(), getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.btn_restore_continue:
                doContinue(false);
                break;
        }
    }

    void doContinue(boolean isSkip) {
        /*//backupTodo: need request to server to delete backup file
        if (isSkip) {
            RestoreManager.deleteBackupFileOnServer(mFileId);
        }*/
        setPassedRestorePhase();
        RestoreManager.setRestoring(false);
//        mApplication.connectByToken();
        mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
        ProfileRequestHelper.getInstance(mApplication).getUserInfo(
                mApplication.getReengAccountBusiness().getCurrentAccount(), new ProfileRequestHelper.onResponseUserInfoListener() {
                    @Override
                    public void onResponse(ReengAccount account) {
                        Log.i(TAG, "name: " + account.getName());
                        if (TextUtils.isEmpty(account.getName()))
                            goToPersonalInfo();
                        else
                            goToHome();

                    }

                    @Override
                    public void onError(int errorCode) {
                        goToPersonalInfo();
                    }
                });
    }

    private void goToPersonalInfo() {
        Intent i = new Intent(this, LoginActivity.class);
        /*i.putExtra("is_edit", true);
        i.putExtra(LoginActivity.class.getSimpleName(), true);*/
        startActivity(i);
        finish();
        mApplication.getXmppManager().sendAvailable();
    }

    void setPassedRestorePhase() {
        try {
            SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
            if (pref != null) {
                pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                Log.i(TAG, "setPassedRestorePhase");
            }
        } catch (Exception e) {
        }
    }

    void doRestore() {
        if (!NetworkHelper.isConnectInternet(this)) {
            Toast.makeText(ApplicationController.self(), getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
            return;
        }

        //do something here
        handleRestoreStart();
    }

    void handleRestoreStart() {
        //update view for restore progress
        mRestoreBackupImg.setImageResource(R.drawable.ic_backup_restore);
        mTvRestoreBackup.setText(getString(R.string.bk_restore_message));
        mTvRestoreDescription.setText(getString(R.string.bk_restore_description));
        mRestoreBtnLayout.setVisibility(View.GONE);
        mRestoreProgressLayout.setVisibility(View.VISIBLE);
    }

    void handleRestoreFail() {
        mTvRestoreBackup.setText(getString(R.string.bk_restore_fail));
        mTvRestoreDescription.setText(getString(R.string.bk_restore_fail_des));
        mRestoreProgressLayout.setVisibility(View.GONE);
        mRestoreBtnLayout.setVisibility(View.VISIBLE);
        mBtnRestoreContinue.setVisibility(View.GONE);
        mBtnRestore.setText(getString(R.string.retry));
    }

    void handleRestoreSuccessfully(int messageCount, int threadMessageCount) {
        setPassedRestorePhase();
//        mApplication.recreateBusiness();
        Log.i(TAG, "handleRestoreSuccessfully");
        mTvRestoreBackup.setText(getString(R.string.bk_restored_successfully));
        if (messageCount == 1) {
            if (threadMessageCount == 1) {
                mTvRestoreDescription.setText(getString(R.string.bk_restore_result_11));
            }
        } else {
            if (threadMessageCount == 1) {
                mTvRestoreDescription.setText(getString(R.string.bk_restore_result_21, messageCount));
            } else {
                mTvRestoreDescription.setText(getString(R.string.bk_restore_result_22, messageCount, threadMessageCount));
            }
        }
        mRestoreProgressLayout.setVisibility(View.GONE);
        mRestoreBtnLayout.setVisibility(View.GONE);
        mBtnRestoreContinue.setVisibility(View.VISIBLE);
        RestoreManager.setRestoring(false);
        mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
        mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
        mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
    }

    void updateFileSizeView(long size) {
        if (mTvRestoreSize != null) {
            mTvRestoreSize.setText(getString(R.string.bk_file_size) + " : " + getFileSize(size));
        }
    }

    void updateFileDateView() {
        try {
            if (mTvRestoreBackupLastTime != null) {
                if (mFileDate == 0) mFileDate = System.currentTimeMillis();
                mTvRestoreBackupLastTime.setText("" + formatCommonTime(mFileDate, System.currentTimeMillis(), getResources()));
            }
        } catch (Exception e) {
            Log.e(TAG, "updateFileDateView ", e);
        }
    }

    public static String formatCommonTime(long mTime, long currentTime, Resources res) {
        SimpleDateFormat inDay = new SimpleDateFormat(SDF_IN_DAY);
        SimpleDateFormat inYear = new SimpleDateFormat(SDF_IN_YEAR);
        SimpleDateFormat othYear = new SimpleDateFormat(SDF_OTH_YEAR);
        Calendar currentCal = Calendar.getInstance();
        currentCal.setTimeInMillis(currentTime);
        Calendar lastSeenCal = Calendar.getInstance();
        lastSeenCal.setTimeInMillis(mTime);
        int currentDay = currentCal.get(Calendar.DAY_OF_YEAR);
        int lastSeenDay = lastSeenCal.get(Calendar.DAY_OF_YEAR);
        int currentMonth = currentCal.get(Calendar.MONTH);
        int lastSeenMonth = lastSeenCal.get(Calendar.MONTH);
        int currentYear = currentCal.get(Calendar.YEAR);
        int lastSeenYear = lastSeenCal.get(Calendar.YEAR);

        if (lastSeenYear == currentYear) {
            if (lastSeenMonth == currentMonth) {
                if (lastSeenDay == currentDay) {
                    return res.getString(R.string.today) + ", " + inDay.format(mTime);
                } else {
                    int dayOfYear = currentDay - lastSeenDay;
                    if (dayOfYear == 1) {// Hom qua
                        return res.getString(R.string.yesterday) + ", " + inDay.format(mTime);
                    } else {
                        return inYear.format(mTime) + ", " + inDay.format(mTime);
                    }
                }
            } else {
                return inYear.format(mTime) + ", " + inDay.format(mTime);
            }
        } else {
            return othYear.format(mTime) + ", " + inDay.format(mTime);
        }
    }

    private String getFileSize(long size) {
        String fileSize = "";
        if (size > ONE_MB) {
            fileSize = String.format("%.2f MB", (size / ONE_MB));
        } else if (size > ONE_KB) {
            fileSize = String.format("%.2f KB", (size / ONE_KB));
        } else {
            fileSize = size + " B";
        }
        return fileSize;
    }

    void doSkip() {
        showSkipDialog();
    }

    private void showSkipDialog() {
        DialogConfirm dialogConfirm = new DialogConfirm(this, true);
        dialogConfirm.setLabel(getString(R.string.bk_skip_restoring));
        dialogConfirm.setMessage(getString(R.string.bk_skip_restore_confirmation));
        dialogConfirm.setNegativeLabel(getString(R.string.bk_skip_restoring));
        dialogConfirm.setPositiveLabel(getString(R.string.cancel));
        dialogConfirm.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                doContinue(true);
                //delete file on server
            }
        });
        dialogConfirm.show();
    }

    @Override
    public void onBackPressed() {
        if (!isRunning()) {
            super.onBackPressed();
        }
    }

    boolean isRunning() {
        return RestoreManager.isRunning();
    }

    @Override
    public void onStartDownload() {
        handleRestoreStart();
    }

    @Override
    public void onDownloadProgress(int percent) {
        if (mTvRestoreProgress != null) {
            if (percent > 100) percent = 100;
            mTvRestoreProgress.setText(getString(R.string.bk_downloading_data) + " (" + percent + "%)");
        }
    }

    @Override
    public void onDownloadComplete() {
    }

    @Override
    public void onDowloadFail(String message) {
        handleRestoreFail();
    }

    @Override
    public void onStartRestore() {
        if (mTvRestoreProgress != null) {
            mTvRestoreProgress.setText(getString(R.string.bk_restoring_wait));
        }
    }

    @Override
    public void onRestoreProgress(int percent) {
        if (mTvRestoreProgress != null) {
            if (percent > 100) percent = 100;
            mTvRestoreProgress.setText(getString(R.string.bk_restoring_wait) + " (" + percent + "%)");

        }
    }

    @Override
    public void onRestoreComplete(int messageCount, int threadCount) {
        handleRestoreSuccessfully(messageCount, threadCount);

    }

    @Override
    public void onRestoreFail(String message) {
        handleRestoreFail();
        //Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        RestoreManager.cancelRestore();
        super.onDestroy();
    }
}

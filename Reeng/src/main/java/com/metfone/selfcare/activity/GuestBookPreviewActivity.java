package com.metfone.selfcare.activity;

import android.os.Bundle;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.guestbook.GuestBookPreviewFragment;

/**
 * Created by toanvk2 on 4/26/2017.
 */
public class GuestBookPreviewActivity extends BaseSlidingFragmentActivity {
    private ApplicationController mApplication;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplicationContext();
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        setToolBar(findViewById(R.id.tool_bar));
        getDataAndDisplayFragment(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        GuestBookPreviewFragment previewFragment = GuestBookPreviewFragment.newInstance();
        executeFragmentTransaction(previewFragment, R.id.fragment_container, false, false);
    }


}

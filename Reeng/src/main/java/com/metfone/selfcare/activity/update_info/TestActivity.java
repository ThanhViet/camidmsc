package com.metfone.selfcare.activity.update_info;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.ActivityTestBinding;
import com.metfone.selfcare.helper.NavigateActivityHelper;

public class TestActivity extends BaseSlidingFragmentActivity {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityTestBinding binding = ActivityTestBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.fabTest.setOnClickListener(v -> {
            NavigateActivityHelper.navigateToUpdateInfo(this, createUserIdentity());
        });
    }

    private UserIdentityInfo createUserIdentity() {
        UserIdentityInfo userIdentityInfo = new UserIdentityInfo();
        userIdentityInfo.setIdType("7");
        userIdentityInfo.setName("Khoai lang");
        userIdentityInfo.setBirthDate("27/05/1991");
        userIdentityInfo.setSex("M");

        userIdentityInfo.setIdIssueDate("11/06/2006");
        userIdentityInfo.setIdExpireDate("27/05/2031");
        userIdentityInfo.setExpireVisa("27/05/2041");
        userIdentityInfo.setTelFax("0978334388");
        userIdentityInfo.setProvince("PNP");
        userIdentityInfo.setDistrict("001");
        userIdentityInfo.setHome("1A");
        userIdentityInfo.setStreetName("Tan Vien");
        userIdentityInfo.setAddress("Toa nha PN");
        userIdentityInfo.setIdNo("194333678");
        userIdentityInfo.setSubName("Hoa");
        userIdentityInfo.setSubGender("F");
        userIdentityInfo.setRelationship("P");
        userIdentityInfo.setSubDateBirth("15/11/1956");
        userIdentityInfo.setAddedDate("11/03/2020");
        userIdentityInfo.setNationality("VietNam");
        userIdentityInfo.setCorrectCus("true");
        userIdentityInfo.setIsScan("false");
        return userIdentityInfo;
//
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");
//        userIdentityInfo.setIdType("1");


    }
}

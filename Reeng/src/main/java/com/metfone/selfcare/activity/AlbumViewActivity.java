package com.metfone.selfcare.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.fragment.browser.AlbumPreviewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by tungt on 3/22/2016.
 */
public class AlbumViewActivity extends BaseSlidingFragmentActivity {
    private static String TAG = AlbumViewActivity.class.getSimpleName();
    public static final int REQUEST_PREVIEW_IMAGE = 0x1;

    private String mThreadName;
    private String mPhoneNumber = "";
//    private ApplicationController mApplication;

    private ArrayList<ImageProfile> mListImage;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, " onCreate ... ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
//        mApplication = (ApplicationController) getApplication();
        trackingScreen(TAG);
        // action bar
        findComponent();
        // get param
        getParam(savedInstanceState);
        // display fragment
        displayFragment();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.ONMEDIA.PARAM_IMAGE.NAME, mThreadName);
        outState.putString(Constants.ONMEDIA.PARAM_IMAGE.MSISDN, mPhoneNumber);
        outState.putSerializable(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE, mListImage);
    }

    private void getParam(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mThreadName = savedInstanceState.getString(Constants.ONMEDIA.PARAM_IMAGE.NAME);
            mPhoneNumber = savedInstanceState.getString(Constants.ONMEDIA.PARAM_IMAGE.MSISDN);
            mListImage = (ArrayList<ImageProfile>)
                    savedInstanceState.getSerializable(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE);
        } else {
            mThreadName = getIntent().getStringExtra(Constants.ONMEDIA.PARAM_IMAGE.NAME);
            mPhoneNumber = getIntent().getStringExtra(Constants.ONMEDIA.PARAM_IMAGE.MSISDN);
            mListImage = (ArrayList<ImageProfile>)
                    getIntent().getSerializableExtra(Constants.ONMEDIA.PARAM_IMAGE.LIST_IMAGE);
        }
    }

    private void findComponent() {
        LayoutInflater mLayoutInflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        View abView = findViewById(R.id.tool_bar);
        setToolBar(abView);
        setCustomViewToolBar(mLayoutInflater.inflate(R.layout.ab_detail, null));

        ImageView moreBtn = (ImageView) abView.findViewById(R.id.ab_more_btn);
        moreBtn.setVisibility(View.GONE);

        ImageView mImgBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void displayFragment() {
        AlbumPreviewFragment fragment = AlbumPreviewFragment.newInstance(mThreadName, mPhoneNumber, mListImage);
        executeFragmentTransaction(fragment, R.id.fragment_container, false, true);
    }
}

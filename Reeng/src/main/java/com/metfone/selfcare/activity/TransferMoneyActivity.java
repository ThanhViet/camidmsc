package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.fragment.transfermoney.MoneyConfirmFragment;
import com.metfone.selfcare.fragment.transfermoney.TransferMoneyFragment;
import com.metfone.selfcare.util.Log;


public class TransferMoneyActivity extends BaseSlidingFragmentActivity implements
        TransferMoneyFragment.OnFragmentInteractionListener {
    private static final String TAG = TransferMoneyActivity.class.getSimpleName();
    private View mViewActionBar;
    private String friendNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            friendNumber = bundle.getString(NumberConstant.NUMBER);
        }
        findComponentViews();
        setComponentViews();
        displayTransferMoneyFragment(friendNumber);
        trackingScreen(TAG);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            friendNumber = intent.getStringExtra(NumberConstant.NUMBER);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onDestroy: ");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    private void findComponentViews() {
//        mViewActionBar = getLayoutInflater().inflate(R.layout.ab_detail, null);
        mViewActionBar = findViewById(R.id.tool_bar);
        setToolBar(mViewActionBar);
        setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_detail, null));
    }

    private void setComponentViews() {
    }

    public View getmViewActionBar() {
        return mViewActionBar;
    }

    private void displayTransferMoneyFragment(String friendPhoneNumber) {
        // Create an instance of newInstance
        TransferMoneyFragment statusMessageFragment = TransferMoneyFragment.newInstance(friendPhoneNumber);
        executeFragmentTransaction(statusMessageFragment, R.id.fragment_container, false, false);
    }

    @Override
    public void navigateToMoneyConfirmFragment(String requestId, String friendPhoneNumber,
                                               long moneyAmount, String moneyAmountFormat) {
        MoneyConfirmFragment moneyConfirmFragment = MoneyConfirmFragment.
                newInstance(requestId, friendPhoneNumber, moneyAmount, moneyAmountFormat);
        executeFragmentTransaction(moneyConfirmFragment, R.id.fragment_container, false, true);
    }
}
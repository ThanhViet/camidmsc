package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ReengMessageWrapper;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.contact.AllMessageAndContactFragment;
import com.metfone.selfcare.fragment.contact.ChooseMultiNumberFragment;
import com.metfone.selfcare.fragment.contact.ChooseSingleNumberFragment;
import com.metfone.selfcare.fragment.contact.CreateChatFragment;
import com.metfone.selfcare.fragment.home.tabmobile.AddFriendFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;

//import com.metfone.selfcare.database.model.contact.PhoneNumber;

/**
 * Created by toanvk2 on 7/3/14.
 */
public class ChooseContactActivity extends BaseSlidingFragmentActivity implements
        ChooseMultiNumberFragment.OnFragmentInteractionListener,
        ChooseSingleNumberFragment.OnFragmentInteractionListener,
        AllMessageAndContactFragment.OnFragmentInteractionListener,
        CreateChatFragment.OnFragmentInteractionListener,
        InitDataListener {
    private final String TAG = ChooseContactActivity.class.getSimpleName();
    private View mToolBarView;
    private int mType, mThreadId, mThreadType, mTypeScreen = -1;
    private String guestBookId;
    private String packageIdAVNO;
    private int guestBookPageId;
    private ArrayList<String> mListMembers;
    private ReengMessage forwardingMessage;
    private MediaModel songModel;
    private String roomId;
    private String deepLinkCampaignId;
    private ReengMessageWrapper arrayMessage;
    private ApplicationController mApplicationController;
    private Bundle savedInstanceState;
    private String msg;
    boolean autoForwardMsg;

    private boolean isAllowContactPermission;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_search_box, null));
        mApplicationController = (ApplicationController) getApplicationContext();
        setWhiteStatusBar();
        Log.i(TAG, "intent: " + ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)) + " data: " + mApplicationController.isDataReady());
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0) {
            Log.i(TAG, "gotohome");
            goToHome();
        } else if (!mApplicationController.isDataReady()) {
            Log.i(TAG, "data not ready");
            ListenerHelper.getInstance().addInitDataListener(this);
            showLoadingDialog("", getResources().getString(R.string.loading), false);
            this.savedInstanceState = savedInstanceState;
        } else {
            findComponentViews();
            getDataAndDisplayFragment(savedInstanceState);
            trackingScreen(TAG);
        }

        isAllowContactPermission = PermissionHelper.checkPermissionContact(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, mType);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID, mThreadId);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE, mThreadType);
        outState.putString(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID, guestBookId);
        outState.putInt(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID, guestBookPageId);
        outState.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, mListMembers);
        outState.putSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC, songModel);
        outState.putSerializable(Constants.CHOOSE_CONTACT.TYPE_SCREEN, mTypeScreen);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isAllowContactPermission){
            if(PermissionHelper.onlyCheckPermissionContact(getApplicationContext())){
                isAllowContactPermission = true;
                mApplicationController.reLoadContactAfterPermissionsResult();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        ListenerHelper.getInstance().removeInitDataListener(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    public void onBackCreateGroup() {

        Intent intent = new Intent();
        intent.setAction(Constants.CHOOSE_CONTACT.RESULT_CREATE_GROUP);
        setResult(RESULT_OK, intent);
        finish();
    }
    @Override
    public void returnResultShareContact(PhoneNumber phone) {
        if (phone != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME, phone.getName());
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER, phone.getJidNumber());
            setResult(RESULT_OK, returnIntent);
        }
        onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Constants.PERMISSION.PERMISSION_CONTACT){
            if (PermissionHelper.onlyCheckPermissionContact(getApplicationContext())) {
                isAllowContactPermission = true;
                mApplicationController.reLoadContactAfterPermissionsResult();
            }
        }
    }

    @Override
    public void returnResultAssignedPage(PhoneNumber phone) {
        if (phone != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME, phone.getName());
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER, phone.getJidNumber());
            setResult(RESULT_OK, returnIntent);
        }
        onBackPressed();
    }

    @Override
    public void returnResultAddFavorite(PhoneNumber phone) {
        if (phone != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_PHONE_ID, phone.getId());
            setResult(RESULT_OK, returnIntent);
        }
        onBackPressed();
    }

    @Override
    public void createThreadSoloMessage(String number) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER, number);
        setResult(RESULT_OK, returnIntent);
        onBackPressed();
    }

    @Override
    public void forwardMessageToContact(String number) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER, number);
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, forwardingMessage);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void forwardMessageToThread(ThreadMessage thread) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE, thread);
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE, forwardingMessage);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void navigateToChatActivity(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(ChooseContactActivity.this, threadMessage);
    }

    @Override
    public void sendMultiMsgToThread(ThreadMessage thread) {
        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, thread.getThreadType());
        bundle.putSerializable(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE, thread);
        bundle.putSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE, arrayMessage);
        intent.putExtras(bundle);
        startActivity(intent, true);
    }

    @Override
    public void notifyInviteToGroupComplete(ThreadMessage invitingGroup) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, invitingGroup.getId());
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_TYPE, invitingGroup.getThreadType());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void returnCreatedThread(ThreadMessage threadMessage) {
        if (autoForwardMsg) {
            Intent intent = getIntent();
            if (intent != null) {
                ArrayList<ReengMessage> list = null;
                try {
                    Serializable serializable = intent.getSerializableExtra(Constants.CHOOSE_CONTACT.DATA_LIST_REENG_MESSAGE);
                    if (serializable != null) list = (ArrayList<ReengMessage>) serializable;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Utilities.notEmpty(list)) {
                    for (ReengMessage reengMessage : list) {
                        if (reengMessage != null) {
                            if (reengMessage.isForwardingMessage()) {
                                boolean result = mApplicationController.getMessageBusiness().sendForwardingMessage(this, threadMessage
                                        , reengMessage, null);
                                Log.e(TAG, "returnCreatedThread sendForwardingMessage: " + result);
                            } else {
                                boolean result = mApplicationController.getMessageBusiness().sendNewMessage(this, threadMessage
                                        , reengMessage, null);
                                Log.e(TAG, "returnCreatedThread sendNewMessage: " + result);
                            }
                        }
                    }
                }
            }
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_TYPE, threadMessage.getThreadType());
        returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, threadMessage.getId());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void navigateToChatIfNeed(ArrayList<String> listMembers) {
        ThreadMessage threadMessage = mApplicationController.getMessageBusiness().findExistingOrCreateNewThread(listMembers.get(0));
        if (autoForwardMsg) {
            Intent intent = getIntent();
            if (intent != null) {
                ArrayList<ReengMessage> list = null;
                try {
                    Serializable serializable = intent.getSerializableExtra(Constants.CHOOSE_CONTACT.DATA_LIST_REENG_MESSAGE);
                    if (serializable != null) list = (ArrayList<ReengMessage>) serializable;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Utilities.notEmpty(list)) {
                    for (ReengMessage reengMessage : list) {
                        if (reengMessage != null) {
                            if (reengMessage.isForwardingMessage()) {
                                boolean result = mApplicationController.getMessageBusiness().sendForwardingMessage(this, threadMessage
                                        , reengMessage, null);
                                Log.e(TAG, "returnCreatedThread sendForwardingMessage: " + result);
                            } else {
                                boolean result = mApplicationController.getMessageBusiness().sendNewMessage(this, threadMessage
                                        , reengMessage, null);
                                Log.e(TAG, "returnCreatedThread sendNewMessage: " + result);
                            }
                        }
                    }
                }
            }
        }
        String myNumber = mApplicationController.getReengAccountBusiness().getJidNumber();
        if (myNumber != null && myNumber.equals(listMembers.get(0))) {
            showToast(R.string.msg_not_send_me);
        } else {
            NavigateActivityHelper.navigateToChatDetail(this, threadMessage);
            finish();
        }
    }

    @Override
    public void inviteFriend() {
        InputMethodUtils.hideSoftKeyboard(ChooseContactActivity.this);
        mType = Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND;
        displayChooseMultiNumberFragment(mType, new ArrayList<String>(), -1, true, -2);
    }

    @Override
    public void returnRoomInvite() {
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void returnResultShareContact(ArrayList<PhoneNumber> listPhone) {
        if (listPhone != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_LIST_CONTACT, (Serializable) listPhone);
            setResult(RESULT_OK, returnIntent);
        }
        onBackPressed();
    }

    @Override
    public void navigateToChooseContacts(int type) {
        displayChooseMultiNumberFragment(type, new ArrayList<String>(), -1, false, -2);
    }

    private void displayChooseMultiNumberFragment(int actionType, ArrayList<String> listNumbers, int threadId, boolean animation, int typeScreen) {
        // Create an instance Fof newInstance
        ChooseMultiNumberFragment chooseMultiNumberFragment = ChooseMultiNumberFragment.
                newInstance(listNumbers, actionType, threadId, typeScreen);
        executeFragmentTransaction(chooseMultiNumberFragment, R.id.fragment_container, false, animation);
    }

    private void displayFragmentInviteRoom(int actionType, ArrayList<String> listNumbers, int threadId, boolean animation) {
        // Create an instance of newInstance
        ChooseMultiNumberFragment chooseMultiNumberFragment = ChooseMultiNumberFragment.
                newInstance(listNumbers, actionType, threadId, roomId);
        executeFragmentTransaction(chooseMultiNumberFragment, R.id.fragment_container, false, animation);
    }

    private void displayFragmentDeepLinkCampaign(int actionType, String campaignId, boolean animation) {
        ChooseMultiNumberFragment chooseMultiNumberFragment = ChooseMultiNumberFragment.newInstance(actionType, campaignId);
        executeFragmentTransaction(chooseMultiNumberFragment, R.id.fragment_container, false, animation);
    }

    private void displayChooseMultiNumberFragment(int actionType, ArrayList<String> listNumbers,
                                                  int threadId, boolean animation, ReengMessage message) {
        // Create an instance of newInstance
        ChooseMultiNumberFragment chooseMultiNumberFragment = ChooseMultiNumberFragment.
                newInstance(listNumbers, actionType, threadId, message);
        executeFragmentTransaction(chooseMultiNumberFragment, R.id.fragment_container, false, animation);
    }

    private void displayChooseSingleNumberFragment(int actionType, int threadType) {
        // Create an instance of newInstance
        ChooseSingleNumberFragment chooseSingleNumberFragment = ChooseSingleNumberFragment.newInstance(actionType, threadType);
        executeFragmentTransaction(chooseSingleNumberFragment, R.id.fragment_container, false, false);
    }

    private void displayToShareContact(int actionType, ArrayList<String> listNumbers, int threadId, boolean animation) {
        // Create an instance of newInstance
//        ChooseSingleNumberFragment chooseSingleNumberFragment = ChooseSingleNumberFragment.
//                newInstance(actionType, threadType, threadId);
        ChooseMultiNumberFragment chooseMultiNumberFragment = ChooseMultiNumberFragment.
                newInstance(listNumbers, actionType, threadId);
        executeFragmentTransaction(chooseMultiNumberFragment, R.id.fragment_container, false, animation);
    }

    private void displayAllToForward(int actionType, int threadType, ReengMessage reengMessage) {
        // Create an instance of newInstance
        AllMessageAndContactFragment allMsgAndContactFragment = AllMessageAndContactFragment.
                newInstance(actionType, threadType, reengMessage);
        executeFragmentTransaction(allMsgAndContactFragment, R.id.fragment_container, false, false);
    }

    private void displayAllToForward(int actionType, int threadType, ReengMessageWrapper listmsg, boolean showShareOnMedia) {
        // Create an instance of newInstance
        AllMessageAndContactFragment allMsgAndContactFragment = AllMessageAndContactFragment.
                newInstance(actionType, threadType, listmsg, showShareOnMedia);
        executeFragmentTransaction(allMsgAndContactFragment, R.id.fragment_container, false, false);
    }

    private void displayChooseFriendTogetherMusic(int actionType, MediaModel songModel) {
        // Create an instance of newInstance
        AllMessageAndContactFragment allMsgAndContactFragment = AllMessageAndContactFragment.
                newInstance(actionType, songModel);
        executeFragmentTransaction(allMsgAndContactFragment, R.id.fragment_container, false, false);
    }

    private void displayToGuestBookAssigned(int actionType, String bookId, int pageId) {
        // Create an instance of newInstance
        ChooseSingleNumberFragment chooseSingleNumberFragment = ChooseSingleNumberFragment.newInstance(actionType, bookId, pageId);
        executeFragmentTransaction(chooseSingleNumberFragment, R.id.fragment_container, false, false);
    }

    private void displayToSelectCallOutFree(int actionType) {
        ChooseSingleNumberFragment chooseSingleNumberFragment = ChooseSingleNumberFragment.newInstance(actionType);
        executeFragmentTransaction(chooseSingleNumberFragment, R.id.fragment_container, false, false);
    }

    private void displayToSelectContactFreePackage(int actionType, String packageId, String msg) {
        ChooseSingleNumberFragment chooseSingleNumberFragment = ChooseSingleNumberFragment.newInstance(actionType, packageId, msg);
        executeFragmentTransaction(chooseSingleNumberFragment, R.id.fragment_container, false, false);
    }

    private void displayToCreateChat() {
        CreateChatFragment createChatFragment = CreateChatFragment.newInstance();
        executeFragmentTransaction(createChatFragment, R.id.fragment_container, false, false);
    }

    private void displayNewFriend() {
        AddFriendFragment createChatFragment = AddFriendFragment.newInstance();
        executeFragmentTransaction(createChatFragment, R.id.fragment_container, false, false);
    }

    private void findComponentViews() {
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
            mThreadId = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID);
            mThreadType = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE);
            guestBookId = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID);
            guestBookPageId = savedInstanceState.getInt(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID);
            mListMembers = savedInstanceState.getStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER);
            forwardingMessage = (ReengMessage) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE);
            arrayMessage = (ReengMessageWrapper) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE);
            songModel = (MediaModel) savedInstanceState.getSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC);
            roomId = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID);
            deepLinkCampaignId = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN);
            packageIdAVNO = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_PACKAGE_ID);
            msg = savedInstanceState.getString(Constants.CHOOSE_CONTACT.DATA_MSG);
            autoForwardMsg = savedInstanceState.getBoolean(Constants.CHOOSE_CONTACT.DATA_AUTO_FORWARD, false);
            //khong lam gi ca ??????????
        } else {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                mType = bundle.getInt(Constants.CHOOSE_CONTACT.DATA_TYPE);
                mThreadId = bundle.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_ID);
                mThreadType = bundle.getInt(Constants.CHOOSE_CONTACT.DATA_THREAD_TYPE);
                guestBookId = bundle.getString(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_ID);
                guestBookPageId = bundle.getInt(Constants.CHOOSE_CONTACT.DATA_GUEST_BOOK_PAGE_ID);
                mListMembers = bundle.getStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER);
                forwardingMessage = (ReengMessage) bundle.getSerializable(Constants.CHOOSE_CONTACT.DATA_REENG_MESSAGE);
                arrayMessage = (ReengMessageWrapper) bundle.getSerializable(Constants.CHOOSE_CONTACT.DATA_ARRAY_MESSAGE);
                songModel = (MediaModel) bundle.getSerializable(Constants.CHOOSE_CONTACT.DATA_TOGETHER_MUSIC);
                roomId = bundle.getString(Constants.CHOOSE_CONTACT.DATA_ROOM_ID);
                deepLinkCampaignId = bundle.getString(Constants.CHOOSE_CONTACT.DATA_DEEPLINK_CAMPAIGN);
                packageIdAVNO = bundle.getString(Constants.CHOOSE_CONTACT.DATA_PACKAGE_ID);
                mTypeScreen = bundle.getInt(Constants.CHOOSE_CONTACT.TYPE_SCREEN);
                msg = bundle.getString(Constants.CHOOSE_CONTACT.DATA_MSG);
                autoForwardMsg = bundle.getBoolean(Constants.CHOOSE_CONTACT.DATA_AUTO_FORWARD, false);

                boolean showOnMedia = bundle.getBoolean(Constants.CHOOSE_CONTACT.DATA_SHOW_SHARE_ONMEDIA);
                switch (mType) {
                    case Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO:
                        Log.d(TAG, mType + "," + mThreadType + "");
                        displayChooseSingleNumberFragment(mType, mThreadType);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                        displayChooseMultiNumberFragment(mType, mListMembers, -1, false, mTypeScreen);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_INVITE_GROUP:
                        displayChooseMultiNumberFragment(mType, mListMembers, mThreadId, false, -2);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_SHARE_CONTACT:
                        displayToShareContact(mType, mListMembers, -1, false);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE:
                        displayChooseSingleNumberFragment(mType, -1);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND:
                    case Constants.CHOOSE_CONTACT.TYPE_INVITE_MAKE_TURN_LW:
                    case Constants.CHOOSE_CONTACT.TYPE_SOS_MAKE_TURN_LW:
                        displayChooseMultiNumberFragment(mType, mListMembers, -1, false, -2);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_INVITE_NOPHOSO:
                        displayChooseMultiNumberFragment(mType, mListMembers, -1, false, forwardingMessage);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT:
                        //forward message thi nhay vao day
                        displayAllToForward(mType, mThreadType, forwardingMessage);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_TOGETHER_MUSIC:
                        displayChooseFriendTogetherMusic(mType, songModel);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_SHARE_FROM_OTHER_APP:
                        //forward message thi nhay vao day
                        displayAllToForward(mType, mThreadType, arrayMessage, showOnMedia);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER:
                        displayChooseMultiNumberFragment(mType, mListMembers, -1, false, -2);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                        displayChooseMultiNumberFragment(mType, mListMembers, -1, false, -2);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_INVITE_BROADCAST:
                        displayChooseMultiNumberFragment(mType, mListMembers, mThreadId, false, -2);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_INVITE_TO_ROOM:
                        displayFragmentInviteRoom(mType, mListMembers, -1, false);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_DEEPLINK_CAMPAIGN:
                        displayFragmentDeepLinkCampaign(mType, deepLinkCampaignId, false);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_GUEST_BOOK_ASSIGNED:
                        displayToGuestBookAssigned(mType, guestBookId, guestBookPageId);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_CALL_OUT_FREE_USER:
                        displayToSelectCallOutFree(mType);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                        displayToCreateChat();
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_BROADCAST_CHANGE_NUMBER:
                        displayChooseMultiNumberFragment(mType, mListMembers, -1, false, -2);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_SELECT_NUMBER_CALL_FREE_CALLOUT_GUIDE:
                        displayToSelectCallOutFree(mType);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_SET_CALLED_LIMITED:
                        displayToSelectContactFreePackage(mType, packageIdAVNO, msg);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_ADD_FRIEND:
                        displayNewFriend();
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_SET_CALLOUT_FREE:
                        displayToSelectContactFreePackage(mType, packageIdAVNO, msg);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_SELFCARE_RECHARGE:
                        displayToSelfcareRecharge(mType);
                        break;
                    case Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_TRANSFER_MONEY:
                        displayToSelectCallOutFree(mType);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void displayToSelfcareRecharge(int actionType){
        ChooseSingleNumberFragment chooseSingleNumberFragment = ChooseSingleNumberFragment.newInstance(actionType);
        executeFragmentTransaction(chooseSingleNumberFragment, R.id.fragment_container, false, false);
    }

    @Override
    public void onDataReady() {
        Log.i(TAG, "onDataReady");
        hideLoadingDialog();
        findComponentViews();
        getDataAndDisplayFragment(savedInstanceState);
    }
}
package com.metfone.selfcare.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.CropImageNew.MonitoredActivity;
import com.metfone.selfcare.ui.CropImageNew.Utils;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by thanhnt72 on 6/22/2016.
 */
public class CropImageActivity extends MonitoredActivity {

    private static final String TAG = CropImageActivity.class.getSimpleName();
    public static final String FILE_PATH = "FILE_PATH";

    private CropView mCropView, mCropBackGround;
    private Button mBtnSave, mBtnCancel;
    private ImageView mImgRotateLeft;
    private Uri mSaveUri = null;
    private String mImageOutputPath;
    private ContentResolver mContentResolver;
    private Bitmap.CompressFormat mOutputFormat = Bitmap.CompressFormat.JPEG;
    private final Handler mHandler = new Handler();
    private String filePath = "";
    private boolean isOval = false;
    private boolean isPhotoFullScreen = false;
    private Bitmap croppedImage;

    private RelativeLayout mLayoutPreviewCover;
    private ApplicationController mApp;
    private TextView mTvwAvatar;
    private CircleImageView mImgAvatar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        mCropBackGround = findViewById(R.id.crop_view_full_screen);
        mCropView = findViewById(R.id.crop_view);
        mBtnSave = findViewById(R.id.save);
        mBtnCancel = findViewById(R.id.discard);
        mImgRotateLeft = findViewById(R.id.rotateLeft);
        mLayoutPreviewCover = findViewById(R.id.layout_preview_cover);
        mTvwAvatar = findViewById(R.id.contact_avatar_text);
        mImgAvatar = findViewById(R.id.profile_avatar_image);
        mApp = (ApplicationController) getApplication();
        mContentResolver = getContentResolver();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            filePath = bundle.getString(CropView.IMAGE_PATH);
            Log.i(TAG, "file path: " + filePath);
            mImageOutputPath = bundle.getString(CropView.OUTPUT_PATH);
            mSaveUri = getImageUri(mImageOutputPath);
            isOval = bundle.getBoolean(CropView.MASK_OVAL, false);
            isPhotoFullScreen = bundle.getBoolean(CropView.PHOTO_FULL_SCREEN, false);
        }
        //if true: show cropview fullscreen, hide crop oval
        if (isPhotoFullScreen) {
            mCropBackGround.setVisibility(View.VISIBLE);
            mCropView.setVisibility(View.GONE);
            mLayoutPreviewCover.setVisibility(View.GONE);
            mCropBackGround.setViewportRatio(mApp.getWidthPixels() / (float) mApp.getHeightPixels());
            if (new File(filePath).exists()) {
                mCropBackGround.extensions().load(filePath);
            } else {
                mCropBackGround.extensions().load("file://" + filePath);
            }
        } else {
            mCropView.setIsOval(isOval);
            if (new File(filePath).exists()) {
                mCropView.extensions().load(filePath);
            } else {
                mCropView.extensions().load("file://" + filePath);
            }
            mCropBackGround.setVisibility(View.GONE);
            mCropView.setVisibility(View.VISIBLE);
            if (isOval) {
                mLayoutPreviewCover.setVisibility(View.GONE);
            } else {
                mLayoutPreviewCover.setVisibility(View.VISIBLE);
                mApp.getAvatarBusiness().setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatar,
                        mApp.getReengAccountBusiness().getCurrentAccount(), null);
            }
        }
        setViewListener();

    }

    private void setViewListener() {
        mBtnCancel.setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });

        mBtnSave.setOnClickListener(v -> {
            try {
                onSaveClicked();

                /*final File croppedFile = new File(mImageOutputPath);

                mCropView.extensions()
                        .crop()
                        .quality(100)
                        .format(JPEG)
                        .into(croppedFile);*/
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                finish();
            }
        });

        mImgRotateLeft.setOnClickListener(v -> {
            Log.i(TAG, "xoay nao");
            if (isPhotoFullScreen) {
                Bitmap rotateImage = Utils.rotateImage(mCropBackGround.getImageBitmap(), -90);
                mCropBackGround.extensions()
                        .load(rotateImage);
            } else {
                Bitmap rotateImage = Utils.rotateImage(mCropView.getImageBitmap(), -90);
                mCropView.extensions()
                        .load(rotateImage);
            }
        });
    }

    private void onSaveClicked() {

        // If we are circle cropping, we want alpha channel, which is the
        // third param here.
        if (isPhotoFullScreen) {
            croppedImage = mCropBackGround.crop();
        } else {
            croppedImage = mCropView.crop();
        }
        if (croppedImage == null) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        // Return the cropped image directly or save it to the specified URI.
        Bundle myExtras = getIntent().getExtras();
        if (myExtras != null && (myExtras.getParcelable("data") != null
                || myExtras.getBoolean(CropView.RETURN_DATA))) {
            Bundle extras = new Bundle();
            extras.putParcelable(CropView.RETURN_DATA_AS_BITMAP, croppedImage);
            setResult(RESULT_OK,
                    (new Intent()).setAction(CropView.ACTION_INLINE_DATA).putExtras(extras));
            finish();
        } else {
            Utils.startBackgroundJob(this, null, getString(R.string.saving_image),
                    () -> saveOutput(croppedImage), mHandler);
        }
    }

    private void saveOutput(Bitmap croppedImage) {
        if (mSaveUri != null) {
            OutputStream outputStream = null;
            try {
                outputStream =  mContentResolver.openOutputStream(mSaveUri);
                if (outputStream != null) {
                    croppedImage.compress(mOutputFormat, 90, outputStream);
                }
            } catch (IOException ex) {
                Log.e(TAG, "Cannot open file: " + mSaveUri, ex);
                setResult(RESULT_CANCELED);
                finish();
                return;
            } finally {
                Utils.closeSilently(outputStream);
            }

            Bundle extras = new Bundle();
            Intent intent = new Intent(mSaveUri.toString());
            intent.putExtras(extras);
            intent.putExtra(CropView.OUTPUT_PATH, mImageOutputPath);
            intent.putExtra(CropView.ORIENTATION_IN_DEGREES, Utils.getOrientationInDegree(this));
            setResult(RESULT_OK, intent);
        } else {
            Log.e(TAG, "not defined image url");
        }
        croppedImage.recycle();
        finish();
    }

    private Uri getImageUri(String path) {
        return FileHelper.fromFile(mApp, new File(path));
    }

}

package com.metfone.selfcare.activity;

import android.graphics.Color;
import android.os.Bundle;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.metfoneservice.MetFoneServiceRegisterFragment;
import com.metfone.selfcare.fragment.metfoneservice.OTPAddMetfoneProfileFragment;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.util.EnumUtils;

import butterknife.ButterKnife;

public class MetFoneServiceActivity extends BaseSlidingFragmentActivity implements MetFoneServiceRegisterFragment.OnFragmentInteractionListener {
    MetFoneServiceRegisterFragment metFoneServiceRegisterFragment;
    OTPAddMetfoneProfileFragment otpAddMetfoneProfileFragment;
    public static ApplicationController mApplication;
    String fromSource = "";
    int serviceId = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metfone_service);
        ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplication();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        displayServiceRegisterFragment();
        serviceId = getIntent().getIntExtra(EnumUtils.OBJECT_KEY, 1);
    }

    public void displayServiceRegisterFragment() {
        metFoneServiceRegisterFragment = MetFoneServiceRegisterFragment.newInstance();
        executeFragmentTransactionAllowLoss(metFoneServiceRegisterFragment, R.id.container, false, false, "");
    }


    @Override
    public void displayOtpProfileFragment(String phoneNumber, int checkPhoneCode) {
        otpAddMetfoneProfileFragment = new OTPAddMetfoneProfileFragment();
        Bundle bundle = new Bundle();
        if (phoneNumber.startsWith("0")){
            bundle.putString(EnumUtils.OBJECT_KEY, phoneNumber);
        }else {
            bundle.putString(EnumUtils.OBJECT_KEY, "0"+phoneNumber);
        }
        bundle.putInt(EnumUtils.OTP_KEY, checkPhoneCode);
        otpAddMetfoneProfileFragment.setArguments(bundle);
        executeFragmentTransactionAllowLoss(otpAddMetfoneProfileFragment, R.id.container, true, true, "");
    }


    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if(count > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            NavigateActivityHelper.navigateToSetUpProfile(this);
            finish();
        }

    }

    public int getServiceId() {
        return serviceId;
    }
}
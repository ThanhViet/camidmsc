package com.metfone.selfcare.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.fragment.setting.EditProfileFragment;
import com.metfone.selfcare.fragment.setting.ProfileFragmentNew;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.listeners.OnEditProfileListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.network.file.UploadListener;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.ListImageProfileActivity;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;

/**
 * Created by thanhnt72 on 9/3/2015.
 */
public class ProfileActivity extends BaseSlidingFragmentActivity implements
        ProfileFragmentNew.OnFragmentInteractionListener,
        FacebookHelper.OnFacebookListener,
        OnEditProfileListener,
        OnMediaInterfaceListener, TransferFileBusiness.UploadImageProfileListener {
    public static final int REQUEST_PREVIEW_IMAGE = 0x1;
    private static final String TAG = ProfileActivity.class.getSimpleName();
    private static final String EDIT_PROFILE_SHOW = "editProfileShow";
    private SharedPreferences mPref;
    //    private CoverUploadAsyncTask mUploadCoverAsyncTask;
    private ReengAccountBusiness mAccountBusiness;
    private ApplicationController mApplication;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;
    private int typeTakePhoto;
    private ProfileFragmentNew mProfileFragment;
    private EditProfileFragment mEditProfileFragment;
    private FacebookHelper facebookHelper;
    private FeedBusiness mFeedBusiness;


    //    private UploadImageAlbumAsyncTask mAlbumUploader;
    private UploadListener uploadCoverListener = new UploadListener() {
        @Override
        public void onUploadStarted(UploadRequest uploadRequest) {

        }

        @Override
        public void onUploadComplete(UploadRequest uploadRequest, String response) {
            Log.i(TAG, "response upload cover: " + response);
            hideLoadingDialog();
            try {
                JSONObject object = new JSONObject(response);
                int errorCode = -1;
                if (object.has(Constants.HTTP.REST_ERROR_CODE)) {
                    errorCode = object.getInt(Constants.HTTP.REST_ERROR_CODE);
                }
                if (errorCode == 200) {
                    ImageProfile cover = mApplication.getImageProfileBusiness().getImageCover();
                    String id = "";
                    if (cover == null) {
                        cover = new ImageProfile();
                        cover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
                    }
                    if (object.has(Constants.HTTP.REST_LINK))
                        cover.setImageUrl(object.getString(Constants.HTTP.REST_LINK));
                    if (object.has(Constants.HTTP.REST_DESC)) {
                        id = object.getString(Constants.HTTP.REST_DESC);
                        cover.setIdServerString(id);
                    }
                    cover.setUpload(true);
                    cover.setTypeImage(ImageProfileConstant.IMAGE_COVER);
                    cover.setImagePathLocal(uploadRequest.getFilePath());
                    cover.setTime((new Date()).getTime());

                    ListenerHelper.getInstance().onCoverChange(uploadRequest.getFilePath());
                    mApplication.getImageProfileBusiness().insertOrUpdateCover(cover);
                    handleUploadAlbumToOnMedia(id);
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }

        @Override
        public void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage) {
            Log.e(TAG, "onUploadFailed cover code: " + errorMessage + " msg: " + errorMessage);
            showToast(R.string.e601_error_but_undefined);
            hideLoadingDialog();
        }

        @Override
        public void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress, long speed) {

        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onpause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onresume");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "oncreate ProfileActivity");
        changeLightStatusBar();
        setContentView(R.layout.activity_my_profile);
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        /*if (savedInstanceState != null) {
            isEdit = savedInstanceState.getBoolean("is_edit", false);
        } else if (getIntent() != null) {
            isEdit = getIntent().getBooleanExtra("is_edit", false);
            fromLoginActivity = getIntent().getBooleanExtra(LoginActivity.class.getSimpleName(), false);
        }*/
        mApplication = (ApplicationController) getApplicationContext();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        facebookHelper = new FacebookHelper(this);
        /*if (isEdit) {
            mEditProfileFragment = EditProfileFragment.newInstance(fromLoginActivity);
            executeAddFragmentTransaction(mEditProfileFragment, R.id.fragment_container, false, true);
        } else {*/
        mProfileFragment = ProfileFragmentNew.newInstance();
        executeFragmentTransaction(mProfileFragment, R.id.fragment_container, false, true);
//        }

        //TODO sao lai co cai nay nhi
        /*if (mPref.getBoolean(EDIT_PROFILE_SHOW, false)) {
            String url = mPref.getString(PREF_AVATAR_FILE_CROP, "");
            mEditProfileFragment = EditProfileFragment.newInstance(url);
            executeAddFragmentTransaction(mEditProfileFragment, R.id.fragment_container, true, true);
        }*/
        trackingScreen(TAG);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("typeUploadImage", typeUploadImage);
        outState.putInt("typeTakePhoto", typeTakePhoto);
//        outState.putString(EDIT_PROFILE_SHOW, "hoang anh tuan");
//
//        outState.putBoolean("is_edit", isEdit);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            typeUploadImage = savedInstanceState.getInt("typeUploadImage");
            typeTakePhoto = savedInstanceState.getInt("typeTakePhoto");
//            isEdit = savedInstanceState.getBoolean("is_edit");
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        boolean isHD = SettingBusiness.getInstance(getApplicationContext()).getPrefEnableHDImage();
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                // get information from facebook
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        Log.d(TAG, "onActivityResult ACTION_PICK_PICTURE");
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                                String pathImage = picturePath.get(0);
                                cropAvatarImage(pathImage);
                            } else if (typeUploadImage == ImageProfileConstant.IMAGE_IC_FRONT ||
                                    typeUploadImage == ImageProfileConstant.IMAGE_IC_BACK) {
                                if (mEditProfileFragment != null) {
                                    mEditProfileFragment.uploadImageIdentifyCard(picturePath.get(0), typeUploadImage);
                                }
                            } else {
                                showLoadingDialog("", R.string.text_uploading);
                                mApplication.getTransferFileBusiness().uploadAlbumImage(picturePath, this);
                            }
                        }
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    //Uri takeImageUri;
                    File imageFile = new File(mPref.getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                        cropAvatarImage(imageFile.getPath());
                    } else if (typeUploadImage == ImageProfileConstant.IMAGE_IC_FRONT ||
                            typeUploadImage == ImageProfileConstant.IMAGE_IC_BACK) {
                        if (mEditProfileFragment != null) {
                            mEditProfileFragment.uploadImageIdentifyCard(imageFile.getPath(), typeUploadImage);
                        }
                    } else {
                        ArrayList<String> picturePath = new ArrayList<>();
                        picturePath.add(imageFile.getPath());
                        mApplication.getTransferFileBusiness().uploadAlbumImage(picturePath, this);
                        showLoadingDialog("", R.string.text_uploading);
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mPref.getString(PREF_AVATAR_FILE_CROP, "");
//                        setAvatarEditProfile(fileCrop);
                        if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                            if (mEditProfileFragment != null) {
                                Log.i(TAG, "setAvatarEditProfile");
                                mEditProfileFragment.setButtonDoneEnable(true);
                            }
//                            mAccountBusiness.processUploadAvatarTask(fileCrop);
                            Log.i(TAG, "upload anh IMAGE_AVATAR sau khi crop");
                        } else if (typeUploadImage == ImageProfileConstant.IMAGE_COVER) {
                            Log.i(TAG, "upload anh IMAGE_COVER sau khi crop path=" + fileCrop);
                            fileCrop = MessageHelper.getPathOfCompressedFile(fileCrop,
                                    Config.Storage.PROFILE_PATH, "", isHD);
                            mApplication.getTransferFileBusiness().uploadCover(fileCrop, uploadCoverListener);
                            showLoadingDialog("", R.string.text_uploading);
                        } else if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL) {
                            Log.i(TAG, "upload anh IMAGE_NORMAL sau khi crop");
                            ArrayList<String> picturePath = new ArrayList<>();
                            picturePath.add(fileCrop);
                            mApplication.getTransferFileBusiness().uploadAlbumImage(picturePath, this);
                            showLoadingDialog("", R.string.text_uploading);
                        }
                    }
                    break;
                //=========onmedia
                case Constants.ACTION.ACTION_WRITE_STATUS:
                    boolean isEdit = data.getBooleanExtra(OnMediaActivityNew.EDIT_SUCCESS, false);
                    boolean isPost = data.getBooleanExtra(OnMediaActivityNew.POST_SUCCESS, false);
                    if (isEdit) {
                        if (mProfileFragment != null && mProfileFragment.isVisible()) {
                            mProfileFragment.notifyNewFeed(false);
                        }
                    } else if (isPost) {
                        if (mProfileFragment != null && mProfileFragment.isVisible()) {
                            mProfileFragment.notifyNewFeed(true);
                        }
                    } else {
                        //TODO vao day thi cu notify roi cho len dau, unknown case
                        if (mProfileFragment != null && mProfileFragment.isVisible()) {
                            mProfileFragment.notifyNewFeed(true);
                        }
                    }
                    break;

                case Constants.ACTION.ACTION_ONMEDIA_COMMENT:
                    if (mProfileFragment != null && mProfileFragment.isVisible()) {
                        mProfileFragment.notifyNewFeed(false);
                    }
                    break;

                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    int threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;

                default:
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void handleUploadAlbumToOnMedia(String idCover) {
        ArrayList<String> list = new ArrayList<>();
        list.add(idCover);
        WSOnMedia rest = new WSOnMedia(mApplication);
        rest.uploadAlbumToOnMedia(list, FeedContent.ITEM_TYPE_PROFILE_COVER, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "onResponse: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "onErrorResponse: " + volleyError.toString());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO &&
                PermissionHelper.verifyPermissions(grantResults)) {
            takePhoto(typeTakePhoto);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * CROP IMAGE
     *
     * @param inputFilePath
     */
    private void cropAvatarImage(String inputFilePath) {
        try {
            String time = String.valueOf(System.currentTimeMillis());
            File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "/avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            mPref.edit().putString(PREF_AVATAR_FILE_CROP, cropImageFile.toString()).apply();
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                intent.putExtra(CropView.MASK_OVAL, true);
            }
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);

        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    @Override
    public void navigateToEditStatus() {
        Intent intentSetting = new Intent(getApplicationContext(), SettingActivity.class);
        intentSetting.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.CHANGE_STATUS);
        startActivity(intentSetting, true);
    }

    @Override
    public void navigateToEditProfile() {
       /* mPref.edit().putBoolean(EDIT_PROFILE_SHOW, true).apply();
        mEditProfileFragment = EditProfileFragment.newInstance(false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, mEditProfileFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();*/

//        executeAddFragmentTransaction(mEditProfileFragment, R.id.fragment_container, true, true);

        EditProfileFragment.startActivity(this, false);
    }

    @Override
    public void onEditSuccess(boolean isSuccess) {
        /*if (isSuccess) {
            fileCropTmp = "";
            onBackPressed();
            if (!fromLoginActivity) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "onEditSuccess");
                        if (mProfileFragment != null) {
                            Log.i(TAG, "drawProfile");
                            mProfileFragment.drawProfile();
                        }
                    }
                });
            }
        }*/
    }

    @Override
    public void goToHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra("home_action", Constants.ACTION.VIEW_CREATE_CHAT);
        startActivity(intent, false);
        clearBackStack();
        finish();
    }

    @Override
    public void getInfoFacebook() {

    }

    /*@Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressedđ");
        mPref.edit().putString(PREF_AVATAR_FILE_CROP, "").apply();
        mPref.edit().putBoolean(EDIT_PROFILE_SHOW, false).apply();
        if (mEditProfileFragment != null && mEditProfileFragment.isVisible()) {
            if (!needUploadAvatar) {
                if (forceUpIdCard) {
                    if (!TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getAvnoICBack()) &&
                            !TextUtils.isEmpty(mAccountBusiness.getCurrentAccount().getAvnoICFront())) {
                        super.onBackPressed();
                    } else {
                        showToast(R.string.must_upload_id_card);
                    }
                } else {
                    super.onBackPressed();
                }
            } else {
                showDialogConfirmUploadAvatar();
            }
        } else {
            super.onBackPressed();
        }
    }*/


    @Override
    public void takeAPhoto(int typeUpload) {
        typeTakePhoto = typeUpload;
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(ProfileActivity.this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto(typeUpload);
        }
    }

    private void takePhoto(int typeUpload) {
        try {
            typeUploadImage = typeUpload;
            mAccountBusiness.removeFileFromProfileDir();
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.IMAGE_COMPRESSED_FOLDER, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            intent.putExtra("return-data", true);
            mPref.edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, takePhotoFile.toString()).apply();
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    @Override
    public void openGallery(int typeUpload) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @Override
    public void openGallery(int typeUpload, int maxImages) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, maxImages);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @Override
    public void openListImage() {
//        executeFragmentTransaction(ListImageFragment.newInstance(), R.id.fragment_container, true, true);
        ListImageProfileActivity.startActivity(this);
    }

    //========onmedia
    @Override
    public void navigateToWebView(FeedModelOnMedia feed) {
        mFeedBusiness.setFeedProfileProcess(feed);
        NavigateActivityHelper.navigateToWebView(this, feed, true);
    }

    @Override
    public void navigateToVideoView(FeedModelOnMedia feed) {
        mFeedBusiness.setFeedProfileProcess(feed);
//        NavigateActivityHelper.navigateToAutoPlayVideo(ProfileActivity.this, feed);
        /*Video video = Video.convertFeedContentToVideo(feed.getFeedContent());
        if (video == null) return;*/
        mApplication.getApplicationComponent().providesUtils().openVideoDetail(this, feed);
    }

    @Override
    public void navigateToMovieView(FeedModelOnMedia feed) {
        if (feed != null && feed.getFeedContent() != null) {
            Movie movie = FeedContent.convert2Movie(feed.getFeedContent());
            if (movie != null)
                playMovies(movie);
            else
                Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
        }
    }

    @Override
    public void navigateToProcessLink(FeedModelOnMedia feed) {
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToComment(FeedModelOnMedia feed) {
        mFeedBusiness.setFeedProfileProcess(feed);
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_PROFILE);
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_MY_PROFILE);
        boolean showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(feed);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        startActivityForResult(intent, Constants.ACTION.ACTION_ONMEDIA_COMMENT);
    }

    @Override
    public void navigateToAlbum(FeedModelOnMedia feed) {
        /*mFeedBusiness.setFeedPlayList(feed);
        mFeedBusiness.setFeedProfileProcess(feed);
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.ALBUM_DETAIL);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_PROFILE);
        startActivity(intent, true);*/
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToListShare(String url) {
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, url, false);
        /*Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_SHARE);
        startActivity(intent, true);*/
    }

    @Override
    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
        try {
            mApplication.getReengAccountBusiness().getCurrentAccount().setFacebookId(userId);
            // gender
            ListenerHelper.getInstance().onRequestFacebookChange(name, birthDayStr, gender);
            mApplication.getAvatarBusiness().downloadAndSaveAvatar(mApplication, this, userId);
            Log.d(TAG, "requestGetProfile fullName: " + name + " userId: " + userId + " birthDay: " +
                    birthDayStr + " gender: " + gender + "=====:");
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            this.showError(R.string.facebook_get_error, "");
        }
    }

    @Override
    public void onUploadCompleted(int total) {
        Log.d(TAG, "Upload profile image successfully");
        if (total > 1) {
            showToast(String.format(getString(R.string.upload_image_profile_success), total), Toast.LENGTH_LONG);
        } else if (total == 1) {
            showToast(String.format(getString(R.string.upload_image_profile_success), total), Toast.LENGTH_LONG);
        }
        if (total > 0) {
            mProfileFragment.drawProfile();
        }
        hideLoadingDialog();
    }

    @Override
    public void onUploadFailed(int code, String mess, final int total) {
        Log.i(TAG, "onUploadFailed: " + code + " mess: " + mess);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoadingDialog();
                if (total >= 1) {
                    mProfileFragment.drawProfile();
                    showToast(String.format(getString(R.string.upload_image_profile_success), total), Toast
                            .LENGTH_LONG);
                } else {
                    showToast(R.string.upload_image_profile_fail);
                }
            }
        });


        /*if (code == Constants.HTTP.CODE_UPLOAD_FAILED_EXCEEDED) {
            String content = mess;
            try {
                content = new String(content.getBytes("ISO-8859-1"), "UTF-8");
                if (!TextUtils.isEmpty(content))
                    showToast(content, Toast.LENGTH_LONG);
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG,"Exception",e);
            }
        } else {
            showToast(R.string.upload_image_profile_fail);
        }*/
    }
}
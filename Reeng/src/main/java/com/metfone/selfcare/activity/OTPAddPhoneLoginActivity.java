package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Selection;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.fragment.login.EnterPhoneNumberFragment;
import com.metfone.selfcare.fragment.login.OTPLoginFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;

import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;

public class OTPAddPhoneLoginActivity extends BaseSlidingFragmentActivity implements /*SmsReceiverListener,*/ ClickListener.IconListener, MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String TAG = OTPLoginFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PHONE_NUMBER_PARAM = "param1";
    private static final String REGION_CODE_PARAM = "param2";
    private static final String CODE_PARAM = "code";
    private static final String PASSWORD = "password";
    //    private CountDownTimer countDownTimer;
    private static final int TIME_DEFAULT = 60000;
    private static final int REQ_USER_CONSENT = 6;
    private static boolean handlerflag = false;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.rlContainer)
    RelativeLayout rlContainer;
    @BindView(R.id.tvPhoneNumber)
    CamIdTextView tvPhoneNumber;
    @BindView(R.id.text_expried)
    CamIdTextView text_expried;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.tvNotReceivedOtp)
    CamIdTextView tvNotReceivedOtp;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    @BindView(R.id.tv_error)
    AppCompatTextView tvError;
    @Nullable
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.tvEnterNumber)
    CamIdTextView tvEnterNumber;
    @BindView(R.id.tvEnterNumberTitle)
    CamIdTextView tvEnterNumberTitle;
    TextView btnPassword;
    String phoneNumber = "";
    int checkPhoneCode = 0;
    int remain = 0;

    /*@BindView(R.id.btnContinue)
    RoundTextView btnContinue;*/
    Unbinder unbinder;
    String countDownString;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    int TYPE;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private int codeGenOtp;
    private String password = "";
    private OTPLoginFragment.OnFragmentInteractionListener mListener;
    private EnterPhoneNumberFragment.OnFragmentInteractionListener fragmentInteractionListener;
    private ApplicationController mApplication;
    private boolean isPhoneExisted = false;
    private ClickListener.IconListener mClickHandler;
    //------------Variable Views-------------------
    private Handler mHandler;
    private int START_TEXT = 85;
    private int END_TEXT = 0;
    private boolean isSaveInstanceState = false;
    private boolean isLoginDone = false;
    private long countDownTimer;
    private MySMSBroadcastReceiver mySMSBroadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p_add_phone_login);
        mHandler = new Handler();
        handlerflag = true;
        unbinder = ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplicationContext();
        findComponentViews();
        setViewListeners();
        setupUI(rlContainer);
        setColorStatusBar(R.color.white);
        tvEnterNumberDes.setText(fomatString());//
        mApplication.registerSmsOTPObserver();
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.BLACK);
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
        mySMSBroadcastReceiver.initOTPListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        // setWhiteStatusBar();
        setColorOTP();

        mClickHandler = this;
//        SmsReceiver.addSMSReceivedListener(this);
        if (isSaveInstanceState) {          //Vao sau khi da saveinstance
            if (isLoginDone) {              //Da dang nhap thanh cong
                if (mListener != null) {    //Chuyen view

                    mListener.navigateToNextScreen();
                }
            }
        }

        isSaveInstanceState = false;
//        etOtp.setText("");
        if (!handlerflag && remain > 0) {
            startCountDown(remain * 1000);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mClickHandler = null;
        handlerflag = false;
        InputMethodUtils.hideSoftKeyboard(etOtp, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mySMSBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, mCurrentNumberJid);
        outState.putString(REGION_CODE_PARAM, mCurrentRegionCode);
        outState.putInt(CODE_PARAM, codeGenOtp);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }


    private void findComponentViews() {
        etOtp.requestFocus();
//            InputMethodUtils.hideKeyboardWhenTouch(rootView, mLoginActivity);
        //
        PhoneNumberUtil phoneUtil = mApplication.getPhoneUtil();
        Phonenumber.PhoneNumber phoneNumberProtocol = PhoneNumberHelper.getInstant().
                getPhoneNumberProtocol(phoneUtil, mCurrentNumberJid, mCurrentRegionCode);
        StringBuilder sb = new StringBuilder();

        phoneNumber = getIntent().getStringExtra(EnumUtils.PHONE_NUMBER_KEY);
        checkPhoneCode = getIntent().getIntExtra(EnumUtils.CHECK_PHONE_CODE_TYPE, 0);
        if (PhoneNumberHelper.getInstant().isValidPhoneNumber(phoneUtil,
                phoneNumberProtocol)) {
            sb.append(" (+");
            sb.append(String.valueOf(phoneNumberProtocol.getCountryCode())).append(") ");
            sb.append(String.valueOf(phoneNumberProtocol.getNationalNumber()));
            tvPhoneNumber.setText(sb.toString());
        } else {
            tvPhoneNumber.setText(mCurrentNumberJid);
        }
        /*if (codeGenOtp == 200) {
            tvDescInfo.setText(mRes.getString(R.string.login_header_notice_2));
        } else if (codeGenOtp == 201) {
            tvDescInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_201), mCurrentNumberJid));
        } else if (codeGenOtp == 202) {
            tvDescInfo.setText(String.format(mRes.getString(R.string.msg_gen_otp_code_202), mCurrentNumberJid));
        }*/

    }

    private void setColorOTP() {
        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY
                || ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
            changeStatusBar(Color.parseColor("#1F1F1F"));
            setSystemBarTheme(OTPAddPhoneLoginActivity.this, true);
            rlContainer.setBackgroundColor(getResources().getColor(R.color.color_rlContainer));
            ivBack.setImageResource(R.drawable.ic_navigate_back);
            tvEnterNumber.setTextColor(Color.WHITE);
            tvEnterNumberDes.setTextColor(Color.WHITE);
            etOtp.setPinBackground(ContextCompat.getDrawable(this, R.drawable.bg_otp_pin_verify));
            etOtp.setTextColor(Color.WHITE);
            etOtp.setHintTextColor(Color.WHITE);
            text_expried.setTextColor(Color.WHITE);
            tvNotReceivedOtp.setTextColor(Color.WHITE);

        }
    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        generateOtpByServer();
        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
                Intent intent = new Intent(OTPAddPhoneLoginActivity.this, SetYourNewPasswordActivity.class);
                intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
                intent.putExtra(EnumUtils.OTP_KEY, str.toString());
                intent.putExtra(EnumUtils.CHECK_PHONE_CODE_TYPE, checkPhoneCode);
                startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
            }
        });
    }

    private void setResendPassListener() {
        tvResendOtp.setText(getString(R.string.mc_resend_otp));
        tvResendOtp.setTextColor(ContextCompat.getColor(this, R.color.resend));
        tvResendOtp.setOnClickListener(view -> showDialogConfirmResendCode());
    }

    private void startCountDown(int time) {
        tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    setResendPassListener();
                } else if (remain > 0) {
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                } else {
                    remain = 0;
                }
                String countDownString = getString(R.string.mc_resend_otp);
                tvResendOtp.setText(countDownString);
                tvTimeResendOtp.setText(remain +" "+ getString(R.string.text_seconds));
            }
        }, 1000);
    }

    public void generateOtpByServer() {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.generateOtp(phoneNumber, new ApiCallback<BodyResponse>() {
            @Override
            public void onResponse(Response<BodyResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                startSmsUserConsent();
                android.util.Log.d("TAG", "onResponse: " + response.body());
            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                android.util.Log.d("TAG", "onError: " + error.getMessage());
            }
        });


    }

    private String fomatString() {
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        String mPhoneNumber = phoneNumber.substring(0,3)+" "+ phoneNumber.substring(3);
        fmt.format(getString(R.string.text_otp_has_been_sent_to), mPhoneNumber);
        return sbuf.toString();
    }

    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(OTPAddPhoneLoginActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    private void showDialogConfirmResendCode() {
        String labelOK = getString(R.string.ok);
        String labelCancel = getString(R.string.cancel);
        String title = getString(R.string.title_confirm_resend_code);
        String msg = String.format(getString(R.string.msg_confirm_resend_code),  phoneNumber.substring(0,3)+" "+phoneNumber.substring(3));
        PopupHelper.getInstance().showDialogConfirm(this, title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    private void setPasswordEditText(String pass) {
        etOtp.setText(pass);
        Selection.setSelection(etOtp.getText(), etOtp.getText().length());
    }

    @OnClick({R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
                onBackPressed();
                break;
        }
    }

    //XU ly phan sms otp
    @Override
    public void onOTPReceived(Intent intent) {
        startActivityForResult(intent, REQ_USER_CONSENT);

    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onBackPressed() {
        ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EnumUtils.MY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            int result = data.getIntExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
            Intent returnIntent = new Intent();
            android.util.Log.d(TAG, "onActivityResult: " + result);
            returnIntent.putExtra(EnumUtils.RESULT, result);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == RESULT_OK && data != null) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (message != null) {
                    getOtpFromMessage(message);
                }

            }

        }
    }

    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(this);

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                Log.d(TAG,"Success");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,"Fail");

            }
        });
    }
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            etOtp.setText(matcher.group(0));
        }
    }

}
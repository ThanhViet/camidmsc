package com.metfone.selfcare.activity.update_info;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;

import java.util.ArrayList;
import java.util.List;

class UpdateInfoPagerAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> fragments = new ArrayList<>();
    private Context context;

    public UpdateInfoPagerAdapter(Context context, @NonNull FragmentManager fm, List<BaseFragment> fragments) {
        super(fm);
        this.fragments = fragments;
        this.context = context;
    }

    public UpdateInfoPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return context.getString(R.string.tab_id_picture);
        } else if (position == 1) {
            return context.getString(R.string.tab_info);
        } else {
            return context.getString(R.string.tab_address);
        }
    }
}

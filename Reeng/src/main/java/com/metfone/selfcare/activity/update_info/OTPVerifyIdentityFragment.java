package com.metfone.selfcare.activity.update_info;

import static android.app.Activity.RESULT_OK;
import static androidx.constraintlayout.motion.utils.Oscillator.TAG;
import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.broadcast.MySMSBroadcastReceiver;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.FrgamentOtpIdentityBinding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubGetInfoCusResponse;
import com.metfone.selfcare.util.EnumUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Response;

public class OTPVerifyIdentityFragment extends Fragment implements View.OnClickListener, ClickListener.IconListener,
        MySMSBroadcastReceiver.OTPReceiveListener {
    private static final String PHONE_NUMBER_PARAM = "phoneNumber";
    private static final String SERVICE_ID_PARAM = "serviceId";
    private static final String CODE_CHECK_PHONE_PARAM = "codeCheckPhone";
    private static final int TIME_DEFAULT = 60000;
    private static boolean handlerflag = false;
    private static final int REQ_USER_CONSENT = 6;
    UserInfo currentUser;
    UserInfoBusiness userInfoBusiness;
    int remain = 0;
    private VerifyUserActivity metFoneServiceActivity;
    private Handler mHandler;
    private long countDownTimer;
    private ClickListener.IconListener mClickHandler;
    private String phoneNumber = "";
    private int checkPhoneCode;
    private ApplicationController mApplication;
    private boolean isSaveInstanceState = false;
    private MySMSBroadcastReceiver mySMSBroadcastReceiver;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private FrgamentOtpIdentityBinding binding;

    public static OTPVerifyIdentityFragment newInstance() {
        Bundle args = new Bundle();
        OTPVerifyIdentityFragment fragment = new OTPVerifyIdentityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        metFoneServiceActivity = (VerifyUserActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        handlerflag = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FrgamentOtpIdentityBinding.inflate(inflater, container, false);
        mySMSBroadcastReceiver = new MySMSBroadcastReceiver();
        mySMSBroadcastReceiver.initOTPListener(this);
        metFoneServiceActivity.registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getData();
        setViewListeners();
        mApplication = (ApplicationController) requireActivity().getApplicationContext();
        binding.tvEnterNumberDes.setText(fomatString());
        mApplication.registerSmsOTPObserver();
        userInfoBusiness = new UserInfoBusiness(requireContext());
        currentUser = userInfoBusiness.getUser();
        setupUI(binding.rllOtpAddMetfone);
        binding.etOtpVerify.setSingleCharHint("-");
        binding.etOtpVerify.setHintTextColor(Color.WHITE);
        metFoneServiceActivity.setColorStatusBar(R.color.color_rlContainer);
        if (getActivity() != null) {
            setSystemBarTheme((getActivity()), true);
        }
        binding.ivBack.setOnClickListener(v -> requireActivity().onBackPressed());
    }

    private void getData() {
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(EnumUtils.OBJECT_KEY);
            checkPhoneCode = getArguments().getInt(EnumUtils.OTP_KEY);
        }
    }

    @Optional
    @OnClick({R.id.ivBack, R.id.tvResendOtp})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvResendOtp:
                showDialogConfirmResendCode();
                break;
            case R.id.ivBack:
                metFoneServiceActivity.onBackPressed();
                break;
        }
    }

    private void startCountDown(int time) {
        binding.tvResendOtp.setOnClickListener(null);
        countDownTimer = time;
        String countDownString = getString(R.string.mc_resend_otp);
        binding.tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain == 0) {
                    setResendPassListener();
                } else if (remain > 0) {
                    if (mHandler != null) mHandler.postDelayed(this, 1000);
                } else {
                    remain = 0;
                }
                String countDownString = getString(R.string.mc_resend_otp);
                binding.tvResendOtp.setText(countDownString);
                binding.tvTimeResendOtp.setText(remain + " " + getString(R.string.text_seconds));
            }
        }, 1000);
    }

    private void setResendPassListener() {
        binding.tvResendOtp.setText(getString(R.string.mc_resend_otp));
        binding.tvResendOtp.setTextColor(ContextCompat.getColor(requireContext(), R.color.resend));
        binding.tvResendOtp.setOnClickListener(view -> {
            showDialogConfirmResendCode();
        });
    }

    private String fomatString() {
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        String mPhoneNumber = phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3);
        fmt.format(getString(R.string.text_otp_has_been_sent_to), mPhoneNumber);
        return sbuf.toString();
    }

    private void showDialogConfirmResendCode() {
        String labelOK = getString(R.string.ok);
        String labelCancel = getString(R.string.cancel);
        String title = getString(R.string.title_confirm_resend_code);
        String msg = String.format(getString(R.string.msg_confirm_resend_code), phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3));
        PopupHelper.getInstance().showDialogConfirm((BaseSlidingFragmentActivity) requireActivity(), title,
                msg, labelOK, labelCancel, mClickHandler, null, Constants.MENU.POPUP_YES);
    }

    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener((v, event) -> {
                hideKeyboard(requireActivity());
                return false;
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    private void showDialogError(String msg) {
        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(),
                R.drawable.image_error_dialog,
                getString(R.string.title_sorry), msg, false);
        baseMPSuccessDialog.show();
    }


    public void generateOtpByServer(String phoneNumber) {
        binding.pbLoading.setVisibility(View.VISIBLE);
        new MetfonePlusClient().wsSubscriberGetOTPMetFone(phoneNumber, new MPApiCallback<WsGetOtpResponse>() {
            @Override
            public void onResponse(Response<WsGetOtpResponse> response) {
                binding.pbLoading.setVisibility(View.GONE);
                if (response.body() == null || response.body().getResult() == null) {
                    showDialogError(getString(R.string.e560_error_over_total_message));
                    return;
                }
                WsGetOtpDetailResponse result = response.body().getResult();
                if (result.getErrorCode().equals("401")) {
                    showDialogError(result.getUserMsg());
                    return;
                }
//                if (checkResend) {
//                    checkResend = false;
//                    ivWrongOTP.setVisibility(View.VISIBLE);
//                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
//                        ivWrongOTP.setText(getString(R.string.mc_resend_success));
//                        mHandler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                ivWrongOTP.setVisibility(View.GONE);
//                            }
//                        }, 3000);
//                    }
//                }

                startSmsUserConsent();
                android.util.Log.d("TAG", "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(), binding.etOtpVerify);
                    }
                }, 200);
            }

            @Override
            public void onError(Throwable error) {
                binding.pbLoading.setVisibility(View.GONE);
                showDialogError(error.getMessage());
            }
        });
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer(phoneNumber);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mClickHandler = this;
        binding.etOtpVerify.requestFocus();
        if (!handlerflag && remain > 0) {
            UIUtil.showKeyboard(getContext(), binding.etOtpVerify);
            startCountDown(remain * 1000);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        handlerflag = false;
        InputMethodUtils.hideSoftKeyboard(binding.etOtpVerify, getContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacksAndMessages(null);
        mHandler = null;
        metFoneServiceActivity.unregisterReceiver(mySMSBroadcastReceiver);
    }

    //XU ly phan sms otp
    @Override
    public void onOTPReceived(Intent intent) {
        startActivityForResult(intent, REQ_USER_CONSENT);
    }

    @Override
    public void onOTPTimeOut() {
    }


    private void getUserByOTP(String otp, String phoneNumber) {
        new MetfonePlusClient().wsSubscriberGetInfoCusByISDN(otp, phoneNumber, new MPApiCallback<WsSubGetInfoCusResponse>() {
            @Override
            public void onResponse(Response<WsSubGetInfoCusResponse> response) {
                binding.pbLoading.setVisibility(View.GONE);
                if (response.body() != null && response.body().getResult() != null) {
                    if ("1".equals(response.body().getResult().getWsResponse().getErrorCode())) {
                        baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog,
                                response.body().getResult().getWsResponse().getErrorDescription(),
                                getString(R.string.empty_string),
                                false);
                        baseMPSuccessDialog.setOnDismissListener((l) -> {
                          //  requireActivity().onBackPressed();
//                            UserIdentityInfo userIdentityInfo = response.body().getResult().getWsResponse().getUserIdentityInfo();
//                            NavigateActivityHelper.navigateToFullInfo(((BaseSlidingFragmentActivity) requireActivity()), userIdentityInfo);
//                            metFoneServiceActivity.onBackPressed();
                        });
                        baseMPSuccessDialog.show();
                    } else if ("0".equals(response.body().getResult().getWsResponse().getErrorCode())) {
                        UserIdentityInfo userIdentityInfo = response.body().getResult().getWsResponse().getUserIdentityInfo();
                        userIdentityInfo.setPhone(phoneNumber);
                        NavigateActivityHelper.navigateToFullInfo(((BaseSlidingFragmentActivity) requireActivity()), userIdentityInfo);
                        requireActivity().finish();
                    }
                } else {
                    baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), getString(R.string.empty_string), false);
                    baseMPSuccessDialog.show();
                    getChildFragmentManager().executePendingTransactions();
                    if (baseMPSuccessDialog != null) {
                        baseMPSuccessDialog.setOnDismissListener(dialogInterface -> {
                            requireActivity().onBackPressed();
//                            NavigateActivityHelper.navigateToFullInfo(((BaseSlidingFragmentActivity) requireActivity()), null);
                        });
                    }
                }

            }

            @Override
            public void onError(Throwable error) {
                binding.pbLoading.setVisibility(View.GONE);
                com.metfone.selfcare.util.Log.e("TAG", "Exception", error);
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), error.getMessage(), false);
                baseMPSuccessDialog.show();
                getChildFragmentManager().executePendingTransactions();
                if (baseMPSuccessDialog != null) {
                    baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (getActivity() != null) {
                                metFoneServiceActivity.onBackPressed();
                            }
                        }
                    });
                }
            }
        });
    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        generateOtpByServer(phoneNumber);

        binding.etOtpVerify.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 5) {
                getUserByOTP(str.toString(), phoneNumber);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == RESULT_OK && data != null) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                if (message != null) {
                    getOtpFromMessage(message);
                }

            }

        }
    }

    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Success");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Fail");

            }
        });
    }

    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{5}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            binding.etOtpVerify.setText(matcher.group(0));
        }
    }

}
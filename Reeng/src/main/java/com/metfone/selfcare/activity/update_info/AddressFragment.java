package com.metfone.selfcare.activity.update_info;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.update_info.listener.EditButtonListener;
import com.metfone.selfcare.adapter.SpinAdapter;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.FragmentAddressBinding;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.MappingUtils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by toanns on 10/16/2021 .
 */

public class AddressFragment extends BaseFragment {
    private UserIdentityInfo userIdentityInfo;
    private String[] provinceList;
    private List<String> genders;
    private List<String> relationship;
    private SpinAdapter districtAdapter;
    boolean isFirstSelectDistrict = true;
    private EditButtonListener listener;

    private FragmentAddressBinding binding;
    View.OnTouchListener onTouchListener = (v, event) -> {
        ((BaseSlidingFragmentActivity) requireActivity()).hideKeyboard();
        return v.performClick();
    };

    public static AddressFragment newInstance(UserIdentityInfo userIdentityInfo) {
        Bundle args = new Bundle();
        AddressFragment fragment = new AddressFragment();
        args.putSerializable(EnumUtils.OBJECT_KEY, userIdentityInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (EditButtonListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAddressBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI(binding.scrFullfill);
        provinceList = MappingUtils.getProvinceList(requireContext());
        initView();
        if (getArguments() != null) {
            userIdentityInfo = (UserIdentityInfo) getArguments().getSerializable(EnumUtils.OBJECT_KEY);
            if (userIdentityInfo != null) {
                drawProfile(userIdentityInfo);
            }
        }
        setListener();
    }

    private void setupUI(View view) {
        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener((v, event) -> {
                UserInfoBusiness.hideKeyboard(requireActivity());
                return false;
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    private void initView() {
        binding.spnSex.setOnTouchListener(onTouchListener);
        genders = Arrays.asList(getResources().getStringArray(R.array.sex_your));
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(requireContext(), R.layout.display_spinner_dark, genders) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return genders.size() - 2;
            }
        };
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnSex.setAdapter(genderAdapter);
        binding.spnSex.setSelection(3);

        binding.spnRelationship.setOnTouchListener(onTouchListener);
        relationship = Arrays.asList(getResources().getStringArray(R.array.relationship));
        ArrayAdapter<String> relationAdapter = new ArrayAdapter<String>(requireContext(), R.layout.display_spinner_dark, relationship) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return relationship.size() - 1;
            }
        };
        relationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnRelationship.setAdapter(relationAdapter);
        binding.spnRelationship.setSelection(2);
        districtAdapter = new SpinAdapter(requireContext(), R.layout.display_spinner_dark,
                new String[]{getString(R.string.hint_default_value)});
        districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnDistrict.setAdapter(districtAdapter);
        binding.spnProvince.setOnTouchListener(onTouchListener);
        binding.spnDistrict.setSelection(0);

        provinceList = MappingUtils.getProvinceList(requireContext());
        ArrayAdapter<String> provinceAdapter = new ArrayAdapter(requireContext(), R.layout.display_spinner_dark, provinceList) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return provinceList.length - 1;
            }
        };
        provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnProvince.setAdapter(provinceAdapter);
        binding.spnProvince.setSelection(provinceList.length - 1);
        binding.spnDistrict.setOnTouchListener(onTouchListener);
    }

    private void setListener() {
        binding.cbAnother.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                binding.clGroupPerson.setVisibility(View.VISIBLE);
            } else {
                binding.clGroupPerson.setVisibility(View.GONE);
            }
        });

        boolean isChecked = !TextUtils.isEmpty(userIdentityInfo.getSubName())
                || !TextUtils.isEmpty(userIdentityInfo.getSubDateBirth())
                || !TextUtils.isEmpty(userIdentityInfo.getSubGender());
        binding.cbAnother.setChecked(isChecked);

        binding.edtDob.setOnClickListener((v) -> {
            InputMethodUtils.hideSoftKeyboard(requireActivity());
            showDialogTimePicker(binding.edtDob, 2021);
        });
        binding.btnUpdate.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).handleUpdate();
        });
        binding.btnBack.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).handleBack(2);
        });
        binding.btnNext.setOnClickListener(v -> {
            if (validateInfo()) {
                ((ConfirmIdentityInfoActivity) requireActivity()).handleNext(2);
            }
        });
        binding.btnEdit.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).setScan(false);
            if (listener != null) {
                listener.onEditData();
            }
            updateButtonEdit();
        });

        binding.spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String province = binding.spnProvince.getSelectedItem().toString();
                String code = MappingUtils.getProvinceCode(requireContext(), province);
                districtAdapter.setValues(MappingUtils.getDistrictList(requireContext(), code));
                if (!isFirstSelectDistrict) {
                    binding.spnDistrict.setSelection(0);
                }
                InputMethodUtils.hideSoftKeyboard(requireActivity());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void showDialogTimePicker(CamIdTextView view, int maxYear) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        if (mYear < maxYear) {
            mYear = maxYear;
        }
        String dateChoseDefault;
        if (TextUtils.isEmpty(view.getText().toString())
                || !DateTimeUtils.checkYear(view.getText().toString(), DateTimeUtils.FORMAT_1)) {
            dateChoseDefault = UserInfoBusiness.getCurrentDate();
        } else {
            dateChoseDefault = view.getText().toString();
        }
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(requireContext(), (year, month, day, dateDesc) -> {
            view.setText(dateDesc);
        }).textConfirm(getResources().getString(R.string.confirm_button)) //text of confirm button
                .textCancel(getResources().getString(R.string.cancel_button)) //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .dateChose(dateChoseDefault) // date chose when init popwindow
                .minYear(1900)
                .maxYear(mYear + 1) // max year in loop
                .isGreater(false)
                .build();
        pickerPopWin.showPopWin(requireActivity());
    }

    private void drawProfile(UserIdentityInfo userInfo) {
        Log.e(TAG, "tabInfo drawProfile: " + userInfo.toString());

        String address = userInfo.getAddress();
        if (TextUtils.isEmpty(address)) {
            binding.edtCurrentAddress.setText("");
        } else {
            binding.edtCurrentAddress.setText(address);
        }

        String subName = userInfo.getSubName();
        String subGender = userInfo.getSubGender();
        String subBirth = userInfo.getSubDateBirth();
        String street = userInfo.getStreetName();
        String homeNo = userInfo.getHome();
        String relation = userInfo.getRelationship();
        if (TextUtils.isEmpty(subName)) {
            binding.edtFullName.setText(R.string.empty_string);
        } else {
            binding.edtFullName.setText(subName);
        }
        if (subGender != null && !subGender.isEmpty()) {
            //Male
            if (subGender.toLowerCase(Locale.ROOT).equals("M".toLowerCase(Locale.ROOT)))
                binding.spnSex.setSelection(0);
                //Female
            else if (subGender.toLowerCase(Locale.ROOT).equals("F".toLowerCase(Locale.ROOT)))
                binding.spnSex.setSelection(1);
        }

        if (TextUtils.isEmpty(subBirth)) {
            binding.edtDob.setText(R.string.empty_string);
        } else {
            binding.edtDob.setText(DateTimeUtils.dateStrToStringMP(subBirth));
        }

        if (!TextUtils.isEmpty(relation)) {
            if (relation.toLowerCase(Locale.ROOT).equals("1".toLowerCase(Locale.ROOT))) {
                binding.spnRelationship.setSelection(0);
            } else if (relation.toLowerCase(Locale.ROOT).equals("2".toLowerCase(Locale.ROOT))) {
                binding.spnRelationship.setSelection(1);
            }
        }

        if (TextUtils.isEmpty(street)) {
            binding.edtStreet.setText(R.string.empty_string);
        } else {
            binding.edtStreet.setText(street);
        }

        if (TextUtils.isEmpty(homeNo)) {
            binding.edtHomeNo.setText(R.string.empty_string);
        } else {
            binding.edtHomeNo.setText(homeNo);
        }

        try {
            if (userInfo.getProvince() != null) {
                int provincePos = MappingUtils.getProvinceIndexWithCode(requireContext(), userInfo.getProvince());
                binding.spnProvince.setSelection(provincePos);
                if (userInfo.getDistrict() != null) {
                    String[] districtList = MappingUtils.getDistrictList(requireContext(), userInfo.getProvince());
                    int districtPos = 0;
                    if (districtList.length != 0) {
                        districtPos = foundIndex(districtList, userInfo.getDistrict()) - 1;
                    }
                    int finalDistrictPos = districtPos;
                    binding.spnDistrict.postDelayed(() ->
                            binding.spnDistrict.setSelection(finalDistrictPos), 100);
                }
            } else {
                binding.spnProvince.setSelection(15);
            }

        } catch (Exception ex) {
            Log.e("ttt", "drawProfile: Address " + ex.getMessage());
        }
    }

    private int foundIndex(String[] list, String str) {
        for (int i = 0; i < list.length; i++) {
            if (str.equals(list[i])) {
                return i + 1;
            }
        }
        return 0;
    }

    public UserIdentityInfo informationIdentity() {
        UserIdentityInfo userInfo = new UserIdentityInfo();
        if (binding.cbAnother.isChecked()) {
            String fullName = binding.edtFullName.getText().toString().trim();
            String dob = binding.edtDob.getText().toString().trim();
            int relationship = binding.spnRelationship.getSelectedItemPosition() + 1;
            String gender = "M";
            if (getString(R.string.sex_male).equals(binding.spnSex.getSelectedItem().toString())) {
                gender = "M";
            } else if (getString(R.string.sex_female).equals(binding.spnSex.getSelectedItem().toString())) {
                gender = "F";
            }
            if (!TextUtils.isEmpty(fullName)) {
                userInfo.setSubName(fullName);
            } else {
                userInfo.setSubName(getString(R.string.empty_string));
            }
            if (!TextUtils.isEmpty(dob)) {
                userInfo.setSubDateBirth(dob);
            } else {
                userInfo.setSubDateBirth(getString(R.string.empty_string));
            }
            if (!TextUtils.isEmpty(gender)) {
                userInfo.setSubGender(gender);
            } else {
                userInfo.setSubGender(getString(R.string.empty_string));
            }
            if (relationship == 1 || relationship == 2) {
                userInfo.setRelationship(String.valueOf(relationship));
            } else {
                userInfo.setRelationship("");
            }
        } else {
            userInfo.setSubName("");
            userInfo.setSubDateBirth("");
            userInfo.setSubGender("");
            userInfo.setRelationship("");
        }

        String address = binding.edtCurrentAddress.getText().toString().trim();
        String province = binding.spnProvince.getSelectedItem().toString().trim();
        String district = binding.spnDistrict.getSelectedItem().toString().trim();
        String street = binding.edtStreet.getText().toString().trim();
        String homeNo = binding.edtHomeNo.getText().toString().trim();
        String provinceCode = MappingUtils.getProvinceCode(requireContext(), province);
        String districtCode = MappingUtils.getDistrictCode(requireContext(), district, provinceCode);

        userInfo.setAddress(address);
        userInfo.setProvince(provinceCode);
        userInfo.setDistrict(districtCode);
        userInfo.setStreetName(street);
        userInfo.setHome(homeNo);
        return userInfo;
    }

    public UserInfo informationUser() {
        String address = binding.edtCurrentAddress.getText().toString().trim();
        String province = binding.spnProvince.getSelectedItem().toString().trim();
        String district = binding.spnDistrict.getSelectedItem().toString().trim();

        UserInfo userInfo = new UserInfo();
        userInfo.setAddress(address);
        userInfo.setProvince(province);
        userInfo.setDistrict(district);
        return userInfo;
    }

    public boolean validateInfo() {
        String address = binding.edtCurrentAddress.getText().toString().trim();
        String province = binding.spnProvince.getSelectedItem().toString().trim();

        if (TextUtils.isEmpty(address)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_input_full_address));
            return false;
        }
        if (TextUtils.isEmpty(province) || province.equals(getString(R.string.hint_default_value))) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_select_province));
            return false;
        }

        if (binding.cbAnother.isChecked()) {
            String fullName = binding.tvFullName.getText().toString().trim();
            String dob = binding.spnProvince.getSelectedItem().toString().trim();

            if (TextUtils.isEmpty(fullName)
                    || TextUtils.isEmpty(dob)
                    || binding.spnSex.getSelectedItem().toString().trim().isEmpty()) {
                ToastUtils.showToast(requireContext(), getString(R.string.txt_error_fill_relationship_info));
                return false;
            }
        }

        return true;
    }
    private String getIDType() {
        return ((ConfirmIdentityInfoActivity) requireActivity()).getTypeID();
    }

    private boolean checkPassport() {
        return getIDType().toLowerCase(Locale.ROOT)
                .equals(getString(R.string.passport).toLowerCase(Locale.ROOT));
    }

    public void updateInformationOCR(String provinceEn) {
        if (!TextUtils.isEmpty(provinceEn)) {
            binding.edtCurrentAddress.setText(provinceEn);
        }
        if(checkPassport()) {
            binding.edtCurrentAddress.setEnabled(true);
        } else {
            binding.btnEdit.setVisibility(View.VISIBLE);
            binding.edtCurrentAddress.setEnabled(false);
        }
    }

    public void updateButtonEdit() {
        binding.btnEdit.setVisibility(View.GONE);
        binding.edtCurrentAddress.setEnabled(true);
        binding.spnProvince.setEnabled(true);
        binding.spnDistrict.setEnabled(true);
    }
}

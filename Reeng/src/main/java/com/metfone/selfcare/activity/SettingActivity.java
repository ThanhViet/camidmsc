package com.metfone.selfcare.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.blankj.utilcode.util.StringUtils;
import com.facebook.login.LoginManager;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.database.model.BlockContactModel;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.contact.StrangerDetailFragmentNew;
import com.metfone.selfcare.fragment.musickeeng.NearYouFragment;
import com.metfone.selfcare.fragment.setting.AboutFragment;
import com.metfone.selfcare.fragment.setting.AccountSettingFragment;
import com.metfone.selfcare.fragment.setting.BlockListFragment;
import com.metfone.selfcare.fragment.setting.ConfigTabHomeFragment;
import com.metfone.selfcare.fragment.setting.DataPackDetailFragment;
import com.metfone.selfcare.fragment.setting.EditStatusFragment;
import com.metfone.selfcare.fragment.setting.ListRoomFragment;
import com.metfone.selfcare.fragment.setting.ListRoomUtilitiesFragment;
import com.metfone.selfcare.fragment.setting.NoteMessageFragment;
import com.metfone.selfcare.fragment.setting.SearchRoomFragment;
import com.metfone.selfcare.fragment.setting.SettingCallAndMessageFragment;
import com.metfone.selfcare.fragment.setting.SettingFragment;
import com.metfone.selfcare.fragment.setting.SettingImageAndSoundFragment;
import com.metfone.selfcare.fragment.setting.SettingNotificationFragment;
import com.metfone.selfcare.fragment.setting.SettingPrivateFragment;
import com.metfone.selfcare.fragment.setting.TranslationFragment;
import com.metfone.selfcare.fragment.setting.WebviewSettingFragment;
import com.metfone.selfcare.fragment.setting.hidethread.HideThreadSettingFragment;
import com.metfone.selfcare.fragment.setting.hidethread.PINSettingFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.workmanager.SettingWorkManager;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;
import java.util.HashSet;

import retrofit2.Response;

//import com.metfone.selfcare.fragment.setting.NoteMessageFragment;

/**
 * Created by toanvk2 on 10/08/14.
 */
public class SettingActivity extends BaseSlidingFragmentActivity implements SettingFragment.OnFragmentSettingListener,
        AboutFragment.OnFragmentAppInfoListener,
        ClickListener.IconListener,
        ListRoomFragment.OnFragmentListRoomListener,
        SearchRoomFragment.OnFragmentSearchRoomListener,
        ListRoomUtilitiesFragment.OnFragmentInteractionListener,
        SearchRoomFragment.OnFragmentInteractionListener,
        SettingPrivateFragment.OnFragmentSettingListener, StrangerDetailFragmentNew.OnStrangerFragmentInteractionListener {
    private final String TAG = SettingActivity.class.getSimpleName();
    private SharedPreferences mPref;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private int fragment = -1;
    private ThreadMessage threadMessage;
    private Handler mHandler;
    //    private CallbackManager callbackManager;
    private BlockListFragment mBlockListFragment;
    private FacebookHelper facebookHelper;
    private boolean resetPin = false;
    private MessageBusiness mMessageBusiness;
    private UserInfoBusiness userInfoBusiness;

    public static void startActivityForResultOpenHidden(BaseSlidingFragmentActivity activity, ThreadMessage threadHiddenOpen, int action) {
        Intent intent = new Intent(activity, SettingActivity.class);
        intent.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.SETTINGS.SETTING_OPEN_HIDDEN_THREAD);
        intent.putExtra(Constants.SETTINGS.DATA_THREAD, threadHiddenOpen);
        activity.startActivityForResult(intent, action);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeWhiteStatusBar();
        setContentView(R.layout.activity_setting_v5);
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        mApplication = (ApplicationController) getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        userInfoBusiness = new UserInfoBusiness(this);
        facebookHelper = new FacebookHelper(this);
//        setActionBar();
        getData();
        if (savedInstanceState == null) {
            displaySettingFragment();
        }
        trackingScreen(TAG);
    }

    private void setActionBar() {
//        setToolBar(findViewById(R.id.tool_bar));
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        mHandler.postDelayed(() -> InputMethodUtils.hideSoftKeyboard(SettingActivity.this), 60);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mHandler = null;
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("fragment", fragment);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        mHandler = null;
        super.onStop();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                // get information from facebook
                case Constants.ACTION.ADD_CONTACT:
                    setActivityForResult(false);
                    setTakePhotoAndCrop(false);
                    //  business.insertNewContactFromIntentData(data);
                    ContentObserverBusiness.getInstance(this).setAction(-1);
                    break;
                case Constants.ACTION.EMAIL_CREATE:
                    setActivityForResult(false);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER:
                    ArrayList<String> list = data.getStringArrayListExtra("result");
                    Log.d(TAG, "TYPE_BLOCK_NUMBER size: " + list.size());
                    processListBlock(list);
                    break;
                default:
                    break;
            }
        } else {
            if (requestCode == Constants.ACTION.ADD_CONTACT) {
                ContentObserverBusiness.getInstance(this).setAction(-1);
            }
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void processListBlock(ArrayList<String> list) {
        ArrayList<BlockContactModel> listBlockModel = new ArrayList<>();
        ArrayList<String> listBlockInDb = mBlockListFragment.getListPhoneNumberContactBlock();
        if (list.isEmpty()) {
            for (String s : listBlockInDb) {
                listBlockModel.add(new BlockContactModel(s, BlockContactModel.STATE_UNBLOCK));
            }
        } else {
            if (listBlockInDb == null || listBlockInDb.isEmpty()) {
                for (String jidHash : list) {
                    listBlockModel.add(new BlockContactModel(jidHash, 1));
                }
            } else {
                HashSet<String> hashSetInDb = new HashSet<>(listBlockInDb);

                for (int i = 0; i < list.size(); i++) {
                    String jid = list.get(i);
                    if (!hashSetInDb.contains(jid)) { //neu list trong db ko co thi la new block
                        listBlockModel.add(new BlockContactModel(jid, BlockContactModel.STATE_BLOCK));
                    } else {
//                    listBlockModel.add(new BlockContactModel(jid, BlockContactModel.STATE_UNBLOCK));
                        hashSetInDb.remove(jid);
                        list.remove(i);
                        i--;
                    }
                }
                for (String s : hashSetInDb) {
                    listBlockModel.add(new BlockContactModel(s, BlockContactModel.STATE_UNBLOCK));
                }
            }
        }
        mBlockListFragment.updateBlockList(listBlockModel);

    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fragment = bundle.getInt(Constants.SETTINGS.DATA_FRAGMENT);
            threadMessage = (ThreadMessage) bundle.getSerializable(Constants.SETTINGS.DATA_THREAD);
            resetPin = bundle.getBoolean(Constants.SETTINGS.RESET_PIN);
        }
    }

    @Override
    public void onBackPressed() {
        if (mApplication.isSettingLanguage()) {
            mApplication.setSettingLanguage(false);
            goToHome();
        } else {
            if (fragment == Constants.SETTINGS.SETTING_HIDDEN_THREAD && resetPin) {
                String pin = mApplication.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
                if (TextUtils.isEmpty(pin)) goToHome();
                else super.onBackPressed();
            } else if (fragment == Constants.SETTINGS.SETTING_CONFIG_TAG && configTab != null) {
                if (configTab.isCurrentStateButton()) {
                    DialogConfirm dialogConfirm = DialogConfirm.newInstance(null,
                            getString(R.string.want_to_save_your_change_tab), DialogConfirm.CONFIRM_TYPE,
                            R.string.dont_save, R.string.save);
                    dialogConfirm.setSelectListener(
                            new BaseDialogFragment.DialogListener() {
                                @Override
                                public void dialogRightClick(int value) {
                                    configTab.onSaveList();
                                }

                                @Override
                                public void dialogLeftClick() {
                                    finish();
                                }
                            });
                    dialogConfirm.show(getSupportFragmentManager(), "");
//                    final DialogConfirm dialogConfirm = new DialogConfirm(this, true);
//                    dialogConfirm.setPositiveLabel(getString(R.string.save));
//                    dialogConfirm.setNegativeLabel(getString(R.string.dont_save));
//                    dialogConfirm.setMessage(getString(R.string.want_to_save_your_change_tab));
//                    dialogConfirm.setNegativeListener(new NegativeListener() {
//                        @Override
//                        public void onNegative(Object result) {
//                            dialogConfirm.dismiss();
//                            finish();
//                        }
//                    });
//                    dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
//                        @Override
//                        public void onPositive(Object result) {
//                            configTab.onSaveList();
//                        }
//                    });
//                    dialogConfirm.show();
                } else
                    super.onBackPressed();
            } else
                super.onBackPressed();
        }
    }

    ConfigTabHomeFragment configTab;

    @Override
    public void navigateToSettingDetail(int settingId) {
        Fragment fragmentSettingDetail = null;
        switch (settingId) {
            case Constants.SETTINGS.SETTING_NOTIFICATION:
                fragmentSettingDetail = SettingNotificationFragment.newInstance();
                break;
            case Constants.SETTINGS.SETTING_PRIVATE:
                fragmentSettingDetail = SettingPrivateFragment.newInstance();
                break;
            case Constants.SETTINGS.SETTING_CALL_MESSAGE:
                fragmentSettingDetail = SettingCallAndMessageFragment.newInstance();
                break;
            case Constants.SETTINGS.SETTING_IMAGE_SOUND:
                fragmentSettingDetail = SettingImageAndSoundFragment.newInstance();
                break;
            case Constants.SETTINGS.NOTE_MESSAGE:
                fragmentSettingDetail = NoteMessageFragment.newInstance();
                break;
            case Constants.SETTINGS.ABOUT:
                fragmentSettingDetail = AboutFragment.newInstance();
                break;
            case Constants.SETTINGS.SETTING_ACCOUNT:
                fragmentSettingDetail = AccountSettingFragment.newInstance();
                break;
        }
        if (fragmentSettingDetail != null) {
            executeFragmentTransitionWithAnimation(fragmentSettingDetail, R.id.fragment_container, true);
        }
    }


    @Override
    public void navigateToLockApp() {
        Intent lockApp = new Intent(getApplicationContext(), LockAppActivity.class);
        if (mApplication.getAppLockManager().isEnableSettingLockApp()) {
            lockApp.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.LOCK_APP.LOCK_APP_OPEN_SETTING);
        } else {
            lockApp.putExtra(Constants.SETTINGS.DATA_FRAGMENT, Constants.LOCK_APP.LOCK_APP_SETTING);
        }
        startActivity(lockApp, true);
    }

    public void navigateToAbout() {
        AboutFragment mAboutFragment = AboutFragment.newInstance();
        executeFragmentTransaction(mAboutFragment, R.id.fragment_container, false, false);
    }

    /*@Override
    public void navigateToDataPackDetail() {
        DataPackDetailFragment mDataPackDetailFragment = DataPackDetailFragment.newInstance();
        executeFragmentTransaction(mDataPackDetailFragment, R.id.fragment_container, true, true);
    }*/

    @Override
    public void inviteFriend() {
        NavigateActivityHelper.navigateToInviteFriends(SettingActivity.this, Constants.CHOOSE_CONTACT
                .TYPE_INVITE_FRIEND);
    }

    @Override
    public void navigateToWebView(String url) {
        UrlConfigHelper.openBrowser(this, url);
    }

    @Override
    public void navigateWebViewInApp(String url, int idTitle) {
        Fragment fragmentWebViewInApp = WebviewSettingFragment.newInstance(url, idTitle);
        executeFragmentTransitionWithAnimation(fragmentWebViewInApp, R.id.fragment_container, true);
    }

    @Override
    public void navigateToTranslation() {
        TranslationFragment frag = TranslationFragment.newInstance();
        executeFragmentTransaction(frag, R.id.fragment_container, true, true);
    }

    @Override
    public void addNewContact() {
        mApplication.getContactBusiness().navigateToAddContact(SettingActivity.this, null, null);
    }

    @Override
    public void navigateToReengChatActivity(String number) {
        ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread(number);
        NavigateActivityHelper.navigateToChatDetail(SettingActivity.this, thread);
    }

    @Override
    public void navigateToThreadDetail(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(SettingActivity.this, threadMessage);
    }

    @Override
    public void saveContact(String number, String name) {

    }

    @Override
    public void openListImage(ArrayList<ImageProfile> imageProfiles, String name, String jIdNumber) {

    }

    @Override
    public void deactiveAccount() {
        if (NetworkHelper.isConnectInternet(getApplicationContext())) {
            if (mApplication.getXmppManager().isAuthenticated()) {
                new DeactiveAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
            }
        } else {
            showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                if (NetworkHelper.isConnectInternet(getApplicationContext())) {
                    if (mApplication.getXmppManager().isAuthenticated()) {
                        new DeactiveAsyncTask().execute("deactive");
                    } else {
                        showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
                    }
                } else {
                    showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
                }
                break;
            case Constants.MENU.CONFIRM_SHARE_FACEBOOK:
                facebookHelper.shareContentToFacebook(this, getCallbackManager(), mApplication.getString(R.string.fb_share_url),
                        null, null, null, null);
                break;
            default:
                break;
        }
    }

    @Override
    public void navigateToBlockList() {
        mBlockListFragment = BlockListFragment.newInstance();
        executeFragmentTransitionWithAnimation(mBlockListFragment, R.id.fragment_container, true);
    }

    private void displaySettingFragment() {
        if (fragment == Constants.SETTINGS.CHANGE_STATUS) {
            EditStatusFragment mEditStatusFragment = EditStatusFragment.newInstance();
            executeFragmentTransaction(mEditStatusFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.LOGOUT) {
            SettingFragment mSettingFragment = SettingFragment.newInstance(Constants.SETTINGS.LOGOUT);
            executeFragmentTransaction(mSettingFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.TRANSLATE) {
            TranslationFragment mMoreAppFragment = TranslationFragment.newInstance();
            executeFragmentTransaction(mMoreAppFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.LIST_ROOM_MUSIC) {
            navigateToListRoomFragmnet();
        } else if (fragment == Constants.SETTINGS.ABOUT) {
            navigateToAbout();
        } else if (fragment == Constants.SETTINGS.PACKAGE_DETAIL) {
            DataPackDetailFragment mDataPackDetailFragment = DataPackDetailFragment.newInstance();
            executeFragmentTransaction(mDataPackDetailFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.NOTE_MESSAGE) {
            NoteMessageFragment mSettingFragment = NoteMessageFragment.newInstance();
            executeFragmentTransaction(mSettingFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.NEAR_YOU) {
            NearYouFragment nearYouFragment = NearYouFragment.newInstance(true);
            executeFragmentTransaction(nearYouFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.SETTING_CALL_MESSAGE) {
            SettingCallAndMessageFragment callMessageFragment = SettingCallAndMessageFragment.newInstance();
            executeFragmentTransaction(callMessageFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.SETTING_CONFIG_TAG) {
            configTab = ConfigTabHomeFragment.newInstance();
            executeFragmentTransaction(configTab, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.SETTING_CREATE_PIN_HIDE_THREAD) {
            PINSettingFragment pinSettingFragment = PINSettingFragment.newInstance(PINSettingFragment.TYPE_SET_NEW_PIN);
            executeFragmentTransaction(pinSettingFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.SETTING_HIDE_THREAD) {
            PINSettingFragment pinSettingFragment = PINSettingFragment.newInstance(PINSettingFragment.TYPE_SET_HIDE_THREAD);
            executeFragmentTransaction(pinSettingFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.SETTING_OPEN_HIDDEN_THREAD) {
            PINSettingFragment pinSettingFragment = PINSettingFragment.newInstance(PINSettingFragment.TYPE_OPEN_THREAD, threadMessage);
            executeFragmentTransaction(pinSettingFragment, R.id.fragment_container, false, false);
        } else if (fragment == Constants.SETTINGS.SETTING_HIDDEN_THREAD) {
            HideThreadSettingFragment fragment = HideThreadSettingFragment.newInstance();
            executeFragmentTransaction(fragment, R.id.fragment_container, false, false);
        } /*else if (fragment == Constants.SETTINGS.SETTING_MEDIA_BOX) {
            SettingMediaBoxFragment fragment = SettingMediaBoxFragment.newInstance();
            executeFragmentTransaction(fragment, R.id.fragment_container, false, false);
        }*/ else {
            SettingFragment mSettingFragment = SettingFragment.newInstance();
            executeFragmentTransaction(mSettingFragment, R.id.fragment_container, false, false);
        }
    }

    private void clearAndGotoHome() {
        SettingWorkManager.cancelSettingNotiWork();
        mApplication.recreateBusiness();
        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(i);
    }

    public void navigateToNumberList() {
        Intent intent = new Intent(getApplicationContext(), ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER);
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, mBlockListFragment
                .getListPhoneNumberContactBlock());
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_BLOCK_NUMBER, true);
        trackingEvent(R.string.ga_category_home, R.string.ga_action_click_new_group, R.string
                .ga_action_click_new_group);
    }

    @Override
    public void navigateToSearchFragmnet() {
        SearchRoomFragment mSearchRoomFragment = SearchRoomFragment.newInstance();
        executeFragmentTransaction(mSearchRoomFragment, R.id.fragment_container, false, true);
    }

    @Override
    public void navigateToBackup() {
        Intent backupIntent = new Intent(this, BackupActivity.class);
        startActivity(backupIntent, true);
    }

    @Override
    public void navigateToHideThread() {
        HideThreadSettingFragment fragment = HideThreadSettingFragment.newInstance();
        executeFragmentTransitionWithAnimation(fragment, R.id.fragment_container, true);
    }

    @Override
    public void navigateToListRoomFragmnet() {
        ListRoomFragment mListRoomFragment = ListRoomFragment.newInstance();
        executeFragmentTransaction(mListRoomFragment, R.id.fragment_container, false, true);
    }

    public class DeactiveAsyncTask extends AsyncTask<String, String, Boolean> {
        private ProgressDialog pDialog;
        private XMPPManager mXmppManager;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mXmppManager = ((ApplicationController) getApplication()).getXmppManager();
            pDialog = new ProgressDialog(SettingActivity.this);
            pDialog.setTitle(getString(R.string.app_name));
            pDialog.setMessage(getString(R.string.msg_waiting));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... action) {
            try {
                //    mFacebookBusiness.closeSession();
                if (userInfoBusiness.getUser() != null && !StringUtils.isEmpty(String.valueOf(userInfoBusiness.getUser().getUser_id()))) {
                    String language = LocaleManager.getLanguage(getApplicationContext());
                    MetfonePlusClient.getInstance().updateDeviceStatus(String.valueOf(userInfoBusiness.getUser().getUser_id()), "0", language, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<BaseResponse>() {
                        @Override
                        public void onResponse(Response<BaseResponse> response) {
                        }

                        @Override
                        public void onError(Throwable error) {
                        }
                    });
                }
                mXmppManager.removeXmppListener();
                mAccountBusiness.deactivateAccount(getApplicationContext());
                userInfoBusiness.clearCache();
                return true;
            } catch (XMPPException xe) {
                Log.e(TAG, "XMPPException", xe);
                return false;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if (result) {
//                ReengNotificationManager.clearMessagesNotification(getApplicationContext(),
//                        Constants.NOTIFICATION.NOTIFY_MESSAGE);
//                ReengNotificationManager.clearMessagesNotification(getApplicationContext(),
//                        Constants.NOTIFICATION.NOTIFY_ONMEDIA);
//                ReengNotificationManager.clearMessagesNotification(getApplicationContext(),
//                        Constants.NOTIFICATION.NOTIFY_OTHER);
                mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_MESSAGE);
                mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_ONMEDIA);
                mApplication.cancelNotification(Constants.NOTIFICATION.NOTIFY_OTHER);
                ((ApplicationController) getApplication()).getXmppManager().destroyXmpp();
                mApplication.removeCountNotificationIcon();
                mApplication.setRegisterSmsOtp(false);
                //logout facebook + reinit
                if (!Config.Server.FREE_15_DAYS) {
                    LoginManager.getInstance().logOut();
                }
                // reset music
                mApplication.getMusicBusiness().clearSessionAndNotifyMusic();
                clearAndGotoHome();
                // restartApplication();
            } else {
                showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
                // add xmpp listener
                mXmppManager.addXmppListener();
            }
        }
    }

    public void navigateToChangePINHideThread() {
        PINSettingFragment pinSettingFragment = PINSettingFragment.newInstance(PINSettingFragment.TYPE_CHANGE_PIN);
        executeFragmentTransaction(pinSettingFragment, R.id.fragment_container, true, false);
    }
}
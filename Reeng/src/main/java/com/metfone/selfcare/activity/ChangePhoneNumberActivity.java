package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.ChangePhoneAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.ServicesModel;
import com.metfone.selfcare.model.account.UpdateLoginInfo;
import com.metfone.selfcare.model.account.UpdateLoginRequest;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.RefreshTokenResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.jivesoftware.smack.Connection;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_REFRESH_TOKEN;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

public class ChangePhoneNumberActivity extends BaseSlidingFragmentActivity {
    Unbinder unbinder;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.btnAddAnother)
    AppCompatTextView btnAddAnother;
    @BindView(R.id.btnDone)
    AppCompatTextView btnDone;
    @BindView(R.id.rvChangeNumber)
    RecyclerView rvChangeNumber;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    UserInfoBusiness userInfoBusiness;
    UserInfo currentUser;
    ServicesModel model;
    ArrayList<ServicesModel> servicesModelList;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private ChangePhoneAdapter changePhoneAdapter;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone_number);
        unbinder = ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplication();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        userInfoBusiness = new UserInfoBusiness(this);
        servicesModelList = new ArrayList<>();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        btnDone.setBackground(ContextCompat.getDrawable(this, R.drawable.bgr_btn_gery));
        initView();
    }

    private void initView() {
        changePhoneAdapter = new ChangePhoneAdapter(this);
        rvChangeNumber.setAdapter(changePhoneAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        currentUser = userInfoBusiness.getUser();
        getData();
    }

    @Optional
    @OnClick({R.id.btnBack, R.id.btnDone, R.id.btnAddAnother})
    public void onClick(View view) {
        // ...
        switch (view.getId()) {
            case R.id.btnAddAnother:
                Intent intent = new Intent(ChangePhoneNumberActivity.this, EnterChangePhoneNumberActivity.class);
                startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
                break;
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnDone:
//                doLogout();
                ServicesModel model = servicesModelList.get(changePhoneAdapter.getLastSelectedPosition());
//                String oldPhoneNumber = currentUser.getPhone_number();
                updateLogin(model.getPhone_number(), model.getUser_service_id());
                break;
        }
    }

    /**
     * Change phone number
     *
     * @param phoneNumber
     * @param userServiceId
     */
    private void updateLogin(String phoneNumber, int userServiceId) {
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        UpdateLoginInfo updateLoginInfo = new UpdateLoginInfo(phoneNumber, userServiceId);
        UpdateLoginRequest updateLoginRequest = new UpdateLoginRequest(updateLoginInfo, "", "", "", "");
        apiService.updateLogin(token, updateLoginRequest).enqueue(new Callback<BaseResponse<BaseUserResponseData>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseUserResponseData>> call, Response<BaseResponse<BaseUserResponseData>> response) {
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    BaseResponse<BaseUserResponseData> dataBaseResponse = response.body();
                    if ("00".equals(dataBaseResponse.getCode())) {
                        UserInfo userInfo = dataBaseResponse.getData().getUser();
                        userInfoBusiness.setServiceList(dataBaseResponse.getData().getServices());
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        mCurrentRegionCode = "KH";
                        if (userInfo != null) {
                            userInfoBusiness.setUser(userInfo);
                            ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                              reengAccount.setToken(token);
                            if (reengAccount != null) {
                                reengAccount.setName(userInfo.getFull_name());
                                reengAccount.setEmail(userInfo.getEmail());
                                reengAccount.setBirthday(userInfo.getDate_of_birth());
                                reengAccount.setAddress(userInfo.getAddress());
                                mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                            }
                            if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                                if (userInfo.getPhone_number().startsWith("0")) {
                                    mCurrentNumberJid = "+855" + userInfo.getPhone_number().substring(1);
                                } else {
                                    mCurrentNumberJid = "+855" + userInfo.getPhone_number();
                                }
                            } else {
                                ToastUtils.showToast(ChangePhoneNumberActivity.this, "Phone number null -> Login old mocha failed!");
                            }
                        }
                        String refreshToken = SharedPrefs.getInstance().get(PREF_REFRESH_TOKEN, String.class);
                        refreshToken(refreshToken);
                        //case full service
                    } else if (dataBaseResponse.getCode().equals("61")) {
                        baseMPSuccessDialog = new BaseMPSuccessDialog(ChangePhoneNumberActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), String.format(getString(R.string.content_error_social_combine), phoneNumber.substring(0, 2) + " " + phoneNumber.substring(2)), false);
                        baseMPSuccessDialog.show();
                        getSupportFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    ChangePhoneNumberActivity.this.finish();
                                }
                            });
                        }
                        ToastUtils.showToast(ChangePhoneNumberActivity.this, dataBaseResponse.getMessage());
                    } else {
                        baseMPSuccessDialog = new BaseMPSuccessDialog(ChangePhoneNumberActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), dataBaseResponse.getMessage(), false);
                        baseMPSuccessDialog.show();
                        getSupportFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    ChangePhoneNumberActivity.this.finish();
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseUserResponseData>> call, Throwable t) {
                ToastUtils.showToast(ChangePhoneNumberActivity.this, getString(R.string.service_error));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EnumUtils.MY_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            int result = data.getIntExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(EnumUtils.RESULT, result);
            setResult(Activity.RESULT_OK, returnIntent);
            android.util.Log.d(TAG, "onActivityResult: " + result);
            if (result == EnumUtils.KeyBackTypeDef.DONE) {
                finish();
            }
        }
    }


    private void getData() {
        List<Services> services = userInfoBusiness.getServiceList();
        servicesModelList = UserInfoBusiness.convertServicesToServiceModelList(services);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ArrayList<ServicesModel> mobileServicesModels = getMobileServiceList(servicesModelList);
//                    new ArrayList<>(servicesModelList.stream().filter(s -> "MOBILE".equals(s.getService_code())).collect(Collectors.toList()));
            changePhoneAdapter.setItems(mobileServicesModels);
            if (mobileServicesModels.size() > 0 && currentUser.getPhone_number() != null) {
                changePhoneAdapter.setLastSelectedPosition(foundIndex(currentUser.getPhone_number()));
                changePhoneAdapter.notifyDataSetChanged();
            }
        }
    }

    public ArrayList<ServicesModel> getMobileServiceList(ArrayList<ServicesModel> servicesModelList) {
        ArrayList<ServicesModel> list = new ArrayList<>();
        for (ServicesModel model : servicesModelList) {
            if ("MOBILE".equals(model.getService_code())) {
                list.add(model);



                String phoneNumber = null;
                if (userInfoBusiness.getUser() != null) {
                    phoneNumber = userInfoBusiness.getUser().getPhone_number();
                }
                if (phoneNumber != null && model.getPhone_number() != null) {
                    String phoneService = model.getPhone_number().substring(0, 2).concat(model.getPhone_number().substring(2));
                    Log.d("TAG", "bindView: " + phoneNumber.concat(" ").concat(phoneService));
                    if (!phoneService.equals(phoneNumber)) {
                        btnDone.setEnabled(true);
                        btnDone.setBackground(ContextCompat.getDrawable(this, R.drawable.bgr_btn_confirm));
                    } else if (servicesModelList.size() < 2) {
                        btnDone.setEnabled(false);
                        btnDone.setBackground(ContextCompat.getDrawable(this, R.drawable.bgr_btn_gery));
                    } else {
                        btnDone.setEnabled(true);
                        btnDone.setBackground(ContextCompat.getDrawable(this, R.drawable.bgr_btn_confirm));
                    }


                }
            }
        }
        return list;
    }

    private int foundIndex(String phoneNumber) {
        try {
            for (int i = 0; i < servicesModelList.size(); i++) {
                if (servicesModelList.get(i).getPhone_number().equals(phoneNumber)) {
                    return i;
                }
            }
        } catch (Exception ex) {
            return 0;
        }
        return 0;
    }

    private void refreshToken(String token) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.refreshToken(token, new ApiCallback<RefreshTokenResponse>() {
            @Override
            public void onResponse(Response<RefreshTokenResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() == null) {

                    Toast.makeText(ChangePhoneNumberActivity.this, "Refresh Token Failed", Toast.LENGTH_LONG).show();
                } else {
                    if (response.body().getCode().equals("00")) {
                        String token = "Bearer " + response.body().getData().getAccessToken();
                        SharedPrefs.getInstance().put(PREF_ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(PREF_REFRESH_TOKEN, response.body().getData().getRefreshToken());
                        generateOldMochaOTP(response.body().getData().getAccessToken());

                    } else {
                        Toast.makeText(ChangePhoneNumberActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Toast.makeText(ChangePhoneNumberActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void doLogout() {
        if (NetworkHelper.isConnectInternet(getApplicationContext())) {
            if (mApplication.getXmppManager().isAuthenticated()) {
//                new ChangePhoneNumberActivity.DeactiveAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
            }
        } else {
            showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
        }
    }

    private void generateOldMochaOTP(String token) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(ChangePhoneNumberActivity.this);
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(ChangePhoneNumberActivity.this).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(ChangePhoneNumberActivity.this, response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(ChangePhoneNumberActivity.this, "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(ChangePhoneNumberActivity.this, error.getMessage());
            }
        });

    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new ChangePhoneNumberActivity.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);

            try {
//                mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    //navigate next
                    BaseMPSuccessDialog baseMPSuccessDialog = new BaseMPSuccessDialog(ChangePhoneNumberActivity.this,
                        R.drawable.img_dialog_1, R.string.successfully,
                        String.format(getString(R.string.content_link_update_success),
                            "0" + mCurrentNumberJid.substring(4, 6) + " " + mCurrentNumberJid.substring(6)));
                    baseMPSuccessDialog.show();
                    getSupportFragmentManager().executePendingTransactions();
                    baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            finish();
                        }
                    });
//                    String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
//                    String otp = getArguments().getString(EnumUtils.OTP_KEY);
//                    String pass = edtPassWord.getEditText().getText().toString();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
//                    if (tvError != null) {
//                        tvError.setVisibility(View.GONE);
//                    }
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
//                    showError(responseCode.getDescription(), null);
//                    if (tvError != null) {
//                        tvError.setVisibility(View.VISIBLE);
//                    }
//                    getResourceOfCode(responseCode.getCode());
                    ToastUtils.showToast(ChangePhoneNumberActivity.this, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                ToastUtils.showToast(ChangePhoneNumberActivity.this, e.getMessage());
                hideLoadingDialog();
            }
        }
    }
}
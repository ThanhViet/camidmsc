package com.metfone.selfcare.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.fragment.app.Fragment;
import android.view.Menu;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.contact.ContactListFragment;
import com.metfone.selfcare.fragment.contact.ContactWithActionFragment;
import com.metfone.selfcare.fragment.contact.ListFriendMochaFragment;
import com.metfone.selfcare.fragment.contact.OfficerListFragment;
import com.metfone.selfcare.fragment.contact.SocialFriendFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.listeners.ContactListInterface;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 9/30/14.
 */
public class ContactListActivity extends BaseSlidingFragmentActivity implements
        ContactListInterface,
        OfficerListFragment.OnFragmentInteractionListener {
    private final String TAG = ContactListActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private ContactBusiness mContactBusiness;
    private String mContactIdEdit;
    private int fragment = -1;
    private int tabPos = 0;
    private boolean runSocialSearch = true;

    private boolean isAllowContactPermission;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, " onCreate ... ");
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mContactBusiness = mApplication.getContactBusiness();
        setWhiteStatusBar();
        setContentView(R.layout.activity_detail);
        findComponentViews();
        setComponentViews();
        getData();
        if (savedInstanceState == null) {
            displayFragment();
        }
        trackingScreen(TAG);

        isAllowContactPermission = PermissionHelper.checkPermissionContact(this);
    }

    private void displayFragment() {
        if (fragment == Constants.CONTACT.FRAG_LIST_UTIL) {
            displayOfficerListFragment(false);
        } else if (fragment == Constants.CONTACT.FRAG_LIST_SOCIAL_REQUEST) {
            displaySocialRequestFragment();
        } else if (fragment == Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION) {
            displayContactListWithActionFragment();
        } else if (fragment == Constants.CONTACT.FRAG_LIST_FRIEND_MOCHA) {
            displayFriendMochaFragment();
        } else {
            displayContactListFragment();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isAllowContactPermission) {
            if (PermissionHelper.onlyCheckPermissionContact(getApplicationContext())) {
                isAllowContactPermission = true;
                mApplication.reLoadContactAfterPermissionsResult();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.PERMISSION.PERMISSION_CONTACT) {
            if (PermissionHelper.onlyCheckPermissionContact(getApplicationContext())) {
                isAllowContactPermission = true;
                mApplication.reLoadContactAfterPermissionsResult();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onDestroy: ");
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
        setActivityForResult(false);
        setTakePhotoAndCrop(false);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case Constants.ACTION.ADD_CONTACT:
                    //  business.insertNewContactFromIntentData(data);
                    ContentObserverBusiness.getInstance(this).setAction(-1);
                    break;
                case Constants.ACTION.EDIT_CONTACT:
                    if (mContactIdEdit == null) {
                        break;
                    }
                    Contact contact = mContactBusiness.getContactFromContactId(mContactIdEdit);
                    mContactBusiness.updateContactFromIntentData(data, contact);
                    mContactBusiness.initArrayListPhoneNumber();
                    mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
                    ContentObserverBusiness.getInstance(this).setAction(-1);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE:
                    String phoneId = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_PHONE_ID);
                    mContactBusiness.changeFavorite(
                            (mContactBusiness.getPhoneNumberFromPhoneId(phoneId)).getContactId(), true);
                    Log.d(TAG, "requestCode: " + requestCode + " phoneId: " + phoneId);
                    break;
                default:
                    break;
            }
        } else {
            Log.d(TAG, "DATA null: ");
            if (requestCode == Constants.ACTION.ADD_CONTACT ||
                    requestCode == Constants.ACTION.EDIT_CONTACT) {
                ContentObserverBusiness.getInstance(this).setAction(-1);
            }
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        boolean closeSearch = false;
        if (fragment != null) {
            if (fragment instanceof ContactListFragment) {
                closeSearch = ((ContactListFragment) fragment).backPressListener();
            }
        }
        Log.d(TAG, "CloseSearch" + closeSearch);
        if (!closeSearch) {
            super.onBackPressed();
        }
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fragment = bundle.getInt(Constants.CONTACT.DATA_FRAGMENT);
            tabPos = bundle.getInt("tab_position");
        }
    }

    private void findComponentViews() {
    }

    private void setComponentViews() {
        setToolBar(findViewById(R.id.tool_bar));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Create the search view
        /*getSupportMenuInflater().inflate(R.menu.contact_list, menu);
        menu.findItem(R.id.menu_contact_search).setVisible(false);*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void editContactActivity(String contactId) {
        if (PermissionHelper.declinedPermission(this, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(ContactListActivity.this,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
        } else {
            mContactIdEdit = contactId;
            ContentObserverBusiness.getInstance(this).setAction(Constants.ACTION.EDIT_CONTACT);
            Intent editContact = new Intent(Intent.ACTION_EDIT);
            editContact.setData(Uri.parse(ContactsContract.Contacts.CONTENT_URI + "/" + contactId));
            editContact.putExtra("finishActivityOnSaveCompleted", true);
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(editContact, Constants.ACTION.EDIT_CONTACT, true);
        }
    }

    @Override
    public void addNewContactActivity() {
        mApplication.getContactBusiness().navigateToAddContact(ContactListActivity.this, null, null);
    }

    @Override
    public void navigateToAddFavarite() {
        Intent intent = new Intent(getApplicationContext(), ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE, true);
    }

    @Override
    public void navigateToInviteFriendActivity(String number) {
        ArrayList<String> listChecked = new ArrayList<>();
        if (number != null) {
            listChecked.add(number);
        }
        Intent intent = new Intent(getApplicationContext(), ChooseContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(Constants.CHOOSE_CONTACT.DATA_MEMBER, listChecked);
        bundle.putInt(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.CHOOSE_CONTACT.TYPE_INVITE_FRIEND, true);
    }

    @Override
    public void navigateToThreadDetail(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(ContactListActivity.this, threadMessage);
    }

    private void displayContactListFragment() {
        // Create an instance of newInstance
        ContactListFragment mContactListFragment = ContactListFragment.newInstance();
        executeFragmentTransaction(mContactListFragment, R.id.fragment_container, false, false);
    }

    private void displayContactListWithActionFragment() {
        // Create an instance of newInstance
        ContactWithActionFragment mContactListFragment = ContactWithActionFragment.newInstance(Constants.CONTACT.FRAG_LIST_CONTACT_WITH_ACTION);
        executeFragmentTransaction(mContactListFragment, R.id.fragment_container, false, false);
    }

    private void displayFriendMochaFragment() {
        executeFragmentTransaction(ListFriendMochaFragment.create(), R.id.fragment_container, false, false);
    }

    private void displaySocialRequestFragment() {
        SocialFriendFragment fragment;
        fragment = SocialFriendFragment.newInstance();
        executeFragmentTransaction(fragment, R.id.fragment_container, false, true);
    }

    public boolean isRunSocialSearch() {
        return runSocialSearch;
    }

    public void setRunSocialSearch(boolean runSocialSearch) {
        this.runSocialSearch = runSocialSearch;
    }

    private void displayOfficerListFragment(boolean addBackStack) {
        OfficerListFragment officerListFragment = OfficerListFragment.newInstance(Constants.CONTACT.SHOW_LIST_GROUP);
        executeFragmentTransaction(officerListFragment, R.id.fragment_container, addBackStack, addBackStack);
    }
}
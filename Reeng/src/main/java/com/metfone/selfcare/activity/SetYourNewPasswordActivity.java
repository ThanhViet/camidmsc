package com.metfone.selfcare.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.account.AddLoginRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.PhoneLinkedInfo;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.RestoreModel;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.util.OpenIDErrorCode.ERROR_CODE_00;
import static com.metfone.selfcare.util.OpenIDErrorCode.ERROR_CODE_01;
import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

public class SetYourNewPasswordActivity extends BaseSlidingFragmentActivity implements View.OnClickListener, View.OnFocusChangeListener {
    private AppCompatImageView ivBack;
    private RoundLinearLayout rledtPassword, rledtConfirmPassword;
    private RelativeLayout rlSetpass;
    private ProgressBar pbLoading;
    private CamIdTextView tvTermsAndConditions, tvCreateAccount, tvEnterNumberDes;
    private RoundTextView btnConfirm;
    private FrameLayout flSecondPinView, flPinViewPassword;
    boolean isPasswordVisible = false;
    boolean isConfirmPasswordVisible = false;
    private ImageView ivPassVisible, ivConfirmPassVisible;
    private String phoneNumber;
    private int checkPhoneCode;
    private String otp;
    private BaseMPSuccessDialog baseMPSuccessDialog;
    private BaseMPSuccessDialog errorOtpDialog;
    private ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private PinView pinPassword, pinConfirmPassword;
    private PinView hintPinView, hintConfirmPinView;
    private ApplicationController mApplication;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;
    private List<Services> services;
    private UserInfoBusiness userInfoBusiness;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_your_new_password);
        initData();
        initViews();
    }

    private void initData() {
        userInfoBusiness = new UserInfoBusiness(SetYourNewPasswordActivity.this);
        new Handler().postDelayed(this::detectKeyboard, 500);
    }

    private void initViews() {
        setColorStatusBar(R.color.white);
        mApplication = (ApplicationController) getApplicationContext();
        rledtPassword = findViewById(R.id.rledtPassword);
        rledtConfirmPassword = findViewById(R.id.rledtConfirmPassword);
        rlSetpass = findViewById(R.id.rlSetpass);
        pbLoading = findViewById(R.id.pbLoading);
        tvCreateAccount = findViewById(R.id.tvCreateAccount);
        tvEnterNumberDes = findViewById(R.id.tvEnterNumberDes);
        ivBack = findViewById(R.id.ivBack);
        tvTermsAndConditions = findViewById(R.id.tvTermsAndConditions);
        btnConfirm = findViewById(R.id.btnConfirm);
        ivPassVisible = findViewById(R.id.imgPassVisible);
        ivConfirmPassVisible = findViewById(R.id.imgConfirmPassVisible);
        flSecondPinView = findViewById(R.id.flSecondPinView);
        flPinViewPassword = findViewById(R.id.flPinViewPassword);
        ivBack.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        phoneNumber = getIntent().getStringExtra(EnumUtils.PHONE_NUMBER_KEY);
        checkPhoneCode = getIntent().getIntExtra(EnumUtils.CHECK_PHONE_CODE_TYPE, 0);
        otp = getIntent().getStringExtra(EnumUtils.OTP_KEY);
        pinPassword = findViewById(R.id.pinPassword);
        hintPinView = findViewById(R.id.hintPinView);
        pinConfirmPassword = findViewById(R.id.pinConfirmPassword);
        hintConfirmPinView = findViewById(R.id.hintConfirmPinView);
        pinPassword.setPasswordHidden(true);
        pinConfirmPassword.setPasswordHidden(true);
        ivPassVisible.setOnClickListener(view1 -> {
            isPasswordVisible = !isPasswordVisible;
            pinPassword.setPasswordHidden(!isPasswordVisible);
            hintPinView.setVisibility(isPasswordVisible ? View.GONE : View.VISIBLE);
            if (isPasswordVisible) {
                ivPassVisible.setImageResource(R.drawable.ic_visibility_24px_outlined);
            } else {
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                    ivPassVisible.setImageResource(R.drawable.ic_visibility_password);
                } else {
                    ivPassVisible.setImageResource(R.drawable.ic_visibility_off_24px_outlined);
                }
            }
        });
        ivConfirmPassVisible.setOnClickListener(view2 -> {
            isConfirmPasswordVisible = !isConfirmPasswordVisible;
            pinConfirmPassword.setPasswordHidden(!isConfirmPasswordVisible);
            hintConfirmPinView.setVisibility(isConfirmPasswordVisible ? View.GONE : View.VISIBLE);
            if (isConfirmPasswordVisible) {
                ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_24px_outlined);
            } else {
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                    ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_password);
                } else {
                    ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_off_24px_outlined);
                }
            }
        });
        pinPassword.setOnFocusChangeListener(this);
        pinPassword.setOnClickListener(this);
        pinConfirmPassword.setOnClickListener(this);
        pinConfirmPassword.setOnFocusChangeListener(this);
        flSecondPinView.setOnClickListener(this);
        flPinViewPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pinPassword:
            case R.id.flSecondPinView:
                flSecondPinView.requestFocus();
                flPinViewPassword.clearFocus();
                break;
            case R.id.pinConfirmPassword:
            case R.id.rledtConfirmPassword:
                flSecondPinView.clearFocus();
                flPinViewPassword.requestFocus();
                break;
            case R.id.ivBack:
                Intent returnIntent = new Intent();
                returnIntent.putExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                break;
            case R.id.btnConfirm:
                //Validate
                if (TextUtils.isEmpty(pinPassword.getText().toString()) || TextUtils.isEmpty(pinConfirmPassword.getText().toString())) {
                    showDialog(getString(R.string.please_fill_all_the_fields));
                } else if (!pinPassword.getText().toString().equals(pinConfirmPassword.getText().toString())) {
                    showDialog(getString(R.string.confirm_password));
                } else if (pinPassword.getText().toString().length() != 6) {
                    showDialog(getString(R.string.password_must_have));
                } else {
//                    checkPhone();
                    addLogin(phoneNumber, otp, pinConfirmPassword.getText().toString());

                }
                break;
        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.txt_title_error));
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        pinPassword.requestFocus();
        UIUtil.showKeyboard(getContext(), pinPassword);
        setColorpassword();
        setupUI(rlSetpass);

    }

    private void successDialogDismissListener() {
        getSupportFragmentManager().executePendingTransactions();
        if (baseMPSuccessDialog != null) {
            baseMPSuccessDialog.setOnDismissListener(dialogInterface -> autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                @Override
                public void onStartDownload() {

                }

                @Override
                public void onDownloadProgress(int percent) {

                }

                @Override
                public void onDownloadComplete() {

                }

                @Override
                public void onDowloadFail(String message) {

                }

                @Override
                public void onStartRestore() {

                }

                @Override
                public void onRestoreProgress(int percent) {

                }

                @Override
                public void onRestoreComplete(int messageCount, int threadMessageCount) {
                    try {
                        SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                        if (pref != null) {
                            pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                            com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                        }
                    } catch (Exception e) {
                    }
                    RestoreManager.setRestoring(false);
                    mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                    mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                    mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                    navigateNextScreen();
                }

                @Override
                public void onRestoreFail(String message) {
                    RestoreManager.setRestoring(false);
                    navigateNextScreen();
                }

            }));
        }
    }

    private void navigateNextScreen() {
        UserInfo currentUser = userInfoBusiness.getUser();

        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY && !UserInfoBusiness.checkEmptyPhoneNumber(SetYourNewPasswordActivity.this) && userInfoBusiness.getIdentifyProviderList() != null && userInfoBusiness.getIdentifyProviderList().size() > 1) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.DONE);
            setResult(Activity.RESULT_OK, returnIntent);
            NavigateActivityHelper.navigateToShareAndGetMoreActivity(SetYourNewPasswordActivity.this);
            finish();
        } else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.DONE);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }


    }

    public void autoBackUp(ApplicationController mApplication, DBImporter.RestoreProgressListener listener) {
        Context context = mApplication.getApplicationContext();
        SharedPreferences mPref = context.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        if (NetworkHelper.isConnectInternet(context)) {
            if (Config.Features.FLAG_SUPPORT_BACKUP_MESSAGES && mPref != null && !mPref.getBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, false)) {
                RestoreManager.getBackupFileInfo(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        try {
                            Gson gson = new Gson();
                            RestoreModel restoreModel = gson.fromJson(data, RestoreModel.class);
                            if (restoreModel != null && restoreModel.getLstFile() != null && !restoreModel.getLstFile().isEmpty()) {
                                //restore
                                int lastIndex = 0;
                                RestoreModel.FileInfo fileInfo = restoreModel.getLstFile().get(lastIndex);
                                mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, data).apply();
                                if (NetworkHelper.isConnectInternet(context)) {
                                    RestoreManager.restoreMessages(listener, UrlConfigHelper.getInstance(mApplication).getDomainFile() + "/" + fileInfo.getPath());
                                } else {
                                    Toast.makeText(ApplicationController.self(), context.getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                //skip
                                RestoreManager.setRestoring(false);
                                navigateNextScreen();
                            }
                        } catch (Exception e) {
                            //skip
                            RestoreManager.setRestoring(false);
                            navigateNextScreen();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        RestoreManager.setRestoring(false);
                        com.metfone.selfcare.util.Log.i("TAG", "getBackupFileInfo: fail " + message);
                        navigateNextScreen();
                    }
                });
            } else {
                RestoreManager.setRestoring(false);
                navigateNextScreen();
            }
        } else {
            ToastUtils.showToast(SetYourNewPasswordActivity.this, context.getString(R.string.no_connectivity_check_again));
        }
    }

    public void addLogin(String phoneNumber, String otp, String password) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        AddLoginRequest request = new AddLoginRequest(phoneNumber, otp, password);
        BaseRequest<AddLoginRequest> baseRequest = new BaseRequest<>();
        baseRequest.setWsRequest(request);
        String json = new Gson().toJson(baseRequest);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);

        Call<GetUserResponse> call = apiService.addLogin(token, requestBody);
        call.enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboardPassword(getContext(), flPinViewPassword);
                        UIUtil.showKeyboardPassword(getContext(), flSecondPinView);
                    }
                }, 200);
                if (response.body() != null && response.body().getCode() != null) {
                    String code = response.body().getCode();
                    if (ERROR_CODE_00.equals(code)) {
                        handleAddLoginSuccess(response, token);
                    } else {

                        if (ERROR_CODE_01.equals(code)) {
                            showErrorOtpDialog(response);
                        } else {
                            showErrorDialog(response);
                        }

                    }
                } else {
                    ToastUtils.showToast(SetYourNewPasswordActivity.this, getString(R.string.add_login_fail));
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(SetYourNewPasswordActivity.this,getString(R.string.service_error));
            }
        });
    }

    private void handleAddLoginSuccess(Response<GetUserResponse> response, String token) {
        UserInfo userInfo = response.body().getData().getUser();
        if (response.body().getData().getServices() != null) {
            userInfoBusiness.setServiceList(response.body().getData().getServices());
            services = response.body().getData().getServices();
        }
        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
        mCurrentRegionCode = "KH";
        if (userInfo != null) {
            userInfoBusiness.setUser(userInfo);
            ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
            if (reengAccount != null) {
                reengAccount.setName(userInfo.getFull_name());
                reengAccount.setEmail(userInfo.getEmail());
                reengAccount.setBirthday(userInfo.getDate_of_birth());
                reengAccount.setAddress(userInfo.getAddress());
                mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
            }
            if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                if (userInfo.getPhone_number().startsWith("0")) {
                    mCurrentNumberJid = "+855" + userInfo.getPhone_number().substring(1);
                } else {
                    mCurrentNumberJid = "+855" + userInfo.getPhone_number();
                }
                String originToken = token.substring(7);
                generateOldMochaOTP(originToken);
            } else {
                ToastUtils.showToast(SetYourNewPasswordActivity.this, getString(R.string.add_login_fail));
            }
        }
    }

    private void showErrorDialog(Response<GetUserResponse> response) {
        // 01-14-22 Fix crash bug when set new password failed
        baseMPSuccessDialog = new BaseMPSuccessDialog(SetYourNewPasswordActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
            baseMPSuccessDialog.setBgWhite(false);
        }
        if (!SetYourNewPasswordActivity.this.isFinishing()) {
            baseMPSuccessDialog.show();
        }
    }

    private void showErrorOtpDialog(Response<GetUserResponse> response) {
        errorOtpDialog = new BaseMPSuccessDialog(SetYourNewPasswordActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
            errorOtpDialog.setBgWhite(false);
        }
        errorOtpDialog.show();
        errorOtpDialog.setOnDismissListener(listener -> {
            finish();
        });
    }

    public void setColorpassword() {
        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY
                || ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
            //    changeStatusBar(Color.parseColor("#1F1F1F"));
            setColorStatusBar(R.color.color_rlContainer);
            setSystemBarTheme(SetYourNewPasswordActivity.this, true);
            ivBack.setImageResource(R.drawable.ic_navigate_back);
            tvCreateAccount.setTextColor(Color.WHITE);
            tvEnterNumberDes.setTextColor(Color.WHITE);
            rlSetpass.setBackgroundColor(Color.parseColor("#1F1F1F"));
            pinPassword.setTextColor(Color.parseColor("#CCFFFFFF"));
            pinConfirmPassword.setTextColor(Color.parseColor("#CCFFFFFF"));
            pinPassword.setCursorColor(Color.parseColor("#FFFFFF"));
            hintPinView.setCursorColor(getResources().getColor(R.color.color_password));
            pinConfirmPassword.setCursorColor(Color.parseColor("#FFFFFF"));
            hintConfirmPinView.setCursorColor(getResources().getColor(R.color.color_password));
            ivConfirmPassVisible.setImageResource(R.drawable.ic_visibility_password);
            ivPassVisible.setImageResource(R.drawable.ic_visibility_password);
            rledtPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.m_p_base_grey), 0);
            rledtConfirmPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.m_p_base_grey), 0);
            rledtPassword.setBackgroundColorRound(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.m_p_base_grey));
            rledtConfirmPassword.setBackgroundColorRound(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.m_p_base_grey));
        }

    }

    private void generateOldMochaOTP(String token) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(SetYourNewPasswordActivity.this);
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(SetYourNewPasswordActivity.this).detectSubscription(json);
                        doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(SetYourNewPasswordActivity.this, response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(SetYourNewPasswordActivity.this, "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(SetYourNewPasswordActivity.this, error.getMessage());
            }
        });

    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new SetYourNewPasswordActivity.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(SetYourNewPasswordActivity.this);
                    return v.performClick();
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText || innerView instanceof ImageView) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    private void showSuccessDialog() {
        //navigate next
        String titleDialog = getString(R.string.successfully);
        if (checkPhoneCode == 42) {
            titleDialog = getString(R.string.success_combining);
        }
        baseMPSuccessDialog = new BaseMPSuccessDialog(SetYourNewPasswordActivity.this,
                R.drawable.img_dialog_1, titleDialog, String.format(getString(R.string.content_dialog_success_add_login),
                phoneNumber.substring(0, 3) + " " + phoneNumber.substring(3)), false);
        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
            baseMPSuccessDialog.setBgWhite(true);
        }
        baseMPSuccessDialog.show();
        successDialogDismissListener();
    }

    private void updatePhoneToPhoneService() {
        int userServiceID = -1;
        for (Services s : services) {
            List<PhoneLinkedInfo> phoneLinkeds = s.getPhoneLinkedList();
            for (int i = 0; i < phoneLinkeds.size(); i++) {
                PhoneLinkedInfo phoneLinked = phoneLinkeds.get(i);
                if (phoneNumber.contains(phoneLinked.getPhoneNumber())) {
                    phoneNumber = phoneLinked.getPhoneNumber();
                    userServiceID = phoneLinked.getUserServiceId();
                }
            }
        }

        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.updateMetfone(null, null, userServiceID, new ApiCallback<AddServiceResponse>() {
            @Override
            public void onResponse(Response<AddServiceResponse> response) {
                if (response.body() != null
                        && response.body().getData() != null
                        && ERROR_CODE_00.equals(response.body().getCode())) {
                    mApplication.getCamIdUserBusiness().setPhoneService(mCurrentNumberJid);
                    mApplication.getCamIdUserBusiness().setUser(response.body().getData().getUser());
                    mApplication.getCamIdUserBusiness().setService(response.body().getData().getServices());

                    autoLogin(phoneNumber);
                } else {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable error) {
                android.util.Log.e(TAG, "onError: " + error);
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        });
    }

    /**
     * AutoLogin
     *
     * @param userName phone_number,
     */
    public void autoLogin(String userName) {
        MetfonePlusClient client = new MetfonePlusClient();
        client.autoLogin(userName, new MPApiCallback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response) {
                if (response.body() != null && response.body().getUsername() != null) {
                    onFinish();
                }
            }

            @Override
            public void onError(Throwable error) {
                onFinish();
            }

            private void onFinish() {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                showSuccessDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
        super.onBackPressed();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (v.getId() == R.id.pinPassword) {
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                    rledtPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.color_edt_full_name_stroke), 1);
                } else {
                    rledtPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.resend), 1);
                }
            } else if (v.getId() == R.id.pinConfirmPassword) {
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                    rledtConfirmPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.color_edt_full_name_stroke), 1);
                } else {
                    rledtConfirmPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.resend), 1);
                }
            }
        } else {
            if (v.getId() == R.id.pinPassword) {
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                    rledtPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, android.R.color.transparent), 0);
                } else {
                    rledtPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.color_edt_full_name_stroke), 1);
                }
            } else if (v.getId() == R.id.pinConfirmPassword) {
                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                    rledtConfirmPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, android.R.color.transparent), 0);
                } else {
                    rledtConfirmPassword.setStroke(ContextCompat.getColor(SetYourNewPasswordActivity.this, R.color.color_edt_full_name_stroke), 1);
                }

            }
        }
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pbLoading != null)
                pbLoading.setVisibility(View.GONE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);
            try {
//                mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
//                    pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(SetYourNewPasswordActivity.this);
                    UserInfo userInfo = userInfoBusiness.getUser();
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    reengAccount.setName(userInfo.getFull_name());
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);

//                    //navigate next
//                    String titleDialog = getString(R.string.successfully);
//                    if (checkPhoneCode == 42){
//                        titleDialog = getString(R.string.title_successfullly_combination);
//                    }
//                    baseMPSuccessDialog = new BaseMPSuccessDialog(SetYourNewPasswordActivity.this, R.drawable.img_dialog_1, titleDialog, String.format(getString(R.string.content_dialog_success_add_login), phoneNumber), false);
//                    if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
//                        baseMPSuccessDialog.setBgWhite(true);
//                    }
//                    baseMPSuccessDialog.show();
//                    successDialogDismissListener();

                    if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
                        updatePhoneToPhoneService();
                    } else {
                        if (pbLoading != null)
                            pbLoading.setVisibility(View.GONE);
                        showSuccessDialog();
                    }

//                    String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
//                    String otp = getArguments().getString(EnumUtils.OTP_KEY);
//                    String pass = pinConfirmPassword.getText().toString();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
//                    if (tvError != null) {
//                        tvError.setVisibility(View.GONE);
//                    }
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    if (pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
//                    showError(responseCode.getDescription(), null);
//                    if (tvError != null)
//                        tvError.setVisibility(View.VISIBLE);
//                    }
                    ToastUtils.showToast(SetYourNewPasswordActivity.this, "Login Old Mocha Failed");
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }

    public void detectKeyboard() {
        rlSetpass.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rlSetpass.getWindowVisibleDisplayFrame(r);
                int heightDiff = rlSetpass.getRootView().getHeight() - (r.bottom - r.top);
                int screenHeight = rlSetpass.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened

                } else {
                    // keyboard is closed
                    pinPassword.clearFocus();
                    pinConfirmPassword.clearFocus();
                }
            }
        });
    }
}
package com.metfone.selfcare.activity.update_info;

import android.graphics.Color;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.util.EnumUtils;

public class VerifyUserActivity extends BaseSlidingFragmentActivity implements
        ConfirmNumberPhoneFragment.IConfirmPhoneNumber {
    ConfirmNumberPhoneFragment confirmNumberPhoneFragment;
    OTPVerifyIdentityFragment otpAddMetfoneProfileFragment;
    public static ApplicationController mApplication;
    int serviceId = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verify_number_phone);
        mApplication = (ApplicationController) getApplication();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        displayConfirmNumberPhoneFragment();
        serviceId = getIntent().getIntExtra(EnumUtils.OBJECT_KEY, 1);
    }

    private void displayConfirmNumberPhoneFragment() {
        confirmNumberPhoneFragment = ConfirmNumberPhoneFragment.newInstance("", getIntent().getStringExtra(EnumUtils.OBJECT_KEY));
        confirmNumberPhoneFragment.setInterface(this);
        executeFragmentTransactionAllowLoss(confirmNumberPhoneFragment, R.id.container, false, false, "");
    }

    @Override
    public void confirm(String phone) {
        otpAddMetfoneProfileFragment = new OTPVerifyIdentityFragment();
        Bundle bundle = new Bundle();
        if (phone.startsWith("0")) {
            bundle.putString(EnumUtils.OBJECT_KEY, phone.substring(1));
        } else {
            bundle.putString(EnumUtils.OBJECT_KEY, phone);
        }
        bundle.putInt(EnumUtils.OTP_KEY, 0);
        otpAddMetfoneProfileFragment.setArguments(bundle);
        executeFragmentTransactionAllowLoss(otpAddMetfoneProfileFragment, R.id.container, true, true, "");
    }

    @Override
    public void onNegative() {
        NavigateActivityHelper.navigateToVerifyInformation(this);
    }
}

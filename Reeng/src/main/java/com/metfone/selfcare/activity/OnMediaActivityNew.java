package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.fragment.onmedia.CommentStatusFragment;
import com.metfone.selfcare.fragment.onmedia.HomeDiscoveryNewFragment;
import com.metfone.selfcare.fragment.onmedia.NotificationOnmediaFragment;
import com.metfone.selfcare.fragment.onmedia.ReplyCommentFragment;
import com.metfone.selfcare.fragment.onmedia.WriteStatusFragment;
import com.metfone.selfcare.helper.BackStackHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.listeners.FilePickerListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 9/29/2015.
 */
public class OnMediaActivityNew extends BaseSlidingFragmentActivity implements
        OnMediaInterfaceListener {
    public static final String POST_SUCCESS = "POST_SUCCESS";
    public static final String EDIT_SUCCESS = "EDIT_SUCCESS";
    public static final String FEED_PROCESS = "FEED_PROCESS";
    private static final String TAG = OnMediaActivityNew.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    //onmedia
    private Fragment mCurrent;
    private int fragment = -1;
    private String myPhoneNumber;
    private FeedBusiness mFeedBusiness;
    private FilePickerListener filePicker;

    private Bundle bundle;

    private HomeDiscoveryNewFragment discovery;
    private CommentStatusFragment commentStatusFragment;
    private FeedAction feedAction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            fragment = bundle.getInt(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT);
        }
        if (fragment == Constants.ONMEDIA.COMMENT) {
            Log.i(TAG, "----------- overridePendingTransition cmt");
            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        } else if (fragment == Constants.ONMEDIA.DISCOVERY) {
            setTheme(R.style.AppTheme_FullScreen);
        }

        mApplication = (ApplicationController) getApplicationContext();
        myPhoneNumber = mApplication.getReengAccountBusiness().getJidNumber();
        mFeedBusiness = mApplication.getFeedBusiness();
        mRes = getResources();
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        setActionBar();
        getDataAndDisplayFragment();
        trackingScreen(TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        WriteStatusFragment fragment = (WriteStatusFragment) getSupportFragmentManager().findFragmentByTag
                (WriteStatusFragment.class.getSimpleName());
        if (fragment != null && fragment.isVisible()) {
            if (fragment.canBackPress()) {
                super.onBackPressed();
            }
        } else {
            if (discovery != null && discovery.isVisible()) {
                if (discovery.isShowDetail()) {
                    discovery.hideViewDetail();
                } else {
                    super.onBackPressed();
                }

            } else {
                if (BackStackHelper.getInstance().checkIsLastInStack(getApplication(), OnMediaActivityNew.class
                        .getName()
                )) {
                    goToHome();
                } else {
                    super.onBackPressed();
                    if (this.fragment == Constants.ONMEDIA.COMMENT) {
                        Log.i(TAG, "onbackpress comment");
                        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
                    }

                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
        setActivityForResult(false);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case Constants.ACTION.ACTION_WRITE_STATUS:
                    boolean isEdit = data.getBooleanExtra(OnMediaActivityNew.EDIT_SUCCESS, false);
                    boolean isPost = data.getBooleanExtra(OnMediaActivityNew.POST_SUCCESS, false);
                    if (isEdit) {
                        mFeedBusiness.notifyNewFeed(true, false);
                    } else if (isPost) {
                        mFeedBusiness.notifyNewFeed(true, true);
                    } else {
                        //TODO vao day thi cu notify roi cho len dau, unknown case
                        mFeedBusiness.notifyNewFeed(true, true);
                    }
                    break;
                case Constants.ACTION.ACTION_PICK_PICTURE: {
                    ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                    if (picturePath != null) {
                        if (filePicker != null) {
                            filePicker.transferPickedPhoto(picturePath);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    public void setActionBar() {
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(getLayoutInflater().inflate(
                R.layout.ab_detail, null));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("fragment", fragment);
        super.onSaveInstanceState(outState);
    }

    private void getDataAndDisplayFragment() {
        /*if (fragment == Constants.ONMEDIA.LIST_LIKE) {
            displayListLikeFragment();
        } else */
        /*if (fragment == Constants.ONMEDIA.SETTING) {
            displaySettingFragment();
        } else if (fragment == Constants.ONMEDIA.SETTING_CONTENT) {
            displaySettingContentFragment();
        } else if (fragment == Constants.ONMEDIA.SUGGEST_FOLLOW) {
            displaySuggestFollowFragment();
        } else if (fragment == Constants.ONMEDIA.NOT_SHOW_CONTENT) {
            displayHideContentFragment();
        } else if (fragment == Constants.ONMEDIA.SETTING_PRIVATE) {
            displaySettingPrivateFragment();
        } else */
        if (fragment == Constants.ONMEDIA.ALBUM_DETAIL) {
            FeedModelOnMedia feed = (FeedModelOnMedia) getIntent().getSerializableExtra(Constants.ONMEDIA
                    .EXTRAS_FEEDS_DATA);
            displayAlbumDetailFragment(feed, false, false);
        } else if (fragment == Constants.ONMEDIA.COMMENT) {
            displayCommentFragment();
        } else if (fragment == Constants.ONMEDIA.WRITE_STATUS) {
            boolean isEdit = bundle.getBoolean(Constants.ONMEDIA.EXTRAS_EDIT_FEED, false);
            String rowId = bundle.getString(Constants.ONMEDIA.EXTRAS_ROW_ID);
            String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
            ArrayList<String> listImg = getIntent().getStringArrayListExtra(Constants.ONMEDIA.EXTRAS_LIST_IMAGE);
            if (isEdit) {
                displayEditStatusFragment();
            } else {
                if (listImg != null && !listImg.isEmpty()) {
                    displayWriteStatusFragment(listImg);
                } else {
                    displayWriteStatusFragment(url, isEdit, rowId);
                }
            }
        }
        /*else if (fragment == Constants.ONMEDIA.LIST_SHARE) {
            String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
            displayListShareFragment(url);
        } */
        else if (fragment == Constants.ONMEDIA.DISCOVERY) {
            displayDiscoveryFragment();
        } else if (fragment == Constants.ONMEDIA.NOTIFICATION) {
            displayNotificationFragment();
        } else if (fragment == Constants.ONMEDIA.REPLY_COMMENT) {
            displayReplyCommentFragment();
        } else {
            Log.i(TAG, "what the f*ck");
        }
    }

    private void displayNotificationFragment() {
        NotificationOnmediaFragment notificationOnmediaFragment = NotificationOnmediaFragment.newInstance();
        executeFragmentTransaction(notificationOnmediaFragment, R.id.fragment_container, false, false);
    }

    private void displayReplyCommentFragment() {
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        String urlSubComment = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_URL_SUB_COMMENT);
        String rowid = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_ROW_ID);
        ReplyCommentFragment notificationOnmediaFragment = ReplyCommentFragment.newInstance(url, urlSubComment, rowid);
        executeFragmentTransaction(notificationOnmediaFragment, R.id.fragment_container, false, false);
    }

    public FeedAction getFeedAction() {
        return feedAction;
    }

    public void showReplyCommentFragment(FeedAction feedAction, FeedModelOnMedia feed, boolean needShowKeyboard) {
        this.feedAction = feedAction;
        ReplyCommentFragment notificationOnmediaFragment = ReplyCommentFragment.newInstance(feed, needShowKeyboard);
        executeAddFragmentTransaction(notificationOnmediaFragment, R.id.fragment_container, true, true);
    }

    private void displayDiscoveryFragment() {
        View toolbar = findViewById(R.id.tool_bar);
        toolbar.setVisibility(View.GONE);
        showOrHideStatusBar(true);
        discovery = HomeDiscoveryNewFragment.newInstance();
        executeFragmentTransaction(discovery, R.id.fragment_container, false, false);
    }

    public void displayWriteStatusFragment(String url, boolean isEdit, String rowId) {
        int action = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_ACTION, 0);
        FeedContent feedContent = (FeedContent) getIntent().getSerializableExtra(Constants.ONMEDIA.EXTRAS_CONTENT_DATA);
        Log.i(TAG, "action: " + action);
        WriteStatusFragment writeStatusFragment = WriteStatusFragment.newInstance(action, url, feedContent);
        executeFragmentTransaction(writeStatusFragment, R.id.fragment_container, false, false,
                WriteStatusFragment.class.getSimpleName());
    }

    public void displayWriteStatusFragment(ArrayList<String> listImage) {
        WriteStatusFragment writeStatusFragment = WriteStatusFragment.newInstance(listImage);
        executeFragmentTransaction(writeStatusFragment, R.id.fragment_container, false, false,
                WriteStatusFragment.class.getSimpleName());
    }

    public void displayEditStatusFragment() {
        int action = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_ACTION, 0);
        FeedModelOnMedia feedModelOnMedia = (FeedModelOnMedia) getIntent().getSerializableExtra(Constants.ONMEDIA
                .EXTRAS_FEEDS_DATA);
        Log.i(TAG, "action: " + action);
        WriteStatusFragment writeStatusFragment = WriteStatusFragment.newInstance(action, feedModelOnMedia);
        executeFragmentTransaction(writeStatusFragment, R.id.fragment_container, false, false,
                WriteStatusFragment.class.getSimpleName());
    }

    private void displayCommentFragment() {
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        boolean showPreview = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, false);
        String rowId = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_ROW_ID);
        int feedType = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, 0);
        FeedContent feedContent = (FeedContent) getIntent().getSerializableExtra(Constants.ONMEDIA.EXTRAS_CONTENT_DATA);
        String actionType = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_ACTION);
        int feedFrom = getIntent().getIntExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, -1);
        boolean getDetailUrl = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, false);
        boolean showMenuCopy = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, false);
        FeedModelOnMedia feedModelOnMedia = (FeedModelOnMedia) getIntent().getSerializableExtra(Constants.ONMEDIA
                .EXTRAS_FEEDS_DATA);
        if (feedType == Constants.ONMEDIA.FEED_TAB_VIDEO || feedModelOnMedia != null) {
            commentStatusFragment = CommentStatusFragment.newInstance(url, showPreview, rowId, feedType, actionType,
                    feedModelOnMedia, feedFrom, getDetailUrl, showMenuCopy, false);
        } else {
            if (feedContent == null) {
                commentStatusFragment = CommentStatusFragment.newInstance(url, showPreview, rowId, feedType, actionType,
                        getDetailUrl, showMenuCopy);
            } else {
                commentStatusFragment = CommentStatusFragment.newInstance(url, showPreview, rowId, feedType, actionType,
                        feedContent, feedFrom, getDetailUrl, showMenuCopy);
            }
        }

        executeFragmentTransaction(commentStatusFragment, R.id.fragment_container, false, false);
    }

    private void displayCommentFragment(String url, boolean hasShowCopy) {
        CommentStatusFragment commentStatusFragment = CommentStatusFragment.newInstance(url, hasShowCopy);
        executeFragmentTransaction(commentStatusFragment, R.id.fragment_container, false, false);
    }

    public void displayAlbumDetailFragment(FeedModelOnMedia feed, boolean addToBackStack, boolean
            animation) {
        /*int feedType = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, 0);
        ListMediaPlayerFragment mListMediaPlayerFragment = ListMediaPlayerFragment.newInstance(url, feed, feedType);
        mCurrent = mListMediaPlayerFragment;
        executeFragmentTransaction(mListMediaPlayerFragment, R.id.fragment_container, addToBackStack, animation);*/
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        String rowId = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_ROW_ID);
        int feedType = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_SONG);
        String actionType = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_ACTION);
        int feedFrom = getIntent().getIntExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, -1);
        boolean getDetailUrl = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_GET_DETAIL_URL, false);
        boolean showMenuCopy = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, false);
        commentStatusFragment = CommentStatusFragment.newInstance(url, false, rowId, feedType, actionType,
                feed, feedFrom, getDetailUrl, showMenuCopy, true);
        executeFragmentTransaction(commentStatusFragment, R.id.fragment_container, false, false);
    }

    /*    private void displayListLikeFragment() {
     *//*String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        ListUserLikeFragment listUserLikeFragment = ListUserLikeFragment.newInstance(url);
        executeFragmentTransaction(listUserLikeFragment, R.id.fragment_container, false, false);*//*
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_LIKE);
        startActivity(intent, true);
    }

    public void displayListLikeFragment(String url) {
        *//*ListUserLikeFragment listUserLikeFragment = ListUserLikeFragment.newInstance(url);
        executeAddFragmentTransaction(listUserLikeFragment, R.id.fragment_container, true, true);*//*
        Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_LIKE);
        startActivity(intent, true);
    }

    private void displayListShareFragment() {
        *//*String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        ListUserShareFragment listUserShareFragment = ListUserShareFragment.newInstance(url);
        executeFragmentTransaction(listUserShareFragment, R.id.fragment_container, false, false);*//*
        String url = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
        Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_SHARE);
        startActivity(intent, true);
    }

    public void displayListShareFragment(String url) {
        Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_SHARE);
        startActivity(intent, true);
    }*/

    public int getFragment() {
        return fragment;
    }

    @Override
    public void navigateToListShare(String url) {
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, url, false);
    }

    @Override
    public void navigateToWebView(FeedModelOnMedia feed) {
        NavigateActivityHelper.navigateToWebView(this, feed, false);
    }

  /*  @Override
    public void navigateToSetting() {
        displaySettingFragment();
    }*/

    @Override
    public void navigateToVideoView(FeedModelOnMedia feed) {
//        NavigateActivityHelper.navigateToAutoPlayVideo(OnMediaActivityNew.this, feed);
        /*Video video = Video.convertFeedContentToVideo(feed.getFeedContent());
        if (video == null) return;*/
        mApplication.getApplicationComponent().providesUtils().openVideoDetail(this, feed);
    }

    @Override
    public void navigateToMovieView(FeedModelOnMedia feed) {
        if (feed != null && feed.getFeedContent() != null) {
            Movie movie = FeedContent.convert2Movie(feed.getFeedContent());
            if (movie != null)
                playMovies(movie);
            else
                Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
        }
    }

    @Override
    public void navigateToProcessLink(FeedModelOnMedia feed) {
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToComment(FeedModelOnMedia feed) {
        boolean showCopyMenu = mFeedBusiness.checkFeedToShowMenuCopy(feed);
        displayCommentFragment(feed.getFeedContent().getUrl(), showCopyMenu);
    }

    @Override
    public void navigateToAlbum(FeedModelOnMedia feed) {
        /*Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.ALBUM_DETAIL);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        mFeedBusiness.setFeedPlayList(feed);
        startActivity(intent, true);*/
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    public void navigateToCommentWithPreview(FeedModelOnMedia feed, String rowIdNotify) {
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEEDS_DATA, feed);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_PREVIEW, true);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_ROW_ID, rowIdNotify);
        boolean showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(feed);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        startActivity(intent, true);
    }

    public void navigateToReplyFromNotify(FeedModelOnMedia feed, String urlComment, String rowid) {
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.REPLY_COMMENT);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_URL_SUB_COMMENT, urlComment);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_ROW_ID, rowid);
        startActivity(intent, true);
    }

    public void dispatchPickPictureIntent(ArrayList<String> listSelected) {
        // gủi anh
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, Constants.MESSAGE.IMAGE_LIMIT);
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_ICON_CAMERA, true);
        i.putExtra(ImageBrowserActivity.PARAM_LIST_IMAGE_SELECTED, listSelected);
        setActivityForResult(true);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    public void setFilePickerListener(FilePickerListener f) {
        Log.i(TAG, "mThreadId setFilePickerListener " + f.hashCode());
        filePicker = f;
    }

    public void showOrHideStatusBar(boolean isShow) {
        if (isShow) {
            showStatusBar();
        } else {
            hideStatusBar();
        }
    }

    public void notifyAdapterListComment(FeedAction comment) {
        if (commentStatusFragment != null) {
            commentStatusFragment.notifyDataListComment(comment);
        }
    }
}
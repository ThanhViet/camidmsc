package com.metfone.selfcare.activity.update_info;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.databinding.FragmentDialogConfirmNumberPhoneBinding;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPErrorDialog;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDetachIPResponse;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.Utils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import retrofit2.Response;

public class ConfirmNumberPhoneFragment extends BaseFragment {

    private static final String TITLE = "TITLE";
    private static final String CONTENT = "CONTENT";
    private IConfirmPhoneNumber iConfirmPhoneNumber;
    private BaseMPErrorDialog baseMPErrorDialog;


    private FragmentDialogConfirmNumberPhoneBinding binding;

    public void setInterface(IConfirmPhoneNumber iConfirmPhoneNumber) {
        this.iConfirmPhoneNumber = iConfirmPhoneNumber;
    }

    public static ConfirmNumberPhoneFragment newInstance(String title, String content) {
        ConfirmNumberPhoneFragment dialog = new ConfirmNumberPhoneFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(CONTENT, content);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDialogConfirmNumberPhoneBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        playAnimation();

        binding.edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    binding.btnYes.setEnabled(true);
                    binding.btnYes.setBackground(ContextCompat.getDrawable(requireContext(),
                            R.drawable.bg_btn_confirm_information));
                } else {
                    binding.btnYes.setEnabled(false);
                    binding.btnYes.setBackground(ContextCompat.getDrawable(requireContext(),
                            R.drawable.bg_disable_btn_update_information));
                }
            }
        });

        binding.ivBack.setOnClickListener((v) -> {
            requireActivity().onBackPressed();
        });
        binding.btnYes.setOnClickListener(view12 -> {
            if (iConfirmPhoneNumber != null && validPhoneLocal()) {
                String phoneNumber = binding.edtPhone.getText().toString();
                if (!phoneNumber.startsWith("0")) {
                    phoneNumber = "0" + phoneNumber;
                }
                if (Utilities.isMetPhone(phoneNumber)) {
                    iConfirmPhoneNumber.confirm(phoneNumber);
                } else {
                    baseMPErrorDialog = new BaseMPErrorDialog(getActivity(), R.drawable.image_error_dialog,
                            phoneNumber, getString(R.string.phone_number_do_not_support), true,
                            R.string.btn_try_again,
                            v -> {
                                baseMPErrorDialog.dismiss();
                            });
                    baseMPErrorDialog.show();
                    getChildFragmentManager().executePendingTransactions();
                }
            }
        });
        binding.btnNo.setOnClickListener(view1 -> {
            if (iConfirmPhoneNumber != null) {
                iConfirmPhoneNumber.onNegative();
            }
        });

//        Bundle bundle = getArguments();
//        if (bundle != null) {
////            binding.tvTitle.setText(bundle.getString(TITLE, ""));
//            binding.edtPhone.setText(bundle.getString(CONTENT, ""));
//        }
        setupView();

        getIsdnFromIP(Utils.getIpOf3G());
    }

    private void playAnimation() {
        binding.dialogHeroImg.setImageResource(R.drawable.img_dialog_3);
        binding.dialogHeroImg.setVisibility(View.GONE);
        binding.flImage.setVisibility(View.VISIBLE);
        binding.lavAnimations.setVisibility(View.VISIBLE);
        binding.lavAnimations.setAnimation("lottie/img_dialog_women_thinking.json");
        binding.lavAnimations.playAnimation();
        binding.lavAnimations.setVisibility(View.VISIBLE);
    }

    private void setupView() {
        if (binding.edtPhone.getText().toString().isEmpty()) {
            binding.btnYes.setEnabled(false);
            binding.btnYes.setBackground(ContextCompat.getDrawable(requireContext(),
                    R.drawable.bg_disable_btn_update_information));
        } else {
            binding.btnYes.setEnabled(true);
            binding.btnYes.setBackground(ContextCompat.getDrawable(requireContext(),
                    R.drawable.bg_btn_confirm_information));
        }
    }


    public void getIsdnFromIP(String ip) {
        if (!NetworkHelper.isConnectInternet(requireContext())) {
            return;
        }
        binding.pbLoading.setVisibility(View.VISIBLE);
        new MetfonePlusClient().wsDetachIp(ip, new MPApiCallback<WsDetachIPResponse>() {
            @Override
            public void onResponse(Response<WsDetachIPResponse> response) {
                if(binding != null) {
                    binding.pbLoading.setVisibility(View.GONE);
                }
                if (response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().getResult().getErrorCode().equals("0")) {
                    WsDetachIPResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    if (wsResponse != null && wsResponse.getIsdn() != null) {
                        if(binding != null) {
                            binding.edtPhone.setText(wsResponse.getIsdn());
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                if(binding != null) {
                    binding.pbLoading.setVisibility(View.GONE);
                }
            }
        });
    }


    private boolean validPhoneLocal() {
        String phone = binding.edtPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
            return false;
        }
        if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
            ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
            return false;
        }
        return true;
    }

    public interface IConfirmPhoneNumber {
        void confirm(String phone);

        void onNegative();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

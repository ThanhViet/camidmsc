package com.metfone.selfcare.activity;

import android.graphics.Color;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.fragment.addAccount.AddAccountFragment;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.util.EnumUtils;

import butterknife.ButterKnife;

public class AddAccountActivity extends BaseSlidingFragmentActivity implements AddAccountFragment.OnFragmentInteractionListener {
    AddAccountFragment metFoneServiceRegisterFragment;
    public static ApplicationController mApplication;
    String fromSource = "";
    int serviceId = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metfone_service);
        ButterKnife.bind(this);
        mApplication = (ApplicationController) getApplication();
        changeStatusBar(Color.parseColor("#1F1F1F"));
        displayServiceRegisterFragment();
        serviceId = getIntent().getIntExtra(EnumUtils.OBJECT_KEY, 1);
    }

    public void displayServiceRegisterFragment() {
        metFoneServiceRegisterFragment = AddAccountFragment.newInstance();
        executeFragmentTransactionAllowLoss(metFoneServiceRegisterFragment, R.id.container, false, false, "");
    }


    @Override
    public void displayVerifyIdFragment(String phoneNumber, int checkPhoneCode) {
        metFoneServiceRegisterFragment = AddAccountFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(EnumUtils.OBJECT_KEY, phoneNumber);
        bundle.putBoolean(EnumUtils.IS_VERIFY_SCREEN, true);
        metFoneServiceRegisterFragment.setArguments(bundle);
        executeFragmentTransactionAllowLoss(metFoneServiceRegisterFragment, R.id.container, true, true, "");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public int getServiceId() {
        return serviceId;
    }
}
package com.metfone.selfcare.activity;

import android.animation.Animator;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.onmedia.ContentDiscovery;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.TextureVideoView;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 7/3/2017.
 */
public class OMDiscoveryDetailActivity extends BaseSlidingFragmentActivity {

    private static final String TAG = OMDiscoveryDetailActivity.class.getSimpleName();
    private ApplicationController mApp;
    private AspectImageView mImgThumb;
    private TextureVideoView mVideoView;
    private View mViewNext, mViewPrevious;
    private View mViewProgress;

    private View layoutDiscoveryDetail;
    private View layoutInfo;
    private TextView tvRead;
    boolean isTouchLayoutInfo = false;
    private int heightScreen = 0;

    private FeedBusiness mFeedBusiness;
    private Handler mHandler;
    private int positionSeekVideo;
    private boolean isPauseVideo;
    private int currentPosition;
    private ContentDiscovery contentDiscovery;
    private int width, height;

//    private ImageViewAwareTargetSize imageViewAwareTargetSize;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discovery);
        changeStatusBar(true);
        mApp = (ApplicationController) getApplication();
        mFeedBusiness = mApp.getFeedBusiness();
        mHandler = new Handler();
        supportPostponeEnterTransition();
        currentPosition = getIntent().getIntExtra(Constants.ONMEDIA.DATA_INDEX_VIDEO_DISCOVERY, -1);
        if (currentPosition >= 0) {
            contentDiscovery = mFeedBusiness.getListContentDiscoveries().get(currentPosition);
        }

        setViewComponent();
        setViewListener();
        onTouchAction();

        width = mApp.getWidthPixels();
        height = mApp.getHeightPixels();
        float ratio = (float) width / (float) height;
        if (width > 600) {
            width = 600;
            height = Math.round(width / ratio);
        }

//        imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgThumb, width, height);

        if (contentDiscovery == null) {
            showToast(R.string.e601_error_but_undefined);
            finish();
            return;
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (contentDiscovery != null && !TextUtils.isEmpty(contentDiscovery.getVideoUrl())) {
                    playVideo(contentDiscovery.getVideoUrl());
                }
//                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
        }, 200);

        ImageLoader.setImage(this, contentDiscovery.getImageUrl(), mImgThumb);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            String imageTransitionName = contentDiscovery.getImageUrl();
//            mImgThumb.setTransitionName(imageTransitionName);
//        }
//
//        ImageLoaderManager.getInstance(mApp).displayImageFeedsWithListener(imageViewAwareTargetSize, contentDiscovery.getImageUrl
//                (), new SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                super.onLoadingComplete(imageUri, view, loadedImage);
//                supportStartPostponedEnterTransition();
//            }
//
//            @Override
//            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                super.onLoadingFailed(imageUri, view, failReason);
//                supportStartPostponedEnterTransition();
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        if (mVideoView != null) {
            mVideoView.stop();
            mImgThumb.setVisibility(View.VISIBLE);
            Log.i(TAG, "GONE videoview");
        }
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoView != null && mVideoView.isPlaying()) {
            Log.i(TAG, "pause video");
            positionSeekVideo = mVideoView.getMediaPlayer().getCurrentPosition();
            isPauseVideo = true;
            mVideoView.pause();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPauseVideo) {
            mVideoView.play();
            mVideoView.seekTo(positionSeekVideo);
            isPauseVideo = false;
        }
    }

    @Override
    protected void onDestroy() {
        if (mVideoView != null && mVideoView.getMediaPlayer() != null) {
            mVideoView.getMediaPlayer().release();
            mVideoView.setMediaPlayer(null);
        }
        super.onDestroy();
    }

    private void setViewListener() {
        mVideoView.setListener(new TextureVideoView.MediaPlayerListener() {
            @Override
            public void onVideoPrepared() {
                Log.i(TAG, "onVideoPrepared");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewProgress.setVisibility(View.GONE);
                        mImgThumb.setVisibility(View.GONE);

                    }
                }, 500);
            }

            @Override
            public void onVideoEnd() {
                Log.i(TAG, "onVideoEnd");
                nextVideo();
            }

            @Override
            public void onBufferStart() {

            }

            @Override
            public void onBufferEnd() {

            }

            @Override
            public void onError() {

            }
        });

//        mViewNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i(TAG, "click next");
//                int size = mFeedBusiness.getListContentDiscoveries().size();
//                if (currentPosition < (size - 1)) {
//                    currentPosition++;
//                    ContentDiscovery content = mFeedBusiness.getListContentDiscoveries().get(currentPosition);
//                    if (content != null) {
//                        contentDiscovery = content;
//                    }
//                }
//
//                ImageLoaderManager.getInstance(mApp).setImageFeeds(imageViewAwareTargetSize, contentDiscovery.getImageUrl());
//                mImgThumb.setVisibility(View.VISIBLE);
//                playVideo(contentDiscovery.getVideoUrl());
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    mImgThumb.setTransitionName(contentDiscovery.getImageUrl());
//                }
//            }
//        });
//
//        mViewPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i(TAG, "click previous");
//                if (currentPosition > 0) {
//                    currentPosition--;
//                    ContentDiscovery content = mFeedBusiness.getListContentDiscoveries().get(currentPosition);
//                    if (content != null) {
//                        contentDiscovery = content;
//                    }
//
//                }
//                ImageLoaderManager.getInstance(mApp).setImageFeeds(imageViewAwareTargetSize, contentDiscovery.getImageUrl());
//                mImgThumb.setVisibility(View.VISIBLE);
//                playVideo(contentDiscovery.getVideoUrl());
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    mImgThumb.setTransitionName(contentDiscovery.getImageUrl());
//                }
//            }
//        });
    }

    private void nextVideo() {
        int size = mFeedBusiness.getListContentDiscoveries().size();
        if (currentPosition < (size - 1)) {
            currentPosition++;
            ContentDiscovery content = mFeedBusiness.getListContentDiscoveries().get(currentPosition);
            if (content != null) {
                contentDiscovery = content;
            }
        }
        ImageLoaderManager.getInstance(mApp).setImageFeeds(mImgThumb, contentDiscovery.getImageUrl(), width, height);
        mImgThumb.setVisibility(View.VISIBLE);
        playVideo(contentDiscovery.getVideoUrl());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mImgThumb.setTransitionName(contentDiscovery.getImageUrl());
        }
    }

    private void setViewComponent() {
        heightScreen = mApp.getHeightPixels();
        layoutDiscoveryDetail = findViewById(R.id.layoutDiscoveryDetail);
        layoutInfo = findViewById(R.id.layout_info);
        tvRead = (TextView) findViewById(R.id.tvRead);
        layoutInfo.setY(heightScreen);
        layoutInfo.setVisibility(View.VISIBLE);

        mImgThumb = (AspectImageView) findViewById(R.id.img_thumb_content_discovery);
        mVideoView = (TextureVideoView) findViewById(R.id.texturevideo);
        mVideoView.setScaleType(TextureVideoView.ScaleType.CENTER_CROP);

        mViewNext = findViewById(R.id.view_next);
        mViewPrevious = findViewById(R.id.view_previous);

        mViewProgress = findViewById(R.id.view_progress_loading_video);

    }

    private void playVideo(String filePath) {
        mViewProgress.setVisibility(View.VISIBLE);
        mVideoView.stop();
        mVideoView.setDataSource(filePath);
        mVideoView.play();
        mVideoView.setLooping(false);
        Log.i(TAG, "play video: " + filePath);
    }

    private void onTouchAction()
    {
//        layoutDiscoveryDetail.setY(heightScreen);
//        layoutDiscoveryDetail.animate().translationY(0).setDuration(500).start();

        tvRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                layoutInfo.setY(heightScreen);
                layoutInfo.animate().translationY(0).setDuration(500).start();
            }
        });

        mVideoView.setListener(new TextureVideoView.MediaPlayerListener() {
            @Override
            public void onVideoPrepared() {
                Log.i(TAG, "onVideoPrepared");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mViewProgress.setVisibility(View.GONE);
                        mImgThumb.setVisibility(View.GONE);
                    }
                }, 500);
            }

            @Override
            public void onVideoEnd() {
                Log.i(TAG, "onVideoEnd");
            }

            @Override
            public void onBufferStart() {

            }

            @Override
            public void onBufferEnd() {

            }

            @Override
            public void onError() {

            }
        });

        layoutInfo.setOnTouchListener(new View.OnTouchListener() {

            float dx = 0, dy = 0, x = 0, y = 0;
            int NONE = 0;
            int DRAG = 1;
            int mode = NONE;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                x = motionEvent.getRawX();
                y = motionEvent.getRawY();

                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        dx = x - view.getX();
                        dy = y - view.getY();
                        mode = DRAG;
                        isTouchLayoutInfo = true;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        isTouchLayoutInfo = true;
                        if (mode == DRAG) {
                            view.setY(y - dy);
                            if(view.getY() < 0f)
                            {
                                view.setY(0f);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        mode = NONE;
                        float viewY = view.getY();

                        if(viewY >= heightScreen / 3)
                        {
                            layoutInfo.animate().translationY(heightScreen).setDuration(500).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    isTouchLayoutInfo = false;
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {
                                    isTouchLayoutInfo = false;
                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            }).start();
                        }
                        else
                        {
                            layoutInfo.animate().translationY(0).setDuration(500).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    isTouchLayoutInfo = false;
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {
                                    isTouchLayoutInfo = false;
                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            }).start();
                        }
                        break;
                }

                return true;
            }

        });


        layoutDiscoveryDetail.setOnTouchListener(new View.OnTouchListener() {

            float dx = 0, dy = 0, x = 0, y = 0, dxLayoutInfo = 0, dyLayoutInfo = 0;
            float yPrev = 1;
            float yCurrent = 1;
            int NONE = 0;
            int DRAG = 1;
            //            int ZOOM = 2;
            int mode = NONE;
            float statusBar = getResources().getDimension(R.dimen.status_bar_height);

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                x = motionEvent.getRawX();
                y = motionEvent.getRawY();

                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        dx = x - view.getX();
                        dy = y - view.getY();
                        dxLayoutInfo = x - layoutInfo.getX();
                        dyLayoutInfo = y - layoutInfo.getY();
                        yPrev = y;
                        mode = DRAG;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG && !isTouchLayoutInfo) {
                            yCurrent = y;

                            view.setY(y - dy);
                            if(view.getY() <= 0)
                            {
                                view.setY(0);
                                layoutInfo.setY(y - dyLayoutInfo);
                            }
                            else if(view.getY() > heightScreen)
                            {
                                view.setY(heightScreen);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        mode = NONE;
                        if(view.getY() <= 0)
                        {
                            if(layoutInfo.getY() < heightScreen * 2 / 3)
                            {
                                layoutInfo.animate().translationY(0).setDuration(500).start();
                            }
                            else
                            {
                                layoutInfo.animate().translationY(heightScreen).setDuration(500).start();
                            }
                            view.setY(0);
                        }
                        else if(view.getY() > heightScreen)
                        {
                            view.setY(heightScreen);
                        }
                        else
                        {
                            if(view.getY() >= heightScreen / 4)
                            {
                                view.animate().translationY(heightScreen).setDuration(500).setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animator) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animator) {
                                        onBackPressed();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animator) {
                                        onBackPressed();
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animator) {

                                    }
                                }).start();

                            }
                            else
                            {
                                view.animate().translationY(0).setDuration(500).start();
                            }
                        }
                        break;
                }
                return true;
            }

        });
    }

}

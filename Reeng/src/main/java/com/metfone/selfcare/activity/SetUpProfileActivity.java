package com.metfone.selfcare.activity;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.google.GoogleSignInHelper.RC_SIGN_IN;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;
import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.ServicesAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.google.GoogleSignInHelper;
import com.metfone.selfcare.model.account.DeleteServiceRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.IdentifyProvider;
import com.metfone.selfcare.model.account.LinkAccountRequest;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPConfirmRemoveDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.view.CamIdButton;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetUpProfileActivity extends BaseSlidingFragmentActivity implements View.OnClickListener,
        ServicesAdapter.OnItemServiceClickListener, FacebookHelper.OnFacebookListener,
        GoogleSignInHelper.OnGoogleListener, GoogleSignInHelper.OnGoogleSignOutListener {
    Unbinder unbinder;
    @BindView(R.id.clContainer)
    ConstraintLayout clContainer;
    @BindView(R.id.clLoginPhone)
    ConstraintLayout clLoginPhone;
    @BindView(R.id.sc_setup)
    NestedScrollView mScrSetup;
    @BindView(R.id.clAddLogin)
    ConstraintLayout clAddLogin;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.tvName)
    CamIdTextView tvName;
    @BindView(R.id.tvFacebookConnection)
    CamIdTextView tvFacebookConnection;
    @BindView(R.id.tvGoogleConnection)
    CamIdTextView tvGoogleConnection;
    @BindView(R.id.edtPhone)
    AppCompatEditText edtPhone;
    @BindView(R.id.btnFromFacebook)
    LinearLayout btnFromFacebook;
    @BindView(R.id.btnFromGoogle)
    LinearLayout btnFromGoogle;
    @BindView(R.id.tvChangePhone)
    AppCompatEditText tvChangePhone;
    @BindView(R.id.ivProfileAvatar)
    CircleImageView ivProfileAvatar;
    @BindView(R.id.btnCheckVerify)
    CamIdButton btnCheckVerify;
    @BindView(R.id.rvServices)
    RecyclerView rvServices;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    ServicesAdapter servicesAdapter;
    ApplicationController mApplication;
    UserInfoBusiness userInfoBusiness;
    GoogleSignInHelper googleSignInHelper;
    BaseMPConfirmDialog baseMPConfirmDialog;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_profile);
        unbinder = ButterKnife.bind(this);
        tvName.setOnClickListener(this);
        changeStatusBar(Color.parseColor("#C41627"));
        drawBackground();
        rvServices.setNestedScrollingEnabled(false);
        mApplication = (ApplicationController) getApplication();
        servicesAdapter = new ServicesAdapter(this);
        servicesAdapter.setOnItemClickListener(this);
        rvServices.setAdapter(servicesAdapter);
        userInfoBusiness = new UserInfoBusiness(this);
        googleSignInHelper = new GoogleSignInHelper(this, this);
        setListener();
        mScrSetup.post(new Runnable() {
            @Override
            public void run() {
                mScrSetup.scrollTo(0, 0);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        EnumUtils.FROM_ADD_LOGIN_MODE = EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY;
        if (!NetworkHelper.isConnectInternet(this)) {
            showError(getString(R.string.error_internet_disconnect), null);
        } else {
            //call api to pass reject verify id number
            getUserInformation();
        }
        drawProfile();
        hideKeyboard();
    }

    public void drawProfile() {
        UserInfo currentUser = userInfoBusiness.getUser();
        List<Services> services = userInfoBusiness.getServiceList();
        List<IdentifyProvider> identifyProviderList = userInfoBusiness.getIdentifyProviderList();
        if (currentUser != null) {
            String name = currentUser.getFull_name();
            String phoneNumber = currentUser.getPhone_number();
            if (name != null) {
                tvName.setText(UserInfoBusiness.getMaxLengthName(name));
            }
            if (phoneNumber != null && phoneNumber.length() > 0) {
                clLoginPhone.setVisibility(View.VISIBLE);
                clAddLogin.setVisibility(View.GONE);//088 298989898
                edtPhone.setText("0" + phoneNumber.substring(0, 2) + " " + phoneNumber.substring(2));

            } else {
                clLoginPhone.setVisibility(View.GONE);
                clAddLogin.setVisibility(View.VISIBLE);
            }
            if (TextUtils.isEmpty(currentUser.getAvatar())) {
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    getRankAccount();
                }
            } else {
                Glide.with(this)
                        .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .dontAnimate()
                        .into(ivProfileAvatar);
            }


            if (!TextUtils.isEmpty(currentUser.getVerified()) &&
                    EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(currentUser.getVerified())) {
                btnCheckVerify.setText(getString(R.string.verified_identity));
                btnCheckVerify.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_verified_identity));
                btnCheckVerify.setEnabled(false);
            } else if (!TextUtils.isEmpty(currentUser.getVerified()) &&
                    EnumUtils.IdNumberVerifyStatusTypeDef.PENDING.equals(currentUser.getVerified())) {
                btnCheckVerify.setEnabled(false);
            }
        }
        if (services != null && services.size() > 0) {
            servicesAdapter.submitList(UserInfoBusiness.convertServicesToServiceModelList(services));
//            if(services.get(0).getPhoneLinkedList() != null && services.get(0).getPhoneLinkedList().size() > 0 ){
//                UserInfo userInfo = new UserInfo();
//                ApplicationController.self().getCamIdUserBusiness().setMetfoneUsernameIsdn(services.get(0).getPhoneLinkedList().get(0).getPhoneNumber());
//                EventBus.getDefault().postSticky(userInfo);
//            }
        }

        if (identifyProviderList != null && identifyProviderList.size() > 0) {
            if (foundIndex(identifyProviderList, getString(R.string.text_social_type_facebook)) != -1) {
                tvFacebookConnection.setText(R.string.text_account_conntected);
                btnFromFacebook.setOnClickListener(null);
            }

            if (foundIndex(identifyProviderList, getString(R.string.text_social_type_google)) != -1) {
                tvGoogleConnection.setText(R.string.text_account_conntected);
                btnFromGoogle.setOnClickListener(null);
            }
        }
    }

    private int foundIndex(List<IdentifyProvider> identifyProviderList, String type) {
        for (int i = 0; i < identifyProviderList.size(); i++) {
            if (type.equals(identifyProviderList.get(i).getIdentityProvider())) {
                return i;
            }
        }
        return -1;
    }

    private void drawBackground() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            clContainer.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        } else {
            clContainer.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        }
    }

    public void setListener() {
        edtPhone.setOnEditorActionListener((textView, i, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (i == EditorInfo.IME_ACTION_DONE) || i == EditorInfo.IME_ACTION_NEXT) {
                edtPhone.setEnabled(false);
                UIUtil.hideKeyboardFrom(this, edtPhone);
            }
            return false;
        });

        ivProfileAvatar.setOnClickListener(this);
    }

    @Optional
    @OnClick({R.id.btnBack, R.id.ivProfileAvatar, R.id.btnFromFacebook, R.id.btnFromGoogle,
            R.id.tvChangePhone, R.id.btnCheckVerify, R.id.clAddLogin})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCheckVerify:
                String numberPhone = "";
                if (Utilities.isMetPhone(userInfoBusiness.getUser().getPhone_number())) {
                    numberPhone = userInfoBusiness.getUser().getPhone_number();
                }
                NavigateActivityHelper.navigateToVerifyNewInformation(this, numberPhone);
                break;
            case R.id.btnBack:
                finish();
                break;
            case R.id.ivProfileAvatar:
            case R.id.tvName:
                NavigateActivityHelper.navigateToMyProfileNew(this);
                break;
            case R.id.btnFromFacebook:
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    FacebookHelper facebookHelper = new FacebookHelper(this);
                    facebookHelper.getProfile(getCallbackManager(), this, FacebookHelper.PendingAction.GET_USER_INFO);
                }
                break;
            case R.id.btnFromGoogle:
                if (!NetworkHelper.isConnectInternet(this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                } else {
                    googleSignInHelper = null;
                    googleSignInHelper = new GoogleSignInHelper(this, this);
                    if (googleSignInHelper.isSignedIn()) {
                        googleSignInHelper.signOut(this);
                        return;
                    }
                    signIn();
                }
                break;
            case R.id.tvChangePhone:
                baseMPConfirmDialog = new BaseMPConfirmDialog(R.string.dialog_change_phone,
                        R.string.change_phone, R.string.change, R.string.cancel,
                        v -> {
                            //letdoit
                            Intent intent = new Intent(SetUpProfileActivity.this, ChangePhoneNumberActivity.class);
                            startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
                            if (baseMPConfirmDialog != null) {
                                baseMPConfirmDialog.dismiss();
                            }
                        }, v -> {
                    //back
                    if (baseMPConfirmDialog != null) {
                        baseMPConfirmDialog.dismiss();
                    }
                });
                baseMPConfirmDialog.show(getSupportFragmentManager(), null);
                break;
            case R.id.clAddLogin:
                linkPhoneNumber();
                break;
        }
    }

    private void linkPhoneNumber() {
        Intent intent = new Intent(SetUpProfileActivity.this, EnterAddPhoneNumberActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, EnumUtils.PhoneNumberTypeDef.ADD_LOGIN);
        startActivity(intent, false);
    }

    private void signIn() {

        googleSignInHelper.getProfile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            googleSignInHelper.handleSignInIntent(data);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onItemClickDelete(int position, int serviceId, String phoneNumber, String serviceName) {
        MPConfirmRemoveDialog confirmRemoveDialog = MPConfirmRemoveDialog.newInstance();
        Gson gson = new Gson();
        String json = gson.toJson(new DeleteServiceRequest(phoneNumber, serviceId, serviceName));
        Bundle bundle = new Bundle();
        bundle.putString(EnumUtils.OBJECT_KEY, json);
        confirmRemoveDialog.setArguments(bundle);
        confirmRemoveDialog.show(getSupportFragmentManager(), TAG);
    }

    @Override
    public void onItemUpdateInfo(int position, int serviceId, String phoneNumber, String serviceName) {
        NavigateActivityHelper.navigateToWebView(this, "https://info.metfone.com.kh");
    }

    @Override
    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (!NetworkHelper.isConnectInternet(this)) {
            showError(getString(R.string.error_internet_disconnect), null);
        } else {
            linkAccount(accessToken.getToken(), getString(R.string.text_social_type_facebook));
        }
    }

    @Override
    public void onGetInfoGoogleFinish(String id, String userName, String email, Uri avatarUri) {
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (!NetworkHelper.isConnectInternet(this)) {
            showError(getString(R.string.error_internet_disconnect), null);
        } else {
            linkAccount(googleSignInAccount != null ? googleSignInAccount.getIdToken() : null, getString(R.string.text_social_type_google));
        }
    }

    public void linkAccount(String socialToken, String socialType) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        LinkAccountRequest request = new LinkAccountRequest(socialToken, socialType);
        BaseRequest<LinkAccountRequest> linkAccountRequestBaseRequest = new BaseRequest<>();
        linkAccountRequestBaseRequest.setWsRequest(request);
        String json = new Gson().toJson(linkAccountRequestBaseRequest);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);
        Call<GetUserResponse> call = apiService.linkAccount(token, requestBody);
        call.enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.code() == 401) {
                    restartApp();
                } else {
                    if (response.body() != null) {
                        if ("00".equals(response.body().getCode())) {
                            if (response.body().getData() != null) {
                                userInfoBusiness.setUser(response.body().getData().getUser());
                                userInfoBusiness.setServiceList(response.body().getData().getServices());
                                userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                            }
                            if (!NetworkHelper.isConnectInternet(SetUpProfileActivity.this)) {
                                showError(getString(R.string.error_internet_disconnect), null);
                            } else {
                                //call api to pass reject verify id number
                                getUserInformation();
                            }
                        } else {
                            ToastUtils.showToast(SetUpProfileActivity.this, response.body().getMessage());
                        }
                    } else {
                        try {
                            JSONObject jObjError = null;
                            if (response.errorBody() != null) {
                                jObjError = new JSONObject(response.errorBody().string());
                            }
                            if (jObjError != null) {
                                ToastUtils.showToast(SetUpProfileActivity.this, jObjError.getJSONObject("error").getString("message"));
                            }
                        } catch (Exception e) {
                            ToastUtils.showToast(SetUpProfileActivity.this, e.getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(SetUpProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void getUserInformation() {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.code() == 401) {
                    restartApp();
                } else {
                    if (response.body() != null) {
                        if ("00".equals(response.body().getCode())) {
                            UserInfo userInfo = response.body().getData().getUser();
                            userInfoBusiness.setUser(userInfo);
                            if (response.body().getData().getServices() != null) {
                                userInfoBusiness.setServiceList(response.body().getData().getServices());
                            }
                            userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                            if (response.body().getData().getServices() != null) {
                                userInfoBusiness.setServiceList(response.body().getData().getServices());
                            }
                            userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());

                            // Fix crash NullPointerException Button.setEnabled : check btnCheckVerify null
                            if(btnCheckVerify!= null){
                                if (!TextUtils.isEmpty(userInfo.getVerified()) &&
                                        EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(userInfo.getVerified())) {
                                    btnCheckVerify.setText(getString(R.string.verified_identity));
                                    btnCheckVerify.setBackground(
                                            ContextCompat.getDrawable(SetUpProfileActivity.this,
                                                    R.drawable.bg_btn_verified_identity));
                                    btnCheckVerify.setEnabled(false);

                                } else {
                                    btnCheckVerify.setEnabled(!TextUtils.isEmpty(userInfo.getVerified()) &&
                                            !EnumUtils.IdNumberVerifyStatusTypeDef.PENDING.equals(userInfo.getVerified()));
                                }
                            }

                            List<Services> services = response.body().getData().getServices();
                            if (services != null && services.size() > 0) {
                                servicesAdapter.submitList(UserInfoBusiness.convertServicesToServiceModelList(services));
                            }
                            List<IdentifyProvider> identifyProviderList = response.body().getData().getIdentifyProviders();
                            if (identifyProviderList != null && identifyProviderList.size() > 0) {
                                if (foundIndex(identifyProviderList, getString(R.string.text_social_type_facebook)) != -1) {
                                    tvFacebookConnection.setText(R.string.text_account_conntected);
                                    btnFromFacebook.setOnClickListener(null);
                                }

                                if (foundIndex(identifyProviderList, getString(R.string.text_social_type_google)) != -1) {
                                    tvGoogleConnection.setText(R.string.text_account_conntected);
                                    btnFromGoogle.setOnClickListener(null);
                                }
                            }
                            String phoneNumber = userInfo.getPhone_number();
                            if (phoneNumber != null && phoneNumber.length() > 0) {
                                clLoginPhone.setVisibility(View.VISIBLE);
                                clAddLogin.setVisibility(View.GONE);
                                edtPhone.setText("0" + phoneNumber.substring(0, 2) + " " + phoneNumber.substring(2));

                            } else {
                                clLoginPhone.setVisibility(View.GONE);
                                clAddLogin.setVisibility(View.VISIBLE);
                            }
                        } else {
                            ToastUtils.showToast(SetUpProfileActivity.this, response.body().getMessage());
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(SetUpProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount() {
        KhHomeClient homeKhClient = new KhHomeClient();
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                initRankInfo(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                ToastUtils.showToast(SetUpProfileActivity.this, message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                ToastUtils.showToast(SetUpProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void initRankInfo(AccountRankDTO account) {
        if (account != null) {
            int rankID = account.rankId;
            EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
            if (myRank != null) {
                Glide.with(this)
                        .load(myRank.drawable)
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            } else {
                Glide.with(this)
                        .load(ContextCompat.getDrawable(SetUpProfileActivity.this, R.drawable.ic_avatar_member))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            }
        } else {
            ivProfileAvatar.setImageDrawable(
                    ContextCompat.getDrawable(SetUpProfileActivity.this, R.drawable.ic_avatar_member));
        }
    }

    @Override
    public void onSignOutGoogleFinish() {
        googleSignInHelper = new GoogleSignInHelper(this, this);
        signIn();
    }

}
package com.metfone.selfcare.activity.update_info;

import android.os.Bundle;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.databinding.ActivityIdAndPictureBinding;

public class IDAndPictureInfoActivity extends BaseSlidingFragmentActivity {
    private ActivityIdAndPictureBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityIdAndPictureBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initAction();
    }

    private void initAction() {
        binding.btnBack.setOnClickListener(v -> onBackPressed());
    }
}

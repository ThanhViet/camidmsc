package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.adapter.NewsHotAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.NewsHotModel;
import com.metfone.selfcare.database.model.onmedia.RestNewsHot;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.ui.recyclerview.headerfooter.RecyclerViewUtils;
import com.metfone.selfcare.restful.WSOnMedia;

import java.util.ArrayList;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotActivity extends BaseSlidingFragmentActivity implements NewsHotAdapter.NewsHotInterface{

    private RecyclerView recyclerView;
    private View btnBack;
    private View layout_loading;
    private NewsHotAdapter adapter;
    HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterAdapter;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<NewsHotModel> datas = new ArrayList<>();
    private int currentPage = 1;
    private int firstVisibleItem;
    private int lastVisibleItem;
    private int totalItemCount;
    private boolean isLoadMore, isEndList;
    private View mLayoutFooter, mLoadmoreFooterView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_hot);
        changeStatusBar(true);
        btnBack = findViewById(R.id.ab_back_btn);
        layout_loading = findViewById(R.id.layout_loading);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new NewsHotAdapter(getBaseContext(), datas, this);
        mHeaderAndFooterAdapter = new HeaderAndFooterRecyclerViewAdapter(adapter);
        recyclerView.setAdapter(mHeaderAndFooterAdapter);

        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayoutFooter = inflater.inflate(R.layout.item_onmedia_loading_footer, null, true);
        mLoadmoreFooterView = mLayoutFooter.findViewById(R.id.layout_loadmore);
        mLoadmoreFooterView.setVisibility(View.GONE);
        RecyclerViewUtils.setFooterView(recyclerView, mLayoutFooter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setupRecyclerView();
        loadData();
    }

    private void setupRecyclerView()
    {
        recyclerView.addOnScrollListener(((ApplicationController)getApplication()).getPauseOnScrollRecyclerViewListener(new RecyclerView
                .OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                if (!isEndList && newState == 0) {
                    boolean needToLoad = (lastVisibleItem + 1 >= totalItemCount) && (firstVisibleItem > 0);
                    if (!isLoadMore && needToLoad) {// dang ko load more
                        isLoadMore = true;
                        onLoadMore();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (linearLayoutManager == null) return;

                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                totalItemCount = linearLayoutManager.getItemCount();
            }
        }));
    }

    private void loadData()
    {
        if(currentPage == 1)
        {
            layout_loading.setVisibility(View.VISIBLE);
        }

        final WSOnMedia rest = new WSOnMedia((ApplicationController) getApplication());
        rest.getNewsHotList(currentPage, new Response.Listener<RestNewsHot>() {
            @Override
            public void onResponse(RestNewsHot restNewsHot) {
                isLoadMore = false;
                if(restNewsHot != null)
                {
                    if(restNewsHot.getData() != null && restNewsHot.getData().size() > 0)
                    {
                        if(restNewsHot.getData().size() == 0)
                        {
                            isEndList = true;
                        }
                        else
                        {
                            if(currentPage == 1)
                            {
                                datas.clear();
                            }
                            datas.addAll(restNewsHot.getData());
                            mHeaderAndFooterAdapter.notifyDataSetChanged();
                        }
                    }
                }
                layout_loading.setVisibility(View.GONE);
                mLoadmoreFooterView.setVisibility(View.GONE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                isLoadMore = false;
                layout_loading.setVisibility(View.GONE);
                mLoadmoreFooterView.setVisibility(View.GONE);
            }
        });
    }

    private void onLoadMore()
    {
        currentPage ++;
        mLoadmoreFooterView.setVisibility(View.VISIBLE);
        loadData();
    }

    @Override
    public void onClickItem(NewsHotModel entry) {
        Intent intent = new Intent(getApplication(), NewsHotDetailActivity.class);
        intent.putExtra("NEWS_HOT_ITEM", entry);
        startActivity(intent);
    }
}

package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Book;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.database.model.guestbook.Template;
import com.metfone.selfcare.fragment.guestbook.GuestBookMainFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookMainFriendFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookMyBookDetailFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookTemplateDetailFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookTemplateListFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookUpdateBookInfoFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookVoteTabFragment;
import com.metfone.selfcare.fragment.guestbook.GuestBookVotesFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.GuestBookHelper;
import com.metfone.selfcare.ui.imageview.AspectImageView;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class GuestBookActivity extends BaseSlidingFragmentActivity implements
        GuestBookMainFragment.OnFragmentInteractionListener,
        GuestBookTemplateListFragment.OnFragmentInteractionListener,
        GuestBookTemplateDetailFragment.OnFragmentInteractionListener,
        GuestBookMyBookDetailFragment.OnFragmentInteractionListener,
        GuestBookMainFriendFragment.OnFragmentInteractionListener,
        GuestBookVotesFragment.OnFragmentInteractionListener,
        View.OnClickListener {
    private ApplicationController mApplication;
    private AspectImageView mImgBanner;
    private int bannerType = Constants.GUEST_BOOK.BANNER_TYPE_NON;
    private int fragmentType;
    private String friendNumber;
    private String friendName;
    //

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplicationContext();
        setContentView(R.layout.activity_guest_book);
        changeStatusBar(true);
        mImgBanner = (AspectImageView) findViewById(R.id.guest_book_main_banner);
        setToolBar(findViewById(R.id.tool_bar));
        getDataAndDisplayFragment(savedInstanceState);
        mImgBanner.setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.GUEST_BOOK.MAIN_FRAGMENT_TYPE, fragmentType);
        outState.putString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER, friendNumber);
        outState.putString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME, friendName);
        super.onSaveInstanceState(outState);
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            fragmentType = savedInstanceState.getInt(Constants.GUEST_BOOK.MAIN_FRAGMENT_TYPE);
            friendNumber = savedInstanceState.getString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER, null);
            friendName = savedInstanceState.getString(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME, null);
        } else if (getIntent() != null) {
            fragmentType = getIntent().getIntExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_TYPE, Constants.GUEST_BOOK.MAIN_TYPE_BOOKS);
            friendNumber = getIntent().getStringExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_NUMBER);
            friendName = getIntent().getStringExtra(Constants.GUEST_BOOK.MAIN_FRAGMENT_NAME);
        }
        if (fragmentType == Constants.GUEST_BOOK.MAIN_TYPE_BOOKS) {
            if (TextUtils.isEmpty(friendNumber)) {
                displayHomeFragment();
            } else {
                displayHomeFriendFragment(friendNumber, friendName);
            }
        } else {
            displayTabVoteFragment();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.guest_book_main_banner:
                if (bannerType == Constants.GUEST_BOOK.BANNER_TYPE_HOME ||
                        bannerType == Constants.GUEST_BOOK.BANNER_TYPE_HOME_FRIEND) {
                    GuestBookVoteTabFragment voteFragment = GuestBookVoteTabFragment.newInstance();
                    executeFragmentTransaction(voteFragment, R.id.fragment_container, true, true);
                } else if (bannerType == Constants.GUEST_BOOK.BANNER_TYPE_TAB_VOTE) {
                    UrlConfigHelper.getInstance(GuestBookActivity.this).
                            gotoWebViewOnMedia(mApplication, GuestBookActivity.this, Constants.GUEST_BOOK.LINK_BANNER_VOTE);
                }
                break;
        }
    }

    public void setBannerType(int bannerType) {
        this.bannerType = bannerType;
        if (bannerType == Constants.GUEST_BOOK.BANNER_TYPE_HOME) {
            mImgBanner.setVisibility(View.VISIBLE);
            mImgBanner.setImageResource(R.drawable.banner_guest_book_main);
        } else if (bannerType == Constants.GUEST_BOOK.BANNER_TYPE_HOME_FRIEND) {
            mImgBanner.setVisibility(View.VISIBLE);
            mImgBanner.setImageResource(R.drawable.banner_guest_book_main_friend);
        } /*else if (bannerType == Constants.GUEST_BOOK.BANNER_TYPE_TAB_VOTE) {
            mImgBanner.setVisibility(View.VISIBLE);
            mImgBanner.setImageResource(R.drawable.banner_guest_book_vote);
        }*/ else {
            mImgBanner.setVisibility(View.GONE);
        }
    }

    @Override
    public void navigateToListTemplate() {
        GuestBookTemplateListFragment fragment = GuestBookTemplateListFragment.newInstance();
        executeFragmentTransaction(fragment, R.id.fragment_container, true, true);
    }

    @Override
    public void navigateToTemplateDetail(Template template) {
        GuestBookTemplateDetailFragment fragment = GuestBookTemplateDetailFragment.newInstance(template.getId(),
                template.getName());
        executeFragmentTransaction(fragment, R.id.fragment_container, true, true);
    }

    @Override
    public void navigateToUpdateBookInfo() {
        GuestBookUpdateBookInfoFragment fragment = GuestBookUpdateBookInfoFragment.newInstance();
        executeFragmentTransaction(fragment, R.id.fragment_container, true, true);
    }

    @Override
    public void navigateToBookPreview() {
        if (GuestBookHelper.getInstance(mApplication).isCurrentBookEmpty()) {
            showToast(R.string.request_send_error);
        } else {
            Intent previewIntent = new Intent(GuestBookActivity.this, GuestBookPreviewActivity.class);
            startActivity(previewIntent, true);
        }
    }

    @Override
    public void navigateToBookDetail(Book book, boolean isNew) {
        GuestBookHelper.getInstance(mApplication).setCurrentBookEditor(book);
        GuestBookMyBookDetailFragment fragment = GuestBookMyBookDetailFragment.newInstance();
        /*if (isNew) {
            executeAndRemoveCurrentFragmentTransaction(fragment, R.id.fragment_container);
        } else {*/
        executeFragmentTransaction(fragment, R.id.fragment_container, true, true);
        //}
    }

    @Override
    public void navigateToEditPage(Page page) {
        GuestBookHelper.getInstance(mApplication).navigateToGuestBookEditor(this, page);
    }

    private void displayHomeFragment() {
        GuestBookMainFragment homeFragmnet = GuestBookMainFragment.newInstance();
        executeFragmentTransaction(homeFragmnet, R.id.fragment_container, false, false);
    }

    private void displayHomeFriendFragment(String jidFriend, String friendName) {
        GuestBookMainFriendFragment mainFriendFragment = GuestBookMainFriendFragment.newInstance(jidFriend, friendName);
        executeFragmentTransaction(mainFriendFragment, R.id.fragment_container, false, false);
    }

    private void displayTabVoteFragment() {
        GuestBookVoteTabFragment voteFragment = GuestBookVoteTabFragment.newInstance();
        executeFragmentTransaction(voteFragment, R.id.fragment_container, false, false);
    }
}

package com.metfone.selfcare.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.ActivityShareAndGetMoreBinding;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.ShareAndGetMoreContract;
import com.metfone.selfcare.model.account.IdentifyProvider;
import com.metfone.selfcare.model.account.InvitedCodeResponse;
import com.metfone.selfcare.model.account.Services;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.camid.OpenIdConfig;
import com.metfone.selfcare.module.home_kh.fragment.setting.share.ShareAndGetMorePresenter;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdEditText;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import retrofit2.Response;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;
import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

public class ShareAndGetMoreActivity extends BaseSlidingFragmentActivity implements View.OnClickListener, ShareAndGetMoreContract.View  {
    ActivityShareAndGetMoreBinding binding;
    BaseMPSuccessDialog baseMPSuccessDialog;
    ShareAndGetMorePresenter shareAndGetMorePresenter;
    private static final String TIMES_ONLINE = "TIMES_ONLINE";
    private static final String BONUS_SCORE_CODE = "BONUS_SCORE_CODE";
    AtomicReference<String> param1 = new AtomicReference<>("");
    AtomicReference<String> param2 = new AtomicReference<>("");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_share_and_get_more);
        binding.setHandlers(this);
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        changeStatusBar(Color.parseColor("#1F1F1F"));
        initViews();
        handleEvents();
        setupUI(binding.frameParent);
        shareAndGetMorePresenter = new ShareAndGetMorePresenter(this);
        shareAndGetMorePresenter.getListConfig();
    }

    private void initViews() {
//        binding.edtCode.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(6)});
        binding.btnConfirm.setEnabled(false);
        binding.btnConfirm.setBackground(ContextCompat.getDrawable(ShareAndGetMoreActivity.this, R.drawable.bg_grey_corner_32));
        binding.edtCode.setCursorVisible(false);
    }

    private void handleEvents() {
        binding.edtCode.setOnClickListener(this);
        binding.mLineearLayout.setOnClickListener(this);
        binding.edtCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 6) {
                    binding.btnConfirm.setEnabled(false);
                    binding.btnConfirm.setBackground(ContextCompat.getDrawable(ShareAndGetMoreActivity.this, R.drawable.bg_grey_corner_32));
                } else {
                    binding.btnConfirm.setEnabled(true);
                    binding.btnConfirm.setBackground(ContextCompat.getDrawable(ShareAndGetMoreActivity.this, R.drawable.bg_button_corner_32_red));
                }
                if (binding.tvError.getVisibility() == View.VISIBLE) {
                    binding.edtCode.setBackgroundDrawable(ContextCompat.getDrawable(ShareAndGetMoreActivity.this, R.drawable.bg_input_text));
                    binding.tvError.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnConfirm:
                if (NetworkHelper.isConnectInternet(this)) {
                    invitedCode(binding.edtCode.getText().toString());
                } else {
                    showError(getString(R.string.error_internet_disconnect), null);
                }
                break;
            case R.id.btnSkip:
                NavigateActivityHelper.navigateToHomeScreenActivity(ShareAndGetMoreActivity.this, false, true);
                break;
            case R.id.edtCode:
                binding.edtCode.setCursorVisible(true);
                break;
            case R.id.mLineearLayout:
                binding.edtCode.setCursorVisible(false);
                break;
        }
    }

    private void invitedCode(String code) {
        binding.pbLoading.setVisibility(View.VISIBLE);
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.invitedCode(code, new ApiCallback<InvitedCodeResponse>() {
            @Override
            public void onResponse(Response<InvitedCodeResponse> response) {
                binding.pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    String code = response.body().getCode();
                    Log.d(TAG, "onResponse: " + response.body().getMessage());

                    if ("00".equals(code)) {
                        baseMPSuccessDialog = new BaseMPSuccessDialog(ShareAndGetMoreActivity.this, R.drawable.img_dialog_1, getString(R.string.title_upper_successful),
                             getString(R.string.content_success_share_and_get_more,param1.get(),param2.get()));
                        baseMPSuccessDialog.show();
                        getSupportFragmentManager().executePendingTransactions();
                        if (baseMPSuccessDialog != null) {
                            baseMPSuccessDialog.setOnDismissListener(dialogInterface -> {
                                NavigateActivityHelper.navigateToHomeScreenActivity(ShareAndGetMoreActivity.this, false, false);
                            });
                        }
                    } else {
                        binding.tvError.setVisibility(View.VISIBLE);
                        binding.edtCode.setBackgroundDrawable(ContextCompat.getDrawable(ShareAndGetMoreActivity.this, R.drawable.bg_error_input_text));
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                binding.pbLoading.setVisibility(View.VISIBLE);
                binding.tvError.setVisibility(View.VISIBLE);
                Log.e(TAG, "onError: ", error);
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        shareAndGetMorePresenter.onClear();
    }

    @Override
    public void onBackPressed() {
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(this);
        List<IdentifyProvider> identifyProviderList = userInfoBusiness.getIdentifyProviderList();
        if (identifyProviderList != null && identifyProviderList.size() > 1 && !UserInfoBusiness.checkEmptyPhoneNumber(this)) {
            NavigateActivityHelper.navigateToHomeScreenActivity(ShareAndGetMoreActivity.this, false, false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSuccess(List<OpenIdConfig> listConfigResponse) {
        for (OpenIdConfig openIdConfig : listConfigResponse) {
            if (openIdConfig.getKey().equals(TIMES_ONLINE)) {
                param1.set(openIdConfig.getValue());
                Log.d("TAG", "getListConfig: " + param1);
            }
            if (openIdConfig.getKey().equals(BONUS_SCORE_CODE)) {
                param2.set(openIdConfig.getValue());
            }
        }

    }

    @Override
    public void onFail(String message) {
    }

    @Override
    public void setPresenter(ShareAndGetMoreContract.Presenter presenter) {

    }
    public void setupUI(View view) {
        if (!(view instanceof FrameLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                 UserInfoBusiness.hideKeyboard(ShareAndGetMoreActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if(innerView instanceof CamIdEditText){
                    continue;
                }
                setupUI(innerView);
            }
        }
    }
}
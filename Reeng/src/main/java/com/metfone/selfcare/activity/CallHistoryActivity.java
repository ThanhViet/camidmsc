package com.metfone.selfcare.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.call.CallHistoryFragment;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.listeners.InitDataListener;

public class CallHistoryActivity extends BaseSlidingFragmentActivity implements InitDataListener{

    private ApplicationController mApplication;
    private Handler mHandler;

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        mApplication = (ApplicationController) getApplicationContext();
        changeWhiteStatusBar();
        setContentView(R.layout.activity_detail_history_phone);
        findComponentViews();
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_title_center, null));
        setUpCustomToolbar();
        getDataAndDisplayFragment();
    }

    private void setUpCustomToolbar() {
        View abView = getToolBarView();
        ImageView btnBack = abView.findViewById(R.id.ab_back_btn);
        btnBack.setOnClickListener(view -> onBackPressed());
        TextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(getString(R.string.menu_call_history));
    }

    @Override
    public void onResume() {
        ListenerHelper.getInstance().addInitDataListener(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        ListenerHelper.getInstance().removeInitDataListener(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mHandler = null;
        super.onDestroy();
    }

    @Override
    public void onDataReady() {
        mHandler.post(this::displayFragment);
    }

    private void findComponentViews() {
    }

    private void getDataAndDisplayFragment() {
        if (!mApplication.isDataReady()) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            removeFragment(fragment);
        } else {
            displayFragment();
        }
    }


    private void displayFragment() {
        CallHistoryFragment callHistoryDetailFragment = CallHistoryFragment.newInstance();
        executeFragmentTransaction(callHistoryDetailFragment, R.id.fragment_container, false, false);
    }
}
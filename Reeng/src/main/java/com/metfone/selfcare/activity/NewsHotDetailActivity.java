package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.adapter.NewsHotDetailAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.NewsHotDetailModel;
import com.metfone.selfcare.database.model.onmedia.NewsHotModel;
import com.metfone.selfcare.database.model.onmedia.RestNewsHot;
import com.metfone.selfcare.database.model.onmedia.RestNewsHotDetail;
import com.metfone.selfcare.restful.WSOnMedia;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotDetailActivity extends BaseSlidingFragmentActivity implements NewsHotDetailAdapter.NewsHotInterface, View.OnClickListener{

    private RecyclerView recyclerView;
    private View layout_back;

    private NewsHotDetailAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private NewsHotModel newsHotModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_hot_detail);
        changeStatusBar(true);
        newsHotModel = (NewsHotModel) getIntent().getSerializableExtra("NEWS_HOT_ITEM");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layout_back = findViewById(R.id.layout_back);
        layout_back.setOnClickListener(this);

        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new NewsHotDetailAdapter(getBaseContext(), newsHotModel, this);
        recyclerView.setAdapter(adapter);

        loadData();
    }

    private void loadData()
    {
        if(newsHotModel == null) return;

        final WSOnMedia rest = new WSOnMedia((ApplicationController) getApplication());
        rest.getNewsHotDetail(newsHotModel.getPid().toString(), newsHotModel.getCid().toString(), newsHotModel.getID().toString(), new Response.Listener<RestNewsHotDetail>() {
            @Override
            public void onResponse(RestNewsHotDetail restNewsHot) {
                if(restNewsHot != null)
                {
                    if(restNewsHot.getData() != null && restNewsHot.getData().getBody() != null && restNewsHot.getData().getBody().size() > 0)
                    {
                        newsHotModel.getBody().clear();
                        newsHotModel.getBody().addAll(restNewsHot.getData().getBody());
                        adapter.setDatas(restNewsHot.getData().getBody());

                        loadDataRelate(newsHotModel.getPid(), newsHotModel.getCid(), newsHotModel.getID());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

    }

    private void loadDataRelate(int pid, int cid, int contentId)
    {
        final WSOnMedia rest = new WSOnMedia((ApplicationController) getApplication());
        rest.getNewsHotRelate(pid, cid, contentId, new Response.Listener<RestNewsHot>() {
            @Override
            public void onResponse(RestNewsHot restNewsHot) {
                if(restNewsHot != null)
                {
                    if(restNewsHot.getData() != null && restNewsHot.getData().size() > 0)
                    {
                        adapter.setRelateDatas(restNewsHot.getData());
                        adapter.notifyDataSetChanged();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

    }

    @Override
    public void onClickImageItem(NewsHotDetailModel entry) {

    }

    @Override
    public void onClickVideoItem(NewsHotDetailModel entry) {

    }

    @Override
    public void onClickRelateItem(NewsHotModel entry) {
        Intent intent = new Intent(getApplication(), NewsHotDetailActivity.class);
        intent.putExtra("NEWS_HOT_ITEM", entry);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.layout_back:
                onBackPressed();
                break;
        }
    }
}

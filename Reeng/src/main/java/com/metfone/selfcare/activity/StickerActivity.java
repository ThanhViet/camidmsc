package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.fragment.sticker.PreviewStickerFragment;
import com.metfone.selfcare.fragment.sticker.StickerStoreFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

/**
 * Created by huybq7 on 2/11/2015.
 */
public class StickerActivity extends BaseSlidingFragmentActivity {
    private static String TAG = StickerActivity.class.getSimpleName();
    public static final String PARAM_STICKER_COLLECTION_ID = "sticker_collection_id";
    public static final String PARAM_UPDATE_STICKER = "update_sticker";
    private int mCollectionId;
    private TextView mTvwTitle;
    private Button buySticker, mySticker;
    private View buyStickerView,myStickerView;
    private LinearLayout tabLayout;
    private boolean isUpdateSticker;
    private boolean checkPreview = false;
    private ChatActivity mParentActivity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate StickerActivity");
        Log.i(TAG, " onCreate ... ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticker_store);
        changeStatusBar(true);
        // action bar
        setActionBar();
        // get param
        getParam();
        trackingScreen(TAG);
        setColorStatusBar(R.color.white);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        /*if (downloadStickerAsynctask != null) {
            downloadStickerAsynctask.cancel(true);
        }*/

        super.onPause();
    }

    private void getParam() {
        mCollectionId = getIntent().getIntExtra(PARAM_STICKER_COLLECTION_ID, 0);
        isUpdateSticker = getIntent().getBooleanExtra(StickerActivity.PARAM_UPDATE_STICKER, false);
        if (mCollectionId == 0) {
            StickerStoreFragment stickerStoreFragment = StickerStoreFragment.newInstance(Constants.STICKER.STORE);
            executeFragmentTransaction(stickerStoreFragment, R.id.fragment_container, false, true);
        } else {
            goToPreviewSticker(mCollectionId, isUpdateSticker, false);
        }
    }

    private void setActionBar() {
        LayoutInflater mLayoutInflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        View abView = findViewById(R.id.tool_bar);
        View layoutView = findViewById(R.id.tabLayout);
        setToolBar(abView);
        setCustomViewToolBar(View.inflate(getApplicationContext(), R.layout.ab_title_center, null));

        mTvwTitle = (TextView) abView.findViewById(R.id.ab_title);
        buySticker = (Button) layoutView.findViewById(R.id.buySticker);
        buyStickerView = (View) layoutView.findViewById(R.id.buyStickerView);
        myStickerView = (View) layoutView.findViewById(R.id.myStickerView);
        tabLayout = layoutView.findViewById(R.id.tabLayout);

        buySticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StickerStoreFragment stickerStoreFragment = StickerStoreFragment.newInstance(Constants.STICKER.STORE);
                executeFragmentTransaction(stickerStoreFragment, R.id.fragment_container, true, true);
                buySticker.setTextColor(getResources().getColor(R.color.bg_button_red));
                buySticker.setAlpha(1);
                buyStickerView.setBackgroundColor(getResources().getColor(R.color.bg_button_red));
                mySticker.setTextColor(getResources().getColor(R.color.color_tvphone));
                myStickerView.setBackgroundColor(getResources().getColor(R.color.transparent));
            }
        });
        mySticker = (Button) layoutView.findViewById(R.id.mySticker);
        mySticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StickerStoreFragment stickerStoreFragmentMySticker = StickerStoreFragment.newInstance(Constants.STICKER.COLLECTION);
                executeFragmentTransaction(stickerStoreFragmentMySticker, R.id.fragment_container, true, true);
                buySticker.setTextColor(getResources().getColor(R.color.color_tvphone));
                buyStickerView.setBackgroundColor(getResources().getColor(R.color.transparent));
                mySticker.setTextColor(getResources().getColor(R.color.bg_button_red));
                mySticker.setAlpha(1);
                myStickerView.setBackgroundColor(getResources().getColor(R.color.bg_button_red));
            }
        });
        mTvwTitle.setText(getString(R.string.sticker_store));
        buySticker.setTextColor(getResources().getColor(R.color.bg_button_red));
        buySticker.setAlpha(1);
        buyStickerView.setBackgroundColor(getResources().getColor(R.color.bg_button_red));
        mySticker.setTextColor(getResources().getColor(R.color.color_tvphone));
        myStickerView.setBackgroundColor(getResources().getColor(R.color.transparent));
        //
        ImageView mImgBack = (ImageView) abView.findViewById(R.id.ab_back_btn);
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    public void onBackPressed() {
        finish();
    }
    public void getShowTablayout(){
        tabLayout.setVisibility(View.VISIBLE);
    }
    public boolean getCheckPreview(){
        return checkPreview;
    }
    public void goToPreviewSticker(int collectionId, boolean isUpdate, boolean isAddStackBack) {
        tabLayout.setVisibility(View.GONE);
        checkPreview = true;
        PreviewStickerFragment previewStickerFragment = PreviewStickerFragment.newInstance(collectionId, isUpdate);
        executeFragmentTransaction(previewStickerFragment, R.id.fragment_container, isAddStackBack, true);
    }
}
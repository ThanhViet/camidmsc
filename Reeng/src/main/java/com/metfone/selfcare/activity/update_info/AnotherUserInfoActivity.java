package com.metfone.selfcare.activity.update_info;

import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.ActivityAnotherPersonInfoBinding;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.util.EnumUtils;

public class AnotherUserInfoActivity extends BaseSlidingFragmentActivity {

    private ActivityAnotherPersonInfoBinding binding;
    private UserIdentityInfo userIdentityInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAnotherPersonInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        userIdentityInfo = (UserIdentityInfo) getIntent().getSerializableExtra(EnumUtils.OBJECT_KEY);

        initData();

        initAction();
    }

    private void initData() {
        if (userIdentityInfo != null) {
            binding.edtFullName.setText(userIdentityInfo.getSubName());
            binding.edtDob.setText(DateTimeUtils.dateStrToStringMP(userIdentityInfo.getSubDateBirth()));

            String gender = userIdentityInfo.getSubGender();
            String relationShip = userIdentityInfo.getRelationship();
            if (gender != null && !gender.isEmpty()) {
                //Male
                if (gender.equals("M")) binding.spnSex.setText(R.string.sex_male);
                    //Female
                else if (gender.equals("F")) binding.spnSex.setText(R.string.sex_female);
                    //Other
                else binding.spnSex.setText(R.string.sc_other);
            }
            if (relationShip.equals("1")) {
                binding.edtRelationship.setText(R.string.relationship_parent);
            } else if (relationShip.equals("2")) {
                binding.edtRelationship.setText(R.string.relationship_child);
            } else {
                binding.edtRelationship.setText(R.string.empty_string);
            }
        }
    }

    private void initAction() {
        binding.btnBack.setOnClickListener(v -> onBackPressed());
        binding.btnBackButton.setOnClickListener(v -> onBackPressed());
    }
}

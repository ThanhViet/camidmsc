package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

/**
 * Created by thanhnt72 on 4/24/2019.
 */

public class SplashActivity extends BaseSlidingFragmentActivity implements ContentConfigBusiness.OnConfigChangeListener {

    private static final String TAG = SplashActivity.class.getSimpleName();
    ApplicationController mApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);
        mApp = (ApplicationController) getApplication();

        Log.i(TAG, "isLoad ReengAccount Done: " + mApp.getReengAccountBusiness().isLoadDataDone());
        if (mApp.getReengAccountBusiness().isLoadDataDone()) {
            onLoadedReengAccount();
        } else {
            if (!EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().register(this);
            }
        }
    }

    @Override
    protected void onDestroy() {
        Log.i("SplashActivity", "ondestroy");
        ListenerHelper.getInstance().removeConfigChangeListener(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void onLoadedReengAccount() {
        Log.i(TAG, "onLoadedReengAccount");
        if (!mApp.getReengAccountBusiness().isValidAccount()) {
            Log.i(TAG, "login anonymous");
            mApp.getApplicationComponent().provideUserApi().getInfoAnonymous(BuildConfig.KEY_XXTEA, new ApiCallbackV2<String>() {
                @Override
                public void onSuccess(String lastId, String s) throws JSONException {
                    ListenerHelper.getInstance().addConfigChangeListener(SplashActivity.this);
                    mApp.getConfigBusiness().getConfigFromServer(true);
                }

                @Override
                public void onError(String s) {
//                    showToast(s);
                    Log.e("SplashActivity", "error:" + s);
                }

                @Override
                public void onComplete() {
                    hideLoadingDialog();
                }
            });

        } else {
            Log.i(TAG, "goto home");
            gotoHomeActivity();
        }
    }

    @Override
    public void onConfigStickyBannerChanged() {

    }

    private void gotoHomeActivity() {
        Log.i(TAG, "postDelayed gotoHomeActivity");
        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(null);
        }
    }

    @Override
    public void onConfigTabChange() {
        Log.i(TAG, "onconfigtabchange");
        gotoHomeActivity();
    }

    @Override
    public void onConfigBackgroundHeaderHomeChange() {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInitReengAccountEvent(final InitReengAccount event) {
        Log.i(TAG, "onInitReengAccountEvent");
        onLoadedReengAccount();
    }

    public static class InitReengAccount {

        public InitReengAccount() {

        }
    }
}

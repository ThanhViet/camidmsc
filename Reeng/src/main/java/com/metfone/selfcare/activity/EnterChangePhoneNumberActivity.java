package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.OTPChangePhoneActivity;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.CheckPhoneInfo;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdEditText;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

public class EnterChangePhoneNumberActivity extends BaseSlidingFragmentActivity {
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    @BindView(R.id.etNumber)
    AppCompatEditText etNumber;
    @BindView(R.id.rll_change_phone)
    RelativeLayout mRllChangePhone;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    BaseMPConfirmDialog baseMPConfirmDialog;
    BaseMPSuccessDialog baseMPSuccessDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_change_phone_number);
        ButterKnife.bind(this);
        changeStatusBar(Color.parseColor("#1F1F1F"));
        setPhoneNumberEditTextListener();
        enableOrDisableContinueButton();
        setupUI(mRllChangePhone);
        ivBack.setOnClickListener(view -> finish());
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = etNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                } else {
                    String phoneNumber = etNumber.getText().toString();
                    if (!phoneNumber.startsWith("0")){
                        phoneNumber = "0" + phoneNumber;
                    }
                    checkPhone(phoneNumber);
//                    processNext();
                }
            }
        });

    }
    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(EnterChangePhoneNumberActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if(innerView instanceof RoundTextView || innerView instanceof EditText){
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        etNumber.requestFocus();
        UIUtil.showKeyboard(getContext(),etNumber);
    }

    private void checkPhone(String phoneNumber) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        CheckPhoneInfo checkPhoneInfo = new CheckPhoneInfo(phoneNumber);
        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest(checkPhoneInfo, "", "", "", "");
        apiService.checkPhone(checkPhoneRequest).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, retrofit2.Response<BaseResponse<String>> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getCode().equals("00") || response.body().getCode().equals("42") || response.body().getCode().equals("68")) {
//                        checkPhoneCode = 42;
                        //case exist in camID
                        baseMPConfirmDialog = new BaseMPConfirmDialog("lottie/img_dialog_man_thinking.json", R.string.m_p_dialog_combine_phone_title,
                                R.string.m_p_dialog_combine_phone_content, R.string.m_p_dialog_combine_phone_top_btn, R.string.m_p_dialog_combine_phone_bottom_btn,
                                view -> {
                                    //letdoit
//                                    updateLogin(phoneNumber, etOtp.getText().toString());
                                    processNext(42);
                                    if (baseMPConfirmDialog != null) {
                                        baseMPConfirmDialog.dismiss();
                                    }
                                }, view -> {
                            //back

                            if (baseMPConfirmDialog != null) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
                                setResult(Activity.RESULT_OK, returnIntent);
                                setResult(RESULT_OK);
                                finish();
                                baseMPConfirmDialog.dismiss();
                            }
                        });
                        baseMPConfirmDialog.show(getSupportFragmentManager(), null);
                        //case full service
                    } else {
//                        updateLogin(phoneNumber, etOtp.getText().toString());
                        processNext(0);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(EnterChangePhoneNumberActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void processNext(int code) {
        String phoneNumber = etNumber.getText().toString();
        Intent intent = new Intent(EnterChangePhoneNumberActivity.this, OTPChangePhoneActivity.class);
        if (phoneNumber.startsWith("0")){
            intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        }else {
            intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, "0" + phoneNumber);
        }
        intent.putExtra(EnumUtils.OBJECT_KEY, code);
        startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
    }

    private void setPhoneNumberEditTextListener() {
        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                enableOrDisableContinueButton();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

//        etNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    String phone = etNumber.getText().toString();
//                    if (TextUtils.isEmpty(phone)) {
//                        ToastUtils.showToast(getContext(), "Phone number must be not empty!");
//                    } else if (phone.length() < 6 || phone.length() > 13) {
//                        ToastUtils.showToast(getContext(), "Phone number is not valid!");
//                    } else {
//                        processNext();
//                    }
//                }
//                return false;
//            }
//        });
    }

    /**
     * enable or disable continue Button
     */
    private void enableOrDisableContinueButton() {
        if (etNumber.getText().toString().trim().length() >= Constants.CONTACT.MIN_LENGTH_NUMBER) {
            setEnableButton(true);
        } else {
            setEnableButton(false);
        }
    }

    private void setEnableButton(boolean enableButton) {
        if (btnContinue != null) {
            btnContinue.setClickable(enableButton);
            if (enableButton) {
                btnContinue.setTextColor(ContextCompat.getColor(this, R.color.v5_text_7));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(this, R.color.button_continue),
                        ContextCompat.getColor(this, R.color.button_continue));
            } else {
                btnContinue.setTextColor(ContextCompat.getColor(this, R.color.v5_text_3));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(this, R.color.v5_cancel),
                        ContextCompat.getColor(this, R.color.v5_cancel));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EnumUtils.MY_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            int result= data.getIntExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
            Intent returnIntent = new Intent();
            android.util.Log.d(TAG, "onActivityResult: " + result);
            returnIntent.putExtra(EnumUtils.RESULT, result);
            setResult(Activity.RESULT_OK,returnIntent);
            if (result ==  EnumUtils.KeyBackTypeDef.DONE){
                finish();
            }
        }
    }
}
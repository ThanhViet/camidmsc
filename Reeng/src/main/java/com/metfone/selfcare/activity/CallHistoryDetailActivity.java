package com.metfone.selfcare.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.call.CallSubscription;
import com.metfone.selfcare.fragment.call.CallHistoryDetailFragment;
import com.metfone.selfcare.fragment.call.CallSubscriptionFragment;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 10/14/2016.
 */

public class CallHistoryDetailActivity extends BaseSlidingFragmentActivity implements InitDataListener,
        CallHistoryDetailFragment.OnFragmentInteractionListener {
    private final String TAG = CallHistoryDetailActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private Handler mHandler;
    private long historyId;
    private int fragmentType;
    private CallSubscription callSubscription;

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, " onCreate ... ");
        mHandler = new Handler();
        mApplication = (ApplicationController) getApplicationContext();
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        findComponentViews();
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_detail, null));
        getDataAndDisplayFragment(savedInstanceState);
    }

    @Override
    public void onResume() {
        ListenerHelper.getInstance().addInitDataListener(this);
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID, historyId);
        outState.putInt(CallHistoryConstant.FRAGMENT_HISTORY_TYPE, fragmentType);
        outState.putSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA, callSubscription);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        ListenerHelper.getInstance().removeInitDataListener(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mHandler = null;
        super.onDestroy();
    }

    @Override
    public void onDataReady() {
        mHandler.post(this::displayFragment);
    }

    @Override
    public void navigateToFriendProfile(String friendNumber) {
        Intent intent = new Intent(getApplicationContext(), ContactDetailActivity.class);
        Bundle bundle = new Bundle();
        PhoneNumber phone = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendNumber);
        if (phone != null) {
            bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
            bundle.putString(NumberConstant.ID, phone.getId());
        } else {
            StrangerPhoneNumber stranger = mApplication.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(friendNumber);
            if (stranger != null) {
                bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_STRANGER_EXIST);
                bundle.putString(NumberConstant.NAME, stranger.getFriendName());
                bundle.putString(StrangerConstant.STRANGER_JID_NUMBER, stranger.getPhoneNumber());
                bundle.putString(NumberConstant.LAST_CHANGE_AVATAR, stranger.getFriendAvatarUrl());
            } else {
                bundle.putInt(NumberConstant.CONTACT_DETAIL_TYPE, NumberConstant.TYPE_CONTACT);
                bundle.putString(NumberConstant.NUMBER, friendNumber);
            }
        }
        intent.putExtras(bundle);
        startActivity(intent, true);
    }

    @Override
    public void navigateToChatActivity(String number) {
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(number);
        NavigateActivityHelper.navigateToChatDetail(CallHistoryDetailActivity.this, thread);
        //bundle.putBoolean(ChatActivity.BUNDLE_BACK_NORMAL, true);
    }

    private void findComponentViews() {
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (savedInstanceState != null) {
            historyId = savedInstanceState.getLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID);
            fragmentType = savedInstanceState.getInt(CallHistoryConstant.FRAGMENT_HISTORY_TYPE);
            callSubscription = (CallSubscription) savedInstanceState.getSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA);
        } else if (bundle != null) {
            historyId = bundle.getLong(CallHistoryConstant.FRAGMENT_HISTORY_DETAIL_ID);
            fragmentType = bundle.getInt(CallHistoryConstant.FRAGMENT_HISTORY_TYPE);
            callSubscription = (CallSubscription) bundle.getSerializable(CallHistoryConstant.FRAGMENT_CALLOUT_DATA);
        }
        if (!mApplication.isDataReady()) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            removeFragment(fragment);
        } else {
            displayFragment();
        }
    }


    CallSubscriptionFragment callSubscriptionFragment;

    private void displayFragment() {
        if (fragmentType == CallHistoryConstant.HISTORY_TYPE_DETAIL) {
            CallHistoryDetailFragment callHistoryDetailFragment = CallHistoryDetailFragment.newInstance(historyId);
            executeFragmentTransaction(callHistoryDetailFragment, R.id.fragment_container, false, false);
        } else {    // (fragmentType == CallHistoryConstant.HISTORY_TYPE_SUBSCRIPTION)
            callSubscriptionFragment = CallSubscriptionFragment.newInstance(callSubscription);
            executeFragmentTransaction(callSubscriptionFragment, R.id.fragment_container, false, false);
        }
    }

    public void reloadDataCallSubscription() {
        if (callSubscriptionFragment != null && callSubscriptionFragment.isVisible()) {
            callSubscriptionFragment.reloadData();
        }
    }
}
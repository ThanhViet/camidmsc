package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.musickeeng.CreateInviteListenTogetherFragment;
import com.metfone.selfcare.fragment.musickeeng.SearchUserFragment;
import com.metfone.selfcare.fragment.musickeeng.TopSongFragment;
import com.metfone.selfcare.fragment.musickeeng.UploadSongFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.util.Log;


public class SearchSongActivity extends BaseSlidingFragmentActivity implements
        TopSongFragment.OnFragmentInteractionListener,
        UploadSongFragment.OnFragmentInteractionListener, TextWatcher {
    private static final String TAG = SearchSongActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private TopSongFragment mTopSongFragment;
    private SearchUserFragment mSearchUserFragment;
    private int actionCode;
    private int threadId = -1;
    private String friendNumber, friendName, friendAvatar;
    private ThreadMessage threadMessage;
    private boolean isSearchUser = false;
    private boolean uploadSong = false;
    private String mJid;
    private ContactBusiness mContactBusiness;
//    private AppBarLayout appBarLayout;
//    private AppCompatEditText edtSearch;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setWhiteStatusBar();
        mApplication = (ApplicationController) getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mJid = mApplication.getReengAccountBusiness().getJidNumber();
        mContactBusiness = mApplication.getContactBusiness();
        setContentView(R.layout.activity_search_song_v5);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            actionCode = bundle.getInt("actionCode");
            threadId = bundle.getInt("threadId", -1);
            friendNumber = bundle.getString("friendNumber");
            friendName = bundle.getString("friendName");
            friendAvatar = bundle.getString("friendAvatar");
            isSearchUser = bundle.getBoolean(Constants.MOCHA_INTENT.EXTRA_INTENT_SEARCH_USER);
            uploadSong = bundle.getBoolean("uploadSong");
        } else if (savedInstanceState != null) {
            actionCode = savedInstanceState.getInt("actionCode");
            threadId = savedInstanceState.getInt("threadId", -1);
            friendNumber = savedInstanceState.getString("friendNumber");
            friendName = savedInstanceState.getString("friendName");
            friendAvatar = savedInstanceState.getString("friendAvatar");
            isSearchUser = savedInstanceState.getBoolean(Constants.MOCHA_INTENT.EXTRA_INTENT_SEARCH_USER);
            uploadSong = savedInstanceState.getBoolean("uploadSong");
        }
        findComponentViews();
        setComponentViews();
        if (isSearchUser) {
            displaySearchUserFragment();
        }

        /*else if (isEnableUpload()) {
//            displaySearchSongFragment();
        }*/

        else if (uploadSong) {
            navigateToUploadSong(false);
        } else {
//            appBarLayout.setVisibility(View.GONE);
            displayTopSongFragment();
        }
        trackingScreen(TAG);
    }

    private void displaySearchUserFragment() {
        mSearchUserFragment = SearchUserFragment.newInstance();
        executeFragmentTransaction(mSearchUserFragment, R.id.fragment_container, false, false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            actionCode = intent.getIntExtra("actionCode", -1);
            threadId = intent.getIntExtra("threadId", -1);
            friendNumber = intent.getStringExtra("friendNumber");
            friendName = intent.getStringExtra("friendName");
            friendAvatar = intent.getStringExtra("friendAvatar");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("actionCode", actionCode);
        outState.putInt("threadId", threadId);
        outState.putString("friendNumber", friendNumber);
        outState.putString("friendName", friendName);
        outState.putString("friendAvatar", friendAvatar);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    @Override
    public void navigateToWebView(String url) {
        UrlConfigHelper.openBrowser(this, url);
    }

    @Override
    public void processResultSong(MediaModel songModel) {
        if (songModel == null) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        Log.i(TAG, " processResultSong : " + songModel.getName());
        switch (actionCode) {
            case Constants.ACTION.SELECT_SONG:
                threadMessage = mMessageBusiness.findThreadByThreadId(threadId);
                if (threadMessage != null) {
                    mApplication.getMessageBusiness().createAndSendMessageInviteMusic(threadMessage, songModel, SearchSongActivity.this);
                    finish();
                } else {
                    returnResultSong(songModel);
                }
                break;
            case Constants.ACTION.CHANGE_SONG:
                threadMessage = mMessageBusiness.findThreadByThreadId(threadId);
                if (threadMessage != null) {
                    boolean isRequestChange = !threadMessage.isAdmin();
                    mApplication.getMessageBusiness().createAndSendMessageChangeMusic(threadMessage,
                            songModel, SearchSongActivity.this, isRequestChange);
                    finish();
                } else {
                    returnResultSong(songModel);
                }
                break;
            case Constants.ACTION.STRANGER_MUSIC_SELECT_SONG:
                returnResultSong(songModel);
                break;
            case Constants.ACTION.SELECT_SONG_AND_CREATE_THREAD:
                if (TextUtils.isEmpty(friendNumber)) {
                    finish();
                } else {
                    mApplication.trackingEvent(R.string.ga_category_stranger_music, R.string.ga_action_interaction, R.string.ga_label_around_invite);
                    String mName = mApplication.getReengAccountBusiness().getUserName();
                    threadMessage = mApplication.getStrangerBusiness().processOutGoingStrangerMusic(mName, friendNumber,
                            friendName, friendAvatar, Constants.CONTACT.STRANGER_MUSIC_ID);
                    mApplication.getMessageBusiness().createAndSendMessageInviteMusic(threadMessage, songModel, SearchSongActivity.this);
                    navigateToChat(threadMessage);
                }
                break;
            case Constants.ACTION.CHANGE_SONG_AND_CREATE_THREAD:
                if (TextUtils.isEmpty(friendNumber)) {
                    finish();
                } else {
                    String mName = mApplication.getReengAccountBusiness().getUserName();
                    threadMessage = mApplication.getStrangerBusiness().processOutGoingStrangerMusic(mName, friendNumber,
                            friendName, friendAvatar, Constants.CONTACT.STRANGER_MUSIC_ID);
                    mApplication.getMessageBusiness().createAndSendMessageInviteMusic(threadMessage, songModel, SearchSongActivity.this);
                    mApplication.getMessageBusiness().createAndSendMessageChangeMusic(threadMessage, songModel, SearchSongActivity.this, false);
                    navigateToChat(threadMessage);
                }
                break;
            default:
                returnResultSong(songModel);
                break;
        }
    }

    public void returnResultSong(MediaModel songModel) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.KEENG_MUSIC.SONG_OBJECT, songModel);
        setResult(RESULT_OK, returnIntent);
        finish();
    }


    public void navigateToChat(ThreadMessage threadMessage) {
        if (threadMessage != null) {
            NavigateActivityHelper.navigateToChatDetail(SearchSongActivity.this, threadMessage);
            //overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }
        finish();
    }

    private void findComponentViews() {
//        setToolBar(findViewById(R.id.tool_bar));
//        setCustomViewToolBar(getLayoutInflater().inflate(R.layout.ab_search_box, null));
//        edtSearch = findViewById(R.id.edtSearch);
//        appBarLayout = findViewById(R.id.appBarLayout);
//        edtSearch.addTextChangedListener(this);
//        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
////                    textSearch = mSearchView.getText().toString().trim();
////                    handleSearch(page);
////                    mPrbLoading.setVisibility(View.VISIBLE);
//                }
//                return false;
//            }
//        });
    }

    private void setComponentViews() {
        // setActionBar();
    }

    public void displayTopSongFragment() {
        executeFragmentTransaction( CreateInviteListenTogetherFragment.newInstance(), R.id.fragment_container, false, false);
    }

    public void navigateToProfile(PhoneNumber phoneNumber) {
        Log.i(TAG, "phoneNumber: " + phoneNumber.toString());
        if (phoneNumber.getJidNumber().equals(mJid)) {
            NavigateActivityHelper.navigateToMyProfile(SearchSongActivity.this);
        } else {
            PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(phoneNumber.getJidNumber());
            if (mPhoneNumber != null && !TextUtils.isEmpty(mPhoneNumber.getId())) {
                goToContactDetail(mPhoneNumber);
            } else {
                goToStrangerDetail(phoneNumber.getJidNumber(), phoneNumber.getName(),
                        phoneNumber.getLastChangeAvatar(), phoneNumber.getStatus(), phoneNumber.getGender());
            }
        }
    }


    public void navigateToUploadSong(boolean addBackStack) {
        UploadSongFragment uploadSongFragment = UploadSongFragment.newInstance();
        executeFragmentTransaction(uploadSongFragment, R.id.fragment_container, addBackStack, false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        String query = SearchHelper.convertQuery(s.toString());
//            Fragment fragment = getSupportFragmentManager().getFragments().get(0);
//            if(fragment != null) {
//                if ( fragment instanceof UploadSongFragment) {
//                    ((UploadSongFragment) (fragment)).actionSearchSong(query);
//                } else if (fragment instanceof SearchUserFragment){
//                    ((SearchUserFragment)fragment)
//                }
//
//            }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
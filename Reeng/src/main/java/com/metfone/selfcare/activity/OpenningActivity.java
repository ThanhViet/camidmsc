package com.metfone.selfcare.activity;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIREBASE_REFRESHED_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_SEND_TOKEN_SUCCESS;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.airbnb.lottie.LottieAnimationView;
import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.service.LoadCinemaService;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.movienew.activity.SetupLanguageActivity;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import retrofit2.Response;

public class OpenningActivity extends AppCompatActivity {

    private LottieAnimationView mLavOpenning;
    private TextView mTvMetphoneIntroduce;
    private ImageView mIvOpenning;
    private VideoView videoView;
    private View layoutVideo;
    private MediaPlayer mediaPlayerVideo;

    private UserInfoBusiness userInfoBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoBusiness = new UserInfoBusiness(this);
        setContentView(R.layout.activity_openning);
        initView();

        com.metfone.selfcare.util.Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        Utilities.adaptViewForInsertBottom(mTvMetphoneIntroduce);
        if (DynamicSharePref.getInstance().get(Constants.KEY_IS_FIRST_OPEN_APP, Boolean.class)) {
            LoadCinemaService.start(OpenningActivity.this);
        }
        actionOpenning();
    }

    private void initView() {
        mLavOpenning = findViewById(R.id.lav_openning);
        mTvMetphoneIntroduce = findViewById(R.id.tv_metfone_introduce);
        mIvOpenning = findViewById(R.id.vv_openning);
        videoView = findViewById(R.id.videoView);
        layoutVideo = findViewById(R.id.layoutVideo);
        updateToken();
    }

    private void actionOpenning() {
        if (!DynamicSharePref.getInstance().get(Constants.KEY_IS_NOT_FIRST_OPENNING, Boolean.class)) {
            //set language default to khmer
            LocaleManager.setNewLocale(this, "km");
            DynamicSharePref.getInstance().put(Constants.KEY_IS_NOT_FIRST_OPENNING, true);
        }

//            Glide.with(this)
//                    .asGif()
//                    .load(R.drawable.cam)
//                    .addListener(new RequestListener<GifDrawable>() {
//                        @Override
//                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
//                            resource.setLoopCount(1);
//                            return false;
//                        }
//                    })
//                    .into(mIvOpenning);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int widthScreen = displayMetrics.widthPixels;
        int heightScreen = displayMetrics.heightPixels;
        videoView.setVideoURI(getMedia("cam_openning"));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                int videoWidth = mediaPlayer.getVideoWidth();
                int videoHeight = mediaPlayer.getVideoHeight();
                int widthProportion = widthScreen;
                int heightProportion;
                if (videoWidth == 0 || videoHeight == 0) {
                    mediaPlayerVideo = MediaPlayer.create(OpenningActivity.this, R.raw.cam_openning);
                    mediaPlayerVideo.start();
                    mediaPlayerVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            redirectActivity();
                            finish();
                        }
                    });
                } else {
                    heightProportion = widthScreen * videoHeight / videoWidth;
                    ViewGroup.LayoutParams layoutParams = videoView.getLayoutParams();
                    layoutParams.width = widthProportion;
                    layoutParams.height = heightProportion;
                    videoView.setLayoutParams(layoutParams);

                    layoutVideo.setVisibility(View.VISIBLE);
                    videoView.setBackgroundColor(Color.TRANSPARENT);
                    mediaPlayer.start();
                }
//                    if (heightProportion > heightScreen) {
//                        heightProportion = heightScreen;
//                        widthProportion = heightScreen * videoWidth / videoHeight;
//                    }
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                redirectActivity();
                finish();
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                redirectActivity();
                videoView.stopPlayback();
                finish();
                return false;
            }
        });
//        } else {
//            mLavOpenning.setVisibility(View.VISIBLE);
//            mTvMetphoneIntroduce.setVisibility(View.VISIBLE);
//            mIvOpenning.setVisibility(View.GONE);
//            mLavOpenning.playAnimation();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    redirectActivity();
//                    mLavOpenning.cancelAnimation();
//                    finish();
//                }
//            }, 2000);
//        }
    }

    private Uri getMedia(String mediaName) {
        return Uri.parse("android.resource://" + getPackageName() +
                "/raw/" + mediaName);
    }

    private void redirectActivity() {
        boolean isFirstOpenApp = DynamicSharePref.getInstance().get(Constants.KEY_IS_FIRST_OPEN_APP, Boolean.class);
        Intent intent;
        if (!isFirstOpenApp) {
            intent = new Intent(this, SetupLanguageActivity.class);
        } else {
            intent = new Intent(OpenningActivity.this, HomeActivity.class);
            Bundle bundle = getIntent().getExtras();
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean(MoviePagerFragment.LOAD_DATA_SERVICE, true);
            intent.putExtras(bundle);
            getIntent().putExtras(new Bundle());
        }
        startActivity(intent);
        finish();
    }

    private void updateToken() {
        if (!SharedPrefs.getInstance().get(PREF_SEND_TOKEN_SUCCESS, Boolean.class)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    try {
                        if (task.isSuccessful() && task.getResult() != null) {
                            String token = task.getResult().getToken();
                            String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                            Log.d("ABC","ABC 1 = " + token);
                            Log.d("ABC","ABC 2 = " + deviceId);
                            String language = LocaleManager.getLanguage(OpenningActivity.this);
                            String camId = "";
                            if (userInfoBusiness.getUser() == null || userInfoBusiness.getUser().getUser_id() != 0) {
                                camId = String.valueOf(userInfoBusiness.getUser().getUser_id());
                            }
                            MetfonePlusClient.getInstance().updateDeviceToken(camId, token, language, deviceId, new MPApiCallback<BaseResponse>() {
                                @Override
                                public void onResponse(Response<BaseResponse> response) {
                                    if (response != null && response.body() != null && response.body().getErrorCode().equals("S200")) {
                                        SharedPrefs.getInstance().put(PREF_SEND_TOKEN_SUCCESS, true);
                                    }
                                }

                                @Override
                                public void onError(Throwable error) {

                                }
                            });
                        }
                    } catch (Exception ex) {
                    }

                }
            });
        }
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/2
 *
 */

package com.metfone.selfcare.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.TagOnMediaAdapter;
import com.metfone.selfcare.adapter.dialog.BottomSheetMenuAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedAction;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.share.ShareBottomDialog;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayout;
import com.metfone.selfcare.ui.SwipyRefresh.SwipyRefreshLayoutDirection;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.recyclerview.RecyclerClickListener;
import com.metfone.selfcare.ui.tokenautocomplete.FilteredArrayAdapter;
import com.metfone.selfcare.ui.tokenautocomplete.TagsCompletionView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class WebViewNewActivity extends BaseSlidingFragmentActivity {
    public static final String ORIENTATION_AUTO = "0";

    /*
    //Các param của deeplink mocha://webapp
    //+ ref: link web
    //+ orientation: 0 (tự động), 1 (màn hình dọc), 2 (màn hình ngang). Mặc định là 0.
    //+ onmedia: 1 là hiển thị giao diện webview có tương tác onmedia. Có thanh toolbar hiển thị icon back, tiêu đề, icon option và box comment giống giao diện hiện tại. Ngược lại thì không hiển thị box comment, thanh toolbar: hiển thị icon close và tiêu đề. Mặc định là 0;
    //+ title: tiêu đề. Nếu title khác rỗng thì fix cứng tiêu đề webview còn không thì set tiêu đề theo thông tin webview trả về. Trường hợp onmedia =1 thì tiêu đề được cập nhật theo thông tin onmedia và có thể click vào được. Mặc định là rỗng.
    //+ fullscreen: 1 (full màn hình), 0 (không full màn hình). Trường hợp full màn hình thì ẩn thanh toolbar và box comment. Mặc định là 0.
    //+ close: 1 (top left nút nổi có background), 2 (top right nút nổi có background), 3 (top left không có background) , 4 (top right không có background). Icon close cho màn hình fullscreen. Mặc định là 0, không hiển thị.
    //+ expand: 1 (top), 2 (left), 3(right). Viền màu đen nằm bên trên, trái, phải của webview. Mặc định là 0, không hiển thị.
    //
    //Luồng xử lý:
    //Nếu fullscreen = 1:
    //-	Ẩn toolbar, ẩn box comment
    //-	Kiểm tra “close” để hiển thị icon close cho màn hình fullscreen
    //-	Kiểm tra “expand” để hiển thị viền màu đen
    //Ngược lại:
    //-	Nếu onmedia = 1:
    //+ Hiển thị toolbar có: icon close, có thể có icon back khi mở các link khác, tiêu đề, icon option
    //+ Hiển thị box comment
    //-	Ngược lại:
    //+ Hiển thị toolbar có: icon close, tiêu đề
    //+ Ẩn box comment
    * */
    public static final String ORIENTATION_PORTRAIT = "1";
    public static final String ORIENTATION_LANDSCAPE = "2";
    private static final String TAG = WebViewNewActivity.class.getSimpleName();
    private static final int COUNT_SHOW_DIALOG = 3;
    private static final String BUTTON_CLOSE_TOP_LEFT_FLOATING = "1";
    private static final String BUTTON_CLOSE_TOP_RIGHT_FLOATING = "2";
    private static final String BUTTON_CLOSE_TOP_LEFT = "3";
    private static final String BUTTON_CLOSE_TOP_RIGHT = "4";

    private static final String VIEW_EXPAND_TOP = "1";
    private static final String VIEW_EXPAND_LEFT = "2";
    private static final String VIEW_EXPAND_RIGHT = "3";
    private static final int RESULT_CODE_FILE_CHOOSER = 123;

    protected TagOnMediaAdapter adapterUserTag;
    private RelativeLayout viewContent;
    private FrameLayout viewContainer;
    private View customView;
    private View viewPayment;
    private View btnClosePayment;
    private View viewToolbar;
    private ImageView btnClose;
    private ImageView btnBack;
    private TextView tvTitle;
    private View btnOption;
    private ProgressBar progressLoading;
    private SwipyRefreshLayout swipyRefreshLayout;
    private WebView webView;
    private View viewExpandTop;
    private View viewExpandLeft;
    private View viewExpandRight;
    private ImageView btnCloseTopLeft;
    private ImageView btnCloseTopRight;
    private View viewBoxComment;
    private TagsCompletionView editComment;
    private View btnSendComment;
    private View btnOpenComment;
    private ImageView btnLikeContent;
    private View btnShareContent;
    private LinearLayout viewSponsor;
    private TextView tvSponsorTimeOut;
    private Button btnSponsor;
    private View viewTopAd, viewBottomAd;
    private View btnCloseTopAd, btnCloseBottomAd;
    private ImageView ivBottomAd;
    private MyWebChromeClient mWebChromeClient = null;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private ArrayList<ItemContextMenu> mOverFlowItems;
    private Resources mRes;
    private ApplicationController mApplication;
    private FeedModelOnMedia mFeed;
    private ReengAccount mAccount;
    private String parentUrl;
    private WSOnMedia rest;
    private ArrayList<UserInfo> userLikesInComment = new ArrayList<>();
    private FeedBusiness mFeedBusiness;
    private int feedType;
    private String currentUrl;
    private int gaCategoryId, gaActionId;
    private boolean isLikeUrl = false;
    private long totalLike;
    private String preUrlGetTitleLike = "";
    private String mAdsUrl = "";
    private String mAdsImageUrl = "";
    private boolean isShowingDialog = false;
    private boolean isWebViewFullscreen = false;
    private boolean isWebViewPayment = false;
    //sponsor
    private Timer mTimeoutTimer;
    private boolean requestSponsorComplete = false;
    private boolean isWebViewSponsor = false;
    private String sponsorId;
    private int sponsorTimeout;
    private int sponsorAmount;
    private boolean showConfirmBack;
    private boolean isClickableTitle = false;
    private boolean canUpdateTitle = false;
    private boolean isWebViewOnMedia = false;
    private String webTitle = "";
    private String closeState;
    private String expandState;
    private ValueCallback<Uri[]> mUploadMessage;
    private ShareBottomDialog dialogOption;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData(savedInstanceState);
        setContentView(R.layout.activity_webview_new);
        changeStatusBar(true);
        initViewComponent();
        mApplication = (ApplicationController) getApplication();
        mRes = getResources();
        mFeedBusiness = mApplication.getFeedBusiness();
        mAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
        rest = new WSOnMedia(mApplication);
        gaCategoryId = R.string.ga_category_onmedia;
        gaActionId = R.string.ga_onmedia_action_web;
        if (TextUtils.isEmpty(parentUrl)) {
            parentUrl = "http://mocha.com.vn";
        }
        if (feedType == Constants.ONMEDIA.FEED_CONTACT_DETAIL) {
            mFeed = mFeedBusiness.getFeedProfileProcess();
        } else if (feedType == Constants.ONMEDIA.FEED_PROFILE) {
            mFeed = mFeedBusiness.getFeedProfileFromUrl(parentUrl);
        } else {
            mFeed = mFeedBusiness.getFeedModelFromUrl(parentUrl);
        }
        if (mFeed != null && mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_TOTAL)) {
            parentUrl = mFeed.getFeedContent().getOpenLink();
        }
        currentUrl = parentUrl;
        initWebView();
        initListener();
        initView();
        if (checkNotShowDialogFeeData(parentUrl)) {
            loadUrl();
            logOpenWebview();
            getTitleLike(parentUrl);
            btnSendComment.setClickable(true);
        } else {
            showDialogFeeData();
        }
        trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_news);
    }

    private void initView() {
        if (isWebViewFullscreen) {
            //Nếu fullscreen = 1:
            //-	Ẩn toolbar, ẩn box comment
            //-	Kiểm tra “close” để hiển thị icon close cho màn hình fullscreen
            //-	Kiểm tra “expand” để hiển thị viền màu đen
            viewToolbar.setVisibility(View.GONE);
            viewBoxComment.setVisibility(View.GONE);
            swipyRefreshLayout.setCanTouch(false);
            viewSponsor.setVisibility(View.GONE);
            if (BUTTON_CLOSE_TOP_LEFT_FLOATING.equals(closeState)) {
                btnCloseTopLeft.setVisibility(View.VISIBLE);
                btnCloseTopRight.setVisibility(View.GONE);
                btnCloseTopLeft.setImageDrawable(getResources().getDrawable(R.drawable.webview_icon_close));
            } else if (BUTTON_CLOSE_TOP_RIGHT_FLOATING.equals(closeState)) {
                btnCloseTopLeft.setVisibility(View.GONE);
                btnCloseTopRight.setVisibility(View.VISIBLE);
                btnCloseTopRight.setImageDrawable(getResources().getDrawable(R.drawable.webview_icon_close));
            } else if (BUTTON_CLOSE_TOP_LEFT.equals(closeState)) {
                btnCloseTopLeft.setVisibility(View.VISIBLE);
                btnCloseTopRight.setVisibility(View.GONE);
                btnCloseTopLeft.setImageDrawable(getResources().getDrawable(R.drawable.webview_ic_close));
            } else if (BUTTON_CLOSE_TOP_RIGHT.equals(closeState)) {
                btnCloseTopLeft.setVisibility(View.GONE);
                btnCloseTopRight.setVisibility(View.VISIBLE);
                btnCloseTopRight.setImageDrawable(getResources().getDrawable(R.drawable.webview_ic_close));
            } else {
                btnCloseTopLeft.setVisibility(View.GONE);
                btnCloseTopRight.setVisibility(View.GONE);
            }
            if (isWebViewPayment) {
                viewPayment.setVisibility(View.VISIBLE);
            } else {
                viewPayment.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
            if (VIEW_EXPAND_TOP.equals(expandState)) {
                viewExpandTop.setVisibility(View.VISIBLE);
                viewExpandLeft.setVisibility(View.GONE);
                viewExpandRight.setVisibility(View.GONE);
            } else if (VIEW_EXPAND_LEFT.equals(expandState)) {
                viewExpandTop.setVisibility(View.GONE);
                viewExpandLeft.setVisibility(View.VISIBLE);
                viewExpandRight.setVisibility(View.GONE);
            } else if (VIEW_EXPAND_RIGHT.equals(expandState)) {
                viewExpandTop.setVisibility(View.GONE);
                viewExpandLeft.setVisibility(View.GONE);
                viewExpandRight.setVisibility(View.VISIBLE);
            } else {
                viewExpandTop.setVisibility(View.GONE);
                viewExpandLeft.setVisibility(View.GONE);
                viewExpandRight.setVisibility(View.GONE);
            }
            showLoadingDialog("", mRes.getString(R.string.loading), true);
        } else {
            //-	Nếu onmedia = 1:
            //+ Hiển thị toolbar có: icon back, tiêu đề, icon option
            //+ Hiển thị box comment
            //-	Ngược lại:
            //+ Hiển thị toolbar có: icon close, tiêu đề
            //+ Ẩn box comment
            viewToolbar.setVisibility(View.VISIBLE);
            swipyRefreshLayout.setCanTouch(true);

            viewPayment.setVisibility(View.GONE);
            btnCloseTopLeft.setVisibility(View.GONE);
            btnCloseTopRight.setVisibility(View.GONE);
            viewExpandTop.setVisibility(View.GONE);
            viewExpandLeft.setVisibility(View.GONE);
            viewExpandRight.setVisibility(View.GONE);

            if (isWebViewOnMedia) {
                canUpdateTitle = false;
                isClickableTitle = true;
                btnOption.setVisibility(View.VISIBLE);
                viewBoxComment.setVisibility(View.VISIBLE);
                tvTitle.setMaxLines(3);
            } else {
                isClickableTitle = false;
                btnOption.setVisibility(View.GONE);
                viewBoxComment.setVisibility(View.GONE);
                tvTitle.setMaxLines(1);
            }
            tvTitle.setSelected(true);
            if (isWebViewSponsor) {
                viewSponsor.setVisibility(View.VISIBLE);
                viewBoxComment.setVisibility(View.GONE);
                startTimerTimeout();
            } else {
                viewSponsor.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.ONMEDIA.EXTRAS_DATA, parentUrl);
        outState.putInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, feedType);
        outState.putBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_FULLSCREEN, isWebViewFullscreen);
        outState.putBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_SPONSOR, isWebViewSponsor);
        outState.putString(Constants.ONMEDIA.EXTRAS_SPONSOR_ID, sponsorId);
        outState.putInt(Constants.ONMEDIA.EXTRAS_SPONSOR_TIME_OUT, sponsorTimeout);
        outState.putInt(Constants.ONMEDIA.EXTRAS_SPONSOR_AMOUNT, sponsorAmount);
        super.onSaveInstanceState(outState);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void getData(Bundle savedInstanceState) {
        String orientation = ORIENTATION_AUTO;
        if (savedInstanceState != null) {
            parentUrl = savedInstanceState.getString(Constants.ONMEDIA.EXTRAS_DATA);
            feedType = savedInstanceState.getInt(Constants.ONMEDIA.EXTRAS_FEED_TYPE, 0);
            isWebViewFullscreen = savedInstanceState.getBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_FULLSCREEN, false);
            isWebViewPayment = savedInstanceState.getBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_PAYMENT, false);
            isWebViewSponsor = savedInstanceState.getBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_SPONSOR, false);
            sponsorId = savedInstanceState.getString(Constants.ONMEDIA.EXTRAS_SPONSOR_ID);
            sponsorTimeout = savedInstanceState.getInt(Constants.ONMEDIA.EXTRAS_SPONSOR_TIME_OUT, 1);
            sponsorAmount = savedInstanceState.getInt(Constants.ONMEDIA.EXTRAS_SPONSOR_AMOUNT, 0);
            showConfirmBack = savedInstanceState.getBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_CONFIRM_BACK, false);
            orientation = savedInstanceState.getString(Constants.ONMEDIA.EXTRAS_WEBVIEW_ORIENTATION, ORIENTATION_AUTO);
            isWebViewOnMedia = savedInstanceState.getBoolean(Constants.ONMEDIA.EXTRAS_WEBVIEW_ON_MEDIA, false);
            webTitle = savedInstanceState.getString(Constants.ONMEDIA.EXTRAS_WEBVIEW_TITLE, "");
            closeState = savedInstanceState.getString(Constants.ONMEDIA.EXTRAS_WEBVIEW_CLOSE, "");
            expandState = savedInstanceState.getString(Constants.ONMEDIA.EXTRAS_WEBVIEW_EXPAND, "");
        } else if (getIntent() != null) {
            parentUrl = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_DATA);
            feedType = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, 0);
            isWebViewFullscreen = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_FULLSCREEN, false);
            isWebViewPayment = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_PAYMENT, false);
            isWebViewSponsor = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_SPONSOR, false);
            sponsorId = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_SPONSOR_ID);
            sponsorTimeout = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_SPONSOR_TIME_OUT, 1);
            sponsorAmount = getIntent().getIntExtra(Constants.ONMEDIA.EXTRAS_SPONSOR_AMOUNT, 0);
            showConfirmBack = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_CONFIRM_BACK, false);
            orientation = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_ORIENTATION);
            isWebViewOnMedia = getIntent().getBooleanExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_ON_MEDIA, false);
            webTitle = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_TITLE);
            closeState = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_CLOSE);
            expandState = getIntent().getStringExtra(Constants.ONMEDIA.EXTRAS_WEBVIEW_EXPAND);
        }
        if (TextUtils.isEmpty(orientation)) orientation = ORIENTATION_AUTO;
        canUpdateTitle = TextUtils.isEmpty(webTitle);
        if (ORIENTATION_PORTRAIT.equals(orientation))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else if (ORIENTATION_LANDSCAPE.equals(orientation))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void showDialogFeeData() {
        new DialogConfirm(WebViewNewActivity.this, false).setLabel(mRes.getString(R.string.note_title)).
                setMessage(mRes.getString(R.string.notification_fee_data)).
                setNegativeLabel(mRes.getString(R.string.skip)).
                setPositiveLabel(mRes.getString(R.string.cont)).setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                finish();
            }
        }).setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                loadUrl();
                logOpenWebview();
                getTitleLike(parentUrl);
                btnSendComment.setClickable(true);
            }
        }).show();
        btnSendComment.setClickable(false);
        SharedPrefs sharedPrefs = SharedPrefs.getInstance();
        sharedPrefs.put(Constants.PREFERENCE.PREF_LAST_TIME_SHOW_DIALOG_WEB, System.currentTimeMillis());
        int count = sharedPrefs.get(Constants.PREFERENCE.PREF_COUNT_SHOW_DIALOG_WEB, Integer.class, 0);
        sharedPrefs.put(Constants.PREFERENCE.PREF_COUNT_SHOW_DIALOG_WEB, count + 1);
    }

    private boolean checkNotShowDialogFeeData(String url) {
        if (isFreeDataDomain(url)) {// domain free không show dialog
            return true;
        } else {
            int count = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_COUNT_SHOW_DIALOG_WEB, Integer.class, 0);
            if (mApplication.getReengAccountBusiness().isVip() &&
                    NetworkHelper.checkTypeConnection(this) == NetworkHelper.TYPE_MOBILE &&
                    count <= COUNT_SHOW_DIALOG) {
                long timeLastClick = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_LAST_TIME_SHOW_DIALOG_WEB, Long.class, 0L);
                return TimeHelper.checkTimeInDay(timeLastClick);
            } else {
                return true;
            }
        }
    }

    private boolean isFreeDataDomain(String url) {
        try {
            String domain = HttpHelper.getDomainName(url);
            Log.d(TAG, "isFreeDataDomain: " + domain);
            String domainFree = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_FREE_DATA);
            String[] listDomain = domainFree.split(";");
            for (String aListDomain : listDomain) {
                if (domain.equals(aListDomain)) return true;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return false;
    }

    private void getTitleLike(String url) {
        if (!isWebViewOnMedia || isWebViewFullscreen || isWebViewPayment) {
            return;
        }
        url = TextHelper.replaceUrl(url);
        if (!TextUtils.isEmpty(preUrlGetTitleLike) && url.equals(preUrlGetTitleLike)) {
            return;
        }
        preUrlGetTitleLike = url;
        rest.getLikeTitle(url, new OnMediaInterfaceListener.GetListFeedActionListener() {
            @Override
            public void onGetListFeedAction(RestAllFeedAction restAllFeedAction) {
                if (restAllFeedAction.getCode() == HTTPCode.E200_OK) {
                    if (restAllFeedAction.getIsBlackList() == 1) {
                        if (isShowingDialog) return;
                        new AlertDialog.Builder(new ContextThemeWrapper(WebViewNewActivity.this, R.style.DialogWebView))
                                .setTitle(mRes.getString(R.string.warning))
                                .setCancelable(false)
                                .setMessage(R.string.content_fishing_wap)
                                .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        isShowingDialog = false;
                                        finish();
//                                        handler.proceed();
                                    }
                                })
                                .show();
                        isShowingDialog = true;
                    } else {
                        isLikeUrl = (restAllFeedAction.getIsLike() != 0);
                        userLikesInComment = restAllFeedAction.getUserLikes();
                        if (isLikeUrl) {
                            btnLikeContent.setImageResource(R.drawable.ic_onmedia_like_press);
                        } else {
                            btnLikeContent.setImageResource(R.drawable.ic_onmedia_like);
                        }
                        if (restAllFeedAction.getAdsImageUrl() != null) {
                            Log.d(TAG, restAllFeedAction.getAdsImageUrl());
                            mAdsImageUrl = restAllFeedAction.getAdsImageUrl();
                        }
                        if (restAllFeedAction.getAdsUrl() != null) {
                            Log.d(TAG, restAllFeedAction.getAdsUrl());
                            mAdsUrl = restAllFeedAction.getAdsUrl();
                        }
                        totalLike = restAllFeedAction.getTotalLike();
                        //String title = OnMediaHelper.getTitleLikeInHtml(restAllFeedAction.getTotalLike(),
                        //        userLikesInComment, mApplication, null);
                        //tvTitle.setText(TextHelper.fromHtml(title));
                        drawAds();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i(TAG, "getTitleLike onErrorResponse: " + volleyError.toString());
            }
        });
    }

    private void drawAds() {
        if (!TextUtils.isEmpty(mAdsImageUrl)) {
            ImageLoaderManager.getInstance(mApplication.getApplicationContext())
                    .loadAdsImage(
                            viewBottomAd,
                            ivBottomAd,
                            webView,
                            mAdsImageUrl,
                            mAdsUrl);
        }
    }

    private void initOverFlowData() {
        mOverFlowItems = new ArrayList<>();
        mOverFlowItems.add(new ItemContextMenu(mRes.getString(R.string.share)
                , R.drawable.ic_bottom_list_share, mFeed, Constants.MENU.MENU_SHARE_LINK));
        mOverFlowItems.add(new ItemContextMenu(mRes.getString(R.string.dialog_copy_content)
                , R.drawable.ic_bottom_copy_link, mFeed, Constants.MENU.COPY));
        mOverFlowItems.add(new ItemContextMenu(mRes.getString(R.string.onmedia_open_url_by_browser)
                , R.drawable.ic_bottom_open_in_browser, mFeed, Constants.MENU.MENU_OPEN_IN_BROWSER));
    }

    private void initListener() {
        if (mFeed != null) {
            if (mFeed.getIsLike() == 1) {
                btnLikeContent.setImageResource(R.drawable.ic_onmedia_like_press);
            } else {
                btnLikeContent.setImageResource(R.drawable.ic_onmedia_like);
            }
        }
        if (DeviceHelper.isTablet(this)) {
            editComment.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
        } else {
            editComment.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        ArrayList<PhoneNumber> listPhone = mApplication.getContactBusiness().getListNumberUseMocha();
        if (listPhone == null) listPhone = new ArrayList<>();
        adapterUserTag = new TagOnMediaAdapter(mApplication, listPhone, editComment);
        editComment.setAdapter(adapterUserTag);
        editComment.setHint(R.string.onmedia_hint_enter_comment);
        editComment.setThreshold(0);
        adapterUserTag.setListener(new FilteredArrayAdapter.OnChangeItem() {
            @Override
            public void onChangeItem(int count) {
                Log.i(TAG, "onChangeItem: " + count);
                if (count > 2) {
                    int height = mRes.getDimensionPixelOffset(R.dimen.max_height_drop_down_tag);
                    editComment.setDropDownHeight(height);
                } else
                    editComment.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        });

        btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickClose();
            }
        });
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onBackPressed();
            }
        });
        btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickOption();
            }
        });
        if (TextUtils.isEmpty(webTitle)) {
            tvTitle.setText(TextHelper.getDomain(currentUrl));
        } else
            tvTitle.setText(webTitle);
        if (isClickableTitle) {
            tvTitle.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (isClickableTitle)
                        navigateToListUserLike();
                }
            });
        }
        btnClosePayment.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                showDialogConfirmBack();
            }
        });
        btnCloseBottomAd.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (viewBottomAd != null) viewBottomAd.setVisibility(View.GONE);
            }
        });
        btnCloseTopAd.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (viewTopAd != null) viewTopAd.setVisibility(View.GONE);
            }
        });
        swipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                loadUrl();
            }
        });
        editComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.i(TAG, "link: " + charSequence.toString());
                String text = charSequence.toString();
                if (TextUtils.isEmpty(text)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnSendComment.setVisibility(View.GONE);
                            btnOpenComment.setVisibility(View.VISIBLE);
                            btnShareContent.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnSendComment.setVisibility(View.VISIBLE);
                            btnOpenComment.setVisibility(View.GONE);
                            btnShareContent.setVisibility(View.GONE);
                            btnSendComment.setClickable(true);
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnSendComment.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else
                    sendComment();
            }
        });
        btnOpenComment.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else {
                    Log.e(TAG, "open comment");
                    String link = "mocha://onmedia?ref=" + HttpHelper.EncoderUrl(currentUrl);
                    DeepLinkHelper.getInstance().openSchemaLink(WebViewNewActivity.this, link);
                }
            }
        });
        btnLikeContent.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_like);
                if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else {
                    if (mFeed != null && currentUrl.equals(parentUrl)
                            && !mFeed.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_TOTAL)) {
                        onClickLikeFeed(mFeed);
                    } else {
                        onClickLike(isLikeUrl);
                    }
                }
            }
        });
        btnShareContent.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share);
                if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else {
                    ShareContentBusiness business = new ShareContentBusiness(WebViewNewActivity.this, currentUrl);
                    business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_CONTENT);
                    business.showPopupShareContent();
                }
            }
        });
        btnSponsor.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                handleSponsorComplete();
            }
        });
        btnCloseTopLeft.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickClose();
            }
        });
        btnCloseTopRight.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onClickClose();
            }
        });
    }

    private void onClickClose() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InputMethodUtils.hideSoftKeyboard(WebViewNewActivity.this);
            }
        });
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    public void onClickLikeFeed(final FeedModelOnMedia feed) {
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }
        final boolean isLiked = (feed.getIsLike() == 1);
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(feed, !isLiked);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), action, "", feed.getBase64RowId(), "", null,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionLike: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code != HTTPCode.E200_OK) {
                                    setLikeFeed(feed, isLiked);
                                    showToast(R.string.e601_error_but_undefined);
                                } else {
                                    //islike----unlike, !islike----like
                                    if (!isLiked) {
                                        userLikesInComment.add(0, new UserInfo(mAccount.getJidNumber(), mAccount
                                                .getName()));
                                        if (userLikesInComment.size() > 2) {
                                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                                            listTmp.add(0, userLikesInComment.get(0));
                                            listTmp.add(1, userLikesInComment.get(1));
                                            userLikesInComment = listTmp;
                                        }
                                    } else {
                                        if (userLikesInComment.size() == 0) {
                                            Log.i(TAG, "Loi roi, size phai khac 0");
                                        } else if (userLikesInComment.size() == 1) {
                                            userLikesInComment.clear();
                                        } else if (userLikesInComment.size() == 2) {
                                            int indexToDelete = -1;
                                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                                if (userLikesInComment.get(i).getMsisdn().equals(mAccount
                                                        .getJidNumber())) {
                                                    indexToDelete = i;
                                                    break;
                                                }
                                            }
                                            if (indexToDelete != -1) {
                                                userLikesInComment.remove(indexToDelete);
                                            }
                                        } else {
                                            Log.i(TAG, "Loi roi, size phai < 3");
                                        }
                                    }
//                                    drawTitleLike(feed.getFeedContent().getCountLike());
                                    //String title = OnMediaHelper.getTitleLikeInHtml(feed.getFeedContent()
                                    //        .getCountLike(), userLikesInComment, mApplication, null);
                                    //tvTitle.setText(TextHelper.fromHtml(title));
                                }
                            } else {
                                setLikeFeed(feed, isLiked);
                                showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                            setLikeFeed(feed, isLiked);
                            showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        setLikeFeed(feed, isLiked);
                        showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    public void onClickLike(final boolean isLiked) {
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }
        FeedModelOnMedia.ActionLogApp action;
        if (isLiked) {
            action = FeedModelOnMedia.ActionLogApp.UNLIKE;
        } else {
            action = FeedModelOnMedia.ActionLogApp.LIKE;
        }
        setLikeFeed(!isLiked);
        rest.logAppV6(currentUrl, "", null, action, "", "", "", null,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionLike: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code != HTTPCode.E200_OK) {
                                    setLikeFeed(isLiked);
                                    showToast(R.string.e601_error_but_undefined);
                                } else {
                                    //islike----unlike, !islike----like
                                    if (!isLiked) {
                                        userLikesInComment.add(0, new UserInfo(mAccount.getJidNumber(), mAccount
                                                .getName()));
                                        if (userLikesInComment.size() > 2) {
                                            ArrayList<UserInfo> listTmp = new ArrayList<>();
                                            listTmp.add(0, userLikesInComment.get(0));
                                            listTmp.add(1, userLikesInComment.get(1));
                                            userLikesInComment = listTmp;
                                        }
                                    } else {
                                        if (userLikesInComment.size() == 0) {
                                            Log.i(TAG, "Loi roi, size phai khac 0");
                                        } else if (userLikesInComment.size() == 1) {
                                            userLikesInComment.clear();
                                        } else if (userLikesInComment.size() == 2) {
                                            int indexToDelete = -1;
                                            for (int i = 0; i < userLikesInComment.size(); i++) {
                                                if (userLikesInComment.get(i).getMsisdn().equals(mAccount
                                                        .getJidNumber())) {
                                                    indexToDelete = i;
                                                    break;
                                                }
                                            }
                                            if (indexToDelete != -1) {
                                                userLikesInComment.remove(indexToDelete);
                                            }
                                        } else {
                                            Log.i(TAG, "Loi roi, size phai < 3");
                                        }
                                    }
//                                    drawTitleLike(totalLike);
                                    //String title = OnMediaHelper.getTitleLikeInHtml(totalLike,
                                    //        userLikesInComment, mApplication, null);
                                    //tvTitle.setText(TextHelper.fromHtml(title));
                                }
                            } else {
                                setLikeFeed(isLiked);
                                showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                            setLikeFeed(isLiked);
                            showToast(R.string.e601_error_but_undefined);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        setLikeFeed(isLiked);
                        showToast(R.string.e601_error_but_undefined);
                    }
                });
    }

    private void setLikeFeed(FeedModelOnMedia feed, boolean isLike) {
        //neu la like thi tang like them 1, neu la unlike thi tru 1.
        int delta;
        if (isLike) {
            delta = 1;
            feed.setIsLike(1);
            btnLikeContent.setImageResource(R.drawable.ic_onmedia_like_press);
        } else {
            delta = -1;
            feed.setIsLike(0);
            btnLikeContent.setImageResource(R.drawable.ic_onmedia_like);
        }
        long countLike = feed.getFeedContent().getCountLike();
        feed.getFeedContent().setCountLike(countLike + delta);
    }

    private void setLikeFeed(boolean isLike) {
        //neu la like thi tang like them 1, neu la unlike thi tru 1.
        int delta;
        if (isLike) {
            delta = 1;
            btnLikeContent.setImageResource(R.drawable.ic_onmedia_like_press);
            isLikeUrl = true;
        } else {
            delta = -1;
            isLikeUrl = false;
            btnLikeContent.setImageResource(R.drawable.ic_onmedia_like);
        }
        totalLike = totalLike + delta;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        webView.setWebViewClient(new MyWebViewClient());
        mWebChromeClient = new MyWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDisplayZoomControls(false);
        //Fix loi ko xem dc video google driver
        CookieManager.getInstance().setAcceptCookie(true);
        //In Lower than LOLLIPOP Third party cookies are enabled by default
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }
        webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {

            try {
                Log.d(TAG, "onDownloadStart url: " + url
                        + "\nuserAgent: " + userAgent
                        + "\ncontentDisposition: " + contentDisposition
                        + "\nmimetype: " + mimetype
                        + "\ncontentLength: " + contentLength
                );
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void initViewComponent() {
        viewContent = findViewById(R.id.layout_content);
        viewContainer = findViewById(R.id.layout_container);

        viewPayment = findViewById(R.id.layout_payment);
        btnClosePayment = findViewById(R.id.button_close_payment);

        viewToolbar = findViewById(R.id.layout_toolbar);
        btnClose = findViewById(R.id.button_close);
        btnBack = findViewById(R.id.button_back);
        tvTitle = findViewById(R.id.tv_title);
        btnOption = findViewById(R.id.button_option);

        progressLoading = findViewById(R.id.progress_loading);
        btnCloseTopLeft = findViewById(R.id.button_close_top_left);
        btnCloseTopRight = findViewById(R.id.button_close_top_right);
        viewExpandTop = findViewById(R.id.layout_expand_top);
        viewExpandLeft = findViewById(R.id.layout_expand_left);
        viewExpandRight = findViewById(R.id.layout_expand_right);
        swipyRefreshLayout = findViewById(R.id.refresh_layout);
        webView = findViewById(R.id.web_view);

        viewTopAd = findViewById(R.id.layout_top_ads);
        viewTopAd.setVisibility(View.GONE);
        btnCloseTopAd = viewTopAd.findViewById(R.id.ic_close);
        viewBottomAd = findViewById(R.id.layout_bottom_ads);
        viewBottomAd.setVisibility(View.GONE);
        btnCloseBottomAd = viewBottomAd.findViewById(R.id.ic_close);
        ivBottomAd = viewBottomAd.findViewById(R.id.img_ad);

        viewSponsor = findViewById(R.id.layout_sponsor);
        tvSponsorTimeOut = findViewById(R.id.tv_sponsor_timeout);
        btnSponsor = findViewById(R.id.button_sponsor);

        viewBoxComment = findViewById(R.id.layout_box_comment);
        editComment = findViewById(R.id.edit_comment);
        btnSendComment = findViewById(R.id.button_send_comment);
        btnOpenComment = findViewById(R.id.button_open_comment);
        btnLikeContent = findViewById(R.id.button_like_content);
        btnShareContent = findViewById(R.id.button_share_content);
    }

    private void logOpenWebview() {
        rest.logClickLink(parentUrl, Constants.ONMEDIA.LOG_CLICK.CLICK_WAP_VIEW, AppStateProvider.getInstance().getCampId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "logOpenWebview response: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
    }

    private void sendComment() {
        if (editComment.getText().toString().length() == 0) {
            return;
        }
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }
        /*if (TextHelper.countWords(editComment.getText().toString()) < Constants.ONMEDIA.MIN_WORDS_COMMENT) {
            showToast(R.string.msg_text_minimum);
            return;
        }*/
        Log.i(TAG, "send text: " + editComment.getText().toString());
        ArrayList<TagMocha> mListTagFake =
                FeedBusiness.getListTagFromListPhoneNumber(editComment.getUserInfo());
        String textTag = FeedBusiness.getTextTag(mListTagFake);

        String messageContent = TextHelper.trimTextOnMedia(editComment.getTextTag());
//        messageContent = TextHelper.getInstant().badwordFilter(messageContent);
        editComment.resetObject();
        Log.i(TAG, "text after getRaw: " + messageContent);
        addStatus();
        String rowId = "";
        FeedContent feedContent = null;
        if (mFeed != null && currentUrl.equals(parentUrl)) {
            rowId = mFeed.getBase64RowId();
            feedContent = mFeed.getFeedContent();
        }
        rest.logAppV6(currentUrl, "", feedContent, FeedModelOnMedia.ActionLogApp.COMMENT, messageContent,
                rowId, textTag, null,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "actionComment: onresponse: " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.has(Constants.HTTP.REST_CODE)) {
                                int code = jsonObject.getInt(Constants.HTTP.REST_CODE);
                                if (code == HTTPCode.E200_OK) {
                                    showToast(R.string.comment_success);
                                } else {
                                    showToast(R.string.e601_error_but_undefined);
                                }
                            } else {
                                showToast(R.string.e601_error_but_undefined);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                            showToast(R.string.e601_error_but_undefined);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i(TAG, "Response error" + volleyError.getMessage());
                        showToast(R.string.e601_error_but_undefined);
                    }
                });
        trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_comment);
    }

    private void addStatus() {
        if (currentUrl.equals(parentUrl) && mFeed != null) {
            long countLike = mFeed.getFeedContent().getCountComment();
            mFeed.getFeedContent().setCountComment(countLike + 1);
        }
    }

    @Override
    public void onBackPressed() {
        if (viewContainer != null && customView != null) {
            Log.i(TAG, "mCustomViewContainer !null");
            mWebChromeClient.onHideCustomView();
        } else if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            if (showConfirmBack) {
                showDialogConfirmBack();
            } else
                super.onBackPressed();
        }
    }

    private void showDialogConfirmBack() {
        DialogConfirm dialogConfirm = new DialogConfirm(this, true);
        dialogConfirm.setTitle(R.string.note_title);
        dialogConfirm.setMessage(mRes.getString(R.string.msg_exit_survey));
        dialogConfirm.setPositiveLabel(mRes.getString(R.string.ok));
        dialogConfirm.setNegativeLabel(mRes.getString(R.string.cancel));
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {

            @Override
            public void onPositive(Object result) {
                finish();
            }
        });
        dialogConfirm.show();
    }

    /**
     * Called when the fragment is visible to the user and actively running. Resumes the WebView.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    /**
     * Called when the fragment is no longer resumed. Pauses the WebView.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onResume() {
        webView.onResume();
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onStop() {
        mFeedBusiness.notifyNewFeed(true, false);
        super.onStop();
    }

    /**
     * Called when the fragment is no longer in use. Destroys the internal state of the WebView.
     */
    @Override
    public void onDestroy() {
        if (webView != null) {
            webView.destroy();
            webView = null;
        }
        stopTimerTimeout();
        super.onDestroy();
    }

    private void navigateToListUserLike() {
//        displayListLikeFragment(currentUrl);
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, currentUrl, true);
        trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_list_like);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case RESULT_CODE_FILE_CHOOSER:
                if (mUploadMessage == null || intent == null)
                    return;
                try {
                    Uri uri = intent.getData();
                    if (uri != null) {
                        Log.v(TAG, TAG + " # result.getPath()=" + uri.getPath());
                        mUploadMessage.onReceiveValue(new Uri[]{uri});
                    } else {
                        mUploadMessage.onReceiveValue(new Uri[]{});
                    }
                    mUploadMessage = null;
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                break;

            case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
            case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                if (resultCode == RESULT_OK && intent != null) {
                    int threadId = intent.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                }
                break;

            default:
                break;
        }
    }

    //////////////////////////// sponsor  ////////////////////////
    private void startTimerTimeout() {
        stopTimerTimeout();//stop truoc
        drawStateSponsor();
        if (sponsorTimeout > 0) {
            mTimeoutTimer = new Timer();
            TimerTask mTimeoutTimerTask = new TimerTask() {
                @Override
                public void run() {
                    sponsorTimeout--;
                    Log.d(TAG, "mTimeoutTimerTask run: " + sponsorTimeout);
                    if (sponsorTimeout <= 0) {
                        stopTimerTimeout();
                    }
                    drawStateSponsor();
                }
            };
            mTimeoutTimer.schedule(mTimeoutTimerTask, 1000, 1000);
        }
    }

    private void stopTimerTimeout() {
        if (mTimeoutTimer != null) {
            mTimeoutTimer.cancel();
            mTimeoutTimer = null;
        }
    }

    private void drawStateSponsor() {
        runOnUiThread(new TimerTask() {
            @Override
            public void run() {
                if (sponsorTimeout > 0) {
                    if (tvSponsorTimeOut != null) {
                        tvSponsorTimeOut.setVisibility(View.VISIBLE);
                        tvSponsorTimeOut.setText(String.format(mRes.getString(R.string.sponsor_timeout),
                                sponsorTimeout, sponsorAmount));
                    }
                    if (btnSponsor != null) btnSponsor.setVisibility(View.GONE);
                } else {
                    if (tvSponsorTimeOut != null) tvSponsorTimeOut.setVisibility(View.GONE);
                    if (btnSponsor != null) {
                        btnSponsor.setVisibility(View.VISIBLE);
                        btnSponsor.setEnabled(!requestSponsorComplete);
                        btnSponsor.setText(String.format(mRes.getString(R.string.sponsor_button), sponsorAmount));
                    }
                }
            }
        });
    }

    private void handleSponsorComplete() {
        Log.d(TAG, "handleSponsorComplete: ");
        showLoadingDialog(null, R.string.waiting);
        AccumulatePointHelper.getInstance(mApplication).requestReadSponsorSuccess(sponsorId, new
                AccumulatePointHelper.ReadSponsorSuccessListener() {
                    @Override
                    public void onSuccess(String desc) {
                        hideLoadingDialog();
                        showToast(desc, Toast.LENGTH_LONG);
                        requestSponsorComplete = true;
                        drawStateSponsor();
                    }

                    @Override
                    public void onError(int errorCode, String desc) {
                        hideLoadingDialog();
                        showToast(desc, Toast.LENGTH_LONG);
                    }
                });
    }

    private void onClickOption() {
        boolean enableBack = false;
        boolean enableForward = false;
        if (webView != null) {
            enableBack = webView.canGoBack();
            enableForward = webView.canGoForward();
        }
        if (dialogOption != null && dialogOption.isShowing()) {
            dialogOption.dismiss();
            dialogOption = null;
        }
        dialogOption = new ShareBottomDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_option_web_view, null, false);
        final View btnBack = sheetView.findViewById(R.id.button_back);
        View btnReload = sheetView.findViewById(R.id.button_reload);
        final View btnForward = sheetView.findViewById(R.id.button_forward);
        RecyclerView recyclerView = sheetView.findViewById(R.id.recycler_view);
        if (btnBack != null) {
            btnBack.setEnabled(enableBack);
            btnBack.setAlpha(enableBack ? 1f : 0.5f);

            btnBack.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (webView != null && webView.canGoBack()) {
                        webView.goBack();
                        if (btnBack != null) {
                            boolean enableBack = webView.canGoBack();
                            btnBack.setEnabled(enableBack);
                            btnBack.setAlpha(enableBack ? 1f : 0.5f);
                        }
                        if (btnForward != null) {
                            boolean enableForward = webView.canGoForward();
                            btnForward.setEnabled(enableForward);
                            btnForward.setAlpha(enableForward ? 1f : 0.5f);
                        }
                    }
                }
            });
        }
        if (btnReload != null) {
            btnReload.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (webView != null) {
                        webView.reload();
                        if (btnBack != null) {
                            boolean enableBack = webView.canGoBack();
                            btnBack.setEnabled(enableBack);
                            btnBack.setAlpha(enableBack ? 1f : 0.5f);
                        }
                        if (btnForward != null) {
                            boolean enableForward = webView.canGoForward();
                            btnForward.setEnabled(enableForward);
                            btnForward.setAlpha(enableForward ? 1f : 0.5f);
                        }
                    }
                }
            });
        }
        if (btnForward != null) {
            btnForward.setEnabled(enableForward);
            btnForward.setAlpha(enableForward ? 1f : 0.5f);
            btnForward.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (webView != null && webView.canGoForward()) {
                        webView.goForward();
                        if (btnBack != null) {
                            boolean enableBack = webView.canGoBack();
                            btnBack.setEnabled(enableBack);
                            btnBack.setAlpha(enableBack ? 1f : 0.5f);
                        }
                        if (btnForward != null) {
                            boolean enableForward = webView.canGoForward();
                            btnForward.setEnabled(enableForward);
                            btnForward.setAlpha(enableForward ? 1f : 0.5f);
                        }
                    }
                }
            });
        }
        if (mOverFlowItems == null) initOverFlowData();
        BottomSheetMenuAdapter adapter = new BottomSheetMenuAdapter(mOverFlowItems);
        adapter.setRecyclerClickListener(new RecyclerClickListener() {
            @Override
            public void onClick(View v, int pos, Object object) {
                if (object instanceof ItemContextMenu) {
                    ItemContextMenu item = (ItemContextMenu) object;
                    switch (item.getActionTag()) {
                        case Constants.MENU.MENU_SHARE_LINK:
                            trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_click_share);
                            if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                                showDialogLogin();
                            else {
                                ShareContentBusiness business = new ShareContentBusiness(WebViewNewActivity.this, currentUrl);
                                business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_CONTENT);
                                business.showPopupShareContent();
                            }
                            break;

                        case Constants.MENU.COPY:
                            TextHelper.copyToClipboard(WebViewNewActivity.this, currentUrl);
                            showToast(R.string.copy_to_clipboard);
                            trackingEvent(gaCategoryId, gaActionId, R.string.ga_onmedia_label_opt_copy);
                            break;

                        case Constants.MENU.MENU_OPEN_IN_BROWSER:
                            if (!TextUtils.isEmpty(currentUrl)) {
                                NavigateActivityHelper.navigateToActionView(WebViewNewActivity.this, Uri
                                        .parse(currentUrl));
                            }
                            break;
                        default:
                            break;
                    }
                }
                dialogOption.dismiss();
            }

            @Override
            public void onLongClick(View v, int pos, Object object) {

            }
        });
        BaseAdapter.setupVerticalRecycler(this, recyclerView, null, adapter, false);
        dialogOption.setContentView(sheetView);
        dialogOption.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

            }
        });
        dialogOption.show();
    }

    private class MyWebChromeClient extends WebChromeClient {

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            try {
                mUploadMessage = filePathCallback;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                startActivityForResult(Intent.createChooser(i, "Image Browser"), RESULT_CODE_FILE_CHOOSER, false);
                return true;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return false;
            }
        }

        @Override
        public void onProgressChanged(WebView webView, int newProgress) {
            super.onProgressChanged(webView, newProgress);
            if (newProgress < 20) {
                newProgress = 20;
            }
            if (newProgress >= 80) {
                if (!TextUtils.isEmpty(webView.getUrl())) {
                    currentUrl = Utilities.getOriginalUrl(TextHelper.replaceUrl(webView.getUrl()));
                }
                if (BuildConfig.DEBUG) Log.i(TAG, "onProgressChanged newProgress: " + newProgress
                        + "\ngetUrl: " + webView.getUrl() + "\ngetOriginalUrl: " + webView.getOriginalUrl()
                        + "\ncurrentUrl: " + currentUrl
                );
            }
            if (progressLoading != null) {
                progressLoading.setProgress(newProgress);
                progressLoading.setVisibility((newProgress >= 90) ? View.INVISIBLE : View.VISIBLE);
            }
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            // if a view already exists then immediately terminate the new one
            if (customView != null) {
                callback.onCustomViewHidden();
                return;
            }
            Log.i(TAG, "onShowCustomView");
            customView = view;
            if (viewContent != null) viewContent.setVisibility(View.GONE);
            if (viewContainer != null) {
                viewContainer.setVisibility(View.VISIBLE);
                viewContainer.addView(view);
            }
            customViewCallback = callback;
        }

        @Override
        public void onHideCustomView() {
            if (customView != null) {
                // Hide the custom view.
                customView.setVisibility(View.GONE);
                if (viewContent != null) viewContent.setVisibility(View.VISIBLE);
                if (viewContainer != null) {
                    viewContainer.setVisibility(View.GONE);
                    // Remove the custom view from its container.
                    viewContainer.removeView(customView);
                }
                if (customViewCallback != null) customViewCallback.onCustomViewHidden();
                customView = null;
            }
        }
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView webView, String url, Bitmap favicon) {
            super.onPageStarted(webView, url, favicon);
            Log.i(TAG, "onPageStarted: " + url);
            if (canUpdateTitle && tvTitle != null) tvTitle.setText(TextHelper.getDomain(url));
            if (btnBack != null)
                btnBack.setVisibility((webView != null && webView.canGoBack() && isWebViewOnMedia) ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onPageFinished(WebView webView, String url) {
            Log.i(TAG, "onPageFinished currentUrl: " + currentUrl + "\nactionUrl: " + parentUrl
                    + "\nurl: " + url);
            getTitleLike(currentUrl);
            if (swipyRefreshLayout.isRefreshing()) {
                swipyRefreshLayout.setRefreshing(false);
            }
            if (isWebViewFullscreen) {
                hideLoadingDialog();
            }
            if (canUpdateTitle && tvTitle != null) tvTitle.setText(TextHelper.getDomain(url));
            if (btnBack != null)
                btnBack.setVisibility((webView != null && webView.canGoBack() && isWebViewOnMedia) ? View.VISIBLE : View.GONE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            Log.i(TAG, "shouldOverrideUrlLoading currentUrl: " + currentUrl + "\nurl: " + url);
            if (Utilities.processOnShouldOverrideUrlLoading(WebViewNewActivity.this, webView, url)) {
                return true;
            } else return !URLUtil.isNetworkUrl(url);
        }

        @Override
        public void onReceivedSslError(WebView webView, final SslErrorHandler handler, SslError error) {
            if (isShowingDialog) return;
            new AlertDialog.Builder(new ContextThemeWrapper(WebViewNewActivity.this, R.style.DialogWebView))
                    .setTitle(mRes.getString(R.string.warning))
                    .setCancelable(false)
                    .setMessage(mRes.getString(R.string.notification_error_ssl_cert_invalid))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            isShowingDialog = false;
                            handler.proceed();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                            isShowingDialog = false;
                            handler.cancel();
                            onBackPressed();
                        }
                    })
                    .show();
            isShowingDialog = true;
        }
    }

    private void loadUrl() {
        String formatUrl = Utilities.formatUrl(currentUrl);
        Log.i(TAG, "currentUrl: " + currentUrl);
        Log.d(TAG, "WebView.loadUrl: " + formatUrl);
        if (webView != null) webView.loadUrl(formatUrl);
    }

}
package com.metfone.selfcare.activity.update_info.listener;

public interface EditButtonListener {
    void onEditData();
}

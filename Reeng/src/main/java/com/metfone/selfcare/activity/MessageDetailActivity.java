package com.metfone.selfcare.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.fragment.message.StatusMessageFragment;
import com.metfone.selfcare.helper.Constants;

public class MessageDetailActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = MessageDetailActivity.class.getSimpleName();
    private int threadId, messageId;
    private String abStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        setActionBar();
        getDataAndDisplayFragment(savedInstanceState);
        trackingScreen(TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setActionBar() {
        LayoutInflater mLayoutInflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        setToolBar(findViewById(R.id.tool_bar));
        setCustomViewToolBar(mLayoutInflater.inflate(
                R.layout.ab_message_detail, null));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ReengMessageConstant.MESSAGE_THREAD_ID, threadId);
        outState.putInt(ReengMessageConstant.MESSAGE_ID, messageId);
        outState.putString(Constants.MESSAGE.AB_DESC, abStatus);
        super.onSaveInstanceState(outState);
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            threadId = bundle.getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
            messageId = bundle.getInt(ReengMessageConstant.MESSAGE_ID);
            abStatus = bundle.getString(Constants.MESSAGE.AB_DESC);
        } else if (savedInstanceState != null) {
            threadId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_THREAD_ID);
            messageId = savedInstanceState.getInt(ReengMessageConstant.MESSAGE_ID);
            abStatus = savedInstanceState.getString(Constants.MESSAGE.AB_DESC);
        }
        displayStatusMessageFragment(threadId, messageId, abStatus);
    }

    private void displayStatusMessageFragment(int threadId, int messageId, String abStatus) {
        StatusMessageFragment statusMessageFragment = StatusMessageFragment.newInstance(threadId, messageId, abStatus);
        executeFragmentTransaction(statusMessageFragment, R.id.fragment_container, false, false);
    }
}
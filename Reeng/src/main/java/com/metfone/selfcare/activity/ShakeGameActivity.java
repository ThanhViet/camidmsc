package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.game.ShakeGameHelper;
import com.metfone.selfcare.ui.dialog.PopupLuckyWheel;
import com.metfone.selfcare.ui.game.ShakeDetector;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/29/2018.
 */

public class ShakeGameActivity extends BaseSlidingFragmentActivity implements ShakeDetector.OnShakeListener,
        View.OnClickListener,
        ShakeGameHelper.ShakeGameListener {
    private static final String TAG = ShakeGameActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private Animation shakeAnimation;

    private ImageView mAbBack, mImgShakeMain;
    private RoundLinearLayout mBtnShareFacebook;
    private TextView mTvwNote, mTvwResult;
    private RelativeLayout mViewMainContent;

    private long duration = 100;
    private Bitmap scaledBitmap;
    private boolean isSharing = false;
    private boolean isDelay = false, isDialog = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shake_game);
        mApplication = (ApplicationController) getApplicationContext();
        mRes = mApplication.getResources();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(this);
        if (mAccelerometer == null) {
            Log.e(TAG, "mAccelerometer==null");
        }
        shakeAnimation = AnimationUtils.loadAnimation(mApplication, R.anim.shake_main);
        ShakeGameHelper.getInstance(mApplication).setShakeListener(this);
        findViewComponents();
        setViewListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        isSharing = false;// reset state khi show dialog share
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mShakeDetector);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ShakeGameHelper.getInstance(mApplication).setShakeListener(null);
        if (shakeAnimation != null) shakeAnimation.cancel();
        if (scaledBitmap != null) scaledBitmap.recycle();
    }

    @Override
    public void onShake(int count) {
        Log.d(TAG, "onShake: " + count);
        onShake();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                onBackPressed();
                break;
            case R.id.shake_share_fb:
                handleShareFacebook();
                break;
        }
    }

    @Override
    public void onResult(int id, String msg) {
        isDialog = true;
        mTvwNote.setVisibility(View.GONE);
        mTvwResult.setVisibility(View.VISIBLE);
        mBtnShareFacebook.setVisibility(View.VISIBLE);
        mTvwResult.setText(msg);
        mImgShakeMain.clearAnimation();
        /*new DialogConfirm(ShakeGameActivity.this, false)
                .setLabel(null)
                .setMessage(msg)
                .setPositiveLabel(mRes.getString(R.string.non_ok))
                .setDismissListener(new DismissListener() {
                    @Override
                    public void onDismiss() {
                        Log.d(TAG, "DialogConfirm dismiss");
                        isDialog = false;
                    }
                }).show();*/

        new PopupLuckyWheel(ShakeGameActivity.this, true)
                // .setIconUrl(currentWheelSpin.getIconUrl())
                .setIconUrl(null)
                .setTitle(null)
                .setDesc(msg)
                .setPositive(mRes.getString(R.string.non_ok), new PopupLuckyWheel.PositiveListener() {
                    @Override
                    public void onPositive() {
                        Log.d(TAG, "DialogConfirm dismiss");
                        isDialog = false;
                    }
                }).show();
    }

    @Override
    public void onError(int code, String msg) {
        showToast(msg);
        mImgShakeMain.clearAnimation();
    }

    private void findViewComponents() {
        mViewMainContent = findViewById(R.id.shake_main_content);
        mAbBack = findViewById(R.id.ab_back_btn);
        mBtnShareFacebook = findViewById(R.id.shake_share_fb);
        mImgShakeMain = findViewById(R.id.shake_image);
        mTvwNote = findViewById(R.id.shake_text_note);
        mTvwResult = findViewById(R.id.shake_text_result);
        mTvwNote.setVisibility(View.VISIBLE);
        mTvwResult.setVisibility(View.GONE);
        mBtnShareFacebook.setVisibility(View.INVISIBLE);
    }

    private void setViewListener() {
        mAbBack.setOnClickListener(this);
        mBtnShareFacebook.setOnClickListener(this);
        shakeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (duration < 3000)
                    duration += 20;
                animation.setDuration(duration);
            }
        });
    }

    private void onShake() {
        if (isDialog || isDelay || ShakeGameHelper.getInstance(mApplication).isRequesting()) {
            Log.d(TAG, "onShake: isDelay: " + isDelay + " isDialog: " + isDialog
                    + " isRequesting: " + ShakeGameHelper.getInstance(mApplication).isRequesting());
        } else {
            mTvwNote.setVisibility(View.VISIBLE);
            mTvwResult.setVisibility(View.GONE);
            mBtnShareFacebook.setVisibility(View.INVISIBLE);
            duration = 100;
            shakeAnimation.setDuration(duration);
            mImgShakeMain.startAnimation(shakeAnimation);
            isDelay = true;
            ShakeGameHelper.getInstance(mApplication).setShakeListener(this);
            mImgShakeMain.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShakeGameHelper.getInstance(mApplication).requestShake();
                    isDelay = false;
                }
            }, 2000);
            //mImgShakeMain.removeCallbacks()
        }
    }

    private void handleShareFacebook() {
        Log.d(TAG, "share facebook: " + isSharing);
        if (mBtnShareFacebook.getVisibility() != View.VISIBLE || isSharing) return;
        if (scaledBitmap != null) scaledBitmap.recycle();
        isSharing = true;
        scaledBitmap = Bitmap.createBitmap(mViewMainContent.getWidth(), mViewMainContent.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(scaledBitmap);
        mViewMainContent.draw(canvas);
        shareImageFacebook(scaledBitmap, mRes.getString(R.string.ga_facebook_label_share_zodiac));
    }
}
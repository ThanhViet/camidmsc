package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.PhoneNumberAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.GiftLixiModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.message.BankPlusHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 1/23/2018.
 */

public class GiftLixiActivity extends BaseSlidingFragmentActivity implements TextWatcher {

    private static final String TAG = GiftLixiActivity.class.getSimpleName();
    private static final long MIN_LIXI_PER_MEMBER = 2000;
    private static final long MULTIPLY_LIXI = 1000;
    public static final String LIST_FRIEND_JID = "list_friend_jid";
    public static final String THREAD_ID = "thread_id";
    private ArrayList<String> listFriendJid = new ArrayList<>();
    private ArrayList<PhoneNumber> listFriendPhoneNumber = new ArrayList<>();
    private int threadId;
    private ThreadMessage mThreadMessage;
    private boolean splitRandom = false;
    private Resources mRes;


    private RecyclerView mRecyclerViewListMember;
    private EditText mEdtAmountMoney;
    private EditText mEdtMessage;
    private TextView mBtnSend;
    private View mViewHeader;

    private TextView mTvwAvatar;
    private CircleImageView mImgAvatar;
    private TextView mTvwSendGiftTo;
    private View mViewLixiGroup;
    private ImageView ivSplitEqual;
    private ImageView ivSplitRandom;
    private View vSplitEqual;
    private View vSplitRandom;
    private View mViewDivideReceiver;
    private TextView mTvwExplainSplit;
    private TextView tvTitleReceiver;

    private View rlAvatarGroup;

    private ApplicationController mApp;
    private ContactBusiness mContactBusiness;
    private PhoneNumberAdapter mPhoneNumberAdapter;
    private HeaderAndFooterRecyclerViewAdapter mWrapperAdapter;

//    private ListImageLixiAdapter mListImageLixiAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_lixi);
//        changeStatusBar(true);

        mApp = (ApplicationController) getApplication();
        mRes = mApp.getResources();
        mContactBusiness = mApp.getContactBusiness();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            listFriendJid = bundle.getStringArrayList(LIST_FRIEND_JID);
            threadId = bundle.getInt(THREAD_ID);
        }
        mThreadMessage = mApp.getMessageBusiness().getThreadById(threadId);
        if (listFriendJid != null && !listFriendJid.isEmpty() && mThreadMessage != null) {
            getListPhoneNumber();
        } else {
            showToast(R.string.e601_error_but_undefined);
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
        setActionBar();
        setViewComponent();
        setViewListener();

        drawViewDetail();
    }

    private void getListPhoneNumber() {
        for (String s : listFriendJid) {
            PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(s);
            if (phoneNumber != null) {
                listFriendPhoneNumber.add(phoneNumber);
            } else {
                NonContact nonContact = mContactBusiness.getExistNonContact(s);
                if (nonContact != null) {
                    PhoneNumber p = mContactBusiness.createPhoneNumberFromNonContact(nonContact);
                    p.setContactId("-1");
                    listFriendPhoneNumber.add(p);
                } else {
                    PhoneNumber phone = new PhoneNumber();
                    phone.setJidNumber(s);
                    phone.setName(s);
                    phone.setContactId("-1");
                    listFriendPhoneNumber.add(phone);
                }

            }
        }
    }

    private void drawViewDetail() {
        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            mViewLixiGroup.setVisibility(View.GONE);
        } else if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            mViewLixiGroup.setVisibility(View.VISIBLE);
        } else {
            showToast(R.string.e601_error_but_undefined);
            setResult(Activity.RESULT_CANCELED);
            finish();
        }

        drawAvatar();
        String textSendFor;

        if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            mViewDivideReceiver.setVisibility(View.GONE);
            mViewLixiGroup.setVisibility(View.GONE);
            textSendFor = String.format(mRes.getString(R.string.text_lixi_for_person), mThreadMessage.getThreadName());
            mPhoneNumberAdapter = new PhoneNumberAdapter(mApp, new ArrayList<PhoneNumber>(),
                    null, Constants.CONTACT.CONTACT_VIEW_AVATAR_AND_NAME);
            mEdtAmountMoney.setText(TextHelper.formatTextDecember("10000"));
        } else {

            mViewDivideReceiver.setVisibility(View.VISIBLE);
            mViewLixiGroup.setVisibility(View.VISIBLE);
            textSendFor = String.format(mRes.getString(R.string.text_lixi_for_group), listFriendJid.size(),
                    mThreadMessage.getThreadName());
            mPhoneNumberAdapter = new PhoneNumberAdapter(mApp, listFriendPhoneNumber,
                    null, Constants.CONTACT.CONTACT_VIEW_AVATAR_AND_NAME);

            setStateSelected(ivSplitRandom);
            setStateUnselected(ivSplitEqual);
            splitRandom = true;
            long money = listFriendJid.size() * 10000;
            mEdtAmountMoney.setText(TextHelper.formatTextDecember(String.valueOf(money)));
            mTvwExplainSplit.setText(mRes.getString(R.string.text_explain_split_money_random));
        }
        mEdtAmountMoney.setSelection(mEdtAmountMoney.getText().toString().length());
        mRecyclerViewListMember.setLayoutManager(new LinearLayoutManager(mApp));

        mPhoneNumberAdapter.setSmallItem(true);
        mWrapperAdapter = new HeaderAndFooterRecyclerViewAdapter(mPhoneNumberAdapter);
        mRecyclerViewListMember.setAdapter(mWrapperAdapter);
        mWrapperAdapter.addHeaderView(mViewHeader);
        mTvwSendGiftTo.setText(textSendFor);
        tvTitleReceiver.setText(String.format(getString(R.string.title_receiver_gift_lixi), listFriendJid.size()));
    }

    private void setViewComponent() {
        mRecyclerViewListMember = (RecyclerView) findViewById(R.id.recycler_view);
        LayoutInflater inflater = (LayoutInflater) mApp.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewHeader = inflater.inflate(R.layout.header_gift_lixi, null);


//        mRecyclerViewSticker = (RecyclerView) findViewById(R.id.recycler_view_list_image_gift);
        mEdtAmountMoney = (EditText) mViewHeader.findViewById(R.id.tvw_amount_money);
        mEdtMessage = (EditText) mViewHeader.findViewById(R.id.edt_msg_gift_lixi);
        mBtnSend = mViewHeader.findViewById(R.id.tvw_send_gift);


        mTvwAvatar = findViewById(R.id.tvw_avatar_text);
        mImgAvatar = findViewById(R.id.thread_avatar);
        rlAvatarGroup = findViewById(R.id.rlAvatarGroup);
        mTvwSendGiftTo = findViewById(R.id.tvw_send_gift_to);

        mViewLixiGroup = mViewHeader.findViewById(R.id.view_gift_lixi_group);
        ivSplitEqual = mViewHeader.findViewById(R.id.ivSplitEqual);
        ivSplitRandom = mViewHeader.findViewById(R.id.ivSplitRandom);
        vSplitEqual = mViewHeader.findViewById(R.id.llSplitEqual);
        vSplitRandom = mViewHeader.findViewById(R.id.llSplitRandom);
        tvTitleReceiver = mViewHeader.findViewById(R.id.tvListReceiver);

        mViewDivideReceiver = mViewHeader.findViewById(R.id.layout_divider_receiver);
        mTvwExplainSplit = (TextView) mViewHeader.findViewById(R.id.tvw_note_send_gift_group);
    }

    private void setViewListener() {
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickButtonSend();
            }
        });


        vSplitEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setStateSelected(ivSplitEqual);
                setStateUnselected(ivSplitRandom);
                splitRandom = false;
                setNoteSplitEqualMoney();
            }
        });

        vSplitRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setStateSelected(ivSplitRandom);
                setStateUnselected(ivSplitEqual);
                splitRandom = true;
                mTvwExplainSplit.setText(mRes.getString(R.string.text_explain_split_money_random));
            }
        });

        mEdtAmountMoney.addTextChangedListener(this);

        mRecyclerViewListMember.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        GiftLixiActivity.this.hideKeyboard();
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    private void setNoteSplitEqualMoney() {
        String amoutStr = mEdtAmountMoney.getText().toString().trim();
        long amount = TextHelper.convertTextDecemberToLong(amoutStr, 0);
        long splitAmount = amount / listFriendJid.size();
        String text = String.format(mRes.getString(R.string.text_explain_split_money_equal),
                TextHelper.formatTextDecember(String.valueOf(splitAmount)));
        mTvwExplainSplit.setText(text);
    }

    private void setStateSelected(ImageView btnSelected) {
        btnSelected.setImageResource(R.drawable.ic_lixi_selected);
    }

    private void setStateUnselected(ImageView btnUnSelected) {
        btnUnSelected.setImageResource(R.drawable.ic_lixi_unselect);
    }

    private void onClickButtonSend() {
        if (!NetworkHelper.isConnectInternet(this)) {
            showToast(R.string.no_connectivity_check_again);
            return;
        }

        String msg = mEdtMessage.getText().toString().trim();
        String amoutStr = mEdtAmountMoney.getText().toString().trim();
        long amount = TextHelper.convertTextDecemberToLong(amoutStr, 0);
        if (amount > 0 && !TextUtils.isEmpty(msg)) {
            if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && amount % MULTIPLY_LIXI != 0) {
                String toastMultiPly = String.format(mRes.getString(R.string.lixi_note_money_split),
                        TextHelper.formatTextDecember(String.valueOf(MULTIPLY_LIXI)));
                showToast(toastMultiPly);
            } else if (amount < MIN_LIXI_PER_MEMBER * listFriendJid.size()) {
                String contentToast = String.format(mRes.getString(R.string.lixi_note_money_receiver),
                        listFriendJid.size(),
                        TextHelper.formatTextDecember(String.valueOf(MIN_LIXI_PER_MEMBER * listFriendJid.size())));
                showToast(contentToast);
            } else {
                String orderid = BankPlusHelper.generateRequestIdLixi(mApp.getReengAccountBusiness().getJidNumber(),
                        System.currentTimeMillis());
                int splitRandomInt = -1;
                if (mThreadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    if (splitRandom) {
                        splitRandomInt = 1;
                    } else {
                        splitRandomInt = 0;
                    }
                }
                final GiftLixiModel giftLixiModel = new GiftLixiModel(msg, amount, orderid, listFriendJid,
                        splitRandomInt);

                BankPlusHelper.getInstance(mApp).sendRequestLixi(this, msg, amount, orderid, listFriendJid,
                        new BankPlusHelper.RequestSendLixiCallBack() {

                            @Override
                            public void onSuccess(String requestId) {
                                Intent intent = new Intent();
                                giftLixiModel.setRequestId(requestId);
                                intent.putExtra("data", giftLixiModel);
                                setResult(Activity.RESULT_OK, intent);
                                finish();
                            }

                            @Override
                            public void onError(int errorCode, String desc) {
                                if (errorCode != -1) {
                                    showToast(desc);
                                } else {
                                    showToast(R.string.e601_error_but_undefined);
                                }
                            }
                        });
            }
        } else {
            showToast(R.string.bplus_input_valid);
        }
    }

    private void setActionBar() {

        ImageView mImgBack = findViewById(R.id.ab_back_btn);
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void drawAvatar() {
        int threadType = mThreadMessage.getThreadType();
        int sizeAvatar = (int) mApp.getResources().getDimension(R.dimen.avatar_small_size);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String numberFriend = mThreadMessage.getSoloNumber();
            PhoneNumber phoneNumber = mApp.getContactBusiness().getPhoneNumberFromNumber(numberFriend);
            StrangerPhoneNumber stranger = mThreadMessage.getStrangerPhoneNumber();
            //avatar
            if (phoneNumber != null) {
                mApp.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, sizeAvatar);
            } else {
                if (mThreadMessage.isStranger()) {
                    if (stranger != null) {
                        mApp.getAvatarBusiness().setStrangerAvatar(mImgAvatar, mTvwAvatar,
                                stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                    } else {
                        mApp.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, numberFriend,
                                sizeAvatar);
                    }
                } else {
                    mApp.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, numberFriend, sizeAvatar);
                }
            }
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
            mApp.getAvatarBusiness().setGroupThreadAvatar(mImgAvatar, rlAvatarGroup, mThreadMessage);
            mTvwAvatar.setVisibility(View.GONE);        // Text
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (!splitRandom) {
            setNoteSplitEqualMoney();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        mEdtAmountMoney.removeTextChangedListener(this);
        try {
            String input = s.toString().trim();
            if (TextUtils.isEmpty(input)) {
                mEdtAmountMoney.addTextChangedListener(this);
                return;
            }
            int cp = mEdtAmountMoney.getSelectionStart();
            int inLen = input.length();
            String output = TextHelper.formatTextDecember(input);
            mEdtAmountMoney.setText(output);
            int outLen = mEdtAmountMoney.getText().length();
            int sel = (cp + (outLen - inLen));
            if (sel > 0 && sel <= outLen) {
                mEdtAmountMoney.setSelection(sel);
            } else {
                mEdtAmountMoney.setSelection(outLen - 1);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        mEdtAmountMoney.addTextChangedListener(this);
    }
}

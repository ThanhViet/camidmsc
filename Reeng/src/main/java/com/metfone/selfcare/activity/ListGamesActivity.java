package com.metfone.selfcare.activity;

import android.os.Bundle;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.game.AccumulateFragment;
import com.metfone.selfcare.fragment.game.GameAppFragment;
import com.metfone.selfcare.fragment.game.LuckyWheelGameFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

public class ListGamesActivity extends BaseSlidingFragmentActivity {

    private static final String TAG = ListGamesActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        trackingScreen(TAG);
        setActionBar();
        ApplicationController.self().logEventFacebookSDKAndFirebase(getString(R.string.c_trochoi));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int idGame = bundle.getInt(Constants.GAME.ID_GAME);
            Log.i(TAG, "idGame = " + idGame);
            if (idGame == Constants.GAME.LIST_GAME) {
                displayListGameApp();
            } else
                navigateToMochaGame(idGame, false);
        } else {
            displayListGameApp();
        }
    }

    private void navigateToMochaGame(int idGame, boolean isAddToBackStack) {
        if (idGame == Constants.GAME.VQMM_ID) {
            LuckyWheelGameFragment mLuckyWheelFragment = LuckyWheelGameFragment.newInstance();
            executeFragmentTransaction(mLuckyWheelFragment, R.id.fragment_container, isAddToBackStack, false);
        } else if (idGame == Constants.GAME.ACCUMULATE_ID) {
            AccumulateFragment mAccumulateFragment = AccumulateFragment.newInstance();
            executeFragmentTransaction(mAccumulateFragment, R.id.fragment_container, isAddToBackStack, false);
        }
    }

    private void displayListGameApp() {
        GameAppFragment mListGameFragment = GameAppFragment.newInstance();
        executeFragmentTransaction(mListGameFragment, R.id.fragment_container, false, false);
    }

    private void setActionBar() {
        setToolBar(findViewById(R.id.tool_bar));
    }

    public void navigateToAccumulateFragment() {
        AccumulateFragment mAccumulateFragment = AccumulateFragment.newInstance();
        executeFragmentTransaction(mAccumulateFragment, R.id.fragment_container, true, true);
    }
}

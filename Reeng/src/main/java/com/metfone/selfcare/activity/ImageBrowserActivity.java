package com.metfone.selfcare.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.FileObserver;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.adapter.image.ImageDirectoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.fragment.browser.DirectoryViewerFragment;
import com.metfone.selfcare.fragment.browser.ImageViewerFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.images.ImageDirectory;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.IImageBrowser.SelectImageDirectoryListener;
import com.metfone.selfcare.listeners.IImageBrowser.SelectImageListener;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.ui.dialog.BottomSheetListener;
import com.metfone.selfcare.ui.dialog.BottomSheetMenu;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Slide;
import androidx.transition.TransitionManager;
import co.mobiwise.materialintro.utils.Utils;

import static android.view.View.TEXT_ALIGNMENT_CENTER;

/**
 * Created by vtsoft on 11/5/2014.
 */
public class ImageBrowserActivity extends BaseSlidingFragmentActivity implements
        SelectImageDirectoryListener,
        SelectImageListener {
    public static final String ACTION_MULTIPLE_PICK = "action.MULTIPLE_PICK";
    public static final String ACTION_SIMPLE_PICK = "action.SIMPLE_PICK";

    public static final String PARAM_ACTION = "param_action";
    public static final String PARAM_PATH_ROOT = "path_root";
    public static final String PARAM_ACCEPT_TEXT = "accept_text";
    public static final String PARAM_PATH_SELECTED = "path_selected";
    public static final String PARAM_GEN_CACHE_FILE = "gen_cache_file"; // gen file fit to screen (resize and crop)
    public static final String PARAM_CROP_SIZE = "crop_size";
    public static final String PARAM_THREAD_ID = "thread_id";
    public static final String PARAM_SHOW_TAKE_GALLERY = "show_take_and_gallery";
    public static final String PARAM_SHOW_ALL_VIDEO = "show_all_video";
    public static final String PARAM_PICK_BG = "pick_background";
    public static final String PARAM_MAX_IMAGES = "max_images";
    public static final String PARAM_SHOW_ICON_CAMERA = "show_icon_camera";
    public static final String PARAM_LIST_IMAGE_SELECTED = "list_image_selected";

    private boolean uploadLimited = true;
    private int maxImagesNotifi = 1;
    //
    private static final String TAG = ImageBrowserActivity.class.getSimpleName();
    //
    private SharedPreferences mPref;
    private Resources mRes;
    private ImageViewerFragment mImageViewerFragment;
    private DirectoryViewerFragment mDirectoryViewerFragment;
    private ApplicationController mApplication;
    //
    private ArrayList<ImageDirectory> mImageDirectoryList;

    private ImageDirectory mCurrentImageDirectory = null;
    private LinkedHashMap<String, String> mImageSelected = new LinkedHashMap<>();
    private Map<String, ImageInfo> mImageSelectedContainObject = new HashMap<>();
    private List<ImageInfo> mImageSelectedObject = new ArrayList<>();

    private ArrayList<ItemContextMenu> mListItemMenu;
    //
    private AppCompatTextView tvTitle, tvDone, tvSwitchAlbum;
    private AppCompatImageView tvCancel;
    //    private ImageView mImgSelectCamera;
    private CheckBox mCheckBoxApplyAll;
    private LinearLayout mLayoutApplyAll;
//    private ImageView mImgSelectImage;

    private String pathBackgroundImage;
    //
    private String mAction;
    private boolean paramGenCacheFile = false;
    /**
     * paramPathRoot = "/", Duyệt tất cả các ảnh trong máy
     * paramPathRoot = "assets:<ten thu muc>" Duyet anh trong 1 thu muc của asset
     */
    private String paramPathRoot;
    /**
     * paramAcceptText: Text button submit select
     */
    private String paramAcceptText;
    /**
     * paramPathSelected: Path Image đã trọn trước đó: (chỉ với assets)
     */
    private String paramPathSelected;
    private String paramThreadId;
    /**
     * width, height khi anh trọn từ gallery hoặc chụp.
     * nếu không put 2 giá trị này sẽ không có crop anh
     */
    private int paramCropSize = 1;
    /**
     * show item take picture, gallery ở action bar browser
     */
    private int parShowTakeAndGallery = 1;
    private boolean paramShowVideo = false;
    // dialog waiting
    private ProgressDialog dialog;
    private long durationFileSelected = 0L;
    private long sizeFileSelected = 0L;
    private String fileResolution = "";
    private String image_source = "";
    private boolean isApplyAll = false;
    private boolean isPickBackGround = false;
    public int maxImages = 1;
    private boolean showIconCamera = false;
    private boolean isAllowPermissionStorage = true;

    private ArrayList<String> listSelected;

    private FrameLayout frameContainer;

    FileObserver observer = new FileObserver(Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.IMAGE_FOLDER) {

        @Override
        public void onEvent(int event, String fileName) {
            if ((event == FileObserver.CREATE
                    || event == FileObserver.DELETE
                    || event == FileObserver.MODIFY)
                    && !".probe".equals(fileName)) {
                String filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.IMAGE_FOLDER + File.separator
                        + fileName;
                Log.d(TAG, "File created [" + filePath + "]");
                // check and update
                for (ImageDirectory imageDirectory : mImageDirectoryList) {
                    String parentName = imageDirectory.getParentName();
                    if (parentName.equals(Config.Storage.IMAGE_FOLDER)) {
                        ArrayList<ImageInfo> listImage = imageDirectory.getListFile();
                        boolean has = false;
                        for (ImageInfo imageInfo : listImage) {
                            if (imageInfo.getImagePath().contains(fileName)) {
                                has = true;
                                break;
                            }
                        }
                        if (!has) { // add to ImageDirect
                            ImageInfo imageInfo = new ImageInfo(filePath);
                            imageDirectory.addListFile(imageInfo);
                        }
                    }
                }
            }
        }
    };

    ///
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, " onCreate ... ");
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
            }
        }
        setContentView(R.layout.activity_browser_image_v5);
//        changeStatusBar(true);
        trackingScreen(TAG);
        mApplication = (ApplicationController) getApplication();
        mRes = getResources();
        mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        initBottomMenu();
        setActionBar();
        initViewComponent();
        // action bar
        // get param
        getParam(savedInstanceState);
        // get List Image Directory
        if (paramPathRoot.startsWith("assets:")) {
            mImageDirectoryList = getImageDirectorOfAssets(paramPathRoot);
//            tvSwitchAlbum.setVisibility(View.GONE);
//            icArrowAlbum.setVisibility(View.GONE);
//            constraintAgainLayoutAppbar();
            mImageDirectoryList.addAll(getImageDirectorOfRoot());
            actionHandleAfterAcceptPermission();
        } else {
            if ((isAllowPermissionStorage = PermissionHelper.checkPermissionStorage(this))) {
                if (paramShowVideo) {
                    mImageDirectoryList = getVideoDirectorOfRoot();
                } else {
                    mImageDirectoryList = getImageDirectorOfRoot();
                }
                actionHandleAfterAcceptPermission();
            }
        }
        setVisibleIconCamera(showIconCamera);
        updateStatus();
        try {
            maxImagesNotifi = mApplication.getImageProfileBusiness().getMaxAlbumImages();
            uploadLimited = true;
        } catch (Exception e) {
            uploadLimited = false;
            maxImagesNotifi = 1;
            Log.e(TAG, "Exception", e);
        }
        if (getParShowTakeAndGallery() == 0) {
//            mImgSelectImage.setVisibility(View.GONE);
        } else {
//            mImgSelectImage.setVisibility(View.VISIBLE);
        }
    }

    private void actionHandleAfterAcceptPermission() {
        detectImageSelected();
        if (mImageDirectoryList == null || mImageDirectoryList.isEmpty()) return;
        if (mImageDirectoryList.size() == 1) {
            ImageDirectory imageDirectory = mImageDirectoryList.get(0);
            mCurrentImageDirectory = imageDirectory;
            if (paramPathRoot.startsWith("assets:")) {
                imageDirectory.setParentName(getString(R.string.background_app_source));
                image_source = Constants.BACKGROUND.FROM_SERVER;
            } else {
                imageDirectory.setParentName(getString(R.string.browser_title));
                image_source = Constants.BACKGROUND.FROM_DEVICE;
            }
            processSetListSelected();
            displayImageViewerFragment(imageDirectory, false);
        } else {
            mCurrentImageDirectory = mImageDirectoryList.get(0); // album all image
            displayImageViewerFragment(mCurrentImageDirectory, false);

//            displayDirectoryViewerFragment();
        }

    }

    private void initViewComponent() {
        tvDone.setEnabled(false);
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImageComplete();
            }
        });
        frameContainer = findViewById(R.id.fragment_container);
        mLayoutApplyAll = (LinearLayout) findViewById(R.id.layout_applly);
        mCheckBoxApplyAll = (CheckBox) findViewById(R.id.cb_apply_all);
        mCheckBoxApplyAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isApplyAll = b;
                mPref.edit().putBoolean(Constants.PREFERENCE.PREF_APPLY_BACKGROUND_ALL, isApplyAll).apply();
                Log.d(TAG, "" + isApplyAll);
            }
        });

        mLayoutApplyAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckBoxApplyAll.setChecked(!mCheckBoxApplyAll.isChecked());
            }
        });

//        mImgSelectCamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                processTakeAPhoto();
//            }
//        });
    }

    @Override
    public void onResume() {
        observer.startWatching();
        if (isPickBackGround) {
            mLayoutApplyAll.setVisibility(View.VISIBLE);
            isApplyAll = mPref.getBoolean(Constants.PREFERENCE.PREF_APPLY_BACKGROUND_ALL, false);
            mCheckBoxApplyAll.setChecked(isApplyAll);
            Log.d(TAG, "" + isApplyAll);
        } else {
            mLayoutApplyAll.setVisibility(View.GONE);
        }
        super.onResume();
        if (!isAllowPermissionStorage) {
            if (isAllowPermissionStorage = PermissionHelper.onlyCheckPermissionStorage(getApplicationContext())) {
                if (paramShowVideo) {
                    mImageDirectoryList = getVideoDirectorOfRoot();
                } else {
                    mImageDirectoryList = getImageDirectorOfRoot();
                }
                actionHandleAfterAcceptPermission();
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PARAM_ACTION, mAction);
        outState.putBoolean(PARAM_PICK_BG, isPickBackGround);
        outState.putString(PARAM_PATH_ROOT, paramPathRoot);
        outState.putString(PARAM_ACCEPT_TEXT, paramAcceptText);
        outState.putString(PARAM_THREAD_ID, paramThreadId);
        outState.putBoolean(PARAM_GEN_CACHE_FILE, paramGenCacheFile);
        outState.putString(PARAM_PATH_SELECTED, paramPathSelected);
        outState.putInt(PARAM_CROP_SIZE, paramCropSize);
        outState.putInt(PARAM_SHOW_TAKE_GALLERY, parShowTakeAndGallery);
        outState.putBoolean(PARAM_SHOW_ALL_VIDEO, paramShowVideo);
        outState.putInt(PARAM_MAX_IMAGES, maxImages);
        super.onSaveInstanceState(outState);
    }

    private void detectImageSelected() {
        if (paramPathSelected != null) {
            for (ImageDirectory imageDirectory : mImageDirectoryList) {
                ArrayList<ImageInfo> listFile = imageDirectory.getListFile();
                for (ImageInfo info : listFile) {
                    if (paramPathSelected.equals(info.getImagePath())) {
                        imageDirectory.setCountSelectedImage(1);
                        //
                        info.setSelected(true);
                        mImageSelected.put(info.getImagePath(), info.getImagePath());
                        mImageSelectedObject.add(info);
                        break;
                    }
                }
            }
        }

        if (mImageSelected != null && !mImageSelected.isEmpty() && listSelected != null && !listSelected.isEmpty()) {
            ArrayList<String> listParentSelected = new ArrayList<>();

            for (String s : listSelected) {
                int indexLast = s.lastIndexOf("/");
                String sub = s.substring(0, indexLast);
                if (!TextUtils.isEmpty(sub)) {
                    listParentSelected.add(sub);
                }
            }
            for (ImageDirectory imageDirectory : mImageDirectoryList) {
                String parentPath = imageDirectory.getParentPath();
                for (String path : listParentSelected) {
                    if (parentPath.equals(path)) {
                        imageDirectory.setCountSelectedImage(imageDirectory.getCountSelectedImage() + 1);
                        for (ImageInfo img : imageDirectory.getListFile()) {
                            if (mImageSelected.containsValue(img.getImagePath())) {
                                img.setSelected(true);
                            }
                        }
                    }
                }
            }
            int currentIndexSelect = 1;
            if(listSelected != null) {
                for (String path : listSelected) {
                    mImageSelectedContainObject.get(path).setIndexSelect(currentIndexSelect);
                    currentIndexSelect++;
                }
            }
        }
    }

    private void getParam(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mAction = savedInstanceState.getString(PARAM_ACTION);
            paramPathRoot = savedInstanceState.getString(PARAM_PATH_ROOT);
            paramAcceptText = savedInstanceState.getString(PARAM_ACCEPT_TEXT);
            isPickBackGround = savedInstanceState.getBoolean(PARAM_PICK_BG, false);
            paramThreadId = savedInstanceState.getString(PARAM_THREAD_ID);
            paramGenCacheFile = savedInstanceState.getBoolean(PARAM_GEN_CACHE_FILE, false);
            paramPathSelected = savedInstanceState.getString(PARAM_PATH_SELECTED);
            paramCropSize = savedInstanceState.getInt(PARAM_CROP_SIZE, 1);
            parShowTakeAndGallery = savedInstanceState.getInt(PARAM_SHOW_TAKE_GALLERY, 1);
            paramShowVideo = savedInstanceState.getBoolean(PARAM_SHOW_ALL_VIDEO, false);
            maxImages = savedInstanceState.getInt(PARAM_MAX_IMAGES, 1);
            showIconCamera = savedInstanceState.getBoolean(PARAM_SHOW_ICON_CAMERA);
            listSelected = savedInstanceState.getStringArrayList(PARAM_LIST_IMAGE_SELECTED);
        } else {
            Intent intent = getIntent();
            mAction = intent.getStringExtra(PARAM_ACTION);
            paramPathRoot = intent.getStringExtra(PARAM_PATH_ROOT);
            paramAcceptText = intent.getStringExtra(PARAM_ACCEPT_TEXT);
            isPickBackGround = intent.getBooleanExtra(PARAM_PICK_BG, false);
            paramThreadId = intent.getStringExtra(PARAM_THREAD_ID);
            paramGenCacheFile = intent.getBooleanExtra(PARAM_GEN_CACHE_FILE, false);
            paramPathSelected = intent.getStringExtra(PARAM_PATH_SELECTED);
            paramCropSize = intent.getIntExtra(PARAM_CROP_SIZE, 1);
            parShowTakeAndGallery = intent.getIntExtra(PARAM_SHOW_TAKE_GALLERY, 1);
            paramShowVideo = intent.getBooleanExtra(PARAM_SHOW_ALL_VIDEO, false);
            maxImages = intent.getIntExtra(PARAM_MAX_IMAGES, 1);
            showIconCamera = intent.getBooleanExtra(PARAM_SHOW_ICON_CAMERA, false);
            listSelected = intent.getStringArrayListExtra(PARAM_LIST_IMAGE_SELECTED);
        }
        if (mAction == null) {
            mAction = ACTION_MULTIPLE_PICK;
        }
        if (paramPathRoot == null) {
            paramPathRoot = "/";
        }
        if (paramAcceptText == null) {
            paramAcceptText = mApplication.getResources().getString(R.string.send);
        }
        if (paramThreadId == null) {
            paramThreadId = String.valueOf(System.currentTimeMillis());
        }
        tvDone.setText(paramAcceptText);
        if (listSelected != null && !listSelected.isEmpty()) {
            for (String s : listSelected) {
                mImageSelected.put(s, s);
            }
        }
    }

    private void processSetListSelected() {
        if (!mImageSelected.isEmpty()) {
            ArrayList<ImageInfo> listImg = getListImage();
            for (ImageInfo imgInfo : listImg) {
                if (mImageSelected.containsValue(imgInfo.getImagePath())) {
                    imgInfo.setSelected(true);
                }
            }
        }
    }

    private void setActionBar() {
        tvTitle = findViewById(R.id.tvTitle);
        tvCancel = findViewById(R.id.tvCancel);
        tvDone = findViewById(R.id.tvDone);
        tvSwitchAlbum = findViewById(R.id.tvSwitchAlbum);
        icArrowAlbum = findViewById(R.id.imgDownSwitchAlbum);

//        LayoutInflater mLayoutInflater = (LayoutInflater) getApplicationContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        // action bar
//        View abView = findViewById(R.id.tool_bar);
//        setToolBar(abView);
//        setCustomViewToolBar(mLayoutInflater.inflate(R.layout.ab_browser_title, null));
//
//        tvTitle =  abView.findViewById(R.id.ab_title);
//        tvTitle.setText(getString(R.string.browser_title));
//
//        tvDone = abView.findViewById(R.id.tvw_ab_done);
//        mImgSelectCamera = abView.findViewById(R.id.img_ab_select_camera);
//        mImgSelectImage = abView.findViewById(R.id.img_ab_select_image);
//
//        View mViewBack = abView.findViewById(R.id.ab_back_btn);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvSwitchAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowAlbum();
            }
        });
//
//        mImgSelectImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onClickSelectImage();
//            }
//        });
    }

    private RecyclerView recyclerViewAlbum;
    private ConstraintLayout layoutListAlbum;
    private ImageDirectoryAdapter adapterAlbum;
    private AppCompatImageView icArrowAlbum;

    private void toggleShowAlbum() {
        if (layoutListAlbum == null) {
            layoutListAlbum = (ConstraintLayout) getLayoutInflater().inflate(R.layout.layout_list_album_v5, null, false);
            recyclerViewAlbum = layoutListAlbum.findViewById(R.id.recyclerViewAlbum);
            recyclerViewAlbum.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
            adapterAlbum = new ImageDirectoryAdapter(this, this);
            recyclerViewAlbum.setAdapter(adapterAlbum);
            Fade fade = new Fade();
            fade.addTarget(layoutListAlbum);
            fade.setMode(Slide.MODE_IN);
            fade.setInterpolator(new DecelerateInterpolator());
            fade.setDuration(getResources().getInteger(R.integer.anim_duration_medium));
            TransitionManager.beginDelayedTransition(frameContainer, fade);
            frameContainer.addView(layoutListAlbum);
            animationRotateDownIcSwitchAlbum();
            layoutListAlbum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleShowAlbum();
                }
            });
        } else {
            if (layoutListAlbum.getVisibility() == View.VISIBLE) {
                Fade fade = new Fade();
                fade.addTarget(layoutListAlbum);
                fade.setMode(Slide.MODE_OUT);
                fade.setInterpolator(new DecelerateInterpolator());
                fade.setDuration(getResources().getInteger(R.integer.anim_duration_medium));
                TransitionManager.beginDelayedTransition(frameContainer, fade);
                layoutListAlbum.setVisibility(View.GONE);
                animationRotateUpIcSwitchAlbum();
            } else {
                Fade fade = new Fade();
                fade.addTarget(layoutListAlbum);
                fade.setMode(Slide.MODE_IN);
                fade.setInterpolator(new DecelerateInterpolator());
                fade.setDuration(getResources().getInteger(R.integer.anim_duration_medium));
                TransitionManager.beginDelayedTransition(frameContainer, fade);
                layoutListAlbum.setVisibility(View.VISIBLE);
                animationRotateDownIcSwitchAlbum();
            }
        }
    }

    private void animationRotateUpIcSwitchAlbum() {
        RotateAnimation rotateAnimation = new RotateAnimation(90, 0,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        rotateAnimation.setDuration(getResources().getInteger(R.integer.anim_duration_medium));
        rotateAnimation.setFillAfter(true);
        icArrowAlbum.setAnimation(rotateAnimation);
        icArrowAlbum.startAnimation(rotateAnimation);
    }

    private void animationRotateDownIcSwitchAlbum() {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 90,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setInterpolator(new DecelerateInterpolator());
        rotateAnimation.setDuration(getResources().getInteger(R.integer.anim_duration_medium));
        rotateAnimation.setFillAfter(true);
        icArrowAlbum.setAnimation(rotateAnimation);
        icArrowAlbum.startAnimation(rotateAnimation);
    }

    private void onClickSelectImage() {
        new BottomSheetMenu(this, true)
                .setListItem(mListItemMenu)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onItemClick(int itemId, Object entry) {
                        switch (itemId) {
                            case Constants.MENU.CAPTURE_IMAGE:
                                processTakeAPhoto();
                                break;
                            case Constants.MENU.SELECT_GALLERY:
                                openGallery();
                                break;
                        }
                    }
                }).show();
    }

    private void initBottomMenu() {
        mListItemMenu = new ArrayList<>();
        ItemContextMenu openCamera = new ItemContextMenu(mRes.getString(R.string.camera),
                R.drawable.ic_bottom_camera, null, Constants.MENU.CAPTURE_IMAGE);
        ItemContextMenu openGallery = new ItemContextMenu(mRes.getString(R.string.select_from_gallery),
                R.drawable.ic_bottom_image, null, Constants.MENU.SELECT_GALLERY);
        mListItemMenu.add(openCamera);
        mListItemMenu.add(openGallery);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mPref == null) {
            mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        }
        switch (requestCode) {
            case Constants.ACTION.ACTION_PICK_PICTURE:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                    if (picturePath != null && picturePath.size() > 0) {
                        pathBackgroundImage = picturePath.get(0);
                        // show progress bar
                        // get image, crop, gen to cache file
                        if (paramCropSize == 1) {
                            cropBackgroundImage(pathBackgroundImage);
                        } else {
                            SelectImageComplete(pathBackgroundImage);
                        }
                    } else {
                        // thong bao
                    }
                }
                setTakePhotoAndCrop(false);
                setActivityForResult(false);
                break;
            case Constants.ACTION.ACTION_TAKE_PHOTO:
                try {
                    String pathImage = mPref.getString(Constants.PREFERENCE.PREF_IMAGE_FILE_CAPTURE, "");
                    if (resultCode == RESULT_OK) {
                        FileHelper.refreshGallery(mApplication, pathImage);
                        if (showIconCamera) {
                            SelectImageComplete(pathImage);
                        } else {
                            if (paramCropSize == 1) {
                                //cropAvatarImage(takeImageUri);
                                android.util.Log.e(TAG, "123123: ");
                                cropBackgroundImage(pathImage);
                            } else {
                                SelectImageComplete(pathImage);
                                android.util.Log.e(TAG, "456456: ");
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception", e);
                    showToast(getResources().getString(R.string.file_not_found_exception), Toast.LENGTH_LONG);
                }
                setTakePhotoAndCrop(false);
                setActivityForResult(false);
                break;
            case Constants.ACTION.ACTION_CROP_IMAGE: {
                if (data != null) {
                    String fileCrop = mPref.getString(Constants.PREFERENCE.PREF_IMAGE_FILE_CROP, "");
                    genCacheFile(fileCrop);
                }
                break;
            }
        }
        setActivityForResult(false);
        setTakePhotoAndCrop(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO &&
                PermissionHelper.verifyPermissions(grantResults)) {
            takePhoto();
        }
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_FILE) {
            if (PermissionHelper.verifyPermissions(grantResults)) {
                if (paramShowVideo) {
                    mImageDirectoryList = getVideoDirectorOfRoot();
                } else {
                    mImageDirectoryList = getImageDirectorOfRoot();
                }

                actionHandleAfterAcceptPermission();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void processTakeAPhoto() {
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(ImageBrowserActivity.this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            takePhoto();
        }
    }

    private void takePhoto() {
        try {
            image_source = Constants.BACKGROUND.FROM_DEVICE;
            String time = String.valueOf(System.currentTimeMillis());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File takePhotoFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                    Config.Storage.PROFILE_PATH, "tmp" + time + Constants.FILE.JPEG_FILE_SUFFIX);
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, FileHelper.fromFile(mApplication, takePhotoFile));
            intent.putExtra("return-data", true);
            mPref.edit().putString(Constants.PREFERENCE.PREF_IMAGE_FILE_CAPTURE, takePhotoFile.toString()).apply();
            setActivityForResult(true);
            setTakePhotoAndCrop(true);
            startActivityForResult(intent, Constants.ACTION.ACTION_TAKE_PHOTO);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.permission_activity_notfound);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showToast(R.string.prepare_photo_fail);
        }
    }

    public void openGallery() {
        Log.d(TAG, "openGallery");
        image_source = Constants.BACKGROUND.FROM_DEVICE;
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(PARAM_ACTION, ACTION_SIMPLE_PICK);
        //Intent i = new Intent(ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        if (isPickBackGround) {
            i.putExtra(ImageBrowserActivity.PARAM_PICK_BG, true);
        }
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    /**
     * CROP IMAGE
     *
     * @param inputFilePath
     */
    private void cropBackgroundImage(String inputFilePath) {
        // crop file
        try {
            File cropImageFile = new File(ImageHelper.getInstance(ImageBrowserActivity.this).
                    getBackgroundImagePath(paramThreadId));
            mPref.edit().putString(Constants.PREFERENCE.PREF_IMAGE_FILE_CROP, cropImageFile.toString()).apply();
            // intent crop
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, inputFilePath);
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.RETURN_DATA, false);
            intent.putExtra(CropView.PHOTO_FULL_SCREEN, true);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_IMAGE);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    private void SelectImageComplete() {
        if (paramGenCacheFile) {
            Log.d(TAG, "SelectImageComplete paramGenCacheFile true");
            // gen cache file
            Collection<String> collection = mImageSelected.values();
            if (collection.size() > 0) {
                for (String path : collection) {
                    if (path != null && !"".equals(path)) {
                        genCacheFile(path);
                        break;
                    }
                }
            } else {
                finishBrower();
            }
        } else {
            Log.d(TAG, "SelectImageComplete paramGenCacheFile false");
            finishBrower();
        }
    }

    private void SelectImageComplete(String pathImage) {
        ArrayList<String> selected = new ArrayList<>();
        selected.add(pathImage);
        Intent data = new Intent().putStringArrayListExtra("data", selected);
        data.putExtra(Constants.BACKGROUND.KEY_CHAT_SCREEN, image_source);
        data.putExtra(Constants.BACKGROUND.KEY_IS_APPPLY_ALL, mCheckBoxApplyAll.isChecked());
        setResult(RESULT_OK, data);
        finish();
    }

    private void genCacheFile(final String filePath) {
        dialog = new ProgressDialog(this);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage(getString(R.string.msg_waiting));
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();
        GlideImageLoader.ImageLoadingListener imageLoadingListener = new GlideImageLoader.ImageLoadingListener() {
            @Override
            public void onLoadingStarted() {

            }

            @Override
            public void onLoadingFailed(String imageUri, GlideException e) {
                if (dialog != null && !ImageBrowserActivity.this.isFinishing()) {
                    dialog.dismiss();
                    showToast(getResources().getString(R.string.error_cache_background), Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onLoadingComplete(Bitmap loadedImage) {
                if (dialog != null && !ImageBrowserActivity.this.isFinishing()) {
                    dialog.dismiss();
                    SelectImageComplete(filePath);
                }
            }
        };
        ImageLoaderManager.getInstance(ImageBrowserActivity.this).
                loadCacheBackgroundOfThreadDetail(filePath, imageLoadingListener);
    }

    private void finishBrower() {
        ArrayList<String> selected = new ArrayList<>();
        selected.addAll(mImageSelected.values());
        Intent data = new Intent().putStringArrayListExtra("data", selected);
        if (paramShowVideo) {
            Log.i(TAG, "sizeFileSelected: " + sizeFileSelected);
            if (sizeFileSelected > Constants.FILE.VIDEO_MAX_SIZE) {
                showToast(String.format(getString(R.string.video_size_limit),
                        (int) FileHelper.getSizeInMbFromByte(Constants.FILE.VIDEO_MAX_SIZE)), Toast.LENGTH_SHORT);
                return;
            }
            data.putExtra("duration", durationFileSelected);
            data.putExtra("resolution", fileResolution);
        }

        data.putExtra(Constants.BACKGROUND.KEY_CHAT_SCREEN, image_source);
        Log.d(TAG, "finishBrower image_source:" + image_source);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onSelectImageDirectory(ImageDirectory directory, int position) {
        if (directory != null && !directory.equals(mCurrentImageDirectory)) {
            mCurrentImageDirectory = directory;
            displayImageViewerFragment(directory, false);
        }
        toggleShowAlbum();
    }

    @Override
    public void onSelectImage(ImageDirectory directory, ImageInfo image, int position) {
        if (image != null) {
            if (mAction.equals(ACTION_SIMPLE_PICK)) {
                if (!image.isSelected()) {
                    // update all image to unSelect
                    updateAllImageToUnSelect();
                    image.setSelected(true);
                    image.setIndexSelect(1);
                    // clear array seleted
                    mImageSelected.clear();
                    mImageSelected.put(image.getImagePath(), image.getImagePath());
                    mImageSelectedObject.add(image);
                    if (paramShowVideo) {
                        durationFileSelected = image.getDurationInSecond();
                        fileResolution = image.getResolution();
                        sizeFileSelected = image.getFileSize();
                    }
                    directory.setCountSelectedImage(1);
                } else {
                    // voi simple pick khong xu truong hop click vao item da select
                }
            } else {
                int count;
                count = mImageSelected.size();
                if (!image.isSelected() && uploadLimited && count >= maxImages && maxImages >= 1) {
                    showToast(String.format(getString(R.string.upload_image_maximum), maxImages), Toast.LENGTH_SHORT);
                    return;
                }

                // change state Image
                image.setSelected(!image.isSelected());
                if (image.isSelected()) {
                    image.setIndexSelect(++count);
                    mImageSelected.put(image.getImagePath(), image.getImagePath());
                    mImageSelectedObject.add(image);
                    directory.setCountSelectedImage(directory.getCountSelectedImage() + 1);
                } else {
                    int indexRemove = image.getIndexSelect();
                    for (ImageInfo imageInfo : mImageSelectedObject) {
                        if (imageInfo.getIndexSelect() > indexRemove) {
                            imageInfo.setIndexSelect(imageInfo.getIndexSelect() - 1);
                        }
                    }
                    image.setIndexSelect(-1);
                    mImageSelected.remove(image.getImagePath());
                    mImageSelectedObject.remove(image);
                    directory.setCountSelectedImage(directory.getCountSelectedImage() - 1);
                }
            }
            updateStatus();
        }
    }

    private void updateAllImageToUnSelect() {
        for (ImageDirectory imageDirectory : mImageDirectoryList) {
            ArrayList<ImageInfo> listFile = imageDirectory.getListFile();
            imageDirectory.setCountSelectedImage(0);
            for (ImageInfo info : listFile) {
                info.setSelected(false);
            }
        }
    }

    private void displayDirectoryViewerFragment() {
        mDirectoryViewerFragment = new DirectoryViewerFragment();
        tvTitle.setText(getString(R.string.browser_title));
        image_source = Constants.BACKGROUND.FROM_DEVICE;
        executeFragmentTransaction(mDirectoryViewerFragment, R.id.fragment_container, false, false);
    }

    private void updateStatus() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int count = mImageSelected.size();
                if (mAction.equals(ACTION_SIMPLE_PICK)) {
                    if (count > 0) {
                        tvDone.setEnabled(true);
                    }
                } else {
                    if (count > 0) {
                        tvDone.setEnabled(true);
                        tvDone.setText(paramAcceptText + "  (" + count + ")");
                        setVisibleIconCamera(false);
                    } else {
                        tvDone.setEnabled(false);
                        tvDone.setText(paramAcceptText);
                        setVisibleIconCamera(true);
                    }
                }
            }
        });
    }

    private void setVisibleIconCamera(boolean visible) {
        if (visible) {
            tvDone.setVisibility(View.GONE);
//            mImgSelectCamera.setVisibility(View.VISIBLE);
        } else {
            tvDone.setVisibility(View.VISIBLE);
//            mImgSelectCamera.setVisibility(View.GONE);
        }
    }

    private void displayImageViewerFragment(ImageDirectory directory, boolean addBackTask) {
        mImageViewerFragment = new ImageViewerFragment();
        tvTitle.setText(directory.getParentName());
        mImageViewerFragment.setSelectImageListener(this);
        executeFragmentTransaction(mImageViewerFragment, R.id.fragment_container, addBackTask, true);
    }

    public ArrayList<ImageDirectory> getImageDirectoryList() {
        return mImageDirectoryList;
    }

    /**
     * dainv
     * TODO: rewrite this function to asynchronous function
     *
     * @return list ImageDirectory, Trong moi ImageDirectory co list path Image
     */

    private synchronized ArrayList<ImageDirectory> getImageDirectorOfRoot() {
        ArrayList<ImageDirectory> directoryList = new ArrayList<>();
        HashMap<String, ImageDirectory> hashMap = new HashMap<>();
        Cursor imageCursor = null;
        try {
            final String[] columns = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,};
            final String orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC";
            imageCursor = getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
                    null, null, orderBy);
            if (imageCursor != null && imageCursor.getCount() > 0) {
                // add album All Image
                ImageDirectory albumAllImage = new ImageDirectory();
                albumAllImage.setParentName(getString(R.string.all_image));
                albumAllImage.setParentPath(ImageDirectory.PATH_ALL_IMAGE);
//                hashMap.put(ImageDirectory.PATH_ALL_IMAGE, albumAllImage);
                directoryList.add(0, albumAllImage);

                ImageDirectory dir;
                while (imageCursor.moveToNext()) {
                    int dataColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    //  get Image path
                    String filePath = imageCursor.getString(dataColumnIndex);
                    // get Parent directory path,  name
                    String parentPath = getParentPath(filePath);
                    String parentName = getParentName(filePath);
                    if (parentPath != null) {
                        dir = hashMap.get(parentPath);
                        if (dir == null) {
                            dir = new ImageDirectory();
                            dir.setParentPath(parentPath);
                            dir.setParentName(parentName);
                            hashMap.put(parentPath, dir);
                        }
                        ImageInfo imageInfo = new ImageInfo(filePath);
                        if(listSelected != null && listSelected.contains(imageInfo.getImagePath())){
                            mImageSelectedContainObject.put(imageInfo.getImagePath(), imageInfo);
                        }
                        dir.addListFile(imageInfo);
                        directoryList.get(0).addListFile(imageInfo); // add image to album all image
                    }
                }
                directoryList.addAll(hashMap.values());


            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (imageCursor != null) {
                imageCursor.close();
            }
        }
        //ko can sort theo thu tu nua
//        Collections.sort(directoryList, new ImageDirectoryComparator());
        return directoryList;
    }

    private ArrayList<ImageDirectory> getVideoDirectorOfRoot() {
        ArrayList<ImageDirectory> directoryList = new ArrayList<>();
        HashMap<String, ImageDirectory> hashMap = new HashMap<>();
        Cursor imageCursor = null;
        try {
            String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE,
                    MediaStore.Video.Media.DURATION, MediaStore.Video.Media._ID, MediaStore.Video.Media.RESOLUTION};
            String orderBy = MediaStore.Video.Media.DATE_ADDED + " DESC";

            imageCursor = getContentResolver().query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns,
                    null, null, orderBy);
            if (imageCursor != null && imageCursor.getCount() > 0) {
                ImageDirectory dir;
                while (imageCursor.moveToNext()) {
                    int id = imageCursor.getInt(imageCursor
                            .getColumnIndex(MediaStore.MediaColumns._ID));
                    int dataColumnIndex = imageCursor.getColumnIndex(MediaStore.Video.Media.DATA);
                    long SIZE = imageCursor.getLong(imageCursor.getColumnIndex(MediaStore.Video.Media.SIZE));
                    long DURATION = imageCursor.getLong(imageCursor.getColumnIndex(MediaStore.Video.Media.DURATION));
                    int durationInSecond = Math.round(DURATION / 1000.0f);
                    //  get Image path
                    String filePath = imageCursor.getString(dataColumnIndex);
                    String videoContentURI = "content://media/external/video/media/" + id;
                    // get Parent directory path,  name
                    String parentPath = getParentPath(filePath);
                    String parentName = getParentName(filePath);
                    if (parentPath != null) {
                        dir = hashMap.get(parentPath);
                        if (dir == null) {
                            dir = new ImageDirectory();
                            dir.setParentPath(parentPath);
                            dir.setParentName(parentName);
                            hashMap.put(parentPath, dir);
                        }
                        ImageInfo imageInfo = new ImageInfo(filePath, videoContentURI);
/*                        int duration = imagecursor.getInt(durationIndex);
                        float size = imagecursor.getInt(sizeIndex);*/
                        String res = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Video.Media
                                .RESOLUTION));
                        Log.i(TAG, "duration: " + durationInSecond + " size: " + SIZE + " filepath: " + filePath + " " +
                                "video uri: " + videoContentURI);
                        Log.i(TAG, "RESOLUTION: " + res);
                        imageInfo.setResolution(res);
                        imageInfo.setDurationInSecond(durationInSecond);
                        imageInfo.setFileSize(SIZE);
                        dir.addListFile(imageInfo);
                    }
                }
                directoryList.addAll(hashMap.values());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            if (imageCursor != null) {
                imageCursor.close();
            }
        }
        // show newest photo at beginning of the list
        // sap xep  theo thoi gian - lam sau
        // Collections.reverse(directoryList);
        return directoryList;
    }

    private ArrayList<ImageDirectory> getImageDirectorOfAssets(String pathAssets) {
        ArrayList<ImageDirectory> directoryList = new ArrayList<>();
        try {
            String subPath = pathAssets.replaceAll("assets:", "");
            String[] listPathFile = getAssets().list(subPath);
            if (listPathFile != null) {
                ImageDirectory imageDirectory = new ImageDirectory();
                imageDirectory.setParentName(getString(R.string.background_app_source));
                imageDirectory.setFromAsset(true);
                for (String pFile : listPathFile) {
                    ImageInfo imageInfo = new ImageInfo(pathAssets + File.separator + pFile);
                    imageDirectory.addListFile(imageInfo);
                }
                directoryList.add(imageDirectory);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
        return directoryList;
    }

    private String getParentPath(String filePath) {
        if (filePath == null)
            return "Other";

        File file = new File(filePath);
        if (file.exists()) {
            return file.getParent();
        } else {
            // reget with error file
            //String temp = filePath.substring(0, filePath.lastIndexOf("/"));
            return null;
        }
    }

    private String getParentName(String filePath) {
        if (filePath == null)
            return "Other";
        File file = new File(filePath);
        if (file.exists()) {
            return file.getParentFile().getName();
        } else {
            String temp = filePath.substring(0, filePath.lastIndexOf("/"));
            if (temp.indexOf("/") > 0) {
                temp = temp.substring(filePath.lastIndexOf("/"));
                return temp;
            }
        }
        return "Other";
    }

    public ImageDirectory getCurrentImageDirectory() {
        return mCurrentImageDirectory;
    }

    public ArrayList<ImageInfo> getListImage() {
        if (mCurrentImageDirectory != null) {
            return mCurrentImageDirectory.getListFile();
        }
        return new ArrayList<>();
    }

    public String getAction() {
        return mAction;
    }

    public String getParamPathRoot() {
        return paramPathRoot;
    }

    public int getParShowTakeAndGallery() {
        return parShowTakeAndGallery;
    }

    public boolean getParamShowVideo() {
        return paramShowVideo;
    }

    public int getMaxImages() {
        return maxImages;
    }

    public int getCountSelected() {
        if (mImageSelected == null) {
            return 0;
        }
        return mImageSelected.values().size();
    }

    private void constraintAgainLayoutAppbar() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(((ConstraintLayout) tvCancel.getParent()));
        constraintSet.connect(tvTitle.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
        constraintSet.connect(tvCancel.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
        constraintSet.connect(tvCancel.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
        constraintSet.connect(tvDone.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
        constraintSet.setMargin(tvTitle.getId(), ConstraintSet.TOP, 0);
        tvCancel.setPadding(Utils.dpToPx(15), Utils.dpToPx(10), Utils.dpToPx(15), Utils.dpToPx(10));
        tvDone.setPadding(Utils.dpToPx(15), Utils.dpToPx(10), Utils.dpToPx(15), Utils.dpToPx(10));
        tvCancel.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        tvDone.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        constraintSet.applyTo((ConstraintLayout) tvCancel.getParent());
    }
}
package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.hoanganhtuan95ptit.upload.UploadLayout;
import com.hoanganhtuan95ptit.upload.UploadProgress;
import com.hoanganhtuan95ptit.upload.UploadSingleton;
import com.metfone.selfcare.adapter.feedback.FeedbackImageAdapter;
import com.metfone.selfcare.adapter.feedback.FeedbackItemDecoration;
import com.metfone.selfcare.adapter.feedback.ImageSelectedFeedbackViewHolder;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackProgressV2;
import com.metfone.selfcare.common.api.FileApi;
import com.metfone.selfcare.common.api.FileApiImpl;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.BaseAdapterV2;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import io.reactivex.functions.Consumer;

public class FeedbackActivity extends BaseActivity {

    private static final String TAG = "FeedbackActivity";
    private static final int MAX_PHOTOS = 5;

    @BindView(R.id.icBackToolbar)
    AppCompatImageView ivBack;
    //    @BindView(R.id.ll_header)
//    LinearLayout llHeader;
    @BindView(R.id.edtInputFeedback)
    TextInputEditText etContent;
    @BindView(R.id.recyclerViewImageFeedback)
    RecyclerView recPhoto;
    @BindView(R.id.tvSend)
    AppCompatTextView tvSend;
    @BindView(R.id.tvSendImageFeedback)
    AppCompatTextView tvSendImageFeedback;

//    @BindView(R.id.btn_send_feedback)
//    RoundTextView btnSendFeedback;

    public static void start(BaseSlidingFragmentActivity activity) {
        Intent starter = new Intent(activity, FeedbackActivity.class);
        activity.startActivity(starter);
    }

    private FileApi mFileApi;

    private Unbinder unbinder;

    private ArrayList<Object> feeds;
    private ArrayList<String> photos;
    private Map<String, String> uploadPhotos;

    //    private PhotoAdapter mPhotoAdapter;
    private FeedbackImageAdapter imageAdapter;

    private OnPhotoListener onPhotoListener;

    private @Nullable
    FinishRunnable mFinishRunnable;
    private @Nullable
    UploadRunnable mUploadRunnable;
    private @Nullable
    PositiveListener mPositiveListener;
//    private @Nullable
//    DialogConfirm mDialogConfirm;
//    private @Nullable
//    DialogConfirm mDialogConfirmNotContent;

    private boolean isSend = false;

    private int currentpositionSelect;

    private int countUploadImageSuccess;
    private int countImageUpload;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
            }
        }

        setContentView(R.layout.activity_feedback_v5);
        unbinder = ButterKnife.bind(this);
        mFileApi = new FileApiImpl();
        initView();
    }

    @Override
    protected void onPause() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(etContent.getWindowToken(), 0);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
//        if (mDialogConfirm != null && mDialogConfirm.isShowing()) {
//            mDialogConfirm.dismiss();
//        }
        if (mPositiveListener != null) {
            mPositiveListener.setFeedbackActivity(null);
        }
        if (mUploadRunnable != null) {
            mUploadRunnable.setFeedbackActivity(null);
            recPhoto.removeCallbacks(mUploadRunnable);
        }
        if (mFinishRunnable != null) {
            mFinishRunnable.setFeedbackActivity(null);
            recPhoto.removeCallbacks(mFinishRunnable);
        }
        if (onPhotoListener != null) {
            onPhotoListener.setFeedbackActivity(null);
        }
//        if (mPhotoAdapter != null) {
//            mPhotoAdapter.setOnPhotoListener(null);
//        }
        if (imageAdapter != null) {
            imageAdapter.removeObservable();
            imageAdapter = null;
        }
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @OnClick(R.id.icBackToolbar)
    public void onIvBackClicked() {
        finish();
    }

    @Optional
    @OnClick(R.id.tvSend)
    public void onBtnSendFeedbackClicked() {
        if (etContent.getText() == null || etContent.getText().toString().trim().length() <= 0) {
//            mDialogConfirmNotContent = new DialogConfirm(this, false);
//            mDialogConfirmNotContent.setLabel(getString(R.string.notification));
//            mDialogConfirmNotContent.setMessage(getString(R.string.feedback_title_notify_not_content));
//            mDialogConfirmNotContent.setPositiveLabel(getString(R.string.close));
//            mDialogConfirmNotContent.show();
//            com.metfone.selfcare.v5.dialog.DialogConfirm
            DialogConfirm dialogConfirm = new DialogConfirm.Builder(DialogConfirm.INTRO_TYPE)
                    .setTitle(getString(R.string.notification))
                    .setMessage(getString(R.string.feedback_title_notify_not_content))
                    .setTitleRightButton(R.string.close)
                    .create();
            dialogConfirm.show(getSupportFragmentManager(), "");
        } else {
            // start upload all image
            isSend = true;
            if(photos != null && photos.size() > 0) {
                showLoadingDialog("", R.string.loading);
                countImageUpload = photos.size();
                for (String path : photos) {
                    uploadPhoto(this, path, -1);
                }
            }else {
                startFeedbackUpload();
            }
//            isSend = true;
//            recPhoto.removeCallbacks(mUploadRunnable);
//            recPhoto.postDelayed(mUploadRunnable, 300);
        }
    }

    private void startFeedbackUpload() {
        recPhoto.removeCallbacks(mUploadRunnable);
        recPhoto.postDelayed(mUploadRunnable, 300);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case Constants.ACTION.ACTION_PICK_PICTURE:
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (Utilities.notEmpty(picturePath)) {
//                            String pathImage = picturePath.get(0);
//                            uploadPhoto(this, pathImage, currentpositionSelect);
                            photos.removeAll(picturePath);
                            photos.addAll(picturePath);
                            tvSendImageFeedback.setText(getString(R.string.feedback_send_us_your_screenshots) + " " + (photos.size()) + "/5");
                            feeds.clear();
                            feeds.addAll(photos);
                            for (int i = feeds.size(); i < MAX_PHOTOS; i++) {
                                feeds.add(i);
                            }
                            feeds.add(PhotoAdapter.EMPTY);
//                            mPhotoAdapter.bindData(feeds);
//                            List list = new ArrayList();
//                            list.add(pathImage);
                            imageAdapter.addItems(picturePath);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void initView() {
        tvSend.setPaintFlags(tvSend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvSend.setText(R.string.send);
        feeds = new ArrayList<>();
        photos = new ArrayList<>();
        uploadPhotos = new HashMap<>();
        onPhotoListener = new OnPhotoListener();
        onPhotoListener.setFeedbackActivity(this);

//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

//        mPhotoAdapter = new PhotoAdapter(this);
//        mPhotoAdapter.setOnPhotoListener(onPhotoListener);

        mFinishRunnable = new FinishRunnable();
        mFinishRunnable.setFeedbackActivity(this);

        mUploadRunnable = new UploadRunnable();
        mUploadRunnable.setFeedbackActivity(this);

        mPositiveListener = new PositiveListener();
        mPositiveListener.setFeedbackActivity(this);

        Context context;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recPhoto.setLayoutManager(gridLayoutManager);
        recPhoto.addItemDecoration(new FeedbackItemDecoration(10, false, 3));
        imageAdapter = new FeedbackImageAdapter(this);
        recPhoto.setNestedScrollingEnabled(false);
        recPhoto.setAdapter(imageAdapter);

        for (int i = feeds.size(); i < MAX_PHOTOS; i++) {
            feeds.add(i);
        }
        feeds.add(PhotoAdapter.EMPTY);
//        mPhotoAdapter.bindData(feeds);
        imageAdapter.getAddImageObservable().subscribe(new Consumer() {
            @Override
            public void accept(Object o) throws Exception {
                currentpositionSelect = (int) o;
                onGetPhoto();
            }
        });
        imageAdapter.getRemoveObservable().subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                onDeletePhoto(s);
            }
        });
        tvSendImageFeedback.setText(getString(R.string.feedback_send_us_your_screenshots) + " " + (photos.size()) + "/5");
    }

    private void feedback() {
        if (Utilities.notEmpty(photos)) {
            boolean isUpload = false;
            for (String photo : photos) {
                UploadProgress uploadProgress = UploadSingleton.getInstance().get(photo);
                if (uploadProgress != null && uploadProgress.getState() == UploadProgress.State.PROGRESS) {
                    isUpload = true;
                    break;
                }
            }
            if (!isUpload) {
                feedback(etContent.getText().toString(), photos);
            }
        } else {
            feedback(etContent.getText().toString(), new ArrayList<String>());
        }
    }

    private void feedback(String content, ArrayList<String> photos) {
        isSend = false;
        hideLoadingDialog();
        DialogConfirm dialogConfirm = new DialogConfirm.Builder(DialogConfirm.INTRO_TYPE)
                .setTitle(getString(R.string.feedback_title_notify))
                .setMessage(getString(R.string.feedback_title_notify_send_feedback_success))
                .setTitleRightButton(R.string.ok)
                .create();
        dialogConfirm.setSelectListener(new BaseDialogFragment.DialogListener() {
            @Override
            public void dialogRightClick(int value) {
                finish();
            }

            @Override
            public void dialogLeftClick() {

            }
        });
        dialogConfirm.show(getSupportFragmentManager(), "");
//        mDialogConfirm = new DialogConfirm(this, false);
//        mDialogConfirm.setLabel(getString(R.string.feedback_title_notify));
//        mDialogConfirm.setMessage(getString(R.string.feedback_title_notify_send_feedback_success));
//        mDialogConfirm.setPositiveLabel(getString(R.string.close));
//        mDialogConfirm.setPositiveListener(mPositiveListener);
//        mDialogConfirm.show();

        recPhoto.removeCallbacks(mFinishRunnable);
        recPhoto.postDelayed(mFinishRunnable, 15 * 1000);

        ArrayList<String> images = new ArrayList<>();
        if (Utilities.notEmpty(photos)) {
            for (String photo : photos) {
                images.add(uploadPhotos.get(photo));
            }
        }
        mFileApi.feedback(content, images);
    }

    private void onGetPhoto() {
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, MAX_PHOTOS - photos.size());
        i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    private void onDeletePhoto(String pathImage) {
        photos.remove(pathImage);
        tvSendImageFeedback.setText(getString(R.string.feedback_send_us_your_screenshots) + " " + (photos.size()) + "/5");
        feeds.clear();
        feeds.addAll(photos);
        for (int i = feeds.size(); i < MAX_PHOTOS; i++) {
            feeds.add(i);
        }
        feeds.add(PhotoAdapter.EMPTY);
//        mPhotoAdapter.bindData(feeds);
    }

    private static void uploadPhoto(FeedbackActivity activity, final String pathImage, final int positionAdapter) {
        final WeakReference<FeedbackActivity> weakReference = new WeakReference<>(activity);

//        final UploadProgress uploadProgress = new UploadProgress();
        UploadSingleton.getInstance().add(pathImage, null);
        FeedbackActivity feedbackActivity = weakReference.get();
        if (feedbackActivity == null) return;

        feedbackActivity.mFileApi.upload(pathImage, new ApiCallbackProgressV2<String>() {
            @Override
            public void onSuccess(String lastId, String s) throws JSONException {
                JSONObject jsonObject = new JSONObject(s);
//                uploadProgress.setState(UploadProgress.State.SUCCESS);
                FeedbackActivity feedbackActivity = weakReference.get();
                if (feedbackActivity == null) return;
                if (feedbackActivity.photos.contains(pathImage)) {

//                    feedbackActivity.updateProgress(100, positionAdapter);
                    feedbackActivity.uploadPhotos.put(pathImage, jsonObject.optString("code"));
                }
            }

            @Override
            public void onProgress(float progress) {
//                uploadProgress.setProgress((int) progress);
//                FeedbackActivity feedbackActivity = weakReference.get();
//                if (feedbackActivity != null) {
//                    if (feedbackActivity.photos.contains(pathImage)) {
//                        feedbackActivity.updateProgress((int) progress, positionAdapter);
//                    }
//                }
            }

            @Override
            public void onError(String s) {
                FeedbackActivity feedbackActivity = weakReference.get();
                if (feedbackActivity != null) {
//                    if (feedbackActivity.photos.contains(pathImage)) {
//                        feedbackActivity.updateProgress(-1, positionAdapter);
//                    }
                    feedbackActivity.countUploadImageSuccess += 1;
                    if (feedbackActivity.countUploadImageSuccess == feedbackActivity.countImageUpload) {
                        feedbackActivity.startFeedbackUpload();
                    }
                    feedbackActivity.onDeletePhoto(pathImage);
                    feedbackActivity.showToast(R.string.e500_internal_server_error);
                }
//                uploadProgress.setState(UploadProgress.State.FAILURE);
            }

            @Override
            public void onComplete() {
                UploadSingleton.getInstance().remove(pathImage);
                FeedbackActivity feedbackActivity = weakReference.get();
                if (feedbackActivity == null) return;
//                if (feedbackActivity.photos.contains(pathImage)) {
//                    feedbackActivity.updateProgress(100, positionAdapter);
//                }

                feedbackActivity.countUploadImageSuccess += 1;
                if (feedbackActivity.countUploadImageSuccess == feedbackActivity.countImageUpload) {
                    feedbackActivity.startFeedbackUpload();
                }
//                if (feedbackActivity.recPhoto != null && feedbackActivity.isSend) {
//                    feedbackActivity.showLoadingDialog("", R.string.loading);
//                    feedbackActivity.recPhoto.removeCallbacks(feedbackActivity.mUploadRunnable);
//                    feedbackActivity.recPhoto.postDelayed(feedbackActivity.mUploadRunnable, 300);
//                }
            }
        });
    }

    private void updateProgress(int progress, int position) {
        if (recPhoto == null) {
            return;
        }
        if (recPhoto.findViewHolderForAdapterPosition(position) != null
                && recPhoto.findViewHolderForAdapterPosition(position) instanceof ImageSelectedFeedbackViewHolder) {
            ImageSelectedFeedbackViewHolder imageSelectedFeedbackViewHolder = (ImageSelectedFeedbackViewHolder) recPhoto.findViewHolderForAdapterPosition(position);
            imageSelectedFeedbackViewHolder.uploadProgress(progress);
        }
    }

    private static class PositiveListener implements com.metfone.selfcare.ui.dialog.PositiveListener<Object> {

        private @Nullable
        FeedbackActivity feedbackActivity;

        void setFeedbackActivity(@Nullable FeedbackActivity feedbackActivity) {
            this.feedbackActivity = feedbackActivity;
        }

        @Override
        public void onPositive(Object result) {
            if (feedbackActivity != null) {
                feedbackActivity.recPhoto.removeCallbacks(feedbackActivity.mFinishRunnable);
                feedbackActivity.finish();
            }
        }
    }

    private static class UploadRunnable implements Runnable {

        private @Nullable
        FeedbackActivity feedbackActivity;

        void setFeedbackActivity(@Nullable FeedbackActivity feedbackActivity) {
            this.feedbackActivity = feedbackActivity;
        }

        @Override
        public void run() {
            if (feedbackActivity != null) {
                feedbackActivity.feedback();
            }
        }
    }

    private static class FinishRunnable implements Runnable {

        private @Nullable
        FeedbackActivity feedbackActivity;

        void setFeedbackActivity(@Nullable FeedbackActivity feedbackActivity) {
            this.feedbackActivity = feedbackActivity;
        }

        @Override
        public void run() {
            if (feedbackActivity != null) {
                feedbackActivity.finish();
            }
        }
    }

    private static class OnPhotoListener implements PhotoAdapter.OnPhotoListener {

        private @Nullable
        FeedbackActivity feedbackActivity;

        void setFeedbackActivity(@Nullable FeedbackActivity feedbackActivity) {
            this.feedbackActivity = feedbackActivity;
        }

        @Override
        public void onGetPhoto() {
            if (feedbackActivity != null) {
                feedbackActivity.onGetPhoto();
            }
        }

        @Override
        public void onDeletePhoto(String photo) {
            if (feedbackActivity != null) {
                feedbackActivity.onDeletePhoto(photo);
            }
        }
    }

    public static class PhotoAdapter extends BaseAdapterV2<Object, LinearLayoutManager, PhotoAdapter.ViewHolder> {

        private static final int EMPTY = -1;
        private static final int NON = 0;
        private static final int PHOTO = 1;

        @Nullable
        OnPhotoListener mOnPhotoListener;

        PhotoAdapter(Activity act) {
            super(act);
        }

        void setOnPhotoListener(@Nullable OnPhotoListener onPhotoListener) {
            this.mOnPhotoListener = onPhotoListener;
        }

        @Override
        public int getItemViewType(int position) {
            Object item = items.get(position);
            if (item instanceof String) {
                return PHOTO;
            } else if (Utilities.equals(item, EMPTY)) {
                return EMPTY;
            } else {
                return NON;
            }
        }

        @Override
        public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
            super.onViewAttachedToWindow(holder);
            holder.setOnPhotoListener(mOnPhotoListener);
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case NON:
                    return new NonHolder(activity, layoutInflater, parent);
                case EMPTY:
                    return new EmptyHolder(activity, layoutInflater, parent);
                default:
                    return new PhotoHolder(activity, layoutInflater, parent);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.bindData(items, position);
        }

        @Override
        public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            holder.setOnPhotoListener(null);
        }

        public static class ViewHolder extends BaseAdapterV2.ViewHolder {

            @Nullable
            OnPhotoListener mOnPhotoListener;

            ViewHolder(View view) {
                super(view);
            }

            void setOnPhotoListener(OnPhotoListener onPhotoListener) {
                this.mOnPhotoListener = onPhotoListener;
            }
        }

        private static class EmptyHolder extends ViewHolder implements View.OnClickListener {

            EmptyHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
                super(layoutInflater.inflate(R.layout.item_feedback_empty, parent, false));
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (mOnPhotoListener != null) {
                    mOnPhotoListener.onGetPhoto();
                }
            }
        }

        private static class NonHolder extends ViewHolder implements View.OnClickListener {

            NonHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
                super(layoutInflater.inflate(R.layout.item_feedback_non, parent, false));
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (mOnPhotoListener != null) {
                    mOnPhotoListener.onGetPhoto();
                }
            }
        }

        public static class PhotoHolder extends ViewHolder implements View.OnClickListener {

            @BindView(R.id.upload_layout)
            UploadLayout uploadLayout;
            @BindView(R.id.iv_close)
            ImageView ivClose;

            private String photo;

            PhotoHolder(Activity activity, LayoutInflater layoutInflater, ViewGroup parent) {
                super(layoutInflater.inflate(R.layout.item_feedback_photo, parent, false));
                ivClose.setOnClickListener(this);
            }

            @Override
            public void bindData(ArrayList<Object> items, int position) {
                super.bindData(items, position);
                Object item = items.get(position);
                if (item instanceof String) {
                    photo = (String) item;
                    uploadLayout.bindData(photo);
                }
            }

            @Override
            public void onClick(View v) {
                if (mOnPhotoListener != null && Utilities.notEmpty(photo)) {
                    mOnPhotoListener.onDeletePhoto(photo);
                }
            }
        }

        interface OnPhotoListener {
            void onGetPhoto();

            void onDeletePhoto(String photo);
        }
    }

    // dainv

}

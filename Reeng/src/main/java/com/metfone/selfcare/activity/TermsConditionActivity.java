package com.metfone.selfcare.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.metfone.esport.common.Common;
import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.metfone.selfcare.business.UserInfoBusiness.setTextBold;

public class TermsConditionActivity extends BaseSlidingFragmentActivity {
//    @BindView(R.id.txt_terms1)
//    TextView txt_terms1;
//    @BindView(R.id.txt_terms2)
//    TextView txt_terms2;
//    @BindView(R.id.txt_terms3)
//    TextView txt_terms3;
//    @BindView(R.id.txt_terms4)
//    TextView txt_terms4;
//    @BindView(R.id.txt_terms5)
//    TextView txt_terms5;
//    @BindView(R.id.btn_terms_continue)
//    TextView btn_terms_continue;
//    @BindView(R.id.sv_terms)
//    ScrollView sv_terms;
        @BindView(R.id.ivBack)
        ImageButton ivBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        ButterKnife.bind(this);
        setColorStatusBar(R.color.white);
        WebView view = findViewById(R.id.webView);
        view.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl("https://metfone.com.kh/en/camid");
//        spanText();
    }
    private void spanText(){
//        setTextBold(txt_terms1,getString(R.string.terms1));
//        setTextBold(txt_terms2,getString(R.string.terms2));
//        setTextBold(txt_terms3,getString(R.string.terms3));
//        setTextBold(txt_terms4,getString(R.string.terms4));
//        setTextBold(txt_terms5,getString(R.string.terms5));
    }
    @OnClick({ R.id.ivBack})
    public void onClick(View view) {
        finish();
    }
}

package com.metfone.selfcare.activity.update_info;

import static com.metfone.selfcare.business.UserInfoBusiness.isEmailValid;
import static com.metfone.selfcare.business.UserInfoBusiness.isValidPhoneNumber;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.update_info.listener.EditButtonListener;
import com.metfone.selfcare.adapter.SpinAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.FragmentInformationBinding;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.MappingUtils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by toanvk2 on 7/27/2017.
 */

public class InformationFragment extends BaseFragment {
    private UserIdentityInfo userIdentityInfo;

    private FragmentInformationBinding binding;
    private SpinAdapter districtAdapter;
    private List<String> genders;
    private List<String> countries;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayAdapter<String> countriesAdapter;
    private EditButtonListener listener;
    View.OnTouchListener onTouchListener = (v, event) -> {
        ((BaseSlidingFragmentActivity) requireActivity()).hideKeyboard();
        return v.performClick();
    };


    public static InformationFragment newInstance(UserIdentityInfo userIdentityInfo) {
        InformationFragment fragment = new InformationFragment();
        Bundle args = new Bundle();
        args.putSerializable(EnumUtils.OBJECT_KEY, userIdentityInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (EditButtonListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentInformationBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUI(binding.scrFullfill);
        setAction();
        if (getArguments() != null) {
            userIdentityInfo = (UserIdentityInfo) getArguments().getSerializable(EnumUtils.OBJECT_KEY);
            if (userIdentityInfo != null) {
                drawProfile(userIdentityInfo);
            }
        }
    }

    private void setupUI(View view) {
        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener((v, event) -> {
                UserInfoBusiness.hideKeyboard(requireActivity());
                return false;
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    private void setAction() {
        binding.edtDob.setOnClickListener((v) -> {
            InputMethodUtils.hideSoftKeyboard(requireActivity());
            showDialogTimePicker(binding.edtDob, 2021, false);
        });
        binding.edtIssueDate.setOnClickListener((v) -> {
            InputMethodUtils.hideSoftKeyboard(requireActivity());
            showDialogTimePicker(binding.edtIssueDate, 2021, false);
        });
        binding.iclOther.edtExpireDate.setOnClickListener((v) -> {
            InputMethodUtils.hideSoftKeyboard(requireActivity());
            showDialogTimePicker(binding.iclOther.edtExpireDate, 2100, true);
        });
        binding.iclPassport.edtExpireDateP.setOnClickListener((v) -> {
            InputMethodUtils.hideSoftKeyboard(requireActivity());
            showDialogTimePicker(binding.iclPassport.edtExpireDateP, 2100, true);
        });
        binding.iclPassport.edtVisaDateP.setOnClickListener((v) -> {
            InputMethodUtils.hideSoftKeyboard(requireActivity());
            showDialogTimePicker(binding.iclPassport.edtVisaDateP, 2050, true);
        });

        binding.iclOther.spnSex.setOnTouchListener(onTouchListener);
        binding.iclPassport.spnSexP.setOnTouchListener(onTouchListener);
        genders = Arrays.asList(getResources().getStringArray(R.array.sex_your));
        arrayAdapter = new ArrayAdapter<String>(requireContext(), R.layout.spinner_custom_textcolor, genders) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return genders.size() - 2;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.iclOther.spnSex.setAdapter(arrayAdapter);
        binding.iclOther.spnSex.setSelection(3);
        binding.iclPassport.spnSexP.setAdapter(arrayAdapter);
        binding.iclPassport.spnSexP.setSelection(3);

        binding.iclOther.spnNationality.setOnTouchListener(onTouchListener);
        binding.iclPassport.spnNationalityP.setOnTouchListener(onTouchListener);
        countries = Arrays.asList(getResources().getStringArray(R.array.nationality));
        countriesAdapter = new ArrayAdapter<String>(requireContext(), R.layout.display_spinner_dark, countries) {
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }

            public int getCount() {
                return countries.size() - 1;
            }
        };
        countriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.iclOther.spnNationality.setAdapter(countriesAdapter);
        binding.iclOther.spnNationality.setSelection(8);
        binding.iclPassport.spnNationalityP.setAdapter(countriesAdapter);
        binding.iclPassport.spnNationalityP.setSelection(8);
        binding.btnUpdate.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).handleUpdate();
        });
        binding.btnEdit.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).setScan(false);
            if (listener != null) {
                listener.onEditData();
            }
            updateButtonEdit();
        });

        binding.btnBack.setOnClickListener(v -> {
            ((ConfirmIdentityInfoActivity) requireActivity()).handleBack(1);
        });
        binding.btnNext.setOnClickListener(v -> {
            if (validateInfo()) {
                ((ConfirmIdentityInfoActivity) requireActivity()).handleNext(1);
            }
        });
    }

    private void drawProfile(UserIdentityInfo userInfo) {
        if (checkPassport()) {
            binding.iclPassport.clRoot.setVisibility(View.VISIBLE);
            binding.iclOther.clRoot.setVisibility(View.GONE);
        } else {
            binding.iclPassport.clRoot.setVisibility(View.GONE);
            binding.iclOther.clRoot.setVisibility(View.VISIBLE);
        }
        Log.e(TAG, "tabInfo drawProfile: " + userInfo.showInfo());
        String userName = userInfo.getName();
        String idNo = userInfo.getIdNo();
        String gender = userInfo.getSex();
        String nationality = userInfo.getNationality();
        String email = userInfo.getEmail();
        if (TextUtils.isEmpty(userName)) {
            binding.edtFullName.setText(R.string.empty_string);
        } else {
            binding.edtFullName.setText(userName);
        }

        if (gender != null && !gender.isEmpty()) {
            boolean equals = gender.toLowerCase(Locale.ROOT).equals("M".toLowerCase(Locale.ROOT));
            if (checkPassport()) {
                //Male
                if (equals)
                    binding.iclPassport.spnSexP.setSelection(0);
                    //Female
                else
                    binding.iclPassport.spnSexP.setSelection(1);
            } else {
                //Male
                if (equals)
                    binding.iclOther.spnSex.setSelection(0);
                    //Female
                else
                    binding.iclOther.spnSex.setSelection(1);
            }
        }
        if (TextUtils.isEmpty(userInfo.getIdIssueDate())) {
            binding.edtIssueDate.setText(R.string.empty_string);
        } else {
            binding.edtIssueDate.setText(DateTimeUtils.dateStrToStringMP(userInfo.getIdIssueDate()));
        }
        if (TextUtils.isEmpty(userInfo.getExpireVisa())) {
            binding.iclPassport.edtVisaDateP.setText(R.string.empty_string);
        } else {
            binding.iclPassport.edtVisaDateP.setText(DateTimeUtils.dateStrToStringMP(userInfo.getExpireVisa()));
        }

        if (TextUtils.isEmpty(userInfo.getIdExpireDate())) {
            binding.iclPassport.edtExpireDateP.setText(R.string.empty_string);
            binding.iclOther.edtExpireDate.setText(R.string.empty_string);
//            if (checkPassport()) {
//                binding.iclPassport.edtExpireDateP.setText(R.string.empty_string);
//            } else {
//                binding.iclOther.edtExpireDate.setText(R.string.empty_string);
//            }
        } else {
            binding.iclOther.edtExpireDate.setText(DateTimeUtils.dateStrToStringMP(userInfo.getIdExpireDate()));
            binding.iclPassport.edtExpireDateP.setText(DateTimeUtils.dateStrToStringMP(userInfo.getIdExpireDate()));
//            if (checkPassport()) {
//                binding.iclPassport.edtExpireDateP.setText(DateTimeUtils.dateStrToStringMP(userInfo.getIdExpireDate()));
//            } else {
//                binding.iclOther.edtExpireDate.setText(DateTimeUtils.dateStrToStringMP(userInfo.getIdExpireDate()));
//            }
        }
        if (TextUtils.isEmpty(userInfo.getBirthDate())) {
            binding.edtDob.setText(R.string.empty_string);
        } else {
            binding.edtDob.setText(DateTimeUtils.dateStrToStringMP(userInfo.getBirthDate()));
        }
        if (TextUtils.isEmpty(idNo)) {
            binding.edtIdNumber.setText(R.string.empty_string);
        } else {
            binding.edtIdNumber.setText(idNo);
        }
        if (TextUtils.isEmpty(userInfo.getTelFax()) && !isValidPhoneNumber(userInfo.getTelFax())) {
            binding.iclOther.edtContact.setText(getString(R.string.empty_string));
            binding.iclPassport.edtContactP.setText(getString(R.string.empty_string));
        } else {
            binding.iclPassport.edtContactP.setText(userInfo.getTelFax());
            binding.iclOther.edtContact.setText(userInfo.getTelFax());
        }
        if (getIDType().toLowerCase(Locale.ROOT)
                .equals(getString(R.string.cambodia_id_card).toLowerCase(Locale.ROOT))) {
            binding.iclOther.spnNationality.setSelection(MappingUtils.mapNationality(
                    requireContext(), getString(R.string.nationality_cam)));
            binding.iclPassport.spnNationalityP.setSelection(
                    MappingUtils.mapNationality(requireContext(), getString(R.string.nationality_cam)));
            binding.iclOther.spnNationality.setEnabled(false);
        } else {
            if (checkPassport()) {
                binding.iclPassport.spnNationalityP.setSelection(MappingUtils.mapNationality(requireContext(), nationality));
                binding.iclPassport.spnNationalityP.setEnabled(true);
            } else {
                binding.iclOther.spnNationality.setSelection(MappingUtils.mapNationality(requireContext(), nationality));
                binding.iclOther.spnNationality.setEnabled(true);
            }
        }

        if (TextUtils.isEmpty(email)) {
            binding.edtEmail.setText("");
        } else {
            binding.edtEmail.setText(email);
        }
    }

    private String getIDType() {
        return ((ConfirmIdentityInfoActivity) requireActivity()).getTypeID();
    }

    private boolean checkPassport() {
        return getIDType().toLowerCase(Locale.ROOT)
                .equals(getString(R.string.passport).toLowerCase(Locale.ROOT));
    }

    public UserInfo informationUser() {
        String phoneNumber = application.getCamIdUserBusiness().getMetfoneUsernameIsdn();
        String fullName = binding.edtFullName.getText().toString().trim();
        String idNumber = binding.edtIdNumber.getText().toString().trim();
        String contact = binding.iclOther.edtContact.getText().toString();
        String dob = TimeHelper.convertTimeBCCSToCamID(binding.edtDob.getText().toString().trim(),
                UserInfoBusiness.getCurrentLanguage(((BaseSlidingFragmentActivity) requireActivity()),
                        ((ApplicationController) requireActivity().getApplication())));
        if (checkPassport()) {
            contact = binding.iclPassport.edtContactP.getText().toString();
        }

        String email = binding.edtEmail.getText().toString();

        int gender = 0;
        if (getString(R.string.sex_female).equals(binding.iclOther.spnSex.getSelectedItem().toString())) {
            gender = 1;
        }
        if (checkPassport()) {
            gender = binding.iclPassport.spnSexP.getSelectedItemPosition();
        }
        if (TextUtils.isEmpty(contact)) {
            contact = phoneNumber;
        }

        UserInfo userInfo = new UserInfo();
        userInfo.setGender(gender + 1);
        userInfo.setPhone_number(phoneNumber);
        userInfo.setFull_name(fullName);
        userInfo.setIdentity_number(idNumber);
        userInfo.setDate_of_birth(dob);
        userInfo.setContact(contact);
        userInfo.setEmail(email);
        return userInfo;
    }

    public UserIdentityInfo informationIdentity() {
        String phoneNumber = application.getCamIdUserBusiness().getMetfoneUsernameIsdn();
        String dob = binding.edtDob.getText().toString().trim();
        String idNumber = binding.edtIdNumber.getText().toString().trim();
        String fullName = binding.edtFullName.getText().toString().trim();
        String issueDate = binding.edtIssueDate.getText().toString();
        String email = binding.edtEmail.getText().toString().trim();
        String contact = binding.iclOther.edtContact.getText().toString().trim();
        String nationality = binding.iclOther.spnNationality.getSelectedItem().toString();
        String expireDate = binding.iclOther.edtExpireDate.getText().toString();
        String visaExpireDate = binding.iclPassport.edtVisaDateP.getText().toString();

        String gender;
        if (getString(R.string.sex_male).equals(binding.iclOther.spnSex.getSelectedItem().toString())) {
            gender = "M";
        } else if (getString(R.string.sex_female).equals(binding.iclOther.spnSex.getSelectedItem().toString())) {
            gender = "F";
        } else {
            gender = "O";
        }

        if (checkPassport()) {
            contact = binding.iclPassport.edtContactP.getText().toString();
            expireDate = binding.iclPassport.edtExpireDateP.getText().toString();
            nationality = binding.iclPassport.spnNationalityP.getSelectedItem().toString();
            if (getString(R.string.sex_male).equals(binding.iclPassport.spnSexP.getSelectedItem().toString())) {
                gender = "M";
            } else if (getString(R.string.sex_female).equals(binding.iclPassport.spnSexP.getSelectedItem().toString())) {
                gender = "F";
            } else {
                gender = "O";
            }
        }
        if (TextUtils.isEmpty(contact)) {
            contact = phoneNumber;
        }

        UserIdentityInfo userInfo = userIdentityInfo.copy();
        userInfo.setSex(gender);
        userInfo.setBirthDate(dob);
        userInfo.setIdIssueDate(issueDate);
        userInfo.setIdExpireDate(expireDate);
        userInfo.setExpireVisa(visaExpireDate);
        userInfo.setTelFax(contact);
        userInfo.setNationality(nationality);
        userInfo.setIdNo(idNumber);
        userInfo.setEmail(email);
        userInfo.setName(fullName);
        return userInfo;
    }

    private void showDialogTimePicker(CamIdTextView view, int maxYear, boolean isGreater) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        if (mYear < maxYear) {
            mYear = maxYear;
        }
        String dateChoseDefault;
        if (TextUtils.isEmpty(view.getText().toString())
                || !DateTimeUtils.checkYear(view.getText().toString(), DateTimeUtils.FORMAT_1)) {
            dateChoseDefault = UserInfoBusiness.getCurrentDate();
        } else {
            dateChoseDefault = view.getText().toString();
        }
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(requireContext(), (year, month, day, dateDesc) -> {
            view.setText(dateDesc);
        }).textConfirm(getResources().getString(R.string.confirm_button)) //text of confirm button
                .textCancel(getResources().getString(R.string.cancel_button)) //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(25) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .dateChose(dateChoseDefault) // date chose when init popwindow
                .minYear(1900)
                .maxYear(mYear + 1) // max year in loop
                .isGreater(isGreater)
                .build();
        pickerPopWin.showPopWin(requireActivity());
    }

    public boolean validateInfo() {
        String fullName = binding.edtFullName.getText().toString();
        String issueDate = binding.edtIssueDate.getText().toString();
        String dob = binding.edtDob.getText().toString();
        String idNo = binding.edtIdNumber.getText().toString();
        String expireDate = binding.iclOther.edtExpireDate.getText().toString();
        String email = binding.edtEmail.getText().toString().trim();
        int gender = binding.iclOther.spnSex.getSelectedItemPosition();
        String nationality = binding.iclOther.spnNationality.getSelectedItem().toString();
        String visa = binding.iclPassport.edtVisaDateP.getText().toString();
        String idType = ((ConfirmIdentityInfoActivity) requireActivity()).getTypeID();
        String contact = binding.iclOther.edtContact.getText().toString().trim();

        if (checkPassport()) {
            expireDate = binding.iclPassport.edtExpireDateP.getText().toString();
        }
        if (checkPassport()) {
            gender = binding.iclPassport.spnSexP.getSelectedItemPosition();
        }
        if (checkPassport()) {
            nationality = binding.iclPassport.spnNationalityP.getSelectedItem().toString();
        }
        if (checkPassport()) {
            contact = binding.iclPassport.edtContactP.getText().toString().trim();
        }

        if (TextUtils.isEmpty(fullName)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_fullname));
            return false;
        }

        if (TextUtils.isEmpty(issueDate)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_issue_date));
            return false;
        }

        if (TextUtils.isEmpty(dob)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_dob));
            return false;
        }

        if (TextUtils.isEmpty(idNo)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_id_number));
            return false;
        }

        if (TextUtils.isEmpty(expireDate)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_expire_date));
            return false;
        }

        if (checkPassport() && TextUtils.isEmpty(visa)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_visa_expire_date));
            return false;
        }

        if (gender > 2) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_gender));
            return false;
        }

        if (!TextUtils.isEmpty(email) && !isEmailValid(email)) {
            ToastUtils.showToast(requireContext(), getString(R.string.invalid_email_address));
            return false;
        }

        if (TextUtils.isEmpty(nationality)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_nationality));
            return false;
        }

        if (TextUtils.isEmpty(contact)) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_contact));
            return false;
        }

        if (!nationality.equals(getString(R.string.nationality_cam)) && idType.equals(getString(R.string.cambodia_id_card))) {
            ToastUtils.showToast(requireContext(), getString(R.string.txt_error_not_match_info_type_id));
            return false;
        }

        return true;
    }

    public void updateInformationOCR(String name, String idNumber, String dob, String sexEn,
                                     String expireDate, String nationality) {

        binding.btnEdit.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(name)) {
            binding.edtFullName.setText(name);
        }

        if (!TextUtils.isEmpty(idNumber)) {
            binding.edtIdNumber.setText(idNumber);
        }

        if (!TextUtils.isEmpty(dob)) {
            binding.edtDob.setText(DateTimeUtils.getDateFromOCR(dob));
        }

        if (checkPassport()) {
            if (!TextUtils.isEmpty(expireDate)) {
                binding.iclPassport.edtExpireDateP.setText(DateTimeUtils.getDateFromOCR(expireDate));
            }
        } else {
            if (!TextUtils.isEmpty(expireDate)) {
                binding.iclOther.edtExpireDate.setText(DateTimeUtils.getDateFromOCR(expireDate));
            }
        }

        if ("Female".toLowerCase(Locale.ROOT).equals(sexEn.toLowerCase(Locale.ROOT))) {
            binding.iclOther.spnSex.setSelection(1);
            binding.iclPassport.spnSexP.setSelection(1);
        } else if ("Male".toLowerCase(Locale.ROOT).equals(sexEn.toLowerCase(Locale.ROOT))) {
            binding.iclOther.spnSex.setSelection(0);
            binding.iclPassport.spnSexP.setSelection(0);
        }

        if (!TextUtils.isEmpty(nationality)) {
            if (getIDType().toLowerCase(Locale.ROOT)
                    .equals(getString(R.string.cambodia_id_card).toLowerCase(Locale.ROOT))) {
                binding.iclOther.spnNationality.setSelection(
                        MappingUtils.mapNationality(requireContext(), getString(R.string.nationality_cam)));
            } else {
                binding.iclOther.spnNationality.setSelection(MappingUtils.mapNationality(requireContext(), nationality));
                binding.iclPassport.spnNationalityP.setSelection(MappingUtils.mapNationality(requireContext(), nationality));
            }
        } else {
            if (getIDType().toLowerCase(Locale.ROOT)
                    .equals(getString(R.string.cambodia_id_card).toLowerCase(Locale.ROOT))) {
                binding.iclOther.spnNationality.setSelection(
                        MappingUtils.mapNationality(requireContext(), getString(R.string.nationality_cam)));
            }
        }

        if (checkPassport()) {
            binding.edtFullName.setEnabled(false);
            binding.iclPassport.spnNationalityP.setEnabled(false);
            binding.edtIdNumber.setEnabled(false);
            binding.edtDob.setEnabled(false);
            binding.iclPassport.edtExpireDateP.setEnabled(false);
            binding.iclPassport.spnSexP.setEnabled(false);
        } else {
            binding.edtFullName.setEnabled(false);
            binding.iclOther.spnNationality.setEnabled(false);
            binding.edtIdNumber.setEnabled(false);
            binding.edtDob.setEnabled(false);
            binding.iclOther.spnSex.setEnabled(false);
        }
    }

    public void updateNationality(String typeID) {
        if (typeID.toLowerCase(Locale.ROOT)
                .equals(getString(R.string.cambodia_id_card).toLowerCase(Locale.ROOT))) {
            binding.iclOther.spnNationality.setSelection(
                    MappingUtils.mapNationality(requireContext(), getString(R.string.nationality_cam)));
            binding.iclOther.spnNationality.setEnabled(false);
        } else {
            binding.iclOther.spnNationality.setSelection(0);
            binding.iclPassport.spnNationalityP.setSelection(0);
            binding.iclOther.spnNationality.setEnabled(true);
            binding.iclPassport.spnNationalityP.setEnabled(true);
        }

        if (checkPassport()) {
            binding.iclPassport.clRoot.setVisibility(View.VISIBLE);
            binding.iclOther.clRoot.setVisibility(View.GONE);
        } else {
            binding.iclPassport.clRoot.setVisibility(View.GONE);
            binding.iclOther.clRoot.setVisibility(View.VISIBLE);
        }
    }

    public void updateButtonEdit() {
        binding.btnEdit.setVisibility(View.GONE);
        binding.edtFullName.setEnabled(true);
        binding.edtDob.setEnabled(true);
        binding.edtIdNumber.setEnabled(true);
        binding.iclOther.spnSex.setEnabled(true);
        binding.iclPassport.spnSexP.setEnabled(true);
        binding.iclOther.edtExpireDate.setEnabled(true);
        binding.iclPassport.edtExpireDateP.setEnabled(true);
        binding.iclPassport.spnNationalityP.setEnabled(true);

        if (!getIDType().toLowerCase(Locale.ROOT).equals(getString(R.string.cambodia_id_card).toLowerCase(Locale.ROOT))) {
            binding.iclOther.spnNationality.setEnabled(true);
        }
    }
}

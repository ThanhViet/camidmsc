package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ApplicationLockManager;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants.LOCK_APP;
import com.metfone.selfcare.helper.Constants.SETTINGS;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.ui.dialog.BottomSheetFingerPrint;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.dialog.DialogRadioSelect;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;
import com.metfone.selfcare.v5.utils.ToastUtils;
import com.metfone.selfcare.v5.utils.Util;
import com.metfone.selfcare.v5.widget.SwitchButton;
import com.metfone.selfcare.v5.widget.ViewPIN;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 3/9/2016.
 */
public class LockAppActivity extends BaseSlidingFragmentActivity implements
        View.OnClickListener, BottomSheetFingerPrint.FingerPrintCallback {
    private static final String TAG = LockAppActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private ApplicationLockManager mAppLockStatManager;
    private ArrayList<ItemContextMenu> mListChooseTime;
    private Handler mHandler;
    private int type;
    private boolean isRefreshDetail, isChangePass = false;
    //
//    private StringBuilder sbEnterCode = new StringBuilder();
    private String currentNewPass;
    private boolean isSupportFingerPrintAuth = false;
    private boolean isFingerPrintEnrolled = false;
    private BottomSheetFingerPrint mBottomSheetFingerPrint;
    private DialogConfirm mFingerPrintConfirmDialog = null;

    //view field
    private ConstraintLayout mLlEnterCode;
    private LinearLayoutCompat mLlSetting, mRllChangePass;
    private ConstraintLayout mRllEnableLock, mRllChangeTime, mRllFingerPrint;
    private SwitchButton mTgbEnabelLock, mTgbEnableFingerPrint;
    private AppCompatTextView mTvwChangeTimeValue, tvTitleToolbar, tvUseFingerPrint, tvChangePass, tvAutoLockApp;
    private AppCompatTextView mTvwCodeLabel;
    private AppCompatImageView icQuestionLockApp, icNext;
    private ImageView mImgFingerPrint, mImgLogo;
    private ViewPIN viewPIN;

    private boolean isShowingKeyboard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_lock_activity);
        mApplication = (ApplicationController) getApplicationContext();
        mAppLockStatManager = mApplication.getAppLockManager();
        checkSupportFingerPrintAuth();
        getData(savedInstanceState);
        setToolBar(findViewById(R.id.tool_bar));
        changeStatusBar(true);
        setActionBar();
        setComponentViews();
        mAppLockStatManager.setShowLockAppActivity(true);
        drawDetail();
        setListener();
        trackingScreen(TAG);
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mTgbEnableFingerPrint.setClickable(false);
    }

    private void checkSupportFingerPrintAuth() {
        try {
            if (!Config.Features.FLAG_SUPPORT_FINGERPRINT_AUTH) {
                isSupportFingerPrintAuth = false;
                isFingerPrintEnrolled = false;
                mAppLockStatManager.setEnableFingerPrint(false);
                return;
            }
            FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(this);
            isSupportFingerPrintAuth = fingerprintManager.isHardwareDetected();
            isFingerPrintEnrolled = fingerprintManager.hasEnrolledFingerprints();
            boolean isFingerPrintEnable = mAppLockStatManager.isEnableFingerPrintLock();
            mAppLockStatManager.setEnableFingerPrint(isFingerPrintEnable && isSupportFingerPrintAuth && isFingerPrintEnrolled);
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        super.onResume();
        checkSupportFingerPrintAuth();
        if (type != LOCK_APP.LOCK_APP_LOCKED && mAppLockStatManager.isAppLocked()) {
            type = LOCK_APP.LOCK_APP_LOCKED_SETTING;
            drawDetail();
        }

        if (type == LOCK_APP.LOCK_APP_LOCKED || type == LOCK_APP.LOCK_APP_LOCKED_SETTING && mAppLockStatManager.isAppLocked()) {
            handleFingerPrintAuth(false);
        }
    }

    @Override
    protected void onPause() {
        /*if (type != Constants.LOCK_APP.LOCK_APP_LOCKED) {
            finish();
        }*/
        mHandler = null;
        if (mBottomSheetFingerPrint != null) {
            mBottomSheetFingerPrint.dismiss();
            mBottomSheetFingerPrint = null;
        }
        dismissFingerprintRegisterDialog();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if (type == LOCK_APP.LOCK_APP_LOCKED) {
            finish();
        }*/
        // finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SETTINGS.DATA_FRAGMENT, type);
        outState.putInt(SETTINGS.DATA_FINGERPRINT_ERROR, mFingerPrintErrorCount);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
//        sbEnterCode = new StringBuilder();
        viewPIN.reset();
        if (type == LOCK_APP.LOCK_APP_SETTING || type == LOCK_APP.LOCK_APP_OPEN_SETTING) {
            super.onBackPressed();
        } else if (type == LOCK_APP.LOCK_APP_LOCKED || type == LOCK_APP.LOCK_APP_LOCKED_SETTING) {
            UIUtil.hideKeyboard(this, viewPIN);
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                this.startActivity(intent);
            } catch (Exception e) {// alps Nova_F3 không hỗ trợ mở home launcher
                Log.e(TAG, "Exception", e);
            } finally {
                mApplication.getAppStateManager().setAppWentToBg(true);
                finish();
            }
        } else {
            UIUtil.hideKeyboard(this, viewPIN);
            type = LOCK_APP.LOCK_APP_SETTING;
            drawDetail();
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        mAppLockStatManager.setShowLockAppActivity(false);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ab_back_btn:
                onBackPressed();
                break;
            case R.id.app_lock_setting_enable_layout:
                mTgbEnabelLock.setChecked(!mTgbEnabelLock.isChecked());
                isChangePass = false;
                break;
            case R.id.app_lock_setting_change_pass_layout:
                type = LOCK_APP.LOCK_APP_CHANGE_OLD_PASS;
                isChangePass = true;
                drawDetail();
                break;
            case R.id.app_lock_setting_change_time_layout:
                processSettingChangeTime();
                break;

            case R.id.imv_fingerprint:
                handleFingerPrintAuth(true);
                break;

            case R.id.app_lock_setting_fingerprint_layout:
//                mTgbEnableFingerPrint.setChecked(!mTgbEnableFingerPrint.isChecked());
                processFingerPrintEnable();
                break;
            case R.id.icQuestionLockApp:
                float y = icQuestionLockApp.getY() + findViewById(R.id.appBar).getHeight() + mRllChangeTime.getY();
                Util.showDialogExplain(this, getString(R.string.lock_app_setting_note), icQuestionLockApp.getX(), y, icQuestionLockApp);
                break;
        }
    }

    private void processFingerPrintEnable() {
        if (mAppLockStatManager == null || !isSupportFingerPrintAuth) return;
        boolean isEnableFingerPrint = !mTgbEnableFingerPrint.isChecked();
        if (isEnableFingerPrint) {
            if (!isFingerPrintEnrolled) {
                showFingerPrintRegisterDialog();
//                mTgbEnableFingerPrint.setOnCheckedChangeListener(null);
                mTgbEnableFingerPrint.setChecked(false);
                mAppLockStatManager.setEnableFingerPrint(false);
//                mTgbEnableFingerPrint.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
//                        processFingerPrintEnable();
//                    }
//                });
            } else {
                mTgbEnableFingerPrint.setChecked(true);
                mAppLockStatManager.setEnableFingerPrint(true);
            }
        } else {
            mTgbEnableFingerPrint.setChecked(false);
            mAppLockStatManager.setEnableFingerPrint(false);
        }
    }

    private void showFingerPrintRegisterDialog() {
        mFingerPrintConfirmDialog = new DialogConfirm(this, true);
        mFingerPrintConfirmDialog.setLabel(getString(R.string.popup_fingerprint_register));
        mFingerPrintConfirmDialog.setMessage(getString(R.string.fingerprint_need_register));
        mFingerPrintConfirmDialog.setNegativeLabel(getString(R.string.cancel));
        mFingerPrintConfirmDialog.setPositiveLabel(getString(R.string.title_message_setting));
        mFingerPrintConfirmDialog.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                // go to fingerprint settings
                try {
                    Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                    startActivity(intent);
                } catch (Exception e) {
                }
            }
        });
        mFingerPrintConfirmDialog.show();
    }

    private void dismissFingerprintRegisterDialog() {
        try {
            if (mFingerPrintConfirmDialog != null) {
                mFingerPrintConfirmDialog.dismiss();
            }
        } catch (Exception e) {
        }
    }

//    @Override
//    public void onIconClickListener(View view, Object entry, int menuId) {
//        mAppLockStatManager.setTimeCheckLock(menuId);
//        mTvwChangeTimeValue.setText(String.format(getString(R.string.lock_app_setting_change_time_value),
//                mAppLockStatManager.getTimeLockString()));
//    }


    private void getData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            type = savedInstanceState.getInt(SETTINGS.DATA_FRAGMENT);
            mFingerPrintErrorCount = savedInstanceState.getInt(SETTINGS.DATA_FINGERPRINT_ERROR);
        } else {
            type = getIntent().getIntExtra(SETTINGS.DATA_FRAGMENT, LOCK_APP.LOCK_APP_LOCKED);
            mFingerPrintErrorCount = 0;
        }
    }

    private void setActionBar() {
    }

    private void setComponentViews() {
        findViewById(R.id.icBackToolbar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        viewPIN = findViewById(R.id.viewPIN);
        tvUseFingerPrint = findViewById(R.id.app_lock_setting_fingerprint_tv);
        tvChangePass = findViewById(R.id.txtResetPin);
        tvAutoLockApp = findViewById(R.id.txtAutoLockTitle);
        tvTitleToolbar = findViewById(R.id.txtTitleToolbar);
        tvTitleToolbar.setText(R.string.lock_app_title);
        icQuestionLockApp = findViewById(R.id.icQuestionLockApp);
        icNext = findViewById(R.id.icNext);
        mLlSetting = findViewById(R.id.app_lock_setting_layout);
        mLlEnterCode = findViewById(R.id.app_lock_code_layout);
        mRllEnableLock = findViewById(R.id.app_lock_setting_enable_layout);
        mRllChangePass = findViewById(R.id.app_lock_setting_change_pass_layout);
        mRllChangeTime = findViewById(R.id.app_lock_setting_change_time_layout);
        mRllFingerPrint = findViewById(R.id.app_lock_setting_fingerprint_layout);
        mTgbEnabelLock = findViewById(R.id.app_lock_setting_enable_toggle);
        mTgbEnableFingerPrint = findViewById(R.id.app_lock_setting_fingerprint_toggle);
        mTvwChangeTimeValue = findViewById(R.id.txtAutoLockTitleDes);
//        mTvwLockAppDes = (TextView) findViewById(R.id.tv_lock_app_des);
        mTvwCodeLabel = findViewById(R.id.app_lock_label);
        mImgLogo = (ImageView) findViewById(R.id.imv_login_logo);
        mImgFingerPrint = (ImageView) findViewById(R.id.imv_fingerprint);
        setTextLockAppDescription();
    }

    private void setTextLockAppDescription() {
//        if (mTvwLockAppDes == null) return;
//        mTvwLockAppDes.setMovementMethod(LinkMovementMethod.getInstance());
//        setTextViewHTML(mTvwLockAppDes, getString(R.string.lock_app_setting_note_new));
    }

    private void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
    }

    private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                String deeplink = span.getURL();
                DeepLinkHelper.getInstance().openSchemaLink(LockAppActivity.this, deeplink);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mApplication, R.color.bg_mocha)),
                start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void drawDetail() {
        if (type == LOCK_APP.LOCK_APP_LOCKED || type == LOCK_APP.LOCK_APP_LOCKED_SETTING) {
            if (isSupportFingerPrintAuth && mAppLockStatManager.isAppLocked() && mAppLockStatManager.isEnableFingerPrintLock()) {
                mImgLogo.setVisibility(View.VISIBLE);
                mImgFingerPrint.setVisibility(View.VISIBLE);
                mTvwCodeLabel.setText(getString(R.string.confirm_fingerprint_or_passcode));
            } else {
                mImgLogo.setVisibility(View.GONE);
                mImgFingerPrint.setVisibility(View.GONE);
            }
        } else {
            mImgLogo.setVisibility(View.GONE);
            mImgFingerPrint.setVisibility(View.GONE);
        }
        if (type == LOCK_APP.LOCK_APP_SETTING) {
            mLlEnterCode.setVisibility(View.GONE);
            tvTitleToolbar.setVisibility(View.VISIBLE);
            mLlSetting.setVisibility(View.VISIBLE);
            mTgbEnabelLock.setChecked(mAppLockStatManager.isEnableSettingLockApp());
            mRllChangePass.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            mRllChangeTime.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            mRllFingerPrint.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            tvUseFingerPrint.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            mTgbEnableFingerPrint.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            mTvwChangeTimeValue.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            tvAutoLockApp.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            tvChangePass.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            icQuestionLockApp.setEnabled(mAppLockStatManager.isEnableSettingLockApp());
            if(!mAppLockStatManager.isEnableSettingLockApp()) {
                icQuestionLockApp.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.text_disable), android.graphics.PorterDuff.Mode.SRC_IN);
                icNext.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.text_disable), android.graphics.PorterDuff.Mode.SRC_IN);
            }
            mTvwChangeTimeValue.setText(String.format(getString(R.string.lock_app_setting_change_time_value), mAppLockStatManager.getTimeLockString()));
            if (isSupportFingerPrintAuth) {
                mRllFingerPrint.setVisibility(View.VISIBLE);
                if (isFingerPrintEnrolled && mAppLockStatManager.isEnableFingerPrintLock()) {
                    mTgbEnableFingerPrint.setChecked(true);
                } else {
                    mTgbEnableFingerPrint.setChecked(false);
                }
            } else {
                mRllFingerPrint.setVisibility(View.GONE);
            }
        } else {
            mLlEnterCode.setVisibility(View.VISIBLE);
            tvTitleToolbar.setVisibility(View.GONE);
            mLlEnterCode.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (viewPIN != null) {
                        viewPIN.requestFocus();
                        viewPIN.setFocusable(true);
                        UIUtil.showKeyboard(getApplicationContext(), viewPIN);
                    }
                }
            }, 250);
            viewPIN.requestFocus();
            mLlSetting.setVisibility(View.GONE);
            if (type == LOCK_APP.LOCK_APP_CHANGE_PASS_RE) {
                mTvwCodeLabel.setText(R.string.lock_app_label_change_new_re);
            } else if (type == LOCK_APP.LOCK_APP_CHANGE_OLD_PASS) {
                mTvwCodeLabel.setText(R.string.lock_app_label_change_old);
            } else if (type == LOCK_APP.LOCK_APP_CHANGE_PASS) {
                mTvwCodeLabel.setText(R.string.lock_app_label_change_new);
            } else if (type == LOCK_APP.LOCK_APP_LOCKED || type == LOCK_APP.LOCK_APP_LOCKED_SETTING) {
                if (isSupportFingerPrintAuth && mAppLockStatManager.isAppLocked() && mAppLockStatManager.isEnableFingerPrintLock()) {
                    mTvwCodeLabel.setText(getString(R.string.confirm_fingerprint_or_passcode));
                } else {
                    mTvwCodeLabel.setText(R.string.lock_app_label);
                }
            } else {
                mTvwCodeLabel.setText(R.string.lock_app_label);
            }
        }
    }

    private void setListener() {
        mRllEnableLock.setOnClickListener(this);
        mRllChangeTime.setOnClickListener(this);
        mRllChangePass.setOnClickListener(this);
        mImgFingerPrint.setOnClickListener(this);
        mRllFingerPrint.setOnClickListener(this);
        icQuestionLockApp.setOnClickListener(this);
        mTgbEnabelLock.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                processSettingEnable();
            }
        });
//        mTgbEnableFingerPrint.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
//                processFingerPrintEnable();
//            }
//        });
//        if (type == LOCK_APP.LOCK_APP_LOCKED) {
        viewPIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && charSequence.length() == viewPIN.getItemCount()) {
                    onPassCodeInputted(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//        }
        viewPIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE && viewPIN.getText().length() == viewPIN.getItemCount()) {
                    onPassCodeInputted(viewPIN.getText().toString());
                }
                return true;
            }
        });
    }

    /**
     * xu ly setting
     */
    private void processSettingEnable() {
        boolean state = mTgbEnabelLock.isChecked();
        if (state) {
            type = LOCK_APP.LOCK_APP_CHANGE_PASS;
            drawDetail();
        } else {
            mAppLockStatManager.setEnableSettingLockApp(false);
            type = LOCK_APP.LOCK_APP_SETTING;
            drawDetail();
        }
    }

    private void processSettingChangeTime() {
        DialogRadioSelect dialogRadioSelect = DialogRadioSelect.newInstance(DialogRadioSelect.TIME_LOCK_APP_TYPE
                , convertLongtimeToPosition(mAppLockStatManager.getCurrentTimeCheckLock()), R.string.lock_app_setting_change_time);
        dialogRadioSelect.setSelectListener(new BaseDialogFragment.DialogListener() {
            @Override
            public void dialogRightClick(int value) {
                mAppLockStatManager.setTimeCheckLock(convertPositionToLongTime(value));
                mTvwChangeTimeValue.setText(String.format(getString(R.string.lock_app_setting_change_time_value),
                        mAppLockStatManager.getTimeLockString()));
            }

            @Override
            public void dialogLeftClick() {

            }
        });
        dialogRadioSelect.show(getSupportFragmentManager(), "");
    }

    //use for show dialog select time lock app
    private long convertPositionToLongTime(int position) {
        switch (position) {
            case 0:
                return 100;
            case 1:
                return 5000;
            case 2:
                return 10000;
            case 3:
                return 30000;
            case 4:
                return 60000;
            default:
                return 100;
        }
    }

    private int convertLongtimeToPosition(long time) {
        if (time <= LOCK_APP.LOCK_APP_TIME_NOW) {
            return 0;
        } else if (time <= LOCK_APP.LOCK_APP_TIME_5_S) {
            return 1;
        } else if (time <= LOCK_APP.LOCK_APP_TIME_10_S) {
            return 2;
        } else if (time <= LOCK_APP.LOCK_APP_TIME_30_S) {
            return 3;
        } else {
            return 4;
        }
    }

    /**
     * xu ly enter code
     *
     * @param code
     */
    private void onPassCodeInputted(String code) {
        isRefreshDetail = false;
        if (type == LOCK_APP.LOCK_APP_CHANGE_PASS) {
            currentNewPass = code;
            type = LOCK_APP.LOCK_APP_CHANGE_PASS_RE;
            isRefreshDetail = true;
        } else if (type == LOCK_APP.LOCK_APP_CHANGE_PASS_RE) {
            if (code.equals(currentNewPass)) {
                mAppLockStatManager.setChangePassword(code);
                type = LOCK_APP.LOCK_APP_SETTING;
                isRefreshDetail = true;
                if (isChangePass) {
                    ToastUtils.showToastSuccess(getApplicationContext(), getString(R.string.lock_app_setting_success_change_pass));
                    mLlEnterCode.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            UIUtil.hideKeyboard(mApplication, viewPIN);
                        }
                    }, 250);
                } else {
                    ToastUtils.showToastSuccess(getApplicationContext(), getString(R.string.lock_app_setting_success_new_pass));
                    mLlEnterCode.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            UIUtil.hideKeyboard(mApplication, viewPIN);
                        }
                    }, 250);
                }
            } else {
                ToastUtils.showToast(getApplicationContext(), getString(R.string.lock_app_wrong_code));
            }

        } else if (mAppLockStatManager.isValidPassword(code)) {
            if (type == LOCK_APP.LOCK_APP_LOCKED) {
                hideKeyboard();
                mAppLockStatManager.passedPassword();
                finish();
                return;
            } else if (type == LOCK_APP.LOCK_APP_CHANGE_OLD_PASS) {
                type = LOCK_APP.LOCK_APP_CHANGE_PASS;
            } else if (type == LOCK_APP.LOCK_APP_LOCKED_SETTING) {
                type = LOCK_APP.LOCK_APP_SETTING;
                hideKeyboard();
                mAppLockStatManager.passedPassword();
            } else {
                type = LOCK_APP.LOCK_APP_SETTING;
                mLlEnterCode.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.hideKeyboard(mApplication, viewPIN);
                    }
                }, 250);
            }
            isRefreshDetail = true;
        } else {
            ToastUtils.showToast(getApplicationContext(), getString(R.string.lock_app_wrong_code));
        }
        Log.d(TAG, "onPassCodeInputted: " + code);
        // sbEnterCode = new StringBuilder();//reset
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPIN.reset();
                if (isRefreshDetail) {
                    drawDetail();
                }
            }
        }, 300);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        if (mBottomSheetFingerPrint != null) {
            mBottomSheetFingerPrint.dismiss();
        }
        if (mTvwCodeLabel != null) {
            mTvwCodeLabel.setText(errString);
        }
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {

    }

    @Override
    public void onAuthenticationFailed() {
        if (mVibrator != null) {
            long[] pattern = {0, 50, 250, 50, 250};
            mVibrator.vibrate(pattern, -1);
        }
        mFingerPrintErrorCount++;
        if (mFingerPrintErrorCount >= 5) {
            if (mBottomSheetFingerPrint != null) {
                mBottomSheetFingerPrint.dismiss();
            }
            if (mTvwCodeLabel != null) {
                mTvwCodeLabel.setText(R.string.fingerprint_too_many_error);
            }
        }
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        if (mVibrator != null) {
            long[] pattern = {0, 50, 250};
            mVibrator.vibrate(pattern, -1);
        }
        if (mBottomSheetFingerPrint != null) {
            mBottomSheetFingerPrint.dismiss();
        }
        if (type == LOCK_APP.LOCK_APP_LOCKED) {
            if (mAppLockStatManager != null) {
                mAppLockStatManager.passedPassword();
            }
            isRefreshDetail = true;
            finish();
        } else if (type == LOCK_APP.LOCK_APP_LOCKED_SETTING) {
            type = LOCK_APP.LOCK_APP_SETTING;

            if (mAppLockStatManager != null) {
                mAppLockStatManager.passedPassword();
            }
            isRefreshDetail = true;
            if (mHandler != null) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        viewPIN.reset();
                        if (isRefreshDetail) {
                            drawDetail();
                        }
                    }
                }, 300);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void handleFingerPrintAuth(boolean isFingerPrintImvClick) {
        if (!isSupportFingerPrintAuth || !mAppLockStatManager.isEnableFingerPrintLock()) return;
        if (mFingerPrintErrorCount < 5) {
            if (isFingerPrintEnrolled) {
                mBottomSheetFingerPrint = new BottomSheetFingerPrint(this, this);
                mBottomSheetFingerPrint.show();
            } else {
                if (isFingerPrintImvClick) {
                    ToastUtils.showToast(this, getString(R.string.fingerprint_need_register));
                }
            }
        } else {
            if (mTvwCodeLabel != null) {
                mTvwCodeLabel.setText(getString(R.string.fingerprint_too_many_error));
            }
        }
    }


    private Vibrator mVibrator;
    private int mFingerPrintErrorCount = 0;
}

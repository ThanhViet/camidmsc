package com.metfone.selfcare.activity;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.ListDataPackagesAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.CommonApi;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.holder.DataPackageDetailHolder;
import com.metfone.selfcare.model.DataPackagesResponse;
import com.metfone.selfcare.module.spoint.network.response.BuyPackageResponse;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONException;

import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDataPackagesActivity extends BaseSlidingFragmentActivity implements DataPackageDetailHolder.OnBuyPackage {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.empty_progress)
    View progressEmpty;
    @BindView(R.id.empty_text)
    TextView tvEmpty;
    @BindView(R.id.empty_retry_text1)
    TextView tvEmptyRetry1;
    @BindView(R.id.empty_retry_text2)
    TextView tvEmptyRetry2;
    @BindView(R.id.empty_retry_button)
    ImageView btnEmptyRetry;
    @BindView(R.id.empty_layout)
    View viewEmpty;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    private ArrayList<Object> data;
    private ListDataPackagesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data_packages);
        ButterKnife.bind(this);
        String title = ApplicationController.self().getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FUNCTION_DATA_PACKAGES_TITLE);
        if (TextUtils.isEmpty(title))
            title = getString(R.string.title_list_data_packages);
        tvTitle.setText(title);
        initView();
        loadData();
    }

    private void initView() {
        if (data == null) data = new ArrayList<>();
        adapter = new ListDataPackagesAdapter(this, data, this);
        BaseAdapter.setupVerticalRecycler(this, recyclerView, null, adapter, false);
    }

    private void loadData() {
        showLoading();
        CommonApi.getInstance().getListDataPackages(new ApiCallbackV2<ArrayList<DataPackagesResponse.ListDataPackages>>() {
            @Override
            public void onSuccess(String title, ArrayList<DataPackagesResponse.ListDataPackages> listDataPackages) throws JSONException {
                if (data == null) data = new ArrayList<>();
                else data.clear();
                try {
                    if (Utilities.notEmpty(listDataPackages)) {
                        int size = listDataPackages.size() - 1;
                        for (int i = size; i >= 0; i--) {
                            if (listDataPackages.get(i) == null || Utilities.isEmpty(listDataPackages.get(i).getListPackages())) {
                                listDataPackages.remove(i);
                            }
                        }
                        data.addAll(listDataPackages);
                    }
                } catch (Exception e) {
                    Log.e("ListDataPackagesActivity", e);
                }
                if (adapter != null) {
                    adapter.setLabelEnd(title);
                    adapter.notifyDataSetChanged();
                }
                if (Utilities.isEmpty(data))
                    showLoadedEmpty();
                else
                    showLoadedSuccess();
            }

            @Override
            public void onError(String s) {
                if (Utilities.isEmpty(data))
                    showLoadedError();
                else
                    showLoadedSuccess();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void showLoading() {
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.VISIBLE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
    }

    private void showLoadedEmpty() {
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (tvEmpty != null) {
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText(R.string.no_data);
        }
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
    }

    private void showLoadedSuccess() {
        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
        if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.GONE);
    }

    private void showLoadedError() {
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
        if (btnEmptyRetry != null) btnEmptyRetry.setVisibility(View.VISIBLE);
        if (progressEmpty != null) progressEmpty.setVisibility(View.GONE);
        if (tvEmpty != null) tvEmpty.setVisibility(View.GONE);
        if (NetworkHelper.isConnectInternet(this)) {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.VISIBLE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.GONE);
        } else {
            if (tvEmptyRetry1 != null) tvEmptyRetry1.setVisibility(View.GONE);
            if (tvEmptyRetry2 != null) tvEmptyRetry2.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.button_back)
    public void onBackClick() {
        onBackPressed();
    }

    @OnClick(R.id.empty_retry_button)
    public void onRestryClick() {
        loadData();
    }

    @Override
    public void onClickBuy(DataPackagesResponse.DataPackageInfo data) {
        if (data.getType() == 1) {
            DeepLinkHelper.getInstance().openSchemaLink(this, data.getUrl());
        } else if (data.getType() == 2) {
            NavigateActivityHelper.openAppSMS(this, data.getSmsCodes(), data.getSmsCommand());
        } else if (data.getType() == 3) {
            onBuyMochaVideo("", "spoint_pm", data.getId() + "");
        } else if (data.getType() == 4) {
            Uri uri = Uri.parse(data.getUrl());
            String cmd = uri.getQueryParameter("cmd");
            showDialogBuyPackage(cmd, data.getId() + "");
        } else {
            Log.d("DataPackagesInfoHolder", "type " + data.getType() + " chua duoc dinh nghia");
        }
    }

    private void onBuyMochaVideo(String channel, String cmd, String id) {
        showLoadingDialog(null, R.string.waiting);
        CommonApi.getInstance().getListBuyMochaVideo(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                BuyPackageResponse response = gson.fromJson(data, BuyPackageResponse.class);
                hideLoadingDialog();
                if (response == null) return;
                if (response.getCode() == HTTPCode.E200_OK) {
                    showToastDone(response.getDesc());
                    loadData();
                } else if (response.getCode() == HTTPCode.E201_OK) {
                    if (response.getList() != null) {
                        NavigateActivityHelper.openAppSMS(ListDataPackagesActivity.this, response.getList().getSmsCodes(), response.getList().getSmsCommand());
                    } else {
                        showToast(response.getDesc());
                    }
                } else {
                    showToast(response.getDesc());
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                hideLoadingDialog();
                showToast(R.string.e601_error_but_undefined);
            }
        }, channel, cmd, id);
    }

    private void showDialogBuyPackage(String cmd, String id) {
        Dialog dialog = new Dialog(this, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_buy_package);
        AppCompatTextView tvTitle, tvContent, btn1, btn2;
        Button btnCancel;
        tvTitle = dialog.findViewById(R.id.txtTitle);
        tvContent = dialog.findViewById(R.id.tvMessage);
        btnCancel = dialog.findViewById(R.id.btnLeft);
        btn1 = dialog.findViewById(R.id.tvShortCut1);
        btn2 = dialog.findViewById(R.id.tvShortCut2);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBuyMochaVideo("deeplink", cmd, id);
                dialog.dismiss();
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBuyMochaVideo("", "spoint_pm", id);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}

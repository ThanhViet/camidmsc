package com.metfone.selfcare.activity.update_info;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.aigestudio.wheelpicker.WheelPicker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.update_info.listener.EditButtonListener;
import com.metfone.selfcare.activity.update_info.listener.IOCRDetect;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.databinding.ActivityConfirmIdentityInfoBinding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.account.VerifyInfo;
import com.metfone.selfcare.module.metfoneplus.dialog.MPConfirmUpdateInformationBothDialog;
import com.metfone.selfcare.network.metfoneplus.response.WsDetectORCResponse;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.MappingUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ConfirmIdentityInfoActivity extends BaseSlidingFragmentActivity implements
        IOCRDetect, EditButtonListener {

    private ActivityConfirmIdentityInfoBinding binding;
    private UserIdentityInfo userIdentityInfo;

    private IDPictureFragment idPictureFragment;
    private InformationFragment informationFragment;
    private AddressFragment addressFragment;
    private List<BaseFragment> fragments = new ArrayList<>();
    private List<String> typesID, valueType, valueTypeBCCS;
    private HashMap<String, Integer> mapIDType = new HashMap<>();
    private boolean mIsScan = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityConfirmIdentityInfoBinding.inflate(getLayoutInflater());
        changeStatusBar(Color.parseColor("#1F1F1F"));
        setContentView(binding.getRoot());

        userIdentityInfo = (UserIdentityInfo) getIntent().getSerializableExtra(EnumUtils.OBJECT_KEY);
        initFragmentViewPager();
        UpdateInfoPagerAdapter sectionsPagerAdapter = new UpdateInfoPagerAdapter(this,
                getSupportFragmentManager(), fragments);
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);
        setInitData();

        setAction();
    }

    private void initFragmentViewPager() {
        idPictureFragment = new IDPictureFragment();
        informationFragment = InformationFragment.newInstance(userIdentityInfo);
        addressFragment = AddressFragment.newInstance(userIdentityInfo);
        fragments.add(idPictureFragment);
        fragments.add(informationFragment);
        fragments.add(addressFragment);
    }

    private void setInitData() {
        if (userIdentityInfo != null) {
            String idType = userIdentityInfo.getIdType();
            valueType = Arrays.asList(getResources().getStringArray(R.array.selected_id_option));
            typesID = Arrays.asList(getResources().getStringArray(R.array.selected_id_type_option));
            valueTypeBCCS = Arrays.asList(getResources().getStringArray(R.array.selected_id_option_bccs));
            if (!TextUtils.isEmpty(idType)) {
                binding.edtIDType.setText(MappingUtils.getIDValue(this, Integer.parseInt(idType)));
            } else {
                binding.edtIDType.setText(MappingUtils.getIDValue(this, 0));
            }
        }
    }

    private void setAction() {
        binding.edtIDType.setOnClickListener(v -> {
            showBottomSheetDialogFragment();
        });
        binding.btnBack.setOnClickListener(v -> {
            onBackPressed();
        });
        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.view.setBackgroundColor(Color.TRANSPARENT);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        disableTabClick();
    }

    private void disableTabClick() {
        LinearLayout tabStrips = (LinearLayout) binding.tabs.getChildAt(0);
        for(int i = 0; i < tabStrips.getChildCount(); i++) {
            tabStrips.getChildAt(i).setOnTouchListener((v, event) -> true);
        }
    }

    private boolean validateInformation() {
        if (!idPictureFragment.checkPicture()) {
            return false;
        }

        if (!informationFragment.validateInfo()) {
            return false;
        }

        if (!addressFragment.validateInfo()) {
            return false;
        }

        return true;
    }

    private void updateInformation() {
        MPConfirmUpdateInformationBothDialog confirmInformationDialog = new MPConfirmUpdateInformationBothDialog(this);
        confirmInformationDialog.setUserInfo(combineUser());
        confirmInformationDialog.setUserIdentityInfo(combineIdentity());
        confirmInformationDialog.setVerify(passDataVerify());
        confirmInformationDialog.show();
        confirmInformationDialog.setOnDismissListener(dialogInterface -> {
            //no-op
        });
    }

    private UserInfo combineUser() {
        UserInfo info = informationFragment.informationUser();
        UserInfo address = addressFragment.informationUser();
        info.setAddress(address.getAddress());
        info.setProvince(address.getProvince());
        info.setDistrict(address.getDistrict());
        return info;
    }

    private UserIdentityInfo combineIdentity() {
        String idType = binding.edtIDType.getText().toString().trim();
        String metFone = SharedPrefs.getInstance().get(Constants.MF_PHONE,  String.class);
        UserIdentityInfo identityInfo = informationFragment.informationIdentity();
        UserIdentityInfo address = addressFragment.informationIdentity();
        identityInfo.setAddress(address.getAddress());
        identityInfo.setProvince(address.getProvince());
        identityInfo.setDistrict(address.getDistrict());
        identityInfo.setStreetName(address.getStreetName());
        identityInfo.setHome(address.getHome());
        identityInfo.setSubName(address.getSubName());
        identityInfo.setSubDateBirth(address.getSubDateBirth());
        identityInfo.setSubGender(address.getSubGender());
        identityInfo.setRelationship(address.getRelationship());
        identityInfo.setIsScan(String.valueOf(mIsScan));
        identityInfo.setIdType(MappingUtils.getIDType(this, idType));
        identityInfo.setPhone(metFone);

        return identityInfo;
    }

    private VerifyInfo passDataVerify() {
        String type = binding.edtIDType.getText().toString();
        String idType = MappingUtils.getIDTypeForCam(this, type);
        return idPictureFragment.getData(idType);
    }

    private void showBottomSheetDialogFragment() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this, R.style.NewBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.fragment_bottom_sheet_wheel_picker);
        //
        WheelPicker wheelPicker = bottomSheetDialog.findViewById(R.id.wheelPicker);
        TextView tvDone = bottomSheetDialog.findViewById(R.id.tvDone);
        if (wheelPicker != null) {
            wheelPicker.setData(valueTypeBCCS);
            wheelPicker.setCyclic(false);
            wheelPicker.setSelectedItemPosition(valueTypeBCCS.indexOf(binding.edtIDType.getText().toString()), false);
        }
        if (tvDone != null) {
            tvDone.setOnClickListener(view -> {
                if (wheelPicker != null) {
                    binding.edtIDType.setText(valueTypeBCCS.get(wheelPicker.getCurrentItemPosition()));
                    informationFragment.updateNationality(getTypeID());
                }
                bottomSheetDialog.dismiss();
            });
        }
        //
        bottomSheetDialog.setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            CardView bottomSheet = (CardView) d.findViewById(R.id.cvContent);
            if (bottomSheet == null)
                return;
            bottomSheet.setBackground(null);
        });
        bottomSheetDialog.show();
        //
    }

    public String getTypeID() {
        return binding.edtIDType.getText().toString();
    }

    public void handleUpdate() {
        if (validateInformation()) {
            updateInformation();
        }
    }

    public void handleNext(int position) {
        if (position < binding.viewPager.getChildCount() - 1) {
            binding.viewPager.setCurrentItem(position + 1, true);
        } else {
            handleUpdate();
        }
    }

    public void handleBack(int position) {
        if (position > 0) {
            binding.viewPager.setCurrentItem(position - 1, true);
        } else {
            onBackPressed();
        }
    }

    public boolean getIsScan() {
        return mIsScan;
    }

    public void setScan(boolean isScan) {
        mIsScan = isScan;
    }

    @Override
    public void onSuccess(WsDetectORCResponse.Response response) {
        mIsScan = true;
        informationFragment.updateInformationOCR(
                response.getName(),
                response.getIdNumber(),
                response.getDobEn(),
                response.getSexEn(),
                response.getExpireDate(),
                response.getNationality());
        addressFragment.updateInformationOCR(response.getProvinceEn());
    }

    @Override
    public void onEditData() {
        informationFragment.updateButtonEdit();
        addressFragment.updateButtonEdit();
    }

}

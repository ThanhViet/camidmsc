package com.metfone.selfcare.activity;

import static com.metfone.selfcare.fragment.history.HistoryFragment.isHistoryScreen;
import static com.metfone.selfcare.helper.Constants.NOTIFICATION.NOTIFY_NEWS;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_FIREBASE_REFRESHED_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_SEND_TOKEN_SUCCESS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.blankj.utilcode.util.StringUtils;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.metfone.esport.entity.event.OnBackDeviceEvent;
import com.metfone.esport.entity.event.UpdateTokenEvent;
import com.metfone.esport.ui.live.LiveVideoFragment;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.FavoriteBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.MochaAdsClient;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.fragment.HomePagerFragment;
import com.metfone.selfcare.fragment.call.KeyBoardDialog;
import com.metfone.selfcare.fragment.home.WapHomeFragment;
import com.metfone.selfcare.fragment.home.tabmobile.TabMobileFragment;
import com.metfone.selfcare.fragment.onmedia.OnMediaHotFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.helper.workmanager.SettingWorkManager;
import com.metfone.selfcare.listeners.DownloadDriveResponse;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.listeners.StrangerListInterface;
import com.metfone.selfcare.listeners.ThreadListInterface;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.backup_restore.RestoreModel;
import com.metfone.selfcare.module.backup_restore.backup.BackupManager;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.fragment.game.GameFragment;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationReadResponse;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;
import com.metfone.selfcare.module.keeng.fragment.home.MusicHomeFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPDetailsFragment;
import com.metfone.selfcare.module.metfoneplus.fragment.MPTabMobileLoginSignUpFragment;
import com.metfone.selfcare.module.movie.event.TabMovieReselectedEvent;
import com.metfone.selfcare.module.movienew.activity.GenresActivity;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.module.tiin.base.event.TabTiinEvent;
import com.metfone.selfcare.module.video.event.TabVideoReselectedEvent;
import com.metfone.selfcare.network.fileTransfer.DownloadFileDriveAsyncTask;
import com.metfone.selfcare.network.fileTransfer.GetMultiFileAsyncTask;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.PermissionDialog;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.IOnBackPressed;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.contactintergation.DeviceAccountManager;
import com.metfone.selfcare.v5.home.fragment.TabNewsFragmentV2;
import com.vtm.adslib.AdsListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import bolts.AppLinks;
import lombok.val;
import retrofit2.Response;

public class HomeActivity extends BaseSlidingFragmentActivity implements OnMediaInterfaceListener
        , InitDataListener, FragmentTabHomeKh.GameIconHome {
    private static final String TAG = HomeActivity.class.getSimpleName();
    IOnBackPressed iOnBackPressed;
    long startTime = 0;
    FavoriteBusiness favoriteBusiness; // Dung de gui category,country len server
    String colorTabWap;
    private ApplicationController mApplication;
    private MessageBusiness mMessageBusiness;
    private FeedBusiness mFeedBusiness;
    //    private ReengViewPager mMainPager;
//    private HomeMainAdapter adapterViewPager;
    private int currentTabPosition, currentSubTabMobile = 0, currentSubTabVideo = 0;
    private boolean checkTabMobile = false;
    private String idTabWapFromIntent;
    private TabHomeHelper.HomeTab currentTab = TabHomeHelper.HomeTab.tab_chat;
    private SharedPreferences mPref;
    // callback
    private ThreadListInterface mThreadListHandler;
    private List<StrangerListInterface> mStrangerListHandler;
    // view from home fragment
    private ViewPager mHomePager;
    private BottomNavigationBar navigationBar;
    private boolean isOpenFromOtherApp;
    private boolean isUpdatingToken = false;
    /**
     * thực hiện lấy config whitelist
     */

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private boolean showingCustomViewWap = false;
    private UserInfoBusiness userInfoBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userInfoBusiness = new UserInfoBusiness(this);
        actionHome();
        favoriteBusiness = new FavoriteBusiness(this);
        favoriteBusiness.checkAndSend();

        AppLinkData.fetchDeferredAppLinkData(this, getString(R.string.facebook_app_id),
                appLinkData -> {
                }
        );

    }

    @Override
    public void onResume() {
        super.onResume();
        updateToken();
    }

    private void actionHome() {
        long beginTime = System.currentTimeMillis();
        checkFirstOpen();
//        changeStatusBar(false);
        //Kiem tra neu vao tu shortcut cua GG Play thi finish
        if (!isTaskRoot()) {
            Log.i(TAG, " onCreate isTaskRoot: false");
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) &&
                    intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                Log.d(TAG, "Main Activity is not the root.  Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        } else {
            Log.d(TAG, " onCreate isTaskRoot: true");
        }
        initBusiness();
        ListenerHelper.getInstance().addInitDataListener(this);

//        // set SystemUI & hide navigation
//        Utilities.setNavigationDevice(this, R.color.black);
//        Utilities.setNavigationBarTransparent(this);

        setContentView(R.layout.activity_home_new);
        checkDataAndDisplayWhenStart(getIntent());
        if (mApplication.getReengAccountBusiness() != null
                && !mApplication.getReengAccountBusiness().isAnonymousLogin()
                && mApplication.getReengAccountBusiness().isValidAccount()) {
            String dataBackup = mApplication.getPref().getString(Constants.PREFERENCE.PREF_HAS_BACKUP, "");
            if (!TextUtils.isEmpty(dataBackup)) {
                try {
                    Gson gson = new Gson();
                    RestoreModel restoreModel = gson.fromJson(dataBackup, RestoreModel.class);
                    if (restoreModel != null && restoreModel.getLstFile() != null && !restoreModel.getLstFile().isEmpty()) {
                        int lastIndex = 0;
                        RestoreModel.FileInfo fileInfo = restoreModel.getLstFile().get(lastIndex);
                        displayRestoreInfo(fileInfo.getCapacity(), fileInfo.getPath(), fileInfo.getCreated_date(), fileInfo.getId());
                    }
                } catch (Exception e) {
                }
            }
        }

        Log.d(TAG, "[perform] onCreateView: " + (System.currentTimeMillis() - beginTime));

        if (PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_CONTACTS) == PermissionHelper.PERMISSION_GRANTED) {
            DeviceAccountManager.getInstance(mApplication).addAccountToDevice();
        }

        startTime = System.currentTimeMillis();
        Log.i("ADS", "Time start init ads: " + (System.currentTimeMillis() - startTime) + "|" + System.currentTimeMillis());
        initAds();

        MetfonePlusClient.LANGUAGE = LocaleManager.getLanguage(this);
    }

    private void checkFirstOpen() {
        executeFragmentTransaction(HomePagerFragment.newInstance(), R.id.fragment_container, false, false);
        new Handler().postDelayed(this::getConfigWhitelist, 0);
        BackupManager.autoBackupMessage(this);
    }

    private void runUi(Object... objects) {
        String s = (String) objects[0];
        switch (s) {
            case "skip":
                executeFragmentTransaction(HomePagerFragment.newInstance(), R.id.fragment_container, false, false);
                new Handler().postDelayed(this::getConfigWhitelist, 0);
                BackupManager.autoBackupMessage(this);
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        SettingWorkManager.startWorkOnTimeNotiSetting(SettingBusiness.getInstance(mApplication).getTimeToStartSettingNoti());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /*1 vài máy huawei dở hơi ko có activity nên crash*/

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i(TAG, "------------onNewIntent: " + intent);
        if (((intent.getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) !=
                Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) || Intent.ACTION_SEND.equals(intent.getAction())) {
            initBusiness();
            checkDataAndDisplayWhenStart(intent);
        } else {
            handleNewIntent(intent);
        }
    }

    @Override
    protected void onDestroy() {
        SettingWorkManager.cancelSettingNotiWork();
        Log.i(TAG, "onDestroy");
        try {
            //Kill app thi tat music
            if (mApplication.isReady())
                mApplication.cancelNotification(Constants.NOTIFICATION.MUSIC_NOTIFICATION_ID);
//            if (mApplication.getPlayMusicController() != null && !mApplication.getPlayMusicController().isRunningKeengPlayerActivity()) {
            if (mApplication.getPlayMusicController() != null) {
                mApplication.getPlayMusicController().closeMusic();
            }
        } catch (Exception ex) {
            if (mApplication != null && ex.getMessage() != null)
                mApplication.logDebugContent(ex.getMessage());
        }

        super.onDestroy();
        ListenerHelper.getInstance().removeInitDataListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBackPressed() {
        if (isHistoryScreen) {
            super.onBackPressed();
        } else {
            if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
                ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
            }
            if (MPDetailsFragment.self() != null && MPDetailsFragment.self().isVisible() && MPDetailsFragment.self().isGuideline()) {
                return;
            }
            android.util.Log.e(TAG, "onBackPressed: isProcessing = " + ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone());
            if (HomePagerFragment.self() != null && HomePagerFragment.self().isSelectMode()) {
                //deselect
                EventBus.getDefault().post(new TabMobileFragment.BackPressEvent(false, true));
                return;
            }

            if (!SharedPrefs.getInstance().get(Constants.PREFERENCE.BACK_DEVICE, Boolean.class)) {
                if (currentTabPosition == 0) {
                    EventBus.getDefault().post(new TabMobileFragment.BackPressEvent(true));

                    boolean isAdShowFinish = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_FULL_SHOW_FINISH) == 1;
                    if (isAdShowFinish) {
                        AdsManager.getInstance().showAdsFullScreen(new AdsListener() {
                            @Override
                            public void onAdClosed() {
                                gotoLauncher();
                            }

                            @Override
                            public void onAdShow() {
                                ApplicationController.self().getPref().edit().putBoolean(AdsUtils.KEY_FIREBASE.AD_DISPLAY_FIRST, true).apply();
                                ApplicationController.self().getPref().edit().putLong(AdsUtils.KEY_FIREBASE.AD_DISPLAY_TIME, System.currentTimeMillis()).apply();
                            }
                        });
                    } else {
                        if (iOnBackPressed != null) {
                            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                            if (f != null && f instanceof LiveVideoFragment) {
                                popFragment();
                                return;
                            }
                            boolean stock = iOnBackPressed.onBackPressed();
                            if (!stock) {
                                gotoLauncher();
                            }
                        } else {
                            finish();
                        }
                    }
                } else if (currentTab == TabHomeHelper.HomeTab.tab_metfone) {
                    List<Fragment> fragments = getSupportFragmentManager().getFragments();
                    for (Fragment fragment : fragments) {
                        if (fragment instanceof HomePagerFragment) {
                            ((HomePagerFragment) fragment).popBackStackFragment();
                            break;
                        }
                    }
                    
                }
                /*else if (currentTab == TabHomeHelper.HomeTab.tab_esport) {
                    List<Fragment> fragments = getSupportFragmentManager().getFragments();
                    for (Fragment fragment : fragments) {
                        Fragment f = fragment;
                        if (f instanceof HomePagerFragment) {
                            ((HomePagerFragment) f).popBackStackFragment();
                            ((HomePagerFragment) f).selectTabDefault();
                        }
                    }
                }*/
                else if (currentTabPosition == 2){
//            ((HomePagerFragment) f).popBackStackMoviePagerFragment();
                    if (iOnBackPressed != null) {
                        boolean stock = iOnBackPressed.onBackPressed();
                    }
                } else {
                    if (WapHomeFragment.self() != null && showingCustomViewWap) {
                        boolean backWapCustomWap = WapHomeFragment.self().hideCustomView();
                        if (backWapCustomWap) return;
                    }
                    iOnBackPressed.onBackPressed();
                    setCurrentTab(TabHomeHelper.HomeTab.tab_home, null, -1);
                    EventBus.getDefault().post(new TabMobileFragment.BackPressEvent(false));
                }
            } else {
                SharedPrefs.getInstance().put(Constants.PREFERENCE.BACK_DEVICE, false);
            }
            EventBus.getDefault().post(new OnBackDeviceEvent());
        }
    }

    public void setIOnBackPressed(IOnBackPressed iOnBackPressed) {
        this.iOnBackPressed = iOnBackPressed;
    }

    public void gotoLauncher() {
        try {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        } catch (Exception ex) {
            finish();
        }
    }

    private void getDynamicLinks(Intent intent) {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(pendingDynamicLinkData -> {
                    if (pendingDynamicLinkData == null) return;
                    Uri linkURL = pendingDynamicLinkData.getLink();
                    if(linkURL == null) return;
                    android.util.Log.e(TAG, "onSuccess: " + linkURL);
                    DeepLinkHelper.getInstance().openSchemaLink(
                            HomeActivity.this, linkURL, null, null);
                }).addOnFailureListener(e -> android.util.Log.e(TAG, "onFailure: ", e));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
        setActivityForResult(false);
        super.onActivityResult(requestCode, resultCode, data);

        if (MPTabMobileLoginSignUpFragment.get() != null) {
            MPTabMobileLoginSignUpFragment.get().onActivityResult(requestCode, resultCode, data);
            return;
        }

        if (GameFragment.isWaitingForResult && GameFragment.get() != null) {
            GameFragment.get().onActivityResult(requestCode, resultCode, data);
            return;
        }
        if (requestCode == Constants.ACTION.ADD_CONTACT) {
            ContentObserverBusiness.getInstance(this).setAction(-1);
            setTakePhotoAndCrop(false);
        }
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_SOLO:
                    String number = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
                    ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread(number);
                    NavigateActivityHelper.navigateToChatDetail(HomeActivity.this, thread);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(HomeActivity.this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    break;
                case Constants.ACTION.STRANGER_MUSIC_SELECT_SONG:
                    MediaModel mediaModel = (MediaModel) data.getSerializableExtra(Constants.KEENG_MUSIC.SONG_OBJECT);
                    // goto select tab music
                    Fragment fragment = getSupportFragmentManager().getFragments().get(0);
                    if (fragment != null && fragment instanceof HomePagerFragment) {
                        ((HomePagerFragment) fragment).selectTabOfStranger(0);
                    }
                    handlerPostStrangerMusic(mediaModel, false);
                    break;

                case Constants.ACTION.ACTION_WRITE_STATUS:
                    boolean isEdit = data.getBooleanExtra(OnMediaActivityNew.EDIT_SUCCESS, false);
                    boolean isPost = data.getBooleanExtra(OnMediaActivityNew.POST_SUCCESS, false);
                    if (isEdit) {
                        mFeedBusiness.notifyNewFeed(true, false);
                    } else if (isPost) {
                        mFeedBusiness.notifyNewFeed(true, true);
                    } else {
                        //TODO vao day thi cu notify roi cho len dau, unknown case
                        mFeedBusiness.notifyNewFeed(true, true);
                    }
                    break;
                case Constants.ACTION.ADD_CONTACT:
                    ContentObserverBusiness.getInstance(this).setAction(-1);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    Log.d(TAG, "TYPE_CREATE_CHAT");
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(HomeActivity.this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    break;
                default:
                    break;
            }
        }

        // start activity for result from MPSelectPhoneActivity
        if (requestCode == Constants.ACTION.ACTION_SELECT_METFONE_NUMBER && resultCode == RESULT_OK) {
            ApplicationController.self().getCamIdUserBusiness().setIsSignInUpSuccess(true);
            Fragment f = getSupportFragmentManager().getFragments().get(0);
            if (f instanceof HomePagerFragment) {
                ((HomePagerFragment) f).selectMetfoneTab();
            }
        }

        // start activity for result from MPAddPhoneActivity
        if (requestCode == Constants.ACTION.ACTION_ADD_METFONE_NUMBER && resultCode == RESULT_OK) {
            ApplicationController.self().getCamIdUserBusiness().setIsSignInUpSuccess(true);
            Fragment f = getSupportFragmentManager().getFragments().get(0);
            if (f instanceof HomePagerFragment) {
                ((HomePagerFragment) f).selectMetfoneTab();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: " + requestCode);
        dismissDialogFragment(PermissionDialog.TAG);
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_ALL) {
            if (PermissionHelper.allowedPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                mApplication.initFolder();
            }
            if (PermissionHelper.allowedPermission(this, Manifest.permission.WRITE_CONTACTS) && PermissionHelper.allowedPermission(this, Manifest.permission.READ_CONTACTS)) {
                mApplication.reLoadContactAfterPermissionsResult();
            }
//            if (PermissionHelper.allowedPermission(this, Manifest.permission.READ_SMS)) {
//                mApplication.registerSmsObserver();
//            }
        } else if (requestCode == Constants.PERMISSION.PERMISSION_CONTACT) { // request only contact
            if (PermissionHelper.allowedPermission(this, Manifest.permission.WRITE_CONTACTS)
                    && PermissionHelper.allowedPermission(this, Manifest.permission.READ_CONTACTS)) {
                mApplication.reLoadContactAfterPermissionsResult();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onDataReady() {
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                changeConfigDiscover();
            }
        });*/
    }

    public void initHomePagerAndActionBarView(ViewPager pager, BottomNavigationBar navigationBar) {
        this.mHomePager = pager;
        this.navigationBar = navigationBar;
    }

    public int getCurrentTabPosition() {
        return currentTabPosition;
    }

    public void setCurrentTabPosition(int currentTabPosition) {
        this.currentTabPosition = currentTabPosition;
    }

    public int getCurrentSubTabMobile() {
        return currentSubTabMobile;
    }

    public void setCurrentSubTabMobile(int currentSubTabMobile) {
        this.currentSubTabMobile = currentSubTabMobile;
    }

    public boolean getCheckTabMobile() {
        return checkTabMobile;
    }

    public void setCheckTabMobile(boolean checkTabMobile) {
        this.checkTabMobile = checkTabMobile;
    }

    public int getCurrentSubTabVideo() {
        return currentSubTabVideo;
    }

    public void setCurrentSubTabVideo(int currentSubTabVideo) {
        this.currentSubTabVideo = currentSubTabVideo;
    }

    public void addStrangeListInterface(StrangerListInterface mHandler) {
        if (mStrangerListHandler == null) {
            mStrangerListHandler = new ArrayList<>();
        }
        if (mHandler != null) {
            mStrangerListHandler.add(mHandler);
        }
    }

    public void removeStrangeListInterface(StrangerListInterface mHandler) {
        if (mStrangerListHandler != null) {
            mStrangerListHandler.remove(mHandler);
        }
    }

    public ThreadListInterface getThreadListHandler() {
        return mThreadListHandler;
    }

    public void setThreadListHandler(ThreadListInterface mThreadListHandler) {
        this.mThreadListHandler = mThreadListHandler;
    }

    private void initBusiness() {
        mApplication = (ApplicationController) getApplicationContext();
        mMessageBusiness = mApplication.getMessageBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
    }

    public TabHomeHelper.HomeTab getCurrentTab() {
        return currentTab;
    }

    public String getCurrentColorTabWap() {
        return colorTabWap;
    }

    public void setColorTabWap(String colorTabWap) {
        this.colorTabWap = colorTabWap;
    }

    public void gotoMetfoneTab() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (Fragment fragment: getSupportFragmentManager().getFragments()){
                    if (fragment instanceof HomePagerFragment) {
                        ((HomePagerFragment) fragment).gotoMetfoneTab();
                        return;
                    }
                }
            }
        }, 1000);

    }

    public void setCurrentTab(TabHomeHelper.HomeTab tab, String idTabWap, int indexSubTabVideo) {
        currentTab = tab;
//        currentTabPosition = TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(tab, idTabWap);
        currentTabPosition = TabHomeHelper.getInstant(mApplication).findHomeTab();
        currentSubTabVideo = indexSubTabVideo;
        setCurrentTab();
        /*if (indexSubTabVideo >= 0)
            mHomePager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new TabVideoFragmentV2.TabVideoEvent(indexSubTabVideo));
                }
            }, 300);*/
    }

    private void setCurrentTab() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mHomePager != null) {
                    mHomePager.setCurrentItem(currentTabPosition);
                    showHomeTab(currentTabPosition);
                    if (currentSubTabMobile > 0) {
                        EventBus.getDefault().post(new TabMobileFragment.SetSubTabMobileEvent(currentSubTabMobile));
                    } else if (currentSubTabVideo > 0) {
                        EventBus.getDefault().post(new TabVideoReselectedEvent().setReselectedHome(true).setIndexTabSelected(currentSubTabVideo));
                    }
                }
            }
        });
    }

    private void handlerPostStrangerMusic(MediaModel songModel, final boolean isNavigateToStrangerTab) {
        showLoadingDialog(null, getString(R.string.waiting));
        MusicBusiness.onCreateStrangerRoomListener listener = new MusicBusiness.onCreateStrangerRoomListener() {
            @Override
            public void onResponse(ArrayList<StrangerMusic> strangerMusics, boolean isConfide) {
                hideLoadingDialog();
                mApplication.getMusicBusiness().setSelectedConfide(false);
                notifyOnStrangeChange(strangerMusics, false);
                if (isNavigateToStrangerTab) {
                    currentTabPosition = TabHomeHelper.getInstant(mApplication).findPositionFromDeepLink(TabHomeHelper.HomeTab.tab_stranger, null);
                    currentTab = TabHomeHelper.HomeTab.tab_stranger;
                    setCurrentTab();
                }
            }

            @Override
            public void onError(int errorCode) {
                hideLoadingDialog();
                String msgError = mApplication.getMusicBusiness().getMessageErrorByErrorCode(errorCode, null, false);
                showToast(msgError, Toast.LENGTH_LONG);
            }
        };
        mApplication.getMusicBusiness().createRoomStrangerMusic(songModel, listener);
    }

    public void notifyOnStrangeChange(ArrayList<StrangerMusic> strangers, boolean isConfide) {
        if (mStrangerListHandler != null) {
            for (StrangerListInterface listInterface : mStrangerListHandler) {
                listInterface.onChangeStrangerList(strangers, isConfide);
            }
        }
    }

    private void notifyPageSelected(boolean isVis, int prePos) {
        if (mStrangerListHandler != null) {
            for (StrangerListInterface listInterface : mStrangerListHandler) {
                listInterface.onPageStateVisible(isVis, prePos);
            }
        }
    }

    private void notifyTabReselected() {
        if (mStrangerListHandler != null) {
            for (StrangerListInterface listInterface : mStrangerListHandler) {
                listInterface.onTabReselected();
            }
        }
    }

    /*public void onTabCallHistorySelected(int prePos) {
        if (mThreadListHandler != null) mThreadListHandler.onPageStateVisible(false, prePos);
        if (mStrangerListHandler != null) mStrangerListHandler.onPageStateVisible(false, prePos);
    }*/

    public void setTabSelected(int position) {
        Log.d(TAG, "setTabSelected currentPos: " + currentTabPosition + " ---- position: " + position);
//        TabHomeHelper.HomeTab tab = TabHomeHelper.getInstant(mApplication).findTabFromPosition(position);
        TabHomeHelper.HomeTab tab = TabHomeHelper.getInstant(mApplication).findTabFromPositionKHListActiveItem(position);
        switch (tab) {
            case tab_chat:
                onTabChatSelected(currentTabPosition);
                setCheckTabMobile(true);
                break;
            /*case tab_call:
                onTabCallHistorySelected(currentTabPosition);
                break;*/
            case tab_video:
                onTabVideoSelected(currentTabPosition);
                setCheckTabMobile(true);
                break;
            case tab_stranger:
                onTabConnectSelected(currentTabPosition);
                setCheckTabMobile(true);
                break;
            case tab_hot:
                onTabHotSelected(currentTabPosition);
                setCheckTabMobile(true);
                break;
            case tab_more:
                onTabMoreSelected(currentTabPosition);
                setCheckTabMobile(true);
                break;
            default:
                break;
        }
        currentTab = tab;
        currentTabPosition = position;
//        if(currentTabPosition == 4 && DynamicSharePref.getInstance().get(PREF_FIRST_TIME_OPEN_TAB_METFONE, Boolean.class, true)){
//            EventBus.getDefault().postSticky(new MPExchangeGuideLineFragment());
//        }
        if (position == 2) {
            if (!DynamicSharePref.getInstance().get(Constants.KEY_IS_FIRST_TAB_CINEMA, Boolean.class)) {
                DynamicSharePref.getInstance().put(Constants.KEY_IS_FIRST_TAB_CINEMA, true);
                Intent intent = new Intent(this, GenresActivity.class);
                startActivity(intent);
            }
        }
        android.util.Log.e(TAG, "setTabSelected: " + tab.name() + " - position = " + currentTabPosition);
    }

    public void onTabChatSelected(int prePos) {
        if (mThreadListHandler != null) mThreadListHandler.onPageStateVisible(true, prePos);
        if (mStrangerListHandler != null) notifyPageSelected(false, prePos);
    }

    public void onTabConnectSelected(int prePos) {
        if (mThreadListHandler != null) mThreadListHandler.onPageStateVisible(false, prePos);
        if (mStrangerListHandler != null) notifyPageSelected(true, prePos);
    }

    public void onTabVideoSelected(int prePos) {
        if (mThreadListHandler != null) mThreadListHandler.onPageStateVisible(false, prePos);
        if (mStrangerListHandler != null) notifyPageSelected(false, prePos);
    }

    public void onTabHotSelected(int prePos) {
        if (mThreadListHandler != null) mThreadListHandler.onPageStateVisible(false, prePos);
        if (mStrangerListHandler != null) notifyPageSelected(false, prePos);
    }

    public void onTabMoreSelected(int prePos) {
        if (mThreadListHandler != null) mThreadListHandler.onPageStateVisible(false, prePos);
        if (mStrangerListHandler != null) notifyPageSelected(false, prePos);
    }

    public void onTabReselectedListener(int position) {
        TabHomeHelper.HomeTab tab = TabHomeHelper.getInstant(mApplication).findTabFromPosition(position);
        switch (tab) {
            case tab_chat:
                /**if (mThreadListHandler != null)
                 mThreadListHandler.onTabReselected();*/
                if (currentTabPosition == 1) {
                    if (currentSubTabMobile == 0) {
                        if (mThreadListHandler != null)
                            mThreadListHandler.onTabReselected();
                    } else {
                        EventBus.getDefault().post(new TabMobileFragment.SetSubTabMobileEvent(0));
                    }
                }

                break;
            case tab_video:
                EventBus.getDefault().post(new TabVideoReselectedEvent().setReselectedHome(true));
                break;
            case tab_stranger:
                if (mStrangerListHandler != null) notifyTabReselected();
                break;
            case tab_hot:
                EventBus.getDefault().post(new OnMediaHotFragment.OnMediaTabHotEvent(true));
                break;
            case tab_movie:
                EventBus.getDefault().post(new TabMovieReselectedEvent().setReselectedHome(true));
                break;
            case tab_music:
                EventBus.getDefault().post(new MusicHomeFragment.TabMusicEvent(true));
                break;
            case tab_news:
                EventBus.getDefault().post(new TabNewsFragmentV2.TabNewsEvent(true));
                break;
            case tab_home:
                EventBus.getDefault().post(new TabHomeEvent().setReselected(true));
                break;
            case tab_tiins:
                EventBus.getDefault().post(new TabTiinEvent().setReSelect(true));
                break;
            default:
                break;
        }
    }

    public BottomNavigationBar getNavigationBar() {
        return navigationBar;

    }

    @Override
    public void navigateToWebView(FeedModelOnMedia feed) {
        NavigateActivityHelper.navigateToWebView(this, feed, false);
    }

    @Override
    public void navigateToVideoView(FeedModelOnMedia feed) {
//        NavigateActivityHelper.navigateToAutoPlayVideo(HomeActivity.this, feed);
        Video video = Video.convertFeedContentToVideo(feed.getFeedContent());
        if (video == null) return;
        mApplication.getApplicationComponent().providesUtils().openVideoDetail(this, video);
    }

    @Override
    public void navigateToMovieView(FeedModelOnMedia feed) {
        if (feed != null && feed.getFeedContent() != null) {
            Movie movie = FeedContent.convert2Movie(feed.getFeedContent());
            if (movie != null)
                playMovies(movie);
            else
                Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
        }
    }

    @Override
    public void navigateToProcessLink(FeedModelOnMedia feed) {
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToComment(FeedModelOnMedia feed) {
        boolean showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(feed);
        NavigateActivityHelper.navigateToOnMediaDetail(HomeActivity.this, feed.getFeedContent().getUrl(),
                Constants.ONMEDIA.COMMENT, Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM_TAB_HOT, showMenuCopy);
    }

    @Override
    public void navigateToAlbum(FeedModelOnMedia feed) {
//        mFeedBusiness.setFeedPlayList(feed);
        /*NavigateActivityHelper.navigateToOnMediaDetail(HomeActivity.this, feed.getFeedContent().getUrl(),
                Constants.ONMEDIA.ALBUM_DETAIL, -1, false);*/
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToListShare(String url) {
        Log.i(TAG, "navigateToListShare: " + url);
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, url, false);
    }

    private void checkDataAndDisplayWhenStart(Intent intent) {
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (intent.getBooleanExtra("lesdothis", false)) {
                NavigateActivityHelper.navigateToRegisterScreenActivity(HomeActivity.this, true);
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, true);
                FireBaseHelper.getInstance(mApplication).checkServiceAndRegister(false);
            } else if (!StringUtils.isEmpty(DeepLinkHelper.uriDeepLink) && UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT && (bundle != null && StringUtils.isEmpty(bundle.getString("notificationType")))) {
                Uri uri = Uri.parse(DeepLinkHelper.uriDeepLink);
                DeepLinkHelper.uriDeepLink = null;
                handleDeepLinkAfterLogin(uri);
            } else {
                processDataFromIntent(intent);
            }
        }
    }

    private void handleDeepLinkAfterLogin(Uri uri) {
        String phoneService = ApplicationController.self().getCamIdUserBusiness().getPhoneService();
        if (DeepLinkHelper.needLoginToMetfone(this, uri) && !StringUtils.isEmpty(phoneService)) {
            showLoadingDialog("", "");
            MetfonePlusClient.getInstance().autoLogin(phoneService, new MPApiCallback<LoginResponse>() {
                @Override
                public void onResponse(Response<LoginResponse> response) {
                    hideLoadingDialog();
                    if (response.body() != null && response.body().getUsername() != null) {
                        DeepLinkHelper.getInstance().openSchemaLink(HomeActivity.this, uri, "", null);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    hideLoadingDialog();
                }
            });
        } else {
            DeepLinkHelper.getInstance().openSchemaLink(HomeActivity.this, uri, "", null);
        }
    }

    private void processPermission() {
        final ArrayList<String> array = new ArrayList<>();
        boolean needShowPermissionDialog = false;
        boolean goToSettings = false;
        //contact
        int contactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_CONTACTS);
        if (contactPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            needShowPermissionDialog = true;
        }
        if (contactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.WRITE_CONTACTS);
            if (mPref != null && !mPref.getBoolean("FirstRequest_" + Manifest.permission.WRITE_CONTACTS, true)) {
                goToSettings = true;
            }
        }

        int readContactPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_CONTACTS);
        if (readContactPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            needShowPermissionDialog = true;
            if (mPref != null && !mPref.getBoolean("FirstRequest_" + Manifest.permission.READ_CONTACTS, true)) {
                goToSettings = true;
            }
        }
        if (readContactPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.READ_CONTACTS);
        }

        //storage
        int storagePermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (storagePermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
            needShowPermissionDialog = true;
            if (mPref != null && !mPref.getBoolean("FirstRequest_" + Manifest.permission.WRITE_EXTERNAL_STORAGE, true)) {
                goToSettings = true;
            }
        }
        if (storagePermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
            array.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        //TODO read sms
//            if (mApplication.getReengAccountBusiness().isSmsInEnable() && SettingBusiness.getInstance(mApplication).getPrefReplySms()) {
//                int readSmsPermissionStatus = PermissionHelper.checkPermissionStatus(this, Manifest.permission.READ_SMS);
//                if (readSmsPermissionStatus == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
//                    needShowPermissionDialog = true;
//                    if (mPref != null && !mPref.getBoolean("FirstRequest_" + Manifest.permission.READ_SMS, true)) {
//                        goToSettings = true;
//                    }
//                }
//                if (readSmsPermissionStatus != PermissionHelper.PERMISSION_GRANTED) {
//                    array.add(Manifest.permission.READ_SMS);
//                }
//
//                int readSmsPermissionStatus1 = PermissionHelper.checkPermissionStatus(this, Manifest.permission.RECEIVE_SMS);
//                if (readSmsPermissionStatus1 == PermissionHelper.PERMISSION_NOT_REQUESTED_OR_DENIED_PERMANENTLY) {
//                    needShowPermissionDialog = true;
//                    if (mPref != null && !mPref.getBoolean("FirstRequest_" + Manifest.permission.RECEIVE_SMS, true)) {
//                        goToSettings = true;
//                    }
//                }
//                if (readSmsPermissionStatus1 != PermissionHelper.PERMISSION_GRANTED) {
//                    array.add(Manifest.permission.RECEIVE_SMS);
//                }
//            }

        if (!array.isEmpty()) {
            final String[] permissions = array.toArray(new String[array.size()]);
            int permissionCode = Constants.PERMISSION.PERMISSION_REQUEST_ALL;
            if (array.size() == 1) {
                if (array.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    permissionCode = Constants.PERMISSION.PERMISSION_REQUEST_WRITE_STORAGE;
                }
//                    else if (array.contains(Manifest.permission.READ_SMS) || array.contains(Manifest.permission.RECEIVE_SMS)) {
//                        permissionCode = Constants.PERMISSION.PERMISSION_REQUEST_RECEIVE_MESSAGE;
//                    }
                else if (array.contains(Manifest.permission.READ_CONTACTS) || array.contains(Manifest.permission.WRITE_CONTACTS)) {
                    permissionCode = Constants.PERMISSION.PERMISSION_REQUEST_WRITE_CONTACT;
                }
            }

            final int permission_code = permissionCode;
            if (Config.Features.FLAG_SUPPORT_PERMISSION_GUIDELINE && needShowPermissionDialog) {
                final boolean finalGoToSettings = goToSettings;
                PermissionDialog.newInstance(permission_code, true, array, new PermissionDialog.CallBack() {
                    @Override
                    public void onPermissionAllowClick(boolean allow, int clickCount) {
                        if (allow && finalGoToSettings) {
                            //go to settings
                            dismissDialogFragment(PermissionDialog.TAG);
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            PermissionHelper.showDialogPermissionSettingIntro(HomeActivity.this, array, permission_code);
                        } else {
                            PermissionHelper.requestPermissions(HomeActivity.this, permissions, Constants.PERMISSION.PERMISSION_REQUEST_ALL);
//                                if (mPref != null) {
//                                    mPref.edit().putBoolean("FirstRequest_" + Manifest.permission.READ_SMS, false).apply();
//                                }
//                                if (mPref != null) {
//                                    mPref.edit().putBoolean("FirstRequest_" + Manifest.permission.RECEIVE_SMS, false).apply();
//                                }
                            if (mPref != null) {
                                mPref.edit().putBoolean("FirstRequest_" + Manifest.permission.READ_CONTACTS, false).apply();
                            }
                            if (mPref != null) {
                                mPref.edit().putBoolean("FirstRequest_" + Manifest.permission.WRITE_CONTACTS, false).apply();
                            }
                            if (mPref != null) {
                                mPref.edit().putBoolean("FirstRequest_" + Manifest.permission.WRITE_EXTERNAL_STORAGE, false).apply();
                            }
                        }
                    }
                }).show(getSupportFragmentManager(), PermissionDialog.TAG);
            } else {
                PermissionHelper.requestPermissions(this, permissions, Constants.PERMISSION.PERMISSION_REQUEST_ALL);
            }
        }
    }

    private void processDataFromIntent(Intent intent) {
        Log.i(TAG, "processDataFromIntent: " + intent.toString());
        String action = intent.getAction();
        String type = intent.getType();
        Uri data = intent.getData();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            //log click push tin
            try {
                boolean fromNotify = bundle.getBoolean("from_notify", false);
                int typeNotify = bundle.getInt("type_notify", 0);
                String name = bundle.getString("name_new", "Title ");
                Log.i("Fire push", typeNotify + " : " + fromNotify);
                if (fromNotify && typeNotify == NOTIFY_NEWS) {
                    Log.i("Fire push", "" + name);
                    mApplication.trackingEventFirebase("Revision " + Config.REVISION, name, "CLICK_PUSH_NEWS");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            currentTabPosition = 0;
        }
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        boolean callFromTargetService = intent.getBooleanExtra("from_chooser_target", false);
        if (bundle != null && !StringUtils.isEmpty(bundle.getString("notificationType"))) {
            updateReadNotification(bundle.getString("notifyId"));
            Uri uri = DeepLinkHelper.createDeepLinkForNotification(bundle);
            if (uri != null) {
                DeepLinkHelper.getInstance().openSchemaLink(HomeActivity.this, uri, null, null);
            }
            intent.replaceExtras(new Bundle());
        } else if (Constants.MOCHA_INTENT.ACTION_OPEN_CHAT.equals(action) &&
                Constants.MOCHA_INTENT.INTENT_TYPE.equals(type)) {//ACTION_OPEN_CHAT
            if (accountBusiness.isAnonymousLogin()) {
                loginFromAnonymous();
                finish();
            }
//            else if (accountBusiness.isValidAccount())
//                DeepLinkHelper.getInstance().handleChatOtherApp(mApplication, HomeActivity.this, intent);
        } else if (callFromTargetService && type != null &&
                (Intent.ACTION_SEND.equals(action) || Intent.ACTION_SEND_MULTIPLE.equals(action))) {
            if (accountBusiness.isAnonymousLogin()) {
                loginFromAnonymous();
                finish();
            } else if (accountBusiness.isValidAccount())
                handleShareFromChooserTarget(intent, action, type, intent.getIntExtra("chooser_target_thread_id", -1));
        } else if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (accountBusiness.isAnonymousLogin()) {
                loginFromAnonymous();
                finish();
            } else if (accountBusiness.isValidAccount()) {
                if ("text/*".equals(type) || "text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (type.startsWith("image/")) {
                    handleSendImage(intent); // Handle single image being sent
                } else if (type.equals(ContactsContract.Contacts.CONTENT_VCARD_TYPE)) {
                    handleSendContact(intent);
                } else /*if ("application/vnd.google-apps.document".equals(type) || "application/vnd.google-apps.spreadsheet".equals(type)
                        || "application/pdf".equals(type) || "application/msword".equals(type)
                        || "application/vnd.openxmlformats-officedocument.wordprocessingml.document".equals(type)
                        || type.startsWith("application/vnd.openxmlformats-officedocument"))*/ {
                    handleShareFileDriver(intent);
                }
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (accountBusiness.isAnonymousLogin()) {
                loginFromAnonymous();
                finish();
            } else if (accountBusiness.isValidAccount()) {
                if (type.startsWith("image/"))
                    handleSendMultipleImages(intent); // Handle multiple images being sent
                else {
                    handleSendMultipleFile(intent);
                }
            }
        } else if (Intent.ACTION_VIEW.equals(action)) {
            if ("text/*".equals(type) || "text/plain".equals(type)) {
                if (accountBusiness.isAnonymousLogin()) {
                    loginFromAnonymous();
                    finish();
                } else if (accountBusiness.isValidAccount()) {
                    handleSendText(intent); // Handle text being sent
                }
            } else if (data != null && DeepLinkHelper.DEEP_LINK.SCHEME.equals(data.getScheme())) {
                DeepLinkHelper.getInstance().openSchemaLink(HomeActivity.this, data, null, null);
                isOpenFromOtherApp = true;
                Bundle bun = intent.getExtras();
                if (bun != null) {
                    boolean fromNotify = bun.getBoolean("from_notify", false);
                    if (fromNotify) isOpenFromOtherApp = false;
                }
            } else if (com.metfone.selfcare.util.contactintergation.Constants.MIME_MOCHA_CHAT.equals(type)) {
                if (accountBusiness.isAnonymousLogin()) {
                    loginFromAnonymous();
                    finish();
                } else if (accountBusiness.isValidAccount())
                    handleActionChatFromContact(data);
            } else if (com.metfone.selfcare.util.contactintergation.Constants.MIME_MOCHA_CALL.equals(type)) {
                if (accountBusiness.isAnonymousLogin()) {
                    loginFromAnonymous();
                    finish();
                } else if (accountBusiness.isValidAccount())
                    handleActionCallFromContact(data);
            } else if (data != null && DeepLinkHelper.DEEP_LINK.SCHEME_HTTP.equals(data.getScheme()) && (DeepLinkHelper.DEEP_LINK.HOST_CAM_ID.equals(data.getHost()) || DeepLinkHelper.DEEP_LINK.HOST_CAM_DYNAMIC_LINK.equals(data.getHost()))) {
//                DeepLinkHelper.getInstance().openSchemaHttpMochaVideo(HomeActivity.this, data);
                DeepLinkHelper.getInstance().openSchemaLink(this, intent.getData(), null, null);
                isOpenFromOtherApp = true;
            } else if (DeepLinkHelper.DEEP_LINK.SCHEME_HTTPS.equals(data.getScheme()) && (DeepLinkHelper.DEEP_LINK.HOST_CAM_ID.equals(data.getHost()) || DeepLinkHelper.DEEP_LINK.HOST_CAM_DYNAMIC_LINK.equals(data.getHost()))) {
                DeepLinkHelper.getInstance().openSchemaLink(this, intent.getData(), null, null);
                isOpenFromOtherApp = true;
            } else {
                showToast(R.string.e601_error_but_undefined);
            }
        } else if (!TextUtils.isEmpty(action) && action.startsWith(Constants.MOCHA_INTENT.ACTION_OPEN_FROM_FACEBOOK)) {
//            DeepLinkHelper.getInstance().openSchemaHttpMochaVideo(HomeActivity.this, data);
            isOpenFromOtherApp = true;
        } else {// open home deeplink in app
            if (mHomePager != null) {
                mHomePager.setCurrentItem(currentTabPosition);
                /*if (indexSubTabVideo >= 0) {
                    mHomePager.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new TabVideoFragmentV2.TabVideoEvent(indexSubTabVideo));
                        }
                    }, 300);

                }*/
                /*if (currentSubTabMobile > 0) {
                    EventBus.getDefault().post(new TabMobileFragment.SetSubTabMobileEvent(currentSubTabMobile));
                }*/
                if (intent.getExtras() != null) {
                    String fromNumberChange = intent.getExtras().getString("change_number_from");
                    if (!TextUtils.isEmpty(fromNumberChange)) {
                        DialogMessage dialogMessage = new DialogMessage(this, true);
                        dialogMessage.setLabel(mApplication.getResources().getString(R.string.title_change_number));
                        dialogMessage.setMessage(
                                String.format(mApplication.getResources().getString(R.string.title_msg_change_number_from_to),
                                        fromNumberChange, mApplication.getReengAccountBusiness().getJidNumber()));
                        dialogMessage.show();
                    }
                }
            }
        }
        if (intent != null) {
            intent.setAction("");
        }
    }


    private void handleSendMultipleFile(Intent intent) {
        if (intent == null || intent.getClipData() == null) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        GetMultiFileAsyncTask task = new GetMultiFileAsyncTask(mApplication, intent);
        task.setListener(new GetMultiFileAsyncTask.GetMultiFileListener() {
            @Override
            public void onStartTask() {
                showLoadingDialog(null, R.string.loading);
            }

            @Override
            public void onFinishTask(int totalItems, ArrayList<ReengMessage> results) {
                if (!HomeActivity.this.isFinishing()) {
                    hideLoadingDialog();
                    int countSuccess = 0;
                    if (Utilities.notEmpty(results)) {
                        countSuccess = results.size();
                        displayAllToShare(results, false);
                    }
                    int countFail = totalItems - countSuccess;
                    if (countFail > 0) {
                        String msg = String.format(getResources().getString(countFail <= 1 ?
                                R.string.file_multi_file_fail : R.string.file_multi_files_fail), countFail);
                        showToast(msg, Toast.LENGTH_LONG);
                    }
                    resetIntent(intent);
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void handleShareFileDriver(Intent intent) {
        if (intent.getClipData() == null || intent.getClipData().getItemCount() > 1) {
            showToast(R.string.e601_error_but_undefined);
            return;
        }
        showLoadingDialog(null, R.string.downloading);
        DownloadFileDriveAsyncTask downloadFileDriveAsyncTask = new DownloadFileDriveAsyncTask(mApplication, HomeActivity.this,
                intent.getClipData().getItemAt(0).getUri(), new DownloadDriveResponse() {
            @Override
            public void onResult(String fileName, String filePath) {
                hideLoadingDialog();
                transferFile(filePath, fileName);
                resetIntent(intent);
            }

            @Override
            public void onFail(boolean isMaxSize) {
                hideLoadingDialog();
                if (isMaxSize)
                    showToast(R.string.file_too_big);
                else
                    showToast(R.string.e601_error_but_undefined);
                resetIntent(intent);
            }
        });
        downloadFileDriveAsyncTask.execute();
    }

    private void transferFile(String filePath, String fileName) {
        String extension = FileHelper.getExtensionFile(filePath);
        if (ReengMessageConstant.FileType.isSupported(extension)) {
            ArrayList<ReengMessage> messageToShare = new ArrayList<>();
            ReengMessage msg = new ReengMessage();
            msg.setFilePath(filePath);
            msg.setFileName(fileName);
            msg.setMessageType(ReengMessageConstant.MessageType.file);
            messageToShare.add(msg);
            if (messageToShare != null && !messageToShare.isEmpty()) {
                displayAllToShare(messageToShare, false);
            }
        } else {
            showToast(R.string.e666_not_support_function);
        }
    }

    private void handleShareFromChooserTarget(Intent intent, String action, String type,
                                              int threadId) {
        if (threadId == -1) return;
        ThreadMessage thread = mApplication.getMessageBusiness().findThreadByThreadId(threadId);
        if (thread == null) return;
        ArrayList<ReengMessage> messageToShare = new ArrayList<>();
        if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type.startsWith("image/")) {
            messageToShare = MessageHelper.getListMessageImageFromIntent(intent, this);
        } else if (Intent.ACTION_SEND.equals(action)) {
            if ("text/plain".equals(type)) {
                messageToShare = MessageHelper.getMessagesTextFromIntent(intent, this);
            } else if (type.startsWith("image/")) {
                messageToShare = MessageHelper.getMessageImageFromIntent(intent, this);
            } else if (type.equals(ContactsContract.Contacts.CONTENT_VCARD_TYPE)) {
                messageToShare = MessageHelper.getListMessageContactFromIntent(intent, this);
            }
        }
        NavigateActivityHelper.navigateToChatDetail(HomeActivity.this, thread, messageToShare);
    }

    private void handleSendText(Intent intent) {
        String text = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (text == null) {
            if (intent.getCharSequenceExtra(Intent.EXTRA_TEXT) == null) {
                return;
            } else {
                text = intent.getCharArrayExtra(Intent.EXTRA_TEXT).toString();
            }
        }
        Log.d(TAG, text);
        String subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);
        if (!TextUtils.isEmpty(text)) {
            ArrayList<ReengMessage> messageToShare = new ArrayList<>();
            ReengMessage msg = new ReengMessage();
            boolean showShareOnMedia = false;
            if ((text.startsWith("http://") || text.startsWith("https://"))) {
                msg.setFilePath(text);
                showShareOnMedia = true;
                if (!TextUtils.isEmpty(subject)) {
                    text = subject + "\n" + text;
                }
            }
            msg.setMessageType(ReengMessageConstant.MessageType.text);
            msg.setContent(text);
            messageToShare.add(msg);
            Log.i(TAG, " share text: " + text);
            displayAllToShare(messageToShare, showShareOnMedia);
        }
        resetIntent(intent);
    }

    private void handleSendImage(Intent intent) {
        ArrayList<ReengMessage> messageToShare = MessageHelper.getMessageImageFromIntent(intent, this);
        if (messageToShare != null && !messageToShare.isEmpty()) {
            if (messageToShare.size() > 10) {
                showToast(String.format(getString(R.string.upload_image_maximum), 10));
            } else
                displayAllToShare(messageToShare, true);
        }
        resetIntent(intent);
    }

    private void handleSendMultipleImages(Intent intent) {
        ArrayList<ReengMessage> messageToShare = MessageHelper.getListMessageImageFromIntent(intent, this);
        if (messageToShare != null && !messageToShare.isEmpty()) {
            displayAllToShare(messageToShare, true);
        }
        resetIntent(intent);
    }

    private void handleSendContact(Intent intent) {
        ArrayList<ReengMessage> messageToShare = MessageHelper.getListMessageContactFromIntent(intent, this);
        if (messageToShare != null) {
            displayAllToShare(messageToShare, false);
        }
        resetIntent(intent);
    }

    private void displayAllToShare(ArrayList<ReengMessage> listmsg, boolean showShareOnMedia) {
        if (Config.Features.FLAG_SHARE_OTHER_WITH_BUSINESS) {
            ShareContentBusiness business = new ShareContentBusiness(this, listmsg, showShareOnMedia);
            business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_FROM_OTHER_APP);
            business.showPopupShareMessages();
        } else {
            NavigateActivityHelper.navigateToChooseContact(HomeActivity.this, Constants.CHOOSE_CONTACT
                            .TYPE_SHARE_FROM_OTHER_APP,
                    null, null, listmsg, showShareOnMedia, false, -1, true);
            finish();
        }
    }

    private void handleActionChatFromContact(Uri uri) {
        final Cursor cursor = getContentResolver().query(uri, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            String friendJid = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            if (!TextUtils.isEmpty(friendJid)) {
                friendJid = friendJid.replaceAll(" ", "");
                ThreadMessage threadMessage = mMessageBusiness.findExistingSoloThread(friendJid);
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendJid);
                if (threadMessage == null) {// chua co thread
                    if (phoneNumber == null) {
                        StrangerPhoneNumber strangerPhoneNumber = mApplication.getStrangerBusiness()
                                .getExistStrangerPhoneNumberFromNumber(friendJid);
                        if (strangerPhoneNumber != null) {// so chua luu, co trong bang stranger thi tao
                            // thread stranger
                            if (strangerPhoneNumber.getStrangerType() == StrangerPhoneNumber.StrangerType
                                    .other_app_stranger) {
                                threadMessage = mMessageBusiness.createNewThreadStrangerOtherApp(friendJid,
                                        strangerPhoneNumber, true);
                            } else {
                                threadMessage = mMessageBusiness.createNewThreadStrangerMusic(friendJid,
                                        strangerPhoneNumber, true);
                            }
                        }
                    } else if (phoneNumber.isReeng()
                            || (mApplication.getReengAccountBusiness().isViettel()
                            && phoneNumber.isViettel())) {
                        threadMessage = mMessageBusiness.createNewThreadNormal(friendJid);
                    }
                }
                if (threadMessage != null) {
                    NavigateActivityHelper.navigateToChatDetail(this, threadMessage);
                } else if (phoneNumber != null) {// co trong danh ba, ko dung, hoac khong phai vt
                            /*InviteFriendHelper.getInstance().showRecommendInviteFriendPopup(mApplication, this,
                                    phoneNumber.getName(), phoneNumber.getJidNumber(), false);*/
                    NavigateActivityHelper.navigateToChatDetail(this, threadMessage);
                } else {// so khong luu danh ba, khong co thread lam quen
                    showToast(getResources().getString(R.string.e405_user_invalid), Toast.LENGTH_LONG);
                }
            }

            cursor.close();
        }
    }

    private void handleActionCallFromContact(Uri uri) {
        if (uri != null) {
            final Cursor cursor = getContentResolver().query(uri, null, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                String friendJid = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (!TextUtils.isEmpty(friendJid)) {
                    friendJid = friendJid.replaceAll(" ", "");

                    KeyBoardDialog dialog = new KeyBoardDialog(this, friendJid);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogKeyBoardAnimation;
                    dialog.show();
                    /*currentSubTabMobile = 1;
                    currentTabPosition = 1;
                    setCurrentTab();
                    TabMobileFragment.TabMobileMessageEvent event = new TabMobileFragment.TabMobileMessageEvent();
                    event.setNumbKeyPad(friendJid);
                    EventBus.getDefault().post(event);*/
                }
            }
        }

    }

    private void handleActionVideoCallFromContact(Uri uri) {
        if (uri != null) {
            Cursor c = null;
            try {
                c = getContentResolver().query(uri, new String[]{ContactsContract.Data.DATA1,
                                ContactsContract.Data.DATA2,
                                ContactsContract.Data.DATA3,
                                ContactsContract.Data.DATA4,
                                ContactsContract.Data.DATA5,
                                ContactsContract.Data.MIMETYPE},
                        null, null, null);

                if (c != null && c.moveToFirst()) {
                    String friendJid = c.getString(0);
                    Log.i(TAG, "number: " + c.getString(0) + " " + c.getString(1) + " " + c.getString(2)
                            + " " + c.getString(3) + " " + c.getString(4) + " " + c.getString(5));
                    if (!TextUtils.isEmpty(friendJid)) {
                        PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(friendJid);
                        if (phoneNumber != null) {
                            mApplication.getCallBusiness().checkAndStartVideoCall(this, phoneNumber, false);
                        }
                    }
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        }

    }

    public void openContacts() {
        if (mHomePager != null) {
            mHomePager.setCurrentItem(1);
            EventBus.getDefault().post(new TabMobileFragment.SetSubTabMobileEvent(0)); // TODO: 5/21/2020 ContactFragment 1
        }
    }

    private void handleNewIntent(@Nullable Intent newIntent) {
        if (newIntent == null) return;
        String link = "";
        Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(getApplicationContext(), newIntent);
        if (targetUrl != null) {
            newIntent.removeExtra("al_applink_data");
            newIntent.removeExtra("extras");
            newIntent.removeExtra("target_url");
            try {
                link = URLDecoder.decode(targetUrl.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "Exception", e);
            }
        } else {
            Uri uri = newIntent.getData();
            if (uri == null) return;
            link = uri.toString();
        }
        if (Utilities.isEmpty(link)) return;
        if (link.startsWith("camid://") || link.startsWith("http") || link.startsWith("content://")) {
            Log.e(TAG, "handleNewIntent");
            if (link.contains("install_camid")) {
                getDynamicLinks(newIntent);
            } else {
                processDataFromIntent(newIntent);
            }
        }

    }

    private void getConfigWhitelist() {
//        getAds();
//        getCategoriesV2();
        if (mApplication == null)
            return;

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        long cacheExpiration = 24 * 60 * 60;
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (mFirebaseRemoteConfig == null)
                    return;
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                }
                String config = mFirebaseRemoteConfig.getString(Constants.TabVideo.WHITE_LIST_DEVICE);
                if (TextUtils.isEmpty(config))
                    config = Constants.WHITE_LIST;
                if (mApplication != null)
                    mApplication.getPref().edit().putString(Constants.PREFERENCE.CONFIG.WHITELIST_DEVICE, config).apply();
            }
        });
    }

    private void getAds() {
        new MochaAdsClient();
    }

    private void getCategoriesV2() {
        mApplication.getApplicationComponent().providerVideoApi().getCategoriesV2(null);
        if (mApplication.getReengAccountBusiness().isCambodia())
            new MovieApi().getListSubtab(null, null);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    public void setShowingCustomViewWap(boolean showingCustomViewWap) {
        this.showingCustomViewWap = showingCustomViewWap;
        navigationBar.setVisibility(showingCustomViewWap ? View.GONE : View.VISIBLE);
        if (showingCustomViewWap) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            showStatusBarWap(false);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            showStatusBarWap(true);
        }
    }

    //-------------------Init Ads--------------------//
    private void initAds() {
        try {
            String ad_app_id = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_APP_ID);
            String ad_fullscreen = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_FULLSCREEN);
            String ad_player_music = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_PLAYER_MUSIC);
            String ad_video_detail = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_VIDEO_DETAIL);
            String ad_native = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_NATIVE);

            Log.i("ADS", "Time start init ads firebase: " + (System.currentTimeMillis() - startTime) + "|" + System.currentTimeMillis());

//                            long ad_enable = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.ENABLE_ADS);
            if (AdsUtils.checkShowAds()) {
                AdsManager.getInstance().initAds(HomeActivity.this, ad_app_id);
                AdsManager.getInstance().initAdsFullScreen(ad_fullscreen);
                AdsManager.getInstance().initAdsNative(ad_native);
                AdsManager.getInstance().initAdsBannerMedium(ad_player_music, AdSize.MEDIUM_RECTANGLE);
                AdsManager.getInstance().initAdsBanner(ad_video_detail, AdSize.SMART_BANNER);
            }
        } catch (Exception ex) {

        }
//        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
//                .setDeveloperModeEnabled(true)
//                .build();
//        mFirebaseRemoteConfig.setConfigSettings(configSettings);
//
//        mFirebaseRemoteConfig.fetch(0)
//                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Log.i(TAG, "Fetch Success");
//
//                            // Once the config is successfully fetched it must be activated before newly fetched
//                            // values are returned.
//                            mFirebaseRemoteConfig.activateFetched();
//                        } else {
//                            Log.i(TAG, "Fetch Failed");
//                        }
//                        try {
//                            String ad_app_id = mFirebaseRemoteConfig.getString(AdsUtils.KEY_FIREBASE.AD_APP_ID);
//                            String ad_fullscreen = mFirebaseRemoteConfig.getString(AdsUtils.KEY_FIREBASE.AD_FULLSCREEN);
//                            String ad_player_music = mFirebaseRemoteConfig.getString(AdsUtils.KEY_FIREBASE.AD_PLAYER_MUSIC);
//                            String ad_video_detail = mFirebaseRemoteConfig.getString(AdsUtils.KEY_FIREBASE.AD_VIDEO_DETAIL);
//                            String ad_native = mFirebaseRemoteConfig.getString(AdsUtils.KEY_FIREBASE.AD_NATIVE);
//
//                            Log.i("ADS", "Time start init ads firebase: " + (System.currentTimeMillis() - startTime) + "|" + System.currentTimeMillis());
//
////                            long ad_enable = mFirebaseRemoteConfig.getLong(AdsUtils.KEY_FIREBASE.ENABLE_ADS);
//                            if (AdsUtils.checkShowAds()) {
//                                AdsManager.getInstance().initAds(HomeActivity.this, ad_app_id);
//                                AdsManager.getInstance().initAdsFullScreen(ad_fullscreen);
//                                AdsManager.getInstance().initAdsBannerMedium(ad_player_music, AdSize.MEDIUM_RECTANGLE);
//                                AdsManager.getInstance().initAdsBanner(ad_video_detail, AdSize.SMART_BANNER);
//                                AdsManager.getInstance().initAdsNative(ad_native);
//                            }
//                        } catch (Exception ex) {
//
//                        }
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception exception) {
//
//                    }
//                });
    }

    private void resetIntent(Intent intent) {
        if (BuildConfig.DEBUG) Log.e(TAG, "resetIntent " + intent);
        if (intent != null && !HomeActivity.this.isFinishing()) {
            try {
                intent.setAction("");
                intent.setClipData(null);
                Bundle bundle = null;
                intent.replaceExtras(bundle);
                intent.setData(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showHomeTab(int homeTabPosition) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            Fragment f = fragment;
            if (f instanceof HomePagerFragment) {
                TabHomeHelper.TAB_CURRENT = homeTabPosition;
                ((HomePagerFragment) f).reInitialBottomNavigationBarWithBackground(R.color.m_home_tab_background);
            }
        }
    }

    public void setBottomNavigationBarColor(int idResColor, boolean forceSelectTab) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            Fragment f = fragment;
            if (f instanceof HomePagerFragment) {
                ((HomePagerFragment) f).reInitialBottomNavigationBarWithBackground(idResColor);
                return;
            }
        }
    }

    public void setBottomNavigationBarVisibility(int visibility) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            Fragment f = fragment;
            if (f instanceof HomePagerFragment) {
                ((HomePagerFragment) f).setBottomNavigationBarVisibility(visibility);
                return;
            }
        }
    }

    public static class SetCurrentTabStrangerEvent {
        int pos = 0;

        public SetCurrentTabStrangerEvent(int pos) {
            this.pos = pos;
        }

        public int getPos() {
            return pos;
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    public void logActiveTab(int indexTab) {
        if (!FirebaseEventBusiness.activeTab[indexTab]) {
            ApplicationController.self().getFirebaseEventBusiness().logActiveTab(indexTab);
        }
    }

    private void updateReadNotification(String notificationId) {
        if (StringUtils.isEmpty(notificationId) || userInfoBusiness == null || userInfoBusiness.getUser() == null) {
            return;
        }
        KhHomeClient.getInstance().wsUpdateIsReadCamIDNotification(String.valueOf(userInfoBusiness.getUser().getUser_id()), Integer.parseInt(notificationId), Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<KhNotificationReadResponse>() {
            @Override
            public void onResponse(retrofit2.Response<KhNotificationReadResponse> response) {
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    private void updateToken() {
        if (isUpdatingToken) {
            return;
        }
        if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT && !SharedPrefs.getInstance().get(PREF_SEND_TOKEN_SUCCESS, Boolean.class)) {
            String camId = "";
            if (userInfoBusiness.getUser() == null || userInfoBusiness.getUser().getUser_id() != 0) {
                camId = String.valueOf(userInfoBusiness.getUser().getUser_id());
            }
            String language = LocaleManager.getLanguage(this);
            isUpdatingToken = true;
            String finalCamId = camId;
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    try {
                        if (task.isSuccessful() && task.getResult() != null) {
                            String token = task.getResult().getToken();
                            String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                            Log.d("ABC", "ABC 1 = " + token);
                            Log.d("ABC", "ABC 2 = " + deviceId);
                            MetfonePlusClient.getInstance().updateDeviceToken(finalCamId, token, language, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<BaseResponse>() {
                                @Override
                                public void onResponse(Response<BaseResponse> response) {
                                    if (response != null && response.body() != null && response.body().getErrorCode().equals("S200")) {
                                        android.util.Log.d(TAG, "Token success : " + SharedPrefs.getInstance().get(PREF_FIREBASE_REFRESHED_TOKEN, String.class));
                                        SharedPrefs.getInstance().put(PREF_SEND_TOKEN_SUCCESS, true);
                                    }
                                    isUpdatingToken = false;
                                }

                                @Override
                                public void onError(Throwable error) {
                                    Log.d(TAG, "Update token camid error : " + error.toString());
                                    isUpdatingToken = false;
                                }
                            });
                        }
                    } catch (Exception ex) {
                    }

                }
            });


        } else {
            if (userInfoBusiness.getUser() == null || StringUtils.isEmpty(String.valueOf(userInfoBusiness.getUser().getUser_id())))
                return;
            String language = LocaleManager.getLanguage(this);
            String isLogin = "";
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT)
                isLogin = "0";
            else
                isLogin = "1";
            MetfonePlusClient.getInstance().updateDeviceStatus(String.valueOf(userInfoBusiness.getUser().getUser_id()), isLogin, language, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<BaseResponse>() {
                @Override
                public void onResponse(Response<BaseResponse> response) {
                }

                @Override
                public void onError(Throwable error) {
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(UpdateTokenEvent event) {
        if (!isUpdatingToken) {
            String camId = "";
            if (userInfoBusiness.getUser() == null || userInfoBusiness.getUser().getUser_id() != 0) {
                camId = String.valueOf(userInfoBusiness.getUser().getUser_id());
            }
            String language = LocaleManager.getLanguage(this);
            isUpdatingToken = true;
            String finalCamId = camId;
            try {
                String token = event.getToken();
                MetfonePlusClient.getInstance().updateDeviceToken(finalCamId, token, language, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID), new MPApiCallback<BaseResponse>() {
                    @Override
                    public void onResponse(Response<BaseResponse> response) {
                        if (response != null && response.body() != null && response.body().getErrorCode().equals("S200")) {
                            android.util.Log.d(TAG, "Token success : " + SharedPrefs.getInstance().get(PREF_FIREBASE_REFRESHED_TOKEN, String.class));
                            SharedPrefs.getInstance().put(PREF_SEND_TOKEN_SUCCESS, true);
                        }
                        isUpdatingToken = false;
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.d(TAG, "Update token camid error : " + error.toString());
                        isUpdatingToken = false;
                    }
                });
            } catch (Exception ex) {
            }
            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    // click icon Game in tab Home
    @Override
    public void onClick() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            Fragment f = fragment;
            if (f instanceof HomePagerFragment) {
                ((HomePagerFragment) f).openTabGameFromIconHome();
                return;
            }
        }
    }

}

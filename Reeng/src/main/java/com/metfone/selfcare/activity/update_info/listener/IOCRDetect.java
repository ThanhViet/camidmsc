package com.metfone.selfcare.activity.update_info.listener;

import com.metfone.selfcare.network.metfoneplus.response.WsDetectORCResponse;

public interface IOCRDetect {
    void onSuccess(WsDetectORCResponse.Response response);
}

package com.metfone.selfcare.activity;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDetachIPResponse;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.view.CamIdButton;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.util.Utils;
import com.metfone.selfcare.v5.utils.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewProfileActivity extends BaseSlidingFragmentActivity {
    Unbinder unbinder;
    @BindView(R.id.clContainer)
    ConstraintLayout clContainer;
    @BindView(R.id.ivProfileAvatar)
    CircleImageView ivProfileAvatar;
    @BindView(R.id.tvName)
    CamIdTextView tvName;
    @BindView(R.id.tvGender)
    CamIdTextView tvGender;
    @BindView(R.id.tvDob)
    CamIdTextView tvDob;
    @BindView(R.id.tvEmail)
    CamIdTextView tvEmail;
    @BindView(R.id.tvIDNumber)
    CamIdTextView tvIDNumber;
    @BindView(R.id.tvAddress)
    CamIdTextView tvAddress;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.btnCheckVerify)
    CamIdButton btnCheckVerify;
    @BindView(R.id.tvContact)
    CamIdTextView tvContact;
    ApplicationController mApplication;
    UserInfoBusiness userInfoBusiness;
    UserInfo currentUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_profile);
        unbinder = ButterKnife.bind(this);
        changeStatusBar(Color.parseColor("#C41627"));
        drawBackground();
        mApplication = (ApplicationController) getApplication();
        userInfoBusiness = new UserInfoBusiness(this);

    }

    private void drawBackground() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            clContainer.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        } else {
            clContainer.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        }
    }

    private void initUserView(UserInfo userInfo) {
        if (TextUtils.isEmpty(currentUser.getAvatar())) {
            if (!NetworkHelper.isConnectInternet(this)) {
                showError(getString(R.string.error_internet_disconnect), null);
            } else {
                getRankAccount();
            }
        } else {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(currentUser.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_member)
                    .error(R.drawable.ic_avatar_member)
                    .into(ivProfileAvatar);
        }
        String name = userInfo.getFull_name();
        if (!TextUtils.isEmpty(currentUser.getVerified()) && EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(currentUser.getVerified())) {
            btnCheckVerify.setText(getString(R.string.verified_identity));
            btnCheckVerify.setEnabled(false);
            btnCheckVerify.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_verified_identity));
        } else if (!TextUtils.isEmpty(currentUser.getVerified()) &&
                EnumUtils.IdNumberVerifyStatusTypeDef.PENDING.equals(currentUser.getVerified())) {
            btnCheckVerify.setEnabled(false);
            if (!NetworkHelper.isConnectInternet(this)) {
                showError(getString(R.string.error_internet_disconnect), null);
            } else {
                //call api to pass reject verify id number
                getUserInformation();
            }
        }
        if (TextUtils.isEmpty(name)) {
            tvName.setText(" ");
        } else {
            tvName.setText(UserInfoBusiness.getMaxLengthName(name));
        }
        if (userInfo.getGender() == 1) {
            tvGender.setText(getString(R.string.sex_male));
            tvGender.setVisibility(View.VISIBLE);
        } else if (userInfo.getGender() == 2) {
            tvGender.setText(getString(R.string.sex_female));
            tvGender.setVisibility(View.VISIBLE);
        } else {
            tvGender.setText(getString(R.string.sc_other));
        }
        if (StringUtils.isEmpty(userInfo.getEmail())) {
            tvEmail.setVisibility(View.GONE);
        } else {
            tvEmail.setVisibility(View.VISIBLE);
            tvEmail.setText(userInfo.getEmail());
        }
        if (StringUtils.isEmpty(userInfo.getAddress()) && StringUtils.isEmpty(userInfo.getDistrict()) && StringUtils.isEmpty(userInfo.getProvince())) {
            tvAddress.setVisibility(View.GONE);
        } else {
            tvAddress.setVisibility(View.VISIBLE);
            String address = userInfo.getAddress();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(address);
            String defaultDistrict = "-";
            String comma = ", ";
            String dot = ".";
            if (userInfo.getProvince() != null && userInfo.getDistrict() != null && userInfo.getDistrict().equals(defaultDistrict)) {
                stringBuilder.append(dot);
            } else {
                if (userInfo.getProvince() != null && userInfo.getDistrict() != null) {
                    stringBuilder.append(comma).append(userInfo.getDistrict()).append(comma).append(userInfo.getProvince()).append(dot);
                } else if (userInfo.getProvince() != null && userInfo.getDistrict() == null) {
                    stringBuilder.append(comma).append(userInfo.getProvince()).append(dot);
                } else if (userInfo.getProvince() == null && userInfo.getDistrict() != null) {
                    stringBuilder.append(comma).append(userInfo.getDistrict()).append(dot);
                }
            }
            tvAddress.setText(stringBuilder.toString());
        }

        if (StringUtils.isEmpty(userInfo.getContact())) {
            tvContact.setVisibility(View.GONE);
        } else {
            tvContact.setVisibility(View.VISIBLE);
            tvContact.setText(userInfo.getContact());
        }

        if (StringUtils.isEmpty(userInfo.getIdentity_number())) {
            tvIDNumber.setVisibility(View.GONE);
        } else {
            tvIDNumber.setVisibility(View.VISIBLE);
            tvIDNumber.setText(userInfo.getIdentity_number());
        }
        if (!StringUtils.isEmpty(userInfo.getDate_of_birth())) {
            tvDob.setVisibility(View.VISIBLE);
            try {
                SharedPreferences mPref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                String deviceLanguage = mApplication.getReengAccountBusiness().getDeviceLanguage();
                String mCurrentLanguageCode = mPref.getString(Constants.PREFERENCE.PREF_LANGUAGE_TRANSLATE_SELECTED, deviceLanguage);
                String dob = TimeHelper.convertTimeFromAPi(userInfo.getDate_of_birth(), mCurrentLanguageCode);
                if (dob.length() > 0) {
                    tvDob.setText(dob);
                } else {
                    tvDob.setText(R.string.empty_string);
                }

            } catch (Exception ignored) {
                tvDob.setText(R.string.empty_string);
            }

        } else {
            tvDob.setVisibility(View.GONE);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        currentUser = userInfoBusiness.getUser();
        if (currentUser != null) {
            initUserView(currentUser);
            getIsdnFromIP(Utils.getIpOf3G());
        }
    }


    public void getIsdnFromIP(String ip) {
        if (!NetworkHelper.isConnectInternet(this)) {
            return;
        }
//        binding.pbLoading.setVisibility(View.VISIBLE);
        new MetfonePlusClient().wsDetachIp(ip, new MPApiCallback<WsDetachIPResponse>() {
            @Override
            public void onResponse(Response<WsDetachIPResponse> response) {
//                binding.pbLoading.setVisibility(View.GONE);
                if (response.body() == null || response.body().getResult() == null) {
                    return;
                }
                if (response.body().getResult().getErrorCode().equals("0")) {
                    WsDetachIPResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    if (wsResponse != null && !TextUtils.isEmpty(wsResponse.getIsdn())) {
                        if (TextUtils.isEmpty(tvContact.getText().toString().trim())) {
                            tvContact.setText(wsResponse.getIsdn());
                            currentUser.setContact(wsResponse.getIsdn());
                            userInfoBusiness.setUser(currentUser);
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
//                binding.pbLoading.setVisibility(View.GONE);
            }
        });
    }

    @Optional
    @OnClick({R.id.btnBack, R.id.ivProfileAvatar, R.id.btnFromFacebook, R.id.btnFromGoogle,
            R.id.tvChangePhone, R.id.btnEdit, R.id.btnCheckVerify})
    public void onClick(View view) {
        // ...
        switch (view.getId()) {
            case R.id.btnCheckVerify:
//                NavigateActivityHelper.navigateToVerifyInformation(this);
                String numberPhone = "";
                if (Utilities.isMetPhone(userInfoBusiness.getUser().getPhone_number())) {
                    numberPhone = userInfoBusiness.getUser().getPhone_number();
                }
                NavigateActivityHelper.navigateToVerifyNewInformation(this, numberPhone);
                break;
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnEdit:
                NavigateActivityHelper.navigateToEditProfile(this);
                break;
        }
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount() {
        KhHomeClient homeKhClient = new KhHomeClient();
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                initRankInfo(body.getData().getAccountRankDTO());
            }

            @Override
            public void onFailed(String status, String message) {
                ToastUtils.showToast(NewProfileActivity.this, message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                ToastUtils.showToast(NewProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void initRankInfo(AccountRankDTO account) {
        if (account != null) {
//            tvRankName.setText(account.rankName);
            int rankID = account.rankId;
            EnumUtils.AvatarRank myRank = EnumUtils.AvatarRank.getById(rankID);
            if (myRank != null) {
                Glide.with(this)
                        .load(myRank.drawable)
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            } else {
                Glide.with(this)
                        .load(ContextCompat.getDrawable(NewProfileActivity.this, R.drawable.ic_avatar_member))
                        .centerCrop()
                        .placeholder(R.drawable.ic_avatar_member)
                        .error(R.drawable.ic_avatar_member)
                        .into(ivProfileAvatar);
            }
        } else {
            ivProfileAvatar.setImageDrawable(ContextCompat.getDrawable(NewProfileActivity.this, R.drawable.ic_avatar_member));
        }
    }

    private void getUserInformation() {
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (btnCheckVerify != null) {
                            if (!TextUtils.isEmpty(userInfo.getVerified()) && EnumUtils.IdNumberVerifyStatusTypeDef.APPROVE.equals(userInfo.getVerified())) {
                                btnCheckVerify.setText(getString(R.string.verified_identity));
                                btnCheckVerify.setBackground(ContextCompat.getDrawable(NewProfileActivity.this, R.drawable.bg_btn_verified_identity));
                                btnCheckVerify.setEnabled(false);
                            } else
                                btnCheckVerify.setEnabled(!TextUtils.isEmpty(userInfo.getVerified()) &&
                                        !EnumUtils.IdNumberVerifyStatusTypeDef.PENDING.equals(userInfo.getVerified()));
                        }
                    } else {
                        ToastUtils.showToast(NewProfileActivity.this, response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                ToastUtils.showToast(NewProfileActivity.this, getString(R.string.service_error));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
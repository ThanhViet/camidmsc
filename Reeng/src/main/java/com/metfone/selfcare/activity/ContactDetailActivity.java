package com.metfone.selfcare.activity;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ContentObserverBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.constant.StrangerConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.Contact;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.fragment.contact.ContactDetailFragmentNew;
import com.metfone.selfcare.fragment.contact.StrangerDetailFragmentNew;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.listeners.InitDataListener;
import com.metfone.selfcare.listeners.OnMediaInterfaceListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.ListImageProfileActivity;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;

//import com.metfone.selfcare.fragment.contact.OfficialDetailFragment;

/**
 * Created by toanvk2 on 6/28/14.
 */
public class ContactDetailActivity extends BaseSlidingFragmentActivity implements
        ContactDetailFragmentNew.OnContactDetailInteractionListener,
        StrangerDetailFragmentNew.OnStrangerFragmentInteractionListener,
        OnMediaInterfaceListener,
        InitDataListener {
    private final String TAG = ContactDetailActivity.class.getSimpleName();
    private ApplicationController mApplication;
    private ContactBusiness mContactBusiness;
    private MessageBusiness mMessageBusiness;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private Handler mHandler;
    private ProgressLoading mProgressLoading;
    private PhoneNumber mPhoneNumber;
    private String mPhoneId;
    private String mPhoneName;
    private String mNumber;//truyen tu notification vao la so
    private String mLastChangeAvatar, mBirthdayStr, mStatus;
    private String mStrangerJid;
    private int mGender;
    private int mType;
    private String mContactIdEdit;
    private String mJidNonContact;
    private NonContact mNonContact;
    private FeedBusiness mFeedBusiness;
    private FacebookHelper mFacebookHelper;
    private int userType;
//    private CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, " onCreate ... ");
        //changeLightStatusBar();
        setWhiteStatusBar();
        setContentView(R.layout.activity_my_profile);
        findComponentViews();
        getDataAndDisplayFragment(savedInstanceState);
        trackingScreen(TAG);
        mFacebookHelper = new FacebookHelper(this);
//        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        mHandler = new Handler();
        ListenerHelper.getInstance().addInitDataListener(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        ListenerHelper.getInstance().removeInitDataListener(this);
        mHandler = null;
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(NumberConstant.ID, mPhoneId);
        outState.putString(NumberConstant.NAME, mPhoneName);
        outState.putString(NumberConstant.LAST_CHANGE_AVATAR, mLastChangeAvatar);
        outState.putString(NumberConstant.NUMBER, mJidNonContact);
        outState.putString(NumberConstant.STATUS, mStatus);
        outState.putString(NumberConstant.BIRTHDAY_STRING, mBirthdayStr);
        outState.putInt(NumberConstant.GENDER, mGender);
        outState.putInt(NumberConstant.CONTACT_DETAIL_TYPE, mType);
        outState.putString(StrangerConstant.STRANGER_JID_NUMBER, mStrangerJid);
        outState.putString("contact_notification_number", mNumber);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "ondestroy");
        super.onDestroy();
    }

    @Override
    public void onDataReady() {
        if (mHandler == null) {
            return;
        }
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgressLoading.setVisibility(View.GONE);
                mProgressLoading.setEnabled(false);
                displayFragment();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        setActivityForResult(false);
        setTakePhotoAndCrop(false);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case Constants.ACTION.ADD_CONTACT:
                    //  business.insertNewContactFromIntentData(data);
                    ContentObserverBusiness.getInstance(this).setAction(-1);
                    break;
                case Constants.ACTION.EDIT_CONTACT:
                    if (mContactIdEdit == null) {
                        break;
                    }
                    Contact contact = mContactBusiness.getContactFromContactId(mContactIdEdit);
                    mContactBusiness.updateContactFromIntentData(data, contact);
                    mContactBusiness.initArrayListPhoneNumber();
                    mApplication.getMessageBusiness().updateThreadStrangerAfterSyncContact();
                    ContentObserverBusiness.getInstance(this).setAction(-1);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_ADD_FAVORITE:
                    String phoneId = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_PHONE_ID);
                    mContactBusiness.changeFavorite(
                            (mContactBusiness.getPhoneNumberFromPhoneId(phoneId)).getContactId(), true);
                    Log.d(TAG, "requestCode: " + requestCode + " phoneId: " + phoneId);
                    break;
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    int threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        } else {
            Log.d(TAG, "DATA null: ");
            if (requestCode == Constants.ACTION.ADD_CONTACT ||
                    requestCode == Constants.ACTION.EDIT_CONTACT) {
                ContentObserverBusiness.getInstance(this).setAction(-1);
            }
        }
//        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // call back
    @Override
    public void navigateToReengChatActivity(String number) {
        ThreadMessage thread = mMessageBusiness.findExistingOrCreateNewThread(number);
        NavigateActivityHelper.navigateToChatDetail(ContactDetailActivity.this, thread);
    }

    @Override
    public void navigateToThreadDetail(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(ContactDetailActivity.this, threadMessage);
    }

    @Override
    public void editContact(String contactID) {
        if (PermissionHelper.declinedPermission(this, Manifest.permission.WRITE_CONTACTS)) {
            PermissionHelper.requestPermissionWithGuide(ContactDetailActivity.this,
                    Manifest.permission.WRITE_CONTACTS,
                    Constants.PERMISSION.PERMISSION_REQUEST_EDIT_CONTACT);
        } else {
            mContactIdEdit = contactID;
            ContentObserverBusiness.getInstance(this).setAction(Constants.ACTION.EDIT_CONTACT);
            Intent editContact = new Intent(Intent.ACTION_EDIT);
            editContact.setData(Uri.parse(ContactsContract.Contacts.CONTENT_URI + "/" + contactID));
            editContact.putExtra("finishActivityOnSaveCompleted", true);
            setActivityForResult(true);
            startActivityForResult(editContact, Constants.ACTION.EDIT_CONTACT, true);
        }
    }

    @Override
    public void saveContact(String number, String name) {
        mApplication.getContactBusiness().navigateToAddContact(ContactDetailActivity.this, number, name);
    }

    @Override
    public void openListImage(ArrayList<ImageProfile> imageProfiles, String name, String jidNumber) {
//        executeAddFragmentTransaction(ListImageFragment.newInstance(imageProfiles, name, jidNumber), R.id.fragment_container, true, true);
        ListImageProfileActivity.startActivity(this, imageProfiles, name, jidNumber);

    }

    @Override
    public void navigateToWebView(FeedModelOnMedia feed) {
        mFeedBusiness.setFeedProfileProcess(feed);
        NavigateActivityHelper.navigateToWebView(this, feed, false);
    }

    @Override
    public void navigateToVideoView(FeedModelOnMedia feed) {
        mApplication.getApplicationComponent().providesUtils().openVideoDetail(this, feed);
        /*mFeedBusiness.setFeedProfileProcess(feed);
        Video video = Video.convertFeedContentToVideo(feed.getFeedContent());
        if (video == null) return;
        mApplication.getApplicationComponent().providesUtils().openVideoDetail(this, video);*/
//        NavigateActivityHelper.navigateToAutoPlayVideo(ContactDetailActivity.this, feed);
    }

    @Override
    public void navigateToAlbum(FeedModelOnMedia feed) {
        /*mFeedBusiness.setFeedProfileProcess(feed);
        mFeedBusiness.setFeedPlayList(feed);
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.ALBUM_DETAIL);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_CONTACT_DETAIL);
        startActivity(intent, true);*/
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    @Override
    public void navigateToMovieView(FeedModelOnMedia feed) {
        if (feed != null && feed.getFeedContent() != null) {
            Movie movie = FeedContent.convert2Movie(feed.getFeedContent());
            if (movie != null)
                playMovies(movie);
            else
                Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
        }
    }

    @Override
    public void navigateToProcessLink(FeedModelOnMedia feed) {
        Utilities.processOpenLink(mApplication, this, feed.getFeedContent().getLink());
    }

    /*@Override
    public void navigateToWriteStatus(FeedModelOnMedia feed, boolean isEdit) {
        mFeedBusiness.setFeedProfileProcess(feed);
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_CONTACT_DETAIL);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.WRITE_STATUS);
        startActivityForResult(intent, Constants.ACTION.ACTION_WRITE_STATUS);
    }*/

    @Override
    public void navigateToComment(FeedModelOnMedia feed) {
        mFeedBusiness.setFeedProfileProcess(feed);
        Intent intent = new Intent(mApplication, OnMediaActivityNew.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.COMMENT);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_FEED_TYPE, Constants.ONMEDIA.FEED_CONTACT_DETAIL);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, feed.getFeedContent().getUrl());
        intent.putExtra(Constants.ONMEDIA.PARAM_IMAGE.FEED_FROM, Constants.ONMEDIA.PARAM_IMAGE
                .FEED_FROM_FRIEND_PROFILE);
        boolean showMenuCopy = mFeedBusiness.checkFeedToShowMenuCopy(feed);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_SHOW_MENU_COPY, showMenuCopy);
        startActivity(intent, true);
    }

    @Override
    public void navigateToListShare(String url) {
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(this, url, false);
        /*Intent intent = new Intent(this, OnMediaListLikeShareActivity.class);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_DATA, url);
        intent.putExtra(Constants.ONMEDIA.EXTRAS_TYPE_FRAGMENT, Constants.ONMEDIA.LIST_SHARE);
        startActivity(intent, true);*/
    }

    /*@Override
    public void navigateToInviteSmsActivity(String number) {
        String myNumber = mAccountBusiness.getUserNumber(getApplicationContext());
        String smsBody = String.format(getResources().getString(R.string.invite_sms_content), myNumber);
        Intent inviteSms = new Intent(Intent.ACTION_VIEW);
        inviteSms.putExtra("address", number);
        inviteSms.putExtra("sms_body", smsBody);
        inviteSms.setData(Uri.parse("smsto:" + number));
        if (Build.VERSION.SDK_INT > 14) {
            inviteSms.putExtra("finishActivityOnSaveCompleted", true);
        }
        startActivity(inviteSms);
    }*/

    private void displayFragment() {
        if (mType == NumberConstant.TYPE_STRANGER_MOCHA || mType == NumberConstant.TYPE_STRANGER_EXIST) {
            displayStrangerDetailFragment();
        } else if (mType == NumberConstant.TYPE_OFFICIAL_ONMEDIA) {
//            displayOfficialDetailFragment();
            showToast(R.string.e601_error_but_undefined);
            finish();
        } else {
            displayContactDetailFragment();
        }
    }

    /**
     * display Register Fragment
     */
    private void displayContactDetailFragment() {
        if (mType == NumberConstant.TYPE_CONTACT) {
            mPhoneNumber = mContactBusiness.getPhoneNumberFromPhoneId(mPhoneId);
        } else {
            mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(mNumber);
        }
        mNonContact = mContactBusiness.getExistNonContact(mJidNonContact);
        if (mPhoneNumber == null && mNonContact == null) {// danh ba bi xoa khi application bi huy.
            finish();
        } else {
            // Create an instance of newInstance
            if (mPhoneNumber != null) {
                mPhoneName = mPhoneNumber.getName();
                ContactDetailFragmentNew mContactDetailFragment = ContactDetailFragmentNew.
                        newInstance(mPhoneNumber.getId());
                executeFragmentTransaction(mContactDetailFragment, R.id.fragment_container, false, false);
            } else {
                ContactDetailFragmentNew mContactDetailFragment = ContactDetailFragmentNew.
                        newInstanceNonContact(mNonContact.getJidNumber());
                executeFragmentTransaction(mContactDetailFragment, R.id.fragment_container, false, false);
            }
        }
    }

    private void displayStrangerDetailFragment() {
        if (TextUtils.isEmpty(mStrangerJid)) { // so da bi xoa
            finish();
        }
        if (mType == NumberConstant.TYPE_STRANGER_MOCHA) {
            StrangerDetailFragmentNew mStrangerDetailFragment = StrangerDetailFragmentNew.newInstance(
                    mStrangerJid, mPhoneName, mLastChangeAvatar, mStatus, mBirthdayStr, mGender);
            executeFragmentTransaction(mStrangerDetailFragment, R.id.fragment_container, false, false);
        } else {
            StrangerDetailFragmentNew mStrangerDetailFragment = StrangerDetailFragmentNew.
                    newInstance(mStrangerJid, mPhoneName);
            executeFragmentTransaction(mStrangerDetailFragment, R.id.fragment_container, false, false);
        }
    }

    private void displayOfficialDetailFragment() {
        /*if (TextUtils.isEmpty(mNumber)) { // so da bi xoa
            finish();
        }
        OfficialDetailFragment mOfficalFragment = OfficialDetailFragment.
                newInstance(mNumber, mPhoneName, mLastChangeAvatar, userType);
        executeFragmentTransaction(mOfficalFragment, R.id.fragment_container, false, false);*/
    }

    private void findComponentViews() {
        mProgressLoading = (ProgressLoading) findViewById(R.id.progress_loading);
        mApplication = (ApplicationController) this.getApplicationContext();
        mContactBusiness = mApplication.getContactBusiness();
        mMessageBusiness = mApplication.getMessageBusiness();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        mRes = mApplication.getResources();
    }

    //get data
    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        //chua khoi tao xong data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mPhoneId = bundle.getString(NumberConstant.ID);
            mStrangerJid = bundle.getString(StrangerConstant.STRANGER_JID_NUMBER);
            mType = bundle.getInt(NumberConstant.CONTACT_DETAIL_TYPE);
            if (mType == NumberConstant.TYPE_STRANGER_MOCHA || mType == NumberConstant.TYPE_STRANGER_EXIST) {
                mPhoneName = bundle.getString(NumberConstant.NAME);
                mLastChangeAvatar = bundle.getString(NumberConstant.LAST_CHANGE_AVATAR);
                mStatus = bundle.getString(NumberConstant.STATUS);
                mBirthdayStr = bundle.getString(NumberConstant.BIRTHDAY_STRING);
                mGender = bundle.getInt(NumberConstant.GENDER, -1);
            } else if (mType == NumberConstant.TYPE_CONTACT_NOTIFICATION) {
                mNumber = bundle.getString("contact_notification_number");
            } else if (mType == NumberConstant.TYPE_OFFICIAL_ONMEDIA) {
                mPhoneName = bundle.getString(Constants.ONMEDIA.OFFICIAL_NAME);
                mNumber = bundle.getString(Constants.ONMEDIA.OFFICIAL_ID);
                mLastChangeAvatar = bundle.getString(Constants.ONMEDIA.OFFICIAL_AVATAR);
                userType = bundle.getInt(Constants.ONMEDIA.OFFICIAL_USER_TYPE);
            } else {
                mJidNonContact = bundle.getString(NumberConstant.NUMBER);
            }
        } else if (savedInstanceState != null) {
            mPhoneId = savedInstanceState.getString(NumberConstant.ID);
            mStrangerJid = savedInstanceState.getString(StrangerConstant.STRANGER_JID_NUMBER);
            mType = savedInstanceState.getInt(NumberConstant.CONTACT_DETAIL_TYPE);
            mPhoneName = savedInstanceState.getString(NumberConstant.NAME);
            mLastChangeAvatar = savedInstanceState.getString(NumberConstant.LAST_CHANGE_AVATAR);
            mStatus = savedInstanceState.getString(NumberConstant.STATUS);
            mBirthdayStr = savedInstanceState.getString(NumberConstant.BIRTHDAY_STRING);
            mGender = savedInstanceState.getInt(NumberConstant.GENDER, -1);
            mJidNonContact = savedInstanceState.getString(NumberConstant.NUMBER);
            mNumber = savedInstanceState.getString("contact_notification_number");
        }
        //chua load xong du lieu
        if (!mApplication.isDataReady()) {
            mProgressLoading.setVisibility(View.VISIBLE);
            mProgressLoading.setEnabled(true);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            removeFragment(fragment);
        } else {
            mProgressLoading.setVisibility(View.GONE);
            mProgressLoading.setEnabled(false);
            displayFragment();
        }
    }
}
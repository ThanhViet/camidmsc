package com.metfone.selfcare.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.fragment.login.EnterPhoneNumberFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.CheckPhoneInfo;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.backup_restore.restore.DBImporter;
import com.metfone.selfcare.module.backup_restore.restore.RestoreManager;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.login.SelectCountryActivity;
import com.metfone.selfcare.v5.utils.ToastUtils;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.jivesoftware.smack.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.business.UserInfoBusiness.setSystemBarTheme;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

public class EnterAddPhoneNumberActivity extends BaseSlidingFragmentActivity {
    private static final String PHONE_NUMBER_PARAM = "param1";// so nhan dien dc (dau vao)
    private static final String REGION_CODE_PARAM = "param2";
    private static final String NUMBER_JID_PARAM = "number_jid_param";
    public static final String NEED_CHANGE_PARAM = "need_change_color";
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.rlContainer)
    RelativeLayout rlContainer;
    @BindView(R.id.etNumber)
    AppCompatEditText etNumber;
    @BindView(R.id.btnSkip)
    CamIdTextView btnSkip;
    @BindView(R.id.tvpl_enterNumber)
    CamIdTextView tvpl_enterNumber;
    @BindView(R.id.cbTerm)
    AppCompatCheckBox cbTerm;
    @BindView(R.id.btnContinue)
    RoundTextView btnContinue;
    Unbinder unbinder;
    /*@BindView(R.id.country_code)
    Spinner spCountryCode;*/
    @BindView(R.id.tvTryNow)
    CamIdTextView tvTryNow;
    @BindView(R.id.tvTryNowDes)
    CamIdTextView tvTryNowDes;
    @BindView(R.id.llCountryCode)
    LinearLayout llCountryCode;
    @BindView(R.id.ivFlCountry)
    ImageView ivFlCountry;
    @BindView(R.id.tvCountry)
    TextView tvCountry;
    @BindView(R.id.tv_rules)
    CamIdTextView tvRules;
    @BindView(R.id.tvEnterNumberDes)
    CamIdTextView tvEnterNumberDes;
    @BindView(R.id.tvEnterNumber)
    CamIdTextView tvEnterNumber;
    @BindView(R.id.lnl_view)
    LinearLayout lnl_view;
    @BindView(R.id.tv_metfone)
    CamIdTextView mTvMetfone;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    int TYPE;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    private EnterPhoneNumberFragment.OnFragmentInteractionListener fragmentInteractionListener;
    private ApplicationController mApplication;
    private Resources mRes;
    private String mCurrentRegionCode = "VN";// mac dinh vn
    private String mCurrentNumberJid;
    private String phoneNumber;
    private ClickListener.IconListener mClickHandler;
    private boolean isSaveInstanceState = false;
    private boolean isRegisterDone = false;
    private String responseString = "";
    private RegionSpinnerAdapter mRegionAdapter;
    private ArrayList<Region> mRegions;
    private String numberInput;
    private int position;
    private String fromSource = "";
    BaseMPConfirmDialog baseMPConfirmDialog;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private boolean isLoginDone = false;
    UserInfoBusiness userInfoBusiness;
    private boolean needChangeColor = false;
    //--------------------End---------------------

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_phone);
        unbinder = ButterKnife.bind(this);
        userInfoBusiness = new UserInfoBusiness(this);
        mApplication = (ApplicationController) getApplicationContext();
        needChangeColor = getIntent().getBooleanExtra(NEED_CHANGE_PARAM,false);
        setViewListeners();
        initView();
        setSpinnerView();

        tvRules.setText(Html.fromHtml(getString(R.string.agree_term)));
    }

    private void initView() {
        setColorStatusBar(R.color.white);
        etNumber.requestFocus();
//        if (SharedPrefs.getInstance().get(PREF_IS_LOGIN, Boolean.class)) {
//            tvEnterNumberDes.setText("");
//        } else {
            tvEnterNumberDes.setText(getText(R.string.link_your));
//        }
        setupUI(rlContainer);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(etNumber, this);
        mClickHandler = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mClickHandler = this;
        isSaveInstanceState = false;
        enableOrDisableContinueButton();
    //    setWhiteStatusBar();
        UIUtil.showKeyboard(getContext(),etNumber);
        setColor();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PHONE_NUMBER_PARAM, numberInput);
        outState.putString(REGION_CODE_PARAM, mCurrentRegionCode);
        outState.putString(NUMBER_JID_PARAM, mCurrentNumberJid);
        outState.putString(Constants.KEY_POSITION, fromSource);
        super.onSaveInstanceState(outState);
        isSaveInstanceState = true;
    }


    public void updateRegionCode(int index) {
        mRegions.get(position).setSelected(false);

        position = index;
        Region region = mRegions.get(index);
        region.setSelected(true);
        mCurrentRegionCode = region.getRegionCode();
        String patch = "file:///android_asset/fl_country/" + region.getRegionCode().toLowerCase() + ".png";
        Glide.with(this)
                .load(Uri.parse(patch))
                .apply(new RequestOptions()
                        .error(R.color.gray)
                        .fitCenter())
                .into(ivFlCountry);

        String text = region.getRegionName();
        text = text.split("\\(")[1];
        text = text.substring(0, text.length() - 1);
        tvCountry.setText(text);
    }

    /**
     * find all View
     */

    private void setViewListeners() {
        enableOrDisableContinueButton();
        setPhoneNumberEditTextListener();
        setSpinnerItemSelectedListener();
        tvRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UrlConfigHelper.gotoWebViewOnMocha(ApplicationController.self(), EnterAddPhoneNumberActivity.this, "http://mocha.com.vn/vi/provision.html");
            }
        });
    }

    private void setSpinnerView() {
        /*// set current default = vn
        mRegions = mApplication.getLoginBusiness().getAllRegions();
        int position = mApplication.getLoginBusiness().getPositionRegion(mCurrentRegionCode);
        mRegionAdapter = new RegionSpinnerAdapter(mLoginActivity, mRegions);
        spCountryCode.setAdapter(mRegionAdapter);
        spCountryCode.setSelection(position);*/
        mRegions = mApplication.getLoginBusiness().getAllRegions();
        updateRegionCode(35);
    }

    /**
     * listener for change of phoneNumber EditText -> enable or disable button
     */
    private void setPhoneNumberEditTextListener() {
        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                enableOrDisableContinueButton();
            }

            @Override
            public void afterTextChanged(Editable s) {
                mTvMetfone.setVisibility(View.GONE);
            }
        });

        etNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String phone = Objects.requireNonNull(etNumber.getText()).toString().trim();
                    if (TextUtils.isEmpty(phone)) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                    } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                        ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                    } else {
                        if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
                            checkCarrier(etNumber.getText().toString());
                        } else {
                            checkPhone(etNumber.getText().toString());
                        }
                    }
                }
                return false;
            }
        });
    }

    /**
     * enable or disable continue Button
     */
    private void enableOrDisableContinueButton() {
        if (etNumber.getText().toString().trim().length() >= Constants.CONTACT.MIN_LENGTH_NUMBER) {
            setEnableButton(true);
        } else {
            setEnableButton(false);
        }
    }

    private void setEnableButton(boolean enableButton) {
        if (btnContinue != null) {
            btnContinue.setClickable(enableButton);
            if (enableButton) {
                btnContinue.setTextColor(ContextCompat.getColor(this, R.color.v5_text_7));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(this, R.color.button_continue),
                        ContextCompat.getColor(this, R.color.button_continue));
            } else {
                btnContinue.setTextColor(ContextCompat.getColor(this, R.color.v5_text_3));
                btnContinue.setBackgroundColorAndPress(ContextCompat.getColor(this, R.color.v5_cancel),
                        ContextCompat.getColor(this, R.color.v5_cancel));
            }
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_EXIT:
                //                mLoginActivity.finish();
                break;
            default:
                break;
        }
    }

    private void setSpinnerItemSelectedListener() {
        /*spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                mCurrentRegionCode = mRegions.get(position).getRegionCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
    }

    private void setColor() {
        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY
                || ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone() || needChangeColor) {
//            bgaddphonenumber.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_addphone_number));
           changeStatusBar(Color.parseColor("#C41627"));
            drawBackground();
            setSystemBarTheme(EnterAddPhoneNumberActivity.this,true);
//            mTvMetfone.setVisibility(View.VISIBLE);
         //   getWindow().getDecorView().setSystemUiVisibility(View.Sy);
           //  setColorStatusBar(R.color.button_continue);
            tvEnterNumber.setTextColor(Color.WHITE);
            tvEnterNumberDes.setTextColor(Color.WHITE);
            tvpl_enterNumber.setTextColor(Color.WHITE);
//            etNumber.setTextColor(Color.WHITE);
//            lnl_view.setBackgroundColor(Color.parseColor("#1AFFFFFF"));
//            tvCountry.setTextColor(Color.WHITE);
            UserInfoBusiness.setCursorColor(etNumber, Color.WHITE);
            ivBack.setImageResource(R.drawable.ic_navigate_back);
            btnSkip.setVisibility(View.GONE);
//            btnSkip.setTextColor(Color.WHITE);
//            btnSkip.setText(getString(R.string.cancel));
        }
    }

    // xu ly khi gui yc sinh ma
    private void processClickContinue(int checkPhoneCode) {
        Intent intent = new Intent(EnterAddPhoneNumberActivity.this, OTPAddPhoneLoginActivity.class);
        String phoneNumber = Objects.requireNonNull(etNumber.getText()).toString();
        if (phoneNumber.startsWith("0")) {
            intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, phoneNumber);
        } else {
            intent.putExtra(EnumUtils.PHONE_NUMBER_KEY, "0" + phoneNumber);
        }
        intent.putExtra(EnumUtils.KEY_TYPE, TYPE);
        intent.putExtra(EnumUtils.CHECK_PHONE_CODE_TYPE, checkPhoneCode);
        startActivityForResult(intent, EnumUtils.MY_REQUEST_CODE);
    }

    private void drawBackground() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            rlContainer.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        } else {
            rlContainer.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_frg_set_up_information));
        }
    }

    @OnClick({R.id.cbTerm, R.id.btnContinue, R.id.ivBack, R.id.tvTryNow, R.id.btnSkip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cbTerm:
                break;
            case R.id.btnContinue:
                String phone = etNumber.getText().toString().trim();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_must_be_not_empty));
                } else if (phone.length() < 8 || phone.length() > 14 || !UserInfoBusiness.isPhoneNumberValid(phone, "KH")) {
                    ToastUtils.showToast(getContext(), getString(R.string.phone_number_is_not_valid));
                } else {
                    if (ApplicationController.self().getCamIdUserBusiness().isProcessingLoginSignUpMetfone()) {
                        checkCarrier(etNumber.getText().toString());
                    } else {
                        checkPhone(etNumber.getText().toString());
                    }
                }
                break;
            case R.id.ivBack:
                DeepLinkHelper.uriDeepLink = null;
                ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
                finish();
                break;
            case R.id.btnSkip:
//                if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.SET_UP_PROFILE_ACTIVITY) {
                finish();
//                } else {
//                    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(EnterAddPhoneNumberActivity.this);
//                    String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
//                    UserInfo userInfo = userInfoBusiness.getUser();
//                    mCurrentRegionCode = "KH";
//                    if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
//                         if (userInfo.getPhone_number().startsWith("0")) {
//                            mCurrentNumberJid = "+855"+userInfo.getPhone_number().substring(1);
//                        } else {
//                            mCurrentNumberJid = "+855"+userInfo.getPhone_number();
//                        }
//                        String originToken = token.substring(7);
//                        generateOldMochaOTP(originToken);
//                    } else {
////                    ToastUtils.showToast(AddPhoneNumberActivity.this, "Phone number null -> Login old mocha failed!");
//                        NavigateActivityHelper.navigateToHomeScreenActivity(EnterAddPhoneNumberActivity.this, true, true);
//
//                    }
//                    getUserInformation();
//                }

                break;
        }
    }


    private void checkPhone(String phoneNumber) {

        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        CheckPhoneInfo checkPhoneInfo = new CheckPhoneInfo(phoneNumber);

        CheckPhoneRequest checkPhoneRequest = new CheckPhoneRequest(checkPhoneInfo, "", "", "", "");
        apiService.checkPhone(checkPhoneRequest).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, retrofit2.Response<BaseResponse<String>> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(),etNumber);
                    }
                }, 200);
                if (response.body() != null) {
                    if (response.body().getCode().equals("00") || response.body().getCode().equals("42") || response.body().getCode().equals("68")) {
                        //case exist in camID
                        baseMPConfirmDialog = new BaseMPConfirmDialog(R.drawable.iv_camid_account,
                                R.string.m_p_dialog_combine_phone_title,
                                R.string.m_p_dialog_combine_phone_content,
                                R.string.m_p_dialog_combine_phone_top_btn,
                                R.string.m_p_dialog_combine_phone_bottom_btn,
                                view -> {
                                    //letdoit
                                    processClickContinue(42);
                                    if (baseMPConfirmDialog != null) {
                                        baseMPConfirmDialog.dismiss();
                                    }
                                }, view -> {
                            //back

                            if (baseMPConfirmDialog != null) {
                                baseMPConfirmDialog.dismiss();
                            }
                        });
                        baseMPConfirmDialog.setBgWhite(false);
                        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
                            baseMPConfirmDialog.setBgWhite(true);
                        }
                        baseMPConfirmDialog.show(getSupportFragmentManager(), null);
                        //case camid social
                    }
//                    else if (response.body().getCode().equals("49")) {
//                        baseMPSuccessDialog = new BaseMPSuccessDialog(EnterAddPhoneNumberActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), String.format(getString(R.string.content_error_social_combine), phoneNumber), false);
//                        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
//                            baseMPSuccessDialog.setBgWhite(true);
//                        }
//                        baseMPSuccessDialog.show();
//                        getSupportFragmentManager().executePendingTransactions();
//                        if (baseMPSuccessDialog != null) {
//                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialogInterface) {
//                                    EnterAddPhoneNumberActivity.this.finish();
//                                }
//                            });
//                        }
//
//                        //21,24,41,43,45,64: Call api add login và hiển thị popup success
//                    } else if (response.body().getCode().equals("21") || response.body().getCode().equals("24") || response.body().getCode().equals("41") || response.body().getCode().equals("43") || response.body().getCode().equals("45") || response.body().getCode().equals("64")) {
//                        processClickContinue(0);
//                    }
                    else {
                        processClickContinue(0);
//                        baseMPSuccessDialog = new BaseMPSuccessDialog(EnterAddPhoneNumberActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), response.body().getMessage(), false);
//                        if (EnumUtils.FROM_ADD_LOGIN_MODE == EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY) {
//                            baseMPSuccessDialog.setBgWhite(true);
//                        }
//                        baseMPSuccessDialog.show();
//                        getSupportFragmentManager().executePendingTransactions();
//                        if (baseMPSuccessDialog != null) {
//                            baseMPSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialogInterface) {
//                                    EnterAddPhoneNumberActivity.this.finish();
//                                }
//                            });
//                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(EnterAddPhoneNumberActivity.this, getString(R.string.service_error));
            }
        });
    }

    @OnClick(R.id.llCountryCode)
    public void onViewClicked() {
        SelectCountryActivity.startActivityForResult(this, position, LoginActivity.REQUEST_CODE_COUNTRY);
    }

    private void getUserInformation() {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        FireBaseHelper.getInstance(ApplicationController.self()).checkServiceAndRegister(true);
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(EnterAddPhoneNumberActivity.this);
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
                        mCurrentRegionCode = "KH";
                        if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                            if (userInfo.getPhone_number().startsWith("0")) {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number().substring(1);
                            } else {
                                mCurrentNumberJid = "+855" + userInfo.getPhone_number();
                            }
                            String originToken = token.substring(7);
                            generateOldMochaOTP(originToken);
                        } else {
                            NavigateActivityHelper.navigateToHomeScreenActivity(EnterAddPhoneNumberActivity.this, false, true);
                        }
                    } else {
                        ToastUtils.showToast(EnterAddPhoneNumberActivity.this, response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(EnterAddPhoneNumberActivity.this, "Get user information fail");
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(EnterAddPhoneNumberActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void generateOldMochaOTP(String token) {
        if(pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
        String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(EnterAddPhoneNumberActivity.this);
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(EnterAddPhoneNumberActivity.this).detectSubscription(json);
                   doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(EnterAddPhoneNumberActivity.this, response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(EnterAddPhoneNumberActivity.this, "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(EnterAddPhoneNumberActivity.this, error.getMessage());
            }
        });

    }

    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(EnterAddPhoneNumberActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof RoundTextView || innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new EnterAddPhoneNumberActivity.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(pbLoading != null)
                pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);

            try {
//                mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    RestoreManager.setRestoring(true);
                    userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
                        @Override
                        public void onStartDownload() {

                        }

                        @Override
                        public void onDownloadProgress(int percent) {

                        }

                        @Override
                        public void onDownloadComplete() {

                        }

                        @Override
                        public void onDowloadFail(String message) {

                        }

                        @Override
                        public void onStartRestore() {

                        }

                        @Override
                        public void onRestoreProgress(int percent) {

                        }

                        @Override
                        public void onRestoreComplete(int messageCount, int threadMessageCount) {
                            try {
                                SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
                                if (pref != null) {
                                    pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
                                    com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
                                }
                            } catch (Exception e) {
                            }
                            RestoreManager.setRestoring(false);
                            mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
                            mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
                            NavigateActivityHelper.navigateToHomeScreenActivity(EnterAddPhoneNumberActivity.this, false, true);
                        }

                        @Override
                        public void onRestoreFail(String message) {
                            RestoreManager.setRestoring(false);
                            NavigateActivityHelper.navigateToHomeScreenActivity(EnterAddPhoneNumberActivity.this, false, true);
                        }
                    }, EnterAddPhoneNumberActivity.this);

//                    String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
//                    String otp = getArguments().getString(EnumUtils.OTP_KEY);
//                    String pass = edtPassWord.getEditText().getText().toString();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
//                    if (tvError != null) {
//                        tvError.setVisibility(View.GONE);
//                    }
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
                    if(pbLoading != null)
                        pbLoading.setVisibility(View.GONE);
//                    showError(responseCode.getDescription(), null);
//                    if (tvError != null) {
//                        tvError.setVisibility(View.VISIBLE);
//                    }
                    ToastUtils.showToast(EnterAddPhoneNumberActivity.this, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
                if(pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
            }
        }
    }

    private void checkCarrier(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.equals("")) return;
        RetrofitInstance retrofitInstance = new RetrofitInstance();
        retrofitInstance.checkCarrier(phoneNumber, new ApiCallback<CheckCarrierResponse>() {
            @Override
            public void onResponse(Response<CheckCarrierResponse> response) {
                if (response.body() != null) {
                    String code = response.body().getCode();
                    Log.d(TAG, "onResponse: " + response.body().getMessage());

                    if ("00".equals(code)) {
                        if (response.body().getData().getMetfone()) {
                            mTvMetfone.setVisibility(View.GONE);
                            checkPhone(phoneNumber);
                        } else {
                            mTvMetfone.setVisibility(View.VISIBLE);
                        }
                    } else {
//                        mTvMetfone.setText(View.VISIBLE);
                        /*
                        * fix bug firebase: String resource ID #0x0
                        * */
                        mTvMetfone.setVisibility(View.VISIBLE);
                        mTvMetfone.setText(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "onError: ", error);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DeepLinkHelper.uriDeepLink = null;
        ApplicationController.self().getCamIdUserBusiness().setProcessingLoginSignUpMetfone(false);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EnumUtils.MY_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            int result= data.getIntExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
            Intent returnIntent = new Intent();
            android.util.Log.d(TAG, "onActivityResult: " + result);
            returnIntent.putExtra(EnumUtils.RESULT, result);
            setResult(Activity.RESULT_OK, returnIntent);
            if (result ==  EnumUtils.KeyBackTypeDef.DONE){
                finish();
            }
        }
    }
}
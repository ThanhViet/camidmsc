package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.ItemInfo;
import com.metfone.selfcare.database.model.avno.PackageAVNO;
import com.metfone.selfcare.fragment.avno.AVNOFragment;
import com.metfone.selfcare.fragment.avno.CalloutGlobalFragment;
import com.metfone.selfcare.fragment.avno.ListNumberAvailableFragment;
import com.metfone.selfcare.fragment.avno.PackageAVNODetailFragment;
import com.metfone.selfcare.fragment.avno.PreSelectNumberAVNOFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 2/24/2018.
 */

public class AVNOActivity extends BaseSlidingFragmentActivity {

    private static final String TAG = AVNOActivity.class.getSimpleName();

    public static final String CALL_BACK_SUCCESS = "call_back";
    public static final int GOTO_EDIT_PROFILE = 0;
    public static final int FINISH_ACTIVITY = 1;

    public static final String FRAGMENT_DATA = "data";
    public static final int FRAGMENT_AVNO = 0;
    public static final int FRAGMENT_MANAGER_CALLOUT = 1;
    public static final int FRAGMENT_LIST_NUMBER = 2;

    private ApplicationController mApplication;
    private ListNumberAvailableFragment mFragmentListNumber;
    private CalloutGlobalFragment mFragmentAVNOManagerV3;
    private AVNOFragment mFragmentAVNO;
//    private SearchNumberAVNOFragment mFragmentSearch;

    private int callBackSuccess;

    private int framentData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, " onCreate ... ");
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplicationContext();
        callBackSuccess = getIntent().getIntExtra(CALL_BACK_SUCCESS, 0);
        framentData = getIntent().getIntExtra(FRAGMENT_DATA, 0);
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
//        setToolBar(findViewById(R.id.tool_bar));
        findViewById(R.id.tool_bar).setVisibility(View.GONE);
        if (framentData == FRAGMENT_AVNO && mApplication.getReengAccountBusiness().isAvnoEnable()) {
            navigateToAVNOFragment();
        } else if (framentData == FRAGMENT_LIST_NUMBER) {
            navigateToListNumberAvailable(false);
        } else {
            navigateToManagerCallout();
        }
        trackingScreen(TAG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.CHOOSE_CONTACT.TYPE_SET_CALLED_LIMITED) {
                if (mFragmentAVNO != null && mFragmentAVNO.isVisible()) {
                    mFragmentAVNO.getDataAVNO();
                } else if (mFragmentAVNOManagerV3 != null && mFragmentAVNOManagerV3.isVisible()) {
                    mFragmentAVNOManagerV3.getDataAVNO();
                }
            }
        }
    }

    public void navigateToListNumberAvailable(boolean addToBackStack) {
        mFragmentListNumber = ListNumberAvailableFragment.newInstance(callBackSuccess);
        executeFragmentTransaction(mFragmentListNumber, R.id.fragment_container, addToBackStack, false);
    }


    public void navigateToManagerCallout() {
        mFragmentAVNOManagerV3 = CalloutGlobalFragment.newInstance();
        executeFragmentTransaction(mFragmentAVNOManagerV3, R.id.fragment_container, false, false);
    }

    public void navigateToAVNOFragment() {
        mFragmentAVNO = AVNOFragment.newInstance();
        executeFragmentTransaction(mFragmentAVNO, R.id.fragment_container, false, false);
    }

    public void navigateToPreSelectNumberAVNOFragment(ArrayList<ItemInfo> itemInfos) {
        PreSelectNumberAVNOFragment fragment = PreSelectNumberAVNOFragment.newInstance(itemInfos);
        executeAddFragmentTransaction(fragment, R.id.fragment_container, true, false);
    }

    public void navigateToPackageAVNOFragment(PackageAVNO packageAVNO) {
        PackageAVNODetailFragment fragment = PackageAVNODetailFragment.newInstance(packageAVNO);
        executeAddFragmentTransaction(fragment, R.id.fragment_container, true, false);
    }

    /*public void navigateToSearchNumber() {
        mFragmentSearch = SearchNumberAVNOFragment.newInstance();
        executeAddFragmentTransaction(mFragmentSearch, R.id.fragment_container, true, false);
    }*/

    @Override
    public void onBackPressed() {
        /*if (mFragmentSearch != null && mFragmentSearch.isVisible()) {
            restoreToolbarListNumber();
        }*/
        super.onBackPressed();
    }

    /*public void restoreToolbarListNumber() {
        if (mFragmentListNumber != null) {
            mFragmentListNumber.setToolbar();
        }
    }*/


    public static class AVNOManagerMessage {

    }
}

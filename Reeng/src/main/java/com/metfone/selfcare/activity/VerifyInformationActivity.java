package com.metfone.selfcare.activity;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_AVATAR_FILE_CROP;
import static com.metfone.selfcare.util.contactintergation.OpenIDErrorCode.ERROR_UNAUTHORIZED;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.aigestudio.wheelpicker.WheelPicker;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.TransferFileBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ImageProfileConstant;
import com.metfone.selfcare.fragment.setting.EditProfileFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.DetectInfo;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.account.VerifyInfo;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPErrorDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDetectORCResponse;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyInformationActivity extends BaseSlidingFragmentActivity {
    Unbinder unbinder;
    @BindView(R.id.contrainLayout)
    ConstraintLayout clcontrainLayout;
    @BindView(R.id.clFrontSize)
    ConstraintLayout clFrontSize;
    @BindView(R.id.clBackSize)
    ConstraintLayout clBackSize;
    @BindView(R.id.clYourSelfie)
    ConstraintLayout clYourSelfie;
    @BindView(R.id.ivPhotoFS)
    ImageView ivPhotoFS;
    @BindView(R.id.ivTakePhotoFS)
    ImageView ivTakePhotoFS;
    @BindView(R.id.tvTakePhotoFS)
    CamIdTextView tvTakePhotoFS;
    @BindView(R.id.tvUploadPhotoFS)
    CamIdTextView tvUploadPhotoFS;
    @BindView(R.id.ivPhotoBS)
    ImageView ivPhotoBS;
    @BindView(R.id.ivTakePhotoBS)
    ImageView ivTakePhotoBS;
    @BindView(R.id.tvTakePhotoBS)
    CamIdTextView tvTakePhotoBS;
    @BindView(R.id.tvUploadPhotoBS)
    CamIdTextView tvUploadPhotoBS;
    @BindView(R.id.ivPhotoYS)
    ImageView ivPhotoYS;
    @BindView(R.id.ivTakePhotoYS)
    ImageView ivTakePhotoYS;
    @BindView(R.id.tvTakePhotoYS)
    CamIdTextView tvTakePhotoYS;
    @BindView(R.id.tvUploadPhotoYS)
    CamIdTextView tvUploadPhotoYS;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.btnConfirm)
    CamIdTextView btnConfirm;
    @BindView(R.id.edtIDType)
    CamIdTextView edtIDType;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    List<String> data, idData;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private TransferFileBusiness transferFileBusiness;
    private String filePathFS;
    private String filePathBS;
    private String filePathYS;
    private int uploadOption = ImageProfileConstant.IMAGE_AVATAR;
    private int typeUploadImage = ImageProfileConstant.IMAGE_AVATAR;
    private int typeTakePhoto;
    private String typePhoto;
    private BottomSheetDialog bottomSheetDialog;
    private UserInfoBusiness userInfoBusiness;
    private UserInfo currentUser;
    private DetectInfo currentDetectInfo;

    View.OnClickListener setBottomSheetItemClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvTakeAPhoto:
                    takeAPhoto(uploadOption);
                    break;
                case R.id.tvSelectFromGallery:
                    openGallery(uploadOption);
                    break;
            }
            bottomSheetDialog.dismiss();

        }
    };
    private BaseMPErrorDialog baseMPErrorDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_information);
        unbinder = ButterKnife.bind(this);
        changeStatusBar(Color.parseColor("#1F1F1F"));
        mApplication = (ApplicationController) getApplication();
        mAccountBusiness = mApplication.getReengAccountBusiness();
        userInfoBusiness = new UserInfoBusiness(this);
        currentUser = userInfoBusiness.getUser();
        transferFileBusiness = new TransferFileBusiness(mApplication);
        initViews();
    }

    private void initViews() {
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        data = Arrays.asList(getResources().getStringArray(R.array.selected_id_option));
        idData = Arrays.asList(getResources().getStringArray(R.array.selected_id_type_option));
    }

    @OnClick({R.id.clFrontSize, R.id.clBackSize, R.id.clYourSelfie, R.id.btnBack, R.id.btnConfirm, R.id.edtIDType})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.edtIDType:
                showBottomSheetDialogFragment();
                break;
            case R.id.clFrontSize:
                typePhoto = EnumUtils.VerifyImageTypeDef.FRONT_SIDE;
                showPopupChangeAvatar();
                break;
            case R.id.clBackSize:
                typePhoto = EnumUtils.VerifyImageTypeDef.BACK_SIDE;
                showPopupChangeAvatar();
                break;
            case R.id.clYourSelfie:
                typePhoto = EnumUtils.VerifyImageTypeDef.YOUR_SELFIE;
                showPopupChangeAvatar();
                break;
            case R.id.btnConfirm:
                if (edtIDType.getText().toString().trim().equals(getString(R.string.select_id_type))) {
                    Toast.makeText(this, getResources().getString(R.string.select_id_type), Toast.LENGTH_SHORT).show();
                } else {
                    String type = edtIDType.getText().toString();
                    String idType = idData.get(data.indexOf(type));
                    String imageEncodeFS = "";
                    String imageEncodeBS = "";
                    String imageEncodeYS = "";
                    imageEncodeFS = ImageUtils.getFileToByte(filePathFS);
                    imageEncodeBS = ImageUtils.getFileToByte(filePathBS);
                    imageEncodeYS = ImageUtils.getFileToByte(filePathYS);
                    mAccountBusiness.removeFileExternalDir(this);

                    if (!TextUtils.isEmpty(imageEncodeFS) && !TextUtils.isEmpty(imageEncodeBS) && !TextUtils.isEmpty(imageEncodeYS)) {
                        VerifyInfo verifyInfo = new VerifyInfo(imageEncodeFS, imageEncodeBS, imageEncodeYS, idType);
                        verifyInformation(verifyInfo);
                    } else {
                        ToastUtils.showToast(VerifyInformationActivity.this, getString(R.string.error));
                    }
                }

                break;
            case R.id.btnBack:
                finish();
                break;
        }
    }

    private void verifyInformation(VerifyInfo verifyInfo) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.verifyID(token, new BaseDataRequest<>(verifyInfo, "string", "string", "string", "string")).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.code() == ERROR_UNAUTHORIZED) {
                    restartApp();
                    return;
                }
                if (response.body() != null) {
                    //comment to test
                    if ("00".equals(response.body().getCode())) {
                        NavigateActivityHelper.navigateToFullFillInfo(
                                VerifyInformationActivity.this, currentDetectInfo);
                    } else {
                        ToastUtils.showToast(VerifyInformationActivity.this,
                                response.body().getMessage());
                    }
                } else {
                    ToastUtils.showToast(VerifyInformationActivity.this,
                            getString(R.string.service_error));
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(VerifyInformationActivity.this, getString(R.string.service_error));
            }
        });
    }

    private void showPopupChangeAvatar() {
        bottomSheetDialog = new BottomSheetDialog(this, R.style.NewBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.fragment_botton_sheet_dialog_verify);
        TextView tvTakeAPhoto = bottomSheetDialog.findViewById(R.id.tvTakeAPhoto);
        TextView tvSelectFromGallery = bottomSheetDialog.findViewById(R.id.tvSelectFromGallery);
        tvTakeAPhoto.setOnClickListener(setBottomSheetItemClick);
        tvSelectFromGallery.setOnClickListener(setBottomSheetItemClick);
        bottomSheetDialog.setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            CardView bottomSheet = (CardView) d.findViewById(R.id.cvContent);
            if (bottomSheet == null)
                return;
            bottomSheet.setBackground(null);
        });
        bottomSheetDialog.show();

    }

    public void takeAPhoto(int typeUpload) {
        typeTakePhoto = typeUpload;
        if (PermissionHelper.declinedPermission(this, Manifest.permission.CAMERA)) {
            PermissionHelper.requestPermissionWithGuide(this,
                    Manifest.permission.CAMERA,
                    Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
        } else {
            dispatchTakePictureIntent(typeUpload);
        }
    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent(int typeUpload) {
        typeUploadImage = typeUpload;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, currentPhotoPath).apply();
            } catch (IOException ex) {
                // Error occurred while creating the File
                android.util.Log.e(TAG, "IOException: ");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileHelper.fromFile(mApplication, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constants.ACTION.ACTION_TAKE_PHOTO);
            }
        }
    }

    public void openGallery(int typeUpload) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    public void openGallery(int typeUpload, int maxImages) {
        typeUploadImage = typeUpload;
        mAccountBusiness.removeFileFromProfileDir();
        Intent i = new Intent(getApplicationContext(), ImageBrowserActivity.class);
        if (typeUploadImage == ImageProfileConstant.IMAGE_NORMAL)
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_MULTIPLE_PICK);
        else
            i.putExtra(ImageBrowserActivity.PARAM_ACTION, ImageBrowserActivity.ACTION_SIMPLE_PICK);
        i.putExtra(ImageBrowserActivity.PARAM_PATH_ROOT, "/");
        i.putExtra(ImageBrowserActivity.PARAM_ACCEPT_TEXT, getResources().getString(R.string.action_done));
        i.putExtra(ImageBrowserActivity.PARAM_SHOW_TAKE_GALLERY, 0);
        i.putExtra(ImageBrowserActivity.PARAM_CROP_SIZE, 0);
        i.putExtra(ImageBrowserActivity.PARAM_MAX_IMAGES, maxImages);
        startActivityForResult(i, Constants.ACTION.ACTION_PICK_PICTURE);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                //success update info
                case EnumUtils.MY_REQUEST_CODE:
                    if (data != null) {
                        int result = data.getIntExtra(EnumUtils.RESULT, EnumUtils.KeyBackTypeDef.BACK);
                        Intent returnIntent = new Intent();
                        android.util.Log.d(TAG, "onActivityResult: " + result);
                        returnIntent.putExtra(EnumUtils.RESULT, result);
                        setResult(Activity.RESULT_OK, returnIntent);
                        if (result == EnumUtils.KeyBackTypeDef.DONE) {
                            finish();
                        }
                    }
                    break;
                // get information from facebook
                case Constants.ACTION.ACTION_PICK_PICTURE:
                    if (data != null) {
                        Log.d(TAG, "onActivityResult ACTION_PICK_PICTURE");
                        ArrayList<String> picturePath = data.getStringArrayListExtra("data");
                        if (picturePath != null && picturePath.size() > 0) {
                            if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                                String pathImage = picturePath.get(0);
                                resizeImage(pathImage);
                                setAvatarEditProfile(pathImage);
                                if (typePhoto != null && typePhoto.equals(EnumUtils.VerifyImageTypeDef.FRONT_SIDE)) {
                                    String type = edtIDType.getText().toString();
                                    if (type.toLowerCase(Locale.ROOT).equals(
                                            getString(R.string.cambodia_id).toLowerCase(Locale.ROOT))
                                            || type.toLowerCase(Locale.ROOT).equals(
                                            getString(R.string.passport_card).toLowerCase(Locale.ROOT))
                                    ) {
                                        detectOCRImage(type, pathImage);
                                    }
                                }
                            } else if (typeUploadImage == ImageProfileConstant.IMAGE_IC_FRONT ||
                                    typeUploadImage == ImageProfileConstant.IMAGE_IC_BACK) {
                                uploadImageIdentifyCard(picturePath.get(0), typeUploadImage);
                            }
                        }
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_TAKE_PHOTO:
                    File imageFile = new File(mApplication.getPref().getString(Constants.PREFERENCE.PREF_AVATAR_FILE_CAPTURE, ""));
                    if (typeUploadImage == ImageProfileConstant.IMAGE_AVATAR) {
                        resizeImage(imageFile.getPath());
                        setAvatarEditProfile(imageFile.getPath());
                        if (typePhoto != null && typePhoto.equals(EnumUtils.VerifyImageTypeDef.FRONT_SIDE)) {
                            String type = edtIDType.getText().toString();
                            if (type.toLowerCase(Locale.ROOT).equals(
                                    getString(R.string.cambodia_id).toLowerCase(Locale.ROOT))
                                    || type.toLowerCase(Locale.ROOT).equals(
                                    getString(R.string.passport_card).toLowerCase(Locale.ROOT))
                            ) {
                                detectOCRImage(type, imageFile.getPath());
                            }
                        }
                    } else if (typeUploadImage == ImageProfileConstant.IMAGE_IC_FRONT ||
                            typeUploadImage == ImageProfileConstant.IMAGE_IC_BACK) {
                        uploadImageIdentifyCard(imageFile.getPath(), typeUploadImage);
                    }
                    setTakePhotoAndCrop(false);
                    setActivityForResult(false);
                    break;
                case Constants.ACTION.ACTION_CROP_IMAGE:
                    if (data != null) {
                        String fileCrop = mApplication.getPref().getString(PREF_AVATAR_FILE_CROP, "");
                        resizeImage(fileCrop);
                        setAvatarEditProfile(fileCrop);
                    }
                    break;


                default:
                    break;
            }
        } else {
            setActivityForResult(false);
            setTakePhotoAndCrop(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void resizeImage(String pathImage) {
        File file = new File(pathImage);
        if (file.exists()) {
            int fileSize1 = Integer.parseInt(String.valueOf(file.length() / 1024));
            file = ImageUtils.saveBitmapToFile(file);
            int fileSize = Integer.parseInt(String.valueOf(file.length() / 1024));
            android.util.Log.e("ttt", "onActivityResult: \n path: " + pathImage + "\n size1: " + fileSize1 + "\n size2: " + fileSize);
        }
    }

    public void setAvatarEditProfile(String filePath) {
        Log.i(TAG, "setAvatarEditProfile");
        drawPhoto(filePath);
    }

    private void drawPhoto(String filePath) {
        if (Objects.equals(typePhoto, EnumUtils.VerifyImageTypeDef.FRONT_SIDE)) {
            ImageUtils.loadImageView(this, ivPhotoFS, filePath);
            filePathFS = filePath;
            ivPhotoFS.setVisibility(View.VISIBLE);
            ivTakePhotoFS.setVisibility(View.GONE);
            tvTakePhotoFS.setVisibility(View.GONE);
            tvUploadPhotoFS.setVisibility(View.GONE);
            clFrontSize.setBackgroundColor(Color.parseColor("#1F1F1F"));
            if (ivPhotoBS.getVisibility() == View.VISIBLE && ivPhotoYS.getVisibility() == View.VISIBLE) {
                btnConfirm.setEnabled(true);
                btnConfirm.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_confirm_information));
            }
        } else if (Objects.equals(typePhoto, EnumUtils.VerifyImageTypeDef.BACK_SIDE)) {
            ImageUtils.loadImageView(this, ivPhotoBS, filePath);
            filePathBS = filePath;
            ivPhotoBS.setVisibility(View.VISIBLE);
            ivTakePhotoBS.setVisibility(View.GONE);
            tvTakePhotoBS.setVisibility(View.GONE);
            tvUploadPhotoBS.setVisibility(View.GONE);
            clBackSize.setBackgroundColor(Color.parseColor("#1F1F1F"));
            if (ivPhotoFS.getVisibility() == View.VISIBLE && ivPhotoYS.getVisibility() == View.VISIBLE) {
                btnConfirm.setEnabled(true);
                btnConfirm.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_confirm_information));
            }
        } else {
            ImageUtils.loadImageView(this, ivPhotoYS, filePath);
            filePathYS = filePath;
            ivPhotoYS.setVisibility(View.VISIBLE);
            ivTakePhotoYS.setVisibility(View.GONE);
            tvTakePhotoYS.setVisibility(View.GONE);
            tvUploadPhotoYS.setVisibility(View.GONE);
            clYourSelfie.setBackgroundColor(Color.parseColor("#1F1F1F"));
            if (ivPhotoBS.getVisibility() == View.VISIBLE && ivPhotoFS.getVisibility() == View.VISIBLE) {
                btnConfirm.setEnabled(true);
                btnConfirm.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_confirm_information));
            }
        }
    }

    public void uploadImageIdentifyCard(String filepath, int uploadOption) {
        if (!NetworkHelper.isConnectInternet(mApplication)) {
            showToast(R.string.no_connectivity);
            return;
        }
        if (uploadOption == ImageProfileConstant.IMAGE_IC_FRONT) {
            transferFileBusiness.uploadIdentityCard(filepath, true, new EditProfileFragment.UploadIdentifyCardListener() {
                @Override
                public void onUploadSuccess(String link) {
                    String newlink = link + "?t=" + System.currentTimeMillis();
                    Log.i(TAG, "newlink: " + newlink);
//                    visibleHolderFront(false);
                    Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                            + newlink).into(ivPhotoFS);
                    mAccountBusiness.updateAVNOIdenfityCardFront(newlink);
                }

                @Override
                public void onError(int code, String msg) {
                    showToast(msg);
                }
            });
        } else {
            transferFileBusiness.uploadIdentityCard(filepath, false, new EditProfileFragment.UploadIdentifyCardListener() {
                @Override
                public void onUploadSuccess(String link) {
                    String newlink = link + "?t=" + System.currentTimeMillis();
                    Log.i(TAG, "newlink: " + newlink);
//                    visibleHolderBack(false);
                    Glide.with(mApplication).load(UrlConfigHelper.getInstance(mApplication).getDomainFile()
                            + newlink).into(ivPhotoBS);
                    mAccountBusiness.updateAVNOIdenfityCardBack(newlink);
                }

                @Override
                public void onError(int code, String msg) {
                    showToast(msg);
                }
            });
        }
    }

    public void setEdtIDType(String text) {
        edtIDType.setText(text);
    }

    public void showBottomSheetDialogFragment() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this, R.style.NewBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.fragment_bottom_sheet_wheel_picker);
        //
        WheelPicker wheelPicker = bottomSheetDialog.findViewById(R.id.wheelPicker);
        TextView tvDone = bottomSheetDialog.findViewById(R.id.tvDone);
        if (wheelPicker != null) {
            wheelPicker.setCyclic(false);
            wheelPicker.setSelectedItemPosition(data.indexOf(edtIDType.getText().toString()), false);
        }
        if (tvDone != null) {
            tvDone.setOnClickListener(view -> {
                if (wheelPicker != null) {
                    String currentType = edtIDType.getText().toString();
                    String typeChanged = data.get(wheelPicker.getCurrentItemPosition());
                    setEdtIDType(typeChanged);
                    if (filePathFS != null && !currentType.equals(typeChanged) &&
                            (typeChanged.toLowerCase(Locale.ROOT).equals(
                                    getString(R.string.cambodia_id).toLowerCase(Locale.ROOT))
                                    || typeChanged.toLowerCase(Locale.ROOT).equals(
                                    getString(R.string.passport_card).toLowerCase(Locale.ROOT)))
                    ) {
                        detectOCRImage(typeChanged, filePathFS);
                    }
                }
                bottomSheetDialog.dismiss();
            });
        }
        //
        bottomSheetDialog.setOnShowListener(dialog -> {
            BottomSheetDialog d = (BottomSheetDialog) dialog;
            CardView bottomSheet = (CardView) d.findViewById(R.id.cvContent);
            if (bottomSheet == null)
                return;
            bottomSheet.setBackground(null);
        });
        bottomSheetDialog.show();
        //
    }

    private void detectOCRImage(String type, String filePathFS) {
        pbLoading.setVisibility(View.VISIBLE);
        String base64 = "";
        if (filePathFS != null && !filePathFS.isEmpty()) {
            base64 = ImageUtils.getFileToByte(filePathFS);
            android.util.Log.e("ttt", "drawPhoto: number " + base64.length());
        }

        String id = "1";
        if (type.toLowerCase(Locale.ROOT).equals(
                getString(R.string.passport_card).toLowerCase(Locale.ROOT))
        ) {
            id = "3";
        }
        new MetfonePlusClient().wsDetectORCImage(id, base64, new MPApiCallback<WsDetectORCResponse>() {
            @Override
            public void onResponse(Response<WsDetectORCResponse> response) {
                pbLoading.setVisibility(View.GONE);
                if (response.body() != null && response.body().getResult() != null) {
                    WsDetectORCResponse.Response wsResponse = response.body().getResult().getWsResponse();
                    if (wsResponse != null) {
                        if ("0".equals(wsResponse.getErrorCode())) {
                            DetectInfo detectInfo = new DetectInfo();
                            detectInfo.setDob(wsResponse.getDobEn());
                            detectInfo.setIdNumber(wsResponse.getIdNumber());
                            detectInfo.setAddress(wsResponse.getProvinceEn());
                            detectInfo.setSex(wsResponse.getSexEn());
                            detectInfo.setFullname(wsResponse.getName());
                            detectInfo.setExpireDate(wsResponse.getExpireDate());
                            detectInfo.setNationality(wsResponse.getNationality());
//                            showDialogDetectConfirm(detectInfo);
                            currentDetectInfo = detectInfo;
                        } else {
                            showDialogError();
                        }
                    } else {
                        showDialogError();
                    }
                } else {
                    showDialogError();
                }
            }

            @Override
            public void onError(Throwable error) {
                pbLoading.setVisibility(View.GONE);
                showDialogError();
            }
        });
    }

    private void showDialogError() {
        baseMPErrorDialog = new BaseMPErrorDialog(this,
                R.drawable.image_error_dialog,
                getString(R.string.text_error_detect_ocr_fail),
                getString(R.string.empty_string),
                true,
                R.string.btn_try_again,
                v -> {
                    baseMPErrorDialog.dismiss();
                });
        baseMPErrorDialog.show();
    }

//    private void showDialogDetectConfirm(DetectInfo detectInfo) {
//        DialogConfirmDetectInfo dialogConfirm = DialogConfirmDetectInfo.newInstance(detectInfo);
//        dialogConfirm.setInterface((detectInfo1) -> {
//            currentDetectInfo = detectInfo1;
//            android.util.Log.e("ttt", "showDialogDetectConfirm detect user =" + detectInfo1);
//        });
//        dialogConfirm.show(getSupportFragmentManager(), "detect");
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}

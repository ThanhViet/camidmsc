package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.fragment.message.ChooseDocumentFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.util.Log;

/**
 * Created by Toanvk2 on 5/6/2017.
 */

public class ChooseDocumentActivity extends BaseSlidingFragmentActivity implements
        ChooseDocumentFragment.OnFragmentInteractionListener {
    private static final String TAG = ChooseDocumentActivity.class.getCanonicalName();
    private String groupId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        setToolBar(findViewById(R.id.tool_bar));
        getData();
        displayFragment();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void getData() {
        if (getIntent() != null) {
            groupId = getIntent().getStringExtra(Constants.DOCUMENT.GROUP_ID);
        }
    }

    /*@Override
    public void returnResultShareContact(PhoneNumber phone) {
        if (phone != null) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NAME, phone.getName());
            returnIntent.putExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER, phone.getJidNumber());
            setResult(RESULT_OK, returnIntent);
        }
        onBackPressed();
    }*/

    @Override
    public void onReturnData(DocumentClass document) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.DOCUMENT.DOCUMENT_DATA, document);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void displayFragment() {
        Log.d(TAG, "displayFragment: " + groupId);
        ChooseDocumentFragment fragment = ChooseDocumentFragment.newInstance(groupId);
        executeFragmentTransaction(fragment, R.id.fragment_container, false, false);
    }
}
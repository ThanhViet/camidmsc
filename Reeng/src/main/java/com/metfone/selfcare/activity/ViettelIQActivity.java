package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.PubSubManager;
import com.metfone.selfcare.common.api.ViettelIQApi;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.database.model.viettelIQ.QuestionIQ;
import com.metfone.selfcare.fragment.viettelIQ.CountFirstQuestionViettelIQFragment;
import com.metfone.selfcare.fragment.viettelIQ.FlashViettelIQFragment;
import com.metfone.selfcare.fragment.viettelIQ.QuestionViettelIQFragment;
import com.metfone.selfcare.fragment.viettelIQ.WinViettelIQFragment;
import com.metfone.selfcare.helper.GameIQHelper;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.risenumber.CountAnimationTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.metfone.selfcare.helper.GameIQHelper.DURATION_ANIM;

public class ViettelIQActivity extends BaseSlidingFragmentActivity {

    private static final String TAG = ViettelIQActivity.class.getSimpleName();
    private static final String ID_GAME = "idgame";

    @BindView(R.id.tv_number_players)
    CountAnimationTextView tvNumberPlayers;
    @BindView(R.id.ll_number_players)
    LinearLayout llNumberPlayers;
    @BindView(R.id.tv_number_viewers)
    CountAnimationTextView tvNumberViewers;
    @BindView(R.id.ll_number_viewers)
    LinearLayout llNumberViewers;
    @BindView(R.id.parentView)
    RelativeLayout parentView;
    @BindView(R.id.iv_background)
    ImageView ivBackground;
    @BindView(R.id.tv_number_heart)
    TextView tvNumberHeart;
    @BindView(R.id.ll_number_heart)
    LinearLayout llNumberHeart;
    @BindView(R.id.llInfo)
    RelativeLayout llInfo;
    @BindView(R.id.tvNumberPlayer)
    TextView tvNumberPlayerEndGame;
    private boolean isVisible;
    private int heartRemain;
    RelativeLayout.LayoutParams paramsHeart;
    private int marginNormal;

    public static void start(Context context, String idGame) {
        Intent starter = new Intent(context, ViettelIQActivity.class);
        starter.putExtra(ID_GAME, idGame);
        context.startActivity(starter);
    }

    private String startTimeSrt;
    private String currentIdGame;
    private String matchId;
    private ApplicationController mApp;

    private long startTime = 0;
    private long serverTime = 0;

    private long answerTime = 10;
    private long displayTime = 10;

    private long numPlayer = 0;
    private long numViewer = 0;

    private int totalQuestion;

    private String background;
    private String home;

    private QuestionIQ currentQuestionIQ;
    private long deltaTime;
    private boolean isPlayer = false;
    private boolean isOpenQuestion = false;
    private int canUseHeart = 0;
    private boolean heartUsed = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (ApplicationController) getApplication();
        currentIdGame = getIntent().getStringExtra(ID_GAME);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        subGame(true);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_viettel_iq);
        ButterKnife.bind(this);
        paramsHeart = (RelativeLayout.LayoutParams) llNumberHeart.getLayoutParams();
        marginNormal = getResources().getDimensionPixelOffset(R.dimen.margin_more_content_40);
        initView();
        getSub();
    }

    @Override
    public void onStart() {
        super.onStart();
        isVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisible = false;
    }

    private void initView() {
        DecimalFormat df = new DecimalFormat("###,###,###");
        DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
        sym.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(sym);
        tvNumberPlayers.setDecimalFormat(df);
        tvNumberViewers.setDecimalFormat(df);
        tvNumberPlayers.setKeepScreenOn(true);
    }

    @Override
    protected void onDestroy() {
        subGame(false);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (isPlayer && isOpenQuestion) {
            Resources mRes = getResources();
            DialogConfirm dialogConfirm = new DialogConfirm(this, true);
            dialogConfirm.setTitle(R.string.gameiq_title_exit);
            dialogConfirm.setMessage(mRes.getString(R.string.gameiq_confirm_exit));
            dialogConfirm.setPositiveLabel(mRes.getString(R.string.gameiq_yes));
            dialogConfirm.setNegativeLabel(mRes.getString(R.string.gameiq_no));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {

                @Override
                public void onPositive(Object result) {
                    finish();
                }
            });
            dialogConfirm.show();
        } else {
            super.onBackPressed();
        }
    }

    public void setPlayer(boolean player) {
        isPlayer = player;
    }

    public boolean isPlayer() {
        return isPlayer;
    }

    public void setHeartRemain(int heartRemain) {
        this.heartRemain = heartRemain;
        tvNumberHeart.setText(String.valueOf(heartRemain));
    }

    private void subGame(boolean isSub) {
        PubSubManager.getInstance(mApp).sendSubUnSubGame(currentIdGame, isSub);
    }

    public void getSub() {
        showLoadingDialog("", R.string.loading);
        ViettelIQApi.getInstance().sub(this, currentIdGame);
    }

    public String getCurrentIdGame() {
        return currentIdGame;
    }

    public String getBackground() {
        return background;
    }

    public String getHome() {
        return home;
    }

    /**
     * cập nhật lại time server
     *
     * @param serverTime
     */
    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
        this.deltaTime = serverTime - System.currentTimeMillis();
    }

    public long getDeltaTime() {
        return deltaTime;
    }

    public long getDisplayTime() {
        return displayTime;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public String getStartTimeSrt() {
        return startTimeSrt;
    }

    public void onSubSuccess(String data) throws JSONException {
        hideLoadingDialog();
        JSONObject jsonObject = new JSONObject(data);
        int code = jsonObject.optInt("code");
        totalQuestion = jsonObject.optInt("numQuestion");
        displayTime = jsonObject.optInt("displayTime");
        answerTime = jsonObject.getInt("answerTime");
        startTime = jsonObject.optLong("startTime") + displayTime * 1000;
        serverTime = jsonObject.optLong("serverTime");
        startTimeSrt = jsonObject.optString("startTimeSrt");
        long numPlayer = jsonObject.optInt("numPlayer");
        long numViewer = jsonObject.optInt("numViewer");
        matchId = jsonObject.optString("matchId");
        isPlayer = jsonObject.optInt("isPlay") == 1;
        background = jsonObject.optString("background");
        home = jsonObject.optString("home");
        deltaTime = serverTime - System.currentTimeMillis();
        setHeartRemain(jsonObject.optInt("hearts"));
        if (code == 200) {
            currentQuestionIQ = ApplicationController.self().getGson().fromJson(jsonObject.optString("currentQuestion"), QuestionIQ.class);
            canUseHeart = currentQuestionIQ.getCanUseHeart();
            if (currentQuestionIQ == null || Utilities.isEmpty(currentQuestionIQ.getId())) {
                String msg = jsonObject.optString("desc", getResources().getString(R.string.e601_error_but_undefined));
                DialogMessage dialogMessage = new DialogMessage(this, false);
                dialogMessage.setLabelButton(getResources().getString(R.string.ok));
                dialogMessage.setMessage(msg);
                dialogMessage.setNegativeListener(new NegativeListener() {
                    @Override
                    public void onNegative(Object result) {
                        finish();
                    }
                });
                dialogMessage.show();
            }
            updateView(numPlayer, numViewer);

            if (serverTime > startTime) {
                /*
                 * game đẵ bắt đầu
                 */
                if (serverTime - startTime < totalQuestion * (displayTime + answerTime) * 1000)
                    openQuestionByIndex();
                else
                    openWinner();
            } else {
                /*
                 * hiển thị màn hình count down
                 */
                if (startTime - getCurrentTime() <= displayTime * 1000) {
                    openCountFirstQuestion();
                } else {
                    openFlash();
                }
            }
        } else if (code == 206) {   //vừa kết thúc game, đang tổng kết,
            Log.f(TAG, "-----GAME IQ----- vừa kết thúc game, đang tổng kết");
            openWinner();
        } else {
            String msg = jsonObject.optString("desc", getResources().getString(R.string.e601_error_but_undefined));
            DialogMessage dialogMessage = new DialogMessage(this, false);
            dialogMessage.setLabelButton(getResources().getString(R.string.ok));
            dialogMessage.setMessage(msg);
            dialogMessage.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    finish();
                }
            });
            dialogMessage.show();
        }
    }

    public long getCurrentTime() {
        return System.currentTimeMillis() + deltaTime;
    }

    public void openCountFirstQuestion() {
        isOpenQuestion = true;
        tvNumberPlayerEndGame.setVisibility(View.GONE);
        llInfo.setVisibility(View.VISIBLE);
        paramsHeart.setMargins(0, 0, 0, 0);
        ImageManager.showImageGameIQ(background, ivBackground);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_content,
                CountFirstQuestionViettelIQFragment.newInstance(startTime)).commitAllowingStateLoss();
        setViewNumbPlayerViewer(true, true);
//        openWinner();
    }

    public void openFlash() {
        isOpenQuestion = false;
        tvNumberPlayerEndGame.setVisibility(View.GONE);
        llInfo.setVisibility(View.VISIBLE);
        paramsHeart.setMargins(0, 0, 0, 0);
        llNumberHeart.setLayoutParams(paramsHeart);
        ImageManager.showImageGameIQ(home, ivBackground);
        setViewNumbPlayerViewer(true, false);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_content,
                FlashViettelIQFragment.newInstance(startTime, numPlayer, numViewer)).commitAllowingStateLoss();
    }

    public void openQuestionByIndex() {
        isOpenQuestion = true;
        tvNumberPlayerEndGame.setVisibility(View.GONE);
        llInfo.setVisibility(View.VISIBLE);
        paramsHeart.setMargins(0, marginNormal, 0, 0);
        llNumberHeart.setLayoutParams(paramsHeart);
        ImageManager.showImageGameIQ(background, ivBackground);
        setViewNumbPlayerViewer(true, true);
        Log.i(TAG, "----GAME IQ---- openQuestionByIndex: " + currentQuestionIQ);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, QuestionViettelIQFragment.newInstance(currentQuestionIQ, currentIdGame, matchId, totalQuestion, startTime, answerTime, displayTime)).commitAllowingStateLoss();
    }

    public void openQuestionByIndex(QuestionIQ questionIQ) {
        isOpenQuestion = true;
        tvNumberPlayerEndGame.setVisibility(View.GONE);
        llInfo.setVisibility(View.VISIBLE);
        paramsHeart.setMargins(0, marginNormal, 0, 0);
        llNumberHeart.setLayoutParams(paramsHeart);
        ImageManager.showImageGameIQ(background, ivBackground);
        this.currentQuestionIQ = questionIQ;
        Log.i(TAG, "----GAME IQ---- openQuestionByIndex: QuestionIQ" + questionIQ);
        setViewNumbPlayerViewer(true, true);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, QuestionViettelIQFragment.newInstance(questionIQ, currentIdGame, matchId, totalQuestion, startTime, answerTime, displayTime)).commitAllowingStateLoss();
    }

    public void openWinner() {
        isOpenQuestion = false;
        tvNumberPlayerEndGame.setVisibility(View.VISIBLE);
        llInfo.setVisibility(View.GONE);
        ImageManager.showImageGameIQ(background, ivBackground);
        setViewNumbPlayerViewer(true, true);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_content,
                WinViettelIQFragment.newInstance(startTime, matchId)).commitAllowingStateLoss();
    }

    public void updateTextEndGame(long total) {
        tvNumberPlayerEndGame.setText(String.format(getResources().getString(R.string.gameiq_watching), String.valueOf(total)));
    }

    public void setNumPlayerViewer(long numPlayer, long numViewer) {
        this.numPlayer = numPlayer;
        this.numViewer = numViewer;
    }

    private void updateView(long player, long watcher) {
        Log.i(TAG, "-----IQ----- updateView: cplayer: " + numPlayer + " cwatcher: " + numViewer + " p: " + player + " w: " + watcher);
        if (player != numPlayer) {
            if (player > 1000) {
                String textPlayer = Utilities.shortenLongNumber(player);
                tvNumberPlayers.setText(textPlayer);
            } else
                tvNumberPlayers.setAnimationDuration(DURATION_ANIM).countAnimation((int) numPlayer, (int) player);
//            numPlayer = player;
        }
        if (watcher != numViewer) {
            if (watcher > 1000) {
                String textWatcher = Utilities.shortenLongNumber(watcher);
                tvNumberViewers.setText(textWatcher);
            } else
                tvNumberViewers.setAnimationDuration(DURATION_ANIM).countAnimation((int) numViewer, (int) watcher);
//            numViewer = watcher;
        }
        setNumPlayerViewer(player, watcher);
    }

    public void setViewNumbPlayerViewer(boolean showPlayer, boolean showViewer) {
        llNumberPlayers.setVisibility(showPlayer ? View.VISIBLE : View.GONE);
        llNumberViewers.setVisibility(showViewer ? View.VISIBLE : View.GONE);
    }

    public void playResultAudio(boolean isCorrect) {
        if (!isVisible) return;
        if (!isPlayer) isCorrect = true;
        Log.i(TAG, "playResultAudio: " + isCorrect);
        MediaPlayer mp = MediaPlayer.create(getApplication(), isCorrect ? R.raw.iq_correct : R.raw.iq_wrong);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                mp.release();
                mp = null;
            }

        });
        mp.start();
    }

    public void playSoundClick() {
        Log.i(TAG, "playSoundClick: ");
        MediaPlayer mp = MediaPlayer.create(getApplication(), R.raw.iq_click);
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                mp.release();
                mp = null;
            }

        });
        mp.start();
    }

    public RelativeLayout getParentView() {
        return parentView;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onIQGameEvent(final GameIQHelper.IQGameEvent event) {
        Log.i(TAG, "-----IQ-----onIQGameEvent: " + event.toString());
        if (!TextUtils.isEmpty(event.getIdGame()) && event.getIdGame().equals(currentIdGame)) {
            updateView(event.getPlayNumb(), event.getViewNumb());
        }
    }

    public int getHeartRemain() {
        return heartRemain;
    }

    public int getCanUseHeart() {
        Log.i(TAG, "getCanUseHeart: " + canUseHeart);
        if (currentQuestionIQ != null && currentQuestionIQ.getQuestInx() <= 7) return 1;
        return 0;
    }

    public void setCanUseHeart(int canUseHeart) {
        Log.i(TAG, "setCanUseHeart: " + canUseHeart);
        this.canUseHeart = canUseHeart;
    }

    public boolean isHeartUsed() {
        return heartUsed;
    }

    public void setHeartUsed(boolean heartUsed) {
        this.heartUsed = heartUsed;
    }
}

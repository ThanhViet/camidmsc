package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Space;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.LoginBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.helper.google.GoogleSignInHelper;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.IdentifyProvider;
import com.metfone.selfcare.model.account.LinkAccountRequest;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPConfirmDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.network.ApiService;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.RetrofitMochaInstance;
import com.metfone.selfcare.v5.utils.ToastUtils;

import org.jivesoftware.smack.Connection;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_ACCESS_TOKEN;
import static com.metfone.selfcare.helper.google.GoogleSignInHelper.RC_SIGN_IN;

public class AddMoreMethodActivity extends BaseSlidingFragmentActivity implements FacebookHelper.OnFacebookListener, GoogleSignInHelper.OnGoogleListener {
    private static final int FACEBOOK_METHOD = 1;
    private static final int GOOGLE_METHOD = 2;
    private static final int PHONE_METHOD = 0;
    private static final String METHOD = "method";
    GoogleSignInHelper googleSignInHelper;
    FacebookHelper facebookHelper;
    @BindView(R.id.layoutFacebook)
    ConstraintLayout layoutFacebook;
    @BindView(R.id.layoutGoogle)
    ConstraintLayout layoutGoogle;
    @BindView(R.id.layoutPhone)
    ConstraintLayout layoutPhone;
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.tvSkip)
    CamIdTextView tvSkip;
    @BindView(R.id.tvGuide)
    CamIdTextView mTvGuide;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    ApiService apiService = RetrofitInstance.getInstance().create(ApiService.class);
    View customLayout;
    AlertDialog alertDialog;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private int previousMethod = -1;
    private Intent intent;
    private ApplicationController mApplication;
    private Resources mRes;
    private String mCurrentNumberJid;
    private String mCurrentRegionCode;
    private boolean isLoginDone = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_login_method);
        ButterKnife.bind(this);

        intent = getIntent();
        initView();
    }

    private void initView() {
        setWhiteStatusBar();
        setColorStatusBar(R.color.white);
        mApplication = (ApplicationController) getApplicationContext();
        mRes = getResources();
        if (intent != null) {
            previousMethod = intent.getIntExtra(METHOD, -1);
            switch (previousMethod) {
                case FACEBOOK_METHOD:
                    layoutFacebook.setVisibility(View.GONE);
                    layoutGoogle.setVisibility(View.VISIBLE);
                    layoutPhone.setVisibility(View.VISIBLE);
                    mTvGuide.setText(getText(R.string.add_more_login_method_facebook));
                    mTvGuide.setTextColor(getResources().getColor(R.color.line_register));

                    break;
                case GOOGLE_METHOD:
                    layoutFacebook.setVisibility(View.VISIBLE);
                    layoutGoogle.setVisibility(View.GONE);
                    layoutPhone.setVisibility(View.VISIBLE);
                    mTvGuide.setTextColor(getResources().getColor(R.color.line_register));
                    mTvGuide.setText(getText(R.string.add_more_login_method_google));
                    break;
                case PHONE_METHOD:
                    layoutFacebook.setVisibility(View.VISIBLE);
                    layoutGoogle.setVisibility(View.VISIBLE);
                    layoutPhone.setVisibility(View.GONE);
                    mTvGuide.setText(getText(R.string.add_more_login_method_guide));
                    break;
            }
        }

        facebookHelper = new FacebookHelper(this);
        googleSignInHelper = new GoogleSignInHelper(this, this);


        layoutFacebook.setOnClickListener(view1 -> {
            if (!NetworkHelper.isConnectInternet(this)) {
                showError(getString(R.string.error_internet_disconnect), null);
            }else {
                facebookHelper.getProfile(getCallbackManager(), this, FacebookHelper.PendingAction.GET_USER_INFO);
            }
        });

        layoutGoogle.setOnClickListener(view2 -> {
            if (!NetworkHelper.isConnectInternet(this)) {
                showError(getString(R.string.error_internet_disconnect), null);
            }else {
                googleSignInHelper.getProfile();
            }
        });

        layoutPhone.setOnClickListener(view3 -> {
            if (!NetworkHelper.isConnectInternet(this)) {
                showError(getString(R.string.error_internet_disconnect), null);
            }else {
                linkPhoneNumber();
            }
        });
        ivBack.setOnClickListener(view4 -> {
            onBackPressed();
        });
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                UserInfoBusiness userInfoBusiness = new UserInfoBusiness(AddMoreMethodActivity.this);
//                String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
//                UserInfo userInfo = userInfoBusiness.getUser();
//                mCurrentRegionCode = "KH";
//                if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
//                     if (userInfo.getPhone_number().startsWith("0")) {
//                            mCurrentNumberJid = "+855"+userInfo.getPhone_number().substring(1);
//                        } else {
//                            mCurrentNumberJid = "+855"+userInfo.getPhone_number();
//                        }
//                    String originToken = token.substring(7);
//                    generateOldMochaOTP(originToken);
//                } else {
//                    NavigateActivityHelper.navigateToHomeScreenActivity(AddMoreMethodActivity.this, true, true);
//                }
                if (!NetworkHelper.isConnectInternet(AddMoreMethodActivity.this)) {
                    showError(getString(R.string.error_internet_disconnect), null);
                }else {
                    getUserInformation();
                }

            }
        });
    }

    private void linkPhoneNumber() {
        Intent intent = new Intent(AddMoreMethodActivity.this, EnterAddPhoneNumberActivity.class);
        intent.putExtra(EnumUtils.OBJECT_KEY, EnumUtils.PhoneNumberTypeDef.ADD_LOGIN);
        startActivity(intent, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        //check connected login method
        EnumUtils.FROM_ADD_LOGIN_MODE = EnumUtils.FromAddLoginModeTypeDef.ADD_MORE_METHODS_ACTIVITY;
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(AddMoreMethodActivity.this);
        UserInfo currentUser = userInfoBusiness.getUser();
        List<IdentifyProvider> identifyProviderList = userInfoBusiness.getIdentifyProviderList();
        if (currentUser != null && !TextUtils.isEmpty(currentUser.getPhone_number()) && layoutPhone.getVisibility() == View.VISIBLE) {
            layoutPhone.setVisibility(View.GONE);
        }
        if (identifyProviderList != null && identifyProviderList.size() > 0) {
            if (foundIndex(identifyProviderList, "facebook") != -1 && layoutFacebook.getVisibility() == View.VISIBLE) {
                layoutFacebook.setVisibility(View.GONE);
            }

            if (foundIndex(identifyProviderList, "google") != -1 && layoutGoogle.getVisibility() == View.VISIBLE) {
                layoutGoogle.setVisibility(View.GONE);
            }
        }
//        userInfoBusiness.
    }

    private int foundIndex(List<IdentifyProvider> identifyProviderList, String type) {
        for (int i = 0; i < identifyProviderList.size(); i++) {
            if (type.equals(identifyProviderList.get(i).getIdentityProvider())) {
                return i;
            }
        }
        return -1;
    }

    private void showLinkedDialog(int resId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        customLayout = getLayoutInflater().inflate(resId, null);
        builder.setView(customLayout);
        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(this);

        TextView btnLinkPhone = customLayout.findViewById(R.id.btnLinkPhone);
        Space mSpaceIGotIt = customLayout.findViewById(R.id.space_igotit);
        if (previousMethod != PHONE_METHOD) {
            if (TextUtils.isEmpty(userInfoBusiness.getUser().getPhone_number()) ){
                btnLinkPhone.setVisibility(View.VISIBLE);
            }else {
                btnLinkPhone.setVisibility(View.GONE);
                mSpaceIGotIt.setVisibility(View.VISIBLE);
            }
            btnLinkPhone.setOnClickListener(v -> {
                linkPhoneNumber();
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            });
        } else {
            btnLinkPhone.setVisibility(View.GONE);
            mSpaceIGotIt.setVisibility(View.VISIBLE);
        }

        alertDialog = builder.create();
        if (alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            googleSignInHelper.handleSignInIntent(data);
        }
    }

    @Override
    public void onGetInfoFinish(String userId, String name, String email, String birthDayStr, int gender) {
        showLinkedDialog(R.layout.dialog_facebook_linked);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        TextView btnGotFacebook = customLayout.findViewById(R.id.btnGotFacebook);
        if (previousMethod != PHONE_METHOD) {
            btnGotFacebook.setVisibility(View.VISIBLE);
            btnGotFacebook.setOnClickListener(v -> {
                linkAccount(accessToken.getToken(), "facebook");
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            });
        } else {
            linkAccount(accessToken.getToken(), "facebook");
            btnGotFacebook.setVisibility(View.GONE);
        }

    }

    @Override
    public void onGetInfoGoogleFinish(String id, String userName, String email, Uri avatarUri) {
        showLinkedDialog(R.layout.dialog_google_linked);
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);
        TextView btnGotGoogle = customLayout.findViewById(R.id.btnGotGoogle);
        if (previousMethod != PHONE_METHOD) {
            btnGotGoogle.setVisibility(View.VISIBLE);
            btnGotGoogle.setOnClickListener(v -> {
                linkAccount(googleSignInAccount != null ? googleSignInAccount.getIdToken() : null, "google");
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            });
        } else {
            linkAccount(googleSignInAccount != null ? googleSignInAccount.getIdToken() : null, "google");
            btnGotGoogle.setVisibility(View.GONE);
        }
    }

    private void getUserInformation() {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        apiService.getUser(token).enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, retrofit2.Response<GetUserResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(AddMoreMethodActivity.this);
                        UserInfo userInfo = response.body().getData().getUser();
                        userInfoBusiness.setUser(userInfo);
                        ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
//                        reengAccount.setToken(token);
                        if (reengAccount != null) {
                            reengAccount.setName(userInfo.getFull_name());
                            reengAccount.setEmail(userInfo.getEmail());
                            reengAccount.setBirthday(userInfo.getDate_of_birth());
                            reengAccount.setAddress(userInfo.getAddress());
                            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                        }
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        if (response.body().getData().getServices() != null) {
                            userInfoBusiness.setServiceList(response.body().getData().getServices());
                        }
                        userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
                        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
                        mCurrentRegionCode = "KH";
                        if (!TextUtils.isEmpty(userInfo.getPhone_number())) {
                            if (userInfo.getPhone_number().startsWith("0")) {
                                mCurrentNumberJid = "+855"+userInfo.getPhone_number().substring(1);
                            } else {
                                mCurrentNumberJid = "+855"+userInfo.getPhone_number();
                            }
                            String originToken = token.substring(7);
                            generateOldMochaOTP(originToken);
                        } else {
                            NavigateActivityHelper.navigateToShareAndGetMoreActivity(AddMoreMethodActivity.this);
//                            NavigateActivityHelper.navigateToHomeScreenActivity(AddMoreMethodActivity.this, false, true);

//                    ToastUtils.showToast(AddMoreMethodActivity.this, "Phone number null -> Login old mocha failed!");
                        }
                    } else {
                        ToastUtils.showToast(AddMoreMethodActivity.this, response.body().getMessage());
                    }

                } else {
                    ToastUtils.showToast(AddMoreMethodActivity.this, "Get user information fail");
                }
            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(AddMoreMethodActivity.this, getString(R.string.service_error));
            }
        });
    }

    public void linkAccount(String socialToken, String socialType) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        String token = SharedPrefs.getInstance().get(PREF_ACCESS_TOKEN, String.class);
        LinkAccountRequest request = new LinkAccountRequest(socialToken, socialType);
        BaseRequest<LinkAccountRequest> linkAccountRequestBaseRequest = new BaseRequest<>();
        linkAccountRequestBaseRequest.setWsRequest(request);
        String json = new Gson().toJson(linkAccountRequestBaseRequest);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json);
        Call<GetUserResponse> call = apiService.linkAccount(token, requestBody);
        call.enqueue(new Callback<GetUserResponse>() {
            @Override
            public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                alertDialog.dismiss();
                if (response.body() != null) {
                    if ("00".equals(response.body().getCode())) {
//                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(AddMoreMethodActivity.this);
//                        if (response.body().getData() != null){
//                            userInfoBusiness.setUser(response.body().getData().getUser());
//                            userInfoBusiness.setServiceList(response.body().getData().getServices());
//                            userInfoBusiness.setIdentifyProviderList(response.body().getData().getIdentifyProviders());
//                        }
                        if ("google".equals(socialType)) {
                            layoutGoogle.setVisibility(View.GONE);
                        } else if ("facebook".equals(socialType)) {
                            layoutFacebook.setVisibility(View.GONE);
                        }
                        //check link all method
                        if (layoutPhone.getVisibility() == View.GONE && layoutGoogle.getVisibility() == View.GONE && layoutFacebook.getVisibility() == View.GONE){
                            NavigateActivityHelper.navigateToShareAndGetMoreActivity(AddMoreMethodActivity.this);
                        }
                    } else {
                      //  ToastUtils.showToast(AddMoreMethodActivity.this, response.body().getMessage());
                        String error = response.body().getMessage();
                        if (error != null){
                            baseMPSuccessDialog = new BaseMPSuccessDialog(AddMoreMethodActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry), error, false);
                            baseMPSuccessDialog.show();
                        }

                    }
                } else {
                    try {
                        JSONObject jObjError = null;
                        if (response.errorBody() != null) {
                            jObjError = new JSONObject(response.errorBody().string());
                        }
                        if (jObjError != null) {
                            ToastUtils.showToast(AddMoreMethodActivity.this, jObjError.getJSONObject("error").getString("message"));
                        }
                    } catch (Exception e) {
                        baseMPSuccessDialog = new BaseMPSuccessDialog(AddMoreMethodActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry),e.getMessage(), false);
                        baseMPSuccessDialog.show();
                    }

                }

            }

            @Override
            public void onFailure(Call<GetUserResponse> call, Throwable t) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
              //  ToastUtils.showToast(AddMoreMethodActivity.this, t.getMessage());
                baseMPSuccessDialog = new BaseMPSuccessDialog(AddMoreMethodActivity.this, R.drawable.image_error_dialog, getString(R.string.title_sorry),getString(R.string.service_error), false);
                baseMPSuccessDialog.show();
            }
        });
    }

    private void generateOldMochaOTP(String token) {
        if (pbLoading != null)
            pbLoading.setVisibility(View.VISIBLE);
        RetrofitMochaInstance retrofitMochaInstance = new RetrofitMochaInstance();
         String username = mCurrentNumberJid;
        retrofitMochaInstance.getOtpOldMocha(token, username, mCurrentRegionCode, new ApiCallback<OTPOldMochaResponse>() {
            @Override
            public void onResponse(Response<OTPOldMochaResponse> response) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    //success get otp
                    if (response.body().getCode() == 200) {
                        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(AddMoreMethodActivity.this);
                        userInfoBusiness.setOTPOldMochaResponse(response.body());
                        String json = userInfoBusiness.getOTPOldMochaResponseJson();
                        UrlConfigHelper.getInstance(AddMoreMethodActivity.this).detectSubscription(json);
                   doLoginAction(response.body().getOtp());
                    } else {
                        ToastUtils.showToast(AddMoreMethodActivity.this, response.body().getDesc());
                    }
                } else {
                    ToastUtils.showToast(AddMoreMethodActivity.this, "generateOldMochaOTP Failed");

                }

            }

            @Override
            public void onError(Throwable error) {
                if (pbLoading != null)
                    pbLoading.setVisibility(View.GONE);
                ToastUtils.showToast(AddMoreMethodActivity.this, error.getMessage());
            }
        });

    }

    private void doLoginAction(String password) {
        if (mApplication.getReengAccountBusiness().isProcessingChangeNumber()) return;
//        mApplication.getReengAccountBusiness().setProcessingChangeNumber(true);
        mApplication.getXmppManager().manualDisconnect();
        new AddMoreMethodActivity.LoginByCodeAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, password);
    }

    private class LoginByCodeAsyncTask extends AsyncTask<String, XMPPResponseCode, XMPPResponseCode> {
        String mPassword;
        int actionType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbLoading.setVisibility(View.VISIBLE);
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) actionType = 1;
        }

        @Override
        protected XMPPResponseCode doInBackground(String[] params) {
            mPassword = params[0];
            ApplicationController applicationController = (ApplicationController) getApplication();
            LoginBusiness loginBusiness = applicationController.getLoginBusiness();
            XMPPResponseCode responseCode = loginBusiness.loginByCode(applicationController, mCurrentNumberJid,
                    mPassword, mCurrentRegionCode, !mApplication.getReengAccountBusiness().isAnonymousLogin(), Connection.CODE_AUTH_NON_SASL, null, null);
            return responseCode;
        }

        @Override
        protected void onPostExecute(XMPPResponseCode responseCode) {
            super.onPostExecute(responseCode);
            com.metfone.selfcare.util.Log.i(TAG, "responseCode: " + responseCode);
            pbLoading.setVisibility(View.GONE);

            try {
//                mApplication.getReengAccountBusiness().setProcessingChangeNumber(false);
                if (responseCode.getCode() == XMPPCode.E200_OK) {
                    isLoginDone = true;
                    com.metfone.selfcare.util.Log.i(TAG, "E200_OK: " + responseCode);
                    mApplication.getApplicationComponent().provideUserApi().unregisterRegid(actionType);
//                    if (mApplication.getReengAccountBusiness().isInProgressLoginFromAnonymous()) {
                    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(AddMoreMethodActivity.this);
                    UserInfo userInfo = userInfoBusiness.getUser();
                    ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
                    reengAccount.setNumberJid(mCurrentNumberJid);
                    reengAccount.setRegionCode("KH");
                    reengAccount.setName(userInfo.getFull_name());
                    mApplication.getReengAccountBusiness().setAnonymous(false);
                    mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
                    mApplication.getReengAccountBusiness().setInProgressLoginFromAnonymous(false);
                    NavigateActivityHelper.navigateToShareAndGetMoreActivity(AddMoreMethodActivity.this);
//                    RestoreManager.setRestoring(true);
//                    userInfoBusiness.autoBackUp(mApplication, new DBImporter.RestoreProgressListener() {
//                        @Override
//                        public void onStartDownload() {
//
//                        }
//
//                        @Override
//                        public void onDownloadProgress(int percent) {
//
//                        }
//
//                        @Override
//                        public void onDownloadComplete() {
//
//                        }
//
//                        @Override
//                        public void onDowloadFail(String message) {
//
//                        }
//
//                        @Override
//                        public void onStartRestore() {
//
//                        }
//
//                        @Override
//                        public void onRestoreProgress(int percent) {
//
//                        }
//
//                        @Override
//                        public void onRestoreComplete(int messageCount, int threadMessageCount) {
//                            try {
//                                SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
//                                if (pref != null) {
//                                    pref.edit().putBoolean(SharedPrefs.KEY.BACKUP_PASSED_RESTORE_PHASE, true).apply();
//                                    com.metfone.selfcare.util.Log.i(TAG, "setPassedRestorePhase");
//                                }
//                            } catch (Exception e) {
//                            }
//                            RestoreManager.setRestoring(false);
//                            mApplication.getMessageBusiness().loadAllThreadMessageOnFirstTime(null);
//                            mApplication.getMessageBusiness().updateThreadStrangerAfterLoadData();
//                            mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_HAS_BACKUP, "").apply();
//                        }
//
//                        @Override
//                        public void onRestoreFail(String message) {
//                            RestoreManager.setRestoring(false);
//                            NavigateActivityHelper.navigateToHomeScreenActivity(AddMoreMethodActivity.this, false, true);
//                        }
//                    }, AddMoreMethodActivity.this);

//                    String phoneNumber = getArguments().getString(EnumUtils.PHONE_NUMBER_KEY);
//                    String otp = getArguments().getString(EnumUtils.OTP_KEY);
//                    String pass = edtPassWord.getEditText().getText().toString();
//                    }

//                    if (mListener != null && !isSaveInstanceState) {
//                        mListener.navigateToNextScreen();
//                    }
//                    if (tvError != null) {
//                        tvError.setVisibility(View.GONE);
//                    }
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_complete));
                } else {
//                    showError(responseCode.getDescription(), null);
//                    if (tvError != null) {
//                        tvError.setVisibility(View.VISIBLE);
//                    }
                    ToastUtils.showToast(AddMoreMethodActivity.this, responseCode.getDescription());
                    mApplication.logEventFacebookSDKAndFirebase(getString(R.string.c_login_fail));

//                    mListener.navigateToNextScreen();
                }
            } catch (Exception e) {
                com.metfone.selfcare.util.Log.e(TAG, "Exception", e);
            }
        }
    }
//
}

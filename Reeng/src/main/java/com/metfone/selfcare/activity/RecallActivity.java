package com.metfone.selfcare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.call.CallConstant;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by thanhnt72 on 7/30/2018.
 */

public class RecallActivity extends BaseSlidingFragmentActivity {
    private static final String TAG = RecallActivity.class.getSimpleName();


    private ApplicationController mApp;

    private String jidFriend;
    private boolean isCallVideo;

    @BindView(R.id.ab_back_btn)
    ImageView abBackBtn;
    @BindView(R.id.ab_title)
    EllipsisTextView abTitle;
    @BindView(R.id.ab_more_btn)
    ImageView abMoreBtn;
    @BindView(R.id.ivCallAvatar)
    RoundedImageView ivCallAvatar;
    @BindView(R.id.tvCallName)
    TextView tvCallName;
    @BindView(R.id.tvCallLost)
    TextView tvCallLost;
    @BindView(R.id.tvCancel)
    RoundTextView tvCancel;
    @BindView(R.id.tvCallBack)
    RoundTextView tvCallBack;


    public static void start(Context context, String jid, boolean isCallVideo) {
        Intent starter = new Intent(context, RecallActivity.class);
        starter.putExtra(CallConstant.JIDNUMBER, jid);
        starter.putExtra(CallConstant.IS_CALLVIDEO, isCallVideo);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(starter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recall);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mApp = (ApplicationController) getApplication();

        Bundle bundle = getIntent().getExtras();
        jidFriend = bundle.getString(CallConstant.JIDNUMBER);
        isCallVideo = bundle.getBoolean(CallConstant.IS_CALLVIDEO);

        if (isCallVideo) {
            abTitle.setText(getResources().getString(R.string.call_video));
        } else {
            abTitle.setText(getResources().getString(R.string.call));
        }
        abMoreBtn.setVisibility(View.GONE);

        drawAvatarAndName();

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void drawAvatarAndName() {
        String avatarSmallUrl = null;
        String name = jidFriend;
        int size = (int) getResources().getDimension(R.dimen.avatar_small_size);

        PhoneNumber mPhoneNumber = mApp.getContactBusiness().getPhoneNumberFromNumber(jidFriend);
        AvatarBusiness avatarBusiness = mApp.getAvatarBusiness();
        if (mPhoneNumber != null) {
            name = mPhoneNumber.getName();
            if (mPhoneNumber.isReeng()) {
                avatarSmallUrl = avatarBusiness.getAvatarUrl(mPhoneNumber.getLastChangeAvatar(), jidFriend, size);
            }
        } else {
            StrangerPhoneNumber stranger = mApp.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(jidFriend);
            if (stranger != null) { // neu so co trong bang stranger thi lay ten o day
                if (TextUtils.isEmpty(stranger.getFriendName())) {
                    name = Utilities.hidenPhoneNumber(jidFriend);
                } else {
                    name = stranger.getFriendName();
                }

                if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                    avatarSmallUrl = stranger.getFriendAvatarUrl();
                } else {
                    NonContact nonContactStr = mApp.getContactBusiness().getExistNonContact(jidFriend);
                    // co non contact lay lavatar cho moi nhat
                    if (nonContactStr != null && nonContactStr.getState() == Constants.CONTACT.ACTIVE) {
                        avatarSmallUrl = avatarBusiness.getAvatarUrl(nonContactStr.getLAvatar(), jidFriend, size);
                    } else {// chua co non contact thi lay tu stranger
                        avatarSmallUrl = avatarBusiness.getAvatarUrl(stranger.getFriendAvatarUrl(), jidFriend, size);
                    }
                }
            } else {
                name = jidFriend;

                NonContact nonContact = mApp.getContactBusiness().getExistNonContact(jidFriend);
                if (nonContact != null && nonContact.getState() == Constants.CONTACT.ACTIVE) {
                    avatarSmallUrl = avatarBusiness.getAvatarUrl(nonContact.getLAvatar(), jidFriend, size);
                }
            }
        }

        tvCallName.setText(name);
        tvCallLost.setText(String.format(getResources().getString(R.string.suggest_call_again), name));
        mApp.getImageProfileBusiness().displayAvatarCallImage(ivCallAvatar, avatarSmallUrl, null);
    }

    @OnClick({R.id.ab_back_btn, R.id.tvCancel, R.id.tvCallBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                onBackPressed();
                break;
            case R.id.tvCancel:
                onBackPressed();
                break;
            case R.id.tvCallBack:
                ThreadMessage threadMessage = mApp.getMessageBusiness().findExistingSoloThread(jidFriend);
                if (isCallVideo) {
                    mApp.getCallBusiness().checkAndStartVideoCall(RecallActivity.this, threadMessage, false);
                } else {
                    mApp.getCallBusiness().checkAndStartCall(RecallActivity.this, threadMessage);
                }
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRecallEvent(final RecallActivityEvent event) {
        Log.i(TAG, "onRecallEvent");
        finish();
    }

    public static class RecallActivityEvent {
    }
}

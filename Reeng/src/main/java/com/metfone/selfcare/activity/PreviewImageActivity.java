package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.fragment.message.GridImageThreadFragment;
import com.metfone.selfcare.fragment.message.PreviewImageThreadFragment;
import com.metfone.selfcare.fragment.message.PreviewSingleImageFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.ui.CropImageNew.CropView;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.util.ArrayList;

public class PreviewImageActivity extends BaseSlidingFragmentActivity {
    public static final String TAG = PreviewImageActivity.class.getSimpleName();
    public static final String PARAM_THREAD_MESSAGE_ID = "thread_message_id";
    public static final String PARAM_CURRENT_IMAGE = "current_image";
    public static final String PARAM_LINK_IMAGE = "link_image";

    public static final String PARAM_TITLE = "title";
    public static final String PARAM_FILE_PATH = "file_path";
    public static final String PARAM_LINK = "link_image";
    public static final String PARAM_PRIVATE = "private";

    private int threadId;
    private String oldCurrentImagePath;
    private String linkImagePath;
    private String title, linkImage;
    private boolean isPrivateEncrypt;
    private ApplicationController mApp;
    private MessageBusiness mMessageBusiness;

//    private ElasticDragDismissRelativeLayout dragDismissRelativeLayout ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, " onCreate ... ");
        /*getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);*/
        changeStatusBar(true);
        setContentView(R.layout.activity_preview_image_detail);
        setToolBar(findViewById(R.id.tool_bar));
        mApp = (ApplicationController) getApplication();
        mMessageBusiness = mApp.getMessageBusiness();

//        dragDismissRelativeLayout = findViewById(R.id.drag_dissmiss_layout);
//        dragDismissRelativeLayout.addListener(new ElasticDragDismissCallback() {
//            @Override
//            public void onDrag(float elasticOffset, float elasticOffsetPixels, float rawOffset, float rawOffsetPixels) {
//                super.onDrag(elasticOffset, elasticOffsetPixels, rawOffset, rawOffsetPixels);
//            }
//
//            @Override
//            public void onDragDismissed() {
//                super.onDragDismissed();
//            }
//        });
//        if (Build.VERSION.SDK_INT >= 21) {
//            dragDismissRelativeLayout.addListener(new SystemChromeFader(this));
//        }

        if (savedInstanceState != null) {
            threadId = savedInstanceState.getInt(PARAM_THREAD_MESSAGE_ID);
            oldCurrentImagePath = savedInstanceState.getString(PARAM_CURRENT_IMAGE);
            linkImagePath = savedInstanceState.getString(PARAM_LINK_IMAGE);
            title = savedInstanceState.getString(PARAM_TITLE);
            linkImage = savedInstanceState.getString(PARAM_LINK);
            isPrivateEncrypt = savedInstanceState.getBoolean(PARAM_PRIVATE);
        } else {
            Intent intent = getIntent();
            if (intent == null) {
                finish();
                return;
            }
            threadId = intent.getIntExtra(PARAM_THREAD_MESSAGE_ID, -1);
            oldCurrentImagePath = intent.getStringExtra(PARAM_CURRENT_IMAGE);
            linkImagePath = intent.getStringExtra(PARAM_LINK_IMAGE);
            title = intent.getStringExtra(PARAM_TITLE);
            linkImage = intent.getStringExtra(PARAM_LINK);
            isPrivateEncrypt = intent.getBooleanExtra(PARAM_PRIVATE, false);
        }

        if (threadId == -2) {
            displayPreviewSingleImageFragment(oldCurrentImagePath, title, linkImage);
        } else {
            if (TextUtils.isEmpty(oldCurrentImagePath)) {
                displayGridImageThreadFragment(threadId);
            } else if (!TextUtils.isEmpty(linkImagePath)) {
                displayPreviewImageThreadFragment(threadId, oldCurrentImagePath, linkImagePath, false);
            } else {
                displayPreviewImageThreadFragment(threadId, oldCurrentImagePath, false);
            }
        }
        if (isPrivateEncrypt)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        trackingScreen(TAG);



        /*mHandler = new Handler();
        mHandler.postDelayed(runnable, TIME_DELAY_SHOW);*/
    }

    private void displayGridImageThreadFragment(int threadId) {
        executeFragmentTransaction(GridImageThreadFragment.newInstance(threadId), R.id.fragment_container, false,
                false);
    }

    public void displayPreviewImageThreadFragment(int threadId, String currentImage, boolean addBackStack) {
        getToolBarView().setVisibility(View.GONE);
        executeFragmentTransaction(PreviewImageThreadFragment.newInstance(threadId, currentImage), R.id
                .fragment_container, addBackStack, false);
    }

    public void displayPreviewImageThreadFragment(int threadId, String currentImage, String linkImage, boolean addBackStack) {
        getToolBarView().setVisibility(View.GONE);
        executeFragmentTransaction(PreviewImageThreadFragment.newInstance(threadId, currentImage, linkImage), R.id
                .fragment_container, addBackStack, false);
    }

    public void displayPreviewSingleImageFragment(String filePath, String name, String linkImage) {
        getToolBarView().setVisibility(View.GONE);
        executeFragmentTransaction(PreviewSingleImageFragment.newInstance(name, filePath, linkImage), R.id
                .fragment_container, false, false);
    }

    public boolean isPrivateEncrypt() {
        return isPrivateEncrypt;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PopupHelper.getInstance().destroyOverFlowMenu();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(PARAM_THREAD_MESSAGE_ID, threadId);
        outState.putString(PARAM_CURRENT_IMAGE, oldCurrentImagePath);
        outState.putString(PARAM_LINK_IMAGE, linkImagePath);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "on Destroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (mHandler != null) mHandler.removeCallbacks(runnable);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Log.i(TAG, "onActivityResult requestCode = " + requestCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (!mApp.isDataReady()) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    while (!mApp.isDataReady()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            Log.e(TAG, "InterruptedException", e);
                        }
                    }
                    processOnActivityResult(requestCode, resultCode, data);
                }
            };
            thread.start();
        } else {
            processOnActivityResult(requestCode, resultCode, data);
        }
    }

    private void processOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case Constants.ACTION.ACTION_CROP_AVATAR:
                if (data != null) {
                    String fileCrop = data.getStringExtra(CropView.OUTPUT_PATH);
                    mApp.getReengAccountBusiness().processUploadAvatarTask(fileCrop);
                    showToast(R.string.change_setting_success);
                }
                break;
            case Constants.ACTION.ACTION_CROP_BACKGROUND:
                if (data != null) {
                    //String fileCrop = mPref.getString(PREF_CROP_FILE, "");
                    String fileCrop = data.getStringExtra(CropView.OUTPUT_PATH);
                    if (fileCrop != null) {
                        ThreadMessage mThreadMessage = mMessageBusiness.getThreadById(threadId);
                        mThreadMessage.setBackground(fileCrop);
                        mMessageBusiness.updateThreadMessage(mThreadMessage);
                        mThreadMessage.setForeUpdateBg(true);
                        showToast(R.string.change_setting_success);
                    }
                }
                break;
            case Constants.CHOOSE_CONTACT.TYPE_FORWARD_CONTENT:

                // goto new thread and send message
                ReengMessage messageForward = (ReengMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT
                        .DATA_REENG_MESSAGE);
                ThreadMessage threadMessage;
                if (data.hasExtra(Constants.CHOOSE_CONTACT.DATA_THREAD_MESSAGE)) {
                    threadMessage = (ThreadMessage) data.getSerializableExtra(Constants.CHOOSE_CONTACT
                            .DATA_THREAD_MESSAGE);
                } else {
                    String number = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
                    threadMessage = mMessageBusiness.findExistingOrCreateNewThread(number);
                }
                if (threadMessage != null) {
                    ArrayList<ReengMessage> ls = new ArrayList<>();
                    ls.add(messageForward);
                    NavigateActivityHelper.navigateToChatDetail(PreviewImageActivity.this, threadMessage, ls);
                }
                break;
            default:
                break;
        }
    }

    /**
     * CROP IMAGE
     *
     * @param inputFilePath
     */
    public void cropBackgroundImage(String inputFilePath, int threadId) {
        File cropImageFile = new File(ImageHelper.getInstance(PreviewImageActivity.this).
                getBackgroundImagePath(String.valueOf(threadId)));
        File fileInput = new File(inputFilePath);
        try {
            // intent crop
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, fileInput.getAbsolutePath());
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.PHOTO_FULL_SCREEN, true);
            intent.putExtra(CropView.RETURN_DATA, false);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_BACKGROUND);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

    public void cropAvatarImage(String filePath) {
        String time = String.valueOf(System.currentTimeMillis());
        File cropImageFile = new File(Config.Storage.REENG_STORAGE_FOLDER +
                Config.Storage.IMAGE_COMPRESSED_FOLDER, "/avatar" + time + Constants.FILE.JPEG_FILE_SUFFIX);

        File fileInput = new File(filePath);
        try {
            // intent crop
            Intent intent = new Intent(this, CropImageActivity.class);
            intent.putExtra(CropView.IMAGE_PATH, fileInput.getAbsolutePath());
            intent.putExtra(CropView.OUTPUT_PATH, cropImageFile.getPath());
            intent.putExtra(CropView.MASK_OVAL, true);
            intent.putExtra(CropView.RETURN_DATA, false);
            startActivityForResult(intent, Constants.ACTION.ACTION_CROP_AVATAR);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            showError(R.string.file_not_found_exception, null);
        }
    }

}


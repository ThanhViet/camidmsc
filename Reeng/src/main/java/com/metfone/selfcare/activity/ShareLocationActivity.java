package com.metfone.selfcare.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.sharelocation.ShareLocationFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanh on 3/12/2015.
 */
public class ShareLocationActivity extends BaseSlidingFragmentActivity implements
        ClickListener.IconListener, ShareLocationFragment.OnFragmentAppInfoListener {
    private static final String TAG = ShareLocationActivity.class.getSimpleName();
    private View mToolBarView;
    private ApplicationController mApplication;
    private ClickListener.IconListener mIconListener;
    private int mType = 0;
    private String mAddress, mLatitude, mLongitude;
    /*   private GoogleMap map;
    private Marker currentMarker;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_detail);
        changeStatusBar(true);
        setToolBar(findViewById(R.id.tool_bar));
        mToolBarView = findViewById(R.id.tool_bar);
        getDataAndDisplayFragment(savedInstanceState);
        mIconListener = this;
        trackingScreen(TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (!LocationHelper.getInstant(mApplication).isLocationServiceEnabled()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LocationHelper.getInstant(mApplication).showDialogSettingLocationProviders(ShareLocationActivity.this, mIconListener);
                }
            });
        } else if (!LocationHelper.getInstant(mApplication).isNetworkLocationEnabled()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LocationHelper.getInstant(mApplication).showDialogSettingHighLocation(ShareLocationActivity.this, mIconListener);
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.LOCATION.DATA_INPUT_TYPE, mType);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    private void getDataAndDisplayFragment(Bundle savedInstanceState) {
        mApplication = (ApplicationController) getApplicationContext();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mType = bundle.getInt(Constants.LOCATION.DATA_INPUT_TYPE);
            mAddress = bundle.getString(Constants.LOCATION.DATA_ADDRESS);
            mLatitude = bundle.getString(Constants.LOCATION.DATA_LATITUDE);
            mLongitude = bundle.getString(Constants.LOCATION.DATA_LONGITUDE);
        } else if (savedInstanceState != null) {
            mType = savedInstanceState.getInt(Constants.LOCATION.DATA_INPUT_TYPE);
            mAddress = savedInstanceState.getString(Constants.LOCATION.DATA_ADDRESS);
            mLatitude = savedInstanceState.getString(Constants.LOCATION.DATA_LATITUDE);
            mLongitude = savedInstanceState.getString(Constants.LOCATION.DATA_LONGITUDE);
        }
        displaySendLocationFragment();
    }

    private void displaySendLocationFragment() {
        ShareLocationFragment shareLocationFragment = ShareLocationFragment.
                newInstance(mType, mAddress, mLatitude, mLongitude);
        executeFragmentTransaction(shareLocationFragment, R.id.fragment_container, false, false);
    }

    public View getmToolBarView() {
        return mToolBarView;
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.ACTION.ACTION_SETTING_LOCATION:
                try {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
                catch (ActivityNotFoundException ex)
                {
                    Log.e(TAG, "Exception" + ex);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void returnResultShareLocation(String address, String latitude, String longitude) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.LOCATION.DATA_ADDRESS, address);
        returnIntent.putExtra(Constants.LOCATION.DATA_LATITUDE, latitude);
        returnIntent.putExtra(Constants.LOCATION.DATA_LONGITUDE, longitude);
        setResult(RESULT_OK, returnIntent);
        onBackPressed();
    }

    @Override
    public void navigateToDirect(LatLng fromLaLng, LatLng toLatLng) {
        Uri uri = Uri.parse(LocationHelper.getInstant(mApplication).getUrlDirectIntent(fromLaLng, toLatLng));
        NavigateActivityHelper.navigateToActionView(ShareLocationActivity.this, uri);
    }

    @Override
    public void navigateToMaps(LatLng toLatLng, String address) {
        Uri uri = Uri.parse(LocationHelper.getInstant(mApplication).getUrlMapsView(toLatLng, address));
        NavigateActivityHelper.navigateToActionView(ShareLocationActivity.this, uri);
    }
}
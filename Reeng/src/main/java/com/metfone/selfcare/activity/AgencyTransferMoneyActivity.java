package com.metfone.selfcare.activity;

import android.content.Intent;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.transfermoney.agency.ChangePassFragment;
import com.metfone.selfcare.fragment.transfermoney.agency.CreateNewTransferFragment;
import com.metfone.selfcare.helper.Constants;

/**
 * Created by thanhnt72 on 9/20/2019.
 */

public class AgencyTransferMoneyActivity extends BaseSlidingFragmentActivity {

    CreateNewTransferFragment createNewTransferFragment;

    public static void startActivity(BaseSlidingFragmentActivity activity) {
        Intent intent = new Intent(activity, AgencyTransferMoneyActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_new);
        createNewTransferFragment = CreateNewTransferFragment.newInstance();
        executeFragmentTransaction(createNewTransferFragment, R.id.fragment_container, false, false);
    }


    public void showResetPassFragment() {
        executeAddFragmentTransaction(ChangePassFragment.newInstance(), R.id.fragment_container, true, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setActivityForResult(false);
        switch (requestCode) {
            case Constants.CHOOSE_CONTACT.TYPE_CHOOSE_CONTACT_TRANSFER_MONEY:
                if (data != null) {
                    String jidNumber = data.getStringExtra(Constants.CHOOSE_CONTACT.RESULT_CONTACT_NUMBER);
                    if (createNewTransferFragment != null) {
                        createNewTransferFragment.onSelectContact(jidNumber);
                    }
                }
                break;
        }
    }


}

package com.metfone.selfcare.activity;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.event.SCQRScanEvent;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.ui.QRPointsOverlayView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by toanvk2 on 1/11/2017.
 */
public class QRCodeActivity extends BaseSlidingFragmentActivity implements QRCodeReaderView.OnQRCodeReadListener,
        View.OnClickListener {
    private static final String TAG = QRCodeActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSION = Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO;//1009

    private static final String TAG_REQUEST = "request_qr";
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private Resources mRes;
    private Handler mHandler;
    private ViewGroup mainLayout;
    private Button mBtnMyQrcode;
    private TextView ab_title;
    private QRCodeReaderView mQRReaderView;
    private ImageView mImgTorch, mImgAbBack;
    private QRPointsOverlayView mQROverlayView;
    private boolean isEnableTorch = false;
    private boolean isQRCodeRead = false;
    private EventOnMediaHelper eventOnMediaHelper;
    private int fromSource = Constants.QR_SOURCE.FROM_NONE;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scan);
        changeStatusBar(true);
        mHandler = new Handler();
        mApplication = (ApplicationController) getApplicationContext();
        mRes = mApplication.getResources();
        mainLayout = (ViewGroup) findViewById(R.id.qr_scan_main_layout);

        Intent intent = getIntent();
        fromSource = intent.getIntExtra("KEY_FROM_SOURCE", Constants.QR_SOURCE.FROM_NONE);
        if (PermissionHelper.allowedPermission(this, Manifest.permission.CAMERA)) {
            initQRCodeReaderView();
        } else {
            PermissionHelper.requestPermissionWithGuide(QRCodeActivity.this, Manifest.permission.CAMERA, REQUEST_PERMISSION);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mQRReaderView != null) {
            mQRReaderView.startCamera();
            handleTorch();
        }
    }

    @Override
    protected void onPause() {
        isEnableTorch = false;
        isQRCodeRead = false;
        if (mQRReaderView != null) {
            handleTorch();
            mQROverlayView.reset();
            mQRReaderView.stopCamera();
        }
        VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_REQUEST);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mHandler = null;
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if (PermissionHelper.allowedPermission(this, Manifest.permission.CAMERA)) {
                initQRCodeReaderView();
            } else {
                Toast.makeText(ApplicationController.self(), R.string.qr_err_camera_permission_denied, Toast.LENGTH_LONG).show();
                finish();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ab_back_btn:
                onBackPressed();
                break;
            case R.id.qr_scan_torch:
                isEnableTorch = !isEnableTorch;
                handleTorch();
                break;
            case R.id.btn_my_qrcode:
                if (mApplication.getReengAccountBusiness().isAnonymousLogin())
                    showDialogLogin();
                else
                    gotoMyQRCode();
                break;
        }
    }

    private void gotoMyQRCode() {
        Intent in = new Intent(QRCodeActivity.this, MyQRCodeActivity.class);
        startActivity(in);
    }

    private void initQRCodeReaderView() {
        try {
            View content = getLayoutInflater().inflate(R.layout.view_qr_code_v2, mainLayout, true);
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            );
            mImgAbBack = (ImageView) content.findViewById(R.id.ab_back_btn);
            mImgTorch = (ImageView) content.findViewById(R.id.qr_scan_torch);
            mBtnMyQrcode = (Button) content.findViewById(R.id.btn_my_qrcode);
            mQRReaderView = (QRCodeReaderView) content.findViewById(R.id.qr_scan_reader_view);
            mQROverlayView = (QRPointsOverlayView) content.findViewById(R.id.qr_scan_overlay_view);
            ab_title = (TextView) content.findViewById(R.id.ab_title);
            mBtnMyQrcode = (Button) content.findViewById(R.id.btn_my_qrcode);
            if (fromSource == Constants.QR_SOURCE.SCAN_SERIAL) {
                ab_title.setText("Scan Serial");
            }
            mImgAbBack.setOnClickListener(this);
            mBtnMyQrcode.setOnClickListener(this);
            mImgTorch.setOnClickListener(this);
            //
            mQRReaderView.setOnQRCodeReadListener(this);
            mQRReaderView.setQRDecodingEnabled(true);
            mQRReaderView.setAutofocusInterval(2000L);
            mQRReaderView.setBackCamera();
            mQRReaderView.startCamera();
            handleTorch();

            if (fromSource == Constants.QR_SOURCE.SCAN_SERIAL || fromSource == Constants.QR_SOURCE.FROM_SC_RECHARGE || fromSource == Constants.QR_SOURCE.FROM_SC_RECHARGE_HOME) {
                mBtnMyQrcode.setVisibility(View.GONE);
            } else {
                mBtnMyQrcode.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            Toast.makeText(ApplicationController.self(), R.string.qr_err_camera_permission_denied, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        Log.i(TAG, "onQRCodeRead: " + text);
        if (TextUtils.isEmpty(text)) return;
        if (!isQRCodeRead) {
            isQRCodeRead = true;
            mQROverlayView.setPoints(points);
            if (fromSource == Constants.QR_SOURCE.SCAN_SERIAL) {
                Intent intent = new Intent();
                intent.putExtra("QR_CODE", text.trim());
                setResult(RESULT_OK, intent);
                finish();

            } else
                handleReadQRCode(text.trim());
        }
    }

    private void handleReadQRCode(String response) {
        if (mQRReaderView != null) mQRReaderView.stopCamera();
        Log.d(TAG, "handleReadQRCode: " + response);

        //Check response co phai topup selfcare mytel
        if (response.startsWith("*121*") && response.endsWith("#")) {
            fromSource = Constants.QR_SOURCE.FROM_SC_RECHARGE_HOME;
            mBtnMyQrcode.setVisibility(View.GONE);
        }

        if (fromSource == Constants.QR_SOURCE.FROM_SC_RECHARGE) {

            if (response.contains("*")) {
                String[] listCode = response.split("\\*");
                if (listCode.length > 2)
                    response = listCode[2];

                response = response.replace("#", "");
            }

            final String finalResponse = response;
            String msg = String.format(mRes.getString(R.string.qrcode_link_msg), response);
            new DialogConfirm(QRCodeActivity.this, false)
                    .setLabel(mRes.getString(R.string.qr_title_dialog))
                    .setMessage(msg)
                    .setNegativeLabel(mRes.getString(R.string.close))
                    .setPositiveLabel(mRes.getString(R.string.update))
                    .setEntry(response)
                    .setNegativeListener(new NegativeListener() {
                        @Override
                        public void onNegative(Object result) {
                            onBackPressed();
                        }
                    })
                    .setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {
                            EventBus.getDefault().postSticky(new SCQRScanEvent(finalResponse));
                            finish();
                        }
                    }).show();
        } else if (fromSource == Constants.QR_SOURCE.FROM_SC_RECHARGE_HOME) {
            if (response.contains("*")) {
                String[] listCode = response.split("\\*");
                if (listCode.length > 2)
                    response = listCode[2];

                response = response.replace("#", "");
            }

            final String finalResponse1 = response;
            new DialogConfirm(QRCodeActivity.this, false)
                    .setLabel(mRes.getString(R.string.qr_title_dialog))
                    .setMessage(response)
                    .setNegativeLabel(mRes.getString(R.string.close))
                    .setPositiveLabel(mRes.getString(R.string.update))
                    .setEntry(response)
                    .setNegativeListener(new NegativeListener() {
                        @Override
                        public void onNegative(Object result) {
                            onBackPressed();
                        }
                    })
                    .setPositiveListener(new PositiveListener<Object>() {
                        @Override
                        public void onPositive(Object result) {

                            Intent intent = new Intent(QRCodeActivity.this, TabSelfCareActivity.class);
                            intent.putExtra(Constants.KEY_TYPE, SCConstants.SELF_CARE.TAB_RECHARGE);
                            intent.putExtra(Constants.KEY_DATA, new SCQRScanEvent(finalResponse1));
                            startActivity(intent);

                            finish();
                        }
                    }).show();
        } else {
            if (response != null &&
                    (response.startsWith("mocha.com.vn/api") ||
                            response.startsWith("http://mocha.com.vn/api") ||
                            response.startsWith("http://mocha.com.vn/user") ||
                            response.startsWith("https://mocha.com.vn/user") ||
                            response.startsWith("https://mocha.com.vn/api"))) {
                showLoadingDialog(null, R.string.waiting);
                VolleyHelper.getInstance(mApplication).cancelPendingRequests(TAG_REQUEST);
                requestSendQRCode(response, new QRCodeListener() {
                    @Override
                    public void onResponse(int type, String desc, UserInfo userInfo) {
                        hideLoadingDialog();
                        if (type == Constants.QR_CODE.TYPE_DISCOUNT) {
                            showDialogClose(desc);
                        } else if (type == Constants.QR_CODE.TYPE_DEEPLINK) {
                            DeepLinkHelper.getInstance().openSchemaLink(QRCodeActivity.this, desc);
                            finish();
                        } else if (type == Constants.QR_CODE.TYPE_USER_INFO) {
                            if (eventOnMediaHelper == null) {
                                eventOnMediaHelper = new EventOnMediaHelper(QRCodeActivity.this);
                            }

                            if (userInfo != null) {
                                eventOnMediaHelper.processUserClick(userInfo);
                            }
                        } else {
                            showToast(desc, Toast.LENGTH_LONG);
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            }, 1000);
                        }
                    }

                    @Override
                    public void onError(int errorCode) {
                        hideLoadingDialog();
                        showDialogClose(mRes.getString(R.string.qr_err_request));
                    /*if (mQRReaderView != null) {
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isQRCodeRead=false;
                                mQROverlayView.reset();
                                mQRReaderView.startCamera();
                                handleTorch();
                            }
                        }, 2000);
                    }*/
                    }
                });
            } else if (URLUtil.isValidUrl(response)) {
                new DialogConfirm(QRCodeActivity.this, false)
                        .setLabel(mRes.getString(R.string.qr_title_dialog))
                        .setMessage(response)
                        .setNegativeLabel(mRes.getString(R.string.close))
                        .setPositiveLabel(mRes.getString(R.string.qr_open_url))
                        .setEntry(response)
                        .setNegativeListener(new NegativeListener() {
                            @Override
                            public void onNegative(Object result) {
                                onBackPressed();
                            }
                        })
                        .setPositiveListener(new PositiveListener<Object>() {
                            @Override
                            public void onPositive(Object result) {
                                UrlConfigHelper.getInstance(mApplication).gotoWebViewOnMedia(mApplication, QRCodeActivity.this, (String) result);
                                finish();
                            }
                        }).show();
            } else {
                new DialogConfirm(QRCodeActivity.this, false)
                        .setLabel(mRes.getString(R.string.qr_title_dialog))
                        .setMessage(response)
                        .setNegativeLabel(mRes.getString(R.string.close))
                        .setPositiveLabel(mRes.getString(R.string.copy))
                        .setEntry(response)
                        .setNegativeListener(new NegativeListener() {
                            @Override
                            public void onNegative(Object result) {
                                onBackPressed();
                            }
                        })
                        .setPositiveListener(new PositiveListener<Object>() {
                            @Override
                            public void onPositive(Object result) {
                                TextHelper.copyToClipboard(mApplication, (String) result);
                                onBackPressed();
                            }
                        }).show();
            }
        }
    }

    private void handleTorch() {
        Log.d(TAG, "handleTorch: " + isEnableTorch);
        if (mQRReaderView != null) {
            mQRReaderView.setTorchEnabled(isEnableTorch);
            if (isEnableTorch)
                mImgTorch.setImageResource(R.drawable.ic_flash_on);
            else
                mImgTorch.setImageResource(R.drawable.ic_flash_off);
        }
    }

    private void requestSendQRCode(final String qrResult, final QRCodeListener listener) {
        mAccountBusiness = mApplication.getReengAccountBusiness();
        if (!mAccountBusiness.isValidAccount() || !NetworkHelper.isConnectInternet(mApplication)) {
            listener.onError(-2);
            return;
        }
        final String url = UrlConfigHelper.getInstance(mApplication).getUrlConfigOfFile(Config.UrlEnum.SCAN_QR_CODE);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String decryptResponse = HttpHelper.decryptResponse(response, mApplication.getReengAccountBusiness().getToken());

                        Log.d(TAG, "onResponse: scan qr code: " + decryptResponse);
                        int errorCode = -1;
                        try {
                            JSONObject responseObject = new JSONObject(decryptResponse);
                            errorCode = responseObject.optInt(Constants.HTTP.REST_CODE);
                            if (errorCode == HTTPCode.E200_OK) {
                                String desc = responseObject.optString(Constants.HTTP.REST_DESC, mRes.getString(R.string.request_send_success));
                                int type = responseObject.optInt("type", -1);
                                UserInfo userInfo = new UserInfo();
                                if (responseObject.has("userInfo")) {
                                    JSONObject objectInfo = responseObject.getJSONObject("userInfo");
                                    userInfo.setMsisdn(objectInfo.optString("msisdn"));
                                    userInfo.setName(objectInfo.optString("name"));
                                    userInfo.setAvatar(objectInfo.optString("lavatar"));
                                    userInfo.setBirthday(objectInfo.optString("birthdayStr"));
                                    userInfo.setStatus(objectInfo.optString("status"));
                                    userInfo.setUser_type(UserInfo.USER_ONMEDIA_NORMAL);
                                    userInfo.setStateMocha(1);
                                }
                                listener.onResponse(type, desc, userInfo);
                            } else {
                                listener.onError(errorCode);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                            listener.onError(errorCode);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError", error);
                listener.onError(-1);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                long currentTime = TimeHelper.getCurrentTime();
                StringBuilder sb = new StringBuilder().
                        append(mAccountBusiness.getJidNumber()).
                        append(qrResult).
                        append(mAccountBusiness.getCurrentLanguage()).
                        append(mAccountBusiness.getCountryCode()).
                        append(mAccountBusiness.getToken()).
                        append(currentTime);
                String encrypt = HttpHelper.encryptDataV2(mApplication, sb.toString(), mAccountBusiness.getToken());
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.HTTP.REST_MSISDN, mAccountBusiness.getJidNumber());
                params.put("qrCode", qrResult);
                params.put("language", mAccountBusiness.getCurrentLanguage());
                params.put("country", mAccountBusiness.getCountryCode());
                params.put(Constants.HTTP.TIME_STAMP, String.valueOf(currentTime));
                params.put(Constants.HTTP.DATA_SECURITY, encrypt);
                return params;
            }
        };
        VolleyHelper.getInstance(mApplication).addRequestToQueue(request, TAG_REQUEST, false);
    }

    private void showDialogClose(String message) {
        new DialogConfirm(QRCodeActivity.this, false)
                .setLabel(mRes.getString(R.string.qr_title_dialog))
                .setMessage(message)
                .setNegativeLabel(mRes.getString(R.string.close))
                .setPositiveLabel(null)
                .setNegativeListener(new NegativeListener() {
                    @Override
                    public void onNegative(Object result) {
                        onBackPressed();
                    }
                }).show();
    }

    public interface QRCodeListener {
        void onResponse(int type, String desc, UserInfo userInfo);

        void onError(int errorCode);
    }
}

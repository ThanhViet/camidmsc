package com.metfone.selfcare.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.network.file.DownloadListener;
import com.metfone.selfcare.network.file.DownloadRequest;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ProgressWheel;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

public class VideoMochaActivity extends BaseSlidingFragmentActivity implements
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        SeekBar.OnSeekBarChangeListener,
        MediaPlayer.OnErrorListener,
        DownloadListener {
    private static final String TAG = VideoMochaActivity.class.getSimpleName();
    private ApplicationController mApplication;

    private String videoPath;
    private VideoView mVideoView;
    private SeekBar mSeekbar;
    private RelativeLayout barTopPlayer;
    private RelativeLayout barBotPlayer;
    private ImageView btnFullScreen;
    private AppCompatImageView bBack;
    private ProgressLoading vProgress;
    private ImageView bPause;
    private TextView tTime;
    private TextView tFulltime;
    private CountDownTimer cdHide;
    private ProgressWheel mProgress;

    boolean mFocusShow = true;
    boolean isPrepared = false;
    private Handler mHandler;
    private boolean flagAlwaysShowController = false;
    private boolean isFullScreen = false;
    private int height;
    //    private RelativeLayout mRlBot;
    private String msToClockTime;
    private static final int ERROR_SUPPORT_MEDIA = -38;
    private int mThreadId;
    private int mMessageId;

    private ThreadMessage mThreadMessage;
    private ReengMessage mReengMesage;

    private MessageBusiness mMessageBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mApplication = (ApplicationController) getApplicationContext();
        setContentView(R.layout.activity_mocha_playvideo);
        changeStatusBar(true);
        findComponentViews();
        setViewListener();
        setComponentViews();
        trackingScreen(TAG);
        mMessageBusiness = mApplication.getMessageBusiness();
        getMediaData();
        if (mReengMesage.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD ||
                mReengMesage.getStatus() == ReengMessageConstant.STATUS_FAIL) {
            mApplication.getTransferFileBusiness().startDownloadMessageFile(mReengMesage);
        } else {
            videoPath = mReengMesage.getFilePath();
            showVideo();
        }
    }

    private void setViewListener() {
        bPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isPrepared || mVideoView == null) return;
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                    bPause.setBackgroundResource(R.drawable.ic_video_play);
                } else {
                    mSeekbar.setProgress(0);
                    mVideoView.start();
                    bPause.setBackgroundResource(R.drawable.ic_video_pause);
                    updateProgressBar();

                }
            }
        });
        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toogleScreen();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ListenerHelper.getInstance().addDownloadListener(this);
//        doShowVideo();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onDestroy: ");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    private void getMediaData() {
        mThreadId = getIntent().getIntExtra(ReengMessageConstant.MESSAGE_THREAD_ID, -1);
        mMessageId = getIntent().getIntExtra(ReengMessageConstant.MESSAGE_ID, -1);
        if (mThreadId != -1 && mMessageId != -1) {
            mThreadMessage = mMessageBusiness.getThreadById(mThreadId);
            mReengMesage = mMessageBusiness.getMessageByMessageIdInThread(mMessageId, mThreadMessage);
        } else {
            finish();
        }
    }

    private void findComponentViews() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mProgress = (ProgressWheel) findViewById(R.id.progress_loading);
        mSeekbar = (SeekBar) findViewById(R.id.v_p_seekbar);
        mVideoView = (VideoView) findViewById(R.id.video);
        barTopPlayer = (RelativeLayout) findViewById(R.id.video_topbar);
        barBotPlayer = (RelativeLayout) findViewById(R.id.video_bot_bar);
        vProgress = (ProgressLoading) findViewById(R.id.video_progress);
        tTime = (TextView) findViewById(R.id.v_p_text);
        tFulltime = (TextView) findViewById(R.id.v_p_fulltime);
        btnFullScreen = (ImageView) findViewById(R.id.btnSwitch);

        bPause = (ImageView) findViewById(R.id.v_p_full);
        bBack = (AppCompatImageView) findViewById(R.id.btn_back);
//        mRlBot = (RelativeLayout) findViewById(R.id.rl_bot);
        Display mDisplay = getWindowManager().getDefaultDisplay();
        height = mDisplay.getHeight();

        btnFullScreen.setVisibility(View.GONE);
    }

    private void setComponentViews() {
        mHandler = new Handler();
    }

    private void showVideo() {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            doShowVideo();
        } else {
            if (mHandler == null) mHandler = new Handler();
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    doShowVideo();
                }
            });
        }
    }

    private void doShowVideo() {
        try {
            flagAlwaysShowController = false;
            mVideoView.setVideoPath(videoPath);
            vProgress.setVisibility(View.VISIBLE);
            mVideoView.requestFocus();
            mVideoView.start();
            mVideoView.setOnPreparedListener(this);
            mVideoView.setOnCompletionListener(this);
            mVideoView.setOnErrorListener(this);
            mSeekbar.setOnSeekBarChangeListener(this);
            mProgress.setVisibility(View.GONE);
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        if (what == ERROR_SUPPORT_MEDIA && extra == 0) {
            showError2();
            return true;
        }
        Log.i(TAG, "onError" + what + "/"
                + extra);
        showError();
        // showError();
        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPrepared: ");
        isPrepared = true;
        mediaPlayer.start();
//        hideBar();
        msToClockTime = TimeHelper.msToClockTime(mVideoView.getDuration());
        bPause.setBackgroundResource(R.drawable.ic_video_pause);
        tTime.setText("00:00 ");
        tFulltime.setText(msToClockTime);
        updateProgressBar();
        hideBar();
        vProgress.setVisibility(View.GONE);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        if (fromUser) {
            int totalDuration = mVideoView.getDuration();
            int temp = progress * totalDuration / 100;
            tTime.setText(Utilities.milliSecondsToTimer(temp));
            tFulltime.setText(msToClockTime);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        if (mVideoView != null) {
            int totalDuration = mVideoView.getDuration();
            int currentPosition = Utilities.progressToTimer(
                    seekBar.getProgress(), totalDuration);
            mVideoView.seekTo(currentPosition);
            updateProgressBar();

        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        bPause.setBackgroundResource(R.drawable.ic_video_play);
        mSeekbar.setProgress(100);
        bPause.setVisibility(View.VISIBLE);
        barTopPlayer.setVisibility(View.VISIBLE);
        barBotPlayer.setVisibility(View.VISIBLE);
        if (cdHide != null) {
            cdHide.cancel();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int top = (int) (0.1 * height);
        int bot = (int) (0.9 * height);
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (!(event.getY() < top || event.getY() > bot)) {
                if (barTopPlayer.getVisibility() == View.GONE) {
                    barTopPlayer.setVisibility(View.VISIBLE);
                    barBotPlayer.setVisibility(View.VISIBLE);
                    bPause.setVisibility(View.VISIBLE);
                } else {
                    barTopPlayer.setVisibility(View.GONE);
                    barBotPlayer.setVisibility(View.GONE);
                    bPause.setVisibility(View.GONE);
                    // currentHide++;
                }
                return true;
            }
        }
        return false;
    }

    private void hideBar() {
        barTopPlayer.setVisibility(View.VISIBLE);
        barBotPlayer.setVisibility(View.VISIBLE);
        bPause.setVisibility(View.VISIBLE);
        mFocusShow = true;
        if (cdHide != null) {
            cdHide.cancel();
        }
        cdHide = new CountDownTimer(1000 * 60 * 60, 8000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (flagAlwaysShowController || mFocusShow) {
                    barTopPlayer.setVisibility(View.VISIBLE);
                    barBotPlayer.setVisibility(View.VISIBLE);
                    if (vProgress.getVisibility() != View.VISIBLE) {
                        bPause.setVisibility(View.VISIBLE);
                    }
                    mFocusShow = false;
                } else {
                    barTopPlayer.setVisibility(View.INVISIBLE);
                    barBotPlayer.setVisibility(View.INVISIBLE);
                    bPause.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFinish() {
            }
        };
        cdHide.start();
    }

    private void smallScreen() {
        isFullScreen = false;
        rotateScreen();
        btnFullScreen.setImageResource(R.drawable.ic_om_video_fullscreen);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//        mRlBot.setVisibility(View.VISIBLE);
        if (barTopPlayer != null)
            barTopPlayer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
        if (barBotPlayer != null)
            barBotPlayer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mVideoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = (int) (200 * metrics.density);
        Log.i(TAG, "VideoPlayerActivity.smallScreen() p " + metrics.widthPixels + "/ " + metrics.heightPixels);
        mVideoView.setLayoutParams(params);
        mVideoView.getParent().requestLayout();
    }

    private void fullScreen() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        isFullScreen = true;
        rotateScreen();

        btnFullScreen.setImageResource(R.drawable.ic_om_video_smallscreen);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mVideoView
                .getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;

        params.leftMargin = 0;
        params.topMargin = 0;
        params.bottomMargin = 0;
        params.rightMargin = 0;

        Log.i(TAG, "VideoPlayerActivity.fullScreen() p " + metrics.widthPixels + "/ " + metrics.heightPixels);
        //        mRlBot.setVisibility(View.GONE);
        if (barTopPlayer != null)
            barTopPlayer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
        if (barBotPlayer != null)
            barBotPlayer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
        mVideoView.setLayoutParams(params);
        mVideoView.getParent().requestLayout();
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private void rotateScreen() {
        if (isFullScreen)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void toogleScreen() {
        if (!isFullScreen) {
            fullScreen();
        } else {
            smallScreen();
        }

    }

    public void updateProgressBar() {
        mHandler.removeCallbacks(mUpdateTimeTask);
        if (mVideoView != null) {
            mHandler.postDelayed(mUpdateTimeTask, Constants.ONMEDIA.ONE_SECOND);
        }
    }

    private Runnable mUpdateTimeTask = new Runnable() {

        public void run() {
            if (!mVideoView.isPlaying()) {
                return;
            }
            int totalDuration = mVideoView.getDuration();
            long currentDuration = mVideoView.getCurrentPosition();
            tTime.setText(Utilities.milliSecondsToTimer(currentDuration));
            tFulltime.setText(msToClockTime);
            int progress = Utilities.getProgressPercentage(currentDuration, totalDuration);
            mSeekbar.setProgress(progress);
            mHandler.postDelayed(this, Constants.ONMEDIA.ONE_SECOND);
        }
    };

    @Override
    public void onBackPressed() {
        if (isFullScreen) {
            toogleScreen();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ListenerHelper.getInstance().removeDownloadListener(this);
        if (mVideoView != null)
            mVideoView.stopPlayback();
        mHandler.removeCallbacks(mUpdateTimeTask);
        if (cdHide != null)
            cdHide.cancel();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
        setActivityForResult(false);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                default:
                    break;
            }
        }
    }

    private void showError2() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setMessage("" + getString(R.string.video_load_fail_support));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        adb.show();
    }

    private void showError() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setMessage("" + getString(R.string.video_load_fail_support2));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        adb.setIcon(R.mipmap.ic_launcher);
        adb.show();
    }

    @Override
    public void onDownloadStarted(DownloadRequest request) {
        mProgress.setProgress(1);
    }

    @Override
    public void onDownloadComplete(DownloadRequest downloadRequest) {
        Log.d(TAG, "onDownloadSuccess: fileUri" + downloadRequest.getFilePath());
        videoPath = downloadRequest.getFilePath();
        showVideo();
    }

    @Override
    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {

    }

    @Override
    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
        int percentProgress = 360 * progress / 100;
        mProgress.setProgress(percentProgress);
    }
}
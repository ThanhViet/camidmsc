package com.metfone.selfcare.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

public class QuickMissCallActivity extends BaseSlidingFragmentActivity implements View.OnClickListener {
    public static final String TAG = QuickMissCallActivity.class.getSimpleName();
    public static final String BG_TRANSPARENT = "bgTranparent";
    public static final String MISS_CALL_NUMBER = "miss_call_number";
    public static final String MISS_CALL_TIME = "miss_call_time";
    public static final String SCREEN_OFF = "screen_off";
    private ApplicationController mApplication;
    private ApplicationStateManager mAppStateManager;
    private ContactBusiness mContactBusiness;
    private Resources mRes;

    private View parentView, mViewBtnEmpty;
    private ImageView mImgClose;
    private CircleImageView mImgAvatar;
    private TextView mTvwAvatar, mTvwFriendName, mTvwMissCallState;
    private RoundTextView mBtnChat, mBtnCallBack;
    private View mViewHeader;

    private Handler mHandler;
    private CountDownTimer countDownTimer = null;
    private static final int TIME_DEFAULT = 15000;

    private boolean bgTransparent = true, isTouchQuickReply = false, isScreenOff = false;
    private boolean disableCountdown = false;

    private boolean isStop = false;
    //quick miss call
    private String missCallFriendNumber;
    private long timeCall;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        mApplication = (ApplicationController) getApplicationContext();
        mAppStateManager = mApplication.getAppStateManager();
        mContactBusiness = mApplication.getContactBusiness();
        mRes = getResources();
        changeStatusBar(true);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0 ||
                !mApplication.isDataReady()) {
            navigateToHome();
        } else {
            setFlagWindow();
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.activity_quick_miss_call, null);
            setContentView(view);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            findComponentViews(view);
            getData();
            drawDetail();
            setViewListener();
        }
        trackingScreen(TAG);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (mHandler == null) {
            mHandler = new Handler();
        }
        drawStateAndTime();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "isStop: " + isStop);
        if (isStop) {
            cancelCountDown();
            mAppStateManager.setShowQuickReply(false);
            mAppStateManager.applicationEnterBackground(this, isTouchQuickReply);
            mHandler = null;
            mApplication.wakeLockRelease();
            Window win = getWindow();
            win.clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                    | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            );
        } else {
            isStop = true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        startCountDown();
        Log.i(TAG, "onStart");
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!disableCountdown) {
            disableCountdown = true;
            cancelCountDown();
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.quick_miss_call_root:
                mAppStateManager.setShowQuickReply(false);
                finish();
                break;
            case R.id.quick_miss_call_close:
                mAppStateManager.setShowQuickReply(false);
                finish();
                break;
            case R.id.quick_miss_call_header_layout:
            case R.id.quick_miss_call_button_chat:
                navigateToChatDetail();
                break;
            case R.id.quick_miss_call_button_call:
                navigateToCall();
                break;
        }
    }

    private void setFlagWindow() {
        if (mAppStateManager.isScreenLocker() || !mAppStateManager.isScreenOn()) {
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        } else {
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
    }

    private void findComponentViews(View rootView) {
        parentView = rootView.findViewById(R.id.quick_miss_call_root);
        mViewHeader = rootView.findViewById(R.id.quick_miss_call_header_layout);
        View buttonView = rootView.findViewById(R.id.quick_miss_call_button_layout);
        mImgClose = (ImageView) rootView.findViewById(R.id.quick_miss_call_close);
        mImgAvatar = (CircleImageView) rootView.findViewById(R.id.quick_miss_call_avatar);
        mTvwAvatar = (TextView) rootView.findViewById(R.id.quick_miss_call_avatar_text);
        mTvwFriendName = (TextView) rootView.findViewById(R.id.quick_miss_call_friend_name);
        mTvwMissCallState = (TextView) rootView.findViewById(R.id.quick_miss_call_state);
        mBtnChat = (RoundTextView) rootView.findViewById(R.id.quick_miss_call_button_chat);
        mBtnCallBack = (RoundTextView) rootView.findViewById(R.id.quick_miss_call_button_call);
        mViewBtnEmpty = rootView.findViewById(R.id.quick_miss_call_button_empty);
        buttonView.setOnClickListener(this);
    }

    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isScreenOff = extras.getBoolean(SCREEN_OFF);
            Log.i(TAG, "bgTransparent: " + bgTransparent + " screen off: " + isScreenOff);
            isStop = !isScreenOff && !mAppStateManager.isScreenLocker();
            missCallFriendNumber = extras.getString(MISS_CALL_NUMBER);
            timeCall = extras.getLong(MISS_CALL_TIME, TimeHelper.getCurrentTime());
        }
        if (!bgTransparent) {
            parentView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
        }
    }

    private void drawDetail() {
        isTouchQuickReply = false;
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(missCallFriendNumber);
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        if (mPhoneNumber != null) {
            mTvwFriendName.setText(mPhoneNumber.getName());
            avatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, mPhoneNumber, size);
            if (mPhoneNumber.isReeng()) {
                mBtnCallBack.setVisibility(View.VISIBLE);
                mViewBtnEmpty.setVisibility(View.VISIBLE);
                mBtnChat.setText(mRes.getString(R.string.chat));
            } else {
                mBtnCallBack.setVisibility(View.GONE);
                mViewBtnEmpty.setVisibility(View.GONE);
                mBtnChat.setText(mRes.getString(R.string.quick_miss_free_sms));
            }
        } else {
            StrangerPhoneNumber stranger = mApplication.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(missCallFriendNumber);
            if (stranger != null) { // neu so co trong bang stranger thi lay ten o day
                if (TextUtils.isEmpty(stranger.getFriendName())) {
                    mTvwFriendName.setText(Utilities.hidenPhoneNumber(missCallFriendNumber));
                } else {
                    mTvwFriendName.setText(stranger.getFriendName());
                }
                avatarBusiness.setStrangerAvatar(mImgAvatar, mTvwAvatar, stranger, missCallFriendNumber, null, null, size);
            } else {
                mTvwFriendName.setText(missCallFriendNumber);
                avatarBusiness.setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, missCallFriendNumber, size);
            }
            mBtnCallBack.setVisibility(View.VISIBLE);
            mViewBtnEmpty.setVisibility(View.VISIBLE);
            mBtnChat.setText(mRes.getString(R.string.chat));
        }
        drawStateAndTime();
    }

    private void drawStateAndTime() {
        int minutes = (int) (TimeHelper.getCurrentTime() - timeCall) / 60000;
        if (minutes <= 1) {
            mTvwMissCallState.setText(String.format(mRes.getString(R.string.quick_miss_call_state), "1"));
        } else {
            mTvwMissCallState.setText(String.format(mRes.getString(R.string.quick_miss_call_state_2), String.valueOf(minutes)));
        }
    }

    private void setViewListener() {
        mViewHeader.setOnClickListener(this);
        parentView.setOnClickListener(this);
        mImgClose.setOnClickListener(this);
        mBtnChat.setOnClickListener(this);
        mBtnCallBack.setOnClickListener(this);
    }

    private void navigateToHome() {
        mAppStateManager.setShowQuickReply(false);
        mAppStateManager.applicationEnterForeground(QuickMissCallActivity.this, true);
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void navigateToCall() {
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(missCallFriendNumber);
        mApplication.getMessageBusiness().markAllMessageIsReadAndCheckSendSeen(thread, thread.getAllMessages());

        mAppStateManager.setShowQuickReply(false);
        mAppStateManager.applicationEnterForeground(QuickMissCallActivity.this, true);
        mApplication.getCallBusiness().checkAndStartCall(QuickMissCallActivity.this, thread);
        finish();
    }

    private void navigateToChatDetail() {
        mAppStateManager.setShowQuickReply(false);
        mAppStateManager.applicationEnterForeground(QuickMissCallActivity.this, true);
        ThreadMessage thread = mApplication.getMessageBusiness().findExistingOrCreateNewThread(missCallFriendNumber);
        Intent intent;
        intent = new Intent(getApplicationContext(), ChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(ThreadMessageConstant.THREAD_ID, thread.getId());
        bundle.putInt(ThreadMessageConstant.THREAD_IS_GROUP, ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    //mApplication.getXmppManager().sendPresenceBackgroundOrForeground(true);//TODO send background, no mark state
    private void startCountDown() {
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(TIME_DEFAULT, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    mAppStateManager.setShowQuickReply(false);
                    finish();
                }
            }.start();
        }
    }

    private void cancelCountDown() {
        try {
            if (countDownTimer != null) {
                countDownTimer.cancel();
                countDownTimer = null;
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }
}
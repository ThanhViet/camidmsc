package com.metfone.selfcare.common.api;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogImpl extends BaseApi implements LogApi {

    public LogImpl(ApplicationController application) {
        super(application);
    }

    @Override
    public void logBanner(String deepLink, TypeService serviceType, ApiCallback callback) {
        long time = System.currentTimeMillis();
        String timeStamp = String.valueOf(time);
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String date = format.format(new Date(time));
        // msisdn + serviceType + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + serviceType.VALUE
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(getDomainFile(), Url.File.LOG_BANNER)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.SERVICE_TYPE, serviceType.VALUE)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.DEEP_LINK, deepLink)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.DATE, date)
                .putParameter(Parameter.DATA_ENCRYPT, security)
                .execute();
    }

    @Override
    public void logAction(LogType logType, TabType tabType, String data, ApiCallback callback) {
        Log.d("LogImpl", "logAction: " + data);
        if (TextUtils.isEmpty(data)) return;
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + data + Constants.HTTP.CLIENT_TYPE_STRING
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());
        post(getDomainFile(), Url.File.LOG_ACTION)
                .putParameter("data", data)
                .putParameter("source", tabType.VALUE)
                .putParameter("logType", logType.VALUE)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.LANGUAGE_CODE, getReengAccountBusiness().getCurrentLanguage())
                .putParameter(Parameter.COUNTRY_CODE, getReengAccountBusiness().getRegionCode())
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .execute();
    }
}

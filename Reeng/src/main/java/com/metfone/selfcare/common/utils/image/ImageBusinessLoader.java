package com.metfone.selfcare.common.utils.image;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


public final class ImageBusinessLoader {
    public static void loadImageNormal(String url, ImageView imageView) {
        Glide.with(imageView).load(url).into(imageView);
    }

    public static void loadImageNormal(String url, int thumb, ImageView imageView) {
        Glide.with(imageView).load(url).apply(new RequestOptions().placeholder(thumb)).into(imageView);
    }

    public static void loadImageNormal(String url, int thumb, int fail, ImageView imageView) {
        Glide.with(imageView).load(url).apply(new RequestOptions().placeholder(thumb).error(fail)).into(imageView);
    }
}

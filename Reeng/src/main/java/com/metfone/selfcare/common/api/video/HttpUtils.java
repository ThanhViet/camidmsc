package com.metfone.selfcare.common.api.video;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.util.Utilities;

public final class HttpUtils {

    public static Http.Builder createBuilder(ApplicationController app) {
        Http.Builder builder = Http.create(app);
        builder.putHeader(BaseApi.MOCHA_API, app.getReengAccountBusiness().getMochaApi());
        builder.putHeader(BaseApi.UUID, Utilities.getUuidApp());
        return builder;
    }
}

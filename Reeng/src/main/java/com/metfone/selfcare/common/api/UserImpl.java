package com.metfone.selfcare.common.api;

import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.ConfigLocalized;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

import static com.metfone.selfcare.firebase.FireBaseHelper.FIREBASE_PROPERTY_SEND_SERVER;

public class UserImpl extends BaseApi implements UserApi {

    private static final String TAG = UserImpl.class.getSimpleName();

    public UserImpl(ApplicationController application) {
        super(application);
    }

    @Override
    public void getAvatars(CompositeDisposable compositeDisposable, String contact, final AvatarCallback avatarCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + contact + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(getDomainFile(), Url.File.LIST_AVATAR)
                .setCompositeDisposable(compositeDisposable)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CONTACT, contact)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        JSONObject jsonObject = new JSONObject(data);
                        int code = jsonObject.optInt("code");
                        if (code == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("listUsersAvatar");
                            ArrayList<ImageProfile> imageProfiles = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject itemObject = jsonArray.getJSONObject(i);
                                ImageProfile imageProfile = new ImageProfile();
                                imageProfile.setIdServerString(itemObject.optString("onmediaId"));
                                imageProfile.setImageUrl(itemObject.optString("imageUrl"));
                                imageProfile.setTime(itemObject.optLong("lavatar"));
//                                imageProfile.setImagePathLocal(itemObject.optString("uname") + "_" + itemObject.optString("lavatar"));
                                imageProfiles.add(imageProfile);
                            }
                            if (avatarCallback != null)
                                avatarCallback.onGetAvatarSuccess(imageProfiles);
                        } else {
                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (avatarCallback != null) avatarCallback.onGetAvatarError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (avatarCallback != null) avatarCallback.onGetAvatarCompleted();
                    }
                }).execute();
    }

    @Override
    public void removeAvatar(CompositeDisposable compositeDisposable, String lAvatar, final RemoveAvatarCallback avatarCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + lAvatar
//                + Constants.HTTP.CLIENT_TYPE_STRING
//                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        post(domainFile + Url.File.REMOVE_AVATAR)
                .setCompositeDisposable(compositeDisposable)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("lavatar", lAvatar)
//                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
//                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        JSONObject jsonObject = new JSONObject(data);
                        int code = jsonObject.optInt("code");
                        if (code == 200) {
                            String lAvatar = jsonObject.optString("lavatar");
                            if (avatarCallback != null)
                                avatarCallback.onRemoveSuccess(lAvatar);
                        } else {
                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (avatarCallback != null) avatarCallback.onRemoveFail(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                }).execute();
    }

    @Override
    public void removeImageAlbum(CompositeDisposable compositeDisposable, ImageProfile imageProfile,
                                 final ProfileRequestHelper.onResponseRemoveImageProfileListener listener) {
        long timeStamp = System.currentTimeMillis();
        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

        String security = HttpHelper.encryptDataObject(application,
                account.getJidNumber(), account.getToken(), timeStamp, imageProfile.getIdServerString());

        post(domainFile + Url.File.REMOVE_IMAGE_ALBUM)
                .setCompositeDisposable(compositeDisposable)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter("dataEncrypt", security)
                .putParameter("reqTime", String.valueOf(timeStamp))
                .putParameter("imageId", imageProfile.getIdServerString())
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws JSONException {
                        int errorCode = -1;
                        JSONObject responseObject = new JSONObject(response);
                        if (responseObject.has(Constants.HTTP.REST_CODE))
                            errorCode = responseObject.getInt(Constants.HTTP.REST_CODE);
                        if (errorCode == HTTPCode.E200_OK) {
                            listener.onResponse();
                        } else {
                            listener.onError(errorCode);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        listener.onError(-1);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                }).execute();
    }

    @Override
    public void getInfoAnonymous(final String keyXxtea, final ApiCallbackV2<String> apiCallback) {
//        String timeStamp = String.valueOf(System.currentTimeMillis());
//        final String domainFile = getDomainFile();
//        post(domainFile + Url.File.GET_INFO_ANONYMOUS)
//                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
//                .putParameter(Parameter.REVISION, Config.REVISION)
//                .putParameter(Parameter.COUNTRY_CODE, "VN")
//                .putParameter("uuid", Utilities.getUuidApp())
//                .putParameter("app_name", "mocha")
//                .putParameter("switchCode", "dev")
//                .putParameter(Parameter.DEVICE, Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL)
//                .withCallBack(new HttpCallBack() {
//                    @Override
//                    public void onSuccess(String data) throws JSONException {
//                        String decryptResponse = HttpHelper.decryptResponse(data, keyXxtea);
//                        if (TextUtils.isEmpty(decryptResponse)) {
//                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
//                            return;
//                        }
//                        Log.i(TAG, "getInfoAnonymous decryptResponse: " + decryptResponse);
//
//                        JSONObject jsonObject = new JSONObject(decryptResponse);
//                        int code = jsonObject.optInt("code");
//                        if (code == 200) {
//                            String jid = jsonObject.optString("msisdn");
//                            String token = jsonObject.optString("token");
//                            String countryCode = jsonObject.optString("country_code", "VN");
//                            application.getReengAccountBusiness().createReengAccountBeforeLogin(application, jid, "VN");
//
//                            ReengAccount reengAccount = application.getReengAccountBusiness().getCurrentAccount();
//                            reengAccount.setName("Mocha");
//                            reengAccount.setToken(token);
//                            reengAccount.setActive(true);
//                            reengAccount.setRegionCode(countryCode);
//                            application.getReengAccountBusiness().setAnonymous(true);
//                            application.getReengAccountBusiness().updateReengAccount(reengAccount);
//
//                            JSONObject subsObject = jsonObject.optJSONObject("packageInfo");
//                            if (subsObject != null) {
//                                UrlConfigHelper urlConfigHelper = UrlConfigHelper.getInstance(application);
//                                String jDomainFile = subsObject.optString("domain_file", ConfigLocalized.DOMAIN_FILE);
//                                String jDomainImage = subsObject.optString("domain_img", ConfigLocalized.DOMAIN_IMAGE);
//                                String jDomainOnmedia = subsObject.optString("domain_on_media", ConfigLocalized.DOMAIN_ON_MEDIA);
//                                String jDomainKservice = subsObject.optString("kservice", ConfigLocalized.DOMAIN_SERVICE_KEENG);
//                                String jDomainKmedia = subsObject.optString("kmedia", ConfigLocalized.DOMAIN_MEDIA2_KEENG);
//                                String jDomainKimage = subsObject.optString("kimage", ConfigLocalized.DOMAIN_IMAGE_KEENG);
//                                urlConfigHelper.updateDomainMocha(jDomainFile, "", jDomainOnmedia, jDomainImage, "");
//                                urlConfigHelper.updateDomainKeeng(jDomainKservice, jDomainKmedia, jDomainKimage);
//
//                                boolean isDev = false;
//                                if (application.getReengAccountBusiness() != null)
//                                    isDev = application.getReengAccountBusiness().isDev();
//                                urlConfigHelper.updateDomainFileV1(subsObject.optString("domain_file_v1", urlConfigHelper.getDomainDefault("FILE", isDev)));
//                                urlConfigHelper.updateDomainImageV1(subsObject.optString("domain_img_v1", urlConfigHelper.getDomainDefault("IMAGE", isDev)));
//                                urlConfigHelper.updateDomainOnMediaV1(subsObject.optString("domain_on_media_v1", urlConfigHelper.getDomainDefault("ON_MEDIA", isDev)));
//                                urlConfigHelper.updateDomainMochaVideo(subsObject.optString("domain_mcvideo", urlConfigHelper.getDomainDefault("MC_VIDEO", isDev)));
//                                urlConfigHelper.updateDomainKeengMusic(subsObject.optString("domain_kmusic", urlConfigHelper.getDomainDefault("KMUSIC", isDev)));
//                                urlConfigHelper.updateDomainKeengMusicSearch(subsObject.optString("domain_kmusic_search", urlConfigHelper.getDomainDefault("KMUSIC_SEARCH", isDev)));
//                                urlConfigHelper.updateDomainKeengMovies(subsObject.optString("domain_kmovies", urlConfigHelper.getDomainDefault("KMOVIES", isDev)));
//                                urlConfigHelper.updateDomainNetNews(subsObject.optString("domain_netnews", urlConfigHelper.getDomainDefault("NETNEWS", isDev)));
//                                urlConfigHelper.updateDomainTiin(subsObject.optString("domain_tiin", urlConfigHelper.getDomainDefault("TIIN", isDev)));
//                            }
//                            application.getPref().edit().putBoolean(FIREBASE_PROPERTY_SEND_SERVER, false).apply();
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    FireBaseHelper.getInstance(application).checkServiceAndRegister(false);
//                                }
//                            }, 10000);
//                            if (apiCallback != null)
//                                apiCallback.onSuccess("", data);
//                        } else {
//                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(String message) {
//                        super.onFailure(message);
//                        if (apiCallback != null) apiCallback.onError(message);
//                    }
//
//                    @Override
//                    public void onCompleted() {
//                        super.onCompleted();
//                        if (apiCallback != null) apiCallback.onComplete();
//                    }
//                }).execute();
    }

    @Override
    public void setRegId(String regid) {
        String uuid = Settings.Secure.getString(application.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i(TAG, "uuid: " + uuid);
        if (uuid == null) uuid = "";
        final String domainFile = getDomainFile();

        int action_type = 0;
        if (application.getReengAccountBusiness() != null
                && application.getReengAccountBusiness().isAnonymousLogin()) {
            action_type = 1;
        }
        String msisdn = "";
        if (application.getReengAccountBusiness() != null) {
            if (!TextUtils.isEmpty(application.getReengAccountBusiness().getJidNumber()))
                msisdn = application.getReengAccountBusiness().getJidNumber();

        }

        post(domainFile + Url.File.SET_REG_ID)
                .putParameter("msisdn", msisdn)
                .putParameter("uuid", uuid)
                .putParameter("regid", regid)
                .putParameter("packageId", application.getPackageName())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.COUNTRY_CODE, application.getReengAccountBusiness().getRegionCode())
                .putParameter(Parameter.LANGUAGE_CODE, application.getReengAccountBusiness().getCurrentLanguage())
                .putParameter("action_type", String.valueOf(action_type))
                /*.putParameter(Parameter.SECURITY, HttpHelper.encryptDataV2(application, sb.toString(),
                        application.getReengAccountBusiness().getToken()))
                .putParameter(Parameter.TIMESTAMP, timeStamp)*/

                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        Log.i(TAG, "data: " + data);
                        JSONObject jsonObject = new JSONObject(data);
                        int code = jsonObject.optInt("code");
                        if (code == 200) {
                            FireBaseHelper.getInstance(application).saveSendingRegIdToServerResult(application, true);
                        } else {
                            FireBaseHelper.getInstance(application).saveSendingRegIdToServerResult(application, false);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        FireBaseHelper.getInstance(application).saveSendingRegIdToServerResult(application, false);
                        super.onFailure(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                }).execute();
    }

    @Override
    public void unregisterRegid(int actionType) {
        String uuid = Settings.Secure.getString(application.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (uuid == null) uuid = "";
        final String domainFile = getDomainFile();
        /*String timeStamp = String.valueOf(System.currentTimeMillis());

        StringBuilder sb = new StringBuilder();
        sb.append(application.getReengAccountBusiness().getJidNumber()).append(uuid).append(regid)
                .append(Constants.HTTP.CLIENT_TYPE_STRING).append(Config.REVISION)
                .append(application.getReengAccountBusiness().getCurrentLanguage()).append("MM")
                .append(application.getReengAccountBusiness()
                        .getToken()).append(timeStamp);*/
        String msisdn = "";
        if (application.getReengAccountBusiness() != null) {
            if (!TextUtils.isEmpty(application.getReengAccountBusiness().getJidNumber()))
                msisdn = application.getReengAccountBusiness().getJidNumber();

        }


        post(domainFile + Url.File.UNREGISTER_REG_ID)
                .putParameter("msisdn", msisdn)
                .putParameter("uuid", uuid)
                .putParameter("packageId", application.getPackageName())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("os_version", Build.VERSION.RELEASE)
                .putParameter("provision_profile", "")
                .putParameter(Parameter.COUNTRY_CODE, application.getReengAccountBusiness().getRegionCode())
                .putParameter(Parameter.LANGUAGE_CODE, application.getReengAccountBusiness().getCurrentLanguage())
                .putParameter("action_type", String.valueOf(actionType))
                /*.putParameter(Parameter.SECURITY, HttpHelper.encryptDataV2(application, sb.toString(),
                        application.getReengAccountBusiness().getToken()))
                .putParameter(Parameter.TIMESTAMP, timeStamp)*/

                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        Log.i(TAG, "data: " + data);
                        JSONObject jsonObject = new JSONObject(data);
                        int code = jsonObject.optInt("code");
                        if (code == 200) {
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                }).execute();
    }

    @Override
    public void logOpenApp() {
        String uuid = Settings.Secure.getString(application.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (uuid == null) uuid = "";
        final String domainFile = getDomainFile();

        post(domainFile + Url.File.LOG_OPEN_APP)
                .putParameter("uuid", uuid)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)

                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        String decryptResponse = HttpHelper.decryptResponse(data, BuildConfig.KEY_XXTEA);
                        if (TextUtils.isEmpty(decryptResponse)) {
                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                            return;
                        }
                        Log.i(TAG, "logOpenApp decryptResponse: " + decryptResponse);

                        JSONObject jsonObject = new JSONObject(decryptResponse);
                        int code = jsonObject.optInt("code");
                        if (code == 200) {
                            application.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_FIRST_OPEN, true).apply();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                }).execute();
    }
}

package com.metfone.selfcare.common.api;

public interface ConstantApi {

    String CODE = "code";
    String RESULT = "result";
    String LAST_ID = "lastId";
    String LAST_ID_STR = "lastIdStr";
    String LIST_BANNER_ITEMS = "listBannerItems";

    interface Url {
        String API_IMAGE_COVER_UPLOAD_URL = "/api/cover/upload";
        String API_AVATAR_GROUP_DOWNLOAD = "/api/group/download??crop=center&w=%d&h=%d&v=%s&q=70&ac=%s&lavatar=%s&groupid=%s";
        String API_AVATAR_GROUP_UPLOAD = "/api/group/upload";
        String API_GET_LIST_VIDEO_DISCOVERY = "/api/keeng/video-keeng";
        String API_UPLOAD_LOG_DEBUG = "/api/log/upload";
        String API_FILE_DOWNLOAD_URL = "/api/media/download?msisdn=%1$s&token=%2$s&msgid=%3$s";
        String API_FILE_UPLOAD_URL = "/api/media/upload";
        String API_BOOK_UPLOAD_IMAGE = "/api/media/upload-luubut";
        String API_UPLOAD_IMAGE_SOCIAL_ONMEDIA = "/api/media/upload-onmedia";
        String API_GET_DETAIL_VIDEO = "/api/onbox/detail-video?id=%1$s&msisdn=%2$s";
        String API_DETAIL_VIDEO_NEXT = "/api/onbox/detail-video-sieu-hai?id=%1$s&msisdn=%2$s";
        String API_GET_LIST_VIDEO_HOT = "/api/onbox/list-video";
        String API_SEARCH_VIDEO_NEXT = "/api/onbox/seach-video?msisdn=%1$s&name=%2$s";
        String API_SEARCH_VIDEO = "/api/onbox/search?q=%1$s";
        String API_SHARE_SIEU_HAI = "/api/onbox/upload-link";
        String API_AVNO_REMOVE_IC = "/api/profile/removeic";
        String API_AVNO_UPLOAD_IC = "/api/profile/uploadic";
        String API_IMAGE_PROFILE_UPLOAD_URL = "/api/profile/upload-v1";
        String API_LOG_VIEW_SIEUHAI = "http://apiphim.onbox.vn:8086/serviceapi/sieuhai/api/writeViewHotInMocha";
        String API_GET_SONG_UPLOAD = "http://vip.service.keeng.vn/KeengBackendBiz/ws/internal/mocha/getSongUpload";

        interface File {
            //todo domain_file_v1 (thay thế domain_file). Domain gọi các API có path bắt đầu với “ReengBackendBiz”.

            String FEED_BACK = "/ReengBackendBiz/user/feedback/send";// feedback
            String LIST_AVATAR = "/ReengBackendBiz/user/getListAvatar";// lấy danh sách avatar
            String BACKUP_MESSAGE_UPLOAD = "/ReengBackendBiz/backupmessage/upload";
            String BACKUP_FILE_LIST = "/ReengBackendBiz/backupmessage/list";
            String BACKUP_FILE_DELETE = "/ReengBackendBiz/backupmessage/delete";
            String REMOVE_AVATAR = "/ReengBackendBiz/upload/avatar/remove";
            String REMOVE_IMAGE_ALBUM = "/ReengBackendBiz/user/removeImage";
            String GET_INFO_ANONYMOUS = "/ReengBackendBiz/genotp/getInfoAnonymous";
            String SET_REG_ID = "/ReengBackendBiz/anonymous/registerRegId";
            String UNREGISTER_REG_ID = "/ReengBackendBiz/anonymous/unRegisterRegId";
            String LOG_OPEN_APP = "/ReengBackendBiz/openapp/logs";
            String GET_LIST_DATA_PACKAGES = "/ReengBackendBiz/data/getInfo"; // lấy danh sách gói cước data
            String ADS_GET_MAIN_TAB_POPUP = "/ReengBackendBiz/data/maintab/popup"; // lấy danh sách quảng cáo ở các tab chính (video, news, movies)
            String LOG_BANNER = "/ReengBackendBiz/bannerClick/savelog";// log hành vi người dùng khi click vào banner
            String LOG_ACTION = "/ReengBackendBiz/bannerClick/saveloghomemocha";// log action

            //todo Viettel IQ
            String API_VT_IQ_SUB = "/ReengBackendBiz/game/vtiq/sub/v1";
            String API_VT_IQ_GET_STAT = "/ReengBackendBiz/game/vtiq/getStat/v1";
            String API_VT_IQ_GET_REPORT = "/ReengBackendBiz/game/vtiq/getReport";
            String API_VT_IQ_ANSWER = "/ReengBackendBiz/game/vtiq/answer/v1";
            String API_VT_IQ_BREAK_HEART = "/ReengBackendBiz/game/vtiq/breakHeart";
            String API_VT_IQ_GET_SUB = "/ReengBackendBiz/game/vtiq/getSub";

            //todo game countdown
            String API_GAME_CD_SUB = "/CountDownService/countdown/sub";
            String API_GAME_CD_CLAIM_GIFT = "/CountDownService/countdown/open";
            String API_GAME_CD_GET_RULE = "/CountDownService/countdown/getRule";
            String API_GAME_CD_GET_LIST_WIN = "/CountDownService/countdown/report";

            //todo Viettel Security
            String API_SECURITY_GET_CONFIG = "/ReengBackendBiz/mobilesecurity/getconfig";
            String API_SECURITY_GET_LOG = "/ReengBackendBiz/mobilesecurity/getlog";
            String API_SECURITY_FIREWALL = "/ReengBackendBiz/mobilesecurity/fw";
            String API_SECURITY_FIREWALL_WHITELIST = "/ReengBackendBiz/mobilesecurity/fwwhitelist";
            String API_SECURITY_SPAM_WHITELIST = "/ReengBackendBiz/mobilesecurity/spwhitelist";
            String API_SECURITY_SPAM_BLACKLIST = "/ReengBackendBiz/mobilesecurity/spblacklist";
            String API_SECURITY_GET_NEWS = "/ReengBackendBiz/mobilesecurity/getNews";

            //todo AVNO
            String API_AVNO_GET_SAVING_STATISTIC = "/ReengBackendBiz/callout/getSavingStatistic";
            String API_AVNO_GET_SAVING_DETAIL = "/ReengBackendBiz/callout/getSavingDetail";
            String API_AVNO_GET_SAVING_BUDGET_NUM = "/ReengBackendBiz/callout/getSavingBudgetNum";
            String API_AVNO_GET_HISTORY = "/ReengBackendBiz/callout/getHistory";
            String API_AVNO_CREATE_SESSION = "/ReengBackendBiz/callout/createSession";
            String API_AVNO_TRANSFER_MONEY = "/ReengBackendBiz/callout/tranferMoney";
            String API_AVNO_CHANGE_PASS = "/ReengBackendBiz/callout/changePass";

            String API_AVNO_ABORT = "/ReengBackendBiz/avno/abort";
            String API_AVNO_CANCEL_PACKAGE_V2 = "/ReengBackendBiz/avno/cancelPkg/v2";
            String API_AVNO_BROADCAST_CHANGE_NUMBER_AVNO = "/ReengBackendBiz/avno/notify";
            String API_AVNO_PAYMENT_CONVERT_SPOINT = "/ReengBackendBiz/avno/payment/convertSpoint";
            String API_AVNO_PAYMENT_SCRATCH_CARD = "/ReengBackendBiz/avno/payment/scratchCard";
            String API_AVNO_REGISTER = "/ReengBackendBiz/avno/register";
            String API_AVNO_REGISTER_PACKAGE_V2 = "/ReengBackendBiz/avno/registerPkg/v2";
            String API_AVNO_SEARCH = "/ReengBackendBiz/avno/search";
            String API_AVNO_REGISTER_FREE = "/ReengBackendBiz/avno/setFree";
            String API_AVNO_SUGGEST = "/ReengBackendBiz/avno/suggestion";
            String API_AVNO_CREATE_OTP = "/ReengBackendBiz/avno/createOtp";
            String API_AVNO_BUY_PACKAGE_BY_OTP = "/ReengBackendBiz/avno/buyPackageByOTPCode";
            String API_AVNO_BUY_VIRTUAL_NUMBER_BY_OTP = "/ReengBackendBiz/avno/buyVirtualNumberByOTPCode";

            String API_UPLOAD_CALL_REPORT = "/ReengBackendBiz/call_logger/webrtc_dump";
            String API_SETTING_ON_OFF_TAB = "/ReengBackendBiz/v2/user/setting/onOffTab";
            String API_SEND_INVITE_SMS = "/ReengBackendBiz/invitation/sendInviteSms/v2";
            String API_REPORT_DH_VTT = "/ReengBackendBiz/mucroom/report_vtt";

            //
            String API_ACCUMULATE_CONVERT = "/ReengBackendBiz/accumulate/convertPoint/v2";
            String API_GET_LIST_ACCUMULATE_V4 = "/ReengBackendBiz/accumulate/getListAccumulate/v4";
            String API_ACCUMULATE_GET_POINT = "/ReengBackendBiz/accumulate/getPoint/v1";
            String API_LOG_ACCUMULATE = "/ReengBackendBiz/accumulate/log";
            String API_ANONYMOUS_DETAIL = "/ReengBackendBiz/anonymous/getInfo/v2?msisdn=%1$s&anonymous_msisdn=%2$s&app_id=%3$s&timestamp=%4$s&security=%5$s";

            String API_LIXI_GENOTP = "/ReengBackendBiz/bankplus/genotp/v2";
            String API_LIXI_TRANSFER_MONEY = "/ReengBackendBiz/bankplus/payment/v2";
            String API_LIXI_CLICK = "/ReengBackendBiz/bankplusads/click";
            String API_GET_BLOCKLIST_V5 = "/ReengBackendBiz/block/getBlockList/v5";
            String API_SET_BLOCKLIST_V5 = "/ReengBackendBiz/block/setBlockChat/v5";
            String API_LOGGER_CALL = "/ReengBackendBiz/call_logger/freecall_api";
            String API_LOGGER_CALL_QUALITY = "/ReengBackendBiz/call_logger/report_bwe";
            String API_CALLOUT_CANCEL_PACKAGE = "/ReengBackendBiz/callout/cancelPkg";
            String API_CALLOUT_GET_INFO = "/ReengBackendBiz/callout/getInfo";
            String API_CALL_SUBSCRIPTION_GET_REMAIN = "/ReengBackendBiz/callout/getRemainTime";
            String API_CALLOUT_REGISTER_PACKAGE = "/ReengBackendBiz/callout/registerPkg";
            String API_CALLOUT_SET_CALL_LIMITED = "/ReengBackendBiz/callout/setCalledLimited";
            String API_CALLOUT_REGISTER_FREE = "/ReengBackendBiz/callout/setFree";
            String API_CALL_SUBSCRIPTION_SET_FREE_FROM_CALLOUTGUIDE = "/ReengBackendBiz/callout/trying";
            String API_DEEPLINK_CAMPAIGN = "/ReengBackendBiz/campaign/submit/v2";
            String API_ACCEPT_STRANGER_CONFIDE = "/ReengBackendBiz/confideStrangers/accept";
            String API_CANCEL_STRANGER_CONFIDE = "/ReengBackendBiz/confideStrangers/cancel";
            String API_GET_STRANGER_CONFIDE = "/ReengBackendBiz/confideStrangers/getList/v2?msisdn=%1$s&countryCode=%2$s&groupId=%3$s&filterGender=%4$s&state=%5$s&lastRoomId=%6$s&filterLocation=%7$s&filterAgeMin=%8$s&filterAgeMax=%9$s&vip=%10$s&timestamp=%11$s&security=%12$s&currOperator=%13$s";
            String API_POST_STRANGER_CONFIDE = "/ReengBackendBiz/confideStrangers/post";
            String API_GET_CONTENT_CONFIG = "/ReengBackendBiz/config/getConfig/v2.2?msisdn=%1$s&clientType=%2$d&language=%3$s&country=%4$s&timestamp=%5$s&security=%6$s";
            String API_GET_LIST_OFFICER_ACCOUNT = "/ReengBackendBiz/config/getOfficalAcount/v2?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_GET_POPUP_INTRO = "/ReengBackendBiz/config/getPopup";
            String API_SEARCH_OFFICER_ACCOUNT = "/ReengBackendBiz/config/searchOfficalAcc/v1?msisdn=%1$s&timestamp=%2$s&security=%3$s&content=%4$s";
            String API_ADD_CONTACT = "/ReengBackendBiz/contact/addContact/v3";
            String API_GET_CONTACT = "/ReengBackendBiz/contact/getContact/v4";
            String API_REMOVE_CONTACT = "/ReengBackendBiz/contact/removeContact/v3";
            String API_SET_CONTACT = "/ReengBackendBiz/contact/setContact/v3";
            String API_CRBT_ACCEPT = "/ReengBackendBiz/crbt/accept/v1";
            String API_CRBT_GIFT = "/ReengBackendBiz/crbt/dedicate/v1";
            String API_GET_GROUP_DOCUMENT = "/ReengBackendBiz/document/getList?msisdn=%1$s&group_id=%2$s&timestamp=%3$s&security=%4$s";
            String API_DOWNLOAD_STICKER_COLLECTION = "/ReengBackendBiz/downloadSticker/v21?msisdn=%1$s&stickerid=%2$s&timestamp=%3$s&security=%4$s";
            String API_FAKE_MO = "/ReengBackendBiz/fakemo/send/v3";
            String API_FEEDBACK_SEND_KEY_V2 = "/ReengBackendBiz/feedback/getAction/v2?msisdn=%1$s&service_id=%2$s&actionkey=%3$s&timestamp=%4$s&security=%5$s";
            String API_FEEDBACK_GET_DEFAULT_V2 = "/ReengBackendBiz/feedback/getServiceActionList/v2?msisdn=%1$s&service_id=%2$s&timestamp=%3$s&security=%4$s";
            String API_GET_FORTUNE = "/ReengBackendBiz/fortune/double/v1";
            String API_GET_LIST_GAME = "/ReengBackendBiz/game/listgame/v1";
            String API_LUCKY_WHEEL_GET_BUDGET = "/ReengBackendBiz/game/luckywheel/getMochalott?msisdn=%1$s&clientType=%2$s&revision=%3$s&timestamp=%4$s&security=%5$s";
            String API_LUCKY_WHEEL_SPIN = "/ReengBackendBiz/game/luckywheel/spin/v5";
            String API_LUCKY_WHEEL_SOS_ACCEPT = "/ReengBackendBiz/game/luckywheel/turn/acceptHelp/v4";
            String API_LUCKY_WHEEL_SOS_SEND = "/ReengBackendBiz/game/luckywheel/turn/askingHelp/v4";
            String API_SCAN_QR_CODE = "/ReengBackendBiz/game/qr/scan/v2";
            String API_SPONSOR_READ_SUCCESS = "/ReengBackendBiz/game/read/news/v1/success";
            String API_GAME_SHAKE = "/ReengBackendBiz/game/shaking/shake/v1";
            String API_GET_GIFT_WOMEN = "/ReengBackendBiz/game/womenday/v1";
            String API_GEN_OTP_FREE = "/ReengBackendBiz/genotp/free/v31";
            String API_GET_COUNTRY = "/ReengBackendBiz/genotp/getCountry";
            String API_GEN_OTP_INTERNATIONAL = "/ReengBackendBiz/genotp/v31";
            String API_INAPP_ACTION_SMS_SENT = "/ReengBackendBiz/inapp/actionSms/v1";
            String API_INAPP_CHECK_FREE_SMS = "/ReengBackendBiz/inapp/checkSmsFree/v1?msisdn=%1$s&cmsisdn=%2$s&cname=%3$s&timestamp=%4$s&security=%5$s";
            String API_INVITE_NOP_HOSO = "/ReengBackendBiz/invite/sms/diemthi";
            String API_REQUEST_INVITE_ROOM = "/ReengBackendBiz/inviteroom/inviteRoom/v1";
            String API_GET_FRIEND_PROFILE_V2 = "/ReengBackendBiz/keeng/getFriendProfile/v2?msisdn=%1$s&friend=%2$s&timestamp=%3$s&security=%4$s";
            String API_LOG_LISTEN_TOGETHER = "/ReengBackendBiz/keeng/getLogTogether";
            String API_GET_PROFILE_V2 = "/ReengBackendBiz/keeng/getProfile/v2?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_ACCEPT_STRANGER_KEENG = "/ReengBackendBiz/keeng/v1/accept";
            String API_GET_FRIEND_KEENG_PROFILE = "/ReengBackendBiz/keeng/v1/getFriendProfile?msisdn=%1$s&token=%2$s&friend=%3$s";
            String API_WATCH_VIDEO_GET_LIST_VIDEO = "/ReengBackendBiz/keengvideo/getListVideo?msisdn=%1$s&page=%2$s&security=%3$s&timestamp=%4$s";
            String API_WATCH_VIDEO_GET_DETAIL = "/ReengBackendBiz/keengvideo/getVideoDetail?msisdn=%1$s&song_id=%2$s&security=%3$s&timestamp=%4$s";
            String API_GG_TRANSLATE = "/ReengBackendBiz/language/v1/translate";
            String API_LIXI_GET_LIST_IMAGE = "/ReengBackendBiz/lixi//listimage";
            String API_LIXI_OPEN = "/ReengBackendBiz/lixi//open";
            String API_LIXI_LOG_BPLUS_FAIL = "/ReengBackendBiz/lixi/log";
            String API_POST_LOCATION = "/ReengBackendBiz/location/postLocation";
            String API_BOOK_ASSIGN_PAGE = "/ReengBackendBiz/memory/assigned";
            String API_BOOK_GET_PAGES_ASSIGNED = "/ReengBackendBiz/memory/getListAssignedPage?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_BOOK_GET_BACKGROUND = "/ReengBackendBiz/memory/getListBackground?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_BOOK_GET_MUSIC = "/ReengBackendBiz/memory/getListSong?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_BOOK_GET_STICKERS = "/ReengBackendBiz/memory/getListSticker?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_BOOK_GET_TEMPLATES = "/ReengBackendBiz/memory/getListTemplate?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_BOOK_GET_LIST_BOOK = "/ReengBackendBiz/memory/getLstMemory?msisdn=%1$s&owner=%2$s&timestamp=%3$s&security=%4$s";
            String API_BOOK_VOTE_GET_LIST = "/ReengBackendBiz/memory/getLstVote?msisdn=%1$s&type=%2$s&page=%3$s&timestamp=%4$s&security=%5$s";
            String API_BOOK_GET_BOOK_DETAIL = "/ReengBackendBiz/memory/getMemory?msisdn=%1$s&id=%2$s&timestamp=%3$s&security=%4$s";
            String API_BOOK_GET_TEMPLATE_DETAIL = "/ReengBackendBiz/memory/getTemplateDetail?msisdn=%1$s&id=%2$s&timestamp=%3$s&security=%4$s";
            String API_BOOK_VOTE_GET_DETAIL = "/ReengBackendBiz/memory/getVoteDetail?msisdn=%1$s&id_memory=%2$s&timestamp=%3$s&security=%4$s";
            String API_BOOK_PROCESS_BOOK = "/ReengBackendBiz/memory/proccesMemory/v1";
            String API_BOOK_SAVE = "/ReengBackendBiz/memory/save";
            String API_BOOK_VOTE = "/ReengBackendBiz/memory/voteMemory";
            String API_GET_APP_LIST = "/ReengBackendBiz/mobileapps/get/v1?msisdn=%1$s&os=%2$s&timestamp=%3$s&security=%4$s";
            String API_SEND_LOG_APP = "/ReengBackendBiz/mobileapps/log/v1";
            String API_AUTO_DETECT_FREE_URL_V23 = "/ReengBackendBiz/msisdn/free/v23";
            String API_AUTO_DETECT_URL_V23 = "/ReengBackendBiz/msisdn/v23";
            String API_GET_LIST_GROUP = "/ReengBackendBiz/mucroom/getlist/v2";
            String API_ACCEPT_STRANGER_MUSIC = "/ReengBackendBiz/musicRoom/accept/v2";
            String API_CANCEL_STRANGER_MUSIC = "/ReengBackendBiz/musicRoom/cancel/v2";
            String API_DELETE_STRANGER_HISTORY = "/ReengBackendBiz/musicRoom/delete";
            String API_STRANGER_AROUND = "/ReengBackendBiz/musicRoom/getLisStrangerAround/v2";
            String API_GET_STRANGER_MUSIC = "/ReengBackendBiz/musicRoom/getList/v4?msisdn=%1$s&countryCode=%2$s&groupId=%3$s&filterGender=%4$s&state=%5$s&lastRoomId=%6$s&filterLocation=%7$s&filterAgeMin=%8$s&filterAgeMax=%9$s&vip=%10$s&timestamp=%11$s&security=%12$s&currOperator=%13$s";
            String API_GET_STRANGER_STAR_MUSIC = "/ReengBackendBiz/musicRoom/getListStarV2/v2?msisdn=%1$s&countryCode=%2$s&groupId=%3$s&lastRoomId=%4$s&timestamp=%5$s&security=%6$s";
            String API_POST_STRANGER_MUSIC = "/ReengBackendBiz/musicRoom/post/v2";
            String API_POLL_CREATE = "/ReengBackendBiz/poll/v1/create";
            String API_POLL_GET_DETAIL = "/ReengBackendBiz/poll/v1/find/byId?msisdn=%1$s&pollId=%2$s&timestamp=%3$s&security=%4$s";
            String API_POLL_GET_ITEM_DETAIL = "/ReengBackendBiz/poll/v1/find/voters/byPoll?msisdn=%1$s&pollId=%2$s&optionIds=%3$s&timestamp=%4$s&security=%5$s";
            String API_POLL_VOTE = "/ReengBackendBiz/poll/v1/vote";
            String API_RATING_CALL = "/ReengBackendBiz/rating/call";
            String API_REPORT_ROOM = "/ReengBackendBiz/report/spam/star/v2";
            String API_RESTORE_MESSAGE_V2 = "/ReengBackendBiz/restore/message/v2";
            String API_SEARCH_USER_MOCHA = "/ReengBackendBiz/search/all/v1";
            String API_REPORT_ERROR = "/ReengBackendBiz/sendlog";
            String API_UPLOAD_LOG = "/ReengBackendBiz/sendlogs/save";
            String API_SEND_LOG_KPI = "/ReengBackendBiz/sendlogs/save/kqi";
            String API_LOG_ERROR = "/ReengBackendBiz/sendlogs/save/v2";
            String API_SETTING_RECEIVE_SMSOUT = "/ReengBackendBiz/sms/receive?msisdn=%1$s&receiveSms=%2$s&timestamp=%3$s&security=%4$s";
            String API_MOCHA_2_SMS = "/ReengBackendBiz/sms/v4/send";
            String API_ROOM_CHAT_FOLLOW = "/ReengBackendBiz/star/follow/v1";
            String API_GET_STAR_ROOM_TAB = "/ReengBackendBiz/star/starRoomTab?msisdn=%1$s&countryCode=%2$s&languageCode=%3$s&timestamp=%4$s&security=%5$s";
            String API_GET_ALL_STICKER = "/ReengBackendBiz/sticker/getAllCollection/v2.2?msisdn=%1$s&reqTime=%2$s&dataEncrypt=%3$s&countryCode=%4$s";
            String API_GET_PACK_DATA_INFO = "/ReengBackendBiz/subpackage/getPackageInfo/v2?timestamp=%1$s&security=%2$s&msisdn=%3$s";
            String API_AVATAR_UPLOAD = "/ReengBackendBiz/upload/avatar/v3";
            String API_GET_PROFILE_URL = "/ReengBackendBiz/user/getProfile/v1?msisdn=%1$s&security=%2$s&timestamp=%3$s&contact=%4$s";
            String API_GET_USER_INFO = "/ReengBackendBiz/user/getUserInfo/v3?msisdn=%1$s&timestamp=%2$s&security=%3$s";
            String API_REMOVE_IMAGE_PROFILE = "/ReengBackendBiz/user/removeImage?msisdn=%1$s&dataEncrypt=%2$s&reqTime=%3$s&imageId=%4$s";
            String API_SET_REMINDER = "/ReengBackendBiz/user/setBirthdayReminder";
            String API_SET_DEVICE_ID = "/ReengBackendBiz/user/setDeviceInfo";
            String API_SET_PERMISSION = "/ReengBackendBiz/user/setPermission?msisdn=%1$s&dataEncrypt=%2$s&reqTime=%3$s&permission=%4$s";
            String API_CHANGE_STATUS = "/ReengBackendBiz/user/setState/v1";
            String API_AUTO_SMS_OUT = "/ReengBackendBiz/user/setting/autoConvertSmsOut?msisdn=%1$s&type=%2$s&timestamp=%3$s&security=%4$s";
            String API_SET_HIDE_STRANGER_HISTORY = "/ReengBackendBiz/user/setting/hideStrangleHistory?msisdn=%1$s&hide=%2$s&timestamp=%3$s&security=%4$s";
            String API_SET_USER_INFO = "/ReengBackendBiz/user/setUserInfo/v3";
            String API_GET_APP_DRIVER = "/ReengBackendBiz/v1/appdriver/getListApp?msisdn=%1$s&source=%2$s&timestamp=%3$s&security=%4$s";
            String API_APP_DRIVER_REDIRECT = "/ReengBackendBiz/v1/appdriver/redirectUrl?msisdn=%1$s&source=%2$s&url=%3$s&timestamp=%4$s&security=%5$s";
            String API_LOG_SHARE_FB = "/ReengBackendBiz/v1/sharefblog/savelog";
            String API_GET_OFFICER_LIST = "/ReengBackendBiz/v2/config/getOfficalAcount?msisdn=%1$s&token=%2$s";
            String API_INVITE_FRIENDS = "/ReengBackendBiz/v2/sms/sendInviteSms/v1";

        }

        interface OnMedia {
            //todo domain_on_media_v1 (full domain có chứa protocol): Sử dụng cho các API “/onMediaBackendBiz/onmedia/…” (ngoại trừ API video mocha).

            String API_ADS_GET_MAIN_TAB = "/onMediaBackendBiz/ads/maintab"; // lấy danh sách quảng cáo ở các tab chính (video, news, movies)
            String API_GET_TAB_HOME = "/onMediaBackendBiz/onmedia/getTabHome"; // lấy dữ liệu màn hình tab home
            String API_GET_TIMELINE_BY_FEED_ID = "/onMediaBackendBiz/onmedia/getTimelineByFeedid"; // lấy dữ liệu màn hình tab home

            String API_ONMEDIA_FOLLOW_OFFICIAL = "/onMediaBackendBiz/offical/follow";
            String API_ONMEDIA_GET_ACTIVITIES_OFFICIAL_V2 = "/onMediaBackendBiz/offical/getActivities/v2";
            String API_ONMEDIA_GET_PROFILE_OFFICIAL = "/onMediaBackendBiz/offical/getProfile";
            String API_ONMEDIA_ACTION_APP_V6 = "/onMediaBackendBiz/onmedia/actionApp/v6";
            String API_ONMEDIA_UPLOAD_ALBUM = "/onMediaBackendBiz/onmedia/album";
            String API_ONMEDIA_LOG_CLICK_LINK = "/onMediaBackendBiz/onmedia/clickLink";
            String API_ONMEDIA_GET_CONTENT_DISCOVERY = "/onMediaBackendBiz/onmedia/discovery/list/v1";
            String API_ONMEDIA_GET_COMMENT_V2 = "/onMediaBackendBiz/onmedia/getComments/v2";
            String API_ONMEDIA_GET_DETAIL_NOTIFY_V1 = "/onMediaBackendBiz/onmedia/getDetailNotify/v1";
            String API_ONMEDIA_GET_DETAIL_URL = "/onMediaBackendBiz/onmedia/getDetailUrl";
            String API_ONMEDIA_GET_FEED_NOTIFY = "/onMediaBackendBiz/onmedia/getFeedNotify";
            String API_ONMEDIA_GET_HOME_TIMELINE = "/onMediaBackendBiz/onmedia/getHomeTimelinePaging/v3";
            String API_ONMEDIA_GET_HOME_TIMELINE_V6 = "/onMediaBackendBiz/onmedia/getHomeTimelinePaging/v6";
            String API_ONMEDIA_GET_IMAGE_DETAIL_V3 = "/onMediaBackendBiz/onmedia/getImageDetail/v3";
            String API_GET_LIKE_TITLE_V2 = "/onMediaBackendBiz/onmedia/getLikes/statistic/v2";
            String API_ONMEDIA_GET_LIKE_V2 = "/onMediaBackendBiz/onmedia/getLikes/v2";
            String API_ONMEDIA_GET_METADATA = "/onMediaBackendBiz/onmedia/getMetadata";
            String API_GET_METADATA_WITH_ACTION = "/onMediaBackendBiz/onmedia/getMetadata/t2";
            String API_ONMEDIA_GET_NOTIFY_V1 = "/onMediaBackendBiz/onmedia/getNotifies/v1";
            String API_ONMEDIA_GET_NUMBER_NOTIFY = "/onMediaBackendBiz/onmedia/getNumberNotifies";
            String API_ONMEDIA_GET_SHARE_V2 = "/onMediaBackendBiz/onmedia/getShares/v2";
            String API_ONMEDIA_GET_USER_TIMELINE_V3 = "/onMediaBackendBiz/onmedia/getUserTimeline/v3";
            String API_ONMEDIA_GET_LIKE_AND_COMMENT_IMAGE = "/onMediaBackendBiz/onmedia/item/getLikesAndComment/v1";
            String API_REDUCE_TOTAL_NOTIFY = "/onMediaBackendBiz/onmedia/reduceTotalNotify";
            String API_SOCIAL_CANCEL_FRIEND = "/onMediaBackendBiz/onmedia/relationship/cancelFriend";
            String API_SOCIAL_GET_FRIENDS = "/onMediaBackendBiz/onmedia/relationship/list/friends?msisdn=%1$s&limit=%2$s&page=%3$s&timestamp=%4$s&security=%5$s";
            String API_SOCIAL_ACCEPT_REQUEST = "/onMediaBackendBiz/onmedia/relationship/receiver/acceptRequest";
            String API_SOCIAL_CANCEL_FRIEND_REQUEST = "/onMediaBackendBiz/onmedia/relationship/receiver/cancelRequest";
            String API_SOCIAL_GET_FRIEND_REQUESTS = "/onMediaBackendBiz/onmedia/relationship/receiver/pendingRequests?msisdn=%1$s&limit=%2$s&page=%3$s&timestamp=%4$s&security=%5$s";
            String API_SOCIAL_CANCEL_MY_REQUEST = "/onMediaBackendBiz/onmedia/relationship/sender/cancelRequest";
            String API_SOCIAL_SEND_REQUEST = "/onMediaBackendBiz/onmedia/relationship/sender/requestFriend";
            String API_SOCIAL_GET_DETAIL = "/onMediaBackendBiz/onmedia/relationship/status?msisdn=%1$s&otherMsisdn=%2$s&timestamp=%3$s&security=%4$s";
            String API_GET_SUGGEST_CONTACT = "/onMediaBackendBiz/onmedia/relationship/suggest/ct";
            String API_ONMEDIA_GET_LIST_SUGGEST_FRIEND_V2 = "/onMediaBackendBiz/onmedia/relationship/suggest/om/v2";
            String API_ONMEDIA_REPORT_VIOLATION = "/onMediaBackendBiz/onmedia/reportViolations";
            String API_ONMEDIA_RESET_NOTIFY_V1 = "/onMediaBackendBiz/onmedia/resetNotify/v1";
            String API_ONMEDIA_UNFOLLOW = "/onMediaBackendBiz/onmedia/unfollow";
            String API_ONMEDIA_UNFOLLOW_OFFICIAL_TIMELINE = "/onMediaBackendBiz/onmedia/unfollowfeed";
            String API_TAB_WAP_LOG_CLICK = "/onMediaBackendBiz/tablog/tabclick";
        }

        interface LuckyWheel {
            String API_GET_VALUE_BUGGET = "/ReengBackendBiz/game/luckywheel/getMochalott";
            String API_REQUEST_SOS_FRIEND = "/ReengBackendBiz/game/luckywheel/turn/askingHelp/v4";
            String API_ACCEPT_HELP = "/ReengBackendBiz/game/luckywheel/turn/acceptHelp/v4";
            String API_REQUEST_SPIN = "/ReengBackendBiz/game/luckywheel/spin/v6";
            String API_LOG_DONE_MISSION = "/ReengBackendBiz/game/luckywheel/reward-turn/v6";
        }

    }

    interface Parameter {
        String SERVICE_TYPE = "serviceType";
        String REVISION = "revision";
        String DEEP_LINK = "deepLink";
        String CLIENT_TYPE = "clientType";
        String GENDER = "gender";
        String TIMESTAMP = "timestamp";
        String DATE = "date";
        String DATA_ENCRYPT = "dataEncrypt";
        String SECURITY = "security";
        String CONTACT = "contact";
        String LIMIT = "limit";
        String OFFSET = "offset";
        String FILM_GROUP_ID = "filmGroupID";
        String POST_ON_PROFILE = "postOnProfile";
        String CATEGORY_ID = "categoryId";
        String CATEGORYID = "categoryid";
        String LINK_YOUTUBE = "linkYoutube";
        String REPORT_ID = "reportId";
        String REASON = "reason";
        String CONTENT = "content";
        String IMAGES = "images";
        String Q = "q";
        String DEVICE = "device";
        String VERSION = "version";
        String COUNTRY_CODE = "countryCode";
        String LANGUAGE_CODE = "languageCode";

        interface User {
            String BIRTHDAY = "birthday";
            String MSISDN = "msisdn";
            String GENDER = "gender";
            String NAME = "name";
            String VIP = "vip";
        }

        interface Video {
            String ID = "videoId";
            String NAME = "videoName";
            String LINK = "videoLink";
            String PATH = "videoPath";
            String LIST_RESOLUTION = "list_resolution";
            String LIST_ADSENSE = "adsense";
        }

        interface Channel {
            String CHANNELID = "channelid";
            String CHANNEL_ID = "channelId";
        }

        interface Http {
            String DOMAIN = "domain";
            String URL = "url";
            String NETWORK_TYPE = "networkType";
        }

    }
}

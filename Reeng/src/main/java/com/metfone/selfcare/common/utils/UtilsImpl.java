package com.metfone.selfcare.common.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.screen.ScreenUtils;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.EventOnMediaHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.module.search.activity.RewardDetailActivity;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.tabvideo.activity.createChannel.CreateChannelActivity;
import com.metfone.selfcare.ui.tabvideo.activity.uploadVideo.UploadVideoActivity;
import com.metfone.selfcare.ui.tabvideo.channelDetail.ChannelDetailActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.util.DialogUtils;
import com.vtm.adslib.AdsListener;

import java.util.ArrayList;

public class UtilsImpl implements Utils {
    private ApplicationController application;
    private int widthScreen;
    private int heightScreen;
    private int adsVideoIndex = 0;

    public UtilsImpl(ApplicationController application,
                     ScreenUtils screenUtils) {
        this.application = application;

        widthScreen = screenUtils.getScreenWidth();
        heightScreen = screenUtils.getScreenHeight();
    }

    @Override
    public void openVideoDetail(final BaseSlidingFragmentActivity activity, final Video video) {
        new EventOnMediaHelper(activity).handleClickMediaItem(FeedModelOnMedia.convertVideoToFeedModelOnMedia(video),
                null, (view, entry, menuId) -> {
                    if (menuId == Constants.MENU.POPUP_EXIT) {
                        new EventOnMediaHelper(activity).handleClickPopupExitListenTogether(
                                (FeedModelOnMedia) entry,
                                null);
                        VideoPlayerActivity.start(application, video, "", true);
//                                VideoDetailLoadingActivity.start(activity, video);
                    }
                });

        //Show ads
        showAds();
    }

    @Override
    public void openRewardDetail(BaseSlidingFragmentActivity activity, SearchRewardResponse.Result.ItemReward reward) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_ITEM_REWARD, reward);
        Intent intent = new Intent(activity, RewardDetailActivity.class);
        intent.putExtra(Constants.KEY_BUNDLE_REWARD, bundle);
        activity.startActivity(intent);
    }

    @Override
    public void openMovieDetail(final BaseSlidingFragmentActivity activity, final Movie movie) {
        FeedModelOnMedia feedModelOnMedia = FeedModelOnMedia.convertVideoToFeedModelOnMedia(Movie.movie2Video(movie));
        new EventOnMediaHelper(activity).handleClickMediaItem(feedModelOnMedia, null, (view, entry, menuId) -> {
            if (menuId == Constants.MENU.POPUP_EXIT) {
                new EventOnMediaHelper(activity).handleClickPopupExitListenTogether((FeedModelOnMedia) entry,
                        null);
                VideoPlayerActivity.start(activity, movie, "", true);
            }
        });

        //Show ads
        showAds();
    }

    @Override
    public void openVideoDetail(final BaseSlidingFragmentActivity activity, final FeedModelOnMedia feedModelOnMedia) {
        new EventOnMediaHelper(activity).handleClickMediaItem(feedModelOnMedia, null, (view, entry, menuId) -> {
            if (menuId == Constants.MENU.POPUP_EXIT) {
                new EventOnMediaHelper(activity).handleClickPopupExitListenTogether((FeedModelOnMedia) entry,
                        null);
                VideoPlayerActivity.start(activity, Video.convertFeedContentToVideo(feedModelOnMedia.getFeedContent()), "", true);
//                        VideoDetailLoadingActivity.start(activity, Video.convertFeedContentToVideo(feedModelOnMedia.getFeedContent()));
            }
        });
    }

    @Override
    public void openCommentVideo(BaseSlidingFragmentActivity activity, Video video) {
        NavigateActivityHelper.navigateToOnMediaDetail(activity,
                video.getLink(),
                Constants.ONMEDIA.COMMENT,
                true,
                FeedModelOnMedia.convertVideoToFeedModelOnMedia(video), Constants.ONMEDIA.FEED_TAB_VIDEO);
    }

    @Override
    public void openShareMenu(final BaseSlidingFragmentActivity activity, final Video video) {
        ShareUtils.openShareMenu(activity, video);
    }

    @Override
    public void openChannelInfo(BaseSlidingFragmentActivity activity, Channel channel) {
        ChannelDetailActivity.start(activity, channel);
    }

    @Override
    public void openDetailCategoryFilm(BaseSlidingFragmentActivity activity, MovieKind movieKind) {
        SubtabInfo tabInfo = new SubtabInfo(movieKind);
        NavigateActivityHelper.navigateToTabMoviesCategoryDetail(activity, tabInfo);
    }

    @Override
    public int getHeightScreen() {
        return heightScreen;
    }

    @Override
    public int getWidthScreen() {
        return widthScreen;
    }

    @Override
    public void openCreateChannel(BaseSlidingFragmentActivity activity) {
        CreateChannelActivity.start(activity);
    }

    @Override
    public void openConfigRemove(final BaseSlidingFragmentActivity context, final OnConfigRemoveVideoListener onConfigRemoveVideoListener) {
        DialogConfirm dialogConfirm = new DialogConfirm(context, true);
        dialogConfirm.setLabel(context.getString(R.string.notification));
        dialogConfirm.setMessage(context.getString(R.string.removeVideoMessage));
        dialogConfirm.setPositiveLabel(context.getString(R.string.unRemoveVideoUpperCase));
        dialogConfirm.setNegativeLabel(context.getString(R.string.noUpperCase));
        dialogConfirm.setNegativeListener(result -> {

        });
        dialogConfirm.setPositiveListener(result -> {
            if (onConfigRemoveVideoListener != null)
                onConfigRemoveVideoListener.onConfigRemoveVideo();
        });
        dialogConfirm.setDismissListener(() -> {

        });
        dialogConfirm.show();
    }

    @Override
    public void showOptionLibraryVideoItem(BaseSlidingFragmentActivity activity, Video item, String type, OnClickMoreItemListener listener) {
        DialogUtils.showOptionLibraryVideoItem(activity, item, type, listener);
    }

    @Override
    public void saveChannelInfo(Channel channel) {
        SharedPrefs.getInstance().put(Constants.TabVideo.CACHE_MY_CHANNEL_INFO, channel);
    }

    @Override
    public Channel getChannelInfo() {
        return SharedPrefs.getInstance().get(Constants.TabVideo.CACHE_MY_CHANNEL_INFO, Channel.class);
    }

    @Override
    public void saveCategories(ArrayList<Category> list) {
        SharedPreferences sharedpreferences = application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Category", new Gson().toJson(list));
        editor.apply();
    }

    @Override
    public ArrayList<Category> getCategories() {
        SharedPreferences sharedpreferences = application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
        String data = sharedpreferences.getString("Category", "");
        ArrayList<Category> list = new Gson().fromJson(data, new TypeToken<ArrayList<Category>>() {
        }.getType());
        if (list == null) list = new ArrayList<>();
        return list;
    }

    @Override
    public void openUploadVideo(BaseSlidingFragmentActivity activity) {
        UploadVideoActivity.start(activity);
    }

    private void showAds() {
        try {
//            long adsDisplayStart = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_DISPLAY_START);
            long adsDisplayCount = FirebaseRemoteConfig.getInstance().getLong(AdsUtils.KEY_FIREBASE.AD_DISPLAY_COUNT);

//            if (adsVideoIndex == adsDisplayStart || (adsDisplayCount > 0 && adsVideoIndex > 0 && adsVideoIndex % adsDisplayCount == 0)) {
            if ((adsDisplayCount > 0 && adsVideoIndex > 0 && adsVideoIndex % adsDisplayCount == 0)) {
                AdsManager.getInstance().showAdsFullScreen(new AdsListener() {
                    @Override
                    public void onAdClosed() {

                    }

                    @Override
                    public void onAdShow() {

                    }
                });
            }
            adsVideoIndex++;
        } catch (Exception ex) {
        }
    }

}

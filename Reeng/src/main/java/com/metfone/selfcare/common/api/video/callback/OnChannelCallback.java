package com.metfone.selfcare.common.api.video.callback;

import com.metfone.selfcare.model.tab_video.Channel;

import java.util.ArrayList;

/**
 * Created by tuanha00 on 3/13/2018.
 */

public interface OnChannelCallback {
    void onGetChannelsSuccess(ArrayList<Channel> channels);
    void onGetChannelsError(String s);
    void onGetChannelsComplete();
}

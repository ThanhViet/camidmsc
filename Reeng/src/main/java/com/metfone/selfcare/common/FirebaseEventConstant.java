package com.metfone.selfcare.common;

public class FirebaseEventConstant {
    public static final String EVENT_SELECT_TAB = "select_tab";
    public static final String EVENT_ISHARE_SUCCESS  = "metfonePlus_iShare_success";
    public static final String EVENT_ISHARE_FAILURE  = "metfonePlus_iShare_failure";
    public static final String EVENT_CHANGENUMBER_KEEPSIM_SUCCESS  = "metfonePlus_changeNumberKeepSim_success";
    public static final String EVENT_CHANGENUMBER_KEEPSIM_FAILURE  = "metfonePlus_changeNumberKeepSim_failure";
    public static final String EVENT_ORDER_NEWSIM_SUCCESS  = "metfonePlus_orderNewSim_success";
    public static final String EVENT_ORDER_NEWSIM_FAILURE  = "metfonePlus_orderNewSim_failure";
    public static final String EVENT_TOPUP_QRCODE_SUCCESS  = "metfonePlus_topup_QRCode_success";
    public static final String EVENT_TOPUP_QRCODE_FAILURE  = "metfonePlus_topup_QRCode_failure";
    public static final String EVENT_TOPUP_SUCCESS  = "metfonePlus_topup_success";
    public static final String EVENT_TOPUP_FAILURE  = "metfonePlus_topup_failure";
    public static final String EVENT_SELECT_SERVICE_PACK  = "metfoneplus_select_servicePack";
    public static final String EVENT_SELECT_EXCHANGE_SERVICE  = "metfoneplus_select_exchangeService";
    public static final String EVENT_SELECT_REGISTER_SERVICE  = "metfoneplus_register_service";
    public static final String EVENT_REGISTER_SERVICE_SUCCESS  = "metfonePlus_register_service_success";
    public static final String EVENT_REGISTER_SERVICE_FAILURE  = "metfonePlus_register_service_failure";

    public static final String EVENT_STOP_WATCH_FILM  = "stop_watch_film";
    public static final String EVENT_DETAIL_FILM_PAUSE_TRAILER  = "detail_film_pause_trailer";
    public static final String EVENT_DETAIL_FILM_SELECT_BUTTON  = "detail_film_selected_button";
    // name button for EVENT_DETAIL_FILM_SELECT_BUTTON
    public static final String BUTTON_LIKE  = "Like";
    public static final String BUTTON_UNLIKE  = "Unlike";
    public static final String BUTTON_RATE  = "Rate";
    public static final String BUTTON_MORELIKETHIS  = "More like this";
    public static final String BUTTON_REVIEW  = "Review";
    public static final String BUTTON_EPISODES  = "Episodes";
    public static final String BUTTON_SHARE  = "Share";
    public static final String BUTTON_PLAY  = "Play now";
    public static final String BUTTON_COMMENT  = "Comment";
    public static final String BUTTON_WATCH_FULLSCREEN_TRAILER  = "Watch fullscreen trailer";

    public static final String EVENT_SELECT_SUBTITLE  = "select_subtitle";
    public static final String EVENT_WATCH_FILM_IN_COUNTRY_GENRES  = "watch_film_in_country_genres";
    public static final String EVENT_WATCH_FILM_IN_CATEGORY_GENRES  = "watch_film_in_category_genres";
    public static final String EVENT_WATCH_FILM_IN_TOPIC  = "watch_film_in_topic";
    public static final String EVENT_OPEN_FILM_DETAIL_IN_COUNTRY  = "open_film_detail_in_country";
    public static final String EVENT_OPEN_FILM_DETAIL_IN_CATEGORY  = "open_film_detail_in_category";
    public static final String EVENT_OPEN_FILM_DETAIL_IN_TOPIC  = "open_film_detail_in_topic";
    public static final String EVENT_OPEN_FILM_DETAIL  = "open_film_detail";
    public static final String EVENT_WATCH_FILM  = "watch_film";

    public static final String EVENT_REWARD_VISIT_PAGE   = "CamIDReward_visit_page";
    public static final String EVENT_REWARD_REDEEM   = "CamIDReward_select_redeem";
    public static final String EVENT_REWARD_QRCODE   = "CamIDReward_select_QRCode";

    public static final String EVENT_ACTIVE_TAB = "active_tab";
    public static final String EVENT_WATCH_ESPORT = "eSport_watch_video";
    public static final String EVENT_WATCH_VIDEO_POPULAR_LIVE_ESPORT = "eSport_watch_video_popular_live_channel";
    public static final String EVENT_WATCH_PAST_BROADCAST_ESPORT = "eSport_watch_past_broadcast";
    public static final String EVENT_WATCH_UPLOADED_VIDEO_ESPORT = "eSport_watch_uploaded_video";
    public static final String EVENT_WATCH_LIVE_VIDEO_ESPORT = "eSport_watch_live_video";
    public static final String EVENT_OPEN_GAME  = "game_open";
}

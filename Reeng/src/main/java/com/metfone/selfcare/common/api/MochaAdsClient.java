package com.metfone.selfcare.common.api;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.tab_video.BannerVideo;
import com.metfone.selfcare.util.Utilities;

import java.util.concurrent.CopyOnWriteArrayList;

public class MochaAdsClient extends BaseApi {

    public static final String BANNER_VIDEO = "BannerVideo";
    public static final String CHANNEL_BANNER_VIDEO = "ChannelBannerVideo";
    public static final String TIME_BANNER_VIDEO = "TimeBannerVideo";

    public MochaAdsClient() {
        super(ApplicationController.self());
        getAdsInfo();
    }

    /**
     * lấy thông tin quảng cáo
     */
    private void getAdsInfo() {
        long timeResponse = SharedPrefs.getInstance().get(TIME_BANNER_VIDEO, Long.class);
        if (TimeHelper.checkTimeInDay(timeResponse)) return;

        BannerVideo bannerVideo = SharedPrefs.getInstance().get(MochaAdsClient.BANNER_VIDEO, BannerVideo.class);
        if (bannerVideo != null) {
            BannerVideo.Filter filter = bannerVideo.getFilter();
            if (filter != null) {
                if (Utilities.notEmpty(filter.getChannelId())) {
                    for (String s : filter.getChannelId()) {
                        SharedPrefs.getInstance().put(CHANNEL_BANNER_VIDEO + s, 1);
                    }
                } else {
                    SharedPrefs.getInstance().put(CHANNEL_BANNER_VIDEO + "", 1);
                }
            }
        }
        if (getReengAccountBusiness() == null
                || getReengAccountBusiness().getCurrentAccount() == null
                || !getReengAccountBusiness().isViettel()) return;
        if (getReengAccount() == null) return;

        long time = System.currentTimeMillis();
        String timeStamp = String.valueOf(time);

        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;

        ReengAccount account = getReengAccount();

        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + domainParam
                + vip
                + account.getGender()
                + account.getBirthdayString()
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        get(domain, VideoApi.ADS)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.User.VIP, vip)
                .putParameter(Parameter.User.GENDER, String.valueOf(account.getGender()))
                .putParameter(Parameter.User.BIRTHDAY, String.valueOf(account.getBirthdayString()))
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        SharedPrefs.getInstance().put(TIME_BANNER_VIDEO, System.currentTimeMillis());
                        BannerVideo bannerVideo = ApplicationController.self().getGson().fromJson(data, BannerVideo.class);
                        if (bannerVideo != null) {
//                            ArrayList<String> networkType = new ArrayList<>();
//                            networkType.add("3G");
//                            bannerVideo.getFilter().setIsVip(new ArrayList<String>());
//                            bannerVideo.getFilter().setChannelId(new ArrayList<String>());
//                            bannerVideo.getFilter().setNetworkType(networkType);
                            SharedPrefs.getInstance().put(BANNER_VIDEO, bannerVideo);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                })
                .execute();
    }

    public static class MochaAdsEvent {
        private static final MochaAdsEvent ourInstance = new MochaAdsEvent();

        public static MochaAdsEvent getInstance() {
            return ourInstance;
        }

        private CopyOnWriteArrayList<MochaAdsEvent.OnMochaAdsListener> onMochaAdsListeners;

        private MochaAdsEvent() {
            onMochaAdsListeners = new CopyOnWriteArrayList<>();
        }

        public void addListener(MochaAdsEvent.OnMochaAdsListener listener) {
            if (onMochaAdsListeners != null && !onMochaAdsListeners.contains(listener))
                onMochaAdsListeners.add(listener);
        }

        public void removeListener(MochaAdsEvent.OnMochaAdsListener listener) {
            if (onMochaAdsListeners != null)
                onMochaAdsListeners.remove(listener);
        }

        public void notifyHideAdsBy(String id) {
            if (Utilities.notEmpty(onMochaAdsListeners)) {
                for (OnMochaAdsListener onMochaAdsListener : onMochaAdsListeners) {
                    if (onMochaAdsListener != null) onMochaAdsListener.onHideAdsBy(id);
                }
            }
        }

        public interface OnMochaAdsListener {
            void onHideAdsBy(String adsId);
        }

    }

}

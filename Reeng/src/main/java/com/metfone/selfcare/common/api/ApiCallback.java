package com.metfone.selfcare.common.api;

public interface ApiCallback {

    void onError(String s);

    void onComplete();
}

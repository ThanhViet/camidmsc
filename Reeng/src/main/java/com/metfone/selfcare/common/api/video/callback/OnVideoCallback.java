package com.metfone.selfcare.common.api.video.callback;

import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

/**
 * Created by tuanha00 on 3/8/2018.
 */

public interface OnVideoCallback {
    void onGetVideosSuccess(ArrayList<Video> list);

    void onGetVideosError(String s);

    void onGetVideosComplete();
}

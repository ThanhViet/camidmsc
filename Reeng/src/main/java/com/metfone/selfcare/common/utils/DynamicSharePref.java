package com.metfone.selfcare.common.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.util.Log;

import java.lang.reflect.Type;

public class DynamicSharePref {
    private final String TAG = "Dynamic sharedpref";
    private static DynamicSharePref mInstance;
    private static SharedPreferences mSharedPreferences;

    private DynamicSharePref() {

    }

    public static DynamicSharePref getInstance() {
        if (mInstance == null) {
            mInstance = new DynamicSharePref();
            mInstance.mSharedPreferences = ApplicationController.self().getApplicationContext()
                    .getSharedPreferences("DYNAMIC SHARE PREF FINGER", Context.MODE_PRIVATE);
        }
        return mInstance;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass) {
        if (mSharedPreferences != null) {
            try {
                if (anonymousClass == String.class) {
                    return (T) mSharedPreferences.getString(key, "");
                } else if (anonymousClass == Boolean.class) {
                    return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, false));
                } else if (anonymousClass == Float.class) {
                    return (T) Float.valueOf(mSharedPreferences.getFloat(key, 0));
                } else if (anonymousClass == Integer.class) {
                    return (T) Integer.valueOf(mSharedPreferences.getInt(key, 0));
                } else if (anonymousClass == Long.class) {
                    return (T) Long.valueOf(mSharedPreferences.getLong(key, 0));
                } else {
                    return ApplicationController.self()
                            .getGson()
                            .fromJson(mSharedPreferences.getString(key, ""), anonymousClass);
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (NoSuchMethodError e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Type typeOfT) {
        if (mSharedPreferences != null) {
            try {
                return (T) ApplicationController.self()
                        .getGson()
                        .fromJson(mSharedPreferences.getString(key, ""), typeOfT);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (NoSuchMethodError e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass, T defaultValue) {
        if (mSharedPreferences != null) {
            try {
                if (anonymousClass == String.class) {
                    return (T) mSharedPreferences.getString(key, (String) defaultValue);
                } else if (anonymousClass == Boolean.class) {
                    return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, (Boolean) defaultValue));
                } else if (anonymousClass == Float.class) {
                    return (T) Float.valueOf(mSharedPreferences.getFloat(key, (Float) defaultValue));
                } else if (anonymousClass == Integer.class) {
                    return (T) Integer.valueOf(mSharedPreferences.getInt(key, (Integer) defaultValue));
                } else if (anonymousClass == Long.class) {
                    return (T) Long.valueOf(mSharedPreferences.getLong(key, (Long) defaultValue));
                } else {
                    return ApplicationController.self()
                            .getGson()
                            .fromJson(mSharedPreferences.getString(key, ""), anonymousClass);
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (NoSuchMethodError e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        return defaultValue;
    }

    public <T> void put(String key, T data) {
        if (mSharedPreferences != null) {
            try {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                if (data instanceof String) {
                    editor.putString(key, (String) data);
                } else if (data instanceof Boolean) {
                    editor.putBoolean(key, (Boolean) data);
                } else if (data instanceof Float) {
                    editor.putFloat(key, (Float) data);
                } else if (data instanceof Integer) {
                    editor.putInt(key, (Integer) data);
                } else if (data instanceof Long) {
                    editor.putLong(key, (Long) data);
                } else {
                    editor.putString(key, ApplicationController.self().getGson().toJson(data));
                }
                editor.apply();
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e);
            } catch (NoSuchMethodError e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }
}

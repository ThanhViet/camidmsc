package com.metfone.selfcare.common.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.metfone.selfcare.helper.Constants;

import java.util.Locale;

public class LocaleManager {

    private static final String TAG = "LocaleManager";

    private static final String PREF_LANGUAGE = "language_pref";
    private static final String LANGUAGE_DEFAULT = "km";
    private static final String LANGUAGE_KEY = "language_key";

    public static Context setLocale(Context c) {
        return updateResources(c, getLanguage(c));
    }

    public static Context setNewLocale(Context c, String language) {
        Log.e(TAG, "setNewLocale language: " + language);
        persistLanguage(c, language);
        return updateResources(c, language);
    }

    public static String getLanguage(Context c) {
        SharedPreferences prefs = c.getSharedPreferences(PREF_LANGUAGE, Context.MODE_PRIVATE);
        String language = prefs.getString(LANGUAGE_KEY, getKeyLocale());
        Log.e(TAG, "getLanguage: " + language);
        return language;
    }

    private static String getKeyLocale() {
        try {
            Locale localeDefault = Locale.getDefault();
            if (!TextUtils.isEmpty(localeDefault.getLanguage())) {
                return localeDefault.getLanguage();
            } else {
                return LANGUAGE_DEFAULT;
            }
        } catch (Exception e) {
            return LANGUAGE_DEFAULT;
        }
    }

    @SuppressLint("ApplySharedPref")
    private static void persistLanguage(Context c, String language) {
        SharedPreferences prefs = c.getSharedPreferences(PREF_LANGUAGE, Context.MODE_PRIVATE);
        prefs.edit().putString(LANGUAGE_KEY, language).commit();
    }

    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        return context;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }

    public static boolean isKmLanguage(Context context){
        if(getLanguage(context).equals(LANGUAGE_DEFAULT)){
            return true;
        }
        return false;
    }
}

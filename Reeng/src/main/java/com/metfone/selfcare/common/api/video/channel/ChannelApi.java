package com.metfone.selfcare.common.api.video.channel;

import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.video.callback.OnChannelInfoCallback;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.ChannelStatistical;

/**
 * Created by tuanha00 on 3/14/2018.
 */

public interface ChannelApi {

    void getStatistical(String id, ApiCallbackV2<ChannelStatistical> callback);

    void callApiSubOrUnsubChannel(String id, boolean follow);

    void getChannelInfo(String id, OnChannelInfoCallback channelInfoCallback);

    @Deprecated
    void getChannelByOwnerId(OnChannelInfoCallback channelInfoCallback);

    void getMyChannelInfoV2(ApiCallbackV2<Channel> callback);

    void updateOrCreateChannel(boolean uploadImage, Channel channel, OnChannelInfoCallback onChannelInfoCallback);
}

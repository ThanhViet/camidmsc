package com.metfone.selfcare.common.api;

public interface LogApi {
    int POS_HOME_SLIDE_BANNER = 1;
    int POS_HOME_FEATURE = 2;
    int POS_HOME_CONTACT = 3;
    int POS_HOME_SLIDE_VIDEO = 4;
    int POS_HOME_VIDEO = 5;
    int POS_HOME_CHANNEL = 6;
    int POS_HOME_SLIDE_MOVIE = 7;
    int POS_HOME_MOVIE = 8;
    int POS_HOME_SLIDE_MUSIC = 9;
    int POS_HOME_MUSIC = 10;
    int POS_HOME_SLIDE_NEWS = 11;
    int POS_HOME_NEWS = 12;
    int POS_HOME_SLIDE_TIIN = 13;
    int POS_HOME_TIIN = 14;
    int POS_HOME_SLIDE_COMIC = 15;
    int POS_HOME_COMIC = 16;

    void logAction(LogType logType, TabType tabType, String data, ApiCallback callback);

    /**
     * api log hành vi người dùng click vào banner
     *
     * @param deepLink    deepLink
     * @param serviceType serviceType
     * @param callback    callback
     */
    void logBanner(String deepLink, TypeService serviceType, ApiCallback callback);

    /**
     * typeService : là vị trí banner mà user click :
     * + inbox_tab : banner hiển thị trên cùng các thread chat ở tab Phone.
     * + music_tab : banner hiển thị trên tab nhạc
     * + onmedia_tab: banner hiển thị trên tab onmedia
     * + more_tab: banner hiển thị trên tab more (sở thú WC 2018)
     * + vqmm_tab: banner hiển thị trong màn hình VQMM (vị trí logo WC 2018)
     */
    enum TypeService {
        INBOX("inboxBanner"),
        MUSIC("strangerBanner"),
        ON_MEDIA("onmediaBanner"),
        MORE("moreBanner"),
        VQMM("vqmm_tab"),
        BUTTON_DEEPLINK_CHAT("buttonMessage"),
        VIDEO_HOT_CLICK("videoHotPlay"),
        VIDEO_HOT_EXIT("videoHotExit"),
        HOME_BANNER("tabHomeBanner"),
        SUGGEST_CONTENT("suggestContent");
        public String VALUE;

        TypeService(String value) {
            this.VALUE = value;
        }
    }

    enum TabType {
        TAB_HOME("tab_home"),
        TAB_PHONE_CHAT("tab_phone_chat"),
        TAB_PHONE_CALL("tab_phone_call"),
        TAB_PHONE_CONTACT("tab_phone_contact"),
        TAB_PHONE_GROUP("tab_phone_group"),
        TAB_VIDEO("tab_video"),
        TAB_MOVIE("tab_movie"),
        TAB_MUSIC("tab_music"),
        TAB_NEWS("tab_news"),
        TAB_TIIN("tab_tiin"),
        TAB_WAP("tab_wap"),
        TAB_SEARCH("tab_search"),
        ;

        public String VALUE;

        TabType(String value) {
            this.VALUE = value;
        }
    }

    enum LogType {
        CLICK_CONTENT("click_content"),
        CLICK_BANNER("click_banner"),
        CLICK_CONTACT("click_contact"),
        CLICK_FEATURE("click_func"),
        LIKE("like"),
        UNLIKE("unlike"),
        DISLIKE("dislike"),
        SHARE("share"),
        COMMENT("comment"),
        ADD_FAV("add_favorite"),
        DEL_FAV("del_favorite"),
        ADD_LATER("add_later"),
        DEL_LATER("del_later"),
        FOLLOW("follow"),
        UNFOLLOW("unfollow"),
        ;

        public String VALUE;

        LogType(String value) {
            this.VALUE = value;
        }
    }

    enum ContentType {
        BANNER("banner"),
        VIDEO("video"),
        MUSIC("music"),
        MOVIE("movie"),
        NEWS("news"),
        TIIN("tiin"),
        LINK("link"),
        THREAD_CHAT("thread_chat"),
        CONTACT("contact"),
        FEATURE("more_func"),
        COMIC("comic"),
        ;

        public String VALUE;

        ContentType(String value) {
            this.VALUE = value;
        }
    }

    enum FuncName {
        FUNC_SOCIAL("social"),
        FUNC_STRANGER("stranger"),
        FUNC_VIDEO("video"),
        FUNC_MOVIE("movie"),
        FUNC_MUSIC("music"),
        FUNC_NEWS("news"),
        FUNC_WAP("wap"),
        FUNC_TIIN("tiin"),
        FUNC_SECURITY("security"),
        FUNC_SELFCARE("selfcare"),
        FUNC_MY_VIETTEL("my_viettel"),
        FUNC_DEEPLINK("deeplink"),
        FUNC_SPOINT("spoint"),
        FUNC_PRIVILEGE("privilege"),
        FUNC_LUCKY_WHEEL("lucky_wheel"),
        FUNC_CALL_OUT("call_out"),
        FUNC_QR_CODE("qr_code"),
        FUNC_GAME("game"),
        FUNC_FIND_FRIEND("find_friend"),
        FUNC_INVITE_FRIEND("invite_friend"),
        FUNC_DATA_CHALLENGE("data_challenge"),
        FUNC_DATA_PACKAGE("data_package"),
        ;

        public String VALUE;

        FuncName(String value) {
            this.VALUE = value;
        }
    }
}

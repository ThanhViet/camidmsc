package com.metfone.selfcare.common.utils.listener;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.movie.fragment.CategoryDetailFragment;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.video.fragment.VideoPagerFragment;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnTabListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelFragment;

import java.util.ArrayList;

/**
 * Created by HoangAnhTuan on 3/26/2018.
 */

public class ListenerUtilsImpl implements ListenerUtils {

    private ArrayList<Listener> listenerArrayList;

    public ListenerUtilsImpl() {
        listenerArrayList = new ArrayList<>();
    }

    @Override
    public void addListener(Listener listener) {
        if (listener != null && listenerArrayList != null) listenerArrayList.add(listener);
    }

    @Override
    public void removerListener(Listener listener) {
        if (listener != null && listenerArrayList != null) listenerArrayList.remove(listener);
    }

    @Override
    public void notifyVideoLikeChangedData(Video video) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnVideoChangedDataListener) {
                ((OnVideoChangedDataListener) listener).onVideoLikeChanged(video);
            }
        }
    }

    @Override
    public void notifyVideoShareChangedData(Video video) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnVideoChangedDataListener) {
                ((OnVideoChangedDataListener) listener).onVideoShareChanged(video);
            }
        }
    }

    @Override
    public void notifyVideoCommentChangedData(Video video) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnVideoChangedDataListener) {
                ((OnVideoChangedDataListener) listener).onVideoCommentChanged(video);
            }
        }
    }

    @Override
    public void notifyChannelSubscriptionsData(Channel channel) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnChannelChangedDataListener) {
                ((OnChannelChangedDataListener) listener).onChannelSubscribeChanged(channel);
            }
        }
    }

    @Override
    public void notifyTabReselected(int parentTabId, int position) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnTabListener) {
                if (parentTabId == Constants.TAB_MOVIE_HOME && (listener instanceof MoviePagerFragment
                        || listener instanceof CategoryDetailFragment
                )) {
                    ((OnTabListener) listener).onTabReselected(position);
                } else if (parentTabId == Constants.TAB_VIDEO_HOME && (listener instanceof VideoPagerFragment
                        || listener instanceof com.metfone.selfcare.module.video.fragment.MoviePagerFragment
                        || listener instanceof SubscribeChannelFragment
                )) {
                    ((OnTabListener) listener).onTabReselected(position);
                }
            }
        }
    }

    @Override
    public void notifyTabSelected(int parentTabId, int position) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnTabListener) {
                if (parentTabId == Constants.TAB_MOVIE_HOME && (listener instanceof MoviePagerFragment
                        || listener instanceof CategoryDetailFragment
                )) {
                    ((OnTabListener) listener).onTabSelected(position);
                } else if (parentTabId == Constants.TAB_VIDEO_HOME && (listener instanceof VideoPagerFragment
                        || listener instanceof com.metfone.selfcare.module.video.fragment.MoviePagerFragment
                        || listener instanceof SubscribeChannelFragment
                )) {
                    ((OnTabListener) listener).onTabSelected(position);
                }
            }
        }
    }

    @Override
    public void notifyInternetChanged() {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnInternetChangedListener) {
                ((OnInternetChangedListener) listener).onInternetChanged();
            }
        }
    }

    @Override
    public void notifyVideoSaveChangedData(Video video) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnVideoChangedDataListener) {
                ((OnVideoChangedDataListener) listener).onVideoSaveChanged(video);
            }
        }
    }

    @Override
    public void notifyVideoWatchLaterChangedData(Video video) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnVideoChangedDataListener) {
                ((OnVideoChangedDataListener) listener).onVideoWatchLaterChanged(video);
            }
        }
    }

    @Override
    public void notifyCreateChannel(Channel channel) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnChannelChangedDataListener) {
                ((OnChannelChangedDataListener) listener).onChannelCreate(channel);
            }
        }
    }

    @Override
    public void notifyUpdateChannel(Channel channel) {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnChannelChangedDataListener) {
                ((OnChannelChangedDataListener) listener).onChannelUpdate(channel);
            }
        }
    }

    @Override
    public void notifyDisableLoading() {
        for (Listener listener : listenerArrayList) {
            if (listener instanceof OnTabListener) {
                ((OnTabListener) listener).onDisableLoading();
            }
        }
    }
}

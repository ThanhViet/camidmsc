package com.metfone.selfcare.common.api.video.callback;

import com.metfone.selfcare.model.tab_video.Channel;

/**
 * Created by tuanha00 on 3/12/2018.
 */

public interface OnChannelInfoCallback {

    void onGetChannelInfoSuccess(Channel channel);

    void onGetChannelInfoError(String s);

    void onGetChannelInfoComplete();
}

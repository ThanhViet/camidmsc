package com.metfone.selfcare.common.utils.listener;

import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;

/**
 * Created by HoangAnhTuan on 3/26/2018.
 */

public interface ListenerUtils {
    void addListener(Listener listener);

    void removerListener(Listener listener);

    void notifyVideoLikeChangedData(Video video);

    void notifyVideoShareChangedData(Video video);

    void notifyVideoCommentChangedData(Video video);

    void notifyChannelSubscriptionsData(Channel mChannel);

    void notifyTabReselected(int parentTabId, int currentPosition);

    void notifyTabSelected(int parentTabId, int currentPosition);

    void notifyInternetChanged();

    void notifyVideoSaveChangedData(Video video);

    void notifyVideoWatchLaterChangedData(Video video);

    void notifyCreateChannel(Channel channel);

    void notifyUpdateChannel(Channel channel);

    void notifyDisableLoading();
}

package com.metfone.selfcare.common.api;

import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.net.URL;
import java.util.Map;

public class BaseApi implements ConstantApi {
    public static final String MOCHA_API = "mocha-api";
    public static final String UUID = "uuid";
    protected String TAG = BaseApi.class.getSimpleName();

    protected final ApplicationController application;
    protected final Gson gson;

    public BaseApi(ApplicationController app) {
        application = app;
        gson = app.getGson();
    }

    protected Http.Builder configBuilder(@NonNull Http.Builder builder) {
        builder.putHeader(MOCHA_API, getReengAccountBusiness().getMochaApi());
        builder.putHeader("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putHeader("countryCode", getReengAccountBusiness().getRegionCode());
        builder.putHeader(UUID, Utilities.getUuidApp());
        return builder;
    }

    protected Http.Builder get(String url) {
        Http.Builder builder = Http.get(url);
        builder.setContext(application);
        return configBuilder(builder);
    }

    protected Http.Builder post(String url) {
        Http.Builder builder = Http.post(url);
        builder.setContext(application);
        return configBuilder(builder);
    }

    protected Http.Builder upload(String url) {
        Http.Builder builder = Http.upload(url);
        builder.setContext(application);
        return configBuilder(builder);
    }

    protected Http.Builder get(String baseUrl, String url) {
        Http.Builder builder = Http.get(application, baseUrl, url);
        return configBuilder(builder);
    }

    protected Http.Builder post(String baseUrl, String url) {
        Http.Builder builder = Http.post(application, baseUrl, url);
        return configBuilder(builder);
    }

    protected Http.Builder upload(String baseUrl, String url) {
        Http.Builder builder = Http.upload(application, baseUrl, url);
        return configBuilder(builder);
    }

    protected Http.Builder get(String baseUrl, String url, String tag) {
        Http.Builder builder = Http.get(application, baseUrl, url);
        return configBuilder(builder);
    }

    protected <T> T getParam(Map<String, Object> params, String name, T valueDefault) {
        T t = (T) params.get(name);
        if (t == null) t = valueDefault;
        return t;
    }

    /**
     * trả về thông tin người dùng
     *
     * @return thông tin người dùng
     */
    protected ReengAccountBusiness getReengAccountBusiness() {
        return application.getReengAccountBusiness();
    }

    /**
     * trả về thông tin người dùng
     *
     * @return thông tin người dùng
     */
    protected ReengAccount getReengAccount() {
        return getReengAccountBusiness().getCurrentAccount();
    }

    /**
     * cung cấp domain file
     *
     * @return domain file
     */
    public String getDomainFile() {
        return UrlConfigHelper.getInstance(application).getDomainFile();
    }

    /**
     * thực hiện chuyển domain sang param
     *
     * @param domain domain
     * @return param
     */
    public static String convertDomainToDomainParam(String domain) {
        if (TextUtils.isEmpty(domain))
            return "";
        try {
            URL url = new URL(domain);
            return url.getHost();
        } catch (Exception e) {
            Log.e("BaseApi", e);
        }
        return domain
                .replace(":8080", "")
                .replace(":8088", "")
                .replace("http://", "")
                .replace("https://", "");
    }

    public String getDomainOnMedia() {
        return UrlConfigHelper.getInstance(application).getDomainOnMedia();
    }

    public String getDomainMochaVideo() {
        return UrlConfigHelper.getInstance(application).getDomainMochaVideo();
    }

    public String getDomainKeengMusic() {
        return UrlConfigHelper.getInstance(application).getDomainKeengMusic();
    }

    public String getDomainKeengMovies() {
        return UrlConfigHelper.getInstance(application).getDomainKeengMovies();
    }

    public String getDomainNetNews() {
        return UrlConfigHelper.getInstance(application).getDomainNetnews();
    }

    public String getDomainTiin() {
        return UrlConfigHelper.getInstance(application).getDomainTiin();
    }

    public String getDomainKeengMusicSearch() {
        return UrlConfigHelper.getInstance(application).getDomainKeengMusicSearch();
    }

    public void getDetailUrl(String url, boolean isFullData, HttpCallBack listener) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + url + (isFullData ? "GetMetadata" : "mocha")
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());
        get(getDomainMochaVideo(), VideoApi.GET_DETAIL_URL_V2)
                .setTag("getDetailUrl:" + url)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.URL, url)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .putParameter("type", isFullData ? "GetMetadata" : "mocha")
                .withCallBack(listener).execute();
    }

    public void getDetailUrl(String url, final boolean isFullData, final ApiCallbackV2<RestAllFeedsModel> listener) {
        getDetailUrl(url, isFullData, new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                try {
                    RestAllFeedsModel model = new Gson().fromJson(data, RestAllFeedsModel.class);
                    if (listener != null) listener.onSuccess("", model);
                } catch (Exception e) {
                    onFailure(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onFailure(String message) {
                if (listener != null) listener.onError(message);
            }

            @Override
            public void onCompleted() {
                if (listener != null) listener.onComplete();
            }
        });
    }

}

package com.metfone.selfcare.common.api;

import com.metfone.selfcare.common.api.http.HttpCallBack;

import java.util.ArrayList;

public interface FileApi {

    void feedback(String s, ArrayList<String> photos);

    void upload(String pathImage, ApiCallbackProgressV2<String> apiCallbackProgressV2);

    void uploadBackupFile(String file, int synDesktop, ApiCallbackProgressV2<String> apiCallbackProgressV2);

    void getBackupInfo(HttpCallBack callBack);

    void deleteBackupFile(String fileId);
}

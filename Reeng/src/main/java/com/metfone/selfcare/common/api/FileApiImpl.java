package com.metfone.selfcare.common.api;

import android.os.Build;

import com.hoanganhtuan95ptit.compressor.Compressor;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.http.HttpMethod;
import com.metfone.selfcare.common.api.http.HttpProgressCallBack;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.util.Log;

import org.json.JSONException;

import java.util.ArrayList;

public class FileApiImpl extends BaseApi implements FileApi {
    private static final String TAG = FileApiImpl.class.getSimpleName();

    public FileApiImpl() {
        super(ApplicationController.self());
    }

    @Override
    public void feedback(String content, ArrayList<String> photos) {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();

//        msisdn, content, images, clientType, revision, token, timestamp
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + content
                + gson.toJson(photos)
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        post(domainFile + Url.File.FEED_BACK)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter(Parameter.CONTENT, content)
                .putParameter(Parameter.IMAGES, gson.toJson(photos))
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .putParameter("version_app", BuildConfig.VERSION_NAME)
                .putParameter("device_os", Build.VERSION.RELEASE)
                .putParameter("device_name", Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL)

                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }
                }).execute();
    }

    @Override
    public void upload(final String pathImage, final ApiCallbackProgressV2<String> apiCallbackProgressV2) {
        Compressor.compress(ApplicationController.self(), false, pathImage, new Compressor.OnCompressListener() {
            @Override
            public void onSuccess(String newLocal) {
                String baseUrl = UrlConfigHelper.getInstance(application).getDomainImage();
                String url = UrlConfigHelper.getInstance(application).getUrlByKey(Config.UrlEnum.FILE_UPLOAD_URL);
                ReengAccount reengAccount = getReengAccount();
                upload(baseUrl + url)
                        .setMethod(HttpMethod.FILE)
                        .putFile(Constants.HTTP.FILE.REST_UP_FILE, newLocal)
                        .putParameter(Constants.HTTP.REST_MSISDN, reengAccount.getJidNumber())
                        .putParameter(Constants.HTTP.FILE.REST_FILE_TYPE, "image")
                        .putParameter(Constants.HTTP.REST_TOKEN, reengAccount.getToken())
                        .withCallBack(new HttpProgressCallBack() {
                            @Override
                            public void onSuccess(String response) throws JSONException {
                                if (apiCallbackProgressV2 != null)
                                    apiCallbackProgressV2.onSuccess("", response);
                            }

                            @Override
                            public void onProgressUpdate(int position, int sum, int percentage) {
                                if (apiCallbackProgressV2 != null)
                                    apiCallbackProgressV2.onProgress(percentage);
                            }

                            @Override
                            public void onFailure(String msg) {
                                super.onFailure(msg);
                                if (apiCallbackProgressV2 != null)
                                    apiCallbackProgressV2.onError(msg);
                            }

                            @Override
                            public void onCompleted() {
                                super.onCompleted();
                                if (apiCallbackProgressV2 != null)
                                    apiCallbackProgressV2.onComplete();
                            }
                        })
                        .execute();
            }
        });
    }

    @Override
    public void uploadBackupFile(String file, int synDesktop, final ApiCallbackProgressV2<String> apiCallbackProgressV2) {
        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String security = HttpHelper.encryptDataV2(application,
                account.getJidNumber()
                        + Constants.HTTP.CLIENT_TYPE_STRING
                        + Config.REVISION
                        + String.valueOf(synDesktop)
                        + account.getToken()
                        + timeStamp
                , account.getToken());

        upload(domainFile + Url.File.BACKUP_MESSAGE_UPLOAD)
                .setMethod(HttpMethod.FILE)
                .putFile(Constants.HTTP.FILE.REST_UP_FILE, file)
                .putParameter(Constants.HTTP.REST_MSISDN, account.getJidNumber())
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("synDesktop", String.valueOf(synDesktop))
                .withCallBack(new HttpProgressCallBack() {
                    @Override
                    public void onSuccess(String response) throws JSONException {
                        if (apiCallbackProgressV2 != null)
                            apiCallbackProgressV2.onSuccess("", response);
                    }

                    @Override
                    public void onProgressUpdate(int position, int sum, int percentage) {
                        if (apiCallbackProgressV2 != null)
                            apiCallbackProgressV2.onProgress(percentage);
                    }

                    @Override
                    public void onFailure(String msg) {
                        super.onFailure(msg);
                        if (apiCallbackProgressV2 != null)
                            apiCallbackProgressV2.onError(msg);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallbackProgressV2 != null)
                            apiCallbackProgressV2.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getBackupInfo(HttpCallBack callBack) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(getDomainFile(), Url.File.BACKUP_FILE_LIST)
                .putParameter(Constants.HTTP.REST_MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(callBack)
                .setTimeOut(30)
                .execute();
    }

    @Override
    public void deleteBackupFile(String fileId) {
        String domainFile = getDomainFile();
        ReengAccount account = getReengAccount();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber() + fileId
                + account.getToken()
                + timeStamp, account.getToken());

        post(domainFile + Url.File.BACKUP_FILE_DELETE)
                .putParameter(Constants.HTTP.REST_MSISDN, account.getJidNumber())
                .putParameter(Constants.HTTP.FILE.REST_FILE_ID, fileId)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        Log.d(TAG, "deleteBackupFile: " + data);
                    }

                    @Override
                    public void onFailure(String message) {
                        Log.d(TAG, "deleteBackupFile: Fail - " + message);
                        super.onFailure(message);
                    }
                })
                .setTimeOut(30)
                .execute();
    }
}

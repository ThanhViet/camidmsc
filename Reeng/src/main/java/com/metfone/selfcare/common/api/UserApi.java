package com.metfone.selfcare.common.api;

import com.metfone.selfcare.database.model.ImageProfile;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.httprequest.ProfileRequestHelper;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

public interface UserApi {
    void getAvatars(CompositeDisposable compositeDisposable, String msisdn, AvatarCallback avatarCallback);

    void removeAvatar(CompositeDisposable compositeDisposable, String lAvatar, RemoveAvatarCallback avatarCallback);

    void removeImageAlbum(CompositeDisposable compositeDisposable, ImageProfile imageProfile,
                          ProfileRequestHelper.onResponseRemoveImageProfileListener listener);

    void getInfoAnonymous(String keyXxtea, ApiCallbackV2<String> apiCallback);

    void setRegId(String regid);

    void unregisterRegid(int actionType);

    void logOpenApp();

    interface AvatarCallback {
        void onGetAvatarSuccess(ArrayList<ImageProfile> list);

        void onGetAvatarError(String s);

        void onGetAvatarCompleted();
    }

    interface RemoveAvatarCallback {
        void onRemoveSuccess(String lAvatar);

        void onRemoveFail(String msg);
    }
}

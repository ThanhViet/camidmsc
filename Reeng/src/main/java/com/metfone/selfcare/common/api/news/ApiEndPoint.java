/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.metfone.selfcare.common.api.news;

/**
 * Created by HaiKE on 01/02/17.
 */

public final class ApiEndPoint {

    //    public static final String ENDPOINT_BASE_URL = "http://api.tinngan.vn/Tinngan.svc/";//BuildConfig.BASE_URL;
    public static final String ENDPOINT_BASE_URL_TEST = "http://hlvip2.mocha.com.vn:80/Tinngan.svc/";
    public static final String ENDPOINT_BASE_URL = "http://hlvip2.mocha.com.vn:8080/Tinngan.svc/";
    public static final String PREFIX_ENDPOINT_URL = "/Tinngan.svc/";

    public static final String GET_NEWS_BY_CATEGORY = PREFIX_ENDPOINT_URL + "getCateList";
    public static final String GET_NEWS_CATEGORY = PREFIX_ENDPOINT_URL + "getCategory";
    public static final String GET_NEWS_HOME = PREFIX_ENDPOINT_URL + "getNewsHomev2";
    public static final String GET_NEWS_HOME_CATE = PREFIX_ENDPOINT_URL + "getHomeCate";
    public static final String GET_NEWS_CONTENT = PREFIX_ENDPOINT_URL + "getContent";
    public static final String GET_NEWS_RELATE = PREFIX_ENDPOINT_URL + "getListRelateReadNews";
    public static final String GET_NEWS_RELATE_FROM_HOME = PREFIX_ENDPOINT_URL + "getListCateRelate";
    public static final String GET_NEWS_RELATE_FROM_CATEGORY = PREFIX_ENDPOINT_URL + "getListCateRelateOld";
    public static final String GET_NEWS_RELATE_FROM_CATEGORY_POSTION_0 = PREFIX_ENDPOINT_URL + "getListCateRelateOldCM";
    public static final String GET_NEWS_RELATE_FROM_EVENT = PREFIX_ENDPOINT_URL + "getListCateRelateEvent";
    public static final String GET_NEWS_BY_SOURCE = PREFIX_ENDPOINT_URL + "getListNewsOfSource";
    public static final String GET_EVENT_LIST = PREFIX_ENDPOINT_URL + "getHomeEvent";
    public static final String GET_NEWS_BY_EVENT = PREFIX_ENDPOINT_URL + "getEventView";
    public static final String GET_HOT_NEWS = PREFIX_ENDPOINT_URL + "getMostView";
    public static final String GET_SEARCH = PREFIX_ENDPOINT_URL + "getSearch";
    public static final String GET_MOST_NEWS = PREFIX_ENDPOINT_URL + "getMostView";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }
}

package com.metfone.selfcare.common.utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import android.text.TextUtils;
import android.webkit.URLUtil;

import androidx.core.content.FileProvider;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.facebook.FacebookHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.share.listener.ShareImagesOnFacebookListener;
import com.metfone.selfcare.module.share.task.DownloadImagesLuckeyWheelForShareFacebookTask;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.util.Utilities;

import java.io.File;
import java.util.ArrayList;

public class ShareUtils {

    private static ItemContextMenu providerContextMenu(BaseSlidingFragmentActivity activity
            , int mStringRes, int mDrawableRes, int itemId) {
        return new ItemContextMenu(activity.getString(mStringRes), mDrawableRes, null, itemId);
    }

    private static ArrayList<ItemContextMenu> providerContextMenus(BaseSlidingFragmentActivity activity
            , boolean isShowPeopleShared, boolean isShowPostOnSocial) {
        ArrayList<ItemContextMenu> list = new ArrayList<>();
//            list.add(providerContextMenu(activity, R.string.onmedia_share_now, R.drawable.ic_share, Constants
//                    .MENU.SHARE_NOW));
        list.add(providerContextMenu(activity, R.string.menu_send_message, R.drawable.ic_share_send_message,
                Constants.MENU.SEND_MESSAGE));
        if (isShowPostOnSocial)
            list.add(providerContextMenu(activity, R.string.post_on_social, R.drawable.ic_post_on_social,
                    Constants.MENU.MENU_WRITE_STATUS));
        list.add(providerContextMenu(activity, R.string.post_facebook, R.drawable.ic_facebook,
                Constants.MENU.CONFIRM_SHARE_FACEBOOK));
        list.add(providerContextMenu(activity, R.string.menu_copy_link, R.drawable.ic_share_copy,
                Constants.MENU.COPY));
        if (isShowPeopleShared)
            list.add(providerContextMenu(activity, R.string.onmedia_title_user_share, R.drawable.ic_people_share,
                    Constants.MENU.MENU_LIST_SHARE));
        list.add(providerContextMenu(activity, R.string.tab_video_more_option, R.drawable.ic_more,
                Constants.MENU.MORE));
        return list;
    }

    public static void openPeopleShareContent(BaseSlidingFragmentActivity activity, String link) {
        if (activity == null || TextUtils.isEmpty(link)) return;
        NavigateActivityHelper.navigateToOnMediaLikeOrShare(activity, link, false);
    }

    public static void shareWithIntent(BaseSlidingFragmentActivity activity, String content, String title) {
        if (activity == null || TextUtils.isEmpty(content)) return;
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        if (URLUtil.isNetworkUrl(content))
            intent.setType("text/plain");
        else
            intent.setType("text/html");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        activity.startActivity(Intent.createChooser(intent, title));
    }

    //TODO check param add ref=fb
    public static void shareFacebook(final BaseSlidingFragmentActivity activity, final String link, String serviceType) {
        if (activity == null || TextUtils.isEmpty(link)) return;
        new FacebookHelper(activity).shareContentToFacebook(activity, activity.getCallbackManager(), link,
                null, null, null, serviceType);
    }
    public static void shareMes(final BaseSlidingFragmentActivity activity, String text, String image, String link) {
        if (activity == null || TextUtils.isEmpty(text)) return;
        new FacebookHelper(activity).shareContentToFacebook(activity, activity.getCallbackManager(), null,
                image, null, text, null);
    }
    public static void shareVideo(BaseSlidingFragmentActivity activity, Video video) {
        if (activity == null || video == null) return;
        if (video.isShare()) {
            activity.showToast(R.string.onmedia_already_shared);
        } else {
            activity.showToast(R.string.onmedia_share_success);
            video.setShare(true);
            video.setTotalShare(video.getTotalShare() + 1);
            ListenerUtils listenerUtils = ApplicationController.self().getApplicationComponent().providerListenerUtils();
            VideoApi videoApi = ApplicationController.self().getApplicationComponent().providerVideoApi();
            listenerUtils.notifyVideoShareChangedData(video);
            videoApi.shareVideo(FeedModelOnMedia.convertVideoToFeedModelOnMedia(video));
        }
    }

    public static void sendToFriend(BaseSlidingFragmentActivity activity, String link) {
        if (activity == null || TextUtils.isEmpty(link)) return;
        ReengMessage msg = new ReengMessage();
        msg.setFilePath(link);
        msg.setContent("\n" + link);
        msg.setMessageType(ReengMessageConstant.MessageType.text);
        ArrayList<ReengMessage> listMsg = new ArrayList<>();
        listMsg.add(msg);
        if (Config.Features.FLAG_SHARE_OTHER_WITH_BUSINESS) {
            ShareContentBusiness business = new ShareContentBusiness(activity, listMsg, true);
            business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_TO_FRIEND);
            business.showPopupShareMessages();
        } else
            NavigateActivityHelper.navigateToChooseContact(activity, Constants.CHOOSE_CONTACT
                    .TYPE_SHARE_FROM_OTHER_APP, null, null, listMsg, true, true, -1, false);
    }

    public static void openShareMenu(final BaseSlidingFragmentActivity activity, final Object object) {
        if (activity == null || activity.isFinishing() || object == null) return;
        ShareContentBusiness business = new ShareContentBusiness(activity, object);
        business.setTypeSharing(ShareContentBusiness.TYPE_SHARE_CONTENT);
        business.showPopupShareContent();
    }

    public static String getLinkShare(NewsModel item) {
        String result = "";
        if (item != null && !TextUtils.isEmpty(item.getUrl())) {
            if (item.getUrl().contains("netnews.vn") || item.getUrl().contains("http:")) {
                result = item.getUrl();
            } else {
                result = CommonUtils.DOMAIN + item.getUrl();
            }
            if (result.contains("?alias=app"))
                result = result.replace("?alias=app", "");
        }
        return result;
    }

    public static String getLinkShareTiin(TiinModel item) {
        String result = "";
        if (item != null && !TextUtils.isEmpty(item.getUrl())) {
            if (item.getUrl().contains("tiin.vn") || item.getUrl().contains("http:")) {
                result = item.getUrl();
            } else {
                result = CommonUtils.DOMAIN + item.getUrl();
            }
            if (result.contains("?alias=app"))
                result = result.replace("?alias=app", "");
        }
        return result;
    }

    public static void sendToFriend(BaseSlidingFragmentActivity activity, FeedModelOnMedia feed) {
        if (activity == null || feed == null || feed.getFeedContent() == null)
            return;
        sendToFriend(activity, feed.getFeedContent().getLink());
    }

    public static void shareWithIntent(BaseSlidingFragmentActivity activity, FeedModelOnMedia feed) {
        if (activity == null || feed == null || feed.getFeedContent() == null)
            return;
        String titleShare;
        if (OnMediaHelper.isFeedViewVideo(feed))
            titleShare = activity.getResources().getString(R.string.title_share_video_other);
        else
            titleShare = activity.getResources().getString(R.string.share);
        shareWithIntent(activity, feed.getFeedContent().getLink(), titleShare);
    }

    public static void shareImageFileWithIntent(BaseSlidingFragmentActivity activity, String filePath, String title) {
        //https://stackoverflow.com/questions/38200282/android-os-fileuriexposedexception-file-storage-emulated-0-test-txt-exposed
        if (activity == null || TextUtils.isEmpty(filePath)) return;
        File file = new File(filePath);
        if (file.isFile()) {
            Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", file);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("image/*");
            activity.startActivity(Intent.createChooser(intent, title));

            //   Intent share = new Intent(Intent.ACTION_SEND);
            //                            share.setType("image/jpeg");
            //                            share.putExtra(Intent.EXTRA_STREAM, FileHelper.fromFile(mApplication, fileShare));
            //                            startActivity(Intent.createChooser(share, mRes.getString(R.string.share)));
        }
    }

    public static void shareImageWithIntent(BaseSlidingFragmentActivity activity, ArrayList<Uri> list, String title) {
        if (activity == null || Utilities.isEmpty(list)) return;
        //intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        //Intent intent = new Intent(Intent.ACTION_SEND);
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(Intent.EXTRA_STREAM, list);
        intent.setType("image/*");
        activity.startActivity(Intent.createChooser(intent, title));
    }

    public static void shareImageFileToFacebook(final BaseSlidingFragmentActivity activity, final String filePath) {
        if (activity == null || TextUtils.isEmpty(filePath)) return;
        ArrayList<String> list = new ArrayList<>();
        list.add(filePath);
        new FacebookHelper(activity).shareImageFileToFacebook(activity, activity.getCallbackManager(), list);
    }

    public static void shareImageFileToFacebook(final BaseSlidingFragmentActivity activity, final ArrayList<String> listImagePaths) {
        if (activity == null || Utilities.isEmpty(listImagePaths)) return;
        new FacebookHelper(activity).shareImageFileToFacebook(activity, activity.getCallbackManager(), listImagePaths);
    }

    public static void shareImageBitmapToFacebook(final BaseSlidingFragmentActivity activity, final ArrayList<Bitmap> bitmaps) {
        if (activity == null || Utilities.isEmpty(bitmaps)) return;
        new FacebookHelper(activity).shareImageBitmapToFacebook(activity, activity.getCallbackManager(), bitmaps);
    }

    public static void shareImageLuckeyWheelToFacebook(final BaseSlidingFragmentActivity activity, String url, String serviceCode) {
        if (activity == null || TextUtils.isEmpty(url)) return;
        DownloadImagesLuckeyWheelForShareFacebookTask task = new DownloadImagesLuckeyWheelForShareFacebookTask(ApplicationController.self(), url);
        task.setListener(new ShareImagesOnFacebookListener() {
            @Override
            public void onPrepareDownload() {
                if (activity != null) activity.showLoadingDialog("", R.string.processing);
            }

            @Override
            public void onCompletedDownload(ArrayList<Bitmap> list) {

            }

            @Override
            public void onCompletedDownload(Bitmap bitmap) {
                if (activity == null || activity.isFinishing()) return;
                activity.hideLoadingDialog();
                if (bitmap == null) {
                    activity.showToast(R.string.e601_error_but_undefined);
                } else {
                    new FacebookHelper(activity).shareImageBitmapToFacebookLuckyWheel(activity, activity.getCallbackManager(), bitmap, url, serviceCode);
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    //TODO check param add ref=fb
    public static void shareFacebookLuckyWheel(final BaseSlidingFragmentActivity activity, final String link, String serviceType) {
        if (activity == null || TextUtils.isEmpty(link)) return;
        new FacebookHelper(activity).shareContentToFacebookLuckyWheel(activity, activity.getCallbackManager(), link,
                null, null, null, serviceType);
    }

}

package com.metfone.selfcare.common.api.video.callback;

import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Category;

import java.util.ArrayList;

/**
 * Created by tuanha00 on 3/7/2018.
 */

public interface OnCategoryCallback {
    void onGetCategoriesSuccess(ArrayList<Category> list, ArrayList<AdsRegisterVip> listAds);

    void onGetCategoriesError(String s);

    void onGetCategoriesComplete();
}

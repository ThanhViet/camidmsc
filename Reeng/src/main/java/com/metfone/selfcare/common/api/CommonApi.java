package com.metfone.selfcare.common.api;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.DataPackagesResponse;
import com.metfone.selfcare.model.LocationObject;
import com.metfone.selfcare.model.home.AdsMainTab;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.stringee.StringeeCall.ENABLE_UPLOAD_CALL_REPORT;

/**
 * Created by thanhnt72 on 4/1/2019.
 */

public class CommonApi extends BaseApi {

    public static final String TAG_GET_LIST_MEDIA_BOX = "LIST_MEDIA_BOX";
    public static final String TAG_UPLOG_REPORT_CALL = "TAG_UPLOG_REPORT_CALL";
    private static final String BUY_MOCHA_VIDEO = "/ReengBackendBiz/fakemo/send/v4";
    private final String TAG = "CommonApi";

    private static CommonApi mInstance;

    public CommonApi() {
        super(ApplicationController.self());
    }

    public static CommonApi getInstance() {
        if (mInstance == null) mInstance = new CommonApi();
        return mInstance;
    }

    public void getAdsMainTab() {
        if (isLoadingAdsMainTab) return;
        isLoadingAdsMainTab = true;
        String domain = getDomainFile();

        String timestamp = String.valueOf(System.currentTimeMillis());
        String msisdn = getReengAccountBusiness().getJidNumber();
        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;
        String gender = "";
        String birthday = "0";
        if (getReengAccount() != null) {
            gender = String.valueOf(getReengAccount().getGender());
            birthday = getReengAccount().getBirthday();
        }
        if (birthday == null) birthday = "";
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String countryCode = getReengAccountBusiness().getRegionCode();
        String languageCode = getReengAccountBusiness().getCurrentLanguage();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, msisdn + domainParam
                + vip + gender + birthday + clientType + revision + countryCode + languageCode
                + getReengAccountBusiness().getToken() + timestamp, getReengAccountBusiness().getToken());

        Http.Builder builder = get(domain, Url.File.ADS_GET_MAIN_TAB_POPUP);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("domain", domainParam);
        builder.putParameter("vip", vip);
        builder.putParameter("gender", gender);
        builder.putParameter("birthday", birthday);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("countryCode", countryCode);
        builder.putParameter("languageCode", languageCode);
        builder.putParameter("security", security);
        builder.putParameter("timestamp", timestamp);
        builder.withCallBack(new HttpCallBack() {
                                 @Override
                                 public void onSuccess(String data) throws Exception {
                                     SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LAST_TIME_GET_ADS_MAIN_TAB_DATA, System.currentTimeMillis());
                                     SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_LIST_ADS_MAIN_TAB);
                                     application.setLoadedAdsMainTab(true);
                                     JSONObject jsonObject = new JSONObject(data);
                                     JSONObject jsonData = jsonObject.optJSONObject("data");
                                     if (jsonData != null) {
                                         ArrayList<AdsMainTab> list = gson.fromJson(jsonData.optString("listAdsMaintab"), new TypeToken<ArrayList<AdsMainTab>>() {
                                         }.getType());
                                         Log.d(TAG, "getAdsMainTab: " + list);
                                         SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LIST_ADS_MAIN_TAB, list);
                                     }
                                 }

                                 @Override
                                 public void onFailure(String message) {

                                 }

                                 @Override
                                 public void onCompleted() {
                                     isLoadingAdsMainTab = false;
                                 }
                             }
        );
        builder.execute();
    }

    private void sendMyLocation(final LocationObject locationObject) {
        if (application == null) return;
        Log.i(TAG, "sendMyLocation");
        String url = UrlConfigHelper.getInstance(application).getUrlConfigOfFile(Config.UrlEnum.POST_LOCATION);
        long timestamp = System.currentTimeMillis();
        String msisdn = getReengAccountBusiness().getJidNumber();
        String token = getReengAccountBusiness().getToken();
        //msisdn+ country_code, + country_name + city + postal + latitude + longitude+ ipv4 + state+ token+timestamp
        String createAt = TimeHelper.formatTimeLocation(timestamp);
        StringBuilder sb = new StringBuilder();
        sb.append(msisdn)
                .append(locationObject.getCountryCode())
                .append(locationObject.getCountryName())
                .append(locationObject.getCity())
                .append(locationObject.getPostal())
                .append(locationObject.getLatitude())
                .append(locationObject.getLongitude())
                .append(locationObject.getIPv4())
                .append(locationObject.getState())
                .append(createAt)
                .append(Constants.HTTP.CLIENT_TYPE_STRING)
                .append(Config.REVISION)
                .append(token)
                .append(timestamp);
        String security = HttpHelper.encryptDataV2(application, sb.toString(), token);
        Http.Builder builder = post(url);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("country_code", locationObject.getCountryCode());
        builder.putParameter("country_name", locationObject.getCountryName());
        builder.putParameter("city", locationObject.getCity());
        builder.putParameter("postal", locationObject.getPostal());
        builder.putParameter("latitude", locationObject.getLatitude());
        builder.putParameter("longitude", locationObject.getLongitude());
        builder.putParameter("IPv4", locationObject.getIPv4());
        builder.putParameter("state", locationObject.getState());
        builder.putParameter("created_at", createAt);
        builder.putParameter("clientType", "Android");
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("timestamp", String.valueOf(timestamp));
        builder.putParameter(Constants.HTTP.DATA_SECURITY, security);
        builder.withCallBack(new HttpCallBack() {
                                 @Override
                                 public void onSuccess(String data) throws Exception {

                                 }

                                 @Override
                                 public void onFailure(String message) {

                                 }

                                 @Override
                                 public void onCompleted() {

                                 }
                             }
        );
        builder.execute();
    }

    private boolean isLoadingLocation = false;
    private boolean isLoadingAdsMainTab = false;

    public void setLoadingLocation(boolean loadingLocation) {
        isLoadingLocation = loadingLocation;
    }

    public void setLoadingAdsMainTab(boolean loadingAdsMainTab) {
        isLoadingAdsMainTab = loadingAdsMainTab;
    }

    public void getMyLocation() {
        //if (application.getReengAccountBusiness().isAnonymousLogin()) return;
        if (application == null) return;
        Log.i(TAG, "getMyLocation isLoadingLocation: " + isLoadingLocation);
        if (isLoadingLocation) return;
        String url = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.API_LOCATION_GET);
        if (Utilities.notEmpty(url)) {
            isLoadingLocation = true;
            Http.Builder builder = get(url);
            builder.putParameter("clientType", "mocha_app");
            builder.withCallBack(new HttpCallBack() {
                                     @Override
                                     public void onSuccess(String data) throws Exception {
                                         SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_LOCATION_CLIENT_IP);
                                         SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_LOCATION_CLIENT_COUNTRY_CODE);
                                         try {
                                             if (Utilities.notEmpty(data)) {
                                                 String xxteaKey = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.SECURE_LOCATION_GET);
                                                 String tmpDecrypt = XXTEACrypt.decryptBase64StringToString(data, xxteaKey);
                                                 Log.d(TAG, "getMyLocation tmpDecrypt: " + tmpDecrypt);
                                                 JSONObject jsonObject = new JSONObject(tmpDecrypt);
                                                 JSONObject jsonData = jsonObject.optJSONObject("data");
                                                 if (jsonData != null) {
                                                     String ipAddress = jsonData.optString("ipAddress");
                                                     String countryCode = jsonData.optString("countryCode");
                                                     String countryName = jsonData.optString("countryName");
                                                     SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LOCATION_CLIENT_IP, ipAddress);
                                                     SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_LOCATION_CLIENT_COUNTRY_CODE, countryCode);
                                                     LocationObject location = new LocationObject();
                                                     location.setCountryCode(countryCode);
                                                     location.setCountryName(countryName);
                                                     location.setIPv4(ipAddress);
                                                     sendMyLocation(location);
                                                 }
                                             }
                                         } catch (Exception e) {
                                             Log.e(TAG, e);
                                         }
                                     }

                                     @Override
                                     public void onFailure(String message) {
                                         if (NetworkHelper.isConnectInternet(application)) {
                                             SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_LOCATION_CLIENT_IP);
                                             SharedPrefs.getInstance().remove(Constants.PREFERENCE.PREF_LOCATION_CLIENT_COUNTRY_CODE);
                                         }
                                     }

                                     @Override
                                     public void onCompleted() {
                                         isLoadingLocation = false;
                                     }
                                 }
            );
            builder.execute();
        } else {
            isLoadingLocation = false;
        }
    }

    public static class CALLTYPE {
        public static final String CALL_DATA = "calldata";
        public static final String CALL_OUT = "callout";
        public static final String CALL_VIDEO = "callvideo";
        public static final String CALL_IN = "callin";
        public static final String CALL_FS = "callfs";
    }

    public void uploadCallReport(String logcall, String caller, String callee, String session,
                                 boolean isCaller, String callType) {
        if (!ENABLE_UPLOAD_CALL_REPORT) return;

        if (TextUtils.isEmpty(logcall)) return;
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(application.getReengAccountBusiness().getJidNumber())
                .append(caller)
                .append(callee)
                .append(session)
                .append(Constants.HTTP.CLIENT_TYPE_STRING)
                .append(isCaller)
                .append(callType)
                .append(logcall)
                .append(Config.REVISION)
                .append(application.getReengAccountBusiness().getToken())
                .append(timeStamp);

        post(getDomainFile(), Url.File.API_UPLOAD_CALL_REPORT)
                .putParameter("msisdn", application.getReengAccountBusiness().getJidNumber())
                .putParameter("caller", caller)
                .putParameter("callee", callee)
                .putParameter("session", session)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("is_caller", String.valueOf(isCaller))
                .putParameter("call_type", callType)
                .putParameter("data", logcall)
                .putParameter("revision", Config.REVISION)
                .putParameter("security", HttpHelper.encryptDataV2(application, sb.toString(), application.getReengAccountBusiness().getToken()))
                .putParameter("timestamp", timeStamp)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String response) throws Exception {
                        Log.d("uploadCallReport", response);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.d("uploadCallReport", message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                    }

                })
                .setTag(TAG_UPLOG_REPORT_CALL)
                .execute();
    }

    public void getListDataPackages(final ApiCallbackV2<ArrayList<DataPackagesResponse.ListDataPackages>> apiCallback) {
        String timestamp = String.valueOf(System.currentTimeMillis());
        String msisdn = getReengAccountBusiness().getJidNumber();
        String clientType = Constants.HTTP.CLIENT_TYPE_STRING;
        String revision = Config.REVISION;
        String countryCode = getReengAccountBusiness().getRegionCode();
        String languageCode = getReengAccountBusiness().getCurrentLanguage();
        String operator = getReengAccountBusiness().getOperator();
        String security = HttpHelper.encryptDataV2(application, msisdn + operator
                + clientType + revision + languageCode + countryCode
                + getReengAccountBusiness().getToken() + timestamp, getReengAccountBusiness().getToken());

        String url = getDomainFile() + Url.File.GET_LIST_DATA_PACKAGES;
        Http.Builder builder = get(url);
        builder.putParameter("msisdn", msisdn);
        builder.putParameter("currOperator", operator);
        builder.putParameter("clientType", clientType);
        builder.putParameter("revision", revision);
        builder.putParameter("languageCode", languageCode);
        builder.putParameter("countryCode", countryCode);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("security", security);
        builder.putHeader("security", security);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                DataPackagesResponse response = gson.fromJson(data, DataPackagesResponse.class);
                if (apiCallback != null)
                    apiCallback.onSuccess(response.getLabelExtend(), response.getResult());
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) apiCallback.onError(message);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.execute();
    }

    public void getListBuyMochaVideo(HttpCallBack httpCallBack, String channel, String cmd, String id){
        ReengAccount myAccount = application.getReengAccountBusiness().getCurrentAccount();
        String timeStamp = System.currentTimeMillis() + "";
        String countryCode = getReengAccountBusiness().getRegionCode();
        String languageCode = getReengAccountBusiness().getCurrentLanguage();
        //todo token, timestamp luon co
        StringBuilder sb = new StringBuilder().
                append(myAccount.getJidNumber()).
                append(cmd).
                append(channel).
                append(Constants.HTTP.CLIENT_TYPE_STRING).
                append(Config.REVISION).
                append(countryCode).
                append(languageCode).
                append(myAccount.getToken()).
                append(timeStamp);
        post(getDomainFile(),BUY_MOCHA_VIDEO)
                .withCallBack(httpCallBack)
                .putParameter("id", id)
                .putParameter(Constants.HTTP.REST_MSISDN, myAccount.getJidNumber())
                .putParameter("cmd", cmd)
                .putParameter("channel", channel)
                .putParameter("clientType", "Android")
                .putParameter("revision", Config.REVISION)
                .putParameter("countryCode", countryCode)
                .putParameter("languageCode", languageCode)
                .putParameter("timestamp", timeStamp)
                .putParameter(Constants.HTTP.DATA_SECURITY, HttpHelper.encryptDataV2(application, sb.toString(), myAccount.getToken()))
                .execute();
    }
}

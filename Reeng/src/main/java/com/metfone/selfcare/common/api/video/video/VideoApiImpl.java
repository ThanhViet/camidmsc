package com.metfone.selfcare.common.api.video.video;

import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.ApiCallback;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.http.HttpProgressCallBack;
import com.metfone.selfcare.common.api.video.callback.OnCategoryCallback;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.VideoConstant;
import com.metfone.selfcare.database.datasource.VideoDataSource;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.SearchHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.AdSense;
import com.metfone.selfcare.model.tab_video.AdsRegisterVip;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.ChannelStatistical;
import com.metfone.selfcare.model.tab_video.FilmGroup;
import com.metfone.selfcare.model.tab_video.Resolution;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.model.tab_video.VideoBannerItem;
import com.metfone.selfcare.model.tab_video.VideoRevenue;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail.MovieDetailPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement.ChannelManagementPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelPresenterImpl;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by tuanha00 on 3/7/2018.
 * lấy dữ liệu video
 */

public class VideoApiImpl extends BaseApi implements VideoApi {
    private static final String API_UPDATE_VIDEO = "/serviceapi/upload/uploadVideo";
    private static final String TAG = "VideoApiImpl";

    public VideoApiImpl(ApplicationController application) {
        super(application);
    }

    public static Video convertStringToVideoInfo(@NonNull JSONObject jsonObject) {
        Video video = new Video();
        video.setId(jsonObject.optString("id"));
        video.setTitle(jsonObject.optString("name"));
        video.setDescription(jsonObject.optString("description"));
        video.setSourceName(jsonObject.optString("SourceName"));
        int durationInt = jsonObject.optInt("durationS");
        if (durationInt > 0)
            video.setDuration(DateTimeUtils.getDuration(durationInt));
        else
            video.setDuration(jsonObject.optString("duration"));
        String originalPath = jsonObject.optString("original_path");

        video.setOriginalPath(originalPath);
        video.setLink(jsonObject.optString("link"));
        video.setSourceType(jsonObject.optString("type"));
        video.setAspectRatio(jsonObject.optDouble("aspecRatio"));
        video.setImagePath(jsonObject.optString("image_path"));
        video.setImage_path_small(jsonObject.optString("image_path_small"));
        video.setImage_path_thump(jsonObject.optString("image_path_thump"));
        video.setTotalLike(jsonObject.optInt("total_like"));
        video.setTotalComment(jsonObject.optInt("total_comment"));
        video.setTotalShare(jsonObject.optInt("total_share"));
        video.setTotalView(jsonObject.optInt("total_view"));
        if (video.getTotalView() == 0) {
            video.setTotalView(jsonObject.optInt("isView"));
        }
        video.setLike(jsonObject.optInt("is_like") != 0);
        video.setShare(jsonObject.optInt("is_share") != 0);
        video.setItemStatus(jsonObject.optInt("itemStatus"));
        video.setVideoType(jsonObject.optString("videoType"));
        video.setChapter(jsonObject.optString("chapter"));
        video.setUrlAds(jsonObject.optString("url_ADS"));
        video.setUrlAdsEnd(jsonObject.optString("url_ADS_End"));
        video.setCampaign(jsonObject.optString("campaign"));
        video.setIsPrivate(jsonObject.optInt("isPrivate"));
        video.setRecommendType(jsonObject.optString("recommendType"));
        video.setLive(jsonObject.optInt("isLive") == 1);
        String filmGroupsID = jsonObject.optString("filmGroupsID");
        Gson gson = ApplicationController.self().getGson();
        FilmGroup filmGroup = gson.fromJson(jsonObject.optString("filmGroup"), FilmGroup.class);
        if (filmGroup == null && Utilities.notEmpty(filmGroupsID)) {
            filmGroup = new FilmGroup();
            filmGroup.setGroupId(filmGroupsID);
        }
        video.setFilmGroup(filmGroup);
        ArrayList<Channel> channels = convertStringToChannelArrayListBase(jsonObject.optString("channels"));
        if (Utilities.notEmpty(channels) && channels.get(0) != null)
            video.setChannel(channels.get(0));
        ArrayList<Resolution> resolutions = gson.fromJson(jsonObject.optString(Parameter.Video.LIST_RESOLUTION), new TypeToken<ArrayList<Resolution>>() {
        }.getType());
        if (Utilities.notEmpty(resolutions)) {
            Resources resources = ApplicationController.self().getResources();
            Resolution autoResolution = new Resolution(resources.getString(R.string.auto_speed_video), resources.getString(R.string.auto_speed_video), video.getOriginalPath());
            resolutions.add(0, autoResolution);
            video.setListResolution(resolutions);
        } else if (video.isLive()) {
            resolutions = new ArrayList<>();
            Resources resources = ApplicationController.self().getResources();
            Resolution autoResolution = new Resolution(resources.getString(R.string.auto_speed_video), resources.getString(R.string.auto_speed_video), video.getOriginalPath());
            resolutions.add(0, autoResolution);
            video.setListResolution(resolutions);
        }
        ArrayList<AdSense> listAds = gson.fromJson(jsonObject.optString(Parameter.Video.LIST_ADSENSE), new TypeToken<ArrayList<AdSense>>() {
        }.getType());
        video.setListAds(listAds);
        video.setShowAds(jsonObject.optInt("showAds"));
        video.setLogo(jsonObject.optString("logo"));
        video.setLogoPosition(jsonObject.optInt("logo_position"));
        video.setQueryRecommendation(jsonObject.optString("QuerryRecommendation"));
        video.setStartLiveTime(jsonObject.optLong("startLiveTime"));
        video.setEndLiveTime(jsonObject.optLong("endLiveTime"));
        ArrayList<Category> categories = convertStringToCategoryArrayListBase(jsonObject.optString("categories"));
        if (Utilities.notEmpty(categories) && categories.get(0) != null) {
            video.setCategory(categories.get(0));
        }
        video.setPublishTime(jsonObject.optLong("publish_time"));
        if (video.getPublishTime() == 0) {
            video.setPublishTime(jsonObject.optLong("DatePublish"));
        }
//        video.setSubTitleUrl(jsonObject.optString("subtitles_url"));
        return video;
    }

    public static ArrayList<Channel> convertStringToChannelArrayListBase(String data) {
        ArrayList<Channel> channels = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                channels.add(convertStringToChannelInfoBase(jsonArray.getString(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return channels;
    }

    public static Channel convertStringToChannelInfoBase(String data) {
        Channel channel = new Channel();
        try {
            JSONObject jsonObject = new JSONObject(data);
            channel.setId(jsonObject.optString("id"));

            String name = jsonObject.optString("name");
            if (Utilities.isEmpty(name))
                name = jsonObject.optString("categoryname");
            channel.setName(name);

            String image = jsonObject.optString("url_images");
            if (Utilities.isEmpty(image))
                image = jsonObject.optString("url_avatar");
            channel.setUrlImage(image);

            channel.setUrlImageCover(jsonObject.optString("url_images_cover"));
            channel.setDescription(jsonObject.optString("description"));
            channel.setFollow(jsonObject.optInt("is_follow") == 1);
            channel.setPackageAndroid(jsonObject.optString("package_android"));
            channel.setNumFollow(jsonObject.optInt("numfollow"));
            channel.setNumVideo(jsonObject.optInt("numVideo"));
            channel.setTypeChannel(jsonObject.optInt("type"));
            channel.setIsOfficial(jsonObject.optInt("isOfficial"));
            channel.setLastPublishVideo(jsonObject.optLong("lastPublishVideo"));
            channel.setTextTotalPoint(jsonObject.optString("totalPoint"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return channel;
    }

    public static ArrayList<Video> convertStringToVideoArrayList(String data) {
        ArrayList<Video> videos = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.optJSONArray(RESULT);
            if (jsonArray != null) {
                String lastId = jsonObject.optString(LAST_ID_STR);
                videos.addAll(convertJSONArrayToVideos(jsonArray, lastId));
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }

        return videos;
    }

    public static ArrayList<Video> convertJSONArrayToVideos(JSONArray jsonArray, String lastId) {
        ArrayList<Video> videos = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            if (jsonArray.optJSONObject(i) != null) {
                try {
                    Video video = convertStringToVideoInfo(jsonArray.optJSONObject(i));
                    video.setLastId(lastId);
                    videos.add(video);
                } catch (Exception e) {
                    Log.e(TAG, e);
                }
            }
        }
        return videos;
    }

    public static ArrayList<Category> convertStringToCategoryArrayListBase(String data) {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                if (jsonObject != null) categories.add(convertStringToCategoryInfo(jsonObject));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categories;
    }

    private static ArrayList<Category> convertStringToCategoryArrayList(String data) {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(data).optJSONArray("result");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                if (jsonObject != null) categories.add(convertStringToCategoryInfo(jsonObject));
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }

        return categories;
    }

    private static Category convertStringToCategoryInfo(@NonNull JSONObject jsonObject) {
        Category category = new Category();
        category.setId(jsonObject.optString("categoryid"));
        category.setName(jsonObject.optString("categoryname"));
        category.setContentType(jsonObject.optString("contentType"));
        category.setUrlImage(jsonObject.optString("url_images"));
        return category;
    }

    private static ArrayList<AdsRegisterVip> convertStringToAdsRegisterVipArrayList(String data) {
        ArrayList<AdsRegisterVip> adsRegisterVips = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            if (jsonObject.has("listAds")) {
                JSONArray jsonArray = jsonObject.getJSONArray("listAds");
                for (int i = 0; i < jsonArray.length(); i++) {
                    adsRegisterVips.add(convertStringToAdsRegisterVip(jsonArray.getJSONObject(i)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return adsRegisterVips;
    }

    private static AdsRegisterVip convertStringToAdsRegisterVip(JSONObject jsonObject) {
        AdsRegisterVip adsRegisterVip = new AdsRegisterVip();
        adsRegisterVip.setCategoryId(jsonObject.optString("categoryId"));
        adsRegisterVip.setTitle(jsonObject.optString("title"));
        adsRegisterVip.setContent(jsonObject.optString("content"));
        adsRegisterVip.setConfirmString(jsonObject.optString("confirmString"));
        adsRegisterVip.setCommand(jsonObject.optString("command"));
        adsRegisterVip.setSMS(jsonObject.optInt("isSMS"));
        adsRegisterVip.setSmsCommand(jsonObject.optString("smsCommand"));
        adsRegisterVip.setSmsCodes(jsonObject.optString("smsCodes"));
        adsRegisterVip.setTitleButton(jsonObject.optString("titleButton"));
        adsRegisterVip.setNumberShowDialog(jsonObject.optInt("numClick"));

        adsRegisterVip.setDeeplink(jsonObject.optString("deeplink"));

        return adsRegisterVip;
    }

    @Override
    public void getVolumeFilmGroups(String groupId, final ApiCallback apiCallback) {
        long time = System.currentTimeMillis();
        String timeStamp = String.valueOf(time);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + vip + groupId + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_VOLUME_FILM_GROUPS)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.User.VIP, vip)
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getNetworkSubType(application))
                .putParameter(Parameter.FILM_GROUP_ID, groupId)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);
                        ArrayList<Video> videos = convertStringToVideoArrayList(jsonObject.toString());
                        if (apiCallback instanceof MovieDetailPresenterImpl.VolumeFilmGroupsCallback)
                            ((MovieDetailPresenterImpl.VolumeFilmGroupsCallback) apiCallback).onSuccess(videos);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null)
                            apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null)
                            apiCallback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getMovieInfo(final Video video, final ApiCallbackV2<Video> apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        // md5 =msisdn + url + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + video.getLink() + "mocha"
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());
        get(getDomainMochaVideo(), VideoApi.GET_DETAIL_URL_V2)
                .setTag("getMovieInfo")
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.URL, video.getLink())
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .putParameter("type", "mocha")
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONArray jsonArray = new JSONObject(data).optJSONArray("listContentTimeline");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            JSONObject itemObject = jsonArray.getJSONObject(0);
                            int isLike = itemObject.optInt("isLike");
                            int isShare = itemObject.optInt("isShare");
                            JSONObject contentObject = itemObject.optJSONObject("content");
                            if (contentObject != null) {
                                long totalLike = contentObject.optLong("like");
                                long totalShare = contentObject.optLong("share");
                                long totalComment = contentObject.optLong("comment");
                                video.setLike(isLike == 1);
                                video.setShare(isShare == 1);
                                video.setTotalLike(totalLike);
                                video.setTotalShare(totalShare);
                                video.setTotalComment(totalComment);
                                if (apiCallback != null) apiCallback.onSuccess("", video);
                            }
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) apiCallback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getVideoNewPublish(final String lastId, int offsetInt, final ApiCallback apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String offset = String.valueOf(offsetInt);
        String limit = String.valueOf(20);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        // msisdn + domain + limit + offset + lastId + clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + limit + offset + lastId + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_VIDEO_NEW_PUBLISH)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(LAST_ID_STR, lastId)
                .putParameter(Parameter.LIMIT, limit)
                .putParameter(Parameter.OFFSET, offset)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);
                        String lastIdStr = jsonObject.optString(LAST_ID_STR);
                        ArrayList<Video> videos = convertStringToVideoArrayList(jsonObject.toString());
                        if (apiCallback instanceof SubscribeChannelPresenterImpl.VideoNewPublishCallback)
                            ((SubscribeChannelPresenterImpl.VideoNewPublishCallback) apiCallback).onSuccess(lastIdStr, videos);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null)
                            apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null)
                            apiCallback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getChannelUserFollow(int offsetInt, final ApiCallback apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String offset = String.valueOf(offsetInt);
        String limit = String.valueOf(20);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        ReengAccount account = getReengAccount();
        // msisdn + domain + limit + offset + clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + domainParam
                + limit
                + offset
                + Constants.HTTP.CLIENT_TYPE_STRING
                + Config.REVISION
                + account.getToken()
                + timeStamp, account.getToken());

        get(domain, VideoApi.GET_CHANNEL_USER_FOLLOW)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.LIMIT, limit)
                .putParameter(Parameter.OFFSET, offset)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);
                        String lastIdStr = jsonObject.optString(LAST_ID_STR);
                        ArrayList<Channel> channels = convertStringToChannelArrayListBase(jsonObject.optString(RESULT));/*gson.fromJson(jsonObject.optString(RESULT), new TypeToken<ArrayList<Channel>>(){}.getType());*/
                        if (apiCallback instanceof SubscribeChannelPresenterImpl.ChannelUserFollowCallback)
                            ((SubscribeChannelPresenterImpl.ChannelUserFollowCallback) apiCallback).onSuccess(channels);
                        if (apiCallback instanceof ChannelManagementPresenterImpl.ChannelUserFollowCallback)
                            ((ChannelManagementPresenterImpl.ChannelUserFollowCallback) apiCallback).onSuccess(lastIdStr, channels);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null)
                            apiCallback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null)
                            apiCallback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getCategoriesV2(@Nullable final ApiCallbackV2<ArrayList<Object>> callback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        ReengAccountBusiness reengAccountBusiness = getReengAccountBusiness();
        if (reengAccountBusiness == null || !reengAccountBusiness.isValidAccount()) return;
        ReengAccount account = getReengAccount();

        String security = HttpHelper.encryptDataV2(application, account.getJidNumber()
                + domainParam + account.getToken() + timeStamp, account.getToken());

        get(domain, VideoApi.GET_CATEGORY)
                .putParameter(Parameter.User.MSISDN, account.getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.GENDER, account.getGender() + "")
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        try {
                            ArrayList<Object> items = new ArrayList<>();
                            ArrayList<AdsRegisterVip> adsRegisterList = convertStringToAdsRegisterVipArrayList(data);
                            ArrayList<Category> categoriesList = convertStringToCategoryArrayList(data);
                            items.add(adsRegisterList);
                            items.add(categoriesList);

                            if (Utilities.isEmpty(adsRegisterList))
                                SharedPrefs.getInstance().remove(Constants.TabVideo.CACHE_ADS_REGISTER_VIPS);
                            else
                                SharedPrefs.getInstance().put(Constants.TabVideo.CACHE_ADS_REGISTER_VIPS, adsRegisterList);

                            if (Utilities.isEmpty(categoriesList))
                                SharedPrefs.getInstance().remove(Constants.TabVideo.CACHE_CATEGORIES);
                            else
                                SharedPrefs.getInstance().put(Constants.TabVideo.CACHE_CATEGORIES, categoriesList);

                            if (callback != null) {
                                callback.onSuccess("", items);
                            }
                            LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                        } catch (Exception e) {
                            if (callback != null) {
                                callback.onError(application.getResources().getString(R.string.e601_error_but_undefined));
                            }
                            LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        if (callback != null) {
                            callback.onError(message);
                        }
                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null) {
                            callback.onComplete();
                        }
                    }
                })
                .execute();
    }

    @Override
    public void getCategoryUpload(final OnCategoryCallback onCategoryCallback) {
        final String timeStamp = System.currentTimeMillis() + "";
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.API_GET_VIDEO_CATEGORY_UPDATE)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter("timestamp", timeStamp)
                .putParameter("security", security)
                .putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        Log.i(TAG, "getCategoryUpload --> onSuccess: " + data);
                        if (onCategoryCallback != null)
                            onCategoryCallback.onGetCategoriesSuccess(convertStringToCategoryArrayList(data), null);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "getCategoryUpload --> onFailure: " + message);
                        if (onCategoryCallback != null)
                            onCategoryCallback.onGetCategoriesError(message);
                    }
                })
                .execute();
    }

    @Override
    public void getVideosByCategoryId(CompositeDisposable compositeDisposable, final String categoryId, int limit, int offset, String lastId, final OnVideoCallback onVideoCallback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + categoryId + limit + offset + lastId
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_VIDEO_BY_CATE)
                .setCompositeDisposable(compositeDisposable)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("categoryid", categoryId)
                .putParameter("limit", String.valueOf(limit))
                .putParameter("offset", String.valueOf(offset))
                .putParameter("timestamp", timeStamp)
                .putParameter("lastIdStr", lastId)
                .putParameter("security", security)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";
                        if (onVideoCallback != null)
                            onVideoCallback.onGetVideosSuccess(convertStringToVideoArrayList(data));
                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CATEGORY,
                                time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    }

                    @Override
                    public void onFailure(String error) {
                        super.onFailure(error);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        if (onVideoCallback != null)
                            onVideoCallback.onGetVideosError(error);

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CATEGORY,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (onVideoCallback != null)
                            onVideoCallback.onGetVideosComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getMoviesByCategoryV2(String categoryId, int offset, int limit, String lastId, @Nullable final ApiCallbackV2<ArrayList<Video>> callback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        // msisdn + domain + categoryid + limit + lastIdStr + clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + categoryId + limit + lastId + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_FILM_GROUPS)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .putParameter(Parameter.CATEGORY_ID, categoryId)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, String.valueOf(offset))
                .putParameter(LAST_ID_STR, lastId)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);

                        String lastId = jsonObject.optString(LAST_ID_STR);
                        if (callback != null)
                            callback.onSuccess(lastId, convertStringToVideoArrayList(data));
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (callback != null)
                            callback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null)
                            callback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getVideosByCategoryV2(final String categoryId, final int offset, int limit, final String lastId, @Nullable final ApiCallbackV2<ArrayList<Object>> callback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + categoryId + limit + offset + lastId
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_VIDEO_BY_CATE)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.CATEGORYID, categoryId)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, String.valueOf(offset))
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(LAST_ID_STR, lastId)
                .putParameter(Parameter.SECURITY, security)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        JSONObject jsonObject = new JSONObject(data);
                        String lastIdStr = jsonObject.optString(LAST_ID_STR);
                        ArrayList<VideoBannerItem> listBannerItems = new Gson().fromJson(jsonObject.optString(LIST_BANNER_ITEMS), new TypeToken<ArrayList<VideoBannerItem>>() {
                        }.getType());
                        ArrayList<Video> videos = convertStringToVideoArrayList(data);
                        ArrayList<Object> items = new ArrayList<>();
                        items.add(listBannerItems);
                        items.add(videos);
                        if (offset == 0) {
                            if (Utilities.isEmpty(lastIdStr))
                                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.TabVideo.CACHE_LAST_ID);
                            else
                                SharedPrefs.getInstance().put(categoryId + "_" + Constants.TabVideo.CACHE_LAST_ID, lastIdStr);

                            if (Utilities.isEmpty(listBannerItems))
                                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.TabVideo.CACHE_VIDEO_BANNER_ITEM);
                            else
                                SharedPrefs.getInstance().put(categoryId + "_" + Constants.TabVideo.CACHE_VIDEO_BANNER_ITEM, listBannerItems);

                            if (Utilities.isEmpty(videos))
                                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.TabVideo.CACHE_GET_VIDEOS_BY_CATEGORY);
                            else
                                SharedPrefs.getInstance().put(categoryId + "_" + Constants.TabVideo.CACHE_GET_VIDEOS_BY_CATEGORY, videos);
                        }
                        if (callback != null) {
                            callback.onSuccess(lastIdStr, items);
                        }

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    }

                    @Override
                    public void onFailure(String error) {
                        super.onFailure(error);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        if (callback != null) {
                            callback.onError(error);
                        }

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null) {
                            callback.onComplete();
                        }
                    }
                })
                .execute();
    }

    @Override
    public void getVideosByCategoryV2(final String categoryId, final int offset, int limit, final String lastId
            , final String type, @Nullable final ApiCallbackV2<ArrayList<Object>> callback) {
        //ở tab hot: type = 1 là box video hot, type = 2 video mới
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + categoryId + limit + offset + lastId
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_VIDEO_BY_CATE)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.CATEGORYID, categoryId)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, String.valueOf(offset))
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(LAST_ID_STR, lastId)
                .putParameter("type", type)
                .putParameter(Parameter.SECURITY, security)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";
                        JSONObject jsonObject = new JSONObject(data);
                        String title = jsonObject.optString("title");
                        ArrayList<VideoBannerItem> listBannerItems = new Gson().fromJson(jsonObject.optString(LIST_BANNER_ITEMS), new TypeToken<ArrayList<VideoBannerItem>>() {
                        }.getType());
//                        ArrayList<Video> videos = convertStringToVideoArrayList(data);
                        ArrayList<Object> items = new ArrayList<>();
                        if (listBannerItems != null && !listBannerItems.isEmpty()) {
                            items.addAll(listBannerItems);
                        }
                        items.addAll(convertStringToVideoArrayList(data));
//                        if (offset == 0) {
//                            if (Utilities.isEmpty(listBannerItems))
//                                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.TabVideo.CACHE_VIDEO_BANNER_ITEM);
//                            else
//                                SharedPrefs.getInstance().put(categoryId + "_" + Constants.TabVideo.CACHE_VIDEO_BANNER_ITEM, listBannerItems);
//
//                            if (Utilities.isEmpty(videos))
//                                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.TabVideo.CACHE_GET_VIDEOS_BY_CATEGORY);
//                            else
//                                SharedPrefs.getInstance().put(categoryId + "_" + Constants.TabVideo.CACHE_GET_VIDEOS_BY_CATEGORY, videos);
//                        }
                        if (callback != null) {
                            callback.onSuccess(title, items);
                        }

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    }

                    @Override
                    public void onFailure(String error) {
                        super.onFailure(error);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        if (callback != null) {
                            callback.onError(error);
                        }

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CATEGORY, time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null) {
                            callback.onComplete();
                        }
                    }
                })
                .execute();
    }

    @Override
    public void getVideosByChannelIdV2(String channelId, int offset, int limit, String lastId, final ApiCallbackV2<ArrayList<Video>> callback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + channelId + limit + offset + lastId
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_VIDEO_BY_CHANNEL)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.Channel.CHANNELID, channelId)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, String.valueOf(offset))
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(LAST_ID_STR, lastId)
                .putParameter(Parameter.SECURITY, security)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";
                        JSONObject jsonObject = new JSONObject(data);
                        String lastId = jsonObject.optString(LAST_ID_STR);
                        if (callback != null) {
                            callback.onSuccess(lastId, convertStringToVideoArrayList(data));
                        }
                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CHANNEL,
                                time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";
                        if (callback != null) {
                            callback.onError(message);
                        }
                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_LIST_BY_CHANNEL,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback == null) return;
                        callback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getVideoRevenuesByChannelIdV2(String channelId, String videoName, int offset, int limit, String lastId, final ApiCallbackV2<ArrayList<VideoRevenue>> callback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        //msisdn + domain + videoName + limit + offset clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + videoName + limit + offset + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_STAT_OF_VIDEO)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.Video.NAME, videoName)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, String.valueOf(offset))
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);
                        if (jsonObject.optInt("code") == 200) {
                            ChannelStatistical statistical = application.getGson().fromJson(jsonObject.optString("InfoChannel"), ChannelStatistical.class);
                            if (statistical != null) {
                                if (callback != null)
                                    callback.onSuccess("", statistical.getVideoRevenue());
                            } else {
                                onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                            }
                        } else {
                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (callback != null) callback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null) callback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getMoviesByChannelIdV2(String channelId, int offsetInt, int limit, String lastIdStr, final ApiCallbackV2<ArrayList<Video>> callback) {
        long time = System.currentTimeMillis();
        String timeStamp = String.valueOf(time);
        String offset = String.valueOf(offsetInt);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String categoryId = String.valueOf(10);

        // msisdn + domain + channelId + limit + lastIdStr + clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + categoryId + limit + lastIdStr + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_FILM_GROUPS_OF_CHANNEL)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .putParameter(Parameter.CATEGORY_ID, categoryId)
                .putParameter(Parameter.Channel.CHANNEL_ID, channelId)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, offset)
                .putParameter(LAST_ID_STR, lastIdStr)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);

                        String lastId = jsonObject.optString(LAST_ID_STR);
                        ArrayList<Video> videos = convertStringToVideoArrayList(jsonObject.toString());

                        if (callback != null) callback.onSuccess(lastId, videos);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (callback != null)
                            callback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null)
                            callback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getVideosRelationship(CompositeDisposable compositeDisposable, String titleSearch
            , String videoType, String url, String categoryId, int offset, int limit, String lastId
            , String queryRecommendation, final OnVideoCallback onGetVideoCallback) {
        final long startTime = System.currentTimeMillis();
        titleSearch = SearchHelper.convertQuery(titleSearch);
        if (TextUtils.isEmpty(lastId)) lastId = "";
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + titleSearch + limit + offset + lastId + videoType + url
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        Http.Builder builder = get(domain, VideoApi.SEARCH_VIDEO_V1);
        builder.setCompositeDisposable(compositeDisposable);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("domain", domainParam);
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("q", titleSearch);
        try {
            builder.putParameter("qRec", queryRecommendation == null ? "" : URLEncoder.encode(queryRecommendation, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "queryRecommendation: " + queryRecommendation);
        builder.putParameter("video_type", videoType);
        builder.putParameter("cateId", categoryId);
        builder.putParameter("url", url);
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("timestamp", timeStamp);
        builder.putParameter("lastIdStr", lastId);
        builder.putParameter("source", "SUGGEST");
        builder.putParameter("security", security);
        builder.putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getNetworkSubType(application));
        builder.putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP);

        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";
                ArrayList<Video> list = convertStringToVideoArrayList(response);
                if (Utilities.notEmpty(list)) {
                    int size = list.size();
                    for (int i = size - 1; i >= 0; i--) {
                        if (list.get(i) == null || list.get(i).isLive()) list.remove(i);
                    }
                }
                if (onGetVideoCallback != null)
                    onGetVideoCallback.onGetVideosSuccess(list);
                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_RELATE,
                        time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onFailure(String message) {
                long endTime = System.currentTimeMillis();
                String time = (endTime - startTime) + "";
                if (onGetVideoCallback != null) onGetVideoCallback.onGetVideosError(message);
                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_RELATE,
                        time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (onGetVideoCallback != null) onGetVideoCallback.onGetVideosComplete();
            }
        });
        builder.setTag(VideoApi.SEARCH_VIDEO_V1);
        builder.execute();
    }

    @Override
    public void likeOrUnlikeVideo(Video video) {
        FeedModelOnMedia feed = FeedModelOnMedia.convertVideoToFeedModelOnMedia(video);
        WSOnMedia rest = new WSOnMedia(application);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), video.isLike() ? FeedModelOnMedia.ActionLogApp.LIKE :
                        FeedModelOnMedia.ActionLogApp.UNLIKE, "", feed.getBase64RowId(), "", FeedModelOnMedia.ActionFrom.mochavideo,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "onErrorResponse: ");
                    }
                });
    }

    @Override
    public void shareVideo(FeedModelOnMedia feed) {
        WSOnMedia rest = new WSOnMedia(application);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), FeedModelOnMedia.ActionLogApp.SHARE, "", feed
                        .getBase64RowId(), "", FeedModelOnMedia.ActionFrom.mochavideo,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "onErrorResponse: " + volleyError.getMessage());
                    }
                });
    }

    @Override
    public void getInfoVideoFromUrl(CompositeDisposable compositeDisposable, final Video video, final OnVideoCallback onVideoCallback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + video.getLink() +
                getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_VIDEO_DETAIL_BY_URL)
                .setCompositeDisposable(compositeDisposable)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("url", video.getLink())
                .putParameter("timestamp", timeStamp)
                .putParameter("security", security)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter("recommendType", video.getRecommendType())
                .putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getNetworkSubType(application))
                .putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        JSONObject js = new JSONObject(data);
                        int code = js.optInt("code");
                        if (code == 200) {
                            JSONObject jsVideo = js.optJSONObject("video_detail");
                            Video video = null;
                            if (jsVideo != null) {
                                video = convertStringToVideoInfo(jsVideo);
                                ArrayList<Video> list = new ArrayList<>();
                                list.add(video);
                                if (onVideoCallback != null)
                                    onVideoCallback.onGetVideosSuccess(list);
                            } else {
                                if (onVideoCallback != null)
                                    onVideoCallback.onGetVideosError(application
                                            .getResources().getString(R.string.e601_error_but_undefined));
                            }
                            if (video != null && !TextUtils.isEmpty(video.getOriginalPath()))
                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL,
                                        time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                            else
                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL,
                                        time, timeStamp, Constants.LogKQI.StateKQI.VIDEO_DETAIL_NULL.getValue());
                        } else {
                            if (onVideoCallback != null)
                                onVideoCallback.onGetVideosError(application
                                        .getResources().getString(R.string.e601_error_but_undefined));

                            LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL, time, timeStamp, code + "");
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        if (onVideoCallback != null) onVideoCallback.onGetVideosError(application
                                .getResources().getString(R.string.e601_error_but_undefined));

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (onVideoCallback != null) onVideoCallback.onGetVideosComplete();
                    }
                })
                .execute();
    }

    @Override
    public void callApiLog(Video video) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_LOG_ACTION_MEDIA);
        if (!TextUtils.isEmpty(domain) && !"-".equals(domain)) {
        } else {
            domain = getDomainMochaVideo();
        }
        String device = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL + "-"
                + video.getSurfaceName() + "-" + video.isCodecNeedsSetOutputSurfaceWorkaround();
        String mediaLink = video.getStartMediaUrl();
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber() + timeStamp
                + "CDR" + video.getStateLog() + (getReengAccountBusiness().isVip() ? "VIP" : "NOVIP")
                + video.getFromSource() + "APP" + NetworkHelper.getIPAddress(true) + device
                + video.getId() + mediaLink + video.getLink() + "0" + video.getTimeLog()
                + Config.REVISION + "ANDROID" + NetworkHelper.getNetworkSubType(application) + video.getDuration()
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.API_LOG_VIEW)
                .setTag(video.getStateLog())
                .putParameter("DATE", timeStamp)
                .putParameter("CDR", "CDR")
                .putParameter("STATE", video.getStateLog())
                .putParameter("MSISDN", getReengAccountBusiness().getJidNumber())
                .putParameter("VIP", getReengAccountBusiness().isVip() ? "VIP" : "NOVIP")
                .putParameter("SOURCE", video.getFromSource())
                .putParameter("CHANNEL", "APP")
                .putParameter("IP_ADDRESS", NetworkHelper.getIPAddress(true))
                .putParameter("USER_AGENT", device)
                .putParameter("ID", video.getId())
                .putParameter("MEDIA_LINK", mediaLink)
                .putParameter("PAGE_LINK", video.getLink())
                .putParameter("PRICE", "0")
                .putParameter("VOLUME", video.getVolume() + "")
                .putParameter("TIME_LOG", video.getTimeLog())
                .putParameter("LAG_ARR", TextUtils.isEmpty(video.getLagArr()) ? "" : video.getLagArr())
                .putParameter("PLAY_ARR", TextUtils.isEmpty(video.getPlayArr()) ? "" : video.getPlayArr())
                .putParameter("BANDWIDTH_ARR", TextUtils.isEmpty(video.getBandwidthArr()) ? "" : video.getBandwidthArr())
                .putParameter("NETWORK_ARR", TextUtils.isEmpty(video.getNetworkArr()) ? "" : video.getNetworkArr())
                .putParameter("ERROR_DESC", TextUtils.isEmpty(video.getErrorDes()) ? "" : video.getErrorDes())
//                .putParameter("ADS_INFO", TextUtils.isEmpty(video.getAdsInfo()) ? "" : video.getAdsInfo())
                .putParameter("ADS_INFO", "")
                .putParameter("isVideoLive", video.isLive() ? "1" : "0")
                .putParameter("cateId", video.getCategoryId())
                .putParameter("RECOMMEND_TYPE", video.getRecommendType())
                .putParameter("REVISION", Config.REVISION)
                .putParameter("CLIENT_TYPE", "ANDROID")
                .putParameter("NETWORK_TYPE", NetworkHelper.getNetworkSubType(application))
                .putParameter("duration", video.getDuration())
                .putParameter("timestamp", timeStamp)
                .putParameter("security", security)
                .putParameter("DOMAIN", convertDomainToDomainParam(domain))
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                    }
                })
                .execute();

    }

    @Override
    public void callApiAdsLog(Video video, int adsPosition, String adsToken) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_LOG_ACTION_MEDIA);
        if (!TextUtils.isEmpty(domain) && !"-".equals(domain)) {
        } else {
            domain = getDomainMochaVideo();
        }
        String device = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL + "-"
                + video.getSurfaceName() + "-" + video.isCodecNeedsSetOutputSurfaceWorkaround();
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber() + timeStamp
                + (getReengAccountBusiness().isVip() ? "VIP" : "NOVIP") + NetworkHelper.getIPAddress(true)
                + device + video.getId() + video.getOriginalPath() + video.getLink() + Config.REVISION
                + "ANDROID" + NetworkHelper.getNetworkSubType(application) + adsPosition
                + video.getDuration() + adsToken + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.API_LOG_ADS)
                .setTag(video.getStateLog())
                .putParameter("MSISDN", getReengAccountBusiness().getJidNumber())
                .putParameter("DATE", timeStamp)
                .putParameter("VIP", getReengAccountBusiness().isVip() ? "VIP" : "NOVIP")
                .putParameter("IP_ADDRESS", NetworkHelper.getIPAddress(true))
                .putParameter("USER_AGENT", device)
                .putParameter("ID", video.getId())
                .putParameter("MEDIA_LINK", video.getOriginalPath())
                .putParameter("PAGE_LINK", video.getLink())
                .putParameter("DOMAIN", convertDomainToDomainParam(domain))
                .putParameter("REVISION", Config.REVISION)
                .putParameter("CLIENT_TYPE", "ANDROID")
                .putParameter("NETWORK_TYPE", NetworkHelper.getNetworkSubType(application))
                .putParameter("ADS_INFO", TextUtils.isEmpty(video.getAdsInfo()) ? "" : video.getAdsInfo())
                .putParameter("ADS_POSITION", adsPosition + "")
                .putParameter("ADS_TOKEN", adsToken)
                .putParameter("SOURCE", video.getFromSource())
                .putParameter("duration", video.getDuration())
                .putParameter("timestamp", timeStamp)
                .putParameter("security", security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                    }
                })
                .execute();
    }

    @Override
    public void callApiLogView(Video video) {
        String type = video.getFromSource();
        if (video.isFromOnMedia())
            type = Constants.ONMEDIA.LOG_CLICK.CLICK_ONMEDIA;

//        type = video.isFromOnMedia() ? Constants.ONMEDIA.LOG_CLICK.CLICK_ONMEDIA : Constants.ONMEDIA.LOG_CLICK.CLICK_MOCHA_VIDEO;

        new WSOnMedia(application).logClickLink(video.getLink(), type, AppStateProvider.getInstance().getCampId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "callApiLogView response: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
    }

    @Override
    public void addOrRemoveSaveVideo(Video video) {
        VideoDataSource.getInstance(application).saveOrUnSaveVideo(video);
    }

    @Override
    public boolean isSave(Video video) {
        return VideoDataSource.getInstance(application).isSave(video);
    }

    @Override
    public boolean isWatchLater(Video video) {
        return VideoDataSource.getInstance(application).isWatchLater(video);
    }

    @Override
    public void addOrRemoveWatchLater(Video video) {
        VideoDataSource.getInstance(application).addOrRemoveWatchLater(video);
    }

    @Override
    public void getVideosByType(String type, OnVideoCallback onVideoCallback) {
        if (onVideoCallback == null) return;
        ArrayList<Video> videos = VideoDataSource.getInstance(application).getAllVideoByType(type);
        onVideoCallback.onGetVideosSuccess(videos);
        onVideoCallback.onGetVideosComplete();
    }

    @Override
    public void removerVideo(String type, Video video) {
        if (VideoConstant.Type.LATER.VALUE.equals(type)) {
            video.setWatchLater(false);
            addOrRemoveWatchLater(video);
        } else {
            video.setSave(false);
            addOrRemoveSaveVideo(video);
        }
    }

    @Override
    @Deprecated
    public void uploadVideo(Video video, String categoryId, OnVideoCallback onVideoCallback) {
        uploadLinkVideo(video, categoryId, onVideoCallback);
    }

    @Override
    public Http uploadVideo(Video video, String categoryId, HttpProgressCallBack httpProgressCallBack) {
        if (Utilities.checkAndGetNewPath(video.getOriginalPath(), application.getApplicationContext(), false).equals("false")) {
            if (httpProgressCallBack != null) {
                httpProgressCallBack.onFailure(application.getString(R.string.invalid_file_path_for_upload));
            }
            return null;
        }
        String userName = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_USER);
        String password = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_PASS);
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_VIDEO_UPLOAD);
        return upload(domain, API_UPDATE_VIDEO)
                .setTimeOut(3 * 60)
                .setTag("uploadVideo")
                .putFile("uploadfile", video.getOriginalPath())
                .putParameter("username", userName)
                .putParameter("password", password)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .withCallBack(httpProgressCallBack)
                .execute();
    }

    @Override
    public void uploadVideoYoutube(final String link, String categoryId, boolean isUploadOnMedia, final OnVideoCallback onVideoCallback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + link + categoryId + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.CREATE_VIDEO_BY_LINK_YOUTUBE)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.LINK_YOUTUBE, link)
                .putParameter(Parameter.CATEGORY_ID, categoryId)
                .putParameter(Parameter.POST_ON_PROFILE, isUploadOnMedia ? "1" : "0")
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        Log.i(TAG, "onSuccess: " + data);
                        if (onVideoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("code");
                            if (code == 200) {
                                onVideoCallback.onGetVideosSuccess(new ArrayList<Video>());

                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_YOUTUBE,
                                        time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                            } else {
                                onVideoCallback.onGetVideosError(js.getString("desc"));

                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_YOUTUBE, time, timeStamp, code + "");
                            }
                            onVideoCallback.onGetVideosComplete();
                        } catch (Exception e) {
                            onVideoCallback.onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                            onVideoCallback.onGetVideosComplete();

                            LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_YOUTUBE,
                                    time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        Log.i(TAG, "onFailure: " + message);
                        if (onVideoCallback == null) return;
                        onVideoCallback.onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                        onVideoCallback.onGetVideosComplete();

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_YOUTUBE,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }
                })
                .execute();
    }

    @Deprecated
    private void uploadLinkVideo(final Video video, final String categoryId, final OnVideoCallback onVideoCallback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String userName = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_USER);
        String password = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_PASS);
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_VIDEO_UPLOAD);
        upload(domain, API_UPDATE_VIDEO)
                .putFile("uploadfile", video.getOriginalPath())
                .putParameter("username", userName)
                .putParameter("password", password)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        Log.i(TAG, "onSuccess: " + data);
                        if (onVideoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("errorCode");
                            if (code == 0) {
                                String url = js.optString("lavatar");
                                String itemvideo = js.getString("itemvideo");
                                video.setOriginalPath(url);
                                uploadVideoInfo(video, false, categoryId, itemvideo, onVideoCallback);

                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_FILE,
                                        time, time, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                            } else {
                                onVideoCallback.onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                                onVideoCallback.onGetVideosComplete();

                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_FILE, timeStamp, timeStamp, code + "");
                            }
                        } catch (Exception e) {
                            onVideoCallback.onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                            onVideoCallback.onGetVideosComplete();

                            LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_FILE, time,
                                    timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        Log.i(TAG, "onFailure: " + message);
                        if (onVideoCallback == null) return;
                        onVideoCallback.onGetVideosError(message);
                        onVideoCallback.onGetVideosComplete();

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_FILE,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                    }
                })
                .execute();
    }

    @Override
    public void uploadVideoInfo(Video video, boolean isUploadOnMedia, String categoryId, String itemvideo, final OnVideoCallback onVideoCallback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + video.getTitle() + video.getDescription() + "imagelead" + itemvideo
                + categoryId + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.CREATE_VIDEO_UPLOAD_BY_USER)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter("videoTitle", video.getTitle())
                .putParameter("videoDesc", video.getDescription())
                .putParameter("imagelead", "imagelead")
                .putParameter(Parameter.POST_ON_PROFILE, isUploadOnMedia ? "1" : "0")
                .putParameter("itemvideo", itemvideo)
                .putParameter(Parameter.CATEGORY_ID, categoryId)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        Log.i(TAG, "onSuccess: " + data);
                        if (onVideoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("code");
                            if (code == 200) {
                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_INFO,
                                        time, timeStamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                                onVideoCallback.onGetVideosSuccess(new ArrayList<Video>());
                            } else {
                                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_INFO, time, timeStamp, code + "");
                                onVideoCallback.onGetVideosError(js.getString("desc"));
                            }
                            onVideoCallback.onGetVideosComplete();
                        } catch (Exception e) {
                            LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_INFO,
                                    time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                            onVideoCallback.onGetVideosError(application.getResources().getString(R.string.e601_error_but_undefined));
                            onVideoCallback.onGetVideosComplete();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);

                        long endTime = System.currentTimeMillis();
                        String time = (endTime - startTime) + "";

                        if (onVideoCallback == null) return;
                        onVideoCallback.onGetVideosError(message);
                        onVideoCallback.onGetVideosComplete();

                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_UPLOAD_INFO,
                                time, timeStamp, Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue());
                    }
                })
                .execute();
    }

    /**
     * thực hiện report video
     *
     * @param videoId   videoId
     * @param videoName videoName
     * @param videoLink videoLink
     * @param videoPath videoPath
     * @param reportId  reportId
     * @param reason    reason
     */
    @Override
    public void reportVideo(String videoId, String videoName, String videoLink, String videoPath, String reportId, String reason) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String userName = "";
        if (getReengAccount() != null) userName = getReengAccount().getName();
        /*
         * msisdn + videoId + videoName + videoLink + reportId + reason + token + timestamp
         */
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + videoId + videoName + videoLink + reportId + reason + domainParam
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.REPORT_VIDEO)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.User.NAME, userName)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? "VIP" : "NOVIP")// chỗ này cần cho vào constant
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getTextTypeConnectionForLog(application))
                .putParameter(Parameter.Video.ID, videoId)
                .putParameter(Parameter.Video.NAME, videoName)
                .putParameter(Parameter.Video.LINK, videoLink)
                .putParameter(Parameter.Video.PATH, videoPath)
                .putParameter(Parameter.REPORT_ID, reportId)
                .putParameter(Parameter.REASON, reason)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .execute();
    }

    @Override
    public void getFilmGroupsRelated(String filmGroupID, @Nullable final ApiCallbackV2<ArrayList<Object>> apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String vip = getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP;
//        msisdn + domain + vip + filmGroupID + clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + vip + filmGroupID + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_FILM_GROUPS_RELATED)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.User.VIP, vip)
                .putParameter("filmGroupID", filmGroupID)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ArrayList<Object> resultsObject = new ArrayList<>();
                        JSONObject jsonObject = new JSONObject(data);
                        ArrayList<Video> videoRelated = convertJSONArrayToVideos(jsonObject.optJSONArray("videoRelated"), "");
                        ArrayList<Video> filmRelated = convertJSONArrayToVideos(jsonObject.optJSONArray("filmRelated"), "");

                        resultsObject.add(videoRelated);
                        resultsObject.add(filmRelated);

                        if (apiCallback != null) {
                            apiCallback.onSuccess("", resultsObject);
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (apiCallback != null) {
                            apiCallback.onError(message);
                        }
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (apiCallback != null) {
                            apiCallback.onComplete();
                        }
                    }
                })
                .execute();
    }

    @Override
    public void likeOrUnlikeMovie(Movie model) {
        FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia(model);
        WSOnMedia rest = new WSOnMedia(application);
        rest.logAppV6(feed.getFeedContent().getUrl(), "", feed.getFeedContent(), model.isLike() ? FeedModelOnMedia.ActionLogApp.LIKE :
                        FeedModelOnMedia.ActionLogApp.UNLIKE, "", feed.getBase64RowId(), "", FeedModelOnMedia.ActionFrom.mochavideo,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "onErrorResponse: ");
                    }
                });
    }

    @Override
    public void getHotChannel(final boolean isTabHome, final String categoryId, int offset, final int limit, final ApiCallbackV2<ArrayList<Channel>> apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        // msisdn + domain + limit + offset + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + limit + offset
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());
        Http.Builder builder = get(domain, VideoApi.GET_LIST_HOT_CHANNEL);
        builder.putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber());
        builder.putParameter(Parameter.Http.DOMAIN, domainParam);
        builder.putParameter(Parameter.LIMIT, String.valueOf(limit));
        builder.putParameter(Parameter.OFFSET, String.valueOf(offset));
        builder.putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter(Parameter.REVISION, Config.REVISION);
        builder.putParameter(Parameter.TIMESTAMP, timeStamp);
        builder.putParameter(Parameter.SECURITY, security);
        builder.putParameter("categoryid", categoryId);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                JSONObject jsonObjectData = jsonObject.optJSONObject("data");
                String title = jsonObject.optString("title");
                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.PREFERENCE.PREF_LAST_TIME_GET_TOP_CHANNEL_BY_CATEGORY);
                SharedPrefs.getInstance().remove(categoryId + "_" + Constants.TabVideo.CACHE_GET_TOP_CHANNEL_BY_CATEGORY);
                if (jsonObjectData != null) {
                    ArrayList<Channel> list = convertStringToChannelArrayListBase(jsonObjectData.optString("listHotChannel"));
                    if (isTabHome && list != null) {
                        SharedPrefs.getInstance().put(categoryId + "_" + Constants.TabVideo.CACHE_GET_TOP_CHANNEL_BY_CATEGORY, list);
                        SharedPrefs.getInstance().put(categoryId + "_" + Constants.PREFERENCE.PREF_LAST_TIME_GET_TOP_CHANNEL_BY_CATEGORY, System.currentTimeMillis());
                    }
                    if (apiCallback != null) apiCallback.onSuccess(title, list);
                } else {
                    if (apiCallback != null) apiCallback.onSuccess("", null);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(message);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null)
                    apiCallback.onComplete();
            }
        });
        builder.execute();
    }

    @Override
    public void getTopMoneyUpload(int offset, int limit, final ApiCallbackV2<ArrayList<Channel>> apiCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        // msisdn + domain + limit + offset + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + limit + offset
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());
        Http.Builder builder = get(domain, VideoApi.GET_LIST_TOP_MONEY_CHANNEL);
        builder.putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber());
        builder.putParameter(Parameter.Http.DOMAIN, domainParam);
        builder.putParameter(Parameter.LIMIT, String.valueOf(limit));
        builder.putParameter(Parameter.OFFSET, String.valueOf(offset));
        builder.putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter(Parameter.REVISION, Config.REVISION);
        builder.putParameter(Parameter.TIMESTAMP, timeStamp);
        builder.putParameter(Parameter.SECURITY, security);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                JSONObject jsonObjectData = jsonObject.optJSONObject("data");
                if (jsonObjectData != null) {
                    ArrayList<Channel> list = convertStringToChannelArrayListBase(jsonObjectData.optString("listHotChannel"));
                    if (apiCallback != null) apiCallback.onSuccess("", list);
                } else {
                    if (apiCallback != null) apiCallback.onSuccess("", null);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(message);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null)
                    apiCallback.onComplete();
            }
        });
        builder.execute();
    }

    @Override
    public void getInfoVideoFromUrl(String link, final ApiCallbackV2<Video> apiCallback) {
        final long startTime = System.currentTimeMillis();
        final String timestamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber() + domainParam + link +
                getReengAccountBusiness().getToken() + timestamp, getReengAccountBusiness().getToken());

        Http.Builder builder = get(domain, VideoApi.GET_VIDEO_DETAIL_BY_URL);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("domain", domainParam);
        builder.putParameter("url", link);
        builder.putParameter("timestamp", timestamp);
        builder.putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("recommendType", "");
        builder.putParameter(Parameter.Http.NETWORK_TYPE, NetworkHelper.getNetworkSubType(application));
        builder.putParameter("vip", getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP);
        builder.putParameter("security", security);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                long endTime = System.currentTimeMillis();
                String time = String.valueOf(endTime - startTime);
                JSONObject js = new JSONObject(data);
                int code = js.optInt("code");
                if (code == 200) {
                    JSONObject jsVideo = js.optJSONObject("video_detail");
                    Video video = null;
                    if (jsVideo != null) {
                        video = convertStringToVideoInfo(jsVideo);
                        if (apiCallback != null) apiCallback.onSuccess("", video);
                    } else {
                        if (apiCallback != null) apiCallback.onError(application
                                .getResources().getString(R.string.e601_error_but_undefined));
                    }
                    if (video != null && !TextUtils.isEmpty(video.getOriginalPath()))
                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL,
                                time, timestamp, Constants.LogKQI.StateKQI.SUCCESS.getValue());
                    else
                        LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL,
                                time, timestamp, Constants.LogKQI.StateKQI.VIDEO_DETAIL_NULL.getValue());
                } else {
                    if (apiCallback != null) apiCallback.onError(application
                            .getResources().getString(R.string.e601_error_but_undefined));
                    LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL, time, timestamp, code + "");
                }
            }

            @Override
            public void onFailure(String message) {
                long endTime = System.currentTimeMillis();
                String time = String.valueOf(endTime - startTime);
                if (apiCallback != null) apiCallback.onError(application
                        .getResources().getString(R.string.e601_error_but_undefined));
                LogKQIHelper.getInstance(application).saveLogKQI(Constants.LogKQI.VIDEO_GET_DETAIL,
                        time, timestamp, Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.execute();
    }

    @Override
    public void getSuggestVideo(String categoryId, int offset, int limit, boolean onRefresh, final ApiCallbackV2<ArrayList<Video>> apiCallback) {
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                        + domainParam + limit + offset + getReengAccountBusiness().getToken() + timeStamp
                , getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_LIST_VIDEO_RECOMMEND)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.LIMIT, String.valueOf(limit))
                .putParameter(Parameter.OFFSET, String.valueOf(offset))
                .putParameter("overwrite", String.valueOf(onRefresh))
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.User.VIP, getReengAccountBusiness().isVip() ? Constants.TabVideo.VIP : Constants.TabVideo.NOVIP)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);
                        String title = jsonObject.optString("title");
                        if (apiCallback != null) {
                            apiCallback.onSuccess(title, convertStringToVideoArrayList(data));
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        if (apiCallback != null) {
                            apiCallback.onError(error);
                        }
                    }

                    @Override
                    public void onCompleted() {
                        if (apiCallback != null) {
                            apiCallback.onComplete();
                        }
                    }
                })
                .execute();
    }

}

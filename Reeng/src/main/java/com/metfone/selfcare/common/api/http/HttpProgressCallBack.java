package com.metfone.selfcare.common.api.http;

public abstract class HttpProgressCallBack extends HttpCallBack {
    public abstract void onProgressUpdate(int position, int sum, int percentage);
}

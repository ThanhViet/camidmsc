package com.metfone.selfcare.common.api.video.video;

import androidx.annotation.Nullable;

import com.metfone.selfcare.common.api.ApiCallback;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpProgressCallBack;
import com.metfone.selfcare.common.api.video.callback.OnCategoryCallback;
import com.metfone.selfcare.common.api.video.callback.OnVideoCallback;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.model.tab_video.VideoRevenue;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

public interface VideoApi {
    //todo domain_mcvideo: Sử dụng cho các API module video mocha. Domain gọi các API có path bắt đầu với “/onMediaBackendBiz/onmedia/video/…”
    String ADS = "/onMediaBackendBiz/onmedia/video/getAds";// hiển thị quảng cáo
    String REPORT_VIDEO = "/onMediaBackendBiz/onmedia/video/report";// báo cáo video
    String SEARCH_VIDEO_V1 = "/onMediaBackendBiz/onmedia/video/search/v1";// lấy dánh sách video liên quan
    String GET_CATEGORY = "/onMediaBackendBiz/onmedia/video/getCategory";// lấy danh sách category
    String GET_DETAIL_URL_V2 = "/onMediaBackendBiz/onmedia/getDetailUrl/v2";// lấy thông tin số lượt like và share video
    String GET_VIDEO_DETAIL_BY_URL = "/onMediaBackendBiz/onmedia/video/getVideoDetail";// lấy thông tin video
    String GET_STAT_OF_VIDEO = "/onMediaBackendBiz/onmedia/video/getStatOfVideo";// lấy thông tin doanh thu của kênh
    String GET_LIST_FILM_GROUPS = "/onMediaBackendBiz/onmedia/video/getListFilmGroups"; // lấy danh sách phim
    String GET_VIDEO_NEW_PUBLISH = "/onMediaBackendBiz/onmedia/video/getVideoNewPublish";// API lấy danh sách video mới publish ( Chú ý api có trao đổi lastId với server theo như cơ chế đã thông nhất)
    String GET_VOLUME_FILM_GROUPS = "/onMediaBackendBiz/onmedia/video/getVolumeFilmGroups";// Lấy tập trong phim
    String GET_FILM_GROUPS_RELATED = "/onMediaBackendBiz/onmedia/video/getFilmGroupsRelated";// lấy danh sách video
    String GET_LIST_VIDEO_BY_CATE = "/onMediaBackendBiz/onmedia/video/getListVideoByCate/v1";// lấy danh sách video theo category
    String GET_CHANNEL_USER_FOLLOW = "/onMediaBackendBiz/onmedia/video/getChannelUserfollow";// upload video từ youtube
    String GET_LIST_VIDEO_BY_CHANNEL = "/onMediaBackendBiz/onmedia/video/getListVideoByChannel/v1";// lấy danh sách video by channel
    String CREATE_VIDEO_UPLOAD_BY_USER = "/onMediaBackendBiz/onmedia/video/createVideoUploadByUser";// upload video từ youtube
    String CREATE_VIDEO_BY_LINK_YOUTUBE = "/onMediaBackendBiz/onmedia/video/createVideoByLinkYoutube";// upload video từ youtube
    String GET_LIST_FILM_GROUPS_OF_CHANNEL = "/onMediaBackendBiz/onmedia/video/getListFilmGroupsOfChannel"; // lấy danh sách phim
    String GET_LIST_HOT_CHANNEL = "/onMediaBackendBiz/onmedia/video/listHotChannel"; // lấy danh sách kênh hot
    String GET_LIST_TOP_MONEY_CHANNEL = "/onMediaBackendBiz/onmedia/video/listTopMoneyUpload"; // lấy danh sách top kênh kiếm tiền
    String SEARCH_CHANNEL_VIDEO = "/onMediaBackendBiz/onmedia/video/searchChannel"; // tìm kiếm danh sách kênh
    String API_GET_VIDEO_CATEGORY_UPDATE = "/onMediaBackendBiz/onmedia/video/getCategoryUpload"; // lấy danh sách các chủ đề cho màn hình upload video
    String API_LOG_VIEW = "/onMediaBackendBiz/onmedia/video/logview"; // log lượt xem video
    String API_LOG_ADS = "/onMediaBackendBiz/onmedia/video/logads"; // log quảng cáo
    String GET_STAT_OF_CHANNEL = "/onMediaBackendBiz/onmedia/video/getStatOfChannel";// lấy thống kê cho kênh
    String API_FOLLOW_CHANNEL = "/onMediaBackendBiz/onmedia/video/userFollowChanel"; // follow/unfollow kênh
    String API_GET_INFO_CHANNEL = "/onMediaBackendBiz/onmedia/video/getInfoChannel"; // lấy thông tin chi tiết kênh
    String GET_CHANNEL_OF_USER = "/onMediaBackendBiz/onmedia/video/getChannelOfUser";// lấy thông tin channel của user
    String GET_CREATE_CHANNEL = "/onMediaBackendBiz/onmedia/video/createChannel";// tạo kênh
    String GET_LIST_VIDEO_RECOMMEND = "/onMediaBackendBiz/onmedia/video/getListVideoRecommand";// lấy danh sách video gợi ý

    void getCategoriesV2(ApiCallbackV2<ArrayList<Object>> callback);

    @Deprecated
    void getVideosByCategoryId(CompositeDisposable compositeDisposable, String categoryid, int limit, int offset, String lastId, final OnVideoCallback onVideoCallback);

    void getVideosByCategoryV2(String categoryId, int i, int limit, String s, @Nullable ApiCallbackV2<ArrayList<Object>> callback);

    void getVideosByCategoryV2(String categoryId, int offset, int limit, String lastId, String type, @Nullable ApiCallbackV2<ArrayList<Object>> callback);

    void getMoviesByCategoryV2(String categoryId, int offset, int limit, String lastId, ApiCallbackV2<ArrayList<Video>> callback);

    void getVideosByChannelIdV2(String mChannelId, int offset, int limit, String id, @Nullable ApiCallbackV2<ArrayList<Video>> callback);

    void getMoviesByChannelIdV2(String id, int offset, int limit, String lastId, @Nullable ApiCallbackV2<ArrayList<Video>> callback);

    void getVideoRevenuesByChannelIdV2(String channelId, String videoName, int offset, int limit, String lastId, @Nullable ApiCallbackV2<ArrayList<VideoRevenue>> callback);

    void getVideosRelationship(CompositeDisposable compositeDisposable, String titleSearch, String videoType, String url, String categoryId, int offset, int limit, String lastId, String queryRecommendation, OnVideoCallback onGetVideoCallback);

    void likeOrUnlikeVideo(Video video);

    void shareVideo(FeedModelOnMedia feed);

    void getInfoVideoFromUrl(CompositeDisposable compositeDisposable, Video video, final OnVideoCallback onVideoCallback);

    void callApiLog(Video video);

    void callApiAdsLog(Video video, int adsPosition, String adsToken);

    void callApiLogView(Video video);

    void addOrRemoveSaveVideo(Video video);

    boolean isSave(Video video);

    boolean isWatchLater(Video video);

    void addOrRemoveWatchLater(Video video);

    void getVideosByType(String type, OnVideoCallback onVideoCallback);

    void removerVideo(String type, Video video);

    void uploadVideo(Video video, String categoryId, OnVideoCallback onVideoCallback);

    Http uploadVideo(Video video, String categoryId, HttpProgressCallBack httpProgressCallBack);

    void uploadVideoYoutube(String link, String categoryId, boolean isUploadOnMedia, OnVideoCallback onVideoCallback);

    void uploadVideoInfo(Video video, boolean isUploadOnMedia, String categoryId, String itemvideo, OnVideoCallback onVideoCallback);

    void getCategoryUpload(OnCategoryCallback onCategoryCallback);

    void reportVideo(String videoId, String videoName, String videoLink, String videoPath, String reportId, String reason);

    void getVideoNewPublish(String lastId, int offset, ApiCallback apiCallback);

    void getChannelUserFollow(int offset, ApiCallback apiCallback);

    void getVolumeFilmGroups(String groupId, ApiCallback apiCallback);

    void getMovieInfo(Video video, ApiCallbackV2<Video> apiCallback);

    void getFilmGroupsRelated(String filmGroupID, ApiCallbackV2<ArrayList<Object>> apiCallback);

    void likeOrUnlikeMovie(Movie model);

    void getHotChannel(boolean isTabHome, String categoryId, int offset, int limit, ApiCallbackV2<ArrayList<Channel>> apiCallback);

    void getTopMoneyUpload(int offset, int limit, ApiCallbackV2<ArrayList<Channel>> apiCallback);

    void getInfoVideoFromUrl(String link, ApiCallbackV2<Video> apiCallback);

    void getSuggestVideo(String categoryId, int offset, int limit, boolean onRefresh, ApiCallbackV2<ArrayList<Video>> apiCallback);

}

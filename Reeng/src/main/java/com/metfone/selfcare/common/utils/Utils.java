package com.metfone.selfcare.common.utils;

import android.content.Context;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tab_video.Category;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;

import java.util.ArrayList;

/**
 * Created by tuanha00 on 3/22/2018.
 */

public interface Utils {

    void openVideoDetail(BaseSlidingFragmentActivity activity, Video video);

    void openRewardDetail(BaseSlidingFragmentActivity activity, SearchRewardResponse.Result.ItemReward reward);

    void openMovieDetail(BaseSlidingFragmentActivity activity, Movie movie);

    void openVideoDetail(BaseSlidingFragmentActivity activity, FeedModelOnMedia feedModelOnMedia);

    void openCommentVideo(BaseSlidingFragmentActivity activity, Video video);

    void openShareMenu(BaseSlidingFragmentActivity activity, Video video);

    void openChannelInfo(BaseSlidingFragmentActivity activity, Channel channel);

    void openDetailCategoryFilm(BaseSlidingFragmentActivity activity, MovieKind movieKind);

    int getHeightScreen();

    int getWidthScreen();

    void openCreateChannel(BaseSlidingFragmentActivity activity);

    void openConfigRemove(BaseSlidingFragmentActivity context, OnConfigRemoveVideoListener onConfigRemoveVideoListener);

    void showOptionLibraryVideoItem(BaseSlidingFragmentActivity activity, final Video item, String type
            , final OnClickMoreItemListener listener);

    void saveChannelInfo(Channel channel);

    Channel getChannelInfo();

    void saveCategories(ArrayList<Category> list);

    ArrayList<Category> getCategories();

    void openUploadVideo(BaseSlidingFragmentActivity activity);

    interface OnConfigRemoveVideoListener {
        void onConfigRemoveVideo();
    }
}

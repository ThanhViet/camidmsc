package com.metfone.selfcare.common.api;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import com.metfone.selfcare.activity.ViettelIQActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.fragment.viettelIQ.QuestionViettelIQFragment;
import com.metfone.selfcare.fragment.viettelIQ.WinViettelIQFragment;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.GameIQHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

public class ViettelIQApi extends BaseApi {

    private static final String TAG = "ViettelIQApi";

    private static ViettelIQApi mInstance;
    private String domain;

    public static ViettelIQApi getInstance() {
        if (mInstance == null) mInstance = new ViettelIQApi();
        mInstance.domain = ApplicationController.self().getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_GAME_IQ);
        return mInstance;
    }

    public ViettelIQApi() {
        super(ApplicationController.self());
        domain = ApplicationController.self().getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_GAME_IQ);
    }

    public void sub(final Activity activity, final String idGame) {
        ApplicationController.self().logDebugContent("Sub game: " + idGame);
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
//        md5 = msisdn + clientType + revision + sToken + timestamp
        boolean isFisrtTime = ApplicationController.self().getReengAccountBusiness().isFirstTimePlayGameIQ(idGame);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + idGame + isFisrtTime + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, Url.File.API_VT_IQ_SUB)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("gameId", idGame)
                .putParameter("firstTime", String.valueOf(isFisrtTime))
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ApplicationController.self().logDebugContent("Sub game onSuccess: " + (System.currentTimeMillis() - startTime));
                        String decryptData = HttpHelper.decryptResponse(data, getReengAccountBusiness().getToken());
                        if (activity instanceof ViettelIQActivity)
                            ((ViettelIQActivity) activity).onSubSuccess(decryptData);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (activity instanceof ViettelIQActivity) {
                            ((ViettelIQActivity) activity).showToast(R.string.e601_error_but_undefined);
                            activity.finish();
                        }
                    }
                })
                .execute();
    }

    public void getStat(String gameId, String matchId, String questId, int questInx, final Fragment fragment) {
        ApplicationController.self().logDebugContent("getStat: " + gameId + " questId: " + questId);
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
//      md5 = msisdn + gameId  + matchId + questId +  questInx  + clientType + revision + sToken + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + gameId + matchId + questId + questInx + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, Url.File.API_VT_IQ_GET_STAT)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("gameId", gameId)
                .putParameter("matchId", matchId)
                .putParameter("questId", questId)
                .putParameter("questInx", String.valueOf(questInx))
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ApplicationController.self().logDebugContent("getStat onSuccess: " + (System.currentTimeMillis() - startTime));
                        String decryptData = HttpHelper.decryptResponse(data, getReengAccountBusiness().getToken());
                        if (fragment instanceof QuestionViettelIQFragment)
                            ((QuestionViettelIQFragment) fragment).onStat(decryptData);
                    }
                })
                .execute();
    }

    public void getReport(String gameId, String matchId, final Fragment fragment) {
        ApplicationController.self().logDebugContent("getReport: " + gameId);
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
//        md5 = msisdn + clientType + revision + sToken + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + gameId + matchId + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, Url.File.API_VT_IQ_GET_REPORT)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("gameId", gameId)
                .putParameter("matchId", matchId)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ApplicationController.self().logDebugContent("getReport onSuccess: " + (System.currentTimeMillis() - startTime));
                        String decryptData = HttpHelper.decryptResponse(data, getReengAccountBusiness().getToken());
                        if (fragment instanceof WinViettelIQFragment)
                            ((WinViettelIQFragment) fragment).onGetListDone(decryptData);
                        Log.i(TAG, "onSuccess: " + data);

                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (fragment instanceof WinViettelIQFragment)
                            ((WinViettelIQFragment) fragment).onGetListDone("");
                    }
                })
                .execute();
    }

    public void answer(String gameId, String matchId, String questId, int questInx, String questContent, String selectedContent, String preSelected, String selected, final Fragment fragment) {
        ApplicationController.self().logDebugContent("answer: " + gameId + " questId: " + questId);
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
//        msisdn + gameId + matchId + questId + questInx + preselected + selected + clientType + revision + sToken + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + gameId + matchId + questId + questInx + preSelected + selected + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, Url.File.API_VT_IQ_ANSWER)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("gameId", gameId)
                .putParameter("matchId", matchId)
                .putParameter("questId", questId)
                .putParameter("questInx", String.valueOf(questInx))
                .putParameter("questContent", questContent)
                .putParameter("preSelected", preSelected)
                .putParameter("selected", selected)
                .putParameter("selectedContent", selectedContent)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ApplicationController.self().logDebugContent("answer onSuccess: " + (System.currentTimeMillis() - startTime));
                        String decryptData = HttpHelper.decryptResponse(data, getReengAccountBusiness().getToken());
                        if (fragment instanceof QuestionViettelIQFragment)
                            ((QuestionViettelIQFragment) fragment).onAnswer(decryptData);

                    }
                })
                .execute();
    }

    public void useHeart(String gameId, String matchId, String questId, int questInx, int hearts, final Fragment fragment) {
        ApplicationController.self().logDebugContent("useHeart: " + gameId + " questId: " + questId);
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
//        msisdn + gameId + matchId + questId + questInx + preselected + selected + clientType + revision + sToken + timestamp
        String security = HttpHelper.encryptDataV2(application,
                getReengAccountBusiness().getJidNumber()
                        + gameId + matchId + "breakHeart" + questId + questInx + hearts
                        + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                        + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, Url.File.API_VT_IQ_BREAK_HEART)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("gameId", gameId)
                .putParameter("matchId", matchId)
                .putParameter("questId", questId)
                .putParameter("questInx", String.valueOf(questInx))
                .putParameter("api", "breakHeart")
                .putParameter("hearts", String.valueOf(hearts))
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ApplicationController.self().logDebugContent("useHeart onSuccess: " + (System.currentTimeMillis() - startTime));
                        String decryptData = HttpHelper.decryptResponse(data, getReengAccountBusiness().getToken());
                        if (fragment instanceof QuestionViettelIQFragment)
                            ((QuestionViettelIQFragment) fragment).onUseHeart(decryptData);

                    }
                })
                .execute();
    }

    public void getSubWaiting(final String gameId) {
        Http.cancel(Url.File.API_VT_IQ_GET_SUB);
        ApplicationController.self().logDebugContent("getSubWaiting: " + gameId);
        final long startTime = System.currentTimeMillis();
        final String timeStamp = String.valueOf(startTime);
        String apidef = "getSub";
//      md5 = msisdn + gameId  + matchId + questId +  questInx  + clientType + revision + sToken + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + gameId + apidef + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, Url.File.API_VT_IQ_GET_SUB)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter("gameId", gameId)
                .putParameter("apidef", apidef)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .setTag(Url.File.API_VT_IQ_GET_SUB)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        ApplicationController.self().logDebugContent("getSubWaiting onSuccess: " + (System.currentTimeMillis() - startTime));
                        String decryptData = HttpHelper.decryptResponse(data, getReengAccountBusiness().getToken());
                        if (!TextUtils.isEmpty(decryptData)) {
                            JSONObject jsonObject = new JSONObject(decryptData);
                            if (jsonObject.optInt("code") == 200) {
                                long numPlayer = jsonObject.optLong("numPlayer");
                                long numViewer = jsonObject.optLong("numViewer");
                                EventBus.getDefault().post(new GameIQHelper.IQGameEvent(gameId, numPlayer, numViewer));
                            }
                        }

                    }
                })
                .execute();
    }
}

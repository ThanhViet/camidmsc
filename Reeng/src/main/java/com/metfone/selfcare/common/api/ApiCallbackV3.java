package com.metfone.selfcare.common.api;

import com.metfone.selfcare.model.tabMovie.MovieWatched;

import org.json.JSONException;

public interface ApiCallbackV3<T> extends ApiCallback {
    void onSuccess(String msg, T result, MovieWatched movieWatched) throws JSONException;
}

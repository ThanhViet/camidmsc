package com.metfone.selfcare.common.api.video.channel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.video.callback.OnChannelInfoCallback;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.ChannelStatistical;
import com.metfone.selfcare.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tuanha00 on 3/14/2018.
 */

public class ChannelApiImpl extends BaseApi implements ChannelApi {

    public static final String API_UPLOAD_LEAD_IMAGE = "/serviceapi/upload/uploadLeadImage"; // upload ảnh
    private static final String TAG = "ChannelApiImpl";

    public ChannelApiImpl(ApplicationController application) {
        super(application);
    }

    @Override
    public void getStatistical(String id, final ApiCallbackV2<ChannelStatistical> callback) {
        long time = System.currentTimeMillis();

        String timeStamp = String.valueOf(time);
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        // msisdn + domain + clientType + revision + token + timestamp
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_STAT_OF_CHANNEL)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws Exception {
                        JSONObject jsonObject = new JSONObject(data);
                        if (jsonObject.optInt("code") == 200) {
                            ChannelStatistical statistical = gson.fromJson(jsonObject.optString("InfoChannel"), ChannelStatistical.class);
                            if (statistical != null) {
                                if (callback != null) callback.onSuccess("", statistical);
                            } else {
                                onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                            }
                        } else {
                            onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (callback != null) callback.onError(message);
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null) callback.onComplete();
                    }
                })
                .execute();
    }

    @Override
    public void callApiSubOrUnsubChannel(final String channelId, boolean follow) {
//        if(!follow)
//            ChannelDataSource.getInstance().deleteChannel(channelId);

        final String status = follow ? "FOLLOW" : "UNFOLLOW";

        final String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        final String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + channelId + status
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.API_FOLLOW_CHANNEL)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("timestamp", timeStamp)
                .putParameter("channelid", channelId)
                .putParameter("status", status)
                .putParameter("security", security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                    }
                })
                .execute();
    }

    @Override
    public void getChannelInfo(String channelId, final OnChannelInfoCallback channelInfoCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + channelId
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.API_GET_INFO_CHANNEL)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("timestamp", timeStamp)
                .putParameter("channelid", channelId)
                .putParameter("security", security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                        if (channelInfoCallback == null) return;
                        Channel channel = new Channel();
                        try {
                            JSONObject jsonObject = new JSONObject(data).getJSONObject("InfoChannel");
                            channel.setId(jsonObject.optString("categoryid"));
                            channel.setName(jsonObject.optString("categoryname"));
                            channel.setNumFollow(jsonObject.optInt("numfollow"));
                            channel.setUrlImage(jsonObject.optString("url_images"));
                            channel.setUrlImageCover(jsonObject.optString("url_images_cover"));
                            channel.setFollow(jsonObject.optInt("is_follow") == 1);
                            channel.setMyChannel(jsonObject.optInt("isMyChannel") == 1);
                            channel.setDescription(jsonObject.optString("description"));
                            channel.setLastPublishVideo(jsonObject.optLong("lastPublishVideo"));
                            channel.setNumVideo(jsonObject.optInt("numVideo"));
                            channel.setHasFilmGroup(jsonObject.optInt("hasFilmGroup"));
                            channel.setCreatedDate(jsonObject.optString("createdDate"));
                            channel.setIsOfficial(jsonObject.optInt("isOfficial"));
                            channel.setTextTotalPoint(jsonObject.optString("totalPoint"));
//                            channel.setUrl(jsonObject.optString("url"));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        channelInfoCallback.onGetChannelInfoSuccess(channel);
                        channelInfoCallback.onGetChannelInfoComplete();
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                        if (channelInfoCallback == null) return;
                        channelInfoCallback.onGetChannelInfoError(message);
                        channelInfoCallback.onGetChannelInfoComplete();
                    }
                })
                .execute();
    }

    @Deprecated
    @Override
    public void getChannelByOwnerId(final OnChannelInfoCallback channelInfoCallback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_CHANNEL_OF_USER)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter("timestamp", timeStamp)
                .putParameter("security", security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                        if (channelInfoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("code");
                            if (code == 200) {
                                JSONArray jsonArray = js.getJSONArray("result");
                                Channel channel;
                                if (jsonArray.length() > 0) {
                                    channel = convertStringToChannelInfo(js.getJSONArray("result").getString(0));
                                } else {
                                    channel = new Channel();
                                }
                                channelInfoCallback.onGetChannelInfoSuccess(channel);
                            } else {
                                channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                        }
                        channelInfoCallback.onGetChannelInfoComplete();
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                        if (channelInfoCallback == null) return;
                        channelInfoCallback.onGetChannelInfoError(message);
                        channelInfoCallback.onGetChannelInfoComplete();
                    }
                })
                .execute();
    }

    @Override
    public void getMyChannelInfoV2(@Nullable final ApiCallbackV2<Channel> callback) {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);

        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        get(domain, VideoApi.GET_CHANNEL_OF_USER)
                .putParameter(Parameter.User.MSISDN, getReengAccountBusiness().getJidNumber())
                .putParameter(Parameter.Http.DOMAIN, domainParam)
                .putParameter(Parameter.CLIENT_TYPE, Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter(Parameter.REVISION, Config.REVISION)
                .putParameter(Parameter.TIMESTAMP, timeStamp)
                .putParameter(Parameter.SECURITY, security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) throws JSONException {
                        JSONObject js = new JSONObject(data);
                        int code = js.optInt(CODE);
                        if (code == 200) {
                            JSONArray jsonArray = js.optJSONArray(RESULT);
                            Channel channel = null;
                            if (jsonArray.length() > 0) {
                                channel = convertStringToChannelInfo(jsonArray.getString(0));
                            }
                            if (callback != null) {
                                callback.onSuccess("", channel);
                            }
                            return;
                        }
                        onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        if (callback != null) {
                            callback.onError(message);
                        }
                    }

                    @Override
                    public void onCompleted() {
                        super.onCompleted();
                        if (callback != null) {
                            callback.onComplete();
                        }
                    }
                })
                .execute();
    }

    @Override
    public void updateOrCreateChannel(boolean isUploadChannelAvatar, final Channel channel, final OnChannelInfoCallback channelInfoCallback) {
        if (isUploadChannelAvatar) uploadAvatar(channel, channelInfoCallback);
        else updateOrCreateChannelInfo("", channel, channelInfoCallback);
    }

    private void uploadAvatar(final Channel channel, final OnChannelInfoCallback channelInfoCallback) {
        String userName = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_USER);
        String password = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_PASS);
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_VIDEO_UPLOAD);
        upload(domain, API_UPLOAD_LEAD_IMAGE)
                .putFile("uploadfile", channel.getUrlImage())
                .putParameter("username", userName)
                .putParameter("password", password)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                        if (channelInfoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("errorCode");
                            if (code == 0) {
                                String url = js.optString("imagelead");
                                channel.setUrlImage(js.optString("imagelead_link"));
                                updateOrCreateChannelInfo(url, channel, channelInfoCallback);
                            } else {
                                channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                                channelInfoCallback.onGetChannelInfoComplete();
                            }
                        } catch (Exception e) {
                            channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                            channelInfoCallback.onGetChannelInfoComplete();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                        if (channelInfoCallback == null) return;
                        channelInfoCallback.onGetChannelInfoError(message);
                        channelInfoCallback.onGetChannelInfoComplete();
                    }
                })
                .execute();
    }

    private void updateOrCreateChannelInfo(final String url, final Channel channel, final OnChannelInfoCallback channelInfoCallback) {
        String isEdit = TextUtils.isEmpty(channel.getId()) ? "0" : "1";
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + channel.getName() + channel.getDescription() + url + isEdit
                + Constants.HTTP.CLIENT_TYPE_STRING + Config.REVISION
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.GET_CREATE_CHANNEL)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("channelName", channel.getName())
                .putParameter("channelDesc", channel.getDescription())
                .putParameter("channelAvatar", url)
                .putParameter("isEdit", isEdit)
                .putParameter("clientType", Constants.HTTP.CLIENT_TYPE_STRING)
                .putParameter("revision", Config.REVISION)
                .putParameter("timestamp", timeStamp)
                .putParameter("security", security)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                        if (channelInfoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("code");
                            if (code == 200) {
                                Channel channel1 = convertStringToChannelInfo(js.getJSONArray("result").getString(0));
                                channel1.setUrlImage(channel.getUrlImage());
                                channel1.setMyChannel(true);
                                channel1.setName(channel.getName());
                                channel1.setDescription(channel.getDescription());
                                channel1.setNumFollow(channel.getNumfollow());
                                channel1.setTextTotalPoint(channel.getTextTotalPoint());
                                channelInfoCallback.onGetChannelInfoSuccess(channel1);
                            } else {
                                channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                                channelInfoCallback.onGetChannelInfoComplete();
                            }
                        } catch (Exception e) {
                            channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                            channelInfoCallback.onGetChannelInfoComplete();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                        if (channelInfoCallback == null) return;
                        channelInfoCallback.onGetChannelInfoError(message);
                        channelInfoCallback.onGetChannelInfoComplete();
                    }
                })
                .execute();
    }

    public void createChannel(final Channel channel, final OnChannelInfoCallback channelInfoCallback) {
        String userName = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_USER);
        String password = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.VIDEO_UPLOAD_PASS);
        String domain = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.DOMAIN_VIDEO_UPLOAD);
        upload(domain, API_UPLOAD_LEAD_IMAGE)
                .putParameter("UploadFile uf", channel.getUrlImage())
                .putParameter("username", userName)
                .putParameter("password", password)
                .withCallBack(new HttpCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        Log.i(TAG, "onSuccess: " + data);
                        if (channelInfoCallback == null) return;
                        try {
                            JSONObject js = new JSONObject(data);
                            int code = js.optInt("errorCode");
                            if (code == 0) {
                                String url = js.getString("imagelead_link");
                                channel.setUrlImage(url);

                                channelInfoCallback.onGetChannelInfoSuccess(channel);
                            } else {
                                channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                                channelInfoCallback.onGetChannelInfoComplete();
                            }
                        } catch (Exception e) {
                            channelInfoCallback.onGetChannelInfoError(application.getResources().getString(R.string.e601_error_but_undefined));
                            channelInfoCallback.onGetChannelInfoComplete();
                        }
                    }

                    @Override
                    public void onFailure(String message) {
                        super.onFailure(message);
                        Log.i(TAG, "onFailure: " + message);
                        if (channelInfoCallback == null) return;
                        channelInfoCallback.onGetChannelInfoError(message);
                        channelInfoCallback.onGetChannelInfoComplete();
                    }
                })
                .execute();
    }

    private Channel convertStringToChannelInfo(String data) {
        Channel channel = new Channel();
        try {
            JSONObject jsonObject = new JSONObject(data);
            channel.setId(jsonObject.optString("id"));
            channel.setName(jsonObject.optString("name"));
            channel.setUrlImage(jsonObject.optString("url_images"));
            channel.setUrlImageCover(jsonObject.optString("url_images_cover"));
            channel.setDescription(jsonObject.optString("description"));
            channel.setFollow(jsonObject.optInt("is_follow") == 1);
            channel.setNumFollow(jsonObject.optInt("numfollow"));
            channel.setIsOfficial(jsonObject.optInt("isOfficial"));
            channel.setLastPublishVideo(jsonObject.optLong("lastPublishVideo"));
        } catch (Exception e) {
            Log.e(TAG, "convertStringToChannelInfo", e);
        }
        return channel;
    }

    public void subscribeChannel(Channel channel, @NonNull HttpCallBack callBack) {
        final String channelId = channel.getId();
        final String status = channel.isFollow() ? "FOLLOW" : "UNFOLLOW";

        final String timeStamp = String.valueOf(System.currentTimeMillis());
        String domain = getDomainMochaVideo();
        String domainParam = convertDomainToDomainParam(domain);
        final String security = HttpHelper.encryptDataV2(application, getReengAccountBusiness().getJidNumber()
                + domainParam + channelId + status
                + getReengAccountBusiness().getToken() + timeStamp, getReengAccountBusiness().getToken());

        post(domain, VideoApi.API_FOLLOW_CHANNEL)
                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
                .putParameter("domain", domainParam)
                .putParameter("timestamp", timeStamp)
                .putParameter("channelid", channelId)
                .putParameter("status", status)
                .putParameter("security", security)
                .withCallBack(callBack)
                .execute();
    }
}

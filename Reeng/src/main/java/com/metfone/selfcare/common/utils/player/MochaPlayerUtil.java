package com.metfone.selfcare.common.utils.player;

import com.metfone.selfcare.app.dev.ApplicationController;

import java.util.HashMap;
import java.util.Map;

public class MochaPlayerUtil {

    private static MochaPlayerUtil ourInstance;

    public static MochaPlayerUtil getInstance() {
        if (ourInstance == null)
            ourInstance = new MochaPlayerUtil();
        return ourInstance;
    }

    public static MochaPlayer getPlayer(String tag) {
        return getInstance().providePlayerBy(tag);
    }

    public static void removePlayer(String tag) {
        getInstance().removerPlayerBy(tag);
    }

    private final Map<String, MochaPlayer> playerMap;
    private final ApplicationController mApp;

    private MochaPlayerUtil() {
        mApp = ApplicationController.self();
        playerMap = new HashMap<>();
    }

    public void removerPlayerBy(String tag) {
        MochaPlayer player = playerMap.remove(tag);
        if (player != null) {
            player.setPlayWhenReady(false);
            player.release();
        }
    }

    public MochaPlayer providePlayerBy(String tag) {
        MochaPlayer player = playerMap.get(tag);
        if (player == null && mApp != null) {
            player = new MochaPlayer(mApp);
            playerMap.put(tag, player);
        }
        return player;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/25
 *
 */

package com.metfone.selfcare.common.api.news;

import androidx.annotation.NonNull;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.netnews.request.NewsContentRequest;
import com.metfone.selfcare.module.netnews.request.NewsRequest;
import com.metfone.selfcare.module.netnews.request.NormalRequest;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsContentResponse;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

public class NetNewsApi extends BaseApi {
    public static final String GET_NEWS_BY_CATEGORY = "/Tinngan.svc/getCateList";
    public static final String GET_NEWS_CATEGORY = "/Tinngan.svc/getCategory";
    public static final String GET_NEWS_HOME = "/Tinngan.svc/getNewsHomev2";
    public static final String GET_NEWS_HOME_CATE = "/Tinngan.svc/getHomeCate";
    public static final String GET_NEWS_CONTENT = "/Tinngan.svc/getContent";
    public static final String GET_NEWS_RELATE = "/Tinngan.svc/getListRelateReadNews";
    public static final String GET_NEWS_RELATE_FROM_HOME = "/Tinngan.svc/getListCateRelate";
    public static final String GET_NEWS_RELATE_FROM_CATEGORY = "/Tinngan.svc/getListCateRelateOld";
    public static final String GET_NEWS_RELATE_FROM_CATEGORY_POSTION_0 = "/Tinngan.svc/getListCateRelateOldCM";
    public static final String GET_NEWS_RELATE_FROM_EVENT = "/Tinngan.svc/getListCateRelateEvent";
    public static final String GET_NEWS_BY_SOURCE = "/Tinngan.svc/getListNewsOfSource";
    public static final String GET_EVENT_LIST = "/Tinngan.svc/getHomeEvent";
    public static final String GET_NEWS_BY_EVENT = "/Tinngan.svc/getEventView";
    public static final String GET_HOT_NEWS = "/Tinngan.svc/getMostView";
    public static final String GET_SEARCH = "/Tinngan.svc/getSearch";
    public static final String GET_MOST_NEWS = "/Tinngan.svc/getMostView";
    public static final String GET_READ_CONTENT = "/api/news/read-content"; // lấy nội dung của bài đọc từ netnews để load nhanh
//    public static final String GET_READ_RELATE = "/api/news/read-relate"; // lấy tin liên quan của bài đọc từ netnews
//    public static final String GET_READ_HOT = "/api/news/read-hot"; // lấy tin đọc nhiều từ bài đọc netnews
    public static final String GET_NEWS_HOME_V5 = "/Tinngan.svc/getNewsHomeV5";//"http://apinew.tinngan.vn
    public static final String GET_NEWS_CAN_CARE_HOME_V5 = "/Tinngan.svc/getNewsCanInterestedV5";

    private boolean hasNetNewsHeader = true;
    private String domain;
    private ApplicationController app;

    public NetNewsApi(ApplicationController app) {
        super(app);
        domain = getDomainNetNews();
        this.app = app;
    }

    @Override
    protected Http.Builder configBuilder(@NonNull Http.Builder builder) {
        if (hasNetNewsHeader) {
            String msisdn = "";
            if (getReengAccountBusiness().getJidNumber() != null)
                msisdn = getReengAccountBusiness().getJidNumber();
            builder.putHeader("api_key", CommonUtils.API_KEY);
            builder.putHeader("user", CommonUtils.USER);
            builder.putHeader("password", CommonUtils.PASS);
            builder.putHeader("version", BuildConfig.VERSION_NAME);
            builder.putHeader("device", CommonUtils.DEVICE);
            builder.putHeader("imei", DeviceHelper.getDeviceId(application, true));
            builder.putHeader("msisdn", msisdn);
            builder.putHeader("uuid", Utilities.getUuidApp());
            builder.putHeader("Platform", "Mocha");
        }
        return super.configBuilder(builder);
    }

    @Override
    protected Http.Builder get(String url) {
        Http.Builder builder = super.get(url);
        builder.setTimeOut(60);
        return builder;
    }

    @Override
    protected Http.Builder get(String baseUrl, String url) {
        Http.Builder builder = super.get(baseUrl, url);
        builder.setTimeOut(60);
        return builder;
    }

    public String getApiUrl(String apiPath) {
        return domain + apiPath;
    }

//    public void getNewsContent(String url, HttpCallBack callBack) {
//        hasNetNewsHeader = false;
//        get(domain, GET_READ_CONTENT)
//                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
//                .putParameter("url", url)
//                .withCallBack(callBack)
//                .execute();
//    }
//
//    public void getNewsRelate(String url, HttpCallBack callBack) {
//        hasNetNewsHeader = false;
//        get(domain, GET_READ_RELATE)
//                .putParameter("msisdn", getReengAccountBusiness().getJidNumber())
//                .putParameter("url", url)
//                .withCallBack(callBack)
//                .execute();
//    }
//
//    public void getNewsHot(String url, HttpCallBack callBack) {
//        hasNetNewsHeader = false;
//        String msisdn = getReengAccountBusiness().getJidNumber();
//        get(domain, GET_READ_HOT)
//                .putParameter("msisdn", msisdn)
//                .putParameter("url", url)
//                .withCallBack(callBack)
//                .execute();
//    }

    public void getNewsContent(NewsModel model, HttpCallBack callBack) {
        hasNetNewsHeader = true;
        get(domain, GET_NEWS_CONTENT + "/" + model.getPid() + "/" + model.getCid()
                + "/" + model.getID())
                .withCallBack(callBack).execute();
    }

    public void getNewsRelate(NewsModel model, int page, long unixTime, HttpCallBack callBack) {
        hasNetNewsHeader = true;
        get(domain, GET_NEWS_RELATE_FROM_HOME + "/" + model.getPid() + "/" + model.getID()
                + "/" + page + "/" + unixTime)
                .withCallBack(callBack).execute();
    }

    public void getNewsHot(NewsModel model, int page, long unixTime, HttpCallBack callBack) {
        hasNetNewsHeader = true;
        get(domain, GET_NEWS_RELATE + "/" + model.getPid() + "/" + model.getID()
                + "/" + page + "/" + unixTime)
                .withCallBack(callBack).execute();
    }

    public void loadDataRelateFromCategory(NewsContentRequest request, HttpCallBack callBack) {
        hasNetNewsHeader = true;
        get(domain, GET_NEWS_RELATE_FROM_CATEGORY + "/" + request.getPid() + "/" + request.getContentId()
                + "/" + request.getPage() + "/" + request.getUnixTime())
                .withCallBack(callBack).execute();
    }

    public void loadDataRelateFromCategoryPosition0(NewsContentRequest request, HttpCallBack callBack) {
        hasNetNewsHeader = true;
        get(domain, GET_NEWS_RELATE_FROM_CATEGORY_POSTION_0 + "/" + request.getPid() + "/" + request.getContentId()
                + "/" + request.getPage() + "/" + request.getUnixTime())
                .withCallBack(callBack).execute();
    }

    public void getNewsRelateFromEvent(NewsContentRequest request, HttpCallBack callBack) {
        hasNetNewsHeader = true;
        get(domain, GET_NEWS_RELATE_FROM_EVENT + "/" + request.getPid() + "/" + request.getCateId()
                + "/" + request.getContentId() + "/" + request.getPage() + "/" + request.getUnixTime())
                .withCallBack(callBack).execute();
    }

    public void getNewsDetail(final String link, final ApiCallbackV2<NewsContentResponse> apiCallback) {
        hasNetNewsHeader = false;
        Http.Builder builder = get(domain, GET_READ_CONTENT);
        String encodeUrl = CommonUtils.encodeStringBase64(link);
        if (encodeUrl != null) {
            encodeUrl = encodeUrl.replaceAll("/", "_");
            encodeUrl = encodeUrl.replaceAll("\\+", "-");
        }
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putParameter("url", encodeUrl);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                NewsContentResponse result = gson.fromJson(response, NewsContentResponse.class);
                if (apiCallback != null) apiCallback.onSuccess("", result);

                if (result.getError() != null) {
                    LogDebugHelper.getInstance().logDebugContent("News detail error:" + response + " | " + link);
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_NEWS_DETAIL, "News detail error:" + response + " | " + link);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));

                LogDebugHelper.getInstance().logDebugContent("News detail error:" + message + " | " + link);
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_NEWS_DETAIL, "News detail error:" + message + " | " + link);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.execute();
    }

    public void getNewsByCategory(NewsRequest request, HttpCallBack callBack) {
        if (request.getCateId() == CommonUtils.DEFAULT_HOT_NEWS_ID) {
            // hot news
            get(domain, GET_HOT_NEWS + "/" + 0 + "/" + request.getPage() + "/" + request.getNum()
                    + "/" + request.getUnixTime()).withCallBack(callBack).execute();
        } else {
            get(domain, GET_NEWS_BY_CATEGORY + "/" + request.getCateId() + "/" + request.getPage()
                    + "/" + request.getNum() + "/" + request.getUnixTime())
                    .withCallBack(callBack).execute();
        }
    }

    public void getNewsCategory(HttpCallBack httpCallBack) {
        get(domain, GET_NEWS_CATEGORY).withCallBack(httpCallBack).execute();
    }

    public void getNewsByEvent(NewsRequest request, HttpCallBack callBack) {
        get(domain, GET_NEWS_BY_EVENT + "/" + request.getCateId() + "/" + request.getPage()
                + "/" + request.getNum())
                .withCallBack(callBack).execute();
    }

    public void getEventList(NormalRequest request, HttpCallBack callBack) {
        get(domain, GET_EVENT_LIST + "/" + request.getPage() + "/" + request.getNum())
                .withCallBack(callBack).execute();
    }

    public void getNewsHome(HttpCallBack callBack) {
        get(domain, GET_NEWS_HOME)
                .withCallBack(callBack).execute();
    }

    public void getNewsHomeCate(HttpCallBack callBack) {
        get(domain, GET_NEWS_HOME_CATE)
                .withCallBack(callBack).execute();
    }

    public void getMostNews(NewsRequest request, HttpCallBack callBack) {
        get(domain, GET_MOST_NEWS + "/" + request.getCateId() + "/" + request.getPage()
                + "/" + request.getNum() + "/" + request.getUnixTime())
                .withCallBack(callBack).execute();
    }

    public void getNewsBySource(NewsRequest request, HttpCallBack callBack) {
        get(domain, GET_NEWS_BY_SOURCE + "/" + request.getCateId() + "/" + request.getPage()
                + "/" + request.getNum())
                .withCallBack(callBack).execute();
    }
    public void getNewsHomeV5(HttpCallBack callBack){
        String result = "";
//        LocationHelper.getInstant(app).getLocationFromPref();
//        if(LocationHelper.getInstant(app).isExistCacheLocation()) {
//            double longitude = LocationHelper.getInstant(app).
//                    getCurrentLocation().getLongitude();
//            double latitude = LocationHelper.getInstant(app).
//                    getCurrentLocation().getLatitude();
//            if (latitude != 0 && longitude != 0) {
//                result = "/"+latitude + "," + longitude;
//            }
//        }
        get(domain,GET_NEWS_HOME_V5+result)
                .withCallBack(callBack).execute();
    }

    public void getNewCanCareHomeV5(String listId,HttpCallBack callBack){
        get(domain,GET_NEWS_CAN_CARE_HOME_V5+"/"+listId).withCallBack(callBack).execute();
    }

}

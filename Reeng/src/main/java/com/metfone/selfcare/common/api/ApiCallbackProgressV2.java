package com.metfone.selfcare.common.api;

public interface ApiCallbackProgressV2<T> extends ApiCallbackV2<T> {
    void onProgress(float progress);
}

package com.metfone.selfcare.common;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LanguageUtils;
import com.google.gson.reflect.TypeToken;
import com.metfone.esport.common.Constant;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.ApiCallbackV3;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabMovie.DrmInfo;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.Movie2;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.request.WsGetHomeMovieApp2Request;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListFilmForUserRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetListLogWatchedRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieCategoryV2Request;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieCountryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieDetailRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsGetMovieRelatedRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsMovieGetCategoryByIdsRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsMovieGetListFilmOfCategoryRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsMovieGetListGroupDetailRequest;
import com.metfone.selfcare.module.home_kh.api.response.WsGetHomeMovieApp2Response;
import com.metfone.selfcare.module.home_kh.api.response.WsGetListFilmForUserResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetListLogWatchedResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieCategoryV2Response;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieCountryResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetMovieDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetCategoryByIdsResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetListFilmOfCategoryResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetListGroupDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsMovieGetRelatedResponse;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.movie.event.UpdateWatchedEvent;
import com.metfone.selfcare.module.movienew.model.ActionFilmModel;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.module.movienew.model.CategoryModel;
import com.metfone.selfcare.module.movienew.model.Country;
import com.metfone.selfcare.module.movienew.model.CountryModel;
import com.metfone.selfcare.module.movienew.model.HomeCategoryInfo;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.HomeResult;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.module.movienew.model.MaybeModel;
import com.metfone.selfcare.network.camid.response.BaseResponse;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Utilities;
import com.viettel.util.LogDebugHelper;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

public class MovieApi extends BaseApi {
    public static final String DELETE_LIKED_MOVIES = "/serviceapi/keeng/api/deleteListFilmUserLike";
    public static final String DELETE_VIEWED_MOVIES = "/serviceapi/keeng/api/deleteLogWatched";
    public static final String ADD_DEVICE_DRM = "/serviceapi/keeng/api/DRMdevice_Manager";
    public static final String GET_CATEGORY = "/serviceapi/keeng/api/getCategory"; // lấy danh sách thể loại
    public static final String GET_CATEGORY_V3 = "/serviceapi/keeng/api/getCategoryV3"; // lấy danh sách subtab của trang chủ, chuyên mục
    public static final String GET_COUNTRY = "/serviceapi/keeng/api/getCountry";
    public static final String GET_DETAIL_MOVIES = "/v2/api/get-detail-film";
    public static final String GET_ODD_MOVIES = "/serviceapi/keeng/api/getFilmOdd";
    public static final String GET_HOME_V3 = "/serviceapi/keeng/api/getHomeV3";
    public static final String GET_HOT_MOVIES = "/serviceapi/keeng/api/getHot";
    public static final String GET_MOVIES_OF_CATEGORY = "/serviceapi/keeng/api/getListFilmOfCategory";
    public static final String GET_LIKED_MOVIES = "/serviceapi/keeng/api/getListFilmUserLike";
    public static final String GET_LIST_SERIES_MOVIES = "/serviceapi/keeng/api/getListGroups";
    public static final String GET_DETAIL_SERIES_MOVIES = "/v2/api/get-list-groups-detail";
    //public static final String GET_DETAIL_SERIES_MOVIES = "/serviceapi/keeng/api/getListGroupsDetail";
    public static final String GET_VIEWED_MOVIES = "/serviceapi/keeng/api/getListLogWatched";
    public static final String GET_NEW_MOVIES = "/serviceapi/keeng/api/getNew";
    public static final String GET_RELATE_MOVIES = "/serviceapi/keeng/api/getRelateFilm";
    public static final String GET_RELATE_MOVIES_NEW = "/v2/api/get-relate-film";
    public static final String GET_DETAIL_CATEGORY = "/serviceapi/keeng/api/getTopCategory";
    public static final String GET_DETAIL_COUNTRY = "/serviceapi/keeng/api/getTopCountry";
    public static final String GET_TOP_KEYWORD = "/serviceapi/keeng/api/getTopKeyword";
    public static final String GET_INFO_DRM = "/serviceapi/keeng/api/getUser_DRM_Info";
    public static final String INSERT_VIEWED_MOVIES = "/serviceapi/keeng/api/insertLogWatched";
    public static final String SEARCH_MOVIES = "/solr/phimkeeng/select";
    public static final String SEARCH_MOVIES_CINEMA = "/v2/api/search";
    public static final String INSERT_WATCH_LATER = "/serviceapi/keeng/api/insertWatchLater";
    public static final String DELETE_WATCH_LATER = "/serviceapi/keeng/api/deleteWatchLater";
    public static final String GET_LIST_WATCH_LATER = "/serviceapi/keeng/api/getListWatchLater";

    public static final String GET_COUNTRY_V2 = "/v2/api/get-country";
    public static final String GET_CATEGORY_V2 = "/v2/api/get-category-v2";
    public static final String GET_HOME_APP_V2 = "/v2/api/get-home-app-2";
    public static final String GET_FILM_FOR_USER = "/v2/api/get-list-film-for-user";
    public static final String LIKE_VIDEO = "/v2/api/like-video";
    public static final String UNLIKE_VIDEO = "/v2/api/unlike-video";
    public static final String RATE_VIDEO = "v2/api/rate-film";
    public static final String ADD_FAVORITE_CATEGORY = "/v2/api/add-favourite-category";
    public static final String ADD_FAVORITE_COUNTRY = "/v2/api/add-favourite-country";
    public static final String GET_LIST_FILM_LIKED = "/v2/api/get-list-film-user-like";
    public static final String INSERT_LOG_WATCHED = "/v2/api/insert-log-watched";
    public static final String GET_TOPIC = "/v2/api/get-topic-category?limit=1000&offset=0";
    public static final String GET_LIST_LOG_WATCH = "/v2/api/get-list-log-watched";
    public static final String GET_LIST_FILM_OF_CATEGORY = "/v2/api/get-list-film-of-category";
    public static final String GET_LIST_FILM_BY_INFO = "/v2/api/get-list-film-by-info";
    public static final String GET_CATEGORY_BY_IDS = "/v2/api/get-category-by-ids";
    public static final String GET_MAYBE_YOU_LIKE = "/v2/api/get-list-film-maybe-like";
    public static final String GET_TOP_COUNTRY = "/v2/api/get-top-country";

    private static final String TAG = "MovieApi";
    private static final String USER_MOVIE = "974802c5178ab63eba02d8035ad3f6ab";
    private static final String PASS_MOVIE = "974802c5178ab63eba02d8035ad3f6ab";
//    public static final String DOMAIN_MOVIE = "http://36.37.242.22/";
    public static final String DOMAIN_MOVIE = "http://apifilm.camid.net/";
    public static final String TOKEN = "Bearer fdTaIJ1PKUiTUD8yBy1Z59EJeNGF7daX7evLTYAYxzXyFGBoJ4yWVsXHNzgg/IfshFX7eSpnMR2mo+SQHBOKWUkwEIKiAXCDU924ojlDMEE/bErxWgHovByFe1bLmO3IbqrJ4Xj+OaKBeRnsZZDwbzc8YNG5+gG/Gg4feG39gJc=";

    private String domain;
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private UserInfo currentUser = userInfoBusiness.getUser();
    private static MovieApi instance;
    private KhHomeClient homeClient;

    public MovieApi() {
        super(ApplicationController.self());
//        domain = getDomainKeengMovies();
//        domain = "http://117.7.199.210:9123";
        //domain = "http://171.255.192.28:8086";
        domain = DOMAIN_MOVIE;
        homeClient = new KhHomeClient();
    }

    public static MovieApi getInstance() {
        if (instance == null) {
            instance = new MovieApi();
        }
        return instance;
    }

    @Override
    protected Http.Builder get(String baseUrl, String url) {
        Http.Builder builder = super.get(baseUrl, url);
        builder.putHeader("Authorization", TOKEN);
        if (ApplicationController.self().getCurrentActivity() != null) {
            builder.putHeader("language", LocaleManager.getLanguage(ApplicationController.self().getCurrentActivity()));
            builder.putParameter("language", LocaleManager.getLanguage(ApplicationController.self().getCurrentActivity()));
        } else {
            builder.putHeader("language", LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
            builder.putParameter("language", LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
        }
        return builder;
    }

    @Override
    protected Http.Builder post(String baseUrl, String url) {
        Http.Builder builder = super.post(baseUrl, url);
        builder.putHeader("Authorization", TOKEN);
        if (ApplicationController.self().getCurrentActivity() != null) {
            builder.putHeader("language", LocaleManager.getLanguage(ApplicationController.self().getCurrentActivity()));
            builder.putParameter("language", LocaleManager.getLanguage(ApplicationController.self().getCurrentActivity()));
        } else {
            builder.putHeader("language", LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
            builder.putParameter("language", LocaleManager.getLanguage(ApplicationController.self().getApplicationContext()));
        }
        return builder;
    }

    private String getMsisdn() {
        if (!StringUtils.isEmpty(getReengAccountBusiness().getJidNumber())) {
            return getReengAccountBusiness().getJidNumber();
        }
        return "011111111";
    }

    private String getRefId() {
        String ref_id = "1";
        if (userInfoBusiness.getUser() != null) {
            ref_id = String.valueOf(userInfoBusiness.getUser().getUser_id());
        }
        ref_id = ref_id.isEmpty() || ref_id.equals("0") ? "1" : ref_id;
        return ref_id;
    }

    public void getCountryV2(@Nullable final ApiCallbackV2<ArrayList<Country>> apiCallback) {
        final long startTime = System.currentTimeMillis();

        WsGetMovieCountryRequest.Request request = new WsGetMovieCountryRequest.Request();
        request.offset = "0";
        request.limit = "1000";

        homeClient.wsGetMovieCountry(new MPApiCallback<WsGetMovieCountryResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieCountryResponse> response) {
                final long endTime = System.currentTimeMillis();
                ArrayList<Country> countries = new ArrayList();
                if(response.body() != null && response.body().result != null){
                    countries = new ArrayList(response.body().result);
                }
                if (Utilities.notEmpty(countries) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", countries);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (apiCallback != null)
                        apiCallback.onError(application.getResources().getString(R.string.movies_info_unavailable));
                }

                apiCallback.onComplete();

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_COUNTRY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_COUNTRY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());

            }
        }, request);


    }

    public void getCategoryV2(@Nullable final ApiCallbackV2<ArrayList<Category>> apiCallback) {
        final long startTime = System.currentTimeMillis();
        WsGetMovieCategoryV2Request.Request request = new WsGetMovieCategoryV2Request.Request();
        request.limit = "100";
        request.offset = "0";

        homeClient.wsGetMovieCategoryV2(new MPApiCallback<WsGetMovieCategoryV2Response>() {
            @Override
            public void onResponse(Response<WsGetMovieCategoryV2Response> response) {
                final long endTime = System.currentTimeMillis();
                ArrayList<Category> categories = new ArrayList();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    categories = new ArrayList(response.body().response.result);
                }
                if (Utilities.notEmpty(categories) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", categories);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (apiCallback != null)
                        apiCallback.onError(application.getResources().getString(R.string.cinema_info_unavailable));
                }
                apiCallback.onComplete();
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        }, request);
    }
   // public void setGetMaybeYouLike(int misdn, int ref_in, ApiCallbackV2<Lis>)

    public void getCategoryByIds(String ids, ApiCallbackV2<List<Category>> apiCallback) {
        WsMovieGetCategoryByIdsRequest.Request request = new WsMovieGetCategoryByIdsRequest.Request();
        request.setClientType(Constants.HTTP.CLIENT_TYPE_STRING);
        request.setIDS(ids);
        request.setMsisdn(getMsisdn());
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieCategoryByIds(new MPApiCallback<WsMovieGetCategoryByIdsResponse>() {
            @Override
            public void onResponse(Response<WsMovieGetCategoryByIdsResponse> response) {
                List<Category> categoryResponse = new ArrayList();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    categoryResponse = response.body().response.result;
                }
                if (apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", categoryResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    apiCallback.onComplete();
                }
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);
    }

    public void getHomeAppV2(int limit, int offset, @Nullable final ApiCallbackV2<HomeResult> apiCallback) {
        if (currentUser == null) return;
        final long startTime = System.currentTimeMillis();
        WsGetHomeMovieApp2Request.Request request = new WsGetHomeMovieApp2Request.Request();
        request.setRefID(getRefId());
        request.setRevision(Config.REVISION);
        request.setVersion(BuildConfig.VERSION_NAME);
        request.setClientType(Constants.HTTP.CLIENT_TYPE_STRING);
        request.setDeviceID(DeviceHelper.getDeviceId(application));
        request.setUsername(USER_MOVIE);
        request.setPassword(PASS_MOVIE);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setMsisdn(UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT ? "0111111111" : getMsisdn());
        request.setRecommendCategoryid("382084");
        request.setAvaiableCategoryid("868462");
        request.setAwardCategoryid("10623");
        request.setLimit(String.valueOf(limit));
        request.setOffset(String.valueOf(offset));
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());

        homeClient.wsGetHomeMovieApp2(new MPApiCallback<WsGetHomeMovieApp2Response>() {
            @Override
            public void onResponse(Response<WsGetHomeMovieApp2Response> response) {
                final long endTime = System.currentTimeMillis();

                WsGetHomeMovieApp2Response body = response.body();
                if (body != null && body.response.result != null) {
                    HomeResult homeResult = body.response.result;
                    if (apiCallback != null) {
                        try {
                            apiCallback.onSuccess("", homeResult);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        finally {
                            apiCallback.onComplete();
                        }
                    }
                    return;
                }
                onError(new Throwable(application.getResources().getString(R.string.e601_error_but_undefined)));

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        }, request);

    }

    public void getMovieDetail(String link, boolean isLink, @Nullable final ApiCallbackV2<Movie> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String filmId;
        if (isLink) {
            filmId = TextHelper.getInstant().getMoviesId(link);
        } else
            filmId = link;
        WsGetMovieDetailRequest.Request request = new WsGetMovieDetailRequest.Request();
        request.setFilmid(filmId);
        request.setMsisdn(getMsisdn());
        request.setRefID(getRefId());
        request.setClientType(Constant.CLIENT_TYPE);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieDetail(new MPApiCallback<WsGetMovieDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieDetailResponse> response) {
                final long endTime = System.currentTimeMillis();
                ArrayList<Movie> data = new ArrayList();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    data.addAll(response.body().response.result);
                }
                // kieu du lieu la boolean nhung api tra ve kieu int, can cast ve boolean
//                JSONObject ob = new JSONObject(response);
//                if (ob.getJSONArray("result").getJSONObject(0).getInt("is_liked") == 1) {
//                    data.get(0).setLike(true);
//                } else {
//                    data.get(0).setLike(false);
//                }

                if (Utilities.notEmpty(data) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", data.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        apiCallback.onComplete();
                    }
                } else {
                    if (apiCallback != null){
                        apiCallback.onError(application.getResources().getString(R.string.movies_info_unavailable));
                        apiCallback.onComplete();
                    }
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_DETAIL, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_DETAIL, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        }, request);
    }

    public void getMovieDetail(String link, boolean isLink, String msisdn, @Nullable final ApiCallbackV3<Movie> apiCallback) {
        final long startTime = System.currentTimeMillis();
        String filmId;
        if (isLink) {
            filmId = TextHelper.getInstant().getMoviesId(link);
        } else
            filmId = link;
        WsGetMovieDetailRequest.Request request = new WsGetMovieDetailRequest.Request();
        request.setFilmid(filmId);
        request.setMsisdn(getMsisdn());
        request.setRefID(getRefId());
        request.setClientType(Constant.CLIENT_TYPE);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieDetail(new MPApiCallback<WsGetMovieDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieDetailResponse> response) {
                final long endTime = System.currentTimeMillis();
                ArrayList<Movie> data = new ArrayList();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    data.addAll(response.body().response.result);
                }
                if (Utilities.notEmpty(data) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", data.get(0), null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        apiCallback.onComplete();
                    }
                } else {
                    if (apiCallback != null){
                        apiCallback.onError(application.getResources().getString(R.string.movies_info_unavailable));
                        apiCallback.onComplete();
                    }
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_DETAIL, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_DETAIL, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        }, request);
    }

    public void getListGroupsDetail(String idGroup, @Nullable final ApiCallbackV3<ArrayList<Movie>> apiCallback) {
        WsMovieGetListGroupDetailRequest.Request request = new WsMovieGetListGroupDetailRequest.Request();
        request.setRefID(getRefId());
        request.setMsisdn(getMsisdn());
        request.setID(idGroup);
        request.setClientType(Constant.CLIENT_TYPE);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieListGroupDetail(new MPApiCallback<WsMovieGetListGroupDetailResponse>() {
            @Override
            public void onResponse(Response<WsMovieGetListGroupDetailResponse> response) {
                ArrayList<Movie> data = new ArrayList<>();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    data.addAll(response.body().response.result);
                }
//                MovieWatched movieWatched = gson.fromJson(new JSONObject(response).optString("last_viewed"),
//                        new TypeToken<MovieWatched>() {
//                        }.getType());
                MovieWatched movieWatched = null;
                if (movieWatched == null) movieWatched = new MovieWatched();
                if (apiCallback != null) {
                    if (Utilities.notEmpty(data)) {
                        try {
                            apiCallback.onSuccess("", data, movieWatched);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        apiCallback.onError(application.getResources().getString(R.string.movies_info_unavailable));
                    }
                    apiCallback.onComplete();
                }
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);
    }

    public void getRelateFilm(String filmid, String msisdn, @Nullable final ApiCallbackV2<ArrayList<Movie>> apiCallback) {

        WsGetMovieRelatedRequest.Request request = new WsGetMovieRelatedRequest.Request();
        request.setClientType(Constant.CLIENT_TYPE);
        request.setFilmid(filmid);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setMsisdn(msisdn);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setRefID(getRefId());

        homeClient.wsGetMovieRelated(new MPApiCallback<WsMovieGetRelatedResponse>() {
            @Override
            public void onResponse(Response<WsMovieGetRelatedResponse> response) {
                ArrayList<Movie> data = new ArrayList<>();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    data.addAll(response.body().response.result);
                }
                if (Utilities.notEmpty(data) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    apiCallback.onComplete();
                } else {
                    onError(new Throwable(""));
                }
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);

    }

    public void getHotKeyword(@Nullable final ApiCallbackV2<ArrayList<String>> apiCallback) {
        Http.Builder builder = get(domain, GET_TOP_KEYWORD);
        builder.putParameter("limit", "20");
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                ArrayList<String> listKeyword = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.optJSONArray("result");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String keyword = jsonArray.getJSONObject(i).optString("keyword");
                        listKeyword.add(keyword);
                    }
                }
                if (Utilities.notEmpty(listKeyword) && apiCallback != null) {
                    apiCallback.onSuccess("", listKeyword);
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void deleteLogWatched(boolean isDeleteAll, Movie movie, @Nullable final ApiCallbackV2<String> apiCallback) {
        Http.Builder builder = get(domain, DELETE_VIEWED_MOVIES);
        builder.putParameter("id_film", isDeleteAll ? "0" : movie.getId());
        builder.putParameter("id_group", isDeleteAll ? "0" : movie.getIdGroup());
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                int code = jsonObject.optInt("code");
                if (code == 0 || code == 200) {
                    if (apiCallback != null) apiCallback.onSuccess("", "");
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
                UpdateWatchedEvent event = EventHelper.getDefault().getStickyEvent(UpdateWatchedEvent.class);
                if (event == null)
                    event = new UpdateWatchedEvent(true);
                else
                    event.setUpdate(true);
                EventHelper.getDefault().postSticky(event);
            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }
    public void getMaybeYouLike (@Nullable final ApiCallbackV2<ArrayList<HomeData>> apiCallback){
        Http.Builder builder = get(DOMAIN_MOVIE, GET_MAYBE_YOU_LIKE);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.putParameter("limit", "3");
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                JSONObject jsonObject = new JSONObject(response);
                final ArrayList<HomeData> data = gson.fromJson(jsonObject.optString("result"), new TypeToken<ArrayList<HomeData>>() {
                }.getType());
                if (apiCallback != null)
                    apiCallback.onSuccess("", data);
            }
            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getFilmForUser(@Nullable final ApiCallbackV2<ArrayList<HomeData>> apiCallback) {
//        Http.Builder builder = get(DOMAIN_MOVIE, GET_FILM_FOR_USER);
//        builder.putParameter("msisdn", getMsisdn());
//        builder.putParameter("ref_id", getRefId());
//        builder.putParameter("limit", "1000");
//        builder.putParameter("offset", "0");
//
//        builder.withCallBack(new HttpCallBack() {
//            @Override
//            public void onSuccess(String response) throws Exception {
//                JSONObject jsonObject = new JSONObject(response);
//                final ArrayList<HomeData> data = gson.fromJson(jsonObject.optString("result"), new TypeToken<ArrayList<HomeData>>() {
//                }.getType());
//                if (apiCallback != null)
//                    apiCallback.onSuccess("", data);
//            }
//
//            @Override
//            public void onFailure(String message) {
//                if (apiCallback != null) {
//                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
//                }
//            }
//
//            @Override
//            public void onCompleted() {
//                if (apiCallback != null) apiCallback.onComplete();
//            }
//        });
//        builder.setTimeOut(60);
//        builder.execute();

        WsGetListFilmForUserRequest.Request request = new WsGetListFilmForUserRequest.Request();
        request.setMsisdn(getMsisdn());
        request.setRefID(getRefId());
        request.setLimit("1000");
        request.setOffset("0");
        request.setClientType(Constant.CLIENT_TYPE);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setRevision(Config.REVISION);

        homeClient.wsGetListMovieForUser(new MPApiCallback<WsGetListFilmForUserResponse>() {
            @Override
            public void onResponse(Response<WsGetListFilmForUserResponse> response) {
                final List<HomeData> data = response.body().response.result;
                if (apiCallback != null){
                    try {
                        apiCallback.onSuccess("", new ArrayList<>(data));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        apiCallback.onComplete();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);
    }

    public void deleteMovieLiked(boolean isDeleteAll, Movie movie, @Nullable final ApiCallbackV2<String> apiCallback) {
        Http.Builder builder = get(domain, DELETE_LIKED_MOVIES);
        builder.putParameter("filmid", isDeleteAll ? "0" : movie.getId());
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                int code = jsonObject.optInt("code");
                if (code == 0 || code == 200) {
                    if (apiCallback != null) apiCallback.onSuccess("", "");
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getListMovieWatched(int limit, int offset, @Nullable final ApiCallbackV2<ArrayList<MovieWatched>> apiCallback) {
        Http.Builder builder = get(domain, GET_VIEWED_MOVIES);
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null) {
                    ArrayList<MovieWatched> data = gson.fromJson(new JSONObject(response).optString("result"), new TypeToken<ArrayList<MovieWatched>>() {
                    }.getType());
                    if (data == null) data = new ArrayList<>();
                    apiCallback.onSuccess("", data);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getHomeOfSubtab(final SubtabInfo tabInfo, @Nullable final ApiCallbackV2<ArrayList<MovieKind>> apiCallback) {
        final long startTime = System.currentTimeMillis();
        Log.i(TAG, "getHomeOfSubtab: " + tabInfo);
        Http.Builder builder = get(domain, GET_HOME_V3);
        //               builder     .putParameter("isVip", "");
//                 builder   .putParameter("PackageVip", "");
        if (tabInfo != null) {
            builder.putParameter("categoryid", tabInfo.getCategoryId());
//        builder.putParameter("parentid", String.valueOf(tabInfo.getParentId()));
//        builder.putParameter("subTabID", tabInfo.getSubtabId());
//        builder.putParameter("type", String.valueOf(tabInfo.getType()));
//        builder.putParameter("eventID", String.valueOf(tabInfo.getEventId()));
//        builder.putParameter("typeFilmID", String.valueOf(tabInfo.getTypeFilmId()));
//        builder.putParameter("countryID", tabInfo.getCountryId());
        }
        builder.putParameter("revision", Config.REVISION);
        builder.putParameter("version", BuildConfig.VERSION_NAME);
        builder.putParameter("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
        builder.putParameter("device_id", DeviceHelper.getDeviceId(application));
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                long endTime = System.currentTimeMillis();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    ArrayList<MovieKind> data = gson.fromJson(jsonObject.optString("result"), new TypeToken<ArrayList<MovieKind>>() {
                    }.getType());
                    String categoryId = "";
                    if (tabInfo != null) {
                        categoryId = tabInfo.getCategoryId();
                        if (tabInfo.isTabHome())
                            SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_MOVIES_DATA_SUBTAB_HOME, data);
                    }
                    if (apiCallback != null) apiCallback.onSuccess(categoryId, data);
                } catch (OutOfMemoryError e) {
                    if (apiCallback != null)
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                } catch (Exception e) {
                    if (apiCallback != null)
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onFailure(String message) {
                long endTime = System.currentTimeMillis();

                Log.d(TAG, "onFailure: " + message);
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getListMovieBySubtabInfo(SubtabInfo tabInfo, int offset, int limit, @Nullable final ApiCallbackV2<ArrayList<Object>> apiCallback) {
        Http.Builder builder = null;
        if (tabInfo != null) {
            if (tabInfo.isSubtab()) {
                if (tabInfo.getType() == MovieKind.TYPE_LOCAL) {
                    if (MovieKind.CATEGORYID_GET_LIKED.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_LIKED_MOVIES);
                    } else if (MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_VIEWED_MOVIES);
                    } else if (MovieKind.CATEGORYID_GET_LATER.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_LIST_WATCH_LATER);
                    } else if (MovieKind.CATEGORYID_GET_LIKED_ODD.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_LIKED_MOVIES);
                        builder.putParameter("type", "1");
                    } else if (MovieKind.CATEGORYID_GET_LIKED_SERIES.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_LIKED_MOVIES);
                        builder.putParameter("type", "2");
                    }
                } else {
                    if (MovieKind.TYPE_HORIZONTAL == tabInfo.getType()
                            && MovieKind.CATEGORYID_GET_NEW.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_NEW_MOVIES);
                    } else if (MovieKind.TYPE_HORIZONTAL == tabInfo.getType()
                            && MovieKind.CATEGORYID_GET_HOT.equals(tabInfo.getCategoryId())) {
                        builder = get(domain, GET_HOT_MOVIES);
                    } else {
                        builder = get(domain, GET_MOVIES_OF_CATEGORY);
                    }
                    builder.putParameter("type", String.valueOf(tabInfo.getType()));
                }
            } else {
                int type = tabInfo.getType();
                if (type == SubtabInfo.TYPE_CATEGORY_DETAIL)
                    builder = get(domain, GET_DETAIL_CATEGORY);
                else if (type == SubtabInfo.TYPE_COUNTRY_DETAIL)
                    builder = get(domain, GET_DETAIL_COUNTRY);
                else if (type == SubtabInfo.TYPE_ODD)
                    builder = get(domain, GET_ODD_MOVIES);
                else if (type == SubtabInfo.TYPE_SERIES)
                    builder = get(domain, GET_LIST_SERIES_MOVIES);
                else if (type == SubtabInfo.TYPE_VIEW_MORE)
                    builder = get(domain, GET_HOT_MOVIES);
                else if (type == SubtabInfo.TYPE_NEW)
                    builder = get(domain, GET_NEW_MOVIES);
            }
        }
        if (builder != null) {
//               builder     .putParameter("isVip", "");
//                 builder   .putParameter("PackageVip", "");
            if (tabInfo.getType() != MovieKind.TYPE_LOCAL) {
                builder.putParameter("categoryid", tabInfo.getCategoryId());
                builder.putParameter("parentid", String.valueOf(tabInfo.getParentId()));
                builder.putParameter("subTabID", tabInfo.getSubtabId());
                builder.putParameter("eventID", String.valueOf(tabInfo.getEventId()));
                builder.putParameter("typeFilmID", String.valueOf(tabInfo.getTypeFilmId()));
                builder.putParameter("countryID", tabInfo.getCountryId());
            }
            builder.putParameter("offset", String.valueOf(offset));
            builder.putParameter("limit", String.valueOf(limit));
            builder.putParameter("password", USER_MOVIE);
            builder.putParameter("username", PASS_MOVIE);
            builder.putParameter("revision", Config.REVISION);
            builder.putParameter("version", BuildConfig.VERSION_NAME);
            builder.putParameter("client_type", Constants.HTTP.CLIENT_TYPE_STRING);
            builder.putParameter("device_id", DeviceHelper.getDeviceId(application));
            builder.putParameter("Platform", Constants.HTTP.PLATFORM);
            builder.putParameter("msisdn", getMsisdn());
            builder.withCallBack(new HttpCallBack() {
                @Override
                public void onSuccess(String response) throws Exception {
                    JSONObject jsonObject = new JSONObject(response);
                    ArrayList<Object> data;
                    if (tabInfo != null && tabInfo.getType() == MovieKind.TYPE_LOCAL
                            && MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId())) {
                        data = gson.fromJson(jsonObject.optString("result"), new TypeToken<ArrayList<MovieWatched>>() {
                        }.getType());
                    } else {
                        data = gson.fromJson(jsonObject.optString("result"), new TypeToken<ArrayList<Movie>>() {
                        }.getType());
                    }
                    if (apiCallback != null) apiCallback.onSuccess("", data);
                }

                @Override
                public void onFailure(String message) {
                    Log.d(TAG, "onFailure: " + message);
                    if (apiCallback != null)
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }

                @Override
                public void onCompleted() {
                    if (apiCallback != null) apiCallback.onComplete();
                }
            }).setTimeOut(30);
            builder.execute();
        } else if (apiCallback != null) {
            try {
                apiCallback.onSuccess("", null);
                apiCallback.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void insertLogWatched(MovieWatched movie) {
        if (movie == null) return;
        if (getReengAccountBusiness().isVietnam() && (movie.getTimeSeekInt() <= 180 || (movie.getDurationInt() - movie.getTimeSeekInt() <= 300) || getReengAccountBusiness().isAnonymousLogin()))
            return;
        //thời gian user đã xem lớn hơn > 3 phút
        //thời gian còn lại (tổng thời gian phim – thời gian mà user dừng xem) > 5 phút
        Http.Builder builder = get(domain, INSERT_VIEWED_MOVIES);
        builder.putParameter("current_film", movie.getCurrentEpisode());
        builder.putParameter("id_film", movie.getId());
        builder.putParameter("id_group", movie.getIdGroup());
        builder.putParameter("id_part", movie.getIdPart());
        builder.putParameter("image_path", movie.getImagePath());
        builder.putParameter("poster_path", movie.getPosterPath());
        builder.putParameter("name", movie.getName());
        builder.putParameter("time_seek", movie.getTimeSeek());
        builder.putParameter("duration", movie.getDuration());
        builder.putParameter("type_film", movie.getTypeFilm());
        builder.putParameter("total", movie.getTotalEpisodes());
        builder.putParameter("link_wap", movie.getLink());
        builder.putParameter("throughput", movie.getThroughput());
        builder.putParameter("total_time_play", movie.getTotalTimePlay());
        builder.putParameter("uuid", Utilities.getUuidApp());
        builder.putParameter("countryCode", getReengAccountBusiness().getRegionCode());
        builder.putParameter("languageCode", getReengAccountBusiness().getCurrentLanguage());
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Log.i(TAG, "onSuccess insertLogWatched");
            }

            @Override
            public void onFailure(String message) {
                Log.i(TAG, "onFailure insertLogWatched");
            }

            @Override
            public void onCompleted() {
                Log.i(TAG, "onCompleted insertLogWatched");
                UpdateWatchedEvent event = EventHelper.getDefault().getStickyEvent(UpdateWatchedEvent.class);
                if (event == null)
                    event = new UpdateWatchedEvent(true);
                else
                    event.setUpdate(true);
                EventHelper.getDefault().postSticky(event);
            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    /***
     * @param  itemId: id of film.
     * */
    public void getUserDrmInfo(String itemId, final ApiCallbackV2<DrmInfo> apiCallback) {
        if (!NetworkHelper.isConnectInternet(application)) {
            if (apiCallback != null)
                apiCallback.onError(application.getResources().getString(R.string.error_internet_disconnect));
            return;
        }
        Http.Builder builder = get(domain, GET_INFO_DRM);
        builder.putParameter("ItemmovieID", itemId);
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
                                 @Override
                                 public void onSuccess(String data) throws Exception {
                                     JSONObject jsonObject = new JSONObject(data);
                                     if (jsonObject.has("result")) {
                                         String resultTmp = jsonObject.optString("result");
                                         DrmInfo result = gson.fromJson(resultTmp, new TypeToken<DrmInfo>() {
                                         }.getType());
                                         if (apiCallback != null) apiCallback.onSuccess("", result);
                                         return;
                                     }
                                     onFailure(application.getResources().getString(R.string.e601_error_but_undefined));
                                 }

                                 @Override
                                 public void onFailure(String message) {
                                     if (apiCallback != null) apiCallback.onError(message);
                                 }

                                 @Override
                                 public void onCompleted() {
                                     if (apiCallback != null) apiCallback.onComplete();
                                 }
                             }
        );
        builder.setTimeOut(60);
        builder.execute();
    }

    /***
     * @param  deviceId: deviceId was got from DRM library. (WindmillConfiguration.deviceId)
     * */
    public void addDeviceDrm(String deviceId, final ApiCallbackV2<String> apiCallback) {
        if (!NetworkHelper.isConnectInternet(application)) {
            if (apiCallback != null)
                apiCallback.onError(application.getResources().getString(R.string.error_internet_disconnect));
            return;
        }
        String deviceType = "WEB_ANDROID";
        String entityType = "DEVICE";
        String smsPackageId = "MEDIA_KEENG";
        String smsNetworkId = "OTT";
        String source = "KEENG";
        String prefix = "KEENG_";
        String Command = "Add";

        Http.Builder builder = get(domain, ADD_DEVICE_DRM);
        builder.putParameter("networkDeviceId", deviceId);
        builder.putParameter("deviceType", deviceType);
        builder.putParameter("entityType", entityType);
        builder.putParameter("smsPackageId", smsPackageId);
        builder.putParameter("smsNetworkId", smsNetworkId);
        builder.putParameter("source", source);
        builder.putParameter("prefix", prefix);
        builder.putParameter("smsDeviceId", deviceId);
        builder.putParameter("Command", Command);
        builder.putParameter("isVIP", "0");
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
                                 @Override
                                 public void onSuccess(String data) throws Exception {
                                     JSONObject jsonObject = new JSONObject(data);
                                     int code = jsonObject.optInt("code");
                                     if (code == 0 || code == 200) {
                                         if (apiCallback != null) apiCallback.onSuccess("", data);
                                     } else {
                                         onFailure("");
                                     }
                                 }

                                 @Override
                                 public void onFailure(String message) {
                                     if (apiCallback != null) apiCallback.onError(message);
                                 }

                                 @Override
                                 public void onCompleted() {
                                     if (apiCallback != null) apiCallback.onComplete();

                                 }
                             }

        );
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getListSubtab(final SubtabInfo tabInfo, @Nullable final ApiCallbackV2<ArrayList<SubtabInfo>> apiCallback) {
        final long startTime = System.currentTimeMillis();
        Http.Builder builder;
        if (tabInfo == null) {
            builder = get(domain, GET_CATEGORY_V3);
            builder.putParameter("categoryid", "0");
        } else {
            if (tabInfo.isSubtab()) {
                builder = get(domain, GET_CATEGORY_V3);
                builder.putParameter("categoryid", tabInfo.getCategoryId());
                builder.putParameter("parentid", String.valueOf(tabInfo.getParentId()));
                builder.putParameter("subTabID", tabInfo.getSubtabId());
                builder.putParameter("type", String.valueOf(tabInfo.getType()));
                builder.putParameter("eventID", String.valueOf(tabInfo.getEventId()));
                builder.putParameter("typeFilmID", String.valueOf(tabInfo.getTypeFilmId()));
                builder.putParameter("countryID", tabInfo.getCountryId());
            } else {
                int type = tabInfo.getType();
                if (type == SubtabInfo.TYPE_LIST_CATEGORIES)
                    builder = get(domain, GET_CATEGORY);
                else if (type == SubtabInfo.TYPE_LIST_COUNTRIES)
                    builder = get(domain, GET_COUNTRY);
                else
                    builder = get(domain, GET_CATEGORY);
            }
        }
        if (builder != null) {
            builder.putParameter("Platform", Constants.HTTP.PLATFORM);
            builder.putParameter("msisdn", getMsisdn());
            builder.withCallBack(new HttpCallBack() {
                @Override
                public void onSuccess(String response) throws Exception {
                    long endTime = System.currentTimeMillis();
                    Log.d(TAG, "loadData onSuccress: " + response);
                    LogDebugHelper.getInstance().logDebugContent("loadData onSuccress: " + response);
                    JSONObject jsonObject = new JSONObject(response);
                    final ArrayList<SubtabInfo> data = gson.fromJson(jsonObject.optString("result"), new TypeToken<ArrayList<SubtabInfo>>() {
                    }.getType());
                    if (tabInfo == null) {
                        ApplicationController.self().setLoadedSubtabHomeMovie(true);
                        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_MOVIES_SUBTAB_HOME_LIST, data);
                    }
                    if (apiCallback != null) apiCallback.onSuccess("", data);
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                }

                @Override
                public void onFailure(String message) {
                    long endTime = System.currentTimeMillis();
                    Log.d(TAG, "loadData onFailure: " + message);
                    LogDebugHelper.getInstance().logDebugContent("loadData onFailure: " + message);
                    if (apiCallback != null)
                        apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
                }

                @Override
                public void onCompleted() {
                    Log.d(TAG, "loadData onCompleted");
                    LogDebugHelper.getInstance().logDebugContent("loadData onCompleted");
                    if (apiCallback != null) apiCallback.onComplete();
                }
            });
            //builder.setTag(Constants.TabMovies.TAG_GET_LIST_SUBTAB);
            builder.setTimeOut(60);
            builder.execute();
        }
    }

    public void insertWatchLater(Movie movie, ApiCallbackV2<String> apiCallback) {
        if (movie == null)
            return;
        Http.Builder builder = get(domain, INSERT_WATCH_LATER);
        builder.putParameter("current_film", String.valueOf(movie.getCurrentEpisode()));
        builder.putParameter("id_film", movie.getId());
        builder.putParameter("id_group", movie.getIdGroup());
        //builder.putParameter("id_part", "";
        builder.putParameter("image_path", movie.getImagePath());
        builder.putParameter("poster_path", movie.getPosterPath());
        builder.putParameter("name", movie.getName());
        builder.putParameter("type_film", movie.getTypeFilm());
        builder.putParameter("total", String.valueOf(movie.getTotalEpisodes()));
        builder.putParameter("duration", movie.getDuration());
        builder.putParameter("link_wap", movie.getLinkWap());
        builder.putParameter("is_subtitle", movie.getSubtitle());
        builder.putParameter("is_narrative", movie.getNarrative());
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                int code = jsonObject.optInt("code");
                if (code == 0 || code == 200) {
                    if (apiCallback != null) apiCallback.onSuccess("", data);
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) apiCallback.onError(message);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();

            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void deleteWatchLater(boolean isDeleteAll, Movie movie, @Nullable final ApiCallbackV2<String> apiCallback) {
        Http.Builder builder = get(domain, DELETE_WATCH_LATER);
        builder.putParameter("id_film", isDeleteAll ? "0" : movie.getId());
        builder.putParameter("id_group", isDeleteAll ? "0" : movie.getIdGroup());
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject jsonObject = new JSONObject(data);
                int code = jsonObject.optInt("code");
                if (code == 0 || code == 200) {
                    if (apiCallback != null) apiCallback.onSuccess("", data);
                } else {
                    onFailure("");
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) apiCallback.onError(message);
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();

            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getListMovieWatchLater(int limit, int offset, @Nullable final ApiCallbackV2<ArrayList<MovieWatched>> apiCallback) {
        Http.Builder builder = get(domain, GET_LIST_WATCH_LATER);
        builder.putParameter("limit", String.valueOf(limit));
        builder.putParameter("offset", String.valueOf(offset));
        builder.putParameter("username", USER_MOVIE);
        builder.putParameter("password", PASS_MOVIE);
        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getMsisdn());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null) {
                    ArrayList<MovieWatched> data = gson.fromJson(new JSONObject(response).optString("result"), new TypeToken<ArrayList<MovieWatched>>() {
                    }.getType());
                    if (data == null) data = new ArrayList<>();
                    apiCallback.onSuccess("", data);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    // api for notification
    public void getMovieDetail(String filmId, @Nullable final ApiCallbackV2<Movie> apiCallback) {
        final long startTime = System.currentTimeMillis();
        WsGetMovieDetailRequest.Request request = new WsGetMovieDetailRequest.Request();
        request.setFilmid(filmId);
        request.setMsisdn(getMsisdn());
        request.setRefID(getRefId());
        request.setClientType(Constant.CLIENT_TYPE);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieDetail(new MPApiCallback<WsGetMovieDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieDetailResponse> response) {
                final long endTime = System.currentTimeMillis();
                ArrayList<Movie> data = new ArrayList();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    data.addAll(response.body().response.result);
                }
                // kieu du lieu la boolean nhung api tra ve kieu int, can cast ve boolean
//                JSONObject ob = new JSONObject(response);
//                if (ob.getJSONArray("result").getJSONObject(0).getInt("is_liked") == 1) {
//                    data.get(0).setLike(true);
//                } else {
//                    data.get(0).setLike(false);
//                }

                if (Utilities.notEmpty(data) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", data.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        apiCallback.onComplete();
                    }
                } else {
                    if (apiCallback != null){
                        apiCallback.onError(application.getResources().getString(R.string.movies_info_unavailable));
                        apiCallback.onComplete();
                    }
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_DETAIL, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.MOVIE_GET_DETAIL, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            }
        }, request);


    }

    public void likeVideo(String videoId, @Nullable final ApiCallbackV2<BaseResponse> apiCallback) {
        Http.Builder builder = post(domain, LIKE_VIDEO);
        builder.putParameter("video_id", videoId);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                BaseResponse baseResponse = gson.fromJson(response, BaseResponse.class);
                if (apiCallback != null) {
                    apiCallback.onSuccess("", baseResponse);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void unLikeVideo(String videoId, @Nullable final ApiCallbackV2<BaseResponse> apiCallback) {
        Http.Builder builder = post(domain, UNLIKE_VIDEO);
        builder.putParameter("video_id", videoId);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                BaseResponse baseResponse = gson.fromJson(response, BaseResponse.class);
                if (apiCallback != null) {
                    apiCallback.onSuccess("", baseResponse);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void addFavoriteCategory(String categoryid, @Nullable final ApiCallbackV2<BaseResponse> apiCallback) {
        Http.Builder builder = post(domain, ADD_FAVORITE_CATEGORY);
        builder.putParameter("categoryid", categoryid);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                BaseResponse baseResponse = gson.fromJson(response, BaseResponse.class);
                if (apiCallback != null) {
                    apiCallback.onSuccess("", baseResponse);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void addFavoriteCountry(String countryid, @Nullable final ApiCallbackV2<BaseResponse> apiCallback) {
        Http.Builder builder = post(domain, ADD_FAVORITE_COUNTRY);
        builder.putParameter("countryid", countryid);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                BaseResponse baseResponse = gson.fromJson(data, BaseResponse.class);
                if (apiCallback != null) {
                    apiCallback.onSuccess("", baseResponse);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(message);
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }
        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void rateFilm(String videoId, float rate, @Nullable final ApiCallbackV2<BaseResponse> apiCallback) {
        Http.Builder builder = post(domain, RATE_VIDEO);
        builder.putParameter("filmid", videoId);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.putParameter("rate", String.valueOf(rate));
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                BaseResponse baseResponse = gson.fromJson(response, BaseResponse.class);
                apiCallback.onSuccess("", baseResponse);
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getListFilmLiked(@Nullable final ApiCallbackV2<ArrayList<Movie2>> apiCallback) {
        Http.Builder builder = get(domain, GET_LIST_FILM_LIKED);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.putParameter("limit", "10");
        builder.putParameter("offset", "0");
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                JSONObject jsonObject = new JSONObject(response);
                List<Movie2> movies = gson.fromJson(jsonObject.optString("result"), new TypeToken<List<Movie2>>() {
                }.getType());
                if (apiCallback != null) {
                    apiCallback.onSuccess("", (ArrayList<Movie2>) movies);
                }
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void insertLogWatch(String filmId, long timeSeek, @Nullable final ApiCallbackV2<BaseResponse> apiCallback) {
        Http.Builder builder = post(domain, INSERT_LOG_WATCHED);
        builder.putParameter("id_film", filmId);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.putParameter("time_seek", String.valueOf(timeSeek));
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                BaseResponse baseResponse = gson.fromJson(response, BaseResponse.class);
                apiCallback.onSuccess("", baseResponse);
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getTopicCountry(final ApiCallbackV2<ArrayList<Country>> apiCallback) {
        final long startTime = System.currentTimeMillis();
        WsGetMovieCountryRequest.Request request = new WsGetMovieCountryRequest.Request();
        request.offset = "0";
        request.limit = "1000";

        homeClient.wsGetMovieCountry(new MPApiCallback<WsGetMovieCountryResponse>() {
            @Override
            public void onResponse(Response<WsGetMovieCountryResponse> response) {
                final long endTime = System.currentTimeMillis();
                ArrayList<Country> countries = new ArrayList();
                if(response.body() != null && response.body().result != null){
                    countries = new ArrayList(response.body().result);
                }

                if (Utilities.notEmpty(countries) && apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", countries);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (apiCallback != null)
                        apiCallback.onError(application.getResources().getString(R.string.movies_info_unavailable));
                }

                apiCallback.onComplete();

                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_COUNTRY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            }

            @Override
            public void onError(Throwable error) {
                final long endTime = System.currentTimeMillis();
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.CINEMA_GET_COUNTRY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());

            }
        }, request);
    }

    public void getTopicCategory(@Nullable final ApiCallbackV2<ArrayList<CategoryModel>> apiCallback) {
        Http.Builder builder = get(domain, GET_TOPIC);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                JSONObject jsonObject = new JSONObject(response);
                ArrayList<CategoryModel> categoryModels = gson.fromJson(jsonObject.optString("result"), new TypeToken<List<CategoryModel>>() {
                }.getType());
                apiCallback.onSuccess("", categoryModels);
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getListLogWatch(ApiCallbackV2<ListLogWatchResponse> apiCallback) {

        WsGetListLogWatchedRequest.Request request = new WsGetListLogWatchedRequest.Request();
        request.setMsisdn(getMsisdn());
        request.setRefID(getRefId());
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setClientType(Constants.HTTP.CLIENT_TYPE_STRING);
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setRevision(Config.REVISION);
        homeClient.wsGetListLogWatched(new MPApiCallback<WsGetListLogWatchedResponse>() {
            @Override
            public void onResponse(Response<WsGetListLogWatchedResponse> response) {
                ListLogWatchResponse watchResponse = new ListLogWatchResponse();
                if(response.body() != null && response.body().response != null){
                    watchResponse = response.body().response;
                }
                try {
                    apiCallback.onSuccess("", watchResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finally {
                    if (apiCallback != null) {
                        apiCallback.onComplete();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);
    }

    public Http getListLogWatchFriend(String phoneNumber,ApiCallbackV2<ListLogWatchResponse> apiCallback) {
        Http.Builder builder = get(domain, GET_LIST_LOG_WATCH);
        builder.putParameter("msisdn", phoneNumber);
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                ListLogWatchResponse watchResponse = gson.fromJson(response, ListLogWatchResponse.class);
                apiCallback.onSuccess("", watchResponse);
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        return builder.execute();
    }

    public void getListFilmOfCategory(String categoryId, int page, int limit, ApiCallbackV2<ActionFilmModel> apiCallback) {
        WsMovieGetListFilmOfCategoryRequest.Request request = new WsMovieGetListFilmOfCategoryRequest.Request();
        request.setCategoryid(categoryId);
        request.setClientType(Constant.CLIENT_TYPE);
        request.setLanguage(LanguageUtils.getCurrentLocale().getLanguage());
        request.setMsisdn(getMsisdn());
        request.setRefID(getRefId());
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieListFilmOfCategory(new MPApiCallback<WsMovieGetListFilmOfCategoryResponse>() {
            @Override
            public void onResponse(Response<WsMovieGetListFilmOfCategoryResponse> response) {
                ActionFilmModel actionFilmModel = new ActionFilmModel();
                if(response.body() != null && response.body().response != null){
                    actionFilmModel = response.body().response;
                }
                try {
                    apiCallback.onSuccess("", actionFilmModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                apiCallback.onComplete();
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);
    }

    public void getListFilmByInfo(String info, ApiCallbackV2<ActionFilmModel> apiCallback) {
        Http.Builder builder = get(domain, GET_LIST_FILM_BY_INFO);
        builder.putParameter("msisdn", getMsisdn());
        builder.putParameter("ref_id", getRefId());
        builder.putParameter("info", info);
        builder.putParameter("limit", "10");
        builder.putParameter("offset", "0");
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                ActionFilmModel actionFilmModel = gson.fromJson(response, ActionFilmModel.class);
                apiCallback.onSuccess("", actionFilmModel);
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }

    public void getCategoryByIds(String[] ids, ApiCallbackV2<List<Category>> apiCallback) {
        String data = "";
        if (ids != null && ids.length > 0) {
            for (int index = 0; index < ids.length; index++) {
                if (index < ids.length - 1) {
                    data += ids[index] + ",";
                } else {
                    data += ids[index];
                }
            }
        }

        WsMovieGetCategoryByIdsRequest.Request request = new WsMovieGetCategoryByIdsRequest.Request();
        request.setClientType(Constants.HTTP.CLIENT_TYPE_STRING);
        request.setIDS(data);
        request.setMsisdn(getMsisdn());
        request.setPlatform(Constants.HTTP.PLATFORM);
        request.setRevision(Config.REVISION);

        homeClient.wsGetMovieCategoryByIds(new MPApiCallback<WsMovieGetCategoryByIdsResponse>() {
            @Override
            public void onResponse(Response<WsMovieGetCategoryByIdsResponse> response) {
                List<Category> categoryResponse = new ArrayList<>();
                if(response.body() != null && response.body().response != null && response.body().response.result != null){
                    categoryResponse = response.body().response.result;
                }
                if (apiCallback != null) {
                    try {
                        apiCallback.onSuccess("", categoryResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    apiCallback.onComplete();
                }
            }

            @Override
            public void onError(Throwable error) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                    apiCallback.onComplete();
                }
            }
        }, request);
    }

    public void getTopCountry(String countryId, int page, int limit, @Nullable final ApiCallbackV2<ActionFilmModel> apiCallback) {
        Http.Builder builder = get(domain, GET_TOP_COUNTRY);
        builder.putParameter("categoryid", countryId);
        builder.putParameter("offset", String.valueOf((page - 1) * limit));
        builder.putParameter("limit", String.valueOf(limit));
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                ActionFilmModel actionFilmModel = gson.fromJson(response, ActionFilmModel.class);
                apiCallback.onSuccess("", actionFilmModel);
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null) {
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
                }
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) {
                    apiCallback.onComplete();
                }
            }

        });
        builder.setTimeOut(60);
        builder.execute();
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsCheckOtpIshareRequest extends BaseRequest<WsCheckOtpIshareRequest.Request> {
    public class Request {
        @SerializedName("isdnSender")
        @Expose
        private String isdnSender;
        @SerializedName("isdnReceiver")
        @Expose
        private String isdnReceiver;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("amount")
        @Expose
        private Integer amount;
        @SerializedName("language")
        @Expose
        private String language;

        public String getIsdnSender() {
            return isdnSender;
        }

        public void setIsdnSender(String isdnSender) {
            this.isdnSender = isdnSender;
        }

        public String getIsdnReceiver() {
            return isdnReceiver;
        }

        public void setIsdnReceiver(String isdnReceiver) {
            this.isdnReceiver = isdnReceiver;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public Integer getAmount() {
            return amount;
        }

        public void setAmount(Integer amount) {
            this.amount = amount;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

    }
}

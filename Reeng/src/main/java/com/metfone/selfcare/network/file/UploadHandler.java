package com.metfone.selfcare.network.file;

import android.content.res.Resources;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.SettingBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.network.file.UploadQueue.CallBackDelivery;
import com.metfone.selfcare.network.okhttp.ProgressRequestBody;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by toanvk2 on 7/26/2016.
 */
public class UploadHandler {
    private static final String TAG = UploadHandler.class.getSimpleName();
    private ApplicationController mApplication;
    private CallBackDelivery mDelivery;
    private long mContentLength;
    private int currentProgress;
    private boolean isUtf8Charset = true;
    private HttpURLConnection httpConn = null;
    private byte[] boundaryBytes;
    private byte[] trailerBytes;
    private String fileName, formData, contentType;
    private File file;
    private long t;

    public UploadHandler(ApplicationController application, CallBackDelivery callBackDelivery) {
        this.mApplication = application;
        this.mDelivery = callBackDelivery;
    }

    public void upload(UploadRequest request) {
        t = System.currentTimeMillis();
        updateUploadState(request, Constants.STATUS_STARTED);
        mDelivery.postUploadStarted(request);
        file = checkFile(request);
        if (file == null || !file.exists()) {
            updateUploadFailed(request, Constants.ERROR_FILE_ERROR, "Could not find file at path: " + request
                    .getFilePath());
        } else {
            initUpload(request, file);
            executeUpload(request);
        }
        Log.d(TAG, "end upload");
    }

    private void executeUpload(UploadRequest request) {
//        String url = "https://pvvip.mocha.com.vn/api/media/upload";
        if (isCancelled(request)) return;
        URL mUrl;
        try {
            Log.i(TAG, "url: " + request.getUrl());
            mUrl = new URL(request.getUrl());
        } catch (MalformedURLException e) {
            Log.e(TAG, "Exception", e);
            updateUploadFailed(request, Constants.ERROR_MALFORMED_URI, "MalformedURLException: URI passed is " +
                    "malformed.");
            return;
        }
        try {
//            File sourceFile = new File(request.getFilePath());
            Log.d(TAG, "------------------File...::::" + file + " : " + file.exists());

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            if (!request.getCustomParams().isEmpty()) {
                for (Map.Entry<String, String> entry : request.getCustomParams().entrySet()) {
                    Log.i(TAG, "entry key: " + entry.getKey() + " value: " + entry.getValue());
                    builder.addFormDataPart(entry.getKey(), entry.getValue());
                }
            }
            Log.i(TAG, "formdata: " + formData + " | fileName: " + fileName + " | contentType: "
                    + contentType + " | " + "source: " + file.getAbsolutePath());
            builder.addFormDataPart(formData, fileName, RequestBody.create(MediaType.parse(contentType), file));

            ProgressRequestBody progressRequestBody = new ProgressRequestBody(builder.build(), request, mDelivery);

            Request mRequest = new Request.Builder()
                    .url(mUrl)
                    .post(progressRequestBody)
                    .build();
            OkHttpClient client = new OkHttpClient();
            Response mResponse = client.newCall(mRequest).execute();
            processResponse(request, mResponse);
            //GA
            Resources mRes = mApplication.getResources();
            mApplication.trackingSpeed(mRes.getString(R.string.ga_category_speed_file),
                    mRes.getString(R.string.ga_label_speed_up),
                    (System.currentTimeMillis() - t));
        } catch (SocketTimeoutException e) {
            Log.f(TAG, "SocketTimeoutException", e);
            attemptRetryOnTimeOutException(request);// Retry.
        } catch (IOException e) {
            Log.f(TAG, "IOException", e);
            attemptRetryOnTimeOutException(request);// Retry.
        } catch (Exception e) {
            Log.f(TAG, "Exception", e);
            attemptRetryOnTimeOutException(request);// Retry.
            //updateUploadFailed(-1, "Exception unknown");
        }
    }

    private void initUpload(UploadRequest request, File file) {
        String boundary = "UploadFile " + System.currentTimeMillis();
        boundaryBytes = (Constants.LINE_FEED + Constants.TWO_HYPHENS + boundary + Constants.LINE_FEED).getBytes
                (Constants.US_ASCII);
        trailerBytes = (Constants.LINE_FEED + Constants.TWO_HYPHENS + boundary + Constants.TWO_HYPHENS + Constants
                .LINE_FEED).getBytes(Constants.US_ASCII);
        contentType = request.getContentType();
        if (TextUtils.isEmpty(contentType)) {
            contentType = detectMimeType(file);
        }
        fileName = request.getFileName();
        if (TextUtils.isEmpty(fileName)) {
            fileName = file.getName();
        }
        formData = request.getFormData();
        if (TextUtils.isEmpty(formData)) {
            formData = "data";
        }
        try {
            mContentLength = getTotalLength(request, file);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            mContentLength = 0;
        }
    }

    private File checkFile(UploadRequest request) {
        if (TextUtils.isEmpty(request.getFilePath())) return null;
        File file = new File(request.getFilePath());
        if (file.exists()) {
            if (request.getContainer() != null && request.getContainer() instanceof ReengMessage) {
                ReengMessage message = (ReengMessage) request.getContainer();
                if (message.getMessageType() == ReengMessageConstant.MessageType.image &&
                        file.length() > com.metfone.selfcare.helper.Constants.MESSAGE.MAX_TRANSFER_PHOTO_SIZE) {
                    boolean isHD = SettingBusiness.getInstance(mApplication).getPrefEnableHDImage();
                    String compressedFilePath = MessageHelper.getPathOfCompressedFile(
                            request.getFilePath(), Config.Storage.IMAGE_COMPRESSED_FOLDER, message.getPacketId(), isHD);
                    if (compressedFilePath != null)
                        return new File(compressedFilePath);
                }
            }
            return file;
        }
        return null;
    }

    private boolean isCancelled(UploadRequest request) {
        if (request.isCancelled()) {
            updateUploadFailed(request, Constants.ERROR_CANCELLED, "UploadManager cancelled");
            return true;
        }
        return false;
    }

    private void processResponse(UploadRequest request, Response mResponse) throws Exception {
        if (isCancelled(request)) return;
        // checks server's status code first
        int responseCode = mResponse.code();
        Log.d(TAG, "responseCode: " + responseCode);
        String errorMsg = "";
        switch (responseCode) {
            case HttpURLConnection.HTTP_PARTIAL:
            case HttpURLConnection.HTTP_OK:
                String response = "";
                if (mResponse.body() != null)
                    response = mResponse.body().string();
                Log.i(TAG, "processResponse response: " + response);
                if (TextUtils.isEmpty(response)) {
                    updateUploadFailed(request, HttpURLConnection.HTTP_INTERNAL_ERROR, "response empty");
                } else {
                    updateUploadComplete(request, response);
                }
                break;
            case Constants.HTTP_REQUESTED_RANGE_NOT_SATISFIABLE:
                if (httpConn != null) errorMsg = httpConn.getResponseMessage();
                updateUploadFailed(request, Constants.HTTP_REQUESTED_RANGE_NOT_SATISFIABLE, errorMsg);
                break;
            case HttpURLConnection.HTTP_UNAVAILABLE:
                if (httpConn != null) errorMsg = httpConn.getResponseMessage();
                updateUploadFailed(request, HttpURLConnection.HTTP_UNAVAILABLE, errorMsg);
                break;
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                if (httpConn != null) errorMsg = httpConn.getResponseMessage();
                updateUploadFailed(request, HttpURLConnection.HTTP_INTERNAL_ERROR, errorMsg);
                break;
            default:
                if (httpConn != null) errorMsg = httpConn.getResponseMessage();
                updateUploadFailed(request, Constants.ERROR_UNHANDLED_HTTP_CODE, "Unhandled HTTP response:" +
                        responseCode + " message:" + errorMsg);
                break;
        }
    }

    private String detectMimeType(File file) {
        String extension = null;
        String absolutePath = file.getAbsolutePath();
        int index = absolutePath.lastIndexOf(".") + 1;
        if (index <= absolutePath.length()) {
            extension = absolutePath.substring(index);
        }
        if (extension == null || extension.isEmpty()) {
            return Constants.APPLICATION_OCTET_STREAM;
        }
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        if (mimeType == null) {
            return Constants.APPLICATION_OCTET_STREAM;
        }
        return mimeType;
    }

    private void attemptRetryOnTimeOutException(final UploadRequest request) {
        Log.d(TAG, "attemptRetryOnTimeOutException: " + request.getFilePath());
        try {
            if (httpConn != null) httpConn.disconnect();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        updateUploadState(request, Constants.STATUS_RETRYING);
        RetryPolicy retryPolicy = request.getRetryPolicy();
        try {
            retryPolicy.retry();
            /*SystemClock.sleep(5000);
            executeUpload(request);*/
            synchronized (this) {
                wait(5000);//5s
                executeUpload(request);
            }
            //mDelivery.postRetryRequest(request, retryPolicy.getCurrentTimeout());
        } catch (RetryException | InterruptedException e) {
            Log.e(TAG, "Exception", e);
            updateUploadFailed(request, Constants.ERROR_CONNECTION_TIMEOUT_AFTER_RETRIES,
                    "Connection time out after maximum retires attempted");
        }
    }

    public void updateUploadState(UploadRequest request, int state) {
        request.setUploadState(state);
    }

    public void updateUploadComplete(UploadRequest request, String response) {
        mDelivery.postUploadComplete(request, response);
        request.setUploadState(Constants.STATUS_SUCCESSFUL);
    }

    public void updateUploadFailed(UploadRequest request, int errorCode, String errorMsg) {
        Log.f(TAG, "Upload File Error: " + errorMsg);
        request.setUploadState(Constants.STATUS_FAILED);
        mDelivery.postUploadFailed(request, errorCode, errorMsg);
    }

    public void updateUploadProgress(UploadRequest request, int progress, long uploadedBytes, long speed) {
        if (progress != currentProgress) {
            currentProgress = progress;
            mDelivery.postProgressUpdate(request, mContentLength, uploadedBytes, progress, speed);
        }
    }

    private long getTotalLength(UploadRequest request, File file) throws UnsupportedEncodingException {
        // get the content length of the entire HTTP/Multipart request body
        return (getRequestParametersLength(request) + getFilesLength(file) + trailerBytes.length);
    }

    private long getRequestParametersLength(UploadRequest request) throws UnsupportedEncodingException {
        long size = 0;
        if (!request.getCustomParams().isEmpty()) {
            for (Map.Entry<String, String> entry : request.getCustomParams().entrySet()) {
                size += boundaryBytes.length + getMultipartParam(entry.getKey(), entry.getValue()).length;
            }
        }
        return size;
    }

    private long getFilesLength(File file) throws UnsupportedEncodingException {
        long size = 0;
        size += boundaryBytes.length;
        size += getMultipartHeader().length;
        size += file.length();
        return size;
    }

    private byte[] getMultipartHeader() throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();
        builder.append("Content-Disposition: form-data; name=\"")
                .append(formData).append("\"; filename=\"")
                .append(fileName).append("\"").append(Constants.LINE_FEED);
        builder.append("Content-Type: ").append(contentType).append(Constants.LINE_FEED).append(Constants.LINE_FEED);
        return builder.toString().getBytes(isUtf8Charset ? Constants.CHARSET_UTF8 : Constants.US_ASCII);
    }

    private byte[] getMultipartParam(String key, String value) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();
        builder.append("Content-Disposition: form-data; name=\"")
                .append(key).append("\"")
                .append(Constants.LINE_FEED).append(Constants.LINE_FEED)
                .append(value);
        return builder.toString().getBytes(isUtf8Charset ? Constants.CHARSET_UTF8 : Constants.US_ASCII);
    }
}

package com.metfone.selfcare.network.file;

import android.content.res.Resources;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.network.file.DownloadQueue.CallBackDelivery;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by toanvk2 on 7/25/2016.
 */
public class DownloadHandler {
    private static final String TAG = DownloadHandler.class.getSimpleName();
    private ApplicationController mApplication;
    private CallBackDelivery mDelivery;

    private final int BUFFER_SIZE = 4096;
    private int mRedirectionCount = 0;
    public final int MAX_REDIRECTS = 5; // can't be more than 7.

    private int numbFile = 0;
    private String newFilePath;
    private long mContentLength;
    private int currentProgress;
    boolean shouldAllowRedirects = true;
    private long t;

    public DownloadHandler(ApplicationController application, CallBackDelivery callBackDelivery) {
        this.mApplication = application;
        this.mDelivery = callBackDelivery;
    }

    public void download(DownloadRequest request) {
        t = System.currentTimeMillis();
        updateDownloadState(request, Constants.STATUS_STARTED);
        numbFile = 0;
        mRedirectionCount = 0;
        if (checkFileDownloaded(request)) {
            updateDownloadComplete(request);
        } else {
            executeDownload(request, request.getUrl());
        }
        Log.d(TAG, "end download");
    }

    private void executeDownload(DownloadRequest request, String downloadUrl) {
        if (isCancelled(request)) return;
        try {
            OkHttpClient client = new OkHttpClient();
            Request myRequest = new Request.Builder()
                    .url(downloadUrl)
                    .get()
                    .build();
            Response response = client.newCall(myRequest).execute();
            updateDownloadState(request, Constants.STATUS_CONNECTING);

            Log.d(TAG, "Response code obtained for downloaded url "
                    + request.getUrl()
                    + " : httpResponse Code "
                    + response.code());

            switch (response.code()) {
                case HttpURLConnection.HTTP_PARTIAL:
                case HttpURLConnection.HTTP_OK:
                    shouldAllowRedirects = false;
                    if (response.isSuccessful()) {
                        transferData(request, response);
                    } else {
                        updateDownloadFailed(request, Constants.ERROR_SIZE_UNKNOWN, "Transfer-Encoding not found as well as can't know size of download, giving up");
                    }
                    break;
                case HttpURLConnection.HTTP_MOVED_PERM:
                case HttpURLConnection.HTTP_MOVED_TEMP:
                case HttpURLConnection.HTTP_SEE_OTHER:
                case Constants.HTTP_TEMP_REDIRECT:
                    // Take redirect url and call executeDownload recursively until
                    // MAX_REDIRECT is reached.
                    while (mRedirectionCount++ < MAX_REDIRECTS && shouldAllowRedirects) {
                        Log.d(TAG, "Redirect for downloaded Id " + request.getDownloadId());
                        final String location = response.header("Location");
//                        final String location = conn.getHeaderField("Location");
                        executeDownload(request, location);
                    }
                    if (mRedirectionCount > MAX_REDIRECTS) {
                        updateDownloadFailed(request, Constants.ERROR_TOO_MANY_REDIRECTS, "Too many redirects, giving up");
                        return;
                    }
                    break;
                case Constants.HTTP_REQUESTED_RANGE_NOT_SATISFIABLE:
                    updateDownloadFailed(request, Constants.HTTP_REQUESTED_RANGE_NOT_SATISFIABLE, response.message());
                    break;
                case HttpURLConnection.HTTP_UNAVAILABLE:
                    updateDownloadFailed(request, HttpURLConnection.HTTP_UNAVAILABLE, response.message());
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    updateDownloadFailed(request, HttpURLConnection.HTTP_INTERNAL_ERROR, response.message());
                    break;
                default:
                    updateDownloadFailed(request, Constants.ERROR_UNHANDLED_HTTP_CODE, "Unhandled HTTP response:" + response.code() + " message:" + response.message());
                    break;
            }
        } catch (Exception e) {
            Log.f(TAG, "Exception", e);
            updateDownloadFailed(request, -1, e.getMessage());
//            attemptRetryOnTimeOutException(request, request.getUrl(), conn);// Retry.
            //updateDownloadFailed(request,-1, "Exception");
        } finally {
            /*try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            }*/
        }
    }

    private boolean isCancelled(DownloadRequest request) {
        if (request.isCancelled()) {
            updateDownloadFailed(request, Constants.ERROR_CANCELLED, "DownloadManager cancelled");
            return true;
        }
        return false;
    }

    private void transferData(DownloadRequest request, Response response) {
        long mCurrentBytes = 0;
        InputStream httpInputStream = null;
        InputStream bufferInputStream = null;
        OutputStream fileOutputStream = null;
        try {
            httpInputStream = response.body().byteStream();
            bufferInputStream = new BufferedInputStream(httpInputStream, BUFFER_SIZE);
            fileOutputStream = new FileOutputStream(request.getFilePath());
            byte data[] = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = bufferInputStream.read(data)) != -1) { // writing data to file
                mCurrentBytes += bytesRead;
                fileOutputStream.write(data, 0, bytesRead);
                if (mContentLength > 0) {
                    int progress = (int) ((mCurrentBytes * 100) / mContentLength);
                    updateDownloadProgress(request, progress, mCurrentBytes);
                }
            }
            fileOutputStream.flush();
            //fileOutputStream.close();
            updateDownloadComplete(request);
            //GA
            Resources mRes = mApplication.getResources();
            mApplication.trackingSpeed(mRes.getString(R.string.ga_category_speed_file),
                    mRes.getString(R.string.ga_label_speed_down),
                    (System.currentTimeMillis() - t));
        } catch (FileNotFoundException e) {
            updateDownloadFailed(request, Constants.ERROR_FILE_ERROR, "Error in FileNotFoundException");
            Log.e(TAG, "Exception", e);
        } catch (IOException e) {
            updateDownloadFailed(request, Constants.ERROR_FILE_ERROR, "Error in IOException");
            Log.e(TAG, "Exception", e);
        } catch (Exception e) {
            updateDownloadFailed(request, Constants.ERROR_FILE_ERROR, "Error in Exception");
            Log.e(TAG, "Exception", e);
        } finally {
            try {
                if (httpInputStream != null) httpInputStream.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (bufferInputStream != null) bufferInputStream.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
            try {
                if (fileOutputStream != null) fileOutputStream.flush();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                try {
                    if (fileOutputStream != null) fileOutputStream.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            try {
                if (response != null) response.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }


    private void cleanupDestination(DownloadRequest request) {
        Log.d(TAG, "cleanupDestination() deleting " + request.getFilePath());
        File destinationFile = new File(request.getFilePath());
        if (destinationFile.exists()) {
            destinationFile.delete();
        }
    }

    public void updateDownloadState(DownloadRequest request, int state) {
        request.setDownloadState(state);
    }

    public void updateDownloadComplete(DownloadRequest request) {
        mDelivery.postDownloadComplete(request);
        request.setDownloadState(Constants.STATUS_SUCCESSFUL);
        //request.finish();
    }

    public void updateDownloadFailed(DownloadRequest request, int errorCode, String errorMsg) {
        Log.f(TAG, "Download File Error: " + errorMsg);
        shouldAllowRedirects = false;
        request.setDownloadState(Constants.STATUS_FAILED);
        if (request.getDeleteDestinationFileOnFailure()) {
            cleanupDestination(request);
        }
        mDelivery.postDownloadFailed(request, errorCode, errorMsg);
        //request.finish();
    }

    public void updateDownloadProgress(DownloadRequest request, int progress, long downloadedBytes) {
        if (progress != currentProgress) {
            currentProgress = progress;
            mDelivery.postProgressUpdate(request, mContentLength, downloadedBytes, progress);
        }
    }

    private boolean checkFileDownloaded(DownloadRequest request) {
        Object container = request.getContainer();
        if (container != null && container instanceof ReengMessage) {
            ReengMessage message = (ReengMessage) container;
            if (checkFile(message, request.getFilePath())) {
                Log.d(TAG, " check file da down truoc do");
                request.setFilePath(newFilePath);
                return true;
            } else {
                request.setFilePath(newFilePath);//set file path moi vao object download request
                return false;
            }
        }
        return false;
    }

    private boolean checkFile(ReengMessage message, String filePath) {
        File file = new File(filePath);
        //kiem tra xem file da ton tai chua
        if (file.exists()) {
            if (file.length() == (long) message.getSize()) {    //file da download
                newFilePath = filePath;
                return true;
            } else {
                numbFile++;    //tang id file download len 1
                newFilePath = FileHelper.addNameFileDuplicate(filePath, numbFile);  //them ten
                Log.i(TAG, "kiem tra file: " + newFilePath);
                return checkFile(message, newFilePath);   //kiem tra xem file moi
            }
        }
        newFilePath = filePath;
        return false;
    }
}
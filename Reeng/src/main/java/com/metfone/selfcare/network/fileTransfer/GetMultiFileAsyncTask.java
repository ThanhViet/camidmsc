package com.metfone.selfcare.network.fileTransfer;

import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.OpenableColumns;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/31/19.
 */

public class GetMultiFileAsyncTask extends AsyncTask<Void, Void, ArrayList<ReengMessage>> {
    private static final String TAG = "GetMultiFileAsyncTask";
    private WeakReference<ApplicationController> mApplication;
    private Intent intent;
    private boolean isLargeFile = false;
    private int numbFile = 0;
    private int totalItems = 0;
    private GetMultiFileListener listener;

    public GetMultiFileAsyncTask(ApplicationController application, Intent intent) {
        this.mApplication = new WeakReference<>(application);
        this.intent = intent;
    }

    public void setListener(GetMultiFileListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onStartTask();
    }

    @Override
    protected void onPostExecute(ArrayList<ReengMessage> results) {
        if (listener != null) listener.onFinishTask(totalItems, results);
    }

    @Override
    protected ArrayList<ReengMessage> doInBackground(Void... params) {
        if (intent == null) {
            isLargeFile = false;
            return null;
        }
        ClipData clipData = intent.getClipData();
        if (clipData == null) {
            isLargeFile = false;
            return null;
        }
        totalItems = clipData.getItemCount();
        ArrayList<ReengMessage> listMessage = new ArrayList<>();
        final ContentResolver contentResolver = mApplication.get().getContentResolver();
        final String[] projection = {OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE};
        for (int i = 0; i < clipData.getItemCount(); i++) {
            Uri mUri = clipData.getItemAt(i).getUri();
            long fileSize = 0;
            String fileName = "";
            String filePath = "";
            Cursor cursor = null;
            try {
                cursor = contentResolver.query(mUri, projection, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    int nameIndex = cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = cursor.getColumnIndexOrThrow(OpenableColumns.SIZE);
                    fileName = cursor.getString(nameIndex);
                    fileSize = cursor.getLong(sizeIndex);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            } finally {
                try {
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            if (TextUtils.isEmpty(fileName)) {
                isLargeFile = false;
            } else if (fileSize >= Constants.FILE.DOCUMENT_MAX_SIZE) {
                isLargeFile = true;
            } else {
                filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.DOWNLOAD_FOLDER + "/" + fileName;
                String oldFile = checkFileGetPath(filePath, fileSize);
                if (TextUtils.isEmpty(oldFile)) {
                    InputStream is = null;
                    OutputStream os = null;
                    try {
                        is = contentResolver.openInputStream(mUri);
                        os = new FileOutputStream(filePath);
                        byte[] buffer = new byte[Constants.FILE.BUFFER_SIZE_DEFAULT];
                        int length;
                        while ((length = is.read(buffer)) > 0) {
                            os.write(buffer, 0, length);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        filePath = null;
                    } finally {
                        try {
                            if (is != null) {
                                is.close();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                        try {
                            if (os != null) {
                                os.close();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception", e);
                        }
                    }

                } else
                    filePath = oldFile;
            }

            if (!TextUtils.isEmpty(filePath)) {
                ReengMessage msg = getMessage(filePath, fileName);
                listMessage.add(msg);
                if (listMessage.size() == 9) break;
            } else {
                Log.e(TAG, "file not found: ");
            }

        }

        return listMessage;
    }

    private ReengMessage getMessage(String imagePath, String fileName) {
        if (!TextUtils.isEmpty(imagePath)) {
            ReengMessage msg = new ReengMessage();
            msg.setFilePath(imagePath);
            boolean isImage = false;
            String extension = FileHelper.getExtensionFile(imagePath);
            if ("png".equals(extension) || "jpg".equals(extension)) isImage = true;
            msg.setMessageType(isImage ? ReengMessageConstant.MessageType.image : ReengMessageConstant.MessageType.file);
            msg.setFileName(fileName);

            return msg;
        } else
            return null;
    }

    private String checkFileGetPath(String filePath, long fileSize) {
        File file = new File(filePath);
        if (file.exists()) {//kiem tra xem file da ton tai chua
            if (file.length() == fileSize) {    //file da download
                return filePath;
            } else {
                numbFile++;
                String newFilePath = FileHelper.addNameFileDuplicate(filePath, numbFile);  //them ten
                Log.i(TAG, "kiem tra file: " + newFilePath);
                return checkFileGetPath(newFilePath, fileSize);
            }
        }
        return null;
    }

    public interface GetMultiFileListener {
        void onStartTask();

        void onFinishTask(int totalItems, ArrayList<ReengMessage> results);
    }
}
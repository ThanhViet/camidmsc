package com.metfone.selfcare.network.metfoneplus.response;

import com.google.android.gms.common.util.Strings;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

public class WsGetAccountPaymentInfoResponse extends BaseResponse<WsGetAccountPaymentInfoResponse.Response> {
    public static class Response implements Serializable {
        public String phoneNumber;
        public double paid;
        public String customerName;
        public String paymentAmount;
        public String totalAmount;
        public String contractIdInfo;

        public String getDebitAsCurrencyFormat(){
            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
            if(Strings.isEmptyOrWhitespace(totalAmount)){
                totalAmount = "0";
            }
            return formatter.format(Double.parseDouble(totalAmount));
        }
    }
}

package com.metfone.selfcare.network.xmpp;

import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.sasl.NonSASLAuthInfo;

/**
 * listener get token
 *
 * @author thaodv
 */
public class SuccessNonSASLListener implements PacketListener {
    private static final String TAG = SuccessNonSASLListener.class.getSimpleName();
    private ApplicationController mApplication;

    public SuccessNonSASLListener(ApplicationController mApplication) {
        this.mApplication = mApplication;
    }

    @Override
    public void processPacket(Packet packet) {
        //  XMPPManager.notifyXMPPConnected();
        //  neu notify luon o day thi ham XMPPManager.isAuthenticated() van return false do chua cap nhat kip thoi
        NonSASLAuthInfo success = (NonSASLAuthInfo) packet;
        String token = success.getToken();
        XMPPManager mXMPPManager = mApplication.getXmppManager();
        if (!TextUtils.isEmpty(token)) {
            ReengAccount reengAccount = mApplication.getReengAccountBusiness().getCurrentAccount();
            reengAccount.setToken(token);
            reengAccount.setActive(true);
            mApplication.getReengAccountBusiness().updateReengAccount(reengAccount);
            mXMPPManager.setTokenForConfig(token);
        } else {
            Log.i(TAG, "received empty token");
        }
        //TODO khong nhan config từ bản tin auth nữa
        // save subscription domain
       /* boolean isDomainMsgChange = UrlConfigHelper.getInstance(mApplication).updateDomainMocha(success.getDomainFile(),
                success.getDomainMessage(), success.getDomainOnMedia(), success.getPublicRSAKey());
        //change domain keeng
        UrlConfigHelper.getInstance(mApplication).changeDomainKeeng(success.getDomainServiceKeeng(),
                success.getDomainMedia2Keeng(), success.getDomainImageKeeng());
        //change config
        mApplication.getReengAccountBusiness().setConfigFromServer(success.getVipInfo(),
                success.getCBNV(), success.getCall(), success.getSSL(), success.getSmsIn());
        Log.d(TAG, "processPacket--isDomainMsgChange: " + isDomainMsgChange);
        if (isDomainMsgChange) {
            mXMPPManager.changeConfigXmpp();
            // reconnect
            if (IMService.isReady()){
                IMService.getInstance().connectByToken();
            }else {
                mApplication.startService(new Intent(mApplication.getApplicationContext(), IMService.class));
            }
        }*/
    }
}
package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsDoActionServiceRequest extends BaseRequest<WsDoActionServiceRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("language")
        private String language;
        @SerializedName("serviceCode")
        private String serviceCode;
        @SerializedName("actionType")
        private int actionType;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public int getActionType() {
            return actionType;
        }

        public void setActionType(int actionType) {
            this.actionType = actionType;
        }
    }
}

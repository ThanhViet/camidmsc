package com.metfone.selfcare.network.metfoneplus.response;

import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetServicesByGroupResponse extends BaseResponse<List<ServiceGroup>> {
}

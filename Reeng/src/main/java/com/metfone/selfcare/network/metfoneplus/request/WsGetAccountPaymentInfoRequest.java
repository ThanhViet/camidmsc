package com.metfone.selfcare.network.metfoneplus.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAccountPaymentInfoRequest extends BaseRequest<WsGetAccountPaymentInfoRequest.Request> {
    public static class Request{
        public String language;
        public String isdn;
    }
}

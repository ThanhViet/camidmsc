package com.metfone.selfcare.network.file;

import android.text.TextUtils;

import com.metfone.selfcare.util.Log;

import java.util.HashMap;

/**
 * Created by toanvk2 on 7/21/2016.
 */
public class UploadRequest implements Comparable<UploadRequest> {
    private static final String TAG = UploadRequest.class.getSimpleName();
    private int mUploadState;
    private int mUploadId;
    private String mUrl;
    private String filePath;
    private String fileName;
    private String contentType;
    private String formData;
    private Object container;
    private RetryPolicy mRetryPolicy;
    private boolean mCancelled = false;
    private UploadListener mUploadListener;
    private HashMap<String, String> mCustomParams;
    private HashMap<String, String> mCustomHeader;
    private Priority mPriority = Priority.HIGH;

    public UploadRequest(String url) throws NullPointerException, IllegalArgumentException {
        if (TextUtils.isEmpty(url)) {
            throw new NullPointerException();
        }
        if ((!url.startsWith("http") && !url.startsWith("https"))) {
            Log.e(TAG, "exception");
            throw new IllegalArgumentException("Can only upload HTTP/HTTPS URIs: " + url);
        }
        mCustomHeader = new HashMap<>();
        mCustomParams = new HashMap<>();
        mUploadState = Constants.STATUS_PENDING;
        mRetryPolicy = new RetryPolicy();
        mUrl = url;
    }

    /**
     * Returns the {@link Priority} of this request; {@link Priority#NORMAL} by default.
     */
    public Priority getPriority() {
        return mPriority;
    }

    /**
     * Set the {@link Priority}  of this request;
     *
     * @param priority
     * @return request
     */
    public UploadRequest setPriority(Priority priority) {
        mPriority = priority;
        return this;
    }

    /**
     * Associates this request with the given queue. The request queue will be notified when this
     * request has finished.
     */

    public RetryPolicy getRetryPolicy() {
        if (mRetryPolicy == null) {
            mRetryPolicy = new RetryPolicy();
        }
        return mRetryPolicy;
    }

    public UploadRequest setRetryPolicy(RetryPolicy mRetryPolicy) {
        this.mRetryPolicy = mRetryPolicy;
        return this;
    }

    public final int getUploadId() {
        return mUploadId;
    }

    /**
     * Sets the upload Id of this request.  Used by {@link }.
     */
    final void setUploadId(int uploadId) {
        mUploadId = uploadId;
    }

    public int getUploadState() {
        return mUploadState;
    }

    public void setUploadState(int uploadState) {
        this.mUploadState = uploadState;
    }

    /**
     * Gets the status listener. For internal use.
     *
     * @return the status listener
     */
    public UploadListener getUploadListener() {
        return mUploadListener;
    }

    public UploadRequest setUploadListener(UploadListener uploadListener) {
        mUploadListener = uploadListener;
        return this;
    }

    public String getUrl() {
        return mUrl;
    }

    public UploadRequest setUrl(String url) {
        this.mUrl = url;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public UploadRequest setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public UploadRequest setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public UploadRequest setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public String getFormData() {
        return formData;
    }

    public UploadRequest setFormData(String formData) {
        this.formData = formData;
        return this;
    }

    public Object getContainer() {
        return container;
    }

    public UploadRequest setContainer(Object container) {
        this.container = container;
        return this;
    }

    /**
     * Mark this request as canceled.  No callback will be delivered.
     */
    public void cancel() {
        mCancelled = true;
    }

    //Package-private methods.

    /**
     * Returns true if this request has been canceled.
     */
    public boolean isCancelled() {
        return mCancelled;
    }

    /**
     * Adds custom header to request
     *
     * @param key
     * @param value
     */
    public UploadRequest addCustomHeader(String key, String value) {
        mCustomHeader.put(key, value);
        return this;
    }

    /**
     * Returns all custom headers set by user
     *
     * @return
     */
    public HashMap<String, String> getCustomHeaders() {
        return mCustomHeader;
    }

    public UploadRequest addParams(String key, String value) {
        mCustomParams.put(key, value);
        return this;
    }

    public HashMap<String, String> getCustomParams() {
        return mCustomParams;
    }

    @Override
    public int compareTo(UploadRequest other) {
        Priority left = this.getPriority();
        Priority right = other.getPriority();
        return left == right ?
                this.mUploadId - other.mUploadId :
                right.ordinal() - left.ordinal();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
package com.metfone.selfcare.network.xmpp;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.MessageBusiness;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.EventReceivedMessage;
import org.jivesoftware.smack.packet.Packet;

public class RecevedSoloMessageListener implements PacketListener {
    private ApplicationController application;

    public RecevedSoloMessageListener(ApplicationController application) {
        this.application = application;
    }


    @Override
    public void processPacket(Packet packet) {
        EventReceivedMessage event = (EventReceivedMessage) packet;
        MessageBusiness messageBusiness = application.getMessageBusiness();
        messageBusiness.processReceivedPacket(application, event);
    }
}
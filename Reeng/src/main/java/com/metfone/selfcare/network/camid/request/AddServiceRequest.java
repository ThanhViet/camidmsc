package com.metfone.selfcare.network.camid.request;

import com.google.gson.annotations.SerializedName;

public class AddServiceRequest extends BaseRequest<AddServiceRequest.Request> {
    public class Request {
        @SerializedName("phone_number")
        private String phoneNumber;

        @SerializedName("otp")
        private String otp;

        @SerializedName("service_id")
        private int serviceId;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public int getServiceId() {
            return serviceId;
        }

        public void setServiceId(int serviceId) {
            this.serviceId = serviceId;
        }
    }
}

package com.metfone.selfcare.network.file;

import com.metfone.selfcare.app.dev.ApplicationController;

/**
 * Created by toanvk2 on 7/20/2016.
 */
public class DownloadManager {
    private DownloadQueue mRequestQueue;
    private ApplicationController mApplication;

    /**
     * Default constructor
     */
    public DownloadManager(ApplicationController application) {
        this.mApplication = application;
        mRequestQueue = new DownloadQueue(mApplication);
        // mRequestQueue.start();
    }

    /**
     * Construct with provided callback handler
     *
     * @param
     *//*
    public DownloadManager(ApplicationController application,Handler callbackHandler) throws InvalidParameterException {
        mRequestQueue = new DownloadRequestQueue(callbackHandler);
        mRequestQueue.start();
    }

    public DownloadManager(ApplicationController application,int threadPoolSize) {
        mRequestQueue = new DownloadRequestQueue(threadPoolSize);
        mRequestQueue.start();
    }*/
    public void add(DownloadRequest request) throws IllegalArgumentException {
        if (request == null) {
            throw new IllegalArgumentException("DownloadRequest cannot be null");
        }
        mRequestQueue.addRequest(request);
    }

   /* public int cancel(int downloadId) {
        return mRequestQueue.cancel(downloadId);
    }*/

    public void cancelAll() {
        mRequestQueue.cancelAll();
    }

    /*public int query(int downloadId) {
        return mRequestQueue.query(downloadId);
    }

    public void release() {
        if (mRequestQueue != null) {
            mRequestQueue.release();
            mRequestQueue = null;
        }
    }*/
}
package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WsLstOrderedNumberResponse {

    @SerializedName("isdn")
    @Expose
    private String isdn;
    @SerializedName("orderedDate")
    @Expose
    private String orderedDate;
    @SerializedName("expiredDate")
    @Expose
    private String expiredDate;
    @SerializedName("monthlyFee")
    @Expose
    private Double monthlyFee;
    @SerializedName("registerFee")
    @Expose
    private Double registerFee;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("product")
    @Expose
    private String product;

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedDate(String orderedDate) {
        this.orderedDate = orderedDate;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Double getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(Double monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public Double getRegisterFee() {
        return registerFee;
    }

    public void setRegisterFee(Double registerFee) {
        this.registerFee = registerFee;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}

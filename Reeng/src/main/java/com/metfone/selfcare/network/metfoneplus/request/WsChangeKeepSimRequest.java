package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsChangeKeepSimRequest extends BaseRequest<WsChangeKeepSimRequest.Request> {
    public class Request {
        @SerializedName("otpCode")
        @Expose
        private String otpCode;
        @SerializedName("orderIsdn")
        @Expose
        private String orderIsdn;
        @SerializedName("priceIsdn")
        @Expose
        private String priceIsdn;
        @SerializedName("oldIsdn")
        @Expose
        private String oldIsdn;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("orderType")
        @Expose
        private String orderType;

        @SerializedName("registerFee")
        @Expose
        private Integer registerFee;

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getOtpCode() {
            return otpCode;
        }

        public void setOtpCode(String otpCode) {
            this.otpCode = otpCode;
        }

        public String getOrderIsdn() {
            return orderIsdn;
        }

        public void setOrderIsdn(String orderIsdn) {
            this.orderIsdn = orderIsdn;
        }

        public String getPriceIsdn() {
            return priceIsdn;
        }

        public void setPriceIsdn(String priceIsdn) {
            this.priceIsdn = priceIsdn;
        }

        public String getOldIsdn() {
            return oldIsdn;
        }

        public void setOldIsdn(String oldIsdn) {
            this.oldIsdn = oldIsdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public Integer getRegisterFee() {
            return registerFee;
        }

        public void setRegisterFee(Integer registerFee) {
            this.registerFee = registerFee;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsDetectORCResponse extends BaseResponse<WsDetectORCResponse.Response> {
    public class Response {
        @SerializedName("errorCode")
        private String errorCode;
        @SerializedName("errorDescription")
        private String errorDescription;
        @SerializedName("messageCode")
        private String messageCode;

        @SerializedName("idNumber")
        private String idNumber;
        @SerializedName("dobEn")
        private String dobEn;
        @SerializedName("provinceEn")
        private String provinceEn;
        @SerializedName("sexEn")
        private String sexEn;
        @SerializedName("dobKh")
        private String dobKh;
        @SerializedName("provinceKh")
        private String provinceKh;
        @SerializedName("sexKh")
        private String sexKh;
        @SerializedName("name")
        private String name;

        @SerializedName("nationality")
        private String nationality;

        @SerializedName("expiryDate")
        private String expireDate;

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorDescription() {
            return errorDescription;
        }

        public void setErrorDescription(String errorDescription) {
            this.errorDescription = errorDescription;
        }

        public String getMessageCode() {
            return messageCode;
        }

        public void setMessageCode(String messageCode) {
            this.messageCode = messageCode;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getDobEn() {
            return dobEn;
        }

        public void setDobEn(String dobEn) {
            this.dobEn = dobEn;
        }

        public String getProvinceEn() {
            return provinceEn;
        }

        public void setProvinceEn(String provinceEn) {
            this.provinceEn = provinceEn;
        }

        public String getSexEn() {
            return sexEn;
        }

        public void setSexEn(String sexEn) {
            this.sexEn = sexEn;
        }

        public String getDobKh() {
            return dobKh;
        }

        public void setDobKh(String dobKh) {
            this.dobKh = dobKh;
        }

        public String getProvinceKh() {
            return provinceKh;
        }

        public void setProvinceKh(String provinceKh) {
            this.provinceKh = provinceKh;
        }

        public String getSexKh() {
            return sexKh;
        }

        public void setSexKh(String sexKh) {
            this.sexKh = sexKh;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(String expireDate) {
            this.expireDate = expireDate;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.ChangeCardDamaged;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsChangeCardListResponse extends BaseResponse<WsChangeCardListResponse.Response> {
    public class Response {
        @SerializedName("listChangeDamagedCard")
        private List<ChangeCardDamaged> changeCardDamageds;

        public List<ChangeCardDamaged> getChangeCardDamageds() {
            return changeCardDamageds;
        }

        public void setChangeCardDamageds(List<ChangeCardDamaged> changeCardDamageds) {
            this.changeCardDamageds = changeCardDamageds;
        }
    }
}

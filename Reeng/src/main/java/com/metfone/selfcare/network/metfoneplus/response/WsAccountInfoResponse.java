package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsAccountInfoResponse extends BaseResponse<WsAccountInfoResponse.Response> {
    public class Response {
        @SerializedName("basic")
        private double basic;

        @SerializedName("exchange")
        private double exchange;

        @SerializedName("freeMinute")
        private double freeMinute;

        @SerializedName("data")
        private String data;

        public double getBasic() {
            return basic;
        }

        public void setBasic(double basic) {
            this.basic = basic;
        }

        public double getExchange() {
            return exchange;
        }

        public void setExchange(double exchange) {
            this.exchange = exchange;
        }

        public double getFreeMinute() {
            return freeMinute;
        }

        public void setFreeMinute(double freeMinute) {
            this.freeMinute = freeMinute;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }
}

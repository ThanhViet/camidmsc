package com.metfone.selfcare.network.metfoneplus.response;

import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsUpdateComplaintResponse extends BaseResponse<WsUpdateComplaintResponse.Response> {
    public class Response {
        Object object;
    }
}

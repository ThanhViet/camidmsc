package com.metfone.selfcare.network.xmpp;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.GSMMessage;
import org.jivesoftware.smack.packet.Packet;


public class GSMMessageListener implements PacketListener {
    private ApplicationController context;
    private static final String TAG = "GSMMessageListener";
    private MessageBusiness mMessageBusiness;

    public GSMMessageListener(ApplicationController context) {
        this.context = context;
        mMessageBusiness = context.getMessageBusiness();
    }


    @Override
    public void processPacket(Packet packet) {
        // get packet from message
        GSMMessage message = (GSMMessage) packet;
        Log.i(TAG, "" + message.toXML());
        mMessageBusiness.processGsmResponse(message);
    }
}
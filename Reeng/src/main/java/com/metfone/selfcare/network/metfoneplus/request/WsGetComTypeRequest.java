package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

/**
 * Request for get list error in Add Feedback screen
 */
public class WsGetComTypeRequest extends BaseRequest<WsGetComTypeRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;

        @SerializedName("language")
        private String language;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

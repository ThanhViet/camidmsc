package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.CheckComplaintNotification;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsCheckComplaintNotificationResponse extends BaseResponse<WsCheckComplaintNotificationResponse.Response> {
    public class Response {
        @SerializedName("complaintConfirm")
        public CheckComplaintNotification checkComplaintNotification;
    }
}

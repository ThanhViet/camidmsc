package com.metfone.selfcare.network.metfoneplus;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;

public class ResultResponse<T> {
    @SerializedName("errorCode")
    private String errorCode;

    @SerializedName("code")
    public String code;

    @SerializedName("message")
    private String message;

    @SerializedName("object")
    private Object object;

    @SerializedName("userMsg")
    private String userMsg;

    @SerializedName("wsResponse")
    private T wsResponse;
    
    @SerializedName("accountRankDTO")
    public AccountRankDTO accountRankDTO;

    @SerializedName("data")
    private T data;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public T getWsResponse() {
        return wsResponse;
    }

    public void setWsResponse(T wsResponse) {
        this.wsResponse = wsResponse;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsSubscriberUpdateCustomerInfoRequest extends BaseRequest<WsSubscriberUpdateCustomerInfoRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("language")
        private String language;
        @SerializedName("lang")
        private String lang;
        @SerializedName("isScan")
        private String isScan;
        @SerializedName("idType")
        private int idType;
        @SerializedName("fullName")
        private String fullName;
        @SerializedName("idNumber")
        private String idNumber;
        @SerializedName("dob")
        private String dob;
        @SerializedName("gender")
        private String gender;
        @SerializedName("issueDate")
        private String issueDate;
        @SerializedName("province")
        private String province;
        @SerializedName("district")
        private String district;
        @SerializedName("commune")
        private String commune;
        @SerializedName("street")
        private String street;
        @SerializedName("fullAddress")
        private String fullAddress;
        @SerializedName("homeNo")
        private String homeNo;
        @SerializedName("nationality")
        private String nationality;
        @SerializedName("contact")
        private String contact;
        @SerializedName("expireDate")
        private String expireDate;
        @SerializedName("visaDate")
        private String visaDate;

        @SerializedName("precinct")
        private String precinct;

        @SerializedName("relationship")
        private String relationship;

        @SerializedName("subDateBirth")
        private String subDateBirth;

        @SerializedName("subGender")
        private String subGender;

        @SerializedName("subName")
        private String subName;

        @SerializedName("image1Name")
        private String image1Name;

        @SerializedName("image1Data")
        private String image1Data;

        @SerializedName("image2Name")
        private String image2Name;

        @SerializedName("image2Data")
        private String image2Data;

        @SerializedName("image3Name")
        private String image3Name;

        @SerializedName("image3Data")
        private String image3Data;

        public String getIsScan() {
            return isScan;
        }

        public void setIsScan(String isScan) {
            this.isScan = isScan;
        }

        public int getIdType() {
            return idType;
        }

        public void setIdType(int idType) {
            this.idType = idType;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getIssueDate() {
            return issueDate;
        }

        public void setIssueDate(String issueDate) {
            this.issueDate = issueDate;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getCommune() {
            return commune;
        }

        public void setCommune(String commune) {
            this.commune = commune;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getFullAddress() {
            return fullAddress;
        }

        public void setFullAddress(String fullAddress) {
            this.fullAddress = fullAddress;
        }

        public String getHomeNo() {
            return homeNo;
        }

        public void setHomeNo(String homeNo) {
            this.homeNo = homeNo;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getContact() {
            return contact;
        }

        public String getVisaDate() {
            return visaDate;
        }

        public void setVisaDate(String visaDate) {
            this.visaDate = visaDate;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(String idExpireDate) {
            this.expireDate = idExpireDate;
        }

        public String getPrecinct() {
            return precinct;
        }

        public void setPrecinct(String precinct) {
            this.precinct = precinct;
        }

        public String getRelationship() {
            return relationship;
        }

        public void setRelationship(String relationship) {
            this.relationship = relationship;
        }

        public String getSubDateBirth() {
            return subDateBirth;
        }

        public void setSubDateBirth(String subDateBirth) {
            this.subDateBirth = subDateBirth;
        }

        public String getSubGender() {
            return subGender;
        }

        public void setSubGender(String subGender) {
            this.subGender = subGender;
        }

        public String getSubName() {
            return subName;
        }

        public void setSubName(String subName) {
            this.subName = subName;
        }

        public String getImage1Name() {
            return image1Name;
        }

        public void setImage1Name(String image1Name) {
            this.image1Name = image1Name;
        }

        public String getImage1Data() {
            return image1Data;
        }

        public void setImage1Data(String image1Data) {
            this.image1Data = image1Data;
        }

        public String getImage2Name() {
            return image2Name;
        }

        public void setImage2Name(String image2Name) {
            this.image2Name = image2Name;
        }

        public String getImage2Data() {
            return image2Data;
        }

        public void setImage2Data(String image2Data) {
            this.image2Data = image2Data;
        }

        public String getImage3Name() {
            return image3Name;
        }

        public void setImage3Name(String image3Name) {
            this.image3Name = image3Name;
        }

        public String getImage3Data() {
            return image3Data;
        }

        public void setImage3Data(String image3Data) {
            this.image3Data = image3Data;
        }

        @SerializedName("service")
        @Expose
        private String service;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        @Override
        public String toString() {
            return "Request{" +
                    "isdn='" + isdn + '\'' +
                    ", language='" + language + '\'' +
                    ", lang='" + lang + '\'' +
                    ", isScan='" + isScan + '\'' +
                    ", idType=" + idType +
                    ", fullName='" + fullName + '\'' +
                    ", idNumber='" + idNumber + '\'' +
                    ", dob='" + dob + '\'' +
                    ", gender='" + gender + '\'' +
                    ", issueDate='" + issueDate + '\'' +
                    ", province='" + province + '\'' +
                    ", district='" + district + '\'' +
                    ", commune='" + commune + '\'' +
                    ", street='" + street + '\'' +
                    ", fullAddress='" + fullAddress + '\'' +
                    ", homeNo='" + homeNo + '\'' +
                    ", nationality='" + nationality + '\'' +
                    ", contact='" + contact + '\'' +
                    ", expireDate='" + expireDate + '\'' +
                    ", visaDate='" + visaDate + '\'' +
                    ", precinct='" + precinct + '\'' +
                    ", relationship='" + relationship + '\'' +
                    ", subDateBirth='" + subDateBirth + '\'' +
                    ", subGender='" + subGender + '\'' +
                    ", subName='" + subName + '\'' +
                    '}';
        }
    }
}

/**

 * "idType": "1",
 * "fullName": "Khoai lang tien sinh",
 * "idNumber": "123",
 * "dob": "25/08/1999",
 * "gender": "m",
 * "issueDate": "12/12/2021",
 * "province": "PNP",
 * "district": "Khamr",
 * "commune": "Amge",
 * "street": "200",
 * "homeNo": "200",
 * "fullAddress": "200/200",
 * "nationality": "Laos",
 * "contact": "byName",
 * "expireDate": "29/02/2011",
 * "visaDate": "20/10/2042",
 * "subName": "Mr",
 * "subGender": "F",
 * "subDateBirth": "29/02/2011",
 * "relationship": "?",
 * "image1Name": "1name",
 * "image1Data": "1DataBase64",
 * "image2Name": "2name",
 * "image2Data": "2DataBase64",
 * "image3Name": "3name",
 * "image3Data": "3DataBase64"
 */

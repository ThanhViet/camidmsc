package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.database.model.UserIdentityInfo;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsSubGetInfoCusResponse extends BaseResponse<WsSubGetInfoCusResponse.Response> {
    public class Response {
        @SerializedName("errorCode")
        private String errorCode;
        @SerializedName("errorDescription")
        private String errorDescription;
        @SerializedName("messageCode")
        private String messageCode;
        @SerializedName("cus")
        UserIdentityInfo userIdentityInfo;

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorDescription() {
            return errorDescription;
        }

        public void setErrorDescription(String errorDescription) {
            this.errorDescription = errorDescription;
        }

        public String getMessageCode() {
            return messageCode;
        }

        public void setMessageCode(String messageCode) {
            this.messageCode = messageCode;
        }

        public UserIdentityInfo getUserIdentityInfo() {
            return userIdentityInfo;
        }

        public void setUserIdentityInfo(UserIdentityInfo userIdentityInfo) {
            this.userIdentityInfo = userIdentityInfo;
        }
    }
}

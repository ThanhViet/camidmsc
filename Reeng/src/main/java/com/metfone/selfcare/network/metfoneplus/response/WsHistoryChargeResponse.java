package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.HistoryDataInfo;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsHistoryChargeResponse extends BaseResponse<WsHistoryChargeResponse.Response> {
    public class Response {
        @SerializedName("total")
        private Double total = null;
        @SerializedName("unit")
        private String unit = null;
        @SerializedName("data")
        private Double data = null;
        @SerializedName("calling")
        private Double calling = null;
        @SerializedName("sms")
        private Double sms = null;
        @SerializedName("vas")
        private Double vas = null;
        @SerializedName("quantity")
        private Integer quantity = null;
        @SerializedName("history")
        private List<HistoryDataInfo> history = null;

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public Double getData() {
            return data;
        }

        public void setData(Double data) {
            this.data = data;
        }

        public Double getCalling() {
            return calling;
        }

        public void setCalling(Double calling) {
            this.calling = calling;
        }

        public Double getSms() {
            return sms;
        }

        public void setSms(Double sms) {
            this.sms = sms;
        }

        public Double getVas() {
            return vas;
        }

        public void setVas(Double vas) {
            this.vas = vas;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public List<HistoryDataInfo> getHistory() {
            return history;
        }

        public void setHistory(List<HistoryDataInfo> history) {
            this.history = history;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;

public class ABAMobilePaymentBillPaymentResponse {
    public String responseCode;
    public String responseMessage;
    public String qrString;
    @SerializedName("abapay_deeplink")
    public String abapayDeeplink;
    @SerializedName("app_store")
    public String appStore;
    @SerializedName("play_store")
    public String playStore;
    public String txnid;
}

package com.metfone.selfcare.network.camid.request;

import com.google.gson.annotations.SerializedName;

public class CheckPhoneRequest extends BaseRequest<CheckPhoneRequest.Request> {
    public class Request {
        @SerializedName("phone_number")
        private String phoneNumber;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }
}

package com.metfone.selfcare.network.camid.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckCarrierResponse extends BaseResponse<CheckCarrierResponse.Response> {
    public  class Response {
        @SerializedName("metfone")
        @Expose
        private Boolean metfone;

        public Boolean getMetfone() {
            return metfone;
        }

        public void setMetfone(Boolean metfone) {
            this.metfone = metfone;
        }
    }
}

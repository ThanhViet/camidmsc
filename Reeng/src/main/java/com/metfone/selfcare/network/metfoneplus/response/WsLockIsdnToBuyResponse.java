package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsLockIsdnToBuyResponse {
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("errorMessage")
    @Expose
    private Object errorMessage;
    @SerializedName("result")
    @Expose
    private WsLockIsdnToBuyResultResponse result;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public WsLockIsdnToBuyResultResponse getResult() {
        return result;
    }

    public void setResult(WsLockIsdnToBuyResultResponse result) {
        this.result = result;
    }
}

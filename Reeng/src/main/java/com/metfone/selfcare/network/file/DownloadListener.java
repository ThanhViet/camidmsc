package com.metfone.selfcare.network.file;

/**
 * A Listener for the DownloadManager Status. Implement this interface to listen to DownloadManager Events.
 * Created by toanvk2 on 7/20/2016.
 */
public interface DownloadListener {
    void onDownloadStarted(DownloadRequest request);

    void onDownloadComplete(DownloadRequest downloadRequest);

    void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage);

    void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress);
}

package com.metfone.selfcare.network.fileTransfer;

import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.OpenableColumns;
import android.text.TextUtils;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.listeners.DownloadDriveResponse;
import com.metfone.selfcare.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by toanvk2 on 11/30/2017.
 */

public class DownloadFileDriveAsyncTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "DownloadFileDriveAsyncTask";
    private ApplicationController mApplication;
    private DownloadDriveResponse mListener;
    private Uri mUri;
    private String fileName, filePath, newFilePath;
    private long fileSize;
    private boolean isLargeFile = false;
    private int numbFile = 0;

    public DownloadFileDriveAsyncTask(ApplicationController application, BaseSlidingFragmentActivity activity,
                                      Uri uri, DownloadDriveResponse listener) {
        this.mApplication = application;
        this.mUri = uri;
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        if (mListener == null || mUri == null) {
            isLargeFile = false;
            return null;
        }
        final String[] projection = {OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE};
        Cursor cursor = null;
        try {
            cursor = mApplication.getContentResolver().query(mUri, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int nameIndex = cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = cursor.getColumnIndexOrThrow(OpenableColumns.SIZE);
                fileName = cursor.getString(nameIndex);
                fileSize = cursor.getLong(sizeIndex);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        } finally {
            try {
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
        if (TextUtils.isEmpty(fileName)) {
            isLargeFile = false;
        } else if (fileSize >= Constants.FILE.DOCUMENT_MAX_SIZE) {
            isLargeFile = true;
        } else {
            String filePath = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.DOWNLOAD_FOLDER + "/" + fileName;
            if (checkFile(filePath, fileSize)) {
                this.filePath = filePath;
            } else {
                InputStream is = null;
                OutputStream os = null;
                try {
                    is = mApplication.getContentResolver().openInputStream(mUri);
                    os = new FileOutputStream(newFilePath);
                    byte[] buffer = new byte[Constants.FILE.BUFFER_SIZE_DEFAULT];
                    int length;
                    while ((length = is.read(buffer)) > 0) {
                        os.write(buffer, 0, length);
                    }
                    this.filePath = newFilePath;
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                } finally {
                    try {
                        if (is != null) {
                            is.close();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                    try {
                        if (os != null) {
                            os.close();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (TextUtils.isEmpty(filePath)) {
            mListener.onFail(isLargeFile);
        } else {
            mListener.onResult(fileName, filePath);
        }
    }

    private boolean checkFile(String filePath, long fileSize) {
        File file = new File(filePath);
        if (file.exists()) {//kiem tra xem file da ton tai chua
            if (file.length() == fileSize) {    //file da download
                newFilePath = filePath;
                return true;
            } else {
                numbFile++;
                newFilePath = FileHelper.addNameFileDuplicate(filePath, numbFile);  //them ten
                Log.i(TAG, "kiem tra file: " + newFilePath);
                return checkFile(newFilePath, fileSize);
            }
        }
        newFilePath = filePath;
        return false;
    }
}
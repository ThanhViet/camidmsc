package com.metfone.selfcare.network.metfoneplus.request;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsSubmitComplaintMyMetfoneRequest extends BaseRequest<WsSubmitComplaintMyMetfoneRequest.Request> {

    public class Request {
        @SerializedName("image")
        private String image;
        @SerializedName("compContent")
        private String compContent;
        @SerializedName("compTypeId")
        private String compTypeId;
        @SerializedName("complainerPhone")
        private String complainerPhone;
        @SerializedName("errorPhone")
        private String errorPhone;
        @SerializedName("serviceTypeId")
        private String serviceTypeId;
        @SerializedName("complainerAddress")
        private String complainerAddress;

        public String getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(String serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public String getComplainerAddress() {
            return complainerAddress;
        }

        public void setComplainerAddress(String complainerAddress) {
            this.complainerAddress = complainerAddress;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCompContent() {
            return compContent;
        }

        public void setCompContent(String compContent) {
            this.compContent = compContent;
        }

        public String getCompTypeId() {
            return compTypeId;
        }

        public void setCompTypeId(String compTypeId) {
            this.compTypeId = compTypeId;
        }


        public String getComplainerPhone() {
            return complainerPhone;
        }

        public void setComplainerPhone(String complainerPhone) {
            this.complainerPhone = complainerPhone;
        }

        public String getErrorPhone() {
            return errorPhone;
        }

        public void setErrorPhone(String errorPhone) {
            this.errorPhone = errorPhone;
        }
    }

    /**
     * Create compTemplate for wsSubmitComplaintMyMetfone API
     *
     * @param compTemplate    get from 'compTemplate' of wsGetComType API
     * @param latitude        null if location denied
     * @param longitude       null if location denied
     * @param feedbackContent feedback of user (required)
     * @param email           null if email not input
     * @return compTemplate
     */
    public String createCompContent(String compTemplate,
                                    String latitude, String longitude,
                                    String feedbackContent,
                                    String email) {
        Context context = ApplicationController.self().getApplicationContext();

        String newline = "newline";
        StringBuilder builder = new StringBuilder(compTemplate);
        builder.append(newline);

        if (latitude != null && !"".equals(latitude)) {
            builder.append(latitude)
                    .append(", ")
                    .append(longitude)
                    .append(newline);
        }

        builder.append(context.getString(R.string.m_p_add_feedback_us_customer_update))
                .append(feedbackContent)
                .append(newline)
                .append(context.getString(R.string.m_p_add_feedback_us_email));

        if (email != null && !"".equals(email)) {
            builder.append(email);
        }

        return builder.toString();
    }
}

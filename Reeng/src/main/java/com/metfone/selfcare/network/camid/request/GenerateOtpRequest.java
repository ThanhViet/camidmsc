package com.metfone.selfcare.network.camid.request;

import com.google.gson.annotations.SerializedName;

public class GenerateOtpRequest extends BaseRequest<GenerateOtpRequest.Request> {
    public class Request {
        @SerializedName("phone_number")
        private String phoneNumber;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }
}

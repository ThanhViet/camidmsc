package com.metfone.selfcare.network.file;

import java.nio.charset.Charset;

/**
 * Created by toanvk2 on 7/21/2016.
 */
public class Constants {
    /**
     * Status when the transfer is currently pending.
     */
    public final static int STATUS_PENDING = 1 << 0;
    /**
     * Status when the transfer is currently pending.
     */
    public final static int STATUS_STARTED = 1 << 1;
    /**
     * Status when the transfer network call is connecting to destination.
     */
    public final static int STATUS_CONNECTING = 1 << 2;
    /**
     * Status when the transfer is currently running.
     */
    public final static int STATUS_RUNNING = 1 << 3;
    /**
     * Status when the transfer has successfully completed.
     */
    public final static int STATUS_SUCCESSFUL = 1 << 4;
    /**
     * Status when the transfer has failed.
     */
    public final static int STATUS_FAILED = 1 << 5;
    /**
     * Status when the transfer has failed due to broken url or invalid transfer url
     */
    public final static int STATUS_NOT_FOUND = 1 << 6;
    /**
     * Status when the transfer is attempted for retry due to connection timeouts.
     */
    public final static int STATUS_RETRYING = 1 << 7;
    /**
     * Error code when writing transfer content to the destination file.
     */
    public final static int ERROR_FILE_ERROR = 1001;
    /**
     * Error code when an HTTP code was received that transfer manager can't
     * handle.
     */
    public final static int ERROR_UNHANDLED_HTTP_CODE = 1002;
    /**
     * Error code when an error receiving or processing data occurred at the
     * HTTP level.
     */
    public final static int ERROR_HTTP_DATA_ERROR = 1004;
    /**
     * Error code when there were too many redirects.
     */
    public final static int ERROR_TOO_MANY_REDIRECTS = 1005;
    /**
     * Error code when size of the file is unknown.
     */
    public final static int ERROR_SIZE_UNKNOWN = 1006;
    /**
     * Error code when passed URI is malformed.
     */
    public final static int ERROR_MALFORMED_URI = 1007;
    /**
     * Error code when transfer is cancelled.
     */
    public final static int ERROR_CANCELLED = 1008;
    /**
     * Error code when there is connection timeout after maximum retries
     */
    public final static int ERROR_CONNECTION_TIMEOUT_AFTER_RETRIES = 1009;

    public static final int HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    public static final int HTTP_TEMP_REDIRECT = 307;

    public static final int THREAD_POOL_SIZE_DEFAULT = 1;
    public static final int DEFAULT_TIMEOUT = 7000;//7s
    /**
     *
     */
    public static final String UTF8 = "UTF-8";
    public static final Charset CHARSET_UTF8 = Charset.forName("UTF-8");
    public static final Charset US_ASCII = Charset.forName("US-ASCII");
    public static final String LINE_FEED = "\r\n";
    public static final String TWO_HYPHENS = "--";
    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
}
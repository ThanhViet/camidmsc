package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.ServicePackage;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class WsGetServiceDetailResponse extends BaseResponse<WsGetServiceDetailResponse.Response> implements Serializable {
    public class Response implements Serializable{
        @SerializedName("name")
        private String name;
        @SerializedName("code")
        private String code;
        @SerializedName("fullDes")
        private String fullDes;
        @SerializedName("imgDesUrl")
        private String imgDesUrl;
        @SerializedName("webLink")
        private String webLink;
        @SerializedName("price")
        private String price;
        @SerializedName("isRegisterAble")
        private int isRegisterAble;
        @SerializedName("packages")
        private List<ServicePackage> servicePackages;
        @SerializedName("shortDes")
        private String shortDes;
        @SerializedName("validity")
        private String validity;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getFullDes() {
            return fullDes;
        }

        public void setFullDes(String fullDes) {
            this.fullDes = fullDes;
        }

        public String getImgDesUrl() {
            return imgDesUrl;
        }

        public void setImgDesUrl(String imgDesUrl) {
            this.imgDesUrl = imgDesUrl;
        }

        public String getWebLink() {
            return webLink;
        }

        public void setWebLink(String webLink) {
            this.webLink = webLink;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getIsRegisterAble() {
            return isRegisterAble;
        }

        public void setIsRegisterAble(int isRegisterAble) {
            this.isRegisterAble = isRegisterAble;
        }

        public List<ServicePackage> getServicePackages() {
            return servicePackages;
        }

        public void setServicePackages(List<ServicePackage> servicePackages) {
            this.servicePackages = servicePackages;
        }

        public String getShortDes() {
            return shortDes;
        }

        public void setShortDes(String shortDes) {
            this.shortDes = shortDes;
        }

        public String getValidity() {
            return validity;
        }

        public void setValidity(String validity) {
            this.validity = validity;
        }
    }
}

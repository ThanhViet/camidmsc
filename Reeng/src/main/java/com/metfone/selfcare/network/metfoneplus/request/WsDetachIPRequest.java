package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsDetachIPRequest extends BaseRequest<WsDetachIPRequest.Request> {
    public class Request {
        @SerializedName("ip")
        private String ip;
        @SerializedName("language")
        private String language;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }
    }
}

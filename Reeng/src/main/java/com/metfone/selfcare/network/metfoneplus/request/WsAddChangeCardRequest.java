package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.util.Utilities;

public class WsAddChangeCardRequest extends BaseRequest<WsAddChangeCardRequest.Request> {
    public class Request {
        @SerializedName("user")
        private String isdn;
        @SerializedName("language")
        private String language;
        @SerializedName("serial")
        private String serial;
        @SerializedName("phoneNumber")
        private String phoneNumber;
        @SerializedName("dataImage")
        private String dataImage;
        @SerializedName("urlVideo")
        private String video;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDataImage() {
            return dataImage;
        }

        public void setDataImage(String dataImage) {
            this.dataImage = dataImage;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }
    }
}

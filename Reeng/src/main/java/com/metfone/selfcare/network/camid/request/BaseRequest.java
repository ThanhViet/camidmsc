package com.metfone.selfcare.network.camid.request;

import com.google.gson.annotations.SerializedName;

public class BaseRequest<T> {
    @SerializedName("apiKey")
    private String apiKey = "string";

    @SerializedName("sessionId")
    private String sessionId = "string";

    @SerializedName("username")
    private String username = "string";

    @SerializedName("wsCode")
    private String wsCode = "string";

    @SerializedName("wsRequest")
    private T wsRequest = null;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWsCode() {
        return wsCode;
    }

    public void setWsCode(String wsCode) {
        this.wsCode = wsCode;
    }

    public T getWsRequest() {
        return wsRequest;
    }

    public void setWsRequest(T wsRequest) {
        this.wsRequest = wsRequest;
    }

    @Override
    public String toString() {
        return "BaseRequest{" +
                "apiKey='" + apiKey + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", username='" + username + '\'' +
                ", wsCode='" + wsCode + '\'' +
                ", wsRequest=" + wsRequest +
                '}';
    }
}

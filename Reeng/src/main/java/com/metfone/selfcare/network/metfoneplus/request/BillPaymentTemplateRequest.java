package com.metfone.selfcare.network.metfoneplus.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BillPaymentTemplateRequest extends BaseRequest<BillPaymentTemplateRequest.Request> {
    public static class Request{
        public String language;
        public String isdn;
        public int pageSize;
        public int pageNum;
        public String fromDate;
        public String toDate;

        public Request(){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Calendar last3Months = Calendar.getInstance();
            last3Months.add(Calendar.MONTH, -3);
            pageSize =  20;
            pageNum = 1;
            toDate = dateFormat.format(new Date());
            fromDate = dateFormat.format(last3Months.getTime());
        }
    }
}

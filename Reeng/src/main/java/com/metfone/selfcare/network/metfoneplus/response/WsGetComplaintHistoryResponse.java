package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.HistoryComplaint;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetComplaintHistoryResponse extends BaseResponse<WsGetComplaintHistoryResponse.Response> {
    public class Response {
        @SerializedName("listComplaintHistory")
        private List<HistoryComplaint> historyComplaints;

        public List<HistoryComplaint> getHistoryComplaints() {
            return historyComplaints;
        }

        public void setHistoryComplaints(List<HistoryComplaint> historyComplaints) {
            this.historyComplaints = historyComplaints;
        }
    }
}

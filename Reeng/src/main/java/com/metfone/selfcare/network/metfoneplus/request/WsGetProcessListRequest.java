package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.HistoryComplaint;
import com.metfone.selfcare.model.camid.ProcessComplaint;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

/**
 * Get list process for compare
 * status value in {@link HistoryComplaint} and paramId value in {@link ProcessComplaint}
 * for display content and text color.
 */
public class WsGetProcessListRequest extends BaseRequest<WsGetProcessListRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;

        @SerializedName("language")
        private String language;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

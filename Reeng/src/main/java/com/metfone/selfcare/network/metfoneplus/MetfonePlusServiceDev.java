package com.metfone.selfcare.network.metfoneplus;

import com.metfone.selfcare.module.metfoneplus.topup.model.request.BaseDataRequestMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.HistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TopUpQrCodeRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TransMobileRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TopUpQrCodeResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TransMobileResponse;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDoActionServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsFindStoreByAddrResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountsOcsDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComTypeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComplaintHistoryResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetCurrentUsedServicesResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetExchangeServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetNearestStoreResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProcessListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProvinceDistrictResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServicesByGroupResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetSubAccountInfoNewResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubmitComplaintMyMetfoneResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * MetfonePlus(selfcare) apis
 * Dùng cho api có domain: 36.37.242.104
 * Code này sẽ được merge vào @{@link MetfonePlusService} và dùng chung domain vtcapigw.viettelglobal.net
 * Thời gian merge ?
 */
public interface MetfonePlusServiceDev {
    // 0. AutoLogin
    @POST("CamIDAutoLogin")
    Call<LoginResponse> autoLogin(@Body RequestBody requestBody);

    // 1. Login
    @POST("UserLogin")
    Call<LoginResponse> login(@Body RequestBody requestBody);

    // 2. wsAccountInfo
    @POST("UserRouting")
    Call<WsAccountInfoResponse> wsAccountInfo(@Body RequestBody requestBody);

    // 3. wsGetSubAccountInfoNew
    @POST("UserRouting")
    Call<WsGetSubAccountInfoNewResponse> wsGetSubAccountInfoNew(@Body RequestBody requestBody);

    // 4. wsGetAccountsOcsDetail
    @POST("UserRouting")
    Call<WsGetAccountsOcsDetailResponse> wsGetAccountsOcsDetail(@Body RequestBody requestBody);

    // 5. wsGetExchangeService
    @POST("UserRouting")
    Call<WsGetExchangeServiceResponse> wsGetExchangeService(@Body RequestBody requestBody);

    // 6. wsTopUp
    @POST("UserRouting")
    Call<BaseResponseMP<TopUpQrCodeResponse>> wsTopUp(@Body BaseDataRequestMP<TopUpQrCodeRequest> qrCodeRequest);

    // 7. wsHistoryTopUp
    @POST("UserRouting")
    Call<BaseResponseMP<HistoryResponse>> wsHistoryTopUp(@Body BaseDataRequestMP<HistoryRequest> requestBaseDataRequestMP);

    // 8. wsGetServices

    // 9. wsGetServicesByGroup
    @POST("UserRouting")
    Call<WsGetServicesByGroupResponse> wsGetServicesByGroup(@Body RequestBody requestBody);

    // 10. wsGetServiceDetail
    @POST("UserRouting")
    Call<WsGetServiceDetailResponse> wsGetServiceDetail(@Body RequestBody requestBody);

    // 11. wsHistoryTopUpNew
    @POST("UserRouting")
    Call<BaseResponseMP<HistoryResponse>> wsHistoryTopUpNew(@Body BaseDataRequestMP<HistoryRequest> requestBaseDataRequestMP);

    // 12. wsDoActionService
    @POST("UserRouting")
    Call<WsDoActionServiceResponse> wsDoActionService(@Body RequestBody requestBody);

    // 13. wsHistoryCharge
    @POST("UserRouting")
    Call<WsHistoryChargeResponse> wsHistoryCharge(@Body RequestBody requestBody);

    // 14. Payment - createTransMobile
    @POST(".")
    Call<TransMobileResponse> transMobileMP(@Body TransMobileRequest transMobileRequest);

    // 15. wsGetCurrentUsedServices
    @POST("UserRouting")
    Call<WsGetCurrentUsedServicesResponse> wsGetCurrentUsedServices(@Body RequestBody requestBody);

    // 16. wsHistoryChargeDetail
    @POST("UserRouting")
    Call<WsHistoryChargeDetailResponse> wsHistoryChargeDetail(@Body RequestBody requestBody);

    // 17. wsCheckUseGame

    // 18. wsGetComType - Feedback: list complaint
    @POST("UserRouting")
    Call<WsGetComTypeResponse> wsGetComTypeList(@Body RequestBody requestBody);

    // 19. wsGetProcessList
    @POST("UserRouting")
    Call<WsGetProcessListResponse> wsGetProcessList(@Body RequestBody requestBody);

    // 20. wsGetComplaintHistory
    @POST("UserRouting")
    Call<WsGetComplaintHistoryResponse> wsGetComplaintHistoryList(@Body RequestBody requestBody);

    // 21. wsSubmitComplaintMyMetfone
    @POST("UserRouting")
    Call<WsSubmitComplaintMyMetfoneResponse> wsSubmitComplaintMyMetfone(@Body RequestBody requestBody);

    // 22. wsCloseComplain
    @POST("UserRouting")
    Call<WsSubmitComplaintMyMetfoneResponse> wsCloseComplain(@Body RequestBody requestBody);

    // 23. wsFindStoreByAddr
    @POST("UserRouting")
    Call<WsFindStoreByAddrResponse> wsFindStoreByAddr(@Body RequestBody requestBody);

    // 24. wsGetProvinces
    @POST("UserRouting")
    Call<WsGetProvinceDistrictResponse> wsGetProvinces(@Body RequestBody requestBody);

    // 25. wsGetDistricts
    @POST("UserRouting")
    Call<WsGetProvinceDistrictResponse> wsGetDistricts(@Body RequestBody requestBody);

    // 26. wsGetNearestStores
    @POST("UserRouting")
    Call<WsGetNearestStoreResponse> wsGetNearestStores(@Body RequestBody requestBody);
}

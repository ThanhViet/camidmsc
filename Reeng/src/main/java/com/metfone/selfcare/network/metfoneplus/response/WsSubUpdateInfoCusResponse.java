package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsSubUpdateInfoCusResponse extends BaseResponse<WsSubUpdateInfoCusResponse.Response> {
    public class Response {
        @SerializedName("errorCode")
        private String errorCode;
        @SerializedName("errorDescription")
        private String errorDescription;
        @SerializedName("messageCode")
        private String messageCode;
        @SerializedName("requireOtp")
        String requireOtp;

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorDescription() {
            return errorDescription;
        }

        public void setErrorDescription(String errorDescription) {
            this.errorDescription = errorDescription;
        }

        public String getMessageCode() {
            return messageCode;
        }

        public void setMessageCode(String messageCode) {
            this.messageCode = messageCode;
        }

        public String getRequireOtp() {
            return requireOtp;
        }

        public void setRequireOtp(String requireOtp) {
            this.requireOtp = requireOtp;
        }
    }
}

package com.metfone.selfcare.network.camid.request;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class RefreshTokenRequest extends BaseRequest<RefreshTokenRequest.Request> {
    public class Request{
        @SerializedName("refresh_token")
        private String refresh_token;

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }
    }
}

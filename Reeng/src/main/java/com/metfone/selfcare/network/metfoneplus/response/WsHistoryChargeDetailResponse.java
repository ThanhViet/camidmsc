package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.HistoryChargeDetail;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsHistoryChargeDetailResponse extends BaseResponse<WsHistoryChargeDetailResponse.Response> {
    public class Response {
        @SerializedName("total")
        private Integer total;
        @SerializedName("quantity")
        private Integer quantity;
        @SerializedName("money")
        private Double money;
        @SerializedName("data")
        private Double data;
        @SerializedName("history")
        private List<HistoryChargeDetail> historyChargeDetailList;

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getMoney() {
            return money;
        }

        public void setMoney(Double money) {
            this.money = money;
        }

        public Double getData() {
            return data;
        }

        public void setData(Double data) {
            this.data = data;
        }

        public List<HistoryChargeDetail> getHistoryChargeDetailList() {
            return historyChargeDetailList;
        }

        public void setHistoryChargeDetailList(List<HistoryChargeDetail> historyChargeDetailList) {
            this.historyChargeDetailList = historyChargeDetailList;
        }
    }
}

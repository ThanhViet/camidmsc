package com.metfone.selfcare.network;

import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.model.BaseDataRequest;
import com.metfone.selfcare.model.account.AddServiceRequest;
import com.metfone.selfcare.model.account.BaseResponse;
import com.metfone.selfcare.model.account.BaseUserResponseData;
import com.metfone.selfcare.model.account.CheckPhoneRequest;
import com.metfone.selfcare.model.account.DeleteNumberResponse;
import com.metfone.selfcare.model.account.DeleteServiceRequest;
import com.metfone.selfcare.model.account.GetUserResponse;
import com.metfone.selfcare.model.account.InvitedCodeResponse;
import com.metfone.selfcare.model.account.ResetPasswordInfor;
import com.metfone.selfcare.model.account.ResetPasswordResponse;
import com.metfone.selfcare.model.account.SignInRequest;
import com.metfone.selfcare.model.account.SignInResponse;
import com.metfone.selfcare.model.account.SignUpRequest;
import com.metfone.selfcare.model.account.SignUpResponse;
import com.metfone.selfcare.model.account.UpdateLoginRequest;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.account.VerifyInfo;
import com.metfone.selfcare.model.account.VerifyRequest;
import com.metfone.selfcare.model.addAccount.AddAccountFtthUserResponse;
import com.metfone.selfcare.model.addAccount.AddAccountSendIdRequest;
import com.metfone.selfcare.model.addAccount.AddAccountSendIdResponse;
import com.metfone.selfcare.model.addAccount.AddAccountVerifyIdRequest;
import com.metfone.selfcare.model.camid.MethodLoginResponse;
import com.metfone.selfcare.model.camid.OpenIdConfig;
import com.metfone.selfcare.model.camid.UserOldResponse;
import com.metfone.selfcare.model.oldMocha.OTPOldMochaResponse;
import com.metfone.selfcare.model.tabGame.BestScoreGameIDRequest;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.model.tabGame.GameBaseResponseData;
import com.metfone.selfcare.model.tabGame.GameByNameRequest;
import com.metfone.selfcare.model.tabGame.GameListResponseData;
import com.metfone.selfcare.model.tabGame.GamePlayMostTimeRequest;
import com.metfone.selfcare.model.tabGame.GamePlayedRequest;
import com.metfone.selfcare.model.tabGame.SaveGameRequest;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.module.home_kh.api.response.CheckRequestResponse;
import com.metfone.selfcare.module.home_kh.api.response.ForgotRequestResponse;
import com.metfone.selfcare.module.home_kh.api.response.PaymentHistoryResponse;
import com.metfone.selfcare.module.home_kh.fragment.setting.share.SettingNotificationModel;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.BaseDataRequestMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.ForgotIdRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.SearchFTTHRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TopUpQrCodeRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TransMobileRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.WsPaymentHistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TopUpQrCodeResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TransMobileResponse;
import com.metfone.selfcare.module.movienew.model.ActionFilmModel;
import com.metfone.selfcare.module.movienew.model.CategoryModel;
import com.metfone.selfcare.module.movienew.model.CountryModel;
import com.metfone.selfcare.module.movienew.model.InsertLogWatchResponse;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.module.movienew.model.MaybeModel;
import com.metfone.selfcare.module.movienew.model.RateResponse;
import com.metfone.selfcare.network.camid.response.AddServiceResponse;
import com.metfone.selfcare.network.camid.response.BodyResponse;
import com.metfone.selfcare.network.camid.response.CheckCarrierResponse;
import com.metfone.selfcare.network.camid.response.RefreshTokenResponse;
import com.metfone.selfcare.network.camid.response.ServiceResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Retrofit APIs
 */
public interface ApiService {

    final String domain = "http://117.1.166.2:9123/";

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/check-phone")
    Call<BaseResponse<String>> checkPhone(@Body CheckPhoneRequest checkPhoneInfoBaseDataRequest);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/otp")
    Call<BodyResponse> generateOtp(@Body RequestBody body);

    /**
     * Them metfone
     *
     * @return
     */
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("api/v1/openid/add-service")
    Call<BaseResponse<BaseUserResponseData>> addService(@Header("Authorization") String token,
                                                        @Body AddServiceRequest addServiceRequest);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/auth/signin")
    Call<SignInResponse> signIn(@Header("deviceId") String deviceId,
                                @Body SignInRequest signInRequest);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/auth/signup")
    Call<SignUpResponse> signUp(@Body SignUpRequest signUpRequest);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @DELETE("camid-auth/api/v1/user/delete-service")
    Call<SignUpResponse> deleteNumber(@Body DeleteNumberResponse deleteNumberResponse);


    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET("camid-auth/api/v1/user")
    Call<GetUserResponse> getUser(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user")
    Call<BaseResponse<BaseUserResponseData>> updateUser(@Header("Authorization") String token, @Body BaseDataRequest<UserInfo> updateUserRequest);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/kyc-request")
    Call<GetUserResponse> verifyID(@Header("Authorization") String token, @Body BaseDataRequest<VerifyInfo> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/delete-service")
    Call<GetUserResponse> deleteService(@Header("Authorization") String token, @Body BaseDataRequest<DeleteServiceRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @PUT("camid-auth/api/v1/user/verify-service")
    Call<BaseResponse<BaseUserResponseData>> verifyService(@Header("Authorization") String token, @Body VerifyRequest verifyInfo);

    /**
     * Change Log in Phone number
     */
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/update-login")
    Call<BaseResponse<BaseUserResponseData>> updateLogin(@Header("Authorization") String token, @Body UpdateLoginRequest updateLoginInfoRequest);

    /**
     * Password reset
     */
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/password-reset")
    Call<ResetPasswordResponse> resetPassword(@Header("Authorization") String token, @Body BaseDataRequest<ResetPasswordInfor> resetPasswordRequest);


    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/check-phone")
    Call<BodyResponse> checkPhone(@Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/openid/add-service")
    Call<AddServiceResponse> addService(@Header("Authorization") String token,
                                        @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/openid/add-service")
    Call<GetUserResponse> addServiceProfile(@Header("Authorization") String token,
                                            @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/invited-code")
    Call<InvitedCodeResponse> invitedCode(@Header("Authorization") String token,
                                          @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/openid/refresh")
    Call<RefreshTokenResponse> refreshToken(@Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET("camid-auth/api/v1/user")
    Call<AddServiceResponse> getUserInfo(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/update-metfone")
    Call<AddServiceResponse> updateMetfone(@Header("Authorization") String token, @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/update-metfone")
    Call<GetUserResponse> updateMetfoneProfile(@Header("Authorization") String token, @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET("camid-auth/api/v1/user/services")
    Call<ServiceResponse> getService(@Header("Authorization") String token);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("https://apigw.camid.app/CoreService/UserRouting")
    Call<BaseResponseMP<TopUpQrCodeResponse>> topUpMPQrCode(@Header("Authorization") String token, @Body BaseDataRequestMP<TopUpQrCodeRequest> qrCodeRequest);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("http://metfone.kabarplus.online/metfone-topup/createTransMobile")
    Call<TransMobileResponse> transMobileMP(@Body TransMobileRequest transMobileRequest);


//    @Headers({"Content-Type: application/json",
//            "info: [OS;OS version;Device model;Device Id]",
//            })
//    @POST("http://36.37.242.104:8123/ApiGateway/CoreService/UserRouting")
//    Call<BaseResponseMP<HistoryResponse>> getHistory(@Body BaseDataRequestMP<HistoryRequest> requestBaseDataRequestMP);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/link-account")
    Call<GetUserResponse> linkAccount(@Header("Authorization") String token, @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/user/add-login")
    Call<GetUserResponse> addLogin(@Header("Authorization") String token, @Body RequestBody body);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET(MovieApi.DOMAIN_MOVIE + "/v2/api/get-country")
    Call<ArrayList<CountryModel>> getCountry();

    //getCategory Search
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET(MovieApi.DOMAIN_MOVIE + "/v2/api/get-topic-category?limit=1000&offset=0")
    Call<BaseResponseMP<ArrayList<CategoryModel>>> getCategory();

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("ReengBackendBiz/camId/api/checkTokenAuth")
    Call<OTPOldMochaResponse> generateOldMochaOtp(@QueryMap Map<String, String> options);

    // get list danh sách phim đang play ở chế độ onresum
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET(MovieApi.DOMAIN_MOVIE + "/v2/api/get-list-log-watched?")
    Call<ListLogWatchResponse> getListLogWatch(@Query("msisdn") String msisdn, @Query("ref_id") String refId);

    // insert thông tin các video khi đc pause
    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST(domain + "/v2/api/insert-log-watched?")
    Call<InsertLogWatchResponse> insertLogWatch(@Query("id_film") String id_film, @Query("msisdn") String msisdn, @Query("ref_id") String red_id, @Query("time_seek") String time_seek);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST(MovieApi.DOMAIN_MOVIE + "/v2/api/get-list-film-user-like?limit=10&offset=0")
    Call<ArrayList<Movie>> getMyListMovie(@Query("msisdn") String msisdn, @Query("ref_id") String refId);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST(MovieApi.DOMAIN_MOVIE + "/v2/api/get-list-film-for-user?limit=10&offset=0")
    Call<MaybeModel> getMaybeList(@Query("msisdn") String msisdn, @Query("ref_id") String refId);


    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST(domain + "/v2/api/rate-film?")
    Call<RateResponse> rateVideo(@Query("filmid") String filmid, @Query("msisdn") String msisdn, @Query("ref_id") String ref_id, @Query("rate") int rate);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST(MovieApi.DOMAIN_MOVIE + "/v2/api/get-list-film-of-category?limit=10&offset=0")
    Call<ActionFilmModel> getListFilmOfCategory(@Query("msisdn") String msisdn, @Query("categoryid") String categoryid);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @GET(MovieApi.DOMAIN_MOVIE + "/v2/api/get-list-film-by-info?limit=10&offset=0")
    Call<ActionFilmModel> getListFilmByInfo(@Query("info") String info);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/common/check-carrier")
    Call<CheckCarrierResponse> checkCarrier(@Body RequestBody body);

    @POST("camid-auth/api/v1/user/get-user-metfone")
    Call<UserOldResponse> checkInfoOldUsed(@Body CheckPhoneRequest checkPhoneInfoBaseDataRequest);

    @GET("camid-auth/api/v1/method-login/list")
    Call<MethodLoginResponse> getMethodsLogin();

    @GET("camid-auth/api/v1/common/list-config")
    Observable<BaseResponse<List<OpenIdConfig>>> getListConfig();

    @POST("UserRouting")
    Call<SettingNotificationModel> setNotification(@Body RequestBody body);


    //ApiGame

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/games")
    Call<BaseResponse<GameBaseResponseData>> getGameByName(@Header("Authorization") String token, @Body BaseDataRequest<GameByNameRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/best-score")
    Call<BaseResponse<GameModel>> getBestScoreByGameID(@Header("Authorization") String token, @Body BaseDataRequest<BestScoreGameIDRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/played")
    Call<BaseResponse<GameBaseResponseData>> getPlayedGames(@Header("Authorization") String token, @Body BaseDataRequest<GamePlayedRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/play-most-time")
    Call<BaseResponse<GameBaseResponseData>> getPlayMostTimeGames(@Header("Authorization") String token, @Body BaseDataRequest<GamePlayMostTimeRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/best-score-games")
    Call<BaseResponse<GameListResponseData>> getBestScoreGames(@Header("Authorization") String token, @Body BaseDataRequest<GamePlayedRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/save-game")
    Call<BaseResponse<GameBaseResponseData>> saveGame(@Header("Authorization") String token, @Body BaseDataRequest<SaveGameRequest> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/categories")
    Call<BaseResponse<GameBaseResponseData>> getGameCategories(@Header("Authorization") String token, @Body BaseDataRequest<Object> request);

    @Headers({"Content-Type: application/json",
            "info: [OS;OS version;Device model;Device Id]",
    })
    @POST("camid-auth/api/v1/game/top-ten")
    Call<BaseResponse<GameListResponseData>> getGameTopTen(@Header("Authorization") String token, @Body BaseDataRequest<Object> request);


    @POST("camid-auth/api/v1/openid/sendID")
    Call<BaseResponse<AddAccountSendIdResponse>> addAccountSendId(@Header("Authorization") String token,@Body AddAccountSendIdRequest request);

    @POST("camid-auth/api/v1/openid/verifyID")
    Call<BaseResponse<AddAccountSendIdResponse>> addAccountVerifyId(@Header("Authorization") String token,@Body AddAccountVerifyIdRequest request);

    @GET("camid-auth/api/v1/user/ftth-account")
    Call<BaseResponse<ArrayList<AddAccountFtthUserResponse>>> getAccountFtth(@Header("Authorization") String token);

    @POST("https://apigw.camid.app/CoreService/UserRouting")
    Call<PaymentHistoryResponse> paymentHistory(
            @Body WsPaymentHistoryRequest paymentHistoryRequest
    );

}

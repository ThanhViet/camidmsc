package com.metfone.selfcare.network.metfoneplus.response;

import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetNearestStoreResponse extends BaseResponse<List<Store>> {

}

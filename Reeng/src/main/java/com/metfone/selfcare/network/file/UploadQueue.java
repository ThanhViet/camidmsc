package com.metfone.selfcare.network.file;

import android.os.Handler;
import android.os.Looper;
import android.os.Process;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengMessage;
import com.viettel.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by toanvk2 on 7/26/2016.
 */
public class UploadQueue {
    private static final String TAG = UploadQueue.class.getSimpleName();
    private static final int QUEUE_SIZE = 1000;
    private ApplicationController mApplication;
    private final BlockingQueue<UploadRequest> mQueue;
    private final Set<UploadRequest> mCurrentRequests = new HashSet<>();
    private Thread threadQueue;
    private CallBackDelivery mDelivery;
    private UploadHandler uploadHandler;
    private AtomicInteger mSequenceGenerator = new AtomicInteger();
    private boolean done;

    public UploadQueue(ApplicationController application) {
        this.mApplication = application;
        this.mQueue = new ArrayBlockingQueue<>(QUEUE_SIZE, true);
        init();
    }

    protected void init() {
        done = false;
        mDelivery = new CallBackDelivery(new Handler(Looper.getMainLooper()));
        uploadHandler = new UploadHandler(mApplication, mDelivery);
        threadQueue = new Thread() {
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                processQueue(this);
            }
        };
        threadQueue.setName("Upload queue");
        threadQueue.setDaemon(true);
        startup();
    }

    /**
     * start queue
     */
    public void startup() {
        if (threadQueue.isAlive()) {
            Log.i(TAG, "Return by writeThread existed");
            return;
        }
        threadQueue.start();
    }

    public void shutdown() {
        done = true;
        synchronized (mQueue) {
            mQueue.notifyAll();
        }
        threadQueue = null;
        mQueue.clear();
    }

    public void cancelRequest(UploadRequest request) {
        synchronized (mCurrentRequests) {
            for (UploadRequest item : mCurrentRequests) {
                if (item == request) {
                    request.cancel();
                    break;
                }
            }
            mCurrentRequests.remove(request);
        }
    }

    public void cancelRequest(int requestId) {
        synchronized (mCurrentRequests) {
            for (UploadRequest request : mCurrentRequests) {
                if (request.getUploadId() == requestId) {
                    request.cancel();
                    mCurrentRequests.remove(request);
                    break;
                }
            }
        }
    }

    public void cancelRequest(ReengMessage message) {
        synchronized (mCurrentRequests) {
            for (UploadRequest request : mCurrentRequests) {
                if (request.getContainer() instanceof ReengMessage && ((ReengMessage) request.getContainer()).getId()
                        == message.getId()) {
                    request.cancel();
                    mCurrentRequests.remove(request);
                    break;
                }
            }
        }
    }

    public void cancelAll() {
        synchronized (mCurrentRequests) {
            for (UploadRequest request : mCurrentRequests) {
                request.cancel();
            }
            mCurrentRequests.clear();
        }
    }

    public void addRequest(UploadRequest request) {
        if (request == null) {
            return;
        }
        if (!done) {
            try {
                request.setUploadId(getUploadId());
                synchronized (mCurrentRequests) {
                    mCurrentRequests.add(request);
                }
                synchronized (mQueue) {
                    mQueue.put(request);
                    mQueue.notifyAll();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    public void offerRequest(UploadRequest request, long timeOut) {
        if (request == null) return;
        if (!done) {
            try {
                request.setUploadId(getUploadId());
                synchronized (mCurrentRequests) {
                    mCurrentRequests.add(request);
                }
                synchronized (mQueue) {
                    mQueue.offer(request, timeOut, TimeUnit.MILLISECONDS);
                    mQueue.notifyAll();
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private UploadRequest nextRequest() {
        UploadRequest request = null;
        // Wait until there's a packet or we're done.
        while (!done && (request = mQueue.poll()) == null) {
            try {
                synchronized (mQueue) {
                    mQueue.wait();
                }
            } catch (InterruptedException ie) {
                // Do nothing
            }
        }
        return request;
    }

    private void processQueue(Thread thisThread) {
        try {
            while (!done && (threadQueue != null && threadQueue == thisThread)) {
                UploadRequest request = nextRequest();
                if (request != null) {
                    synchronized (uploadHandler) {
                        uploadHandler.upload(request);
                    }
                }
            }
            Log.i(TAG, "upload thread is died");
            mQueue.clear();
            // Close the stream.
        } catch (Exception ioe) {
            Log.e(TAG, "Exception", ioe);
        }
    }

    public class CallBackDelivery {

        /**
         * Used for posting responses, typically to the main thread.
         */
        private final Executor mCallBackExecutor;

        /**
         * Constructor taking a handler to main thread.
         */
        public CallBackDelivery(final Handler handler) {
            // Make an Executor that just wraps the handler.
            mCallBackExecutor = new Executor() {
                @Override
                public void execute(Runnable command) {
                    handler.post(command);
                }
            };
        }

        public void postUploadStarted(final UploadRequest request) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    if (request.getUploadListener() != null) {
                        request.getUploadListener().onUploadStarted(request);
                    }
                }
            });
        }

        public void postUploadComplete(final UploadRequest request, final String response) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    synchronized (mCurrentRequests) {
                        mCurrentRequests.remove(request);
                    }
                    if (request.getUploadListener() != null) {
                        request.getUploadListener().onUploadComplete(request, response);
                    }
                }
            });
        }

        public void postUploadFailed(final UploadRequest request, final int errorCode, final String errorMsg) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    synchronized (mCurrentRequests) {
                        mCurrentRequests.remove(request);
                    }
                    if (request.getUploadListener() != null) {
                        request.getUploadListener().onUploadFailed(request, errorCode, errorMsg);
                    }
                }
            });
        }

        public void postProgressUpdate(final UploadRequest request, final long totalBytes, final long uploadedBytes,
                                       final int progress, final long speed) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    if (request.getUploadListener() != null) {
                        request.getUploadListener().onProgress(request, totalBytes, uploadedBytes, progress, speed);
                    }
                }
            });
        }

        public void postRetryRequest(final UploadRequest request, final long timeOut) {
            mCallBackExecutor.execute(new Runnable() {
                public void run() {
                    offerRequest(request, timeOut);
                }
            });
        }
    }

    private int getUploadId() {
        return mSequenceGenerator.incrementAndGet();
    }
}

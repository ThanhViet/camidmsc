package com.metfone.selfcare.network;

import android.view.View;

import java.util.concurrent.ConcurrentHashMap;

public class AudioVoicemailManager {

    // A single instance of PhotoManager, used to implement the singleton
    // pattern
    private static AudioVoicemailManager instance = null;
    private static ConcurrentHashMap<Integer, View> mapMessageIdWithConvertView = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<View, Integer> mapConvertViewWithMessageId = new ConcurrentHashMap<>();

    // A static block that sets class fields
    static {
        // Creates a single static instance of PhotoManager
        instance = new AudioVoicemailManager();
    }

    private AudioVoicemailManager() {
    }

    /**
     * Returns the PhotoManager object
     *
     * @return The global PhotoManager object
     */
    public static AudioVoicemailManager getInstance() {
        return instance;
    }

    public static void addToHashmap(int messageId, View convertView) {
        mapMessageIdWithConvertView.put(messageId, convertView);
        mapConvertViewWithMessageId.put(convertView, messageId);
    }

    public static View getConvertViewByMessageId(int messageId) {
        if (mapMessageIdWithConvertView.containsKey(messageId)) {
            return mapMessageIdWithConvertView.get(messageId);
        } else
            return null;
    }

    public static int getMessageIdByConvertView(View convertView) {
        if (mapConvertViewWithMessageId.containsKey(convertView)) {
            return mapConvertViewWithMessageId.get(convertView);
        } else
            return -1;
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsCheckShowPopupRequest extends BaseRequest<WsCheckShowPopupRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        @Expose
        private String isdn;
        @SerializedName("camid")
        @Expose
        private String camId;
        @SerializedName("deviceId")
        @Expose
        private String deviceId;
        @SerializedName("language")
        @Expose
        private String language;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getCamId() {
            return camId;
        }

        public void setCamId(String camId) {
            this.camId = camId;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

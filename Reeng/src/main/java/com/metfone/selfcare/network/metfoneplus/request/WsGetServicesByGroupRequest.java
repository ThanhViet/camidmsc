package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetServicesByGroupRequest extends BaseRequest<WsGetServicesByGroupRequest.Request> {
    public class Request {
        @SerializedName("serviceGroupId")
        private String serviceGroupId;
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("language")
        private String language;

        public String getServiceGroupId() {
            return serviceGroupId;
        }

        public void setServiceGroupId(String serviceGroupId) {
            this.serviceGroupId = serviceGroupId;
        }

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

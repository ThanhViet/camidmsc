package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WsGetOtpResponse {
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("errorMessage")
    @Expose
    private Object errorMessage;
    @SerializedName("result")
    @Expose
    private WsGetOtpDetailResponse result;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public WsGetOtpDetailResponse getResult() {
        return result;
    }

    public void setResult(WsGetOtpDetailResponse result) {
        this.result = result;
    }
}

package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsDetachIPResponse extends BaseResponse<WsDetachIPResponse.Response> {
    public class Response {
        @SerializedName("isdn")
        private String isdn;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }
    }
}

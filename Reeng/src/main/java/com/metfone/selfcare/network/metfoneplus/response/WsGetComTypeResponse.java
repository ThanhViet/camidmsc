package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.ProcessComplaint;
import com.metfone.selfcare.model.camid.TypeComplaint;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetComTypeResponse extends BaseResponse<WsGetComTypeResponse.Response> {
    public class Response {
        @SerializedName("listComType")
        private List<TypeComplaint> typeComplaints;

        public List<TypeComplaint> getTypeComplaints() {
            return typeComplaints;
        }

        public void setTypeComplaints(List<TypeComplaint> typeComplaints) {
            this.typeComplaints = typeComplaints;
        }
    }
}

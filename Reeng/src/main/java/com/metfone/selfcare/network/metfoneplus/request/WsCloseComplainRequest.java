package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

public class WsCloseComplainRequest extends BaseRequest<WsCloseComplainRequest.Request> {
    public class Request {
        @SerializedName("complaintId")
        @Expose
        public String complaintId;
        @SerializedName("isdn")
        @Expose
        public String isdn;
        @SerializedName("language")
        @Expose
        public String language;
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsAccountInfoRequest extends BaseRequest<WsAccountInfoRequest.Request> {
    public class Request {
        @SerializedName("language")
        private String language;
        @SerializedName("isdn")
        public String isdn;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }
    }
}

package com.metfone.selfcare.network.okhttp;

import com.metfone.selfcare.util.Log;

import okhttp3.OkHttpClient;

/**
 * Created by toanvk2 on 11/25/2016.
 */
public class OkHttp {
    private static final String TAG = OkHttp.class.getSimpleName();
    private static OkHttpClient okHttpClient;

    static {
        Log.i(TAG, "init OkHttp ");
        okHttpClient = new OkHttpClient.Builder().build();
    }

    //private static OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

    public static OkHttpClient getClient() {
        return okHttpClient;
    }

    public static synchronized OkHttpClient reCreateClient() {
        okHttpClient = new OkHttpClient.Builder().build();
        return okHttpClient;
    }
}
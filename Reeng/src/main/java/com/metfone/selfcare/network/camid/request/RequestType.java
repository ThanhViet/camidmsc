package com.metfone.selfcare.network.camid.request;

public interface RequestType {
    enum Type {
        PASSWORD("passsword"),
        OTP("otp"),
        GOOGLE("google"),
        FACEBOOK("facebook");

        String VALUE;

        Type(String value) {
            this.VALUE = value;
        }
    }
}

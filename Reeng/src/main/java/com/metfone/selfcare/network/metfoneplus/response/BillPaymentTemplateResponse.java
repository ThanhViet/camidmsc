package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.metfoneplus.billpayment.MPBillPaymentTemplate;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class BillPaymentTemplateResponse extends BaseResponse<BillPaymentTemplateResponse.Response> {
    public static class Response{
        @SerializedName("totalPaymentHistorys")
        public int totalPaymentHistorys;
        @SerializedName("paymentHistoryList")
        public List<MPBillPaymentTemplate> paymentHistoryList;
    }
}

package com.metfone.selfcare.network.xmpp;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.GameIQHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public class PresenceListener implements PacketListener {
    private static final String TAG = PresenceListener.class.getSimpleName();
    private ApplicationController mContext;

    public PresenceListener(ApplicationController context) {
        this.mContext = context;
    }

    // packet listener
    @Override
    public void processPacket(Packet packet) {
        Log.d(TAG, "processPacket: " + packet.toXML());
        ApplicationController application = (ApplicationController) mContext.getApplicationContext();
        Presence presence = (Presence) packet;
        if (presence.getType() == Presence.Type.error) {
            mContext.logDebugContent("Presence.Type.error: " + packet.toXML());
            return;
        }
        Presence.SubType subType = presence.getSubType();
        if (subType == Presence.SubType.normal) {
            if (presence.getType() == Presence.Type.unavailable) {
                // ban tin offline
                application.getContactBusiness().updateContactAfterReceiverPresence(presence);
            } else {
                mContext.logDebugContent("SubType.normal Type.unavailable: " + packet.toXML());
                Log.i(TAG, "subType nomarl and sub");
            }
        } else if (subType == Presence.SubType.change_domain) {
            UrlConfigHelper urlConfigHelper = UrlConfigHelper.getInstance(mContext);
            // change domain
            boolean isDomainMsgChange = urlConfigHelper.updateDomainMocha(presence.getDomainFile()
                    , presence.getDomainMsg(), presence.getDomainOnMedia(), presence.getDomainImage(), "");
            // change domain keeng
            urlConfigHelper.updateDomainKeeng(presence.getDomainServiceKeeng(),
                    presence.getDomainMedia2Keeng(), presence.getDomainImageKeeng());

            urlConfigHelper.updateDomainFileV1(presence.getDomainFileV1());
            urlConfigHelper.updateDomainImageV1(presence.getDomainImageV1());
            urlConfigHelper.updateDomainOnMediaV1(presence.getDomainOnMediaV1());
            urlConfigHelper.updateDomainMochaVideo(presence.getDomainMochaVideo());
            urlConfigHelper.updateDomainKeengMusic(presence.getDomainKeengMusic());
            urlConfigHelper.updateDomainKeengMusicSearch(presence.getDomainKeengMusicSearch());
            urlConfigHelper.updateDomainKeengMovies(presence.getDomainKeengMovies());
            urlConfigHelper.updateDomainNetNews(presence.getDomainNetnews());
            urlConfigHelper.updateDomainTiin(presence.getDomainTiin());

            //change config
            application.getReengAccountBusiness().setConfigFromServer(presence.getVip(),
                    presence.getCBNV(), presence.getCall(), presence.getSSL(),
                    presence.getSmsIn(), presence.getCallOut(), presence.getAvnoNumber(),
                    presence.getMochaApi(), presence.getAvnoEnable(), presence.getTabCallEnable(),
                    presence.getCalloutGlobalEnable(), presence.getOperator(), presence.getChangeNumber(),
                    presence.getUsingDesktop(), presence.getTranslatable(), presence.getfCallViaFS(), presence.getE2eEnable());
            // call config
            if (isDomainMsgChange) {// reconnect
                application.getReengAccountBusiness().updateDomainTask();
            }
        } else if (subType == Presence.SubType.music_info ||
                subType == Presence.SubType.music_sub ||
                subType == Presence.SubType.music_unsub ||
                subType == Presence.SubType.confide_accepted) {
            application.getMusicBusiness().updateStrangerMusicAfterReceiverPresence(presence, subType);
        } else if (subType == Presence.SubType.contactInfo) {
            application.getContactBusiness().updateContactAfterReceiverPresenceV3(presence);
        } else if (subType == Presence.SubType.e2e) {
            application.getContactBusiness().updateE2EContact(presence.getFrom(), presence.getPreKey());
        } else if (subType == Presence.SubType.star_sub ||
                subType == Presence.SubType.star_unsub) {
            Log.i(TAG, "subType star_sub/unSub");
            // khong xu ly truong hop nay
        } else if (subType == Presence.SubType.count_users) {// nhan dc tin bao hieu so nguoi follow sao
            application.getMusicBusiness().processChangeMemberFollow(presence);
        } else if (subType == Presence.SubType.change_background) {
            application.getMusicBusiness().processChangeBackgroundRoom(presence);
        } else if (subType == Presence.SubType.feedInfo) {
            application.getFeedBusiness().updateFeedAfterReceiverPresence(presence);
        } else if (subType == Presence.SubType.updateInfo) {
            application.getReengAccountBusiness().updateUserInfoFromPresence(presence);
        } else if (subType == Presence.SubType.package_info) {
            application.getReengAccountBusiness().updateUserPackageInfo(presence);
        } else if (subType == Presence.SubType.strangerLocation) {
            application.getReengAccountBusiness().updateLocationId(presence.getLocationId());
        } else if (subType == Presence.SubType.game) {
            GameIQHelper.getInstant(application).processViewerPlayerIncoming(presence);
        } else if (subType == Presence.SubType.change_virtual) {
            application.getReengAccountBusiness().updateAVNONumber(presence.getAvnoNumber());
        } else {    // chang avatar, stt, bg,fg ....
            application.getContactBusiness().updateContactAfterReceiverPresence(presence);
        }
    }
}
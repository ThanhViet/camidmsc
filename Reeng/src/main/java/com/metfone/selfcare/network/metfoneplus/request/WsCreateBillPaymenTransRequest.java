package com.metfone.selfcare.network.metfoneplus.request;

import com.metfone.selfcare.network.metfoneplus.GenerateRequestBody;

public class WsCreateBillPaymenTransRequest{
    public String phoneNumber;
    public String totalAmount;
    public String accountEmoney;
    public String ftthAccount;
    public String contractIdInfo;
    public String paymentAmount;
    public String paymentMethod;
    public String ftthType;
    public String ftthName;

}

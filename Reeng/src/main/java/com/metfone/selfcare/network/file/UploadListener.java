package com.metfone.selfcare.network.file;

/**
 * Created by toanvk2 on 7/21/2016.
 */
public interface UploadListener {
    void onUploadStarted(UploadRequest uploadRequest);

    void onUploadComplete(UploadRequest uploadRequest, String response);

    void onUploadFailed(UploadRequest uploadRequest, int errorCode, String errorMessage);

    void onProgress(UploadRequest uploadRequest, long totalBytes, long uploadedBytes, int progress,long speed);
}

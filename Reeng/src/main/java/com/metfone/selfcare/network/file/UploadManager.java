package com.metfone.selfcare.network.file;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengMessage;

/**
 * Created by toanvk2 on 7/21/2016.
 */
public class UploadManager {
    private ApplicationController mApplication;
    private UploadQueue mRequestQueue;

    /**
     * Default constructor
     */
    public UploadManager(ApplicationController application) {
        this.mApplication = application;
        mRequestQueue = new UploadQueue(mApplication);
    }

    /**
     * Construct with provided callback handler
     *
     * @param
     */
    /*public UploadManager(Handler callbackHandler) throws InvalidParameterException {
        mRequestQueue = new UploadRequestQueue(callbackHandler);
        mRequestQueue.start();
    }

    public UploadManager(int threadPoolSize) {
        mRequestQueue = new UploadRequestQueue(threadPoolSize);
        mRequestQueue.start();
    }*/
    public void add(UploadRequest request) throws IllegalArgumentException {
        if (request == null) {
            throw new IllegalArgumentException("UploadRequest cannot be null");
        }
        mRequestQueue.addRequest(request);
    }

    public void cancel(int uploadId) {
        mRequestQueue.cancelRequest(uploadId);
    }

    public void cancel(ReengMessage message) {
        mRequestQueue.cancelRequest(message);
    }

    public void cancelAll() {
        mRequestQueue.cancelAll();
    }
}
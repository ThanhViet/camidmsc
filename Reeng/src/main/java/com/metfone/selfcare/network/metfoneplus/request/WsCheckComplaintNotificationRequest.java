package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

/**
 * API for get complaint history
 */
public class WsCheckComplaintNotificationRequest extends BaseRequest<WsCheckComplaintNotificationRequest.Request> {
    public class Request {
        @SerializedName("language")
        public String language;
        @SerializedName("isdn")
        public String isdn;
    }
}

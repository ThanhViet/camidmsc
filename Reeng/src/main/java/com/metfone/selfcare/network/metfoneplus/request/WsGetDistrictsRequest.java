package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetDistrictsRequest extends BaseRequest<WsGetDistrictsRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("language")
        private String language;
        @SerializedName("provinceId")
        private String provinceId;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }
    }
}

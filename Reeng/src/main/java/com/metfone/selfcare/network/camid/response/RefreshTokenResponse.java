package com.metfone.selfcare.network.camid.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.model.camid.User;

import java.util.List;

public class RefreshTokenResponse extends BaseResponse<RefreshTokenResponse.Response>{
    public  class Response {
        @SerializedName("error")
        @Expose
        private Object error;
        @SerializedName("error_description")
        @Expose
        private Object errorDescription;
        @SerializedName("access_token")
        @Expose
        private String accessToken;
        @SerializedName("expires_in")
        @Expose
        private Integer expiresIn;
        @SerializedName("refresh_expires_in")
        @Expose
        private Integer refreshExpiresIn;
        @SerializedName("refresh_token")
        @Expose
        private String refreshToken;
        @SerializedName("token_type")
        @Expose
        private String tokenType;

        public Object getError() {
            return error;
        }

        public void setError(Object error) {
            this.error = error;
        }

        public Object getErrorDescription() {
            return errorDescription;
        }

        public void setErrorDescription(Object errorDescription) {
            this.errorDescription = errorDescription;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public Integer getExpiresIn() {
            return expiresIn;
        }

        public void setExpiresIn(Integer expiresIn) {
            this.expiresIn = expiresIn;
        }

        public Integer getRefreshExpiresIn() {
            return refreshExpiresIn;
        }

        public void setRefreshExpiresIn(Integer refreshExpiresIn) {
            this.refreshExpiresIn = refreshExpiresIn;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }
    }


}

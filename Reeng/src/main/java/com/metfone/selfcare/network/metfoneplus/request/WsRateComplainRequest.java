package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsRateComplainRequest extends BaseRequest<WsRateComplainRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        @Expose
        public String isdn;
        @SerializedName("complaintId")
        @Expose
        public String complaintId;
        @SerializedName("rate")
        @Expose
        public String rate;
        @SerializedName("language")
        @Expose
        public String language;
    }
}

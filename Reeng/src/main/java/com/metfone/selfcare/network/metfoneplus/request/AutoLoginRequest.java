package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.GenerateRequestBody;

public class AutoLoginRequest implements GenerateRequestBody {
    @SerializedName("appCode")
    private String appCode;
    // isdn
    @SerializedName("username")
    private String userName;
    @SerializedName("prefix")
    private String prefix;
    @SerializedName("device")
    private String device;

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}

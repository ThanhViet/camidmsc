package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsFindStoreByAddrRequest extends BaseRequest<WsFindStoreByAddrRequest.Request> {
    public class Request {
        @SerializedName("districtId")
        private String districtId;
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("latitude")
        private double latitude;
        @SerializedName("language")
        private String language;
        @SerializedName("provinceId")
        private String provinceId;
        @SerializedName("longitude")
        private double longitude;

        public String getDistrictId() {
            return districtId;
        }

        public void setDistrictId(String districtId) {
            this.districtId = districtId;
        }

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }
}

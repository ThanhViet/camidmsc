package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WsSearchNumberToBuyRequest extends BaseRequest<WsSearchNumberToBuyRequest.Request> implements Serializable {
    @Getter
    @Setter
    public class Request {
        @SerializedName("prefix")
        String prefix = "";
        @SerializedName("fromPrice")
        int fromPrice;
        @SerializedName("toPrice")
        int toPrice;
        @SerializedName("typeNumber")
        String typeNumber;
        @SerializedName("pageNo")
        int pageNo;
        @SerializedName("pageSize")
        int pageSize;
        @SerializedName("language")
        String language;
    }
}

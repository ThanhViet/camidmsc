package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WsSearchNumberToBuyResponse extends BaseResponse<WsSearchNumberToBuyResponse.Response> {
    @Getter
    @Setter
    public class Response {
        @SerializedName("totalPage")
        String totalPage;
        @SerializedName("totalSize")
        String totalSize;
        @SerializedName("fee")
        String fee;
        @SerializedName("commitDuration")
        String commitDuration;
        @SerializedName("lstNumberToBuy")
        List<AvailableNumber> lstNumberToBuy;
    }
}

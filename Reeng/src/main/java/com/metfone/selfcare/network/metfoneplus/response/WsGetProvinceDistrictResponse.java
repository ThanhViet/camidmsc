package com.metfone.selfcare.network.metfoneplus.response;

import com.metfone.selfcare.model.camid.ProvinceDistrict;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetProvinceDistrictResponse extends BaseResponse<List<ProvinceDistrict>> {

}

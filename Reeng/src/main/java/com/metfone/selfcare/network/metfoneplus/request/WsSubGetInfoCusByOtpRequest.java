package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsSubGetInfoCusByOtpRequest extends BaseRequest<WsSubGetInfoCusByOtpRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        @Expose
        private String isdn;
        @SerializedName("code")
        @Expose
        private String otp;
        @SerializedName("service")
        @Expose
        private String service;
        @SerializedName("language")
        @Expose
        private String language;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

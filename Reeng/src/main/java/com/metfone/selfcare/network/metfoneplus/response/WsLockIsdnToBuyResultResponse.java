package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WsLockIsdnToBuyResultResponse {
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("object")
    @Expose
    private Object object;
    @SerializedName("userMsg")
    @Expose
    private String userMsg;
    @SerializedName("wsResponse")
    @Expose
    private Object wsResponse;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public Object getWsResponse() {
        return wsResponse;
    }

    public void setWsResponse(Object wsResponse) {
        this.wsResponse = wsResponse;
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsUpdateDeviceTokenRequest extends BaseRequest<WsUpdateDeviceTokenRequest.Request> {
    public class Request{
        public String language;
        public String camId;
        public String token;
        public String os = "0";
        public String deviceId;
    }
}

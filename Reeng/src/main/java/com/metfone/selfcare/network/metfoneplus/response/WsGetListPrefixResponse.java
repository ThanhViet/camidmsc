package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WsGetListPrefixResponse extends BaseResponse<WsGetListPrefixResponse.Response> {
    @Getter
    @Setter
    public class Response {
        @SerializedName("prefix")
        public String prefix;
    }

}

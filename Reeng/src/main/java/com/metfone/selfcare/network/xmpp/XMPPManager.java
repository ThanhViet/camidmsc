package com.metfone.selfcare.network.xmpp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.broadcast.AlarmReceiver;
import com.metfone.selfcare.business.ApplicationStateManager;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageRetryManager;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.business.XMPPCode;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.CPresence;
import com.metfone.selfcare.database.model.message.MochaCallMessage;
import com.metfone.selfcare.database.model.message.MochaCallOutMessage;
import com.metfone.selfcare.firebase.FireBaseHelper;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.ConfigLocalized;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.DummySSLSocketFactory;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.helper.message.PacketMessageId;
import com.metfone.selfcare.listeners.XMPPConnectivityChangeListener;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;
import com.viettel.util.PowerModeHelper;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageEventManager;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromContainsFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.EventReceivedMessage;
import org.jivesoftware.smack.packet.GSMMessage;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.IQInfo;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Ping;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.ReengCallOutPacket;
import org.jivesoftware.smack.packet.ReengCallPacket;
import org.jivesoftware.smack.packet.ReengEventPacket;
import org.jivesoftware.smack.packet.ReengMessagePacket;
import org.jivesoftware.smack.packet.ReengMusicPacket;
import org.jivesoftware.smack.packet.ShareMusicMessagePacket;
import org.jivesoftware.smack.packet.VoicemailGSMResponseMessage;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.sasl.NonSASLAuthInfo;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ThaoDV on 6/13/14.
 */
public class XMPPManager {
    private static final String TAG = XMPPManager.class.getSimpleName();
    private static final int PING_FAIL_THRESHOLD = 1; // 0 --> 1 --> 2
    private static CopyOnWriteArrayList<XMPPConnectivityChangeListener> mXmppConnectivityChangeListeners = new
            CopyOnWriteArrayList<>();
    private static int mConnectionState;
    private MessageEventManager messageEventManager;
    private ConnectionConfiguration mConfig;
    private XMPPConnection mConnection;
    private XMPPConnectionListener2 mXmppConnectionListener;
    private ApplicationController mApplication;
    private boolean isDisconnecting = false;
    // ------------listener-----------------------------------------
    private PresenceListener presenceChangeListener;
    // message
    private ReengMessageListener reengMessageListener;
    private ShareMusicMessageListener shareMusicMessageListener;
    private RecevedSoloMessageListener recevedSoloMessageListener;
    private GSMMessageListener gsmMessageListener;
    private VoicemailGSMResponseMessageListener voicemailGSMResponseMessageListener;
    //
    private PacketFilter saslFilter;
    private SuccessNonSASLListener successNonSASLListener;
    private MessageRetryManager mMessageRetryManager;
    private long lastTimeOfReceivedPacket = 0;
    private long pingInterval = Constants.ALARM_MANAGER.PING_TIMER;
    private SharedPreferences mPref;
    private static long lastTimeConnectedSucceed = 0;
    private boolean needSendIQUpdate;

    public enum IqInfoFirstTime {
        AFTER_INSTALL(0),
        AFTER_UPDATE(1),
        NORMAL(2);
        public int VALUE;

        IqInfoFirstTime(int VALUE) {
            this.VALUE = VALUE;
        }
    }

    public XMPPManager(ApplicationController app) {
        mApplication = app;
        mPref = app.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);
        configSmack();
        mMessageRetryManager = MessageRetryManager.getInstance(app, this);
    }

    public MessageRetryManager getMessageRetryManager() {
        return mMessageRetryManager;
    }

    public static synchronized void addXMPPConnectivityChangeListener(XMPPConnectivityChangeListener
                                                                              reengMessageListener) {
        if (!mXmppConnectivityChangeListeners.contains(reengMessageListener))
            mXmppConnectivityChangeListeners.add(reengMessageListener);
    }

    public static synchronized void removeXMPPConnectivityChangeListener(XMPPConnectivityChangeListener
                                                                                 reengMessageListener) {
        if (mXmppConnectivityChangeListeners.contains(reengMessageListener))
            mXmppConnectivityChangeListeners.remove(reengMessageListener);
    }

    private static synchronized void notifyXMPPConnected() {
        lastTimeConnectedSucceed = System.currentTimeMillis();
        mConnectionState = Constants.CONNECTION_STATE.CONNECTED;
        ApplicationController.self().cancelNotification(Constants.NOTIFICATION.NOTIFY_FAKE_NEW_MSG);
        //PopupHelper.getInstance(mApplication).hideDialogProgressChangeDomain();
        if (mXmppConnectivityChangeListeners == null)
            return;
        Log.i(TAG, "notifyXMPPConnected");
        for (XMPPConnectivityChangeListener listener : mXmppConnectivityChangeListeners) {
            if (listener != null) {
                listener.onXMPPConnected();
            }
        }
    }

    public static synchronized void notifyXMPPConnecting() {
        mConnectionState = Constants.CONNECTION_STATE.CONNECTING;
        if (mXmppConnectivityChangeListeners == null)
            return;
        for (XMPPConnectivityChangeListener listener : mXmppConnectivityChangeListeners) {
            Log.i(TAG, "notifyXMPPConnecting");
            if (listener != null) {
                listener.onXMPPConnecting();
            }
        }
    }

    public static synchronized void notifyXMPPDisconneted() {
        mConnectionState = Constants.CONNECTION_STATE.NOT_CONNECT;
        if (mXmppConnectivityChangeListeners == null)
            return;
        for (XMPPConnectivityChangeListener listener : mXmppConnectivityChangeListeners) {
            if (listener != null) {
                listener.onXMPPDisconnected();
            }
        }
    }

    public static long getLastTimeConnectedSucceed() {
        return lastTimeConnectedSucceed;
    }

    public boolean isConnected() {
        if (mConnection == null)
            return false;
        return mConnection.isConnected();
    }

    public boolean isAuthenticated() {
        //return !(!isConnected() || !mConnection.isAuthenticated());
        if (mConnection != null)
            try {
                boolean connectAuth = mConnection.isAuthenticated();
                return (isConnected() && connectAuth);
            } catch (NullPointerException ex) {
                Log.f(TAG, "NullPointerException mConnection");
            }
        return false;
    }

    public void manualDisconnect() {
        if (isDisconnecting) return;
        isDisconnecting = true;
        if (mConnection != null) {
            Log.i(TAG, "manual disconnect");
            removeConnectionListener();
            removeAllListener();
            mConnection.disconnect(new Presence(Presence.Type.unavailable));
            notifyXMPPDisconneted();
            stopPing();
        }
        mConfig = null;
        mConnection = null;
        isDisconnecting = false;

    }

    /**
     * listener for connection
     */
    private void addConnectionListener() {
        if (isConnected()) {
            mXmppConnectionListener = new XMPPConnectionListener2(mApplication,
                    mConnection, this);
            mConnection.addConnectionListener(mXmppConnectionListener);
            NetworkHelper.addNetworkConnectivityChangeListener(mXmppConnectionListener);
        }
    }

    private void addAllListener(ApplicationController applicationController) {
        // contact
        this.addPresenceChangedListener();
        this.addReengMessageListener(applicationController);
        this.addShareMusicMessageListener(applicationController);
        // message solo
        this.addMessageEventListener();
        this.addMessageReceivedListener(applicationController);
        this.addGSMMessageListener(applicationController);
        this.addVoicemailGSMResponseMessageListener(applicationController);
        //
        //addPacketTimeoutListener();
        //        addMonitorConnectOnBackgroundListener();
    }

    private void addPresenceChangedListener() {
        PacketFilter filter = new AndFilter(new PacketTypeFilter(
                org.jivesoftware.smack.packet.Presence.class));
        presenceChangeListener = new PresenceListener(mApplication);
        mConnection.addPacketListener(presenceChangeListener, filter);
    }

    /**
     * addReengMessageListener
     */
    private void addReengMessageListener(
            ApplicationController applicationController) {
        PacketFilter filter = new AndFilter(new PacketTypeFilter(
                ReengMessagePacket.class));
        reengMessageListener = new ReengMessageListener(applicationController);
        mConnection.addPacketListener(reengMessageListener, filter);
    }

    /**
     * addReengMessageListener
     */
    private void addShareMusicMessageListener(
            ApplicationController applicationController) {
        PacketFilter filter = new AndFilter(new PacketTypeFilter(
                ShareMusicMessagePacket.class));
        shareMusicMessageListener = new ShareMusicMessageListener(
                applicationController);
        mConnection.addPacketListener(shareMusicMessageListener, filter);
    }

    /**
     * add message event listener
     *
     * @author thaodv
     */
    private void addMessageEventListener() {
        // create MessageEventManager
        messageEventManager = new MessageEventManager(mConnection);
    }

    /**
     * addGSMMessageListener
     *
     * @author thaodv
     */
    private void addGSMMessageListener(ApplicationController app) {
        String filterString = Constants.XMPP.XMPP_SMS + "."
                + Constants.XMPP.XMPP_DOMAIN;
        PacketFilter filter = new AndFilter(new PacketTypeFilter(
                GSMMessage.class), new FromContainsFilter(filterString));
        gsmMessageListener = new GSMMessageListener(app);
        mConnection.addPacketListener(gsmMessageListener, filter);
    }

    /**
     * add voicemail response
     *
     * @author thaodv
     */
    private void addVoicemailGSMResponseMessageListener(
            ApplicationController app) {
        PacketFilter filter = new AndFilter(new PacketTypeFilter(
                VoicemailGSMResponseMessage.class));
        voicemailGSMResponseMessageListener = new VoicemailGSMResponseMessageListener(
                app);
        mConnection.addPacketListener(voicemailGSMResponseMessageListener,
                filter);
    }

    private void addMessageReceivedListener(
            ApplicationController applicationController) {
        PacketFilter filter = new AndFilter(new PacketTypeFilter(
                EventReceivedMessage.class));
        recevedSoloMessageListener = new RecevedSoloMessageListener(
                applicationController);
        mConnection.addPacketListener(recevedSoloMessageListener, filter);

    }

    public void removeConnectionListener() {
        if (mConnection != null) {
            if (mXmppConnectionListener != null) {
                mConnection.removeConnectionListener(mXmppConnectionListener);
            }
            removeSASLListener();
        }
        NetworkHelper.addNetworkConnectivityChangeListener(null);
    }

    public void removeAllListener() {
        Log.i(TAG, "removeAllListener");
        if (mConnection != null) {
            if (presenceChangeListener != null) {
                mConnection.removePacketListener(presenceChangeListener);
            }
            if (reengMessageListener != null) {
                mConnection.removePacketListener(reengMessageListener);
            }
            if (recevedSoloMessageListener != null) {
                mConnection.removePacketListener(recevedSoloMessageListener);
            }
            if (gsmMessageListener != null) {
                mConnection.removePacketListener(gsmMessageListener);
            }
            if (voicemailGSMResponseMessageListener != null) {
                mConnection
                        .removePacketListener(voicemailGSMResponseMessageListener);
            }
            /*if (packetTimeoutListener != null) {
                mConnection.deletePacketSendingListener(packetTimeoutListener);
                mConnection.removePacketListener(packetTimeoutListener);
                packetTimeoutListener.shutdown();
            }*/
        }
        // set null listener
        presenceChangeListener = null;
        reengMessageListener = null;
        recevedSoloMessageListener = null;
        gsmMessageListener = null;
        voicemailGSMResponseMessageListener = null;
        mXmppConnectionListener = null;
    }

    private void configSmack() {
        SmackConfiguration
                .setPacketReplyTimeout(Config.Smack.PACKET_REPLY_TIMEOUT);
        SmackConfiguration
                .setKeepAliveInterval(Config.Smack.KEEP_ALIVE_INTERVAL);
        if (PowerModeHelper.isPowerSaveMode(mApplication))
            pingInterval = Long.valueOf(mPref.getString(Constants.PREFERENCE.CONFIG.PING_INTERVAL_POWER_SAVE_MODE, "10000"));
        else
            pingInterval = Long.valueOf(mPref.getString(Constants.PREFERENCE.CONFIG.PING_INTERVAL, "90000"));
        Log.i(TAG, "pingInterval = " + pingInterval);
        SmackConfiguration.setPingInterval(Config.Smack.PING_INTERVAL);
        setConnectionConfiguration();
    }

    private void setConnectionConfiguration() {
        try {
            /*URL url = new URL(UrlConfigHelper.getInstance(mApplication)
                    .getUrlConfigOfMsg());*/
            String[] url = UrlConfigHelper.getInstance(mApplication)
                    .getUrlConfigOfMsg().split(":");
            int port = Integer.parseInt(url[1]);
            String domain = url[0];
            Log.f(TAG, "domain: " + domain + " port: " + port);
            mApplication.logDebugContent("domain: " + domain + " port: " + port);
            int configSSL;
            /*if (Config.Server.SERVER_TEST) {
                configSSL = -1;
            } else {*/
            configSSL = mPref.getInt(Constants.PREFERENCE.PREF_MOCHA_ENABLE_SSL, -1);
//            }
            Log.d(TAG, "setConnectionConfiguration - url: " + domain + " configSSL: " + configSSL);
            //TODO không set lại port khi chưa nhận được config ssl từ sv nữa, config build test thì nhận luôn port
            // nhập vào.
            mConfig = new ConnectionConfiguration(domain, port, Config.Smack.RESOURCE);
            mConfig.setDebuggerEnabled(BuildConfig.DEBUG || LogDebugHelper.getInstance().isEnableLog());
            // do something for a debug build
            mConfig.setReconnectionAllowed(true);
            mConfig.setResource(Config.Smack.RESOURCE);
            mConfig.setSASLAuthenticationEnabled(false);
            mConfig.setMochaCaPem(mApplication.getResources().openRawResource(R.raw.mochachat));
            // nâng cấp từ bản cũ nên bản mới value mặc định của ios là 0 nên sẽ để ssl==1 là on, ==2 là off, ==0 là
            // giá trị default(chưa nhận config)
            if (configSSL == 1) {//enable,
                mConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
//                mConfig.setSocketFactory(new DummySSLSocketFactory(mApplication.getResources().openRawResource(R.raw.mochachat)));
                mConfig.setSocketFactory(new DummySSLSocketFactory(mApplication.getResources().openRawResource(R.raw.mochachat)));
            } else if (configSSL == 2) {//disable
                mConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
            } else {
                //TODO, trường hợp chưa có config ssl của sv thì dùng config default, hoặc check theo port đối với
                // bản cũ
                if (!ConfigLocalized.ENABLE_TLS || port == ConfigLocalized.PORT_NON_SSL) {
                    mConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                } else {
                    mConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
                    mConfig.setSocketFactory(new DummySSLSocketFactory(mApplication.getResources().openRawResource(R.raw.mochachat)));
                }
            }
            mConfig.setRosterLoadedAtLogin(false);
            mConfig.setSendPresence(false);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void setTokenForConfig(String token) {
        if(mConfig != null){
            mConfig.setToken(token);
        }
    }

    public void sendPacketNoWaitResponse(Packet packet) throws XMPPException,
            IllegalStateException, NullPointerException {
        // TODO: [START] Cambodia version
//        if (mConnection == null) {
//            throw new XMPPException("");
//        }
//        if (mConnection == null || !mConnection.isAuthenticated()) {
//            throw new XMPPException("");
//        }
        if (mConnection != null) {
            mConnection.sendPacket(packet);
        }
        // TODO: [START] Cambodia version
//        if (mConnection == null) {
//            throw new XMPPException("");
//        }
//        if (mConnection == null || !mConnection.isAuthenticated()) {
//            throw new XMPPException("");
//        }
//        mConnection.sendPacket(packet);
    }

    /**
     * @param packet
     * @param isAnonymous
     * @return
     */
    public IQ sendPacketThenWaitingResponse(Packet packet, boolean isAnonymous)
            throws XMPPException, IllegalStateException, NullPointerException {
        if (isAnonymous) {
            initConnectionAsAnonymous();
        }
        if (mConnection == null) {
            throw new XMPPException("no connection");
        }
        if (!isAnonymous && !mConnection.isAuthenticated()) {
            throw new XMPPException("not authenticated");
        }
        PacketIDFilter filter = new PacketIDFilter(packet.getPacketID());
        PacketCollector response = mConnection.createPacketCollector(filter);
        mConnection.sendPacket(packet);
        // mConnection.disconnect();
        return (IQ) response.nextResult(SmackConfiguration
                .getPacketReplyTimeout());
    }

    /**
     * @param packet iqGroup
     * @return
     */
    public IQ sendPacketIQGroupThenWaitingResponse(Packet packet)
            throws XMPPException, IllegalStateException, NullPointerException {
        if (mConnection == null) {
            throw new XMPPException("no connection");
        }
        if (!mConnection.isAuthenticated()) {
            throw new XMPPException("not authenticated");
        }
        PacketIDFilter filter = new PacketIDFilter(packet.getPacketID());
        PacketCollector response = mConnection.createPacketCollector(filter);
        mConnection.sendPacket(packet);
        // mConnection.disconnect();
        return (IQ) response.nextResult(SmackConfiguration
                .getPacketReplyTimeout());
    }

    private void initConnectionAsAnonymous() throws XMPPException {
        Log.i(TAG, "initConnectionAsAnonymous");
        if (mConnection == null) {
            Log.i(TAG, "mConnection == null");
            if (mConfig == null) {
                Log.i(TAG, "mConfig == null");
                configSmack();
            }
            mConnection = new XMPPConnection(mConfig, mApplication);
        }
        if (!mConnection.isConnected()) {
            Log.i(TAG, "!mConnection.isConnected()");
//            String revision = mApplication.getResources().getString(R.string.revision);
            mConnection.connect();
        }
    }

    public void destroyAnonymousConnection() {
        if (mConnection != null) {
            mConnection.disconnect();
            mConnection = null;
        }
    }

    /**
     * init connection as anonymous then login by code
     *
     * @param mContext
     * @param mPhoneNumber
     * @param password
     * @throws XMPPException
     * @throws IllegalStateException
     */

    public void connectByCode(ApplicationController mContext, String mPhoneNumber, String password,
                              String countryCode, String mechanism, String oldUser, String oldToken)
            throws XMPPException, IllegalStateException {
        // updateState(MessageConstants.CONNECTION_STATE.CONNECTING);
        initConnectionAsAnonymous();
        // add success packet filter to get token
        addSASLListener(mContext);
        // login
        addAllListener(mContext);
        mConnection.customLogin(mPhoneNumber, password, Config.Smack.RESOURCE,
                mechanism, Config.REVISION, countryCode, oldUser, oldToken, null, mApplication.getReengAccountBusiness().isDev());
        //long elapsedTime = (System.currentTimeMillis() - beginTime) / 1000;
        addConnectionListener();
        // updateState(MessageConstants.CONNECTION_STATE.CONNECTED);
        sendPresenceAfterLogin(Connection.CODE_AUTH_NON_SASL, true);
        // start ping
        startPing();
        // set roster
        ApplicationController application = (ApplicationController) mContext
                .getApplicationContext();
        if (!TextUtils.isEmpty(oldUser) && !TextUtils.isEmpty(oldToken)) {
            //case đổi số thì ko load lại data ở đây
        } else {
            application.loadDataAfterLogin();
        }
        DeviceHelper.checkToSendDeviceIdAfterLogin(application, mPhoneNumber);
    }

    /**
     * @param ctx
     * @param mPhoneNumber
     * @param mToken
     * @throws XMPPException
     * @throws IllegalStateException
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public synchronized void connectByToken(ApplicationController ctx, String mPhoneNumber,
                                            String mToken, String countryCode, String prekey) {
        Log.f(TAG, "[VIETTEL] connect to server by token");
        Log.f(TAG,"[CAMID] token: " + mToken + " > phoneNumber: " + mPhoneNumber);
        ctx.logDebugContent("[VIETTEL] connect to server by token");
        final long time = System.currentTimeMillis();
        mApplication = ctx;
        if (mConfig == null) {
            configSmack();
        }
        if (mConfig == null || mApplication == null || TextUtils.isEmpty(mPhoneNumber) || TextUtils.isEmpty(mToken)) {
            return;
        }
        String messageException = "default";
        String errorCode = Constants.LogKQI.StateKQI.ERROR_EXCEPTION.getValue();
        try {
            mConfig.setUserName(mPhoneNumber);
            mConfig.setToken(mToken);
            mConfig.setRevision(Config.REVISION);
            mConfig.setCountryCode(countryCode);
            mConfig.setPrekey(prekey);
            mConfig.setDev(mApplication.getReengAccountBusiness().isDev());
        } catch (NullPointerException ex) {
            return;
        }

        if (mConnection == null) {
            mConnection = new XMPPConnection(mConfig, mApplication);
        }
        if (mConnection.isAuthenticated()) {
            Log.f(TAG, "[VIETTEL] reconnectByToken but already authenticated --> return now");
            ctx.logDebugContent("[VIETTEL] reconnectByToken but already authenticated --> return now");
            notifyXMPPConnected();
            mApplication.getReengAccountBusiness().processChangeNumber();
            return;
        }
        // updateState(MessageConstants.CONNECTION_STATE.CONNECTING);
        boolean loginSuccessful = false;
        try {
            // add success packet filter to get token
            addSASLListener(mApplication);
            // logint
            addAllListener(mApplication);
            mConnection.connect();
            loginSuccessful = true;
            Log.f(TAG, "login successful");
            long deltaTime = (System.currentTimeMillis() - time);
            ctx.logDebugContent("login successful take: " + deltaTime);
            LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.LOGIN_BY_TOKEN,
                    String.valueOf(deltaTime), String.valueOf(time), Constants.LogKQI.StateKQI.SUCCESS.getValue());
            addConnectionListener();
            sendPresenceAfterLogin(Connection.TOKEN_AUTH_NON_SASL, false);
            /*String ssss = mConnection.getSocket().getLocalAddress().getHostAddress();
            Log.d(TAG, "connected getHostAddress: " + ssss);*/
        } catch (IllegalStateException ie) {
            Log.e(TAG, "Exception", ie);
            messageException = "IllegalStateException";
        } catch (XMPPException xe) {
            Log.e(TAG, "Exception", xe);
            XMPPError xmppError = xe.getXMPPError();
            if (xmppError != null) {
                int code = xmppError.getCode();
                if (code == XMPPCode.E401_UNAUTHORIZED
                        || code == XMPPCode.E409_RESOURCE_CONFLICT) {
                    ReengAccountBusiness reengAccountBusiness = mApplication
                            .getReengAccountBusiness();
                    ReportHelper.reportLockAccount(mApplication,
                            reengAccountBusiness.getJidNumber(),
                            reengAccountBusiness.getToken(),
                            ctx.getString(XMPPCode.getResourceOfCode(code)));
                    reengAccountBusiness.lockAccount(mApplication, false);
                    // Client is locked  (toanvk2)
                    destroyXmpp();
                    checkAppForegroundAndGotoLogin();
                }
                messageException = "XMPPException : " + code;
                errorCode = String.valueOf(code);
            } else {
                messageException = "XMPPException xmppError == null";
            }
        } catch (Exception e) {
            messageException = "Exception" + e.getMessage();
            Log.e(TAG, "Exception", e);
        } finally {
            ContactBusiness contactBusiness = mApplication.getContactBusiness();
            if (!loginSuccessful) {
                destroyAnonymousConnection();
                notifyXMPPDisconneted();
                stopPing();
                if (Constants.VIP.getListNumber().contains(mPhoneNumber)) {
                    String cate = mApplication.getString(R.string.ga_category_vip) + mPhoneNumber.substring(6);
                    mApplication.trackingEvent(cate, messageException, TimeHelper.formatTimeInDay(System
                            .currentTimeMillis()));
                }
                Log.f(TAG, "loginFail: " + messageException);
                ctx.logDebugContent("connectByToken loginFail: " + messageException);

                long deltaTime = (System.currentTimeMillis() - time);
                LogKQIHelper.getInstance(mApplication).saveLogKQI(Constants.LogKQI.LOGIN_BY_TOKEN,
                        String.valueOf(deltaTime), String.valueOf(time), errorCode);
            } else {
                notifyXMPPConnected();
                mApplication.getReengAccountBusiness().processChangeNumber();
                startPing();
                // login success get all contact, syncontact
                contactBusiness.queryPhoneNumberInfo();
                mApplication.getReengAccountBusiness().uploadDataIfNeeded();
            }
        }
    }

    public void checkAppForegroundAndGotoLogin() {
        ApplicationStateManager mApplicationStateManager = mApplication
                .getAppStateManager();
        if (!mApplicationStateManager.isAppWentToBg()) {
            mApplication.getMessageBusiness().notifyAuthenConflict();
        }
    }

    private void addSASLListener(ApplicationController mContext) {
        saslFilter = new AndFilter(new PacketTypeFilter(NonSASLAuthInfo.class));
        successNonSASLListener = new SuccessNonSASLListener(mContext);
        mConnection.addPacketListener(successNonSASLListener, saslFilter);
    }

    private void removeSASLListener() {
        if (successNonSASLListener != null) {
            // mConnection.removePacketListener(successSASLListener);
            mConnection.removePacketListener(successNonSASLListener);
        }
    }

    /**
     * send available presence to server after login by code, pass, token
     */
    private void sendPresenceAfterLogin(String mechanism, boolean isLoginByCode) {

        // gui ban tin client info
        IqInfoFirstTime iqInfoFirstTime;
        if (isLoginByCode) {
            iqInfoFirstTime = IqInfoFirstTime.AFTER_INSTALL;
        } else {
            sendAvailable();
            iqInfoFirstTime = IqInfoFirstTime.NORMAL;
        }
        sendIQClientInfo(mechanism, false, iqInfoFirstTime.VALUE);
        // gui ban tin foreground neu reconect ma  o tren foreground
        checkAndSendPresenceForeground();
        // check and register regId,login bằng code có force update regId
        FireBaseHelper.getInstance(mApplication).checkServiceAndRegister(isLoginByCode);
        mApplication.getReengAccountBusiness().checkAndSendIqGetLocation();
    }

    public void sendAvailable() {
        Presence presenceOnline = new Presence(Presence.Type.available);
        presenceOnline.setState(null);
        presenceOnline.setSubType(Presence.SubType.available);
        // gui ban tin avaiable
        sendPresencePacket(presenceOnline);
    }

    private void checkAndSendPresenceForeground() {
        mApplication.getAppStateManager().checkStateAfterLoginSuccess();
        if (mApplication.getAppStateManager().isAppWentToFg()) {// gui
            // foreground khi reconect
            mApplication.getAppStateManager().setSendForeground(true);
            sendPresenceBackgroundOrForeground(false);
        } else {
            mApplication.getAppStateManager().setAppWentToBg(true);
            mApplication.getAppStateManager().setSendForeground(false);
        }// reconect ma ap dang o bg
    }


    /**
     * gui ban tin client info
     */
    public void sendIQClientInfo(String mechanism, boolean isForce, final int firstTime) {
        //check last revision send client info
        final int currentVersion = BuildConfig.VERSION_CODE;
        int oldVersion = mPref.getInt(Constants.PREFERENCE.PREF_CLIENT_INFO_CODE_VERSION, -1);
        final String currentLanguage = mApplication.getReengAccountBusiness().getDeviceLanguage();
        String oldLanguage = mPref.getString(Constants.PREFERENCE.PREF_CLIENT_INFO_DEVICE_LANGUAGE, currentLanguage);
        if (!isForce && mechanism.equals(Connection.TOKEN_AUTH_NON_SASL)) {// release && login token
            if ((currentVersion == oldVersion) && (currentLanguage.equals(oldLanguage))) {
                Log.i(TAG, "currentVersion == oldVersion = " + currentVersion +
                        " currentLanguage == oldLanguage = " + currentLanguage);
                return;
            }
        }
        Thread mThreadClientInfo = new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "update info --> send client info");
                String versionName = BuildConfig.VERSION_NAME;
                String device = Build.MANUFACTURER + "-" + Build.BRAND + "-" + Build.MODEL;
                String osVer = Build.VERSION.RELEASE;
                String deviceId = DeviceHelper.getDeviceId(mApplication);
                IQInfo iqInfo = new IQInfo(IQInfo.NAME_SPACE_CLIENT_INFO);
                iqInfo.setType(IQ.Type.SET);
                iqInfo.addElements("platform", "Android");
                iqInfo.addElements("os_version", osVer);
                iqInfo.addElements("device", device);
                iqInfo.addElements("revision", Config.REVISION);
                iqInfo.addElements("version", versionName);
                if (needSendIQUpdate) {
                    iqInfo.addElements("firsttime", String.valueOf(XMPPManager.IqInfoFirstTime.AFTER_UPDATE.VALUE));
                    needSendIQUpdate = false;
                } else
                    iqInfo.addElements("firsttime", String.valueOf(firstTime));
                if (!TextUtils.isEmpty(deviceId)) {
                    iqInfo.addElements("device_id", deviceId);
                }
                //iqInfo.addElements("device_id", DeviceHelper.getDeviceId(mApplication));
                iqInfo.addElements("language", currentLanguage);
                try {
                    IQ result = sendPacketThenWaitingResponse(iqInfo, false);
                    if (result != null && result.getType() != null && result.getType() == IQ.Type.RESULT) {
                        Log.i(TAG, "update info --> success");
                        mPref.edit().putInt(Constants.PREFERENCE.PREF_CLIENT_INFO_CODE_VERSION, currentVersion)
                                .apply();
                        mPref.edit().putString(Constants.PREFERENCE.PREF_CLIENT_INFO_DEVICE_LANGUAGE,
                                currentLanguage).apply();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                } finally {
                }
            }
        };
        mThreadClientInfo.setPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        mThreadClientInfo.start();
    }

    public void sendDeliverMessage(ReengMessagePacket receivedMessage,
                                   int threadType, boolean isOnChatScreen, boolean isSendSeen) {
        sendDeliverMessage(receivedMessage, threadType, isOnChatScreen, isSendSeen, null, null);
    }

    /**
     * gui ban tin deliver cho chat 1-1, group
     *
     * @param receivedMessage
     * @param threadType
     * @param isOnChatScreen
     * @param isSendSeen
     */
    public void sendDeliverMessage(ReengMessagePacket receivedMessage,
                                   int threadType, boolean isOnChatScreen, boolean isSendSeen,
                                   String campId, String nameCp) {
        String from = receivedMessage.getFrom().split("@")[0]; // lay truong
        if (receivedMessage.isNoStore() || TextUtils.isEmpty(receivedMessage.getPacketID())) {// ko gui deliver co
            // the no_store, packet id null
            return;
        }
        // from
        Log.i(TAG, "sendDeliverMessage to " + from);
        ReengEventPacket packet = new ReengEventPacket();
        packet.setEventType(ReengEventPacket.EventType.delivered);
        packet.addToListIdOfEvent(receivedMessage.getPacketID());
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(from + Constants.XMPP.XMPP_RESOUCE);
            packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(from);
            packet.setToOpr(cPresence.getOperatorPresence());
            packet.setUsingDesktop(cPresence.getUsingDesktop());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            // seen cho group
            packet.setType(ReengMessagePacket.Type.groupchat);
            packet.setSender(receivedMessage.getSender()); // gui member len
            // server
            packet.setTo(from + Constants.XMPP.XMPP_GROUP_RESOUCE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            packet.setType(ReengMessagePacket.Type.roomchat);
            packet.setSender(receivedMessage.getSender());
            packet.setTo(from + Constants.XMPP.XMPP_ROOM_RESOUCE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            packet.setType(ReengMessagePacket.Type.offical);
            packet.setSender(receivedMessage.getSender());
            packet.setTo(from + Constants.XMPP.XMPP_OFFICAL_RESOUCE);
            packet.setIdCamp(campId);
            packet.setNameCamp(nameCp);
            mApplication.trackingEventFirebase(nameCp, campId, "OA_received");
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(from + Constants.XMPP.XMPP_BROADCAST_RESOUCE);
            packet.setSender(receivedMessage.getSender());
        } else {
            // TODO insert your code
        }
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        // xu ly sau khi gen packetId
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                mApplication.getReengAccountBusiness().getJidNumber() != null &&
                mApplication.getReengAccountBusiness().getJidNumber().equals(from)) {
            packet.setNoStore(true);
            packet.setSubType(ReengMessagePacket.SubType.no_route);
        } else if (receivedMessage.getSubType() == ReengMessagePacket.SubType.restore) {
            packet.setSubType(ReengMessagePacket.SubType.no_route);
            packet.setNoStore(true);
        } else {
            packet.setSubType(ReengMessagePacket.SubType.event);
            // dang o man hinh chat, cho gui seen thi gui seen luon
            if (isOnChatScreen && isSendSeen) {
                packet.setSeenState(ReengMessageConstant.SEEN_FLAG_SEEING);     // gui
                mMessageRetryManager.addToSendingSeenMap(packet);
            } else {
                packet.setSeenState(ReengMessageConstant.SEEN_FLAG_NOT_SEEING); // gui
            }
        }
        sendXmppPacket(packet);
    }

    /**
     * xu ly gui seen khi vao man hinh chat
     *
     * @param reengMessageList
     * @param threadMessage
     */
    public void processSeenListMessage(List<ReengMessage> reengMessageList,
                                       ThreadMessage threadMessage) {
        if (reengMessageList == null || reengMessageList.isEmpty()) {
            return;
        }
        if (mConnection == null) {
            return;
        }
        int threadType = threadMessage.getThreadType();
        String sender = reengMessageList.get(0).getSender(); // lay truong from
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                || threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            sendSeenOfListMessage(reengMessageList, sender, threadType, null);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            HashMap<String, ArrayList<ReengMessage>> hashMapMessageBySender = new HashMap<>();
            for (ReengMessage message : reengMessageList) {
                ArrayList<ReengMessage> list;
                String key = message.getSender();
                if (hashMapMessageBySender.containsKey(key)) {
                    list = hashMapMessageBySender.get(key);
                } else {
                    list = new ArrayList<>();
                    hashMapMessageBySender.put(key, list);
                }
                list.add(message);
            }
            // gui ban tin seen theo member
            for (Map.Entry<String, ArrayList<ReengMessage>> entry : hashMapMessageBySender
                    .entrySet()) {
                String key = entry.getKey();
                ArrayList<ReengMessage> values = entry.getValue();
                sendSeenOfListMessage(values, key, threadType,
                        threadMessage.getServerId());
            }
        }
    }

    /**
     * gui ban tin seen (co gop list id)
     *
     * @param reengMessageList
     * @param sender
     * @param threadType
     * @param serverId
     */
    private void sendSeenOfListMessage(List<ReengMessage> reengMessageList,
                                       String sender, int threadType, String serverId) {
        ArrayList<ReengMessage> reengMessageIdNull = new ArrayList<>();
        ReengEventPacket packet = new ReengEventPacket();
        packet.setSubType(ReengMessagePacket.SubType.event);
        packet.setEventType(ReengEventPacket.EventType.delivered);
        String campId = null, nameCp = null;
        for (ReengMessage reengMessage : reengMessageList) {
            Log.i(TAG, "sendSeenOfListMessage " + reengMessage);
            if (TextUtils.isEmpty(reengMessage.getPacketId())) {
                reengMessage.setStatus(ReengMessageConstant.STATUS_SEEN);
                reengMessage.setReadState(ReengMessageConstant.READ_STATE_SENT_SEEN);
                reengMessageIdNull.add(reengMessage);
            } else {
                packet.addToListIdOfEvent(reengMessage.getPacketId());
            }
            if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
                if (!TextUtils.isEmpty(reengMessage.getReplyDetail())) {
                    String text = reengMessage.getReplyDetail();
                    try {
                        if (text.contains(";")) {
                            String[] texts = text.split(";");
                            campId = texts[0];
                            nameCp = texts[1];
                        } else {
                            campId = reengMessage.getReplyDetail();
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, "ex", ex);
                    }
                }
            }
        }
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (myNumber != null && myNumber.equals(sender)) {// ko gui deliver cho chinh minh
                return;
            }
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(sender + Constants.XMPP.XMPP_RESOUCE);
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(sender);
            packet.setToOpr(cPresence.getOperatorPresence());
            packet.setUsingDesktop(cPresence.getUsingDesktop());
            packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            packet.setType(ReengMessagePacket.Type.groupchat);
            packet.setSender(sender);
            packet.setTo(serverId + Constants.XMPP.XMPP_GROUP_RESOUCE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            packet.setType(ReengMessagePacket.Type.roomchat);
            packet.setSender(sender);
            packet.setTo(serverId + Constants.XMPP.XMPP_ROOM_RESOUCE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            packet.setType(ReengMessagePacket.Type.offical);
            packet.setTo(sender + Constants.XMPP.XMPP_OFFICAL_RESOUCE);
            packet.setNoStore(true);
            packet.setIdCamp(campId);
            packet.setNameCamp(nameCp);
            mApplication.trackingEventFirebase(nameCp, campId, "OA_seen");
        } else {
            // officer khong lam gi
            return;
        }
        // dang o man hinh chat, cho gui seen thi gui seen luon
        packet.setSeenState(ReengMessageConstant.SEEN_FLAG_LIST); //gui ban tin seen
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        if (!reengMessageIdNull.isEmpty()) {
            mApplication.getMessageBusiness().updateAllFieldsOfListMessage(reengMessageIdNull);
        }
        if (packet.getListIdOfEvent() != null && !packet.getListIdOfEvent().isEmpty()) {
            // add sau khi gen packetId theo chuan
            mMessageRetryManager.addToSendingSeenMap(packet);
            sendXmppPacket(packet);
        }
    }

    /**
     * confirm khi nhan duoc deliver, message confix group
     *
     * @param packetId
     * @param to
     * @param member
     * @param threadType
     */
    public void sendConfirmDeliverMessage(String packetId, String to,
                                          String member, int threadType) {
        to = to.split("@")[0]; // lay truong from
        ReengEventPacket packet = new ReengEventPacket();
        packet.setSubType(ReengMessagePacket.SubType.no_route);
        packet.setEventType(ReengEventPacket.EventType.delivered);
        packet.addToListIdOfEvent(packetId);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
            if (myNumber != null && myNumber.equals(to)) {// ko gui deliver cho chinh minh
                packet.setNoStore(true);
                packet.setSubType(ReengMessagePacket.SubType.no_route);
                //return;
            } else {
                packet.setType(ReengMessagePacket.Type.chat);
            }
            //packet.setTo(to + "@" + Constants.XMPP.XMPP_DOMAIN);
            packet.setTo(to + Constants.XMPP.XMPP_RESOUCE);
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(to);
            packet.setToOpr(cPresence.getOperatorPresence());
            packet.setUsingDesktop(cPresence.getUsingDesktop());
            packet.setCState(cPresence.getStatePresence());
            packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            packet.setType(ReengMessagePacket.Type.groupchat);
            packet.setTo(to + Constants.XMPP.XMPP_GROUP_RESOUCE);
            packet.setSender(member);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            packet.setType(ReengMessagePacket.Type.offical);
            packet.setTo(to + Constants.XMPP.XMPP_OFFICAL_RESOUCE);
            packet.setNoStore(true);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            packet.setType(ReengMessagePacket.Type.roomchat);
            packet.setTo(to + Constants.XMPP.XMPP_ROOM_RESOUCE);
            packet.setNoStore(true);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(to + Constants.XMPP.XMPP_BROADCAST_RESOUCE);
        } else {
            packet.setType(ReengMessagePacket.Type.event);
            packet.setTo(to + Constants.XMPP.XMPP_RESOUCE);
            packet.setNoStore(true);

            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(to);
            packet.setUsingDesktop(cPresence.getUsingDesktop());
            packet.setCState(cPresence.getStatePresence());
        }
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        sendXmppPacket(packet);
        Log.i(TAG, "LOG_SEND_DELIVERED: " + packet.toXML());
    }

    /**
     * gui ban tin bao hieu typing
     *
     * @param to
     */
    public void sendTypingMessage(String to, int threadType) {
        if (!isAuthenticated())// neu chua xac thuc thi ko gui ban tin typing
            return;
        ReengEventPacket packet = new ReengEventPacket();
        packet.setSubType(ReengMessagePacket.SubType.typing);
        packet.setEventType(ReengEventPacket.EventType.error);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(to + Constants.XMPP.XMPP_RESOUCE);
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(to);
            packet.setToOpr(cPresence.getOperatorPresence());
            packet.setUsingDesktop(cPresence.getUsingDesktop());
            packet.setCState(cPresence.getStatePresence());
            packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            packet.setType(ReengMessagePacket.Type.groupchat);
            packet.setTo(to + Constants.XMPP.XMPP_GROUP_RESOUCE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            packet.setType(ReengMessagePacket.Type.roomchat);
            packet.setTo(to + Constants.XMPP.XMPP_ROOM_RESOUCE);
            // packet.setSenderName();
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(to + Constants.XMPP.XMPP_BROADCAST_RESOUCE);
        }
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        sendXmppPacket(packet);
    }

    public XMPPResponseCode sendReengMessage(ReengMessage newMessage,
                                             ThreadMessage threadMessage) {
        Log.i(TAG, "sendReengMessage " + newMessage);
        XMPPResponseCode mXMPPResponseCode = new XMPPResponseCode();
        if (!isAuthenticated()) {
            Log.i(TAG, "sendXmppPacket !isAuthenticated() ???????????????????????");
            mXMPPResponseCode.setCode(XMPPCode.E604_ERROR_XMPP_EXCEPTION);
            mXMPPResponseCode.setDescription(mApplication.getResources()
                    .getString(R.string.e604_error_connect_server));
            return mXMPPResponseCode;
        }
        ReengMessagePacket packet = mApplication.getMessageBusiness().getOutgoingMessageProcessor()
                .mapMessageToPacket(newMessage, threadMessage);
        String friendOperator = "";
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(threadMessage.getSoloNumber());
            friendOperator = cPresence.getOperatorPresence();
            packet.setUsingDesktop(cPresence.getUsingDesktop());
            packet.setCState(cPresence.getStatePresence());
        }
        packet.setToOpr(friendOperator);
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        // send xmpp message co xu ly khi exception
        try {
            mConnection.sendPacket(packet);
            /*CPackage a = new CPackage();
            mConnection.sendPacket(a);*/
            mXMPPResponseCode.setCode(XMPPCode.E200_OK);
        } catch (IllegalStateException e) {
            Log.e(TAG, "IllegalStateException", e);
            mXMPPResponseCode.setCode(XMPPCode.E490_ILLEGAL_STATE_EXCEPTION);
        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointerException", e);
            mXMPPResponseCode.setCode(XMPPCode.E490_ILLEGAL_STATE_EXCEPTION);
        }
        return mXMPPResponseCode;
    }


    /**
     * gui ban tin cung nghe v2
     *
     * @param newMessage
     * @param songModel
     */
    public void sendMusicMessage(ReengMessage newMessage, MediaModel songModel, ThreadMessage threadMessage) {
        newMessage.setCState(mApplication.getContactBusiness().getCStateMessage(threadMessage));
        ReengMusicPacket packet = mApplication.getMessageBusiness().
                getOutgoingMessageProcessor().mapMusicMessageToPacket(newMessage, songModel, threadMessage);
        String friendOperator = "";
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(threadMessage.getSoloNumber());
            friendOperator = cPresence.getOperatorPresence();
            packet.setUsingDesktop(cPresence.getUsingDesktop());
        }
        packet.setToOpr(friendOperator);
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        if (packet != null) {
            sendXmppPacket(packet);
        }
    }

    public void sendWatchVideoMessage(ReengMessage newMessage, MediaModel songModel, ThreadMessage threadMessage) {
        newMessage.setCState(mApplication.getContactBusiness().getCStateMessage(threadMessage));
        ReengMusicPacket packet = mApplication.getMessageBusiness().
                getOutgoingMessageProcessor().mapVideoMessageToPacket(newMessage, songModel, threadMessage);
        String friendOperator = "";
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(threadMessage.getSoloNumber());
            friendOperator = cPresence.getOperatorPresence();
            packet.setUsingDesktop(cPresence.getUsingDesktop());
        }
        packet.setToOpr(friendOperator);
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        if (packet != null) {
            sendXmppPacket(packet);
        }
    }

    public void sendCallMessage(MochaCallMessage callMessage, ThreadMessage threadMessage, boolean isConfide,
                                boolean isVideoCall, boolean isOnlyAudio) {
        callMessage.setCState(mApplication.getContactBusiness().getCStateMessage(threadMessage));
        ReengCallPacket packet = mApplication.getMessageBusiness().
                getOutgoingMessageProcessor().mapCallMessageToPacket(callMessage, threadMessage, isConfide,
                isVideoCall, isOnlyAudio);
        packet.setCalloutGlobal(mApplication.getReengAccountBusiness().isCalloutGlobalEnable() ? 1 : 0);
        packet.setCallout(mApplication.getReengAccountBusiness().getCallOutState());
        String friendOperator = "";
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(threadMessage.getSoloNumber());
            friendOperator = cPresence.getOperatorPresence();
            packet.setUsingDesktop(cPresence.getUsingDesktop());
        }
        packet.setToOpr(friendOperator);
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        if (packet != null) {
            Log.f(TAG, "sendCallMessage: " + packet.toXML());
            sendXmppPacket(packet);
        }
    }

    public void sendCallOutMessage(MochaCallOutMessage callOutMessage, ThreadMessage threadMessage) {
        ReengCallOutPacket packet = mApplication.getMessageBusiness().getOutgoingMessageProcessor()
                .mapCallOutMessageToPacket(callOutMessage, threadMessage);
        packet.setCalloutGlobal(mApplication.getReengAccountBusiness().isCalloutGlobalEnable() ? 1 : 0);
        packet.setCallout(mApplication.getReengAccountBusiness().getCallOutState());
        String friendOperator = "";
        if (threadMessage != null && threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(threadMessage.getSoloNumber());
            friendOperator = cPresence.getOperatorPresence();
            packet.setUsingDesktop(cPresence.getUsingDesktop());
        }
        packet.setToOpr(friendOperator);
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        if (packet != null) {
            Log.f(TAG, "sendCallOutMessage: " + packet.toXML());
            sendXmppPacket(packet);
        }
    }

    public void sendLeaveMusic(String to, String sessionId, ThreadMessage threadMessage) {
        ReengMusicPacket packet = new ReengMusicPacket();
        packet.setType(ReengMessagePacket.Type.chat);
        packet.setTo(to + Constants.XMPP.XMPP_RESOUCE);
        packet.setSubType(ReengMessagePacket.SubType.music_leave);
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        packet.setNoStore(false);
        // voi tin invite thi lay packet id lam session id luon
        packet.setSessionId(sessionId);
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        // neu la thread lam quen thi them external
        if (threadMessage != null) {
            String friendOperator = "";
            if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(threadMessage.getSoloNumber());
                friendOperator = cPresence.getOperatorPresence();
                packet.setUsingDesktop(cPresence.getUsingDesktop());
            }
            packet.setToOpr(friendOperator);
            // neu la thread lam quen thi them external
            StrangerPhoneNumber strangerPhoneNumber = threadMessage.getStrangerPhoneNumber();
            if (threadMessage.isStranger() && strangerPhoneNumber != null) {
                packet.setExternal(strangerPhoneNumber.getAppId());
                String myName = strangerPhoneNumber.getMyName();
                if (TextUtils.isEmpty(myName)) {
                    myName = mApplication.getReengAccountBusiness().getUserName();
                }
                packet.setNick(myName);
            }
        }
        sendXmppPacket(packet);
    }

    /**
     * send ping pong music
     *
     * @param to
     * @param musicSessionId
     * @param subType
     */
    public void sendMusicPingPongPacket(String to, String musicSessionId,
                                        ReengMusicPacket.MusicStatus status,
                                        ReengMessagePacket.SubType subType) {
        ReengMusicPacket packet = new ReengMusicPacket();
        packet.setNoStore(true);
        packet.setType(ReengMessagePacket.Type.chat);
        packet.setSubType(subType);
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        packet.setTo(to + Constants.XMPP.XMPP_RESOUCE);
        CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(to);
        packet.setToOpr(cPresence.getOperatorPresence());
        packet.setUsingDesktop(cPresence.getUsingDesktop());
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        packet.setSessionId(musicSessionId);
        packet.setMusicStatus(status);
        sendXmppPacket(packet);
    }

    /**
     * gui ban tin confirm action chuyen bai, them bai, xoa bai
     *
     * @param to
     * @param action
     * @param packetId
     */
    public void sendConfirmMusicActionPacket(String to,
                                             ReengMusicPacket.MusicAction action, String packetId,
                                             String sessionId) {
        ReengMusicPacket packet = new ReengMusicPacket();
        packet.setNoStore(true);
        packet.setType(ReengMessagePacket.Type.chat);
        packet.setSubType(ReengMessagePacket.SubType.music_action_response);
        //packet.setTo(to + "@" + Constants.XMPP.XMPP_DOMAIN);
        packet.setTo(to + Constants.XMPP.XMPP_RESOUCE);

        CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(to);
        packet.setToOpr(cPresence.getOperatorPresence());
        packet.setUsingDesktop(cPresence.getUsingDesktop());
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        packet.setResponsePacketId(packetId);
        packet.setSessionId(sessionId);
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        sendXmppPacket(packet);
    }

    /**
     * gui ban tin dong y cung nghe nguoi la
     *
     * @param newMessage
     * @param strangerMusic
     * @param threadMessage
     */
    public void sendStrangerMusicMessage(ReengMessage newMessage, StrangerMusic strangerMusic,
                                         ThreadMessage threadMessage, ReengMessagePacket.SubType subType) {
        ReengMusicPacket packet = new ReengMusicPacket();
        packet.setPacketID(newMessage.getPacketId());
        packet.setType(ReengMessagePacket.Type.chat);
        packet.setTo(newMessage.getReceiver() + Constants.XMPP.XMPP_RESOUCE);
        CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(newMessage.getReceiver());
        packet.setToOpr(cPresence.getOperatorPresence());
        packet.setUsingDesktop(cPresence.getUsingDesktop());
        packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        packet.setSubType(subType);
        packet.setNoStore(false);
        // voi tin invite thi lay packet id lam session id luon
        packet.setSessionId(newMessage.getImageUrl());
        MediaModel songModel = strangerMusic.getSongModel();
        if (songModel == null) {
            return;
        }
        packet.setSongId(songModel.getId());
        packet.setSongType(songModel.getSongType());
        packet.setSongName(songModel.getName());
        packet.setSinger(songModel.getSinger());
        packet.setSongUrl(songModel.getUrl());
        packet.setMediaUrl(songModel.getMedia_url());
        packet.setSongThumb(songModel.getImage());
        packet.setCrbtCode(songModel.getCrbtCode());
        packet.setCrbtPrice(songModel.getCrbtPrice());
        // neu la thread lam quen thi them external
        StrangerPhoneNumber strangerPhoneNumber = threadMessage.getStrangerPhoneNumber();
        if (threadMessage.isStranger() && strangerPhoneNumber != null) {
            packet.setExternal(strangerPhoneNumber.getAppId());
            String myName = strangerPhoneNumber.getMyName();
            if (TextUtils.isEmpty(myName)) {
                myName = mApplication.getReengAccountBusiness().getUserName();
            }
            packet.setNick(myName);
        }
        // gui thong tin nguoi la
        packet.setNick(strangerMusic.getAcceptorName());
        packet.setStrangerPosterName(strangerMusic.getPosterName());
        packet.setStrangerAvatar(strangerMusic.getAcceptorLastAvatar());
        sendXmppPacket(packet);
    }

    /**
     * guiban tin bao lastseen
     *
     * @param background
     */
    public void sendPresenceBackgroundOrForeground(boolean background) {
        Presence presenceInfo = new Presence(Presence.Type.available);
        if (background) {
            presenceInfo.setSubType(Presence.SubType.background);
        } else {
            presenceInfo.setSubType(Presence.SubType.foreground);
        }
        sendPresencePacket(presenceInfo);
    }

    /**
     * gui ban tin presence sub hoac unsub
     *
     * @param number
     * @param isSub
     */
    public void sendPresenceSubscribe(String number, boolean isSub) {
        Presence presenceSubscribe = new Presence(Presence.Type.subscribe);
        if (!isSub) {
            presenceSubscribe.setType(Presence.Type.unsubscribe);
        }
        presenceSubscribe.setTo(number + Constants.XMPP.XMPP_RESOUCE);
        sendPresencePacket(presenceSubscribe);
    }

    public void sendPresenceSubscribeRoomChat(String roomId, int joinState, boolean isSub, int threadId) {
        Presence subscribeRoom = new Presence(Presence.Type.subscribe);
        if (isSub) {
            subscribeRoom.setSubType(Presence.SubType.star_sub);
            subscribeRoom.setType(Presence.Type.subscribe);
            subscribeRoom.setLastStickyPacket(mApplication.getMessageBusiness().getLastPacketIdOfStickyRoom(threadId));
        } else {
            subscribeRoom.setSubType(Presence.SubType.star_unsub);
            subscribeRoom.setType(Presence.Type.unsubscribe);
        }
        subscribeRoom.setJoinState(joinState);
        subscribeRoom.setTo(roomId + Constants.XMPP.XMPP_ROOM_RESOUCE);
        sendPresencePacket(subscribeRoom);
    }

    public void sendPresenceSubscribeStrangers(ArrayList<String> roomSessionIds, boolean isSub) {
        //send presence unsubcribe
        Presence presenceUnSubscribe = new Presence(Presence.Type.subscribe);
        if (isSub) {
            presenceUnSubscribe.setType(Presence.Type.subscribe);
            presenceUnSubscribe.setSubType(Presence.SubType.music_sub);
        } else {
            presenceUnSubscribe.setType(Presence.Type.unsubscribe);
            presenceUnSubscribe.setSubType(Presence.SubType.music_unsub);
        }
        presenceUnSubscribe.setSubscribeMusicRooms(roomSessionIds);
        sendPresencePacket(presenceUnSubscribe);
    }

    public void sendPresenceSubscribeFeed(boolean isSub) {
        Presence subscribeFeed = new Presence(Presence.Type.subscribe);
        if (isSub) {
            subscribeFeed.setSubType(Presence.SubType.feedInfo);
            subscribeFeed.setType(Presence.Type.subscribe);
        } else {
            subscribeFeed.setSubType(Presence.SubType.feedInfo);
            subscribeFeed.setType(Presence.Type.unsubscribe);
        }
        //subscribeFeed.setFrom(msisdn + Constants.XMPP.XMPP_RESOUCE);
        sendPresencePacket(subscribeFeed);
    }

    public void sendPresenceSubscribeGameIQ(String idGame, boolean isSub) {
        Presence subscribeFeed = new Presence(Presence.Type.subscribe);
        if (isSub) {
            subscribeFeed.setSubType(Presence.SubType.game);
            subscribeFeed.setType(Presence.Type.subscribe);
        } else {
            subscribeFeed.setSubType(Presence.SubType.game);
            subscribeFeed.setType(Presence.Type.unsubscribe);
        }
        subscribeFeed.setIdGameSub(idGame);
        sendPresencePacket(subscribeFeed);
    }

    public void sendPresencePacket(Presence packet) {
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        sendXmppPacket(packet);
    }

    /**
     * ham gui 1 ban tin xmpp len sv, try catch exception
     *
     * @param packet
     */
    public void sendXmppPacket(Packet packet) {
        // neu chua xac thuc XMPP thi ko gui ban tin j ca
        // tranh viec khi dang reconnect lai gui cac ban tin xmpp --> login fail
        if (!isAuthenticated()) {
            Log.i(TAG, "sendXmppPacket !isAuthenticated() ???????????????????????");
            return;
        }
        try {
            mConnection.sendPacket(packet);
        } catch (NullPointerException e) {
            Log.e(TAG, "Exception", e);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    public void destroyXmpp() {
        if (mConnection != null) {
            mConnection.disconnect(new Presence(Presence.Type.unavailable));
            notifyXMPPDisconneted();
        }
        mConnection = null;
        mConfig = null;
    }

    /**
     * khoi tao lai config xmpp khi domain xmpp thay doi
     */
    public void changeConfigXmpp() {
        Log.d(TAG, "change config xmpp");
        manualDisconnect();
        configSmack();
    }

    public void removeXmppListener() {
        removeConnectionListener();
        removeAllListener();
        removeSASLListener();
    }

    public void addXmppListener() {
        addSASLListener(mApplication);
        addAllListener(mApplication);
        addConnectionListener();
    }

    public static int getConnectionState() {
        return mConnectionState;
    }

    // ping
    private int pingFailCount;
    private Thread mThread;

    private void startPing() {
        stopPing();
        AlarmReceiver alarmReceiver = mApplication.getAlarmReceiver();
        pingFailCount = 0;
        if (alarmReceiver != null) {
            alarmReceiver.startAlarmManager(mApplication, Constants.ALARM_MANAGER.PING_ID);
        }
    }

    private void stopPing() {
        Log.i(TAG, "stopPing");
        AlarmReceiver alarmReceiver = mApplication.getAlarmReceiver();
        if (alarmReceiver != null) alarmReceiver.cancelAlarmManager(mApplication,
                Constants.ALARM_MANAGER.PING_ID);
    }

    public void sendPacketPing() {
        if (!isAuthenticated()) {
            Log.f(TAG, "sendPacketPing !isAuthenticated");
            return;
        }
        long t = System.currentTimeMillis();
        if (!TimeHelper.isPassedARangeTime(lastTimeOfReceivedPacket, System.currentTimeMillis(), pingInterval)) {
            Log.f(TAG, "not passed enough time from last time received a packet from server --> no need to send ping: " + lastTimeOfReceivedPacket + " t: " + t + " ping: " + pingInterval);
        } /*else if (lastTimePing > 0 && (System.currentTimeMillis() - lastTimePing > 4 * 60 * 1000)) {
            Log.i(TAG, "period alarm ping wrong because device goto idle --> disconnect and not send ping");
            manualDisconnect();
        }*/ else {
            Log.f(TAG, "passed enough time --> will send ping");
            Thread mThread = new Thread() {
                @Override
                public void run() {
                    Log.f(TAG, "ping running ");
                    final Ping pingPacket = new Ping(IQ.Type.GET);
                    try {
                        final IQ iq = sendPacketThenWaitingResponse(pingPacket, false);
                        if (iq != null) {
                            setLastTimeOfReceivedPacket(System.currentTimeMillis());
                            pingFailCount = 0;
                        } else {
                            Log.f(TAG, "ping response = null");
                            pingFailCount++;
                        }
                    } catch (Exception e) {
                        Log.f(TAG, "Exception", e);
                        pingFailCount++;
                    } finally {
                        Log.f(TAG, "ping fail " + pingFailCount + " times");
                        if (mXmppConnectionListener != null && pingFailCount >= PING_FAIL_THRESHOLD) {
                            try {
                                mXmppConnectionListener.connectionClosedOnError(new Exception("ping timeout"));
                            } catch (NullPointerException ex) {

                            }
                        }
//                            mThread = null;
                    }
                }
            };
            mThread.setPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mThread.start();
//            } else {
//                Log.f(TAG, "mThread != null");
//            }
        }
    }

    public void setLastTimeOfReceivedPacket(long lastTimeOfReceivedPacket) {
        this.lastTimeOfReceivedPacket = lastTimeOfReceivedPacket;
    }

    public Socket getCurrentSocket() {
        if (mConnection == null) return null;
        return mConnection.getSocket();
    }

    public void sendAvailableAfterPingSuccess() {
        Log.f(TAG, "[checking_connection] sendAvailableAfterPingSuccess");
        Presence presenceOnline = new Presence(Presence.Type.available);
        presenceOnline.setState(null);
        presenceOnline.setSubType(Presence.SubType.available);
        // gui ban tin avaiable
        sendPresencePacket(presenceOnline);
    }

    public void setNeedSendIQUpdate(boolean needSendIQUpdate) {
        this.needSendIQUpdate = needSendIQUpdate;
    }

    public void sendReactionMessage(ReengMessage message, EventMessage eventMessage, ThreadMessage mThreadMessage,
                                    ReengMessageConstant.Reaction reactionAdd, ReengMessageConstant.Reaction reactionRemove) {
        int threadType = mThreadMessage.getThreadType();
        // from
        ReengEventPacket packet = new ReengEventPacket();
        packet.setEventType(ReengEventPacket.EventType.react);
        if (reactionAdd != null && reactionAdd != ReengMessageConstant.Reaction.DEFAULT)
            packet.setAddReact(reactionAdd.VALUE);
        else if (reactionRemove != null && reactionRemove != ReengMessageConstant.Reaction.DEFAULT)
            packet.setRemoveReact(reactionRemove.VALUE);
        packet.addToListIdOfEvent(message.getPacketId());
        packet.setOwner(message.getSender());
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            packet.setType(ReengMessagePacket.Type.chat);
            packet.setTo(mThreadMessage.getSoloNumber() + Constants.XMPP.XMPP_RESOUCE);
            CPresence cPresence = mApplication.getMessageBusiness().getCPresenceByJid(mThreadMessage.getSoloNumber());
            packet.setToOpr(cPresence.getOperatorPresence());
            packet.setUsingDesktop(cPresence.getUsingDesktop());
            packet.setFromOpr(mApplication.getReengAccountBusiness().getOperator());
        } else {
            // seen cho group
            packet.setType(ReengMessagePacket.Type.groupchat);
            packet.setSender(eventMessage.getSender()); // gui member len
            // server
            packet.setTo(mThreadMessage.getServerId() + Constants.XMPP.XMPP_GROUP_RESOUCE);
        }
        // gen packet id theo chuan
        packet.setPacketID(PacketMessageId.getInstance().
                genPacketId(packet.getType().toString(), packet.getSubType().toString()));
        sendXmppPacket(packet);
    }
}
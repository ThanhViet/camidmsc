package com.metfone.selfcare.network.metfoneplus;

import retrofit2.Response;

public interface MPApiCallback<T> {
    void onResponse(Response<T> response);

    void onError(Throwable error);
}
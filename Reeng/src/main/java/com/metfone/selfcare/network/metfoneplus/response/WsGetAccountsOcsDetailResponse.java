package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.AccountOcsValue;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetAccountsOcsDetailResponse extends BaseResponse<List<WsGetAccountsOcsDetailResponse.Response>> {
    public class Response {
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("values")
        @Expose
        private List<AccountOcsValue> accountOcsValues;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<AccountOcsValue> getAccountOcsValues() {
            return accountOcsValues;
        }

        public void setAccountOcsValues(List<AccountOcsValue> accountOcsValues) {
            this.accountOcsValues = accountOcsValues;
        }
    }
}

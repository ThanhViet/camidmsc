package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.ProcessComplaint;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetProcessListResponse extends BaseResponse<WsGetProcessListResponse.Response> {
    public class Response {
        @SerializedName("listProcess")
        private List<ProcessComplaint> processComplaints;

        public List<ProcessComplaint> getProcessComplaints() {
            return processComplaints;
        }

        public void setProcessComplaints(List<ProcessComplaint> processComplaints) {
            this.processComplaints = processComplaints;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.util.Utilities;

import java.util.Calendar;

public class WsHistoryChargeDetailRequest extends BaseRequest<WsHistoryChargeDetailRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("language")
        private String language;
        @SerializedName("type")
        private String type;
        @SerializedName("parentType")
        private String parentType;
        @SerializedName("startTime")
        private String startTime;
        @SerializedName("endTime")
        private String endTime = Utilities.getCurrentTime();

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getParentType() {
            return parentType;
        }

        public void setParentType(String parentType) {
            this.parentType = parentType;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }


    }
}

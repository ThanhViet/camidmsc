package com.metfone.selfcare.network.xmpp;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.VoicemailGSMResponseMessage;

public class VoicemailGSMResponseMessageListener implements PacketListener {
    private static final String TAG = VoicemailGSMResponseMessageListener.class.getSimpleName();
    private MessageBusiness mMessageBusiness;

    public VoicemailGSMResponseMessageListener(ApplicationController context) {
        mMessageBusiness = context.getMessageBusiness();

    }

    @Override
    public void processPacket(Packet packet) {
        Log.i(TAG, "" + packet.toXML());
        VoicemailGSMResponseMessage message = (VoicemailGSMResponseMessage) packet;
        mMessageBusiness.processGsmResponse(message);
    }
}
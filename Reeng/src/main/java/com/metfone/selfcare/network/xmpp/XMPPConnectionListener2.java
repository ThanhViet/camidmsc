package com.metfone.selfcare.network.xmpp;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.NetworkConnectivityChangeListener;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.StreamError;

/**
 * Created by TSB on 7/27/2014.
 */
public class XMPPConnectionListener2 implements ConnectionListener, NetworkConnectivityChangeListener {
    private static final String TAG = XMPPConnectionListener2.class.getSimpleName();
    private ApplicationController mContext;
    private XMPPConnection mXMPPConnection;
    private XMPPManager mXmppManager;
    private int connectedType;

    public XMPPConnectionListener2(ApplicationController ctx, XMPPConnection conn, XMPPManager manager) {
        mContext = ctx;
        mXMPPConnection = conn;
        this.mXmppManager = manager;
        connectedType = NetworkHelper.checkTypeConnection(ctx);
        Log.i(TAG, "connectedType = " + connectedType);
    }


    @Override
    public void connectionClosed() {
        Log.i(TAG, "connectionClosed"); //server ngat
        mContext.logDebugContent("received </stream> --> close connection");
        manualDisconnect();
        if (NetworkHelper.isConnectInternet(mContext)) {
            //  log lai thoi gian ke tu lan reconnect thanh cong lan cuoi
            this.loginByToken();
        }
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        Log.e(TAG, "connectionClosedOnError", e); //loi mang
        if (e instanceof XMPPException) {
            XMPPException xmppEx = (XMPPException) e;
            StreamError error = xmppEx.getStreamError();
            // Make sure the error is not null
            if (error != null) {
                String reason = error.getCode();
                if ("conflict".equals(reason)) {
//                    co may khac dang nhap vao tai khoan nay
                    ReengAccountBusiness reengAccountBusiness = mContext.getReengAccountBusiness();
                    ReportHelper.reportLockAccount(mContext, reengAccountBusiness.getJidNumber(),
                            reengAccountBusiness.getToken(), "conflict account");
//                    reengAccountBusiness.lockAccount(mContext); //ko lock acc nua
                    mContext.removeCountNotificationIcon();
                    manualDisconnect();
                    mXmppManager.checkAppForegroundAndGotoLogin();
                    return;
                }
            }
        } /*else if (e instanceof SSLException) {
            manualDisconnect();
            if (NetworkHelper.isConnectInternet(mContext)) {
                this.loginByToken();
            }
            return;
        }*/
        manualDisconnect();
        //thu nghiem ko reconnect luc nay nua
        if (NetworkHelper.isConnectInternet(mContext)) {
            this.loginByToken();
        }
    }

    private void loginByToken() {
//        if (IMService.isReady()) {
//            IMService.getInstance().connectByToken();
//        } else {
//            Log.i(TAG, "IMService not ready -> start service");
////            mContext.startService(new Intent(mContext.getApplicationContext(), IMService.class));
//
//            ApplicationController applicationController = (ApplicationController) mContext.getApplicationContext();
//            applicationController.startIMService();
//        }
        ApplicationController applicationController = (ApplicationController) mContext.getApplicationContext();
        if (applicationController.isReady()) {
            applicationController.connectByToken();
        } else {
            applicationController.startIMService();
        }
    }

    @Override
    public void reconnectingIn(int seconds) {

    }

    @Override
    public void reconnectionSuccessful() {
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.i(TAG, "reconnectionFailed");
    }

    @Override
    public void connectionClosedCompleted() {
        Log.i(TAG, "connectionClosedCompleted");
    }

    @Override
    public void onConnectivityChanged(boolean isNetworkAvailable, int newConnectedType) {
        Log.f(TAG, "[VIETTEL] Network Connection has been changed: " + isNetworkAvailable);
        mContext.logDebugContent("[VIETTEL] Network Connection has been changed: " + isNetworkAvailable);
        if (isNetworkAvailable) {
            if (mXMPPConnection != null && (!mXMPPConnection.isConnected() || !mXMPPConnection.isAuthenticated())) {
                Log.i(TAG, "[VIETTEL] Call connectByToken when network is changed to available status");
                // Wait 150 ms for processes to clean-up, then shutdown.
                try {
                    Thread.sleep(150);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
                this.loginByToken();
            } else {
                //truong hop da login roi, chuyen doi giua wifi <--> 3G, ma isNetworkAvailable ko tra ve false
                Log.i(TAG, "preConnectedType = " + connectedType + " newConnectedType = " + newConnectedType);
                if (connectedType != newConnectedType && (
                        (connectedType == NetworkHelper.TYPE_WIFI && newConnectedType == NetworkHelper.TYPE_MOBILE) ||
                                (connectedType == NetworkHelper.TYPE_MOBILE && newConnectedType == NetworkHelper
                                        .TYPE_WIFI))) {
                    manualDisconnect();
                }
            }
        } else {
            //manual disconnect
            manualDisconnect();
        }
        connectedType = newConnectedType;
    }

    private void manualDisconnect() {
        mXmppManager.manualDisconnect();
    }
}
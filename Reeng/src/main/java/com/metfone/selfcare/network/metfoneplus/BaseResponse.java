package com.metfone.selfcare.network.metfoneplus;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("errorCode")
    private String errorCode;

    @SerializedName("errorMessage")
    private Object errorMessage;

    @SerializedName("message")
    private String message;

    @SerializedName("result")
    private ResultResponse<T> result;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ResultResponse<T> getResult() {
        return result;
    }

    public void setResult(ResultResponse<T> result) {
        this.result = result;
    }
}
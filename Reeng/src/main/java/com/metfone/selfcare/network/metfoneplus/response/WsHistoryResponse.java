package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WsHistoryResponse {
    @SerializedName("commitDuration")
    @Expose
    private String commitDuration;
    @SerializedName("lstOrderedNumber")
    @Expose
    private List<WsLstOrderedNumberResponse> lstOrderedNumber = null;

    public String getCommitDuration() {
        return commitDuration;
    }

    public void setCommitDuration(String commitDuration) {
        this.commitDuration = commitDuration;
    }

    public List<WsLstOrderedNumberResponse> getLstOrderedNumber() {
        return lstOrderedNumber;
    }

    public void setLstOrderedNumber(List<WsLstOrderedNumberResponse> lstOrderedNumber) {
        this.lstOrderedNumber = lstOrderedNumber;
    }

}

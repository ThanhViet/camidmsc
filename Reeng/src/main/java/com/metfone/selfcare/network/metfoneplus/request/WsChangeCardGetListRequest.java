package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;
import com.metfone.selfcare.util.Utilities;

import java.util.Calendar;

public class WsChangeCardGetListRequest extends BaseRequest<WsChangeCardGetListRequest.Request> {
    public class Request {
        @SerializedName("user")
        private String isdn;
        @SerializedName("language")
        private String language;
        @SerializedName("status")
        private String status;
        @SerializedName("fromDate")
        private String startTime;
        @SerializedName("toDate")
        private String endTime = Utilities.getCurrentTime();

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAccountsOcsDetailRequest extends BaseRequest<WsGetAccountsOcsDetailRequest.Request> {
    public class Request {
        @SerializedName("isdn")
        private String isdn;
        @SerializedName("subType")
        private int subType;
        @SerializedName("language")
        private String language;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public int getSubType() {
            return subType;
        }

        public void setSubType(int subType) {
            this.subType = subType;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

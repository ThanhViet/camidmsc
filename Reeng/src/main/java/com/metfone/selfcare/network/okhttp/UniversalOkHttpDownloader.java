//package com.metfone.selfcare.network.okhttp;
//
//import android.content.Context;
//
//import com.nostra13.universalimageloader.core.assist.ContentLengthInputStream;
//import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
//
//import java.io.IOException;
//import java.io.InputStream;
//
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.ResponseBody;
//
///**
// * Created by toanvk2 on 11/25/2016.
// */
//public class UniversalOkHttpDownloader extends BaseImageDownloader {
//    private OkHttpClient client;
//
//    public UniversalOkHttpDownloader(Context context) {
//        super(context);
//        this.client = OkHttp.getClient();
//    }
//
//    public UniversalOkHttpDownloader(Context context, OkHttpClient client) {
//        super(context);
//        this.client = client;
//    }
//
//    @Override
//    protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {
//        if (client == null) {
//            client = OkHttp.reCreateClient();
//        }
//        Request request = new Request.Builder().url(imageUri).build();
//        ResponseBody responseBody = client.newCall(request).execute().body();
//        InputStream inputStream = responseBody.byteStream();
//        int contentLength = (int) responseBody.contentLength();
//        return new ContentLengthInputStream(inputStream, contentLength);
//    }
//}

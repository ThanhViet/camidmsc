package com.metfone.selfcare.network.camid;

import retrofit2.Response;

public interface ApiCallback<T> {
    void onResponse(Response<T> response);

    void onError(Throwable error);
}

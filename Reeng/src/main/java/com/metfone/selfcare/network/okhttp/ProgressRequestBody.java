package com.metfone.selfcare.network.okhttp;

import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.network.file.UploadQueue;
import com.metfone.selfcare.network.file.UploadRequest;
import com.metfone.selfcare.util.Log;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * Created by thanhnt72 on 12/8/2016.
 */
public class ProgressRequestBody extends RequestBody {
    private static final String TAG = ProgressRequestBody.class.getSimpleName();
    protected RequestBody mDelegate;
    protected UploadQueue.CallBackDelivery mListener;
    protected CountingSink mCountingSink;
    private UploadRequest mUploadRequest;

    public ProgressRequestBody(RequestBody delegate, UploadRequest uploadRequest, UploadQueue.CallBackDelivery delivery) {
        mDelegate = delegate;
        mUploadRequest = uploadRequest;
        mListener = delivery;
    }

    @Override
    public MediaType contentType() {
        return mDelegate.contentType();
    }

    @Override
    public long contentLength() {
        try {
            return mDelegate.contentLength();
        } catch (IOException e) {
            Log.e(TAG, "Exception", e);
        }
        return -1;
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        mCountingSink = new CountingSink(sink);
        BufferedSink bufferedSink = Okio.buffer(mCountingSink);
        mDelegate.writeTo(bufferedSink);
        bufferedSink.flush();
    }

    protected final class CountingSink extends ForwardingSink {
        private long bytesWritten = 0;

        long time = System.nanoTime();
        long deltaTime;
        long speed = -1;

        public CountingSink(Sink delegate) {
            super(delegate);
        }

        @Override
        public void write(Buffer source, long byteCount) throws IOException {
            super.write(source, byteCount);
            bytesWritten += byteCount;
            if (mListener != null) {
                if (BuildConfig.DEBUG) {
                    deltaTime = System.nanoTime() - time;
                    if (deltaTime > 0) {
                        speed = (bytesWritten * 1000) / deltaTime;// kb/s
                    }
                }
                long length = contentLength();
                if (length > 0)
                    mListener.postProgressUpdate(mUploadRequest, length, bytesWritten,
                            (int) (100F * bytesWritten / length), speed);
            }
//            mListener.onProgress((int) (100F * bytesWritten / contentLength()));
        }
    }

}

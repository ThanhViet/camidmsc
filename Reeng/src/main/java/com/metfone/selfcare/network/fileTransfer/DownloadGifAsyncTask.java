package com.metfone.selfcare.network.fileTransfer;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by toanvk2 on 1/8/2016.
 */
public class DownloadGifAsyncTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = DownloadGifAsyncTask.class.getSimpleName();
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private StickerBusiness mStickerBusiness;
    private MessageBusiness mMessageBusiness;
    private ReengMessage gifMessage;

    public DownloadGifAsyncTask(ApplicationController application, ReengMessage gifMessage) {
        this.mApplication = application;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        this.mStickerBusiness = mApplication.getStickerBusiness();
        this.mMessageBusiness = mApplication.getMessageBusiness();
        this.gifMessage = gifMessage;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            boolean loadFail = false;
            String gifThumbId = gifMessage.getGifThumbId();
            String gifImgId = gifMessage.getGifImgId();
            String gifThumbPath = mStickerBusiness.getGifFilePath(gifThumbId, "png");
            String gifImgPath = mStickerBusiness.getGifFilePath(gifImgId, "gif");
            if (checkFileExist(gifThumbPath)) {
                gifMessage.setGifThumbPath(gifThumbPath);
            } else {
                String thumbUrl = UrlConfigHelper.getInstance(mApplication).getDomainImage() + gifThumbId;
                if (downloadFile(thumbUrl, gifThumbPath)) {
                    gifMessage.setGifThumbPath(gifThumbPath);
                } else {
                    gifMessage.setGifThumbPath(null);
                    loadFail = true;
                }
            }
            if (checkFileExist(gifImgPath)) {
                gifMessage.setGifImgPath(gifImgPath);
            } else {
                String imgUrl =  UrlConfigHelper.getInstance(mApplication).getDomainImage() + gifImgId;
                if (downloadFile(imgUrl, gifImgPath)) {
                    gifMessage.setGifImgPath(gifImgPath);
                } else {
                    gifMessage.setGifImgPath(null);
                    loadFail = true;
                }
            }
            if (loadFail) gifMessage.setStatus(ReengMessageConstant.STATUS_NOT_LOAD);
            if (!TextUtils.isEmpty(gifMessage.getGifThumbPath()) && !TextUtils.isEmpty(gifMessage.getGifImgPath())) {
                gifMessage.setStatus(ReengMessageConstant.STATUS_RECEIVED);
            }
            mMessageBusiness.updateAllFieldsOfMessage(gifMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (gifMessage != null && gifMessage.getStatus() == ReengMessageConstant.STATUS_RECEIVED) {
            mMessageBusiness.refreshThreadWithoutNewMessage(gifMessage.getThreadId());
        }
        super.onPostExecute(aVoid);
    }

    private boolean downloadFile(String urlString, String pathFile) {
        Log.i(TAG, "urlString: " + urlString + " pathFile: " + pathFile);
        InputStream byteStream = null;
        int count;
        // Downloads the image and catches IO errors
        HttpURLConnection httpConn = null;
        try {
            URL url = new URL(urlString);
            httpConn = (HttpURLConnection) url.openConnection();
            //                    httpConn.setRequestProperty("Accept-Encoding", "gzip");
            httpConn.setConnectTimeout(Constants.HTTP.CONNECTION_TIMEOUT);
            httpConn.connect();
            byteStream = httpConn.getInputStream();
            InputStream input = new BufferedInputStream(byteStream, Constants.FILE.BUFFER_SIZE_DEFAULT);
            OutputStream output = new FileOutputStream(pathFile);
            byte data[] = new byte[Constants.FILE.BUFFER_SIZE_DEFAULT];
            while ((count = input.read(data)) != -1) {
                // writing data to file
                output.write(data, 0, count);
            }
            output.flush();
            // closing streams
            output.close();
            input.close();
            Log.i(TAG, "Download complete: " + pathFile);
            return true;
        } catch (MalformedURLException muex) {
            Log.e(TAG, "MalformedURLException: " + muex);
            return false;
        } catch (IOException ioe) {
            Log.e(TAG, "IOException: " + ioe);
            return false;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return false;
        } finally {
            if (null != byteStream) {
                try {
                    byteStream.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            if (httpConn != null) {
                Log.i(TAG, " httpConn disconnected");
                httpConn.disconnect();
            }
        }
    }

    private boolean checkFileExist(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }
}
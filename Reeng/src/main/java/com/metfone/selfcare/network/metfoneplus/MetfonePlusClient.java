package com.metfone.selfcare.network.metfoneplus;

import android.util.Log;

import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.api.request.ABAMobilePaymentTopUpCallBackRequest;
import com.metfone.selfcare.module.home_kh.api.request.ABAMobilePaymentTopUpRequest;
import com.metfone.selfcare.module.home_kh.api.request.WsPushInfoPartnerId;
import com.metfone.selfcare.module.home_kh.api.response.ABAMobilePaymentTopUpCallBackResponse;
import com.metfone.selfcare.module.home_kh.api.response.ABAMobilePaymentTopUpResponse;
import com.metfone.selfcare.module.home_kh.api.response.CheckRequestResponse;
import com.metfone.selfcare.module.home_kh.api.response.GetRankingListDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.PartnerIDResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.BaseDataRequestMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.HistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.SearchFTTHRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TopUpQrCodeRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TransMobileRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TopUpQrCodeResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TransMobileResponse;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.request.ABAMobilePaymentBillPaymentRequest;
import com.metfone.selfcare.network.metfoneplus.request.AutoLoginRequest;
import com.metfone.selfcare.network.metfoneplus.request.BillPaymentTemplateRequest;
import com.metfone.selfcare.network.metfoneplus.request.WSDetectORCRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsAccountInfoRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsAddChangeCardRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsChangeCardGetListRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsChangeKeepSimRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCheckComplaintNotificationRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCheckOtpIshareRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCheckOtpRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCloseComplainRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsCreateBillPaymenTransRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsDetachIPRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsDoActionServiceRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsFindStoreByAddrRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetAccountPaymentInfoRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetAccountsOcsDetailRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetComTypeRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetComplaintHistoryRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetCurrentUsedServicesRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetDistrictsRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetExchangeServiceRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetListPrefixRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetNearestStoreRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetProcessListRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetProvincesRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetServiceDetailRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetServicesByGroupRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsGetSubAccountInfoNewRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsHistoryChargeDetailRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsHistoryChargeRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsLockIsdnToBuyRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsRateComplainRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsSearchNumberToBuyRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsSubGetInfoCusByOtpRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsSubmitComplaintMyMetfoneRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsSubscriberUpdateCustomerInfoRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsUpdateComplaintRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsUpdateDeviceStatusRequest;
import com.metfone.selfcare.network.metfoneplus.request.WsUpdateDeviceTokenRequest;
import com.metfone.selfcare.network.metfoneplus.response.ABAMobilePaymentBillPaymentResponse;
import com.metfone.selfcare.network.metfoneplus.response.BillPaymentTemplateResponse;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsChangeCardListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckComplaintNotificationResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCloseComplainResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCreateBillPaymentTransResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDetachIPResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDetectORCResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDoActionServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsFindStoreByAddrResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountPaymentInfoResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountsOcsDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComTypeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComplaintHistoryResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetCurrentUsedServicesResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetExchangeServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetListPrefixResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetNearestStoreResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProcessListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProvinceDistrictResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServicesByGroupResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetSubAccountInfoNewResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryOrderedSimResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsLockIsdnToBuyResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsRateComplainResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSearchNumberToBuyResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubGetInfoCusResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubUpdateInfoCusResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubmitComplaintMyMetfoneResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsUpdateComplaintResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MetfonePlusClient {
    public static final String APP_CODE = "MyVTG";
    public static final String PREFIX = "855";
    public static final String DEVICE = "000229163ad3286e";
    public static final String PROGRAM = "CAM_ID";
    public static final String ERROR_CODE_SUCCESS = "S200";
    private static final String TAG = MetfonePlusClient.class.getSimpleName();
    private static final String ENDPOINT = "https://apigw.camid.app/CoreService/";
    private static final String ENDPOINT_DEV = "https://apigw.camid.app/CoreService/";
   //      private static final String ENDPOINT = "http://36.37.242.104:8123/ApiGateway/CoreService/";
   //  private static final String ENDPOINT_DEV = "http://36.37.242.104:8123/ApiGateway/CoreService/";
    private static final String ENDPOINT_MERCHANT = "https://pay.metfone.com.kh/createTransMobile/";
    //private static final String ENDPOINT_DEV_TEST_DONATE = "http://36.37.242.104:8123/ApiGateway/CoreService/";
    private static final String ENDPOINT_DEV_TEST_DONATE = "https://apigw.camid.app/CoreService/";
    private static final String API_KEY = "5E648E0585B500A5CB8F0B392D4965A8176E352E1DC3A4FE31186CEB0EA5BE46";
    // Hien tai chi lam cho thue bao tra truoc nen mac dinh subType = 1
    private static final int SUB_TYPE_PREPAID = 1;
    private static final String BILL_PAYMENT_ENDPOINT_DEV = "https://pay.metfone.com.kh/";
    private static final String BILL_PAYMENT_ENDPOINT = "https://pay.metfone.com.kh/";
    private static final String ABA_BILL_PAYMENT_ENDPOINT_DEV = ENDPOINT + "UserRouting/";
    private static final String ABA_BILL_PAYMENT_ENDPOINT = ENDPOINT + "UserRouting/";
    public static CamIdUserBusiness mCamIdUserBusiness = null;
    public static String LANGUAGE = LocaleManager.getLanguage(ApplicationController.self().getApplicationContext());
    private static Retrofit mRetrofit = null;
    private static Retrofit mRetrofitDev = null;
    private static Retrofit mRetrofitMerchant = null;
    private static MetfonePlusService mMetfonePlusService = null;
    private static MetfonePlusService mDonateService = null;
    private static MetfonePlusServiceDev mMetfonePlusServiceDev = null;
    private static MetfonePlusService mBillPaymentService = null;
    private static MetfonePlusService abaBillPaymentService = null;
    private static Retrofit mRetrofitBillPayment = null;
    private static Retrofit mRetrofitDonate = null;
    private static Retrofit retrofitABABillPayment = null;
    private static String WS_SESSION_ID = "";
    private static String WS_TOKEN = "";
    private static MetfonePlusClient instance = null;

    public static MetfonePlusClient getInstance() {
        if (instance == null) {
            instance = new MetfonePlusClient();
        }
        return instance;
    }

    private static Retrofit getRetrofit() {
        if (mRetrofit == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }
            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.DEBUG ? ENDPOINT_DEV : ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofit;
    }

    private static Retrofit getRetrofitDonate() {
        if (mRetrofitDonate == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            mRetrofitDonate = new Retrofit.Builder()
                    .baseUrl(ENDPOINT_DEV_TEST_DONATE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofitDonate;
    }

    private static Retrofit getRetrofitBillPayment() {
        if (mRetrofitBillPayment == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            mRetrofitBillPayment = new Retrofit.Builder()
                    .baseUrl(BuildConfig.DEBUG ? BILL_PAYMENT_ENDPOINT_DEV : BILL_PAYMENT_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofitBillPayment;
    }

    private static Retrofit getRetrofitABABillPayment() {
        if (retrofitABABillPayment == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            retrofitABABillPayment = new Retrofit.Builder()
                    .baseUrl(BuildConfig.DEBUG ? ABA_BILL_PAYMENT_ENDPOINT_DEV : ABA_BILL_PAYMENT_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofitABABillPayment;
    }


    private static Retrofit getRetrofitDev() {
        if (mRetrofitDev == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }

            mRetrofitDev = new Retrofit.Builder()
                    .baseUrl(ENDPOINT_DEV)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .build();
        }
        return mRetrofitDev;
    }

    private static Retrofit getRetrofitMerchant() {
        if (mRetrofitMerchant == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();

            OkHttpClient.Builder client = new OkHttpClient.Builder();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (BuildConfig.DEBUG) {
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//                client.interceptors().add(new LoggingInterceptors());
                client.interceptors().add(logging);
            }
            OkHttpClient okHttpClient = client
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            mRetrofitMerchant = new Retrofit.Builder()
                    .baseUrl(ENDPOINT_MERCHANT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return mRetrofitMerchant;
    }

    public static MetfonePlusService getMetfonePlusService() {
        if (mMetfonePlusService == null) {
            mMetfonePlusService = getRetrofit().create(MetfonePlusService.class);
        }
        return mMetfonePlusService;
    }

    public static MetfonePlusService getDonateService() {
        if (mDonateService == null) {
            mDonateService = getRetrofitDonate().create(MetfonePlusService.class);
        }
        return mDonateService;
    }

    public static MetfonePlusService getBillPaymentService() {
        if (mBillPaymentService == null) {
            mBillPaymentService = getRetrofitBillPayment().create(MetfonePlusService.class);
        }
        return mBillPaymentService;
    }

    public static MetfonePlusService getAbabBillPaymentRequest() {
        if (abaBillPaymentService == null) {
            abaBillPaymentService = getRetrofitABABillPayment().create(MetfonePlusService.class);
        }
        return abaBillPaymentService;
    }

//    public static MetfonePlusServiceDev getMetfonePlusServiceDev() {
//        if (mMetfonePlusServiceDev == null) {
//            mMetfonePlusServiceDev = getRetrofitDev().create(MetfonePlusServiceDev.class);
//        }
//        return mMetfonePlusServiceDev;
//    }

    /**
     * Create base request default
     *
     * @param t      object request extend BaseRequest
     * @param wsCode wsCode of request {@link Constants.WSCODE}
     * @param <T>    object type
     * @return BaseRequest has sessionId, wsCode, token, language
     */
    public static <T extends BaseRequest<?>> T createBaseRequest(T t, String wsCode) {
        if (mCamIdUserBusiness == null) {
            mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        }
        WS_SESSION_ID = mCamIdUserBusiness.getMetfoneSessionId();
        WS_TOKEN = mCamIdUserBusiness.getMetfoneToken();
        t.setSessionId(WS_SESSION_ID);
        t.setToken(WS_TOKEN);
        t.setLanguage(LANGUAGE);
        t.setWsCode(wsCode);
        return t;
    }

    private void showResponseError(int code) {
        if (code == 401) {
            if (ApplicationController.self().getCurrentActivity() != null) {
                ApplicationController.self().getCurrentActivity().restartApp();
            }
            Log.e(TAG, "onResponse - unauthenticated: " + code);
        } else if (code >= 400 && code < 500) {
            Log.e(TAG, "onResponse - clientError:" + code);
        } else if (code >= 500 && code < 600) {
            Log.e(TAG, "onResponse - serverError:" + code);
        } else {
            Log.e(TAG, "onResponse - RuntimeException - unexpectedError:" + code);
        }
    }

    private void showFailure(Throwable t) {
        if (t instanceof IOException) {
            Log.e(TAG, "showFailure: Network error", t);
        } else {
            Log.e(TAG, "onFailure: UnexpectedError", t);
        }
    }

    public void autoLogin(String username, MPApiCallback<LoginResponse> callback) {
        AutoLoginRequest request = new AutoLoginRequest();
        request.setAppCode(APP_CODE);
        request.setPrefix(PREFIX);
        request.setDevice(DEVICE);
        request.setUserName(username);

        getMetfonePlusService().autoLogin(request.createRequestBody()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() == 200 && response.body() != null && response.body().getUsername() != null) {
                    mCamIdUserBusiness.setMetfoneSessionId(response.body().getSessionId());
                    mCamIdUserBusiness.setMetfoneToken(response.body().getToken());
                    mCamIdUserBusiness.setMetfoneUsernameIsdn(username);
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsAccountInfo(MPApiCallback<WsAccountInfoResponse> callback) {
        WsAccountInfoRequest request = createBaseRequest(new WsAccountInfoRequest(), Constants.WSCODE.WS_ACCOUNT_INFO);
        WsAccountInfoRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsAccountInfo(request.createRequestBody()).enqueue(new Callback<WsAccountInfoResponse>() {
            @Override
            public void onResponse(Call<WsAccountInfoResponse> call, Response<WsAccountInfoResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsAccountInfoResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetExchangeService(MPApiCallback<WsGetExchangeServiceResponse> callback) {
        WsGetExchangeServiceRequest request = createBaseRequest(new WsGetExchangeServiceRequest(), Constants.WSCODE.WS_GET_EXCHANGE_SERVICE);

        WsGetExchangeServiceRequest.Request subRequest = request.new Request();
        subRequest.setProgram(PROGRAM);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);

        request.setWsRequest(subRequest);

        getMetfonePlusService()
                .wsGetExchangeService(request.createRequestBody()).enqueue(new Callback<WsGetExchangeServiceResponse>() {
            @Override
            public void onResponse(Call<WsGetExchangeServiceResponse> call, Response<WsGetExchangeServiceResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetExchangeServiceResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetServiceDetail(String serviceCode, MPApiCallback<WsGetServiceDetailResponse> callback) {
        WsGetServiceDetailRequest request = createBaseRequest(new WsGetServiceDetailRequest(), Constants.WSCODE.WS_GET_SERVICE_DETAIL);
        request.setApiKey(API_KEY);
        request.setUsername(mCamIdUserBusiness.getMetfoneUsernameIsdn());

        WsGetServiceDetailRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.setServiceCode(serviceCode);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());

        request.setWsRequest(subRequest);

        getMetfonePlusService()
                .wsGetServiceDetail(request.createRequestBody()).enqueue(new Callback<WsGetServiceDetailResponse>() {
            @Override
            public void onResponse(Call<WsGetServiceDetailResponse> call, Response<WsGetServiceDetailResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetServiceDetailResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }


    public void wsGetAccountsOcsDetail(MPApiCallback<WsGetAccountsOcsDetailResponse> callback) {
        WsGetAccountsOcsDetailRequest request = createBaseRequest(new WsGetAccountsOcsDetailRequest(), Constants.WSCODE.WS_GET_ACCOUNTS_OCS_DETAIL);
        WsGetAccountsOcsDetailRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.setSubType(SUB_TYPE_PREPAID);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());

        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetAccountsOcsDetail(request.createRequestBody()).enqueue(new Callback<WsGetAccountsOcsDetailResponse>() {
            @Override
            public void onResponse(Call<WsGetAccountsOcsDetailResponse> call, Response<WsGetAccountsOcsDetailResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetAccountsOcsDetailResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetSubAccountInfoNew(MPApiCallback<WsGetSubAccountInfoNewResponse> callback) {
        WsGetSubAccountInfoNewRequest request = createBaseRequest(new WsGetSubAccountInfoNewRequest(), Constants.WSCODE.WS_GET_SUB_ACCOUNT_INFO_NEW);
        request.setUsername(mCamIdUserBusiness.getMetfoneUsernameIsdn());

        WsGetSubAccountInfoNewRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setSubType(SUB_TYPE_PREPAID);
        subRequest.setLanguage(LANGUAGE);

        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetSubAccountInfoNew(request.createRequestBody()).enqueue(new Callback<WsGetSubAccountInfoNewResponse>() {
            @Override
            public void onResponse(Call<WsGetSubAccountInfoNewResponse> call, Response<WsGetSubAccountInfoNewResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetSubAccountInfoNewResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    /**
     * Get History charge
     * Hiện tại đag gọi api: ENDPOINT_DEV -> khai báo api trong MetfonePlusServiceDev
     *
     * @param type       : exchange (default), basic, data, promotion
     * @param startTime: today, 7days, 30days previous from the beginning of the day (00:00:00)
     * @param callback
     */
    public void wsHistoryCharge(String type, String startTime, MPApiCallback<WsHistoryChargeResponse> callback) {
        WsHistoryChargeRequest request = createBaseRequest(new WsHistoryChargeRequest(), Constants.WSCODE.WS_HISTORY_CHARGE);

        WsHistoryChargeRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setType(type);
        subRequest.setStartTime(startTime);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsHistoryCharge(request.createRequestBody()).enqueue(new Callback<WsHistoryChargeResponse>() {
            @Override
            public void onResponse(Call<WsHistoryChargeResponse> call, Response<WsHistoryChargeResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsHistoryChargeResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    /**
     * Get History charge detail: SMS, Calling, Data screen
     * Hiện tại đag gọi api: ENDPOINT_DEV -> khai báo api trong MetfonePlusServiceDev
     *
     * @param parentType: basic, exchange, data, promotion
     * @param type        : sms, calling, data
     * @param startTime:  today, 7days, 30days previous from the beginning of the day (00:00:00)
     * @param callback
     */
    public void wsHistoryChargeDetail(String parentType, String type, String startTime, MPApiCallback<WsHistoryChargeDetailResponse> callback) {
        WsHistoryChargeDetailRequest request = createBaseRequest(new WsHistoryChargeDetailRequest(), Constants.WSCODE.WS_HISTORY_CHARGE_DETAIL);

        WsHistoryChargeDetailRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setParentType(parentType);
        subRequest.setType(type);
        subRequest.setStartTime(startTime);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsHistoryChargeDetail(request.createRequestBody()).enqueue(new Callback<WsHistoryChargeDetailResponse>() {
            @Override
            public void onResponse(Call<WsHistoryChargeDetailResponse> call, Response<WsHistoryChargeDetailResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsHistoryChargeDetailResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetProcessList(MPApiCallback<WsGetProcessListResponse> callback) {
        WsGetProcessListRequest request = createBaseRequest(new WsGetProcessListRequest(), Constants.WSCODE.WS_GET_PROCESS_LIST_CAMID);
        WsGetProcessListRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetProcessList(request.createRequestBody()).enqueue(new Callback<WsGetProcessListResponse>() {
            @Override
            public void onResponse(Call<WsGetProcessListResponse> call, Response<WsGetProcessListResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetProcessListResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetComplaintHistoryList(String fromDate, String toDate, String totalComplaint, String isdn, MPApiCallback<WsGetComplaintHistoryResponse> callback) {
        WsGetComplaintHistoryRequest request = createBaseRequest(new WsGetComplaintHistoryRequest(), Constants.WSCODE.WS_GET_COMPLAINT_HISTORY);
        WsGetComplaintHistoryRequest.Request subRequest = request.new Request();
        subRequest.language = LANGUAGE;
        subRequest.isdn = isdn;
        subRequest.fromDate = fromDate;
        subRequest.toDate = toDate;
        subRequest.totalComplaint = totalComplaint;
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetComplaintHistoryList(request.createRequestBody())
                .enqueue(new Callback<WsGetComplaintHistoryResponse>() {
                    @Override
                    public void onResponse(Call<WsGetComplaintHistoryResponse> call, Response<WsGetComplaintHistoryResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetComplaintHistoryResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void wsUpdateComplaint(String complaintId, String complaintContent, MPApiCallback<WsUpdateComplaintResponse> callback) {
        WsUpdateComplaintRequest request = createBaseRequest(new WsUpdateComplaintRequest(), Constants.WSCODE.WS_UPDATE_COMPLAINT);
        WsUpdateComplaintRequest.Request subRequest = request.new Request();
        subRequest.language = LANGUAGE;
        subRequest.isdn = mCamIdUserBusiness.getMetfoneUsernameIsdn();
        subRequest.complaintId = complaintId;
        subRequest.compContent = complaintContent;
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsUpdateComplaint(request.createRequestBody()).enqueue(new Callback<WsUpdateComplaintResponse>() {
            @Override
            public void onResponse(Call<WsUpdateComplaintResponse> call, Response<WsUpdateComplaintResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsUpdateComplaintResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsCheckComplaintNotification(MPApiCallback<WsCheckComplaintNotificationResponse> callback) {
        WsCheckComplaintNotificationRequest request = createBaseRequest(new WsCheckComplaintNotificationRequest(), Constants.WSCODE.WS_CHECK_COMPLAINT_NOTIFICATION);
        WsCheckComplaintNotificationRequest.Request subRequest = request.new Request();
        subRequest.language = LANGUAGE;
        if (mCamIdUserBusiness != null && mCamIdUserBusiness.getAccountFTTH() != null) {
            subRequest.isdn = mCamIdUserBusiness.getMetfoneUsernameIsdn() + (!mCamIdUserBusiness.getAccountFTTH().trim().isEmpty() ? ("," + mCamIdUserBusiness.getAccountFTTH().trim()) : "");

        } else {
            subRequest.isdn = mCamIdUserBusiness.getMetfoneUsernameIsdn();
        }
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsCheckComplaintNotification(request.createRequestBody()).enqueue(new Callback<WsCheckComplaintNotificationResponse>() {
            @Override
            public void onResponse(Call<WsCheckComplaintNotificationResponse> call, Response<WsCheckComplaintNotificationResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsCheckComplaintNotificationResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    /**
     * Get content complaint list in Add feedback screen (Support -> Feedback -> Add feedback)
     *
     * @param callback
     */
    public void wsGetComType(MPApiCallback<WsGetComTypeResponse> callback) {
        WsGetComTypeRequest request = createBaseRequest(new WsGetComTypeRequest(), Constants.WSCODE.WS_GET_COMPLAINT_TYPE);
        WsGetComTypeRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage("en");
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetComType(request.createRequestBody())
                .enqueue(new Callback<WsGetComTypeResponse>() {
                    @Override
                    public void onResponse(Call<WsGetComTypeResponse> call, Response<WsGetComTypeResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetComTypeResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void wsGetComType(String isdn, MPApiCallback<WsGetComTypeResponse> callback) {
        WsGetComTypeRequest request = createBaseRequest(new WsGetComTypeRequest(), Constants.WSCODE.WS_GET_COMPLAINT_TYPE);
        WsGetComTypeRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(isdn);
        subRequest.setLanguage("en");
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetComType(request.createRequestBody())
                .enqueue(new Callback<WsGetComTypeResponse>() {
                    @Override
                    public void onResponse(Call<WsGetComTypeResponse> call, Response<WsGetComTypeResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetComTypeResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void wsGetListComType(String isdn, MPApiCallback<WsGetComTypeResponse> callback) {
        WsGetComTypeRequest request = createBaseRequest(new WsGetComTypeRequest(), Constants.WSCODE.WS_GET_LIST_COMPLAINT_TYPE);
        WsGetComTypeRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(isdn);
        subRequest.setLanguage("en");
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetComTypeList(request.createRequestBody())
                .enqueue(new Callback<WsGetComTypeResponse>() {
                    @Override
                    public void onResponse(Call<WsGetComTypeResponse> call, Response<WsGetComTypeResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetComTypeResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Create feedback (complaint) for Metfone service
     *
     * @param image           base64 of the service is failed screenshot
     * @param compContent
     * @param compTypeId      id of compType get wsGetComType api
     * @param complainerPhone contact phone number
     * @param callback
     */
    public void wsSubmitComplaintMyMetfone(String serviceTypeId, String complainerAddress, String image, String compContent, String compTypeId, String complainerPhone, String errorPhone,
                                           MPApiCallback<WsSubmitComplaintMyMetfoneResponse> callback) {
        WsSubmitComplaintMyMetfoneRequest request =
                createBaseRequest(new WsSubmitComplaintMyMetfoneRequest(), Constants.WSCODE.WS_SUBMIT_COMPLAINT_MY_METFONE);
        WsSubmitComplaintMyMetfoneRequest.Request subRequest = request.new Request();

        if (image == null) {
            image = "";
        }
        subRequest.setComplainerAddress(complainerAddress);
        subRequest.setServiceTypeId(serviceTypeId);
        subRequest.setImage(image);
        subRequest.setCompContent(compContent);
        subRequest.setCompTypeId(compTypeId);
        subRequest.setComplainerPhone(complainerPhone);
        subRequest.setErrorPhone(errorPhone);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsSubmitComplaintMyMetfone(request.createRequestBody())
                .enqueue(new Callback<WsSubmitComplaintMyMetfoneResponse>() {
                    @Override
                    public void onResponse(Call<WsSubmitComplaintMyMetfoneResponse> call, Response<WsSubmitComplaintMyMetfoneResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsSubmitComplaintMyMetfoneResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void closeComplaint(String complaintId, MPApiCallback<WsCloseComplainResponse> callback) {
        WsCloseComplainRequest request = createBaseRequest(new WsCloseComplainRequest(), Constants.WSCODE.WS_CLOSE_COMPLAINT);
        WsCloseComplainRequest.Request subRequest = request.new Request();
        subRequest.isdn = mCamIdUserBusiness.getMetfoneUsernameIsdn();
        subRequest.complaintId = complaintId;
        subRequest.language = LANGUAGE;
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsCloseComplaint(request.createRequestBody())
                .enqueue(new Callback<WsCloseComplainResponse>() {
                    @Override
                    public void onResponse(Call<WsCloseComplainResponse> call, Response<WsCloseComplainResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsCloseComplainResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void wsRateComplain(String complaintId, String rate, MPApiCallback<WsRateComplainResponse> callback) {
        WsRateComplainRequest request = createBaseRequest(new WsRateComplainRequest(), Constants.WSCODE.WS_RATE_COMPLAINT);
        WsRateComplainRequest.Request subRequest = request.new Request();
        subRequest.isdn = mCamIdUserBusiness.getMetfoneUsernameIsdn();
        subRequest.complaintId = complaintId;
        subRequest.rate = rate;
        subRequest.language = LANGUAGE;
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsRateComplain(request.createRequestBody())
                .enqueue(new Callback<WsRateComplainResponse>() {
                    @Override
                    public void onResponse(Call<WsRateComplainResponse> call, Response<WsRateComplainResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsRateComplainResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Do register service exchange
     *
     * @param serviceCode code of exchange package
     * @param callback
     */
    public void wsDoActionService(String serviceCode, MPApiCallback<WsDoActionServiceResponse> callback) {
        WsDoActionServiceRequest request = createBaseRequest(new WsDoActionServiceRequest(), Constants.WSCODE.WS_DO_ACTION_SERVICE);
        WsDoActionServiceRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setActionType(Constants.WSCODE.WS_DO_ACTION_SERVICE_ACTION_TYPE_REGISTER);
        subRequest.setServiceCode(serviceCode);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsDoActionService(request.createRequestBody())
                .enqueue(new Callback<WsDoActionServiceResponse>() {
                    @Override
                    public void onResponse(Call<WsDoActionServiceResponse> call, Response<WsDoActionServiceResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsDoActionServiceResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void stopAutoRenewService(String serviceCode, MPApiCallback<WsDoActionServiceResponse> callback) {
        WsDoActionServiceRequest request = createBaseRequest(new WsDoActionServiceRequest(), Constants.WSCODE.WS_DO_ACTION_SERVICE);
        WsDoActionServiceRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setActionType(Constants.WSCODE.WS_DO_ACTION_SERVICE_ACTION_TYPE_CANCEL);
        subRequest.setServiceCode(serviceCode);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsDoActionService(request.createRequestBody())
                .enqueue(new Callback<WsDoActionServiceResponse>() {
                    @Override
                    public void onResponse(Call<WsDoActionServiceResponse> call, Response<WsDoActionServiceResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsDoActionServiceResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get nearest store base location
     *
     * @param latitude
     * @param longitude
     * @param callback
     */
    public void wsGetNearestStore(double latitude, double longitude, MPApiCallback<WsGetNearestStoreResponse> callback) {
        WsGetNearestStoreRequest request = createBaseRequest(new WsGetNearestStoreRequest(), Constants.WSCODE.WS_GET_NEAREST_STORES);
        WsGetNearestStoreRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setLatitude(latitude);
        subRequest.setLongitude(longitude);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetNearestStores(request.createRequestBody())
                .enqueue(new Callback<WsGetNearestStoreResponse>() {
                    @Override
                    public void onResponse(Call<WsGetNearestStoreResponse> call, Response<WsGetNearestStoreResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetNearestStoreResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Find store base location, district province
     * if find store follow province -> set districtId = null
     *
     * @param districtId
     * @param latitude
     * @param longitude
     * @param provinceId
     * @param callback
     */
    public void wsFindStoreByAddr(String districtId, double latitude, double longitude, String provinceId,
                                  MPApiCallback<WsFindStoreByAddrResponse> callback) {
        WsFindStoreByAddrRequest request = createBaseRequest(new WsFindStoreByAddrRequest(), Constants.WSCODE.WS_FIND_STORE_BY_ADDR);
        WsFindStoreByAddrRequest.Request subRequest = request.new Request();
        subRequest.setDistrictId(districtId);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLatitude(latitude);
        subRequest.setLanguage(LANGUAGE);
        subRequest.setProvinceId(provinceId);
        subRequest.setLongitude(longitude);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsFindStoreByAddr(request.createRequestBody())
                .enqueue(new Callback<WsFindStoreByAddrResponse>() {
                    @Override
                    public void onResponse(Call<WsFindStoreByAddrResponse> call, Response<WsFindStoreByAddrResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsFindStoreByAddrResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get store of all provinces
     *
     * @param callback
     */
    public void wsGetProvinces(MPApiCallback<WsGetProvinceDistrictResponse> callback) {
        WsGetProvincesRequest request = createBaseRequest(new WsGetProvincesRequest(), Constants.WSCODE.WS_GET_PROVINCES);
        WsGetProvincesRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetProvinces(request.createRequestBody())
                .enqueue(new Callback<WsGetProvinceDistrictResponse>() {
                    @Override
                    public void onResponse(Call<WsGetProvinceDistrictResponse> call, Response<WsGetProvinceDistrictResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetProvinceDistrictResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get all stores of a district
     *
     * @param callback
     */
    public void wsGetDistricts(String provinceId, MPApiCallback<WsGetProvinceDistrictResponse> callback) {
        WsGetDistrictsRequest request = createBaseRequest(new WsGetDistrictsRequest(), Constants.WSCODE.WS_GET_DISTRICTS);
        WsGetDistrictsRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setProvinceId(provinceId);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetDistricts(request.createRequestBody())
                .enqueue(new Callback<WsGetProvinceDistrictResponse>() {
                    @Override
                    public void onResponse(Call<WsGetProvinceDistrictResponse> call, Response<WsGetProvinceDistrictResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetProvinceDistrictResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void wsGetServicesByGroup(String serviceGroupId, MPApiCallback<WsGetServicesByGroupResponse> callback) {
        WsGetServicesByGroupRequest request = createBaseRequest(new WsGetServicesByGroupRequest(), Constants.WSCODE.WS_GET_SERVICES_BY_GROUP);
        request.setUsername(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        WsGetServicesByGroupRequest.Request subRequest = request.new Request();
        subRequest.setServiceGroupId(serviceGroupId);
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetServicesByGroup(request.createRequestBody())
                .enqueue(new Callback<WsGetServicesByGroupResponse>() {
                    @Override
                    public void onResponse(Call<WsGetServicesByGroupResponse> call, Response<WsGetServicesByGroupResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetServicesByGroupResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void wsGetCurrentUsedServices(MPApiCallback<WsGetCurrentUsedServicesResponse> callback) {
        WsGetCurrentUsedServicesRequest request = createBaseRequest(new WsGetCurrentUsedServicesRequest(), Constants.WSCODE.WS_GET_CURRENT_USED_SERVICES);
        request.setApiKey(API_KEY);
        request.setUsername(null);
        WsGetCurrentUsedServicesRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetCurrentUsedServices(request.createRequestBody())
                .enqueue(new Callback<WsGetCurrentUsedServicesResponse>() {
                    @Override
                    public void onResponse(Call<WsGetCurrentUsedServicesResponse> call, Response<WsGetCurrentUsedServicesResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetCurrentUsedServicesResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Topup using QR scan
     *
     * @param qrCodeRequest
     * @param callback
     */
    public void qrCodeTopUpMP(TopUpQrCodeRequest qrCodeRequest, ApiCallback<BaseResponseMP<TopUpQrCodeResponse>> callback) {
        getMetfonePlusService().wsTopUp(new BaseDataRequestMP<>(qrCodeRequest, Constants.WSCODE.WS_TOP_UP)).
                enqueue(new Callback<BaseResponseMP<TopUpQrCodeResponse>>() {
                    @Override
                    public void onResponse(Call<BaseResponseMP<TopUpQrCodeResponse>> call, Response<BaseResponseMP<TopUpQrCodeResponse>> response) {
                        if (callback != null) callback.onResponse(response);
                    }

                    @Override
                    public void onFailure(Call<BaseResponseMP<TopUpQrCodeResponse>> call, Throwable t) {
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void getHistoryTopUpMP(HistoryRequest historyRequest, ApiCallback<BaseResponseMP<HistoryResponse>> callback) {
        getMetfonePlusService().wsHistoryTopUp(new BaseDataRequestMP<>(historyRequest, Constants.WSCODE.WS_HISTORY_TOP_UP_NEW))
                .enqueue(new Callback<BaseResponseMP<HistoryResponse>>() {
                    @Override
                    public void onResponse(Call<BaseResponseMP<HistoryResponse>> call, Response<BaseResponseMP<HistoryResponse>> response) {
                        if (callback != null) callback.onResponse(response);
                    }

                    @Override
                    public void onFailure(Call<BaseResponseMP<HistoryResponse>> call, Throwable t) {
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void transMobileMP(TransMobileRequest transMobileRequest, ApiCallback<TransMobileResponse> callback) {
        getRetrofitMerchant().create(MetfonePlusServiceDev.class).transMobileMP(transMobileRequest).enqueue(new Callback<TransMobileResponse>() {
            @Override
            public void onResponse(Call<TransMobileResponse> call, Response<TransMobileResponse> response) {
                if (callback != null) callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<TransMobileResponse> call, Throwable t) {
                if (callback != null) callback.onError(t);
            }
        });
    }

    /**
     * Get otp
     *
     * @param callback
     */
    public void wsGetOTPMetFonePlus(String phone, MPApiCallback<WsGetOtpResponse> callback) {
        WsGetProvincesRequest request = createBaseRequest(new WsGetProvincesRequest(), Constants.WSCODE.WS_GET_OTP_METFONT);
        WsGetProvincesRequest.Request subRequest = request.new Request();
        if (phone.equals("")) {
            subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        } else
            subRequest.setIsdn(phone);
        subRequest.setService("wsGetOtpMetfone");
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetOTP(request.createRequestBody())
                .enqueue(new Callback<WsGetOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsGetOtpResponse> call, Response<WsGetOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get otp
     *
     * @param callback
     */
    public void wsGetOTPDonate(String phone, MPApiCallback<WsGetOtpResponse> callback) {
        WsGetProvincesRequest request = createBaseRequest(new WsGetProvincesRequest(), Constants.WSCODE.WS_GET_OTP_METFONT);
        WsGetProvincesRequest.Request subRequest = request.new Request();
        if (phone.equals("")) {
            subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        } else
            subRequest.setIsdn(phone);
        subRequest.setService("wsGetOTPDonate");
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetOTP(request.createRequestBody())         //server that
//        getDonateService().wsGetOTP(request.createRequestBody())          //servre test
                .enqueue(new Callback<WsGetOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsGetOtpResponse> call, Response<WsGetOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Check otp
     *
     * @param callback
     */
    public void wsCheckOTPMetFonePlus(String phone, String otp, MPApiCallback<WsCheckOtpResponse> callback) {
        WsCheckOtpRequest request = createBaseRequest(new WsCheckOtpRequest(), Constants.WSCODE.WS_CONFIRM_OTP);
        WsCheckOtpRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setOtp(otp);
        subRequest.setService("wsGetOtpMetfone");
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsCheckOTP(request.createRequestBody())
                .enqueue(new Callback<WsCheckOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsCheckOtpResponse> call, Response<WsCheckOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsCheckOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    public void getListPrefixNumber(ApiCallback<WsGetListPrefixResponse> callback) {
        WsGetListPrefixRequest request = createBaseRequest(new WsGetListPrefixRequest(), Constants.WSCODE.WS_GET_LIST_PREFIX_NUMBER);
        WsGetListPrefixRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().getListPrefixNumber(request.createRequestBody()).enqueue(new Callback<WsGetListPrefixResponse>() {
            @Override
            public void onResponse(Call<WsGetListPrefixResponse> call, Response<WsGetListPrefixResponse> response) {
                if (callback != null) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void onFailure(Call<WsGetListPrefixResponse> call, Throwable t) {
                if (callback != null) {
                    callback.onError(t);
                }
            }
        });
    }


    public void searchNumberToBuy(WsSearchNumberToBuyRequest request, ApiCallback<WsSearchNumberToBuyResponse> callback) {
        WsSearchNumberToBuyRequest.Request subRequest = request.getWsRequest();

        if (subRequest != null) {
            subRequest.setLanguage(LANGUAGE);
        }

        request.setWsRequest(subRequest);
        getMetfonePlusService().searchNumberToBuy(request.createRequestBody()).enqueue(new Callback<WsSearchNumberToBuyResponse>() {
            @Override
            public void onResponse(Call<WsSearchNumberToBuyResponse> call, Response<WsSearchNumberToBuyResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsSearchNumberToBuyResponse> call, Throwable t) {
                if (callback != null) {
                    callback.onError(t);
                }
            }
        });
    }

    /**
     * Check otp
     *
     * @param callback
     */
    public void wsChangeIsdnKeepSim(String phone, String otp, String price, String newPhone, String OrderType, MPApiCallback<WsCheckOtpResponse> callback) {
        WsChangeKeepSimRequest request = createBaseRequest(new WsChangeKeepSimRequest(), Constants.WSCODE.WS_CHANGE_ISDN_KEEP_SIM);
        WsChangeKeepSimRequest.Request subRequest = request.new Request();
        subRequest.setOldIsdn(phone);
        subRequest.setOtpCode(otp);
        subRequest.setRegisterFee(0);
        subRequest.setOrderType(OrderType);
        subRequest.setPriceIsdn(price);
        subRequest.setOrderIsdn(newPhone);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsChangeKeepSim(request.createRequestBody())
                .enqueue(new Callback<WsCheckOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsCheckOtpResponse> call, Response<WsCheckOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsCheckOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Check otp
     *
     * @param callback
     */
    public void wsChangeIsdnOrderSim(String phone, String otp, String price, String newPhone, String OrderType, Integer monthlyFee, MPApiCallback<WsCheckOtpResponse> callback) {
        WsChangeKeepSimRequest request = createBaseRequest(new WsChangeKeepSimRequest(), Constants.WSCODE.WS_CHANGE_ISDN_KEEP_SIM);
        WsChangeKeepSimRequest.Request subRequest = request.new Request();
        subRequest.setOldIsdn(phone);
        subRequest.setOtpCode(otp);
        subRequest.setPriceIsdn(price);
        subRequest.setOrderIsdn(newPhone);
        subRequest.setOrderType(OrderType);
        subRequest.setRegisterFee(monthlyFee);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsChangeKeepSim(request.createRequestBody())
                .enqueue(new Callback<WsCheckOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsCheckOtpResponse> call, Response<WsCheckOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsCheckOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get list history ordered sim
     *
     * @param callback
     */
    public void wsGetLstHistoryOrderedSim(MPApiCallback<WsHistoryOrderedSimResponse> callback) {
        WsGetProvincesRequest request = createBaseRequest(new WsGetProvincesRequest(), Constants.WSCODE.WS_LIST_HISTORY_ORDERED_SIM);
        WsGetProvincesRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetListHistoryOrderedSim(request.createRequestBody())
                .enqueue(new Callback<WsHistoryOrderedSimResponse>() {
                    @Override
                    public void onResponse(Call<WsHistoryOrderedSimResponse> call, Response<WsHistoryOrderedSimResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsHistoryOrderedSimResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get store of all provinces
     *
     * @param callback
     */
    public void wsLockIsdnToBuy(String phone, MPApiCallback<WsLockIsdnToBuyResponse> callback) {
        WsLockIsdnToBuyRequest request = createBaseRequest(new WsLockIsdnToBuyRequest(), Constants.WSCODE.WS_LOCK_ISDN_TO_BUY);
        WsLockIsdnToBuyRequest.Request subRequest = request.new Request();
        subRequest.setIsdnFrm(phone);
        subRequest.setCustomerIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsLockIsdnToBuy(request.createRequestBody())
                .enqueue(new Callback<WsLockIsdnToBuyResponse>() {
                    @Override
                    public void onResponse(Call<WsLockIsdnToBuyResponse> call, Response<WsLockIsdnToBuyResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsLockIsdnToBuyResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Check otp
     *
     * @param callback
     */
    public void wsCheckOTPIshareMetFonePlus(String phone, String otp, int amount, MPApiCallback<WsCheckOtpResponse> callback) {
        WsCheckOtpIshareRequest request = createBaseRequest(new WsCheckOtpIshareRequest(), Constants.WSCODE.WS_CONFIRM_OTP_ISHARE);
        WsCheckOtpIshareRequest.Request subRequest = request.new Request();
        subRequest.setIsdnSender(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setIsdnReceiver(phone);
        subRequest.setOtp(otp);
        subRequest.setAmount(amount);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsCheckOTPIshare(request.createRequestBody())
                .enqueue(new Callback<WsCheckOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsCheckOtpResponse> call, Response<WsCheckOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsCheckOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get otp
     *
     * @param callback
     */
    public void wsGetOTPIshareMetFonePlus(MPApiCallback<WsGetOtpResponse> callback) {
        WsGetProvincesRequest request = createBaseRequest(new WsGetProvincesRequest(), Constants.WSCODE.WS_GET_OTP_MY_METFONT);
        WsGetProvincesRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setService("wsGetOtpMetfone");
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetOTP(request.createRequestBody())
                .enqueue(new Callback<WsGetOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsGetOtpResponse> call, Response<WsGetOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get store of all provinces
     *
     * @param callback
     */
    public void getRankingListDetail(MPApiCallback<GetRankingListDetailResponse> callback) {
        WsLockIsdnToBuyRequest request = createBaseRequest(new WsLockIsdnToBuyRequest(), Constants.WSCODE.WS_GET_RANKING_LIST_DETAIL);
        WsLockIsdnToBuyRequest.Request subRequest = request.new Request();
        subRequest.setIsdnFrm(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().getRankingListDetail(request.createRequestBody())
                .enqueue(new Callback<GetRankingListDetailResponse>() {
                    @Override
                    public void onResponse(Call<GetRankingListDetailResponse> call, Response<GetRankingListDetailResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<GetRankingListDetailResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });

    }


    public void wsABAMobilePaymentTopUp(MPApiCallback<ABAMobilePaymentTopUpResponse> callback, ABAMobilePaymentTopUpRequest.Request requestBody) {
        ABAMobilePaymentTopUpRequest request = createBaseRequest(new ABAMobilePaymentTopUpRequest(), Constants.WSCODE.WS_ABA_MOBILE_PAYMENT_TOP_UP);
        request.setWsRequest(requestBody);

        getMetfonePlusService().wsABAMobilePaymentTopUp(request.createRequestBody()).enqueue(new Callback<ABAMobilePaymentTopUpResponse>() {
            @Override
            public void onResponse(Call<ABAMobilePaymentTopUpResponse> call, Response<ABAMobilePaymentTopUpResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<ABAMobilePaymentTopUpResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsABAMobilePaymentTopUpCallBack(MPApiCallback<ABAMobilePaymentTopUpCallBackResponse> callback, ABAMobilePaymentTopUpCallBackRequest.Request requestBody) {
        ABAMobilePaymentTopUpCallBackRequest request = createBaseRequest(new ABAMobilePaymentTopUpCallBackRequest(), Constants.WSCODE.WS_ABA_MOBILE_PAYMENT_TOP_UP_CALLBACK);
        request.setWsRequest(requestBody);

        getMetfonePlusService().wsABAMobilePaymentTopUpCallBack(request.createRequestBody()).enqueue(new Callback<ABAMobilePaymentTopUpCallBackResponse>() {
            @Override
            public void onResponse(Call<ABAMobilePaymentTopUpCallBackResponse> call, Response<ABAMobilePaymentTopUpCallBackResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<ABAMobilePaymentTopUpCallBackResponse> call, Throwable t) {
                Log.e("ERRR", "ee " + t.getMessage());
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void updateDeviceToken(String userId, String token, String language, String deviceId, MPApiCallback<BaseResponse> callback) {
        WsUpdateDeviceTokenRequest request = createBaseRequest(new WsUpdateDeviceTokenRequest(), Constants.WSCODE.WS_UPDATE_CAMID_DEVICE_TOKEN);
        WsUpdateDeviceTokenRequest.Request subRequest = request.new Request();
        subRequest.camId = userId;
        subRequest.language = language;
        subRequest.token = token;
        subRequest.deviceId = deviceId;
        request.setWsRequest(subRequest);
        getMetfonePlusService().updateDeviceToken(request.createRequestBody()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void updateDeviceStatus(String userId, String isLogin, String language, String deviceId, MPApiCallback<BaseResponse> callback) {
        WsUpdateDeviceStatusRequest request = createBaseRequest(new WsUpdateDeviceStatusRequest(), Constants.WSCODE.WS_UPDATE_CAMID_DEVICE_STATUS);
        WsUpdateDeviceStatusRequest.Request subRequest = request.new Request();
        if ("0".equals(userId)) {
            return;
        }
        subRequest.camid = userId;
        subRequest.language = language;
        subRequest.isLogin = isLogin;
        subRequest.deviceId = deviceId;
        request.setWsRequest(subRequest);
        getMetfonePlusService().updateDeviceToken(request.createRequestBody()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void getBillPaymentInfo(String accountNumber, MPApiCallback<WsGetAccountPaymentInfoResponse> callback) {
        WsGetAccountPaymentInfoRequest request = createBaseRequest(new WsGetAccountPaymentInfoRequest(), Constants.WSCODE.WS_GET_ACCOUNT_PAYMENT_INFO);
        WsGetAccountPaymentInfoRequest.Request subRequest = new WsGetAccountPaymentInfoRequest.Request();
        subRequest.language = LANGUAGE;
        subRequest.isdn = accountNumber;
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetAccountPaymentInfo(request.createRequestBody()).enqueue(new Callback<WsGetAccountPaymentInfoResponse>() {
            @Override
            public void onResponse(Call<WsGetAccountPaymentInfoResponse> call, Response<WsGetAccountPaymentInfoResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsGetAccountPaymentInfoResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void createBillPaymentTrans(WsCreateBillPaymenTransRequest request, ApiCallback<WsCreateBillPaymentTransResponse> callback) {
        getBillPaymentService().wsCreateBillPaymentTrans(request).enqueue(new Callback<WsCreateBillPaymentTransResponse>() {
            @Override
            public void onResponse(Call<WsCreateBillPaymentTransResponse> call, Response<WsCreateBillPaymentTransResponse> response) {
                if (callback != null) callback.onResponse(response);
            }

            @Override
            public void onFailure(Call<WsCreateBillPaymentTransResponse> call, Throwable t) {
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsABAMobileBillPayment(ABAMobilePaymentBillPaymentRequest.Request requestBody, MPApiCallback<ABAMobilePaymentBillPaymentResponse> callback) {
        ABAMobilePaymentBillPaymentRequest request = createBaseRequest(new ABAMobilePaymentBillPaymentRequest(), Constants.WSCODE.WS_ABA_MOBILE_BILL_PAYMENT);
        request.setWsRequest(requestBody);
        getAbabBillPaymentRequest().wsABAMobileBillPayment(request.createRequestBody()).enqueue(new Callback<ABAMobilePaymentBillPaymentResponse>() {
            @Override
            public void onResponse(Call<ABAMobilePaymentBillPaymentResponse> call, Response<ABAMobilePaymentBillPaymentResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<ABAMobilePaymentBillPaymentResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsBillPaymentTemplateRequest(BillPaymentTemplateRequest.Request requestBody, MPApiCallback<BillPaymentTemplateResponse> callback) {
        BillPaymentTemplateRequest request = createBaseRequest(new BillPaymentTemplateRequest(), Constants.WSCODE.WS_BILL_PAYMENT_HISTORY);
        requestBody.language = LANGUAGE;
        requestBody.isdn = mCamIdUserBusiness.getMetfoneUsernameIsdn();
        request.setWsRequest(requestBody);
        getMetfonePlusService().wsGetBillPaymentTemplate(request.createRequestBody()).enqueue(new Callback<BillPaymentTemplateResponse>() {
            @Override
            public void onResponse(Call<BillPaymentTemplateResponse> call, Response<BillPaymentTemplateResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<BillPaymentTemplateResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsGetChangeCardList(String startDate, String endDate, String status, MPApiCallback<WsChangeCardListResponse> callback) {
        WsChangeCardGetListRequest request = createBaseRequest(new WsChangeCardGetListRequest(), Constants.WSCODE.WS_CHANGE_CARD_GET_LIST);
        WsChangeCardGetListRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setStartTime(startDate);
        subRequest.setEndTime(endDate);
        subRequest.setStatus(status);
        request.setWsRequest(subRequest);

        getMetfonePlusService().wsGetChangeCardList(request.createRequestBody()).enqueue(new Callback<WsChangeCardListResponse>() {
            @Override
            public void onResponse(Call<WsChangeCardListResponse> call, Response<WsChangeCardListResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<WsChangeCardListResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    public void wsAddChangeCard(String serial, String phoneNumber, String dataImage, String video, MPApiCallback<BaseResponse> callback) {
        WsAddChangeCardRequest request = createBaseRequest(new WsAddChangeCardRequest(), Constants.WSCODE.WS_ADD_CHANGE_CARD);
        WsAddChangeCardRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(mCamIdUserBusiness.getMetfoneUsernameIsdn());
        subRequest.setLanguage(LANGUAGE);
        subRequest.setSerial(serial);
        subRequest.setPhoneNumber(phoneNumber);
        subRequest.setDataImage(dataImage);
        subRequest.setVideo(video);
        request.setWsRequest(subRequest);

        getMetfonePlusService().addChangeCard(request.createRequestBody()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    /**
     * Get info of user using FTTH service by searchFTTH or forgetRequestID
     *
     * @param isSearchFTTH    call searchFTTH or forgetRequestID
     * @param requestIdOrISDN the value of the requestID or isdn field entered by the user
     * @param callback
     */
    public void searchFTTHRequest(boolean isSearchFTTH, String requestIdOrISDN, MPApiCallback<CheckRequestResponse> callback) {
        SearchFTTHRequest request = createBaseRequest(new SearchFTTHRequest(), Constants.WSCODE.WS_SEARCH_FTTH);
        SearchFTTHRequest.Request subRequest = request.new Request();
        if (isSearchFTTH) {
            subRequest.setRequestId(requestIdOrISDN);
        } else {
            subRequest.setIsdn(requestIdOrISDN);
        }
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);

        getMetfonePlusService().searchFTTHRequest(request.createRequestBody()).enqueue(new Callback<CheckRequestResponse>() {
            @Override
            public void onResponse(Call<CheckRequestResponse> call, Response<CheckRequestResponse> response) {
                if (response.code() == 200) {
                    if (callback != null) callback.onResponse(response);
                } else {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<CheckRequestResponse> call, Throwable t) {
                showFailure(t);
                if (callback != null) callback.onError(t);
            }
        });
    }

    /**
     * Push wsPushInfoPartner
     *
     * @param subReq
     */
    public void wsPushInfoPartner(WsPushInfoPartnerId.Request subReq) {
        WsPushInfoPartnerId request = createBaseRequest(new WsPushInfoPartnerId(), Constants.WSCODE.WS_PARTNER_ID);
        request.setWsRequest(subReq);

        getMetfonePlusService().wsPushInfoPartner(request.createRequestBody()).enqueue(new Callback<PartnerIDResponse>() {
            @Override
            public void onResponse(Call<PartnerIDResponse> call, Response<PartnerIDResponse> response) {
                Log.e(TAG, "onResponse: " + response.code());
                if (response.code() != 200) {
                    showResponseError(response.code());
                }
            }

            @Override
            public void onFailure(Call<PartnerIDResponse> call, Throwable t) {
                showFailure(t);
            }
        });
    }

    /**
     * Get otp
     *
     * @param callback
     */
    public void wsSubscriberGetOTPMetFone(String phone, MPApiCallback<WsGetOtpResponse> callback) {
        WsGetProvincesRequest request = createBaseRequest(new WsGetProvincesRequest(), Constants.WSCODE.WS_SUB_GET_OTP_METFONT);
        WsGetProvincesRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(phone);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetOTP(request.createRequestBody())
                .enqueue(new Callback<WsGetOtpResponse>() {
                    @Override
                    public void onResponse(Call<WsGetOtpResponse> call, Response<WsGetOtpResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsGetOtpResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get idsn from ip
     *
     * @param callback
     */
    public void wsDetachIp(String ip, MPApiCallback<WsDetachIPResponse> callback) {
        WsDetachIPRequest request = createBaseRequest(new WsDetachIPRequest(), Constants.WSCODE.WS_SUB_DETACH_IP);
        WsDetachIPRequest.Request subRequest = request.new Request();
        subRequest.setIp(ip);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsDetachIP(request.createRequestBody())
                .enqueue(new Callback<WsDetachIPResponse>() {
                    @Override
                    public void onResponse(Call<WsDetachIPResponse> call, Response<WsDetachIPResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsDetachIPResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }


    /**
     * Get information customer by ISDN & OTP
     *
     * @param callback
     */
    public void wsSubscriberGetInfoCusByISDN(String otp, String phoneNumber, MPApiCallback<WsSubGetInfoCusResponse> callback) {
        WsSubGetInfoCusByOtpRequest request = createBaseRequest(new WsSubGetInfoCusByOtpRequest(), Constants.WSCODE.WS_SUBSCRIBE_GET_INFO_BY_ISDN);
        WsSubGetInfoCusByOtpRequest.Request subRequest = request.new Request();
        subRequest.setIsdn(phoneNumber);
        subRequest.setOtp(otp);
        subRequest.setLanguage(LANGUAGE);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsGetInfoCus(request.createRequestBody())
                .enqueue(new Callback<WsSubGetInfoCusResponse>() {
                    @Override
                    public void onResponse(Call<WsSubGetInfoCusResponse> call, Response<WsSubGetInfoCusResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsSubGetInfoCusResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * Get information customer by ISDN & OTP
     *
     * @param callback
     */
    public void wsSubscriberUpdateInfoCus(WsSubscriberUpdateCustomerInfoRequest.Request subRequest, MPApiCallback<WsSubUpdateInfoCusResponse> callback) {
        WsSubscriberUpdateCustomerInfoRequest request = createBaseRequest(new WsSubscriberUpdateCustomerInfoRequest(), Constants.WSCODE.WS_SUBSCRIBE_UPDATE_CUSTOMER_INFO);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsSubUpdateCustomerInfo(request.createRequestBody())
                .enqueue(new Callback<WsSubUpdateInfoCusResponse>() {
                    @Override
                    public void onResponse(Call<WsSubUpdateInfoCusResponse> call, Response<WsSubUpdateInfoCusResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsSubUpdateInfoCusResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }

    /**
     * ORC
     *
     * @param callback
     */
    public void wsDetectORCImage(String type, String base64Image, MPApiCallback<WsDetectORCResponse> callback) {
        WSDetectORCRequest request = createBaseRequest(new WSDetectORCRequest(), Constants.WSCODE.WS_DETECT_ORC_FROM_IMAGE);
        WSDetectORCRequest.Request subRequest = request.new Request();
        subRequest.setLanguage(LANGUAGE);
        subRequest.setImage(base64Image);
        subRequest.setType(type);
        request.setWsRequest(subRequest);
        getMetfonePlusService().wsORCDetectImage(request.createRequestBody())
                .enqueue(new Callback<WsDetectORCResponse>() {
                    @Override
                    public void onResponse(Call<WsDetectORCResponse> call, Response<WsDetectORCResponse> response) {
                        if (response.code() == 200) {
                            if (callback != null) callback.onResponse(response);
                        } else {
                            showResponseError(response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<WsDetectORCResponse> call, Throwable t) {
                        showFailure(t);
                        if (callback != null) callback.onError(t);
                    }
                });
    }
}

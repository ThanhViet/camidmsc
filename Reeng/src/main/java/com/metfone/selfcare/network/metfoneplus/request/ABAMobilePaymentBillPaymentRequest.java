package com.metfone.selfcare.network.metfoneplus.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class ABAMobilePaymentBillPaymentRequest extends BaseRequest<ABAMobilePaymentBillPaymentRequest.Request> {
    public static class Request{
        public String isdn;
        public String totalAmount;
        public String accountEmoney;
        public String ftthAccount;
        public String contractIdInfo;
        public String paymentAmount;
        public String paymentMethod;
        public String ftthType;
        public String ftthName;
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;

public class LoginRequest extends AutoLoginRequest {
    @SerializedName("isEncrypt")
    private String isEncrypt;

    @SerializedName("password")
    private String password;

    @SerializedName("otp")
    private String otp;

    @SerializedName("clientOS")
    private String clientOS;

    public String getIsEncrypt() {
        return isEncrypt;
    }

    public void setIsEncrypt(String isEncrypt) {
        this.isEncrypt = isEncrypt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getClientOS() {
        return clientOS;
    }

    public void setClientOS(String clientOS) {
        this.clientOS = clientOS;
    }
}

package com.metfone.selfcare.network.fileTransfer;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by ThanhNT72 on 5/15/2015.
 */
public class DownloadMultiFileAsysncTask extends AsyncTask<Void, Integer, StickerItem> {

    private static final String TAG = DownloadMultiFileAsysncTask.class.getSimpleName();
    private static final int READ_SIZE = 1024 * 32;
    private String pathOfDownloadingFile;
    private StickerBusiness mStickerBusiness;
    private StickerItem mSticker;
    private ApplicationController mApp;
    private String pathImg, pathVoice;

    public DownloadMultiFileAsysncTask(ApplicationController mApp, StickerItem stickerItem) {
        this.mApp = mApp;
        mStickerBusiness = mApp.getStickerBusiness();
        mSticker = stickerItem;
    }

    @Override
    protected StickerItem doInBackground(Void... voids) {
        pathOfDownloadingFile = Config.Storage.REENG_STORAGE_FOLDER + Config.Storage.STICKER_FOLDER
                + mSticker.getCollectionId();
        File file = new File(pathOfDownloadingFile);
        if (!file.exists()) {
            file.mkdirs();
        }
        //TODO url download
        //download file anh thanh cong thi moi down file audio
        if (TextUtils.isEmpty(mSticker.getUrlImg())
                || TextUtils.isEmpty(mSticker.getUrlVoice())) {
            return null;
        }
        pathImg = pathOfDownloadingFile + "/" + FileHelper.getNameFileFromURL(mSticker.getUrlImg());
        pathVoice = pathOfDownloadingFile + "/" + FileHelper.getNameFileFromURL(mSticker.getUrlVoice());

        if (downloadFile(UrlConfigHelper.getInstance(mApp).getDomainFile() + mSticker.getUrlImg(), pathImg)) {
            mSticker.setImagePath(pathImg);
            mSticker.setDownloadImg(true);
            //download file audio thanh cong thi return true
            if (downloadFile(UrlConfigHelper.getInstance(mApp).getDomainFile() + mSticker.getUrlVoice(), pathVoice)) {
                mSticker.setVoicePath(pathVoice);
                mSticker.setDownloadVoice(true);
                return mSticker;
            } else {
                return null;
            }
        }
        return null;
    }


/*    @Override
    protected void onPostExecute(Boolean result) {
        mStickerBusiness.updateStickerItem(mSticker);
        if (result) {
            Log.i(TAG, "Download done all, update DB and refresh");
            mStickerBusiness.updateStickerItem(mSticker);
//            mMessage.setStatus(ReengMessageConstant.STATUS_RECEIVED);
        } else {
//            mMessage.setStatus(ReengMessageConstant.STATUS_FAIL);
        }
//        mMessageBusiness.updateAllFieldsOfMessage(mMessage);
//        mMessageBusiness.refreshThreadWithoutNewMessage(mMessage.getThreadId());
        super.onPostExecute(result);
    }*/

    private boolean downloadFile(String urlString, String pathFile) {
        Log.i(TAG, "urlString: " + urlString + " pathFile: " + pathFile);
        InputStream byteStream = null;
        int count;
        // Downloads the image and catches IO errors
        HttpURLConnection httpConn = null;
        try {
            URL url = new URL(urlString);
            httpConn = (HttpURLConnection) url.openConnection();
            //                    httpConn.setRequestProperty("Accept-Encoding", "gzip");
            httpConn.setConnectTimeout(Constants.HTTP.CONNECTION_TIMEOUT);
            httpConn.connect();
            byteStream = httpConn.getInputStream();
            InputStream input = new BufferedInputStream(byteStream,
                    READ_SIZE);
            OutputStream output = new FileOutputStream(pathFile);
            byte data[] = new byte[READ_SIZE];
            int total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                // writing data to file
                output.write(data, 0, count);
            }
            output.flush();
            // closing streams
            output.close();
            input.close();
            Log.i(TAG, "Download complete: " + pathFile);
            return true;

        } catch (MalformedURLException muex) {
            Log.e(TAG, "MalformedURLException: " + muex);
            return false;
        } catch (IOException ioe) {
            Log.e(TAG, "IOException: " + ioe);
            return false;
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
            return false;
        } finally {
            if (null != byteStream) {
                try {
                    byteStream.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
            if (httpConn != null) {
                Log.i(TAG, " httpConn disconnected");
                httpConn.disconnect();
            }
        }
    }
}
package com.metfone.selfcare.network.camid.request;

import com.google.gson.annotations.SerializedName;

public class UpdateMetfoneRequest extends BaseRequest<UpdateMetfoneRequest.Request> {
    public class Request {
        @SerializedName("user_service_id")
        private Integer userServiceId;

        @SerializedName("phone_number")
        private String phoneNumber;

        @SerializedName("otp")
        private String otp;

        public Integer getUserServiceId() {
            return userServiceId;
        }

        public void setUserServiceId(int userServiceId) {
            if (userServiceId == -1) {
                this.userServiceId = null;
            } else {
                this.userServiceId = userServiceId;
            }
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;

public class WsCreateBillPaymentTransResponse {
    public String responseCode;
    public String responseMessage;
    public String respcode;
    public String respdesc;
    public Boolean status;
    @SerializedName("redirect_url")
    public String redirectUrl;
    @SerializedName("error_description")
    public String errorDescription;
    @SerializedName("error_field")
    public String errorField;
}

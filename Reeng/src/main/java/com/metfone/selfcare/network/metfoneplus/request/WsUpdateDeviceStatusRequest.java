package com.metfone.selfcare.network.metfoneplus.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsUpdateDeviceStatusRequest extends BaseRequest<WsUpdateDeviceStatusRequest.Request> {
    public class Request{
        public String language;
        public String camid;
        public String deviceId;
        public String isLogin;
    }
}

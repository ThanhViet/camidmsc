package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;


public class WSDetectORCRequest extends BaseRequest<WSDetectORCRequest.Request> {
    public class Request {
        @SerializedName("language")
        private String language;
        @SerializedName("isScan")
        private String isScan;
        @SerializedName("type")
        private String type;
        @SerializedName("image")
        private String image;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getIsScan() {
            return isScan;
        }

        public void setIsScan(String isScan) {
            this.isScan = isScan;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}

/**
 */

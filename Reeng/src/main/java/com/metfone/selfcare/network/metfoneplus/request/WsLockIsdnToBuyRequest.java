package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsLockIsdnToBuyRequest extends BaseRequest<WsLockIsdnToBuyRequest.Request> {
    public class Request {
        @SerializedName("isdnFrm")
        private String isdnFrm;
        @SerializedName("customerIsdn")
        private String customerIsdn;
        @SerializedName("language")
        private String language;

        public String getIsdnFrm() {
            return isdnFrm;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public void setIsdnFrm(String isdnFrm) {
            this.isdnFrm = isdnFrm;
        }

        public String getCustomerIsdn() {
            return customerIsdn;
        }

        public void setCustomerIsdn(String customerIsdn) {
            this.customerIsdn = customerIsdn;
        }
    }
}

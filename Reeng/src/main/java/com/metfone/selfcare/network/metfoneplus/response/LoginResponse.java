package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.Application;
import com.metfone.selfcare.model.camid.Function;
import com.metfone.selfcare.model.camid.Role;

import java.util.List;

/**
 * Models the get posts response for AutoLogin n Login
 */
public class LoginResponse {
    @SerializedName("sessionId")
    private String sessionId;

    @SerializedName("username")
    private String username;

    @SerializedName("token")
    private String token;

    @SerializedName("application")
    private List<Application> applications;

    @SerializedName("role")
    private List<Role> roles;

    @SerializedName("function")
    private List<Function> functions;

    @SerializedName("errorCode")
    private String errorCode = null;

    @SerializedName("errorMessage")
    private String errorMessage = null;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

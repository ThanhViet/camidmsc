package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

public class WsGetSubAccountInfoNewResponse extends BaseResponse<WsGetSubAccountInfoNewResponse.Response> {
    public class Response {
        @SerializedName("name")
        private String name;
        @SerializedName("mainAcc")
        private Double mainAcc;
        @SerializedName("proAcc")
        private Double proAcc;
        @SerializedName("dataPkgName")
        private String dataPkgName;
        @SerializedName("dataVolume")
        private Double dataVolume;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getMainAcc() {
            return mainAcc;
        }

        public void setMainAcc(Double mainAcc) {
            this.mainAcc = mainAcc;
        }

        public Double getProAcc() {
            return proAcc;
        }

        public void setProAcc(Double proAcc) {
            this.proAcc = proAcc;
        }

        public String getDataPkgName() {
            return dataPkgName;
        }

        public void setDataPkgName(String dataPkgName) {
            this.dataPkgName = dataPkgName;
        }

        public Double getDataVolume() {
            return dataVolume;
        }

        public void setDataVolume(Double dataVolume) {
            this.dataVolume = dataVolume;
        }
    }
}

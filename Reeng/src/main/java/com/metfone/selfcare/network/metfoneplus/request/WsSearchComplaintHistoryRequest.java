package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

/**
 * API for get complaint history
 */
public class WsSearchComplaintHistoryRequest extends BaseRequest<WsSearchComplaintHistoryRequest.Request> {
    public class Request {

        @SerializedName("isdn")
        private String isdn;

        @SerializedName("fromDate")
        private String fromDate;

        @SerializedName("toDate")
        private String toDate;

        @SerializedName("totalComplaint")
        private String totalComplaint;

        @SerializedName("language")
        private String language;

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        public void setToDate(String toDate) {
            this.toDate = toDate;
        }

        public String getTotalComplaint() {
            return totalComplaint;
        }

        public void setTotalComplaint(String totalComplaint) {
            this.totalComplaint = totalComplaint;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

/**
 * API for get complaint history
 */
public class WsUpdateComplaintRequest extends BaseRequest<WsUpdateComplaintRequest.Request> {
    public class Request {

        @SerializedName("language")
        public String language;
        @SerializedName("isdn")
        public String isdn;
        @SerializedName("complaintId")
        public String complaintId;
        @SerializedName("compContent")
        public String compContent;

    }
}

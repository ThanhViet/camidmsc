package com.metfone.selfcare.network.metfoneplus;

import android.util.Log;

import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public interface GenerateRequestBody {
    default RequestBody createRequestBody() {
        String json = new Gson().toJson(this);
        Log.d("GenerateRequestBody", "createRequestBody: " + json);
        return RequestBody.create(MediaType.parse("application/json"), json);
    }
}
package com.metfone.selfcare.network.file;

/**
 * Priority values.  Requests will be processed from higher priorities to
 * lower priorities, in FIFO order.
 * Created by toanvk2 on 7/21/2016.
 */
public enum Priority {
    LOW,
    NORMAL,
    HIGH,
    IMMEDIATE
}
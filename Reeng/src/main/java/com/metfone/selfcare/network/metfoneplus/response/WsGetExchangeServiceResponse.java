package com.metfone.selfcare.network.metfoneplus.response;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetExchangeServiceResponse extends BaseResponse<List<WsGetExchangeServiceResponse.Response>> {
    public class Response {
        @SerializedName("groupName")
        private String groupName;
        @SerializedName("groupCode")
        private String groupCode;
        @SerializedName("image")
        private String image;
        @SerializedName("listItem")
        private List<ExchangeItem> listItem;

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getGroupCode() {
            return groupCode;
        }

        public void setGroupCode(String groupCode) {
            this.groupCode = groupCode;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public List<ExchangeItem> getListItem() {
            return listItem;
        }

        public void setListItem(List<ExchangeItem> listItem) {
            this.listItem = listItem;
        }
    }
}

package com.metfone.selfcare.network.metfoneplus;

import com.metfone.selfcare.module.home_kh.api.response.ABAMobilePaymentTopUpCallBackResponse;
import com.metfone.selfcare.module.home_kh.api.response.ABAMobilePaymentTopUpResponse;
import com.metfone.selfcare.module.home_kh.api.response.CheckRequestResponse;
import com.metfone.selfcare.module.home_kh.api.response.GetRankingListDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.PartnerIDResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.BaseDataRequestMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.HistoryRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TopUpQrCodeRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TransMobileRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.HistoryResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TopUpQrCodeResponse;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TransMobileResponse;
import com.metfone.selfcare.network.metfoneplus.request.WsCreateBillPaymenTransRequest;
import com.metfone.selfcare.network.metfoneplus.response.ABAMobilePaymentBillPaymentResponse;
import com.metfone.selfcare.network.metfoneplus.response.BillPaymentTemplateResponse;
import com.metfone.selfcare.network.metfoneplus.response.LoginResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsAccountInfoResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsChangeCardListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckComplaintNotificationResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCloseComplainResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsCreateBillPaymentTransResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDetachIPResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDetectORCResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsDoActionServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsFindStoreByAddrResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountPaymentInfoResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountsOcsDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComTypeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetComplaintHistoryResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetCurrentUsedServicesResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetExchangeServiceResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetListPrefixResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetNearestStoreResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProcessListResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetProvinceDistrictResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServicesByGroupResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetSubAccountInfoNewResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeDetailResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryChargeResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsHistoryOrderedSimResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsLockIsdnToBuyResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsRateComplainResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSearchNumberToBuyResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubGetInfoCusResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubUpdateInfoCusResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsSubmitComplaintMyMetfoneResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsUpdateComplaintResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * MetfonePlus(selfcare) apis
 */
public interface MetfonePlusService {
    // 0. AutoLogin
    @POST("CamIDAutoLogin")
    Call<LoginResponse> autoLogin(@Body RequestBody requestBody);

    // 1. Login
    @POST("UserLogin")
    Call<LoginResponse> login(@Body RequestBody requestBody);

    // 2. wsAccountInfo
    @POST("UserRouting")
    Call<WsAccountInfoResponse> wsAccountInfo(@Body RequestBody requestBody);

    // 3. wsGetSubAccountInfoNew
    @POST("UserRouting")
    Call<WsGetSubAccountInfoNewResponse> wsGetSubAccountInfoNew(@Body RequestBody requestBody);

    // 4. wsGetAccountsOcsDetail
    @POST("UserRouting")
    Call<WsGetAccountsOcsDetailResponse> wsGetAccountsOcsDetail(@Body RequestBody requestBody);

    // 5. wsGetExchangeService
    @POST("UserRouting")
    Call<WsGetExchangeServiceResponse> wsGetExchangeService(@Body RequestBody requestBody);

    // 6. wsTopUp
    @POST("UserRouting")
    Call<BaseResponseMP<TopUpQrCodeResponse>> wsTopUp(@Body BaseDataRequestMP<TopUpQrCodeRequest> qrCodeRequest);

    // 7. wsHistoryTopUp
    @POST("UserRouting")
    Call<BaseResponseMP<HistoryResponse>> wsHistoryTopUp(@Body BaseDataRequestMP<HistoryRequest> requestBaseDataRequestMP);

    // 8. wsGetServices

    // 9. wsGetServicesByGroup
    @POST("UserRouting")
    Call<WsGetServicesByGroupResponse> wsGetServicesByGroup(@Body RequestBody requestBody);

    // 10. wsGetServiceDetail
    @POST("UserRouting")
    Call<WsGetServiceDetailResponse> wsGetServiceDetail(@Body RequestBody requestBody);

    // 11. wsHistoryTopUpNew
    @POST("UserRouting")
    Call<BaseResponseMP<HistoryResponse>> wsHistoryTopUpNew(@Body BaseDataRequestMP<HistoryRequest> requestBaseDataRequestMP);

    // 12. wsDoActionService
    @POST("UserRouting")
    Call<WsDoActionServiceResponse> wsDoActionService(@Body RequestBody requestBody);

    // 13. wsHistoryCharge
    @POST("UserRouting")
    Call<WsHistoryChargeResponse> wsHistoryCharge(@Body RequestBody requestBody);

    // 14. Payment - createTransMobile
    @POST(".")
    Call<TransMobileResponse> transMobileMP(@Body TransMobileRequest transMobileRequest);

    // 15. wsGetCurrentUsedServices
    @POST("UserRouting")
    Call<WsGetCurrentUsedServicesResponse> wsGetCurrentUsedServices(@Body RequestBody requestBody);

    // 16. wsHistoryChargeDetail
    @POST("UserRouting")
    Call<WsHistoryChargeDetailResponse> wsHistoryChargeDetail(@Body RequestBody requestBody);

    // 17. wsCheckUseGame

    // 18. wsGetComType - Feedback: list complaint
    @POST("UserRouting")
    Call<WsGetComTypeResponse> wsGetComType(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetComTypeResponse> wsGetComTypeList(@Body RequestBody requestBody);

    // 19. wsGetProcessList
    @POST("UserRouting")
    Call<WsGetProcessListResponse> wsGetProcessList(@Body RequestBody requestBody);

    // 20. wsGetComplaintHistory
    @POST("UserRouting")
    Call<WsGetComplaintHistoryResponse> wsGetComplaintHistoryList(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsUpdateComplaintResponse> wsUpdateComplaint(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsCheckComplaintNotificationResponse> wsCheckComplaintNotification(@Body RequestBody requestBody);

    // 21. wsSubmitComplaintMyMetfone
    @POST("UserRouting")
    Call<WsSubmitComplaintMyMetfoneResponse> wsSubmitComplaintMyMetfone(@Body RequestBody requestBody);

    // 22. wsCloseComplain
    @POST("UserRouting")
    Call<WsCloseComplainResponse> wsCloseComplaint(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsRateComplainResponse> wsRateComplain(@Body RequestBody requestBody);

    // 23. wsFindStoreByAddr
    @POST("UserRouting")
    Call<WsFindStoreByAddrResponse> wsFindStoreByAddr(@Body RequestBody requestBody);

    // 24. wsGetProvinces
    @POST("UserRouting")
    Call<WsGetProvinceDistrictResponse> wsGetProvinces(@Body RequestBody requestBody);

    // 25. wsGetDistricts
    @POST("UserRouting")
    Call<WsGetProvinceDistrictResponse> wsGetDistricts(@Body RequestBody requestBody);

    // 26. wsGetNearestStores
    @POST("UserRouting")
    Call<WsGetNearestStoreResponse> wsGetNearestStores(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetOtpResponse> wsGetOTP(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsDetachIPResponse> wsDetachIP(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsCheckOtpResponse> wsCheckOTP(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetListPrefixResponse> getListPrefixNumber(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsSearchNumberToBuyResponse> searchNumberToBuy(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsCheckOtpResponse> wsChangeKeepSim(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsHistoryOrderedSimResponse> wsGetListHistoryOrderedSim(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsLockIsdnToBuyResponse> wsLockIsdnToBuy(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsCheckOtpResponse> wsCheckOTPIshare(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<GetRankingListDetailResponse> getRankingListDetail(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<ABAMobilePaymentTopUpResponse> wsABAMobilePaymentTopUp(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<ABAMobilePaymentTopUpCallBackResponse> wsABAMobilePaymentTopUpCallBack(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<BaseResponse> updateDeviceToken(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsChangeCardListResponse> wsGetChangeCardList(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<BaseResponse> addChangeCard(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsGetAccountPaymentInfoResponse> wsGetAccountPaymentInfo(@Body RequestBody requestBody);

    //    @POST("metfone-topup-portal_war/createTransMobilePayment")
    @POST("createTransMobilePayment")
    Call<WsCreateBillPaymentTransResponse> wsCreateBillPaymentTrans(@Body WsCreateBillPaymenTransRequest requestBody);

    //    @POST("cdbr/paymentRequestABAMobile")
    @POST(".")
    Call<ABAMobilePaymentBillPaymentResponse> wsABAMobileBillPayment(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<BillPaymentTemplateResponse> wsGetBillPaymentTemplate(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<CheckRequestResponse> searchFTTHRequest(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<PartnerIDResponse> wsPushInfoPartner(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsSubGetInfoCusResponse> wsGetInfoCus(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsSubUpdateInfoCusResponse> wsSubUpdateCustomerInfo(@Body RequestBody requestBody);

    @POST("UserRouting")
    Call<WsDetectORCResponse> wsORCDetectImage(@Body RequestBody requestBody);
}
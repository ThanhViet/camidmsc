package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetServiceDetailRequest extends BaseRequest<WsGetServiceDetailRequest.Request> {
    public class Request {
        @SerializedName("language")
        private String language;
        @SerializedName("serviceCode")
        private String serviceCode;
        @SerializedName("isdn")
        private String isdn;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getIsdn() {
            return isdn;
        }

        public void setIsdn(String isdn) {
            this.isdn = isdn;
        }
    }
}

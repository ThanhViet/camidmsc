package com.metfone.selfcare.network.metfoneplus.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WsGetListPrefixRequest extends BaseRequest<WsGetListPrefixRequest.Request> {
    @Getter
    @Setter
    public class Request {
        @SerializedName("language")
        String language;
    }
}

package com.metfone.selfcare.network.file;

/**
 * Created by toanvk2 on 7/20/2016.
 */
public class RetryException extends Exception {

    public RetryException() {
        super("Maximum retry exceeded");
    }

    public RetryException(Throwable cause) {
        super(cause);
    }
}

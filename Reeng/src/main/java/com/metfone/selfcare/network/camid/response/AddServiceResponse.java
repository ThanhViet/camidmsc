package com.metfone.selfcare.network.camid.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.model.camid.Service;
import com.metfone.selfcare.model.camid.User;

import java.util.List;

/**
 * {
 *     "code": "00",
 *     "message": "SUCCESS",
 *     "data": {
 *         "user": {
 *             "username": "user@92491c79-5f76-494f-86e7-f4756faf9130",
 *             "email": null,
 *             "address": "Phnom Penh",
 *             "gender": 0,
 *             "avatar": null,
 *             "verified": null,
 *             "user_id": 161,
 *             "phone_number": "0882980208",
 *             "full_name": "Yaang Vu",
 *             "date_of_birth": null,
 *             "identity_number": "unknown"
 *         },
 *         "services": [
 *             {
 *                 "service_id": 1,
 *                 "service_code": "MOBILE",
 *                 "service_name": "Mobile",
 *                 "phone_linked": [
 *                     {
 *                         "status": 1,
 *                         "user_service_id": 21,
 *                         "phone_number": "0882980208"
 *                     },
 *                     {
 *                         "status": 1,
 *                         "user_service_id": 84,
 *                         "phone_number": "0314342599"
 *                     }
 *                 ]
 *             },
 *             {
 *                 "service_id": 2,
 *                 "service_code": "FTTH",
 *                 "service_name": "Ftth",
 *                 "phone_linked": []
 *             }
 *         ],
 *         "identity_providers": []
 *     }
 * }
 */
public class AddServiceResponse extends BaseResponse<AddServiceResponse.Response> {
    public  class Response {
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("services")
        @Expose
        private List<Service> services = null;
        @SerializedName("identity_providers")
        @Expose
        private List<Object> identityProviders = null;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public List<Service> getServices() {
            return services;
        }

        public void setServices(List<Service> services) {
            this.services = services;
        }

        public List<Object> getIdentityProviders() {
            return identityProviders;
        }

        public void setIdentityProviders(List<Object> identityProviders) {
            this.identityProviders = identityProviders;
        }
    }
}

package com.metfone.selfcare.di;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.activity.createChannel.CreateChannelPresenter;
import com.metfone.selfcare.ui.tabvideo.activity.createChannel.CreateChannelPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.activity.videoLibrary.TabVideoLibraryPresenter;
import com.metfone.selfcare.ui.tabvideo.activity.videoLibrary.TabVideoLibraryPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device.UploadVideoDevicePresenter;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device.UploadVideoDevicePresenterImpl;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes.UploadVideoYoutubePresenter;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes.UploadVideoYoutubePresenterImpl;
import com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary.VideoLibraryPresenter;
import com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary.VideoLibraryPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerContact;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail.MovieDetailContact;
import com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail.MovieDetailPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.VideoDetailPresenter;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.VideoDetailPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.VideoDetailView;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement.ChannelManagementContact;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement.ChannelManagementFragment;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement.ChannelManagementPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelContact;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelFragment;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelPresenterImpl;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload.TopMoneyUploadContact;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload.TopMoneyUploadFragment;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload.TopMoneyUploadPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tuanha00 on 3/22/2018.
 */

@Module
public class ActivityModule {

    private BaseView baseView;

    public ActivityModule(BaseActivity activity) {
        this.baseView = activity;
    }

    public ActivityModule(BaseFragment fragment) {
        this.baseView = fragment;
    }

    @Provides
    @PerActivity
    BaseView providesMainView() {
        return baseView;
    }

    @Provides
    @PerActivity
    CreateChannelPresenter providesCreateChannelPresenter(ApplicationController application, BaseView baseView) {
        return new CreateChannelPresenterImpl(application, baseView);
    }

    @Provides
    @PerActivity
    TabVideoLibraryPresenter providesTabVideoLibraryPresenter(ApplicationController application, BaseView baseView) {
        return new TabVideoLibraryPresenterImpl(application, baseView);
    }

    @Provides
    @PerActivity
    VideoLibraryPresenter providesVideoLibraryPresenter(ApplicationController application, BaseView baseView) {
        return new VideoLibraryPresenterImpl(application, baseView);
    }

    @Provides
    @PerActivity
    UploadVideoDevicePresenter providesUploadVideoDevicePresenter(ApplicationController application, BaseView baseView) {
        return new UploadVideoDevicePresenterImpl(application, baseView);
    }

    @Provides
    @PerActivity
    UploadVideoYoutubePresenter providesUploadVideoYoutubePresenter(ApplicationController application, BaseView baseView) {
        return new UploadVideoYoutubePresenterImpl(application, baseView);
    }

    @Provides
    @PerActivity
    VideoDetailPresenter provideVideoDetailPresenter(ApplicationController application, BaseView baseView) {
        return new VideoDetailPresenterImpl((VideoDetailView) baseView, application);
    }

    @Provides
    @PerActivity
    VideoPlayerContact.VideoPlayerPresenter provideVideoDetailActivityPresenter(ApplicationController application, BaseView baseView) {
        return new VideoPlayerPresenterImpl((VideoPlayerContact.VideoPlayerView) baseView, application);
    }

    @Provides
    @PerActivity
    SubscribeChannelContact.SubscribeChannelPresenter provideSubscribeChannelPresenter(ApplicationController app) {
        return new SubscribeChannelPresenterImpl((SubscribeChannelContact.SubscribeChannelView) baseView, app);
    }

    @Provides
    @PerActivity
    SubscribeChannelContact.SubscribeChannelProvider provideSubscribeChannelProvider() {
        return new SubscribeChannelFragment();
    }

    @Provides
    @PerActivity
    ChannelManagementContact.ChannelManagementPresenter provideChannelManagementPresenter(ApplicationController app) {
        return new ChannelManagementPresenterImpl((ChannelManagementContact.ChannelManagementView) baseView, app);
    }

    @Provides
    @PerActivity
    ChannelManagementContact.ChannelManagementProvider provideChannelManagementProvider() {
        return new ChannelManagementFragment();
    }

    @Provides
    @PerActivity
    MovieDetailContact.MovieDetailPresenter provideMovieDetailPresenter(ApplicationController app) {
        return new MovieDetailPresenterImpl((MovieDetailContact.MovieDetailView) baseView, app);
    }

    @Provides
    @PerActivity
    TopMoneyUploadContact.TopMoneyUploadPresenter provideTopMoneyUploadPresenter(ApplicationController app) {
        return new TopMoneyUploadPresenterImpl((TopMoneyUploadContact.TopMoneyUploadView) baseView, app);
    }

    @Provides
    @PerActivity
    TopMoneyUploadContact.TopMoneyUploadProvider provideTopMoneyUploadProvider() {
        return new TopMoneyUploadFragment();
    }
}

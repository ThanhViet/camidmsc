package com.metfone.selfcare.di;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.common.utils.image.ImageUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.ui.tabvideo.listener.OnChannelChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<T extends BaseView> implements MvpPresenter,
        OnChannelChangedDataListener,
        OnVideoChangedDataListener,
        OnInternetChangedListener {

    protected CompositeDisposable compositeDisposable;
    protected ApplicationController application;
    protected ListenerUtils listenerUtils;
    protected ChannelApi channelApi;
    protected ImageUtils imageUtils;
    protected VideoApi videoApi;
    protected Utils utils;
    protected T view;

    protected int widthScreen;
    protected int heightScreen;

    public BasePresenter(T view, ApplicationController application) {
        ApplicationComponent applicationComponent = application.getApplicationComponent();
        this.view = view;
        this.application = application;
        this.listenerUtils = applicationComponent.providerListenerUtils();
        this.channelApi = applicationComponent.providerChannelApi();
        this.imageUtils = applicationComponent.providesImageUtils();
        this.videoApi = applicationComponent.providerVideoApi();
        this.utils = applicationComponent.providesUtils();

        this.widthScreen = utils.getWidthScreen();
        this.heightScreen = utils.getHeightScreen();

        compositeDisposable = new CompositeDisposable();
        listenerUtils.addListener(this);
    }

    @Override
    public void onChannelCreate(Channel channel) {
    }

    @Override
    public void onChannelUpdate(Channel channel) {
    }

    @Override
    public void onChannelSubscribeChanged(Channel channel) {
    }

    @Override
    public void onVideoLikeChanged(Video video) {
        videoChangeData(video, Type.LIKE);
    }

    @Override
    public void onVideoShareChanged(Video video) {
        videoChangeData(video, Type.SHARE);
    }

    @Override
    public void onVideoCommentChanged(Video video) {
        videoChangeData(video, Type.COMMENT);
    }

    @Override
    public void onVideoSaveChanged(Video video) {
        videoChangeData(video, Type.SAVE);
    }

    @Override
    public void onVideoWatchLaterChanged(Video video) {
        videoChangeData(video, Type.WATCH_LATER);
    }


    protected void videoChangeData(Video video, Type type) {
    }

    @Override
    public void onInternetChanged() {
    }

    @Override
    public void dispose() {
        if (compositeDisposable != null) compositeDisposable.dispose();
        if (listenerUtils != null) listenerUtils.removerListener(this);
        view = null;
    }

    protected int getThumbnail(int position) {
        switch (position % 10) {
            case 0:
                return R.drawable.error;
            case 1:
                return R.drawable.error_2;
            case 2:
                return R.drawable.error_3;
            case 3:
                return R.drawable.error_4;
            case 4:
                return R.drawable.error_5;
            case 5:
                return R.drawable.error_6;
            case 6:
                return R.drawable.error_7;
            case 7:
                return R.drawable.error_8;
            case 8:
                return R.drawable.error_9;
            case 9:
                return R.drawable.error_10;
            default:
                return R.drawable.error_11;
        }
    }

    protected enum Type {
        LIKE, SHARE, COMMENT, WATCH_LATER, SAVE
    }
}

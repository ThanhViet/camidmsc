package com.metfone.selfcare.di;

public interface MvpPresenter {
    void dispose();
}

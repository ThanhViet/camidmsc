package com.metfone.selfcare.di;

import com.metfone.selfcare.activity.update_info.AddressFragment;
import com.metfone.selfcare.activity.update_info.ConfirmIdentityInfoActivity;
import com.metfone.selfcare.activity.update_info.IDPictureFragment;
import com.metfone.selfcare.activity.update_info.InformationFragment;
import com.metfone.selfcare.ui.tabvideo.activity.createChannel.CreateChannelActivity;
import com.metfone.selfcare.ui.tabvideo.activity.uploadVideo.UploadVideoActivity;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.device.UploadVideoDeviceFragment;
import com.metfone.selfcare.ui.tabvideo.fragment.uploadVideo.youtubes.UploadVideoYoutubeFragment;
import com.metfone.selfcare.ui.tabvideo.fragment.videoLibrary.VideoLibraryFragment;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.movieDetail.MovieDetailFragment;
import com.metfone.selfcare.ui.tabvideo.playVideo.videoDetail.VideoDetailFragment;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.SubscribeChannelActivity;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.channelManagement.ChannelManagementFragment;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.detail.SubscribeChannelFragment;
import com.metfone.selfcare.ui.tabvideo.subscribeChannel.topmoneyupload.TopMoneyUploadFragment;

import dagger.Component;

/**
 * Created by HoangAnhTuan on 3/20/2018.
 */

@PerActivity
@Component(modules = {ActivityModule.class}, dependencies = {ApplicationComponent.class})
public interface ActivityComponent {

    void inject(VideoPlayerActivity videoDetailActivity);

    //void inject(ChannelsActivity channelsActivity);

    void inject(VideoLibraryFragment videoLibraryFragment);

    void inject(CreateChannelActivity createChannelActivity);

    void inject(UploadVideoDeviceFragment uploadVideoDeviceFragment);

    void inject(UploadVideoYoutubeFragment uploadVideoYoutubeFragment);

    void inject(VideoDetailFragment videoDetailFragment);

    void inject(UploadVideoActivity uploadVideoActivity);

    void inject(SubscribeChannelActivity subscribeChannelActivity);

    void inject(SubscribeChannelFragment subscribeChannelFragment);

    void inject(ChannelManagementFragment channelManagementFragment);

    void inject(MovieDetailFragment movieDetailFragment);

    void inject(TopMoneyUploadFragment channelManagementFragment);

    void inject(ConfirmIdentityInfoActivity confirmInformationActivity);

    void inject(AddressFragment addressFragment);
    void inject(InformationFragment informationFragment);
    void inject(IDPictureFragment idPictureFragment);

}

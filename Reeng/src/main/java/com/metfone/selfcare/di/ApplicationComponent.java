package com.metfone.selfcare.di;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.api.UserApi;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.common.utils.image.ImageUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.common.utils.screen.ScreenUtils;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by tuanha00 on 3/14/2018.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(ApplicationController applicationController);

    ApplicationController providerApplicationController();

    VideoApi providerVideoApi();

    ChannelApi providerChannelApi();

    UserApi provideUserApi();

    ListenerUtils providerListenerUtils();

    ScreenUtils providesScreenUtils();

    ImageUtils providesImageUtils();

    Utils providesUtils();

    LogApi provideLogApi();

}


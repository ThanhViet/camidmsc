package com.metfone.selfcare.di;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.api.LogImpl;
import com.metfone.selfcare.common.api.UserApi;
import com.metfone.selfcare.common.api.UserImpl;
import com.metfone.selfcare.common.api.video.channel.ChannelApi;
import com.metfone.selfcare.common.api.video.channel.ChannelApiImpl;
import com.metfone.selfcare.common.api.video.video.VideoApi;
import com.metfone.selfcare.common.api.video.video.VideoApiImpl;
import com.metfone.selfcare.common.utils.Utils;
import com.metfone.selfcare.common.utils.UtilsImpl;
import com.metfone.selfcare.common.utils.image.ImageUtils;
import com.metfone.selfcare.common.utils.image.ImageUtilsImpl;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.common.utils.listener.ListenerUtilsImpl;
import com.metfone.selfcare.common.utils.screen.ScreenUtils;
import com.metfone.selfcare.common.utils.screen.ScreenUtilsImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tuanha00 on 3/14/2018.
 */

@Module
public class ApplicationModule {

    private ApplicationController application;

    public ApplicationModule(ApplicationController application) {
        this.application = application;
    }

    @Provides
    @Singleton
    ApplicationController providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    ChannelApi providesChannelApi() {
        return new ChannelApiImpl(application);
    }

    @Provides
    @Singleton
    VideoApi providesVideoApi() {
        return new VideoApiImpl(application);
    }

    @Provides
    @Singleton
    UserApi provideUserApi() {
        return new UserImpl(application);
    }

    @Provides
    @Singleton
    ListenerUtils providesListenerUtils() {
        return new ListenerUtilsImpl();
    }

    @Provides
    @Singleton
    ScreenUtils providesScreenUtils() {
        return new ScreenUtilsImpl(application);
    }

    @Provides
    @Singleton
    ImageUtils providesImageUtils() {
        return new ImageUtilsImpl(application);
    }

    @Provides
    @Singleton
    Utils providesUtils(ScreenUtils screenUtils, VideoApi videoApi, ListenerUtils listenerUtils) {
        return new UtilsImpl(application, screenUtils);
    }

    @Provides
    @Singleton
    LogApi provideLogApi() {
        return new LogImpl(application);
    }
}


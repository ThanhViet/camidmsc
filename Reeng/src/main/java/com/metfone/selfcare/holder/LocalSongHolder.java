package com.metfone.selfcare.holder;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.LocalSongListAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.LocalSongInfo;

/**
 * Created by tungt on 2/25/16.
 */
public class LocalSongHolder {
    private View mConvertView;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private TextView mTvName, mTvSinger, mTvFileSize;
    private Button mBtnUpload;
    private LocalSongInfo mLocalSongInfo;
    private LocalSongListAdapter.OnItemClicked mItemClickedListener;

    public LocalSongHolder(Context mContext, LayoutInflater mLayoutInflater, LocalSongListAdapter.OnItemClicked mItemClickedListener) {
        this.mContext = mContext;
        this.mLayoutInflater = mLayoutInflater;
        this.mItemClickedListener = mItemClickedListener;
    }

    public void initView(ViewGroup parent){
        View convertView = mLayoutInflater.inflate(R.layout.holder_local_song, parent, false);
        mTvName = (TextView) convertView.findViewById(R.id.title);
        mTvSinger = (TextView) convertView.findViewById(R.id.singer);
        mTvFileSize = (TextView) convertView.findViewById(R.id.file_size_tv);
        mBtnUpload = (Button) convertView.findViewById(R.id.btn_upload);
        ((ImageView) convertView.findViewById(R.id.image_thumnail)).setImageResource(R.drawable.ic_onmedia_music);
        convertView.setTag(this);
        mConvertView = convertView;
    }

    public void showData(LocalSongInfo songInfo){
        mLocalSongInfo = songInfo;
        mTvName.setText(songInfo.getName());
        mTvSinger.setText(songInfo.getSinger());
        mTvFileSize.setText(songInfo.getFileSizeStr());
        if(songInfo.isUploaded()){
            mBtnUpload.setText(mContext.getResources().getString(R.string.try_play_music));
            mBtnUpload.setTextColor(ContextCompat.getColor(mContext,R.color.bg_stranger_music));
            mBtnUpload.setBackgroundResource(R.drawable.selector_rec_w_border_green);
            mTvFileSize.setVisibility(View.GONE);
        }else {
            mBtnUpload.setText(mContext.getResources().getString(R.string.text_button_upload));
            mTvFileSize.setVisibility(View.VISIBLE);
            mBtnUpload.setTextColor(ContextCompat.getColor(mContext,R.color.bg_mocha));
            mBtnUpload.setBackgroundResource(R.drawable.selector_rec_w_border_purple);
        }

        mBtnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickedListener.onClicked(mLocalSongInfo);
            }
        });
    }

    public View getConvertView() {
        return mConvertView;
    }
}

package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 7/8/2014.
 */
public class SendTransferMoneyHolder extends BaseMessageHolder {
    private ReengMessage message;
    private String TAG = SendTransferMoneyHolder.class.getSimpleName();
    private TextView mTvwContent, mTvwTimeTransfer;
    private SmartTextClickListener mSmartTextListener;
    //    private TextView mTvwSmsNotice;
    private int chatMode;

    public SendTransferMoneyHolder(Context ctx, SmartTextClickListener smartTextListener) {
        mSmartTextListener = smartTextListener;
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_transfer_money_send, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mTvwTimeTransfer = (TextView) rowView.findViewById(R.id.time_stransfer_money_txt);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        Resources res = mContext.getResources();
        message = (ReengMessage) obj;
        //        Log.d(TAG, "" + message);
        chatMode = message.getChatMode();
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_4));
        mTvwTimeTransfer.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1));
        if (message.getMessageType() == ReengMessageConstant.MessageType.restore) {
            mTvwContent.setText(res.getString(R.string.message_restored));
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_message_time));
        } else {
            //        mTvwContent.setText(message.getContent(), true, mSmartTextListener, longClickListener);
            String content = message.getContent() + " " + message.getImageUrl();
            mTvwContent.setText(content);
            try {
                String timeTransfer = TimeHelper.formatTimeTransferMoney(Long.parseLong(message.getFilePath()));
                mTvwTimeTransfer.setVisibility(View.VISIBLE);
                mTvwTimeTransfer.setText(String.format(res.getString(R.string.time_transfer_money), timeTransfer));
            } catch (NumberFormatException e) {
                Log.e(TAG, "Exception", e);
                mTvwTimeTransfer.setVisibility(View.GONE);
            }
            setSMSmessage();
        }
    }

    private void setSMSmessage() {
        if (chatMode == ReengMessageConstant.MODE_GSM) {
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            //            mImgSms.setVisibility(View.VISIBLE);
            String smsNote = message.getFileName();
            Log.i(TAG, "smsNOte = " + smsNote);
            if (isDifferSenderWithAfter() && smsNote != null && smsNote.length() > 0) {
                Log.i(TAG, "setSMSmessage show text status " + message);
                mTvwStatus.setVisibility(View.VISIBLE);
                mTvwStatus.setText(message.getFileName());
            } else {
                Log.i(TAG, "setSMSmessage invi text status " + message);
                mTvwStatus.setVisibility(View.GONE);
            }
        } else {
            Log.i(TAG, "setSMSmessage mode ip -ip " + message);
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_message_send));
            //            mTvwSmsNotice.setVisibility(View.GONE);
        }
    }
}
package com.metfone.selfcare.holder.onmedia.feeds;

import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
public class ItemInvisibleHolder extends OMFeedBaseViewHolder {



    public ItemInvisibleHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
    }

    @Override
    public void setElement(Object obj) {
    }
}

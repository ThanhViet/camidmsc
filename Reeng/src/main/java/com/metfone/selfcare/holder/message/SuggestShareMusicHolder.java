package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;

/**
 * Created by toanvk2 on 3/21/2015.
 */
public class SuggestShareMusicHolder extends BaseMessageHolder {
    private ApplicationController mApplication;
    private ReengMessage message;
    private MediaModel mediaModel;
    private TextView mTvwContent;
    private TextView mTvwSongName;
    private TextView mTvwSongSinger;
    private ImageView mImgSongAvatar;

    public SuggestShareMusicHolder(ApplicationController app, MessageInteractionListener listener) {
        mApplication = app;
        setContext(app);
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_suggest_share_music, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_suggest_share_music_title);
        mTvwSongName = (TextView) rowView.findViewById(R.id.message_suggest_share_music_song_name);
        mTvwSongSinger = (TextView) rowView.findViewById(R.id.message_suggest_share_music_song_singer);
        mImgSongAvatar = (ImageView) rowView.findViewById(R.id.message_suggest_share_music_avatar);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        drawHolder();
    }

    private void drawHolder() {
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSongName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSongSinger.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwContent.setText(mContext.getResources().getText(R.string.msg_suggest_share_music));
        mediaModel = message.getSongModel(mApplication.getMusicBusiness());
        if (mediaModel != null) {
            // get data
            String songName = mediaModel.getName();
            String songSinger = mediaModel.getSinger();
            String avatarUrl = mediaModel.getImage();
            // draw
            if (!TextUtils.isEmpty(songName)) {
                mTvwSongName.setText(songName);
            } else {
                mTvwSongName.setText("");
            }
            if (!TextUtils.isEmpty(songSinger)) {
                mTvwSongSinger.setText(songSinger);
            } else {
                mTvwSongSinger.setText("");
            }
            mApplication.getAvatarBusiness().setSongAvatar(mImgSongAvatar, avatarUrl);
        } else {
            // neu chua co media thi get from sv, sau khi get duoc thi notify o thread detail
            mTvwSongName.setText("");
            mTvwSongSinger.setText("");
            mApplication.getAvatarBusiness().setSongAvatar(mImgSongAvatar, "");
            // mApplication.getMusicBusiness().getMediaModelFromServer(message.getSongId(), message.getThreadId());
        }
    }
}
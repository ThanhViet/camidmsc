package com.metfone.selfcare.holder.message;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.textview.EmoTagTextViewListChat;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 7/8/2014.
 */
public class SendTextHolder extends BaseMessageHolder {
    private ReengMessage message;
    private String TAG = SendTextHolder.class.getSimpleName();
    private EmoTagTextViewListChat mTvwContent;
    private ImageView mImgLargeEmo;
    private SmartTextClickListener mSmartTextListener;
    private ApplicationController mApp;

    private View viewPreviewUrl;
    private AspectImageView mImgThumb;
    private TextView mTvwTitle;
    private TextView mTvwDesc;
    private TextView mTvwSite;
    private TextView mTvwTime;
    private int widthImgNews, heightImgNews;
    private LinearLayout mLnTvContent;

    //    private TextView mTvwSmsNotice;

    public SendTextHolder(Context ctx, SmartTextClickListener smartTextListener) {
        mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
        widthImgNews = mApp.getResources().getDimensionPixelOffset(R.dimen.width_button_send_gift);
        heightImgNews = Math.round(widthImgNews / 1.77f);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_text_send, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (EmoTagTextViewListChat) rowView.findViewById(R.id.message_text_content);
        mImgLargeEmo = (ImageView) rowView.findViewById(R.id.message_detail_file_item_content);
        viewPreviewUrl = rowView.findViewById(R.id.view_preview_url);
        mTvwTitle = (TextView) rowView.findViewById(R.id.tvw_title_preview_url);
        mTvwDesc = (TextView) rowView.findViewById(R.id.tvw_desc_preview_url);
        mTvwSite = (TextView) rowView.findViewById(R.id.tvw_site_preview_url);
        mImgThumb = (AspectImageView) rowView.findViewById(R.id.img_thumb_preview_url);
        mTvwTime = (TextView) rowView.findViewById(R.id.message_time);
        mLnTvContent = rowView.findViewById(R.id.ln_tv_content);

        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mLnTvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTvwTime.setVisibility(mTvwTime.getVisibility()==View.VISIBLE? View.GONE:View.VISIBLE);
            }
        });
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1_5));
        mTvwSite.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1_5));
        // setMessage Id
        viewPreviewUrl.setVisibility(View.GONE);
        final MessageInteractionListener mMessageInteractionListener = getMessageInteractionListener();
        View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mMessageInteractionListener != null)
                    mMessageInteractionListener.longClickBgrCallback(message);
                return true;
            }
        };
        if (message.getMessageType() == ReengMessageConstant.MessageType.restore) {
            mTvwContent.setNormalText(getContext().getResources().getString(R.string.message_restored),
                    mSmartTextListener, longClickListener);
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_message_time));
        } else {
            drawContentMessage();
            if (message.getChatMode() != ReengMessageConstant.MODE_GSM && message.isLargeEmo() && isNoReply()) {
                mTvwContent.setVisibility(View.GONE);
                mImgLargeEmo.setVisibility(View.VISIBLE);
                ImageLoaderManager.getInstance(mContext).displayLargeEmoticon(mImgLargeEmo, message.getContent());
            } else if (message.isShowTranslate() && !TextUtils.isEmpty(message.getTextTranslated())) {
                mTvwContent.setVisibility(View.VISIBLE);
                mImgLargeEmo.setVisibility(View.GONE);
                mTvwContent.setEmoticon(message.getTextTranslated(), message.getId(), mSmartTextListener,
                        longClickListener, message);
            } else {
                mTvwContent.setVisibility(View.VISIBLE);
                mImgLargeEmo.setVisibility(View.GONE);
                if (message.getListTagContent() == null || message.getListTagContent().isEmpty()) {
                    mTvwContent.setEmoticon(message.getContent(), message.getId(), mSmartTextListener, longClickListener,
                            message);
                } else {
                    mTvwContent.setEmoticonWithTag(mApp, message.getContent(), message.getId(), message,
                            message.getListTagContent(), onClickTag, mSmartTextListener, longClickListener);
                }

                setDataPreviewUrl(mMessageInteractionListener);
            }
        }
        mTvwContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMessageInteractionListener.textContentClickCallBack(message);
            }
        });
    }

    private void setDataPreviewUrl(final MessageInteractionListener mMessageInteractionListener) {
        if (TextUtils.isEmpty(message.getFilePath())) {
            MessageHelper.checkFirstUrlAndGetMetaData(message, mApp);
        } else {
            Log.d(TAG, "filepath: " + message.getFilePath());
            FeedContent feedContent = null;
            try {
                feedContent = new Gson().fromJson(message.getFilePath(), FeedContent.class);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (feedContent != null) {
                if (TextUtils.isEmpty(feedContent.getImageUrl())) {
                    viewPreviewUrl.setVisibility(View.GONE);
                } else {
                    viewPreviewUrl.setVisibility(View.VISIBLE);
                    mImgThumb.setVisibility(View.VISIBLE);
                    ImageLoaderManager.getInstance(mApp).setImageFeeds(mImgThumb, feedContent.getImageUrl(),
                            widthImgNews, heightImgNews);
                    if (!TextUtils.isEmpty(feedContent.getItemName())) {
                        mTvwTitle.setVisibility(View.VISIBLE);
                        mTvwTitle.setText(feedContent.getItemName());
                    } else {
                        mTvwTitle.setVisibility(View.GONE);
                    }
                    if (!TextUtils.isEmpty(feedContent.getDescription())) {
                        mTvwDesc.setVisibility(View.VISIBLE);
                        mTvwDesc.setText(feedContent.getDescription());
                    } else {
                        mTvwDesc.setVisibility(View.GONE);
                    }
                    if (!TextUtils.isEmpty(feedContent.getSite())) {
                        mTvwSite.setVisibility(View.VISIBLE);
                        mTvwSite.setText(feedContent.getSite());
                    }
                    final String previewUrl = feedContent.getUrl();
                    viewPreviewUrl.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mMessageInteractionListener != null)
                                mMessageInteractionListener.onClickPreviewUrl(message, previewUrl);
                        }
                    });
                }
            }
        }
    }

    private void drawContentMessage() {
        if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            String smsNote = message.getFileName();
            if (isDifferSenderWithAfter() && smsNote != null && smsNote.length() > 0) {
                mTvwStatus.setVisibility(View.VISIBLE);
                mTvwStatus.setText(message.getFileName());
            } else {
                mTvwStatus.setVisibility(View.GONE);
            }
        } else {
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        }
    }
}
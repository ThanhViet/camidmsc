/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.holder.content;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentTiin;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

import androidx.annotation.Nullable;
import butterknife.BindView;

public class TiinDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.tv_total_view)
    @Nullable
    TextView tvTotalView;
    @BindView(R.id.tv_datetime)
    @Nullable
    TextView tvDatetime;
    @BindView(R.id.button_option)
    @Nullable
    View btnOption;

    private Object data;

    public TiinDetailHolder(View view, Activity activity, final OnClickContentTiin listener) {
        super(view);
        initListener(listener);
    }

    public TiinDetailHolder(View view, Activity activity, final OnClickContentTiin listener, int widthLayout) {
        super(view);
        initListener(listener);
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    private void initListener(final OnClickContentTiin listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickTiinItem(data, getAdapterPosition());
                }
            }
        });
        if (btnOption != null) btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreTiinItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void bindDataHome(Content content) {
        if (tvTitle != null) tvTitle.setText(content.getName());
        if (TextUtils.isEmpty(content.getDescription())) {
            if (tvDesc != null) tvDesc.setVisibility(View.GONE);
        } else {
            if (tvDesc != null) {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(content.getDescription());
            }
        }
        if (tvTotalView != null) tvTotalView.setVisibility(View.GONE);
        if (tvDatetime != null) tvDatetime.setVisibility(View.GONE);
        ImageBusiness.setNews(content.getImage(), ivCover);
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        } else if (item instanceof TiinModel) {
            TiinModel model = (TiinModel) item;
            if (tvTitle != null) tvTitle.setText(model.getTitle());
            if (TextUtils.isEmpty(model.getShapo())) {
                if (tvDesc != null) tvDesc.setVisibility(View.GONE);
            } else {
                if (tvDesc != null) {
                    tvDesc.setVisibility(View.VISIBLE);
                    tvDesc.setText(model.getShapo());
                }
            }
            if (tvTotalView != null) tvTotalView.setVisibility(View.GONE);
            if (tvDatetime != null) tvDatetime.setVisibility(View.GONE);
            ImageBusiness.setImageNew219(model.getImage(), ivCover);
        }
    }
}
package com.metfone.selfcare.holder.message;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.imageview.SelectableRoundedImageView;

/**
 * Created by thaodv on 7/18/2014.
 */
public class SendShareLocationHolder extends BaseMessageHolder {
    private TextView mTvwContent;
    private ReengMessage message;
    private SelectableRoundedImageView mImgThumbnail;

    public SendShareLocationHolder(ApplicationController applicationController, MessageInteractionListener listener) {
        setContext(applicationController);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_share_location_send, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mImgThumbnail = (SelectableRoundedImageView) rowView.findViewById(R.id.message_share_location_thumbnail);
        mImgThumbnail.setCornerRadiiDP(0,0,0,0);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwContent.setText(message.getContent());
        ImageLoaderManager.getInstance(mContext).displayThumbnailLocation(mImgThumbnail
        );
    }
}

package com.metfone.selfcare.holder.message;

import android.content.Context;
import androidx.core.content.ContextCompat;

import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 8/29/2016.
 */
public class ReceivedCallHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedCallHolder.class.getSimpleName();

    private ReengMessage message;
    private TextView mTvwContent, mTvwCallBack, mTvwDuration;
    private ImageView mImgCallState;
    private View mViewContent;
    private TextView mTvDate;
    private ImageView mImgState,imgDot;
    //, mCallBackUnderline;
    boolean disableCallButton = false;
    private ApplicationController mApp;

    public ReceivedCallHolder(ApplicationController ctx, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        mApp = ctx;
        setContext(ctx);
    }

    public ReceivedCallHolder(Context ctx) {
        setContext(ctx);
    }

    public void setDisableCallButton(boolean disable) {
        disableCallButton = disable;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_call_received, parent, false);
        initBaseHolder(rowView);
        mViewContent = rowView.findViewById(R.id.message_received_border_bgr);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mImgCallState = (ImageView) rowView.findViewById(R.id.message_call_state);
        mTvwCallBack = (TextView) rowView.findViewById(R.id.message_call_back);
        mTvwDuration = (TextView) rowView.findViewById(R.id.tvw_message_call_duration);
        mTvDate = rowView.findViewById(R.id.tv_date);
        mImgState = rowView.findViewById(R.id.img_state);
        imgDot = rowView.findViewById(R.id.imgDot);
        mTvwCallBack.setVisibility(disableCallButton ? View.GONE : View.VISIBLE);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setContext(Context ctx) {
        mContext = ctx;
        mApp = (ApplicationController) mContext.getApplicationContext();
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        drawContentMessage();
        mViewContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                getMessageInteractionListener().longClickBgrCallback(message);
                return true;
            }
        });
        mViewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onCall(message);
            }
        });
    }

    private void drawContentMessage() {
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants
                .FONT_SIZE.LEVEL_2_5));
        mTvwCallBack.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants
                .FONT_SIZE.LEVEL_2));
        mTvwDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants
                .FONT_SIZE.LEVEL_2));
        String stateStr;
        if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
            mImgCallState.setImageResource(R.drawable.ic_bubble_callout);
            stateStr = mContext.getResources().getString(R.string.call_state_mocha_video); // receive no have callout :)))
            mImgState.setImageResource(R.drawable.ic_someone_call_to_you);
            mTvDate.setTextColor(mContext.getResources().getColor(R.color.black));
            mTvDate.setAlpha((float) 0.5);
            imgDot.setVisibility(View.VISIBLE);
        } else if (message.getChatMode() == ReengMessageConstant.MODE_VIDEO_CALL) {
            mImgCallState.setImageResource(R.drawable.ic_bubble_callvideo);
            stateStr = mContext.getResources().getString(R.string.call_state_mocha_video);
            mTvDate.setTextColor(mContext.getResources().getColor(R.color.black));
            mTvDate.setAlpha((float) 0.5);
            mImgState.setImageResource(R.drawable.ic_someone_call_to_you);
            imgDot.setVisibility(View.VISIBLE);
        } else {
            mImgCallState.setImageResource(R.drawable.ic_bubble_calldata);
            stateStr = message.getSender() +" "+ mContext.getString(R.string.called_you);
            mImgState.setImageResource(R.drawable.ic_someone_call_to_you);
            imgDot.setVisibility(View.VISIBLE);
        }
        mTvDate.setText(message.getDate());
        // content color
        if (message.getSongId() == CallHistoryConstant.STATE_MISS) {
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.chat_main_red_color));
            mTvwContent.setText(mContext.getResources().getString(R.string.callee_call_state_miss));
            imgDot.setVisibility(View.GONE);
            mImgState.setImageResource(R.drawable.call_missed);
            mTvDate.setTextColor(mContext.getResources().getColor(R.color.chat_main_red_color));
            mTvDate.setAlpha(1);
            mTvwCallBack.setTextColor(mContext.getResources().getColor(R.color.black));
            mTvwCallBack.setAlpha((float) 0.5);
            mTvwDuration.setVisibility(View.GONE);
        } else {
            if (message.getSongId() == CallHistoryConstant.STATE_IN_COMING) {
                mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                mTvwContent.setText(stateStr);
                mTvwDuration.setVisibility(View.VISIBLE);
                mTvwDuration.setText(message.getContent());
            } else if (message.getSongId() == CallHistoryConstant.STATE_BUSY) {
                mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                mTvwContent.setText(mContext.getResources().getString(R.string.call_state_busy)); // receive no have call busy
                mTvwDuration.setVisibility(View.GONE);
            } else if (message.getSongId() == CallHistoryConstant.STATE_CANCELLED) {
                mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                mTvwContent.setText(mContext.getResources().getString(R.string.call_state_miss_call));// receive no have call CANCELLED
                mTvwDuration.setVisibility(View.GONE);
            } else if (message.getSongId() == CallHistoryConstant.STATE_REJECTED) {
                mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                mTvwContent.setText(message.getSender() +" "+ mContext.getString(R.string.called_you));
                imgDot.setVisibility(View.GONE);
                mTvDate.setTextColor(mContext.getResources().getColor(R.color.text_color_bubble_received));
                mTvwCallBack.setTextColor(mContext.getResources().getColor(R.color.chat_main_red_color));
                mTvwDuration.setVisibility(View.GONE);
                mImgState.setImageResource(R.drawable.ic_someone_call_to_you);

            } else {
                mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                Log.e(TAG, "sai type: " + message.getSongId());
                mTvwContent.setText(stateStr);
                mTvwDuration.setVisibility(View.GONE);
            }
        }
        if (message.getMessageType() == ReengMessageConstant.MessageType.talk_stranger) {
            //mTvwCallBack.setText(mContext.getResources().getString(R.string.stranger_confide_call_back));
            mTvwCallBack.setVisibility(View.GONE);
        } else {
            StrangerPhoneNumber strangerPhoneNumber = mApp.getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(message.getSender());
            if (strangerPhoneNumber != null)
                mTvwCallBack.setVisibility(View.GONE);
            else {
                mTvwCallBack.setVisibility(disableCallButton ? View.GONE : View.VISIBLE);
                mTvwCallBack.setText(mContext.getResources().getString(R.string.holder_call_back));
            }
        }
    }
//        if(message.getSongId() != CallHistoryConstant.STATE_IN_COMING){
//            if (message.getSenderName() != null) {
//                mTvwContent.setText(message.getSenderName() +" "+ mContext.getString(R.string.called_you));
//            } else {
//                mTvwContent.setText(message.getSender() + " " + mContext.getString(R.string.called_you));
//            }
//            mTvwDuration.setText(message.getContent()+"");
//        }else {
//            mTvwContent.setText(mContext.getString(R.string.you_missed_the_phone_call));
//            mTvwContent.setTextColor(mContext.getResources().getColor(R.color.chat_main_red_color));
//            mTvDate.setTextColor(mContext.getResources().getColor(R.color.chat_main_red_color));
//            mTvDate.setAlpha(1);
//            mTvwDuration.setVisibility(View.GONE);
//            mTvwCallBack.setTextColor(mContext.getResources().getColor(R.color.black));
//            mTvwCallBack.setAlpha((float) 0.5);
//            mImgState.setImageResource(R.drawable.call_missed);
//        }
}
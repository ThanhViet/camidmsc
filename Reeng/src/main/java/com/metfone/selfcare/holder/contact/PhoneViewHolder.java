package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

/**
 * Created by toanvk2 on 9/1/2017.
 */

public class PhoneViewHolder extends BaseViewHolder implements View.OnClickListener {
    private static final String TAG = PhoneViewHolder.class.getSimpleName();
    private ContactsHolderCallBack mCallBack;
    private Context mContext;
    private PhoneNumber entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private RoundedImageView mImgAvatar;
    private TextView mTvContactName;
    private TextView mTvStatus;
    private View mConvertView;
    private TextView mTvwAvatar, mTvwInivte;
    private FrameLayout mFrAvatar;
    private RelativeLayout mRelContent;
    private ImageView mImgCall, mImgVideo, mImgClear;
    private boolean isHideOption = false;
    private boolean isHideCall = false;
    private LinearLayout mLlOption;

    private boolean showCallAVNO = false;

    /*public PhoneViewHolder(View convertView, Context context, ContactsHolderCallBack callBack) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        this.isHideOption = false;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        initView(convertView);
    }*/

    public PhoneViewHolder(View convertView, Context context, ContactsHolderCallBack callBack, boolean isHideOption, boolean isHideCall) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        this.isHideOption = isHideOption;
        this.isHideCall = isHideCall;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        initView(convertView);
    }

    public void setShowCallAVNO(boolean showCallAVNO) {
        this.showCallAVNO = showCallAVNO;
    }

    @Override
    public void setElement(Object obj) {
        entry = (PhoneNumber) obj;
        if (entry == null) {
            return;
        }
        setPhoneView(entry);
        setClickListener();
    }

    private void initView(View convertView) {
        mConvertView = convertView;
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
        mTvStatus = convertView.findViewById(R.id.item_contact_view_status_text);
        mFrAvatar = convertView.findViewById(R.id.item_contact_view_avatar_frame);
        mRelContent = convertView.findViewById(R.id.item_contact_view_content_layout);
        mImgCall = convertView.findViewById(R.id.holder_contact_action_call);
        mImgVideo = convertView.findViewById(R.id.holder_contact_action_video);
        mImgClear = convertView.findViewById(R.id.holder_contact_action_cancel);
        mTvwInivte = convertView.findViewById(R.id.holder_contact_action_label);
        mLlOption = convertView.findViewById(R.id.item_contact_view_option_layout);
    }

    @Override
    public void onClick(View view) {
        if (mCallBack != null && entry != null) {
            switch (view.getId()) {
                case R.id.item_contact_view_avatar_frame:
                    mCallBack.onAvatarClick(entry);
                    break;
                case R.id.holder_contact_action_call:
                    mCallBack.onAudioCall(entry);
                    break;
                case R.id.holder_contact_action_video:
                    mCallBack.onVideoCall(entry);
                    break;
                case R.id.holder_contact_action_label:
                    mCallBack.onActionLabel(entry);
                    break;
              /*  case R.id.item_contact_view_content_layout:
                    mCallBack.onItemClick(entry);
                    break;*/
            }
        }
    }

    //set name, avatar,reeng state
    private void setDetailView(PhoneNumber entry) {
        boolean isUserViettel = false;
        if (mApplication.getReengAccountBusiness().getCurrentAccount() != null) {
            isUserViettel = mApplication.getReengAccountBusiness().isViettel();
        }
        // text
        mLlOption.setVisibility(View.VISIBLE);
        mTvContactName.setText(entry.getName());
        if ("-1".equals(entry.getContactId())) {
            mTvStatus.setVisibility(View.GONE);
            mImgCall.setVisibility(View.GONE);
            mTvwInivte.setVisibility(View.GONE);
            mImgVideo.setVisibility(View.GONE);
        } else {
            mTvStatus.setVisibility(View.GONE);
            mTvStatus.setText(entry.getJidNumber());
            if (showCallAVNO) {
                mImgCall.setVisibility(isHideCall == true ? View.GONE : View.VISIBLE);
                mImgCall.setImageResource(R.drawable.ic_bottom_callsub);
                mTvwInivte.setVisibility(View.GONE);
                mImgVideo.setVisibility(View.GONE);
            } else if (isHideOption) {
                mImgCall.setVisibility(View.GONE);
                mTvwInivte.setVisibility(View.GONE);
                mImgVideo.setVisibility(View.GONE);
            } else if (entry.isReeng()) {
                mImgCall.setVisibility(isHideCall == true ? View.GONE : View.VISIBLE);
                mTvwInivte.setVisibility(View.GONE);
                mImgVideo.setVisibility(isHideCall == true ? View.GONE : View.VISIBLE);
            } else if (isUserViettel && entry.isViettel()) {
                mImgCall.setVisibility(isHideCall == true ? View.GONE : View.VISIBLE);
                mTvwInivte.setVisibility(View.GONE);
                mImgVideo.setVisibility(View.GONE);
            } else {
                mTvwInivte.setVisibility(View.VISIBLE);
                mTvwInivte.setText(mApplication.getCurrentActivity().getResources().getString(R.string.contact_view_invite));
                mImgCall.setVisibility(View.GONE);
                mImgVideo.setVisibility(View.GONE);
            }
        }
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, entry, size);
    }

    private void setPhoneView(PhoneNumber entry) {
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.VISIBLE);
        mImgClear.setVisibility(View.GONE);
        // draw
        /*String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String friendJid = entry.getJidNumber();
        if (getType() == Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP && myNumber != null && myNumber.equals
                (friendJid)) {
            setMyView();
        } else {*/
        setDetailView(entry);
        //}
    }

    private void setClickListener() {
        mImgCall.setOnClickListener(this);
        mImgVideo.setOnClickListener(this);
        mTvwInivte.setOnClickListener(this);
        mFrAvatar.setOnClickListener(this);
        //mConvertView.setOnClickListener(this);
        //mRelContent.setOnClickListener(this);
        mConvertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    mCallBack.onItemClick(entry);
                }
            }
        });
        mConvertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mCallBack != null) {
                    mCallBack.onItemLongClick(entry);
                    return true;
                }
                return false;
            }
        });
    }
}

package com.metfone.selfcare.holder;

import android.content.res.Resources;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.image.ImageBusinessLoader;
import com.metfone.selfcare.database.model.message.MediaBoxItem;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;

/**
 * Created by thanhnt72 on 4/1/2019.
 */

public class MediaBoxHolder extends BaseViewHolder {
    private CircleImageView mImgThreadAvatar;
    private View rlAvatarGroup;
    //private ImageView   mImgStateStranger;
    //private LinearLayout mLlStatusImage;
    private ImageView mImgSong, mImgStatus, mImgWatchVideo;
    private TextView mTvwThreadName, mTvwLastTime, mTvwUnreadMsg, mTvwAvatar, mTvwThreadType;
    private EllipsisTextView mTvwLastContent;
    private ImageView mCbxDelete, mIvIconPin;
    private View mCheckboxLayout;

    private View viewFirstItem, viewLastItem;
    private ImageView ivSetting;
    private TextView tvTitle;

    private ApplicationController mApp;
    private BaseSlidingFragmentActivity activity;
    private Resources res;

    public MediaBoxHolder(View rowView, BaseSlidingFragmentActivity activity) {
        super(rowView);
        this.activity = activity;
        this.mApp = (ApplicationController) activity.getApplication();
        res = activity.getResources();
        mImgThreadAvatar = rowView.findViewById(R.id.thread_avatar);
        rlAvatarGroup = rowView.findViewById(R.id.rlAvatarGroup);
        rlAvatarGroup.setVisibility(View.GONE);
        mImgStatus = rowView.findViewById(R.id.thread_last_status);
        mTvwThreadType = rowView.findViewById(R.id.thread_holder_type);
        mTvwAvatar = rowView.findViewById(R.id.contact_avatar_text);
        mTvwThreadName = rowView.findViewById(R.id.thread_name);
        mTvwUnreadMsg = rowView.findViewById(R.id.thread_number_unread);
        mImgSong = rowView.findViewById(R.id.thread_song_ic);
        mTvwLastContent = rowView.findViewById(R.id.thread_last_content);
        mTvwLastTime = rowView.findViewById(R.id.thread_last_time);
        mCbxDelete = rowView.findViewById(R.id.holder_checkbox);
        mCheckboxLayout = rowView.findViewById(R.id.holder_checkbox_layout);
        mImgWatchVideo = rowView.findViewById(R.id.thread_watch_video);
        mIvIconPin = rowView.findViewById(R.id.ivIconPin);

        viewFirstItem = rowView.findViewById(R.id.viewFirstItem);
        viewLastItem = rowView.findViewById(R.id.viewLastItem);
        ivSetting = rowView.findViewById(R.id.ivSetting);

        tvTitle = rowView.findViewById(R.id.tvTitleMediaBox);
    }

    @Override
    public void setElement(Object obj) {

        final MediaBoxItem mediaBoxItem = (MediaBoxItem) obj;
        mTvwLastContent.setText(mediaBoxItem.getDesc());
        mTvwThreadName.setText(mediaBoxItem.getTitle());

        if (mediaBoxItem.isPin()) {
            mTvwLastTime.setText(activity.getString(R.string.featured));
        } else
            mTvwLastTime.setText(TimeHelper.formatCommonTime(mediaBoxItem.getTime(), System.currentTimeMillis(), res));
        mTvwThreadType.setText(mediaBoxItem.getTitleType());
        mTvwThreadType.setTextColor(ContextCompat.getColor(mApp, R.color.bg_mocha));

        setViewFirstLast(mediaBoxItem);

        if (mediaBoxItem.isNewItem()) {
            mTvwThreadName.setTypeface(null, Typeface.BOLD);
            mTvwUnreadMsg.setVisibility(View.VISIBLE);
            mTvwUnreadMsg.setText("N");
        } else {
            mTvwUnreadMsg.setVisibility(View.GONE);
            mTvwThreadName.setTypeface(null, Typeface.NORMAL);
        }

        ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DeepLinkHelper.getInstance().openSchemaLink(activity, "mocha://mediasetup");
                //mApp.getConfigBusiness().setCloseMediaBoxUntilHasNew(true);
            }
        });

        drawIconMediaBox(mediaBoxItem);

        /*if (!TextUtils.isEmpty(mediaBoxItem.getImg())) {
            Glide.with(mApp).load(mediaBoxItem.getImg()).into(mImgThreadAvatar);
        }*/
    }

    private void drawIconMediaBox(MediaBoxItem mediaBoxItem) {
        String itemType = mediaBoxItem.getType();
        String imageUrl = mediaBoxItem.getImg();
        if (MediaBoxItem.ItemType.MUSIC.equals(itemType)) {
            if (TextUtils.isEmpty(imageUrl))
                Glide.with(mApp).load(R.drawable.ic_func_music).into(mImgThreadAvatar);
            else
                ImageBusinessLoader.loadImageNormal(imageUrl, R.drawable.ic_func_music, R.drawable.ic_func_music, mImgThreadAvatar);
        } else if (MediaBoxItem.ItemType.MOVIE.equals(itemType)) {
            if (TextUtils.isEmpty(imageUrl))
                Glide.with(mApp).load(R.drawable.ic_func_movie).into(mImgThreadAvatar);
            else
                ImageBusinessLoader.loadImageNormal(imageUrl, R.drawable.ic_func_movie, R.drawable.ic_func_movie, mImgThreadAvatar);
        } else if (MediaBoxItem.ItemType.NEWS.equals(itemType)) {
            if (TextUtils.isEmpty(imageUrl))
                Glide.with(mApp).load(R.drawable.ic_func_news).into(mImgThreadAvatar);
            else
                ImageBusinessLoader.loadImageNormal(imageUrl, R.drawable.ic_func_news, R.drawable.ic_func_news, mImgThreadAvatar);
        } else if (MediaBoxItem.ItemType.VIDEO.equals(itemType)) {
            if (TextUtils.isEmpty(imageUrl))
                Glide.with(mApp).load(R.drawable.ic_func_video).into(mImgThreadAvatar);
            else
                ImageBusinessLoader.loadImageNormal(imageUrl, R.drawable.ic_func_video, R.drawable.ic_func_video, mImgThreadAvatar);
        } else {
            if (TextUtils.isEmpty(imageUrl))
                Glide.with(mApp).load(R.drawable.ic_mediabox_thumb).into(mImgThreadAvatar);
            else
                ImageBusinessLoader.loadImageNormal(imageUrl, R.drawable.ic_mediabox_thumb, R.drawable.ic_mediabox_thumb, mImgThreadAvatar);
        }
    }

    private void setViewFirstLast(MediaBoxItem mediaBoxItem) {
        if (mediaBoxItem.isFirstItem() && mediaBoxItem.isLastItem()) {
            viewFirstItem.setVisibility(View.VISIBLE);
            viewLastItem.setVisibility(View.VISIBLE);
            tvTitle.setText(getTitleHeader(mediaBoxItem));
        } else if (!mediaBoxItem.isFirstItem() && !mediaBoxItem.isLastItem()) {
            viewLastItem.setVisibility(View.GONE);
            viewFirstItem.setVisibility(View.GONE);
        } else {
            if (mediaBoxItem.isFirstItem()) {
                viewFirstItem.setVisibility(View.VISIBLE);
                tvTitle.setText(getTitleHeader(mediaBoxItem));
                viewLastItem.setVisibility(View.GONE);
            } else if (mediaBoxItem.isLastItem()) {
                viewLastItem.setVisibility(View.VISIBLE);
                viewFirstItem.setVisibility(View.GONE);
            }
        }
    }

    private String getTitleHeader(MediaBoxItem item) {
        String titleHeader = item.getTitleHeader();
        if (TextUtils.isEmpty(titleHeader)) return res.getString(R.string.title_media_box);
        return titleHeader;
    }
}

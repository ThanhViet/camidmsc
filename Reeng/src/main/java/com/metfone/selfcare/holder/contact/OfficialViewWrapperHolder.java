package com.metfone.selfcare.holder.contact;

import android.content.Context;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.metfone.selfcare.adapter.home.HomeOfficialAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MochaFriendAccountWrapper;
import com.metfone.selfcare.database.model.OfficerAccountWrapper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;

import java.util.ArrayList;

public class OfficialViewWrapperHolder extends BaseViewHolder {
    private Context mContext;
    private ContactsHolderCallBack mCallBack;
    private OfficerAccountWrapper entry;

    private HomeOfficialAdapter homeOfficialAdapter;

    public OfficialViewWrapperHolder(View convertView, Context context, ContactsHolderCallBack callBack) {
        super(convertView);
        this.mContext = context;
        this.mCallBack = callBack;
        initView(convertView);
    }

    @Override
    public void setElement(Object obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof OfficerAccountWrapper) {
            entry = (OfficerAccountWrapper) obj;
            setDetailView();
        }
        if (obj instanceof MochaFriendAccountWrapper) {
            setMochaFriendDetailView((MochaFriendAccountWrapper) obj);
        }
    }

    private void initView(View convertView) {
        RecyclerView recyclerView = convertView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayout.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);

        homeOfficialAdapter = new HomeOfficialAdapter(mContext, new ArrayList<>(), mCallBack);
        recyclerView.setAdapter(homeOfficialAdapter);
    }

    private void setDetailView() {
        homeOfficialAdapter.setData(new ArrayList<>(entry.getListOfficerAccounts()));
        homeOfficialAdapter.notifyDataSetChanged();
    }

    private void setMochaFriendDetailView(MochaFriendAccountWrapper mochaFriendAccountWrapper) {
        homeOfficialAdapter.setData(new ArrayList<>(mochaFriendAccountWrapper.getListMochaAccounts()));
        homeOfficialAdapter.notifyDataSetChanged();
    }
}

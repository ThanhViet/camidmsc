package com.metfone.selfcare.holder.onmedia;

import android.content.res.Resources;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Space;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.OnMediaCommentHolderListener;
import com.metfone.selfcare.ui.TagTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by thanhnt72 on 9/20/2017.
 */
public class OnMediaCommentHolder extends BaseViewHolder {

    private static final String TAG = OnMediaCommentHolder.class.getSimpleName();

    private ApplicationController mApp;
    private ImageView mFeedsAvatar;
    private TextView mTvwAvatar;
    private TextView mUsername;
    private TextView mUserTime;
    private TagTextView mContent;
    private View mLayoutAvatar;
    private Space mSpaceReply;
    private View mLayoutLikeCmt;
    private View mLayoutCommentCmt;
    private AppCompatImageView mImgLikeCmt;
    private AppCompatImageView mImgCommentCmt;
    private TextView mTvwNumbLikeCmt;
    private TextView mTvwNumbCommentCmt;
    private TextView mTvwReply;
    private RoundTextView mTvwNumberReply;
    private AppCompatImageView ivMore;
//    private TextView mTvwDot;

    private ReengAccount account;
    private ContactBusiness mContactBusiness;
    private AvatarBusiness mAvatarBusiness;
    private FeedBusiness mFeedBusiness;
    private Resources mRes;

    private String myName;
    private String myNumber;
    private String mFriendJid, mFriendName;
    //    private boolean isMyUser;
    private PhoneNumber mFriendPhone;
    private OnMediaCommentHolderListener onMediaCmtHolderListener;
    private TagMocha.OnClickTag onClickTag;
    private int currentPos;
    private boolean isReplyFragment;

    public OnMediaCommentHolder(View convertView, ApplicationController mApp) {
        super(convertView);
        this.mApp = mApp;
        mRes = mApp.getResources();
        mAvatarBusiness = mApp.getAvatarBusiness();
        mContactBusiness = mApp.getContactBusiness();
        mFeedBusiness = mApp.getFeedBusiness();
        account = mApp.getReengAccountBusiness().getCurrentAccount();
        myName = account.getName();
        myNumber = account.getJidNumber();

        mUsername = convertView.findViewById(R.id.tvw_comment_name);
        mUserTime = convertView.findViewById(R.id.tvw_comment_time);
        mFeedsAvatar = convertView.findViewById(R.id.img_comment_user);
        mLayoutAvatar = convertView.findViewById(R.id.layout_avatar_comment);
        mTvwAvatar = convertView.findViewById(R.id.tvw_comment_avatar);
        mContent = convertView.findViewById(R.id.tvw_comment_content);
        mContent.setMaxLines(10);

        mSpaceReply = convertView.findViewById(R.id.space_reply);
        mLayoutLikeCmt = convertView.findViewById(R.id.layout_like_cmt);
        mLayoutCommentCmt = convertView.findViewById(R.id.layout_comment_cmt);
        mImgLikeCmt = convertView.findViewById(R.id.img_like_cmt);
        mImgCommentCmt = convertView.findViewById(R.id.img_comment_cmt);
        mTvwNumbLikeCmt = convertView.findViewById(R.id.tvw_number_like_cmt);
        mTvwNumbCommentCmt = convertView.findViewById(R.id.tvw_number_comment_cmt);
        mTvwReply = convertView.findViewById(R.id.tvw_reply);
        mTvwNumberReply = convertView.findViewById(R.id.tvw_number_reply);
        ivMore = convertView.findViewById(R.id.ivMore);
//        mTvwDot = convertView.findViewById(R.id.tvw_dot);
    }

    @Override
    public void setElement(Object obj) {
        FeedAction comment = (FeedAction) obj;
        Log.i(TAG, "comment; " + comment.toString());
        setViewHolderContent(comment);
    }

    public void setCurrentPos(int currentPos) {
        this.currentPos = currentPos;
    }

    public void setReplyFragment(boolean replyFragment) {
        isReplyFragment = replyFragment;
    }

    private void setViewHolderContent(final FeedAction item) {
        final UserInfo userInfo = item.getUserInfo();
        if (userInfo == null) return;
        int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mFriendJid = userInfo.getMsisdn();
        if (userInfo.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL) {
            mUsername.setCompoundDrawables(null, null, null, null);
            if (mFriendJid.equals(myNumber)) {
                mFriendName = myName;
                mAvatarBusiness.setMyAvatar(mFeedsAvatar, mTvwAvatar, mTvwAvatar, account, null);
            } else {
                mFriendPhone = mContactBusiness.getPhoneNumberFromNumber(mFriendJid);
                if (mFriendPhone != null) {// luu danh ba
                    mFriendName = mFriendPhone.getName();
                    mAvatarBusiness.setPhoneNumberAvatar(mFeedsAvatar, mTvwAvatar, mFriendPhone, sizeAvatar);
                } else {
                    if (TextUtils.isEmpty(userInfo.getName())) {
                        mFriendName = Utilities.hidenPhoneNumber(mFriendJid);
                    } else {
                        mFriendName = userInfo.getName();
                    }
                    String mFriendAvatarUrl;
                    if (userInfo.isUserMocha()) {
                        if ("0".equals(userInfo.getAvatar())) {
                            mFriendAvatarUrl = "";
                        } else {
                            mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo.getMsisdn
                                    (), sizeAvatar);
                        }
                    } else {
                        mFriendAvatarUrl = userInfo.getAvatar();
                    }
                    mAvatarBusiness.setAvatarOnMedia(mFeedsAvatar, mTvwAvatar,
                            mFriendAvatarUrl, userInfo.getMsisdn(), mFriendName, sizeAvatar);
                }
            }
        } else {
            mFriendName = userInfo.getName();
            mTvwAvatar.setVisibility(View.GONE);
            ImageLoaderManager.getInstance(mApp).setAvatarOfficialOnMedia(mFeedsAvatar, userInfo.getAvatar(),
                    sizeAvatar);
            if (userInfo.getUser_type() == UserInfo.USER_ONMEDIA_VERIFY) {
                mUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checkbox_selected, 0);
            } else {
                mUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
        mUsername.setText(mFriendName);
        String content = item.getComment();
        if (TextUtils.isEmpty(content)) {
            content = mRes.getString(R.string.connections_share_uppercase);
            mContent.setText(content);
        } else {
            if (item.getListTag() == null || item.getListTag().isEmpty()) {
                mContent.setEmoticon(mApp, content, content.hashCode(), content);
            } else {
                mContent.setEmoticonWithTag(mApp, content, content.hashCode(), content,
                        item.getListTag(), onClickTag);
            }
        }
        if (!item.isReplyCmt()) {
            mSpaceReply.setVisibility(View.GONE);
            if (item.getNumberCmt() > 1 && !isReplyFragment) {
                mTvwNumberReply.setVisibility(View.VISIBLE);
                String textReply;
                if (item.getNumberCmt() == 2)
                    textReply = mRes.getString(R.string.view_one_reply);
                else
                    textReply = String.format(mRes.getString(R.string.view_more_reply), item.getNumberCmt() - 1);
                mTvwNumberReply.setText(textReply);
            } else {
                mTvwNumberReply.setVisibility(View.GONE);
            }


            if (TextUtils.isEmpty(item.getIdCmtLocal())) {
                mUserTime.setText(TimeHelper.caculateTimeFeed(mApp, item.getTimeStamp(),
                        mFeedBusiness.getDeltaTimeServer()));
                mLayoutLikeCmt.setVisibility(View.VISIBLE);
                mLayoutCommentCmt.setVisibility(View.VISIBLE);
//                mTvwDot.setVisibility(View.VISIBLE);
                mTvwReply.setVisibility(View.VISIBLE);
                ivMore.setVisibility(View.VISIBLE);
            } else {
                mUserTime.setText(mRes.getString(R.string.om_posting_cmt));
//                mTvwDot.setVisibility(View.GONE);
                mTvwReply.setVisibility(View.GONE);
                mLayoutLikeCmt.setVisibility(View.GONE);
                mLayoutCommentCmt.setVisibility(View.GONE);
                ivMore.setVisibility(View.GONE);
            }
        } else {
            mSpaceReply.setVisibility(View.VISIBLE);
            mTvwNumberReply.setVisibility(View.GONE);

            mUserTime.setText(TimeHelper.caculateTimeFeed(mApp, item.getTimeStamp(),
                    mFeedBusiness.getDeltaTimeServer()));

            if (TextUtils.isEmpty(item.getIdCmtLocal())) {
                if (isReplyFragment) {
                    mLayoutLikeCmt.setVisibility(View.VISIBLE);
                    mLayoutCommentCmt.setVisibility(View.VISIBLE);
//                    mTvwDot.setVisibility(View.VISIBLE);
                    mTvwReply.setVisibility(View.VISIBLE);
                    ivMore.setVisibility(View.VISIBLE);
                } else {
                    mLayoutLikeCmt.setVisibility(View.GONE);
                    mLayoutCommentCmt.setVisibility(View.GONE);
//                    mTvwDot.setVisibility(View.GONE);
                    mTvwReply.setVisibility(View.GONE);
                    ivMore.setVisibility(View.GONE);
                }
            } else {
                mUserTime.setText(mRes.getString(R.string.om_posting_cmt));
//                mTvwDot.setVisibility(View.GONE);
                mTvwReply.setVisibility(View.GONE);
                mLayoutLikeCmt.setVisibility(View.GONE);
                mLayoutCommentCmt.setVisibility(View.GONE);
                ivMore.setVisibility(View.GONE);
            }
        }

        if (isReplyFragment)
            ivMore.setVisibility(View.GONE);
        if (item.getIsLike() == 1) {
            mImgLikeCmt.setImageResource(R.drawable.ic_v5_like_active);
//            mImgLikeCmt.setImageResource(R.drawable.ic_v5_heart_active);
        } else {
            mImgLikeCmt.setImageResource(R.drawable.ic_v5_like);
//            mImgLikeCmt.setImageResource(R.drawable.ic_v5_heart_normal);
        }

        if (item.getNumberLike() > 0) {
            mTvwNumbLikeCmt.setVisibility(View.VISIBLE);
            mTvwNumbLikeCmt.setText(String.valueOf(item.getNumberLike()));
        } else
            mTvwNumbLikeCmt.setVisibility(View.INVISIBLE);

        if (item.getNumberCmt() > 0) {
            mTvwNumbCommentCmt.setVisibility(View.VISIBLE);
            mTvwNumbCommentCmt.setText(String.valueOf(item.getNumberCmt()));
        } else
            mTvwNumbCommentCmt.setVisibility(View.INVISIBLE);

        if (item.isReplyCmt()) {
            mLayoutCommentCmt.setVisibility(View.GONE);
        } else
            mLayoutCommentCmt.setVisibility(View.VISIBLE);


        mSpaceReply.setOnClickListener(view -> {
            if (!isReplyFragment) {
                onMediaCmtHolderListener.onClickReply(item, currentPos, false);
            }
        });

        mContent.setOnClickListener(view -> {
            if (!isReplyFragment && item.isReplyCmt()) {
                onMediaCmtHolderListener.onClickReply(item, currentPos, false);
            }
        });


        mLayoutAvatar.setOnClickListener(view -> {
            if (item.isReplyCmt() && !isReplyFragment) {
                onMediaCmtHolderListener.onClickReply(item, currentPos, false);
            } else {
                onMediaCmtHolderListener.onAvatarClick(userInfo);
            }
        });

        mUsername.setOnClickListener(view -> {
            if (item.isReplyCmt() && !isReplyFragment) {
                onMediaCmtHolderListener.onClickReply(item, currentPos, false);
            } else {
                onMediaCmtHolderListener.onAvatarClick(userInfo);
            }
        });

        mLayoutAvatar.setOnLongClickListener(v -> {
            if (!item.isReplyCmt()) {
                if (onMediaCmtHolderListener != null) {
                    onMediaCmtHolderListener.onLongClickItem(item);
                }
            }
            return true;
        });

        mContent.setOnLongClickListener(v -> {
            if (!item.isReplyCmt()) {
                if (onMediaCmtHolderListener != null) {
                    onMediaCmtHolderListener.onLongClickItem(item);
                }
            }
            return true;
        });

        mUsername.setOnLongClickListener(v -> {
            if (!item.isReplyCmt()) {
                if (onMediaCmtHolderListener != null) {
                    onMediaCmtHolderListener.onLongClickItem(item);
                }
            }
            return true;
        });

        mUserTime.setOnLongClickListener(v -> {
            if (!item.isReplyCmt()) {
                if (onMediaCmtHolderListener != null) {
                    onMediaCmtHolderListener.onLongClickItem(item);
                }
            }
            return true;
        });

        ivMore.setOnClickListener(v -> {
            if (!item.isReplyCmt()) {
                if (onMediaCmtHolderListener != null) {
                    onMediaCmtHolderListener.onLongClickItem(item);
                }
            }
        });

        mImgLikeCmt.setOnClickListener(view -> {
            if (onMediaCmtHolderListener != null) {
                if (item.getIsLike() == 0) {
                    item.setIsLike(1);
                    item.setNumberLike(item.getNumberLike() + 1);
                } else {
                    item.setIsLike(0);
                    item.setNumberLike(item.getNumberLike() - 1);
                }
                onMediaCmtHolderListener.onClickLike(item, currentPos);
            }
        });

        mTvwNumbLikeCmt.setOnClickListener(view -> {
            if (onMediaCmtHolderListener != null)
                onMediaCmtHolderListener.onClickListLike(item);
        });

        mTvwNumbCommentCmt.setOnClickListener(view -> {
            if (onMediaCmtHolderListener != null)
                onMediaCmtHolderListener.onClickReply(item, currentPos, false);
        });

        mTvwReply.setOnClickListener(view -> {
            if (onMediaCmtHolderListener != null)
                onMediaCmtHolderListener.onClickReply(item, currentPos, true);
        });

        mImgCommentCmt.setOnClickListener(view -> {
            if (onMediaCmtHolderListener != null)
                onMediaCmtHolderListener.onClickReply(item, currentPos, true);
        });

        mTvwNumberReply.setOnClickListener(view -> {
            if (onMediaCmtHolderListener != null)
                onMediaCmtHolderListener.onClickReply(item, currentPos, false);
        });
    }

    public void setOnMediaCommentHolderListener(OnMediaCommentHolderListener onMediaCmtHolderListener) {
        this.onMediaCmtHolderListener = onMediaCmtHolderListener;
    }

    public void setOnClickTag(TagMocha.OnClickTag onClickTag) {
        this.onClickTag = onClickTag;
    }
}
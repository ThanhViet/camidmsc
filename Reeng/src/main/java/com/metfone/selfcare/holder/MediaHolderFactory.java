package com.metfone.selfcare.holder;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageInfo;
import com.metfone.selfcare.util.Log;

import java.io.File;

/**
 * Created by thaodv on 6/4/2015.
 */
public class MediaHolderFactory {
    private static final String TAG = MediaHolderFactory.class.getSimpleName();

    public static MediaHolder createHolder(Context context, LayoutInflater inflater, ViewGroup container, ImageInfo imageInfo) {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            return new CustomL14MediaHolder(context, inflater, container, imageInfo);
        } else {
            return new CustomLegacyMediaHolder(context, inflater, container, imageInfo);
        }
    }

    public static abstract class MediaHolder {
        public View itemView;
        public ImageView mImgContent;
        //public View mVideoViewContent;
        public ImageInfo mImageInfo;
        public View mBtnPlayVideo;
        public Context mContext;
        public ImageView mBtnSend;
        public TextView mTvwVideoDuration;
        public View maskLayout;

        public abstract void playVideo(MediaPlayer.OnCompletionListener onCompletionListener, ApplicationController application);

        public abstract void pauseVideo();

        public void onItemSelectedChanged() {
            Log.i(TAG, "onItemSelectedChanged");
            if (!mImageInfo.isSelected()) {
                mBtnSend.setVisibility(View.GONE);
                maskLayout.setVisibility(View.GONE);
                //neu la video thi hien thi nut play
                if (mImageInfo.isVideo()) {
                    mBtnPlayVideo.setVisibility(View.VISIBLE);
                }
            } else {
                mBtnSend.setVisibility(View.VISIBLE);
                maskLayout.setVisibility(View.VISIBLE);
                mBtnPlayVideo.setVisibility(View.GONE);
            }
        }

        public void init(Context context, LayoutInflater inflater, ViewGroup container, ImageInfo imageInfo) {
            mContext = context;
            mImgContent = (ImageView) itemView.findViewById(R.id.image_video_content);
            mBtnPlayVideo = itemView.findViewById(R.id.play_video_button);
            mBtnSend = (ImageView) itemView.findViewById(R.id.send_button);
            mTvwVideoDuration = (TextView) itemView.findViewById(R.id.video_duration);
            maskLayout = itemView.findViewById(R.id.mask_layout);
            mImageInfo = imageInfo;
            if (imageInfo.isVideo()) {
                mBtnPlayVideo.setVisibility(View.VISIBLE);
                mTvwVideoDuration.setVisibility(View.VISIBLE);
                mTvwVideoDuration.setText(TimeHelper.getDuraionMediaFile(imageInfo.getDurationInSecond()));
            } else {
                mTvwVideoDuration.setVisibility(View.GONE);
                mBtnPlayVideo.setVisibility(View.GONE);
            }
            onItemSelectedChanged();
        }
    }

    public static class CustomLegacyMediaHolder extends MediaHolder {
        public CustomLegacyMediaHolder(Context context, LayoutInflater inflater, ViewGroup container, ImageInfo imageInfo) {
            itemView = inflater.inflate(R.layout.image_video_pager_lower_14_item, container, false);
            init(context, inflater, container, imageInfo);
        }

        public void playVideo(MediaPlayer.OnCompletionListener onCompletionListener, ApplicationController application) {
            //implement api < 14
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(FileHelper.fromFile(application, new File(mImageInfo.getImagePath())), "video/*");
            mContext.startActivity(intent);
        }

        @Override
        public void pauseVideo() {

        }
    }

    @TargetApi(14)
    public static class CustomL14MediaHolder extends MediaHolder {
        public CustomL14MediaHolder(Context context, LayoutInflater inflater, ViewGroup container, ImageInfo imageInfo) {
            itemView = inflater.inflate(R.layout.image_video_pager_item, container, false);
            init(context, inflater, container, imageInfo);
            //mVideoViewContent = itemView.findViewById(R.id.video_view);
        }

        public void playVideo(MediaPlayer.OnCompletionListener onCompletionListener, ApplicationController application) {
            //implement api >= 14
            //implement api < 14
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(FileHelper.fromFile(application, new File(mImageInfo.getImagePath())), "video/*");
            mContext.startActivity(intent);
        }

        @Override
        public void pauseVideo() {

        }
    }
}
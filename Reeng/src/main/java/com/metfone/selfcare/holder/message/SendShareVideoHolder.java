package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.ProgressWheel;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 8/28/14.
 */
public class SendShareVideoHolder extends BaseMessageHolder {
    private static final String TAG = SendShareVideoHolder.class.getSimpleName();
    private RoundedImageView content;
    private View vLoading;
    private int imageThumbnailWidth;
    private ImageView iconAttachment;
    private TextView fileName;
    private ReengMessage message;
    private ImageView imgPlay;
    private ProgressWheel progressWheel;
    private TextView tvwFileSize;
    private TextView tvwDuration;
    private ProgressLoading progress;

    public SendShareVideoHolder(Context ctx) {
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_share_video_send, parent, false);
        initBaseHolder(rowView);
        content = (RoundedImageView) rowView
                .findViewById(R.id.message_detail_file_item_content);
        vLoading = rowView
                .findViewById(R.id.message_detail_file_loading);
        fileName = (TextView) rowView
                .findViewById(R.id.message_detail_file_item_file_name);
        iconAttachment = (ImageView) rowView
                .findViewById(R.id.message_detail_file_item_attachment);
        imgPlay = (ImageView) rowView.findViewById(R.id.img_play_video);
        progressWheel = (ProgressWheel) rowView.findViewById(R.id.progress_loading);
        progressWheel.setVisibility(View.GONE);
        progress = (ProgressLoading) rowView.findViewById(R.id.progress);
        tvwFileSize = (TextView) rowView.findViewById(R.id.txt_media_size);
        tvwDuration = (TextView) rowView.findViewById(R.id.txt_media_duration);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        // Log.i(TAG, "" + message);
        setBaseElements(obj);
        fileName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        tvwFileSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        tvwDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        Log.d(TAG, TAG + "-" + message.getId() + " " + message.getStatus());
        if (message.getVideoContentUri() == null) {
            ImageLoaderManager.getInstance(getContext()).displayDetailOfMessage(message.getFilePath(), content, true);
        } else {
            Log.i(TAG, "video uri" + message.getVideoContentUri());
            ImageLoaderManager.getInstance(getContext()).displayThumbnailVideo(message.getVideoContentUri(), content);
        }
        content.setVisibility(View.VISIBLE);
        iconAttachment.setVisibility(View.GONE);
        fileName.setVisibility(View.GONE);
        tvwFileSize.setText(String.format(getContext().getString(R.string.file_size_in_mb), message.getSize() / 1024.0 / 1024.0));
        tvwDuration.setText(TimeHelper.getDuraionMediaFile(message.getDuration()));
        Log.d(TAG, message.getId() + " " + message.getStatus());
        if (message.getStatus() == ReengMessageConstant.STATUS_NOT_SEND || message.getStatus() == ReengMessageConstant.STATUS_LOADING) {
            vLoading.setVisibility(View.VISIBLE);
            imgPlay.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
            /*int percentProgress = 360 * message.getProgress() / 100;
            progressWheel.setProgress(percentProgress);*/
        } else {
            vLoading.setVisibility(View.GONE);
            imgPlay.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
        }
    }
}

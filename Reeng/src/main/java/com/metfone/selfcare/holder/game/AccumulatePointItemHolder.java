package com.metfone.selfcare.holder.game;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.adapter.AccumulatePagerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.holder.BaseViewHolder;


/**
 * Created by toanvk2 on 11/30/2016.
 */
public class AccumulatePointItemHolder extends BaseViewHolder implements View.OnClickListener {
    private Context mContext;
    private ApplicationController mApplication;
    private TextView mTvwTitle, mTvwDesc, mBtnAction, tvDateValid, tvBalance, tvPoint, tvUnit;
    private ImageView tvDivider;
    private View llHistory;
    private ImageView mImgIcon;
    private AccumulatePagerAdapter.ItemListener mListener;
    private AccumulatePointItem mEntry;
    private int size;

    public AccumulatePointItemHolder(View convertView, Context context, AccumulatePagerAdapter.ItemListener callBack) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) mContext.getApplicationContext();
        this.mListener = callBack;
        mTvwTitle = convertView.findViewById(R.id.holder_accumulate_label);
        mTvwDesc = convertView.findViewById(R.id.holder_accumulate_desc);
        mBtnAction = convertView.findViewById(R.id.holder_accumulate_button);
        mImgIcon = convertView.findViewById(R.id.img_icon_accumulate);

        tvDivider = convertView.findViewById(R.id.tvDividerText);
        tvBalance = convertView.findViewById(R.id.tvBalance);
        tvDateValid = convertView.findViewById(R.id.tvDate);
        tvPoint = convertView.findViewById(R.id.tvPoint);
        tvUnit = convertView.findViewById(R.id.tvUnit);
        llHistory = convertView.findViewById(R.id.llHistory);
        size = (int) mApplication.getResources().getDimension(R.dimen.size_image_intro_group);
    }

    @Override
    public void setElement(Object obj) {
        mEntry = (AccumulatePointItem) obj;
        mTvwTitle.setText(mEntry.getTitle());
        switch (mEntry.getItemType()) {
            case AccumulatePointHelper.TAB_TASK:
                mTvwDesc.setText(mEntry.getDesc());
                mBtnAction.setText(mEntry.getLabelButton());
                mBtnAction.setVisibility(View.VISIBLE);
                mImgIcon.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(mEntry.getIcon())) {
                    mImgIcon.setImageResource(R.drawable.ic_accumulate_default);
//                    mApplication.getUniversalImageLoader().cancelDisplayTask(mImgIcon);
                    Glide.with(mApplication).clear(mImgIcon);
                } else {
                    Glide.with(mApplication).load(mEntry.getIcon()).apply(new RequestOptions().override(size, size)).into(mImgIcon);
//                    mApplication.getUniversalImageLoader().displayImage(mEntry.getIcon(), new ImageViewAwareTargetSize(mImgIcon, size, size));
                }
                tvDivider.setVisibility(View.GONE);
                tvBalance.setVisibility(View.GONE);
                llHistory.setVisibility(View.GONE);
                break;
            case AccumulatePointHelper.TAB_EXCHANGE:
                llHistory.setVisibility(View.GONE);
                if (mEntry.getGiftType() == AccumulatePointItem.GIFT_TYPE_CASH) {
                    mImgIcon.setVisibility(View.GONE);
                    tvDateValid.setVisibility(View.GONE);
                    mBtnAction.setText(mEntry.getLabelButton());
                    mBtnAction.setVisibility(View.VISIBLE);
                    tvDivider.setVisibility(View.VISIBLE);
                    tvBalance.setVisibility(View.VISIBLE);
                    tvBalance.setText(mEntry.getBalance());
                } else if (mEntry.getGiftType() == AccumulatePointItem.GIFT_TYPE_CARD) {
                    mImgIcon.setVisibility(View.GONE);
                    if (TextUtils.isEmpty(mEntry.getCreatedDate()))
                        tvDateValid.setVisibility(View.GONE);
                    else {
                        tvDateValid.setVisibility(View.VISIBLE);
                        tvDateValid.setText(mEntry.getCreatedDate());
                    }
                    mBtnAction.setVisibility(View.GONE);
                    tvDivider.setVisibility(View.VISIBLE);
                    tvBalance.setVisibility(View.VISIBLE);
                    tvBalance.setText(mEntry.getBalance());
                } else if (mEntry.getGiftType() == AccumulatePointItem.GIFT_TYPE_DEEPLINK) {
                    mImgIcon.setVisibility(View.GONE);
                    tvDateValid.setVisibility(View.GONE);
                    mBtnAction.setVisibility(View.GONE);
                    tvDivider.setVisibility(View.VISIBLE);
                    tvBalance.setVisibility(View.VISIBLE);
                    tvBalance.setText(mEntry.getBalance());
                } else if (mEntry.getGiftType() == AccumulatePointItem.GIFT_TYPE_VTPLUS) {
                    mImgIcon.setVisibility(View.GONE);
                    tvDateValid.setVisibility(View.GONE);
                    mBtnAction.setVisibility(View.GONE);
                    tvDivider.setVisibility(View.VISIBLE);
                    tvBalance.setVisibility(View.VISIBLE);
                    tvBalance.setText(mEntry.getBalance());
                } else {
                    mImgIcon.setVisibility(View.VISIBLE);
                    tvDateValid.setVisibility(View.GONE);
                    mBtnAction.setText(mEntry.getLabelButton());
                    mBtnAction.setVisibility(View.VISIBLE);
                    Glide.with(mContext).load(TextUtils.isEmpty(mEntry.getIcon())
                            ? R.drawable.ic_accumulate_default : mEntry.getIcon()).into(mImgIcon);
                    tvDivider.setVisibility(View.GONE);
                    tvBalance.setVisibility(View.GONE);
                }
                mTvwDesc.setText(mEntry.getDesc());

                break;
            case AccumulatePointHelper.TAB_HISTORY:
                mTvwDesc.setText(mEntry.getCreatedDate());
                mBtnAction.setVisibility(View.GONE);
                mImgIcon.setVisibility(View.GONE);
                llHistory.setVisibility(View.VISIBLE);
                tvDateValid.setVisibility(View.GONE);
                tvPoint.setText(mEntry.getPoint());
                tvUnit.setText(mEntry.getUnit());
                break;
        }
        setListener();
    }

    private void setListener() {
        mBtnAction.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.holder_accumulate_button:
                if (mListener != null) {
                    if (mEntry.getItemType() == AccumulatePointHelper.TAB_TASK) {
                        if (AccumulatePointItem.MISSION_TYPE.equals(mEntry.getMissionType())) {
                            mListener.onInstallAppClick(mEntry);
                        } else
                            mListener.onDeepLinkClick(mEntry);
                    } else if (mEntry.getItemType() == AccumulatePointHelper.TAB_EXCHANGE) {
                        if (mEntry.getItemPoint() == AccumulatePointItem.ITEM_CPOINT)
                            mListener.onExchangeClick(mEntry);
                        else if (mEntry.getItemPoint() == AccumulatePointItem.ITEM_SPOINT)
                            mListener.onExchangeClick(mEntry);
                        else if (mEntry.getItemPoint() == AccumulatePointItem.ITEM_MPOINT)
                            mListener.onDeepLinkClick(mEntry);
                        else if (mEntry.getItemPoint() == AccumulatePointItem.ITEM_VTPLUS) {
                            mListener.onInstallAppClick(mEntry);
                        }
                    }
                }
                break;
        }
    }
}
package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.editor.sticker.utils.Constants;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class TemplatePageHolder extends BaseViewHolder {
    private Context mContext;
    private Page entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private ImageView mImgPreview;
    private TextView mTvwName;
    private int screenWidth;

    public TemplatePageHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mContext.getResources();
        mImgPreview = (ImageView) convertView.findViewById(R.id.page_preview_image);
        mTvwName = (TextView) convertView.findViewById(R.id.page_preview_item_name);
        // set layout param
        screenWidth = mApplication.getWidthPixels();
        ViewGroup.LayoutParams textParams = mTvwName.getLayoutParams();
        textParams.width = screenWidth / 2;

        ViewGroup.LayoutParams params = mImgPreview.getLayoutParams();
        params.width = screenWidth / 2;
        params.height = (int) (params.width * Constants.SCALE_HEIGHT);
    }

    @Override
    public void setElement(Object obj) {
        entry = (Page) obj;
        mTvwName.setText(entry.getName());
        String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(entry.getPreview());
        ImageLoaderManager.getInstance(mContext).displayGuestBookPreview(mImgPreview, url);
    }
}

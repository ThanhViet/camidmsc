package com.metfone.selfcare.holder.message;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;


/**
 * Created by thaodv on 7/18/2014.
 */
public class ReceivedShareContactHolder extends BaseMessageHolder {
    private ReengMessage message;
    private String TAG = ReceivedShareContactHolder.class.getSimpleName();
    private TextView mTvwContent;
    private TextView mTvwContactNumber;

    public ReceivedShareContactHolder(ApplicationController applicationController, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        setContext(applicationController);
    }

    public ReceivedShareContactHolder(ApplicationController applicationController) {
        setContext(applicationController);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_share_contact_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mTvwContactNumber = (TextView) rowView.findViewById(R.id.tvw_contact_number);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwContactNumber.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwContent.setText(message.getContent());
        mTvwContactNumber.setText(message.getFileName());

        mTvwContactNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onSmartTextClick(message.getFileName(), Constants.SMART_TEXT.TYPE_NUMBER);
            }
        });

        mTvwContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onSmartTextClick(message.getFileName(), Constants.SMART_TEXT.TYPE_NUMBER);
            }
        });
    }
}

package com.metfone.selfcare.holder.message;

import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.util.Log;

import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifDrawableBuilder;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SendVoiceStickerHolder extends BaseMessageHolder {
    private static final String TAG = SendVoiceStickerHolder.class.getSimpleName();
    private ImageView content;
    private ReengMessage message;
    private int collectionId = EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, itemId = 0;
    private ApplicationController mApplicationController;
    private StickerBusiness mStickerBusiness;
    private GifDrawable gifDrawable;

    public SendVoiceStickerHolder(ApplicationController application) {
        setContext(application);
        mApplicationController = application;
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        try {
            collectionId = Integer.valueOf(message.getFileName());
            itemId = (int) message.getSongId();
        } catch (NumberFormatException nfe) {
            Log.e(TAG, "Exception", nfe);
        }
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
        } else {
            mStickerBusiness = mApplicationController.getStickerBusiness();
            StickerItem mStickerItem = mStickerBusiness.getStickerItem(collectionId, itemId);
            StickerCollection stickerCollection = mStickerBusiness.getStickerCollectionById(collectionId);
            if (mStickerItem == null) {
                ImageLoaderManager.getInstance(mContext).displayStickerOnNetwork(content, collectionId, itemId);
            } else if (stickerCollection != null && stickerCollection.isDownloaded()) {
                if (!TextUtils.isEmpty(mStickerItem.getType())
                        && mStickerItem.getType().equals(StickerConstant.STICKER_TYPE_GIF)) {
                    playGifInFirstTime(mStickerItem);
                } else {
                    ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
                }
            } else {
                ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
            }
        }
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_voice_sticker_send, parent, false);
        initBaseHolder(rowView);
        content = (ImageView) rowView
                .findViewById(R.id.message_detail_file_item_content);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    private void playGifInFirstTime(final StickerItem mStickerItem) {
        ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
        if (TextUtils.isEmpty(mStickerItem.getImagePath())) {
            return;
        }
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i(TAG, "Play gif");
                    gifDrawable = new GifDrawableBuilder().from(mStickerItem.getImagePath()).build();
                    content.setImageDrawable(gifDrawable);
                    gifDrawable.setLoopCount(0);
                    /*gifDrawable.addAnimationListener(new AnimationListener() {
                        @Override
                        public void onAnimationCompleted(int loopNumber) {
                            message.setPlayedGif(true);
                            if (loopNumber >= 3) {
                                if (gifDrawable != null) {
                                    gifDrawable.stop();
                                }
                                ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId,
                                        itemId);
                            }
                        }
                    });*/
                } catch (IOException e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        }, 200);
    }
}
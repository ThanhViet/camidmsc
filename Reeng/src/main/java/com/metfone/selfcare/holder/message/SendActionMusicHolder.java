package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;

/**
 * Created by thanh on 3/23/2015.
 */
public class SendActionMusicHolder extends BaseMessageHolder {

    private static final String TAG = SendActionMusicHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private ReengMessage message;
    private MediaModel mediaModel;
    private TextView mTvwTitle;
    private TextView mTvwSong;
    private TextView mTvwSinger;
    private ImageView mImgAvtSong;
    private ImageButton mIbnGiftCrbt;

    public SendActionMusicHolder(ApplicationController app, MessageInteractionListener listener) {
        setContext(app);
        mApplication = app;
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_action_music_send, parent, false);
        initBaseHolder(rowView);
        mTvwTitle = (TextView) rowView.findViewById(R.id.message_share_share_music_send_title);
        mTvwSong = (TextView) rowView.findViewById(R.id.message_share_music_send_song_name);
        mTvwSinger = (TextView) rowView.findViewById(R.id.message_share_music_send_song_singer);
        mImgAvtSong = (ImageView) rowView.findViewById(R.id.message_share_music_send_avatar);
        mIbnGiftCrbt = (ImageButton) rowView.findViewById(R.id.message_gift_crbt);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        drawHolder(message);
        setListener();
    }

    private void drawHolder(ReengMessage message) {
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSong.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSinger.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        String title = message.getFileName();
        mTvwTitle.setText(TextHelper.fromHtml(title));
        mediaModel = message.getSongModel(mApplication.getMusicBusiness());
        if (mediaModel != null) {
            // get data
            String songName = mediaModel.getName();
            String songSinger = mediaModel.getSinger();
            String avatarUrl = mediaModel.getImage();
            // draw
            if (!TextUtils.isEmpty(songName)) {
                mTvwSong.setText(songName);
            } else {
                mTvwSong.setText("");
            }
            if (!TextUtils.isEmpty(songSinger)) {
                mTvwSinger.setText(songSinger);
            } else {
                mTvwSinger.setText("");
            }
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, avatarUrl);
            if ("0".equals(mediaModel.getCrbtCode())) {
                mIbnGiftCrbt.setVisibility(View.GONE);
            } else if (mApplication.getConfigBusiness().isEnableCrbt() &&
                    getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    !TextUtils.isEmpty(mediaModel.getCrbtCode())) {
                mIbnGiftCrbt.setVisibility(View.VISIBLE);
            } else {
                mIbnGiftCrbt.setVisibility(View.GONE);
            }
        } else {
            // neu chua co media thi get from sv, sau khi get duoc thi notify o thread detail
            mTvwSong.setText("");
            mTvwSinger.setText("");
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, "");
            mIbnGiftCrbt.setVisibility(View.GONE);
            //mApplication.getMusicBusiness().getMediaModelFromServer(message.getSongId(), message.getThreadId());
        }
    }

    private void setListener() {
        mIbnGiftCrbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onSendCrbtGift(message, mediaModel);
            }
        });
    }
}
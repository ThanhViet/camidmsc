package com.metfone.selfcare.holder;

import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.holder.onmedia.feeds.OMFeedBaseViewHolder;

public class DividerHolder extends OMFeedBaseViewHolder {

    public DividerHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
    }

    @Override
    public void setElement(Object obj) {
    }
}

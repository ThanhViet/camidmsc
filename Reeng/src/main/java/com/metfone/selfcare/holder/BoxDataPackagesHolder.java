package com.metfone.selfcare.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.ListDataPackagesAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.DataPackagesResponse;

import java.util.ArrayList;

import butterknife.BindView;

public class BoxDataPackagesHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.line)
    @Nullable
    View line;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_label_end)
    @Nullable
    TextView tvLabelEnd;
    @BindView(R.id.recycler_view)
    @Nullable
    RecyclerView recyclerView;

    private ListDataPackagesAdapter adapter;
    private ArrayList<Object> data;
    DataPackageDetailHolder.OnBuyPackage listener;

    public BoxDataPackagesHolder(View view, Activity activity, DataPackageDetailHolder.OnBuyPackage listener) {
        super(view);
        this.listener = listener;
        data = new ArrayList<>();
        adapter = new ListDataPackagesAdapter(activity, data, listener);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, R.drawable.divider_default);
    }

    public void bindData(Object item, int position, String labelEnd) {
        if (item instanceof DataPackagesResponse.ListDataPackages) {
            DataPackagesResponse.ListDataPackages model = (DataPackagesResponse.ListDataPackages) item;
            if (line != null) line.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
            if (tvTitle != null) tvTitle.setText(model.getTitle());
            if (tvLabelEnd != null) {
                if (TextUtils.isEmpty(labelEnd)) {
                    tvLabelEnd.setVisibility(View.GONE);
                } else {
                    tvLabelEnd.setVisibility(View.VISIBLE);
                    tvLabelEnd.setText(labelEnd);
                }
            }
            data.clear();
            data.addAll(model.getListPackages());
            if (adapter != null) adapter.notifyDataSetChanged();
        }
    }

}
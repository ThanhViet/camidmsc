package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.StrangerBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 9/7/2017.
 */

public class SocialViewHolder extends BaseViewHolder implements View.OnClickListener {

    private static final String TAG = SocialViewHolder.class.getSimpleName();
    private ContactsHolderCallBack mCallBack;
    private Context mContext;
    private SocialFriendInfo entry;
    private UserInfo userInfo;
    private ApplicationController mApplication;
    private Resources mRes;
    private ImageView mImgAvatar;
    private TextView mTvContactName;
    private EllipsisTextView mTvStatus;
    private TextView mTvAge;
    private ImageView mIvGender;
    //private View mConvertView;
    private TextView mTvwAvatar;
    private FrameLayout mFrAvatar;
    private RoundTextView btnCancel, btnAccept;
    private int typeSocial;


    public SocialViewHolder(View convertView, Context context, int typeSocial, ContactsHolderCallBack callBack) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        this.typeSocial = typeSocial;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        initView(convertView);
    }


    @Override
    public void setElement(Object obj) {
        if (obj == null) return;
        if (obj instanceof SocialFriendInfo) {
            entry = (SocialFriendInfo) obj;
            setPhoneView();
        } else if (obj instanceof UserInfo) {
            userInfo = (UserInfo) obj;
            setUserInfo();
        }

    }

    private void setUserInfo() {
        mFrAvatar.setVisibility(View.VISIBLE);

        if (TextUtils.isEmpty(userInfo.getMsisdn()) ||
                userInfo.getMsisdn().equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            Log.d(TAG, "server lỗi bắn cả số của mình về");
            mTvContactName.setText("");
            mTvStatus.setVisibility(View.INVISIBLE);
            mImgAvatar.setImageResource(R.drawable.ic_avatar_default);
            mTvwAvatar.setText("");
            return;
        }


        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        String jidNumber = userInfo.getMsisdn();
        String friendName = userInfo.getName();
        if (TextUtils.isEmpty(friendName)) {
            friendName = Utilities.hidenPhoneNumber(jidNumber);
        }
        mTvContactName.setText(friendName);
        mTvStatus.setVisibility(View.VISIBLE);
        String avatarUrl = null;
        if (!TextUtils.isEmpty(userInfo.getAvatar())) {
            avatarUrl = avatarBusiness.getAvatarUrl(userInfo.getAvatar(), jidNumber, size);
        }
        avatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar, avatarUrl, jidNumber, friendName, size);
        mTvStatus.setText(userInfo.getStatus());
        setClickListener();
        btnAccept.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
    }

    private void initView(View convertView) {
        //mConvertView = convertView;
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
        mTvStatus = convertView.findViewById(R.id.item_contact_view_status_text);
        mTvAge = convertView.findViewById(R.id.tvw_profile_age);
        mIvGender = convertView.findViewById(R.id.img_profile_sex);
        mFrAvatar = convertView.findViewById(R.id.item_contact_view_avatar_frame);
        btnCancel = convertView.findViewById(R.id.btnCancel);
        btnAccept = convertView.findViewById(R.id.btnAccept);
    }

    @Override
    public void onClick(View view) {
        if (mCallBack != null) {
            switch (view.getId()) {
                case R.id.item_contact_view_avatar_frame:
                    processWhenClickItem();
                    break;

                case R.id.btnCancel:
                    if (entry != null)
                        mCallBack.onSocialCancel(entry);
                    break;
                case R.id.btnAccept:
                    if (entry != null)
                        mCallBack.onActionLabel(entry);
                    break;
            }
        }
    }

    private void processWhenClickItem() {
        if (entry != null)
            mCallBack.onAvatarClick(entry);
        else if (userInfo != null)
            mCallBack.onAvatarClick(userInfo);
    }

    //set name, avatar,reeng state
    private void setDetailView() {

        if (TextUtils.isEmpty(entry.getUserFriendJid()) ||
                entry.getUserFriendJid().equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            Log.d(TAG, "server lỗi bắn cả số của mình về");
            mTvContactName.setText("");
            mTvStatus.setVisibility(View.INVISIBLE);
            mImgAvatar.setImageResource(R.drawable.ic_avatar_default);
            mTvwAvatar.setText("");
            return;
        }
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        String jidNumber = entry.getUserFriendJid();
        if (entry.getUserType() == UserInfo.USER_ONMEDIA_NORMAL) {
            PhoneNumber phoneNumber = entry.getPhoneNumber();
            if (phoneNumber != null) {
                mTvContactName.setText(phoneNumber.getName());
                avatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, size);
                if (TextUtils.isEmpty(phoneNumber.getStatus())) {
                    mTvStatus.setVisibility(View.INVISIBLE);
                } else {
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setEmoticon(mContext, phoneNumber.getStatus(), phoneNumber.getIdInt(), phoneNumber);
                }
                if (phoneNumber.isShowBirthday()) {
                    long birthday = TimeHelper.convertBirthdayStringToLong(
                            phoneNumber.getBirthdayString());
                    int old = TimeHelper.getOldFromBirthday(birthday);
                    if (old <= 0) {
                        mTvAge.setVisibility(View.GONE);
                    } else {
                        mTvAge.setText(String.valueOf(old));
                        mTvAge.setVisibility(View.VISIBLE);
                    }
                }
                if (phoneNumber.getGender() == Constants.CONTACT.GENDER_FEMALE) {
                    mIvGender.setImageResource(R.drawable.ic_v5_profile_female);
                    mIvGender.setVisibility(View.VISIBLE);
                } else if (phoneNumber.getGender() == Constants.CONTACT.GENDER_MALE) {
                    mIvGender.setImageResource(R.drawable.ic_v5_profile_male);
                    mIvGender.setVisibility(View.VISIBLE);
                } else {
                    mIvGender.setVisibility(View.GONE);
                }
            } else {
                String friendName = entry.getUserName();
                if (TextUtils.isEmpty(entry.getUserName())) {
                    friendName = Utilities.hidenPhoneNumber(jidNumber);
                }
                mTvContactName.setText(friendName);
                String status = entry.getUserStatus();
                if (TextUtils.isEmpty(status)) {
                    mTvStatus.setVisibility(View.INVISIBLE);
                } else {
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setEmoticon(mApplication, status, status.hashCode(), status);
                }
                String avatarUrl = null;
                if (entry.isUserMocha()) {
                    if (!TextUtils.isEmpty(entry.getUserAvatar())) {
                        avatarUrl = avatarBusiness.getAvatarUrl(entry.getUserAvatar(), jidNumber, size);
                    }
                } else {
                    avatarUrl = entry.getUserAvatar();
                }
                avatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar, avatarUrl, jidNumber, friendName, size);
            }
        } else {
            ImageLoaderManager.getInstance(mApplication).setAvatarOfficialOnMedia(mImgAvatar, entry.getUserAvatar(), size);
            mTvContactName.setText(entry.getUserName());
            String status = entry.getUserStatus();
            if (TextUtils.isEmpty(status)) {
                mTvStatus.setVisibility(View.INVISIBLE);
            } else {
                mTvStatus.setVisibility(View.VISIBLE);
                mTvStatus.setEmoticon(mApplication, status, status.hashCode(), status);
            }
        }
        setClickListener();
        if (typeSocial == StrangerBusiness.TYPE_SOCIAL_BE_FOLLOWED) {
            btnAccept.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.VISIBLE);
        } else {
            btnAccept.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
        }
    }


    private void setPhoneView() {
        mFrAvatar.setVisibility(View.VISIBLE);
        setDetailView();
    }

    private void setClickListener() {
        getItemView().setOnClickListener(v -> processWhenClickItem());
        btnAccept.setOnClickListener(this);
        mFrAvatar.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }
}

package com.metfone.selfcare.holder.onmedia.feeds;

import android.content.res.Resources;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.metfone.selfcare.adapter.onmedia.SuggestFriendAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.ui.recyclerview.headerfooter.HeaderAndFooterRecyclerViewAdapter;

/**
 * Created by thanhnt72 on 9/5/2017.
 */
public class OMFeedSuggestFriendViewHolder extends OMFeedBaseViewHolder {

    private static final String TAG = OMFeedSuggestFriendViewHolder.class.getSimpleName();
    private RecyclerView mRecyclerViewSuggestFriend;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter;
    private AppCompatTextView mTvwTitle;
    private SuggestFriendAdapter suggestFriendAdapter;
    private ApplicationController mApp;
    private Resources mRes;

    public OMFeedSuggestFriendViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        this.mApp = mApplication;
        mRes = mApplication.getResources();
        mRecyclerViewSuggestFriend = (RecyclerView) itemView.findViewById(R.id.scroll_list_friend);
        mRecyclerViewSuggestFriend.setLayoutManager(new LinearLayoutManager(mApplication, LinearLayoutManager
                .HORIZONTAL,
                false));
        suggestFriendAdapter = new SuggestFriendAdapter(mApplication);
        mHeaderAndFooterRecyclerViewAdapter = new HeaderAndFooterRecyclerViewAdapter(suggestFriendAdapter);
        mRecyclerViewSuggestFriend.setAdapter(mHeaderAndFooterRecyclerViewAdapter);
        LinearLayout header = new LinearLayout(mApplication);
        int widthView = mApplication.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_16);
        header.setLayoutParams(new LinearLayout.LayoutParams(widthView, widthView));
        mHeaderAndFooterRecyclerViewAdapter.addHeaderView(header);
        mTvwTitle = itemView.findViewById(R.id.tvw_title_suggest_friend);
    }

    @Override
    public void setElement(Object obj) {
        FeedModelOnMedia feedModelOnMedia = (FeedModelOnMedia) obj;
        suggestFriendAdapter.setListUserInfo(feedModelOnMedia.getListSuggestFriend());
        suggestFriendAdapter.setListener(getOnMediaHolderListener());
        suggestFriendAdapter.notifyDataSetChanged();
        if (TextUtils.isEmpty(feedModelOnMedia.getUserStatus())) {
            mTvwTitle.setText(mRes.getString(R.string.title_suggest_friend));
        } else {
//            mTvwTitle.setEmoticon(mApp, feedModelOnMedia.getUserStatus(), feedModelOnMedia.getUserStatus().hashCode()
//                    , feedModelOnMedia.getUserStatus());
            mTvwTitle.setText(feedModelOnMedia.getUserStatus());
        }
    }
}

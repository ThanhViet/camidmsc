package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.EmoTextViewListChat;

/**
 * Created by thanhnt72 on 10/21/2017.
 */

public class ReceivedBankplusHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedBankplusHolder.class.getSimpleName();
    private ReengMessage message;
    private ApplicationController mApp;
    private Resources mRes;

    private EmoTextViewListChat mTvwContent;
    private TextView mTvwAmountMoney;
    private TextView mTvwDesc;
    private View mViewLogo;
    private Button mBtnAccept;

    public ReceivedBankplusHolder(Context ctx) {
        setContext(ctx);
        mApp = (ApplicationController) ctx;
        mRes = mApp.getResources();
    }

    public ReceivedBankplusHolder(Context ctx, boolean isQuick) {
        setContext(ctx);
        mApp = (ApplicationController) ctx;
        mRes = mApp.getResources();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_bankplus_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (EmoTextViewListChat) rowView.findViewById(R.id.message_text_content);
        mTvwAmountMoney = (TextView) rowView.findViewById(R.id.tvw_amount_money);
        mTvwDesc = (TextView) rowView.findViewById(R.id.tvw_content_send_money);
        mBtnAccept = (Button) rowView.findViewById(R.id.btn_agree_bankplus);
        mViewLogo = rowView.findViewById(R.id.view_logo_bplus);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        drawBPlusDetail();
        setViewListener();
    }

    private void drawBPlusDetail() {
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwAmountMoney.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_6));
        mBtnAccept.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        String msg;
        if (ReengMessageConstant.BPLUS_PAY.equals(message.getFilePath())) {
            msg = mRes.getString(R.string.bplus_holder_pay_received);
            mViewLogo.setVisibility(View.VISIBLE);
            mBtnAccept.setVisibility(View.GONE);
        } else {
            msg = mRes.getString(R.string.bplus_holder_claim_received);
            mViewLogo.setVisibility(View.GONE);
            mBtnAccept.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(message.getContent())) {
            mTvwContent.setText(msg);
        } else {
            mTvwContent.setText(message.getContent());
        }
        mTvwDesc.setText(String.format(mRes.getString(R.string.bplus_holder_desc), message.getVideoContentUri()));
        mTvwAmountMoney.setText(TextHelper.formatTextDecember(message.getImageUrl()));
    }

    private void setViewListener() {
        mBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageInteractionListener listener = getMessageInteractionListener();
                if (listener != null) {
                    listener.onBplus(message);
                }
            }
        });
    }
}

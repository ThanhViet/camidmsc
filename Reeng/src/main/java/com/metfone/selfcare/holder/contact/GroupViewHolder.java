package com.metfone.selfcare.holder.contact;

import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;

/**
 * Created by toanvk2 on 9/1/2017.
 */

public class GroupViewHolder extends BaseViewHolder {
    private ApplicationController mApplication;
    private ContactsHolderCallBack mCallBack;
    private ThreadMessage mEntry;
    private CircleImageView mImgThreadAvatar;
    private ImageView mImgSong;
    private TextView mTvwThreadName, mTvwAvatar, mTvwUnreadMsg, mTvwLastTime;
    private EllipsisTextView mTvwLastContent;
    private TextView mTvwThreadType;

    public GroupViewHolder(View rowView, ApplicationController mApp, ContactsHolderCallBack callBack) {
        super(rowView);
        this.mCallBack = callBack;
        mApplication = mApp;
        mImgThreadAvatar = rowView.findViewById(R.id.thread_avatar);
        mTvwLastTime = rowView.findViewById(R.id.thread_last_time);
        mTvwAvatar = rowView.findViewById(R.id.contact_avatar_text);
        mTvwThreadName = rowView.findViewById(R.id.thread_name);
        mImgSong = rowView.findViewById(R.id.thread_song_ic);
        mTvwLastContent = rowView.findViewById(R.id.thread_last_content);
        //mLlStatusImage = rowView.findViewById(R.id.thread_last_status_parent);
        mTvwThreadType = rowView.findViewById(R.id.thread_holder_type);
        mTvwUnreadMsg = rowView.findViewById(R.id.thread_number_unread);
    }

    @Override
    public void setElement(Object obj) {
        mEntry = (ThreadMessage) obj;
        setMessageView();
        //super.setElement(obj);
        setListener();
    }

    private void setMessageView() {
        //lay tin nhan cuoi cung cua Thread
        Resources res = mApplication.getResources();
        mTvwThreadType.setVisibility(View.GONE);
        mTvwUnreadMsg.setVisibility(View.GONE);
        mImgSong.setVisibility(View.GONE);
        mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.mocha_message_read));
        //mLlStatusImage.setVisibility(View.GONE);
        // mImgStateStranger.setVisibility(View.GONE);

        String threadName = mApplication.getMessageBusiness().getThreadName(mEntry);
        if (!TextUtils.isEmpty(threadName)) {
            mTvwThreadName.setText(threadName);
        } else {
            mTvwThreadName.setText("");
        }
        int sizeAvatar = (int) res.getDimension(R.dimen.avatar_small_size);
        if (mEntry.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            // avatar
            mTvwAvatar.setVisibility(View.GONE);        // Text
            mApplication.getAvatarBusiness().setGroupThreadAvatar(mImgThreadAvatar, mEntry);
            // mImgStateStranger.setVisibility(View.GONE);
            // danh sach thanh vien
            String friendListName = mApplication.getContactBusiness().
                    getListNameOfListNumber(mEntry.getPhoneNumbers());
            if (!TextUtils.isEmpty(friendListName)) {
                mTvwLastContent.setText(friendListName);
            } else {
                mTvwLastContent.setText(res.getString(R.string.group_friend_empty));
            }
        } else if (mEntry.isStranger() && mEntry.getStrangerPhoneNumber() != null) {
//            StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
            // mImgStateStranger.setVisibility(View.VISIBLE);
            mTvwAvatar.setVisibility(View.GONE);
            mTvwLastContent.setVisibility(View.GONE);
           /* if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                mImgStateStranger.setImageResource(R.drawable.ic_stranger_keeng);
            } else {
                mImgStateStranger.setImageResource(R.drawable.ic_stranger_music);
            }*/
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgThreadAvatar, mTvwAvatar,
                    mEntry.getStrangerPhoneNumber(),
                    mEntry.getStrangerPhoneNumber().getPhoneNumber(), null, null, sizeAvatar);
        } else {
            mTvwAvatar.setVisibility(View.GONE);
            // mImgStateStranger.setVisibility(View.GONE);
            mTvwLastContent.setVisibility(View.GONE);
        }
        int messageSize = mEntry.getAllMessages().size();
        if (messageSize <= 0) {
            mTvwLastTime.setVisibility(View.INVISIBLE);
        } else {
            mTvwLastTime.setVisibility(View.VISIBLE);
            mTvwLastTime.setText(TimeHelper.formatCommonTime(mEntry.getAllMessages().get(messageSize - 1).getTime(), TimeHelper.getCurrentTime(), res));
        }
    }

    private void setListener() {
        getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    mCallBack.onItemClick(mEntry);
                }
            }
        });
    }
}

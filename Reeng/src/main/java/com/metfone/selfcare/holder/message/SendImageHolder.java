package com.metfone.selfcare.holder.message;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.message.ProgressFileManager;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.DonutProgress;
import com.metfone.selfcare.ui.EmoTextViewListChat;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 11/21/2017.
 */

public class SendImageHolder extends BaseMessageHolder {
    private RoundedImageView content;
    private View vLoading;
    private static final String TAG = SendImageHolder.class.getSimpleName();
    private EmoTextViewListChat mTvwContent;
    private ReengMessage message;
    private SmartTextClickListener mSmartTextListener;

    private DonutProgress mProgressFirst;
    private DonutProgress mProgressSecond;
    private AnimatorSet animatorSet;
    private ApplicationController mApp;

    public SendImageHolder(Context ctx, SmartTextClickListener smartTextListener) {
        this.mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_image_send, parent, false);
        initBaseHolder(rowView);
        content = (RoundedImageView) rowView.findViewById(R.id.message_detail_file_item_content);
        vLoading = rowView.findViewById(R.id.message_detail_file_loading);
        mTvwContent = (EmoTextViewListChat) rowView.findViewById(R.id.message_text_content);
        mProgressFirst = (DonutProgress) rowView.findViewById(R.id.progress_bar_first);
        mProgressSecond = (DonutProgress) rowView.findViewById(R.id.progress_bar_second);
        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(mContext, R.animator.progress_anim_first);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.setTarget(mProgressFirst);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        // Log.i(TAG, "" + message);
        View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().longClickBgrCallback(message);
                return false;
            }
        };
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants
                .FONT_SIZE.LEVEL_2_5));
        Log.d(TAG, TAG + "-" + message.getId() + " " + message.getStatus());
        if (message.getId() > 0) {// message luu db co id, id==0 thi ko xu ly
            ProgressFileManager.addToHashMap(message.getId(), getConvertView());
            if (ProgressFileManager.getHandler() == null) {
                ProgressFileManager.initHandler();
            }
        }
        // set visible for content
        // set invisible for filename

        content.setVisibility(View.VISIBLE);
        //
        if(message.isPlaying()){
            Log.i(TAG, "is playing animation, do nothing");
        } else {
            if (TextUtils.isEmpty(message.getFileId()) && (message.getStatus() == ReengMessageConstant.STATUS_LOADING
                    || message.getStatus() == ReengMessageConstant.STATUS_NOT_SEND)) {
                vLoading.setVisibility(View.VISIBLE);
                if (!message.isRunFirstAnimation()) {
                    message.setRunFirstAnimation(true);
                    animatorSet.start();
                } else {
                    mProgressFirst.setProgress(30f);
                }
                mProgressFirst.setVisibility(View.VISIBLE);
                mProgressSecond.setVisibility(View.GONE);
            } else
                vLoading.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(message.getContent())) {
            mTvwContent.setVisibility(View.GONE);
        } else {
            mTvwContent.setVisibility(View.VISIBLE);
            mTvwContent.setEmoticon(message.getContent(), message.getId(), mSmartTextListener, longClickListener,
                    message);
        }
        Log.i(TAG, "ratio: " + message.getVideoContentUri() + " id: " + message.getId());
        if (TextUtils.isEmpty(message.getVideoContentUri())) {
            content.getLayoutParams().width = mApp.getWidthPixels() * 2 / 3;
            content.getLayoutParams().height = content.getLayoutParams().width;
                /*content.setAspectRatio(1.7f);
                content.setAspect(RoundedImageView.Aspect.HEIGHT);*/
        } else {
            float ratio = Float.parseFloat(message.getVideoContentUri());
            if (ratio >= 1) {
                if (ratio >= 2) ratio = 2;
                content.getLayoutParams().width = mApp.getWidthPixels() * 2 / 3;
                content.getLayoutParams().height = Math.round(content.getLayoutParams().width / ratio);
            } else {
                if (ratio <= 0.5f) ratio = 0.5f;
                content.getLayoutParams().height = mApp.getHeightPixels() / 2;
                content.getLayoutParams().width = Math.round(content.getLayoutParams().height * ratio);
            }
        }
        content.requestLayout();
        showThumbnailOfImage(message, content, content.getLayoutParams().width, content.getLayoutParams().height);
    }
}

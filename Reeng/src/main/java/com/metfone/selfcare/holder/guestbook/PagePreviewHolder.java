package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.editor.sticker.utils.PreviewImageView;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 4/24/2017.
 */
public class PagePreviewHolder extends AbsContentHolder {
    private static final String TAG = PagePreviewHolder.class.getSimpleName();
    private Context mContext;
    private Page mEntry;
    private PreviewImageView mImgPreview;
    private ProgressLoading mProgressLoading;

    public PagePreviewHolder(Context context) {
        this.mContext = context;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        View convertView = layoutInflater.inflate(R.layout.holder_guest_book_preview, parent, false);
        mImgPreview = (PreviewImageView) convertView.findViewById(R.id.holder_guest_book_preview_image);
        mProgressLoading = (ProgressLoading) convertView.findViewById(R.id.progress_loading_img);
        convertView.setTag(this);
        setConvertView(convertView);
    }

    @Override
    public void setElemnts(Object obj) {
        mEntry = (Page) obj;
        drawHolderDetail();
        //initLayoutParams();
    }

    private void drawHolderDetail() {
        Log.d(TAG, "drawHolderDetail");
        String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(mEntry.getPreview());
        ImageLoaderManager.getInstance(mContext).displayGuestBookPreview(mImgPreview, url, new GlideImageLoader.SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted() {
                mProgressLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(GlideException e) {
                mProgressLoading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete() {
                mProgressLoading.setVisibility(View.GONE);
            }
        });
    }
}

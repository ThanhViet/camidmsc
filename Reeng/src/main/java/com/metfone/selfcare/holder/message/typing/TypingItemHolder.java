package com.metfone.selfcare.holder.message.typing;

import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.message.TypingMessageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.message.TypingItem;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 9/27/2017.
 */

public class TypingItemHolder extends BaseViewHolder {
    private TypingMessageAdapter.TypingMessageListener mListener;
    private Resources mRes;
    private TypingItem mEntry;
    private TextView mTvwTitle;
    private ImageView mImgIcon;

    public TypingItemHolder(ApplicationController application, View itemView, TypingMessageAdapter.TypingMessageListener listener) {
        super(itemView);
        this.mListener = listener;
        this.mRes = application.getResources();
        mTvwTitle = (TextView) itemView.findViewById(R.id.holder_typing_item_title);
        mImgIcon = (ImageView) itemView.findViewById(R.id.holder_typing_item_icon);
    }

    @Override
    public void setElement(Object obj) {
        try {
            mEntry = (TypingItem) obj;
            mTvwTitle.setText(TextHelper.fromHtml(mRes.getString(mEntry.getTitleRes())));
            mImgIcon.setImageResource(mEntry.getIconRes());
            getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemTypeClick(mEntry);
                }
            });
        }
        catch (NullPointerException ex)
        {

        }
    }
}

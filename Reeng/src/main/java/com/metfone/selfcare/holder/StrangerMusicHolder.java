package com.metfone.selfcare.holder;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.listeners.StrangerMusicInteractionListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 1/7/15.
 */
public class StrangerMusicHolder extends BaseViewHolder {
    private static final String TAG = StrangerMusicHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private StrangerMusicInteractionListener mStrangerMusicInteractionListener;
    private Resources mRes;
    private StrangerMusic mEntry;
    //    private FrameLayout mFraRow;
//    private RelativeLayout mRllPoster, mRllAcceptor;
//    private ImageView mImgIconStar;
    private RoundedImageView mImgPosterAvatar, mImgAcceptorAvatar;
    private AppCompatImageView ivGenderOne, ivGenderTwo, ivMusic;
    private AppCompatTextView mTvwSongAndSingerName, mTvwPosterName, mTvwAcceptorName;
    /* private ImageView mImgEqualizer;
     private AnimationDrawable animationEqualizer;*/
//    private int holderHeight, contentMargin, headphoneWidth, starHeight, fraHeight, fraWidth, starMargin;
//    private int sizeAvatar;

    private Typeface typefaceReguler;
    private Typeface typefaceMedium;
    private int sizeViewGroupAvatar;

    public StrangerMusicHolder(View convertView, Context context,
                               StrangerMusicInteractionListener listener,
                               Typeface typeFaceReguler, Typeface typefaceMedium, int sizeViewGroupAvatar) {
        super(convertView);
        this.mRes = context.getResources();
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mStrangerMusicInteractionListener = listener;
        this.typefaceReguler = typeFaceReguler;
        this.typefaceMedium = typefaceMedium;
        this.sizeViewGroupAvatar = sizeViewGroupAvatar;
        //
//        mFraRow =  convertView.findViewById(R.id.stranger_music_frame_layout);
//        mRllPoster = (RelativeLayout) convertView.findViewById(R.id.stranger_music_poster_layout);
//        mRllAcceptor = (RelativeLayout) convertView.findViewById(R.id.stranger_music_acceptor_layout);
        mTvwPosterName = convertView.findViewById(R.id.tvNameOne);
        mImgPosterAvatar = convertView.findViewById(R.id.ivAvatarOne);
        mTvwAcceptorName = convertView.findViewById(R.id.tvNameTwo);
        ivGenderOne = convertView.findViewById(R.id.ivGenOne);
        ivGenderTwo = convertView.findViewById(R.id.ivGenTwo);
        ivMusic = convertView.findViewById(R.id.icMusic);
        /*mTvwAcceptorSongName = (TextView) convertView.findViewById(R.id.stranger_music_acceptor_song_name);
        mTvwAcceptorSingerName = (TextView) convertView.findViewById(R.id.stranger_music_acceptor_singer_name);*/
        mImgAcceptorAvatar = convertView.findViewById(R.id.ivAvatarTwo);
        mTvwSongAndSingerName = convertView.findViewById(R.id.tvMusicTitle);
//        mImgIconStar = (ImageView) convertView.findViewById(R.id.stranger_music_star_icon);
        // set size
        ConstraintLayout constraintLayout = convertView.findViewById(R.id.constraintLayoutAvatar);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) constraintLayout.getLayoutParams();
        layoutParams.width = sizeViewGroupAvatar;
        setListener();
        Log.e("debug", "create View Holder");
    }

    @Override
    public void setElement(Object obj) {
        mEntry = (StrangerMusic) obj;
        drawHolderDetail();
    }

    public void setLayoutParams(int holderHeight, int contentMargin, int headphoneWidth, int starHeight,
                                int fraHeight, int fraWidth, int starMargin, int sizeAvatar) {
//        this.holderHeight = holderHeight;
//        this.contentMargin = contentMargin;
//        this.headphoneWidth = headphoneWidth;
//        this.starHeight = starHeight;
//        this.fraHeight = fraHeight;
//        this.fraWidth = fraWidth;
//        this.starMargin = starMargin;
//        this.sizeAvatar = sizeAvatar;
//        initLayoutParams();
    }

//    private void initLayoutParams() {
//        //parrent row
//        RelativeLayout.LayoutParams paramsRow = (RelativeLayout.LayoutParams) mFraRow.getLayoutParams();
//        paramsRow.height = fraHeight;
//        paramsRow.width = fraWidth;
//        // child row
//        FrameLayout.LayoutParams paramsPoster = (FrameLayout.LayoutParams) mRllPoster.getLayoutParams();
//        paramsPoster.height = holderHeight;
//        paramsPoster.width = holderHeight;
//        paramsPoster.leftMargin = contentMargin;
//        FrameLayout.LayoutParams paramsAcceptor = (FrameLayout.LayoutParams) mRllAcceptor.getLayoutParams();
//        paramsAcceptor.height = holderHeight;
//        paramsAcceptor.width = holderHeight;
//        paramsAcceptor.rightMargin = contentMargin;
//        // name
//        RelativeLayout.LayoutParams paramsPosterName = (RelativeLayout.LayoutParams) mTvwPosterName.getLayoutParams();
//        paramsPosterName.width = holderHeight;
//        paramsPosterName.leftMargin = contentMargin;
//        paramsPosterName.rightMargin = contentMargin;
//        paramsPosterName.bottomMargin = contentMargin;
//        RelativeLayout.LayoutParams paramsAcceptorName = (RelativeLayout.LayoutParams) mTvwAcceptorName.getLayoutParams();
//        paramsAcceptorName.width = holderHeight;
//        paramsAcceptorName.leftMargin = contentMargin;
//        paramsAcceptorName.rightMargin = contentMargin;
//        paramsAcceptorName.bottomMargin = contentMargin;
//        //star
//        FrameLayout.LayoutParams paramsIconStar = (FrameLayout.LayoutParams) mImgIconStar.getLayoutParams();
//        paramsIconStar.height = starHeight;
//        paramsIconStar.width = starHeight;
//        paramsIconStar.topMargin = starMargin;
//        paramsIconStar.leftMargin = contentMargin;
//    }

    private void setListener() {
        mImgPosterAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mStrangerMusicInteractionListener.onClickPoster(mEntry);
            }
        });
        mImgAcceptorAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEntry.getState() == Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_ACCEPTED
                        || mEntry.getState() == Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_STAR_OFF) {
                    mStrangerMusicInteractionListener.onClickAcceptor(mEntry);
                } else if (mEntry.getState() == Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_WAIT) {
                    mStrangerMusicInteractionListener.onClickAccept(mEntry);
                } else {// re invite
                    mStrangerMusicInteractionListener.onClickReinvite(mEntry);
                }
            }
        });
    }

    private void drawHolderDetail() {
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
//        ImageLoader imageLoader = mApplication.getUniversalImageLoader();
        mTvwPosterName.setVisibility(View.VISIBLE);
        mTvwPosterName.setText(mEntry.getPosterName());
        setIconGender(ivGenderOne, mEntry.getPosterGender());
        long t = System.currentTimeMillis();
        // avatar poster
        int resIconWait;
        String myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();
        if (myJidNumber.equals(mEntry.getPosterJid())) {
            resIconWait = R.drawable.ic_stranger_music_cancel;
        } else {
            resIconWait = mEntry.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_CONFIDE ?
                    R.drawable.ic_stranger_confide_wait : R.drawable.ic_stranger_music_wait;
        }
        if (mEntry.isStarRoom()) {
            ivMusic.setVisibility(View.VISIBLE);
            ivMusic.setImageResource(R.drawable.ic_room_star);
            avatarBusiness.setOfficialThreadAvatar(mImgPosterAvatar, mEntry.getPosterLastAvatar(), mImgPosterAvatar.getWidth());
//            Glide.with(mApplication)
//                    .load( mEntry.getPosterLastAvatar())
//                    .into(mImgPosterAvatar);

        } else {
            ivMusic.setImageResource(R.drawable.ic_music_item_stranger);
//            mImgIconStar.setVisibility(View.GONE);
            avatarBusiness.setStrangerAvatar(mImgPosterAvatar, null, null, mEntry.getPosterJid(),
                    mEntry.getPosterName(), mEntry.getPosterLastAvatar(), mImgPosterAvatar.getWidth());
//            Glide.with(mApplication)
//                    .load( mEntry.getPosterLastAvatar())
//                    .into(mImgPosterAvatar);
        }
        Log.i(TAG, "drawHolderDetail poster avatar take: " + (System.currentTimeMillis() - t));
        if (mEntry.getState() == Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_WAIT) {
            mTvwAcceptorName.setText(mRes.getString(R.string.awaiting_pairing));
            mTvwAcceptorName.setVisibility(View.VISIBLE);
            mTvwAcceptorName.setTypeface(typefaceReguler);
            mTvwAcceptorName.setTextColor(ContextCompat.getColor(mApplication, R.color.v5_text_2));
            ivGenderTwo.setVisibility(View.GONE);
            drawSongAndSinger();
            avatarBusiness.setStrangerMusicBanner(mImgAcceptorAvatar, resIconWait);
        } else if (mEntry.getState() == Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_ACCEPTED) {
            mTvwAcceptorName.setText(mEntry.getAcceptorName());
            mTvwAcceptorName.setTypeface(typefaceMedium);
            mTvwAcceptorName.setTextColor(ContextCompat.getColor(mApplication, R.color.v5_text));
            ivGenderTwo.setVisibility(View.VISIBLE);
            setIconGender(ivGenderTwo, mEntry.getAcceptorGender());
            drawSongAndSinger();
            // avatar acceptor
            avatarBusiness.setStrangerAvatar(mImgAcceptorAvatar, null, null, mEntry.getAcceptorJid(),
                    mEntry.getAcceptorName(), mEntry.getAcceptorLastAvatar(), mImgAcceptorAvatar.getWidth());
            mTvwAcceptorName.setVisibility(View.VISIBLE);
        } else if (mEntry.getState() == Constants.KEENG_MUSIC.STRANGE_MUSIC_STATE_TIME_OUT) {// time out
            mTvwAcceptorName.setText(mRes.getString(R.string.awaiting_pairing));
            mTvwAcceptorName.setVisibility(View.VISIBLE);
            mTvwAcceptorName.setTypeface(typefaceReguler);
            mTvwAcceptorName.setTextColor(ContextCompat.getColor(mApplication, R.color.v5_text_2));
            ivGenderTwo.setVisibility(View.GONE);
            drawSongAndSinger();
            ImageHelper.setImageAlpha(mImgPosterAvatar, 1.0f);
            ImageHelper.setImageAlpha(mImgAcceptorAvatar, 1.0f);
            avatarBusiness.setStrangerMusicBanner(mImgAcceptorAvatar, resIconWait);
        } else {// star offline
            mTvwAcceptorName.setVisibility(View.GONE);
//            mTvwAcceptorName.setText(mRes.getString(R.string.awaiting_pairing));
//            imageLoader.cancelDisplayTask(mImgAcceptorAvatar);
            Glide.with(mApplication).clear(mImgAcceptorAvatar);
            avatarBusiness.setStrangerMusicBanner(mImgAcceptorAvatar, R.drawable.ic_stranger_music_star_wait);
            // text detail star
            mTvwSongAndSingerName.setText(mEntry.getMessageSticky(mApplication.getReengAccountBusiness().getCurrentLanguage()));
        }
        Log.i(TAG, "drawHolderDetail total take: " + (System.currentTimeMillis() - t));
        //        mTvwSongAndSingerName.setSelected(true);
    }

    private void drawSongAndSinger() {
        mTvwSongAndSingerName.setVisibility(View.VISIBLE);
        if (mEntry.getTypeObj() == Constants.STRANGER_MUSIC.TYPE_CONFIDE) {
            ivMusic.setVisibility(View.GONE);
            mTvwSongAndSingerName.setText(mEntry.getConfideStatus());
        } else if (mEntry.getSongModel() != null) {
            ivMusic.setVisibility(View.VISIBLE);
            mTvwSongAndSingerName.setText(mEntry.getSongModel().getSongAndSinger());
        } else {
            ivMusic.setVisibility(View.GONE);
            mTvwSongAndSingerName.setText("");
        }
    }

    private void setIconGender(AppCompatImageView ic, int gender) {
        if (gender == Constants.CONTACT.GENDER_MALE) {
            ic.setImageResource(R.drawable.ic_boy_v5);
//            mImgGender.setImageResource(R.drawable.ic_around_gender_male);
//            mImgGender.setColorFilter(ContextCompat.getColor(mContext, R.color.gender_male));
        } else {
            ic.setImageResource(R.drawable.ic_girl_v5);
            //            mImgGender.setImageResource(R.drawable.ic_around_gender_female);
//            mImgGender.setColorFilter(ContextCompat.getColor(mContext, R.color.gender_female));
        }
    }
}
package com.metfone.selfcare.holder.onmedia.feeds;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ImageProfileBusiness;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.ui.imageview.SquareImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 11/14/2016.
 */
public class OMFeedImageAlbumViewHolder extends OMFeedBaseViewHolder {

    private static final String TAG = OMFeedImageAlbumViewHolder.class.getSimpleName();

    private ApplicationController mApp;
    private int padding;
    private FeedModelOnMedia feedModel;
    private ImageProfileBusiness mImageProfileBusiness;
    private View mViewDouble, mViewTriple, mViewQuadruple, mViewQuintuple, mViewMore;
    private SquareImageView mImgDoubleFirst, mImgDoubleSecond;
    private ImageView mImgTripleFirst, mImgTripleSecond, mImgTripleThird;
    private ImageView mImgQuadrupleFirst, mImgQuadrupleSecond, mImgQuadrupleThird, mImgQuadrupleFour;
    private SquareImageView mImgQuintupleFirst, mImgQuintupleSecond, mImgQuintupleThird, mImgQuintupleFour,
            mImgQuintupleFive;
    private TextView mTvwNumberMore;

    private ArrayList<FeedContent.ImageContent> listImg;

    public OMFeedImageAlbumViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        mApp = mApplication;
        mImageProfileBusiness = mApp.getImageProfileBusiness();
        padding = mApp.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_5);
        mViewDouble = itemView.findViewById(R.id.layout_img_double);
        mViewTriple = itemView.findViewById(R.id.layout_img_triple);
        mViewQuadruple = itemView.findViewById(R.id.layout_img_quadruple);
        mViewQuintuple = itemView.findViewById(R.id.layout_img_quintuple_and_more);
        mViewMore = itemView.findViewById(R.id.layout_img_more);

        mImgDoubleFirst = itemView.findViewById(R.id.img_double_first);
        mImgDoubleSecond = itemView.findViewById(R.id.img_double_second);
        mImgTripleFirst = itemView.findViewById(R.id.img_triple_first);
        mImgTripleSecond = itemView.findViewById(R.id.img_triple_second);
        mImgTripleThird = itemView.findViewById(R.id.img_triple_third);
        mImgQuadrupleFirst = itemView.findViewById(R.id.img_quadruple_first);
        mImgQuadrupleSecond = itemView.findViewById(R.id.img_quadruple_second);
        mImgQuadrupleThird = itemView.findViewById(R.id.img_quadruple_third);
        mImgQuadrupleFour = itemView.findViewById(R.id.img_quadruple_four);
        mImgQuintupleFirst = itemView.findViewById(R.id.img_quintuple_first);
        mImgQuintupleSecond = itemView.findViewById(R.id.img_quintuple_second);
        mImgQuintupleThird = itemView.findViewById(R.id.img_quintuple_third);
        mImgQuintupleFour = itemView.findViewById(R.id.img_quintuple_four);
        mImgQuintupleFive = itemView.findViewById(R.id.img_quintuple_five);

        mTvwNumberMore = itemView.findViewById(R.id.tvw_img_five_more);
    }

    @Override
    public void setElement(Object obj) {
        long t1 = System.currentTimeMillis();
        feedModel = (FeedModelOnMedia) obj;
        setBaseElements(obj);

        FeedContent feedContent = feedModel.getFeedContent();
        listImg = feedContent.getListImage();
        int sizeListImg = listImg.size();
        // size =0 va =1 da check tu luc truoc
        if (sizeListImg == 2) {
            setViewDouble();
            setViewDoubleListener();
        } else if (sizeListImg == 3) {
            setViewTriple();
            setViewTripleListener();
        } else if (sizeListImg == 4) {
            setViewQuadruple();
            setViewQuadrupleListener();
        } else if (sizeListImg == 5) {
            setViewQuintuple();
            setViewQuintupleListener();
        } else {
            setViewQuintupleAndMore();
        }
        Log.i(TAG, "-------OnMedia---------setElement take: " + (System.currentTimeMillis() - t1) + " url: " +
                feedContent.getUrl());
    }


    private void setViewDouble() {
        mViewDouble.setVisibility(View.VISIBLE);
        mViewTriple.setVisibility(View.GONE);
        mViewQuadruple.setVisibility(View.GONE);
        mViewQuintuple.setVisibility(View.GONE);

        FeedContent.ImageContent imageContent0 = feedModel.getFeedContent().getListImage().get(0);
        if (imageContent0.getWidthTarget() == 0) {
            setWidthHeight(1, true, padding, imageContent0);
        }
//        ImageViewAwareTargetSize imgTarget0 = new ImageViewAwareTargetSize(mImgDoubleFirst, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget1 = new ImageViewAwareTargetSize(mImgDoubleSecond, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());

        mImageProfileBusiness.displayImageProfileForFeed(mImgDoubleFirst,
                feedModel.getFeedContent().getListImage().get(0).getThumbUrl(mApp), imageContent0.getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgDoubleSecond,
                feedModel.getFeedContent().getListImage().get(1).getThumbUrl(mApp), imageContent0.getWidthTarget(), imageContent0.getHeightTarget());
    }

    private void setViewDoubleListener() {
        mImgDoubleFirst.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 0);
            }
        });

        mImgDoubleSecond.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 1);
            }
        });
    }


    private void setViewTriple() {
        mViewDouble.setVisibility(View.GONE);
        mViewTriple.setVisibility(View.VISIBLE);
        mViewQuadruple.setVisibility(View.GONE);
        mViewQuintuple.setVisibility(View.GONE);

        FeedContent.ImageContent imageContent0 = feedModel.getFeedContent().getListImage().get(0);
        FeedContent.ImageContent imageContent1 = feedModel.getFeedContent().getListImage().get(1);
        if (imageContent0.getWidthTarget() == 0) {
            setWidthHeight(0.5f, true, padding, imageContent0);
        }
        if (imageContent1.getWidthTarget() == 0) {
            setWidthHeight(1, true, padding, imageContent1);
        }
//        ImageViewAwareTargetSize imgTarget0 = new ImageViewAwareTargetSize(mImgTripleFirst, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//
//        ImageViewAwareTargetSize imgTarget1 = new ImageViewAwareTargetSize(mImgTripleSecond, imageContent1
//                .getWidthTarget(), imageContent1.getHeightTarget());
//
//        ImageViewAwareTargetSize imgTarget2 = new ImageViewAwareTargetSize(mImgTripleThird, imageContent1
//                .getWidthTarget(), imageContent1.getHeightTarget());

        //anh to
        mImageProfileBusiness.displayImageProfileForFeed(mImgTripleFirst,
                feedModel.getFeedContent().getListImage().get(0).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgTripleSecond,
                feedModel.getFeedContent().getListImage().get(1).getThumbUrl(mApp), imageContent1
                        .getWidthTarget(), imageContent1.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgTripleThird,
                feedModel.getFeedContent().getListImage().get(2).getThumbUrl(mApp), imageContent1
                        .getWidthTarget(), imageContent1.getHeightTarget());

    }

    private void setViewTripleListener() {
        mImgTripleFirst.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 0);
            }
        });

        mImgTripleSecond.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 1);
            }
        });

        mImgTripleThird.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 2);
            }
        });
    }


    private void setViewQuadruple() {
        mViewDouble.setVisibility(View.GONE);
        mViewTriple.setVisibility(View.GONE);
        mViewQuadruple.setVisibility(View.VISIBLE);
        mViewQuintuple.setVisibility(View.GONE);

        FeedContent.ImageContent imageContent0 = feedModel.getFeedContent().getListImage().get(0);
        FeedContent.ImageContent imageContent1 = feedModel.getFeedContent().getListImage().get(1);
        if (imageContent0.getWidthTarget() == 0) {
            setWidthHeight(0.33f, true, padding, imageContent0);
        }
        if (imageContent1.getWidthTarget() == 0) {
            setWidthHeight(1, true, padding, imageContent1);
        }

//        ImageViewAwareTargetSize imgTarget0 = new ImageViewAwareTargetSize(mImgQuadrupleFirst, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget1 = new ImageViewAwareTargetSize(mImgQuadrupleSecond, imageContent1
//                .getWidthTarget(), imageContent1.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget2 = new ImageViewAwareTargetSize(mImgQuadrupleThird, imageContent1
//                .getWidthTarget(), imageContent1.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget3 = new ImageViewAwareTargetSize(mImgQuadrupleFour, imageContent1
//                .getWidthTarget(), imageContent1.getHeightTarget());

        //anh to
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuadrupleFirst,
                feedModel.getFeedContent().getListImage().get(0).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuadrupleSecond,
                feedModel.getFeedContent().getListImage().get(1).getThumbUrl(mApp), imageContent1
                        .getWidthTarget(), imageContent1.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuadrupleThird,
                feedModel.getFeedContent().getListImage().get(2).getThumbUrl(mApp), imageContent1
                        .getWidthTarget(), imageContent1.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuadrupleFour,
                feedModel.getFeedContent().getListImage().get(3).getThumbUrl(mApp), imageContent1
                        .getWidthTarget(), imageContent1.getHeightTarget());
    }

    private void setViewQuadrupleListener() {
        mImgQuadrupleFirst.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 0);
            }
        });

        mImgQuadrupleSecond.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 1);
            }
        });

        mImgQuadrupleThird.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 2);
            }
        });

        mImgQuadrupleFour.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 3);
            }
        });
    }

    private void setViewQuintuple() {
        mViewDouble.setVisibility(View.GONE);
        mViewTriple.setVisibility(View.GONE);
        mViewQuadruple.setVisibility(View.GONE);
        mViewQuintuple.setVisibility(View.VISIBLE);
        mViewMore.setVisibility(View.GONE);

        FeedContent.ImageContent imageContent0 = feedModel.getFeedContent().getListImage().get(0);
        if (imageContent0.getWidthTarget() == 0) {
            setWidthHeight(1, true, padding, imageContent0);
        }

//        ImageViewAwareTargetSize imgTarget0 = new ImageViewAwareTargetSize(mImgQuintupleFirst, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget1 = new ImageViewAwareTargetSize(mImgQuintupleSecond, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget2 = new ImageViewAwareTargetSize(mImgQuintupleThird, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget3 = new ImageViewAwareTargetSize(mImgQuintupleFour, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());
//        ImageViewAwareTargetSize imgTarget4 = new ImageViewAwareTargetSize(mImgQuintupleFive, imageContent0
//                .getWidthTarget(), imageContent0.getHeightTarget());


        mImageProfileBusiness.displayImageProfileForFeed(mImgQuintupleFirst,
                feedModel.getFeedContent().getListImage().get(0).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuintupleSecond,
                feedModel.getFeedContent().getListImage().get(1).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuintupleThird,
                feedModel.getFeedContent().getListImage().get(2).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuintupleFour,
                feedModel.getFeedContent().getListImage().get(3).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
        mImageProfileBusiness.displayImageProfileForFeed(mImgQuintupleFive,
                feedModel.getFeedContent().getListImage().get(4).getThumbUrl(mApp), imageContent0
                        .getWidthTarget(), imageContent0.getHeightTarget());
    }

    private void setViewQuintupleListener() {
        mImgQuintupleFirst.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 0);
            }
        });

        mImgQuintupleSecond.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 1);
            }
        });

        mImgQuintupleThird.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 2);
            }
        });

        mImgQuintupleFour.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 3);
            }
        });

        mImgQuintupleFive.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 4);
            }
        });
    }

    private void setViewQuintupleAndMore() {
        setViewQuintuple();
        setViewQuintupleListener();
        mViewMore.setVisibility(View.VISIBLE);
        int number = listImg.size() - 5;
        mTvwNumberMore.setText("+" + number);
        mViewMore.setOnClickListener(view -> {
            if (getOnMediaHolderListener() != null) {
                getOnMediaHolderListener().onClickImageItem(feedModel, 4);
            }
        });
    }

    public void setWidthHeight(float ratio, boolean isThumb, int padding, FeedContent.ImageContent imageContent) {
        int screenWidth = mApp.getWidthPixels() - padding;
        int screenHeight = mApp.getHeightPixels() - padding;
        int maxWidth, maxHeight;
        if (!isThumb) {
            maxWidth = 720;
            maxHeight = 1280;
        } else {
            maxWidth = 360;
            maxHeight = 640;
        }
        if (ratio >= 1) {   //width > height
            if (screenWidth > maxWidth) {
                screenWidth = maxWidth;
            }
            screenHeight = Math.round(screenWidth / ratio);

        } else {        //width < height
            if (screenHeight > maxHeight) {
                screenHeight = maxHeight;
            }
            screenWidth = Math.round(screenHeight * ratio);
        }
        Log.i(TAG, "setWidthHeight: width - " + screenWidth + " height - " +
                screenHeight);
        imageContent.setWidthTarget(screenWidth);
        imageContent.setHeightTarget(screenHeight);
    }
}

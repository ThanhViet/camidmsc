package com.metfone.selfcare.holder.message.typing;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.message.TypingMessageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.SearchQuery;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 9/27/2017.
 */

public class TypingSongHolder extends BaseViewHolder {
    private TypingMessageAdapter.TypingMessageListener mListener;
    private ApplicationController mApplication;
    private ImageView mImgThumb;
    private TextView mTvwSongName, mTvwSongSinger;

    public TypingSongHolder(ApplicationController application, View itemView, TypingMessageAdapter.TypingMessageListener listener) {
        super(itemView);
        this.mApplication = application;
        this.mListener = listener;
        mImgThumb = (ImageView) itemView.findViewById(R.id.holder_typing_song_thumb);
        mTvwSongName = (TextView) itemView.findViewById(R.id.holder_typing_song_name);
        mTvwSongSinger = (TextView) itemView.findViewById(R.id.holder_typing_song_singer);
    }

    @Override
    public void setElement(final Object obj) {
        if (obj instanceof SearchQuery) {
            setViewSearchSong((SearchQuery) obj);
        } else {
            setViewTopSong((MediaModel) obj);
        }
        getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSongClick(obj);
            }
        });
    }

    private void setViewTopSong(MediaModel item) {
        mTvwSongName.setText(item.getName());
        if (item.getSinger() != null) {
            mTvwSongSinger.setText(item.getSinger());
        } else {
            mTvwSongSinger.setText("");
        }
        mApplication.getAvatarBusiness().setSongAvatar(mImgThumb, item.getImage());
        /*if (item.isMonopoly()) {
            mTvwMonopoly.setVisibility(View.VISIBLE);
        } else {
            mTvwMonopoly.setVisibility(View.GONE);
        }*/
    }

    private void setViewSearchSong(SearchQuery item) {
        mTvwSongName.setText(item.getFullName());
        if (item.getFullSinger() != null) {
            mTvwSongSinger.setText(item.getFullSinger());
        } else {
            mTvwSongSinger.setText("");
        }
        String imageUrl = UrlConfigHelper.getInstance(mApplication).getDomainKeengMusicImage();
        if (!TextUtils.isEmpty(item.getImage()) && !TextUtils.isEmpty(imageUrl)) {
            imageUrl += item.getImage();
        } else {
            imageUrl = null;
        }
        mApplication.getAvatarBusiness().setSongAvatar(mImgThumb, imageUrl);
    }
}

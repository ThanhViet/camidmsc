package com.metfone.selfcare.holder.onmedia;

import android.content.Context;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.listeners.ClickListener;

/**
 * Created by thanhnt72 on 12/25/2015.
 */
public class OnMediaSongItemHolder extends AbsContentHolder implements View.OnClickListener {
    private static final String TAG = OnMediaSongItemHolder.class.getSimpleName();
    private Context mContext;
    private ClickListener.IconListener mCallBack;
    private Resources mRes;
    private Object entry;
    private TextView mTvwSong;
    private TextView mTvwSinger;
    private ImageView mImgThumb;
    private OnMediaSongItemHolder mHolder;
    private boolean hilight;

    public OnMediaSongItemHolder(Context context) {
        this.mContext = context;
        this.mRes = context.getResources();
        mHolder = this;
    }

    public OnMediaSongItemHolder(Context context, ClickListener.IconListener callBack) {
        this.mContext = context;
        this.mRes = context.getResources();
        this.mCallBack = callBack;
        mHolder = this;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        View convertView = layoutInflater.inflate(R.layout.holder_onmedia_song_item, parent, false);
        mTvwSong = (TextView) convertView.findViewById(R.id.tvw_name_song);
        mTvwSinger = (TextView) convertView.findViewById(R.id.tvw_name_singer);
        mImgThumb = (ImageView) convertView.findViewById(R.id.img_thumb);
        convertView.setTag(mHolder);
        setConvertView(convertView);
    }

    @Override
    public void setElemnts(Object obj) {
        entry = obj;
        setHolderSong((MediaModel) entry);
    }

    @Override
    public void onClick(View view) {
        if (mCallBack != null) {
            mCallBack.onIconClickListener(view, entry, Constants.ACTION.SEARCH_TRY_PLAY_SONG);
        }
    }

    public void setViewVideo(){
        mTvwSong.setMaxLines(2);
        mTvwSinger.setVisibility(View.GONE);
    }


    /*private void setViewHolder(MediaModel item) {
        mTvwSong.setText(item.getTitle());
        if (item.getSinger() != null) {
            mTvwSinger.setText(item.getSinger());
        } else {
            mTvwSinger.setText("");
        }
        ApplicationController mApplication = (ApplicationController) mContext.getApplicationContext();
        mApplication.getAvatarBusiness().setSongAvatar(mImgThumb, item.getIcon());
    }*/

    public void setHolderSong(MediaModel item) {
        mTvwSong.setText(item.getName());
        if (isHilight()) {
            mTvwSong.setTextColor(ContextCompat.getColor(mContext, R.color.bg_mocha));
        } else {
            mTvwSong.setTextColor(ContextCompat.getColor(mContext, R.color.text_song_name));
        }
        //singer
        if (item.getSinger() != null) {
            mTvwSinger.setText(item.getSinger());
        } else {
            mTvwSinger.setText("");
        }
        //img thumb
        if (item.getImage() != null) {
            ApplicationController mApplication = (ApplicationController) mContext.getApplicationContext();
            mApplication.getAvatarBusiness().setSongAvatar(mImgThumb, item.getImage());
        } else {
            mImgThumb.setImageResource(R.color.bg_onmedia_content_item);
        }
    }


    public boolean isHilight() {
        return hilight;
    }

    public void setHilight(boolean hilight) {
        this.hilight = hilight;
    }
}
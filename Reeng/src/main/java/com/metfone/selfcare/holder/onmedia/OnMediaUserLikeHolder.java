package com.metfone.selfcare.holder.onmedia;

import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.metfone.selfcare.adapter.UserActionAdapter.FeedUserInteractionListener;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 29/09/2015.
 */
public class OnMediaUserLikeHolder extends BaseViewHolder {
    private static final String TAG = OnMediaUserLikeHolder.class.getSimpleName();
    private FeedUserInteractionListener mCallBack;
    private ApplicationController mApplication;
    private ReengAccountBusiness mAccountBusiness;
    private ContactBusiness mContactBusiness;
    private AvatarBusiness mAvatarBusiness;
    private Resources mRes;
    //user item
    private CircleImageView mImgAvatar;
    private TextView mTvwName, mTvwAvatar;
    private Button mBtnChat;
    private UserInfo mEntry;
    private String mFriendJid, mFriendName, mFriendAvatarUrl;
    private PhoneNumber mFriendPhone;
    private boolean isMyUser;

    private View convertView;


    public OnMediaUserLikeHolder(View convertView, ApplicationController app, FeedUserInteractionListener callBack) {
        super(convertView);
        this.mRes = app.getResources();
        this.mApplication = app;
        this.mCallBack = callBack;
        this.mAccountBusiness = mApplication.getReengAccountBusiness();
        this.mContactBusiness = mApplication.getContactBusiness();
        this.mAvatarBusiness = mApplication.getAvatarBusiness();

        this.convertView = convertView;

        mBtnChat = (Button) convertView.findViewById(R.id.onmedia_button_chat);
        mTvwName = (TextView) convertView.findViewById(R.id.tvw_onmedia_user);
        mImgAvatar = (CircleImageView) convertView.findViewById(R.id.img_onmedia_avatar);
        mTvwAvatar = (TextView) convertView.findViewById(R.id.tvw_onmedia_avatar);
    }

    /*@Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        convertView = layoutInflater.inflate(R.layout.holder_onmedia_user_like, parent, false);
        mBtnChat = (Button) convertView.findViewById(R.id.onmedia_button_chat);
        mTvwName = (TextView) convertView.findViewById(R.id.tvw_onmedia_user);
        mImgAvatar = (CircleImageView) convertView.findViewById(R.id.img_onmedia_avatar);
        mTvwAvatar = (TextView) convertView.findViewById(R.id.tvw_onmedia_avatar);
        convertView.setTag(this);
        setConvertView(convertView);
    }*/

    /*@Override
    public void setElemnts(Object obj) {
        this.mEntry = (UserInfo) obj;
        setViewHolderContent();
        setViewListener();
    }*/

    private void setViewListener() {
        /*mImgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToProfile();
                *//*if (mCallBack != null)
                    mCallBack.onAvatarClick(mFriendPhone, mFriendJid, mFriendName);*//*
            }
        });*/
        mBtnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isMyUser && mCallBack != null)
                    mCallBack.onChatClick(mEntry);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallBack != null)
                    mCallBack.navigateToProfile(mEntry);
            }
        });

        /*mTvwName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToProfile();
            }
        });*/
    }

    /*private void navigateToProfile() {
        if (mEntry.getUser_type() != UserInfo.USER_ONMEDIA_NORMAL || !mEntry.isUserMocha()) {
            return;
        }
        if (isMyUser) {
            if (mCallBack != null)
                mCallBack.navigateToMyProfile();
        } else {
            if (mCallBack != null)
                mCallBack.navigateToFriendProfile(mFriendJid, mFriendName, mFriendAvatarUrl, mEntry.getStatus(), "", 0);
        }
    }*/

    private void setViewHolderContent() {
        int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
        if (mEntry != null) {
            mFriendJid = mEntry.getMsisdn();
            String myNumber = mAccountBusiness.getJidNumber();
            String myName = mAccountBusiness.getUserName();
            if (TextUtils.isEmpty(mFriendJid)) return;
            if (mFriendJid.equals(myNumber)) {
                mFriendName = myName;
                isMyUser = true;
                mBtnChat.setVisibility(View.INVISIBLE);
                mAvatarBusiness.setMyAvatar(mImgAvatar, mTvwAvatar, mTvwAvatar, mAccountBusiness.getCurrentAccount(), null);
            } else {
                isMyUser = false;
                if (mEntry.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL) {
                    mFriendPhone = mContactBusiness.getPhoneNumberFromNumber(mFriendJid);
                    if (mFriendPhone != null) {// luu danh ba
                        mFriendName = mFriendPhone.getName();
                        mAvatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, mFriendPhone, sizeAvatar);
                    } else {
                        if (TextUtils.isEmpty(mEntry.getName())) {
                            mFriendName = Utilities.hidenPhoneNumber(mFriendJid);
                        } else {
                            mFriendName = mEntry.getName();
                        }
                        if (mEntry.isUserMocha()) {
                            if ("0".equals(mEntry.getAvatar())) {
                                mFriendAvatarUrl = "";
                            } else {
                                mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(mEntry.getAvatar(), mFriendJid, sizeAvatar);
                            }
                        } else {
                            mFriendAvatarUrl = mEntry.getAvatar();
                        }
                        mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar, mFriendAvatarUrl, mFriendJid, mFriendName, sizeAvatar);
                    }


                    if (mEntry.isUserMocha()) {
                        mBtnChat.setVisibility(View.VISIBLE);
                        mBtnChat.setBackgroundResource(R.drawable.selector_rec_w_border_purple);
                        mBtnChat.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                        mBtnChat.setText(mRes.getString(R.string.chat));
                    } else {
                        mBtnChat.setVisibility(View.INVISIBLE);
                    /*if (!PhoneNumberHelper.getInstant().isViettelNumber(mEntry.getMsisdn()) ||
                            !mAccountBusiness.getCurrentAccount().isViettelNumber()) {
                        mBtnChat.setBackgroundResource(R.drawable.button_rec_purple);
                        mBtnChat.setTextColor(mRes.getColor(R.color.white));
                        mBtnChat.setText(mRes.getString(R.string.contact_view_invite));
                    } else {
                        mBtnChat.setBackgroundResource(R.drawable.button_white_with_purple_border);
                        mBtnChat.setTextColor(mRes.getColor(R.color.bg_mocha));
                        mBtnChat.setText(mRes.getString(R.string.chat));
                    }*/
                    }

                } else {
                    mBtnChat.setVisibility(View.INVISIBLE);
                    mFriendName = mEntry.getName();
                    mFriendAvatarUrl = mEntry.getAvatar();
                    mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar, mFriendAvatarUrl, mFriendJid, mFriendName, sizeAvatar);
                }
            }
            mTvwName.setText(mFriendName);
        } else {
            mBtnChat.setVisibility(View.INVISIBLE);
            mTvwName.setText("");
            mImgAvatar.setImageResource(R.drawable.ic_avatar_default);
        }

    }

    @Override
    public void setElement(Object obj) {
        this.mEntry = (UserInfo) obj;
        setViewHolderContent();
        setViewListener();
    }
}
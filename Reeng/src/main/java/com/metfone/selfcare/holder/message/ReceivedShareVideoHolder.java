package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.ProgressWheel;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 8/28/14.
 */
public class ReceivedShareVideoHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedShareVideoHolder.class.getSimpleName();
    private RoundedImageView content;
    private View vLoading;
    private ImageView iconAttachment;
    private TextView fileName;
    private ReengMessage message;
    private ProgressWheel progressWheel;
    private TextView tvwFileSize;
    private TextView tvwDuration;
    private RelativeLayout mLayoutMediaInfo;
    private RelativeLayout mLayoutImgPlay;

    public ReceivedShareVideoHolder(Context ctx, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        setContext(ctx);
    }

    public ReceivedShareVideoHolder(Context ctx) {
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_share_video_received, parent, false);
        initBaseHolder(rowView);
        content = (RoundedImageView) rowView
                .findViewById(R.id.message_detail_file_item_content);
        vLoading = rowView
                .findViewById(R.id.message_detail_file_loading);
        fileName = (TextView) rowView
                .findViewById(R.id.message_detail_file_item_file_name);
        iconAttachment = (ImageView) rowView
                .findViewById(R.id.message_detail_file_item_attachment);
        progressWheel = (ProgressWheel) rowView.findViewById(R.id.progress_loading);
        mLayoutMediaInfo = (RelativeLayout) rowView.findViewById(R.id.info_media);
        tvwFileSize = (TextView) rowView.findViewById(R.id.txt_media_size);
        tvwDuration = (TextView) rowView.findViewById(R.id.txt_media_duration);
        mLayoutImgPlay = (RelativeLayout) rowView.findViewById(R.id.layout_play_video);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        fileName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        tvwFileSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        tvwDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        if (!isQuickReply) {
            content.setVisibility(View.VISIBLE);
            fileName.setVisibility(View.GONE);
            iconAttachment.setVisibility(View.GONE);
            mLayoutMediaInfo.setVisibility(View.VISIBLE);
            tvwFileSize.setText(String.format(getContext().getString(R.string.file_size_in_mb), message.getSize() / 1024.0 / 1024.0));
            tvwDuration.setText(TimeHelper.getDuraionMediaFile(message.getDuration()));
            Log.i(TAG, "thanh---message: " + message.getId() + " path: " + message.getFilePath() + " type: " + message.getStatus()
                    + " thumb: " + message.getImageUrl());
            if (message.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD ||
                    message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
                vLoading.setVisibility(View.GONE);
                mLayoutImgPlay.setVisibility(View.VISIBLE);
                ImageLoaderManager.getInstance(getContext()).displayThumbnailVideo(message.getImageUrl(), content);
            } else if (message.getStatus() == ReengMessageConstant.STATUS_LOADING) {
                vLoading.setVisibility(View.VISIBLE);
                mLayoutImgPlay.setVisibility(View.GONE);
                ImageLoaderManager.getInstance(getContext()).displayThumbnailVideo(message.getImageUrl(), content);
                int percentProgress = 360 * message.getProgress() / 100;
                progressWheel.setProgress(percentProgress);
            } else if (message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
                vLoading.setVisibility(View.GONE);
                mLayoutImgPlay.setVisibility(View.VISIBLE);
                ImageLoaderManager.getInstance(getContext()).displayThumbnailVideo(message.getImageUrl(), content);
            } else {
                vLoading.setVisibility(View.GONE);
                mLayoutImgPlay.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(message.getVideoContentUri())) {
                    Log.i(TAG, "display video uri");
                    ImageLoaderManager.getInstance(getContext()).displayThumbnailVideo(message.getVideoContentUri(), content);
                } else if (!TextUtils.isEmpty(message.getFilePath())) {
                    Log.i(TAG, "display file path");
                    ImageLoaderManager.getInstance(getContext()).displayDetailOfMessage(message.getFilePath(), content, true);
                } else {
                    Log.i(TAG, "display thumb");
                    ImageLoaderManager.getInstance(getContext()).displayThumbnailVideo(message.getImageUrl(), content);
                }
            }
        } else {
            content.setVisibility(View.GONE);
            iconAttachment.setVisibility(View.VISIBLE);
            fileName.setVisibility(View.VISIBLE);
            fileName.setText(mContext.getString(R.string.person_chat_share_video));
            iconAttachment.setImageResource(R.drawable.ic_voice);
            vLoading.setVisibility(View.GONE);
            mLayoutImgPlay.setVisibility(View.GONE);
            mLayoutMediaInfo.setVisibility(View.GONE);
        }
    }
}
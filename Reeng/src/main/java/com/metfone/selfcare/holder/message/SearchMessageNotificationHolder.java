package com.metfone.selfcare.holder.message;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.PinMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TagHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.message.MessageConstants;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.EmoTextViewListChat;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by huongnd38 on 17/10/2018.
 */
public class SearchMessageNotificationHolder extends BaseMessageHolder {
    private String TAG = SearchMessageNotificationHolder.class.getSimpleName();
    private EmoTextViewListChat mTvwContent;
    private ImageView ivThumbNotify;
    private ApplicationController mApplication;
    private CircleImageView mImgNotificationThumb;
    private TextView mTvwTime;
    private TextView mBtnLeft, mBtnRight;
    private LinearLayout mLlButton;

    private SmartTextClickListener mSmartTextListener;
    private ReengMessage mEntry;
    private Resources mRes;

    public SearchMessageNotificationHolder(ApplicationController applicationController, SmartTextClickListener smartTextListener) {
        this.mSmartTextListener = smartTextListener;
        setContext(applicationController);
        mApplication = applicationController;
        mRes = applicationController.getResources();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_search_message_notifi, parent, false);
        initBaseHolder(rowView);
        mImgNotificationThumb = (CircleImageView) rowView.findViewById(R.id.message_notification_image_thumb);
        mTvwContent = (EmoTextViewListChat) rowView.findViewById(R.id.message_text_content);
        ivThumbNotify = rowView.findViewById(R.id.ivThumbNotify);
        mTvwTime = (TextView) rowView.findViewById(R.id.message_time);
        // deeplink
        mLlButton = (LinearLayout) rowView.findViewById(R.id.btn_deep_link_layout);
        mBtnLeft = rowView.findViewById(R.id.btn_deep_link_left);
        mBtnRight = rowView.findViewById(R.id.btn_deep_link_right);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        mEntry = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants
                .FONT_SIZE.LEVEL_2));
        drawDetail();
        setListener();
        // processSentInviteNotify(mEntry);
    }

    private void setContentText() {
        if (mEntry.getMessageType() == ReengMessageConstant.MessageType.pin_message) {
            String senderName;
            if (mEntry.getDirection() == ReengMessageConstant.Direction.send) {
                senderName = mRes.getString(R.string.you);
            } else {
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(mEntry.getSender());
                if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                    senderName = phoneNumber.getName();
                } else {                      //hien thi so dien thoai neu la group chat
                    NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(mEntry
                            .getSender());
                    if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                        senderName = nonContact.getNickName();
                    } else {
                        senderName = mEntry.getSender();
                    }
                }
            }

            if (mEntry.getSize() == PinMessage.ActionPin.ACTION_PIN.VALUE) {
                String content = String.format(mRes.getString(R.string.pin_msg_notify), senderName, getContentOfMessage(mEntry, mRes, mApplication));
                mTvwContent.setNormalText(content, mSmartTextListener, longClickListener);
            } else {
                String contentUnPin = String.format(mRes.getString(R.string.unpin_msg_notify), senderName);
                mTvwContent.setNormalText(contentUnPin, mSmartTextListener, longClickListener);
            }
        } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.lixi) {
            ThreadMessage threadMessage = mApplication.getMessageBusiness().getThreadById(mEntry.getThreadId());
            String content;
            if (threadMessage != null) {
                content = String.format(mRes.getString(R.string.content_notify_send_lixi), threadMessage
                        .getThreadName(), TextHelper.formatTextDecember(mEntry.getImageUrl()));
            } else {
                content = String.format(mRes.getString(R.string.content_notify_send_lixi), "", TextHelper.formatTextDecember(mEntry.getImageUrl()));
            }
            mTvwContent.setNormalText(content, mSmartTextListener, longClickListener);
            Log.i(TAG, "setNormalText: " + content);
        } else {
            mTvwContent.setEmoticon(getContentOfMessage(mEntry, mRes, mApplication), mEntry.getId(), mSmartTextListener, longClickListener, mEntry);
            Log.i(TAG, "setEmotion: " + mEntry.getContent());
        }
    }

    private void setLayoutContent() {
        mTvwTime.setVisibility(View.GONE);//TODO giao dien đang bị cắt time khi content dài, tam thoi off
        mTvwTime.setText(mEntry.getHour());
        if (mEntry.getMessageType() == ReengMessageConstant.MessageType.notification_fake_mo) {
            mLlButton.setVisibility(View.GONE);
            mBtnLeft.setVisibility(View.VISIBLE);
            mBtnRight.setVisibility(View.GONE);
            mTvwContent.setGravity(Gravity.LEFT);
            mImgNotificationThumb.setVisibility(View.GONE);
            if (TextUtils.isEmpty(mEntry.getImageUrl())) {
                mBtnLeft.setText(mRes.getString(R.string.accept));
            } else {
                mBtnLeft.setText(mEntry.getImageUrl());
            }
        } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.poll_action) {
            mLlButton.setVisibility(View.GONE);
            mBtnLeft.setVisibility(View.VISIBLE);
            mBtnRight.setVisibility(View.GONE);
            mTvwContent.setGravity(Gravity.LEFT);
            mImgNotificationThumb.setVisibility(View.GONE);
            mBtnLeft.setText(mRes.getString(R.string.poll_show_vote_detail));
        } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.lixi) {
            if (mEntry.getStatus() == ReengMessageConstant.STATUS_FAIL) {
                mLlButton.setVisibility(View.GONE);
                mBtnLeft.setVisibility(View.VISIBLE);
                mBtnLeft.setText(mRes.getString(R.string.retry));
            } else {
                mLlButton.setVisibility(View.GONE);
                mBtnLeft.setVisibility(View.GONE);
            }
            mBtnRight.setVisibility(View.GONE);
            //mTvwContent.setGravity(Gravity.CENTER);

            mImgNotificationThumb.setVisibility(View.GONE);
            mTvwTime.setVisibility(View.GONE);
        } else {
            /*if (TextUtils.isEmpty(mEntry.getImageUrl())) {
                mImgNotificationThumb.setVisibility(View.GONE);
            } else {
                mImgNotificationThumb.setVisibility(View.VISIBLE);
                mApplication.getAvatarBusiness().setGroupAvatarUrl(mImgNotificationThumb, getThreadMessage(), mEntry
                        .getImageUrl());
            }*/
            mImgNotificationThumb.setVisibility(View.GONE);
            drawDeepLinkDetail();
        }
    }

    private void drawDetail() {
        mBtnLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE
                .LEVEL_2));
        mBtnRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE
                .LEVEL_2));
        setContentText();
        setLayoutContent();
        setImageThumbNotify();
    }

    private void setImageThumbNotify() {
        if (mEntry.getMessageType() == ReengMessageConstant.MessageType.notification_fake_mo) {
            ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_default);
        } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.poll_action) {
            ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_vote);
        } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.lixi) {
            ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_default);
        } else {
            String type = mEntry.getFileName();
            if (TextUtils.isEmpty(type)) {
                ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_default);
            } else {
                if (MessageConstants.NOTIFY_TYPE.create.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_default);
                } else if (MessageConstants.NOTIFY_TYPE.invite.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_add_member);
                } else if (MessageConstants.NOTIFY_TYPE.join.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_add_member);
                } else if (MessageConstants.NOTIFY_TYPE.rename.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_change_name);
                } else if (MessageConstants.NOTIFY_TYPE.leave.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_leave);
                } else if (MessageConstants.NOTIFY_TYPE.kick.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_kick);
                } else if (MessageConstants.NOTIFY_TYPE.makeAdmin.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_set_admin);
                } else if (MessageConstants.NOTIFY_TYPE.removeAdmin.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_remove_admin);
                } else if (MessageConstants.NOTIFY_TYPE.groupPrivate.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_private);
                } else if (MessageConstants.NOTIFY_TYPE.groupPublic.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_group_public);
                } else if (MessageConstants.NOTIFY_TYPE.changeAvatar.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_img);
                } else if (MessageConstants.NOTIFY_TYPE.leaveMusic.name().equals(type)) {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_listen_music);
                } else {
                    ivThumbNotify.setImageResource(R.drawable.ic_notify_msg_default);
                }
            }
        }

    }

    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            if (getMessageInteractionListener() != null)
                getMessageInteractionListener().longClickBgrCallback(mEntry);
            return false;
        }
    };

    private void setListener() {
        mBtnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEntry.getMessageType() == ReengMessageConstant.MessageType.notification_fake_mo) {
                    getMessageInteractionListener().onFakeMoClick(mEntry);
                } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.poll_action) {
                    getMessageInteractionListener().onPollDetail(mEntry, false);
                } else if (mEntry.getMessageType() == ReengMessageConstant.MessageType.lixi) {
                    getMessageInteractionListener().retryClickCallBack(mEntry);
                } else {
                    getMessageInteractionListener().onDeepLinkClick(mEntry, mEntry.getDeepLinkLeftAction());
                }
            }
        });

        mBtnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onDeepLinkClick(mEntry, mEntry.getDeepLinkRightAction());
            }
        });
    }

    private void drawDeepLinkDetail() {
        if (!mEntry.initDeeplink()) {
            mEntry.initDeepLinkInfo(true);
        }
        if (!TextUtils.isEmpty(mEntry.getDeepLinkLeftAction())) {
            mLlButton.setVisibility(View.GONE);
            mTvwContent.setGravity(Gravity.LEFT);
            if (!TextUtils.isEmpty(mEntry.getDeepLinkRightAction())) {// 2 button
                mBtnLeft.setVisibility(View.VISIBLE);
                mBtnRight.setVisibility(View.VISIBLE);
                mBtnLeft.setText(mEntry.getDeepLinkLeftLabel());
                mBtnRight.setText(mEntry.getDeepLinkRightLabel());
            } else {// 1 button
                mBtnLeft.setVisibility(View.VISIBLE);
                mBtnRight.setVisibility(View.GONE);
                mBtnLeft.setText(mEntry.getDeepLinkLeftLabel());
            }
        } else {
            //mTvwContent.setGravity(Gravity.CENTER);
            mLlButton.setVisibility(View.GONE);
        }
    }

    public String getContentOfMessage(ReengMessage message, Resources res, ApplicationController mApp) {
        //last message
        ReengMessageConstant.MessageType type = message.getMessageType();
        if (type == ReengMessageConstant.MessageType.text) {
            ArrayList<TagMocha> listTag = message.getListTagContent();
            return TagHelper.getTextTagCopy(message.getContent(), listTag, mApp);
        } else if (type == ReengMessageConstant.MessageType.notification) {
            if (message.getContent() == null) return "";
            return message.getContent();
        } else if (type == ReengMessageConstant.MessageType.file) {
            return res.getString(R.string.file_message);
        } else if (type == ReengMessageConstant.MessageType.image) {
            return res.getString(R.string.image_message);
        } else if (type == ReengMessageConstant.MessageType.voicemail) {
            return res.getString(R.string.voice_message);
        } else if (type == ReengMessageConstant.MessageType.shareContact) {
            return res.getString(R.string.person_chat_share_contact);
        } else if (type == ReengMessageConstant.MessageType.shareVideo) {
            return res.getString(R.string.person_chat_share_video);
        } else if (type == ReengMessageConstant.MessageType.voiceSticker) {
            return res.getString(R.string.voice_sticker_message);
        } else if (type == ReengMessageConstant.MessageType.suggestShareMusic) {
            return res.getString(R.string.msg_suggest_share_music);
        } else if (type == ReengMessageConstant.MessageType.inviteShareMusic) {
            if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                MediaModel songModel = message.getSongModel(mApplication.getMusicBusiness());
                if (songModel == null) {
                    return message.getContent();
                }
                String songName = songModel.getName();
                return String.format(mRes.getString(R.string.invite_share_music_last_content), songName);
            } else {
                return message.getContent();
            }
        } else if (type == ReengMessageConstant.MessageType.shareLocation) {
            return res.getString(R.string.msg_share_location);
        } else if (type == ReengMessageConstant.MessageType.restore) {
            return res.getString(R.string.message_restored);
        } else if (type == ReengMessageConstant.MessageType.transferMoney) {
            return res.getString(R.string.msg_transfer_money);
        } else if (type == ReengMessageConstant.MessageType.image_link) {
            return res.getString(R.string.image_message);
        } else if (type == ReengMessageConstant.MessageType.bank_plus) {
            return message.getContent();
        } else if (type == ReengMessageConstant.MessageType.lixi) {
            return res.getString(R.string.last_msg_lixi);
        } else if (type == ReengMessageConstant.MessageType.pin_message) {
            return res.getString(R.string.message_pin);
        } else if (type == ReengMessageConstant.MessageType.call) {
            if (message.getDirection() == ReengMessageConstant.Direction.send) {
                if (message.getDuration() == 0) {
                    return res.getString(R.string.call_state_out_call);
                } else {
                    return message.getContent();
                }
            } else {
                if (message.getSongId() == CallHistoryConstant.STATE_REJECTED) {
                    return res.getString(R.string.call_state_rejected);
                } else if (message.getSongId() == CallHistoryConstant.STATE_MISS) {
                    return res.getString(R.string.call_state_miss_call);
                } else {
                    return message.getContent();
                }
            }
        } else if (type == ReengMessageConstant.MessageType.update_app) {
            return res.getString(R.string.message_content_update_app);
        } else {
            return message.getContent();
        }
    }
}
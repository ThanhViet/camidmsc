package com.metfone.selfcare.holder;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.EventMessage;
import com.metfone.selfcare.helper.TimeHelper;

/**
 * Created by toanvk2 on 5/28/15.
 */
public class EventMessageHolder extends AbsContentHolder {
    private static final String TAG = EventMessageHolder.class.getSimpleName();
    private Context mContext;
    private ApplicationController mApplication;
    private Resources mRes;
    private TextView mTvwName;
    private TextView mTvwTime;
    private int threadType;
    private EventMessageHolder mHolder;

    public EventMessageHolder(Context context, int threadType) {
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = context.getResources();
        this.threadType = threadType;
        mHolder = this;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        View convertView = layoutInflater.inflate(R.layout.holder_event_message, parent, false);
        mHolder.mTvwName = convertView.findViewById(R.id.event_message_name);
        mHolder.mTvwTime = convertView.findViewById(R.id.event_message_time);
        convertView.setTag(mHolder);
        setConvertView(convertView);
    }

    @Override
    public void setElemnts(Object obj) {
        setDetailView((EventMessage) obj);
    }

    private void setDetailView(EventMessage entry) {
        String friendName;
        if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            friendName = mApplication.getMessageBusiness().getFriendNameOfGroup(entry.getSender());
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            String sender = "";
            if (!TextUtils.isEmpty(entry.getSender()))
                sender = entry.getSender().split("@")[0];
            friendName = mApplication.getMessageBusiness().getFriendName(sender);
        } else {
            friendName = mApplication.getMessageBusiness().getFriendName(entry.getSender());
        }
        //Log.d(TAG, "setDetailView getSender=" + entry.getSender());
        //Log.d(TAG, "setDetailView=" + friendName);
        mTvwName.setText(friendName);
        if (entry.getStatus() == ReengMessageConstant.STATUS_DELIVERED) {
            mTvwTime.setText(mRes.getString(R.string.delivered) + " " +
                    TimeHelper.formatTimeEventMessage(entry.getTime()));
        } else {// seen
            mTvwTime.setText(mRes.getString(R.string.seen) + " " +
                    TimeHelper.formatTimeEventMessage(entry.getTime()));
        }
    }
}
package com.metfone.selfcare.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.StrangerSticky;
import com.metfone.selfcare.listeners.StrangerStickyInteractionListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by tungt on 3/7/2016.
 */
public class StrangerStickyHolder extends AbsContentHolder {
    private static final String TAG = StrangerStickyHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private Context mContext;
    private StrangerSticky mEntry;
    private ImageView mImgBanner;

    private StrangerStickyInteractionListener mListener;

    public StrangerStickyHolder(Context context, StrangerStickyInteractionListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.mApplication = (ApplicationController) context.getApplicationContext();
    }

    public void setListener(StrangerStickyInteractionListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
       /* View mHeaderStarRoomMusic = layoutInflater.inflate(R.layout.stranger_sticky_holder, parent, false);
        mImgBanner = (ImageView) mHeaderStarRoomMusic.findViewById(R.id.stranger_sticky_image);*/
        mImgBanner = (ImageView) layoutInflater.inflate(R.layout.stranger_sticky_holder, parent, false);
        mImgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onBannerClick(mEntry);
            }
        });
        mImgBanner.setTag(this);
        setConvertView(mImgBanner);
    }

    @Override
    public void setElemnts(Object obj) {
        mEntry = (StrangerSticky) obj;
        drawHolderDetail();
        //initLayoutParams();
    }

    private void drawHolderDetail() {
        Log.d(TAG, "drawHolderDetail");
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        //String title = mEntry.getTitle();
        avatarBusiness.setBannerGameImage(mImgBanner, mEntry.getImageUrl());
    }
}
package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

/**
 * Created by toanvk2 on 7/3/14.
 */
public class OfficerAccountViewHolder extends BaseContactHolder {
    private Context mContext;
    private OfficerAccount entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private RoundedImageView mImgAvatar;
    private EllipsisTextView mTvContactName;
    private EllipsisTextView mTvDescription;
    private TextView mTvwAvatar, mTvwCharecter;
    private FrameLayout mFrAvatar;
    private RelativeLayout mRelContent, mRelCharecter;


    public OfficerAccountViewHolder(View convertView, Context context, ClickListener.IconListener callBack) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mApplication = (ApplicationController) mContext.getApplicationContext();
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        //mLlInviteFriend = convertView.findViewById(R.id.item_contact_view_invite_friend);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
        mFrAvatar = convertView.findViewById(R.id.item_contact_view_avatar_frame);
        mRelContent = convertView.findViewById(R.id.item_contact_view_content_layout);
        mRelCharecter = convertView.findViewById(R.id.item_contact_view_charecter_layout);
        mTvwCharecter = convertView.findViewById(R.id.item_contact_view_charecter_text);
        mTvDescription = convertView.findViewById(R.id.item_description);
        mTvDescription.setVisibility(View.GONE);
    }

    @Override
    public void setElement(Object obj) {
        entry = (OfficerAccount) obj;
        if (entry == null) {
            return;
        }
        // draw
        if (entry.getServerId() != null)
            setOfficerView(entry);
        else
            setCharecterView(entry);
    }

    //set name, avatar,reeng state
    private void setDetailView(OfficerAccount entry) {
        // text
        mTvContactName.setText(entry.getName());
        if (entry.getDescription() != null) {
            mTvDescription.setVisibility(View.VISIBLE);
            mTvDescription.setText(entry.getDescription());
        } else {
            mTvDescription.setVisibility(View.GONE);
        }
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.VISIBLE);
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, size, entry.getServerId(), entry, false);
    }

    private void setCharecterView(OfficerAccount entry) {
        mRelCharecter.setVisibility(View.VISIBLE);
        mFrAvatar.setVisibility(View.GONE);
        mRelContent.setVisibility(View.GONE);
        mTvwCharecter.setText(entry.getName());
    }

    private void setOfficerView(OfficerAccount entry) {
        mRelCharecter.setVisibility(View.GONE);
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.VISIBLE);
        setDetailView(entry);
    }
}
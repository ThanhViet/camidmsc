package com.metfone.selfcare.holder.onmedia.feeds;

import android.content.res.Resources;
import android.graphics.Typeface;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.adapter.onmedia.ListUserActionAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.OnMediaHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.OnMediaHolderListener;
import com.metfone.selfcare.ui.TagTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.widget.CustomTypefaceSpan;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
public abstract class OMFeedBaseViewHolder extends BaseViewHolder {

    private static final String TAG = OMFeedBaseViewHolder.class.getSimpleName();
    private RelativeLayout mLayoutUserInfo;
    private AppCompatTextView mTvwNumbLike, mTvwNumberStatus, mTvwNumberShare, mTvwUserTime;     //mTvwCommentTime
    private ImageView mImgLike, mImgShare, mImgComment;
    private View viewLike, viewShare, viewComment;
    private ImageView mImgUserAvatar; //, mImgCommentAvatar
    private TextView mTvwUserAvatar;        //mTvwCommentAvatar
    private AppCompatTextView mTvwUserName;        //mTvwCommentUserName
    private AppCompatTextView mTvwUserAction;
    private TagTextView mTvwUserComment;
    private TagTextView mTvwUserStatus;    //, mTvwComment
    private AppCompatTextView mTvwVTF;
    private ImageView mTvwDot;
    private ApplicationController mApplication;
    private AvatarBusiness mAvatarBusiness;
    private ContactBusiness mContactBusiness;
    private FeedBusiness mFeedBusiness;
    private ReengAccount account;
    private Resources mRes;
    private String myName, myPhoneNumber;
    private OnMediaHolderListener onMediaHolderListener;

    private ImageView mImgThumb;
    private AppCompatTextView mTvwTitle, mTvwSite;
    private View mViewMediaItem;
    protected FeedModelOnMedia feedModel;
    private View mViewDivider;

    private UserInfo userInfo;

    private ImageView mImgMoreOption;
    private RelativeLayout mLayoutStatus;
    private View mViewFeedUserInfo;
//    private TagTextView mTvwComment;
//    private LinearLayout mLayoutUserComment;

    private View mLayoutUserState;
    private AppCompatTextView mTvwUserStateName;
    private AppCompatTextView mTvwUserStateAction;
    private UserInfo mUserFeedAction;

    private int maxLength;
    private TagMocha.OnClickTag onClickTag;
    private int size;

    private View mViewRefDeeplink;
    private AppCompatTextView mTvwDesRef, mTvwSubDesRef;
    private RoundTextView mTvwRef;
    private View mViewListUserAction;
    private ListUserActionAdapter mListUserActionAdapter;
    private RecyclerView mRecyclerViewUserAction;
    private AppCompatTextView mTvwTotalUserAction;
    private AppCompatTextView mTvwTimeViewListUserAction;

    private int widthImgNews, heightImgNews;
    private int sizeImgSong;
    private int widthImgVideo, heightImgVideo;

    private boolean hideTopAction = false;

    public OMFeedBaseViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView);
        this.mApplication = mApplication;
        mRes = mApplication.getResources();
        size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        widthImgNews = mRes.getDimensionPixelOffset(R.dimen.width_thumb_news);
        heightImgNews = mRes.getDimensionPixelOffset(R.dimen.height_thumb_news);
        sizeImgSong = mRes.getDimensionPixelSize(R.dimen.size_thumb_song);
        widthImgVideo = mApplication.getWidthPixels() - mRes.getDimensionPixelOffset(R.dimen.margin_more_content_20);
        if (widthImgVideo > 800) {
            widthImgVideo = 800;
        }
        heightImgVideo = Math.round(9f * ((float) widthImgVideo) / 16f);
    }

    public void setHideTopAction(boolean hideTopAction) {
        this.hideTopAction = hideTopAction;
    }

    public void initCommonView(View rowView) {
        mTvwNumbLike = rowView.findViewById(R.id.tvw_number_like);
        mImgLike = rowView.findViewById(R.id.img_like_feed);
        mImgShare = rowView.findViewById(R.id.img_share_feed);
        mImgComment = rowView.findViewById(R.id.img_comment_feed);
        mTvwNumberStatus = rowView.findViewById(R.id.tvw_number_comment);
        mTvwNumberShare = rowView.findViewById(R.id.tvw_number_share);
        mViewDivider = rowView.findViewById(R.id.view_divider_om);
        viewLike = rowView.findViewById(R.id.layout_like);
        viewShare = rowView.findViewById(R.id.layout_share);
        viewComment = rowView.findViewById(R.id.layout_comment);

        mImgUserAvatar = rowView.findViewById(R.id.img_onmedia_avatar);
        mTvwUserAvatar = rowView.findViewById(R.id.tvw_onmedia_avatar);
        mTvwUserName = rowView.findViewById(R.id.tvw_onmedia_user);
        mTvwUserAction = rowView.findViewById(R.id.tvw_onmedia_user_action);
        mTvwUserTime = rowView.findViewById(R.id.tvw_onmedia_time);
        mTvwUserStatus = rowView.findViewById(R.id.tvw_status);
        mTvwUserComment = rowView.findViewById(R.id.tvw_comment_feed);
        mImgMoreOption = rowView.findViewById(R.id.img_more_option);

//        mLayoutUserComment = (LinearLayout) rowView.findViewById(R.id.layout_user_comment);
//        mTvwComment = (TagTextView) rowView.findViewById(R.id.tvw_user_comment);

        mViewFeedUserInfo = rowView.findViewById(R.id.view_feed_user_info);
        mLayoutStatus = rowView.findViewById(R.id.layout_user_status);
        mLayoutUserInfo = rowView.findViewById(R.id.layout_user_info);
        mTvwVTF = rowView.findViewById(R.id.tvw_onmedia_vtf);
        mTvwDot = rowView.findViewById(R.id.tvw_onmedia_dot);

        mLayoutUserState = rowView.findViewById(R.id.layout_user_state);
        mTvwUserStateName = rowView.findViewById(R.id.tvw_user_state);
        mTvwUserStateAction = rowView.findViewById(R.id.tvw_user_state_action);

        mViewRefDeeplink = rowView.findViewById(R.id.layout_ref_deeplink_onmedia);
        mTvwDesRef = rowView.findViewById(R.id.tvw_des_deeplink);
        mTvwSubDesRef = rowView.findViewById(R.id.tvw_sub_des_deeplink);
        mTvwRef = rowView.findViewById(R.id.tvw_deeplink_onmedia);

        mViewListUserAction = rowView.findViewById(R.id.view_list_action);
        mRecyclerViewUserAction = rowView.findViewById(R.id.recyclerview_list_action);
        mTvwTotalUserAction = rowView.findViewById(R.id.tvw_total_action);
        mTvwTimeViewListUserAction = rowView.findViewById(R.id.tvw_onmedia_time_user_action);
        mListUserActionAdapter = new ListUserActionAdapter(mApplication);
        if (mRecyclerViewUserAction != null) {
            mRecyclerViewUserAction.setLayoutManager(new LinearLayoutManager(mApplication, LinearLayoutManager
                    .HORIZONTAL, false));
            mRecyclerViewUserAction.setAdapter(mListUserActionAdapter);
        }

    }

    public void initViewMediaHolder(View rowView) {
        mImgThumb = rowView.findViewById(R.id.img_media_item);
        mTvwTitle = rowView.findViewById(R.id.tvw_media_item_title);
        mTvwSite = rowView.findViewById(R.id.tvw_site);
        mViewMediaItem = rowView.findViewById(R.id.feed_content);

        /*mLayoutUserComment = (LinearLayout) rowView.findViewById(R.id.layout_onmedia_comment);
        mImgCommentAvatar = (CircleImageView) rowView.findViewById(R.id.img_comment_user);
        mTvwComment = (TagTextView) rowView.findViewById(R.id.tvw_comment_content);
        mTvwCommentAvatar = (TextView) rowView.findViewById(R.id.tvw_comment_avatar);
        mTvwCommentUserName = (TextView) rowView.findViewById(R.id.tvw_comment_name);
        mTvwCommentTime = (TextView) rowView.findViewById(R.id.tvw_comment_time);
        mTvwCommentTime.setVisibility(View.GONE);*/
    }

    protected void hideMoreOption(boolean isHide) {
        if (mImgMoreOption != null) {
            mImgMoreOption.setVisibility(isHide ? View.GONE : View.VISIBLE);
        }
    }

    protected void setBaseElements(Object obj) {
//        mViewDivider.setVisibility(View.GONE);
        feedModel = (FeedModelOnMedia) obj;
        userInfo = feedModel.getUserInfo();
        mContactBusiness = mApplication.getContactBusiness();
        mAvatarBusiness = mApplication.getAvatarBusiness();
        mFeedBusiness = mApplication.getFeedBusiness();
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        myName = account.getName();
        myPhoneNumber = account.getJidNumber();
        maxLength = mRes.getInteger(R.integer.max_length_name_onmedia);
        String itemType = "";
        String itemSubType = "";
        if (feedModel.getFeedContent() != null) {
            itemType = feedModel.getFeedContent().getItemType();
            itemSubType = feedModel.getFeedContent().getItemSubType();
        }

        if (userInfo != null) {
            if (mViewFeedUserInfo != null) mViewFeedUserInfo.setVisibility(View.VISIBLE);
            setLayoutStatus();
            setVisibleMoreOption();
        } else {
            if (mViewFeedUserInfo != null) mViewFeedUserInfo.setVisibility(View.GONE);
        }
        setupLayoutLikeShareComment(feedModel);
        setupLayoutRefDeepLink(feedModel);
        setUserClickListener();
        if (mTvwUserStatus != null) {
            mTvwUserStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, mRes.getDimension(R.dimen.mocha_text_size_level_2_5));
            mTvwUserStatus.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (onMediaHolderListener != null) {
                        onMediaHolderListener.onLongClickStatus(feedModel);
                    }
                    return false;
                }
            });
        }
        if (OnMediaHelper.isFeedViewAudio(feedModel)
                || FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                || FeedContent.ITEM_TYPE_AUDIO.equals(itemType)
                || FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) && FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType))
                ) {
            setViewMediaItem();
        } else if (FeedContent.ITEM_TYPE_PROFILE_STATUS.equals(itemType)
                || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) &&
                FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(itemSubType))
                ) {
            if (mViewMediaItem != null) mViewMediaItem.setVisibility(View.GONE);
            if (mViewDivider != null) mViewDivider.setVisibility(View.VISIBLE);
            if (mTvwUserStatus != null)
                mTvwUserStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, mRes.getDimension(R.dimen.mocha_text_size_level_5));
        }
//                if (mLayoutUserComment != null) mLayoutUserComment.setVisibility(View.GONE);
        if (mImgMoreOption != null) {
            mImgMoreOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        onMediaHolderListener.onClickMoreOption(feedModel);
                    }
                }
            });
        }
        if (hideTopAction) {
            if (mLayoutUserState != null) mLayoutUserState.setVisibility(View.GONE);
        }
    }

    private void setVisibleMoreOption() {
        String numberActioner = feedModel.getUserInfo().getMsisdn();
        if (numberActioner.equals(mApplication.getReengAccountBusiness().getJidNumber())) {
            hideMoreOption(false);
        } else {
            String itemType = feedModel.getFeedContent().getItemType();
            String itemSubType = feedModel.getFeedContent().getItemSubType();
            if (OnMediaHelper.isFeedViewAudio(feedModel)
                    || OnMediaHelper.isFeedViewMovie(feedModel)
                    || FeedContent.ITEM_TYPE_NEWS.equals(itemType)
                    || FeedContent.ITEM_TYPE_VIDEO.equals(itemType)
                    || FeedContent.ITEM_TYPE_AUDIO.equals(itemType)
                    || FeedContent.ITEM_TYPE_IMAGE.equals(itemType)
                    || (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType) && FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType))
                    ) {
                hideMoreOption(false);
            } else {
                hideMoreOption(true);
            }
        }
    }

    private void setupLayoutRefDeepLink(FeedModelOnMedia feedModel) {
        //todo set layout Ref deep link
        if (mViewRefDeeplink != null) {
            if ((OnMediaHelper.isFeedViewMovie(feedModel) || OnMediaHelper.isFeedViewAudio(feedModel))) {
                mViewRefDeeplink.setVisibility(View.GONE);
                return;
            }
            final FeedContent feedContent = feedModel.getFeedContent();
            if (feedContent != null
                    && !TextUtils.isEmpty(feedContent.getDescDeeplink())
                    && !TextUtils.isEmpty(feedContent.getLeftLabel())
                    && !TextUtils.isEmpty(feedContent.getLeftDeeplink())) {
                if (!TextUtils.isEmpty(feedContent.getPackageAndroid())) {
                    if (Utilities.isAppInstalled(mApplication, feedContent.getPackageAndroid())) {
                        mViewRefDeeplink.setVisibility(View.GONE);
                        feedContent.setLeftDeeplink("");
                    } else {
                        processDeepLinkFeed(feedContent, false);
                    }
                } else {
                    processDeepLinkFeed(feedContent, true);
                }
            } else {
                mViewRefDeeplink.setVisibility(View.GONE);
            }
        }
    }

    private void processDeepLinkFeed(final FeedContent feedContent, final boolean clickDeepLink) {
        mViewRefDeeplink.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(feedContent.getSubDescDeeplink())) {
            mTvwSubDesRef.setVisibility(View.GONE);
        } else {
            mTvwSubDesRef.setVisibility(View.VISIBLE);
            mTvwSubDesRef.setText(feedContent.getSubDescDeeplink());
        }
        mTvwDesRef.setText(feedContent.getDescDeeplink());
        mTvwRef.setText(feedContent.getLeftLabel());
        mTvwRef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickDeepLink)
                    getOnMediaHolderListener().onDeepLinkClick(feedModel, feedContent.getLeftDeeplink());
                else
                    getOnMediaHolderListener().openPlayStore(feedModel, feedContent.getPackageAndroid());
            }
        });
    }

    private void setViewMediaItem() {
        if (FeedContent.ITEM_TYPE_VIDEO.equals(feedModel.getFeedContent().getItemType())) {
            return;
        }
        if (mViewMediaItem != null) {
            mViewMediaItem.setVisibility(View.VISIBLE);
            mViewMediaItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        onMediaHolderListener.onClickMediaItem(feedModel);
                    }
                }
            });
        }
        FeedContent feedContent = feedModel.getFeedContent();
        String imageUrl = feedContent.getImageUrl();
        String type = feedContent.getItemType();
        String subType = feedContent.getItemSubType();
        String title = feedContent.getItemName();
        String site = feedContent.getSite();
        if (mImgThumb != null) {
            //1 so type ko co image
            if (TextUtils.isEmpty(imageUrl)) {
                if (type.equals(FeedContent.ITEM_TYPE_NEWS) ||
                        (type.equals(FeedContent.ITEM_TYPE_SOCIAL)
                                && subType.equals(FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK))) {
                    if (mTvwTitle != null) mTvwTitle.setMaxLines(1);
                    mImgThumb.setVisibility(View.GONE);
                } else {
                    if (mTvwTitle != null) mTvwTitle.setMaxLines(2);
                    mImgThumb.setVisibility(View.VISIBLE);
                }
            } else {
                mImgThumb.setVisibility(View.VISIBLE);
                if (mTvwTitle != null) mTvwTitle.setMaxLines(2);
                if (OnMediaHelper.isFeedViewAudio(feedModel)) {
                    ImageLoaderManager.getInstance(mApplication).setImageFeedsSong(mImgThumb,
                            imageUrl, sizeImgSong, sizeImgSong);
                } else if (FeedContent.ITEM_TYPE_VIDEO.equals(type)) {
                    ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgThumb,
                            imageUrl, widthImgVideo, heightImgVideo);
                } else if (type.equals(FeedContent.ITEM_TYPE_NEWS)
                        || (type.equals(FeedContent.ITEM_TYPE_SOCIAL) && subType.equals(FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK))) {
                    int maxLine = mRes.getInteger(R.integer.max_line_title_onmedia);
                    if (mTvwTitle != null) mTvwTitle.setMaxLines(maxLine);
                    ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgThumb, feedContent.getImageUrl(),
                            widthImgNews, heightImgNews);
                } else {
                    Log.i(TAG, "type moi: " + type);
                    ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgThumb,
                            imageUrl);
                }
            }

            mImgThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        onMediaHolderListener.onClickMediaItem(feedModel);
                    }
                }
            });
        }
        if (TextUtils.isEmpty(title)) {
            title = site;
        }
        if (mTvwTitle != null) {
            mTvwTitle.setText(title);
            mTvwTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        onMediaHolderListener.onClickMediaItem(feedModel);
                    }
                }
            });
        }

        if (mTvwSite != null) {
            mTvwSite.setText(site);
            mTvwSite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        onMediaHolderListener.onClickMediaItem(feedModel);
                    }
                }
            });
        }
    }

    private void setLayoutStatus() {
        Log.i(TAG, "setLayoutStatus: " + feedModel.getFeedContent().getUrl());
        mUserFeedAction = feedModel.getFeedContent().getUserInfo();
        mTvwUserComment.setVisibility(View.GONE);
        if (mUserFeedAction != null) {
            mLayoutUserState.setVisibility(View.VISIBLE);
            Log.i(TAG, "setLayoutStatus: mUserFeedAction != null" + feedModel.getFeedContent().getUrl());
            setLayoutStatusWith2UserInfo();
        } else {
            mLayoutUserState.setVisibility(View.GONE);
            Log.i(TAG, "setLayoutStatus: mUserFeedAction null" + feedModel.getFeedContent().getUrl());
            setNormalLayoutUserInfo(userInfo, false);
        }
    }

    private void setLayoutStatusWith2UserInfo() {

        //set view o tren truoc (abc da binh luan...)
        if (userInfo != null) {
            if (feedModel.getActionType() == FeedModelOnMedia.ActionLogApp.COMMENT ||
                    feedModel.getActionType() == FeedModelOnMedia.ActionLogApp.LIKE ||
                    feedModel.getActionType() == FeedModelOnMedia.ActionLogApp.SHARE) {
                final String phoneNumber = userInfo.getMsisdn();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    if (isMyComment(phoneNumber)) {
                        setTextActionType(myName, mTvwUserStateName, mTvwUserStateAction, false);
                    } else {
                        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(phoneNumber);
                        if (mPhoneNumber != null) {
//                            Log.i(TAG, "avatar: " + userInfo.getAvatar());
                            setTextActionType(mPhoneNumber.getName(), mTvwUserStateName, mTvwUserStateAction, false);
                        } else {
                            String nameFriend;
                            if (TextUtils.isEmpty(userInfo.getName())) {
                                nameFriend = Utilities.hidenPhoneNumber(phoneNumber);
                            } else {
                                nameFriend = userInfo.getName();
                            }
                            setTextActionType(nameFriend, mTvwUserStateName, mTvwUserStateAction, false);
                        }
                    }
                } else {
                    Log.d(TAG, "sao lai vao day dc @@ phoneNumber null");
                }

                if (feedModel.getActionType() == FeedModelOnMedia.ActionLogApp.COMMENT) {
                    String status = feedModel.getUserStatus();
                    if (TextUtils.isEmpty(status)) {
                        mTvwUserComment.setVisibility(View.GONE);
                    } else {
                        mTvwUserComment.setVisibility(View.VISIBLE);
                        if (feedModel.getListTag() == null || feedModel.getListTag().isEmpty()) {
                            mTvwUserComment.setEmoticon(mApplication, status, status.hashCode(), status);
                        } else {
                            mTvwUserComment.setEmoticonWithTag(mApplication, status,
                                    status.hashCode(), status, feedModel.getListTag(), onClickTag);
                        }
                    }
                } else {
                    mTvwUserComment.setVisibility(View.GONE);
                }
            } else {
                mLayoutUserState.setVisibility(View.GONE);
            }
        }
        //set view binh thuong
        setNormalLayoutUserInfo(mUserFeedAction, true);
    }

    private void setNormalLayoutUserInfo(UserInfo mUserInfo, boolean isTextActionFeed) {
        if (mUserInfo != null) {
            mViewFeedUserInfo.setVisibility(View.VISIBLE);
            if (mUserInfo.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL) {
                mViewListUserAction.setVisibility(View.GONE);
                mLayoutStatus.setVisibility(View.VISIBLE);
                mTvwVTF.setVisibility(View.GONE);
                mTvwDot.setVisibility(View.GONE);
                mTvwUserName.setCompoundDrawables(null, null, null, null);
                final String phoneNumber = mUserInfo.getMsisdn();
                if (!TextUtils.isEmpty(phoneNumber)) {
                    if (isMyComment(phoneNumber)) {
                        setTextActionType(myName, mTvwUserName, mTvwUserAction, isTextActionFeed);
                        mAvatarBusiness.setMyAvatar(mImgUserAvatar, mTvwUserAvatar, mTvwUserAvatar, account, null);
                    } else {
                        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(phoneNumber);
                        if (mPhoneNumber != null) {
//                            Log.i(TAG, "avatar: " + userInfo.getAvatar());
                            setTextActionType(mPhoneNumber.getName(), mTvwUserName, mTvwUserAction, isTextActionFeed);
                            mAvatarBusiness.setPhoneNumberAvatar(mImgUserAvatar, mTvwUserAvatar, mPhoneNumber, size);
                        } else {
                            String nameFriend;
                            if (TextUtils.isEmpty(mUserInfo.getName())) {
                                nameFriend = Utilities.hidenPhoneNumber(phoneNumber);
                            } else {
                                nameFriend = mUserInfo.getName();
                            }
                            setTextActionType(nameFriend, mTvwUserName, mTvwUserAction, isTextActionFeed);

                            String mFriendAvatarUrl;
                            if (mUserInfo.isUserMocha()) {
                                if ("0".equals(mUserInfo.getAvatar())) {
                                    mFriendAvatarUrl = "";
                                } else {
                                    mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(mUserInfo.getAvatar(), mUserInfo
                                            .getMsisdn(), size);
                                }
                            } else {
                                mFriendAvatarUrl = mUserInfo.getAvatar();
                            }
                            mAvatarBusiness.setAvatarOnMedia(mImgUserAvatar, mTvwUserAvatar,
                                    mFriendAvatarUrl, mUserInfo.getMsisdn(), nameFriend, size);
                        }
                    }
                } else {
                    Log.d(TAG, "sao lai vao day dc @@ phoneNumber null");
                }
            } else {        //user dac biet
                Log.i(TAG, "userInfo.getUser_type(): " + mUserInfo.getUser_type());
                ArrayList<UserInfo> listUserAction = feedModel.getFeedContent().getListUserAction();
                if (listUserAction != null && !listUserAction.isEmpty()) {
                    mViewListUserAction.setVisibility(View.VISIBLE);
                    mListUserActionAdapter.setListUserInfo(listUserAction);
                    mListUserActionAdapter.setListener(getOnMediaHolderListener());
                    mListUserActionAdapter.notifyDataSetChanged();
                    int totalAction = feedModel.getFeedContent().getTotalAction();
                    if (totalAction > 0) {
                        mTvwTotalUserAction.setVisibility(View.VISIBLE);
                        String textTotalAction;
                        if (totalAction > 1) {
                            textTotalAction = String.format(mRes.getString(R.string.title_peoples_interested),
                                    String.valueOf(totalAction));
                        } else {
                            textTotalAction = String.format(mRes.getString(R.string.title_people_interested),
                                    String.valueOf(totalAction));
                        }
                        mTvwTotalUserAction.setText(textTotalAction);
                    } else {
                        mTvwTotalUserAction.setVisibility(View.GONE);
                    }
                } else {
                    mViewListUserAction.setVisibility(View.GONE);
                }
                setViewAvatarOA(mUserInfo, isTextActionFeed);

            }
//            Log.i(TAG, "status: " + feedModel.getUserStatus());
            if (isFeedFromProfile(feedModel)) {
                String status = feedModel.getFeedContent().getDescription();
                if (!TextUtils.isEmpty(status)) {
                    mTvwUserStatus.setVisibility(View.VISIBLE);
                    mTvwUserStatus.setEmoticon(mApplication, status, status.hashCode(), status);
                } else {
                    mTvwUserStatus.setVisibility(View.GONE);
                }
            } else if (feedModel.getFeedContent().getItemType().equals(FeedContent.ITEM_TYPE_SOCIAL)) {
                String status = feedModel.getFeedContent().getContentStatus();
                Log.i(TAG, "status type:" + status);

                /*if(feedModel.getActionType()== FeedModelOnMedia.ActionLogApp.COMMENT){
                    if(TextUtils.isEmpty(status)) status = feedModel.getUserStatus();
                }*/
                if (!TextUtils.isEmpty(status)) {
                    mTvwUserStatus.setVisibility(View.VISIBLE);
                    if (feedModel.getFeedContent().getContentListTag() == null
                            || feedModel.getFeedContent().getContentListTag().isEmpty()) {
                        mTvwUserStatus.setEmoticon(mApplication, status, status.hashCode(), status);
                    } else {
                        mTvwUserStatus.setEmoticonWithTag(mApplication, status,
                                status.hashCode(), status, feedModel.getFeedContent().getContentListTag(), onClickTag);
                    }
                } else {
                    mTvwUserStatus.setVisibility(View.GONE);
                }
            } else {
                Log.i(TAG, "setNormalLayoutUserInfo: " + feedModel.getFeedContent().getUrl() + " usersts: " + feedModel.getUserStatus());
                if (!TextUtils.isEmpty(feedModel.getUserStatus())) {
                    mTvwUserStatus.setVisibility(View.VISIBLE);
                    String status = feedModel.getUserStatus();
                    if (feedModel.getListTag() == null || feedModel.getListTag().isEmpty()) {
                        mTvwUserStatus.setEmoticon(mApplication, status, status.hashCode(), status);
                    } else {
                        mTvwUserStatus.setEmoticonWithTag(mApplication, status,
                                status.hashCode(), status, feedModel.getListTag(), onClickTag);
                    }
                } else {
                    mTvwUserStatus.setVisibility(View.GONE);
                }
            }
            long timeFeed;
            if (mUserFeedAction != null) {
                timeFeed = feedModel.getFeedContent().getStamp();
            } else {
                timeFeed = feedModel.getTimeStamp();
            }
//            Log.e(TAG, "setNormalLayoutUserInfo: "+TimeHelper.caculateTimeFeed(mApplication, timeFeed, mFeedBusiness.getDeltaTimeServer()));
            mTvwUserTime.setText(TimeHelper.caculateTimeFeed(mApplication, timeFeed, mFeedBusiness.getDeltaTimeServer()));
            mTvwTimeViewListUserAction.setText(TimeHelper.caculateTimeFeed(mApplication, timeFeed,
                    mFeedBusiness.getDeltaTimeServer()));
        } else {
            mViewFeedUserInfo.setVisibility(View.GONE);
            Log.d(TAG, "sao lai vao day dc @@ user null: " + feedModel.getFeedContent().getUrl());
        }
    }

    private void setViewAvatarOA(UserInfo mUserInfo, boolean isTextActionFeed) {
        mLayoutStatus.setVisibility(View.VISIBLE);
        if (mUserInfo.getUser_type() == UserInfo.USER_ONMEDIA_VTF) {
            mTvwVTF.setVisibility(View.VISIBLE);
            mTvwDot.setVisibility(View.VISIBLE);
            mTvwUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else if (mUserInfo.getUser_type() == UserInfo.USER_ONMEDIA_VERIFY) {
            mTvwUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checkbox_selected, 0);
            mTvwVTF.setVisibility(View.GONE);
            mTvwDot.setVisibility(View.GONE);
        } else {
            mTvwVTF.setVisibility(View.GONE);
            mTvwDot.setVisibility(View.GONE);
            mTvwUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        setTextActionType(mUserInfo.getName(), mTvwUserName, mTvwUserAction, isTextActionFeed);
        ImageLoaderManager.getInstance(mApplication).setAvatarOfficialOnMedia(mImgUserAvatar, mUserInfo
                .getAvatar(), size);
    }

    private void setTextActionType(String name, TextView mTvwName, TextView mTvwAction, boolean isTextActionFeed) {
        String action = "";
        if (isTextActionFeed) {
            FeedContent feedContent = feedModel.getFeedContent();
            if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM)) {
                action = mRes.getString(R.string.onmedia_update_album);
//                name = TextHelper.getSubLongText(name, maxLength);
            } else if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR)) {
                action = mRes.getString(R.string.onmedia_update_avatar);
//                name = TextHelper.getSubLongText(name, maxLength);
            } else if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_COVER)) {
                action = mRes.getString(R.string.onmedia_update_cover);
//                name = TextHelper.getSubLongText(name, maxLength);
            } else if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_STATUS)) {
                action = mRes.getString(R.string.onmedia_update_status);
//                name = TextHelper.getSubLongText(name, maxLength);
            } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(feedModel.getFeedContent().getItemType())
                    && FeedContent.ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO.equals(feedModel.getFeedContent().getItemSubType())) {
                action = mRes.getString(R.string.onmedia_publish_video);
//                name = TextHelper.getSubLongText(name, maxLength);
            } else if (OnMediaHelper.isFeedViewChannel(feedContent)
                    && FeedModelOnMedia.ActionLogApp.FOLLOW.equals(feedModel.getActionType())) {
                action = mRes.getString(R.string.onmedia_follow_channel);
//                name = TextHelper.getSubLongText(name, maxLength);
            }
        } else {
            switch (feedModel.getActionType()) {
                case LIKE:
//                    name = TextHelper.getSubLongText(name, maxLength);
                    action = mRes.getString(R.string.connections_cung_thich);
                    break;
                case COMMENT:
//                    name = TextHelper.getSubLongText(name, maxLength);
                    action = mRes.getString(R.string.connections_feed_comment);
                    break;
                case SHARE:
//                    name = TextHelper.textBoldWithHTML(name);
                    if (isFeedFromProfile(feedModel)) {
                        action = mRes.getString(R.string.connections_cung_chia_se);
                    }
//                textActionType = textActionType + " " + mRes.getString(R.string.connections_cung_chia_se);
                    break;
                case POST:
//                    name = TextHelper.textBoldWithHTML(name);
                    if (FeedContent.ITEM_TYPE_SOCIAL.equals(feedModel.getFeedContent().getItemType())
                            && FeedContent.ITEM_SUB_TYPE_SOCIAL_PUBLISH_VIDEO.equals(feedModel.getFeedContent().getItemSubType()))
                        action = mRes.getString(R.string.onmedia_publish_video);
                    break;
                default:
                    break;
            }
        }
//        mTvwName.setText(TextHelper.fromHtml(name));
        mTvwAction.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(action)) {
            Typeface fontMedium = ResourcesCompat.getFont(mApplication, R.font.sf_ui_text_medium);
            Typeface fontRegular = ResourcesCompat.getFont(mApplication, R.font.sf_ui_text_regular);
            SpannableStringBuilder SS = new SpannableStringBuilder(name + " " + action);
            SS.setSpan(new CustomTypefaceSpan("", fontMedium), 0, name.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", fontRegular), name.length() + 1, name.length() + 1 + action.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            mTvwName.setText(SS);
        } else {
            Typeface fontMedium = ResourcesCompat.getFont(mApplication, R.font.sf_ui_text_medium);
            SpannableStringBuilder SS = new SpannableStringBuilder(name);
            SS.setSpan(new CustomTypefaceSpan("", fontMedium), 0, name.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            mTvwName.setText(SS);
        }

    }

    private boolean isMyComment(String phoneNumber) {
//        Log.i(TAG, "msisdn: " + phoneNumber);
        return myPhoneNumber.equals(phoneNumber);
    }

    private void setUserClickListener() {
        //todo set user click listener
        if (mImgUserAvatar != null) {
            mImgUserAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        if (mUserFeedAction != null && !TextUtils.isEmpty(mUserFeedAction.getMsisdn())) {
                            onMediaHolderListener.onClickUser(mUserFeedAction);
                        } else if (userInfo != null && !TextUtils.isEmpty(userInfo.getMsisdn())) {
                            onMediaHolderListener.onClickUser(userInfo);
                        }
                    }

                }
            });
        }
        if (mLayoutUserInfo != null) {
            mLayoutUserInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onMediaHolderListener != null) {
                        if (mUserFeedAction != null && !TextUtils.isEmpty(mUserFeedAction.getMsisdn())) {
                            onMediaHolderListener.onClickUser(mUserFeedAction);
                        } else if (userInfo != null && !TextUtils.isEmpty(userInfo.getMsisdn())) {
                            onMediaHolderListener.onClickUser(userInfo);
                        }
                    }
                }
            });
        }
        if (mLayoutUserState != null && mLayoutUserState.getVisibility() == View.VISIBLE) {
            mLayoutUserState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onMediaHolderListener != null && userInfo != null) {
                        onMediaHolderListener.onClickUser(userInfo);
                    }
                }
            });
        }
    }

    public OnMediaHolderListener getOnMediaHolderListener() {
        return onMediaHolderListener;
    }

    public void setOnMediaHolderListener(OnMediaHolderListener onMediaHolderListener) {
        this.onMediaHolderListener = onMediaHolderListener;
    }

    public TagMocha.OnClickTag getOnClickTag() {
        return onClickTag;
    }

    public void setOnClickTag(TagMocha.OnClickTag onClickTag) {
        this.onClickTag = onClickTag;
    }

    protected boolean isFeedFromProfile(FeedModelOnMedia feed) {
        String feedType = feed.getFeedContent().getItemType();
        return feedType.equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_COVER) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_STATUS);
    }

    protected boolean isFeedSocialStatus(FeedModelOnMedia feed) {
        String type = feed.getFeedContent().getItemType();
        String subType = feed.getFeedContent().getItemSubType();
        return type.equals(FeedContent.ITEM_TYPE_SOCIAL) && subType.equals(FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS);
    }

    protected void setupLayoutLikeShareComment(final FeedModelOnMedia feedModel) {
        //todo set layout like, share, comment
        if (feedModel != null && feedModel.getFeedContent() != null) {
//            if (!FeedContent.ITEM_TYPE_VIDEO.equals(feedModel.getFeedContent().getItemType())) {
            if (mTvwNumbLike != null)
                mTvwNumbLike.setText(Utilities.shortenLongNumber(feedModel.getFeedContent().getCountLike()));
            if (mTvwNumberStatus != null)
                mTvwNumberStatus.setText(Utilities.shortenLongNumber(feedModel.getFeedContent().getCountComment()));
            if (mTvwNumberShare != null)
                mTvwNumberShare.setText(Utilities.shortenLongNumber(feedModel.getFeedContent().getCountShare()));
            if (mImgLike != null) {
                if (feedModel.getIsLike() == 1) {
                    mImgLike.setImageResource(R.drawable.ic_v5_heart_active);
                } else {
                    mImgLike.setImageResource(R.drawable.ic_v5_heart_normal);
                }
                if (viewLike != null)
                    viewLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (onMediaHolderListener != null) {
                                onMediaHolderListener.onClickLikeFeed(feedModel);
                            }
                        }
                    });
            }
            if (viewShare != null) {
                viewShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onMediaHolderListener != null) {
                            onMediaHolderListener.onClickShareFeed(feedModel);
                        }
                    }
                });
            }
            if (viewComment != null) {
                viewComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onMediaHolderListener != null) {
                            onMediaHolderListener.onClickCommentFeed(feedModel);
                        }
                    }
                });
            }
        }
//        }
    }

    public ApplicationController getApplication() {
        return mApplication;
    }

    public Resources getRes() {
        return mRes;
    }
}


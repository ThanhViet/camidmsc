package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 27-Nov-14.
 */
public class ReceivedInviteMusicHolder extends BaseMessageHolder {

    private static final String TAG = ReceivedInviteMusicHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private ReengMessage message;
    private MediaModel mediaModel;
    private TextView mTvwTitle;
    private TextView mTvwSong;
    private TextView mTvwSinger;
    private TextView mTvwCountDown;
    private Button mBtnAccept, mBtnAcceptGroup;
    private Button mBtnReInvite, mBtnTryPlay;
    private Button mBtnViaFB;
    private ImageView mImgAvtSong;
    private RelativeLayout mLayoutReinvite;
    private RelativeLayout mLayoutUserAction, mLayoutUserGroupAction;
    private ImageButton mIbnGiftCrbt;

    public ReceivedInviteMusicHolder(ApplicationController app, MessageInteractionListener listener) {
        setContext(app);
        mApplication = app;
        setMessageInteractionListener(listener);
    }

    public ReceivedInviteMusicHolder(ApplicationController app, MessageInteractionListener listener, boolean isQuickReply) {
        setContext(app);
        mApplication = app;
        this.isQuickReply = isQuickReply;
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_invite_music_received, parent, false);
        initBaseHolder(rowView);
        mBtnAccept = (Button) rowView.findViewById(R.id.btn_accept_music);
        mBtnReInvite = (Button) rowView.findViewById(R.id.btn_reinvite_music);
        mBtnTryPlay = (Button) rowView.findViewById(R.id.btn_try_play_music);
        mBtnViaFB = (Button) rowView.findViewById(R.id.btn_reinvite_via_fb);
        mTvwCountDown = (TextView) rowView.findViewById(R.id.count_down_time);
        mTvwTitle = (TextView) rowView.findViewById(R.id.message_share_share_music_send_title);
        mTvwSong = (TextView) rowView.findViewById(R.id.message_share_music_send_song_name);
        mTvwSinger = (TextView) rowView.findViewById(R.id.message_share_music_send_song_singer);
        mImgAvtSong = (ImageView) rowView.findViewById(R.id.message_share_music_send_avatar);
        mLayoutReinvite = (RelativeLayout) rowView.findViewById(R.id.layout_reinvite_music);
        mLayoutUserAction = (RelativeLayout) rowView.findViewById(R.id.layout_user_action_music);
        mLayoutUserGroupAction = (RelativeLayout) rowView.findViewById(R.id.layout_user_action_group_music);
        mBtnAcceptGroup = (Button) rowView.findViewById(R.id.btn_accept_group_music);
        mIbnGiftCrbt = (ImageButton) rowView.findViewById(R.id.message_gift_crbt);
        rowView.setTag(this);
        setConvertView(rowView);
    }


    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        checkAndUpdateStatus();
        drawHolder(message);
        setListener();
    }

    private void drawHolder(ReengMessage message) {
        mTvwCountDown.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSong.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSinger.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnAccept.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnReInvite.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnTryPlay.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnViaFB.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnAcceptGroup.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        String title = message.getFileName();   //su dung fileName de co chu in dam
        mTvwTitle.setText(TextHelper.fromHtml(title));
        mediaModel = message.getSongModel(mApplication.getMusicBusiness());
        if (mediaModel != null) {
            // get data
            String songName = mediaModel.getName();
            String songSinger = mediaModel.getSinger();
            String avatarUrl = mediaModel.getImage();
            Log.i(TAG, "mediaModel: " + mediaModel);
            // draw
            if (!TextUtils.isEmpty(songName)) {
                mTvwSong.setText(songName);
            } else {
                mTvwSong.setText("");
            }
            if (!TextUtils.isEmpty(songSinger)) {
                mTvwSinger.setText(songSinger);
            } else {
                mTvwSinger.setText("");
            }
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, avatarUrl);
            if ("0".equals(mediaModel.getCrbtCode())) {
                mIbnGiftCrbt.setVisibility(View.GONE);
            } else if (mApplication.getConfigBusiness().isEnableCrbt() &&
                    getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    !TextUtils.isEmpty(mediaModel.getCrbtCode())) {
                mIbnGiftCrbt.setVisibility(View.VISIBLE);
            } else {
                mIbnGiftCrbt.setVisibility(View.GONE);
            }
        } else {
            // neu chua co media thi get from sv, sau khi get duoc thi notify o thread detail
            mTvwSong.setText("");
            mTvwSinger.setText("");
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, "");
            //mApplication.getMusicBusiness().getMediaModelFromServer(message.getSongId(), message.getThreadId());
            mIbnGiftCrbt.setVisibility(View.GONE);
        }
    }

    private void checkAndUpdateStatus() {
        Log.d("------------", "checkAndUpdateStatus: " + message.getStatus() + " *** " + message.isCounting());
        if (message.getId() > 0) {// message luu db co id, id==0 thi ko xu ly
            CountDownInviteManager.getInstance(mApplication).addHolderMessage(ReceivedInviteMusicHolder.this, message.getId());
        }
        if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_ACCEPTED) {
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
        } else if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_CANCEL ||
                message.getMusicState() == ReengMessageConstant.MUSIC_STATE_TIME_OUT) {
            mLayoutReinvite.setVisibility(View.VISIBLE);
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
            mBtnTryPlay.setVisibility(View.GONE);
            /*if (mediaModel == null || TextUtils.isEmpty(mediaModel.getUrl())) {
                mBtnTryPlay.setVisibility(View.GONE);
            } else {
                mBtnTryPlay.setVisibility(View.VISIBLE);
            }*/
        } else if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {// truong hop moi nhan, chua timeout
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
            mLayoutUserAction.setVisibility(View.VISIBLE);
            mTvwCountDown.setVisibility(View.VISIBLE);
            if (!message.isCounting()) {
                long time = mApplication.getMusicBusiness().getCurrentTimeAfterDifferenceTime() - message.getTime();
                if (time > CountDownInviteManager.COUNT_DOWN_DURATION) {// qua thoi gian timeout
                    message.setMusicState(ReengMessageConstant.MUSIC_STATE_TIME_OUT);
                    message.setDuration(0);
                    mApplication.getMessageBusiness().updateSendInviteMessageTimeout(message);
                    CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(message);
                } else {
                    message.setDuration(CountDownInviteManager.COUNT_DOWN_DURATION - ((int) time));
                    CountDownInviteManager.getInstance(mApplication).startCountDownMessage(message);
                }
                // update DB
                ApplicationController application = (ApplicationController) getContext().getApplicationContext();
                MessageBusiness messageBusiness = application.getMessageBusiness();
                messageBusiness.updateAllFieldsOfMessage(message);
            }
            mTvwCountDown.setText(StopWatch.convertSecondInMMSSForm(true, message.getDuration() / 1000));
        } else if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_GROUP ||
                message.getMusicState() == ReengMessageConstant.MUSIC_STATE_REQUEST_CHANGE) {
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.VISIBLE);
        } else {// ngoai le khac thi an het
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
        }
    }

    private void setListener() {
        mBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onAcceptInviteMusicClick(message);
            }
        });
        mBtnReInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onReinviteShareMusicClick(message);
            }
        });
        mBtnTryPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onTryPlayMusicClick(message);
            }
        });
        mBtnViaFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onInviteMusicViaFBClick(message);
            }
        });
        mBtnAcceptGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onAcceptMusicGroupClick(message);
            }
        });
        mIbnGiftCrbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onSendCrbtGift(message, mediaModel);
            }
        });
    }
}
package com.metfone.selfcare.holder.onmedia.feeds;

import android.view.View;
import android.widget.LinearLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 3/8/2018.
 */

public class OMFeedBannerViewHolder extends OMFeedBaseViewHolder {

    private static final String TAG = OMFeedBannerViewHolder.class.getSimpleName();

    private AspectImageView mImgBanner;
    private EllipsisTextView mTvwContent;
    private FeedModelOnMedia mFeed;
    private View itemView;

    private ApplicationController mApp;

    private int width, height;

    public OMFeedBannerViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        mApp = mApplication;
        this.itemView = itemView;
        mImgBanner = (AspectImageView) itemView.findViewById(R.id.img_onmedia_banner);
        mTvwContent = (EllipsisTextView) itemView.findViewById(R.id.tvw_content_banner);

//        getWidthHeight(false);
    }

    @Override
    public void setElement(Object obj) {
        mFeed = (FeedModelOnMedia) obj;
        Log.i(TAG, " setElement width: " + width + " height: " + height);
        /*if (width == 0) {
            getWidthHeight(true);
        } else {*/
//            ImageViewAwareTargetSize imgTargetSize = new ImageViewAwareTargetSize(mImgBanner, width, height);
            ImageLoaderManager.getInstance(mApp).displayImageFeedsWithListener(mImgBanner, mFeed.getFeedContent()
                    .getImageUrl(), width, height);
//        }
        String content = mFeed.getFeedContent().getItemName();
        mTvwContent.setEmoticon(mApp, content, content.hashCode(), content);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String leftDeeplink = mFeed.getFeedContent().getLeftDeeplink();
                getOnMediaHolderListener().onDeepLinkClick(mFeed, leftDeeplink);
            }
        });
    }

    private void getWidthHeight(final boolean needSetImage) {
        mTvwContent.post(new Runnable() {
            @Override
            public void run() {
                width = mImgBanner.getWidth();
                height = (int) (width / 1.78f);
                Log.i(TAG, "width: " + width + " height: " + height);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mImgBanner
                        .getLayoutParams();
                params.width = width;
                params.height = height;
                mImgBanner.setLayoutParams(params);
                if (needSetImage) {
//                    ImageViewAwareTargetSize imgTargetSize = new ImageViewAwareTargetSize(mImgBanner, width, height);
                    ImageLoaderManager.getInstance(mApp).displayImageFeedsWithListener(mImgBanner, mFeed
                            .getFeedContent()
                            .getImageUrl(), width, height);
                }
            }
        });
    }
}

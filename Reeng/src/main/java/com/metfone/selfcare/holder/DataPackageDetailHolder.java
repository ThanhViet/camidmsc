package com.metfone.selfcare.holder;

import android.app.Activity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.DataPackagesResponse;
import com.metfone.selfcare.util.Utilities;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;

public class DataPackageDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;

    @BindView(R.id.tv_short_desc)
    @Nullable
    TextView tvShortDesc;

    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;

    @BindView(R.id.button_submit)
    @Nullable
    TextView btnSubmit;

    @BindView(R.id.layout_root)
    @Nullable
    View viewRoot;

    private DataPackagesResponse.DataPackageInfo data;
    private Activity activity;
    private OnBuyPackage listener;

    public DataPackageDetailHolder(View view, Activity activity, OnBuyPackage listener) {
        super(view);
        this.activity = activity;
        this.listener = listener;
        ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
        layoutParams.width = getWidth();
        viewRoot.setLayoutParams(layoutParams);
        viewRoot.requestLayout();
    }

    public static int getWidth() {
        return (int) ((ApplicationController.self().getWidthPixels() - Utilities.dpToPx(16)) / 1.2);
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof DataPackagesResponse.DataPackageInfo) {
            data = (DataPackagesResponse.DataPackageInfo) item;
            if (tvTitle != null) tvTitle.setText(data.getName());
            if (tvShortDesc != null) {
                if (TextUtils.isEmpty(data.getShortDesc())) {
                    //tvShortDesc.setVisibility(View.GONE);
                    tvShortDesc.setText("");
                } else {
                    //tvShortDesc.setVisibility(View.VISIBLE);
                    tvShortDesc.setText(Html.fromHtml(data.getShortDesc()));
                }
            }
            if (tvDesc != null) {
                if (TextUtils.isEmpty(data.getDesc())) {
                    //tvDesc.setVisibility(View.GONE);
                    tvDesc.setText("");
                } else {
                    //tvDesc.setVisibility(View.VISIBLE);
                    tvDesc.setText(Html.fromHtml(data.getDesc()));
                }
            }

            if (viewRoot != null && btnSubmit != null) {
//                switch (position % 9) {
//                    case 1:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_1);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_1);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 2:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_2);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_2);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 3:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_3);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_3);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 4:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_4);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_4);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 5:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_5);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_5);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 6:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_6);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_6);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 7:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_7);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_7);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    case 8:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail_8);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_8);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                    default:
//                        viewRoot.setBackgroundResource(R.drawable.bg_data_package_detail);
//                        if (data.isClickable()) {
//                            btnSubmit.setEnabled(true);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail);
//                        } else {
//                            btnSubmit.setEnabled(false);
//                            btnSubmit.setBackgroundResource(R.drawable.bg_button_data_package_detail_disable);
//                        }
//                        break;
//                }
                btnSubmit.setText(data.getLabelButton());
            }
        }
    }

    @OnClick(R.id.button_submit)
    public void onClickItem() {
        if (data != null ) {
            if (listener != null) {
                listener.onClickBuy(data);
            }
        }
    }

    public interface OnBuyPackage {
        void onClickBuy(DataPackagesResponse.DataPackageInfo data);
    }
}
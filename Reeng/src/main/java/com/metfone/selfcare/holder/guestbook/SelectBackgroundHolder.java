package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.editor.sticker.utils.Constants;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.glide.GlideImageLoader;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class SelectBackgroundHolder extends BaseViewHolder {
    private Context mContext;
    private Background entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private ImageView mImgBackground;
    private int screenWidth;
    private ProgressLoading mProgressLoading;

    public SelectBackgroundHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mContext.getResources();
        mImgBackground = (ImageView) convertView.findViewById(R.id.background_item);
        mProgressLoading = (ProgressLoading) convertView.findViewById(R.id.progress_loading_img);
        // set layout param
        screenWidth = mApplication.getWidthPixels();
        screenWidth = screenWidth - mRes.getDimensionPixelSize(R.dimen.margin_more_content_23);
        ViewGroup.LayoutParams params = mImgBackground.getLayoutParams();
        params.width = screenWidth / 3;
        params.height = (int) (params.width * Constants.SCALE_HEIGHT);
    }

    @Override
    public void setElement(Object obj) {
        entry = (Background) obj;
        String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(entry.getPath());
        ImageLoaderManager.getInstance(mContext).displayGuestBookPreview(mImgBackground, url, new GlideImageLoader.SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted() {
                mProgressLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(GlideException e) {
                mProgressLoading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete() {
                mProgressLoading.setVisibility(View.GONE);
            }
        });
    }
}
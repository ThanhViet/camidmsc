package com.metfone.selfcare.holder.call;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.CallHistoryAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.call.CallHistory;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

/**
 * Created by toanvk2 on 10/12/2016.
 */

public class CallHistoryHolder extends BaseViewHolder implements View.OnClickListener {
    private static final String TAG = CallHistoryHolder.class.getSimpleName();
    private Context mContext;
    private Resources mRes;
    private CallHistoryAdapter.HolderHistoryListener mListener;
    private CallHistory mCallHistory;
    private RoundedImageView mImgAvatar;
    private TextView mTvwAvatar, mTvwName, mTvwCount, mTvwTime, mTvwCallType;
    private ImageView mImgStatus, mImgActionCall, mImgActionVideo;
    private int nameWidth;
    private boolean isEnableVideo = false;
    private int icVideoWidth = 0;
    private AppCompatTextView mTvwDuration;

    public CallHistoryHolder(View convertView, Context context, CallHistoryAdapter.HolderHistoryListener listener) {
        super(convertView);
        this.mContext = context;
        this.mListener = listener;
        this.mRes = mContext.getResources();
        mImgAvatar = convertView.findViewById(R.id.holder_call_history_avatar);
        mTvwAvatar = convertView.findViewById(R.id.holder_call_history_avatar_text);
        mTvwName = convertView.findViewById(R.id.holder_call_history_name);
        mTvwCallType = convertView.findViewById(R.id.holder_call_history_type);
        mTvwCount = convertView.findViewById(R.id.holder_call_history_count);
        mTvwTime = convertView.findViewById(R.id.holder_call_history_time);
        mImgStatus = convertView.findViewById(R.id.holder_call_history_status);
        mImgActionVideo = convertView.findViewById(R.id.holder_call_history_action_video);
        mImgActionCall = convertView.findViewById(R.id.holder_call_history_action_call);
        mTvwDuration = convertView.findViewById(R.id.holder_call_history_detail_duration);
    }

    @Override
    public void setElement(Object obj) {
        mCallHistory = (CallHistory) obj;
        drawAvatarAndName();
        drawHistoryDetail();
        mImgActionCall.setOnClickListener(this);
        mImgActionVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            if (view.getId() == R.id.holder_call_history_action_call) {
                mListener.onActionCall(mCallHistory);
            } else if (view.getId() == R.id.holder_call_history_action_video) {
                mListener.onActionVideo(mCallHistory);
            }
        }
    }

    public void setNameWidth(int width) {
        this.nameWidth = width;
    }

    private void drawAvatarAndName() {
        ApplicationController application = (ApplicationController) mContext.getApplicationContext();
        ContactBusiness contactBusiness = application.getContactBusiness();
        AvatarBusiness avatarBusiness = application.getAvatarBusiness();
        int sizeAvatar = (int) application.getResources().getDimension(R.dimen.avatar_small_size);
        icVideoWidth = (int) application.getResources().getDimension(R.dimen.call_div_video);
        PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(mCallHistory.getFriendNumber());
        if (phoneNumber != null) {
            avatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, sizeAvatar);
            mTvwName.setText(phoneNumber.getName());
            isEnableVideo = phoneNumber.isReeng();
        } else {
            StrangerPhoneNumber stranger = application.
                    getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(mCallHistory.getFriendNumber());
            if (stranger != null) {
                if (TextUtils.isEmpty(stranger.getFriendName())) {
                    mTvwName.setText(Utilities.hidenPhoneNumber(stranger.getFriendName()));
                } else {
                    mTvwName.setText(stranger.getFriendName());
                }
                avatarBusiness.setStrangerAvatar(mImgAvatar, mTvwAvatar, stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                isEnableVideo = false;
            } else {
                mTvwName.setText(mCallHistory.getFriendNumber());
                NonContact nonContact = application.getContactBusiness().getExistNonContact(mCallHistory.getFriendNumber());
                avatarBusiness.setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, nonContact, mCallHistory.getFriendNumber(), sizeAvatar);
                isEnableVideo = nonContact != null && nonContact.isReeng();
            }
        }
    }

    private void drawHistoryDetail() {
        String stateStr;
        Resources res = mContext.getResources();
//        if (mCallHistory.getCallDirection() == CallHistoryConstant.DIRECTION_SEND) {
//            if (mCallHistory.getCallOut() == ReengMessageConstant.MODE_GSM) {
//                stateStr = res.getString(R.string.caller_call_out_state_call);
//            } else if (mCallHistory.getCallOut() == ReengMessageConstant.MODE_VIDEO_CALL) {
//                stateStr = res.getString(R.string.caller_call_video_state_call);
//            } else {
//                stateStr = res.getString(R.string.caller_call_state_call);
//            }
//            if (mCallHistory.getCallState() == CallHistoryConstant.STATE_MISS) {
//                stateStr = res.getString(R.string.caller_call_state_miss);
//            } else {
//                if (mCallHistory.getCallState() == CallHistoryConstant.STATE_OUT_GOING) {
//                } else if (mCallHistory.getCallState() == CallHistoryConstant.STATE_BUSY) {
//                    stateStr = res.getString(R.string.caller_call_state_busy);
//                } else if (mCallHistory.getCallState() == CallHistoryConstant.STATE_CANCELLED) {
//                    stateStr = res.getString(R.string.caller_call_state_cancelled);
//                } else if (mCallHistory.getCallState() == CallHistoryConstant.STATE_REJECTED) {
//                    stateStr = res.getString(R.string.caller_call_state_reject);
//                }
//            }
//        } else { // call receive
//            if (mCallHistory.getCallOut() == ReengMessageConstant.MODE_GSM) {
//                stateStr = res.getString(R.string.callee_call_state_call); // receive no have callout :)))
//            } else if (mCallHistory.getCallOut() == ReengMessageConstant.MODE_VIDEO_CALL) {
//                stateStr = res.getString(R.string.callee_call_video_state_call);
//            } else {
//                stateStr = res.getString(R.string.callee_call_state_call);
//            }
//            if (mCallHistory.getCallState() == CallHistoryConstant.STATE_MISS) {
//                stateStr = res.getString(R.string.callee_call_state_miss);
//            } else {
//                if (mCallHistory.getCallState() == CallHistoryConstant.STATE_IN_COMING) {
////                    mTvwContent.setText(stateStr);
////                    mTvwDuration.setText(message.getContent());
//                } else if (mCallHistory.getCallState() == CallHistoryConstant.STATE_BUSY) {
//                    stateStr = res.getString(R.string.call_state_busy); // receive no have call busy
//                } else if (mCallHistory.getCallState() == CallHistoryConstant.STATE_CANCELLED) {
//                    stateStr = res.getString(R.string.call_state_miss_call);// receive no have call CANCELLED
//                } else if (mCallHistory.getCallState() == CallHistoryConstant.STATE_REJECTED) {
//                    stateStr = res.getString(R.string.callee_call_state_reject);
//                } else {
//                }
//            }
//        }
//        mTvwCallType.setText(Utilities.getTextCallInfor(res, mCallHistory.getCallDirection(), mCallHistory.getCallOut(), mCallHistory.getCallState()));
        mTvwCallType.setText(Utilities.getTextCallInfor(res, mCallHistory.getCallType()));
        mImgStatus.setImageResource(Utilities.getIconCallInfor(mCallHistory.getCallDirection(), mCallHistory.getCallState()));
        if (mCallHistory.getCallState() == CallHistoryConstant.STATE_MISS) {
            mImgStatus.setColorFilter(ContextCompat.getColor(mContext, R.color.v5_text_11));
            mTvwTime.setTextColor(ContextCompat.getColor(mContext, R.color.v5_text_11));
            mTvwName.setTextColor(ContextCompat.getColor(mContext,R.color.red));
            mTvwCount.setTextColor(ContextCompat.getColor(mContext,R.color.red));
            mTvwCallType.setTextColor(ContextCompat.getColor(mContext, R.color.v5_text_11));
        } /*else if (mCallHistory.getCallOut() == CallHistoryConstant.CALL_OUT) {
            mImgStatus.setColorFilter(ContextCompat.getColor(mContext, R.color.text_time_call_out));
            mTvwTime.setTextColor(ContextCompat.getColor(mContext, R.color.text_time_call_out));
            mTvwCallType.setTextColor(ContextCompat.getColor(mContext, R.color.text_time_call_out));
        }*/ else {
            mImgStatus.setColorFilter(ContextCompat.getColor(mContext, R.color.v5_text_11));
            mTvwTime.setTextColor(ContextCompat.getColor(mContext, R.color.v5_text_11));
            mTvwCallType.setTextColor(ContextCompat.getColor(mContext, R.color.v5_text_11));
            mTvwName.setTextColor(ContextCompat.getColor(mContext,R.color.v5_text_11));
            mTvwCount.setTextColor(ContextCompat.getColor(mContext,R.color.v5_text_11));
        }

        if (isEnableVideo) {
            mImgActionVideo.setVisibility(View.VISIBLE);
        } else {
            mImgActionVideo.setVisibility(View.GONE);
        }
        // text count
        int countWidth;
        if (mCallHistory.getCount() > 1) {
            mTvwCount.setVisibility(View.VISIBLE);
            mTvwCount.setText(String.format("(%d)", mCallHistory.getCount()));
            mTvwCount.measure(0, 0);       //must call measure!
            countWidth = mTvwCount.getMeasuredWidth();  //get width
        } else {
            mTvwCount.setVisibility(View.GONE);
            countWidth = 0;
        }
        countWidth = isEnableVideo ? countWidth : countWidth - icVideoWidth;
        mTvwName.setMaxWidth(nameWidth - countWidth);

        showDateCall();
        if (mCallHistory.getDuration() > 0) {
            mTvwDuration.setText(Utilities.secondsToTimerText(mCallHistory.getDuration()));
        }
    }

    private void showDateCall() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        String dateCurrent =dateFormat.format(cal.getTime());
        String dateCall =TimeHelper.formatSeparatorTimeOfHistory(mCallHistory.getCallTime(), mContext.getResources());

        if (dateCurrent.equals(dateCall)) {
            mTvwTime.setVisibility(View.GONE);
        } else {
            mTvwTime.setVisibility(View.VISIBLE);
            mTvwTime.setText(dateCall);
        }
    }
}
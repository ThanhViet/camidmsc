package com.metfone.selfcare.holder;

import android.content.Context;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.LocalSongInfo;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.SearchQuery;
import com.metfone.selfcare.helper.GlideHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;

/**
 * Created by toanvk2 on 1/8/15.
 */
public class SongHolder extends BaseViewHolder {
    private static final String TAG = SongHolder.class.getSimpleName();
    private Context mContext;
    private ClickListener.IconListener mCallBack;
    private Resources mRes;
    private Object entry;
    private AppCompatTextView mTvwTitle;
    private AppCompatTextView mTvwSinger;
    private RoundedImageView mImgDetail;
    private RoundTextView tvBtn;

    public SongHolder(View itemView, Context context, ClickListener.IconListener callBack) {
        super(itemView);
        this.mContext = context;
        this.mRes = context.getResources();
        this.mCallBack = callBack;

        mTvwTitle = itemView.findViewById(R.id.tvName);
        mTvwSinger = itemView.findViewById(R.id.tvSinger);
        mImgDetail = itemView.findViewById(R.id.ivAvatar);
        tvBtn = itemView.findViewById(R.id.btn);
//        mTvwMonopoly = itemView.findViewById(R.id.song_monopoly);
    }



    /*@Override
    public void onClick(View view) {
        if (mCallBack != null) {
            mCallBack.onIconClickListener(view, entry, Constants.ACTION.SEARCH_TRY_PLAY_SONG);
        }
    }*/

    private void setViewHolderSearch(SearchQuery item) {
//        mTvwMonopoly.setVisibility(View.GONE);
        mTvwTitle.setText(item.getFullName());
        if (item.getFullSinger() != null) {
            mTvwSinger.setText(item.getFullSinger());
        } else {
            mTvwSinger.setText("");
        }
        ApplicationController mApplication = (ApplicationController) mContext.getApplicationContext();
        String imageUrl = UrlConfigHelper.getInstance(mContext).getDomainKeengMusicImage();
        if (!TextUtils.isEmpty(item.getImage()) && !TextUtils.isEmpty(imageUrl)) {
            imageUrl += item.getImage();
        } else {
            imageUrl = null;
        }
        GlideHelper.getInstance(mApplication).setSongAvatar(mImgDetail, imageUrl);
        //        mHolder.mImgDetail.setImageDrawable(mRes.getDrawable(R.drawable.ic_song));
    }

    private void setViewHolderTop(MediaModel item) {
        mTvwTitle.setText(item.getName());
        //        mHolder.mTvwSinger.setVisibility(View.VISIBLE);
        if (item.getSinger() != null) {
            mTvwSinger.setText(item.getSinger());
        } else {
            mTvwSinger.setText("");
        }
        ApplicationController mApplication = (ApplicationController) mContext.getApplicationContext();
        GlideHelper.getInstance(mApplication).setSongAvatar(mImgDetail, item.getImage());
        if (item.isMonopoly()) {
//            mTvwMonopoly.setVisibility(View.VISIBLE);
        } else {
//            mTvwMonopoly.setVisibility(View.GONE);
        }
    }

    private void bindDataMusicUpload(LocalSongInfo localSongInfo){
        mTvwTitle.setText(localSongInfo.getName());
        mTvwSinger.setText(localSongInfo.getSinger());
        mImgDetail.setImageResource(R.drawable.ic_music_upload_v5);
        if(localSongInfo.isUploaded()){
            tvBtn.setText(mContext.getResources().getString(R.string.listen_top_song));
            tvBtn.setTextColor(ContextCompat.getColor(mContext,R.color.white));
            tvBtn.setBackgroundColorRound(ContextCompat.getColor(mContext, R.color.v5_main_color));
        }else {
            tvBtn.setText(mContext.getResources().getString(R.string.text_button_upload));
            tvBtn.setBackgroundColorRound(ContextCompat.getColor(mContext, R.color.v5_cancel));
            tvBtn.setTextColor(ContextCompat.getColor(mContext,R.color.v5_text));
        }
//
//        mBtnUpload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mItemClickedListener.onClicked(mLocalSongInfo);
//            }
//        });
    }

    @Override
    public void setElement(Object obj) {
        entry = obj;
        if (entry instanceof SearchQuery) {
            setViewHolderSearch((SearchQuery) entry);
        } else if (entry instanceof MediaModel) {
            setViewHolderTop((MediaModel) entry);
        } else if(entry instanceof LocalSongInfo){
            bindDataMusicUpload((LocalSongInfo) entry);
        }
    }
}
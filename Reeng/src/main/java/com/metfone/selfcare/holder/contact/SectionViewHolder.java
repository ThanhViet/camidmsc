package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.NumberConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.ui.TooltipShowcase.SimpleTooltipUtils;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 9/1/2017.
 */

public class SectionViewHolder extends BaseContactHolder implements View.OnClickListener {
    private Context mContext;
    private ContactsHolderCallBack mCallBack;
    private PhoneNumber entry;
    private Resources mRes;
    private TextView mTvwName;
    private TextView mTvwDesc;
    private RelativeLayout viewTextContacts;
    private boolean showSync = false;
    private String nameScreen = "";


    public SectionViewHolder(View convertView, Context context, ContactsHolderCallBack callBack) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        initView(convertView);
    }

    public SectionViewHolder(View convertView, Context context, ContactsHolderCallBack callBack, boolean showSync) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        this.showSync = showSync;
        initView(convertView);
    }
    public SectionViewHolder(View convertView, Context context, ContactsHolderCallBack callBack, boolean showSync, String nameScreen) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        this.showSync = showSync;
        this.nameScreen = nameScreen;
        initView(convertView);
    }

    @Override
    public void setElement(Object obj) {
        entry = (PhoneNumber) obj;
        if (entry == null) {
            return;
        }
        if (nameScreen.equals("nameContact") || nameScreen.equals("settingGroup")) {
            mTvwName.setVisibility(View.VISIBLE);
        }

        // draw
        mTvwName.setBackgroundColor(mRes.getColor(R.color.white));
        SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Alphabet);
        mTvwName.setText(entry.getName());
        if (entry.getSectionType() == -1 || entry.getSectionType() == -2 || entry.getSectionType() == -3) {
            if (entry.getSectionType() == -2) {// for contact header (#,A,B...)
                mTvwName.setBackgroundColor(mRes.getColor(R.color.v5_pre_select));
                SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Alphabet);
            } else if (entry.getSectionType() == -3) {// for sub-section: not install Mocha
                mTvwName.setBackgroundColor(mRes.getColor(R.color.v5_pre_select));
                SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Alphabet);
            } else {
                if (nameScreen.equals("nameContact") && ((PhoneNumber) obj).getContact() && ((PhoneNumber) obj).getName().equals(mContext.getResources().getString(R.string.menu_contacts))) {
                    mTvwName.setBackgroundColor(mRes.getColor(R.color.v5_pre_select));
                    SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Contacts);
                } else {
                    mTvwName.setBackgroundColor(mRes.getColor(R.color.v5_pre_select));
                    SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Alphabet);
                }
                if (nameScreen.equals("nameContact") && !((PhoneNumber) obj).getContact()) {
                    mTvwName.setVisibility(View.VISIBLE);
                    mTvwName.setAlpha(0.5f);
                    mTvwName.setTextSize(16);
                    mTvwName.setTextColor(mRes.getColor(R.color.v5_bg_night_new));
                    mTvwName.setBackgroundColor(mRes.getColor(R.color.white));
                }
            }
            mTvwDesc.setVisibility(View.GONE);
        } else {
            if (nameScreen.equals("nameContact") && ((PhoneNumber) obj).getContact() && ((PhoneNumber) obj).getName().equals(mContext.getResources().getString(R.string.menu_contacts))) {
                mTvwName.setBackgroundColor(mRes.getColor(R.color.v5_pre_select));
                SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Contacts);
            } else {
                mTvwName.setBackgroundColor(mRes.getColor(R.color.v5_pre_select));
                SimpleTooltipUtils.setTextAppearance(mTvwName, R.style.V5_List_Tittle_Alphabet);
            }
            if (nameScreen.equals("nameContact") && !((PhoneNumber) obj).getContact()) {
                mTvwName.setVisibility(View.VISIBLE);
                mTvwName.setAlpha(0.5f);
                mTvwName.setTextSize(16);
                mTvwName.setTextColor(mRes.getColor(R.color.v5_bg_night_new));
                mTvwName.setBackgroundColor(mRes.getColor(R.color.white));
            }
            mTvwDesc.setVisibility(View.VISIBLE);
            if (entry.getSectionType() == NumberConstant.SECTION_TYPE_CONTACT) {
                if (showSync) {
                    mTvwDesc.setText(mRes.getString(R.string.section_sync));
                    mTvwDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    mTvwDesc.setVisibility(View.GONE);
                }
            } else {
                mTvwDesc.setText(mRes.getString(R.string.view_all));
                mTvwDesc.setPadding(0, 0, Utilities.dpToPx(5), 0);
                mTvwDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_v5_view_all_video, 0);
            }
        }
        mTvwDesc.setOnClickListener(this);
    }

    private void initView(View convertView) {
        mTvwName = convertView.findViewById(R.id.holder_section_name);
        mTvwDesc = convertView.findViewById(R.id.holder_section_desc);
        viewTextContacts = convertView.findViewById(R.id.viewTextContacts);
    }

    @Override
    public void onClick(View view) {
        if (mCallBack != null && entry != null) {
            switch (view.getId()) {
                case R.id.holder_section_desc:
                    mCallBack.onSectionClick(entry);
                    break;
            }
        }
    }

    /*private void setCharecterView(PhoneNumber entry) {
        int charHeight = (int) mRes.getDimension(R.dimen.item_contact_charecter_height);
        int titleMemberGroupHeight = (int) mRes.getDimension(R.dimen.item_charecter_member_group_height);
        mLlCharecter.setVisibility(View.VISIBLE);
        mFrAvatar.setVisibility(View.GONE);
        mRelContent.setVisibility(View.GONE);
        mLlOption.setVisibility(View.GONE);
        mTvCharecter.setText(entry.getTitle());
        RelativeLayout.LayoutParams paramsChar = (RelativeLayout.LayoutParams) mLlCharecter.getLayoutParams();
        if (getType() == Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP) {
            mTvCharecter.setBackgroundColor(ContextCompat.getColor(mContext, R.color.bg_message_setting));
            mTvCharecter.setTextColor(ContextCompat.getColor(mContext, R.color.bg_mocha));
            mTvCharecter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mRes.getDimension(R.dimen.mocha_text_size_level_2_5));
            paramsChar.height = titleMemberGroupHeight;
        } else {
            mTvCharecter.setBackgroundColor(ContextCompat.getColor(mContext, R.color.bg_contact_charecter));
            mTvCharecter.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
            mTvCharecter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mRes.getDimension(R.dimen.mocha_text_size_level_2));
            paramsChar.height = charHeight;
        }
    }*/
}

package com.metfone.selfcare.holder.onmedia.feeds;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.holder.onmedia.OnMediaCommentHolder;
import com.metfone.selfcare.listeners.SieuHaiNextVideoHolderListener;
import com.metfone.selfcare.ui.TagTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by sonnn00 on 10/5/2017.
 */

public class SieuHaiVideoNextHolder extends BaseViewHolder {

    private static final String TAG = OnMediaCommentHolder.class.getSimpleName();

    private ApplicationController mApp;
    private ImageView mFeedsImage;
    private TagTextView mTvVideoName;
    private TextView mUsername;
    private TextView mUserTime;
    private LinearLayout root;
    private TagTextView mContent;
    private View mLayoutAvatar;
    //    private ReengAccount account;
//    private ContactBusiness mContactBusiness;
//    private AvatarBusiness mAvatarBusiness;
//    private FeedBusiness mFeedBusiness;
    private Resources mRes;

    //    private String myName;
//    private String myNumber;
    private String mFriendJid, mFriendName;
    //    private boolean isMyUser;
    private PhoneNumber mFriendPhone;
    private LinearLayout layoutComment;
    Activity mActivity;
    private SieuHaiNextVideoHolderListener sieuHaiNextVideoListener;
    private TagMocha.OnClickTag onClickTag;

    public SieuHaiVideoNextHolder(View convertView, ApplicationController mApp) {
        super(convertView);
        this.mApp = mApp;
        mRes = mApp.getResources();

//        mAvatarBusiness = mApp.getAvatarBusiness();
//        mContactBusiness = mApp.getContactBusiness();
//        mFeedBusiness = mApp.getFeedBusiness();
//        account = mApp.getReengAccountBusiness().getCurrentAccount();
//        myName = account.getName();
//        myNumber = account.getJidNumber();

        mTvVideoName = (TagTextView) convertView.findViewById(R.id.tv_video_name);
//        mUserTime = (TextView) convertView.findViewById(R.id.tvw_comment_time);
        mFeedsImage = (ImageView) convertView.findViewById(R.id.img_video_image);
        root = (LinearLayout) convertView.findViewById(R.id.root);
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;

//        mLayoutAvatar = convertView.findViewById(R.id.layout_avatar_comment);
//        mTvwAvatar = (TextView) convertView.findViewById(R.id.tvw_comment_avatar);
//        mContent = (TagTextView) convertView.findViewById(R.id.tvw_comment_content);
//        mContent.setMaxLines(10);
//        layoutComment = (LinearLayout) convertView.findViewById(R.id.layout_onmedia_comment);
    }

    @Override
    public void setElement(Object obj) {
        FeedContent comment = (FeedContent) obj;
        Log.i(TAG, "comment; " + comment.toString());
        setViewHolderContent(comment);
    }

    private void setViewHolderContent(final FeedContent item) {
        mTvVideoName.setText(item.getItemName());
        Glide.with(ApplicationController.self()).load(item.getImageUrl()).into(mFeedsImage);
//        universalImageLoader.displayImage(item.getImageUrl(), mFeedsImage, new ImageLoadingListener() {
//            @Override
//            public void onLoadingStarted(String s, View view) {
//
//            }
//
//            @Override
//            public void onLoadingFailed(String s, View view, FailReason failReason) {
//
//            }
//
//            @Override
//            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
//                mFeedsImage.setImageBitmap(bitmap);
//            }
//
//            @Override
//            public void onLoadingCancelled(String s, View view) {
//
//            }
//        });
//        final UserInfo userInfo = item.getUserInfo();
//        if (userInfo == null) return;
//        int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
//        mFriendJid = userInfo.getMsisdn();
//
//        mUsername.setText(mFriendName);
//        String content = item.getComment();
//        if (TextUtils.isEmpty(content)) {
//            content = mRes.getString(R.string.connections_share_uppercase);
//            mContent.setText(content);
//        } else {
//            if (item.getListTag() == null || item.getListTag().isEmpty()) {
//                mContent.setEmoticon(mApp, content, content.hashCode(), content);
//            } else {
//                mContent.setEmoticonWithTag(mApp, content, content.hashCode(), content,
//                        item.getListTag(), onClickTag);
//            }
//        }
////        mUserTime.setText(TimeHelper.caculateTimeFeed(mApp, item.getTimeStamp(), mFeedBusiness.getDeltaTimeServer()));
//
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sieuHaiNextVideoListener != null) {
                    sieuHaiNextVideoListener.onClickItem(item);
                }
            }
        });
//
//        mUsername.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (onMediaCmtHolderListener != null) {
//                    onMediaCmtHolderListener.onAvatarClick(userInfo);
//                }
//            }
//        });
//
//        mLayoutAvatar.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (onMediaCmtHolderListener != null) {
//                    onMediaCmtHolderListener.onLongClickItem(item);
//                }
//                return true;
//            }
//        });
//
//        mUsername.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (onMediaCmtHolderListener != null) {
//                    onMediaCmtHolderListener.onLongClickItem(item);
//                }
//                return true;
//            }
//        });
//
//        mUserTime.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (onMediaCmtHolderListener != null) {
//                    onMediaCmtHolderListener.onLongClickItem(item);
//                }
//                return true;
//            }
//        });
    }

    public void setOnSieuHaiHolderListener(SieuHaiNextVideoHolderListener onMediaCmtHolderListener) {
        this.sieuHaiNextVideoListener = onMediaCmtHolderListener;
    }

//    public void setOnClickTag(TagMocha.OnClickTag onClickTag) {
//        this.onClickTag = onClickTag;
//    }
}
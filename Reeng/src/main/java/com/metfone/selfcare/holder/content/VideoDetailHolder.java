/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.holder.content;

import android.app.Activity;
import android.content.res.Resources;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentVideo;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.module.video.model.VideoPagerModel;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;

public class VideoDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.tv_total_view)
    @Nullable
    TextView tvTotalView;
    @BindView(R.id.tv_datetime)
    @Nullable
    TextView tvDatetime;
    @BindView(R.id.tv_duration)
    @Nullable
    TextView tvDuration;
    @BindView(R.id.button_option)
    @Nullable
    View btnOption;
    @BindView(R.id.iv_avatar)
    @Nullable
    ImageView ivAvatar;
    @BindView(R.id.iv_live_stream)
    @Nullable
    View ivLiveStream;
    private Object data;
    private Resources res;

    public VideoDetailHolder(View view, Activity activity, final OnClickContentVideo listener) {
        super(view);
        res = activity.getResources();
        initListener(listener);
    }

    public VideoDetailHolder(View view, Activity activity, OnClickContentVideo listener, int widthLayout) {
        super(view);
        res = activity.getResources();
        initListener(listener);
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    private void initListener(final OnClickContentVideo listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickVideoItem(data, getAdapterPosition());
                }
            }
        });
        if (btnOption != null) btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreVideoItem(data, getAdapterPosition());
                }
            }
        });
        if (ivAvatar != null) ivAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickChannelVideoItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void bindDataHome(Content model) {
        if (tvTitle != null) tvTitle.setText(model.getName());
        if (tvDuration != null) {
            int durationInt = model.getDurationInt();
            if (durationInt > 0) {
                tvDuration.setText(DateTimeUtils.getDuration(durationInt));
                tvDuration.setVisibility(View.VISIBLE);
            } else {
                tvDuration.setVisibility(View.GONE);
            }
        }
        if (ivAvatar != null) {
            if (model.getChannelId() <= 0) {
                ivAvatar.setVisibility(View.GONE);
            } else {
                ivAvatar.setVisibility(View.VISIBLE);
                ImageBusiness.setChannelVideo(model.getChannelAvatar(), ivAvatar);
            }
        }
        long totalViews = model.getTotalViews();
        String channelName = model.getChannelName();
        boolean showDot = false;
        if (tvDesc != null) {
            if (TextUtils.isEmpty(channelName)) {
                tvDesc.setVisibility(View.GONE);
            } else {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(channelName);
                showDot = true;
            }
        }
        if (tvTotalView != null) {
            if (totalViews == 1) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format(res.getString(R.string.view), "1"));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
                showDot = true;
            } else if (totalViews > 1) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format(res.getString(R.string.video_views), Utilities.getTotalView(totalViews)));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
                showDot = true;
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
        }
        if (tvDatetime != null) {
            if (model.getPublishedTime() > 0) {
                tvDatetime.setVisibility(View.VISIBLE);
                tvDatetime.setText(DateTimeUtils.calculateTime(res, model.getPublishedTime()));
                tvDatetime.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
            } else {
                tvDatetime.setVisibility(View.GONE);
            }
        }
        if (ivLiveStream != null)
            ivLiveStream.setVisibility(model.isLive() ? View.VISIBLE : View.GONE);
        ImageBusiness.setCoverVideo(ivCover, model.getImage());
    }

    private void bindData(Video model) {
        if (tvTitle != null) tvTitle.setText(model.getTitle());
        if (tvDuration != null) {
            if (TextUtils.isEmpty(model.getDuration())) {
                tvDuration.setVisibility(View.GONE);
            } else {
                tvDuration.setText(model.getDuration());
                tvDuration.setVisibility(View.VISIBLE);
            }
        }
        if (ivAvatar != null) {
            if (model.getChannel() == null) {
                ivAvatar.setVisibility(View.GONE);
            } else {
                ivAvatar.setVisibility(View.VISIBLE);
                ImageBusiness.setChannelVideo(model.getChannel().getUrlImage(), ivAvatar);
            }
        }
        long totalViews = model.getTotalView();
        String channelName = model.getChannelName();
        boolean showDot = false;
        if (tvDesc != null) {
            if (TextUtils.isEmpty(channelName)) {
                tvDesc.setVisibility(View.GONE);
            } else {
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setText(channelName);
                showDot = true;
            }
        }
        if (tvTotalView != null) {
            if (totalViews == 1) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format(res.getString(R.string.view), "1"));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
                showDot = true;
            } else if (totalViews > 1) {
                tvTotalView.setVisibility(View.VISIBLE);
                tvTotalView.setText(String.format(res.getString(R.string.video_views), Utilities.getTotalView(totalViews)));
                tvTotalView.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
                showDot = true;
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
        }
        if (tvDatetime != null) {
            if (model.getPublishTime() > 0) {
                tvDatetime.setVisibility(View.VISIBLE);
                tvDatetime.setText(DateTimeUtils.calculateTime(res, model.getPublishTime()));
                tvDatetime.setCompoundDrawablesRelativeWithIntrinsicBounds(showDot ? res.getDrawable(R.drawable.ic_dot) : null, null, null, null);
            } else {
                tvDatetime.setVisibility(View.GONE);
            }
        }
        if (ivLiveStream != null)
            ivLiveStream.setVisibility(model.isLive() ? View.VISIBLE : View.GONE);
        ImageBusiness.setCoverVideo(ivCover, model.getImagePath());
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        } else if (item instanceof Video) {
            Video model = (Video) item;
            bindData(model);
        } else if (item instanceof VideoPagerModel) {
            VideoPagerModel model = (VideoPagerModel) item;
            if (model.getObject() instanceof Video) {
                bindData((Video) model.getObject());
            }
        }
    }
}
package com.metfone.selfcare.holder.message;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.textview.EmoTagTextViewListChat;
import com.metfone.selfcare.util.Log;

public class ReceivedTextHolder extends BaseMessageHolder {
    private ReengMessage message;
    private EmoTagTextViewListChat mTvwContent;
    private ImageView mImgLargeEmo;
    private SmartTextClickListener mSmartTextListener;

    private ApplicationController mApp;

    private View viewPreviewUrl;
    private AspectImageView mImgThumb;
    private TextView mTvwTitle;
    private TextView mTvwDesc;
    private TextView mTvwSite;
    private LinearLayout llActionChangeNumber;
    private RoundTextView btnSave, btnChat, btnSync;
    private int widthImgNews, heightImgNews;

    public ReceivedTextHolder(Context ctx, SmartTextClickListener smartTextListener, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
        widthImgNews = mApp.getResources().getDimensionPixelOffset(R.dimen.width_button_send_gift);
        heightImgNews = Math.round(widthImgNews / 1.77f);
    }

    public ReceivedTextHolder(Context ctx, SmartTextClickListener smartTextListener) {
        mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
        widthImgNews = mApp.getResources().getDimensionPixelOffset(R.dimen.width_button_send_gift);
        heightImgNews = Math.round(widthImgNews / 1.77f);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_text_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = rowView.findViewById(R.id.message_text_content);
        mImgLargeEmo = rowView.findViewById(R.id.message_detail_file_item_content);
        viewPreviewUrl = rowView.findViewById(R.id.view_preview_url);
        mTvwTitle = rowView.findViewById(R.id.tvw_title_preview_url);
        mTvwDesc = rowView.findViewById(R.id.tvw_desc_preview_url);
        mTvwSite = rowView.findViewById(R.id.tvw_site_preview_url);
        mImgThumb = rowView.findViewById(R.id.img_thumb_preview_url);
        llActionChangeNumber = rowView.findViewById(R.id.llActionChangeNumber);
        btnChat = rowView.findViewById(R.id.btnChatNew);
        btnSave = rowView.findViewById(R.id.btnSaveContact);
        btnSync = rowView.findViewById(R.id.btnSync);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setContext(Context ctx) {
        mContext = ctx;
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1_5));
        mTvwSite.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1_5));
        viewPreviewUrl.setVisibility(View.GONE);
        final MessageInteractionListener mMessageInteractionListener = getMessageInteractionListener();
        View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mMessageInteractionListener != null)
                    mMessageInteractionListener.longClickBgrCallback(message);
                return false;
            }
        };
        if (message.getMessageType() == ReengMessageConstant.MessageType.restore) {
            mTvwContent.setNormalText(getContext().getResources().getString(R.string.message_restored),
                    mSmartTextListener, longClickListener);
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_message_time));
        } else if (ReengMessageConstant.MESSAGE_ENCRYPTED.equals(message.getMessageEncrpyt())) {
            mTvwContent.setNormalText(getContext().getResources().getString(R.string.encrypt_message),
                    mSmartTextListener, longClickListener);
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_message_time));
        } else {
            /*if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
            } else {
            }*/
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
            if (message.getChatMode() != ReengMessageConstant.MODE_GSM && message.isLargeEmo() && isNoReply()) {
                mTvwContent.setVisibility(View.GONE);
                mImgLargeEmo.setVisibility(View.VISIBLE);
                ImageLoaderManager.getInstance(mContext).displayLargeEmoticon(mImgLargeEmo, message.getContent());
            } else if (message.isShowTranslate() && !TextUtils.isEmpty(message.getTextTranslated())) {
                mTvwContent.setVisibility(View.VISIBLE);
                mImgLargeEmo.setVisibility(View.GONE);
                mTvwContent.setEmoticon(message.getTextTranslated(), message.getId(), mSmartTextListener,
                        longClickListener, message);
            } else {
                mTvwContent.setVisibility(View.VISIBLE);
                mImgLargeEmo.setVisibility(View.GONE);

                if (message.getListTagContent() == null || message.getListTagContent().isEmpty()) {
                    mTvwContent.setEmoticon(message.getContent(), message.getId(), mSmartTextListener, longClickListener,
                            message);
                } else {
                    mTvwContent.setEmoticonWithTag(mApp, message.getContent(), message.getId(), message,
                            message.getListTagContent(), onClickTag, mSmartTextListener, longClickListener);
                }
                setDataPreviewUrl(mMessageInteractionListener);
            }
        }
        if (isQuickReply) {
            mTvwContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMessageInteractionListener != null)
                        mMessageInteractionListener.textContentClickCallBack(message);
                }
            });
        }
        mTvwContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMessageInteractionListener != null)
                    mMessageInteractionListener.textContentClickCallBack(message);
            }
        });

        if (message.getMessageType() == ReengMessageConstant.MessageType.text && !TextUtils.isEmpty(message.getImageUrl())) {
            llActionChangeNumber.setVisibility(View.VISIBLE);
            setActionViewListener();
        } else {
            llActionChangeNumber.setVisibility(View.GONE);
        }
    }

    private void setActionViewListener() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().onClickActionChangeNumber(message, ReengMessageConstant.ActionChangeNumber.saveContact);
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().onClickActionChangeNumber(message, ReengMessageConstant.ActionChangeNumber.createChat);
            }
        });

        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().onClickActionChangeNumber(message, ReengMessageConstant.ActionChangeNumber.syncChat);
            }
        });
    }

    private void setDataPreviewUrl(final MessageInteractionListener mMessageInteractionListener) {
        if (isQuickReply) return;
        if (TextUtils.isEmpty(message.getFilePath())) {
            MessageHelper.checkFirstUrlAndGetMetaData(message, mApp);
        } else {
            Log.d("ReceivedTextHolder", "filepath: " + message.getFilePath());
            FeedContent feedContent = null;
            try {
                feedContent = new Gson().fromJson(message.getFilePath(), FeedContent.class);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (feedContent != null) {
                if (TextUtils.isEmpty(feedContent.getImageUrl())) {
                    viewPreviewUrl.setVisibility(View.GONE);
                } else {
                    viewPreviewUrl.setVisibility(View.VISIBLE);
                    mImgThumb.setVisibility(View.VISIBLE);
                    ImageLoaderManager.getInstance(mApp).setImageFeeds(mImgThumb, feedContent.getImageUrl(),
                            widthImgNews, heightImgNews);
                    if (!TextUtils.isEmpty(feedContent.getItemName())) {
                        mTvwTitle.setVisibility(View.VISIBLE);
                        mTvwTitle.setText(feedContent.getItemName());
                    } else {
                        mTvwTitle.setVisibility(View.GONE);
                    }
                    if (!TextUtils.isEmpty(feedContent.getDescription())) {
                        mTvwDesc.setVisibility(View.VISIBLE);
                        mTvwDesc.setText(feedContent.getDescription());
                    } else {
                        mTvwDesc.setVisibility(View.GONE);
                    }
                    if (!TextUtils.isEmpty(feedContent.getSite())) {
                        mTvwSite.setVisibility(View.VISIBLE);
                        mTvwSite.setText(feedContent.getSite());
                    }
                    final String previewUrl = feedContent.getUrl();
                    viewPreviewUrl.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mMessageInteractionListener != null)
                                mMessageInteractionListener.onClickPreviewUrl(message, previewUrl);
                        }
                    });
                }
            }
        }
    }
}
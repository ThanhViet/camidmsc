/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/16
 */

package com.metfone.selfcare.holder.onmedia.feeds;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;

public class OMFeedAudioViewHolder extends OMFeedBaseViewHolder {
    private FeedModelOnMedia feedModel;
    private View btnOpenKeeng;
    private TextView mTvwSinger;
    private ImageView mIvCover;
    private BaseSlidingFragmentActivity activity;

    public OMFeedAudioViewHolder(View itemView, ApplicationController mApplication, BaseSlidingFragmentActivity activity) {
        super(itemView, mApplication);
        this.activity = activity;
        mTvwSinger = itemView.findViewById(R.id.tvw_singer);
        mIvCover = itemView.findViewById(R.id.iv_cover);
        btnOpenKeeng = itemView.findViewById(R.id.button_open_keeng);
    }

    @Override
    public void setElement(Object obj) {
        feedModel = (FeedModelOnMedia) obj;
        setBaseElements(obj);
        if (!TextUtils.isEmpty(feedModel.getFeedContent().getSinger())) {
            mTvwSinger.setVisibility(View.VISIBLE);
            mTvwSinger.setText(feedModel.getFeedContent().getSinger());
        } else {
            mTvwSinger.setVisibility(View.GONE);
            mTvwSinger.setText("");
        }
        ImageBusiness.setImageBlur(mIvCover, feedModel.getFeedContent().getImageUrl());
        if (mIvCover != null) mIvCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().onClickMediaItem(feedModel);
            }
        });
        setupButtonOpenKeeng();
    }

    private void setupButtonOpenKeeng() {
        if (btnOpenKeeng != null && activity != null) {
            if (ViewUtils.appInstalledOrNot(activity, Constants.PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE)) {
                btnOpenKeeng.setVisibility(View.GONE);
            } else {
                btnOpenKeeng.setVisibility(View.VISIBLE);
                btnOpenKeeng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NavigateActivityHelper.navigateToPlayStore(activity, Constants.PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE);
                    }
                });
            }
        }
    }
}

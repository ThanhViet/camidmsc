package com.metfone.selfcare.holder.message;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.images.ImageHelper;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.EmoTextViewListChat;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundRelativeLayout;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 11/21/2017.
 */

public class ReceivedImageHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedImageHolder.class.getSimpleName();
    private RoundedImageView content;
    private RoundRelativeLayout vLoading;
    private ProgressLoading mPrgLoading;
    private ImageView iconAttachment;
    private TextView fileName, mTvwLoadingAlert;
    private EmoTextViewListChat mTvwContent;
    private ReengMessage message;
    private ThreadMessage mThreadMessage;
    private SmartTextClickListener mSmartTextListener;
    private ApplicationController mApp;

    public ReceivedImageHolder(Context ctx, SmartTextClickListener smartTextListener, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        this.mSmartTextListener = smartTextListener;
        setContext(ctx);
    }

    public ReceivedImageHolder(Context ctx, SmartTextClickListener smartTextListener, ThreadMessage threadMessage) {
        this.mThreadMessage = threadMessage;
        this.mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_image_received, parent, false);
        initBaseHolder(rowView);
        content = (RoundedImageView) rowView.findViewById(R.id.message_detail_file_item_content);
        vLoading = (RoundRelativeLayout) rowView.findViewById(R.id.message_detail_file_loading);
        mPrgLoading = (ProgressLoading) rowView.findViewById(R.id.message_detail_file_loading_progress);
        mTvwLoadingAlert = (TextView) rowView.findViewById(R.id.message_detail_file_loading_label);
        fileName = (TextView) rowView.findViewById(R.id.message_detail_file_item_file_name);
        iconAttachment = (ImageView) rowView.findViewById(R.id.message_detail_file_item_attachment);
        mTvwContent = (EmoTextViewListChat) rowView.findViewById(R.id.message_text_content);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().longClickBgrCallback(message);
                return false;
            }
        };
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwLoadingAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        fileName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        if (isQuickReply) {
            content.setVisibility(View.GONE);
            iconAttachment.setVisibility(View.VISIBLE);
            fileName.setVisibility(View.VISIBLE);
            fileName.setText(mContext.getString(R.string.image_message));
            iconAttachment.setImageResource(R.drawable.ic_sendimage);
            vLoading.setVisibility(View.GONE);
            mTvwContent.setVisibility(View.GONE);
        } else {
            // set visible for content
            // set invisible for filename
            content.setVisibility(View.VISIBLE);
            fileName.setVisibility(View.GONE);
            iconAttachment.setVisibility(View.GONE);
            if (mThreadMessage != null && getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    mThreadMessage.isStranger() && !mThreadMessage.isJoined()) {
                vLoading.setVisibility(View.VISIBLE);
                vLoading.setBackgroundColorAndPress(ContextCompat.getColor(mContext, R.color.bg_color_blur_holder), 0);
                ImageHelper.setImageAlpha(content, 0.8f);
                mTvwLoadingAlert.setVisibility(View.VISIBLE);
                mPrgLoading.setVisibility(View.GONE);
            } else {
                mTvwLoadingAlert.setVisibility(View.GONE);
                ImageHelper.setImageAlpha(content, 1.0f);
                if (message.getStatus() == ReengMessageConstant.STATUS_LOADING
                        || message.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD) {
                    vLoading.setVisibility(View.VISIBLE);
                    mPrgLoading.setVisibility(View.VISIBLE);
                    vLoading.setBackgroundColorAndPress(ContextCompat.getColor(mContext,
                            R.color.bg_color_loading_holder), 0);
                } else {
                    vLoading.setVisibility(View.GONE);
                    mPrgLoading.setVisibility(View.GONE);
                }
            }
            if (TextUtils.isEmpty(message.getContent())) {
                mTvwContent.setVisibility(View.GONE);
            } else {
                mTvwContent.setVisibility(View.VISIBLE);
                mTvwContent.setEmoticon(message.getContent(), message.getId(), mSmartTextListener, longClickListener,
                        message);
            }
            Log.i(TAG, "ratio: " + message.getVideoContentUri() + " id: " + message.getId());
            if (TextUtils.isEmpty(message.getVideoContentUri())) {
                content.getLayoutParams().width = mApp.getWidthPixels() * 2 / 3;
                content.getLayoutParams().height = content.getLayoutParams().width;
            } else {
                float ratio = Float.parseFloat(message.getVideoContentUri());
                if (ratio >= 1) {
                    if (ratio >= 2) ratio = 2;
                    content.getLayoutParams().width = mApp.getWidthPixels() * 2 / 3;
                    content.getLayoutParams().height = Math.round(content.getLayoutParams().width / ratio);
                } else {
                    if (ratio <= 0.5f) ratio = 0.5f;
                    content.getLayoutParams().height = mApp.getHeightPixels() / 2;
                    content.getLayoutParams().width = Math.round(content.getLayoutParams().height * ratio);
                }
            }
            content.requestLayout();
            vLoading.requestLayout();
            showThumbnailOfImage(message, content, content.getLayoutParams().width, content.getLayoutParams().height);
        }
    }
}

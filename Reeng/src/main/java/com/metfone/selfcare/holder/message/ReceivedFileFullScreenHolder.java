package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

public class ReceivedFileFullScreenHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedFileFullScreenHolder.class.getSimpleName();
    private RoundedImageView content;
    private int width;
    private ApplicationController mApplication;
    private ViewGroup.LayoutParams contentParams;
    private View vLoading;
    private ImageView iconAttachment;
    private TextView fileName;
    private boolean isImage;
    private ReengMessage message;

    public ReceivedFileFullScreenHolder(Context ctx, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        mApplication = (ApplicationController) ctx.getApplicationContext();
        width = mApplication.getWidthPixels() - ctx.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_10);
        setContext(ctx);
    }

    public ReceivedFileFullScreenHolder(Context ctx) {
        mApplication = (ApplicationController) ctx.getApplicationContext();
        width = mApplication.getWidthPixels() - ctx.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_10);
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_file_full_received, parent, false);
        initBaseHolder(rowView);
        content = (RoundedImageView) rowView
                .findViewById(R.id.message_detail_file_item_content);
        vLoading = rowView
                .findViewById(R.id.message_detail_file_loading);
        fileName = (TextView) rowView
                .findViewById(R.id.message_detail_file_item_file_name);
        iconAttachment = (ImageView) rowView
                .findViewById(R.id.message_detail_file_item_attachment);
        rowView.setTag(this);
        setConvertView(rowView);
    }


    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        Log.d(TAG, "" + message.toString());
        setBaseElements(obj);
        isImage = false;
        vLoading.setVisibility(View.GONE);
        if (isQuickReply) {
            iconAttachment.setVisibility(View.VISIBLE);
            fileName.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            fileName.setText(mContext.getString(R.string.image_message));
            iconAttachment.setImageResource(R.drawable.ic_sendimage);
        } else {
            iconAttachment.setVisibility(View.GONE);
            fileName.setVisibility(View.GONE);
            content.setVisibility(View.VISIBLE);
            contentParams = (ViewGroup.LayoutParams) content.getLayoutParams();
            Log.d(TAG, "width: " + width);
            contentParams.height = width;
            contentParams.width = width;
            ImageLoaderManager.getInstance(getContext()).displayDetailOfMessageImageLink(content, message.getImageUrl());
        }
    }
}
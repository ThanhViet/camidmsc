package com.metfone.selfcare.holder.message.typing;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.message.TypingMessageAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 9/27/2017.
 */

public class TypingVideoHolder extends BaseViewHolder {
    private TypingMessageAdapter.TypingMessageListener mListener;
    private ApplicationController mApplication;
    private MediaModel mEntry;
    private ImageView mImgVideoThumb;
    private TextView mTvwVideoDesc;

    public TypingVideoHolder(ApplicationController application, View itemView, TypingMessageAdapter.TypingMessageListener listener) {
        super(itemView);
        this.mApplication = application;
        this.mListener = listener;
        mImgVideoThumb = (ImageView) itemView.findViewById(R.id.holder_typing_video_thumb);
        mTvwVideoDesc = (TextView) itemView.findViewById(R.id.holder_typing_video_desc);
    }

    @Override
    public void setElement(Object obj) {
        mEntry = (MediaModel) obj;
        mTvwVideoDesc.setText(mEntry.getName());
        mApplication.getAvatarBusiness().setSongAvatar(mImgVideoThumb, mEntry.getImage());
        getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onVideoClick(mEntry);
            }
        });
    }
}

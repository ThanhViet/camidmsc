package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThanhNT72 on 5/14/2015.
 */
public class ReceivedGreetingStickerHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedGreetingStickerHolder.class.getSimpleName();
    private TextView mTvwContent;
    private ImageView imgSticker1, imgSticker2;
    private TextView mTvwTouchToView1, mTvwTouchToView2;
    private LinearLayout layoutSticker;
    private ReengMessage message;
    private int collectionId1 = EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, itemId1 = 0;
    private int collectionId2 = EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, itemId2 = 0;
    private StickerBusiness mStickerBusiness;
    private StickerItem stickerItemSV1, stickerItemSV2;
    private StickerItem stickerItemDB1, stickerItemDB2;
    private ApplicationController mApplicationController;
    private List<StickerItem> listStickerItem;


    public ReceivedGreetingStickerHolder(ApplicationController ctx
            , MessageInteractionListener listener, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        setContext(ctx);
        mApplicationController = ctx;
        setMessageInteractionListener(listener);
        mStickerBusiness = ctx.getStickerBusiness();
        listStickerItem = new ArrayList<>();
    }

    public ReceivedGreetingStickerHolder(ApplicationController ctx, MessageInteractionListener listener) {
        setContext(ctx);
        mApplicationController = ctx;
        setMessageInteractionListener(listener);
        mStickerBusiness = ctx.getStickerBusiness();
        listStickerItem = new ArrayList<>();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_greeting_sticker_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        imgSticker1 = (ImageView) rowView.findViewById(R.id.img_sticker_item_1);
        imgSticker2 = (ImageView) rowView.findViewById(R.id.img_sticker_item_2);
        mTvwTouchToView1 = (TextView) rowView.findViewById(R.id.txt_touch_to_preview_1);
        mTvwTouchToView2 = (TextView) rowView.findViewById(R.id.txt_touch_to_preview_2);
        layoutSticker = (LinearLayout) rowView.findViewById(R.id.layout_send_sticker);
        rowView.setTag(this);
        setConvertView(rowView);
    }


    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        initListSticker();
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTouchToView1.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwTouchToView2.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));

        if (listStickerItem != null && listStickerItem.size() == 2) {
            //kiem tra sticker 1 da co trong db chua, chua co thi luu
            stickerItemSV1 = listStickerItem.get(0);
            collectionId1 = stickerItemSV1.getCollectionId();
            itemId1 = stickerItemSV1.getItemId();
            Log.i(TAG, "collectionId1: " + collectionId1 + " itemId1: " + itemId1);
            stickerItemDB1 = mStickerBusiness.getStickerItem(collectionId1, itemId1);
            // ko tim thay thi insert vao db
            if (stickerItemDB1 == null) {
                mStickerBusiness.insertNewStickerItem(stickerItemSV1);
            }

            //kiem tra sticker 2  da co trong db chua, chua co thi luu
            stickerItemSV2 = listStickerItem.get(1);
            collectionId2 = stickerItemSV2.getCollectionId();
            itemId2 = stickerItemSV2.getItemId();
            Log.i(TAG, "collectionId2: " + collectionId2 + " itemId2: " + itemId2);
            stickerItemDB2 = mStickerBusiness.getStickerItem(collectionId2, itemId2);
            // ko tim thay thi insert vao db
            if (stickerItemDB2 == null) {
                mStickerBusiness.insertNewStickerItem(stickerItemSV2);
            }

            layoutSticker.setVisibility(View.VISIBLE);

            //hien anh sticker 1
            if (stickerItemDB1 != null && stickerItemDB1.isDownloadImg()) {
                ImageLoaderManager.getInstance(mContext).displayStickerImageFromSdcard(imgSticker1, stickerItemDB1);
            } else {
                ImageLoaderManager.getInstance(mContext).displayGifStickerNetwork(imgSticker1,
                        stickerItemSV1.getUrlImg());
            }

            //hien anh sticker 2
            if (stickerItemDB2 != null && stickerItemDB2.isDownloadImg()) {
                ImageLoaderManager.getInstance(mContext).displayStickerImageFromSdcard(imgSticker2, stickerItemDB2);
            } else {
                ImageLoaderManager.getInstance(mContext).displayGifStickerNetwork(imgSticker2,
                        stickerItemSV2.getUrlImg());
            }
        }
        mTvwContent.setText(message.getContent());
        clickStickerListener();
        clickTextPreviewListener();
    }

    private void initListSticker() {
        if (message.getListStickerItem() == null) {
            Log.i(TAG, "parce list");
            try {
                listStickerItem = mStickerBusiness.parserJsonSticker(message.getFilePath());
                message.setListStickerItem(listStickerItem);
            } catch (Exception e) {
                Log.e(TAG,"Exception",e);
            }
        } else {
            Log.i(TAG, "da co trong memory");
            listStickerItem = message.getListStickerItem();
        }
    }

    private void clickTextPreviewListener() {
        mTvwTouchToView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stickerItemDB1 != null && stickerItemDB1.isDownloadImg()) {
                    getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItemDB1);
                } else if (stickerItemSV1 != null) {
                    getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItemSV1);
                }
            }
        });
        mTvwTouchToView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stickerItemDB2 != null && stickerItemDB2.isDownloadImg()) {
                    getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItemDB2);
                } else if (stickerItemSV2 != null) {
                    getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItemSV2);
                }
            }
        });
    }

    private void clickStickerListener() {
        imgSticker1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stickerItemDB1 != null && stickerItemSV1 != null) {
                    if (TextUtils.isEmpty(stickerItemDB1.getUrlImg())) {
                        stickerItemDB1.setUrlImg(stickerItemSV1.getUrlImg());
                    }
                    if (TextUtils.isEmpty(stickerItemDB1.getUrlVoice())) {
                        stickerItemDB1.setUrlVoice(stickerItemSV1.getUrlVoice());
                    }
                    getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItemDB1);
                }
            }
        });

        imgSticker2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stickerItemDB2 != null && stickerItemSV2 != null) {
                    if (TextUtils.isEmpty(stickerItemDB2.getUrlImg())) {
                        stickerItemDB2.setUrlImg(stickerItemSV2.getUrlImg());
                    }
                    if (TextUtils.isEmpty(stickerItemDB2.getUrlVoice())) {
                        stickerItemDB2.setUrlVoice(stickerItemSV2.getUrlVoice());
                    }
                    getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItemDB2);
                }
            }
        });
    }
}
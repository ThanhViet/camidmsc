package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.contact.SocialFriendInfo;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.listeners.ContactsHolderCallBack;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 9/6/2017.
 */

public class OfficialViewHolder extends BaseViewHolder {
    private Context mContext;
    private ContactsHolderCallBack mCallBack;
    private OfficerAccount entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private boolean needShowDes;

    private RoundedImageView mImgAvatar;
    private TextView mTvContactName, mTvStatus, mTvwAvatar;

    public OfficialViewHolder(View convertView, Context context, ContactsHolderCallBack callBack, boolean needShowDes) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.needShowDes = needShowDes;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        this.mCallBack = callBack;
        initView(convertView);
    }


    @Override
    public void setElement(Object obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof SocialFriendInfo) {
            setDetailView((SocialFriendInfo) obj);
        } else if (obj instanceof OfficerAccount) {
            entry = (OfficerAccount) obj;
            setDetailView();
        }
    }

    private void initView(View convertView)
    {
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        if (needShowDes) {
            mTvStatus = convertView.findViewById(R.id.item_contact_view_status_text);
        }
    }

    //set name, avatar,reeng state
    private void setDetailView() {
        // text
        mTvContactName.setText(entry.getName());
        if (needShowDes) {
            if (entry.getDescription() != null) {
                mTvStatus.setVisibility(View.VISIBLE);
                mTvStatus.setText(entry.getDescription());
            } else {
                mTvStatus.setVisibility(View.GONE);
            }
        }
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, size, entry.getServerId(), entry, false);
        if (mCallBack != null) {
            getItemView().setOnClickListener(v -> mCallBack.onItemClick(entry));
        }
    }

    private void setDetailView(SocialFriendInfo entry) {
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        String jidNumber = entry.getUserFriendJid();
        if (entry.getUserType() == UserInfo.USER_ONMEDIA_NORMAL) {
            PhoneNumber phoneNumber = entry.getPhoneNumber();
            if (phoneNumber != null) {
                mTvContactName.setText(phoneNumber.getName());
                avatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, size);
            } else {
                String friendName = entry.getUserName();
                if (TextUtils.isEmpty(entry.getUserName())) {
                    friendName = Utilities.hidenPhoneNumber(jidNumber);
                }
                mTvContactName.setText(friendName);
                String avatarUrl = null;
                if (entry.isUserMocha()) {
                    if (!TextUtils.isEmpty(entry.getUserAvatar())) {
                        avatarUrl = avatarBusiness.getAvatarUrl(entry.getUserAvatar(), jidNumber, size);
                    }
                } else {
                    avatarUrl = entry.getUserAvatar();
                }
                avatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar, avatarUrl, jidNumber, friendName, size);
            }
        } else {
            ImageLoaderManager.getInstance(mApplication).setAvatarOfficialOnMedia(mImgAvatar, entry.getUserAvatar(), size);
            mTvContactName.setText(entry.getUserName());
        }
        getItemView().setOnClickListener(v -> mCallBack.onAvatarClick(entry));
    }
}
package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.imageview.LixiImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 1/24/2018.
 */

public class ReceivedGiftLixiHolder extends BaseMessageHolder {

    private static final String TAG = ReceivedGiftLixiHolder.class.getSimpleName();

    private ApplicationController mApp;
    private Resources mRes;
    private ReengMessage message;
    private LixiImageView mImgBg;
    private TextView mTvwContentLixi;
    private TextView mTvwAmountMoney;
    private RoundTextView mBtnOpen;
    private RoundTextView mBtnReplyLixi;
    private View mViewReplyLixi;


    public ReceivedGiftLixiHolder(Context ctx, MessageInteractionListener listener) {
        setContext(ctx);
        setMessageInteractionListener(listener);
        mApp = (ApplicationController) ctx;
        mRes = mApp.getResources();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_gift_lixi_received, parent, false);
        mImgBg = (LixiImageView) rowView.findViewById(R.id.img_bg_holder);
        mTvwContentLixi = (TextView) rowView.findViewById(R.id.tvw_content_send_lixi);
        mTvwAmountMoney = (TextView) rowView.findViewById(R.id.tvw_holder_amount_money);
        mBtnOpen = (RoundTextView) rowView.findViewById(R.id.tvw_open_gift_lixi);
        mViewReplyLixi = rowView.findViewById(R.id.view_reply_lixi);
        mBtnReplyLixi = (RoundTextView) rowView.findViewById(R.id.tvw_reply_lixi);
        initBaseHolder(rowView);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        drawDetail();
        setViewListener();
        /*final View view = getViewBorderMessageLayout();
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams ll = mImgBg.getLayoutParams();
                Log.i(TAG, "width: " + view.getWidth() + " height: " + view.getHeight());
                ll.width = view.getWidth();
                ll.height = view.getHeight(); //height is ready
                mImgBg.setLayoutParams(ll);

            }
        });*/
    }

    private void drawDetail() {
        //size = 0 nghia la chua mo
        if (message.getSize() == 0) {
            mViewReplyLixi.setVisibility(View.GONE);
            mBtnOpen.setVisibility(View.VISIBLE);
            mTvwAmountMoney.setVisibility(View.GONE);
            ThreadMessage threadMessage = mApp.getMessageBusiness().getThreadById(message.getThreadId());
            if (threadMessage != null) {
                String nameBold;
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    PhoneNumber phoneNumber = mApp.getContactBusiness().getPhoneNumberFromNumber(message.getSender());
                    String name;
                    if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                        name = phoneNumber.getName();
                    } else {                      //hien thi so dien thoai neu la group chat
                        name = message.getSender();
                    }
                    nameBold = TextHelper.textBoldWithHTML(name);
                } else {
                    nameBold = TextHelper.textBoldWithHTML(threadMessage.getThreadName());
                }
                String content = String.format(mRes.getString(R.string.text_receive_gift_lixi_holder), nameBold);
                mTvwContentLixi.setText(TextHelper.fromHtml(content));
                mTvwContentLixi.setVisibility(View.VISIBLE);
            } else {
                Log.e(TAG, "threadMessage null");
                mTvwContentLixi.setVisibility(View.GONE);
            }
        } else {
            mViewReplyLixi.setVisibility(View.VISIBLE);
            mBtnOpen.setVisibility(View.GONE);
            mTvwAmountMoney.setVisibility(View.VISIBLE);
            mTvwContentLixi.setText(message.getContent());

            String amountMoney = TextHelper.formatTextDecember(message.getImageUrl());
            mTvwAmountMoney.setText(String.format(mRes.getString(R.string.unit_vnd), amountMoney));


            getViewBorderMessageLayout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getMessageInteractionListener() != null) {
                        getMessageInteractionListener().onClickOpenGiftLixi(message);
                    }
                }
            });
        }
    }

    private void setViewListener() {
        mBtnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getMessageInteractionListener() != null) {
                    getMessageInteractionListener().onClickOpenGiftLixi(message);
                }
            }
        });

        mBtnReplyLixi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getMessageInteractionListener() != null) {
                    getMessageInteractionListener().onClickReplyLixi(message);
                }
            }
        });
    }
}

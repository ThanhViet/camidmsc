/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.holder.content;

import android.app.Activity;
import android.content.res.Resources;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentChannel;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;

public class ChannelDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.button_option)
    @Nullable
    View btnOption;
    private Object data;
    private Resources res;

    public ChannelDetailHolder(View view, Activity activity, final OnClickContentChannel listener) {
        super(view);
        res = activity.getResources();
        initListener(listener);
    }

    public ChannelDetailHolder(View view, Activity activity, OnClickContentChannel listener, int widthLayout) {
        super(view);
        res = activity.getResources();
        initListener(listener);
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    private void initListener(final OnClickContentChannel listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickChannelItem(data, getAdapterPosition());
                }
            }
        });
        if (btnOption != null) btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreChannelItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void bindDataHome(Content content) {
        if (tvTitle != null) tvTitle.setText(content.getName());
        if (tvDesc != null) tvDesc.setText(content.getDescription());
        ImageBusiness.setChannelVideo(content.getImage(), ivCover);
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        } else if (item instanceof Channel) {
            Channel model = (Channel) item;
            if (tvTitle != null) tvTitle.setText(model.getName());
            if (tvDesc != null) {
                tvDesc.setText(getTextFollow(model.getNumFollow()));
                tvDesc.setVisibility((model.getNumFollow() > 0) ? View.VISIBLE : View.GONE);
            }
            ImageBusiness.setChannelVideo(model.getUrlImage(), ivCover);
        }
    }

    private String getTextFollow(long numFollow) {
        if (numFollow <= 0) return "";
        if (res != null) {
            if (numFollow == 1)
                return String.format(res.getString(R.string.onmedia_follower), "1");
            else
                return String.format(res.getString(R.string.onmedia_followers), Utilities.shortenLongNumber(numFollow));
        }
        return String.valueOf(numFollow);
    }
}
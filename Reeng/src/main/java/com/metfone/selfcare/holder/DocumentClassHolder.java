package com.metfone.selfcare.holder;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.DocumentClass;

/**
 * Created by Toanvk2 on 5/6/2017.
 */

public class DocumentClassHolder extends BaseViewHolder implements View.OnClickListener {
    private Context mContext;
    private DocumentClass entry;
    private OnDocumentListener mListener;
    private TextView mTvwName;
    private Button mBtnSend;

    public DocumentClassHolder(View convertView, Context context, OnDocumentListener listener) {
        super(convertView);
        this.mContext = context;
        this.mListener = listener;
        mTvwName = convertView.findViewById(R.id.holder_document_name);
        mBtnSend = convertView.findViewById(R.id.holder_document_button);
    }

    @Override
    public void setElement(Object obj) {
        entry = (DocumentClass) obj;
        mTvwName.setText(entry.getName());
        setViewListeners();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.holder_document_button:
                mListener.onSendDocument(entry);
                break;
        }
    }

    private void setViewListeners() {
        mBtnSend.setOnClickListener(this);
    }

    public interface OnDocumentListener {
        void onSendDocument(DocumentClass document);
    }
}
package com.metfone.selfcare.holder;

import android.content.Context;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;

/**
 * Created by thaodv on 7/7/2014.
 */
public class ThreadGroupHolder extends AbsContentHolder {
    private ThreadGroupHolder mThreadMessageHolder;
    private Context mContext;
    private CircleImageView mImgThreadAvatar;
    private ImageView mImgSong;
    private TextView mTvwThreadName, mTvwAvatar, mTvwUnreadMsg;
    private EllipsisTextView mTvwLastContent;
    private ApplicationController mApplication;

    public ThreadGroupHolder(Context context) {
        mThreadMessageHolder = this;
        mContext = context;
        mApplication = (ApplicationController) mContext.getApplicationContext();
    }

    @Override
    public void setElemnts(Object obj) {
        ThreadMessage mThreadMessage = (ThreadMessage) obj;
        // hien thi thong tin contact trong thread
        setMessageView(mThreadMessage);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position,
                           LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_thread_layout, parent, false);
        mImgThreadAvatar = (CircleImageView) rowView.findViewById(R.id.thread_avatar);
        // mImgStateStranger = (ImageView) rowView.findViewById(R.id.avatar_stranger_img);
        mTvwAvatar = (TextView) rowView.findViewById(R.id.contact_avatar_text);
        mTvwThreadName = (TextView) rowView.findViewById(R.id.thread_name);
        mImgSong = (ImageView) rowView.findViewById(R.id.thread_song_ic);
        mTvwLastContent = (EllipsisTextView) rowView.findViewById(R.id.thread_last_content);
        //mLlStatusImage = (LinearLayout) rowView.findViewById(R.id.thread_last_status_parent);
        TextView mTvwThreadType = (TextView) rowView.findViewById(R.id.thread_holder_type);
        mTvwUnreadMsg = (TextView) rowView.findViewById(R.id.thread_number_unread);
        TextView mTvwTime = (TextView) rowView.findViewById(R.id.thread_last_time);
        mTvwThreadType.setVisibility(View.GONE);
        mTvwUnreadMsg.setVisibility(View.GONE);
        mImgSong.setVisibility(View.GONE);
        mTvwTime.setVisibility(View.GONE);
        rowView.setTag(mThreadMessageHolder);
        setConvertView(rowView);
    }

    private void setMessageView(ThreadMessage threadMessage) {
        //lay tin nhan cuoi cung cua Thread
        Resources res = mContext.getResources();
        mTvwLastContent.setTextColor(ContextCompat.getColor(mContext, R.color.mocha_message_read));
        //mLlStatusImage.setVisibility(View.GONE);
        // mImgStateStranger.setVisibility(View.GONE);

        String threadName = mApplication.getMessageBusiness().getThreadName(threadMessage);
        if (!TextUtils.isEmpty(threadName)) {
            mTvwThreadName.setText(threadName);
        } else {
            mTvwThreadName.setText("");
        }
        int sizeAvatar = (int) res.getDimension(R.dimen.avatar_small_size);
        if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            // avatar
            mTvwAvatar.setVisibility(View.GONE);        // Text
            mApplication.getAvatarBusiness().setGroupThreadAvatar(mImgThreadAvatar, threadMessage);
            // mImgStateStranger.setVisibility(View.GONE);
            // danh sach thanh vien
            String friendListName = mApplication.getContactBusiness().
                    getListNameOfListNumber(threadMessage.getPhoneNumbers());
            if (!TextUtils.isEmpty(friendListName)) {
                mTvwLastContent.setText(friendListName);
            } else {
                mTvwLastContent.setText(res.getString(R.string.group_friend_empty));
            }
        } else if (threadMessage.isStranger() && threadMessage.getStrangerPhoneNumber() != null) {
//            StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
            // mImgStateStranger.setVisibility(View.VISIBLE);
            mTvwAvatar.setVisibility(View.GONE);
            mTvwLastContent.setVisibility(View.GONE);
           /* if (stranger.getStrangerType() == StrangerPhoneNumber.StrangerType.other_app_stranger) {
                mImgStateStranger.setImageResource(R.drawable.ic_stranger_keeng);
            } else {
                mImgStateStranger.setImageResource(R.drawable.ic_stranger_music);
            }*/
            mApplication.getAvatarBusiness().setStrangerAvatar(mImgThreadAvatar, mTvwAvatar,
                    threadMessage.getStrangerPhoneNumber(),
                    threadMessage.getStrangerPhoneNumber().getPhoneNumber(), null, null, sizeAvatar);
        } else {
            mTvwAvatar.setVisibility(View.GONE);
            // mImgStateStranger.setVisibility(View.GONE);
            mTvwLastContent.setVisibility(View.GONE);
        }
        /*int messageSize = threadMessage.getAllMessages().size();
        if (messageSize <= 0) {
            mTvwLastTime.setVisibility(View.INVISIBLE);
        } else {
            mTvwLastTime.setVisibility(View.VISIBLE);
            mTvwLastTime.setText(TimeHelper.formatCommonTime(threadMessage.getAllMessages().get(messageSize - 1).getTime(), TimeHelper.getCurrentTime(), res));
        }*/
    }
}
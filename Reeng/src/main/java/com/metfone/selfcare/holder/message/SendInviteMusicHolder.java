package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.message.CountDownInviteManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanh on 3/23/2015.
 */
public class SendInviteMusicHolder extends BaseMessageHolder {

    private static final String TAG = SendInviteMusicHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private ReengMessage message;
    private MediaModel mediaModel;
    private Button mBtnCancel;
    private Button mBtnReInvite, mBtnTryPlay;
    private Button mBtnViaFB;
    private TextView mTvwTitle;
    private TextView mTvwSong;
    private TextView mTvwSinger;
    private TextView mTvwCountDown;
    private ImageView mImgAvtSong;
    private RelativeLayout mLayoutReinvite;
    private RelativeLayout mLayoutUserAction, mLayoutUserGroupAction;
    private ImageButton mIbnGiftCrbt;

    public SendInviteMusicHolder(ApplicationController app, MessageInteractionListener listener) {
        setContext(app);
        mApplication = app;
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_invite_music_send, parent, false);
        initBaseHolder(rowView);
        mTvwTitle = (TextView) rowView.findViewById(R.id.message_share_share_music_send_title);
        mTvwSong = (TextView) rowView.findViewById(R.id.message_share_music_send_song_name);
        mTvwSinger = (TextView) rowView.findViewById(R.id.message_share_music_send_song_singer);
        mTvwCountDown = (TextView) rowView.findViewById(R.id.count_down_time);
        mBtnCancel = (Button) rowView.findViewById(R.id.btn_cancel_music);
        mBtnReInvite = (Button) rowView.findViewById(R.id.btn_reinvite_music);
        mBtnTryPlay = (Button) rowView.findViewById(R.id.btn_try_play_music);
        mBtnViaFB = (Button) rowView.findViewById(R.id.btn_reinvite_via_fb);
        mImgAvtSong = (ImageView) rowView.findViewById(R.id.message_share_music_send_avatar);
        mLayoutReinvite = (RelativeLayout) rowView.findViewById(R.id.layout_reinvite_music);
        mLayoutUserAction = (RelativeLayout) rowView.findViewById(R.id.layout_user_action_music);
        mLayoutUserGroupAction = (RelativeLayout) rowView.findViewById(R.id.layout_user_action_group_music);
        mIbnGiftCrbt = (ImageButton) rowView.findViewById(R.id.message_gift_crbt);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        checkAndUpdateStatus();
        drawHolder(message);
        setListener();
    }

    private void drawHolder(ReengMessage message) {
        mTvwCountDown.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSong.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSinger.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnCancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnReInvite.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnTryPlay.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnViaFB.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        String title = message.getFileName();
        mTvwTitle.setText(TextHelper.fromHtml(title));
        mediaModel = message.getSongModel(mApplication.getMusicBusiness());
        mTvwCountDown.setVisibility(View.GONE);
        if (mediaModel != null) {
            Log.d(TAG, "drawHolder: " + mediaModel.toString());
            // get data
            String songName = mediaModel.getName();
            String songSinger = mediaModel.getSinger();
            String avatarUrl = mediaModel.getImage();
            // draw
            if (!TextUtils.isEmpty(songName)) {
                mTvwSong.setText(songName);
            } else {
                mTvwSong.setText("");
            }
            if (!TextUtils.isEmpty(songSinger)) {
                mTvwSinger.setText(songSinger);
            } else {
                mTvwSinger.setText("");
            }
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, avatarUrl);
            if ("0".equals(mediaModel.getCrbtCode())) {
                mIbnGiftCrbt.setVisibility(View.GONE);
            } else if (mApplication.getConfigBusiness().isEnableCrbt() &&
                    getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT &&
                    !TextUtils.isEmpty(mediaModel.getCrbtCode())) {
                mIbnGiftCrbt.setVisibility(View.VISIBLE);
            } else {
                mIbnGiftCrbt.setVisibility(View.GONE);
            }
        } else {
            // neu chua co media thi get from sv, sau khi get duoc thi notify o thread detail
            mTvwSong.setText("");
            mTvwSinger.setText("");
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, "");
            // mApplication.getMusicBusiness().getMediaModelFromServer(message.getSongId(), message.getThreadId());
            mIbnGiftCrbt.setVisibility(View.GONE);
        }
    }

    private void checkAndUpdateStatus() {
        if (message.getId() > 0) {// message luu db co id, id==0 thi ko xu ly
            CountDownInviteManager.getInstance(mApplication).addHolderMessage(SendInviteMusicHolder.this, message.getId());
        }
        // set trang thai theo state music
        if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_ROOM) {
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
        } else if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_GROUP ||
                message.getMusicState() == ReengMessageConstant.MUSIC_STATE_REQUEST_CHANGE) {
            // cung nghe group thi chi hien dong y
            mLayoutUserGroupAction.setVisibility(View.GONE);
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserAction.setVisibility(View.GONE);
        } else if (message.getStatus() == ReengMessageConstant.STATUS_NOT_SEND) {// tin nhan chua gui
            mLayoutUserGroupAction.setVisibility(View.GONE);
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserAction.setVisibility(View.VISIBLE);
            mBtnCancel.setEnabled(false);
        } else if (message.getStatus() == ReengMessageConstant.STATUS_FAIL) {// th sau 10s ko nhan dc receive thi bao fail
            mLayoutReinvite.setVisibility(View.VISIBLE);
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
            mBtnTryPlay.setVisibility(View.GONE);
        } else if (message.getStatus() == ReengMessageConstant.STATUS_SENT ||
                message.getStatus() == ReengMessageConstant.STATUS_DELIVERED ||
                message.getStatus() == ReengMessageConstant.STATUS_SEEN) {// tin da gui, da nhan, da xem
            mLayoutUserGroupAction.setVisibility(View.GONE);
            if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_ACCEPTED) {
                mLayoutUserAction.setVisibility(View.GONE);
                mLayoutReinvite.setVisibility(View.GONE);
            } else if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_TIME_OUT ||
                    message.getMusicState() == ReengMessageConstant.MUSIC_STATE_CANCEL) {
                mLayoutReinvite.setVisibility(View.VISIBLE);
                mLayoutUserAction.setVisibility(View.GONE);
                mBtnTryPlay.setVisibility(View.GONE);
            } else if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_WAITING) {
                mLayoutReinvite.setVisibility(View.GONE);
                mBtnCancel.setEnabled(true);
                mLayoutUserAction.setVisibility(View.VISIBLE);
                // tin nhan gui thi ko hien thoi gian dem nguoc
                if (!message.isCounting()) {
                    long time = TimeHelper.getCurrentTime() - message.getTime();
                    if (time > CountDownInviteManager.COUNT_DOWN_DURATION) {// qua thoi gian timeout
                        message.setMusicState(ReengMessageConstant.MUSIC_STATE_TIME_OUT);
                        message.setDuration(0);
                        mApplication.getMessageBusiness().updateSendInviteMessageTimeout(message);
                        CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(message);
                    } else {
                        message.setDuration(CountDownInviteManager.COUNT_DOWN_DURATION - ((int) time));
                        CountDownInviteManager.getInstance(mApplication).startCountDownMessage(message);
                    }
                    // update DB
                    ApplicationController application = (ApplicationController) getContext().getApplicationContext();
                    MessageBusiness messageBusiness = application.getMessageBusiness();
                    messageBusiness.updateAllFieldsOfMessage(message);
                }
                mTvwCountDown.setText(StopWatch.convertSecondInMMSSForm(true, message.getDuration() / 1000));
            } else {// ngoai le thi an het
                mLayoutUserAction.setVisibility(View.GONE);
                mLayoutReinvite.setVisibility(View.GONE);
            }
        } else {// ngoai le thi an het
            mLayoutUserAction.setVisibility(View.GONE);
            mLayoutReinvite.setVisibility(View.GONE);
            mLayoutUserGroupAction.setVisibility(View.GONE);
        }
    }

    private void setListener() {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountDownInviteManager.getInstance(mApplication).stopCountDownMessage(message);
                getMessageInteractionListener().onCancelInviteMusicClick(message);
                // update UI
            }
        });
        mBtnTryPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onTryPlayMusicClick(message);
            }
        });
        mBtnReInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onReinviteShareMusicClick(message);
            }
        });
        mBtnViaFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onInviteMusicViaFBClick(message);
            }
        });
        mIbnGiftCrbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onSendCrbtGift(message, mediaModel);
            }
        });
    }
}
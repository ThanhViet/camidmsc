/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.holder.content;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.listeners.OnClickContentMusic;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.model.SearchModel;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;

import butterknife.BindView;

public class MusicDetailHolder extends BaseAdapter.ViewHolder {

    protected Object data;
    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.tv_total_view)
    @Nullable
    TextView tvTotalView;
    @BindView(R.id.tv_datetime)
    @Nullable
    TextView tvDatetime;
    @BindView(R.id.button_option)
    @Nullable
    View btnOption;

    private String domainImage;

    public MusicDetailHolder(View view, Activity activity, final OnClickContentMusic listener) {
        super(view);
        initListener(listener);
        this.domainImage = UrlConfigHelper.getInstance(activity).getDomainKeengMusicImage();
    }

    public MusicDetailHolder(View view, Activity activity, final OnClickContentMusic listener, int widthLayout) {
        super(view);
        initListener(listener);
        this.domainImage = UrlConfigHelper.getInstance(activity).getDomainKeengMusicImage();
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    private void initListener(final OnClickContentMusic listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMusicItem(data, getAdapterPosition());
                }
            }
        });
        if (btnOption != null) btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreMusicItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void bindDataHome(Content content) {
        if (tvTitle != null) tvTitle.setText(content.getName());
        if (tvDesc != null) {
            if (TextUtils.isEmpty(content.getSingerName())) {
                tvDesc.setVisibility(View.GONE);
            } else {
                tvDesc.setText(content.getSingerName());
                tvDesc.setVisibility(View.VISIBLE);
            }
        }
        if (tvTotalView != null) tvTotalView.setVisibility(View.GONE);
        if (tvDatetime != null) tvDatetime.setVisibility(View.GONE);
        ImageBusiness.setMusic(content.getImage(), ivCover);
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        } else if (item instanceof SearchModel) {
            SearchModel model = (SearchModel) item;
            if (tvTitle != null) tvTitle.setText(model.getFullName());
            if (tvDesc != null) {
                if (TextUtils.isEmpty(model.getFullSinger())) {
                    tvDesc.setVisibility(View.GONE);
                } else {
                    tvDesc.setText(model.getFullSinger());
                    tvDesc.setVisibility(View.VISIBLE);
                }
            }
            if (tvTotalView != null) tvTotalView.setVisibility(View.GONE);
            if (tvDatetime != null) tvDatetime.setVisibility(View.GONE);
            ImageBusiness.setMusic(domainImage + model.getImage(), ivCover);

        }
    }
}
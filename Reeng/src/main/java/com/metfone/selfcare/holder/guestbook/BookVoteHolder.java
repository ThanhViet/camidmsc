package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.editor.sticker.utils.Constants;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.BookVote;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.glide.GlideImageLoader;

/**
 * Created by toanvk2 on 4/26/2017.
 */
public class BookVoteHolder extends BaseViewHolder implements View.OnClickListener {
    private Context mContext;
    private BookVote entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private OnBookVoteListener mListener;
    private ImageView mImgPreview;
    private TextView mTvwTotalVote;
    private ProgressLoading mProgressLoading;
    //private TextView mTvwName;
    private TextView mTvwAuthor;
    private Button mBtnDetail;
    private int screenWidth;

    public BookVoteHolder(View convertView, Context context, OnBookVoteListener listener) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mContext.getResources();
        this.mListener = listener;
        mImgPreview = (ImageView) convertView.findViewById(R.id.vote_image_preview);
        //mTvwName = (TextView) convertView.findViewById(R.id.vote_name);
        mTvwAuthor = (TextView) convertView.findViewById(R.id.vote_author);
        mTvwTotalVote = (TextView) convertView.findViewById(R.id.vote_total_votes);
        mBtnDetail = (Button) convertView.findViewById(R.id.vote_button);
        mProgressLoading = (ProgressLoading) convertView.findViewById(R.id.progress_loading_img);
        // set layout param
        screenWidth = mApplication.getWidthPixels();

        ViewGroup.LayoutParams imageParams = mImgPreview.getLayoutParams();
        imageParams.width = screenWidth / 3;
        imageParams.height = (int) (imageParams.width * Constants.SCALE_HEIGHT);

        /*ViewGroup.LayoutParams params = mImgBackground.getLayoutParams();
        params.width = screenWidth / 3;
        params.height = (int) (params.width * Constants.SCALE_HEIGHT);*/

        /*ViewGroup.LayoutParams parentParams = convertView.getLayoutParams();
        int itemHeight = (int) (screenWidth * Constants.SCALE_HAFL_HEIGHT);
        parentParams.height = itemHeight;*/
    }

    @Override
    public void setElement(Object obj) {
        entry = (BookVote) obj;
        //mTvwName.setText(entry.getName());
        mTvwAuthor.setText(entry.getOwnerName());
        mTvwTotalVote.setText(String.format(mRes.getString(R.string.guest_book_holder_vote), entry.getVoteTotal()));
        String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(entry.getPreview());
        ImageLoaderManager.getInstance(mContext).displayGuestBookPreview(mImgPreview, url, new GlideImageLoader.SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted() {
                mProgressLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(GlideException failReason) {
                mProgressLoading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete() {
                mProgressLoading.setVisibility(View.GONE);
            }
        });
        setViewListeners();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.vote_button:
                mListener.viewDetail(entry);
                break;
            /*case R.id.page_edit_delete:
                mListener.onDelete(entry);
                break;*/
        }
    }

    private void setViewListeners() {
        mBtnDetail.setOnClickListener(this);
    }

    public interface OnBookVoteListener {
        void viewDetail(BookVote bookVote);
    }
}

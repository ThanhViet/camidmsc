package com.metfone.selfcare.holder.onmedia;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.ui.imageview.CircleImageView;

/**
 * Created by thanhnt72 on 3/7/2016.
 */
public class OnmediaTagHolder extends AbsContentHolder {


    private static final String TAG = OnmediaTagHolder.class.getSimpleName();
    private CircleImageView mImgAvatar;
    private TextView mTvwName, mTvwAvatar;
    private PhoneNumber mPhoneNumber;
    private ApplicationController mApplication;
    private Resources mRes;

    public OnmediaTagHolder(ApplicationController mApplication) {
        this.mApplication = mApplication;
        mRes = mApplication.getResources();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        View convertView = layoutInflater.inflate(R.layout.holder_tag_mocha, parent, false);
        mTvwName = (TextView) convertView.findViewById(R.id.tvw_onmedia_user);
        mImgAvatar = (CircleImageView) convertView.findViewById(R.id.img_onmedia_avatar);
        mTvwAvatar = (TextView) convertView.findViewById(R.id.tvw_onmedia_avatar);
        convertView.setTag(this);
        setConvertView(convertView);
    }

    @Override
    public void setElemnts(Object obj) {
        this.mPhoneNumber = (PhoneNumber) obj;
        mTvwName.setText(mPhoneNumber.getName());
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, mPhoneNumber, size);
    }
}

package com.metfone.selfcare.holder.message;

import android.content.res.Resources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.metfone.selfcare.adapter.VoteAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.PollItem;
import com.metfone.selfcare.database.model.PollObject;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.httprequest.PollRequestHelper;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.view.LinkTextView;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thanhnt72 on 10/30/2018.
 */

public class PollHolder extends BaseMessageHolder {
    private String TAG = MessageNotificationHolder.class.getSimpleName();
    private LinkTextView mTvwContent;
    private ApplicationController mApplication;

    private View flPreviewPoll;

    private SmartTextClickListener mSmartTextListener;
    private ReengMessage mEntry;
    private Resources mRes;


    private TextView tvPollOwner, tvPollStatus, tvPollName, tvPollMore, tvPollExpire, tvPollDetail;
    private RecyclerView recPoll;
    private VoteAdapter voteAdapter;
    private String myNumber;

    public PollHolder(ApplicationController applicationController,
                      SmartTextClickListener smartTextListener) {
        this.mSmartTextListener = smartTextListener;
        setContext(applicationController);
        mApplication = applicationController;
        mRes = applicationController.getResources();
        myNumber = mApplication.getReengAccountBusiness().getJidNumber();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_poll, parent, false);
        initBaseHolder(rowView);
        mTvwContent = rowView.findViewById(R.id.message_text_content);


        recPoll = rowView.findViewById(R.id.rec_poll);
        tvPollOwner = rowView.findViewById(R.id.tv_poll_owner);
        tvPollStatus = rowView.findViewById(R.id.tv_poll_status);
        tvPollName = rowView.findViewById(R.id.tv_poll_name);
        tvPollMore = rowView.findViewById(R.id.tvMorePoll);
        tvPollExpire = rowView.findViewById(R.id.tv_expire);
        tvPollDetail = rowView.findViewById(R.id.tvPollDetail);

        flPreviewPoll = rowView.findViewById(R.id.pollView);
        /*float scalingFactor = 0.9f;
        flPreviewPoll.setScaleX(scalingFactor);
        flPreviewPoll.setScaleY(scalingFactor);*/


        rowView.setTag(this);
        setConvertView(rowView);


        voteAdapter = new VoteAdapter(mApplication);
        voteAdapter.setPreview(true);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(mRes.getDrawable(R.drawable.xml_divider_item_decoration));

        recPoll.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,
                false));
        recPoll.addItemDecoration(dividerItemDecoration);
        recPoll.setNestedScrollingEnabled(false);
        recPoll.setAdapter(voteAdapter);
    }

    @Override
    public void setElemnts(Object obj) {
        mEntry = (ReengMessage) obj;
//        setBaseElements(obj);
        initBaseValue(mEntry);
        setSameDate(mEntry);
        setContentMessageLayoutLongClick(mEntry);
        setNewMessage(mEntry);
        mTvwContent.setColorReadMore(R.color.bg_mocha);
        if (mEntry.getSongId() == 1)
            mTvwContent.setReadMore(R.string.poll_see_result);
        else
            mTvwContent.setReadMore(R.string.title_vote);
        drawPollPreview();
        setListener();
        setContentVote();
        // processSentInviteNotify(mEntry);
    }

    private void setContentVote() {
        String content;
        if (mEntry.getMessageType() == ReengMessageConstant.MessageType.poll_create) {
            if (mApplication.getReengAccountBusiness().getJidNumber().equals(mEntry.getSender())) {
                content = String.format(mApplication.getResources().getString(R.string.poll_detail_creator), mApplication.getResources().getString(R.string.you));
            } else {
                String name;
                PhoneNumber p = mApplication.getContactBusiness().getPhoneNumberFromNumber(mEntry.getSender());
                if (p != null) {
                    name = p.getName();
                } else {
                    name = mEntry.getSenderName();
                }
                if (TextUtils.isEmpty(name)) {
                    name = mEntry.getSender();
                }
                content = String.format(mApplication.getResources().getString(R.string.poll_detail_creator), name);
            }
        } else {
            content = mEntry.getContent();
        }
        mTvwContent.setLengthReadMore(content.length());
        mTvwContent.asyncSetText(content + "  ", true);
    }

    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            if (getMessageInteractionListener() != null)
                getMessageInteractionListener().longClickBgrCallback(mEntry);
            return false;
        }
    };

    private void setListener() {
        tvPollDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onPollDetail(mEntry, false);
            }
        });

        mTvwContent.setOnReadMoreListener(new LinkTextView.OnReadMoreListener() {
            @Override
            public void onReadMore() {
                getMessageInteractionListener().onPollDetail(mEntry, false);
            }
        });
    }

    private void drawPollPreview() {
        if (getThreadMessage() != null
                && getThreadMessage().getPollLastId().get(mEntry.getFileId()) == mEntry.getId()) {
            int threadId = -1;
            if (getThreadMessage() != null) {
                threadId = getThreadMessage().getId();
            }
            PollObject pollObject = PollRequestHelper.getInstance(mApplication)
                    .getPollObjectLocal(mEntry.getFileId(), threadId);
            if (pollObject != null) {
                flPreviewPoll.setVisibility(View.VISIBLE);
                voteAdapter.setPollObject(pollObject);
                voteAdapter.setThreadMessage(getThreadMessage());
                int totalVote = 0;
                if (pollObject.getListItems() != null) {
                    for (PollItem item : pollObject.getListItems()) {
                        totalVote = totalVote + item.getTotalVoted();
                    }
                }
                voteAdapter.setTotalVote(totalVote);

                ArrayList<PollItem> pollItems = pollObject.getListItems();
                Collections.sort(pollItems, new Comparator<PollItem>() {
                    @Override
                    public int compare(PollItem fruit2, PollItem fruit1) {
                        if (fruit2.getMemberVoted().size() > fruit1.getMemberVoted().size())
                            return -1;
                        else if (fruit2.getMemberVoted().size() < fruit1.getMemberVoted().size())
                            return 1;
                        else return 0;
                    }
                });
                if (pollItems.size() > 3) {
                    tvPollMore.setVisibility(View.VISIBLE);
                    tvPollMore.setText(String.format(mRes.getString(R.string.poll_view_more_option), pollItems.size() - 3));
                    pollItems = new ArrayList<>(pollItems.subList(0, 3));
                } else
                    tvPollMore.setVisibility(View.GONE);

                ArrayList<Object> feeds = new ArrayList<>();
                feeds.addAll(pollItems);
                voteAdapter.bindData(feeds);
                voteAdapter.notifyDataSetChanged();

                if (Utilities.equals(pollObject.getCreator(), myNumber)) {
                    tvPollOwner.setText(String.format(mRes.getString(R.string.poll_detail_creator), mRes.getString(R.string.you)));
                } else {
                    ArrayList<String> creatorNumbers = new ArrayList<>();
                    creatorNumbers.add(pollObject.getCreator());
                    tvPollOwner.setText(String.format(mRes.getString(R.string.poll_detail_creator), mApplication.getContactBusiness().getListNameOfListNumber(creatorNumbers)));
                }
                if (pollObject.getExpireAt() <= 0) {
                    tvPollExpire.setVisibility(View.GONE);
                } else {
                    tvPollExpire.setVisibility(View.VISIBLE);
                    tvPollExpire.setText(Html.fromHtml(provideStr(mRes.getString(R.string.poll_detail_notify_expire) + ": ", TimeHelper.formatSeparatorTimeOfMessage(pollObject.getExpireAt(), mRes))));
                }

                Map<String, String> member = new HashMap<>();
                ArrayList<Object> items = voteAdapter.getItems();
                for (int i = 0; i < items.size(); i++) {
                    Object o = items.get(i);
                    if (o instanceof PollItem) {
                        PollItem pollItem = (PollItem) o;
                        for (int i1 = 0; i1 < pollItem.getMemberVoted().size(); i1++) {
                            member.put(pollItem.getMemberVoted().get(i1), "");
                        }
                    }
                }
                if (member.size() == 0)
                    tvPollStatus.setText(TimeHelper.formatSeparatorTimeOfMessage(pollObject.getCreatedAt(), mRes));
                else
                    tvPollStatus.setText(String.format(mRes.getString(R.string.poll_detail_status), TimeHelper.formatSeparatorTimeOfMessage(pollObject.getCreatedAt(), mRes), String.valueOf(member.size())));
                tvPollName.setText(pollObject.getTitle());
                if (pollObject.getClose() == 1) {
                    mTvwContent.setReadMore(R.string.poll_see_result);/*poll_see_result*/
                    tvPollDetail.setText(mRes.getString(R.string.poll_closed_view_result));
                } else
                    tvPollDetail.setText(mRes.getString(R.string.poll_view_detail));

            } else
                flPreviewPoll.setVisibility(View.GONE);

        } else
            flPreviewPoll.setVisibility(View.GONE);
    }

    private String provideStr(String key, String value) {
        return String.format("<font color=\"#6D6D6D\">%s </font> <font color=\"#f26522\">%s</font>", key, value);
    }

}

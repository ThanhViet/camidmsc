package com.metfone.selfcare.holder.message;

import android.os.Build;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Pair;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.NonContact;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.ReplyMessage;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.GlideHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.holder.AbsContentHolder;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundLinearLayout;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.jivesoftware.smack.packet.ReengMessagePacket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by thaodv on 7/8/2014.
 */
public abstract class BaseMessageHolder extends AbsContentHolder {
    private static final String TAG = BaseMessageHolder.class.getSimpleName();
    private View mLayoutMessageStatus;
    private View mLayoutImgAvatar, mLayoutImgAvatarSend,mLayoutAvatarRecieve;
    private CircleImageView mImgAvatar, mImgAvatarSend, mImgStatusRightText;
    private TextView mTvwSeparatorDate, mTvwTime, mTvwAvatar, mTvwSenderName, mTvwAvatarSend, mVoicemailDetailLength;
    private View mVwBorderMessageLayout, mVwSeparatorLayout, mViewReply, mViewUnreadBookMark;
    //    private View mVwSenderNameUnderline;
    private ImageView mImgStatus,mIbnRetry;
    private boolean isImage;
    private MessageInteractionListener mMessageInteractionListener;
    private ImageButton  mIbnReport, mIbnReply;
    private int mThreadType;
    private View rowView;
    protected boolean isQuickReply = false;
    private PhoneNumber phoneNumber;
    private ApplicationController mApplication;
    private ReengMessageConstant.MessageType messageType;
    public TextView mTvwStatus;
    private boolean isShowInfo = false;
    private ThreadMessage threadMessage;
    private String senderName;
    private boolean isNoReply = true;
    private int paddingReaction;
    public TagMocha.OnClickTag onClickTag;

    protected void initBaseHolder(View rowView) {
        this.rowView = rowView;
        mLayoutMessageStatus = rowView.findViewById(R.id.layout_message_status);
        mLayoutImgAvatar = rowView.findViewById(R.id.message_row_receiver_avatar_layout);
        mImgAvatar = (CircleImageView) rowView.findViewById(R.id.message_row_receiver_avatar);
        mTvwAvatar = (TextView) rowView.findViewById(R.id.message_row_receiver_avatar_text);

        mLayoutImgAvatarSend = rowView.findViewById(R.id.layout_avatar_send);
        mImgAvatarSend = (CircleImageView) rowView.findViewById(R.id.message_row_send_avatar);
        mTvwAvatarSend = (TextView) rowView.findViewById(R.id.message_row_send_avatar_text);

        mTvwSeparatorDate = (TextView) rowView.findViewById(R.id.message_separator_date);
        mVwBorderMessageLayout = rowView
                .findViewById(R.id.message_received_border_bgr);
        mVwSeparatorLayout = rowView
                .findViewById(R.id.message_separator_layout);
        mViewUnreadBookMark = rowView.findViewById(R.id.rlUnreadBookMark);
        /*contentMessageLayout = rowView
                .findViewById(R.id.message_detail_content_layout);*/
        mTvwTime = (TextView) rowView.findViewById(R.id.message_time);
        mImgStatus = (ImageView) rowView.findViewById(R.id.message_status);
        mImgStatusRightText = rowView.findViewById(R.id.img_message_status);
        mLayoutAvatarRecieve = rowView.findViewById(R.id.layout_info_received);
        mIbnRetry = rowView.findViewById(R.id.message_retry);
        mIbnReply = (ImageButton) rowView.findViewById(R.id.message_reply);
        mIbnReport = (ImageButton) rowView.findViewById(R.id.message_report);
        mTvwSenderName = (TextView) rowView.findViewById(R.id.textview_sender_name);
        //        mVwSenderNameUnderline = rowView.findViewById(R.id.textview_sender_name_underline);
        mTvwStatus = (TextView) rowView.findViewById(R.id.message_sms_notice);
        mViewReply = rowView.findViewById(R.id.message_reply_layout);
        mVoicemailDetailLength = rowView.findViewById(R.id.voicemail_detail_length);
    }

    protected void setBaseElements(Object obj) {
        ReengMessage message = (ReengMessage) obj;
//        Log.d(TAG, "setBaseElements: " + message.getPacketId());
        //chatMode = message.getChatMode();
        initBaseValue(message);
        setReplyView(message);
        setContentMessageLayoutClick(message);
        setContentMessageLayoutLongClick(message);
        checkIsImage(message);
        setSameDate(message);
        setSenderNameOfGroupOrRoomChat(message);
        setSameAvatar(message);
        setBackgroundDrawable(message);
        setStatusOfMessage(message);
        setRetryButtonListener(message);
        setReportButtonListener(message);
        setReplyButtonListener(message);
        //setLoadingProgressBarVisible(message);
        if (message.getMessageType() == ReengMessageConstant.MessageType.notification ||
                message.getMessageType() == ReengMessageConstant.MessageType.notification_fake_mo) {
            if (isFirstNewMessage()) {
                Log.i(TAG, "notification new msg");
            } else {
                Log.i(TAG, "notification not new msg");
            }
        }
        setNewMessage(message);
        mTvwSeparatorDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication,
                Constants.FONT_SIZE.LEVEL_2));
        mTvwTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants
                .FONT_SIZE.LEVEL_0));
        if (mTvwSenderName != null)
            mTvwSenderName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication,
                    Constants.FONT_SIZE.LEVEL_2_5));
        if (mTvwStatus != null)
            mTvwStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants
                    .FONT_SIZE.LEVEL_1));
        setReactionVisible(message);
        setReactionClickListener(message);

    }

    public void initBaseValue(ReengMessage message) {
        messageType = message.getMessageType();
        mApplication = (ApplicationController) getContext();
        ContactBusiness contactBusiness = mApplication.getContactBusiness();
        phoneNumber = contactBusiness.getPhoneNumberFromNumber(message.getSender());
        threadMessage = mApplication.getMessageBusiness().getThreadById(message.getThreadId());
    }

    public void setReactionVisible(ReengMessage reengMessage) {
        if (rowView == null || reengMessage == null) return;
        View reactionParent = rowView.findViewById(R.id.layout_reaction_parent);
        if (reactionParent != null) {
            if (mThreadType != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && mThreadType != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                reengMessage.setReactionType(false);
                reactionParent.setVisibility(View.GONE);
                return;
            }
            reengMessage.setReactionType(true);
            ArrayList<Pair<Integer, Integer>> reactions = getReactionNumber(reengMessage);
            if (reactions != null && !reactions.isEmpty()) {
                Collections.sort(reactions, new Comparator<Pair<Integer, Integer>>() {
                    @Override
                    public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
                        if (o1 == null || o2 == null) return 0;
                        if (o1.second == o2.second)
                            return (o1.first - o2.first);
                        return (o2.second - o1.second);
                    }
                });
            }
            boolean hasReaction = reactions != null && !reactions.isEmpty();
            boolean visible = isLastMessageOfOthers() || hasReaction;
            reactionParent.setVisibility(visible ? View.VISIBLE : View.GONE);
            if (!visible) return;
            if (mLayoutAvatarRecieve != null) {
                LinearLayout.LayoutParams p = (LinearLayout.LayoutParams) mLayoutAvatarRecieve.getLayoutParams();
                p.setMargins(p.leftMargin, Utilities.convertDpToPixel(15), p.rightMargin, p.bottomMargin);
                mLayoutAvatarRecieve.setLayoutParams(p);
            }
            LinearLayout layoutReactionList = (LinearLayout) reactionParent.findViewById(R.id.layout_list_reaction);
            ImageView imageReaction = reactionParent.findViewById(R.id.img_message_reaction);
            View layoutImgReaction = reactionParent.findViewById(R.id.layout_img_reaction);
            if (layoutImgReaction != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    layoutImgReaction.setElevation(mApplication.getResources().getDimension(R.dimen.margin_more_content_1));
                }
            }

            if (hasReaction) {
                if (imageReaction != null) {
                    if (reengMessage.getStateMyReaction() != -1) {
//                        imageReaction.setPadding(paddingReaction, paddingReaction, paddingReaction, paddingReaction);
                        Glide.with(mApplication).load(getImageId(reengMessage.getStateMyReaction())).into(imageReaction);
                    } else {
                        Glide.with(mApplication).load(R.drawable.ic_reaction).into(imageReaction);
//                        imageReaction.setPadding(0, 0, 0, 0);
                    }
                }


                if (layoutReactionList != null) {
                    layoutReactionList.setVisibility(View.VISIBLE);
                    ImageView imageView1 = reactionParent.findViewById(R.id.img_reaction_1);
                    ImageView imageView2 = reactionParent.findViewById(R.id.img_reaction_2);
                    ImageView imageView3 = reactionParent.findViewById(R.id.img_reaction_3);
                    TextView tvReactionNumber = reactionParent.findViewById(R.id.tv_reaction_number);

                    if (imageView1 == null || imageView2 == null || imageView3 == null || tvReactionNumber == null) {
                        layoutReactionList.setVisibility(View.GONE);
                        return;
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        layoutReactionList.setElevation(mApplication.getResources().getDimension(R.dimen.margin_more_content_1));
                    }
                    int reactionSize = reactions.size();
                    if (reactionSize == 1) {
                        imageView2.setVisibility(View.GONE);
                        imageView3.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView1.setImageResource(getImageId(reactions.get(0).first));
                    } else if (reactionSize == 2) {
                        imageView3.setVisibility(View.GONE);
                        imageView1.setVisibility(View.VISIBLE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView2.setImageResource(getImageId(reactions.get(0).first));
                        imageView1.setImageResource(getImageId(reactions.get(1).first));
                    } else if (reactionSize >= 3) {
                        imageView1.setVisibility(View.VISIBLE);
                        imageView2.setVisibility(View.VISIBLE);
                        imageView3.setVisibility(View.VISIBLE);
                        imageView3.setImageResource(getImageId(reactions.get(0).first));
                        imageView2.setImageResource(getImageId(reactions.get(1).first));
                        imageView1.setImageResource(getImageId(reactions.get(2).first));
                    }
                    tvReactionNumber.setVisibility(View.VISIBLE);
                    tvReactionNumber.setText(" " + getTotalReaction(reactions));
                }
            } else {
                if (imageReaction != null) {
                    Glide.with(mApplication).load(R.drawable.ic_reaction).into(imageReaction);
                }
                if (layoutReactionList != null) {
                    layoutReactionList.setVisibility(View.GONE);
                }
            }
        }
    }

    private int getImageId(int index) {
        switch (index) {
            case 0:
                return R.drawable.ic_reaction_like;
            case 1:
                return R.drawable.ic_reaction_love;
            case 2:
                return R.drawable.ic_reaction_smile;
            case 3:
                return R.drawable.ic_reaction_surprise;
            case 4:
                return R.drawable.ic_reaction_sad;
            case 5:
                return R.drawable.ic_reaction_huh;
        }
        return R.drawable.ic_reaction_like;
    }

    private ArrayList<Pair<Integer, Integer>> getReactionNumber(ReengMessage reengMessage) {
        if (reengMessage == null) return null;
        ArrayList<Integer> reactions = reengMessage.getListReaction();
        if (reactions == null || reactions.isEmpty()) return null;
        ArrayList<Pair<Integer, Integer>> reactionMessages = new ArrayList<>();
        for (int i = 0; i < reactions.size(); i++) {
            if (reactions.get(i) > 0) {
                reactionMessages.add(new Pair<Integer, Integer>(i, reactions.get(i)));
            }
        }
        return reactionMessages;
    }

    private int getTotalReaction(ArrayList<Pair<Integer, Integer>> reactions) {
        if (reactions == null || reactions.isEmpty()) return 0;
        int number = 0;
        for (int i = 0; i < reactions.size(); i++) {
            number += reactions.get(i).second;
        }
        return number;
    }

    public void setReactionClickListener(final ReengMessage reengMessage) {
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT && mThreadType != ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            return;
        }
        if (rowView == null) return;
        final View reactionParent = rowView.findViewById(R.id.layout_reaction_parent);
        if (reactionParent == null) return;

        final View imgReaction = reactionParent.findViewById(R.id.img_message_reaction);
        final View layoutReactionList = reactionParent.findViewById(R.id.layout_list_reaction);
        layoutReactionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessageInteractionListener != null) {
                    mMessageInteractionListener.onOpenListReaction(reengMessage, layoutReactionList);
                }
            }
        });

        if (imgReaction != null) {

            imgReaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMessageInteractionListener != null) {
                        if (reengMessage.getStateMyReaction() == -1)
                            mMessageInteractionListener.onClickReaction(reengMessage, imgReaction);
                        else
                            mMessageInteractionListener.onOpenListReaction(reengMessage, imgReaction);
                    }
                }
            });

            imgReaction.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mMessageInteractionListener != null) {
                        mMessageInteractionListener.onOpenViewReaction(reactionParent, reengMessage);
                    }
                    return true;
                }
            });
        }
    }

    protected void setNewMessage(ReengMessage message) {
        if (isFirstNewMessage()) {
            mVwSeparatorLayout.setVisibility(View.VISIBLE);
            String formatTime = TimeHelper.formatSeparatorTimeOfMessage(message.getTime(), getContext().getResources());
            mTvwSeparatorDate.setText(formatTime);
            mViewUnreadBookMark.setVisibility(View.VISIBLE);
        } else {
            mViewUnreadBookMark.setVisibility(View.GONE);
        }
    }

    private void setSenderNameOfGroupOrRoomChat(ReengMessage message) {
        // neu la loai tin nhan den, trong group, khac Sender voi previous message thi show ten
        // mTvwSenderName != null; chi check voi cac ReceiverHolder.
        if (threadMessage == null) {
            return;
        }
        if (message.getDirection() == ReengMessageConstant.Direction.received && mTvwSenderName != null) {
            if (isDifferSenderWithPrevious() || isFirstNewMessage()) {
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    mTvwSenderName.setVisibility(View.VISIBLE);
                    senderName = mApplication.getMessageBusiness().getFriendNameOfRoom(message.getSender(),
                            message.getSenderName(), threadMessage.getThreadName());
                    mTvwSenderName.setText(senderName);
//                    mTvwSenderName.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                    /*if (!TextUtils.isEmpty(message.getSender())) {
                        mTvwSenderName.setTextColor(res.getColor(R.color.bg_mocha));
                    } else {
                        mTvwSenderName.setTextColor(res.getColor(R.color.start_text_color));
                    }*/
                } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    mTvwSenderName.setVisibility(View.VISIBLE);
                    if (phoneNumber != null) {   //neu co trong danh ba thi lay ten danh ba
                        senderName = phoneNumber.getName();
                    } else {                      //hien thi so dien thoai neu la group chat
                        NonContact nonContact = mApplication.getContactBusiness().getExistNonContact(message
                                .getSender());
                        if (nonContact != null && !TextUtils.isEmpty(nonContact.getNickName())) {
                            senderName = message.getSender() + " (" + nonContact.getNickName() + ")";
                        } else {
                            if (!TextUtils.isEmpty(message.getSenderName())) {
                                senderName = message.getSender() + " (" + message.getSenderName() + ")";
                                Log.i(TAG, "senderName: " + senderName);
                            } else
                                senderName = message.getSender();
                        }
                        //senderName = message.getSender();
                    }
                    mTvwSenderName.setText(senderName);
//                    mTvwSenderName.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_received));
                } else {
                    mTvwSenderName.setVisibility(View.GONE);
                }
            } else {
                mTvwSenderName.setVisibility(View.GONE);
            }
        }
    }

    private void setReplyButtonListener(final ReengMessage message) {
        if (mIbnReply == null) return;
        if (message.getDirection() == ReengMessageConstant.Direction.received &&
                (messageType == ReengMessageConstant.MessageType.text || messageType == ReengMessageConstant.MessageType.image) &&
                mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT
                && message.getStatus() != ReengMessageConstant.STATUS_FAIL
                && message.getStatus() != ReengMessageConstant.STATUS_LOADING) {
            mIbnReply.setVisibility(View.VISIBLE);
        } else {
            mIbnReply.setVisibility(View.GONE);
        }
        mIbnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageInteractionListener.replyClickCallBack(message);
            }
        });
    }

    private void setRetryButtonListener(final ReengMessage message) {
        //neu message type la notification hoac invite shareMusic thi khong xu ly
        if (messageType == ReengMessageConstant.MessageType.notification
                || messageType == ReengMessageConstant.MessageType.notification_fake_mo
                || messageType == ReengMessageConstant.MessageType.inviteShareMusic
                || messageType == ReengMessageConstant.MessageType.suggestShareMusic
                || messageType == ReengMessageConstant.MessageType.image_link
                || messageType == ReengMessageConstant.MessageType.poll_create
                || messageType == ReengMessageConstant.MessageType.poll_action
                || messageType == ReengMessageConstant.MessageType.restore) {
            if (mIbnRetry != null) {
                mIbnRetry.setVisibility(View.GONE);
            }
            return;
        }
        if (mIbnRetry == null) {
            return;
        }
        ReengMessageConstant.Direction direction = message.getDirection();
        //video nhan duoc khong xu ly
        if (messageType == ReengMessageConstant.MessageType.shareVideo
                && direction == ReengMessageConstant.Direction.received) {
            mIbnRetry.setVisibility(View.GONE);
            return;
        }
        if (message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
            mIbnRetry.setVisibility(View.VISIBLE);
            if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                mTvwTime.setVisibility(View.GONE);
            } else {
                mTvwTime.setVisibility(View.VISIBLE);
                mTvwTime.setText(message.getHour());
            }
        } else {
            mIbnRetry.setVisibility(View.GONE);
        }
        mIbnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
                    mMessageInteractionListener.retryClickCallBack(message);
                }
            }
        });
        mTvwTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
                    mMessageInteractionListener.retryClickCallBack(message);
                } else {
                    mMessageInteractionListener.onInfoMessageCallBack();
                }
            }
        });
        if (mLayoutMessageStatus != null) {
            mLayoutMessageStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (message.getStatus() == ReengMessageConstant.STATUS_FAIL) {
                        mMessageInteractionListener.retryClickCallBack(message);
                    } else {
                        mMessageInteractionListener.onInfoMessageCallBack();
                    }
                }
            });
        }
        if (mImgStatus != null) {
            mImgStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (message.getStatus() != ReengMessageConstant.STATUS_FAIL) {
                        mMessageInteractionListener.onInfoMessageCallBack();
                    }
                }
            });
        }
    }

    private void setReportButtonListener(final ReengMessage message) {
        //neu message type la notification hoac invite shareMusic thi khong xu ly
        if (mIbnReport == null) {
            return;
        }
        ReengMessageConstant.Direction direction = message.getDirection();
        // tin gui hoac ngoai room thi an
        if (direction == ReengMessageConstant.Direction.send ||
                mThreadType != ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mIbnReport.setVisibility(View.GONE);
        } else if (TextUtils.isEmpty(message.getSender())) {// tin admin thi an
            mIbnReport.setVisibility(View.GONE);
        } else {
            mIbnReport.setVisibility(View.VISIBLE);
        }
        mIbnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageInteractionListener.reportClickCallBack(message);
            }
        });
    }

    private void setContentMessageLayoutClick(final ReengMessage reengMessage) {
        mVwBorderMessageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ReengMessageConstant.MessageType messageType = reengMessage.getMessageType();
                if (mMessageInteractionListener != null) {
                    if (messageType == ReengMessageConstant.MessageType.text) {
                        mMessageInteractionListener.textContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.voicemail) {
                        mMessageInteractionListener.voicemailContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.image) {
                        mMessageInteractionListener.imageContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.file) {
                        mMessageInteractionListener.fileContentClickCallBack(reengMessage, false);
                    } else if (messageType == ReengMessageConstant.MessageType.shareContact) {
                        mMessageInteractionListener.shareContactClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.inviteShareMusic) {
                        mMessageInteractionListener.textContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.notification ||
                            messageType == ReengMessageConstant.MessageType.notification_fake_mo) {
                        mMessageInteractionListener.textContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.voiceSticker) {
                        mMessageInteractionListener.voiceStickerClickCallBack(reengMessage, getConvertView());
                    } else if (messageType == ReengMessageConstant.MessageType.shareVideo) {
                        mMessageInteractionListener.videoContentClickCallBack(reengMessage, getConvertView());
                    } else if (messageType == ReengMessageConstant.MessageType.shareLocation) {
                        mMessageInteractionListener.shareLocationClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.gift) {
                        mMessageInteractionListener.gifContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.image_link) {
                        mMessageInteractionListener.imageContentClickCallBack(reengMessage);
                    } else if (messageType == ReengMessageConstant.MessageType.watch_video) {
                        mMessageInteractionListener.onWatchVideoClick(reengMessage, true);
                    }
                }
            }
        });
    }

    protected void setContentMessageLayoutLongClick(final ReengMessage message) {
        mVwBorderMessageLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mMessageInteractionListener != null)
                    mMessageInteractionListener.longClickBgrCallback(message);
                return true;
            }
        });

    }

    protected void setFriendAvatar(final String number, ReengMessage message) {
        int size = (int) mContext.getResources().getDimension(R.dimen.avatar_small_size);
        if (threadMessage == null) {
            return;
        }
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT) {
            mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, size,
                    threadMessage.getServerId(), null, false);
            mTvwAvatar.setVisibility(View.GONE);
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            if (TextUtils.isEmpty(number)) {        //tin nhan tu admin rooms
                mTvwAvatar.setVisibility(View.GONE);
                String avatarUrl;
                if (!TextUtils.isEmpty(message.getSenderAvatar())) {
                    avatarUrl = message.getSenderAvatar();
                } else {
                    avatarUrl = mApplication.getOfficerBusiness().getOfficerAvatarByServerId(threadMessage
                            .getServerId());
                }
                mApplication.getAvatarBusiness().setOfficialThreadAvatar(mImgAvatar, avatarUrl, size);
            } else {
                //tin nhan tu thanh vien khac
                int sizeSmall = (int) mContext.getResources().getDimension(R.dimen.avatar_thumbnail_size);
                mApplication.getAvatarBusiness().setMemberRoomChatAvatar(mImgAvatar,
                        mTvwAvatar, message.getSender(), senderName, phoneNumber, sizeSmall, message.getSenderAvatar());
            }
        } else if (mThreadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, size);
            } else {
                mApplication.getAvatarBusiness().setUnknownNumberAvatarGroup(mImgAvatar, mTvwAvatar, number, message.getSenderAvatar(), size);
            }
        } else {
            if (phoneNumber != null) {
                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, size);
            } else {
                if (threadMessage.isStranger()) {
                    mApplication.getAvatarBusiness().setStrangerAvatar(mImgAvatar,
                            mTvwAvatar, threadMessage.getStrangerPhoneNumber(),
                            message.getSender(), message.getSender(), null, size);
                } else {
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, number, size);
                }
            }
        }
        // click
        if (mThreadType != ThreadMessageConstant.TYPE_THREAD_OFFICER_CHAT && !TextUtils.isEmpty(number)) {
            mImgAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mMessageInteractionListener != null)
                        mMessageInteractionListener.onFriendAvatarClick(number, senderName);
                }
            });
        } else {
            mImgAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "khong lam gi");
                }
            });
        }
    }

    protected void setSameDate(ReengMessage message) {
        if (isSameDateWithPrevious()) {
            mVwSeparatorLayout.setVisibility(View.GONE);
        } else {
            mVwSeparatorLayout.setVisibility(View.VISIBLE);
            String formatTime = TimeHelper.formatSeparatorTimeOfMessage(message.getTime(), getContext().getResources());
            mTvwSeparatorDate.setText(formatTime);
        }
    }

    private void setSameAvatar(ReengMessage message) {
        //Log.i(TAG, "msgType: " + message.getMessageType().name() + " content: " + message.getContent());
        if (messageType == ReengMessageConstant.MessageType.lixi && message.getDirection() == ReengMessageConstant
                .Direction.send) {
            cancelDisplayAvatar();
            return;
        } else if (messageType == ReengMessageConstant.MessageType.suggestShareMusic) {
            return;
        } else if (messageType == ReengMessageConstant.MessageType.pin_message) {
            return;
        } else if (messageType == ReengMessageConstant.MessageType.notification ||
                messageType == ReengMessageConstant.MessageType.notification_fake_mo ||
                //messageType == ReengMessageConstant.MessageType.image_link ||
                messageType == ReengMessageConstant.MessageType.poll_action ||
                messageType == ReengMessageConstant.MessageType.enable_e2e) {
            cancelDisplayAvatar();
            return;
        }
        if (messageType == ReengMessageConstant.MessageType.inviteShareMusic && message.getSize() == 0) {
            cancelDisplayAvatar();
            return;
        }
        //int padding = (int) getContext().getResources().getDimension(R.dimen.reeng_message_no_avatar_padding);
//        if (isDifferSenderWithPrevious() || !isSameDateWithPrevious()) {
//            if (message.getDirection() == ReengMessageConstant.Direction.received && mImgAvatar != null) {
//                if (isQuickReply) {
//                    cancelDisplayAvatar();
//                    mTvwAvatar.setVisibility(View.GONE);
//                    mImgAvatar.setVisibility(View.INVISIBLE);
//                    mLayoutImgAvatar.setVisibility(View.INVISIBLE);
//                } else {
//                    mImgAvatar.setVisibility(View.VISIBLE);
//                    mLayoutImgAvatar.setVisibility(View.VISIBLE);
//                    setFriendAvatar(message.getSender(), message);
//                }
//            }
//        } else {
//            if (message.getDirection() == ReengMessageConstant.Direction.received) {
//                if (isFirstNewMessage()) {
//                    mImgAvatar.setVisibility(View.VISIBLE);
//                    mLayoutImgAvatar.setVisibility(View.VISIBLE);
//                    setFriendAvatar(message.getSender(), message);
//                } else {
//                    cancelDisplayAvatar();
//                    mTvwAvatar.setVisibility(View.GONE);
//                    mImgAvatar.setVisibility(View.INVISIBLE);
//                    mLayoutImgAvatar.setVisibility(View.INVISIBLE);
//                }
//            } else {
//                mTvwAvatarSend.setVisibility(View.GONE);
//                mImgAvatarSend.setVisibility(View.GONE);
//                mLayoutImgAvatarSend.setVisibility(View.GONE);
//            }
//        }
        /* New logic display avatar for receive message */
        if(message.getDirection() == ReengMessageConstant.Direction.received){
            if(isDifferSenderWithAfter() && mImgAvatar != null){
                mImgAvatar.setVisibility(View.VISIBLE);
                mLayoutImgAvatar.setVisibility(View.VISIBLE);
                setFriendAvatar(message.getSender(), message);
            }else{
                mTvwAvatar.setVisibility(View.GONE);
                mImgAvatar.setVisibility(View.INVISIBLE);
                mLayoutImgAvatar.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void setBackgroundDrawable(ReengMessage message) {
        //TODO set background cac loai tin nhan
        if (messageType == ReengMessageConstant.MessageType.notification
                || messageType == ReengMessageConstant.MessageType.notification_fake_mo
                || messageType == ReengMessageConstant.MessageType.voiceSticker
                || messageType == ReengMessageConstant.MessageType.suggestShareMusic
                || messageType == ReengMessageConstant.MessageType.gift
                || messageType == ReengMessageConstant.MessageType.image_link
                || messageType == ReengMessageConstant.MessageType.poll_action
                //them loai image vi giao dien moi ko co background
                || messageType == ReengMessageConstant.MessageType.image) {
            return;
        }
        if (messageType == ReengMessageConstant.MessageType.inviteShareMusic
                && message.getSize() == 0) {
            return;
        }
        if (isDifferSenderWithPrevious() || !isSameDateWithPrevious()) {
            if (message.getDirection() == ReengMessageConstant.Direction.received) {
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                        TextUtils.isEmpty(message.getSender())) {// addmin room chat
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_sms_out),
                                ContextCompat.getColor(mContext, R.color.bg_sms_out));
                    }
                } else if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_smsout),
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_smsout_press));
                    }
                } else if (message.isLargeEmo() && isNoReply) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                } else {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received),
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received_selected));
                    }
                }
            } else {
                if (message.getChatMode() == ReengMessageConstant.MODE_GSM && message.getMessageType() != ReengMessageConstant.MessageType.call) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_sms_out),
                                ContextCompat.getColor(mContext, R.color.bg_sms_out));
                    }
                } else if (message.isLargeEmo() && isNoReply) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                }
                else if (
                        message.getContent().equals(mApplication.getResources().getString(R.string.call_state_cancelled_call)) ||
                        message.getContent().equals("You cancelled") ||
                        message.getMessageType() == ReengMessageConstant.MessageType.call ||
                        message.getMessageType() == ReengMessageConstant.MessageType.shareContact ||
                        message.getMessageType() == ReengMessageConstant.MessageType.restore
                ) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received),
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received));
                    }
                }  else if(message.getMessageType() == ReengMessageConstant.MessageType.voicemail && message.getDirection() != ReengMessageConstant.Direction.received) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                } else {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        if(message.getMessageType().toString().equals("shareLocation")){
                            ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                    ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color),
                                    ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color));
                        }else
                            if(message.getMessageType() == ReengMessageConstant.MessageType.shareVideo && message.getDirection() != ReengMessageConstant.Direction.received){
                                    ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                        ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color),
                                        ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color));
                        }else
                       {
                            ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                    ContextCompat.getColor(mContext, R.color.setting_chat_text_color_red),
                                    ContextCompat.getColor(mContext, R.color.setting_chat_text_color_red));
                        }
                    }
                }
            }
        } else {
            if (message.getDirection() == ReengMessageConstant.Direction.received) {
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                        TextUtils.isEmpty(message.getSender())) {// addmin room chat
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_sms_out),
                                ContextCompat.getColor(mContext, R.color.bg_sms_out));
                    }
                } else if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_sms_out),
                                ContextCompat.getColor(mContext, R.color.bg_sms_out));
                    }
                } else if (message.isLargeEmo() && isNoReply) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                } else if(message.getMessageType() == ReengMessageConstant.MessageType.voicemail && message.getDirection() != ReengMessageConstant.Direction.received) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                } else {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received),
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received_selected));
                    }
                }
            } else {
                if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_sms_out),
                                ContextCompat.getColor(mContext, R.color.bg_sms_out));
                    }
                } else if (message.isLargeEmo() && isNoReply) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                }   else if (
                        message.getContent().equals(mApplication.getResources().getString(R.string.call_state_cancelled_call)) ||
                        message.getContent().equals("You cancelled") ||
                        message.getMessageType() == ReengMessageConstant.MessageType.call ||
                        message.getMessageType() == ReengMessageConstant.MessageType.shareContact ||
                        message.getMessageType() == ReengMessageConstant.MessageType.restore
                )  {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received),
                                ContextCompat.getColor(mContext, R.color.bg_color_bubble_received));
                    }
                } else if(message.getMessageType() == ReengMessageConstant.MessageType.voicemail && message.getDirection() != ReengMessageConstant.Direction.received) {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.transparent),
                                ContextCompat.getColor(mContext, R.color.transparent));
                    }
                } else {
                    if (mVwBorderMessageLayout instanceof RoundLinearLayout) {
                        if(message.getMessageType().toString().equals("shareLocation")){
                            ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                    ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color),
                                    ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color));
                        }else
                        if(message.getMessageType() == ReengMessageConstant.MessageType.shareVideo && message.getDirection() != ReengMessageConstant.Direction.received){
                            ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color),
                                ContextCompat.getColor(mContext, R.color.chat_bg_edt_search_10_color));
                        }else
                            {
                            ((RoundLinearLayout) mVwBorderMessageLayout).setBackgroundColorAndPress(
                                    ContextCompat.getColor(mContext, R.color.setting_chat_text_color_red),
                                    ContextCompat.getColor(mContext, R.color.setting_chat_text_color_red));
                        }
                    }
                }
            }
        }
    }

    private void checkIsImage(ReengMessage message) {
        isImage = message.getMessageType() == ReengMessageConstant.MessageType.image;
    }

    protected void showThumbnailOfImage(ReengMessage message, RoundedImageView content, int width, int height) {
//        ImageLoaderManager.getInstance(getContext()).displayThumbnailOfMessage(message, content);
        GlideHelper.getInstance(mApplication).displayImageThreadChat(content, message, width, height);
    }

    private void setStatusOfMessage(ReengMessage message) {
        //setstatus message
        if (messageType == ReengMessageConstant.MessageType.notification
                || messageType == ReengMessageConstant.MessageType.notification_fake_mo
//                || messageType == ReengMessageConstant.MessageType.inviteShareMusic
                || messageType == ReengMessageConstant.MessageType.suggestShareMusic
                || messageType == ReengMessageConstant.MessageType.image_link
                || messageType == ReengMessageConstant.MessageType.poll_action) {
            return;
        }
        if (messageType == ReengMessageConstant.MessageType.inviteShareMusic) {
            if (message.getSize() == 0) {
                return;
            }
        }

        //invi time trong room chat
        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
            mTvwTime.setVisibility(View.GONE);
        } else {
            mTvwTime.setText(message.getHour());
            if (isShowInfo) {
                 mTvwTime.setVisibility(View.VISIBLE);
//            startFadeInAnimation(mTvwTime);
            } else {
//            startFadeOutAnimation(mTvwTime);
                if (!message.isForceShow()) {
                    mTvwTime.setVisibility(View.GONE);
                }
            }
        }

        if (mImgStatus != null) {
            // neu la tin nhan thu hoi thi chi hien thoi gian
            if (message.getMessageType() == ReengMessageConstant.MessageType.restore) {
                mImgStatus.setVisibility(View.GONE);
                if (mTvwStatus != null) {
                    mTvwStatus.setVisibility(View.GONE);
                }
                return;
            }

            if (message.getDirection().equals(ReengMessageConstant.Direction.received)) {
                // receive
                if (message.getMessageType() == ReengMessageConstant.MessageType.file) {
                    mImgStatus.setVisibility(View.GONE);
                } else {
                    switch (message.getStatus()) {
                        case ReengMessageConstant.STATUS_FAIL:// = 2 ; //gui loi, nhan loi // khong hien thi o ban Mocha
                            mImgStatus.setVisibility(View.GONE);
                            //mImgStatus.setBackgroundResource(R.drawable.ic_error);
                            break;
                        case ReengMessageConstant.STATUS_RECEIVED: //= 4 ;// file, image
                            mImgStatus.setVisibility(View.GONE);
                            //
                            break;
                        case ReengMessageConstant.STATUS_NOT_LOAD://= 5 ; //chua download
                            if (isShowInfo) {
                                //mImgStatus.setVisibility(View.VISIBLE);
                                mImgStatus.setBackgroundResource(R.drawable.ic_delay);
                            } else {
                                mImgStatus.setVisibility(View.GONE);
                            }

                            break;
                        case ReengMessageConstant.STATUS_LOADING://= 6 ;//dang down , dang up, dang gui
                            if (isShowInfo) {
                                mImgStatus.setVisibility(View.VISIBLE); //ko hien thi vi da hien thi loading progress
                                mImgStatus.setBackgroundResource(R.drawable.ic_delay);
                            } else {
                                mImgStatus.setVisibility(View.GONE);
                            }
                            break;
                        default:
                            mImgStatus.setVisibility(View.GONE);
                            break;
                    }
                }
            } else {
                // send
                // Log.d(TAG, "setStatusOfMessage " + message);
                if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {
                    mTvwTime.setVisibility(View.GONE);
                } else {
                    mTvwTime.setVisibility(View.VISIBLE);
                }
                mImgStatus.setVisibility(View.VISIBLE);
                switch (message.getStatus()) {
                    case ReengMessageConstant.STATUS_NOT_SEND: // = 7 ; //chua gui
                        mImgStatus.setVisibility(View.GONE); //khong hien icon dong ho khi dang gui
//                        mImgStatus.setBackgroundResource(R.drawable.ic_delay);
                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            mTvwStatus.setText(R.string.status_sending);
                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;
                    case ReengMessageConstant.STATUS_SENT://= 1 ; //da gui
                        mImgStatus.setVisibility(View.VISIBLE);
                        mImgStatus.setBackgroundResource(R.drawable.ic_sent);
                        if (message.getChatMode() != ReengMessageConstant.MODE_GSM) {
                            mImgStatusRightText.setImageResource(R.drawable.chat_ic_not_recieve);
                        } else {
                            mImgStatusRightText.setVisibility(View.INVISIBLE);
                        }
                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            mTvwStatus.setText(R.string.status_sent);
                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;

                    case ReengMessageConstant.STATUS_FAIL:// = 2 ; //gui loi, nhan loi // khong hien thi o ban Mochaa
                        mImgStatus.setVisibility(View.GONE);
                        if (message.getChatMode() != ReengMessageConstant.MODE_GSM) {
                            mImgStatusRightText.setImageResource(R.drawable.chat_ic_message_error);
                        } else {
                            mImgStatusRightText.setVisibility(View.INVISIBLE);
                        }
//                        mImgStatus.setBackgroundResource(R.drawable.ic_error);
                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            mTvwStatus.setText(R.string.status_fail);
                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;
                    case ReengMessageConstant.STATUS_DELIVERED: //= 3 ; //da nhan
                        mImgStatus.setBackgroundResource(R.drawable.ic_delivery);
                        if (message.getChatMode() != ReengMessageConstant.MODE_GSM) {
                            mImgStatusRightText.setImageResource(R.drawable.chat_ic_received);
                        } else {
                            mImgStatusRightText.setVisibility(View.INVISIBLE);
                        }
                        mImgStatus.setVisibility(View.VISIBLE);
                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            mTvwStatus.setText(R.string.status_delivery);
                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;
                    case ReengMessageConstant.STATUS_RECEIVED: //= 3 ; //da nhan
                        mImgStatus.setBackgroundResource(R.drawable.ic_delivery);
                        mImgStatus.setVisibility(View.VISIBLE);
                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            mTvwStatus.setText(R.string.status_delivery);
                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;
                    case ReengMessageConstant.STATUS_SEEN: //=8
                        mImgStatus.setBackgroundResource(R.drawable.ic_seen);
                        if (isDifferSenderWithAfter() || isLastSeenBlock()) {
                            ContactBusiness contactBusiness = mApplication.getContactBusiness();
                            phoneNumber = contactBusiness.getPhoneNumberFromNumber(message.getReceiver());
                            mImgStatusRightText.setVisibility(View.VISIBLE);
                            if (phoneNumber != null) {
                                mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgStatusRightText, mTvwAvatar, phoneNumber, 30);
                            }else{
                                int size = (int) mContext.getResources().getDimension(R.dimen.dimen12dp);
                                if (threadMessage.isStranger()) {
                                    mApplication.getAvatarBusiness().setStrangerAvatar(mImgStatusRightText,
                                            null, threadMessage.getStrangerPhoneNumber(),
                                            message.getSender(), message.getSender(), null, size);
                                } else {
                                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgStatusRightText, null, message.getReceiver(), size);
                                }
                            }
                        } else {
                            mImgStatusRightText.setVisibility(View.INVISIBLE);
                        }
                        if (mThreadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {

                            mTvwTime.setVisibility(View.GONE);
                        } else {
                            if (isShowInfo) {
                                mTvwTime.setVisibility(View.VISIBLE);
                            } else {
                                if (!message.isForceShow()) {
                                    mImgStatus.setVisibility(View.GONE);
                                    mTvwTime.setVisibility(View.GONE);
                                }
                            }
                        }
                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            mTvwStatus.setText(R.string.status_seen);
                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;
                    case ReengMessageConstant.STATUS_LOADING://= 6 ;//dang down , dang up, dang gui
                        if (Config.Features.FLAG_FAKE_SEND_STATUS_TEXT) {
                            if (message.getMessageType() == ReengMessageConstant.MessageType.text) {
                                mImgStatus.setBackgroundResource(R.drawable.ic_sent);
                                mImgStatus.setVisibility(View.VISIBLE);
                            } else {
                                mImgStatus.setVisibility(View.GONE);
                            }
                        } else {
                            mImgStatus.setVisibility(View.GONE); //khong hien thi khi dang gui
                        }

                        if (mApplication.getMessageBusiness().isLastSendMsg(message)) {
                            mTvwStatus.setVisibility(View.VISIBLE);
                            if (Config.Features.FLAG_FAKE_SEND_STATUS_TEXT) {
                                if (message.getMessageType() == ReengMessageConstant.MessageType.text) {
                                    mTvwStatus.setText(R.string.status_sent);
                                } else {
                                    mTvwStatus.setText(R.string.status_sending);
                                }
                            } else
                                mTvwStatus.setText(R.string.status_sending);

                        } else {
                            mTvwStatus.setVisibility(View.GONE);
                        }
                        break;
                    default:
                        if (mImgStatus != null)
                            mImgStatus.setVisibility(View.GONE);
                        if (mTvwStatus != null)
                            mTvwStatus.setVisibility(View.GONE);
                        if (isShowInfo) {
                            mTvwTime.setVisibility(View.VISIBLE);
                        } else {
                            if (!message.isForceShow()) {
                                mTvwTime.setVisibility(View.GONE);
                            }
                        }
                        break;
                }
            }
        }
    }

    private void setReplyView(ReengMessage message) {
        if (mViewReply == null) {
            isNoReply = true;
            return;
        }
        if (messageType == ReengMessageConstant.MessageType.text) {
            ReplyMessage replyMessage = message.getReplyMessage();
            if (replyMessage != null && (
                    (replyMessage.isValidReplyText() && replyMessage.getSubType() == ReengMessagePacket.SubType.text)
                            || (replyMessage.isValidReplyImage() && replyMessage.getSubType() == ReengMessagePacket.SubType.image))) {
                isNoReply = false;
                mViewReply.setVisibility(View.VISIBLE);
                MessageHelper.drawReplyViewBubble(mApplication, mViewReply, replyMessage,
                        threadMessage, message.getDirection() == ReengMessageConstant.Direction.send, mMessageInteractionListener);
            } else {
                isNoReply = true;
                mViewReply.setVisibility(View.GONE);
            }
        } else {
            isNoReply = true;
            mViewReply.setVisibility(View.GONE);
        }
    }

    public View getRowView() {
        return rowView;
    }

    public boolean isImage() {
        return isImage;
    }

    public MessageInteractionListener getMessageInteractionListener() {
        return mMessageInteractionListener;
    }

    public void setMessageInteractionListener(MessageInteractionListener mMessageInteractionListener) {
        this.mMessageInteractionListener = mMessageInteractionListener;
    }

    public void setThreadType(int type) {
        mThreadType = type;
    }

    public int getThreadType() {
        return mThreadType;
    }

    public void setShowInfo(boolean isShow) {
        this.isShowInfo = isShow;
    }

    public ThreadMessage getThreadMessage() {
        return threadMessage;
    }

    public boolean isNoReply() {
        return isNoReply;
    }

    private void cancelDisplayAvatar() {
        if (mImgAvatar != null)
//            mApplication.getUniversalImageLoader().cancelDisplayTask(mImgAvatar);
            Glide.with(mApplication).clear(mImgAvatar);
    }

    protected View getViewBorderMessageLayout() {
        return mVwBorderMessageLayout;
    }

    public void setOnClickTag(TagMocha.OnClickTag onClickTag) {
        this.onClickTag = onClickTag;
    }
}
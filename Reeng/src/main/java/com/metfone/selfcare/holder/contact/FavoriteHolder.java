package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

/**
 * Created by toanvk2 on 9/6/2017.
 */

public class FavoriteHolder extends BaseViewHolder {
    private Context mContext;
    private PhoneNumber entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private RoundedImageView mImgAvatar;
    private TextView mTvContactName, mTvwAvatar;

    public FavoriteHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        mApplication = (ApplicationController) mContext.getApplicationContext();
        initView(convertView);
    }


    @Override
    public void setElement(Object obj) {
        entry = (PhoneNumber) obj;
        if (entry == null) {
            return;
        }
        setDetailView(entry);
    }

    private void initView(View convertView) {
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
    }

    //set name, avatar,reeng state
    private void setDetailView(PhoneNumber entry) {
        // text
        mTvContactName.setText(entry.getName());
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, entry, size);
    }
}
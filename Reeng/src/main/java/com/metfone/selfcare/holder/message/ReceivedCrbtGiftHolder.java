package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 27-Nov-14.
 */
public class ReceivedCrbtGiftHolder extends BaseMessageHolder {

    private static final String TAG = ReceivedCrbtGiftHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private ReengMessage message;
    private MediaModel mediaModel;
    private TextView mTvwTitle;
    private TextView mTvwSong;
    private TextView mTvwSinger;
    private TextView mTvwCountDown;
    private Button mBtnAcceptGroup;
    private ImageView mImgAvtSong;
    private RelativeLayout mLayoutReinvite;
    private RelativeLayout mLayoutUserAction, mLayoutUserGroupAction;
    private ImageButton mIbnGiftCrbt;

    public ReceivedCrbtGiftHolder(ApplicationController app, MessageInteractionListener listener) {
        setContext(app);
        mApplication = app;
        setMessageInteractionListener(listener);
    }

    public ReceivedCrbtGiftHolder(ApplicationController app, MessageInteractionListener listener, boolean isQuickReply) {
        setContext(app);
        mApplication = app;
        this.isQuickReply = isQuickReply;
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_invite_music_received, parent, false);
        initBaseHolder(rowView);
        mTvwCountDown = (TextView) rowView.findViewById(R.id.count_down_time);
        mTvwTitle = (TextView) rowView.findViewById(R.id.message_share_share_music_send_title);
        mTvwSong = (TextView) rowView.findViewById(R.id.message_share_music_send_song_name);
        mTvwSinger = (TextView) rowView.findViewById(R.id.message_share_music_send_song_singer);
        mImgAvtSong = (ImageView) rowView.findViewById(R.id.message_share_music_send_avatar);
        mLayoutReinvite = (RelativeLayout) rowView.findViewById(R.id.layout_reinvite_music);
        mLayoutUserAction = (RelativeLayout) rowView.findViewById(R.id.layout_user_action_music);
        mLayoutUserGroupAction = (RelativeLayout) rowView.findViewById(R.id.layout_user_action_group_music);
        mBtnAcceptGroup = (Button) rowView.findViewById(R.id.btn_accept_group_music);
        mIbnGiftCrbt = (ImageButton) rowView.findViewById(R.id.message_gift_crbt);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        checkAndUpdateStatus();
        drawHolder(message);
        setListener();
    }

    private void drawHolder(ReengMessage message) {
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSong.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwSinger.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnAcceptGroup.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        String title = message.getFileName();   //su dung fileName de co chu in dam
        mTvwTitle.setText(TextHelper.fromHtml(title));
        mediaModel = message.getSongModel(mApplication.getMusicBusiness());
        if (mediaModel != null) {
            // get data
            String songName = mediaModel.getName();
            String songSinger = mediaModel.getSinger();
            String avatarUrl = mediaModel.getImage();
            Log.i(TAG, "mediaModel: " + mediaModel);
            // draw
            if (!TextUtils.isEmpty(songName)) {
                mTvwSong.setText(songName);
            } else {
                mTvwSong.setText("");
            }
            if (!TextUtils.isEmpty(songSinger)) {
                mTvwSinger.setText(songSinger);
            } else {
                mTvwSinger.setText("");
            }
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, avatarUrl);
        } else {
            // neu chua co media thi get from sv, sau khi get duoc thi notify o thread detail
            mTvwSong.setText("");
            mTvwSinger.setText("");
            mApplication.getAvatarBusiness().setSongAvatar(mImgAvtSong, "");
            //mApplication.getMusicBusiness().getMediaModelFromServer(message.getSongId(), message.getThreadId());
        }
    }

    private void checkAndUpdateStatus() {
        mIbnGiftCrbt.setVisibility(View.GONE);
        mTvwCountDown.setVisibility(View.GONE);
        mLayoutReinvite.setVisibility(View.GONE);
        mLayoutUserAction.setVisibility(View.GONE);
        if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_NONE) {
            mLayoutUserGroupAction.setVisibility(View.VISIBLE);
        } else {
            mLayoutUserGroupAction.setVisibility(View.GONE);
        }
    }

    private void setListener() {
        mBtnAcceptGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onAcceptCrbtGift(message, mediaModel);
            }
        });
    }
}
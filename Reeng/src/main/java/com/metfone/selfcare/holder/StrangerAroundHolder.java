package com.metfone.selfcare.holder;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.StrangerMusic;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.StrangerMusicInteractionListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

/**
 * Created by toanvk2 on 17/03/16.
 */
public class StrangerAroundHolder extends BaseViewHolder {
    private static final String TAG = StrangerAroundHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private StrangerMusicInteractionListener strangerInteractionListener;
    private Context mContext;
    private Resources mRes;
    private StrangerMusic mEntry;
    private View parentView;
    private RoundedImageView mImgAvatar;
    private AppCompatTextView mTvwName, mTvwInfo;
    private AppCompatTextView mTvwOld, mTvwDistance;
    private AppCompatImageView mImgHear, ivGender;

    public StrangerAroundHolder(View convertView, Context context, StrangerMusicInteractionListener listener) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.strangerInteractionListener = listener;
        //
        parentView = convertView.findViewById(R.id.rootView);
        mImgAvatar = convertView.findViewById(R.id.ivAvatar);
        mTvwName = convertView.findViewById(R.id.tvName);
        mTvwInfo = convertView.findViewById(R.id.tvStatus);
//        mTvwAvatar =  convertView.findViewById(R.id.ivAvatar);
        mTvwOld = convertView.findViewById(R.id.tvAge);
        mTvwDistance = convertView.findViewById(R.id.tvDistance);
        ivGender = convertView.findViewById(R.id.ivGender);
//        mImgHear = convertView.findViewById(R.id.holder_ic_hear);
    }

    @Override
    public void setElement(Object obj) {
        mEntry = (StrangerMusic) obj;
        drawHolderDetail();
        setListener();
    }

    private void setListener() {
        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strangerInteractionListener.onClickPoster(mEntry);
            }
        });
    }

    private void drawHolderDetail() {
        AvatarBusiness avatarBusiness = mApplication.getAvatarBusiness();
        MediaModel songModel = mEntry.getSongModel();
        if (songModel != null) {
            mImgHear.setVisibility(View.VISIBLE);
            mTvwInfo.setText(songModel.getSongAndSinger());
            mTvwInfo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            mImgHear.setVisibility(View.GONE);
            mTvwInfo.setText(mEntry.getInfo());
            mTvwInfo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_quote_left, 0, R.drawable.ic_quote_right, 0);
        }
       /* mTvwInfo.setText(songAndSinger);
        if (TextUtils.isEmpty(songAndSinger)) {
            mTvwInfo.setText(mEntry.getInfo());
        } else {
            mTvwInfo.setText(songAndSinger);
        }*/
        mTvwName.setText(mEntry.getPosterName());
        if (TextUtils.isEmpty(mEntry.getAgeStr())) {
            mTvwOld.setText("");
        } else {
            mTvwOld.setText(mEntry.getAgeStr());
        }
        mTvwDistance.setText(mEntry.getDistanceStr());
        Drawable img;
        if (mEntry.getPosterGender() == Constants.CONTACT.GENDER_MALE) {
            ivGender.setImageResource(R.drawable.ic_boy_v5);
//            mImgGender.setImageResource(R.drawable.ic_around_gender_male);
//            mImgGender.setColorFilter(ContextCompat.getColor(mContext, R.color.gender_male));
        } else {
            ivGender.setImageResource(R.drawable.ic_girl_v5);
//            mImgGender.setImageResource(R.drawable.ic_around_gender_female);
//            mImgGender.setColorFilter(ContextCompat.getColor(mContext, R.color.gender_female));
        }

        int sizeAvatar = (int) mRes.getDimension(R.dimen.avatar_small_size);
        //String myJidNumber = mApplication.getReengAccountBusiness().getJidNumber();

        avatarBusiness.setStrangerAvatar(mImgAvatar, null, null, mEntry.getPosterJid(),
                mEntry.getPosterName(), mEntry.getPosterLastAvatar(), sizeAvatar);
    }
}
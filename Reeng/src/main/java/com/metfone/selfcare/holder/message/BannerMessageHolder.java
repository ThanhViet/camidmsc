package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 2/12/2018.
 */

public class BannerMessageHolder extends BaseMessageHolder {
    private static final String TAG = BannerMessageHolder.class.getSimpleName();

    private ApplicationController mApp;
    private Resources mRes;
    private ReengMessage message;
    private ImageView mImgBackground;
    private RoundTextView mBtnAction;


    public BannerMessageHolder(Context ctx, MessageInteractionListener listener) {
        setContext(ctx);
        setMessageInteractionListener(listener);
        mApp = (ApplicationController) ctx;
        mRes = mApp.getResources();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_message_banner, parent, false);
        mImgBackground = rowView.findViewById(R.id.banner_background);
        mBtnAction = rowView.findViewById(R.id.banner_action);
        //initBaseHolder(rowView);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        //setBaseElements(obj);
        drawDetail();
        setViewListener();
    }

    private void drawDetail() {
        if ("lixi".equals(message.getFilePath())) {
            mImgBackground.setImageResource(R.drawable.banner_lixi_2018);
            mBtnAction.setText(R.string.lixi_now);
        } else {
            mImgBackground.setImageResource(R.drawable.banner_lixi_2018);
            // banner tương tự loại mới thì code them o day
        }
    }

    private void setViewListener() {
        mBtnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getMessageInteractionListener() != null) {
                    if ("lixi".equals(message.getFilePath())) {
                        getMessageInteractionListener().onClickReplyLixi(message);
                    } else {
                        Log.d(TAG, "banner loại # code o day");
                    }
                }
            }
        });
        getConvertView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().longClickBgrCallback(message);
                return true;
            }
        });
    }
}
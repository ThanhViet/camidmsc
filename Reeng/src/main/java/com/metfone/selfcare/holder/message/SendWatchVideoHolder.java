package com.metfone.selfcare.holder.message;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TextHelper;

/**
 * Created by THANHNT72 on 1/11/2017.
 */
public class SendWatchVideoHolder extends BaseMessageHolder {

    private ReengMessage message;
    private ApplicationController mApp;
    private ImageView mImgThumbVideo;
    private TextView mTvwTitle, mTvwInfoVideo;

    public SendWatchVideoHolder(ApplicationController mApp) {
        setContext(mApp);
        this.mApp = mApp;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_watch_video_send, parent, false);
        mImgThumbVideo = (ImageView) rowView.findViewById(R.id.img_thumb_watch_video);
        mTvwTitle = (TextView) rowView.findViewById(R.id.tvw_title_watch_video);
        mTvwInfoVideo = (TextView) rowView.findViewById(R.id.tvw_info_video);
        initBaseHolder(rowView);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwInfoVideo.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        MediaModel mediaModel = new Gson().fromJson(message.getDirectLinkMedia(), MediaModel.class);
        if (mediaModel != null) {
            mApp.getAvatarBusiness().setThumbWatchVideo(mImgThumbVideo, mediaModel.getImage());
        }
        mTvwInfoVideo.setText(message.getFilePath());
        mTvwTitle.setText(TextHelper.fromHtml(message.getFileName()));
    }
}

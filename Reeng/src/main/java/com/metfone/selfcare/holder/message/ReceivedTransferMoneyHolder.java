package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import androidx.core.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.util.Log;

public class ReceivedTransferMoneyHolder extends BaseMessageHolder {
    private ReengMessage message;
    private String TAG = ReceivedTransferMoneyHolder.class.getSimpleName();
    private TextView mTvwContent;
    private TextView mTvwTimeTransfer;
    private SmartTextClickListener mSmartTextListener;

    public ReceivedTransferMoneyHolder(Context ctx, SmartTextClickListener smartTextListener, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        mSmartTextListener = smartTextListener;
        setContext(ctx);
    }

    public ReceivedTransferMoneyHolder(Context ctx, SmartTextClickListener smartTextListener) {
        mSmartTextListener = smartTextListener;
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_transfer_money_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mTvwTimeTransfer = (TextView) rowView.findViewById(R.id.time_stransfer_money_txt);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setContext(Context ctx) {
        mContext = ctx;
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        Resources res = mContext.getResources();
        Log.d(TAG, "" + message);
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_4));
        mTvwTimeTransfer.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1));
        final MessageInteractionListener mMessageInteractionListener = getMessageInteractionListener();
        if (message.getMessageType() == ReengMessageConstant.MessageType.restore) {
            mTvwContent.setText(res.getString(R.string.message_restored));
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_message_time));
        } else {
            //        mTvwContent.setText(message.getContent(), true, mSmartTextListener, longClickListener);
            String content = message.getContent() + " " + message.getImageUrl();
            mTvwContent.setText(content);
            try {
                String timeTransfer = TimeHelper.formatTimeTransferMoney(Long.parseLong(message.getFilePath()));
                mTvwTimeTransfer.setVisibility(View.VISIBLE);
                mTvwTimeTransfer.setText(String.format(res.getString(R.string.time_transfer_money), timeTransfer));
            } catch (NumberFormatException e) {
                Log.e(TAG, "Exception", e);
                mTvwTimeTransfer.setVisibility(View.GONE);
            }
        }
        if (isQuickReply) {
            mTvwContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMessageInteractionListener.textContentClickCallBack(message);
                }
            });
        }
        //-------------------------------same sender-------------------
//        if (isDifferSenderWithAfter()) {
//            int padding = (int) getContext().getResources().getDimension(
//                    R.dimen.reeng_message_no_avatar_padding);
//            contentMessageLayout.setPadding(padding, 0, 0, 0);
//        } else {
//            contentMessageLayout.setPadding(0, 0, 0, 0);
//        }
    }
}

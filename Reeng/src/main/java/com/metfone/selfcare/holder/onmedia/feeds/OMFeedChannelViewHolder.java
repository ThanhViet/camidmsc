/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/4/25
 *
 */

package com.metfone.selfcare.holder.onmedia.feeds;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tab_video.Channel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.ui.view.tab_video.SubscribeChannelLayout;
import com.metfone.selfcare.util.Utilities;

public class OMFeedChannelViewHolder extends OMFeedBaseViewHolder {
    private TextView tvTitle;
    private TextView tvNumFollow;
    private ImageView ivCover;
    private ImageView ivAvatar;
    private View viewContent;
    private SubscribeChannelLayout btnChannelSubscription;
    private BaseSlidingFragmentActivity activity;

    public OMFeedChannelViewHolder(View itemView, ApplicationController mApplication, BaseSlidingFragmentActivity activity) {
        super(itemView, mApplication);
        this.activity = activity;
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvNumFollow = itemView.findViewById(R.id.tv_number_follow);
        ivCover = itemView.findViewById(R.id.iv_cover);
        ivAvatar = itemView.findViewById(R.id.iv_avatar);
        viewContent = itemView.findViewById(R.id.feed_content);
        btnChannelSubscription = itemView.findViewById(R.id.button_channel_subscription);
    }

    @Override
    public void setElement(Object obj) {
        setBaseElements(obj);
        Channel channel = Channel.convertFromChannelOnMedia(feedModel.getFeedContent().getChannel());
        if (channel != null) {
            tvTitle.setText(channel.getName());
            updateUiSubscription(channel);
            ImageBusiness.setAvatarChannelOnFeed(ivAvatar, channel.getUrlImage());
            ImageBusiness.setCoverChannelOnFeed(ivCover, channel.getUrlImageCover());

            if (viewContent != null) viewContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getOnMediaHolderListener() != null)
                        getOnMediaHolderListener().openChannelInfo(feedModel);
                }
            });

            btnChannelSubscription.setChannel(channel);
            btnChannelSubscription.setSubscribeChannelListener(new SubscribeChannelLayout.SubscribeChannelListener() {
                @Override
                public void onOpenApp(Channel channel, boolean isInstall) {
                    Utilities.openApp(activity, channel.getPackageAndroid());
                }

                @Override
                public void onSub(Channel channel) {
                    if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin())
                        activity.showDialogLogin();
                    else {
                        updateUiSubscription(channel);
                        if (feedModel != null && feedModel.getFeedContent() != null && feedModel.getFeedContent().getChannel() != null) {
                            feedModel.getFeedContent().getChannel().setNumFollow(channel.getNumFollow());
                            feedModel.getFeedContent().getChannel().setFollow(channel.isFollow());
                        }
                        if (getOnMediaHolderListener() != null)
                            getOnMediaHolderListener().onSubscribeChannel(feedModel, channel);
                    }
                }
            });

        }
    }

    private void updateUiSubscription(Channel channel) {
        if (channel != null) {
            if (channel.getNumFollow() <= 0) {
                tvNumFollow.setVisibility(View.INVISIBLE);
                tvNumFollow.setText("");
            } else {
                tvNumFollow.setVisibility(View.VISIBLE);
                tvNumFollow.setText(String.format(activity.getString(R.string.people_subscription), Utilities.shortenLongNumber(channel.getNumFollow())));
            }
        }
    }
}

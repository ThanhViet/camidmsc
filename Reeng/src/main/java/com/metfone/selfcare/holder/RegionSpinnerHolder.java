package com.metfone.selfcare.holder;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.Region;
import com.metfone.selfcare.module.myviettel.model.TimeModel;
import com.metfone.selfcare.module.selfcare.model.SCAirTimeInfo;

/**
 * Created by toanvk2 on 26/05/2015.
 */
public class RegionSpinnerHolder extends AbsContentHolder {
    private Context mContext;
    private Resources mRes;
    private TextView mTvwContent;
    private RegionSpinnerHolder mHolder;

    public RegionSpinnerHolder(Context context) {
        this.mContext = context;
        this.mRes = context.getResources();
        mHolder = this;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        View convertView = layoutInflater.inflate(R.layout.holder_spinner_region, parent, false);
        mHolder.mTvwContent = convertView.findViewById(R.id.spinner_content);
        convertView.setTag(mHolder);
        setConvertView(convertView);
    }

    @Override
    public void setElemnts(Object obj) {
        if (obj instanceof Region)
            mHolder.mTvwContent.setText(((Region) obj).getRegionName());
        else if (obj instanceof SCAirTimeInfo)
            mHolder.mTvwContent.setText(((SCAirTimeInfo) obj).getName());
        else if (obj instanceof TimeModel)
            mHolder.mTvwContent.setText(((TimeModel) obj).getTitle());
    }


    public void setCustomView(Object obj) {
        if (obj instanceof Region) {
            String text = ((Region) obj).getRegionName();
            int lastIndex = text.lastIndexOf("(");
            text = text.substring(lastIndex + 1, text.length() - 1);
            mHolder.mTvwContent.setText(text);
        } else if (obj instanceof SCAirTimeInfo)
            mHolder.mTvwContent.setText(((SCAirTimeInfo) obj).getName());
    }

}

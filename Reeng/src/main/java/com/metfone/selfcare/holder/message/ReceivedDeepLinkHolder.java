package com.metfone.selfcare.holder.message;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.EmoTextViewListChat;
import com.metfone.selfcare.util.Log;


public class ReceivedDeepLinkHolder extends BaseMessageHolder {
    private ReengMessage message;
    private EmoTextViewListChat mTvwContent;
    private Button mBtnLeft, mBtnRight;
    private LinearLayout mLlButton;
    private SmartTextClickListener smartTextClickListener;
    private View.OnLongClickListener longClickListener;
    public boolean disableButton = false;
    private ApplicationController mApp;

    public ReceivedDeepLinkHolder(ApplicationController app, MessageInteractionListener listener,
                                  SmartTextClickListener smartTextClickListener) {
        setContext(app);
        mApp = app;
        setMessageInteractionListener(listener);
        this.smartTextClickListener = smartTextClickListener;
    }

    public ReceivedDeepLinkHolder(ApplicationController app, MessageInteractionListener listener,
                                  boolean isQuickReply) {
        setContext(app);
        this.isQuickReply = isQuickReply;
        setMessageInteractionListener(listener);
    }

    public void setDisableButton(boolean disable) {
        disableButton = disable;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        Log.d("ReceivedDeepLinkHolder", "initHolder");
        rowView = layoutInflater.inflate(
                R.layout.holder_deep_link_received, parent, false);
        initBaseHolder(rowView);
        mLlButton = (LinearLayout) rowView.findViewById(R.id.btn_deep_link_layout);
        mBtnLeft = (Button) rowView.findViewById(R.id.btn_deep_link_left);
        mBtnRight = (Button) rowView.findViewById(R.id.btn_deep_link_right);
        mTvwContent = (EmoTextViewListChat) rowView.findViewById(R.id.message_text_content);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        Log.d("ReceivedDeepLinkHolder", "setElemnts");
        message = (ReengMessage) obj;
        longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().longClickBgrCallback(message);
                return false;
            }
        };
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mBtnLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mBtnRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwContent.setEmoticon(message.getContent(), message.getId(), smartTextClickListener, longClickListener, message);
        if (message.getMessageType() == ReengMessageConstant.MessageType.deep_link) {
            drawDeepLinkDetail();
        } else if (message.getMessageType() == ReengMessageConstant.MessageType.update_app) {
            drawUpdateAppDetail();
        } else {
            drawLuckyWheelHelpDetail();
        }
        setButtonClickListener();
    }

    private void drawUpdateAppDetail() {
        mLlButton.setVisibility(!disableButton ? View.VISIBLE : View.GONE);
        mBtnRight.setVisibility(View.GONE);
        mBtnLeft.setText(mContext.getResources().getString(R.string.update));
        mTvwContent.setText(mContext.getResources().getString(R.string.message_content_update_app));
    }

    private void drawDeepLinkDetail() {
        if (TextUtils.isEmpty(message.getDeepLinkLeftLabel()) &&
                TextUtils.isEmpty(message.getDeepLinkRightLabel())) {
            message.initDeepLinkInfo(false);
        }
        if (TextUtils.isEmpty(message.getDeepLinkLeftLabel())) {
            mLlButton.setVisibility(View.GONE);
        } else if (TextUtils.isEmpty(message.getDeepLinkRightLabel())) {// 1 btn
            mLlButton.setVisibility(!disableButton ? View.VISIBLE : View.GONE);
            mBtnRight.setVisibility(View.GONE);
            mBtnLeft.setText(message.getDeepLinkLeftLabel());
        } else {// 2 btn
            mLlButton.setVisibility(!disableButton ? View.VISIBLE : View.GONE);
            mBtnRight.setVisibility(View.VISIBLE);
            mBtnLeft.setText(message.getDeepLinkLeftLabel());
            mBtnRight.setText(message.getDeepLinkRightLabel());
        }
    }

    private void setButtonClickListener() {
        mBtnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getMessageType() == ReengMessageConstant.MessageType.deep_link) {
                    getMessageInteractionListener().onDeepLinkClick(message, message.getDeepLinkLeftAction());
                    mApp.getLogApi().logBanner(message.getDeepLinkLeftAction(), LogApi.TypeService.BUTTON_DEEPLINK_CHAT, null);
                } else if (message.getMessageType() == ReengMessageConstant.MessageType.update_app) {
                    getMessageInteractionListener().onDeepLinkClick(message, "mocha://update");
                } else {
                    getMessageInteractionListener().onDeepLinkClick(message, getContext().getResources().getString(R.string.deep_link_lucky_wheel));
                }
            }
        });
        mBtnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getMessageType() == ReengMessageConstant.MessageType.deep_link) {
                    getMessageInteractionListener().onDeepLinkClick(message, message.getDeepLinkRightAction());
                    mApp.getLogApi().logBanner(message.getDeepLinkRightAction(), LogApi.TypeService.BUTTON_DEEPLINK_CHAT, null);
                } else {
                    getMessageInteractionListener().onLuckyWheelHelpClick(message);
                }
            }
        });
    }

    private void drawLuckyWheelHelpDetail() {
        mLlButton.setVisibility(!disableButton ? View.VISIBLE : View.GONE);
        mBtnLeft.setText(getContext().getResources().getString(R.string.lucky_wheel_play));
        if (message.getMusicState() == ReengMessageConstant.MUSIC_STATE_NONE) {
            mBtnRight.setVisibility(View.VISIBLE);
            mBtnRight.setText(getContext().getResources().getString(R.string.lucky_wheel_sos));
        } else {
            mBtnRight.setVisibility(View.GONE);
        }
    }
}
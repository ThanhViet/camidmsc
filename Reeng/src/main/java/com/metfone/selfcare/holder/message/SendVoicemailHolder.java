package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.network.AudioVoicemailManager;
import com.metfone.selfcare.ui.imageview.RevealView;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SendVoicemailHolder extends BaseMessageHolder {
    private ReengMessage message;
    private ImageView playButton;
    //    private ProgressBar playingSeekBar;
    private RevealView mRevealView;
    private TextView length;

    public SendVoicemailHolder(Context ctx) {
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_voicemail_send, parent, false);
        initBaseHolder(rowView);
        playButton = (ImageView) rowView.findViewById(R.id.voicemail_detail_button_play);
//        playingSeekBar = (ProgressBar) rowView.findViewById(R.id.voicemail_seekbar);
        length = (TextView) rowView.findViewById(R.id.voicemail_detail_length);
        mRevealView = (RevealView) rowView.findViewById(R.id.image_progress);

        Resources mRes = getContext().getResources();
        int width = mRes.getDimensionPixelOffset(R.dimen.dimen_190dp);
        int height = mRes.getDimensionPixelOffset(R.dimen.dimen_36dp);

        Bitmap b = BitmapFactory.decodeResource(mRes, R.drawable
                .ic_voicemail_progress_camid);
        Bitmap bm = Bitmap.createScaledBitmap(b, width, height, false);
        mRevealView.setImageBitmap(bm);
        mRevealView.setSecondBitmap(BitmapFactory.decodeResource(mRes, R.drawable
                .ic_voicemail_progress_gray_camid), width, height);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        length.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        AudioVoicemailManager.addToHashmap(message.getId(), getConvertView());
        if (message.isPlaying()) {
            playButton.setImageResource(R.drawable.pause_circle_voice);
        } else {
            playButton.setImageResource(R.drawable.play_circle_voice);
            mRevealView.setPercentage(0);
        }
        length.setText(StopWatch.convertSecondInMMSSForm(false, message.getDuration()));
    }
}

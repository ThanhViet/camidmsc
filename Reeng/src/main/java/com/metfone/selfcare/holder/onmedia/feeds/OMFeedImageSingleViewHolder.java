package com.metfone.selfcare.holder.onmedia.feeds;

import android.content.res.Resources;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 11/14/2016.
 */
public class OMFeedImageSingleViewHolder extends OMFeedBaseViewHolder {

    private static final String TAG = OMFeedImageSingleViewHolder.class.getSimpleName();

    private ApplicationController mApp;
    private FeedModelOnMedia feedModel;
    private AspectImageView mImgContent;
    private ProgressLoading mProgress;
    private Resources mRes;

    public OMFeedImageSingleViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        mApp = mApplication;
        mImgContent = (AspectImageView) itemView.findViewById(R.id.img_media_item);
        mProgress = (ProgressLoading) itemView.findViewById(R.id.progress_loading_img);
        mRes = mApp.getResources();
    }

    @Override
    public void setElement(Object obj) {
        long t1 = System.currentTimeMillis();
        feedModel = (FeedModelOnMedia) obj;
        setBaseElements(obj);

        /*DisplayMetrics displayMetrics = mApp.getResources().getDisplayMetrics();
        int widthScreen = displayMetrics.widthPixels;

        float ratioImg;
        if (feedModel.getFeedContent().getListImage().get(0).getRatioImage() > FeedContent.ImageContent
        .MAX_RATIO_IMAGE) {
            ratioImg = FeedContent.ImageContent.MAX_RATIO_IMAGE;
        } else if (feedModel.getFeedContent().getListImage().get(0).getRatioImage() < FeedContent.ImageContent
        .MIN_RATIO_IMAGE) {
            ratioImg = FeedContent.ImageContent.MIN_RATIO_IMAGE;
        } else {
            ratioImg = feedModel.getFeedContent().getListImage().get(0).getRatioImage();
        }

        int width = widthScreen - mRes.getDimensionPixelOffset(R.dimen.margin_more_content_15);
        mImgContent.getLayoutParams().width = width;
        mImgContent.getLayoutParams().height = (int) (width / ratioImg);*/

//        mImgContent.getLayoutParams().height = LayoutHelper.getHeightImageSingleViewHolder(mApp.getResources());
//        mImgContent.requestLayout();
        float ratioImg;
        ArrayList<FeedContent.ImageContent> list = feedModel.getFeedContent().getListImage();
        if (list == null || list.isEmpty()) {
            mImgContent.setAspect(1.78f);
//            ImageViewAwareTargetSize imgTargetSize = new ImageViewAwareTargetSize(mImgContent, 640,360);
            mApp.getImageProfileBusiness().displayImageSingleFeed(mImgContent,"", mProgress, 640, 360);
            return;
        }
        if (list.get(0).getRatioImage() > FeedContent.ImageContent
                .MAX_RATIO_IMAGE) {
            ratioImg = FeedContent.ImageContent.MAX_RATIO_IMAGE;
        } else if (list.get(0).getRatioImage() < FeedContent.ImageContent
                .MIN_RATIO_IMAGE) {
            ratioImg = FeedContent.ImageContent.MIN_RATIO_IMAGE;
        } else {
            ratioImg = list.get(0).getRatioImage();
        }
        mImgContent.setAspect(ratioImg);
        /*int width = mApp.getWidthPixels() - mRes.getDimensionPixelOffset(R.dimen.margin_more_content_20);
        int height = Math.round((float) width / ratioImg);
        Log.i(TAG, "width: " + width + " height: " + height + " ratio: " + ratioImg);
        ImageViewAwareTargetSize imgTargetSize = new ImageViewAwareTargetSize(mImgContent, width, height);*/
        FeedContent.ImageContent imageContent = list.get(0);
//        ImageViewAwareTargetSize imgTargetSize;
        if (imageContent.getWidthTarget() == 0 || imageContent.getHeightTarget() == 0) {
//            imgTargetSize = FileHelper.caculateImageViewAware(mApp, mImgContent, ratioImg, false, 0);

            mApp.getImageProfileBusiness().displayImageSingleFeed(FileHelper.caculateImageViewAware(mApp, mImgContent, ratioImg, false, 0),
                    list.get(0).getImgUrl(mApp), mProgress, 640, 360);
        } else {
//            imgTargetSize = new ImageViewAwareTargetSize(mImgContent, imageContent.getWidthTarget(), imageContent
//                    .getHeightTarget());
            mApp.getImageProfileBusiness().displayImageSingleFeed(mImgContent,
                    list.get(0).getImgUrl(mApp), mProgress, 640, 360);
        }
//        mApp.getImageProfileBusiness().displayImageSingleFeed(imgTargetSize,
//                list.get(0).getImgUrl(mApp), mProgress, 640, 360);
        mImgContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getOnMediaHolderListener() != null) {
                    getOnMediaHolderListener().onClickImageItem(feedModel, 0);
                }
            }
        });
        Log.i(TAG, "-------OnMedia---------setElement take: " + (System.currentTimeMillis() - t1) + " url: " +
                feedModel.getFeedContent().getUrl());
    }
}

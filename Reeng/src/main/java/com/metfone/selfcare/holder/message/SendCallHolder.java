package com.metfone.selfcare.holder.message;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.util.Log;

/**
 * Created by toanvk2 on 8/29/2016.
 */
public class SendCallHolder extends BaseMessageHolder {
    private static final String TAG = SendCallHolder.class.getSimpleName();

    private ReengMessage message;
    private TextView mTvwContent, mTvwCallBack, mTvwDuration;
    private ImageView mImgCallState,imgDot;
    private View mViewContent;
    private TextView mTvDate;
    //, mCallBackUnderline;
    boolean disableCallButton = false;

    public SendCallHolder(Context ctx) {
        setContext(ctx);
    }

    public SendCallHolder(Context ctx, boolean disableCallButton) {
        setContext(ctx);
        this.disableCallButton = disableCallButton;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_call_send, parent, false);
        initBaseHolder(rowView);
        mViewContent = rowView.findViewById(R.id.message_received_border_bgr);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mImgCallState = (ImageView) rowView.findViewById(R.id.message_call_state);
        mTvwCallBack = (TextView) rowView.findViewById(R.id.message_call_back);
        mTvwDuration = (TextView) rowView.findViewById(R.id.tvw_message_call_duration);
        imgDot = (ImageView)rowView.findViewById(R.id.imgDot);
        mTvDate = rowView.findViewById(R.id.tv_date);
        mTvwCallBack.setVisibility(disableCallButton ? View.GONE : View.VISIBLE);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        drawContentMessage();
        // setMessage Id
        mViewContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                getMessageInteractionListener().longClickBgrCallback(message);
                return true;
            }
        });
        mViewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onCall(message);
            }
        });
    }

    private void drawContentMessage() {
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwCallBack.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        String stateStr = mContext.getString(R.string.you_called) + " "+ (message.getSenderName() == null ? "" : message.getSenderName());
        mTvDate.setText(message.getDate());
        mTvwContent.setText(stateStr);
        mTvwDuration.setText(message.getContent());
        if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
            mImgCallState.setImageResource(R.drawable.ic_bubble_callout);
            stateStr = mContext.getResources().getString(R.string.call_state_mocha_video);
        } else if (message.getChatMode() == ReengMessageConstant.MODE_VIDEO_CALL) {
            imgDot.setVisibility(View.VISIBLE);
            mImgCallState.setImageResource(R.drawable.ic_bubble_callvideo);
            stateStr = mContext.getResources().getString(R.string.call_state_mocha_video);
        } else {
            imgDot.setVisibility(View.VISIBLE);
            mImgCallState.setImageResource(R.drawable.ic_bubble_calldata);
            stateStr = mContext.getResources().getString(R.string.you_called);
        }
        if (message.getSongId() == CallHistoryConstant.STATE_MISS) {
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_send));
//            mTvwContent.setText(mContext.getResources().getString(R.string.call_state_miss_call));
            mTvwContent.setText(mContext.getResources().getString(R.string.caller_call_state_miss));
            mTvwDuration.setVisibility(View.GONE);
        } else {
            mTvwContent.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_bubble_send));
            if (message.getSongId() == CallHistoryConstant.STATE_OUT_GOING) {
                mTvwContent.setText(stateStr);
                if (message.getDuration() == 0) {
                    mTvwDuration.setVisibility(View.GONE);
                } else {
                    mTvwDuration.setVisibility(View.VISIBLE);
                    mTvwDuration.setText(message.getContent());

                }
            } else if (message.getSongId() == CallHistoryConstant.STATE_BUSY) {
//                mTvwContent.setText(mContext.getResources().getString(R.string.call_state_busy));
                mTvwContent.setText(mContext.getResources().getString(R.string.caller_call_state_busy));
                mTvwDuration.setVisibility(View.GONE);
            } else if (message.getSongId() == CallHistoryConstant.STATE_CANCELLED) {
                mTvwContent.setText(mContext.getResources().getString(R.string.caller_call_state_cancelled));
//                mTvwContent.setText(mContext.getResources().getString(R.string.call_state_out_call));
                imgDot.setVisibility(View.GONE);
                mTvwDuration.setVisibility(View.GONE);
            } else if (message.getSongId() == CallHistoryConstant.STATE_REJECTED) {
                if (message.getChatMode() == ReengMessageConstant.MODE_GSM) {
                    mTvwContent.setText(mContext.getResources().getString(R.string.caller_call_state_fail));
                    imgDot.setVisibility(View.GONE);
                }else {
                    mTvwContent.setText(mContext.getResources().getString(R.string.caller_call_state_reject));
                    imgDot.setVisibility(View.GONE);
                }
                mTvwDuration.setVisibility(View.GONE);
            } else {
                Log.e(TAG, "sai type: " + message.getSongId());
                mTvwContent.setText(stateStr);
                mTvwDuration.setVisibility(View.GONE);
            }
        }
        if (message.getMessageType() == ReengMessageConstant.MessageType.talk_stranger) {
            mTvwCallBack.setVisibility(View.GONE);
        } else {
            mTvwCallBack.setVisibility(disableCallButton ? View.GONE : View.VISIBLE);
            mTvwCallBack.setText(mContext.getResources().getString(R.string.holder_call_back_new));
        }
    }
}
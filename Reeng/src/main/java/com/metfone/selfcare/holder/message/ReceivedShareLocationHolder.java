package com.metfone.selfcare.holder.message;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.imageview.SelectableRoundedImageView;


/**
 * Created by thaodv on 7/18/2014.
 */
public class ReceivedShareLocationHolder extends BaseMessageHolder {
    private ReengMessage message;
    private String TAG = ReceivedShareLocationHolder.class.getSimpleName();
    private TextView mTvwContent;
    private SelectableRoundedImageView mImgThumbnail;

    public ReceivedShareLocationHolder(ApplicationController applicationController, MessageInteractionListener listener, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        setContext(applicationController);
    }

    public ReceivedShareLocationHolder(ApplicationController applicationController, MessageInteractionListener listener) {
        setContext(applicationController);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_share_location_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (TextView) rowView.findViewById(R.id.message_text_content);
        mImgThumbnail = (SelectableRoundedImageView) rowView.findViewById(R.id.message_share_location_thumbnail);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwContent.setText(message.getContent());
        ImageLoaderManager.getInstance(mContext).displayThumbnailLocation(mImgThumbnail
        );
    }
}
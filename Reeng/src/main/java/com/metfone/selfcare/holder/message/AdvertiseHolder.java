package com.metfone.selfcare.holder.message;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.message.AdvertiseItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 8/3/2016.
 */
public class AdvertiseHolder extends BaseMessageHolder {

    private static final String TAG = AdvertiseHolder.class.getSimpleName();

    private ApplicationController mApplication;
    private ReengMessage message;
    private ArrayList<AdvertiseItem> listAdvertise;

    private TextView mTvwTitleFirst, mTvwTitleSecond, mTvwTitleThird, mTvwTitleFour, mTvwTitleFive;
    private TextView mTvwDesFirst;
    private ImageView mImgFirst, mImgSecond, mImgThird, mImgFour, mImgFive;
    private int widthMiniImage, heightMiniImage;
    //, widthBigImage, heightBigImage;
    private View mViewFirstAction, mViewSecondAction, mViewThirdAction, mViewFourAction, mViewFiveAction;
    private View mViewDividerFirst;
    private Resources mRes;

    public AdvertiseHolder(ApplicationController app, MessageInteractionListener listener) {
        mApplication = app;
        mRes = mApplication.getResources();
        setContext(app);
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_advertise, parent, false);
        findComponent(rowView);

        rowView.setTag(this);
        setConvertView(rowView);
    }

    private void findComponent(View rowView) {
        mTvwTitleFirst = (TextView) rowView.findViewById(R.id.tvw_first_action_title);
        mTvwTitleSecond = (TextView) rowView.findViewById(R.id.tvw_second_action_title);
        mTvwTitleThird = (TextView) rowView.findViewById(R.id.tvw_third_action_title);
        mTvwTitleFour = (TextView) rowView.findViewById(R.id.tvw_four_action_title);
        mTvwTitleFive = (TextView) rowView.findViewById(R.id.tvw_five_action_title);

        mViewDividerFirst = rowView.findViewById(R.id.view_divider_first_action);

        mTvwDesFirst = (TextView) rowView.findViewById(R.id.tvw_first_action_des);

        mImgFirst = (ImageView) rowView.findViewById(R.id.img_first_action);
        mImgSecond = (ImageView) rowView.findViewById(R.id.img_second_action);
        mImgThird = (ImageView) rowView.findViewById(R.id.img_third_action);
        mImgFour = (ImageView) rowView.findViewById(R.id.img_four_action);
        mImgFive = (ImageView) rowView.findViewById(R.id.img_five_action);

        mViewFirstAction = rowView.findViewById(R.id.layout_first_action);
        mViewSecondAction = rowView.findViewById(R.id.layout_second_action);
        mViewThirdAction = rowView.findViewById(R.id.layout_third_action);
        mViewFourAction = rowView.findViewById(R.id.layout_four_action);
        mViewFiveAction = rowView.findViewById(R.id.layout_five_action);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        drawHolder();
        setListener();
    }

    private void setListener() {
        mViewFirstAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().onDeepLinkClick(message,
                        message.getListAdvertise().get(0).getAction());
            }
        });
        mViewSecondAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getListAdvertise().size() > 1)
                    getMessageInteractionListener().onDeepLinkClick(message,
                        message.getListAdvertise().get(1).getAction());
            }
        });
        mViewThirdAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getListAdvertise().size() > 2)
                    getMessageInteractionListener().onDeepLinkClick(message,
                        message.getListAdvertise().get(2).getAction());
            }
        });
        mViewFourAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getListAdvertise().size() > 3)
                    getMessageInteractionListener().onDeepLinkClick(message,
                        message.getListAdvertise().get(3).getAction());
            }
        });
        mViewFiveAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.getListAdvertise().size() > 4)
                    getMessageInteractionListener().onDeepLinkClick(message,
                        message.getListAdvertise().get(4).getAction());
            }
        });

        mViewFirstAction.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().longClickBgrCallback(message);
                return true;
            }
        });
    }

    private void drawHolder() {
        listAdvertise = message.getListAdvertise();
        DisplayMetrics displayMetrics = mApplication.getResources().getDisplayMetrics();
        int widthScreen = displayMetrics.widthPixels;
        widthMiniImage = Math.round((float) widthScreen / 3.8f);
        heightMiniImage = 9 * widthMiniImage / 16;

        /*int padding = mRes.getDimensionPixelOffset(R.dimen.margin_more_content_10);
        widthBigImage = widthScreen - padding;
        if (widthBigImage > 800) {
            widthBigImage = 800;
        }
        heightBigImage = Math.round(9f * (((float) widthBigImage) / 16f));

        mImgFirst.getLayoutParams().width = widthBigImage;
        mImgFirst.getLayoutParams().height = heightBigImage;*/

        mImgSecond.getLayoutParams().width = widthMiniImage;
        mImgSecond.getLayoutParams().height = heightMiniImage;

        mImgThird.getLayoutParams().width = widthMiniImage;
        mImgThird.getLayoutParams().height = heightMiniImage;

        mImgFour.getLayoutParams().width = widthMiniImage;
        mImgFour.getLayoutParams().height = heightMiniImage;

        mImgFive.getLayoutParams().width = widthMiniImage;
        mImgFive.getLayoutParams().height = heightMiniImage;
        mTvwTitleFirst.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwDesFirst.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTitleSecond.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTitleThird.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTitleFour.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants.FONT_SIZE.LEVEL_2_5));
        mTvwTitleFive.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mApplication, Constants.FONT_SIZE.LEVEL_2_5));

        if (listAdvertise == null || listAdvertise.isEmpty()) {
            Log.e(TAG, "cltg");
            mViewFirstAction.setVisibility(View.GONE);
            mViewSecondAction.setVisibility(View.GONE);
            mViewThirdAction.setVisibility(View.GONE);
            mViewFourAction.setVisibility(View.GONE);
            mViewFiveAction.setVisibility(View.GONE);
            mViewDividerFirst.setVisibility(View.GONE);
            return;
        }
        if (listAdvertise.size() == 1) {
            setFirstView();
            mViewFirstAction.setVisibility(View.VISIBLE);
            mViewDividerFirst.setVisibility(View.GONE);
            mViewSecondAction.setVisibility(View.GONE);
            mViewThirdAction.setVisibility(View.GONE);
            mViewFourAction.setVisibility(View.GONE);
            mViewFiveAction.setVisibility(View.GONE);
        } else if (listAdvertise.size() == 2) {
            setFirstView();
            setSecondView();
            mViewDividerFirst.setVisibility(View.VISIBLE);
            mViewFirstAction.setVisibility(View.VISIBLE);
            mViewSecondAction.setVisibility(View.VISIBLE);
            mViewThirdAction.setVisibility(View.GONE);
            mViewFourAction.setVisibility(View.GONE);
            mViewFiveAction.setVisibility(View.GONE);
        } else if (listAdvertise.size() == 3) {
            setFirstView();
            setSecondView();
            setThirdView();
            mViewDividerFirst.setVisibility(View.VISIBLE);
            mViewFirstAction.setVisibility(View.VISIBLE);
            mViewSecondAction.setVisibility(View.VISIBLE);
            mViewThirdAction.setVisibility(View.VISIBLE);
            mViewFourAction.setVisibility(View.GONE);
            mViewFiveAction.setVisibility(View.GONE);
        } else if (listAdvertise.size() == 4) {
            setFirstView();
            setSecondView();
            setThirdView();
            setFourView();
            mViewDividerFirst.setVisibility(View.VISIBLE);
            mViewFirstAction.setVisibility(View.VISIBLE);
            mViewSecondAction.setVisibility(View.VISIBLE);
            mViewThirdAction.setVisibility(View.VISIBLE);
            mViewFourAction.setVisibility(View.VISIBLE);
            mViewFiveAction.setVisibility(View.GONE);
        } else if (listAdvertise.size() == 5) {
            setFirstView();
            setSecondView();
            setThirdView();
            setFourView();
            setFiveView();
            mViewDividerFirst.setVisibility(View.VISIBLE);
            mViewFirstAction.setVisibility(View.VISIBLE);
            mViewSecondAction.setVisibility(View.VISIBLE);
            mViewThirdAction.setVisibility(View.VISIBLE);
            mViewFourAction.setVisibility(View.VISIBLE);
            mViewFiveAction.setVisibility(View.VISIBLE);
        } else {
            Log.e(TAG, "cltg");
            mViewFirstAction.setVisibility(View.GONE);
            mViewSecondAction.setVisibility(View.GONE);
            mViewThirdAction.setVisibility(View.GONE);
            mViewFourAction.setVisibility(View.GONE);
            mViewFiveAction.setVisibility(View.GONE);
            mViewDividerFirst.setVisibility(View.GONE);
        }
    }

    private void setFirstView() {
        AdvertiseItem advertiseItem = listAdvertise.get(0);
        mTvwTitleFirst.setText(advertiseItem.getTitle());
        mTvwDesFirst.setText(advertiseItem.getDes());
        /*ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgFirst, widthBigImage,
                heightBigImage);*/
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgFirst, advertiseItem.getIconUrl());
    }

    private void setSecondView() {
        AdvertiseItem advertiseItem = listAdvertise.get(1);
        mTvwTitleSecond.setText(advertiseItem.getTitle());
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgSecond, widthMiniImage,
//                heightMiniImage);
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgSecond, advertiseItem.getIconUrl(), widthMiniImage, heightMiniImage);
    }

    private void setThirdView() {
        AdvertiseItem advertiseItem = listAdvertise.get(2);
        mTvwTitleThird.setText(advertiseItem.getTitle());
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgThird, widthMiniImage,
//                heightMiniImage);
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgThird, advertiseItem.getIconUrl(), widthMiniImage, heightMiniImage);
    }

    private void setFourView() {
        AdvertiseItem advertiseItem = listAdvertise.get(3);
        mTvwTitleFour.setText(advertiseItem.getTitle());
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgFour, widthMiniImage,
//                heightMiniImage);
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgFour, advertiseItem.getIconUrl(), widthMiniImage, heightMiniImage);
    }

    private void setFiveView() {
        AdvertiseItem advertiseItem = listAdvertise.get(4);
        mTvwTitleFive.setText(advertiseItem.getTitle());
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgFive, widthMiniImage,
//                heightMiniImage);
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgFive, advertiseItem.getIconUrl(), widthMiniImage, heightMiniImage);
    }
}

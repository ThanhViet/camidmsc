package com.metfone.selfcare.holder;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.EllipsisTextView;

/**
 * Created by thanhnt72 on 9/27/2016.
 */

public class ThreadBannerViewHolder extends BaseViewHolder {
    private View mViewAvatar, mViewLabel;
    private CircleImageView mImgThreadAvatar;
    private TextView mTvwTitle;
    private EllipsisTextView mTvwDesc;
    private AspectImageView mImgImageBanner;
    private ApplicationController mApplication;
    private BannerMocha mBanner;
    private FrameLayout layout_ads;

    public ThreadBannerViewHolder(View view, ApplicationController mApplication) {
        super(view);
        this.mApplication = mApplication;
        mViewAvatar = view.findViewById(R.id.layout_thread_avatar);
        mViewLabel = view.findViewById(R.id.thread_banner_label);
        mImgThreadAvatar = (CircleImageView) view.findViewById(R.id.thread_avatar);
        mTvwTitle = (TextView) view.findViewById(R.id.thread_name);
        mTvwDesc = (EllipsisTextView) view.findViewById(R.id.thread_last_content);
        mImgImageBanner = (AspectImageView) view.findViewById(R.id.thread_banner_image);
    }

    public void setElement(Object obj) {
        mBanner = (BannerMocha) obj;
        // hien thi thong tin contact trong thread
        if (mBanner.isImage()) {
            drawImageBannerDetail();
        } else {
            drawBannerLabelDetail();
        }
    }

    private void drawBannerLabelDetail() {
        mViewAvatar.setVisibility(View.VISIBLE);
        mViewLabel.setVisibility(View.VISIBLE);
        mImgImageBanner.setVisibility(View.GONE);
        //lay tin nhan cuoi cung cua Thread
        //Resources res = mContext.getResources();
        mTvwTitle.setText(mBanner.getTitle());
        String desc = mBanner.getDesc();
        mTvwDesc.setEmoticon(mApplication, desc, desc.hashCode(), desc);
        mApplication.getAvatarBusiness().setLogoOtherApp(mImgThreadAvatar, mBanner.getIcon());
    }

    private void drawImageBannerDetail() {
        mViewAvatar.setVisibility(View.GONE);
        mViewLabel.setVisibility(View.GONE);
        mImgImageBanner.setVisibility(View.VISIBLE);
//        ImageLoaderManager.getInstance(mApplication).displayImageBanner(mBanner.getImage(),
//                mImgImageBanner, R.color.bg_onmedia_content_item);

        ImageLoaderManager.getInstance(mApplication).displayImageBanner(mBanner.getImage(),
                mImgImageBanner, R.drawable.banner_inbox_default);
    }
}
package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.editor.sticker.utils.Constants;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Template;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.glide.GlideImageLoader;

/**
 * Created by toanvk2 on 4/17/2017.
 */
public class TemplateHolder extends BaseViewHolder {
    private Context mContext;
    private Template entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private ImageView mImgBackground;
    private TextView mTvwName;
    private ProgressLoading mProgressLoading;

    private int screenWidth;

    public TemplateHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mContext.getResources();
        mImgBackground = (ImageView) convertView.findViewById(R.id.background_item);
        mTvwName = (TextView) convertView.findViewById(R.id.background_item_name);
        mProgressLoading = (ProgressLoading) convertView.findViewById(R.id.progress_loading_img);
        // set layout param
        screenWidth = mApplication.getWidthPixels();
       /* ViewGroup.LayoutParams textParams = mTvwName.getLayoutParams();
        textParams.width = screenWidth / 3;*/

        ViewGroup.LayoutParams params = mImgBackground.getLayoutParams();
        params.width = screenWidth / 3;
        params.height = (int) (params.width * Constants.SCALE_HEIGHT);
    }

    @Override
    public void setElement(Object obj) {
        entry = (Template) obj;
        //mTvwName.setText(entry.getName());
        mTvwName.setVisibility(View.GONE);
        String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(entry.getPreview());
        ImageLoaderManager.getInstance(mContext).displayGuestBookPreview(mImgBackground, url, new GlideImageLoader.SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted() {
                mProgressLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(GlideException e) {
                mProgressLoading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete() {
                mProgressLoading.setVisibility(View.GONE);
            }
        });
    }
}
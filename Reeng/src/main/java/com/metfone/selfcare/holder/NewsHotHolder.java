package com.metfone.selfcare.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.NewsHotAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.NewsHotModel;
import com.metfone.selfcare.ui.glide.ImageLoader;

/**
 * Created by HaiKE on 8/11/17.
 */

public class NewsHotHolder  extends BaseViewHolder {
    private static final String TAG = NearYouHolder.class.getSimpleName();
    private NewsHotAdapter.NewsHotInterface mListener;
    private Context mContext;
    private NewsHotModel mEntry;
    private View parentView;
    private ImageView imvAvatar;
    private TextView tvTitle;
    private TextView tvCategory;

    public NewsHotHolder(View convertView, Context context, NewsHotAdapter.NewsHotInterface listener) {
        super(convertView);
        this.mContext = context;
        this.mListener = listener;
        //
        parentView = convertView.findViewById(R.id.holder_parent);
        imvAvatar = (ImageView) convertView.findViewById(R.id.imvImage);
        tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        tvCategory = (TextView) convertView.findViewById(R.id.tvCategory);

    }

    @Override
    public void setElement(Object obj) {
        mEntry = (NewsHotModel) obj;
        drawHolderDetail();
        setListener();
    }

    private void setListener() {
        if(parentView != null)
        {
            parentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClickItem(mEntry);
                }
            });
        }
    }

    private void drawHolderDetail() {
        tvTitle.setText(mEntry.getTitle());
        tvCategory.setText(mEntry.getCategory());
        ImageLoader.setImage(mContext, mEntry.getImage169(), imvAvatar);
    }
}
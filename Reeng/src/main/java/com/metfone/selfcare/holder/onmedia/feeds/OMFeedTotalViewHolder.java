package com.metfone.selfcare.holder.onmedia.feeds;

import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
public class OMFeedTotalViewHolder extends OMFeedBaseViewHolder {

    private static final String TAG = OMFeedTotalViewHolder.class.getSimpleName();
    private View mBtnShareFb, mBtnRotateWheel;
    private View mLayoutButtonNormal, mLayoutButtonDeeplink;
    private TextView mBtnLeft, mBtnRight;
    private FeedModelOnMedia feed;
    private AspectImageView mImgBackground;
    private TextView mTvwTitle, mTvwDes;
    private ApplicationController mApplication;
    private TextView mTvwNumbLike, mTvwNumberStatus, mTvwNumberShare;
    private ImageView mImgLike;
    private View viewLike, viewComment, viewShare;
    //    private TagTextView mTvwComment;
//    private LinearLayout mLayoutUserComment;
    private int maxLength;
    private ContactBusiness mContactBusiness;
    private ReengAccount account;
    private Resources mRes;
    private String myName, myPhoneNumber;

    private View viewUserState;
    private TextView mTvwUserState, mTvwUserStateAction;
//    private ImageView mImgMoreOption;

    private View itemView;
    private View mViewDivider;
    private int width, height;

    public OMFeedTotalViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);

        this.itemView = itemView;
        this.mApplication = mApplication;
        mRes = mApplication.getResources();
        mContactBusiness = mApplication.getContactBusiness();
        account = mApplication.getReengAccountBusiness().getCurrentAccount();
        myName = account.getName();
        myPhoneNumber = account.getJidNumber();
        maxLength = mRes.getInteger(R.integer.max_length_name_onmedia);
        width = Math.round(mApplication.getWidthPixels());
        if (width > 720) width = 720;
        height = Math.round(width / 6.15f);

        mBtnShareFb = itemView.findViewById(R.id.layout_share_fb);
        mBtnRotateWheel = itemView.findViewById(R.id.layout_go_to_lucky_wheel);
        mImgBackground = (AspectImageView) itemView.findViewById(R.id.img_background_feed);
        mTvwTitle = (TextView) itemView.findViewById(R.id.title_horoscope);
        mTvwDes = (TextView) itemView.findViewById(R.id.desc_horo);
        mTvwNumbLike = (TextView) itemView.findViewById(R.id.tvw_number_like);
        mImgLike = (ImageView) itemView.findViewById(R.id.img_like_feed);
        mTvwNumberStatus = (TextView) itemView.findViewById(R.id.tvw_number_comment);
        mTvwNumberShare = (TextView) itemView.findViewById(R.id.tvw_number_share);
        viewLike = itemView.findViewById(R.id.layout_like);
        viewShare = itemView.findViewById(R.id.layout_share);
        viewComment = itemView.findViewById(R.id.layout_comment);

//        mLayoutUserComment = (LinearLayout) itemView.findViewById(R.id.layout_user_comment);
//        mTvwComment = (TagTextView) itemView.findViewById(R.id.tvw_user_comment);

        mLayoutButtonNormal = itemView.findViewById(R.id.layout_button_normal);
        mLayoutButtonDeeplink = itemView.findViewById(R.id.layout_button_deeplink);
        mBtnLeft = (TextView) itemView.findViewById(R.id.btn_left_deeplink);
        mBtnRight = (TextView) itemView.findViewById(R.id.btn_right_deeplink);

        viewUserState = itemView.findViewById(R.id.layout_user_state);
        mTvwUserState = (TextView) itemView.findViewById(R.id.tvw_user_state);
        mTvwUserStateAction = (TextView) itemView.findViewById(R.id.tvw_user_state_action);
//        mImgMoreOption = (ImageView) itemView.findViewById(R.id.img_more_option);

        mViewDivider = itemView.findViewById(R.id.view_divider_om);
    }

    @Override
    public void setElement(Object obj) {
        feed = (FeedModelOnMedia) obj;
        mTvwTitle.setText(feed.getFeedContent().getItemName());
        mTvwDes.setText(feed.getFeedContent().getDescription());

        mViewDivider.setVisibility(View.VISIBLE);
//        ImageViewAwareTargetSize imageViewAwareTargetSize = new ImageViewAwareTargetSize(mImgBackground, width, height);
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgBackground, feed.getFeedContent().getBackgroundUrl(), width, height);
        if (feed.getFeedContent().getItemSubType().equals(FeedContent.ITEM_SUB_TYPE_HOROSCOPE)) {
            mLayoutButtonNormal.setVisibility(View.VISIBLE);
            mLayoutButtonDeeplink.setVisibility(View.GONE);
            mBtnShareFb.setVisibility(View.VISIBLE);
            mBtnRotateWheel.setVisibility(View.GONE);
        } else if (feed.getFeedContent().getItemSubType().equals(FeedContent.ITEM_SUB_TYPE_LUCKY_WHEEL)) {
            mLayoutButtonNormal.setVisibility(View.VISIBLE);
            mLayoutButtonDeeplink.setVisibility(View.GONE);
            mBtnShareFb.setVisibility(View.GONE);
            mBtnRotateWheel.setVisibility(View.VISIBLE);
        } else if (feed.getFeedContent().getItemSubType().equals(FeedContent.ITEM_SUB_TYPE_DEEPLINK)) {
            mLayoutButtonNormal.setVisibility(View.GONE);
            mLayoutButtonDeeplink.setVisibility(View.VISIBLE);
        } else {
            Log.i(TAG, "subtype \"" + feed.getFeedContent().getItemSubType() + "\" not define");
            mLayoutButtonNormal.setVisibility(View.GONE);
            mLayoutButtonDeeplink.setVisibility(View.GONE);
        }
        setLayoutLikeComment();
        setButtonDeeplink();
        setViewUserAction();

        mBtnShareFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnMediaHolderListener() != null) {
                    getOnMediaHolderListener().onClickButtonTotal(itemView, feed);
                }
            }
        });

        mBtnRotateWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnMediaHolderListener() != null) {
                    getOnMediaHolderListener().onClickButtonTotal(itemView, feed);
                }
            }
        });

    }

    private void setViewUserAction() {
        UserInfo userInfo = feed.getUserInfo();
        if (userInfo != null && userInfo.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL && !TextUtils.isEmpty(userInfo
                .getMsisdn())) {
            viewUserState.setVisibility(View.VISIBLE);
            if (isMyComment(userInfo.getMsisdn())) {
//                mImgMoreOption.setVisibility(View.VISIBLE);
                setTextActionType(myName, mTvwUserState, mTvwUserStateAction, false);
            } else {
                PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(userInfo.getMsisdn());
                if (mPhoneNumber != null) {
//                            Log.i(TAG, "avatar: " + userInfo.getAvatar());
                    setTextActionType(mPhoneNumber.getName(), mTvwUserState, mTvwUserStateAction, false);
                } else {
                    String nameFriend;
                    if (TextUtils.isEmpty(userInfo.getName())) {
                        nameFriend = Utilities.hidenPhoneNumber(userInfo.getMsisdn());
                    } else {
                        nameFriend = userInfo.getName();
                    }
                    setTextActionType(nameFriend, mTvwUserState, mTvwUserStateAction, false);
                }
//                mImgMoreOption.setVisibility(View.GONE);
            }
        } else {
            viewUserState.setVisibility(View.GONE);
//            mImgMoreOption.setVisibility(View.GONE);
        }
    }

    private void setTextActionType(String name, TextView mTvwName, TextView mTvwAction, boolean isTextActionFeed) {
        String action = "";
        if (isTextActionFeed) {
            FeedContent feedContent = feed.getFeedContent();
            if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM)) {
                action = mRes.getString(R.string.onmedia_update_album);
                name = TextHelper.textBoldWithHTMLWithMaxLength(name, maxLength);
            } else if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR)) {
                action = mRes.getString(R.string.onmedia_update_avatar);
                name = TextHelper.textBoldWithHTMLWithMaxLength(name, maxLength);
            } else if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_COVER)) {
                action = mRes.getString(R.string.onmedia_update_cover);
                name = TextHelper.textBoldWithHTMLWithMaxLength(name, maxLength);
            } else if (feedContent.getItemType().equals(FeedContent.ITEM_TYPE_PROFILE_STATUS)) {
                action = mRes.getString(R.string.onmedia_update_status);
                name = TextHelper.textBoldWithHTMLWithMaxLength(name, maxLength);
            }
        } else {
            switch (feed.getActionType()) {
                case LIKE:
                    name = TextHelper.textBoldWithHTMLWithMaxLength(name, maxLength);
                    action = mRes.getString(R.string.connections_cung_thich);
                    break;
                case COMMENT:
                    name = TextHelper.textBoldWithHTMLWithMaxLength(name, maxLength);
                    action = mRes.getString(R.string.connections_feed_comment);
                    break;
                case SHARE:
                    name = TextHelper.textBoldWithHTML(name);
                    if (isFeedFromProfile(feed)) {
                        action = mRes.getString(R.string.connections_cung_chia_se);
                    }
//                textActionType = textActionType + " " + mRes.getString(R.string.connections_cung_chia_se);
                    break;
                case POST:
                    name = TextHelper.textBoldWithHTML(name);
//                textActionType = textActionType + " " + mRes.getString(R.string.connections_cung_chia_se);
                    break;
                default:
                    break;
            }
        }
        mTvwName.setText(TextHelper.fromHtml(name));
        if (!TextUtils.isEmpty(action)) {
            mTvwAction.setVisibility(View.VISIBLE);
            mTvwAction.setText(action);
        } else {
            mTvwAction.setVisibility(View.GONE);
        }

    }

    private void setButtonDeeplink() {
        if (!feed.getFeedContent().getItemSubType().equals(FeedContent.ITEM_SUB_TYPE_DEEPLINK)) {
            return;
        }
        String leftLabel = feed.getFeedContent().getLeftLabel();
        String rightLabel = feed.getFeedContent().getRightLabel();
        final String leftDeeplink = feed.getFeedContent().getLeftDeeplink();
        final String rightDeeplink = feed.getFeedContent().getRightDeeplink();
        mLayoutButtonDeeplink.setVisibility(View.VISIBLE);

        if ((TextUtils.isEmpty(leftLabel) || TextUtils.isEmpty(leftDeeplink)) &&
                (TextUtils.isEmpty(rightLabel) || TextUtils.isEmpty(rightDeeplink))) {
            mLayoutButtonDeeplink.setVisibility(View.GONE);
            return;
        }

        if (!TextUtils.isEmpty(leftLabel) && !TextUtils.isEmpty(leftDeeplink)) {
            mBtnLeft.setVisibility(View.VISIBLE);
            mBtnLeft.setText(leftLabel);
            mBtnLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getOnMediaHolderListener().onDeepLinkClick(feed, leftDeeplink);
                }
            });
        } else {
            mBtnLeft.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(rightLabel) && !TextUtils.isEmpty(rightDeeplink)) {
            mLayoutButtonDeeplink.setVisibility(View.VISIBLE);
            mBtnRight.setVisibility(View.VISIBLE);
            mBtnRight.setText(rightLabel);
            mBtnRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getOnMediaHolderListener().onDeepLinkClick(feed, rightDeeplink);
                }
            });
        } else {
            mBtnRight.setVisibility(View.GONE);
        }

    }

    private void setLayoutLikeComment() {

        if (feed != null && feed.getFeedContent() != null) {
            String countLike = Utilities.shortenLongNumber(feed.getFeedContent().getCountLike());//    String.valueOf
            String countComment = Utilities.shortenLongNumber(feed.getFeedContent().getCountComment());
            String countShare = Utilities.shortenLongNumber(feed.getFeedContent().getCountShare());

            mTvwNumbLike.setText(countLike);
            mTvwNumberStatus.setText(countComment);
            mTvwNumberShare.setText(countShare);

            if (mImgLike != null) {
                if (feed.getIsLike() == 1) {
                    mImgLike.setImageResource(R.drawable.ic_v5_heart_active);
                } else {
                    mImgLike.setImageResource(R.drawable.ic_v5_heart_normal);
                }
                if (viewLike != null)
                    viewLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (getOnMediaHolderListener() != null) {
                                getOnMediaHolderListener().onClickLikeFeed(feed);
                            }
                        }
                    });
            }
            if (viewShare != null) {
                viewShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (getOnMediaHolderListener() != null) {
                            getOnMediaHolderListener().onClickShareFeed(feed);
                        }
                    }
                });
            }
            if (viewComment != null) {
                viewComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (getOnMediaHolderListener() != null) {
                            getOnMediaHolderListener().onClickCommentFeed(feed);
                        }
                    }
                });
            }
        }
    }

    private boolean isMyComment(String phoneNumber) {
        Log.i(TAG, "msisdn: " + phoneNumber);
        return myPhoneNumber.equals(phoneNumber);
    }
}
package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 4/19/2017.
 */
public class MainDividerHolder extends BaseViewHolder {
    private Context mContext;
    private String entry;
    private ApplicationController mApplication;
    private TextView mTvwDivider;
    private int screenWidth;

    public MainDividerHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        mTvwDivider = (TextView) convertView.findViewById(R.id.guest_book_divider);
        // set layout param
        screenWidth = mApplication.getWidthPixels();
        ViewGroup.LayoutParams params = convertView.getLayoutParams();
        params.width = screenWidth;
    }

    @Override
    public void setElement(Object obj) {
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams)
                getItemView().getLayoutParams();
        layoutParams.setFullSpan(true);
        entry = (String) obj;
         mTvwDivider.setText(entry);
    }
}

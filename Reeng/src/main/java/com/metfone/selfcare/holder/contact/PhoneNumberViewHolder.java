package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.Constants.PREFERENCE.CONFIG;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/3/14.
 */
public class PhoneNumberViewHolder extends BaseContactHolder implements View.OnClickListener {
    private static final String TAG = PhoneNumberViewHolder.class.getSimpleName();
    private ClickListener.IconListener mCallBack;
    private Context mContext;
    private PhoneNumber entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private RoundedImageView mImgAvatar;
    private ImageView mImgReengState, mImgSelect, mImgBlock,mImgUnSelect;
    private TextView mTvContactName;
    private EllipsisTextView mTvStatus;
    private View mViewOptionMarginRight, mConvertView;
    private TextView mTvwAvatar, mTvwInviteFriendText;
    private FrameLayout mFrAvatar;
    private RelativeLayout mRelContent;
    private LinearLayout mLlOption;
//    private boolean isShowMochaIcon = true;
    private ArrayList<String> mMemberThread = new ArrayList<>();
    private boolean smallIcon;

    public PhoneNumberViewHolder(View convertView, Context context, ClickListener.IconListener callBack) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        initView(convertView);
    }

    public PhoneNumberViewHolder(View convertView, Context context, ClickListener.IconListener callBack, boolean smallIcon) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mCallBack = callBack;
        mApplication = (ApplicationController) mContext.getApplicationContext();
        this.smallIcon = smallIcon;
        initView(convertView);
    }

    @Override
    public void setElement(Object obj) {
        entry = (PhoneNumber) obj;
        if (entry == null) {
            return;
        }
        // draw
        if (isOnlyShowAvatar()) {
            setAvatarView(entry);
        } else {
            setPhoneView(entry);
        }
    }

    private void initView(View convertView) {
        mConvertView = convertView;
        mImgSelect = convertView.findViewById(R.id.item_contact_view_checkbox);
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        mImgReengState = convertView.findViewById(R.id.item_contact_view_avatar_state);
        mImgBlock = convertView.findViewById(R.id.item_contact_view_block_img);
        mTvwInviteFriendText = convertView.findViewById(R.id.item_contact_view_invite_friend_text);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
        mTvStatus = convertView.findViewById(R.id.item_contact_view_status_text);
        mFrAvatar = convertView.findViewById(R.id.item_contact_view_avatar_frame);
        mRelContent = convertView.findViewById(R.id.item_contact_view_content_layout);
        mLlOption = convertView.findViewById(R.id.item_contact_view_option_layout);
        mViewOptionMarginRight = convertView.findViewById(R.id.item_contact_view_option_margin_right);


        if (smallIcon) {
            View viewParent = convertView.findViewById(R.id.rlContent);
            ViewGroup.LayoutParams lpParent = viewParent.getLayoutParams();
            lpParent.height = Utilities.dpToPixels(50, mContext.getResources());
            viewParent.setLayoutParams(lpParent);

            int sizeAvatar = Utilities.dpToPixels(36, mContext.getResources());
            ViewGroup.LayoutParams lpFrAvatar = mFrAvatar.getLayoutParams();
            lpFrAvatar.width = sizeAvatar;
            lpFrAvatar.height = sizeAvatar;
            mFrAvatar.setLayoutParams(lpFrAvatar);

            ViewGroup.LayoutParams lpAvatar = mImgAvatar.getLayoutParams();
            lpAvatar.width = sizeAvatar;
            lpAvatar.height = sizeAvatar;
            mImgAvatar.setLayoutParams(lpAvatar);


            int textAvatar = Utilities.dpToPixels(12, mContext.getResources());
            int textName = Utilities.dpToPixels(14, mContext.getResources());
            mTvwAvatar.setTextSize(TypedValue.COMPLEX_UNIT_PX, textAvatar);
            mTvContactName.setTextSize(TypedValue.COMPLEX_UNIT_PX, textName);
        }

    }

    @Override
    public void onClick(View view) {
        if (mCallBack != null && entry != null) {
            switch (view.getId()) {
                case R.id.item_contact_view_block_img:
                    mCallBack.onIconClickListener(view, entry, Constants.ACTION.BLOCK_CONTACT);
                    break;
                case R.id.item_contact_view_avatar_frame:
                    mCallBack.onIconClickListener(view, entry, Constants.MENU.VIEW_CONTATCT_DETAIL);
                    break;
                case R.id.item_contact_view_invite_friend_text:
                    mCallBack.onIconClickListener(view, entry, Constants.ACTION.HOLDER_INVITE_FRIEND);
                    mApplication.trackingEvent(R.string.ga_category_home, R.string.ga_action_interaction, R.string
                            .ga_label_invite_app);
                    break;
            }
        }
    }

    // show/hide block icon,check box
    private void setOptionView(PhoneNumber entry) {
        boolean enable = !entry.isDisable();
        switch (getType()) {
            case Constants.CONTACT.CONTACT_VIEW_FORWARD_MESSAGE:
            case Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE:
                mImgSelect.setVisibility(View.GONE);
                mImgBlock.setVisibility(View.GONE);
                mConvertView.setEnabled(true);
                break;
            case Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP:
            case Constants.CONTACT.CONTACT_VIEW_NOMAL:
                mImgSelect.setVisibility(View.GONE);
                mImgBlock.setVisibility(View.GONE);
                mConvertView.setEnabled(true);
                break;
            case Constants.CONTACT.CONTACT_VIEW_CHECKBOX:
                mImgSelect.setVisibility(View.VISIBLE);
                mImgBlock.setVisibility(View.GONE);
                // enable
                mImgSelect.setEnabled(enable);
                mConvertView.setEnabled(enable);
                break;
            case Constants.CONTACT.CONTACT_VIEW_ICON_BLOCK:
                mImgSelect.setVisibility(View.GONE);
                mImgBlock.setVisibility(View.GONE);
                mConvertView.setEnabled(true);
                break;
            case Constants.CONTACT.CONTACT_VIEW_SHARE:
                mImgSelect.setVisibility(View.GONE);
                mImgBlock.setVisibility(View.GONE);
                mConvertView.setEnabled(true);
                break;
            default:
                mImgSelect.setVisibility(View.GONE);
                mImgBlock.setVisibility(View.GONE);
                break;
        }
        if (mMemberThread != null && mMemberThread.contains(entry.getJidNumber())) {
            entry.setChecked(true);
        }
        mImgSelect.setSelected(entry.isChecked());
//        if (mImgSelect.isSelected()){
//            mImgSelect.setVisibility(View.VISIBLE);
//        }

        mImgSelect.setEnabled(!entry.isDisable());
        if (getType() == Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE) {
            mTvwInviteFriendText.setOnClickListener(this);
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_ICON_BLOCK) {
            mImgBlock.setOnClickListener(this);
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_NOMAL) {
            mFrAvatar.setOnClickListener(this);
            mTvwInviteFriendText.setOnClickListener(this);
        }
    }

    //set name, avatar,reeng state
    private void setDetailView(PhoneNumber entry) {
        String stt = entry.getStatus();
        boolean isUserViettel = false;
        if (mApplication.getReengAccountBusiness().getCurrentAccount() != null) {
            isUserViettel = mApplication.getReengAccountBusiness().isViettel();
        }
        boolean nvltEnable = false;
        try {
            int nvltInt = Integer.parseInt(mApplication.getConfigBusiness().getContentConfigByKey(CONFIG.NVLT_ENABLE));
            if (nvltInt == 1) {
                nvltEnable = true;
            }
        } catch (NumberFormatException e) {
            Log.e(TAG, "Exception", e);
        }
        String sttNotMocha = mApplication.getConfigBusiness().getContentConfigByKey(CONFIG.DEFAULT_STATUS_NOT_MOCHA);
        if (getType() == Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE) {
            mViewOptionMarginRight.setVisibility(View.VISIBLE);
        } else {
            mViewOptionMarginRight.setVisibility(View.VISIBLE);
        }
        // draw icon user mocha
        if (getType() == Constants.CONTACT.CONTACT_VIEW_CHECKBOX) {
            mImgReengState.setVisibility(View.GONE);
        } else if ("-1".equals(entry.getContactId()) || entry.isReeng() || (isUserViettel && entry.isViettel())) {
            mImgReengState.setVisibility(View.VISIBLE);
        } else {
            mImgReengState.setVisibility(View.GONE);//TODO ẩn state mocha trong danh ba
        }
        // text
        mTvContactName.setText(entry.getName());
        if (getType() == Constants.CONTACT.CONTACT_VIEW_SHARE) {
            mTvStatus.setText(entry.getJidNumber());
            mTvStatus.setVisibility(View.VISIBLE);
            mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
            mTvwInviteFriendText.setVisibility(View.GONE);
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_CHECKBOX) {
            mTvStatus.setText(entry.getJidNumber());
            mTvStatus.setVisibility(View.GONE);
            mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
            mTvwInviteFriendText.setVisibility(View.GONE);
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_ICON_BLOCK) {
            mTvStatus.setVisibility(View.GONE);
            mTvStatus.setEmoticon(mContext, stt, entry.getIdInt(), entry);
            mTvwInviteFriendText.setVisibility(View.GONE);
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_FORWARD_MESSAGE) {
            mTvwInviteFriendText.setVisibility(View.GONE);
            mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
            if ("-1".equals(entry.getContactId())) {
                //mTvStatus.setText(mRes.getString(R.string.add_contact));
                mTvStatus.setVisibility(View.INVISIBLE);
            } else if (entry.isReeng() && stt != null && stt.length() > 0) {
                mTvStatus.setEmoticon(mContext, stt, entry.getIdInt(), entry);
                mTvStatus.setVisibility(View.VISIBLE);
            } else if (!entry.isReeng() && isUserViettel && entry.isViettel()) {
                mTvStatus.setText(mRes.getString(R.string.status_non_mocha));
                mTvStatus.setVisibility(View.VISIBLE);
            } else {
                mTvStatus.setVisibility(View.INVISIBLE);
            }
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_AVATAR_AND_NAME) {
            mTvStatus.setVisibility(View.GONE);
            mTvwInviteFriendText.setVisibility(View.GONE);
            mImgReengState.setVisibility(View.GONE);
        } else if (getType() == Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP) {
            mTvStatus.setVisibility(View.GONE);
            mTvwInviteFriendText.setVisibility(View.GONE);
            mImgReengState.setVisibility(View.GONE);
            mTvContactName.setText(mApplication.getMessageBusiness().getFriendNameOfGroup(entry.getJidNumber()));
        } else {
            if ("-1".equals(entry.getContactId())) {
                //mTvStatus.setText(mRes.getString(R.string.add_contact));
                mTvStatus.setVisibility(View.INVISIBLE);
                mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
                mTvwInviteFriendText.setVisibility(View.GONE);
            } else if (entry.isReeng()) {
                mTvwInviteFriendText.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(stt)) {
                    mTvStatus.setEmoticon(mContext, stt, entry.getIdInt(), entry);
                    mTvStatus.setVisibility(View.VISIBLE);
                    mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
                } else {
                    mTvStatus.setVisibility(View.INVISIBLE);
                }
            } else if (isUserViettel && entry.isViettel()) {
                mTvStatus.setVisibility(View.VISIBLE);
                mTvwInviteFriendText.setVisibility(View.GONE);
                if (nvltEnable) {
                    mTvStatus.setText(sttNotMocha);
                    mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_blue));
                } else {
                    mTvStatus.setText(mRes.getString(R.string.status_non_mocha));
                    mTvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.text_gray));
                }
            } else {
                mTvwInviteFriendText.setVisibility(View.VISIBLE);
                mTvStatus.setVisibility(View.INVISIBLE);
            }
        }
        if (getType() == Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP) {// member group alway an
            mTvwInviteFriendText.setVisibility(View.GONE);
        }
        int size = (int) mRes.getDimension(R.dimen.dimen56dp);
        mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, entry, size);
    }

    //set name, avatar,reeng state, set row chinh minh trong danh sach member group
    private void setMyView() {
        if (getType() == Constants.CONTACT.CONTACT_VIEW_THREAD_MESSAGE) {
            mViewOptionMarginRight.setVisibility(View.VISIBLE);
        } else {
            mViewOptionMarginRight.setVisibility(View.VISIBLE);
        }
        mImgReengState.setVisibility(View.GONE);
        // text
        mTvContactName.setText(mRes.getString(R.string.you));
        mTvStatus.setVisibility(View.GONE);
        mTvwInviteFriendText.setVisibility(View.GONE);
        mTvwAvatar.setVisibility(View.GONE);
        mApplication.getAvatarBusiness().setMyAvatar(mImgAvatar, mApplication.getReengAccountBusiness()
                .getCurrentAccount());
    }

    private void setPhoneView(PhoneNumber entry) {
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.VISIBLE);
        mLlOption.setVisibility(View.VISIBLE);
        // draw
        setOptionView(entry);
        String myNumber = mApplication.getReengAccountBusiness().getJidNumber();
        String friendJid = entry.getJidNumber();
        if (getType() == Constants.CONTACT.CONTACT_VIEW_MEMBER_GROUP && myNumber != null && myNumber.equals
                (friendJid)) {
            setMyView();
        } else {
            setDetailView(entry);
        }
    }

    private void setAvatarView(PhoneNumber entry) {
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.GONE);
        mLlOption.setVisibility(View.GONE);
        // draw
        setOptionView(entry);
        setDetailView(entry);
    }

    public void setSelectedList(ArrayList<String> list) {
        mMemberThread = list;
    }
}
package com.metfone.selfcare.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.BannerMocha;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.EllipsisTextView;

/**
 * Created by toanvk2 on 11/09/2015.
 */
public class ThreadBannerHolder extends AbsContentHolder {
    private ThreadBannerHolder mHolder;
    private Context mContext;
    private CircleImageView mImgThreadAvatar;
    private TextView mTvwTitle;
    private EllipsisTextView mTvwDesc;
    private ApplicationController mApplication;
    private BannerMocha mBanner;

    public ThreadBannerHolder(Context context) {
        mHolder = this;
        mContext = context;
        mApplication = (ApplicationController) mContext.getApplicationContext();
    }

    @Override
    public void setElemnts(Object obj) {
        mBanner = (BannerMocha) obj;
        // hien thi thong tin contact trong thread
        drawDetail();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position,
                           LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_thread_banner_layout, parent, false);
        mImgThreadAvatar = (CircleImageView) rowView.findViewById(R.id.thread_avatar);
        mTvwTitle = (TextView) rowView.findViewById(R.id.thread_name);
        mTvwDesc = (EllipsisTextView) rowView.findViewById(R.id.thread_last_content);
        rowView.setTag(mHolder);
        setConvertView(rowView);
    }

    private void drawDetail() {
        //lay tin nhan cuoi cung cua Thread
        //Resources res = mContext.getResources();
        mTvwTitle.setText(mBanner.getTitle());
        String desc = mBanner.getDesc();
        mTvwDesc.setEmoticon(mContext, desc, desc.hashCode(), desc);
        mApplication.getAvatarBusiness().setLogoOtherApp(mImgThreadAvatar, mBanner.getIcon());
    }
}
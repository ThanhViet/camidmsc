package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;

import com.android.editor.sticker.utils.Constants;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 4/19/2017.
 */
public class MainAddBookHolder extends BaseViewHolder {
    private Context mContext;
    private ApplicationController mApplication;
    private Resources mRes;
    private int screenWidth;

    public MainAddBookHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mContext.getResources();
        // set layout param
        screenWidth = mApplication.getWidthPixels();
        ViewGroup.LayoutParams params = convertView.getLayoutParams();
        params.width = screenWidth / 3;
        params.height = (int) (params.width * Constants.SCALE_HEIGHT);
    }

    @Override
    public void setElement(Object obj) {
    }
}

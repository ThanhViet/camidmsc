package com.metfone.selfcare.holder.message;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.EmoTextViewListChat;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 7/30/2015.
 */
public class ReceivedSearchRankHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedSearchRankHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private ReengMessage message;
    private EmoTextViewListChat mTvwContent;
    private Button mBtnInviteFriend;
    private SmartTextClickListener smartTextClickListener;
    private View.OnLongClickListener longClickListener;

    public ReceivedSearchRankHolder(ApplicationController app, MessageInteractionListener listener,
                                    SmartTextClickListener smartTextClickListener) {
        setContext(app);
        mApplication = app;
        mRes = mApplication.getResources();
        setMessageInteractionListener(listener);
        this.smartTextClickListener = smartTextClickListener;
    }

    public ReceivedSearchRankHolder(ApplicationController app, MessageInteractionListener listener, boolean isQuickReply) {
        setContext(app);
        mApplication = app;
        this.isQuickReply = isQuickReply;
        setMessageInteractionListener(listener);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_search_rank_received, parent, false);
        initBaseHolder(rowView);
        mTvwContent = (EmoTextViewListChat) rowView.findViewById(R.id.message_text_content);
        mBtnInviteFriend = (Button) rowView.findViewById(R.id.btn_invite_friend);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (getMessageInteractionListener() != null)
                    getMessageInteractionListener().longClickBgrCallback(message);
                return false;
            }
        };
        Log.i(TAG, "message: " + message.toString());
        setBaseElements(obj);
        drawDetail();
        setListener();
    }

    private void drawDetail() {
        mTvwContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        mBtnInviteFriend.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        mTvwContent.setEmoticon(message.getContent(), message.getId(), smartTextClickListener, longClickListener, message);
        if (message.getMessageType() == ReengMessageConstant.MessageType.event_follow_room) {
            mBtnInviteFriend.setText(mRes.getString(R.string.join));
            mBtnInviteFriend.setVisibility(View.VISIBLE);
        } else if (message.getMessageType() == ReengMessageConstant.MessageType.fake_mo) {
            if (TextUtils.isEmpty(message.getImageUrl())) {
                mBtnInviteFriend.setText(mRes.getString(R.string.accept));
            } else {
                mBtnInviteFriend.setText(message.getImageUrl());
            }
            mBtnInviteFriend.setVisibility(View.VISIBLE);
        } else {
            mBtnInviteFriend.setText(mRes.getString(R.string.poll_show_vote_detail));
            mBtnInviteFriend.setVisibility(View.VISIBLE);
        }
    }

    private void setListener() {
        mBtnInviteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.getMessageType() == ReengMessageConstant.MessageType.event_follow_room) {
                    getMessageInteractionListener().onFollowRoom(message);
                } else if (message.getMessageType() == ReengMessageConstant.MessageType.fake_mo) {
                    getMessageInteractionListener().onFakeMoClick(message);
                } else {
                    getMessageInteractionListener().onPollDetail(message, false);
                }
            }
        });
    }
}

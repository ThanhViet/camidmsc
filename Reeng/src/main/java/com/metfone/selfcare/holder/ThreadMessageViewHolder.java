package com.metfone.selfcare.holder;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MusicBusiness;
import com.metfone.selfcare.database.constant.CallHistoryConstant;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.OfficerAccount;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by thanhnt72 on 9/27/2016.
 */

public class ThreadMessageViewHolder extends BaseViewHolder {
    private static final String TAG = ThreadMessageViewHolder.class.getSimpleName();

    private ThreadMessage mThreadMessage;
    private CircleImageView ivThreadInfo;
    private RoundedImageView mImgThreadAvatar;
    private View rlAvatarGroup;
    //private ImageView   mImgStateStranger;
    //private LinearLayout mLlStatusImage;
    private ImageView mImgSong, mImgStatus, mImgWatchVideo;
    private TextView mTvwThreadName, mTvwLastTime, mTvwUnreadMsg, mTvwAvatar, mTvwThreadType;
    private EllipsisTextView mTvwLastContent;
    private ImageView mCbxDelete, mIvIconPin;
    private View mCheckboxLayout;
    private RelativeLayout rlRoot;
    private int numOfUnreadMessage;
    private ApplicationController mApplication;
    private Resources res;
    private ContactBusiness contactBusiness;
    private AvatarBusiness avatarBusiness;
    private MusicBusiness musicBusiness;
    private int marginRight10, marginRight30;
    private boolean showIconPin;
    private Context mContext;
    private Activity activity;

    public ThreadMessageViewHolder(View rowView, ApplicationController mApp,Activity activity) {
        super(rowView);
        mApplication = mApp;
        contactBusiness = mApplication.getContactBusiness();
        avatarBusiness = mApplication.getAvatarBusiness();
        musicBusiness = mApplication.getMusicBusiness();
        rlRoot = rowView.findViewById(R.id.rlRoot);
        mImgThreadAvatar = rowView.findViewById(R.id.thread_avatar);
        rlAvatarGroup = rowView.findViewById(R.id.rlAvatarGroup);
        mImgStatus = rowView.findViewById(R.id.thread_last_status);
        mTvwThreadType = rowView.findViewById(R.id.thread_holder_type);
        mTvwAvatar = rowView.findViewById(R.id.contact_avatar_text);
        mTvwThreadName = rowView.findViewById(R.id.thread_name);
        mTvwUnreadMsg = rowView.findViewById(R.id.thread_number_unread);
        mImgSong = rowView.findViewById(R.id.thread_song_ic);
        mTvwLastContent = rowView.findViewById(R.id.thread_last_content);
        mTvwLastTime = rowView.findViewById(R.id.thread_last_time);
        mCbxDelete = rowView.findViewById(R.id.holder_checkbox);
        mCheckboxLayout = rowView.findViewById(R.id.holder_checkbox_layout);
        mImgWatchVideo = rowView.findViewById(R.id.thread_watch_video);
        mIvIconPin = rowView.findViewById(R.id.ivIconPin);
        ivThreadInfo = rowView.findViewById(R.id.ivThreadInfo);
        marginRight10 = mApp.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_10);
        marginRight30 = mApp.getResources().getDimensionPixelOffset(R.dimen.margin_more_content_30);
        //mImgThreadAvatar.setDisableCircularTransformation(true);
        this.activity = activity;
    }

    @Override
    public void setElement(Object obj) {
        mThreadMessage = (ThreadMessage) obj;
        Log.i(TAG, "setElements: " + mThreadMessage.toString());
        res = activity.getResources();
        // hien thi thong tin contact trong thread
        setPhoneNumberView(mThreadMessage);
        setMessageView(mThreadMessage);
    }

    public void setShowIconPin(boolean showIconPin) {
        this.showIconPin = showIconPin;
    }

    private void setMessageView(ThreadMessage threadMessage) {
        //lay tin nhan cuoi cung cua Thread
        CopyOnWriteArrayList<ReengMessage> allMessages = threadMessage.getAllMessages();
        mTvwLastTime.setVisibility(View.INVISIBLE);
        mTvwUnreadMsg.setVisibility(View.GONE);
        if (allMessages.isEmpty()) {
            //neu trong thread ko co tin nhan nao
            numOfUnreadMessage = 0;
            if (threadMessage.isShowTyping()) {
                mImgStatus.setVisibility(View.GONE);
                mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                mTvwLastContent.setTypeface(null, Typeface.NORMAL);
                if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    mTvwLastContent.setText(res.getString(R.string.is_typing));
                } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    String friendName = mApplication.getMessageBusiness()
                            .getFriendName(threadMessage.getPhoneNumberTyping());
                    String textTyping = friendName + " " + res.getString(R.string.is)
                            + " " + res.getString(R.string.is_typing).toLowerCase();
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    mTvwLastContent.setText(textTyping.trim());
                }
            } else if (!TextUtils.isEmpty(threadMessage.getDraftMessage())) {
                mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                mTvwLastContent.setTypeface(null, Typeface.BOLD);
                mImgStatus.setVisibility(View.VISIBLE);
                mImgStatus.setImageResource(R.drawable.ic_draft);
                mTvwLastContent.setText(threadMessage.getDraftMessage());
                mTvwLastContent.setEmoticon(mApplication, threadMessage.getDraftMessage(),
                        threadMessage.getDraftMessage().hashCode(), threadMessage.getDraftMessage());
            } else {
                mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                mTvwLastContent.setTypeface(null, Typeface.NORMAL);
                mImgStatus.setVisibility(View.GONE);
                mTvwLastContent.setText(res.getString(R.string.no_message));
            }
        } else {
            //so tin nhan chua doc
            numOfUnreadMessage = threadMessage.getNumOfUnreadMessage();
            ReengMessage lastMessage = allMessages.get(allMessages.size() - 1);
            //neu khong co tin nhan chua doc
            if (numOfUnreadMessage <= 0) {
                //neu dang typing
                if (threadMessage.isShowTyping()) {
                    mImgStatus.setVisibility(View.GONE);
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                    mTvwLastContent.setTypeface(null, Typeface.NORMAL);
                    if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                        mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                        mTvwLastContent.setText(res.getString(R.string.is_typing));
                    } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                        String friendName = mApplication.getMessageBusiness()
                                .getFriendName(threadMessage.getPhoneNumberTyping());
                        String textTyping = friendName + " " + res.getString(R.string.is)
                                + " " + res.getString(R.string.is_typing).toLowerCase();
                        mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                        mTvwLastContent.setText(textTyping.trim());
                    }
                } else if (!TextUtils.isEmpty(threadMessage.getDraftMessage()) // co draf
                        && threadMessage.getLastTimeSaveDraft() > lastMessage.getTime()) {
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    mTvwLastContent.setTypeface(null, Typeface.BOLD);
                    mImgStatus.setVisibility(View.VISIBLE);
                    mImgStatus.setImageResource(R.drawable.ic_draft);
                    mTvwLastContent.setEmoticon(mApplication, threadMessage.getDraftMessage(),
                            threadMessage.getDraftMessage().hashCode(), threadMessage.getDraftMessage());
                } else if (threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getMessageType() == ReengMessageConstant.MessageType.call
                        && (threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getContent().equals(res.getString(R.string.caller_call_state_cancelled))
                        || threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getContent().equals("00:00"))) {
                    mImgSong.setVisibility(View.VISIBLE);
                    mImgSong.setImageResource(R.drawable.ic_call_out);
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    mTvwLastContent.setTypeface(null, Typeface.NORMAL);
                    setContentLastMessage(lastMessage, threadMessage);
                    setStatusOfMessage(lastMessage);
                } else if (threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getMessageType() == ReengMessageConstant.MessageType.call) {
                    mImgSong.setVisibility(View.VISIBLE);
                    mImgSong.setImageResource(R.drawable.ic_someone_call_to_you);
                    mImgSong.setColorFilter(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    setContentLastMessage(lastMessage, threadMessage);
                    setStatusOfMessage(lastMessage);
                } else {
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    mTvwLastContent.setTypeface(null, Typeface.NORMAL);
                    setContentLastMessage(lastMessage, threadMessage);
                    setStatusOfMessage(lastMessage);
                }
            } else {
                mTvwLastContent.setTypeface(null, Typeface.NORMAL);
                mTvwUnreadMsg.setVisibility(View.VISIBLE);
                if ((threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT)) {
                    mTvwUnreadMsg.setText("N");
                } else {
                    mTvwUnreadMsg.setText(String.valueOf(numOfUnreadMessage));
                }
                if (threadMessage.isShowTyping()) {
                    mImgStatus.setVisibility(View.GONE);
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                    if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
                        mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                        mTvwLastContent.setText(res.getString(R.string.is_typing));
                    } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                        String friendName = mApplication.getMessageBusiness()
                                .getFriendName(threadMessage.getPhoneNumberTyping());
                        String textTyping = friendName + " " + res.getString(R.string.is)
                                + " " + res.getString(R.string.is_typing).toLowerCase();
                        mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                        mTvwLastContent.setText(textTyping.trim());
                    }
                } else if (threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getMessageType() == ReengMessageConstant.MessageType.call &&
                        (threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getContent().equals(res.getString(R.string.call_state_miss_call))
                                || threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getContent().equals("00:00"))) {
                    mImgSong.setVisibility(View.VISIBLE);
                    mImgSong.setImageResource(R.drawable.ic_someone_call_to_you);
                    mImgSong.setColorFilter(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
                    setContentLastMessage(lastMessage, threadMessage);
                    setStatusOfMessage(lastMessage);
                } else if (threadMessage.getAllMessages().get(threadMessage.getAllMessages().size() - 1).getMessageType() == ReengMessageConstant.MessageType.call) {
                    mImgSong.setVisibility(View.VISIBLE);
                    mImgSong.setImageResource(R.drawable.ic_someone_call_to_you);
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    setContentLastMessage(lastMessage, threadMessage);
                    setStatusOfMessage(lastMessage);
                } else {
                    mTvwLastContent.setTextColor(ContextCompat.getColor(mApplication, R.color.text_last_message));
                    setContentLastMessage(lastMessage, threadMessage);
                    setStatusOfMessage(lastMessage);

                }
            }
            //timer
            long timerOfMsg = lastMessage.getTime();
            Date currentDate = new Date();
            long currentTime = currentDate.getTime();
            mTvwLastTime.setVisibility(View.VISIBLE);
            mTvwLastTime.setText(TimeHelper.formatCommonTime(timerOfMsg, currentTime, res));
        }
        if (isSelectMode()) {
            if (mCheckboxLayout != null) {
                mCheckboxLayout.setVisibility(View.VISIBLE);
            }
            mCbxDelete.setVisibility(View.VISIBLE);
            mCbxDelete.setSelected(threadMessage.isChecked());
            if (getItemView() != null) {
                getItemView().setSelected(threadMessage.isChecked());
            }
        } else {
            if (mCheckboxLayout != null) {
                mCheckboxLayout.setVisibility(View.GONE);
            }
            mCbxDelete.setVisibility(View.GONE);
            if (getItemView() != null) {
                getItemView().setSelected(false);
            }
        }
    }

    private void setContentLastMessage(ReengMessage lastMessage, ThreadMessage threadMessage) {
        long beginTime = System.currentTimeMillis();
        mTvwLastTime.setVisibility(View.VISIBLE);
        //last message
        ReengMessageConstant.MessageType type = lastMessage.getMessageType();
        String lastContent;
        if (type == ReengMessageConstant.MessageType.notification ||
                type == ReengMessageConstant.MessageType.notification_fake_mo ||
                type == ReengMessageConstant.MessageType.message_banner) {
            mTvwLastContent.setText(mApplication.getMessageBusiness().getContentOfMessage(lastMessage, res, mApplication));
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT &&
                lastMessage.getDirection() == ReengMessageConstant.Direction.received) {
            String friendName = mApplication.getMessageBusiness().getFriendNameOfGroup(lastMessage.getSender());
            lastContent = TextHelper.getInstant().processFriendName(friendName);
            if (type == ReengMessageConstant.MessageType.text
                    || type == ReengMessageConstant.MessageType.greeting_voicesticker) {
                lastContent = lastContent + lastMessage.getContent();
                mTvwLastContent.setEmoticonWithTag(mApplication, lastContent, lastContent.hashCode(), lastContent,
                        lastMessage.getListTagContent());
            } else {
                lastContent = lastContent + mApplication.getMessageBusiness().getContentOfMessage(lastMessage, res, mApplication);
                mTvwLastContent.setText(lastContent);
            }
        } else if (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT &&
                lastMessage.getDirection() == ReengMessageConstant.Direction.received) {
            String friendName = mApplication.getMessageBusiness().getFriendNameOfRoom(lastMessage.getSender(),
                    lastMessage.getSenderName(), threadMessage.getThreadName());
            lastContent = TextHelper.getInstant().processFriendName(friendName);
            if (type == ReengMessageConstant.MessageType.text
                    || type == ReengMessageConstant.MessageType.greeting_voicesticker) {
                lastContent = lastContent + lastMessage.getContent();
                mTvwLastContent.setEmoticon(mApplication, lastContent, lastContent.hashCode(), lastContent);
            } else {
                lastContent = lastContent + mApplication.getMessageBusiness().getContentOfMessage(lastMessage, res, mApplication);
                mTvwLastContent.setText(lastContent);
            }
        } else {
            if (type == ReengMessageConstant.MessageType.text ||
                    type == ReengMessageConstant.MessageType.greeting_voicesticker) {
                if (ReengMessageConstant.MESSAGE_ENCRYPTED.equals(lastMessage.getMessageEncrpyt())) {
                    mTvwLastContent.setText(res.getString(R.string.encrypt_message));
                } else
                    mTvwLastContent.setEmoticonWithTag(mApplication, lastMessage.getContent(), lastMessage.getId(),
                            lastMessage.getContent(), lastMessage.getListTagContent());
            } else {
                mTvwLastContent.setText(mApplication.getMessageBusiness().getContentOfMessage(lastMessage, res, mApplication));
            }
        }
        Log.i(TAG, "setElements last message content take " + (System.currentTimeMillis() - beginTime));
    }

    private void setStatusOfMessage(ReengMessage lastMessage) {
        int status = lastMessage.getStatus();
         ReengMessageConstant.Direction direction = lastMessage.getDirection();
        if (lastMessage.getMessageType() == ReengMessageConstant.MessageType.watch_video) {
            mImgStatus.setVisibility(View.GONE);
            mImgWatchVideo.setVisibility(View.VISIBLE);
        } else if (lastMessage.getMessageType() == ReengMessageConstant.MessageType.inviteShareMusic ||
                lastMessage.getMessageType() == ReengMessageConstant.MessageType.restore) {
            mImgStatus.setVisibility(View.GONE);
            mImgWatchVideo.setVisibility(View.GONE);
        } else if (lastMessage.getMessageType() == ReengMessageConstant.MessageType.call ||
                lastMessage.getMessageType() == ReengMessageConstant.MessageType.talk_stranger) {
            mImgStatus.setVisibility(View.VISIBLE);
            mImgWatchVideo.setVisibility(View.GONE);
            if (lastMessage.getSongId() == CallHistoryConstant.STATE_MISS) {
                mImgStatus.setImageResource(R.drawable.ic_miss_mini);
            } else if (lastMessage.getSongId() == CallHistoryConstant.STATE_OUT_GOING ||
                    lastMessage.getSongId() == CallHistoryConstant.STATE_BUSY ||
                    lastMessage.getSongId() == CallHistoryConstant.STATE_CANCELLED) {
                mImgStatus.setImageResource(R.drawable.ic_out_going_mini);
            } else {
                mImgStatus.setImageResource(R.drawable.ic_in_coming_mini);
            }
        } else if (lastMessage.getDirection().equals(ReengMessageConstant.Direction.received) ||
                lastMessage.getMessageType().equals(ReengMessageConstant.MessageType.notification) ||
                lastMessage.getMessageType().equals(ReengMessageConstant.MessageType.notification_fake_mo) ||
                lastMessage.getMessageType().equals(ReengMessageConstant.MessageType.poll_action)) {
            mImgStatus.setVisibility(View.GONE);
            mImgWatchVideo.setVisibility(View.GONE);
        } else {
            mImgWatchVideo.setVisibility(View.GONE);
            switch (status) {
                case ReengMessageConstant.STATUS_NOT_SEND: // = 7 ; //chua gui
                    mImgStatus.setVisibility(View.GONE);        //an icon dong ho
                    break;
                case ReengMessageConstant.STATUS_SENT://= 1 ; //da gui
                    mImgStatus.setVisibility(View.VISIBLE);
                    mImgStatus.setImageResource(R.drawable.ic_sent);
                    break;
                case ReengMessageConstant.STATUS_FAIL:// = 2 ; //gui loi, nhan loi
                    mImgStatus.setVisibility(View.VISIBLE);
                    mImgStatus.setImageResource(R.drawable.ic_error);
                    mTvwLastContent.setTypeface(null, Typeface.BOLD);// tin fail bold
                    break;
                case ReengMessageConstant.STATUS_DELIVERED: //= 3 ; //da nhan
                    mImgStatus.setVisibility(View.VISIBLE);
                    mImgStatus.setImageResource(R.drawable.ic_delivery);
                    break;
                case ReengMessageConstant.STATUS_RECEIVED: //= 3 ; //da nhan
                    mImgStatus.setVisibility(View.VISIBLE);
                    mImgStatus.setImageResource(R.drawable.ic_delivery);
                    break;
                case ReengMessageConstant.STATUS_SEEN:
                    mImgStatus.setVisibility(View.VISIBLE);
                    mImgStatus.setImageResource(R.drawable.ic_seen);
                    break;
                case ReengMessageConstant.STATUS_NOT_LOAD://= 5 ; //chua download
                    mImgStatus.setImageResource(R.drawable.ic_delay);
                    break;
                case ReengMessageConstant.STATUS_LOADING://= 6 ;//dang down , dang up, dang gui
                    if (direction.equals(ReengMessageConstant.Direction.send)) {
                        if (Config.Features.FLAG_FAKE_SEND_STATUS_TEXT) {
                            if (lastMessage.getMessageType() == ReengMessageConstant.MessageType.text) {
                                mImgStatus.setVisibility(View.VISIBLE);
                                mImgStatus.setImageResource(R.drawable.ic_sent);
                            } else {
                                mImgStatus.setVisibility(View.GONE);
                            }
                        } else
                            mImgStatus.setVisibility(View.GONE);        //dang gui thi an icon dong ho
                    } else {
                        mImgStatus.setVisibility(View.VISIBLE);
                        mImgStatus.setImageResource(R.drawable.ic_delay);
                    }
                    break;
                default:
                    mImgStatus.setVisibility(View.GONE);
                    break;
            }
        }
    }

    private void setPhoneNumberView(ThreadMessage threadMessage) {
        int threadType = threadMessage.getThreadType();
        if (threadMessage.getLastTimePinThread() == 0 || !showIconPin) {
            mTvwThreadType.setVisibility(View.VISIBLE);
            mIvIconPin.setVisibility(View.GONE);
            rlRoot.setBackgroundColor(mApplication.getResources().getColor(R.color.white));
            mImgThreadAvatar.setBackgroundColor(mApplication.getResources().getColor(R.color.white));
            mTvwLastContent.setPadding(0, 0, marginRight10, 0);
        } else {
            mTvwThreadType.setVisibility(View.GONE);
            mIvIconPin.setVisibility(View.VISIBLE);
            rlRoot.setBackgroundColor(mApplication.getResources().getColor(R.color.v5_background_mess_stick));
            mImgThreadAvatar.setBackgroundColor(mApplication.getResources().getColor(R.color.v5_background_mess_stick));
            mTvwLastContent.setPadding(0, 0, marginRight30, 0);
        }
        mTvwThreadName.setText(mApplication.getMessageBusiness().getThreadName(threadMessage));
        mTvwThreadName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//        rlAvatarGroup.setVisibility(View.GONE);
        mImgThreadAvatar.setVisibility(View.VISIBLE);
        int sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
        if (threadType == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {
            boolean isShowStranger;
            String numberFriend = threadMessage.getSoloNumber();
            PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(numberFriend);
            StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
            //avatar
            if (phoneNumber != null) {
                avatarBusiness.setPhoneNumberAvatar(mImgThreadAvatar, mTvwAvatar, phoneNumber, sizeAvatar);
                //mImgStateStranger.setVisibility(View.GONE);
                isShowStranger = false;
            } else {
                if (threadMessage.isStranger()) {
                    Log.i(TAG, " thread name: " + threadMessage.getThreadName());
                    if (stranger != null) {
                        avatarBusiness.setStrangerAvatar(mImgThreadAvatar, mTvwAvatar,
                                stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                    } else {
                        avatarBusiness.setUnknownNumberAvatar(mImgThreadAvatar, mTvwAvatar, numberFriend, sizeAvatar);
                        //mImgStateStranger.setVisibility(View.GONE);
                    }
                    isShowStranger = true;
                } else {
                    avatarBusiness.setUnknownNumberAvatar(mImgThreadAvatar, mTvwAvatar, numberFriend, sizeAvatar);
                    // mImgStateStranger.setVisibility(View.GONE);
                    isShowStranger = false;
                }
            }
            // show ic cung nghe
            if (musicBusiness.isShowPlayer(numberFriend)) {
                mImgSong.setVisibility(View.VISIBLE);
            } else {
                mImgSong.setVisibility(View.GONE);
            }
            if (isShowStranger) {
                mTvwThreadType.setText(res.getString(R.string.thread_stranger));
                mTvwThreadType.setTextColor(ContextCompat.getColor(mApplication, R.color.thread_orange));
            } else {
//                mTvwThreadType.setText(res.getString(R.string.thread_contact));
                mTvwThreadType.setText("");
                mTvwThreadType.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_stranger_music));
            }
            if (threadMessage.isEncryptThread()) {
                ivThreadInfo.setVisibility(View.VISIBLE);
                ivThreadInfo.setImageResource(R.drawable.ic_e2e_thread);
            } else
                ivThreadInfo.setVisibility(View.GONE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
            avatarBusiness.setGroupThreadAvatar(mImgThreadAvatar, rlAvatarGroup, threadMessage);
            mTvwAvatar.setVisibility(View.GONE);        // Text
            if (musicBusiness.isShowPlayerGroup(threadMessage.getServerId())) {
                mImgSong.setVisibility(View.VISIBLE);
            } else {
                mImgSong.setVisibility(View.GONE);
            }
            mTvwThreadType.setText(res.getString(R.string.thread_group));
            mTvwThreadType.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_button_red));
            ivThreadInfo.setVisibility(View.GONE);
            mTvwThreadType.setVisibility(View.GONE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_ROOM_CHAT) {//chat room
            mTvwThreadType.setVisibility(View.GONE);
            if (musicBusiness.isShowPlayerRoom(threadMessage.getServerId())) {
                mImgSong.setVisibility(View.VISIBLE);
            } else {
                mImgSong.setVisibility(View.GONE);
            }
            avatarBusiness.setOfficialThreadAvatar(mImgThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), null, false);
            mTvwAvatar.setVisibility(View.GONE);
           /* mImgStateStranger.setVisibility(View.VISIBLE);
            mImgStateStranger.setImageResource(R.drawable.ic_room_star);*/
            mTvwThreadType.setText(res.getString(R.string.thread_room));
            mTvwThreadType.setTextColor(ContextCompat.getColor(mApplication, R.color.thread_blue));
            ivThreadInfo.setVisibility(View.GONE);
        } else if (threadType == ThreadMessageConstant.TYPE_THREAD_BROADCAST_CHAT) {
            mImgSong.setVisibility(View.GONE);
            mImgThreadAvatar.setImageResource(R.drawable.chat_ic_broadcast_group);
            mTvwAvatar.setVisibility(View.GONE);
            //mImgStateStranger.setVisibility(View.GONE);
            mTvwThreadType.setText(res.getString(R.string.thread_broadcast));
            mTvwThreadType.setTextColor(ContextCompat.getColor(mApplication, R.color.thread_blue));
            ivThreadInfo.setVisibility(View.GONE);
        } else {    // official
            mTvwThreadName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_official_mark, 0);
            mImgSong.setVisibility(View.GONE);
            OfficerAccount official = mApplication.getOfficerBusiness().getOfficerAccountByServerId(threadMessage.getServerId());
            avatarBusiness.setOfficialThreadAvatar(mImgThreadAvatar, sizeAvatar,
                    threadMessage.getServerId(), official, false);
            mTvwAvatar.setVisibility(View.GONE);
            //mImgStateStranger.setVisibility(View.GONE);
            mTvwThreadType.setText(res.getString(R.string.thread_official));
            mTvwThreadType.setTextColor(ContextCompat.getColor(mApplication, R.color.bg_mocha));
        }
    }

}

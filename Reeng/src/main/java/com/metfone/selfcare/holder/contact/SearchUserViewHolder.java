package com.metfone.selfcare.holder.contact;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thanhnt72 on 6/8/2016.
 */
public class SearchUserViewHolder extends BaseContactHolder {
    private static final String TAG = SearchUserViewHolder.class.getSimpleName();

    private Context mContext;
    private PhoneNumber entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private RoundedImageView mImgAvatar;
    private EllipsisTextView mTvContactName;
    private EllipsisTextView mTvStatus;
    private View mConvertView;
    private TextView mTvwAvatar;
    private FrameLayout mFrAvatar;
    private RelativeLayout mRelContent, mRelCharecter;

    public SearchUserViewHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mApplication = (ApplicationController) mContext.getApplicationContext();
        //
        mConvertView = convertView;
        mImgAvatar = convertView.findViewById(R.id.item_contact_view_avatar_circle);
        mTvwAvatar = convertView.findViewById(R.id.contact_avatar_text);
        mTvContactName = convertView.findViewById(R.id.item_contact_view_name_text);
        mFrAvatar = convertView.findViewById(R.id.item_contact_view_avatar_frame);
        mRelContent = convertView.findViewById(R.id.item_contact_view_content_layout);
        mRelCharecter = convertView.findViewById(R.id.item_contact_view_charecter_layout);
        mTvStatus = convertView.findViewById(R.id.item_description);
        mTvStatus.setVisibility(View.GONE);
    }

    @Override
    public void setElement(Object obj) {
        entry = (PhoneNumber) obj;
        if (entry == null) {
            return;
        }
        // draw
        drawUser(entry);
    }

    private void setDetailView(PhoneNumber entry) {
        // text
        Log.i(TAG, "entry" + entry);
        mTvContactName.setText(entry.getName());
        if (!TextUtils.isEmpty(entry.getStatus())) {
            mTvStatus.setVisibility(View.VISIBLE);
            mTvStatus.setEmoticon(mApplication, entry.getStatus(), entry.getStatus().hashCode(), entry.getStatus());
        } else {
            mTvStatus.setVisibility(View.GONE);
        }
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.VISIBLE);
        int size = (int) mRes.getDimension(R.dimen.avatar_small_size);

        String mFriendAvatarUrl;
        if (TextUtils.isEmpty(entry.getLastChangeAvatar()) || "0".equals(entry.getLastChangeAvatar())) {
            mFriendAvatarUrl = "";
        } else {
            mFriendAvatarUrl = mApplication.getAvatarBusiness().getAvatarUrl
                    (entry.getLastChangeAvatar(), entry.getJidNumber(), size);
        }
        mApplication.getAvatarBusiness().setAvatarOnMedia(mImgAvatar, mTvwAvatar, mFriendAvatarUrl,
                entry.getJidNumber(), entry.getName(), size);
    }

    private void drawUser(PhoneNumber entry) {
        mRelCharecter.setVisibility(View.GONE);
        mFrAvatar.setVisibility(View.VISIBLE);
        mRelContent.setVisibility(View.VISIBLE);
        setDetailView(entry);
    }
}

package com.metfone.selfcare.holder.message;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.StopWatch;
import com.metfone.selfcare.network.AudioVoicemailManager;
import com.metfone.selfcare.ui.imageview.RevealView;

public class ReceivedVoicemailHolder extends BaseMessageHolder {
    private ReengMessage message;
    private ImageView playButton;
    //    private ProgressBar playingSeekBar;
    private TextView length;
    private RevealView mRevealView;

    public ReceivedVoicemailHolder(Context ctx, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        setContext(ctx);
    }

    public ReceivedVoicemailHolder(Context ctx) {
        setContext(ctx);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_voicemail_received, parent, false);
        initBaseHolder(rowView);
        playButton = (ImageView) rowView.findViewById(R.id.voicemail_detail_button_play);
        length = (TextView) rowView.findViewById(R.id.voicemail_detail_length);
//        playingSeekBar = (ProgressBar) rowView.findViewById(R.id.voicemail_seekbar);
        mRevealView = (RevealView) rowView.findViewById(R.id.image_progress);

        Resources mRes = getContext().getResources();
        int width = mRes.getDimensionPixelOffset(R.dimen.voice_progress_bar_width);
        int height = mRes.getDimensionPixelOffset(R.dimen.voice_progress_bar_height);

        Bitmap b = BitmapFactory.decodeResource(mRes, R.drawable
                .ic_voicemail_progress_mocha);
        Bitmap bm = Bitmap.createScaledBitmap(b, width, height, false);
        mRevealView.setImageBitmap(bm);
        mRevealView.setSecondBitmap(BitmapFactory.decodeResource(mRes, R.drawable
                .ic_voicemail_progress_gray), width, height);

        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        length.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        AudioVoicemailManager.addToHashmap(message.getId(), getConvertView());
        //----------------------same sender-----------------------
        if (message.isPlaying()) {
            playButton.setImageResource(R.drawable.pause_circle_filled_recived);
        } else {
            playButton.setImageResource(R.drawable.play_circle_filled_recived);
//            playingSeekBar.setProgress(0);
            mRevealView.setPercentage(0);
        }
        if (isQuickReply) {
//            playingSeekBar.setVisibility(View.GONE);
        }
        length.setText(StopWatch.convertSecondInMMSSForm(false, message.getDuration()));
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.holder.content;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentMusic;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;

public class MusicPlaylistDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    @Nullable
    TextView tvDesc;
    @BindView(R.id.tv_total_view)
    @Nullable
    TextView tvTotalView;
    @BindView(R.id.tv_datetime)
    @Nullable
    TextView tvDatetime;
    @BindView(R.id.button_option)
    @Nullable
    View btnOption;
    @BindView(R.id.layout_image)
    @Nullable
    View viewImage;
    @BindView(R.id.layout_left_image)
    @Nullable
    View viewLeftImage;
    @BindView(R.id.layout_right_image)
    @Nullable
    View viewRightImage;
    @BindView(R.id.iv_top_left)
    @Nullable
    ImageView ivTopLeft;
    @BindView(R.id.iv_top_right)
    @Nullable
    ImageView ivTopRight;
    @BindView(R.id.iv_bottom_left)
    @Nullable
    ImageView ivBottomLeft;
    @BindView(R.id.iv_bottom_right)
    @Nullable
    ImageView ivBottomRight;

    private Object data;

    public MusicPlaylistDetailHolder(View view, Activity activity, final OnClickContentMusic listener) {
        super(view);
        initListener(listener);
    }

    public MusicPlaylistDetailHolder(View view, Activity activity, final OnClickContentMusic listener, int widthLayout) {
        super(view);
        initListener(listener);
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    private void initListener(final OnClickContentMusic listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMusicItem(data, getAdapterPosition());
                }
            }
        });
        if (btnOption != null) btnOption.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickMoreMusicItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void setNoImage() {
        if (ivCover != null) ivCover.setVisibility(View.VISIBLE);
        if (viewImage != null) viewImage.setVisibility(View.GONE);
    }

    private void setFourImages() {
        if (ivCover != null) ivCover.setVisibility(View.GONE);
        if (viewImage != null) viewImage.setVisibility(View.VISIBLE);
        if (ivTopLeft != null) ivTopLeft.setVisibility(View.VISIBLE);
        if (ivTopRight != null) ivTopRight.setVisibility(View.VISIBLE);
        if (ivBottomLeft != null) ivBottomLeft.setVisibility(View.VISIBLE);
        if (ivBottomRight != null) ivBottomRight.setVisibility(View.VISIBLE);
        if (viewLeftImage != null) viewLeftImage.setVisibility(View.VISIBLE);
        if (viewRightImage != null) viewRightImage.setVisibility(View.VISIBLE);
    }

    private void setImage(Content item) {
        ArrayList<String> listImages = item.getListImage();
        if (Utilities.notEmpty(listImages)) {
            if (listImages.size() >= 4) {
                setFourImages();
                ImageBusiness.setTopLeftPlaylist(ivTopLeft, listImages.get(0));
                ImageBusiness.setTopRightPlaylist(ivTopRight, listImages.get(1));
                ImageBusiness.setBottomRightPlaylist(ivBottomRight, listImages.get(2));
                ImageBusiness.setBottomLeftPlaylist(ivBottomLeft, listImages.get(3));
            } else {
                setNoImage();
                ImageBusiness.setMusic(listImages.get(0), ivCover);
            }
        } else {
            setNoImage();
            ImageBusiness.setMusic("", ivCover);
        }
    }

    private void bindDataHome(Content content) {
        if (tvTitle != null) tvTitle.setText(content.getName());
        if (tvDesc != null) {
            if (TextUtils.isEmpty(content.getSingerName())) {
                tvDesc.setVisibility(View.GONE);
            } else {
                tvDesc.setText(content.getSingerName());
                tvDesc.setVisibility(View.VISIBLE);
            }
        }
        if (tvTotalView != null) tvTotalView.setVisibility(View.GONE);
        if (tvDatetime != null) tvDatetime.setVisibility(View.GONE);
        if (btnOption != null) btnOption.setVisibility(View.GONE);
        setImage(content);
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        }
    }
}
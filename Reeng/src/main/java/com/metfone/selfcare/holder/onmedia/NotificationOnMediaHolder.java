package com.metfone.selfcare.holder.onmedia;

import android.content.res.Resources;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FeedBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.NotificationModel;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by thanhnt72 on 9/20/2017.
 */
public class NotificationOnMediaHolder extends BaseViewHolder {

    private static final String TAG = NotificationOnMediaHolder.class.getSimpleName();
    private ApplicationController mApp;
    private ContactBusiness mContactBusiness;
    private AvatarBusiness mAvatarBusiness;
    private FeedBusiness mFeedBusiness;
    private Resources mRes;
    private View layoutNotify;
    private RoundedImageView mImgAvatar;
    private TextView mTvwAvatar;
    private TextView mTvwContentNotification;
    private TextView mTvwTime, mTvwSite;
    private NotificationModel notificationModel;
    private int size;
    private AppCompatImageView ivMore, ivType;

    public NotificationOnMediaHolder(View itemView, ApplicationController mApp) {
        super(itemView);
        this.mApp = mApp;
        this.mRes = mApp.getResources();
        this.mContactBusiness = mApp.getContactBusiness();
        this.mAvatarBusiness = mApp.getAvatarBusiness();
        this.mFeedBusiness = mApp.getFeedBusiness();
        layoutNotify = itemView.findViewById(R.id.layout_holder_notification);
        mImgAvatar = itemView.findViewById(R.id.img_notify_avatar);
        mTvwAvatar = itemView.findViewById(R.id.tvw_notify_avatar);
        mTvwContentNotification = itemView.findViewById(R.id.tvw_notify_content);
        mTvwTime = itemView.findViewById(R.id.tvw_notify_time);
        mTvwSite = itemView.findViewById(R.id.tvw_notify_site);
        size = (int) mRes.getDimension(R.dimen.avatar_small_size);
        ivMore = itemView.findViewById(R.id.button_more);
        ivType = itemView.findViewById(R.id.iv_type_noti);
    }

    @Override
    public void setElement(Object obj) {
        notificationModel = (NotificationModel) obj;
        if (notificationModel.getActive() == NotificationModel.FEED_SEEN) {
            layoutNotify.setBackgroundResource(R.drawable.bg_onmedia_notify_seen);
//            mTvwTime.setTextColor(mRes.getColor(R.color.text_name_site));
        } else {
            layoutNotify.setBackgroundResource(R.drawable.bg_onmedia_notify);
//            mTvwTime.setTextColor(mRes.getColor(R.color.bg_cirle_seekbar));
        }

        mTvwTime.setText(TimeHelper.caculateTimeFeed(mApp, notificationModel.getTime(), mFeedBusiness.getDeltaTimeServer()));
        if ((notificationModel.getContent() != null && !TextUtils.isEmpty(notificationModel.getContent().getSite()))) {
            if (notificationModel.getType() == NotificationModel.TYPE_NORMAL) {
                mTvwSite.setText(notificationModel.getContent().getSite());
                mTvwSite.setVisibility(View.VISIBLE);
            } else {
                mTvwSite.setVisibility(View.GONE);
            }
        } else {
            mTvwSite.setVisibility(View.GONE);
        }
        mTvwContentNotification.setText(TextHelper.fromHtml(notificationModel.getBody()));
//        Log.i(TAG, "body: " + notificationModel.getBody());
        if (notificationModel.getActionType() == FeedModelOnMedia.ActionLogApp.LIKE) {
            ivType.setVisibility(View.VISIBLE);
            ivType.setImageResource(R.drawable.ic_like_noti);
        } else if (notificationModel.getActionType() == FeedModelOnMedia.ActionLogApp.COMMENT) {
            ivType.setVisibility(View.VISIBLE);
            ivType.setImageResource(R.drawable.ic_comment_noti);
        } else {
            ivType.setVisibility(View.INVISIBLE);
        }
        setLayoutStatus();
    }

    private String getTextActionType(String name) {
        String textActionType = TextHelper.textBoldWithHTML(name);
        switch (notificationModel.getActionType()) {
            case LIKE:
                textActionType = textActionType + " " + mRes.getString(R.string.connections_cung_thich);
                break;
            case COMMENT:
                textActionType = textActionType + " " + mRes.getString(R.string.feed_comment_on);
                break;
            case SHARE:
                textActionType = textActionType + " " + mRes.getString(R.string.connections_cung_chia_se);
                break;
            case POST:
                textActionType = textActionType + " " + mRes.getString(R.string.connections_cung_chia_se);
                break;
            default:

                break;
        }

        return textActionType;
    }

    private String getTextFeedProfile(String itemType, String friendName) {
        if (itemType.equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM)) {
            if (TextUtils.isEmpty(friendName)) {
                return mRes.getString(R.string.om_notify_your_photo);
            } else {
                friendName = TextHelper.textBoldWithHTML(friendName);
                return String.format(mRes.getString(R.string.om_notify_friend_photo), friendName);
            }
        } else if (itemType.equals(FeedContent.ITEM_TYPE_PROFILE_STATUS)) {
            if (TextUtils.isEmpty(friendName)) {
                return mRes.getString(R.string.om_notify_your_status);
            } else {
                friendName = TextHelper.textBoldWithHTML(friendName);
                return String.format(mRes.getString(R.string.om_notify_friend_status), friendName);
            }
        } else if (itemType.equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR)) {
            if (TextUtils.isEmpty(friendName)) {
                return mRes.getString(R.string.om_notify_your_avatar);
            } else {
                friendName = TextHelper.textBoldWithHTML(friendName);
                return String.format(mRes.getString(R.string.om_notify_friend_avatar), friendName);
            }
        } else if (itemType.equals(FeedContent.ITEM_TYPE_PROFILE_COVER)) {
            if (TextUtils.isEmpty(friendName)) {
                return mRes.getString(R.string.om_notify_your_cover);
            } else {
                friendName = TextHelper.textBoldWithHTML(friendName);
                return String.format(mRes.getString(R.string.om_notify_friend_cover), friendName);
            }
        } else {
            return "";
        }
    }

    private void setLayoutStatus() {
        UserInfo userInfo = notificationModel.getUserInfo();
        if (userInfo != null) {
            String textBold = "";
            String feedType = notificationModel.getContent().getItemType();
            // neu la feed profile
            if (isFeedFromProfile(feedType)) {
                UserInfo userFeedInAction = notificationModel.getContent().getUserInfo();
                if (userFeedInAction != null) {
                    final String phoneNumber = userFeedInAction.getMsisdn();
                    if (!TextUtils.isEmpty(phoneNumber)) {
                        setContentFriendActionInMyFeed(userInfo, userFeedInAction);
                    } else {
                        Log.d(TAG, "TextUtils.isEmpty(phoneNumber)");
                    }
                } else {
                    Log.d(TAG, "userFeedInAction null");
                }
            } else {
                String nameFriend = "";
                if (userInfo.getUser_type() == UserInfo.USER_ONMEDIA_NORMAL) {
                    final String phoneNumber = userInfo.getMsisdn();
                    if (!TextUtils.isEmpty(phoneNumber)) {
                        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(phoneNumber);
                        if (mPhoneNumber != null) {
                            Log.i(TAG, "avatar: " + userInfo.getAvatar());
                            nameFriend = mPhoneNumber.getName();
                            textBold = getTextActionType(nameFriend);
                            mAvatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, mPhoneNumber, size);
                        } else {
                            if (TextUtils.isEmpty(userInfo.getName())) {
                                nameFriend = Utilities.hidenPhoneNumber(phoneNumber);
                            } else {
                                nameFriend = userInfo.getName();
                            }
                            textBold = getTextActionType(nameFriend);

                            String mFriendAvatarUrl;
                            if (userInfo.isUserMocha()) {
                                if ("0".equals(userInfo.getAvatar())) {
                                    mFriendAvatarUrl = "";
                                } else {
                                    mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(userInfo.getAvatar(), userInfo
                                            .getMsisdn(), size);
                                }
                            } else {
                                mFriendAvatarUrl = userInfo.getAvatar();
                            }
                            mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                                    mFriendAvatarUrl, userInfo.getMsisdn(), nameFriend, size);
                        }
                    } else {
                        Log.d(TAG, "wtf ko tra ve msisdn");
                        mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                                "", userInfo.getMsisdn(), "", size);
                    }
                } else {        //user dac biet
                    nameFriend = userInfo.getName();
                    textBold = getTextActionType(nameFriend);
                    ImageLoaderManager.getInstance(mApp).setAvatarOfficialOnMedia(mImgAvatar, userInfo.getAvatar(),
                            size);
                    Log.i(TAG, "url avatar off: " + userInfo.getAvatar());
                }
                FeedContent feedContent = notificationModel.getContent();
                String content;

                if (needShowContentUpdateInfo()) {
                    if (TextUtils.isEmpty(nameFriend)) {
                        nameFriend = userInfo.getName();
                    }
                    content = getTextContentUpdateInfo(nameFriend);
                    Log.i(TAG, "needShowContentUpdateInfo: " + content);
                } else {
                    Log.i(TAG, "normal info");
                    if (FeedContent.ITEM_TYPE_SOCIAL.equals(feedContent.getItemType())) {
                        content = getTitleContentFeedSocial(textBold, feedContent);
                    } else {
                        String title = "";
                        if (!TextUtils.isEmpty(feedContent.getItemName())) {
                            title = feedContent.getItemName();
                        }
                        content = String.format(mRes.getString(R.string.onmedia_notify), textBold, title);
                        Log.i(TAG, "content != feed social: " + content);
                    }
                }
//                mTvwContentNotification.setText(TextHelper.fromHtml(content));
            }
        } else {
            Log.d(TAG, "sao lai vao day dc @@ user null");
        }
    }

    private String getTitleContentFeedSocial(String textBold, FeedContent feedContent) {
        String content = "";
        String itemSubType = feedContent.getItemSubType();
        UserInfo userInfoContent = feedContent.getUserInfo();
        if (userInfoContent != null) {
            String msisdn = userInfoContent.getMsisdn();
            String friendName = "";
            if (!isMyPhoneNumber(msisdn)) {
                PhoneNumber phoneNumber = mContactBusiness.getPhoneNumberFromNumber(msisdn);
                if (phoneNumber != null) {
                    friendName = phoneNumber.getName();
                } else {
                    friendName = userInfoContent.getName();
                }
            }

            if (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType)) {
                String title = "";
                if (!TextUtils.isEmpty(feedContent.getItemName())) {
                    title = feedContent.getItemName();
                }
                content = String.format(mRes.getString(R.string.onmedia_notify), textBold, title);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(itemSubType)) {
                if (TextUtils.isEmpty(friendName)) {
                    content = textBold + " " + mRes.getString(R.string.om_notify_your_status);
                } else {
                    friendName = TextHelper.textBoldWithHTML(friendName);
                    content = textBold + " " + String.format(mRes.getString(R.string.om_notify_friend_status),
                            friendName);
                }
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(itemSubType)) {
                if (TextUtils.isEmpty(friendName)) {
                    content = textBold + " " + mRes.getString(R.string.om_notify_your_photo);
                } else {
                    friendName = TextHelper.textBoldWithHTML(friendName);
                    content = textBold + " " + String.format(mRes.getString(R.string.om_notify_friend_photo),
                            friendName);
                }
            }
        }
        return content;
    }

    private void setContentFriendActionInMyFeed(UserInfo userAction, UserInfo userFeedInAction) {
        String nameUserFeedInAction = userFeedInAction.getName();
        String jidUserFeedInAction = userFeedInAction.getMsisdn();
        if (isMyPhoneNumber(userFeedInAction.getMsisdn())) {
            nameUserFeedInAction = "";
        } else {
            PhoneNumber phone = mContactBusiness.getPhoneNumberFromNumber(jidUserFeedInAction);
            if (phone != null) {
                nameUserFeedInAction = phone.getName();
            }
        }

        PhoneNumber mPhoneNumber = mContactBusiness.getPhoneNumberFromNumber(userAction.getMsisdn());
        String textBold;
        String nameFriend = "";
        if (mPhoneNumber != null) {
            nameFriend = mPhoneNumber.getName();
            textBold = getTextActionType(mPhoneNumber.getName()) + " " + getTextFeedProfile(notificationModel
                    .getContent().getItemType(), nameUserFeedInAction);
            mAvatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, mPhoneNumber, size);
        } else {
            if (TextUtils.isEmpty(userAction.getName())) {
                nameFriend = Utilities.hidenPhoneNumber(userAction.getMsisdn());
            } else {
                nameFriend = userAction.getName();
            }
            textBold = getTextActionType(nameFriend) + " " + getTextFeedProfile(notificationModel.getContent()
                    .getItemType(), nameUserFeedInAction);

            String mFriendAvatarUrl;
            if (userAction.isUserMocha()) {
                if ("0".equals(userAction.getAvatar())) {
                    mFriendAvatarUrl = "";
                } else {
                    mFriendAvatarUrl = mAvatarBusiness.getAvatarUrl(userAction.getAvatar(), userAction.getMsisdn(),
                            size);
                }
            } else {
                mFriendAvatarUrl = userAction.getAvatar();
            }
            mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                    mFriendAvatarUrl, userAction.getMsisdn(), nameFriend, size);
        }
        if (needShowContentUpdateInfo()) {
            if (TextUtils.isEmpty(nameFriend)) {
                nameFriend = userAction.getName();
            }
            textBold = getTextContentUpdateInfo(nameFriend);
            Log.i(TAG, "needShowContentUpdateInfo: " + textBold);
        } else {
            Log.i(TAG, "textBold: " + textBold);
        }
//        mTvwContentNotification.setText(TextHelper.fromHtml(textBold));
    }

    //change avatar, update status, post content
    private String getTextContentUpdateInfo(String name) {
        String itemType = notificationModel.getContent().getItemType();
        String itemSubType = notificationModel.getContent().getItemSubType();
        String textActionType = TextHelper.textBoldWithHTML(name);
        if (FeedContent.ITEM_TYPE_PROFILE_ALBUM.equals(itemType)) {
            textActionType = textActionType + " " + mRes.getString(R.string.om_notify_update_image);
        } else if (FeedContent.ITEM_TYPE_PROFILE_AVATAR.equals(itemType)) {
            textActionType = textActionType + " " + mRes.getString(R.string.om_notify_update_avatar);
        } else if (FeedContent.ITEM_TYPE_PROFILE_STATUS.equals(itemType)) {
            textActionType = textActionType + " " + mRes.getString(R.string.om_notify_update_status) + " " +
                    notificationModel.getContent().getDescription();
        } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType)) {
            if (FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(itemSubType)) {
                textActionType = textActionType + " " + mRes.getString(R.string.om_notify_update_image);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType)) {
                textActionType = textActionType + " " + mRes.getString(R.string.om_notify_post_content);
            } else if (FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(itemSubType)) {
                textActionType = textActionType + " " + mRes.getString(R.string.om_notify_update_status) + " " +
                        notificationModel.getContent().getContentStatus();
            } else {
                textActionType = textActionType + " " + mRes.getString(R.string.om_notify_post_content);
            }
        } else {
            textActionType = textActionType + " " + mRes.getString(R.string.om_notify_post_content);
        }

        return textActionType;
    }

    public boolean isFeedFromProfile(String feedType) {
        return feedType.equals(FeedContent.ITEM_TYPE_PROFILE_ALBUM) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_COVER) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_AVATAR) ||
                feedType.equals(FeedContent.ITEM_TYPE_PROFILE_STATUS);
    }

    public boolean needShowContentUpdateInfo() {
        if (notificationModel.getActionType() == FeedModelOnMedia.ActionLogApp.POST) {
            String itemType = notificationModel.getContent().getItemType();
            String itemSubType = notificationModel.getContent().getItemSubType();
            if (isFeedFromProfile(itemType)) {
                return true;
            } else if (FeedContent.ITEM_TYPE_SOCIAL.equals(itemType)) {
                return FeedContent.ITEM_SUB_TYPE_SOCIAL_STATUS.equals(itemSubType) ||
                        FeedContent.ITEM_SUB_TYPE_SOCIAL_LINK.equals(itemSubType) ||
                        FeedContent.ITEM_SUB_TYPE_SOCIAL_IMAGE.equals(itemSubType);
            }
        }
        return false;
    }

    private boolean isMyPhoneNumber(String jid) {
        return mApp.getReengAccountBusiness().getJidNumber().equals(jid);
    }
}

package com.metfone.selfcare.holder;

import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.avno.NumberAVNO;
import com.metfone.selfcare.ui.roundview.RoundTextView;

/**
 * Created by thanhnt72 on 2/25/2018.
 */

public class AVNONumberHolder extends BaseViewHolder {


    private TextView mTvwAVNONumber;
    private TextView mTvwPrice;
    private RoundTextView mTvwAVNOSelect;


    public AVNONumberHolder(View itemView) {
        super(itemView);
        mTvwAVNONumber = (TextView) itemView.findViewById(R.id.tvw_number_avno);
        mTvwPrice = itemView.findViewById(R.id.tvw_avno_price);
        mTvwAVNOSelect = (RoundTextView) itemView.findViewById(R.id.tvw_select_number);
    }

    @Override
    public void setElement(Object obj) {
        NumberAVNO avnoNumber = (NumberAVNO) obj;
        mTvwAVNONumber.setText(avnoNumber.getNumber());
        mTvwPrice.setText(avnoNumber.getPrice());
    }
}

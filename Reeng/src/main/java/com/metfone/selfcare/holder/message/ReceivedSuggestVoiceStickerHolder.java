package com.metfone.selfcare.holder.message;

import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.AvatarBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.listeners.MessageInteractionListener;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.AspectImageView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Log;

import java.util.List;

;

/**
 * Created by thanhnt72 on 4/19/2018.
 */

public class ReceivedSuggestVoiceStickerHolder extends BaseMessageHolder {

    private static final String TAG = ReceivedSuggestVoiceStickerHolder.class.getSimpleName();

    private ApplicationController mApplication;
    private ReengMessage message;
    private Resources mRes;
    private AvatarBusiness mAvatarBusiness;

    private AspectImageView mImgCover;
    private CircleImageView mImgAvatar;
    private TextView mTvwAvatar;
    private EllipsisTextView mTvwContent;
    private View layoutSticker, layoutSticker1, layoutSticker2, layoutSticker3;
    private ImageView mImgSticker1, mImgSticker2, mImgSticker3;

    private int sizeAvatar;


    public ReceivedSuggestVoiceStickerHolder(ApplicationController app, MessageInteractionListener listener) {
        mApplication = app;
        mRes = mApplication.getResources();
        mAvatarBusiness = mApplication.getAvatarBusiness();
        setContext(app);
        setMessageInteractionListener(listener);
        sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(
                R.layout.holder_suggest_voice_sticker, parent, false);
        findComponent(rowView);

        rowView.setTag(this);
        setConvertView(rowView);
    }

    private void findComponent(View rowView) {
        mImgCover = rowView.findViewById(R.id.img_cover);
        mImgAvatar = rowView.findViewById(R.id.thread_avatar);
        mTvwAvatar = rowView.findViewById(R.id.contact_avatar_text);
        mTvwContent = rowView.findViewById(R.id.tvw_content);
        layoutSticker = rowView.findViewById(R.id.layout_send_sticker);
        layoutSticker1 = rowView.findViewById(R.id.layout_sticker_1);
        layoutSticker2 = rowView.findViewById(R.id.layout_sticker_2);
        layoutSticker3 = rowView.findViewById(R.id.layout_sticker_3);
        mImgSticker1 = rowView.findViewById(R.id.img_sticker_item_1);
        mImgSticker2 = rowView.findViewById(R.id.img_sticker_item_2);
        mImgSticker3 = rowView.findViewById(R.id.img_sticker_item_3);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        drawHolder();
        setListener();
    }

    private void setListener() {

    }

    private void drawHolder() {
        ThreadMessage threadMessage = mApplication.getMessageBusiness().getThreadById(message.getThreadId());
        if (threadMessage != null) {
            if (!TextUtils.isEmpty(message.getFileName())) {
                mAvatarBusiness.setAvatarOnMedia(mImgAvatar, mTvwAvatar,
                        message.getFileName(), threadMessage.getSoloNumber(), threadMessage.getThreadName(),
                        sizeAvatar);
            } else {
                PhoneNumber phoneNumber = mApplication.getContactBusiness().getPhoneNumberFromNumber(threadMessage
                        .getSoloNumber());
                if (phoneNumber != null) {
                    mAvatarBusiness.setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, phoneNumber, sizeAvatar);
                } else {
                    StrangerPhoneNumber stranger = threadMessage.getStrangerPhoneNumber();
                    if (threadMessage.isStranger()) {
                        if (stranger != null) {
                            mAvatarBusiness.setStrangerAvatar(mImgAvatar, mTvwAvatar,
                                    stranger, stranger.getPhoneNumber(), null, null, sizeAvatar);
                        } else {
                            mAvatarBusiness.setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, threadMessage
                                    .getSoloNumber(), sizeAvatar);
                        }
                    } else {
                        mAvatarBusiness.setUnknownNumberAvatar(mImgAvatar, mTvwAvatar, threadMessage
                                .getSoloNumber(), sizeAvatar);
                    }
                }
            }
            ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgCover, message.getImageUrl());

            if (message.getListStickerItem() == null) {
                message.parseListSticker();
            }
            List<StickerItem> listSticker = message.getListStickerItem();
            if (listSticker == null || listSticker.isEmpty()) {
                layoutSticker.setVisibility(View.GONE);
            } else if (listSticker.size() == 1) {
                layoutSticker1.setVisibility(View.VISIBLE);
                layoutSticker2.setVisibility(View.GONE);
                layoutSticker3.setVisibility(View.GONE);
                setViewSticker(layoutSticker1, mImgSticker1, listSticker.get(0));
            } else if (listSticker.size() == 2) {
                layoutSticker1.setVisibility(View.VISIBLE);
                layoutSticker2.setVisibility(View.VISIBLE);
                layoutSticker3.setVisibility(View.GONE);
                setViewSticker(layoutSticker1, mImgSticker1, listSticker.get(0));
                setViewSticker(layoutSticker2, mImgSticker2, listSticker.get(1));
            } else if (listSticker.size() == 3) {
                layoutSticker1.setVisibility(View.VISIBLE);
                layoutSticker2.setVisibility(View.VISIBLE);
                layoutSticker3.setVisibility(View.VISIBLE);
                setViewSticker(layoutSticker1, mImgSticker1, listSticker.get(0));
                setViewSticker(layoutSticker2, mImgSticker2, listSticker.get(1));
                setViewSticker(layoutSticker3, mImgSticker3, listSticker.get(2));
            } else {
                layoutSticker.setVisibility(View.GONE);
                Log.e(TAG, "size > 3");
            }
            mTvwContent.setEmoticon(mApplication, message.getContent(), message.getContent().hashCode(), message
                    .getContent());

        }

    }

    private void setViewSticker(View layoutSticker, ImageView mImageView, final StickerItem stickerItem) {
        ImageLoaderManager.getInstance(mContext).displayGifStickerNetwork(mImageView,
                stickerItem.getUrlImg());
        layoutSticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMessageInteractionListener().onGreetingStickerPreviewCallBack(stickerItem);
            }
        });

    }
}

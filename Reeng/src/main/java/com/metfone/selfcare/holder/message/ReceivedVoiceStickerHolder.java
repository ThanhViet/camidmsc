package com.metfone.selfcare.holder.message;

import android.os.Handler;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.StickerBusiness;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.constant.StickerConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerCollection;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.emoticon.EmoticonUtils;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.util.Log;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifDrawableBuilder;


public class ReceivedVoiceStickerHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedVoiceStickerHolder.class.getSimpleName();
    private ImageView content;
    private ReengMessage message;
    private int collectionId = EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID, itemId = 0;
    private StickerBusiness mStickerBusiness;
    private TextView mTvwNoteDownload;
    private GifDrawable gifDrawable;

    public ReceivedVoiceStickerHolder(ApplicationController application, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        setContext(application);
        mStickerBusiness = application.getStickerBusiness();
    }

    public ReceivedVoiceStickerHolder(ApplicationController application) {
        setContext(application);
        mStickerBusiness = application.getStickerBusiness();
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_voice_sticker_received, parent, false);
        initBaseHolder(rowView);
        content = (ImageView) rowView
                .findViewById(R.id.message_detail_file_item_content);
        mTvwNoteDownload = (TextView) rowView.findViewById(R.id.note_download);
        rowView.setTag(this);
        setConvertView(rowView);
    }


    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        mTvwNoteDownload.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2));
        if (message.getMessageType() == ReengMessageConstant.MessageType.gift) {
            drawGifFull(message);
        } else {
            drawVoiceSticker(message);
        }
    }

    private void drawGifFull(ReengMessage message) {
        //mTvwNoteDownload.setText(message.getContent());
        mTvwNoteDownload.setVisibility(View.GONE);
        ImageLoaderManager.getInstance(mContext).displayGifThumb(content, message.getGifThumbPath());
        mStickerBusiness.checkAndDownLoadGifFile(message);

    }

    private void drawVoiceSticker(ReengMessage message) {
        try {
            collectionId = Integer.valueOf(message.getFileName());
            itemId = (int) message.getSongId();
            Log.i(TAG, "collectionid: " + collectionId + " itemid: " + itemId);
        } catch (NumberFormatException nfe) {
            Log.e(TAG, "Exception", nfe);
        }
        if (collectionId == EmoticonUtils.STICKER_DEFAULT_COLLECTION_ID) {
            ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
            mTvwNoteDownload.setVisibility(View.GONE);
        } else {
            //kiem tra collection id da duoc download hay chua thi hien thi trong local
            StickerItem mStickerItem = mStickerBusiness.getStickerItem(collectionId, itemId);
            StickerCollection stickerCollection = mStickerBusiness.getStickerCollectionById(collectionId);
            if (stickerCollection == null ||
                    stickerCollection.getCollectionState() != StickerConstant.COLLECTION_STATE_ENABLE) {
                mTvwNoteDownload.setVisibility(View.GONE);
                if (mStickerItem == null) {
                    ImageLoaderManager.getInstance(mContext).displayStickerOnNetwork(content, collectionId, itemId);
                } else {
                    ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
                }
            } else if (mStickerItem == null) {// chua co item
                ImageLoaderManager.getInstance(mContext).displayStickerOnNetwork(content, collectionId, itemId);
                if (stickerCollection.isDefault() == 1) {// bo general
                    mTvwNoteDownload.setVisibility(View.GONE);
                } else if (stickerCollection.isDownloaded()) {
                    if (stickerCollection.isUpdateCollection()) {
                        mTvwNoteDownload.setVisibility(View.VISIBLE);
                        mTvwNoteDownload.setText(mContext.getResources().getString(R.string.touch_to_update));
                    } else {// khong co item nhung bo da update roi
                        mTvwNoteDownload.setVisibility(View.GONE);
                    }
                } else {
                    mTvwNoteDownload.setVisibility(View.VISIBLE);
                    mTvwNoteDownload.setText(mContext.getResources().getString(R.string.download_sticker));
                }
            } else {// da co item
                if (stickerCollection.isDefault() == 1 || stickerCollection.isDownloaded()) {// bo general hoac da down
                    mTvwNoteDownload.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(mStickerItem.getType())
                            && mStickerItem.getType().equals(StickerConstant.STICKER_TYPE_GIF)) {
                        playGifInFirstTime(mStickerItem);
                    } else {
                        ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
                    }
                } else {// bo chua down
                    ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
                    mTvwNoteDownload.setVisibility(View.VISIBLE);
                    mTvwNoteDownload.setText(mContext.getResources().getString(R.string.download_sticker));
                }
            }
        }
    }

    private void playGifInFirstTime(final StickerItem mStickerItem) {
        ImageLoaderManager.getInstance(mContext).displayStickerImage(content, collectionId, itemId);
        if (TextUtils.isEmpty(mStickerItem.getImagePath())) {
            return;
        }
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    gifDrawable = new GifDrawableBuilder().from(mStickerItem.getImagePath()).build();
                    content.setImageDrawable(gifDrawable);
                    gifDrawable.setLoopCount(0);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }
        }, 200);
    }
}
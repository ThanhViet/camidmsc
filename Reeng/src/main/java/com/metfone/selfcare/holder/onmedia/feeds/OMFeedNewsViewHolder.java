package com.metfone.selfcare.holder.onmedia.feeds;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
public class OMFeedNewsViewHolder extends OMFeedBaseViewHolder {
    private FeedModelOnMedia feedModel;
    private ImageView ivFastNews;

    public OMFeedNewsViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        ivFastNews = itemView.findViewById(R.id.ivFastNews);
    }

    @Override
    public void setElement(Object obj) {
        feedModel = (FeedModelOnMedia) obj;
        setBaseElements(obj);
        if (feedModel.getFeedContent().getIsFastNews() == 1) {
            String imageUrl = feedModel.getFeedContent().getImageUrl();
            if (!TextUtils.isEmpty(imageUrl))
                ivFastNews.setVisibility(View.VISIBLE);
            else
                ivFastNews.setVisibility(View.GONE);
        } else
            ivFastNews.setVisibility(View.GONE);
    }
}

package com.metfone.selfcare.holder.onmedia.feeds;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.MochaAdsClient;
import com.metfone.selfcare.common.utils.ScreenManager;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.database.model.onmedia.ChannelOnMedia;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.model.tab_video.BannerVideo;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.utils.ViewUtils;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Utilities;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OMFeedMovieViewHolder extends OMFeedBaseViewHolder implements View.OnClickListener {

    @BindView(R.id.iv_background)
    ImageView ivBackground;
    @BindView(R.id.dark)
    View dark;
    @BindView(R.id.iv_icon_banner)
    ImageView ivIconBanner;
    @BindView(R.id.tv_content_banner)
    TextView tvContentBanner;
    @BindView(R.id.btn_banner)
    RoundTextView btnBanner;
    @BindView(R.id.frame_banner)
    LinearLayout frameBanner;
    @BindView(R.id.iv_icon_banner_conform)
    ImageView ivIconBannerConform;
    @BindView(R.id.tv_content_banner_conform)
    TextView tvContentBannerConform;
    @BindView(R.id.tv_btn_banner_conform)
    RoundTextView tvBtnBannerConform;
    @BindView(R.id.frame_banner_conform)
    LinearLayout frameBannerConform;
    @BindView(R.id.root_frame_banner)
    RelativeLayout rootFrameBanner;
    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.iv_poster)
    ImageView ivPoster;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.ivHear)
    ImageView ivHear;
    @BindView(R.id.tvNumberHear)
    TextView tvNumberHear;
    @BindView(R.id.llControllerHear)
    LinearLayout llControllerHear;
    @BindView(R.id.ivComment)
    ImageView ivComment;
    @BindView(R.id.tvNumberComment)
    TextView tvNumberComment;
    @BindView(R.id.llControllerComment)
    LinearLayout llControllerComment;
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @BindView(R.id.tvNumberShare)
    TextView tvNumberShare;
    @BindView(R.id.llControllerShare)
    LinearLayout llControllerShare;
    @BindView(R.id.ivChannel)
    CircleImageView ivChannel;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.itemVideoRoot)
    LinearLayout itemVideoRoot;
    @BindView(R.id.llControllerChannel)
    RelativeLayout llControllerChannel;
    @BindView(R.id.llController)
    LinearLayout llLikeShareComment;
    @BindView(R.id.button_open_keeng)
    View btnOpenKeeng;
    private int count = 0;
    private Map<String, Integer> stringIntegerMap = new HashMap<>();
    private FeedModelOnMedia feedModel;

    private ApplicationController mApp;
    @Nullable
    private BaseSlidingFragmentActivity activity;
    private BannerVideo bannerVideo;
    private MochaAdsClient.MochaAdsEvent.OnMochaAdsListener mochaAdsListener = new MochaAdsClient.MochaAdsEvent.OnMochaAdsListener() {
        @Override
        public void onHideAdsBy(String adsId) {
            if (rootFrameBanner == null) return;
            rootFrameBanner.setVisibility(View.GONE);
        }
    };

    public OMFeedMovieViewHolder(View itemView, ApplicationController mApplication, @Nullable BaseSlidingFragmentActivity activity) {
        this(itemView, mApplication);
        this.activity = activity;
    }

    public OMFeedMovieViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        this.mApp = mApplication;
        ButterKnife.bind(this, itemView);

        itemVideoRoot.setOnClickListener(this);
        llControllerChannel.setOnClickListener(this);
        llControllerHear.setOnClickListener(this);
        llControllerComment.setOnClickListener(this);
        llControllerShare.setOnClickListener(this);
        btnBanner.setOnClickListener(this);
        tvBtnBannerConform.setOnClickListener(this);

        itemView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                MochaAdsClient.MochaAdsEvent.getInstance().addListener(mochaAdsListener);
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                MochaAdsClient.MochaAdsEvent.getInstance().removeListener(mochaAdsListener);
            }
        });
    }

    @Override
    public void setElement(Object obj) {
        feedModel = (FeedModelOnMedia) obj;
        setBaseElements(obj);
        updateData();
        setupButtonOpenKeeng();
    }

    private void setupButtonOpenKeeng() {
        if (btnOpenKeeng != null && activity != null) {
            if (ViewUtils.appInstalledOrNot(activity, Constants.PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE)) {
                btnOpenKeeng.setVisibility(View.GONE);
            } else {
                btnOpenKeeng.setVisibility(View.VISIBLE);
                btnOpenKeeng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NavigateActivityHelper.navigateToPlayStore(activity, Constants.PREF_DEFAULT.PREF_DEF_KEENG_PACKAGE);
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.itemVideoRoot:
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().onClickMediaItem(feedModel);
                break;
            case R.id.llControllerChannel:
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().openChannelInfo(feedModel);
                break;
            case R.id.llControllerHear:
                /*int isLike = feedModel.getIsLike();
                feedModel.setIsLike(isLike == 0 ? 1 : 0);
                feedContent.setCountLike(isLike == 0 ? feedContent.getCountLike() + 1 : feedContent.getCountLike() - 1);
                updateDataLikeController();*/
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().onClickLikeFeed(feedModel);
                break;
            case R.id.llControllerComment:
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().onClickCommentFeed(feedModel);
                break;
            case R.id.llControllerShare:
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().onClickShareFeed(feedModel);
                break;
            case R.id.btn_banner:
                if (activity == null) break;
                showBannerConfig();
                frameBanner.setVisibility(View.GONE);
                frameBannerConform.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_btn_banner_conform:
                if (activity == null) break;
                MochaAdsClient.MochaAdsEvent.getInstance().notifyHideAdsBy("");
                ReportHelper.checkShowConfirmOrRequestFakeMo(mApp, activity, null, bannerVideo.getActFakeMOInline().getCommand(), "deeplink video");
                rootFrameBanner.setVisibility(View.GONE);
                break;
        }
    }

    void updateData() {
        FeedContent feedContent = feedModel.getFeedContent();
        ChannelOnMedia channel = feedContent.getChannel();
        if (channel == null || TextUtils.isEmpty(channel.getName())) {
            llControllerChannel.setVisibility(View.GONE);
        } else {
            llControllerChannel.setVisibility(View.VISIBLE);
            tvChannel.setText(channel.getName());
            if (TextUtils.isEmpty(feedContent.getChannel().getAvatarUrl()))
                Glide.with(mApp).load(R.drawable.error).into(ivChannel);
            else
                Glide.with(mApp).load(feedContent.getChannel().getAvatarUrl()).apply(new RequestOptions().placeholder(
                        R.drawable.error)).into(ivChannel);
        }

        tvTitle.setText(feedContent.getItemName());
        if (!TextUtils.isEmpty(feedContent.getCategory())) {
            tvCategory.setVisibility(View.VISIBLE);
            tvCategory.setText(feedContent.getCategory());
        } else {
            tvCategory.setVisibility(View.GONE);
            tvCategory.setText("");
        }

        Glide.with(mApp).load(feedContent.getImageUrl()).apply(new RequestOptions().placeholder(
                R.color.bg_onmedia_content_item)).into(ivCover);
        String posterUrl = feedContent.getPosterUrl();
        if (TextUtils.isEmpty(posterUrl)) posterUrl = feedContent.getImageUrl();
        ImageBusiness.setPosterMovie(posterUrl, ivPoster);
        updateDataCommentController(feedContent);
        updateDataShareController(feedContent);
        updateDataLikeController(feedContent);
//        bindAds(feedContent);
    }

    public void updateDataLikeController(FeedContent feedContent) {
        tvNumberHear.setText(Utilities.shortenLongNumber(feedContent.getCountLike()));
        ivHear.setImageResource(feedModel.getIsLike() == 1 ? R.drawable.ic_video_item_video_hear_press : R.drawable
                .ic_favior__new);
    }

    public void updateDataCommentController(FeedContent feedContent) {
        tvNumberComment.setText(Utilities.shortenLongNumber(feedContent.getCountComment()));
    }

    public void updateDataShareController(FeedContent feedContent) {
        tvNumberShare.setText(Utilities.shortenLongNumber(feedContent.getCountShare()));
    }

    public void bindAds(FeedContent feedContent) {
        rootFrameBanner.setVisibility(View.GONE);
        bannerVideo = SharedPrefs.getInstance().get(MochaAdsClient.BANNER_VIDEO, BannerVideo.class);
        if (bannerVideo == null || feedContent == null || activity == null) return;
        BannerVideo.Filter filter = bannerVideo.getFilter();
        if (filter == null) return;
        if (Utilities.notEmpty(filter.getChannelId())) {
            if (!filter.getChannelId().contains(feedContent.getChannel().getId()))
                return;
        }
        if (Utilities.notEmpty(filter.getNetworkType())) {
            boolean networkAccept = false;
            for (String s : filter.getNetworkType()) {
                if (s.toUpperCase().equals(NetworkHelper.getTextTypeConnectionForLog(ApplicationController.self()).toUpperCase()))
                    networkAccept = true;
            }
            if (!networkAccept) return;
        }
        if (Utilities.notEmpty(filter.getIsVip())) {
            String isVip = ApplicationController.self().getReengAccountBusiness().isVip() ? "1" : "0";
            boolean vipAccept = false;
            for (String s : filter.getIsVip()) {
                if (s.toUpperCase().equals(isVip.toUpperCase()))
                    vipAccept = true;
            }
            if (!vipAccept) return;
        }
        if (bannerVideo.getlStartTime() > System.currentTimeMillis() || bannerVideo.getlEndTime() < System.currentTimeMillis())
            return;
        if (Utilities.isEmpty(feedModel.getBase64RowId())) return;
        Integer posi = stringIntegerMap.get(feedModel.getBase64RowId());
        if (posi == null || posi < 0) {
            count++;
            posi = count;
        }
        stringIntegerMap.put(feedModel.getBase64RowId(), posi);
        if (posi != 1 && posi != 3 && posi != 5 && posi != 7) return;

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) rootFrameBanner.getLayoutParams();
        layoutParams.width = ScreenManager.getWidth(activity);
        layoutParams.height = layoutParams.width * 141 / 1080;
        rootFrameBanner.setLayoutParams(layoutParams);

        if (Utilities.notEmpty(bannerVideo.getDisplay().getBackground())) {
            ivBackground.setVisibility(View.VISIBLE);
            ImageManager.showImage(bannerVideo.getDisplay().getBackground(), ivBackground);
        } else {

            ivBackground.setVisibility(View.GONE);
        }

        rootFrameBanner.setVisibility(View.VISIBLE);
        showBanner();
    }

    private void showBanner() {
        if (Utilities.notEmpty(bannerVideo.getDisplay().getContent())) {
            dark.setVisibility(View.VISIBLE);
            tvContentBanner.setVisibility(View.VISIBLE);
            tvContentBanner.setText(bannerVideo.getDisplay().getContent());
        } else {
            dark.setVisibility(View.GONE);
            tvContentBanner.setVisibility(View.GONE);
        }
        if (Utilities.notEmpty(bannerVideo.getDisplay().getIcon())) {
            ivIconBanner.setVisibility(View.VISIBLE);
            ImageManager.showImageNotCenterCrop(bannerVideo.getDisplay().getIcon(), ivIconBanner);
        } else {
            ivIconBanner.setVisibility(View.GONE);
        }
        btnBanner.setText(bannerVideo.getDisplay().getLabelBtn());
    }

    private void showBannerConfig() {
        if (bannerVideo.getActFakeMOInline() != null) {
            if (Utilities.notEmpty(bannerVideo.getActFakeMOInline().getContentConfirm())) {
                dark.setVisibility(View.VISIBLE);
                tvContentBannerConform.setVisibility(View.VISIBLE);
                tvContentBannerConform.setText(bannerVideo.getDisplay().getContent());
            } else {
                dark.setVisibility(View.GONE);
                tvContentBannerConform.setVisibility(View.GONE);
            }
            if (Utilities.notEmpty(bannerVideo.getActFakeMOInline().getIconConfirm())) {
                ivIconBannerConform.setVisibility(View.VISIBLE);
                ImageManager.showImageNotCenterCrop(bannerVideo.getActFakeMOInline().getIconConfirm(), ivIconBannerConform);
            } else {
                ivIconBannerConform.setVisibility(View.GONE);
            }
            tvBtnBannerConform.setText(bannerVideo.getActFakeMOInline().getLabelConfirm());
        } else if (bannerVideo.getActSMS() != null) {
            rootFrameBanner.setVisibility(View.GONE);
            MochaAdsClient.MochaAdsEvent.getInstance().notifyHideAdsBy("");
            if (activity != null)
                NavigateActivityHelper.openAppSMS(activity, bannerVideo.getActSMS().getSmsCodes(), bannerVideo.getActSMS().getSmsCommand());
        }
    }
}

package com.metfone.selfcare.holder.message;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.helper.message.ProgressFileManager;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.DonutProgress;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.textview.MiddleMultilineTextView;
import com.metfone.selfcare.util.Log;

/**
 * Created by thaodv on 7/12/2014.
 */
public class SendFileMessageHolder extends BaseMessageHolder {
    private RoundedImageView iconAttachment;
    private View vLoading;
    private static final String TAG = SendFileMessageHolder.class.getSimpleName();
    private MiddleMultilineTextView fileName;
    private TextView fileSize;
    private ReengMessage message;

    private DonutProgress mProgressFirst;
    private DonutProgress mProgressSecond;
    private AnimatorSet animatorSet;
    private ApplicationController mApp;

    public SendFileMessageHolder(Context ctx, SmartTextClickListener smartTextListener) {
        //this.mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_file_send, parent, false);
        initBaseHolder(rowView);
        iconAttachment = (RoundedImageView) rowView.findViewById(R.id.message_detail_file_item_content);
        vLoading = rowView.findViewById(R.id.message_detail_file_loading);
        fileName = (MiddleMultilineTextView) rowView.findViewById(R.id.message_file_name);
        fileSize = (TextView) rowView.findViewById(R.id.message_file_size);
        mProgressFirst = (DonutProgress) rowView.findViewById(R.id.progress_bar_first);
        mProgressSecond = (DonutProgress) rowView.findViewById(R.id.progress_bar_second);
        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(mContext, R.animator.progress_anim_first);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.setTarget(mProgressFirst);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        // Log.i(TAG, "" + message);
        setBaseElements(obj);
        fileName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext,
                Constants.FONT_SIZE.LEVEL_2_5));
        fileSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext,
                Constants.FONT_SIZE.LEVEL_1_5));
        Log.d(TAG, TAG + "-" + message.getId() + " " + message.getStatus());
        if (message.getId() > 0) {// message luu db co id, id==0 thi ko xu ly
            ProgressFileManager.addToHashMap(message.getId(), getConvertView());
            if (ProgressFileManager.getHandler() == null) {
                ProgressFileManager.initHandler();
            }
        }
        //
        drawFileType();
        if (TextUtils.isEmpty(message.getDirectLinkMedia()) && (message.getStatus() == ReengMessageConstant.STATUS_LOADING
                || message.getStatus() == ReengMessageConstant.STATUS_NOT_SEND)) {
            if (!message.isPlaying()) {
                vLoading.setVisibility(View.VISIBLE);
                if (!message.isRunFirstAnimation()) {
                    message.setRunFirstAnimation(true);
                    animatorSet.start();
                } else {
                    mProgressFirst.setProgress(30f);
                }
                mProgressFirst.setVisibility(View.VISIBLE);
                mProgressSecond.setVisibility(View.GONE);
            }
        } else {
            Log.i(TAG, "isPlaying: " + message.isPlaying());
            if (!message.isPlaying()) {
                vLoading.setVisibility(View.GONE);
            } else {
                vLoading.setVisibility(View.VISIBLE);
                mProgressFirst.setVisibility(View.VISIBLE);
            }
        }
        fileName.setText(message.getFileName());
        fileSize.setText(FileHelper.formatFileSize(message.getSize()));
    }

    private void drawFileType() {
        String fileType = FileHelper.getExtensionFile(message.getFileName());
        switch (fileType) {
            case "doc":
                Glide.with(mContext).load(R.drawable.ic_file_doc).into(iconAttachment);
                break;
            case "docx":
                Glide.with(mContext).load(R.drawable.ic_file_doc).into(iconAttachment);
                break;
            case "mp3":
                Glide.with(mContext).load(R.drawable.ic_file_mp3).into(iconAttachment);
                break;
            case "wav":
                Glide.with(mContext).load(R.drawable.ic_file_mp3).into(iconAttachment);
                break;
            case "pdf":
                Glide.with(mContext).load(R.drawable.ic_file_pdf).into(iconAttachment);
                break;
            case "ppt":
                Glide.with(mContext).load(R.drawable.ic_file_ppt).into(iconAttachment);
                break;
            case "pptx":
                Glide.with(mContext).load(R.drawable.ic_file_ppt).into(iconAttachment);
                break;
            case "psd":
                Glide.with(mContext).load(R.drawable.ic_file_psd).into(iconAttachment);
                break;
            case "rar":
                Glide.with(mContext).load(R.drawable.ic_file_rar).into(iconAttachment);
                break;
            case "txt":
                Glide.with(mContext).load(R.drawable.ic_file_txt).into(iconAttachment);
                break;
            case "xls":
                Glide.with(mContext).load(R.drawable.ic_file_xls).into(iconAttachment);
                break;
            case "xlsx":
                Glide.with(mContext).load(R.drawable.ic_file_xls).into(iconAttachment);
                break;
            case "zip":
                Glide.with(mContext).load(R.drawable.ic_file_zip).into(iconAttachment);
                break;
            default:
                Glide.with(mContext).load(R.drawable.ic_file_other).into(iconAttachment);
                break;
        }

    }
}
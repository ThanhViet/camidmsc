package com.metfone.selfcare.holder.contact;

import android.view.View;

import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 8/13/14.
 */
public abstract class BaseContactHolder extends BaseViewHolder {
    private int type = -1;
    private String numberFocus;
    private boolean isOnlyShowAvatar = false;
    private int sizeList = 0;
    private boolean isDifferCharWithPreview = false;

    public BaseContactHolder(View itemView) {
        super(itemView);
    }

    public String getNumberFocus() {
        return numberFocus;
    }

    public void setNumberFocus(String numberFocus) {
        this.numberFocus = numberFocus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isOnlyShowAvatar() {
        return isOnlyShowAvatar;
    }

    public void setOnlyShowAvatar(boolean isOnlyShowAvatar) {
        this.isOnlyShowAvatar = isOnlyShowAvatar;
    }

    public int getSizeList() {
        return sizeList;
    }

    public void setSizeList(int sizeList) {
        this.sizeList = sizeList;
    }
}
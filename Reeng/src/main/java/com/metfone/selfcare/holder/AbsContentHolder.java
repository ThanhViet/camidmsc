package com.metfone.selfcare.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by toanvk2 on 6/28/14.
 */
public abstract class AbsContentHolder {
    private View convertView;
    private boolean isDifferSenderWithAfter = false;
    private boolean isDifferSenderWithPrevious = false;
    private boolean isSameDateWithPrevious = false;
    private boolean isFirstNewMessage = false;
    private boolean isLastMessageOfOthers = false;
    private boolean isLastMessage = false;
    private boolean isLastSeenBlock = false;
    protected Context mContext;
    private boolean isDeleting = false;

    public View getConvertView() {
        return convertView;
    }

    protected boolean isDifferSenderWithAfter() {
        return isDifferSenderWithAfter;
    }

    public void setDifferSenderWithAfter(boolean isSameWithPrevious) {
        this.isDifferSenderWithAfter = isSameWithPrevious;
    }

    // tin nhan khac loai voi tin nhan truoc do
    protected boolean isDifferSenderWithPrevious() {
        return isDifferSenderWithPrevious;
    }

    public void setDifferSenderWithPrevious(boolean isDifferSenderWithPrevious) {
        this.isDifferSenderWithPrevious = isDifferSenderWithPrevious;
    }

    public boolean isSameDateWithPrevious() {
        return isSameDateWithPrevious;
    }

    public void setSameDateWithPrevious(boolean isSameDateWithPrevious) {
        this.isSameDateWithPrevious = isSameDateWithPrevious;
    }

    public boolean isFirstNewMessage() {
        return isFirstNewMessage;
    }

    public void setFirstNewMessage(boolean isFirstNewMessage) {
        this.isFirstNewMessage = isFirstNewMessage;
    }
    public boolean isLastMessage() {
        return isLastMessage;
    }

    public void setIsLastMessage(boolean isLastMessage) {
        this.isLastMessage = isLastMessage;
    }


    protected void setConvertView(View convertView) {
        convertView.setFocusable(false);
        this.convertView = convertView;
    }

    protected Context getContext() {
        return mContext;
    }

    protected void setContext(Context ctx) {
        mContext = ctx;
    }

    public void setDeleting(boolean isDeleting) {
        this.isDeleting = isDeleting;
    }

    public boolean isDeleting() {
        return isDeleting;
    }

    public boolean isLastMessageOfOthers() {
        return isLastMessageOfOthers;
    }


    public void setLastMessageOfOthers(boolean lastMessageOfOthers) {
        isLastMessageOfOthers = lastMessageOfOthers;
    }

    public boolean isLastSeenBlock() {
        return isLastSeenBlock;
    }

    public void setLastSeenBlock(boolean lastSeenBlock) {
        isLastSeenBlock = lastSeenBlock;
    }

    /**
     * @param parent
     * @param rowView
     * @param position
     * @param layoutInflater
     */
    public abstract void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater);

    /**
     * set all element in holder
     *
     * @param obj
     */
    public abstract void setElemnts(Object obj);


}

package com.metfone.selfcare.holder.onmedia.feeds;

import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.ChannelOnMedia;
import com.metfone.selfcare.database.model.onmedia.FeedContent;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by thanhnt72 on 10/4/2016.
 */
public class OMFeedVideoViewHolder extends OMFeedBaseViewHolder implements View.OnClickListener {

    private static final String TAG = OMFeedVideoViewHolder.class.getSimpleName();
    //    FeedModelOnMedia feedModel;
//    FeedContent feedContent;
//    private ApplicationController mApp;
    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivAvatarChannel)
    ImageView ivChannel;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.itemVideoRoot)
    View itemVideoRoot;
    @BindView(R.id.tv_total_view)
    TextView tvTotalView;

    private @Nullable
    BaseSlidingFragmentActivity activity;

    public OMFeedVideoViewHolder(View itemView, ApplicationController mApplication, @Nullable BaseSlidingFragmentActivity activity) {
        this(itemView, mApplication);
        this.activity = activity;
    }

    public OMFeedVideoViewHolder(View itemView, ApplicationController mApplication) {
        super(itemView, mApplication);
        ButterKnife.bind(this, itemView);
        int imageVideoWidthBig = mApplication.getWidthPixels() - 2 * mApplication.getResources().getDimensionPixelOffset(R.dimen.padding_16);

        ViewGroup.LayoutParams layoutParams = ivVideo.getLayoutParams();
        layoutParams.width = imageVideoWidthBig;
        layoutParams.height = imageVideoWidthBig * 9 / 16;
        ivVideo.setLayoutParams(layoutParams);

        itemVideoRoot.setOnClickListener(this);
        tvTotalView.setOnClickListener(this);

    }

    @Override
    public void setElement(Object obj) {
        feedModel = (FeedModelOnMedia) obj;
        setBaseElements(obj);
        updateData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.itemVideoRoot:
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().onClickMediaItem(feedModel);
                break;
            case R.id.ivAvatarChannel:
                if (getOnMediaHolderListener() != null)
                    getOnMediaHolderListener().openChannelInfo(feedModel);
                break;
        }
    }

    private void updateData() {
        FeedContent feedContent = feedModel.getFeedContent();
        if (feedContent != null) {
            ChannelOnMedia channel = feedContent.getChannel();
            if (channel == null || TextUtils.isEmpty(channel.getName())) {
                tvChannel.setVisibility(View.GONE);
                Glide.with(getApplication()).load(R.drawable.ic_logokeeng_new).into(ivChannel);
            } else {
                tvChannel.setVisibility(View.VISIBLE);
                tvChannel.setText(channel.getName());
                if (TextUtils.isEmpty(feedContent.getChannel().getAvatarUrl()))
                    Glide.with(getApplication()).load(R.drawable.ic_logokeeng_new).into(ivChannel);
                else
                    Glide.with(getApplication()).load(feedContent.getChannel().getAvatarUrl()).apply(new RequestOptions().placeholder(
                            R.drawable.error)).into(ivChannel);
            }
            long views = feedContent.getCountView();
            if (views > 0) {
                tvTotalView.setText(String.format((views <= 1) ? getRes().getString(R.string.view)
                        : getRes().getString(R.string.video_views), Utilities.getTotalView(views)));
                tvTotalView.setVisibility(View.VISIBLE);
            } else {
                tvTotalView.setVisibility(View.GONE);
            }
            tvTitle.setText(feedContent.getItemName());
            Glide.with(getApplication()).load(feedContent.getImageUrl()).apply(new RequestOptions().placeholder(
                    R.color.bg_onmedia_content_item)).into(ivVideo);

        }
    }


}

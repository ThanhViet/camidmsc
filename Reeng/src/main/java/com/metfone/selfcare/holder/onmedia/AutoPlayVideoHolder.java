package com.metfone.selfcare.holder.onmedia;

import android.content.res.Resources;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.metfone.selfcare.adapter.onmedia.AutoPlayListVideoAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.AutoPlayVideoModel;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.autoplay.AspectRelativeLayout;
import com.metfone.selfcare.ui.autoplay.AutoPlayManager;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 1/20/2018.
 */

public class AutoPlayVideoHolder extends BaseViewHolder implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener,
        TextureView.SurfaceTextureListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnVideoSizeChangedListener {
    private static final String TAG = AutoPlayVideoHolder.class.getSimpleName();
    private ApplicationController mApplication;
    private Resources mRes;
    private AutoPlayManager mAutoPlayManager;
    private MediaPlayer mMediaPlayer;
    private AutoPlayVideoModel mEntry;
    private AutoPlayListVideoAdapter.OnItemClickListener mCallback;
    private AspectRelativeLayout mLayoutVideo;
    private View mViewMark, mViewLikeShare, mViewProgressControl;
    private TextureView mTextureView;
    private ImageView mImgThumb;
    private ImageView mImgButtonPlay, mImgFullScreen;
    private ProgressLoading mProgressLoading;
    private TextView mTvwVideoName, mTvwVideoViews;
    private TextView mTvwCurrentTime, mTvwDurationTime;
    private SeekBar mSeekBar;
    private int widthImgNews;
    private int heightImgNews;
    private int mTextureWidth;
    private int mTextureHeight;

    private boolean isActive = false;
    private boolean isPrepared = false;
    private boolean isError = false;
    private int position;

    public AutoPlayVideoHolder(View itemView, ApplicationController app, AutoPlayListVideoAdapter.OnItemClickListener listener) {
        super(itemView);
        this.mApplication = app;
        this.mRes = mApplication.getResources();
        this.mCallback = listener;
        mLayoutVideo = itemView.findViewById(R.id.holder_autoplay_video_layout);
        mTextureView = itemView.findViewById(R.id.holder_autoplay_texture);
        mImgThumb = itemView.findViewById(R.id.holder_autoplay_thumb);
        mImgButtonPlay = itemView.findViewById(R.id.holder_autoplay_button_play);
        mProgressLoading = itemView.findViewById(R.id.holder_autoplay_loading);
        mTvwVideoName = itemView.findViewById(R.id.video_detail_name);
        mTvwVideoViews = itemView.findViewById(R.id.video_detail_views);
        mViewProgressControl = itemView.findViewById(R.id.holder_autoplay_progress_control);
        mTvwCurrentTime = itemView.findViewById(R.id.position);
        mTvwDurationTime = itemView.findViewById(R.id.duration);
        mSeekBar = itemView.findViewById(R.id.seeker);
        mImgFullScreen = itemView.findViewById(R.id.img_toggle_screen);

        mViewLikeShare = itemView.findViewById(R.id.holder_autoplay_like_share);
        mViewMark = itemView.findViewById(R.id.holder_autoplay_mark);
        mImgFullScreen.setImageResource(R.drawable.ic_om_video_fullscreen);
        mViewMark.setAlpha(0.8f);
        mViewLikeShare.setVisibility(View.GONE);
        widthImgNews = mRes.getDimensionPixelOffset(R.dimen.width_thumb_news);
        heightImgNews = mRes.getDimensionPixelOffset(R.dimen.height_thumb_news);
    }

    @Override
    public void setElement(Object obj) {
        Log.d(TAG, "setElement ");
        mAutoPlayManager = AutoPlayManager.getInstance(mApplication);
        mEntry = (AutoPlayVideoModel) obj;
        mEntry.bindHolder(this);
        drawDetail();
        setViewListener();
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void test(String s) {
        Log.d(TAG, "test----- " + s);
    }

    @Override
    public void onClick(View view) {
        if (isActive) {
            switch (view.getId()) {
                case R.id.holder_autoplay_button_play:
                    //if (mEntry.getCurrentDuration() > 0 && mMediaPlayer != null) {
                    //mMediaPlayer.setSurface(new Surface(mTextureView.getSurfaceTexture()));
                    if (mImgThumb.getVisibility() == View.VISIBLE) {
                        mImgThumb.setVisibility(View.GONE);
                    }
                    mAutoPlayManager.toggleVideo();
                    drawButtonPlayer();
                    break;
                case R.id.img_toggle_screen:
                    mCallback.onFullScreen();
                    break;
                case R.id.holder_autoplay_video_layout:
                    if (isPrepared) {
                        boolean isShow = mViewProgressControl.getVisibility() == View.GONE;
                        mAutoPlayManager.showOrHideControl(mImgButtonPlay, mViewProgressControl, isShow);
                    }
                    break;
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (isActive && fromUser && mMediaPlayer != null) {
            int totalDuration = mMediaPlayer.getDuration();
            int temp = progress * totalDuration / 100;
            mTvwCurrentTime.setText(Utilities.milliSecondsToTimer(temp));
            mTvwDurationTime.setText(Utilities.milliSecondsToTimer(totalDuration));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        if (isActive)
            mAutoPlayManager.stopTimer();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mAutoPlayManager.seekVideo(seekBar.getProgress());
    }

    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isPrepared() {
        return isPrepared;
    }

    public void resumeSurface() {
        if (mMediaPlayer != null && mTextureView.isAvailable()) {
            mMediaPlayer.setSurface(new Surface(mTextureView.getSurfaceTexture()));
        }
        drawTimeAndProgress();
        drawState();
    }

    public void onActive() {
        Log.d(TAG, "setActive ");
        isActive = true;
        mAutoPlayManager.setCurrentItem(this, mEntry);
        mAutoPlayManager.showOrHideControl(mImgButtonPlay, mViewProgressControl, mEntry.isUserPause());
        if (mTextureView.isAvailable()) {
            checkPlayOrGetDetail(mEntry.getOnMediaModel().getFeedContent().getMediaUrl(), mTextureView.getSurfaceTexture());
        }
        mViewMark.setAlpha(0.0f);
        drawButtonPlayer();
    }

    public void onDeActive() {
        Log.d(TAG, "onDeActive ");
        isActive = false;
        mViewMark.setAlpha(0.8f);
        if (mMediaPlayer != null) {
            mEntry.setCurrentDuration(mMediaPlayer.getCurrentPosition());
            Log.d(TAG, "CurrentDuration: " + mEntry.getCurrentDuration());
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.pause();
        }
        mAutoPlayManager.showOrHideControl(mImgButtonPlay, mViewProgressControl, false);
    }

    public void onResponseUrl() {
        Log.d(TAG, "onResponseUrl");
        if (isActive && mTextureView.isAvailable()) {
            if (mMediaPlayer != null) {
                releaseMedia();
            }
            String url = mEntry.getOnMediaModel().getFeedContent().getMediaUrl();
            if (!TextUtils.isEmpty(url))
                playVideo(url, mTextureView.getSurfaceTexture());
        }
    }

    private void setViewListener() {
        mImgFullScreen.setOnClickListener(this);
        mImgButtonPlay.setOnClickListener(this);
        mSeekBar.setOnSeekBarChangeListener(this);
        mLayoutVideo.setOnClickListener(this);
        mTextureView.setSurfaceTextureListener(this);
    }

    private void drawDetail() {
        mProgressLoading.setVisibility(View.GONE);
        drawState();
        mTvwVideoName.setText(mEntry.getOnMediaModel().getFeedContent().getItemName());
        long views = mEntry.getOnMediaModel().getFeedContent().getCountView();
        //long views = 100;
        if (views < 0) views = 0;
        mTvwVideoViews.setText(String.format((views <= 1) ? mRes.getString(R.string.view)
                : mRes.getString(R.string.video_views), Utilities.getTotalView(views)));
        ImageLoaderManager.getInstance(mApplication).setImageFeeds(mImgThumb, mEntry.getOnMediaModel().getFeedContent().getImageUrl(),
                widthImgNews, heightImgNews);
    }

    private void drawState() {
        if (mMediaPlayer == null || !isPrepared) {
            mImgThumb.setVisibility(View.VISIBLE);
        } else {
            mImgThumb.setVisibility(View.GONE);
        }
        drawButtonPlayer();
        drawTimeAndProgress();
        mAutoPlayManager.showOrHideControl(mImgButtonPlay, mViewProgressControl, false);
    }

    private void drawButtonPlayer() {
        if (mMediaPlayer == null || !mMediaPlayer.isPlaying()) {
            mImgButtonPlay.setImageResource(R.drawable.ic_play_sieuhai);
        } else {
            mImgButtonPlay.setImageResource(R.drawable.ic_pause_sieuhai);
        }
    }

    public void drawTimeAndProgress() {
        mAutoPlayManager.drawTimeAndProgress(mEntry, this, mSeekBar, mTvwCurrentTime, mTvwDurationTime);
    }

    public void hideControlTask() {
        mAutoPlayManager.showOrHideControl(mImgButtonPlay, mViewProgressControl, false);
    }

    private void checkPlayOrGetDetail(String url, SurfaceTexture surface) {
        if (TextUtils.isEmpty(url)) {
            mAutoPlayManager.getDetailVideo(mApplication, mEntry);
        } else {
            playVideo(url, surface);
        }
    }

    public void forcePlayVideo() {
        checkPlayOrGetDetail(mEntry.getOnMediaModel().getFeedContent().getMediaUrl(), mTextureView.getSurfaceTexture());
    }

    private void playVideo(String url, SurfaceTexture surface) {
        Log.d(TAG, "playVideo: " + url);
        Surface s = new Surface(surface);
        if (mMediaPlayer == null) {
            Log.d(TAG, "playVideo: new media");
            isPrepared = false;
            mProgressLoading.setVisibility(View.VISIBLE);
            try {
                mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setDataSource(url);
                mMediaPlayer.setSurface(s);
                mMediaPlayer.setOnPreparedListener(this);
                mMediaPlayer.setOnErrorListener(this);
                mMediaPlayer.setOnCompletionListener(this);
                mMediaPlayer.setOnVideoSizeChangedListener(this);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.prepareAsync();
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                mMediaPlayer = null;
            }
            mAutoPlayManager.reportPlayVideo(mEntry, url);
        } else {
            Log.d(TAG, "playVideo: start media: " + isPrepared + " - " + mMediaPlayer + " ");
            mMediaPlayer.setSurface(s);
            if (isPrepared && !mAutoPlayManager.isPause() && !mEntry.isUserPause()) {
                mEntry.setCurrentDuration(-1);
                mMediaPlayer.start();
                mImgThumb.setVisibility(View.GONE);
            } else {
                mImgThumb.setVisibility(View.VISIBLE);
            }
        }
        drawTimeAndProgress();
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.d(TAG, "onSurfaceTextureAvailable: " + position + " width: " + width + " height: " + height);
        mTextureWidth = width;
        mTextureHeight = height;
        if (isActive) {
            checkPlayOrGetDetail(mEntry.getOnMediaModel().getFeedContent().getMediaUrl(), surface);
        } else if (mMediaPlayer != null) {
            mMediaPlayer.setSurface(new Surface(surface));
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        //Log.d(TAG, "onSurfaceTextureSizeChanged: " + position + " width: " + width + " height: " + height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.d(TAG, "onSurfaceTextureDestroyed: " + position);
        if (!isActive) {
            releaseMedia();
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    private void releaseMedia() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
            isPrepared = false;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion: " + position + isActive);
        if (isActive) {
            mEntry.setCurrentDuration(-1);
            if (mCallback.onCompleted(position)) {
                mEntry.setUserPause(true);
            }
            drawButtonPlayer();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "onError : " + what + " - " + extra);
        isError = true;
        releaseMedia();
        mProgressLoading.setVisibility(View.GONE);
        mAutoPlayManager.notifyError();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "onPrepared : " + mp.getVideoWidth() + " height: " + mp.getVideoHeight());
        //float ratio=
        isPrepared = true;
        isError = false;
        mAutoPlayManager.notifyPrepared(mp);
        mProgressLoading.setVisibility(View.GONE);
        drawTimeAndProgress();
        if (isActive && !mAutoPlayManager.isPause() && !mEntry.isUserPause()) {
            mImgThumb.setVisibility(View.GONE);
            mp.start();
            if (mEntry.getCurrentDuration() > 0) {
                mp.seekTo(mEntry.getCurrentDuration());
                mEntry.setCurrentDuration(-1);
            }
        } else {
            mImgThumb.setVisibility(View.VISIBLE);
        }
        drawButtonPlayer();
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        Log.d(TAG, "onVideoSizeChanged : " + width + " height: " + height);
        mEntry.setVideoSize(width, height);
        mAutoPlayManager.notifySizeChanged(width, height);
        mAutoPlayManager.adjustAspectRatio(mTextureView, mTextureWidth, mTextureHeight, width, height);
    }
}
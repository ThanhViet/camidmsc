package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.guestbook.Song;
import com.metfone.selfcare.holder.BaseViewHolder;

/**
 * Created by toanvk2 on 4/18/2017.
 */
public class SelectMusicHolder extends BaseViewHolder {
    private Context mContext;
    private Song entry;
    private ApplicationController mApplication;
    private TextView mTvwTitle;
    private TextView mTvwSinger;
    private TextView mTvwMonopoly;
    private ImageView mImgThumb;

    public SelectMusicHolder(View convertView, Context context) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        mTvwTitle = (TextView) convertView.findViewById(R.id.title);
        mTvwSinger = (TextView) convertView.findViewById(R.id.singer);
        mImgThumb = (ImageView) convertView.findViewById(R.id.image_thumnail);
        mTvwMonopoly = (TextView) convertView.findViewById(R.id.song_monopoly);
    }

    @Override
    public void setElement(Object obj) {
        entry = (Song) obj;
        mTvwTitle.setText(entry.getName());
        /*if (entry.getSinger() != null) {
            mTvwSinger.setText(entry.getSinger());
        } else {
            mTvwSinger.setText("");
        }*/
        mTvwSinger.setText("");
        mApplication.getAvatarBusiness().setSongAvatar(mImgThumb, entry.getAvatar());
        mTvwMonopoly.setVisibility(View.GONE);
    }
}
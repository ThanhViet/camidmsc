/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.holder.content;

import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickContentComic;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.tab_home.model.Content;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;

import butterknife.BindView;

public class ComicDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.iv_cover)
    @Nullable
    ImageView ivCover;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;

    private Object data;
    private int viewType;

    public ComicDetailHolder(View view, Activity activity, final OnClickContentComic listener) {
        super(view);
        initListener(listener);
    }

    public ComicDetailHolder(View view, Activity activity, final OnClickContentComic listener, int widthLayout) {
        super(view);
        initListener(listener);
        if (widthLayout > 0) {
            ViewGroup.LayoutParams layoutParams = viewRoot.getLayoutParams();
            layoutParams.width = widthLayout;
            viewRoot.setLayoutParams(layoutParams);
            viewRoot.requestLayout();
        }
    }

    public ComicDetailHolder setViewType(int viewType) {
        this.viewType = viewType;
        return this;
    }

    private void initListener(final OnClickContentComic listener) {
        if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (data != null && listener != null) {
                    listener.onClickComicItem(data, getAdapterPosition());
                }
            }
        });
    }

    private void bindDataHome(Content content) {
        if (tvTitle != null) tvTitle.setText(content.getName());
        if (viewType == TabHomeModel.TYPE_LARGE_COMIC) {
            ImageBusiness.setCoverLargeComic(ivCover, content.getImage());
        } else {
            ImageBusiness.setCoverGridComic(ivCover, content.getImage());
        }
    }

    @Override
    public void bindData(Object item, int position) {
        data = item;
        if (item instanceof Content) {
            bindDataHome((Content) item);
        } else if (item instanceof TabHomeModel) {
            TabHomeModel model = (TabHomeModel) item;
            if (model.getObject() instanceof Content) {
                bindDataHome((Content) model.getObject());
            }
        }
    }
}
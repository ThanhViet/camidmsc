package com.metfone.selfcare.holder.message;

import android.content.Context;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.FileHelper;
import com.metfone.selfcare.helper.MessageHelper;
import com.metfone.selfcare.listeners.SmartTextClickListener;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundRelativeLayout;
import com.metfone.selfcare.ui.textview.MiddleMultilineTextView;
import com.metfone.selfcare.util.Log;

public class ReceivedFileHolder extends BaseMessageHolder {
    private static final String TAG = ReceivedFileHolder.class.getSimpleName();
    private RoundedImageView iconAttachment;
    private RoundRelativeLayout vLoading;
    private ProgressLoading mPrgLoading;
    private MiddleMultilineTextView fileName;
    private TextView fileSize;
    private ReengMessage message;
    private ThreadMessage mThreadMessage;
    private ImageButton mIbnDownload;
    //private SmartTextClickListener mSmartTextListener;
    private ApplicationController mApp;

    public ReceivedFileHolder(Context ctx, SmartTextClickListener smartTextListener, boolean isQuickReply) {
        this.isQuickReply = isQuickReply;
        //this.mSmartTextListener = smartTextListener;
        setContext(ctx);
    }

    public ReceivedFileHolder(Context ctx, SmartTextClickListener smartTextListener, ThreadMessage threadMessage) {
        this.mThreadMessage = threadMessage;
        //this.mSmartTextListener = smartTextListener;
        setContext(ctx);
        mApp = (ApplicationController) ctx;
    }

    @Override
    public void initHolder(ViewGroup parent, View rowView, int position, LayoutInflater layoutInflater) {
        rowView = layoutInflater.inflate(R.layout.holder_file_received, parent, false);
        initBaseHolder(rowView);
        iconAttachment = (RoundedImageView) rowView.findViewById(R.id.message_detail_file_item_content);
        vLoading = (RoundRelativeLayout) rowView.findViewById(R.id.message_detail_file_loading);
        mPrgLoading = (ProgressLoading) rowView.findViewById(R.id.message_detail_file_loading_progress);
        fileName = (MiddleMultilineTextView) rowView.findViewById(R.id.message_file_name);
        fileSize = (TextView) rowView.findViewById(R.id.message_file_size);
        mIbnDownload = (ImageButton) rowView.findViewById(R.id.message_file_download);
        rowView.setTag(this);
        setConvertView(rowView);
    }

    @Override
    public void setElemnts(Object obj) {
        message = (ReengMessage) obj;
        setBaseElements(obj);
        fileName.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_2_5));
        fileSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, MessageHelper.getTextSizeChat(mContext, Constants.FONT_SIZE.LEVEL_1_5));
        Log.d(TAG, "setElemnts: " + message.getStatus());
        drawFileType();
        if (message.getStatus() == ReengMessageConstant.STATUS_LOADING) {
            mIbnDownload.setVisibility(View.GONE);
            vLoading.setVisibility(View.VISIBLE);
            mPrgLoading.setVisibility(View.VISIBLE);
            vLoading.setBackgroundColorAndPress(ContextCompat.getColor(mContext, R.color
                    .bg_color_loading_holder), 0);
        } else if (message.getStatus() == ReengMessageConstant.STATUS_NOT_LOAD || TextUtils.isEmpty(message.getFilePath())) {
            vLoading.setVisibility(View.GONE);
            mPrgLoading.setVisibility(View.GONE);
            mIbnDownload.setVisibility(View.VISIBLE);
        } else {
            vLoading.setVisibility(View.GONE);
            mPrgLoading.setVisibility(View.GONE);
            mIbnDownload.setVisibility(View.GONE);
        }
        fileName.setText(message.getFileName());
        fileSize.setText(FileHelper.formatFileSize(message.getSize()));
        mIbnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMessageInteractionListener().fileContentClickCallBack(message, true);
            }
        });
    }

    private void drawFileType() {
        String fileType = FileHelper.getExtensionFile(message.getFileName());
        switch (fileType) {
            case "doc":
                Glide.with(mContext).load(R.drawable.ic_file_doc).into(iconAttachment);
                break;
            case "docx":
                Glide.with(mContext).load(R.drawable.ic_file_doc).into(iconAttachment);
                break;
            case "mp3":
                Glide.with(mContext).load(R.drawable.ic_file_mp3).into(iconAttachment);
                break;
            case "wav":
                Glide.with(mContext).load(R.drawable.ic_file_mp3).into(iconAttachment);
                break;
            case "pdf":
                Glide.with(mContext).load(R.drawable.ic_file_pdf).into(iconAttachment);
                break;
            case "ppt":
                Glide.with(mContext).load(R.drawable.ic_file_ppt).into(iconAttachment);
                break;
            case "pptx":
                Glide.with(mContext).load(R.drawable.ic_file_ppt).into(iconAttachment);
                break;
            case "psd":
                Glide.with(mContext).load(R.drawable.ic_file_psd).into(iconAttachment);
                break;
            case "rar":
                Glide.with(mContext).load(R.drawable.ic_file_rar).into(iconAttachment);
                break;
            case "txt":
                Glide.with(mContext).load(R.drawable.ic_file_txt).into(iconAttachment);
                break;
            case "xls":
                Glide.with(mContext).load(R.drawable.ic_file_xls).into(iconAttachment);
                break;
            case "xlsx":
                Glide.with(mContext).load(R.drawable.ic_file_xls).into(iconAttachment);
                break;
            case "zip":
                Glide.with(mContext).load(R.drawable.ic_file_zip).into(iconAttachment);
                break;
            default:
                Glide.with(mContext).load(R.drawable.ic_file_other).into(iconAttachment);
                break;
        }
    }
}
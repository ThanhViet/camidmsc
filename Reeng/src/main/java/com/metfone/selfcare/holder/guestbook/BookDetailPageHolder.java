package com.metfone.selfcare.holder.guestbook;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.editor.sticker.utils.Constants;
import com.bumptech.glide.load.engine.GlideException;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.StrangerPhoneNumber;
import com.metfone.selfcare.database.model.guestbook.Page;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.images.ImageLoaderManager;
import com.metfone.selfcare.holder.BaseViewHolder;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.glide.GlideImageLoader;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

/**
 * Created by toanvk2 on 4/18/2017.
 */
public class BookDetailPageHolder extends BaseViewHolder implements View.OnClickListener {
    private Context mContext;
    private Page entry;
    private ApplicationController mApplication;
    private Resources mRes;
    private OnPageEditListener mListener;
    private ImageView mImgPreview;
    private TextView mTvwPageNumber;
    private TextView mTvwPageName;
    //private TextView mTvwPageStyle;
    private View mViewPageGuide;
    private View mViewPageEdit;
    private View mViewPageShare;
    private View mViewPageDelete;
    private View mViewAssigned;
    private ImageView mImgAssignedAdd;
    private View mViewAssignedAvatar;
    private CircleImageView mImgAssignedAvatar;
    private TextView mTvwAssignedAvatar;
    private TextView mTvwAssignedContent;
    private ProgressLoading mProgressLoading;
    private int screenWidth;

    public BookDetailPageHolder(View convertView, Context context, OnPageEditListener listener) {
        super(convertView);
        this.mContext = context;
        this.mApplication = (ApplicationController) context.getApplicationContext();
        this.mRes = mContext.getResources();
        this.mListener = listener;
        mImgPreview = (ImageView) convertView.findViewById(R.id.page_edit_image);
        mTvwPageNumber = (TextView) convertView.findViewById(R.id.page_edit_page_number);
        mTvwPageName = (TextView) convertView.findViewById(R.id.page_edit_name);
        mViewPageEdit = convertView.findViewById(R.id.page_edit_edit);
        mViewPageGuide = convertView.findViewById(R.id.page_edit_guide);
        mViewPageShare = convertView.findViewById(R.id.page_edit_share);
        mViewPageDelete = convertView.findViewById(R.id.page_edit_delete);
        //assigned
        mViewAssigned = convertView.findViewById(R.id.page_edit_assigned_layout);
        mImgAssignedAdd = (ImageView) convertView.findViewById(R.id.page_edit_assigned_add);
        mViewAssignedAvatar = convertView.findViewById(R.id.page_edit_assigned_avatar_layout);
        mImgAssignedAvatar = (CircleImageView) convertView.findViewById(R.id.page_edit_assigned_avatar);
        mTvwAssignedAvatar = (TextView) convertView.findViewById(R.id.page_edit_assigned_avatar_text);
        mTvwAssignedContent = (TextView) convertView.findViewById(R.id.page_edit_assigned_content);
        mProgressLoading = (ProgressLoading) convertView.findViewById(R.id.progress_loading_img);
        // set layout param
        screenWidth = mApplication.getWidthPixels();
        ViewGroup.LayoutParams parentParams = convertView.getLayoutParams();
        int itemHeight = (int) (screenWidth * Constants.SCALE_HAFL_HEIGHT);
        parentParams.height = itemHeight;
    }

    @Override
    public void setElement(Object obj) {
        entry = (Page) obj;
        if (entry.getPosition() <= 0) {
            mTvwPageNumber.setText(mRes.getString(R.string.guest_book_page_cover));
            mViewPageGuide.setVisibility(View.VISIBLE);
        } else {
            mViewPageGuide.setVisibility(View.GONE);
            mTvwPageNumber.setText(String.format(mRes.getString(R.string.guest_book_page_number), entry.getPosition()));
        }
        if (entry.getPosition() <= 1) {
            mViewPageEdit.setVisibility(View.VISIBLE);
        } else {
            mViewPageEdit.setVisibility(View.GONE);
        }
        mTvwPageName.setText(entry.getName());
        String url = UrlConfigHelper.getInstance(mContext).getConfigGuestBookUrl(entry.getPreview());
        ImageLoaderManager.getInstance(mContext).displayGuestBookPreview(mImgPreview, url, new GlideImageLoader.SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted() {
                mProgressLoading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(GlideException failReason) {
                mProgressLoading.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete() {
                mProgressLoading.setVisibility(View.GONE);
            }
        });
        drawAssigned();
        setViewListeners();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.page_edit_share:
                mListener.onShare(entry);
                break;
            case R.id.page_edit_delete:
                mListener.onDelete(entry);
                break;
            case R.id.page_edit_assigned_layout:
                if (TextUtils.isEmpty(entry.getAssigned())) {
                    mListener.onAssignFriend(entry);
                } else {
                    mListener.onAssignedAvatarClick(entry);
                }
                break;
            case R.id.page_edit_guide:
                mListener.onGuideClick();
                break;
            case R.id.page_edit_edit:
                mListener.onEditPage(entry);
                break;
        }
    }

    private void setViewListeners() {
        mViewPageGuide.setOnClickListener(this);
        mViewPageShare.setOnClickListener(this);
        mViewPageDelete.setOnClickListener(this);
        mViewAssigned.setOnClickListener(this);
        mViewPageEdit.setOnClickListener(this);
    }

    private void drawAssigned() {
        if (entry.getPosition() <= 1) {
            mViewAssigned.setVisibility(View.GONE);
        } else {
            mViewAssigned.setVisibility(View.VISIBLE);
        }
        String numberAssigned = entry.getAssigned();
        if (TextUtils.isEmpty(numberAssigned)) {
            mViewAssignedAvatar.setVisibility(View.GONE);
            mImgAssignedAdd.setVisibility(View.VISIBLE);
            mTvwAssignedContent.setText(mRes.getString(R.string.guest_book_assigned_add_label));
        } else {
            mViewAssignedAvatar.setVisibility(View.VISIBLE);
            mImgAssignedAdd.setVisibility(View.GONE);
            ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
            if (numberAssigned.equals(accountBusiness.getJidNumber())) {
                mApplication.getAvatarBusiness().setMyAvatar(mImgAssignedAvatar, accountBusiness.getCurrentAccount());
                mTvwAssignedAvatar.setVisibility(View.GONE);
                mTvwAssignedContent.setText(accountBusiness.getUserName());
            } else {
                int size = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
                ContactBusiness contactBusiness = mApplication.getContactBusiness();
                PhoneNumber phoneNumber = contactBusiness.getPhoneNumberFromNumber(numberAssigned);
                if (phoneNumber != null) {
                    mTvwAssignedContent.setText(phoneNumber.getName());
                    mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAssignedAvatar, mTvwAssignedAvatar, phoneNumber, size);
                } else {
                    mApplication.getAvatarBusiness().setUnknownNumberAvatar(mImgAssignedAvatar, mTvwAssignedAvatar, numberAssigned, size);
                    // name
                    StrangerPhoneNumber strangerPhoneNumber = mApplication.
                            getStrangerBusiness().getExistStrangerPhoneNumberFromNumber(numberAssigned);
                    if (strangerPhoneNumber != null) { // neu so co trong bang stranger thi lay ten o day
                        String name = strangerPhoneNumber.getFriendName();
                        if (TextUtils.isEmpty(name)) {
                            mTvwAssignedContent.setText(Utilities.hidenPhoneNumber(numberAssigned));
                        } else
                            mTvwAssignedContent.setText(strangerPhoneNumber.getFriendName());
                    } else {
                        mTvwAssignedContent.setText(numberAssigned);
                    }
                }
            }
        }
    }

    public interface OnPageEditListener {
        void onEditPage(Page page);

        void onGuideClick();

        void onShare(Page page);

        void onDelete(Page page);

        void onAssignFriend(Page page);

        void onAssignedAvatarClick(Page page);
    }
}

package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.ThreadMessage;

/**
 * Created by toanvk2 on 8/21/14.
 */
public interface ConfigGroupListener {
    void onConfigGroupChange(ThreadMessage threadMessage, int actionChange);

    void onConfigRoomChange(String roomId,boolean unFollow);
}

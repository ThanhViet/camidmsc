package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;

/**
 * Created by thanhnt72 on 1/6/2016.
 */
public interface FeedOnMediaListener {
    void notifyFeedOnMedia(boolean needNotify, boolean needToSetSelection);

    void onPostFeed(FeedModelOnMedia feed);
}

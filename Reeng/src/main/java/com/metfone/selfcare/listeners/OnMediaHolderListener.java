package com.metfone.selfcare.listeners;

import android.view.View;

import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.model.tab_video.Channel;

/**
 * Created by thanhnt72 on 7/2/2015.
 */
public interface OnMediaHolderListener {
    void onClickLikeFeed(FeedModelOnMedia feed);

    void onClickCommentFeed(FeedModelOnMedia feed);

    void onClickShareFeed(FeedModelOnMedia feed);

    void onClickUser(UserInfo userInfo);

    void onClickMediaItem(FeedModelOnMedia feed);

    void onClickImageItem(FeedModelOnMedia feed, int positionImage);

    void onClickMoreOption(FeedModelOnMedia feed);

    void onClickButtonTotal(View rowView, FeedModelOnMedia feed);

    void onLongClickStatus(FeedModelOnMedia feed);

    void onDeepLinkClick(FeedModelOnMedia feed, String link);

    void onClickSuggestFriend(UserInfo userInfo);

    void openChannelInfo(FeedModelOnMedia feed);

    void openPlayStore(FeedModelOnMedia feed, String packageName);

    void onSubscribeChannel(FeedModelOnMedia feed, Channel channel);
}

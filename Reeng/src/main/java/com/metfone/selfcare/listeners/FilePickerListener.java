package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.DocumentClass;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.helper.video.Video;

import java.util.ArrayList;

/**
 * Created by thaodv on 7/11/2014.
 */
public interface FilePickerListener {
    void transferPickedFile(String filePath,String fileName, String extension);

    void transferPickedPhoto(ArrayList<String> filePath);

    void transferTakenPhoto(String filePath);

    void transferSelectedContact(ArrayList<PhoneNumber> lstPhone);

    void transferSelectedDocument(DocumentClass document);

    void transferTakenVideo1(String filePath, String fileName, int duration, int fileSize, Video v);

    void transferShareLocation(String content, String latitude, String longitude);

    void sendVideo(String filePath, String fileName, int duration, int fileSize);
}
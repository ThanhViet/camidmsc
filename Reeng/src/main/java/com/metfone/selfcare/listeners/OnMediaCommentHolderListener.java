package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;

/**
 * Created by thanhnt72 on 1/12/2016.
 */
public interface OnMediaCommentHolderListener {

    void onAvatarClick(UserInfo userInfo);

    void onLongClickItem(FeedAction feedAction);

    void onClickReply(FeedAction feedAction, int pos, boolean needShowKeyboard);

    void onClickLike(FeedAction feedAction, int pos);

    void onClickListLike(FeedAction feedAction);
}

package com.metfone.selfcare.listeners;

/**
 * Created by ThanhNT on 2/5/2015.
 */
public interface KeyguardGoneListener {
    void onKeyguardGone();
}

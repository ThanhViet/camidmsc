package com.metfone.selfcare.listeners;


public interface OnClickLuckyGameBanner {
    void onGameTabClick();
    void onNewYearGameClick();
    void onLuckyWheelGameClick();
}

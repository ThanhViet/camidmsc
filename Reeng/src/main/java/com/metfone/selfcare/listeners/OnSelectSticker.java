package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.guestbook.Background;
import com.metfone.selfcare.database.model.guestbook.EmoItem;

/**
 * Created by toanvk2 on 4/13/2017.
 */
public interface OnSelectSticker {
    void onEmoItemSelected(EmoItem item);

    void onBackgroundItemSelected(Background background);
}

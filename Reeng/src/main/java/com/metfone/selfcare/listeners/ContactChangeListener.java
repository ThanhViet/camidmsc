package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.PhoneNumber;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 7/5/14.
 */
public interface ContactChangeListener {
    void onContactChange();

    void onPresenceChange(ArrayList<PhoneNumber> listNumber);

    void onRosterChange();

    void initListContactComplete(int sizeSupport);

}

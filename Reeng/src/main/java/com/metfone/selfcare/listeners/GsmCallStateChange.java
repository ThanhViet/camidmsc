package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 7/28/2016.
 */
public interface GsmCallStateChange {
    void onCallGsmStateChanged(int state, String incomingNumber);
}
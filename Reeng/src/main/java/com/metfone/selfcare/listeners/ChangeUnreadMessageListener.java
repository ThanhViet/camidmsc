package com.metfone.selfcare.listeners;

/**
 * Created by thanhnt72 on 3/23/2016.
 */
public interface ChangeUnreadMessageListener {
    void onChangeUnreadMessage(int numberUnreadMsg);
}

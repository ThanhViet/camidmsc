package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.StrangerSticky;

/**
 * Created by onsite_kt on 12/10/2015.
 */
public interface StrangerStickyInteractionListener {
    void onBannerClick(StrangerSticky strangerSticky);
}

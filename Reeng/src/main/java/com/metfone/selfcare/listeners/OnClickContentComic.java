/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.listeners;

public interface OnClickContentComic {
    void onClickComicItem(Object item, int position);

    void onClickMoreComicItem(Object item, int position);
}

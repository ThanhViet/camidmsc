package com.metfone.selfcare.listeners;

import com.metfone.selfcare.model.camid.OpenIdConfig;

import java.util.List;

/**
 * @author ITSOL JAPAN
 * Created on 7/1/2021.
 * Copyright � 2020 YSL Solution Co., Ltd. All rights reserved.
 **/
public interface ShareAndGetMoreContract {
    interface View extends IView<Presenter> {
        void onSuccess(List<OpenIdConfig> listConfigResponse);

        void onFail(String message);
    }

    interface Presenter extends IPresenter {
        void getListConfig();
    }
}

package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 2/24/2016.
 */
public interface VipInfoChangeListener {
    void onChange();

    void onMoreConfigChange();

    void onAVNOChange();
}

package com.metfone.selfcare.listeners;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;

/**
 * Created by toanvk2 on 3/9/2016.
 */
public interface AppLockStateListener {
    void onWentToBackground();

    void onWentToForeground(BaseSlidingFragmentActivity activity);

    void onActivityStarted(BaseSlidingFragmentActivity activity);
}

package com.metfone.selfcare.listeners;

public interface IView<T> {
    void setPresenter(T presenter);
}

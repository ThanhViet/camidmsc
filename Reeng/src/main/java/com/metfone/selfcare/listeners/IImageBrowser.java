package com.metfone.selfcare.listeners;

import com.metfone.selfcare.helper.images.ImageDirectory;
import com.metfone.selfcare.helper.images.ImageInfo;

/**
 * Created by vtsoft on 11/5/2014.
 */
public class IImageBrowser {
    public interface SelectImageDirectoryListener {
        void onSelectImageDirectory(ImageDirectory directory, int position);
    }

    public interface SelectImageListener {
        void onSelectImage(ImageDirectory director, ImageInfo image, int position);
    }

}

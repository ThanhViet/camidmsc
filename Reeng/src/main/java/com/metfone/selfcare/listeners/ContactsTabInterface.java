package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 8/31/2017.
 */

public interface ContactsTabInterface {
    void onPageStateVisible(boolean isVisible);

    void onTabReselected();

    void onSearchChange(String textSearch);
}

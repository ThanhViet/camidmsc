package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 11/15/2017.
 */

public interface ThreadListChangeListener {
    void onThreadsChanged();
}

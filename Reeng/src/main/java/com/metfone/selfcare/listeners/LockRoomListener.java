package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 05/11/2015.
 */
public interface LockRoomListener {
    void notifyLockRoom(int threadId, int status, long timeLock);

    void notifySpamStranger(int threadId,String msg);
}

package com.metfone.selfcare.listeners;

/**
 * Created by thaodv on 6/26/2014.
 */
public interface NetworkConnectivityChangeListener {
    void onConnectivityChanged(boolean isNetworkAvailable, int connectedType);
}

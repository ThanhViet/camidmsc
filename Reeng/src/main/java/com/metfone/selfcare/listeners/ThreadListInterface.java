package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 22/09/2015.
 */
public interface ThreadListInterface {

    void onAbDeleteClick();

    void onPageStateVisible(boolean isVisible, int prePos);

    void onTabReselected();

    void onMarkAllAsRead();
}
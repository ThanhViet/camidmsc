package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 3/15/2017.
 */
public interface IMConnection {
    void onConnected();
}

package com.metfone.selfcare.listeners;

/**
 * Created by huybq7 on 9/18/2014.
 */
public class ICusKeyboard {

    public interface ChangeNoMessageViewSizeListener {
        void onChangeNoMessageViewSize(int sizeViewNoMessage);
    }

    public interface ChangeListMessageSizeListener {
        void onChangeListMessageSize(int sizeListMessage);
    }

    public interface OpenCusKeyboarListener {
        void onOpenCusKeyboard();

        void onClickCusKeyboard();
    }

    public interface CloseCusKeyboarListener {
        void onCloseCusKeyboard();
    }

    public interface SendReengClickListener {
        void onSendReengClick();

        void onSendReengKeyboardClick();

        void onSendVoicemail(String filePath, int elapsedTimeSecs, String fileName);

        void onSendImage(String filePath);

        void onOpenPreviewMedia();

        void onClearReplyClick();

        void onChatBarClickBottomAction(int action);

        void onChatBarClick();

        void onSwitchSendButtonComplete(boolean isSendSms);
    }
}
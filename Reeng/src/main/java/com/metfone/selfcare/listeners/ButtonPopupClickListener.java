package com.metfone.selfcare.listeners;

/**
 * Created by sonnn00 on 7/18/2017.
 */

public interface ButtonPopupClickListener {
     void onAcceptClick(Object obj);

     void onCancerClick();
}

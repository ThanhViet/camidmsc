package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.network.xmpp.XMPPResponseCode;

import java.util.List;

public interface ReengMessageListener {
    void notifyNewIncomingMessage(ReengMessage message, ThreadMessage thread);

    void onRefreshMessage(int threadId);

    void notifyNewOutgoingMessage();

    void onGSMSendMessageError(ReengMessage reengMessage, XMPPResponseCode responseCode);

    void onNonReengResponse(int threadId, ReengMessage reengMessage, boolean showAlert, String msgError);

    void notifyMessageSentSuccessfully(int threadId);

    void onUpdateStateTyping(String phoneNumber, ThreadMessage thread);

    void onSendMessagesError(List<ReengMessage> reengMessageList);

    void onUpdateMediaDetail(MediaModel mediaModel, int threadId);

    void onUpdateStateAcceptStranger(String friendJid);

    void onUpdateStateRoom();

    void onBannerDeepLinkUpdate(ReengMessage message, ThreadMessage mCorrespondingThread);
}
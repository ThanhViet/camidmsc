package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.ThreadMessage;

/**
 * Created by thanhnt72 on 6/28/2018.
 */

public interface ContactListInterface  {
    void addNewContactActivity();

    void editContactActivity(String contactId);

    void navigateToThreadDetail(ThreadMessage threadMessage);

    void navigateToAddFavarite();

    void navigateToInviteFriendActivity(String number);
}

package com.metfone.selfcare.listeners;

/**
 * Created by thaodv on 7/11/2014.
 */
public interface ThreadDetailSettingListener {
    void transferGroupAvatar(String filePath);
}
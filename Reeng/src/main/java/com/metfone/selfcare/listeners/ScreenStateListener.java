package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 2/4/15.
 */
public interface ScreenStateListener {
    void onScreenOn();

    void onScreenOff();
}

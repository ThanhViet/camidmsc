package com.metfone.selfcare.listeners;

import com.metfone.selfcare.ui.SwipeLayout;
import com.metfone.selfcare.util.Attributes;

import java.util.List;

/**
 * Created by namnh40 on 6/5/2015.
 */
public interface SwipeManagerInterface {

    void openItem(int position);

    void closeItem(int position);

    void closeAllExcept(SwipeLayout layout);

    void closeAllItems();

    List<Integer> getOpenItems();

    List<SwipeLayout> getOpenLayouts();

    void removeShownLayouts(SwipeLayout layout);

    boolean isOpen(int position);

    Attributes.Mode getMode();

    void setMode(Attributes.Mode mode);
}


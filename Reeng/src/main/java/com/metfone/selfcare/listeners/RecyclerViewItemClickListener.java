package com.metfone.selfcare.listeners;

/**
 * Created by sonnn00 on 7/17/2017.
 */

public interface RecyclerViewItemClickListener {
    void onClick(Object obj);

    void onLongClick(Object obj);
    void onAvatarClick(Object obj);
}

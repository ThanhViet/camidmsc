package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.StrangerMusic;

/**
 * Created by toanvk2 on 06/05/2015.
 */
public interface StrangerMusicInteractionListener {
    void onClickPoster(StrangerMusic strangerMusic);

    void onClickAcceptor(StrangerMusic strangerMusic);

    void onClickAccept(StrangerMusic strangerMusic);

    void onClickReinvite(StrangerMusic strangerMusic);

    void onClickListen(StrangerMusic strangerAround);
}
package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.UserInfo;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedAction;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedContent;
import com.metfone.selfcare.database.model.onmedia.RestAllFeedsModel;
import com.metfone.selfcare.database.model.onmedia.RestAllNotifyModel;
import com.metfone.selfcare.database.model.onmedia.SieuHaiModel;
import com.metfone.selfcare.model.tab_video.Video;

import java.util.ArrayList;

/**
 * Created by thanhnt72 on 3/24/2016.
 */
public interface OnMediaInterfaceListener {
    void navigateToWebView(FeedModelOnMedia feed);

    void navigateToVideoView(FeedModelOnMedia feed);

    void navigateToComment(FeedModelOnMedia feed);

    void navigateToAlbum(FeedModelOnMedia feed);

    void navigateToMovieView(FeedModelOnMedia feed);

    void navigateToProcessLink(FeedModelOnMedia feed);

//    void navigateToWriteStatus(FeedModelOnMedia feed, boolean isEdit);

    void navigateToListShare(String url);

    interface GetListFeedListener {
        void onGetListFeedDone(RestAllFeedsModel restAllFeedsModel);
    }

    interface GetListFeedActionListener {
        void onGetListFeedAction(RestAllFeedAction restAllFeedAction);
    }

    interface GetListFeedContentListener {
        void onGetListFeedContent(RestAllFeedContent restAllFeedContent);
    }

    interface GetListNotifyListener {
        void onGetListNotifyDone(RestAllNotifyModel restAllNotifyModel);
    }

    interface GetListVideoSieuHai {
        void onGetListVideoDone(ArrayList<SieuHaiModel> listVideo);

        void onError(int code, String des);
    }

    interface OnClickItemSieuHai {
        void onClickVideoSieuHai(Video sieuHaiModel, int position);
    }

    interface GetListSuggestFriend {
        void onGetListSuggestFriendDone(ArrayList<UserInfo> listUser, String title);

        void onError(int code, String des);
    }

    interface GetListComment {
        void onGetListCommentSuccess(ArrayList<FeedAction> listComment, ArrayList<UserInfo> listUserLike,
                                     FeedAction commentParent, long timeServer);

        void onGetListCommentError(int code, String des);
    }

}

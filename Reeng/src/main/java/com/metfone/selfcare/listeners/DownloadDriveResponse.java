package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 11/30/2017.
 */

public interface DownloadDriveResponse {
    void onResult(String fileName, String filePath);

    void onFail(boolean isMaxSize);
}

package com.metfone.selfcare.listeners;

/**
 * Created by thanhnt72 on 4/23/2019.
 */

public interface LoginFromAnonymousListener {
    void onLoginSuccess();
}

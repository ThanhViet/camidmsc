package com.metfone.selfcare.listeners;

import android.view.View;

import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.MediaModel;
import com.metfone.selfcare.database.model.ReengMessage;
import com.metfone.selfcare.database.model.StickerItem;
import com.metfone.selfcare.database.model.message.ReplyMessage;

/**
 * Created by thaodv on 7/15/2014.
 */
public interface MessageInteractionListener {
    void longClickBgrCallback(ReengMessage message);

    void retryClickCallBack(ReengMessage message);

    void replyClickCallBack(ReengMessage message);

    void textContentClickCallBack(ReengMessage message);

    void fileContentClickCallBack(ReengMessage message, boolean isClickDownload);

    void imageContentClickCallBack(ReengMessage message);

    void voicemailContentClickCallBack(ReengMessage message);

    void shareContactClickCallBack(ReengMessage reengMessage);

    void voiceStickerClickCallBack(ReengMessage reengMessage, View convertView);

    void gifContentClickCallBack(ReengMessage reengMessage);

    void onMyAvatarClick();

    void onFriendAvatarClick(String friendJid, String friendName);

    void onAcceptInviteMusicClick(ReengMessage reengMessage);

    void onCancelInviteMusicClick(ReengMessage reengMessage);

    void onReinviteShareMusicClick(ReengMessage reengMessage);

    void onTryPlayMusicClick(ReengMessage reengMessage);

    void onInviteMusicViaFBClick(ReengMessage reengMessage);

    void videoContentClickCallBack(ReengMessage message, View convertView);

    void onGreetingStickerPreviewCallBack(StickerItem stickerItem);

    void shareLocationClickCallBack(ReengMessage reengMessage);

    void onInfoMessageCallBack();

    void onAcceptMusicGroupClick(ReengMessage reengMessage);

    void onFollowRoom(ReengMessage reengMessage);

    void reportClickCallBack(ReengMessage message);

    void onAcceptCrbtGift(ReengMessage message, MediaModel songModel);

    void onSendCrbtGift(ReengMessage message, MediaModel songModel);

    void onDeepLinkClick(ReengMessage message, String link);

    void onFakeMoClick(ReengMessage message);

    void onPollDetail(ReengMessage message, boolean pollUpdate);

    void onCall(ReengMessage message);

    void onLuckyWheelHelpClick(ReengMessage message);

    void onWatchVideoClick(ReengMessage message, boolean isUser);

    void onBplus(ReengMessage message);

    void onClickPreviewUrl(ReengMessage message, String url);

    void onClickOpenGiftLixi(ReengMessage message);

    void onClickReplyLixi(ReengMessage message);

    void onSmartTextClick(String content, int type);

    void onClickImageReply(ReplyMessage replyMessage);

    void onClickActionChangeNumber(ReengMessage message, ReengMessageConstant.ActionChangeNumber actionChangeNumber);

    void onOpenViewReaction(View anchorView, ReengMessage reengMessage);

    void onOpenListReaction(ReengMessage reengMessage, View viewReact);

    void onClickReaction(ReengMessage reengMessage, View viewReact);


    public interface LoadUnknownMessageAndReloadAllThread {
        void onStartLoadData();

        void onLoadAllDone();
    }
}
package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedContent;

/**
 * Created by sonnn00 on 10/5/2017.
 */

public interface SieuHaiNextVideoHolderListener {
    void onClickItem(FeedContent feed);
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.listeners;

public interface OnClickContentMusic {
    void onClickMusicItem(Object item, int position);

    void onClickMoreMusicItem(Object item, int position);
}

package com.metfone.selfcare.listeners;

/**
 * Created by thaodv on 7/10/2014.
 */
public interface SmsReceiverListener {
    void onSmsPasswordReceived(String password);

    void onSmsPasswordLixiReceived(String password);

    boolean onSmsChatReceived(String smsSender, String smsContent);
}

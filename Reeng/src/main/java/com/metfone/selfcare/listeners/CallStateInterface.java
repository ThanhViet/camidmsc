package com.metfone.selfcare.listeners;

import com.stringee.StringeeStream;

/**
 * Created by thanhnt72 on 7/25/2016.
 */
public interface CallStateInterface {
    void onRinging();

    void onConnectChange();

    void onCallEnd();

    void onCallBusy();

    void onNotSupport(String msg, boolean isToast);

    void onVideoStateChange();

    //timeCall seconds
    void onTickTimeConnect(int timeCall);

    void onSpeakerChanged(boolean isDisable);

    void onQualityReported(long bandwidth);

    void onLocalStreamCreated(StringeeStream stringeeStream);

    void onAddStream(StringeeStream stringeeStream);
}
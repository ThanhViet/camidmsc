package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 10/13/2016.
 */

public interface HomeContactsInterface {
    void onPageStateVisible(boolean isVisible);

    boolean checkCloseSearch();

    void setCurrentPage(int page);
}

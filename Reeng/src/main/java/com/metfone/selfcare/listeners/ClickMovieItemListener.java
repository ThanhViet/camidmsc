package com.metfone.selfcare.listeners;

import com.metfone.selfcare.model.tabMovie.Kind;

/**
 * Created by thanhnt72 on 12/15/2018.
 */

public interface ClickMovieItemListener {
    void onClickMovieItemListener(Kind kind);
}

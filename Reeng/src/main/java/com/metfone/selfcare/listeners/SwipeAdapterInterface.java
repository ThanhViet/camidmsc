package com.metfone.selfcare.listeners;

/**
 * Created by namnh40 on 6/5/2015.
 */
public interface SwipeAdapterInterface {

    int getSwipeLayoutResourceId(int position);

    void notifyDatasetChanged();

}

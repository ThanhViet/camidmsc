package com.metfone.selfcare.listeners;

/**
 * Created by namnh40 on 11/3/2015.
 */
public interface OnEditProfileListener {
    void takeAPhoto(int typeUpload);

    void openGallery(int typeUpload);

    void onEditSuccess(boolean isSuccess);

    void goToHome();

    void getInfoFacebook();
}

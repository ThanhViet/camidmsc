package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 10/6/2017.
 */

public interface ContactsHolderCallBack {
    void onAudioCall(Object entry);

    void onVideoCall(Object entry);

    void onSectionClick(Object entry);

    void onItemClick(Object entry);

    void onAvatarClick(Object entry);

    void onItemLongClick(Object entry);

    void onActionLabel(Object entry);

    void onSocialCancel(Object entry);
}

package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 7/17/14.
 */
public interface ProfileListener {
    void onProfileChange();

    void onRequestFacebookChange(String fullName, String birthDay, int gender);

    void onAvatarChange(String avatarPath);

    void onCoverChange(String coverPath);

}

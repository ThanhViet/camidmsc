package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 06/16/2015.
 */
public interface OfficerAccountChangeListener {
    void onStickyOfficerChange();
}
/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/12/2
 *
 */

package com.metfone.selfcare.listeners;

public interface OnClickContentVideo {
    void onClickVideoItem(Object item, int position);

    void onClickMoreVideoItem(Object item, int position);

    void onClickChannelVideoItem(Object item, int position);
}

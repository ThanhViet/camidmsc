package com.metfone.selfcare.listeners;

/**
 * Created by thanhnt72 on 4/9/2018.
 */

public interface ChargeMoneyAVNOListener {
    public void onChargeMoneySuccess();

    public void onChargeError(String msg);
}

package com.metfone.selfcare.listeners;

public interface OnClickGameBanner {
    void onGameItemClicked();
}

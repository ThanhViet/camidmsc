package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 11/18/14.
 */
public interface EventAppListener {
    void notifyForceUpdate(String msg, String link, boolean isForce);

    void notifyAuthenConflict();
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/30
 *
 */

package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.ReengMessage;

public interface ForwardMessageListener {
    void onForwardText(ReengMessage reengMessage);

    void onForwardImage(ReengMessage reengMessage);

    void onForwardVideo(ReengMessage reengMessage);

    void onForwardVoiceMail(ReengMessage reengMessage);

    void onForwardVoiceSticker(ReengMessage reengMessage);

    void onForwardContact(ReengMessage reengMessage);

    void onForwardLocation(ReengMessage reengMessage);

    void onForwardFile(ReengMessage reengMessage);
}

package com.metfone.selfcare.listeners;

/**
 * Created by toanvk2 on 10/6/2017.
 */

public class SimpleContactsHolderListener implements ContactsHolderCallBack {
    @Override
    public void onAudioCall(Object entry) {

    }

    @Override
    public void onVideoCall(Object entry) {

    }

    @Override
    public void onSectionClick(Object entry) {

    }

    @Override
    public void onItemClick(Object entry) {

    }

    @Override
    public void onAvatarClick(Object entry) {

    }

    @Override
    public void onItemLongClick(Object entry) {

    }

    @Override
    public void onActionLabel(Object entry) {

    }

    @Override
    public void onSocialCancel(Object entry) {

    }

}

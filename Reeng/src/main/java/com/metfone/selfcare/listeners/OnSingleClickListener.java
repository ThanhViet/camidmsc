/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/21
 *
 */

package com.metfone.selfcare.listeners;

import android.os.SystemClock;
import android.view.View;

public abstract class OnSingleClickListener implements View.OnClickListener {
    private static final long MIN_CLICK_INTERVAL = 800;
    private long lastClickTime;

    public abstract void onSingleClick(View view);

    @Override
    public final void onClick(View view) {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - lastClickTime;
        lastClickTime = currentClickTime;
        if (elapsedTime < MIN_CLICK_INTERVAL)
            return;
        onSingleClick(view);
    }
}
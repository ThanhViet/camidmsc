package com.metfone.selfcare.listeners;

public interface IFingerListener {
    void getFingerprintSuccess();
    void getFingerprintError();
}

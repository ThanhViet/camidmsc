package com.metfone.selfcare.listeners;

/**
 * Created by thanhnt72 on 11/29/2018.
 */

public interface SimpleResponseListener {
    void onSuccess(String response);

    void onError(int code, String msg);
}

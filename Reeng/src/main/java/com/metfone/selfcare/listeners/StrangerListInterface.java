package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.StrangerMusic;

import java.util.ArrayList;

/**
 * Created by toanvk2 on 22/09/2015.
 */
public interface StrangerListInterface {

    void onChangeStrangerList(ArrayList<StrangerMusic> strangers, boolean isConfide);

    void onPageStateVisible(boolean isVisible,int prePos);

    void onTabReselected();
}
package com.metfone.selfcare.listeners;

/**
 * Created by thaodv on 8/5/2014.
 */
public interface XMPPConnectivityChangeListener {
    void onXMPPConnected();
    void onXMPPDisconnected();
    void onXMPPConnecting();
}

package com.metfone.selfcare.listeners;

/**
 * Created by thanhnt72 on 5/19/2018.
 */

public interface UpdateMessageListener {
    void onUpdateMessageDone();
}

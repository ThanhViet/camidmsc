package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.ImageProfile;

import java.util.ArrayList;

/**
 * Created by namnh40 on 11/6/2015.
 */
public interface UploadImageListener {

    void onUploadSuccess(ImageProfile image);

    void onUploadFail(ImageProfile image);

    void onUploadEnd(int type, ArrayList<ImageProfile> imageProfiles, String message);

    void onStartUpload();
}

package com.metfone.selfcare.listeners;

/**
 * Created by toanvk on 7/25/2014.
 */
public interface InitDataListener {
    void onDataReady();
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/9/12
 *
 */

package com.metfone.selfcare.listeners;

import com.metfone.selfcare.database.model.ReengMessage;

public interface SendNewMessageListener {
    void onSendText(ReengMessage reengMessage);

    void onSendImage(ReengMessage reengMessage);

    void onSendVideo(ReengMessage reengMessage);

    void onSendVoiceMail(ReengMessage reengMessage);

    void onSendVoiceSticker(ReengMessage reengMessage);

    void onSendContact(ReengMessage reengMessage);

    void onSendLocation(ReengMessage reengMessage);

    void onSendFile(ReengMessage reengMessage);
}

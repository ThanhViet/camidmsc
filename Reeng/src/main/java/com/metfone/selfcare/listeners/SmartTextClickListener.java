package com.metfone.selfcare.listeners;

/**
 * Created by toanvk on 7/13/2014.
 */
public interface SmartTextClickListener {
    void onSmartTextClick(String content, int type);
}

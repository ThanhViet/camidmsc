package com.metfone.selfcare.module.movienew.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryModel {
    String id;
    int parentid;
    String categoryname;
    String url;
    String categoryid;
    String type;
    String order;
    String status;
    String is_active;
    String description;
    String url_images;
    String date_set;
    String settop;
}

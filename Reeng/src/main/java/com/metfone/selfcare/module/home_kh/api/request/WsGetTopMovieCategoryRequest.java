package com.metfone.selfcare.module.home_kh.api.request;

import com.blankj.utilcode.util.LanguageUtils;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetTopMovieCategoryRequest extends BaseRequest<WsGetTopMovieCategoryRequest.Request> {
    public static class Request{
        public String clientType;
        public String platform;
        public String language;
        public String revision;
        public String limit;
        public String offset;
        @SerializedName("ref_id")
        public String refID;
        public String languageCode;
        public String loginStatus;
        public String uuid;
        public String security;
        public String countryCode;
        public String domain;
        public String msisdn;
        public String vip;
        public String timestamp;
        public String categoryid;

        public Request(){
            categoryid = "868459";
            limit = "1000";
            offset = "0";
        }
    }
}

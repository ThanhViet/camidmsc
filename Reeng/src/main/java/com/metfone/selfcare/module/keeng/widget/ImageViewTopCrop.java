package com.metfone.selfcare.module.keeng.widget;

import android.content.Context;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by HaiKE on 7/10/17.
 */

public class ImageViewTopCrop extends ImageView {

    public ImageViewTopCrop(Context context)
    {
        super(context);
        setScaleType(ScaleType.MATRIX);
    }

    public ImageViewTopCrop(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setScaleType(ScaleType.MATRIX);
    }

    public ImageViewTopCrop(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setScaleType(ScaleType.MATRIX);
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b)
    {
        if(getDrawable() != null)
        {
            Matrix matrix = getImageMatrix();
            float scaleFactor = getWidth()/(float)getDrawable().getIntrinsicWidth();
            matrix.setScale(scaleFactor, scaleFactor, 0, 0);
            setImageMatrix(matrix);
        }

        return super.setFrame(l, t, r, b);

//        if(getDrawable() != null)
//        {
//            Matrix matrix = getImageMatrix();
//
//            float scale;
//            int viewWidth = getWidth() - getPaddingLeft() - getPaddingRight();
//            int viewHeight = getHeight() - getPaddingTop() - getPaddingBottom();
//            int drawableWidth = getDrawable().getIntrinsicWidth();
//            int drawableHeight = getDrawable().getIntrinsicHeight();
//
//            if (drawableWidth * viewHeight > drawableHeight * viewWidth) {
//                scale = (float) viewHeight / (float) drawableHeight;
//            } else {
//                scale = (float) viewWidth / (float) drawableWidth;
//            }
//
//            matrix.setScale(scale, scale);
//            setImageMatrix(matrix);
//        }
//        else
//        {
//            setBackgroundResource(R.drawable.df_cover);
//        }
//
//        return super.setFrame(l, t, r, b);
    }

}
package com.metfone.selfcare.module.keeng.utils;

import com.android.volley.VolleyError;
import com.metfone.selfcare.BuildConfig;

public class Log {
    private static final boolean ENABLE_LOG = BuildConfig.DEBUG;
    private static final String LOG_TAG = "Keeng -> ";

    public static void i(String message) {
        i(LOG_TAG, message);
    }

    public static void d(String message) {
        d(LOG_TAG, message);
    }

    public static void e(String message) {
        e(LOG_TAG, message);
    }

    public static void i(String tag, String message) {
        if (ENABLE_LOG) {
            android.util.Log.i(tag, LOG_TAG + message);
        }
    }

    public static void i(String tag, String message, Throwable throwable) {
        if (ENABLE_LOG && throwable != null) {
            android.util.Log.i(tag, LOG_TAG + message, throwable);
        }
    }

    public static void e(String tag, String message) {
        if (ENABLE_LOG) {
            android.util.Log.e(tag, LOG_TAG + message);
        }
    }

    // chi dung log nay cho response error
    public static void e(String tag, VolleyError error) {
        if (error != null) {
            e(tag, LOG_TAG + "onErrorResponse: " + error.getMessage());
        }
    }

    public static void e(String tag, Throwable e) {
        if (ENABLE_LOG && e != null) {
            android.util.Log.e(tag, LOG_TAG + e.getMessage(), e);
        }
    }

    public static void e(String tag, String message, Throwable e) {
        if (ENABLE_LOG && e != null) {
            android.util.Log.e(tag, LOG_TAG + message, e);
        }
    }

    public static void d(String tag, String message) {
        if (ENABLE_LOG) {
            android.util.Log.d(tag, LOG_TAG + message);
        }
    }

    public static void d(String tag, String message, Throwable throwable) {
        if (ENABLE_LOG && throwable != null) {
            android.util.Log.d(tag, LOG_TAG + message, throwable);
        }
    }

    public static void v(String tag, String message) {
        if (ENABLE_LOG) {
            android.util.Log.v(tag, LOG_TAG + message);
        }
    }

    public static void v(String tag, String message, Throwable throwable) {
        if (ENABLE_LOG && throwable != null) {
            android.util.Log.v(tag, LOG_TAG + message, throwable);
        }
    }

    public static void w(String tag, String message) {
        if (ENABLE_LOG) {
            android.util.Log.w(tag, LOG_TAG + message);
        }
    }

    public static void w(String tag, String message, Throwable throwable) {
        if (ENABLE_LOG && throwable != null) {
            android.util.Log.w(tag, LOG_TAG + message, throwable);
        }
    }
}
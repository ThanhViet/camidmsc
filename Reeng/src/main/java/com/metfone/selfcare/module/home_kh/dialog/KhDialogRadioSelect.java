package com.metfone.selfcare.module.home_kh.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

public class KhDialogRadioSelect extends BaseDialogKhFragment {

    public static final int AUTO_BACKUP_TYPE = 0;
    public static final int BACKUP_VIA_NETWORK_TYPE = 1;
    public static final int TURN_OFF_NOTIFICATION_TYPE = 2;
    public static final int TIME_LOCK_APP_TYPE = 3;
    public static final int FONT_SIZE_TYPE = 4;

    private static final String TYPE_KEY = "type_key";
    private static final String ID_CANCEK_KEY = "text_cancel_type_key";
    private static final String VALUE_KEY = "value_key";
    private static final String TITLE_KEY = "title_key";
    public static final String TAG = "Dialog radio select ";

    private int type;
    private int valueDefault;
    private AppCompatRadioButton radioOne, radioTwo, radioThree, radioFour, radioFive;
    private RadioGroup radioGroup;

    public static KhDialogRadioSelect newInstance(int type, int valueDefault, int idTitle) {
        Bundle args = new Bundle();
        args.putInt(TYPE_KEY, type);
        args.putInt(VALUE_KEY, valueDefault);
        args.putInt(TITLE_KEY, idTitle);
        KhDialogRadioSelect fragment = new KhDialogRadioSelect();
        fragment.setArguments(args);
        return fragment;
    }

    public static KhDialogRadioSelect newInstance(int type, int valueDefault, int idTitle, int idCancel) {
        Bundle args = new Bundle();
        args.putInt(TYPE_KEY, type);
        args.putInt(VALUE_KEY, valueDefault);
        args.putInt(TITLE_KEY, idTitle);
        args.putInt(ID_CANCEK_KEY, idCancel);
        KhDialogRadioSelect fragment = new KhDialogRadioSelect();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt(TYPE_KEY);
        valueDefault = getArguments().getInt(VALUE_KEY);
    }

    @Override
    protected int getResId() {
        return R.layout.dialog_radio_select_kh;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setDefault();
    }


    @Override
    protected void initView() {
        super.initView();
        if (getArguments() != null && getArguments().containsKey(ID_CANCEK_KEY)) {
            if (btnLeft != null) {
                btnLeft.setText(getArguments().getInt(ID_CANCEK_KEY));
            }
        }
        radioGroup = getView().findViewById(R.id.radioGroup);
        radioOne = getView().findViewById(R.id.radioOne);
        radioTwo = getView().findViewById(R.id.radioTwo);
        radioThree = getView().findViewById(R.id.radioThree);
        radioFour = getView().findViewById(R.id.radioFour);
        radioFive = getView().findViewById(R.id.radioFive);
        tvTitle.setText(getArguments().getInt(TITLE_KEY));
        if (type == AUTO_BACKUP_TYPE) {
            radioFive.setVisibility(View.GONE);
            radioOne.setText(R.string.off);
            radioTwo.setText(R.string.kh_setting_backup_daily);
            radioThree.setText(R.string.kh_setting_backup_weekly);
            radioFour.setText(R.string.kh_setting_backup_monthly);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) radioFour.getLayoutParams();
            layoutParams.bottomMargin = Utilities.dpToPixel(R.dimen.margin_5, getResources());
        } else if (type == BACKUP_VIA_NETWORK_TYPE) {
            radioThree.setVisibility(View.GONE);
            radioFour.setVisibility(View.GONE);
            radioFive.setVisibility(View.GONE);
            radioOne.setText(R.string.kh_setting_backup_wifi);
            radioTwo.setText(R.string.kh_setting_backup_wifi_or_mobile_network);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) radioTwo.getLayoutParams();
            layoutParams.bottomMargin = Utilities.dpToPixel(R.dimen.margin_5, getResources());
        } else if (type == TIME_LOCK_APP_TYPE) {
            radioOne.setText(R.string.lock_app_option_now);
            radioTwo.setText(R.string.lock_app_option_5_seconds);
            radioThree.setText(R.string.lock_app_option_10_seconds);
            radioFour.setText(R.string.lock_app_option_30_seconds);
            radioFive.setText(R.string.lock_app_option_minute);
        } else if (type == FONT_SIZE_TYPE) {
            radioFour.setVisibility(View.GONE);
            radioFive.setVisibility(View.GONE);
            radioOne.setText(R.string.setting_font_size_small);
            radioTwo.setText(R.string.setting_font_size_medium);
            radioThree.setText(R.string.setting_font_size_large);
        } else {
            radioFive.setVisibility(View.GONE);
        }
        btnRight.setOnClickListener(view -> {
            if (selectListener != null) {
                selectListener.dialogRightClick(getValueByIdRadio(radioGroup.getCheckedRadioButtonId()));
                getDialog().cancel();
                selectListener = null;
            }
        });
    }

    private int getValueByIdRadio(int id) {
        switch (id) {
            case R.id.radioTwo:
                return 1;
            case R.id.radioThree:
                return 2;
            case R.id.radioFour:
                return 3;
            case R.id.radioFive:
                return 4;
            default:
                return 0;
        }
    }

    private void setDefault() {
        switch (valueDefault) {
            case 0:
                radioOne.setChecked(true);
                return;
            case 1:
                radioTwo.setChecked(true);
                return;
            case 2:
                radioThree.setChecked(true);
                return;
            case 3:
                radioFour.setChecked(true);
                return;
            case 4:
                radioFive.setChecked(true);
                return;
        }
    }

}

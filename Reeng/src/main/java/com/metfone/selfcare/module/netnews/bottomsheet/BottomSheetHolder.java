package com.metfone.selfcare.module.netnews.bottomsheet;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.netnews.event.FontSizeEvent;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.BottomSheetModel;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;

import org.greenrobot.eventbus.EventBus;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by HaiKE on 9/15/17.
 */

public class BottomSheetHolder extends BaseHolder {
    TextView tvTitle;
    ImageView icon;
    SegmentedGroup segmentedGroup;
    RadioButton rbSmall;
    RadioButton rbNormal;
    RadioButton rbLarge;
    BottomSheetModel item;
    AbsInterface.OnClickBottomSheet mListener;

    public BottomSheetHolder(final View itemView, AbsInterface.OnClickBottomSheet listener) {
        super(itemView);
        this.mListener = listener;
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        icon = (ImageView) itemView.findViewById(R.id.icon);
        segmentedGroup = (SegmentedGroup) itemView.findViewById(R.id.segmentedGroup);
        rbSmall = itemView.findViewById(R.id.rbSmall);
        rbNormal = itemView.findViewById(R.id.rbNormal);
        rbLarge = itemView.findViewById(R.id.rbLarge);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onClickSheet(item);
            }
        });
    }

    public void bind(BottomSheetModel item) {
        this.item = item;
        if (item.getResTitle() > 0)
            tvTitle.setText(item.getResTitle());
        else
            tvTitle.setText(item.getTitle());
        icon.setImageResource(item.getResIcon());
        int fontType = AppStateProvider.getInstance().getFontSize();
        if(fontType == CommonUtils.FONT_SIZE_SMALL)
        {
            rbSmall.setChecked(true);
        }
        else if(fontType == CommonUtils.FONT_SIZE_NORMAL)
        {
            rbNormal.setChecked(true);
        }
        else if(fontType == CommonUtils.FONT_SIZE_LARGE)
        {
            rbLarge.setChecked(true);
        }
        if(item.isHaveSegment())
        {
            segmentedGroup.setVisibility(View.VISIBLE);
            segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if(checkedId == R.id.rbSmall)
                    {
                        EventBus.getDefault().post(new FontSizeEvent(CommonUtils.FONT_SIZE_SMALL));
                    }
                    else if(checkedId == R.id.rbNormal)
                    {
                        EventBus.getDefault().post(new FontSizeEvent(CommonUtils.FONT_SIZE_NORMAL));
                    }
                    else if(checkedId == R.id.rbLarge)
                    {
                        EventBus.getDefault().post(new FontSizeEvent(CommonUtils.FONT_SIZE_LARGE));
                    }
                }
            });
        }
        else
        {
            segmentedGroup.setVisibility(View.GONE);
        }
    }
}

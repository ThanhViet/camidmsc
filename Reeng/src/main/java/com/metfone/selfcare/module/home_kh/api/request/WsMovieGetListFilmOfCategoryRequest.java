package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsMovieGetListFilmOfCategoryRequest extends BaseRequest<WsMovieGetListFilmOfCategoryRequest.Request> {
    public static class Request {
        @SerializedName("ref_id")
        private String refID;
        private String platform;
        private String language;
        private String revision;
        private String clientType;
        private String msisdn;
        private String categoryid;

        public String getRefID() { return refID; }
        public void setRefID(String value) { this.refID = value; }

        public String getPlatform() { return platform; }
        public void setPlatform(String value) { this.platform = value; }

        public String getLanguage() { return language; }
        public void setLanguage(String value) { this.language = value; }

        public String getRevision() { return revision; }
        public void setRevision(String value) { this.revision = value; }

        public String getClientType() { return clientType; }
        public void setClientType(String value) { this.clientType = value; }

        public String getMsisdn() { return msisdn; }
        public void setMsisdn(String value) { this.msisdn = value; }

        public String getCategoryid() { return categoryid; }
        public void setCategoryid(String value) { this.categoryid = value; }
    }

}

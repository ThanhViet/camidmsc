package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.AuthKeyModel;

public class CheckUserGamePermissionResponse {
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("wsResponse")
    @Expose
    UserGamePermission permission;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserGamePermission getPermission() {
        return permission;
    }

    public void setPermission(UserGamePermission permission) {
        this.permission = permission;
    }
}


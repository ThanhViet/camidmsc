package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RestSCPackage extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private RestSCPackageList data;

    public RestSCPackageList getData() {
        return data;
    }

    public void setData(RestSCPackageList data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCPackage [data=" + data + "] errror " + getErrorCode();
    }
}

package com.metfone.selfcare.module.keeng.event;

/**
 * Created by viettel_media on 11/27/17.
 */

public class TabMusicEvent {
    boolean isBackFromMovie;
    boolean updateDeepLink;

    public boolean isBackFromMovie() {
        return isBackFromMovie;
    }

    public void setBackFromMovie(boolean backFromMovie) {
        isBackFromMovie = backFromMovie;
    }

    public boolean isUpdateDeepLink() {
        return updateDeepLink;
    }

    public void setUpdateDeepLink(boolean updateDeepLink) {
        this.updateDeepLink = updateDeepLink;
    }

    @Override
    public String toString() {
        return "{" +
                "isBackFromMovie=" + isBackFromMovie +
                '}';
    }
}

package com.metfone.selfcare.module.netnews.event;

/**
 * Created by HaiKE on 9/19/17.
 */

public class SettingCategoryEvent {
    public String sortCategory;
    public int type;

    public SettingCategoryEvent(String sortCategory, int type) {
        this.sortCategory = sortCategory;
        this.type = type;
    }
}

package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.NoteMessageContant;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.database.model.message.NoteMessageItem;
import com.metfone.selfcare.fragment.BaseRecyclerViewFragment;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.NoteMessageHelper;
import com.metfone.selfcare.listeners.ButtonPopupClickListener;
import com.metfone.selfcare.module.home_kh.adapter.SettingListKhAdapter;
import com.metfone.selfcare.ui.dialog.DialogEditText;
import com.metfone.selfcare.ui.recyclerview.DividerItemDecoration;
import com.metfone.selfcare.ui.recyclerview.headerfooter.EndlessRecycleViewScrollListener;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NoteMessageKhFragment extends BaseSettingKhFragment implements BaseRecyclerViewFragment.EmptyViewListener,
        ButtonPopupClickListener {
    private View rootView;
    @BindView(R.id.recycler_view_setting)
    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private List<NoteMessageItem> listItems = new ArrayList<>();
    private SettingListKhAdapter settingListAdapter;
    private int mPage = 1;
    private View viewEmpty;

    private boolean isHasLoadmore;

    public static NoteMessageKhFragment newInstance() {
        NoteMessageKhFragment fragment = new NoteMessageKhFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isHasLoadmore = true;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_recycler_view_setting_kh;
    }

    @Override
    protected void initView(View view) {
        setTitle(R.string.setting_note_message_content);
        rootView = view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData(mPage);
    }

    private void initData(final int current_page) {
        GetDataTask getDataTask = new GetDataTask();
        getDataTask.execute(current_page);
    }

    private void initAdapter() {
        if (settingListAdapter == null) {
            setAdapter();
        }
        settingListAdapter.addAll(listItems);
    }

    private void setAdapter() {
        if (mParentActivity == null) return;
        mLayoutManager = new LinearLayoutManager(mParentActivity, LinearLayoutManager.VERTICAL, false);
        settingListAdapter = new SettingListKhAdapter(mParentActivity, SettingListKhAdapter.SCREEN_SAVED_MESSAGE);
        settingListAdapter.setItemListener(
                new SettingListKhAdapter.ItemListener() {
                    @Override
                    public void removeItem(final int position) {
                        final DialogConfirm dialogConfirm = DialogConfirm.newInstance(
                                getString(R.string.delete_message)
                                , listItems.get(position).getContent()
                                , DialogConfirm.CONFIRM_TYPE
                                , R.string.delete
                                , R.string.cancel);
                        dialogConfirm.setSelectListener(new BaseDialogFragment.DialogListener() {
                            @Override
                            public void dialogRightClick(int value) {
                            }

                            @Override
                            public void dialogLeftClick() {
                                onDeleteNote(listItems.get(position));
                                settingListAdapter.removeItem(position);
                            }
                        });
                        dialogConfirm.show(getChildFragmentManager(), "");
                    }

                    @Override
                    public void itemClick(int position) {
                        final NoteMessageItem messageItem = listItems.get(position);
                        DialogEditText editText = new DialogEditText(mParentActivity, true);
                        editText.setOldContent(messageItem.getContent());
                        editText.setCheckEnable(true);
                        editText.setMultiLine(true);
                        int MAX_LENGTH_DEFAULT = mApplication.getReengAccountBusiness().isCambodia()
                                ? Constants.MESSAGE.TEXT_IP_MAX_LENGTH_CAM : Constants.MESSAGE.TEXT_IP_MAX_LENGTH;
                        editText.setMaxLength(MAX_LENGTH_DEFAULT);
                        editText.setLabel(getString(R.string.edit));
                        editText.setNegativeLabel(getString(R.string.cancel));
                        editText.setPositiveLabel(getString(R.string.save));
                        editText.setPositiveListener(result -> {
                            messageItem.setContent(result);
                            onAcceptClick(messageItem);

                        }).setOnDismissListener(dialog -> mParentActivity.hideKeyboard());
                        editText.show();
                    }

                    @Override
                    public void avatarClickListener(int position) {
                        if (listItems == null || listItems.size() < position) return;
                        NoteMessageItem item = listItems.get(position);
                        ThreadMessage thread;
                        if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT) {

                            thread = mApplication.getMessageBusiness().findExistingSoloThread(item.getThread_jid());

                        } else if (item.getThread_type() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {// chat group
                            thread = mApplication.getMessageBusiness().findGroupThreadByServerId(item.getThread_jid());

                        } else {    // official
//                    thread = mApplication.getMessageBusiness().findExistingOrCreateOfficerThread(item.getThread_jid(), item.getThread_name(), item.getThread_avatar(), OfficerAccountConstant
//                            .ONMEDIA_TYPE_NONE);
                            thread = mApplication.getMessageBusiness().findOfficerThreadById(item.getThread_jid());
                        }
                        if (thread != null) {
                            navigateToChatActivity(thread);
                        } else {
                            mParentActivity.showToast(R.string.error_thread_not_found);
                        }
                    }
                });
        if (mRecyclerView != null) {
//            mRecyclerView.addItemDecoration(new DividerItemDecoration(mParentActivity, LinearLayoutManager.VERTICAL));
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(settingListAdapter);
            mRecyclerView.addOnScrollListener(new EndlessRecycleViewScrollListener(mLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    if (isHasLoadmore) {
                        mPage = current_page;
                        initData(current_page);
                    }
                }
            });
        }
    }

    @Override
    public void onRetryClick() {

    }

    public void navigateToChatActivity(ThreadMessage threadMessage) {
        NavigateActivityHelper.navigateToChatDetail(mParentActivity, threadMessage);
    }

    public void onAcceptClick(Object obj) {
        NoteMessageItem messageItem;
        messageItem = (NoteMessageItem) obj;
        NoteMessageHelper.getInstance(mApplication).updateNoteMessage(messageItem);
        settingListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCancerClick() {

    }

    private void onDeleteNote(Object entry) {
        NoteMessageItem messageItem = (NoteMessageItem) entry;
        NoteMessageHelper.getInstance(mApplication).deleteNoteMessage(messageItem);
        listItems.remove(messageItem);
        if (listItems.isEmpty()) {
            addViewEmpty();
        }
//        initAdapter();
    }

    private class GetDataTask extends AsyncTask<Integer, Void, ArrayList<NoteMessageItem>> {

        @Override
        protected ArrayList<NoteMessageItem> doInBackground(Integer... params) {
            return NoteMessageHelper.getInstance(mApplication).getListNote(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<NoteMessageItem> result) {
            if (result == null) {
                result = new ArrayList<>();
            }
            if (listItems == null) {
                listItems = new ArrayList<>();
            }
            if (result.size() < NoteMessageContant.SIZE) {
                isHasLoadmore = false;
            }
            if (mPage == 1) {
                listItems.clear();
                if (result.isEmpty()) {
                    addViewEmpty();
                }
            }
            listItems.addAll(result);
            initAdapter();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void addViewEmpty() {
        if (viewEmpty == null && getContext() != null && rootView != null) {
            viewEmpty = LayoutInflater.from(getContext()).inflate(R.layout.layout_view_empty_setting_kh, null, false);
            CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            viewEmpty.setLayoutParams(layoutParams);
            // set image, text and description
            //
//            AppCompatTextView tvTitleEmpty = viewEmpty.findViewById(R.id.tvEmptyTitle);
//            tvTitleEmpty.setText(R.string.saved_message_list_empty);
//            AppCompatTextView tvDescription = viewEmpty.findViewById(R.id.tvEmptyDescription);
//            tvDescription.setText(R.string.saved_message_list_empty_des);
            ((ViewGroup) (rootView)).addView(viewEmpty);
        }
//        AnimationUtil.animationScale((ViewGroup) rootView, viewEmpty);
        if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);

    }

    public void hideEmptyView() {
        if (viewEmpty != null) {
//            AnimationUtil.animationScale((ViewGroup) rootView, viewEmpty);
            viewEmpty.setVisibility(View.GONE);
        }
    }
}

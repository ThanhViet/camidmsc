package com.metfone.selfcare.module.home_kh.fragment.rewardshop.model;

public class CategoryFilter {
    String name;
    String url;

    public CategoryFilter(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

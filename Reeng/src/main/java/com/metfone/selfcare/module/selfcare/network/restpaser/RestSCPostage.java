package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCPostage;

import java.io.Serializable;

public class RestSCPostage extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private SCPostage data;

    public SCPostage getData() {
        return data;
    }

    public void setData(SCPostage data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SCPostage [data=" + data + "] errror " + getErrorCode();
    }
}

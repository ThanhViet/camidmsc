package com.metfone.selfcare.module.netnews.MostNews.view;


import com.metfone.selfcare.module.newdetails.view.MvpView;
import com.metfone.selfcare.module.response.NewsResponse;

/**
 * Created by HaiKE on 8/18/17.
 */

public interface IMostNewsView extends MvpView {

    void loadDataSuccess(boolean flag);

    void bindData(NewsResponse childNewsResponse);
}

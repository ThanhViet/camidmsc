/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/7
 *
 */

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.model.tab_video.Channel;

public class SearchHistory {
    public static final int TYPE_KEY_SEARCH = 1;
    public static final int TYPE_CONTACT = 2;
    public static final int TYPE_CHANNEL = 3;
    public static final int TYPE_CHAT = 4;

    private int type;
    private String keySearch;
    private PhoneNumber contact;
    private Channel channel;
    private ThreadMessage threadChat;

    public void setData(Object object) {
        if (object instanceof PhoneNumber) {
            type = TYPE_CONTACT;
            keySearch = ((PhoneNumber) object).getJidNumber();
            contact = (PhoneNumber) object;
        } else if (object instanceof Channel) {
            type = TYPE_CHANNEL;
            keySearch = ((Channel) object).getId();
            channel = (Channel) object;
        } else if (object instanceof String) {
            type = TYPE_KEY_SEARCH;
            keySearch = (String) object;
        } else if (object instanceof ThreadMessage) {
            type = TYPE_CHAT;
            keySearch = ((ThreadMessage) object).getSoloNumber();
            threadChat = (ThreadMessage) object;
        }
    }

    public String getName() {
        if (type == TYPE_KEY_SEARCH)
            return keySearch;
        else if (type == TYPE_CONTACT && contact != null)
            return contact.getName();
        else if (type == TYPE_CHANNEL && channel != null)
            return channel.getName();
        else if (type == TYPE_CHAT && threadChat != null)
            return threadChat.getThreadName();
        return "";
    }

    public boolean isShowIconSearch() {
        return type == TYPE_KEY_SEARCH;
    }

    public boolean isShowAvatar() {
        return type == TYPE_CONTACT || type == TYPE_CHANNEL || type == TYPE_CHAT;
    }

    public int getType() {
        return type;
    }

    public String getKeySearch() {
        return keySearch;
    }

    public PhoneNumber getContact() {
        return contact;
    }

    public Channel getChannel() {
        return channel;
    }

    public ThreadMessage getThreadChat() {
        return threadChat;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }

    public void setContact(PhoneNumber contact) {
        this.contact = contact;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setThreadChat(ThreadMessage threadChat) {
        this.threadChat = threadChat;
    }

    @Override
    public String toString() {
        return "SearchHistory{" +
                "type=" + type +
                ", keySearch='" + keySearch + '\'' +
                ", contact=" + contact +
                ", channel=" + channel +
                ", threadChat=" + threadChat +
                '}';
    }
}

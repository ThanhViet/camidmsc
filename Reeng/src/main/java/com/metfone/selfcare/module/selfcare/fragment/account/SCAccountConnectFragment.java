/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.module.selfcare.activity.SCAccountActivity;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCAccount;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

*/
/**
 * Created by thanhnt72 on 2/13/2019.
 *//*


public class SCAccountConnectFragment extends SCBaseFragment {

    private static final String USER_NAME = "username";

    EditText etAccountName;
    EditText etAccountPass;
    @BindView(R.id.tvNoteAccount)
    TextView tvNoteAccount;
    @BindView(R.id.btnConnect)
    RoundTextView btnConnect;
    Unbinder unbinder;
    @BindView(R.id.tvForgetPass)
    TextView tvForgetPass;
    @BindView(R.id.til_myid_account)
    TextInputLayout tilMyidAccount;
    @BindView(R.id.til_myid_pass)
    TextInputLayout tilMyidPass;


    private String userName;


    public static SCAccountConnectFragment newInstance(String userName) {
        SCAccountConnectFragment fragment = new SCAccountConnectFragment();
        Bundle args = new Bundle();
        args.putString(USER_NAME, userName);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public String getName() {
        return SCAccountConnectFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_connect;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initEditText();
        setActionBar(view);
        if (getArguments() != null) {
            userName = getArguments().getString(USER_NAME);
        }
        drawDetailView();

        return view;
    }

    private void initEditText() {
        etAccountName = tilMyidAccount.getEditText();
        etAccountPass = tilMyidPass.getEditText();
        etAccountName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilMyidAccount.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etAccountPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                tilMyidPass.setError(null);
                tilMyidPass.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void drawDetailView() {

        tvNoteAccount.setText(String.format(mActivity.getString(R.string.sc_note_connect_to_number),
                mApp.getReengAccountBusiness().getJidNumber()));
        SpannableString content = new SpannableString(mActivity.getString(R.string.sc_forget_pass));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvForgetPass.setText(content);
        if (!TextUtils.isEmpty(userName)) {
            etAccountName.setText(userName);
            etAccountName.setFocusable(false);
            etAccountName.setEnabled(false);
            etAccountName.setCursorVisible(false);
            etAccountName.setKeyListener(null);
            etAccountName.setTextColor(ContextCompat.getColor(mActivity, R.color.sc_color_text_normal));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setActionBar(View view) {
        LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        Toolbar abView = view.findViewById(R.id.tool_bar_fragment);
        abView.removeAllViews();
        abView.addView(mLayoutInflater.inflate(
                R.layout.ab_detail, null), new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        (abView.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        ImageView mImBack = abView.findViewById(R.id.ab_back_btn);
        mImBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mActivity.getString(R.string.sc_connect_myid_exist));

    }

    @OnClick({R.id.tvForgetPass, R.id.btnConnect})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvForgetPass:
                ((SCAccountActivity) mActivity).openForgetPass(etAccountName.getText().toString());
                break;
            case R.id.btnConnect:
                //TODO login
                doLoginMyID();
                break;
        }
    }

    private void doLoginMyID() {

        if (checkValidData()) {
            SCAccount scAccount = new SCAccount(etAccountName.getText().toString(), etAccountPass.getText().toString());
            if (TextUtils.isEmpty(userName)) {
                String jid = mApp.getReengAccountBusiness().getJidNumber();
                jid = jid.substring(1);
                scAccount.setPhoneNumber(jid);
            }
            mActivity.showLoadingSelfCare("", R.string.loading);
            SelfCareAccountApi.getInstant(mApp).loginMyID(scAccount, new SCAccountCallback.SCAccountApiListener() {
                @Override
                public void onSuccess(String response) {
                    mActivity.hideLoadingDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("errorCode", -1) == 0) {
                            JSONObject jsResult = jsonObject.optJSONObject("result");
                            if (jsResult != null) {
                                String accessToken = jsResult.optString("accessToken");
                                if (!TextUtils.isEmpty(accessToken)) {

                                    onLoginSuccess(accessToken);

                                } else
                                    mActivity.showToast(R.string.e601_error_but_undefined);
                            } else
                                mActivity.showToast(R.string.e601_error_but_undefined);
                        } else if (jsonObject.optInt("errorCode", -1) == 0)
                            mActivity.showToast(R.string.sc_wrong_info_login);
                        else
                            mActivity.showToast(R.string.e601_error_but_undefined);
                    } catch (JSONException e) {
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }

                @Override
                public void onError(int code, String message) {
                    mActivity.hideLoadingDialog();
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            });
        }
    }

    private void onLoginSuccess(String accessToken) {
        mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_KEY_ACCESS_TOKEN, accessToken).apply();
        SelfCareAccountApi.getInstant(mApp).setAccessToken(accessToken);
        SelfCareAccountApi.getInstant(mApp).getAndSaveInfo();

        DeepLinkHelper.getInstance().openSchemaLink(mActivity, "mytel://home/selfcare?clearStack=1");
        mActivity.clearBackStack();
        mActivity.finish();
    }

    private boolean checkValidData() {
        //TODO check data
        boolean isValid = true;
        if (TextUtils.isEmpty(etAccountPass.getText().toString().trim())) {
            isValid = false;
            tilMyidPass.setError(mActivity.getString(R.string.sc_input_text));
        }
        if (TextUtils.isEmpty(etAccountName.getText().toString().trim())) {
            isValid = false;
            tilMyidAccount.setError(mActivity.getString(R.string.sc_input_text));
        }
        return isValid;
    }
}*/

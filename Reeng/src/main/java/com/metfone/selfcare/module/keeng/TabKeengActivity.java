package com.metfone.selfcare.module.keeng;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.manager.SupportRequestManagerFragment;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.base.BaseActivity;
import com.metfone.selfcare.module.keeng.fragment.category.AlbumDetailFragment;
import com.metfone.selfcare.module.keeng.fragment.category.PlaylistDetailFragment;
import com.metfone.selfcare.module.keeng.fragment.category.RankDetailFragment;
import com.metfone.selfcare.module.keeng.fragment.category.SingerFragment;
import com.metfone.selfcare.module.keeng.fragment.category.TopHitDetailFragment;
import com.metfone.selfcare.module.keeng.fragment.category.TopicDetailFragment;
import com.metfone.selfcare.module.keeng.fragment.home.AlbumHotFragment;
import com.metfone.selfcare.module.keeng.fragment.home.MusicHomeFragment;
import com.metfone.selfcare.module.keeng.fragment.home.PlaylistHotFragment;
import com.metfone.selfcare.module.keeng.fragment.home.SingerHotFragment;
import com.metfone.selfcare.module.keeng.fragment.home.SongHotFragment;
import com.metfone.selfcare.module.keeng.fragment.home.TopHitFragment;
import com.metfone.selfcare.module.keeng.fragment.home.TopicHotFragment;
import com.metfone.selfcare.module.keeng.fragment.home.VideoHotFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.PlayListModel;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.io.Serializable;

public class TabKeengActivity extends BaseActivity {
    protected final String TAG = getClass().getSimpleName();
    protected Fragment mFragment;
    protected int currentTabId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keeng);

        Intent intent = getIntent();
        int type = intent.getIntExtra(Constants.KEY_TYPE, 0);
        Object object = intent.getSerializableExtra(Constants.KEY_DATA);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, (Serializable) object);
        goNextTab(type, bundle);
    }

    public void goNextTab(int tabId, Bundle args) {
        switch (tabId) {
            case Constants.TAB_HOME_MUSIC:
                mFragment = MusicHomeFragment.newInstance();
                break;
            case Constants.TAB_VIDEO_HOT:
                mFragment = VideoHotFragment.newInstance();
                break;
            case Constants.TAB_PLAYLIST_HOT:
                mFragment = PlaylistHotFragment.newInstance();
                break;
            case Constants.TAB_ALBUM_HOT:
                mFragment = AlbumHotFragment.newInstance();
                break;
            case Constants.TAB_TOPIC_HOT:
                mFragment = TopicHotFragment.newInstance();
                break;
            case Constants.TAB_TOP_HIT:
                mFragment = TopHitFragment.newInstance();
                break;
            case Constants.TAB_SONG_HOT:
                mFragment = SongHotFragment.newInstance();
                break;
            case Constants.TAB_SINGER_HOT:
                mFragment = SingerHotFragment.newInstance();
                break;
            case Constants.TAB_ALBUM_DETAIL:
                mFragment = AlbumDetailFragment.newInstance();
                break;
            case Constants.TAB_PLAYLIST_DETAIL:
                mFragment = PlaylistDetailFragment.newInstance();
                break;
            case Constants.TAB_RANK_DETAIL:
                mFragment = RankDetailFragment.newInstance();
                break;
            case Constants.TAB_SINGLE_DETAIL:
                mFragment = SingerFragment.newInstance(args);
                break;
            case Constants.TAB_TOPIC_DETAIL:
                mFragment = TopicDetailFragment.newInstance();
                break;
            case Constants.TAB_TOP_HIT_DETAIL:
                mFragment = TopHitDetailFragment.newInstance();
                break;
            default:
                mFragment = null;
                break;

        }

        if (mFragment != null) {
            currentTabId = tabId;
            try {
                if (!mFragment.isAdded())
                    if (args != null) {
                        if (mFragment.getArguments() == null) {
                            mFragment.setArguments(args);
                        } else {
                            mFragment.getArguments().putAll(args);
                        }
                    }
            } catch (IllegalStateException e) {
                Log.e(TAG, e);
            } catch (RuntimeException e) {
                Log.e(TAG, e);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
            replaceFragment(currentTabId, mFragment);
        }
    }

    public void replaceFragment(final int tabId, final Fragment fragment) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                transactionFragment(tabId, fragment);
            }
        });
    }

    public void transactionFragment(final int tabId, final Fragment fragment) {
        try {
//            if (fragment.isAdded()) {
//                Log.d(TAG, fragment.getClass().getCanonicalName() + " was Added");
//                if (fragment instanceof MusicHomeFragment) {
//                    finish();
//                }
//            } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit)
                    .add(R.id.tabContent, fragment, String.valueOf(tabId))
                    .commitAllowingStateLoss();
//            }
        } catch (RuntimeException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    public void goPrevTab() {
        try {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            int entryCount = mFragmentManager.getFragments().size();
            if (entryCount > 0) {
                for (int i = entryCount - 1; i >= 0; i--) {
                    Fragment fragment = mFragmentManager.getFragments().get(i);
                    if (fragment != null) {
                        if (i == 0
//                                fragment instanceof MusicHomeFragment
//                                || fragment instanceof AudioPlayerFragment
//                                || fragment instanceof VideoPlayerFragment
//                                || fragment instanceof YoutubePlayerFragment
                        ) {
                            finish();
                            break;
                        } else if (fragment instanceof SupportRequestManagerFragment
                                /*|| fragment instanceof com.google.android.gms.common.api.internal.zzc*/) {
                            continue;
                        } else {
                            onFragmentDetached(fragment.getTag());
//                            try {
//                                if (i > 0) {
//                                    for (int j = i - 1; j >= 0; j--) {
//                                        Fragment fragmentPrev = mFragmentManager.getFragments().get(j);
//                                        if (fragmentPrev instanceof SupportRequestManagerFragment) {
//                                            continue;
//                                        }
//                                        if (BuildConfig.DEBUG)
//                                            Log.d(TAG, "fragmentPrev: " + fragmentPrev.getClass().getCanonicalName());
//                                        mFragment = fragmentPrev;
//                                        String tag = mFragment.getTag();
//                                        if (tag != null && TextUtils.isDigitsOnly(tag)) {
//                                            try {
//                                                currentTabId = Integer.parseInt(tag);
//                                                processShowOrHidePlayer(currentTabId);
//                                            } catch (Exception e) {
//                                                Log.e(TAG, e);
//                                            }
//                                        }
//                                        break;
//                                    }
//                                }
//                            } catch (Exception e) {
//                                Log.e(TAG, e);
//                            }
                            break;
                        }
                    }
                }
            } else {
                finish();
            }
        } catch (Exception e) {
            Log.e(TAG, e);
            finish();
        }
    }

    public void onFragmentDetached(String tag) {
        Log.d(TAG, "onFragmentDetached: " + tag);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_left_to_right_enter, R.anim.activity_left_to_right_exit)
                    .remove(fragment)
                    .commitNow();
        }
    }

    @Override
    public void onBackPressed() {
        goPrevTab();
    }

    public void gotoAlbumDetail(final AllModel item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_ALBUM_DETAIL, bundle);
    }

    public void gotoPlaylistDetail(final PlayListModel item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_PLAYLIST_DETAIL, bundle);
    }

    public void gotoRankDetail(final RankModel item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_RANK_DETAIL, bundle);
    }

    public void gotoSingerDetail(final Topic item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_SINGLE_DETAIL, bundle);
    }

    public void gotoTopicDetail(final Topic item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_TOPIC_DETAIL, bundle);
    }

    public void gotoCategoryDetail(final Topic item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_CATEGORY_DETAIL, bundle);
    }

    public void gotoTopHitDetail(Topic item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, item);
        goNextTab(Constants.TAB_TOP_HIT_DETAIL, bundle);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }
}

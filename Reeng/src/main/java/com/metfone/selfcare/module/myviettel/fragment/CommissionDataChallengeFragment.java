/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/30
 *
 */

package com.metfone.selfcare.module.myviettel.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.myviettel.activity.DataChallengeActivity;
import com.metfone.selfcare.module.myviettel.adapter.DataChallengeAdapter;
import com.metfone.selfcare.module.myviettel.model.DataChallenge;
import com.metfone.selfcare.module.myviettel.model.DataChallengeProvisional;
import com.metfone.selfcare.module.myviettel.model.TimeModel;
import com.metfone.selfcare.module.myviettel.network.ApiCallback;
import com.metfone.selfcare.module.myviettel.network.MyViettelApi;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CommissionDataChallengeFragment extends BaseFragment {
    private ProgressBar loadingView;
    private View viewEmpty;
    private View viewError;
    private RecyclerView recyclerView;
    private Spinner spDateTime;

    private ArrayList<DataChallenge> datas;
    private DataChallengeAdapter adapter;
    private ArrayList<TimeModel> dataSpinner;
    private RegionSpinnerAdapter<TimeModel> adapterSpinner;

    private long startTime = 0L;
    private long endTime = 0L;
    private boolean isLoading;
    private int currentState = -1;

    public static CommissionDataChallengeFragment newInstance() {
        Bundle args = new Bundle();
        CommissionDataChallengeFragment fragment = new CommissionDataChallengeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "CommissionDataChallengeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_commission_data_challenge;
    }

    private MyViettelApi getMyViettelApi() {
        if (mActivity instanceof DataChallengeActivity) {
            return ((DataChallengeActivity) mActivity).getMyViettelApi();
        }
        return new MyViettelApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            loadingView = view.findViewById(R.id.loading_view);
            viewEmpty = view.findViewById(R.id.layout_empty);
            viewError = view.findViewById(R.id.tv_error);
            recyclerView = view.findViewById(R.id.recycler_view);
            spDateTime = view.findViewById(R.id.spinner_datetime);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (datas == null) datas = new ArrayList<>();
        adapter = new DataChallengeAdapter(mActivity);
        adapter.setData(datas);
        BaseAdapter.setupVerticalRecycler(mActivity, recyclerView, null, adapter, false);

        if (spDateTime != null) {
            if (dataSpinner == null) dataSpinner = new ArrayList<>();
            else dataSpinner.clear();
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            final int month = calendar.get(Calendar.MONTH);

            for (int i = month; i >= 0; i--) {
                TimeModel model = new TimeModel();
                model.setTitle(mActivity.getString(R.string.commission_month, (i + 1), year));
                calendar.set(Calendar.MONTH, i);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                int lastDateOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                Date startDate = calendar.getTime();
                calendar.set(Calendar.DAY_OF_MONTH, lastDateOfMonth);
                Date endDate = calendar.getTime();
                model.setStartTime(startDate.getTime());
                model.setEndTime(endDate.getTime());
                dataSpinner.add(model);
            }
            if (month < 11) {
                for (int i = 11; i > month; i--) {
                    TimeModel model = new TimeModel();
                    model.setTitle(mActivity.getString(R.string.commission_month, (i + 1), year - 1));
                    calendar.set(Calendar.YEAR, year - 1);
                    calendar.set(Calendar.MONTH, i);
                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    int lastDateOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                    Date startDate = calendar.getTime();
                    calendar.set(Calendar.DAY_OF_MONTH, lastDateOfMonth);
                    Date endDate = calendar.getTime();
                    model.setStartTime(startDate.getTime());
                    model.setEndTime(endDate.getTime());
                    dataSpinner.add(model);
                }
            }

            adapterSpinner = new RegionSpinnerAdapter<>(mActivity, dataSpinner);
            spDateTime.setAdapter(adapterSpinner);
            spDateTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != currentState) {
                        currentState = position;
                        if (dataSpinner != null && dataSpinner.size() > position && position >= 0) {
                            TimeModel model = dataSpinner.get(position);
                            startTime = model.getStartTime();
                            endTime = model.getEndTime();
                        }
                        isLoading = false;
                        if (isVisibleToUser) loadData();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        if (viewError != null) viewError.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData();
            }
        });
    }

    private void loadData() {
        if (isLoading) return;
        if (loadingView != null) loadingView.setVisibility(View.VISIBLE);
        if (viewEmpty != null) viewEmpty.setVisibility(View.GONE);
        if (viewError != null) viewError.setVisibility(View.GONE);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
        isLoading = true;
        getMyViettelApi().getDetailCommissionDC(startTime, endTime, new ApiCallback<DataChallengeProvisional>() {
            @Override
            public void onSuccess(String msg, DataChallengeProvisional result) throws Exception {
                if (datas == null) datas = new ArrayList<>();
                else datas.clear();
                if (Utilities.notEmpty(result.getData())) {
                    datas.addAll(result.getData());
                }
                if (adapter != null) {
                    adapter.setTotalCommission(result.getTotalCommission());
                    adapter.setTotalTransaction(result.getTotalTransaction());
                    adapter.notifyDataSetChanged();
                }
                if (Utilities.isEmpty(datas)) {
                    if (viewEmpty != null) viewEmpty.setVisibility(View.VISIBLE);
                    if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                } else {
                    if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(String s) {
                if (Utilities.isEmpty(datas)) {
                    if (viewError != null) viewError.setVisibility(View.VISIBLE);
                    if (recyclerView != null) recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onComplete() {
                isDataInitiated = true;
                if (loadingView != null) loadingView.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (canLazyLoad()) loadData();
    }
}

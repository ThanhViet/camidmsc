package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TelecomGift implements Serializable {
    @SerializedName("giftTypeId")
    @Expose
    private String giftTypeId;
    @SerializedName("giftTitle")
    @Expose
    private String giftTitle;
    @SerializedName("giftDesc")
    @Expose
    private String giftDesc;
    @SerializedName("giftPoint")
    @Expose
    private int giftPoint;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("transferType")
    @Expose
    private String transferType;
    @SerializedName("dataType")
    @Expose
    private String dataType;
    @SerializedName("giftId")
    @Expose
    private String giftId;
    @SerializedName("giftBlock")
    @Expose
    private int giftBlock;
    @SerializedName("exchangeUnit")
    @Expose
    private String exchangeUnit;
    @SerializedName("exchangeValue")
    @Expose
    private double exchangeValue;
    @SerializedName("lstBlock")
    @Expose
    private List<LstBlock> lstBlock = null;
    @SerializedName("giftRate")
    @Expose
    private GiftRate giftRate;

    public String getGiftTypeId() {
        return giftTypeId;
    }

    public void setGiftTypeId(String giftTypeId) {
        this.giftTypeId = giftTypeId;
    }

    public String getGiftTitle() {
        return giftTitle;
    }

    public void setGiftTitle(String giftTitle) {
        this.giftTitle = giftTitle;
    }

    public String getGiftDesc() {
        return giftDesc;
    }

    public void setGiftDesc(String giftDesc) {
        this.giftDesc = giftDesc;
    }

    public int getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(int giftPoint) {
        this.giftPoint = giftPoint;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public int getGiftBlock() {
        return giftBlock;
    }

    public void setGiftBlock(int giftBlock) {
        this.giftBlock = giftBlock;
    }

    public String getExchangeUnit() {
        return exchangeUnit;
    }

    public void setExchangeUnit(String exchangeUnit) {
        this.exchangeUnit = exchangeUnit;
    }

    public double getExchangeValue() {
        return exchangeValue;
    }

    public void setExchangeValue(double exchangeValue) {
        this.exchangeValue = exchangeValue;
    }

    public List<LstBlock> getLstBlock() {
        return lstBlock;
    }

    public void setLstBlock(List<LstBlock> lstBlock) {
        this.lstBlock = lstBlock;
    }

    public GiftRate getGiftRate() {
        return giftRate;
    }

    public void setGiftRate(GiftRate giftRate) {
        this.giftRate = giftRate;
    }

}

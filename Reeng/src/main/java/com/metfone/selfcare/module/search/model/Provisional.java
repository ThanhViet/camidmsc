package com.metfone.selfcare.module.search.model;

import java.io.Serializable;

public abstract class Provisional implements Serializable {
    public abstract int getSize();
}

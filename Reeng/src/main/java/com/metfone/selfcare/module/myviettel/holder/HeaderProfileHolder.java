/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.AccountInfo;
import com.metfone.selfcare.module.myviettel.model.DataInfo;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;

public class HeaderProfileHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_avatar_default)
    TextView tvAvatarDefault;
    @BindView(R.id.tv_contact_avatar)
    TextView tvContactAvatar;
    @BindView(R.id.iv_profile_avatar)
    CircleImageView ivProfileAvatar;

    @BindView(R.id.iv_cover)
    ImageView ivCover;
    @BindView(R.id.tv_name)
    EllipsisTextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.layout_package_info)
    View viewPackageInfo;
    @BindView(R.id.tv_package_name)
    TextView tvPackageName;
    @BindView(R.id.tv_data_left)
    TextView tvDataLeft;
    @BindView(R.id.tv_expire_date)
    TextView tvExpireDate;
    @BindView(R.id.button_learn_more)
    View btnLearnMore;

    public HeaderProfileHolder(View view, final OnMyViettelListener listener) {
        super(view);
        if (btnLearnMore != null) btnLearnMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) listener.onClickLearnMoreSwitchToViettel();
            }
        });
    }

    @Override
    public void bindData(Object item, int position) {
        ApplicationController mApplication = ApplicationController.self();
        if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
            if (ivProfileAvatar != null) ivProfileAvatar.setVisibility(View.VISIBLE);
            if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
            if (tvAvatarDefault != null) tvAvatarDefault.setVisibility(View.GONE);
            ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_tab_home_avatar_default);
        } else {
            ReengAccount account = mApplication.getReengAccountBusiness().getCurrentAccount();
            if (account != null) {
                String lAvatar = account.getLastChangeAvatar();
                if (TextUtils.isEmpty(lAvatar)) {
                    if (ivProfileAvatar != null) ivProfileAvatar.setVisibility(View.VISIBLE);
                    if (tvContactAvatar != null) tvContactAvatar.setVisibility(View.GONE);
                    if (tvAvatarDefault != null) tvAvatarDefault.setVisibility(View.GONE);
                    ImageBusiness.setResource(ivProfileAvatar, R.drawable.ic_tab_home_avatar_default);
                } else {
                    mApplication.getAvatarBusiness().setMyAvatar(ivProfileAvatar,
                            tvContactAvatar, tvAvatarDefault, account, null);
                }
            }
        }
        if (item instanceof AccountInfo) {
            AccountInfo data = (AccountInfo) item;
            if (tvName != null) tvName.setText(data.getUsername());
            if (tvPhone != null) tvPhone.setText(data.getPhoneNumber());
            if (data.isViettel()) {
                if (btnLearnMore != null) btnLearnMore.setVisibility(View.GONE);
                ImageBusiness.setResource(ivCover, R.drawable.bg_profile_mvt_2);
                if (tvTitle != null) tvTitle.setText(R.string.data_info);
                if (Utilities.isEmpty(data.getData())) {
                    if (viewPackageInfo != null) viewPackageInfo.setVisibility(View.GONE);
                    if (tvDesc != null) {
                        tvDesc.setVisibility(View.VISIBLE);
                        String desc = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_MSG_NOT_REGISTERED_DATA);
                        if (TextUtils.isEmpty(desc)) desc = mApplication.getString(R.string.msg_not_registered_any_data_package);
                        tvDesc.setText(Html.fromHtml(desc));
                    }
                } else {
                    if (viewPackageInfo != null) viewPackageInfo.setVisibility(View.VISIBLE);
                    if (tvDesc != null) tvDesc.setVisibility(View.GONE);
                    DataInfo dataInfo = data.getData().get(0);
                    if (dataInfo != null) {
                        if (tvPackageName != null)
                            tvPackageName.setText(Html.fromHtml(mApplication.getString(R.string.text_package_name, dataInfo.getName())));
                        if (tvDataLeft != null)
                            tvDataLeft.setText(Html.fromHtml(mApplication.getString(R.string.text_data_left, dataInfo.getPromotion())));
                        if (tvExpireDate != null) {
                            tvExpireDate.setText(Html.fromHtml(getExpireDate(mApplication, dataInfo.getExpireDate())));
                        }
                    }
                }
            } else {
                ImageBusiness.setResource(ivCover, R.drawable.bg_profile_mvt);
                if (tvTitle != null) tvTitle.setText(R.string.switch_to_viettel_network);
                if (viewPackageInfo != null) viewPackageInfo.setVisibility(View.GONE);
                if (btnLearnMore != null) btnLearnMore.setVisibility(View.VISIBLE);
                if (tvDesc != null) {
                    tvDesc.setVisibility(View.VISIBLE);
                    String desc = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_MSG_SWITCH_TO_VIETTEL);
                    if (TextUtils.isEmpty(desc)) desc = mApplication.getString(R.string.msg_switch_to_viettel_network);
                    tvDesc.setText(Html.fromHtml(desc));
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getExpireDate(Context context, String expireDateStr) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date expireDate = simpleDateFormat.parse(expireDateStr);
            Date currentDate = Calendar.getInstance().getTime();
            if (expireDate.getDate() != currentDate.getDate() && expireDate.getTime() > currentDate.getTime()) {
                int date = (int) ((expireDate.getTime() - currentDate.getTime()) / DateTimeUtils.DAY);
                if (date <= 0) date = 1;
                return context.getString(R.string.text_expire_date, date);
            } else {
                return context.getString(R.string.text_expire_date, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context.getString(R.string.text_expire_date2, expireDateStr);
    }
}

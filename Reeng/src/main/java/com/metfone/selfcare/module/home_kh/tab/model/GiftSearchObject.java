package com.metfone.selfcare.module.home_kh.tab.model;

import java.util.List;

public class GiftSearchObject {
    public String giftTypeName;     //": "Premium",
    public String id;               //": "12",
    public int isShortCut;          //": 1,
    public String iconUrl;          //": "https://imagemetfone.viettelglobal.net/1581652614844.png",
    public String partner;              //": "https://imagemetfone.viettelglobal.net/1581652614844.png",
    public int isType;                  //": 0
    public List<GiftChildDetailItem> gifts;
    
    public GiftSearchObject() {
    }
}

package com.metfone.selfcare.module.keeng.utils;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.ui.dialog.DismissListener;
import com.metfone.selfcare.util.Log;

public class DialogLoadData  extends Dialog {
    private BaseSlidingFragmentActivity activity;
    private DismissListener dismissListener;

    public DialogLoadData(BaseSlidingFragmentActivity activity, boolean isCancelable) {
        super(activity, R.style.DialogFullscreen);
        this.activity = activity;
        setCancelable(isCancelable);
    }

    public DialogLoadData setDismissListener(DismissListener listener) {
        this.dismissListener = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_loading_data);
    }

    @Override
    public void dismiss() {
        Log.d("DialogEditText", "dismiss");
        super.dismiss();
        if (dismissListener != null) {
            dismissListener.onDismiss();
        }
    }
}
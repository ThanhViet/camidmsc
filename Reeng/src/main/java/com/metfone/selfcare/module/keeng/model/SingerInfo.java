package com.metfone.selfcare.module.keeng.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SingerInfo implements Serializable {

    @SerializedName("numSong")
    private long numSong;
    @SerializedName("numAlbum")
    private long numAlbum;
    @SerializedName("numVideo")
    private long numVideo;

    public long getNumSong() {
        return numSong;
    }

    public long getNumAlbum() {
        return numAlbum;
    }

    public long getNumVideo() {
        return numVideo;
    }
}

package com.metfone.selfcare.module.tiin.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryTiinResponse extends ErrorResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<Category> data = new ArrayList<>();

    public ArrayList<Category> getData() {
        return data;
    }

    public void setData(ArrayList<Category> data) {
        this.data = data;
    }
}

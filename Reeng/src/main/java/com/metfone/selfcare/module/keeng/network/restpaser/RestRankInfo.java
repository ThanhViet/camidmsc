package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.RankModel;

import java.io.Serializable;

public class RestRankInfo extends AbsResultData implements Serializable {

    private static final long serialVersionUID = -3337251157361722174L;

    @SerializedName("data")
    public RankModel data;

    public RankModel getData() {
        return data;
    }

    public void setData(RankModel data) {
        this.data = data;
    }

}

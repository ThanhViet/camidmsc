package com.metfone.selfcare.module.games.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.model.tabGame.GameListResponseData;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.games.adapter.AchievementGameAdapter;
import com.metfone.selfcare.module.games.adapter.CustomAdapter;
import com.metfone.selfcare.module.games.adapter.CustomItem;
import com.metfone.selfcare.module.games.adapter.SpinnerAdapter;
import com.metfone.selfcare.module.games.api.GameApi;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AchievementGameFragment extends BaseSlidingFragmentActivity implements ItemViewClickListener, View.OnClickListener, SpinnerAdapter.OnItemClick {
    protected ApplicationController app;
    private AchievementGameFragment achievementGameFragment;
    private ImageView btnBack;

    private LinearLayout lnSpinner;
    private TextView tvTitleSpinner;
    private TextView tvDay;
    private ImageView ivSpinner;
    private RecyclerView rvSpinner;
    private ArrayList<CustomItem> listRangeDay;
    private SpinnerAdapter rangeAdapter;
    private String rangeDaySelected;

    private RecyclerView rvGameAchievement;
    private Spinner customSpinner;
    private ArrayList<CustomItem> customList;
    int width = 150;

    private LinearLayout lnNotPlayed;

    private BaseSlidingFragmentActivity activity;

    private ArrayList<GameModel> listGame;



    public static AchievementGameFragment newInstance() {
        return new AchievementGameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_achievement);
        app = (ApplicationController) this.getApplication();
        activity = this;
        activity.changeStatusBar(R.color.m_p_base_red);

        initData();
        initViews();
    }

    @Override
    public void changeStatusBar(int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.m_p_base_red));
        }
    }

    private void getGames(int range) {
        GameApi.getInstance().getBestScoreGames(range, new GameApi.ListenerGameApi() {
            @Override
            public void onPreRequest() {
                showLoadingDialog("", "");
            }

            @Override
            public void onSuccess(Object responseData) {
                if (responseData instanceof GameListResponseData) {
                    GameListResponseData resultData = (GameListResponseData) responseData;
                    listGame = new ArrayList<>();
                    listGame.addAll(Arrays.asList(resultData.getItems()));
                    if (listGame.size() == 0) {
                        lnNotPlayed.setVisibility(View.VISIBLE);
                    } else {
                        lnNotPlayed.setVisibility(View.GONE);
                    }
                    AchievementGameAdapter adapter = new AchievementGameAdapter(listGame, activity);
                    rvGameAchievement.setAdapter(adapter);
                }
                hideLoadingDialog();
            }

            @Override
            public void onError(String message) {
                hideLoadingDialog();
            }

            @Override
            public void onFailure(String exp) {
                hideLoadingDialog();
            }
        });
    }

    private void initViews() {
//        Utilities.setSystemUiVisibilityHideNavigation(this);
        lnNotPlayed = findViewById(R.id.ln_not_played);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rvGameAchievement = findViewById(R.id.rv_game_achievement);
        rvGameAchievement.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        lnNotPlayed.setVisibility(View.VISIBLE);

        rvSpinner = findViewById(R.id.rv_item_spinner);
        rvSpinner.setLayoutManager(new LinearLayoutManager(this));
        rangeAdapter = new SpinnerAdapter(listRangeDay, getApplicationContext());
        rangeAdapter.setOnItemClick(this::onItemClick);
        rvSpinner.setAdapter(rangeAdapter);

        lnSpinner = findViewById(R.id.ln_spinner);
        tvTitleSpinner = findViewById(R.id.tv_days);
        tvDay = findViewById(R.id.tvDropDownLayout);
        ivSpinner = findViewById(R.id.ivDropDownLayout);

        tvTitleSpinner.setText(R.string.txt_today);

        lnSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvSpinner.setVisibility(View.VISIBLE);
                for (int i = 0; i < listRangeDay.size(); i ++) {
                    if (tvTitleSpinner.getText().toString() == listRangeDay.get(i).getSpinnerItemName()) {
                        listRangeDay.get(i).setSpinnerItemImage(R.drawable.ic_uncheck_box);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(String data) {
        if (data == getString(R.string.txt_today)) {
            getGames(0);
        } else if (data == getString(R.string.txt_7_days)) {
            getGames(1);
        } else if (data == getString(R.string.txt_30_days)) {
            getGames(2);
        }
        tvTitleSpinner.setText(data);
        rvSpinner.setVisibility(View.GONE);
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {

    }

    private void initData() {
        listRangeDay = new ArrayList<>();
        listRangeDay.add(new CustomItem(getString(R.string.txt_today), R.drawable.ic_uncheck_box));
        listRangeDay.add(new CustomItem(getString(R.string.txt_7_days), R.drawable.ic_uncheck_box));
        listRangeDay.add(new CustomItem(getString(R.string.txt_30_days), R.drawable.ic_uncheck_box));
        getGames(0);
    }



}

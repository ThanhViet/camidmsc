package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentHistoryResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("errorMessage")
    @Expose
    String message;
    @SerializedName("result")
    @Expose
    PaymentHistoryResult request;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaymentHistoryResult getRequest() {
        return request;
    }

    public void setRequest(PaymentHistoryResult request) {
        this.request = request;
    }
}


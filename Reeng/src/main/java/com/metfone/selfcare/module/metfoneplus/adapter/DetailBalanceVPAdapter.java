package com.metfone.selfcare.module.metfoneplus.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.metfone.selfcare.module.metfoneplus.fragment.MPHistoryChargeFragment;

import java.util.List;

public class DetailBalanceVPAdapter extends FragmentStatePagerAdapter {
    private final static int SIZE_VIEW_PAGER_DETAIL_BALANCE = 4;

    private String[] mTitleBalance;
    private List<MPHistoryChargeFragment> mFragmentList;

    public DetailBalanceVPAdapter(@NonNull FragmentManager fm, int behavior, String[] titleBalance, List<MPHistoryChargeFragment> fragmentList) {
        super(fm, behavior);
        this.mTitleBalance = titleBalance;
        this.mFragmentList = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return SIZE_VIEW_PAGER_DETAIL_BALANCE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleBalance[position];
    }
}

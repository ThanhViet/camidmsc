package com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview;

import java.util.List;

public interface ItemViewClickListener {
    void onItemViewClickListener(int position, List<?> list);
}

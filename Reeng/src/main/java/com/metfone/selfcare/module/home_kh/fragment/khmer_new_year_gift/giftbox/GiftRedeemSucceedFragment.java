package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.common.util.Strings;
import com.metfone.selfcare.databinding.FragmentGiftRedeemSucceedBinding;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.api.WsGiftSpinResponse;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.libsignal.Base64;
import com.metfone.selfcare.util.Utilities;

import java.io.IOException;

public class GiftRedeemSucceedFragment extends BaseFragment {
    private static final String PARAM_PRIZE = "PARAM_PRIZE";
    public static final String TAG = GiftRedeemSucceedFragment.class.getSimpleName();

    private FragmentGiftRedeemSucceedBinding binding;

    public ObservableField<WsGiftSpinResponse.Prize> prize = new ObservableField<>();
    public ObservableBoolean shouldShowQr = new ObservableBoolean(false);

    public static GiftRedeemSucceedFragment newInstance(WsGiftSpinResponse.Prize prize) {

        Bundle args = new Bundle();
        args.putSerializable(PARAM_PRIZE, prize);
        GiftRedeemSucceedFragment fragment = new GiftRedeemSucceedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentGiftRedeemSucceedBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Utilities.reCalculateDimensionAnchorView(binding.anchorPointForFrame,
                binding.container,
                binding.imageOriginalSize,
                binding.anchorPointForFrame);

        prize.set((WsGiftSpinResponse.Prize) getArguments().getSerializable(PARAM_PRIZE));
        if(prize.get() != null){
            shouldShowQr.set(!Strings.isEmptyOrWhitespace(prize.get().getQrCode()));
            if(shouldShowQr.get()){
                try {
                    byte [] bytes = Base64.decode(prize.get().getQrCode());
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0, bytes.length);
                    binding.imgQrCode.setImageBitmap(bitmap);

                } catch (IOException e) {
                    shouldShowQr.set(false);
                    e.printStackTrace();
                }
            }
        }

    }

    public void back(){
        ((KhmerNewYearGiftActivity)getActivity()).playTabSound();
        onBackPressed();
    }

    @Override
    public String getName() {
        return GiftRedeemSucceedFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return 0;
    }
}

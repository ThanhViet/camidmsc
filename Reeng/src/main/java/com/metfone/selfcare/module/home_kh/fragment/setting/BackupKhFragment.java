package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.backup_restore.backup.BackupManager;
import com.metfone.selfcare.module.backup_restore.backup.BackupUtils;
import com.metfone.selfcare.module.backup_restore.backup.DBExporter;
import com.metfone.selfcare.module.home_kh.activity.BackupKhActivity;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;
import com.metfone.selfcare.module.home_kh.blur.RealtimeBlurView;
import com.metfone.selfcare.module.home_kh.dialog.KhDialogRadioSelect;
import com.metfone.selfcare.ui.dialog.DialogBackupSettingSelect;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.v5.utils.ToastUtils;
import com.metfone.selfcare.v5.utils.Util;
import com.metfone.selfcare.v5.widget.MochaProgressBarV5;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;

public class BackupKhFragment extends BaseSettingKhFragment implements View.OnClickListener,
        DBExporter.BackupProgressListener, DialogBackupSettingSelect.BackupSettingListener {
    private static final String TAG = BackupKhFragment.class.getSimpleName();
    private BackupKhActivity mParentActivity;
//    private DialogBackupProgress mDialogProgress;

    private static final String SDF_IN_DAY = "HH:mm";
    private static final String SDF_IN_YEAR = "dd/MM";
    private static final String SDF_OTH_YEAR = "dd/MM/yyyy";

    RoundTextView mBtnBackup;
    AppCompatTextView mTvLastBackup, mTvAutoBackupInfo, mTvBackupViaNetwork;

    SharedPreferences mPref;
    View mLayoutAutoBackup, mLayoutBackupViaNetwork, viewBackup;

    @BindView(R.id.progressBar)
    protected MochaProgressBarV5 backupProgressBar;

    @BindView(R.id.blur_view)
    RealtimeBlurView blurView;

    private boolean isBackingUp;
    private boolean isCancelByUser;
    private int syncDesktop;

    public static BackupKhFragment newInstance(int syncDesktop) {
        BackupKhFragment fragment = new BackupKhFragment();
        Bundle args = new Bundle();
        args.putInt(BackupKhActivity.SYNC_DESKTOP, syncDesktop);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mParentActivity = (BackupKhActivity) activity;
        mApplication = (ApplicationController) mParentActivity.getApplication();
        mPref = mApplication.getSharedPreferences(Constants.PREFERENCE.PREF_DIR_NAME, Context.MODE_PRIVATE);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParentActivity = null;
    }

    @Override
    protected void initView(View view) {
        findComponentViews(view);
        setTitle(R.string.kh_setting_backup);
        if (getArguments() != null) {
            syncDesktop = getArguments().getInt(BackupKhActivity.SYNC_DESKTOP);
        }
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
//        if (mDialogProgress != null) {
//            mDialogProgress.dismiss();
//            mDialogProgress = null;
//        }
        BackupManager.cancelBackup();
        super.onDestroy();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_backup_setting_kh;
    }

    private void findComponentViews(final View view) {
        if (view == null) return;
        viewBackup = view.findViewById(R.id.viewBackup);
        mBtnBackup = view.findViewById(R.id.btnBackup);
        mTvLastBackup = view.findViewById(R.id.txtLastBackup);
        mTvAutoBackupInfo = view.findViewById(R.id.tvAutoBackupInfor);
        mTvBackupViaNetwork = view.findViewById(R.id.tvBackupOverInfor);
        mLayoutAutoBackup = view.findViewById(R.id.settingAutoBackup);
        mLayoutBackupViaNetwork = view.findViewById(R.id.settingOverBackup);
        mBtnBackup.setOnClickListener(this);
        mLayoutBackupViaNetwork.setOnClickListener(this);
        mLayoutAutoBackup.setOnClickListener(this);
        if (mPref != null) {
            setLastBackup(mPref.getLong(SharedPrefs.KEY.BACKUP_LAST_TIME, 0l));
        }
        int autoBackupSelected = mPref.getInt(SharedPrefs.KEY.BACKUP_SETTING_AUTO_TYPE, 0);
        updateAutoBackupView(autoBackupSelected);
        boolean isWifiOnly = mPref.getBoolean(SharedPrefs.KEY.BACKUP_SETTING_NETWORK_ONLY_WIFI, true);
        updateNetworkBackupView(isWifiOnly ? 0 : 1);
        final View icQuestion = view.findViewById(R.id.icQuestionBackup);
        icQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // add tool tip hear
                View appbar = getView().findViewById(R.id.appBar);
                Util.showDialogExplain(mParentActivity, getString(R.string.bk_message_des), icQuestion.getX(), viewBackup.getY() + icQuestion.getY() + appbar.getHeight(), icQuestion);
            }
        });

        blurView.setVisibility(View.GONE);
    }

    private void setLastBackup(long time) {
        if (mTvLastBackup != null) {
            if (time == 0) {
                mTvLastBackup.setText(getString(R.string.bk_nerver));
            } else {
                mTvLastBackup.setText(formatCommonTime(time, System.currentTimeMillis(), getResources()).trim());
            }
        }
    }

    public static String formatCommonTime(long mTime, long currentTime, Resources res) {
        SimpleDateFormat inDay = new SimpleDateFormat(SDF_IN_DAY);
        SimpleDateFormat inYear = new SimpleDateFormat(SDF_IN_YEAR);
        SimpleDateFormat othYear = new SimpleDateFormat(SDF_OTH_YEAR);
        Calendar currentCal = Calendar.getInstance();
        currentCal.setTimeInMillis(currentTime);
        Calendar lastSeenCal = Calendar.getInstance();
        lastSeenCal.setTimeInMillis(mTime);
        int currentDay = currentCal.get(Calendar.DAY_OF_YEAR);
        int lastSeenDay = lastSeenCal.get(Calendar.DAY_OF_YEAR);
        int currentMonth = currentCal.get(Calendar.MONTH);
        int lastSeenMonth = lastSeenCal.get(Calendar.MONTH);
        int currentYear = currentCal.get(Calendar.YEAR);
        int lastSeenYear = lastSeenCal.get(Calendar.YEAR);

        if (lastSeenYear == currentYear) {
            if (lastSeenMonth == currentMonth) {
                if (lastSeenDay == currentDay) {
                    return res.getString(R.string.today) + ", " + inDay.format(mTime);
                } else {
                    int dayOfYear = currentDay - lastSeenDay;
                    if (dayOfYear == 1) {// Hom qua
                        return res.getString(R.string.yesterday) + ", " + inDay.format(mTime);
                    } else {
                        return inYear.format(mTime) + ", " + inDay.format(mTime);
                    }
                }
            } else {
                return inYear.format(mTime) + ", " + inDay.format(mTime);
            }
        } else {
            return othYear.format(mTime) + ", " + inDay.format(mTime);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == null) return;
        if (view.getId() == R.id.icBackToolbar) {
            mParentActivity.onBackPressed();
        } else if (view.getId() == R.id.btnBackup) {
            if (NetworkHelper.isConnectInternet(getContext())) {
                doBackup();
            } else {
                Toast.makeText(ApplicationController.self(), getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
            }
        } else if (view.getId() == R.id.settingAutoBackup) {
            int prev_select = mPref.getInt(SharedPrefs.KEY.BACKUP_SETTING_AUTO_TYPE, BackupUtils.BACKUP_AUTO_TYPE_OFF);
//            DialogBackupSettingSelect.newInstance(this, DialogBackupSettingSelect.TYPE_AUTO_BACKUP, prev_select).show(getChildFragmentManager(), DialogBackupSettingSelect.TAG);
            KhDialogRadioSelect dialogRadioSelect = KhDialogRadioSelect.newInstance(KhDialogRadioSelect.AUTO_BACKUP_TYPE, prev_select, R.string.kh_setting_auto_backup);
            dialogRadioSelect.setSelectListener(new BaseDialogKhFragment.DialogListener() {
                @Override
                public void dialogRightClick(int value) {
                    mPref.edit().putInt(SharedPrefs.KEY.BACKUP_SETTING_AUTO_TYPE, value).apply();
                    updateAutoBackupView(value);
                    blurView.setVisibility(View.GONE);
                }

                @Override
                public void dialogLeftClick() {
                    blurView.setVisibility(View.GONE);
                }
            });
            dialogRadioSelect.setDialogListener(new BaseDialogKhFragment.IDialogListener() {
                @Override
                public void dialogDismiss() {
                    blurView.setVisibility(View.GONE);
                }

                @Override
                public void dialogCancel() {
                    blurView.setVisibility(View.GONE);
                }
            });
            dialogRadioSelect.show(getChildFragmentManager(), KhDialogRadioSelect.TAG);
            blurView.setVisibility(View.VISIBLE);
        } else if (view.getId() == R.id.settingOverBackup) {
            int prev_select = mPref.getBoolean(SharedPrefs.KEY.BACKUP_SETTING_NETWORK_ONLY_WIFI, true) ? 0 : 1;
//            DialogBackupSettingSelect.newInstance(this, DialogBackupSettingSelect.TYPE_NETWORK_BACKUP, prev_select).show(getChildFragmentManager(), DialogBackupSettingSelect.TAG);
            KhDialogRadioSelect dialogRadioSelect = KhDialogRadioSelect.newInstance(KhDialogRadioSelect.BACKUP_VIA_NETWORK_TYPE, prev_select, R.string.kh_setting_backup_over);
            dialogRadioSelect.setSelectListener(new BaseDialogKhFragment.DialogListener() {
                @Override
                public void dialogRightClick(int value) {
                    boolean isWifiOnly = (value == 0);
                    mPref.edit().putBoolean(SharedPrefs.KEY.BACKUP_SETTING_NETWORK_ONLY_WIFI, isWifiOnly).apply();
                    updateNetworkBackupView(value);
                    blurView.setVisibility(View.GONE);
                }

                @Override
                public void dialogLeftClick() {
                    blurView.setVisibility(View.GONE);
                }
            });
            dialogRadioSelect.setDialogListener(new BaseDialogKhFragment.IDialogListener() {
                @Override
                public void dialogDismiss() {
                    blurView.setVisibility(View.GONE);
                }

                @Override
                public void dialogCancel() {
                    blurView.setVisibility(View.GONE);
                }
            });
            dialogRadioSelect.show(getChildFragmentManager(), KhDialogRadioSelect.TAG);
            blurView.setVisibility(View.VISIBLE);
        }
    }

    private void doBackup() {
        if (!isBackingUp) {
            isCancelByUser = false;
            mTvLastBackup.setText(mApplication.getString(R.string.sync_contact_progress, "0%"));
            BackupManager.doBackup(this, syncDesktop);
        } else {
            isCancelByUser = true;
            BackupManager.cancelBackup();
            setLastBackup(mPref.getLong(SharedPrefs.KEY.BACKUP_LAST_TIME, 0l));
            resetStateViewBackup();
        }
    }

    public boolean isBackupInProgress() {
        return BackupManager.isBackupInProgress();
    }

    @Override
    public void onStartBackup() {
        //show backup dialog progress
//        mDialogProgress = new DialogBackupProgress(getActivity());
//        mDialogProgress.show();
        isBackingUp = true;
        backupProgressBar.setVisibility(View.VISIBLE);
        mBtnBackup.setText(R.string.cancel);
//        mBtnBackup.setTextColor(ContextCompat.getColor(getContext(), R.color.v5_text));
//        mBtnBackup.setBackgroundColorAndPress(R.color.v5_bg_cancel_btn, R.color.v5_cancel_press);
//        mBtnBackup.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.v5_cancel_press));
//        mBtnBackup.setBackgroundColorRound(ContextCompat.getColor(getContext(), R.color.v5_cancel));

    }

    @Override
    public void onBackupProgress(int percent) {
        if (getActivity() == null) return;
        // update dialog progress
//        backupProgressBar.setSmoothProgress(percent);
    }

    @Override
    public void onBackupUploadProgress(int percent) {
        if (!isCancelByUser && mApplication != null) {
            if (backupProgressBar != null)
                backupProgressBar.setSmoothProgress(percent);
            if (mTvLastBackup != null)
                mTvLastBackup.setText(mApplication.getString(R.string.sync_contact_progress, percent + "%"));
        }
    }

    @Override
    public void onBackupComplete() {
        if (!isCancelByUser) {

            //dismiss backup dialog
            resetStateViewBackup();
            //show toast Backup successfully
//            mBtnBackup.setBackgroundColorRound(ContextCompat.getColor(getContext(), R.color.v5_main_color));
            ToastUtils.showToastSuccess(getContext(), getString(R.string.bk_backup_successfully));
            long backupLastTime = System.currentTimeMillis();
            setLastBackup(backupLastTime);

            if (mPref != null) {
                mPref.edit().putLong(SharedPrefs.KEY.BACKUP_LAST_TIME, backupLastTime).apply();
            }
        }
    }

    @Override
    public void onBackupFail(String message) {
        if (!isCancelByUser) {

            //dismiss backup dialog
            resetStateViewBackup();
            setLastBackup(mPref.getLong(SharedPrefs.KEY.BACKUP_LAST_TIME, 0l));
            //show toast Backup fail
            if (BuildConfig.DEBUG) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
            showBackupFail();
        }
        BackupManager.sendIQBackUpError(1);
    }

    @Override
    public void onBackupFail(int code) {
        if (!isCancelByUser) {
            resetStateViewBackup();
            setLastBackup(mPref.getLong(SharedPrefs.KEY.BACKUP_LAST_TIME, 0l));
            if (code == BackupUtils.BACKUP_CODE_NO_MESSAGES) {
                BackupManager.sendIQBackUpError(0);
                DialogMessage dialogMessage = new DialogMessage((BaseSlidingFragmentActivity) getActivity(), true);
                dialogMessage.setMessage(getString(R.string.bk_no_message_des));
                dialogMessage.setLabel(getString(R.string.bk_no_messsages));
                dialogMessage.show();
            } else {
                BackupManager.sendIQBackUpError(1);
                if (code == BackupUtils.BACKUP_CODE_FAIL_UPLOAD) {
                    Toast.makeText(ApplicationController.self(), getString(R.string.e604_error_connect_server), Toast.LENGTH_LONG).show();
                } else if (code == BackupUtils.BACKUP_CODE_FAIL_NOT_ENOUGH_MEMORY) {
                    Toast.makeText(ApplicationController.self(), getString(R.string.not_enough_space), Toast.LENGTH_LONG).show();
                }
                showBackupFail();
            }
        }
    }

    private void resetStateViewBackup() {
        isBackingUp = false;
        backupProgressBar.resetProgress();
        backupProgressBar.setVisibility(View.GONE);
        mBtnBackup.setText(R.string.bk_backup);
//        mBtnBackup.setTextColor(ContextCompat.getColor(getContext(), R.color.v5_text_7));
//        mBtnBackup.setBackgroundColorRound(ContextCompat.getColor(getContext(), R.color.v5_main_color));
//        mBtnBackup.setBackgroundColorAndPress(ContextCompat.getColor(getContext(), R.color.v5_main_color),
//                ContextCompat.getColor(getContext(), R.color.v5_main_color_press));
    }

    @Override
    public void onBackupSelected(int type, int postion) {
        if (type == DialogBackupSettingSelect.TYPE_AUTO_BACKUP) {
            if (mPref != null) {
                mPref.edit().putInt(SharedPrefs.KEY.BACKUP_SETTING_AUTO_TYPE, postion).apply();
                updateAutoBackupView(postion);
            }
        } else if (type == DialogBackupSettingSelect.TYPE_NETWORK_BACKUP) {
            if (mPref != null) {
                boolean isWifiOnly = (postion == 0);
                mPref.edit().putBoolean(SharedPrefs.KEY.BACKUP_SETTING_NETWORK_ONLY_WIFI, isWifiOnly).apply();
                updateNetworkBackupView(postion);
            }
        }
    }

    private void updateAutoBackupView(int select) {
        if (mTvAutoBackupInfo == null) return;
        if (select == BackupUtils.BACKUP_AUTO_TYPE_OFF) {
            mTvAutoBackupInfo.setText(getString(R.string.off));
            mTvAutoBackupInfo.setTextColor(getResources().getColor(R.color.white_50));
        } else if (select == BackupUtils.BACKUP_AUTO_TYPE_DAILY) {
            mTvAutoBackupInfo.setText(getString(R.string.kh_setting_backup_daily_value));
            mTvAutoBackupInfo.setTextColor(getResources().getColor(R.color.white_50));
        } else if (select == BackupUtils.BACKUP_AUTO_TYPE_WEEKLY) {
            mTvAutoBackupInfo.setText(getString(R.string.kh_setting_backup_weekly_value));
            mTvAutoBackupInfo.setTextColor(getResources().getColor(R.color.white_50));
        } else if (select == BackupUtils.BACKUP_AUTO_TYPE_MONTHLY) {
            mTvAutoBackupInfo.setText(getString(R.string.kh_setting_backup_monthly_value));
            mTvAutoBackupInfo.setTextColor(getResources().getColor(R.color.white_50));
        }
    }

    private void updateNetworkBackupView(int select) {
        if (mTvBackupViaNetwork == null) return;
        if (select == 0) {
            mTvBackupViaNetwork.setText(getString(R.string.kh_setting_backup_wifi));
        } else if (select == 1) {
            mTvBackupViaNetwork.setText(getString(R.string.kh_setting_backup_wifi_or_mobile_network));
        }
    }

    private void showBackupFail() {
        if (getActivity() == null) return;
        DialogConfirm dialogConfirm = new DialogConfirm(getActivity(), false);
        dialogConfirm.setLabel(getString(R.string.bk_backup_fail));
        dialogConfirm.setMessage(getString(R.string.bk_backup_fail_des));
        dialogConfirm.setNegativeLabel(getString(R.string.cancel));
        dialogConfirm.setPositiveLabel(getString(R.string.retry));
        dialogConfirm.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                BackupManager.clearBackupFailData();
            }
        });
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                //retry
                if (NetworkHelper.isConnectInternet(getContext())) {
                    doBackup();
                } else {
                    Toast.makeText(ApplicationController.self(), getString(R.string.no_connectivity_check_again), Toast.LENGTH_LONG).show();
                }
            }
        });
        dialogConfirm.show();
    }
}
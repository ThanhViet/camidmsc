package com.metfone.selfcare.module.keeng.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.helper.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MusicHomeModel implements Serializable {

    /**
     * type = 1 :  hiển thị banner Flashhot
     * type = 2 :  hiển thị  PlaylisMix
     * type = 3 :  hiển thị danh sách MV_HOT
     * type = 4 :  hiển thị KeengTV
     * type = 5 :  hiển thị danh sách MV_YouTube
     * type = 6 :  hiển thị danh sách Playlist Hot
     * type = 7 :  hiển thị danh sách AlBum Hot
     * type = 8 :  hiển thị danh sách TOP_HIT
     * type = 9 :  hiển thị danh sách RANK
     * type = 10 :  hiển thị danh sách Chủ đề
     * type = 11 :  hiển thị danh sách Bài hát Hot
     * type = 12 :  hiển thị danh sách ca sỹ Hot
     */
    @SerializedName("type")
    private int type;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;

    @SerializedName("flashhot")
    private List<AllModel> flashHot;

    @SerializedName("video_hot")
    private List<AllModel> lstVideos;

    @SerializedName("videoYoutube")
    private List<AllModel> lstYoutubes;

    @SerializedName("play_hot")
    private List<PlayListModel> lstPlaylists;

    @SerializedName("album_hot")
    private List<AllModel> lstAlbums;

    @SerializedName("top_hit")
    private List<Topic> lstTopHits;

    @SerializedName("bxh")
    private List<RankModel> lstCharts;

    @SerializedName("topic_hot")
    private List<Topic> lstTopics;

    @SerializedName("song_hot")
    private List<AllModel> lstSongs;

    @SerializedName("singer_hot")
    private List<Topic> lstSingers;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String discription) {
        this.description = discription;
    }

    public List<AllModel> getFlashHot() {
        if (flashHot == null) flashHot = new ArrayList<>();
        return flashHot;
    }

    public List<AllModel> getVideos() {
        if (lstVideos == null) lstVideos = new ArrayList<>();
        return lstVideos;
    }

    public List<AllModel> getYoutubes() {
        if (lstYoutubes == null) lstYoutubes = new ArrayList<>();
        return lstYoutubes;
    }

    public List<PlayListModel> getPlaylists() {
        if (lstPlaylists == null) lstPlaylists = new ArrayList<>();
        return lstPlaylists;
    }

    public List<AllModel> getAlbums() {
        if (lstAlbums == null) lstAlbums = new ArrayList<>();
        return lstAlbums;
    }

    public List<Topic> getTopHits() {
        if (lstTopHits == null) lstTopHits = new ArrayList<>();
        return lstTopHits;
    }

    public List<RankModel> getCharts() {
        if (lstCharts == null) lstCharts = new ArrayList<>();
        return lstCharts;
    }

    public List<Topic> getTopics() {
        if (lstTopics == null) lstTopics = new ArrayList<>();
        return lstTopics;
    }

    public List<AllModel> getSongs() {
        if (lstSongs == null) lstSongs = new ArrayList<>();
        return lstSongs;
    }

    public List<Topic> getSingers() {
        if (lstSingers == null) lstSingers = new ArrayList<>();
        return lstSingers;
    }

    public boolean isEmpty() {
        if (Constants.MUSIC_HOME_TYPE_FLASH_HOT == type && !getFlashHot().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_MIX == type) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_MV_HOT == type && !getVideos().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_MV_YOUTUBE == type && !getYoutubes().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_PLAYLIST_HOT == type && !getPlaylists().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_ALBUM_HOT == type && !getAlbums().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_TOP_HIT == type && !getTopHits().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_CHART == type && !getCharts().isEmpty()) {
            RankModel rankModel = getCharts().get(0);
            return rankModel == null || rankModel.isEmpty();
        } else if (Constants.MUSIC_HOME_TYPE_TOPIC == type && !getTopics().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_SONG_HOT == type && !getSongs().isEmpty()) {
            return false;
        } else if (Constants.MUSIC_HOME_TYPE_SINGER_HOT == type && !getSingers().isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MusicHomeModel{" +
                "type=" + type +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", flashHot=" + flashHot +
                ", lstVideos=" + lstVideos +
                ", lstYoutubes=" + lstYoutubes +
                ", lstPlaylists=" + lstPlaylists +
                ", lstAlbums=" + lstAlbums +
                ", lstTopHits=" + lstTopHits +
                ", lstCharts=" + lstCharts +
                ", lstTopics=" + lstTopics +
                ", lstSongs=" + lstSongs +
                ", lstSingers=" + lstSingers +
                '}';
    }
}

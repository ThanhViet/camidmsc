/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.adapter;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.movie.holder.BoxContentHolder;
import com.metfone.selfcare.module.movie.holder.CustomTopicHolder;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.holder.CinemaBannerHolder;
import com.metfone.selfcare.module.movienew.holder.CinemaPlayerHolder;
import com.metfone.selfcare.module.movienew.holder.CinemaWatchedByFriendHolder;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;

public class MoviePagerAdapter extends BaseAdapter<BaseAdapter.ViewHolder, MoviePagerModel> {

    private TabMovieListener.OnAdapterClick listener;
    private TabMovieListener.IPlayVideoNow mPlayVideoCallback;
    private RecyclerView.RecycledViewPool recycledPoolHeader;
    private RecyclerView.RecycledViewPool recycledPoolVertical;
    private RecyclerView.RecycledViewPool recycledPoolHorizontal;
    private String mNameWatchByFriend;
    private ArrayList<ViewHolder> viewHolders = new ArrayList<>();
    private LoadmoreListener.ILoadmoreAtPositionListener loadmoreAtPositionListener;
    private LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv mLoadmoreAtPositionRclvInsideRclv;

    public MoviePagerAdapter(Activity act, LoadmoreListener.ILoadmoreAtPositionListener loadmoreAtPositionListener,
                             LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv loadmoreAtPositionRclvInsideRclv) {
        super(act);
        recycledPoolHeader = new RecyclerView.RecycledViewPool();
        recycledPoolVertical = new RecyclerView.RecycledViewPool();
        recycledPoolHorizontal = new RecyclerView.RecycledViewPool();
        this.loadmoreAtPositionListener = loadmoreAtPositionListener;
        this.mLoadmoreAtPositionRclvInsideRclv = loadmoreAtPositionRclvInsideRclv;
    }

    public void setListener(TabMovieListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    public void setPlayNowListener(TabMovieListener.IPlayVideoNow iPlayVideoNow) {
        this.mPlayVideoCallback = iPlayVideoNow;
    }

    public void setNameWatchedByFriend(String nameWatchedByFriend) {
        mNameWatchByFriend = nameWatchedByFriend;
    }

    @Override
    public int getItemViewType(int position) {
        MoviePagerModel item = getItem(position);
        if (item != null) return item.getType();
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case MoviePagerModel.TYPE_SLIDER_BANNER: {
                CinemaBannerHolder holder = new CinemaBannerHolder(layoutInflater.inflate(R.layout.cinema_banner, parent, false), activity, listener, mPlayVideoCallback);
                return holder;
            }

            case MoviePagerModel.TYPE_SLIDER_BANNER_EMPTY: {
                CinemaBannerHolder holder = new CinemaBannerHolder(layoutInflater.inflate(R.layout.cinema_banner_empty, parent, false), activity, listener, mPlayVideoCallback);
                holder.startAnimation();
                return holder;
            }

            case MoviePagerModel.TYPE_AVAILABLE_NOW: {
                CinemaPlayerHolder holder = new CinemaPlayerHolder(layoutInflater.inflate(R.layout.cinema_available_now, parent, false), activity, listener, mPlayVideoCallback);
                return holder;
            }

            case MoviePagerModel.TYPE_WATCHED_BY_FRIEND: {
                CinemaWatchedByFriendHolder holder = new CinemaWatchedByFriendHolder(layoutInflater.inflate(R.layout.cinema_watched_by_friend,
                        parent, false), activity, listener, mNameWatchByFriend, mPlayVideoCallback);
                return holder;
            }
            case MoviePagerModel.TYPE_BOX_CONTENT:
            case MoviePagerModel.TYPE_BOX_HOT: {
                BoxContentHolder holder = new BoxContentHolder(layoutInflater.inflate(R.layout.cinema_simple_movie, parent, false),
                        activity, listener, viewType, new LoadmoreListener.ILoadmoreAtPositionListener() {
                    @Override
                    public void loadmoreAtPosition(int position, RecyclerView.Adapter adapter) {
                        loadmoreAtPositionListener.loadmoreAtPosition(position, adapter);
                    }
                });
                if (holder.getRecycler() != null)
                    holder.getRecycler().setRecycledViewPool(recycledPoolVertical);
                return holder;
            }
            case MoviePagerModel.TYPE_CUSTOM_TOPIC: {
                return new CustomTopicHolder(layoutInflater.inflate(R.layout.holder_recycler_view, parent, false),
                        activity, listener, new LoadmoreListener.ILoadmoreAtPositionListener() {
                    @Override
                    public void loadmoreAtPosition(int position, RecyclerView.Adapter adapter) {
                        loadmoreAtPositionListener.loadmoreAtPosition(position, adapter);
                    }
                }, new LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv() {
                    @Override
                    public void loadmoreAtPositionOfRclvInsideRclv(int position, RecyclerView.Adapter adapter) {
                        mLoadmoreAtPositionRclvInsideRclv.loadmoreAtPositionOfRclvInsideRclv(position, adapter);
                    }
                });
            }
            case MoviePagerModel.TYPE_BOX_WATCHED: {
                BoxContentHolder holder = new BoxContentHolder(layoutInflater.inflate(R.layout.cinema_continue, parent, false),
                        activity, listener, viewType, new LoadmoreListener.ILoadmoreAtPositionListener() {
                    @Override
                    public void loadmoreAtPosition(int position, RecyclerView.Adapter adapter) {
                        loadmoreAtPositionListener.loadmoreAtPosition(position, adapter);
                    }
                });
                if (holder.getRecycler() != null)
                    holder.getRecycler().setRecycledViewPool(recycledPoolHorizontal);
                return holder;
            }
        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        if (holder instanceof CinemaWatchedByFriendHolder) {
            ((CinemaWatchedByFriendHolder) holder).setNameWatched(mNameWatchByFriend);
        }
        holder.bindData(item, position);
        viewHolders.add(holder);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        int count = getItemCount();
        for (int i = 0; i < count; i++) {
            RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(i);
            if (viewHolder instanceof CinemaPlayerHolder) {
                ((CinemaPlayerHolder) viewHolder).stopVideo();
            }
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder instanceof CinemaPlayerHolder) {
            ((CinemaPlayerHolder) holder).stopVideo();
        }
    }

    public void updateData() {
        if (viewHolders != null && viewHolders.size() > 0) {
            for (int i = 0; i < viewHolders.size(); i++) {
                ViewHolder viewHolder = viewHolders.get(i);
                if (viewHolder instanceof CinemaPlayerHolder) {
                    ((CinemaPlayerHolder) viewHolder).stopVideo();
                }
            }
            viewHolders.clear();
        }
        notifyDataSetChanged();
    }

}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.tab_home.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.holder.content.ChannelDetailHolder;
import com.metfone.selfcare.holder.content.ComicDetailHolder;
import com.metfone.selfcare.holder.content.MovieDetailHolder;
import com.metfone.selfcare.holder.content.MusicDetailHolder;
import com.metfone.selfcare.holder.content.MusicPlaylistDetailHolder;
import com.metfone.selfcare.holder.content.NewsDetailHolder;
import com.metfone.selfcare.holder.content.TiinDetailHolder;
import com.metfone.selfcare.holder.content.VideoDetailHolder;
import com.metfone.selfcare.module.tab_home.holder.QuickContactDetailHolder;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.HomeContact;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;
import com.metfone.selfcare.util.Log;

public class TabHomeDetailAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {

    protected TabHomeListener.OnAdapterClick listener;
    private int parentViewType = TYPE_EMPTY;

    public TabHomeDetailAdapter(Activity act) {
        super(act);
    }

    public void setParentViewType(int parentViewType) {
        this.parentViewType = parentViewType;
    }

    public void setListener(TabHomeListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (parentViewType == TabHomeModel.TYPE_BOX_GRID_VIDEO)
            return TabHomeModel.TYPE_GRID_VIDEO;
        else if (parentViewType == TabHomeModel.TYPE_BOX_GRID_MUSIC)
            return TabHomeModel.TYPE_GRID_MUSIC;
        else if (parentViewType == TabHomeModel.TYPE_BOX_GRID_MOVIE)
            return TabHomeModel.TYPE_GRID_MOVIE;
        else if (parentViewType == TabHomeModel.TYPE_BOX_GRID_NEWS)
            return TabHomeModel.TYPE_GRID_NEWS;
        else if (parentViewType == TabHomeModel.TYPE_BOX_GRID_COMIC)
            return TabHomeModel.TYPE_GRID_COMIC;
        else if (parentViewType == TabHomeModel.TYPE_BOX_GRID_CHANNEL)
            return TabHomeModel.TYPE_GRID_CHANNEL;
        else if (parentViewType == TabHomeModel.TYPE_BOX_GRID_TIIN)
            return TabHomeModel.TYPE_GRID_TIIN;
        else if (item instanceof TabHomeModel) {
            return ((TabHomeModel) item).getType();
        } else if (item instanceof HomeContact) {
            return TabHomeModel.TYPE_QUICK_CONTACT;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TabHomeModel.TYPE_LARGE_VIDEO:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_large_video, parent, false), activity, listener);
            case TabHomeModel.TYPE_GRID_VIDEO:
                return new VideoDetailHolder(layoutInflater.inflate(R.layout.holder_grid_video, parent, false), activity, listener, TabHomeUtils.getWidthVideo());
            case TabHomeModel.TYPE_GRID_CHANNEL:
                return new ChannelDetailHolder(layoutInflater.inflate(R.layout.holder_grid_channel, parent, false), activity, listener, TabHomeUtils.getWidthChannelVideo());

            case TabHomeModel.TYPE_LARGE_MUSIC:
                return new MusicDetailHolder(layoutInflater.inflate(R.layout.holder_large_music, parent, false), activity, listener);
            case TabHomeModel.TYPE_GRID_MUSIC:
                return new MusicPlaylistDetailHolder(layoutInflater.inflate(R.layout.holder_grid_music_playlist, parent, false), activity, listener, TabHomeUtils.getWidthMusicPlaylist());

            case TabHomeModel.TYPE_LARGE_MOVIE:
                return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_large_movie, parent, false), activity, listener).setShowPoster(false);
            case TabHomeModel.TYPE_GRID_MOVIE:
                return new MovieDetailHolder(layoutInflater.inflate(R.layout.holder_grid_movie, parent, false), activity, listener, TabHomeUtils.getWidthPosterMovie()).setShowPoster(true);

            case TabHomeModel.TYPE_LARGE_NEWS:
                return new NewsDetailHolder(layoutInflater.inflate(R.layout.holder_large_news, parent, false), activity, listener);
            case TabHomeModel.TYPE_GRID_NEWS:
                return new NewsDetailHolder(layoutInflater.inflate(R.layout.holder_grid_news, parent, false), activity, listener, TabHomeUtils.getWidthNews());

            case TabHomeModel.TYPE_LARGE_COMIC:
                return new ComicDetailHolder(layoutInflater.inflate(R.layout.holder_large_comic, parent, false), activity, listener).setViewType(viewType);
            case TabHomeModel.TYPE_GRID_COMIC:
                return new ComicDetailHolder(layoutInflater.inflate(R.layout.holder_grid_comic, parent, false), activity, listener).setViewType(viewType);

            case TabHomeModel.TYPE_QUICK_CONTACT:
                return new QuickContactDetailHolder(layoutInflater.inflate(R.layout.holder_tab_home_quick_contact_detail, parent, false), listener);

            case TabHomeModel.TYPE_LARGE_TIIN:
                return new TiinDetailHolder(layoutInflater.inflate(R.layout.holder_large_tiin, parent, false), activity, listener);
            case TabHomeModel.TYPE_GRID_TIIN:
                return new TiinDetailHolder(layoutInflater.inflate(R.layout.holder_grid_tiin, parent, false), activity, listener, TabHomeUtils.getWidthTiin());

            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

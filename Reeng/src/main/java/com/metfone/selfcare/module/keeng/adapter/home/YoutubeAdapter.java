package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class YoutubeAdapter extends SectionListDataAdapter<AllModel> {

    public YoutubeAdapter(Context context, List<AllModel> itemsList, AbsInterface.OnItemListener onClick) {
        super(context, itemsList, onClick);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_media_youtube_home, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final AllModel item = getItem(position);
        if (item != null) {
            holder.setVisible(R.id.image, true);
            holder.setVisible(R.id.tvTitle, true);
            holder.setVisible(R.id.tvContent, true);
            holder.setVisible(R.id.layout_view_all, false);
            holder.setText(R.id.tvTitle, item.getName());
            holder.setText(R.id.tvContent, item.getSinger());
            View view = holder.getView(R.id.image);
            if (view instanceof ImageView)
                ImageBusiness.setVideo(item.getImage(), ((ImageView) view), position);
            holder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClick != null) {
                        item.setSource(MediaLogModel.SRC_HOT_HOME);
                        onClick.onItemClick(item);
                    }
                }
            });
        } else if (isCanViewAll()) {
            holder.setVisible(R.id.image, false);
            holder.setVisible(R.id.tvTitle, false);
            holder.setVisible(R.id.tvContent, false);
            holder.setVisible(R.id.layout_view_all, true);
            holder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClick != null)
                        onClick.onHeaderClick(view, getTag());
                }
            });
        }
    }

    @Override
    public AllModel getItem(int position) {
        return (null != getItemsList() && getItemsList().size() > (position + 1) ? getItemsList().get(position + 1) : null);
    }

    @Override
    public int getItemCount() {
        if (isCanViewAll() && getItemsList() != null && getItemsList().size() >= (Constants.NO_MIN_ITEMS - 1))
            return getItemsList().size();
        return (null != getItemsList() ? getItemsList().size() - 1 : 0);
    }
}
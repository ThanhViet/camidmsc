package com.metfone.selfcare.module.home_kh.onboarding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.onboarding.KhOnBoardingActivity;
import com.metfone.selfcare.module.home_kh.tab.FragmentTabHomeKh;

public abstract class FrmKhOnBoarding extends Fragment {

    public static final int SIZE_ON_BOARDING = 3;

    protected abstract int getLayoutId();

    protected abstract int getPageIndex();


    protected KhOnBoardingActivity mActivity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mActivity = (KhOnBoardingActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView btn = view.findViewById(R.id.btnNext);
        if (btn != null) {
            btn.setOnClickListener(v -> {
                int pageIndex = getPageIndex();
                if (pageIndex == (SIZE_ON_BOARDING - 1)) {
                    mActivity.finish();
                } else {
                    mActivity.setCurrentPage(getPageIndex() + 1);
                }
            });
        }
    }

}

package com.metfone.selfcare.module.home_kh.fragment.setting.share;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;
import com.metfone.selfcare.ui.roundview.RoundTextView;

import java.util.ArrayList;
import java.util.List;

public class ShareAndGetMoreAdapter extends RecyclerView.Adapter<ShareAndGetMoreAdapter.Holder> {

    private List<PhoneNumber> listData = new ArrayList<>();
    private ApplicationController mApplication;
    private Context mContext;
    private Resources mRes;

    private IOnItemClickListener listener;

    public ShareAndGetMoreAdapter(List<PhoneNumber> phoneNumbers, Context context, IOnItemClickListener listener) {
        this.listData = phoneNumbers;
        this.mContext = context;
        this.mRes = mContext.getResources();
        this.mApplication = (ApplicationController) mContext.getApplicationContext();
        this.listener = listener;
    }

    public void addData(List<PhoneNumber> data) {
        if (data == null || data.isEmpty()) {
            return;
        }
        if (listData == null) {
            listData = new ArrayList<>();
        }
        this.listData.clear();
        listData.addAll(data);
        notifyDataSetChanged();
    }

    public void add(List<PhoneNumber> data) {
        if (data == null || data.isEmpty()) {
            return;
        }
        if (listData == null) {
            listData = new ArrayList<>();
        }
        listData.addAll(data);
        notifyItemRangeInserted(getItemCount() - data.size(), data.size());
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_share_and_get_more, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    abstract static class Holder extends RecyclerView.ViewHolder {

        public Holder(@NonNull View itemView) {
            super(itemView);
        }

        public abstract void onBind(int position);

        protected static View inflate(@NonNull ViewGroup parent, @LayoutRes int layoutRes) {
            return LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
        }
    }

    class ViewHolder extends ShareAndGetMoreAdapter.Holder {
        private AppCompatTextView tvName;
        private RoundTextView btnSend;
        private FrameLayout mFrAvatar;
        private RoundedImageView mImgAvatar;
        private TextView mTvwAvatar;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            btnSend = itemView.findViewById(R.id.btn_send);
            mFrAvatar = itemView.findViewById(R.id.item_contact_view_avatar_frame);
            mImgAvatar = itemView.findViewById(R.id.item_contact_view_avatar_circle);
            mTvwAvatar = itemView.findViewById(R.id.contact_avatar_text);
        }

        @Override
        public void onBind(int position) {
            PhoneNumber entry = listData.get(position);
            tvName.setText(entry.getName());
            int size = (int) mRes.getDimension(R.dimen.dimen50dp);
            mApplication.getAvatarBusiness().setPhoneNumberAvatar(mImgAvatar, mTvwAvatar, entry, size);

            btnSend.setOnClickListener(v -> {
                if (listener != null) {
                    listener.sendClick(entry, position);
                }
            });
        }
    }

    public interface IOnItemClickListener {
        void itemClick(int position);
        void sendClick(PhoneNumber entry, int position);
    }
}

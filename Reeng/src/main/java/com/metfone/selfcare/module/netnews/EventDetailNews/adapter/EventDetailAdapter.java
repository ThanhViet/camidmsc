package com.metfone.selfcare.module.netnews.EventDetailNews.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;

import java.util.List;

/**
 * Created by HaiKE on 8/19/17.
 */

public class EventDetailAdapter extends BaseQuickAdapter<NewsModel, BaseViewHolder> {

    private Context context;
    private AbsInterface.OnEventDetailListener onItemListener;

    public EventDetailAdapter(Context context, int id, List<NewsModel> datas, AbsInterface.OnEventDetailListener onItemListener) {
        super(id, datas);
        this.context = context;
        this.onItemListener = onItemListener;
    }

    @Override
    protected void convert(BaseViewHolder holder, NewsModel model) {
        try {
            if (model != null) {
                if (holder.getView(R.id.tv_title) != null)
                    holder.setText(R.id.tv_title, model.getTitle());

                if (holder.getView(R.id.tv_datetime) != null) {
                    holder.setVisible(R.id.tv_datetime, true);
                    if (model.getTimeStamp() > 0) {
                        holder.setText(R.id.tv_datetime, DateUtilitis.calculateDate(mContext, model.getTimeStamp()));
                    } else {
                        holder.setText(R.id.tv_datetime, Html.fromHtml(model.getDatePub()));
                    }
                }
                if (holder.getView(R.id.tv_desc) != null) {
                    holder.setVisible(R.id.tv_desc, true);
                    holder.setText(R.id.tv_desc, model.getShapo());
                }

                if (holder.getView(R.id.tv_category) != null)
                    holder.setText(R.id.tv_category, TextUtils.isEmpty(model.getSourceName()) ? model.getCategory() : model.getSourceName());

                if (holder.getView(R.id.iv_cover) != null) {
                    ImageBusiness.setImageNew(model.getImage169(), (ImageView) holder.getView(R.id.iv_cover));
                }
                if (holder.getView(R.id.button_option) != null) {
                    holder.setOnClickListener(R.id.button_option, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemListener != null) {
                                onItemListener.onItemClickMore(model);
                            }
                        }
                    });
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemListener.onItemClick(model);
                    }
                });
            }
        } catch (Exception ex) {

        }
    }


}

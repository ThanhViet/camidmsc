package com.metfone.selfcare.module.selfcare.utils;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.google.i18n.phonenumbers.Phonenumber;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.module.keeng.utils.Utilities;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.model.SCBundle;
import com.metfone.selfcare.module.selfcare.model.SCDeeplink;
import com.metfone.selfcare.module.selfcare.model.SCLoyaltyModel;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class SCUtils {

    static String currentPhoneNumber;
    static SCSubListModel currentAccount;
    static SCLoyaltyModel currentLoyalty;

    public static SCLoyaltyModel getCurrentLoyalty() {
        return currentLoyalty;
    }

    public static void setCurrentLoyalty(SCLoyaltyModel currentLoyalty) {
        SCUtils.currentLoyalty = currentLoyalty;
    }

    public static SCSubListModel getCurrentAccount() {
        return currentAccount;
    }

    public static void setCurrentAccount(SCSubListModel currentAccount) {
        SCUtils.currentAccount = currentAccount;
    }

    public static void setCurrentPhoneNumber(String currentPhoneNumber) {
        SCUtils.currentPhoneNumber = currentPhoneNumber;
    }

    public static String getPhoneNumber() {
        ApplicationController mApplication = ApplicationController.self();
        ReengAccountBusiness accountBusiness = mApplication.getReengAccountBusiness();
        return accountBusiness.getJidNumber();

//        return "2095703167";
    }

    public static String getLanguage() {
//        return "en";
        String language = LocaleManager.getLanguage(ApplicationController.self());
        return language;
    }

    public static void doChatSupport(BaseSlidingFragmentActivity context) {
        DeepLinkHelper.getInstance().openSchemaLink(context, "mytel://official?ref=mytel&name=Mytel&avatar=1234");
    }

    public static void deepLinkClick(Context context, SCDeeplink item) {
        switch (item.getDeepLink()) {
            case SCConstants.DEEPLINK.ALL_PACKAGE:
                doSelfCareDetail(context, SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_PACKAGE));
                break;
            case SCConstants.DEEPLINK.ALL_VAS:
                doSelfCareDetail(context, SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_SERVICES));
                break;
            case SCConstants.DEEPLINK.CHARGING_HISTORY:
                doSelfCareDetail(context, SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL, new SCBundle(1));
                break;
            case SCConstants.DEEPLINK.STORE_CENTER:
                doSelfCareDetail(context, SCConstants.SELF_CARE.TAB_STORES, null);
                break;
        }
    }

    private static void doSelfCareDetail(Context mActivity, int type, Object data) {
        if (mActivity != null) {
            Intent intent = new Intent(mActivity, TabSelfCareActivity.class);
            intent.putExtra(Constants.KEY_TYPE, type);
            intent.putExtra(Constants.KEY_DATA, (Serializable) data);
            mActivity.startActivity(intent);
        }
    }

    public static String numberFormatNoDecimal(double value) {
        if (value > 100000) {
            return Utilities.formatNumber((long) value);
        }
        DecimalFormat df2 = new DecimalFormat("#,###,###,##0");
        return df2.format(value);
    }

    public static String numberFormat(double value) {
//        DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
        DecimalFormat df2 = new DecimalFormat("###,###.###", new DecimalFormatSymbols(Locale.US)); //#,###,###,##0
        return df2.format(value);
    }

    public static String numberDistanceFormat(double value) {
        DecimalFormat df2 = new DecimalFormat("#,###,###,##0.0");
        return df2.format(value);
    }

    public static int getRankResource(String level) {
        if (level.toUpperCase().equals(SCConstants.RANK_TYPE.SILVER))
            return R.drawable.ic_sc_silver_rank;
        else if (level.toUpperCase().equals(SCConstants.RANK_TYPE.GOLD))
            return R.drawable.ic_sc_gold_rank;
        else if (level.toUpperCase().equals(SCConstants.RANK_TYPE.PLATINUM))
            return R.drawable.ic_sc_platinum_rank;
        else if (level.toUpperCase().equals(SCConstants.RANK_TYPE.DIAMOND))
            return R.drawable.ic_sc_diamond_rank;
        else
            return R.drawable.ic_sc_welcome_rank;
    }

    public static String formatPhoneNumber(String phone)
    {
        if(!TextUtils.isEmpty(phone))
        {
            Phonenumber.PhoneNumber phoneNumber = PhoneNumberHelper.getInstant().getPhoneNumberProtocol(ApplicationController.self().getPhoneUtil(), phone, "LA");
            return String.valueOf(phoneNumber.getNationalNumber());

//            if(phone.startsWith("+856"))
//                phone = phone.replace("+856", "0");
//            else if(phone.startsWith("856"))
//                phone = phone.replace("856", "0");
////            else if(!phone.startsWith("0"))
////                phone = "0" + phone;

        }
        else
        {
            return "";
        }
    }

    public static String formatPhoneNumber856(String phone)
    {
        if(!TextUtils.isEmpty(phone))
        {
            Phonenumber.PhoneNumber phoneNumber = PhoneNumberHelper.getInstant().getPhoneNumberProtocol(ApplicationController.self().getPhoneUtil(), phone, "LA");
            return phoneNumber.getCountryCode() + "" + phoneNumber.getNationalNumber();

//            if(phone.startsWith("856"))
//            {
//                phone = "+" + phone;
//            }
//            else if(!phone.startsWith("+856"))
//            {
//                phone = "+856" + phone;
//            }
//
//            return phone;
        }
        else
        {
            return "";
        }
    }
}

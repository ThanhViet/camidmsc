/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/25
 */

package com.metfone.selfcare.module.security.model;

import com.google.gson.annotations.SerializedName;

public class LogModel {
    /*
    101	- Tin nhắn bị chặn theo chế độ học máy
    102	- Tin nhắn bị chặn là tin spam theo người dùng cấu hình (blacklist, ngoài danh bạ)
    103	- Tin nhắn bị chặn là tin bị chặn theo chế độ chống làm phiền
    104	- Tin nhắn bị chặn là tin bị chặn theo thời gian
    105	- Tin nhắn bị chặn theo chế độ tường lửa
    106	- Tin nhắn cho gửi đi nhưng đầu số nhận tin nằm trong danh sách cấu hình đầu số dịch vụ cần chặn
        của chế độ tường lửa hoặc là đầu số ở nước ngoài. (nằm trong whitelist tường lửa của người dùng
        hoặc người dùng đăng ký dịch vụ tường lửa nhưng đang tắt tường lửa)
    */
    @SerializedName("id")
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("content")
    private String content;

    @SerializedName("count")
    private String count;

    @SerializedName("dest")
    private String dest;

    @SerializedName("ref")
    private String ref;

    @SerializedName("ref_num")
    private String refNum;

    @SerializedName("seq_num")
    private String seqNum;

    @SerializedName("time")
    private String time;

    @SerializedName("src")
    private String src;

    private boolean isSelected;

    private boolean isReaded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getIdInt() {
        try {
            return Long.parseLong(id);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTypeInt() {
        try {
            return Integer.parseInt(type);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getCountInt() {
        try {
            return Integer.parseInt(count);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public int getRefInt() {
        try {
            return Integer.parseInt(ref);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getRefNum() {
        return refNum;
    }

    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }

    public int getRefNumInt() {
        try {
            return Integer.parseInt(refNum);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(String seqNum) {
        this.seqNum = seqNum;
    }

    public int getSeqNumInt() {
        try {
            return Integer.parseInt(seqNum);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getTimeInt() {
        try {
            return Long.parseLong(time);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public boolean isLogSpam() {
        return "102".equals(type);
    }

    public boolean isLogFirewall() {
        return "105".equals(type);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isReaded() {
        return isReaded;
    }

    public void setReaded(boolean readed) {
        isReaded = readed;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", type=" + type +
                ", content='" + content + '\'' +
                ", count=" + count +
                ", dest='" + dest + '\'' +
                ", ref=" + ref +
                ", refNum=" + refNum +
                ", seqNum=" + seqNum +
                ", time='" + time + '\'' +
                ", src='" + src + '\'' +
                '}';
    }
}

package com.metfone.selfcare.module.movienew.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.github.rubensousa.previewseekbar.PreviewBar;
import com.github.rubensousa.previewseekbar.PreviewLoader;
import com.github.rubensousa.previewseekbar.exoplayer.PreviewTimeBar;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.metfone.esport.common.Common;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.module.movie.event.RequestPermissionBright;
import com.metfone.selfcare.module.movie.event.RequestPermissionVolume;
import com.metfone.selfcare.module.movienew.listener.TrailerPlayerListener;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnSwipeListener;
import com.metfone.selfcare.ui.view.tab_video.ProgressTimeBar;
import com.metfone.selfcare.ui.view.tab_video.VideoPlayerView;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Field;
import java.util.Formatter;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.android.exoplayer2.Player.STATE_ENDED;

public class TrailerFullScreenPlayerDialog extends Dialog {
    private final String TAG = "TrailerFullScreenPlayerDialog";
    @BindView(R.id.sliding_layout)
    RelativeLayout slidingUpPanel;
    @BindView(R.id.frame_video)
    FrameLayout frameVideo;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.seekbar_brightness)
    SeekBar seekbar_brightness;
    @BindView(R.id.seekbar_volume)
    SeekBar seekbar_volumme;
    @BindView(R.id.timePreview)
    TextView timePreview;
    @BindView(R.id.progressNew)
    PreviewTimeBar progressNew;
    @BindView(R.id.progressBarBottom)
    ProgressTimeBar progressBarBottom;
    @BindView(R.id.play)
    ImageView play;
    @BindView(R.id.layout_video)
    RelativeLayout layout_video;
    @BindView(R.id.next)
    ImageView next;
    @BindView(R.id.previous)
    ImageView previous;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.duration)
    TextView tvDuration;

    private BaseSlidingFragmentActivity activity;
    private TrailerPlayerListener trailerPlayerListener;
    private FullPlayerListener.ProviderFullScreen providerFullScreen;
    private SimpleExoPlayer mPlayer;
    private VideoPlayerView playerView;

    private boolean isEndVideo = false;
    private boolean isDismiss = false;
    private AudioManager mAudioManager;
    private int currentVolume;
    private int maxVolume;
    private boolean isUserSwipeUp = false;
    private boolean isLandscape = true;
    private boolean isPlaying = true;
    private String title;
    private String url;
    private long currentTime;
    private long DURATION_HIDE = 3000;
    private long DELTA_TIME = 15000;
    private StringBuilder formatBuilder;
    private Formatter formatter;
    private boolean canChangeVolume = false;
    private OrientationEventListener orientationEventListener;

    private GestureDetector gestureDetector;
    private int lastOrientation;
    private Player.EventListener eventListener = new Player.DefaultEventListener() {
        private int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
            if (currentState != STATE_ENDED && playbackState == STATE_ENDED) {
                isPlaying = false;
                isEndVideo = true;
                handlerEnd();
            }
            currentState = playbackState;
        }
    };
    private Runnable runnableWhenPortrait = () -> {
//        dismiss();
    };
    private Runnable runnableEnableRotateSensor = () -> {
        enableRotateSensor();
    };
    private Runnable runnableTime = new Runnable() {
        @Override
        public void run() {
            if (progressNew != null && mPlayer != null) {
                updateProgressUI(mPlayer.getCurrentPosition(), mPlayer.getDuration());
                progressNew.postDelayed(this, 1000);
            }
        }
    };
    private Runnable runnableHideView = new Runnable() {
        @Override
        public void run() {
            if (layout_video != null) {
                layout_video.setVisibility(View.GONE);
            }
            if (progressBarBottom != null) {
                progressBarBottom.setVisibility(View.VISIBLE);
            }
        }
    };

    public TrailerFullScreenPlayerDialog(BaseSlidingFragmentActivity activity) {
        super(activity, R.style.video_full_screen_dialog);
        this.activity = activity;
        initOrientationListener();
    }

    public void setData(String title, String url, long currentTime) {
        this.title = title;
        this.url = url;
        this.currentTime = currentTime;
    }

    public void setTrailerPlayerListener(TrailerPlayerListener trailerPlayerListener) {
        this.trailerPlayerListener = trailerPlayerListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView(R.layout.dialog_trailer_full_screen_player);
        ButterKnife.bind(this);
        setLayoutParam();
        mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        if (mAudioManager != null) {
            maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
        formatBuilder = new StringBuilder();
        formatter = new Formatter(formatBuilder, Locale.getDefault());
        initView();
        initPlayerView(url);
        if (playerView != null) {
            playerView.enableFast(true);
            playerView.setVisibleLayoutControl(true);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Log.d(TAG, "onWindowFocusChanged hasFocus: " + hasFocus);
        if (hasFocus && getWindow() != null) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void setFullScreen() {
        Log.d(TAG, "setFullScreen");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        if (window != null) {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public void disableRotateSensor() {
        if (playerView != null) playerView.removeCallbacks(runnableEnableRotateSensor);
        if (orientationEventListener != null) orientationEventListener.disable();
    }

    public void enableRotateSensor() {
        if (orientationEventListener != null) {
            if (orientationEventListener.canDetectOrientation()) {
                Log.d(TAG, "orientationEventListener DetectOrientation: true");
                orientationEventListener.enable();
            } else {
                Log.d(TAG, "orientationEventListener DetectOrientation: false");
            }
        }
    }

    private void setLayoutParam() {
        Log.d(TAG, "setLayoutParam");
        Window window = getWindow();
        if (window != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(window.getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
//            layoutParams.dimAmount = 0f;
            window.setAttributes(layoutParams);
        }
    }

    private void initView() {
        Log.d(TAG, "initView");
        tvTitle.setText(title);
        isDismiss = false;
        ivBack.setOnClickListener(v -> dismiss());
        gestureDetector = new GestureDetector(activity, new OnSwipeListener() {
            @Override
            public boolean onSwipe(OnSwipeListener.Direction direction) {
                Log.e(TAG, "GestureDetector onSwipe direction: " + direction + " -------------------------");
                if (direction == Direction.up) {
                    isUserSwipeUp = true;
                    return true;
                } else if (direction == Direction.down) {
                    isUserSwipeUp = false;
                    return true;
                }
                return false;
            }
        });
        int heightStatusBar = Common.getStatusBarHeight(getContext());
        if (heightStatusBar != 0) {
            layout_video.setPadding(heightStatusBar,layout_video.getPaddingTop(),layout_video.getPaddingBottom(),layout_video.getPaddingRight());
        }
        play.setOnClickListener(view -> {
            if (!isPlaying && isEndVideo) {
                isPlaying = true;
                currentTime = 0;
                isEndVideo = false;
                mPlayer.seekTo(currentTime);
            } else {
                isPlaying = !isPlaying;
            }
            updateStatusPlay();
        });
        layout_video.setVisibility(View.VISIBLE);
        layout_video.postDelayed(runnableHideView, DURATION_HIDE);
        slidingUpPanel.setOnTouchListener((view, motionEvent) -> {
            Log.d(TAG, "slidingUpPanel : " + motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                if (layout_video.getVisibility() == View.VISIBLE) {
                    layout_video.setVisibility(View.INVISIBLE);
                    progressBarBottom.setVisibility(View.VISIBLE);
                } else {
                    layout_video.setVisibility(View.VISIBLE);
                    progressBarBottom.setVisibility(View.GONE);
                    layout_video.removeCallbacks(runnableHideView);
                }
                return true;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                layout_video.removeCallbacks(runnableHideView);
                layout_video.postDelayed(runnableHideView, DURATION_HIDE);
                return true;
            }
            return false;
        });
        layout_video.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.d(TAG, "layout_video : " + motionEvent.getAction());
                return false;
            }
        });
        previous.setOnClickListener(v -> {
            if (mPlayer.getCurrentPosition() > DELTA_TIME) {
                currentTime = mPlayer.getCurrentPosition() - DELTA_TIME;
                mPlayer.seekTo(currentTime);
                updateProgressUI(currentTime, mPlayer.getDuration());
            }
        });
        next.setOnClickListener(v -> {
            if (mPlayer.getCurrentPosition() + DELTA_TIME < mPlayer.getDuration()) {
                currentTime = mPlayer.getCurrentPosition() + DELTA_TIME;
                mPlayer.seekTo(currentTime);
                updateProgressUI(currentTime, mPlayer.getDuration());
            }
        });
        eventChangeLightScreen();
        eventChangeVolume();
        eventChangeProgressTime();
    }

    private void initPlayerView(String url) {
        Log.d(TAG, "initPlayerView");
        isEndVideo = false;
        isPlaying = true;

        playerView = new VideoPlayerView(activity);
        playerView.setUseController(false);
        frameVideo.addView(playerView);
        TrackSelector trackSelectorDef = new DefaultTrackSelector();
        mPlayer = ExoPlayerFactory.newSimpleInstance(activity, trackSelectorDef);
        String userAgent = Util.getUserAgent(activity, activity.getString(R.string.app_name));
        DefaultDataSourceFactory sourceFactory = new DefaultDataSourceFactory(activity, userAgent);
        mPlayer.prepare(buildMediaSource(sourceFactory, url));

        playerView.setPlayer(mPlayer);
        mPlayer.seekTo(currentTime);
        progressNew.postDelayed(runnableTime, 0);
        isPlaying = true;
        updateStatusPlay();
        playerView.setRewindIncrementMs(0);
        playerView.setFastForwardIncrementMs(0);
        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == STATE_ENDED) {
                    isPlaying = false;
                    isEndVideo = true;
                    play.setImageResource(R.drawable.ic_play_available);
                }
            }
        });
    }

    private MediaSource buildMediaSource(DefaultDataSourceFactory dataSourceFactory, String url) {
        Uri uri = Uri.parse(url);
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDashChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultSsChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    private void initOrientationListener() {
        if (activity != null) {
            orientationEventListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {
                int curOrientation;
                boolean canRotate = false;

                @Override
                public void onOrientationChanged(int orientation) {
                    if ((orientation >= 0 && orientation <= 45) || (orientation > 315 && orientation <= 360)) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    } else if (orientation > 45 && orientation <= 135) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    } else if (orientation > 135 && orientation <= 225) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    } else if (orientation > 225 && orientation <= 315) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    } else {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
                    }
                    if (curOrientation != lastOrientation) {
                        lastOrientation = curOrientation;
                        if (playerView != null) {
                            playerView.removeCallbacks(runnableWhenPortrait);
                            if (canRotate && curOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT && isLandscape && !DeviceUtils.isDeviceLockRotate(activity) && mPlayer != null) {
                                playerView.postDelayed(runnableWhenPortrait, 300);
                            }
                        }
                        canRotate = true;
                    }
                }
            };
            orientationEventListener.disable();
        }
    }

    private void handlerEnd() {
        if (mPlayer != null) mPlayer.setPlayWhenReady(false);
    }

    @Override
    public void show() {
        super.show();
        Log.d(TAG, "show");
        if (activity != null) {
            activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            lastOrientation = activity.getRequestedOrientation();
        }
        if (playerView != null) {
            playerView.removeCallbacks(runnableEnableRotateSensor);
            playerView.postDelayed(runnableEnableRotateSensor, 800);
        }
    }

    @Override
    public void dismiss() {
        disableRotateSensor();
        Log.d(TAG, "dismiss");
        isDismiss = true;
        if (mPlayer != null) {
            if (eventListener != null)
                mPlayer.removeListener(eventListener);
            if (playerView != null) {
                playerView.showEpisode(false);
                playerView.setExpandedEpisode(false);
                playerView.setVisiblePreviousAndNextButton(false);
                playerView.setGestureDetector(null);
                playerView = null;
            }
            mPlayer.setPlayWhenReady(false);
        }
        if (trailerPlayerListener != null) {
            trailerPlayerListener.dismissTrailerDialog(isPlaying, isEndVideo, mPlayer.getCurrentPosition());
        }
        if (progressNew != null) {
            progressNew.removeCallbacks(runnableTime);
        }
        if (layout_video != null) {
            layout_video.removeCallbacks(runnableHideView);
        }
        super.dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                currentVolume = Math.min(currentVolume + 1, maxVolume);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
                if (seekbar_volumme != null) {
                    seekbar_volumme.setProgress(currentVolume);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                currentVolume = Math.max(currentVolume - 1, 0);
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
                if (seekbar_volumme != null) {
                    seekbar_volumme.setProgress(currentVolume);
                }
                return true;

            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        return super.onTouchEvent(event);
    }

    private void updateStatusPlay() {
        if (mPlayer != null) {
            if (isPlaying) {
                mPlayer.setPlayWhenReady(true);
                play.setImageResource(R.drawable.ic_pause_available);
            } else {
                mPlayer.setPlayWhenReady(false);
                play.setImageResource(R.drawable.ic_play_available);
            }
        }
    }

    public void stopTrailer() {
        if (isPlaying) {
            isPlaying = false;
            updateStatusPlay();
        }
    }

    public boolean canChangeVolume() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
            canChangeVolume = notificationManager.isNotificationPolicyAccessGranted();
        } else {
            canChangeVolume = true;
        }
        return canChangeVolume;
    }

    private void eventChangeVolume() {
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamVolume(AudioManager.STREAM_MUSIC), 0);
        seekbar_volumme.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekbar_volumme.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        seekbar_volumme.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!canChangeVolume) {
                    if (!canChangeVolume()) {
                        EventBus.getDefault().post(new RequestPermissionVolume());
                        return true;
                    }
                }
                return false;
            }
        });
        seekbar_volumme.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, AudioManager.STREAM_MUSIC);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @SuppressLint("NewApi")
    private void eventChangeLightScreen() {
        int max = getMaxBrightness(getContext(), 255);
        int brightness = Settings.System.getInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0);
        seekbar_brightness.setProgress(brightness * 100 / max);
        seekbar_brightness.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                boolean canWriteSettings = Settings.System.canWrite(getContext());
                if (!canWriteSettings) {
                    stopTrailer();
                    EventBus.getDefault().post(new RequestPermissionBright());
                    return true;
                }
                return false;
            }
        });
        seekbar_brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!b) { // if not from user's action
                    return;
                }
                boolean canWriteSettings = Settings.System.canWrite(getContext());
                if (canWriteSettings) {
                    int progress = i * max / 100;
                    Settings.System.putInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                    Settings.System.putInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        int currBrightness = Settings.System.getInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0);
        seekbar_brightness.setProgress(currBrightness * 100 / 255);
    }

    private void eventChangeProgressTime() {
        progressNew.addOnScrubListener(new PreviewBar.OnScrubListener() {
            @Override
            public void onScrubStart(PreviewBar previewBar) {

            }

            @Override
            public void onScrubMove(PreviewBar previewBar, int progress, boolean fromUser) {

            }

            @Override
            public void onScrubStop(PreviewBar previewBar) {
                currentTime = previewBar.getProgress();
                mPlayer.seekTo(currentTime);
                updateProgressUI(currentTime, mPlayer.getDuration());
            }
        });
    }

    private void updateProgressUI(long current, long duration) {
        progressNew.setDuration(duration);
        progressNew.setPosition(current);

        progressBarBottom.setDuration(duration);
        progressBarBottom.setPosition(current);

        tvDuration.setText(Util.getStringForTime(formatBuilder, formatter, duration - current));
    }

    public int getMaxBrightness(Context context, int defaultValue) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            Field[] fields = powerManager.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals("BRIGHTNESS_ON")) {
                    field.setAccessible(true);
                    try {
                        return (int) field.get(powerManager);
                    } catch (IllegalAccessException e) {
                        return defaultValue;
                    }
                }
            }
        }
        return defaultValue;
    }
}

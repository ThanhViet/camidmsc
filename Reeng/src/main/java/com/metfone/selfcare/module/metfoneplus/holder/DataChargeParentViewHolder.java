package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

/**
 * ViewHolder for History charge Data: Quantity, Total charges
 */
public class DataChargeParentViewHolder extends RecyclerView.ViewHolder {
    public AppCompatTextView mQuantity;
    public AppCompatTextView mTotal;
    public AppCompatTextView mContentValue;

    public DataChargeParentViewHolder(View view) {
        super(view);
        mQuantity = view.findViewById(R.id.value_quantity_history_charge_4);
        mTotal = view.findViewById(R.id.value_total_history_charge_4);
        mContentValue = view.findViewById(R.id.content_value_history_charge_4);
    }
}
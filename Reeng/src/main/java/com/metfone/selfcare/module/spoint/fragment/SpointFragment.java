package com.metfone.selfcare.module.spoint.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.HTTPCode;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.database.model.game.AccumulatePointItem;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.spoint.SpointActivity;
import com.metfone.selfcare.module.spoint.SpointListener;
import com.metfone.selfcare.module.spoint.adapter.SpointAdapter;
import com.metfone.selfcare.module.spoint.network.SpointApi;
import com.metfone.selfcare.module.spoint.network.model.SpointModel;
import com.metfone.selfcare.module.spoint.network.response.SpointResponse;
import com.metfone.selfcare.ui.dialog.DialogConvertSpointToVtPlus;
import com.metfone.selfcare.ui.dialog.DialogInputOtp;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.dialog.DialogCaptcha;
import com.metfone.selfcare.v5.dialog.DialogConfirm;
import com.metfone.selfcare.v5.home.base.BaseDialogFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SpointFragment extends Fragment implements SpointListener.OnSpointListener {
    private SpointModel currentSpoint;
    private AppCompatTextView tvSpoint, tvPhoneNumber;
    private RecyclerView recyclerView;
    private SpointAdapter adapter;
    private SpointActivity activity;
    private List<AccumulatePointItem> listSpoint;
    private ReengAccount myAccount;
    private String currentType = "0"; //mac dinh laf spoint
    private SpointApi mSpointApi;
    private boolean checkRefresh = false; // check refresh khi đổi điểm hoặc mua gói cước

    public static SpointFragment newInstance(String s) {
        SpointFragment fragment = new SpointFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TYPE_SPOINT", s);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spoint, container, false);
        activity = (SpointActivity) getActivity();
        initView(view);
        loadData();
        initEvent();
        return view;
    }

    private void initView(View view) {
        tvSpoint = view.findViewById(R.id.tv_spoint);
        recyclerView = view.findViewById(R.id.recycler_view);
        tvPhoneNumber = view.findViewById(R.id.tv_phone_number);
        if (getArguments() != null) {
            currentType = getArguments().getString("TYPE_SPOINT", "0");
        }
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        listSpoint = new ArrayList<>();
        adapter = new SpointAdapter(activity, listSpoint, this);
        recyclerView.setAdapter(adapter);

        myAccount = ApplicationController.self().getReengAccountBusiness().getCurrentAccount();
        if(currentType == SpointActivity.SPOINT) {
            String sdt = String.format(activity.getString(R.string.number_spoint),
                    myAccount.getJidNumber());
            tvPhoneNumber.setText(TextHelper.fromHtml(sdt));
        }else{
            String sdt = String.format(activity.getString(R.string.number_spoint_plus),
                    myAccount.getJidNumber());
            tvPhoneNumber.setText(TextHelper.fromHtml(sdt));
        }
        mSpointApi = new SpointApi(ApplicationController.self());
    }

    public void loadData() {
        if(!checkRefresh) {
            activity.showLoading();
            checkRefresh = true;
        }
        mSpointApi.getSpoint(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                Gson gson = new Gson();
                SpointResponse spointResponse = gson.fromJson(data, SpointResponse.class);
                if (spointResponse.getCode().equals("200")) {
                    currentSpoint = spointResponse.getData();
                    bindData(currentSpoint);
                } else {

                }
                activity.showLoadedSuccess();
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                activity.showLoadedError();
            }
        }, currentType);
    }

    private void initEvent() {

    }

    private void bindData(SpointModel spointModel) {
        if (spointModel == null) return;
        if(currentType == SpointActivity.SPOINT) {
            AccumulatePointHelper.getInstance(ApplicationController.self()).setTotalPoint(spointModel.getBalance() + "");
        }
        listSpoint.clear();
        tvSpoint.setText(spointModel.getBalanceDesc() + activity.getString(R.string.spoint));
        if (spointModel.getListTask().size() > 0) {
            listSpoint.add(new AccumulatePointItem(activity.getString(R.string.accumulate_tab_make), 1));
            listSpoint.addAll(spointModel.getListTask());
        }
        if (spointModel.getListGifChange().size() > 0) {
            listSpoint.add(new AccumulatePointItem("", 2));
            listSpoint.add(new AccumulatePointItem(activity.getString(R.string.accumulate_tab_exchange), 1));
            listSpoint.addAll(spointModel.getListGifChange());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickActionSpoint(AccumulatePointItem model) {
        if (currentSpoint == null) return;
        if (currentSpoint.getGifType() == 2) { // tab spoint
            if (!TextUtils.isEmpty(model.getDeepLink())) {
                DeepLinkHelper.getInstance().openSchemaLink((BaseSlidingFragmentActivity) activity, model.getDeepLink());
            } else {
                if (model.getType() == 2) {// doi diem spoint sang spoint plus
                    showDialogChangeSpoint(model);
                } else if (model.getType() == AccumulatePointItem.TYPE_CONVERT_POINT_VTPLUS) {

                    String point = currentSpoint.getBalanceDesc();
                    if ("0".equals(point)) {
                        activity.showToast(R.string.accumulate_no_spoint);
                        return;
                    }
                    DialogConvertSpointToVtPlus dialogConvert = new DialogConvertSpointToVtPlus(activity, true);
                    dialogConvert.setMessage(String.format(activity.getString(R.string.accumulate_enter_spoint_des)
                            , point));
                    dialogConvert.setLabel(activity.getString(R.string.accumulate_enter_spoint));
                    dialogConvert.setNegativeLabel(activity.getString(R.string.accumulate_cancel));
                    dialogConvert.setPoint(currentSpoint.getBalance());
                    dialogConvert.setPositiveLabel(activity.getString(R.string.accumulate_exchange));
                    dialogConvert.setPositiveListener(new PositiveListener<String>() {
                        @Override
                        public void onPositive(String result) {
                            onConvertSpointToViettelPlus(model, result, "", ""); // lần đầu click đổi chưa có otp
                        }
                    });
                    dialogConvert.show();

                } else {
                    onProcessExchange(model);
                }
            }
        } else if (currentSpoint.getGifType() == 3) { //tab spoint plus
            showDialogConfirmSpointKM(model);
        }

    }

    private void onProcessExchange(AccumulatePointItem item) {
        activity.showLoadingDialog(null, R.string.waiting);
        AccumulatePointHelper.getInstance(ApplicationController.self()).convertGiftPoint(item, new AccumulatePointHelper.GiftPointListener() {
            @Override
            public void onError(int errorCode, String desc, String tranId) {

            }

            @Override
            public void onSuccess(String desc) {
                activity.showToast(desc, Toast.LENGTH_LONG);
                activity.hideLoadingDialog();
                activity.refreshData();
            }

            @Override
            public void onError(int errorCode, String desc) {
                activity.hideLoadingDialog();
                if (!TextUtils.isEmpty(desc)) {
                    activity.showToast(desc, Toast.LENGTH_LONG);
                } else {
                    activity.showToast(R.string.e601_error_but_undefined);
                }
            }
        });
    }

    private void showDialogChangeSpoint(AccumulatePointItem model) {
        Dialog dialog = new Dialog(activity, R.style.DialogFullscreen);
        dialog.setContentView(R.layout.dialog_change_spoint);
        AppCompatTextView tvInputMoney, tvMessage, tvShortCut;
        AppCompatButton btnCancel, btnConfirm;
        LinearLayout llInput;
        EditText edtInputMoney;
        tvInputMoney = dialog.findViewById(R.id.tv_input_money);
        btnCancel = dialog.findViewById(R.id.btnLeft);
        btnConfirm = dialog.findViewById(R.id.btnRight);
        edtInputMoney = dialog.findViewById(R.id.edt_input_money);
        tvMessage = dialog.findViewById(R.id.tvMessage);
        tvShortCut = dialog.findViewById(R.id.tvShortCut);
        llInput = dialog.findViewById(R.id.ll_input);

        String content = String.format(activity.getString(R.string.content_dialog_spoint),
                currentSpoint.getBalanceDesc());
        tvMessage.setText(TextHelper.fromHtml(content));

        edtInputMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    tvInputMoney.setVisibility(View.VISIBLE);
                    tvShortCut.setVisibility(View.GONE);
                    llInput.setActivated(true);
                    llInput.setSelected(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("0".equals(currentSpoint.getBalanceDesc())) {
                    activity.showToast(R.string.accumulate_no_spoint);
                    return;
                }
                if (TextUtils.isEmpty(edtInputMoney.getText().toString().trim())) {
                    llInput.setSelected(true);
                    tvShortCut.setVisibility(View.VISIBLE);
                    return;
                } else {
                    //chua check spoint
                    int spoint = Integer.parseInt(edtInputMoney.getText().toString().trim());
                    if(currentSpoint.getBalance() < spoint){
                        activity.showToast(R.string.toast_limit_spoint);
                        return;
                    }
                    if (spoint > 0 && spoint % 100 == 0) {
                        onChangeSpoint(spoint, model.getId(), "", "");
//                        Toast.makeText(activity, "Okkkkkkkkk", Toast.LENGTH_SHORT).show();
                    } else {
                        llInput.setSelected(true);
                        tvShortCut.setVisibility(View.VISIBLE);
                        return;
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void onChangeSpoint(int spoint, long id, String sid, String captcha) {
        mSpointApi.changeSpointToSpointPlus(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject object = new JSONObject(data);
                int code = object.optInt(Constants.HTTP.REST_CODE);
                String desc = object.optString(Constants.HTTP.REST_DESC);
                if (code == HTTPCode.E200_OK) {
                    String change = String.format(activity.getString(R.string.change_succesful_spoint),
                            spoint + "");
                    activity.showToastDone(change);
                    activity.refreshData();
                } else if (code == HTTPCode.E202_OK) {
                    //show dialog captcha
                    JSONObject objectData = object.getJSONObject("data");
                    String image = objectData.getString("image");
                    String sid = objectData.getString("sid");
                    showDialogCaptcha(spoint, id, image, sid);
                } else {
                    activity.showToast(desc, Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                activity.showToast(R.string.e601_error_but_undefined);
            }
        }, spoint, id, sid, captcha);
    }

    private void showDialogCaptcha(int spoint, long id, String str, String sid) {
        DialogCaptcha dialogCaptcha = new DialogCaptcha(activity, false)
                .setBitmapCaptcha(Utilities.getImageCaptche(str))
                .setDialogListener(new DialogCaptcha.DialogInterface() {
                    @Override
                    public void onClickLeft() {

                    }

                    @Override
                    public void onClickRight(String captcha) {
                        if (TextUtils.isEmpty(captcha)) {

                        } else {
                            if (spoint > 0) {
                                onChangeSpoint(spoint, id, sid, captcha);
                            } else {
//                                onRentMovieKeeng(id, sid, captcha);
                            }
                        }
                    }

                    @Override
                    public void onClickRefresh(AppCompatImageView ivRefresh) {
                        refreshCaptcha(sid, ivRefresh);
                    }
                });
        dialogCaptcha.show();
    }

    private void refreshCaptcha(String sid, AppCompatImageView ivCaptcha) {
        mSpointApi.refreshCaptcha(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject object = new JSONObject(data);
                int code = object.optInt(Constants.HTTP.REST_CODE);
                String desc = object.optString(Constants.HTTP.REST_DESC);
                if (code == HTTPCode.E200_OK) {
                    JSONObject objectData = object.getJSONObject("data");
                    String image = objectData.getString("image");
                    String sid = objectData.getString("sid");
                    ivCaptcha.setImageBitmap(Utilities.getImageCaptche(image));
                }
            }
        }, sid);
    }

    private void onRentSpointPlus(long id) {
        mSpointApi.rentMovie(new HttpCallBack() {
            @Override
            public void onSuccess(String data) throws Exception {
                JSONObject object = new JSONObject(data);
                int code = object.optInt(Constants.HTTP.REST_CODE);
                String desc = object.optString(Constants.HTTP.REST_DESC);
                if (code == HTTPCode.E200_OK) {
                    activity.showToastDone(desc);
                    activity.refreshData();
                } else if (code == HTTPCode.E202_OK) {
                    //hien captcha
//                    showDialogCaptcha(0, id, sid, captcha);
                } else {
                    activity.showToast(desc);
                }
                activity.hideLoadingDialog();
            }

            @Override
            public void onFailure(String message) {
                super.onFailure(message);
                activity.showToast(R.string.e601_error_but_undefined);
                activity.hideLoadingDialog();
            }
        }, id);
    }

    private void onConvertSpointToViettelPlus(AccumulatePointItem item, String point, String otp, String transId) {
        activity.showLoadingDialog(null, R.string.waiting);
        AccumulatePointHelper.getInstance(ApplicationController.self()).convertSpointToViettelPlus(point, item, otp, transId, new AccumulatePointHelper.GiftPointListener() {
            @Override
            public void onSuccess(String desc) {
                activity.showToast(desc, Toast.LENGTH_LONG);
                activity.refreshData();
                activity.hideLoadingDialog();
                int currentPoint = Integer.valueOf(AccumulatePointHelper.getInstance(ApplicationController.self()).getViettelPlusPoint());
                int convertPoint = Integer.valueOf(point);
                int afterPoint = currentPoint + convertPoint;
                AccumulatePointHelper.getInstance(ApplicationController.self()).setViettelPlusPoint(String.valueOf(afterPoint));

            }

            @Override
            public void onError(int errorCode, String desc) {
            }

            @Override
            public void onError(int errorCode, String desc, String tranId) {
                activity.hideLoadingDialog();
                if (errorCode == 202) {
                    if (TextUtils.isEmpty(tranId)) {
                        if (TextUtils.isEmpty(desc)) {
                            activity.showToast(R.string.accumulate_exchange_vt_fail);
                        } else {
                            activity.showToast(desc);
                        }
                    } else {
                        DialogInputOtp dialogInputOtp = new DialogInputOtp(activity, false) {
                            @Override
                            public void onSubmitBuyOtp(String otp) {
                                activity.showLoadingDialog("", R.string.loading);
                                onConvertSpointToViettelPlus(item, point, otp, tranId);
                                this.dismiss();
                            }

                            @Override
                            public void onCancel() {
                                this.dismiss();
                            }

                            @Override
                            public void onResend() {

                            }
                        };
                        if (TextUtils.isEmpty(otp)) {
                            dialogInputOtp.showContentText(activity.getString(R.string.text_content_dialog_otp_viettel_plus));
                        }else {
                            // không phải lần đầu setContent của server
                            dialogInputOtp.showContentText(desc);
                        }
                        dialogInputOtp.setCanceledOnTouchOutside(false);
                        dialogInputOtp.setCancelable(false);
                        dialogInputOtp.show();
                    }
                } else if (!TextUtils.isEmpty(desc)) {
                    activity.showToast(desc, Toast.LENGTH_LONG);
                } else {
                    activity.showToast(R.string.accumulate_exchange_vt_fail);
                }
            }
        });
    }
    public void showDialogConfirmSpointKM(AccumulatePointItem model){
        DialogConfirm dialogConfirm = DialogConfirm.newInstance(model.getTitleFakeMo(),
                model.getConfirmFakeMo(), DialogConfirm.CONFIRM_TYPE,
                R.string.s_cancel_dialog, R.string.s_pay);
        dialogConfirm.setSelectListener(
                new BaseDialogFragment.DialogListener() {
                    @Override
                    public void dialogRightClick(int value) {
                        activity.showLoadingDialog("", R.string.loading);
                        onRentSpointPlus(model.getId());
                        dialogConfirm.dismiss();
                    }

                    @Override
                    public void dialogLeftClick() {
                        dialogConfirm.dismiss();
                    }
                });
        dialogConfirm.show(getChildFragmentManager(), "");
    }

}

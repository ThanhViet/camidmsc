package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCPackage;
import com.metfone.selfcare.module.selfcare.utils.SCImageLoader;

import java.util.ArrayList;

public class SCAccountServiceAdapter extends BaseAdapter<BaseViewHolder> {

    private ArrayList<SCPackage> data;
    private AbsInterface.OnPackageListener listener;

    public SCAccountServiceAdapter(Context context, AbsInterface.OnPackageListener listener) {
        super(context);
        this.listener = listener;
    }

    public void setItemsList(ArrayList<SCPackage> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sc_account_sevice, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final SCPackage model = data.get(position);
        if(model != null)
        {
            holder.setText(R.id.tvName, TextUtils.isEmpty(model.getName()) ? "" : model.getName());
            holder.setText(R.id.tvDescription, TextUtils.isEmpty(model.getShortDes()) ? "" : model.getShortDes());
            SCImageLoader.setImage(mContext, (ImageView)holder.getView(R.id.imvImage), model.getIconUrl());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                        listener.onPackageClick(model);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }
}
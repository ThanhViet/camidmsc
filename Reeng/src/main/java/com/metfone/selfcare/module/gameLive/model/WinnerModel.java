package com.metfone.selfcare.module.gameLive.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;

public class WinnerModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("msisdn")
    private String msisdn;

    @SerializedName("prize")
    private int reward;

    public WinnerModel() {
    }

    public WinnerModel(String name, String msisdn, int reward) {
        this.name = name;
        this.msisdn = msisdn;
        this.reward = reward;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        if (TextUtils.isEmpty(name)) {
            name = Utilities.hidenPhoneNumber(msisdn);
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public int getReward() {
        return reward;
    }

    @Override
    public String toString() {
        return "WinnerModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", reward='" + reward + '\'' +
                '}';
    }
}

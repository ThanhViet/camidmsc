package com.metfone.selfcare.module.keeng.model;

import java.io.Serializable;

/**
 * Created by namnh40 on 3/21/2017.
 */

public class CategoryModel implements Serializable {
    private static final long serialVersionUID = -1493020190989116870L;
    long id = 0;
    int resTitle = 0;
    int resIcon = 0;
    int type;
    String title = "";
    AllModel media;
//    FeedsModel feeds;
    PlayListModel playlist;
    CheckBeforeBuyModel checkBeforeBuy;
    int positionAdapter;

    public CategoryModel() {
    }

    public CategoryModel(int type, int resTitle, int resIcon) {
        this.resTitle = resTitle;
        this.type = type;
        this.resIcon = resIcon;
    }

    public CategoryModel(int type, String title, int resIcon) {
        this.title = title;
        this.type = type;
        this.resIcon = resIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getResTitle() {
        return resTitle;
    }

    public void setResTitle(int resTitle) {
        this.resTitle = resTitle;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getResIcon() {
        return resIcon;
    }

    public void setResIcon(int resIcon) {
        this.resIcon = resIcon;
    }

    public AllModel getMedia() {
        return media;
    }

    public CategoryModel setMedia(AllModel media) {
        this.media = media;
        return this;
    }

//    public FeedsModel getFeeds() {
//        return feeds;
//    }
//
//    public CategoryModel setFeeds(FeedsModel feeds) {
//        this.feeds = feeds;
//        return this;
//    }

    public PlayListModel getPlaylist() {
        return playlist;
    }

    public CategoryModel setPlaylist(PlayListModel playlist) {
        this.playlist = playlist;
        return this;
    }

    public CheckBeforeBuyModel getCheckBeforeBuy() {
        return checkBeforeBuy;
    }

    public CategoryModel setCheckBeforeBuy(CheckBeforeBuyModel checkBeforeBuy) {
        this.checkBeforeBuy = checkBeforeBuy;
        return this;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id=" + id +
                ", resTitle=" + resTitle +
                ", type=" + type +
                ", resIcon=" + resIcon +
                '}';
    }

    public int getPositionAdapter() {
        return positionAdapter;
    }

    public CategoryModel setPositionAdapter(int positionAdapter) {
        this.positionAdapter = positionAdapter;
        return this;
    }
}

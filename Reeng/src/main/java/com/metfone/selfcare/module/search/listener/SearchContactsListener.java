/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/9/13
 *
 */

package com.metfone.selfcare.module.search.listener;

import java.util.ArrayList;

public interface SearchContactsListener {
    void onPrepareSearchContacts();

    void onFinishedSearchContacts(String keySearch, ArrayList<Object> list);
}

package com.metfone.selfcare.module.home_kh.notification.detail;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationItem;
import com.metfone.selfcare.module.metfoneplus.fragment.MPBuyServiceDetailFragment;
import com.metfone.selfcare.module.metfoneplus.topup.TopupMetfoneFragment;
import com.metfone.selfcare.module.movienew.fragment.ActionCategoryFragment;
import com.metfone.selfcare.module.movienew.fragment.MyListFragment;
import com.metfone.selfcare.network.metfoneplus.response.WsGetServiceDetailResponse;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RedirectNotificationActivity extends BaseSlidingFragmentActivity {
    private String actionType = "";
    public static final String NOTIFICATION_KEY = "notification";
    public static final String SERVICE_KEY = "service";
    public static final String EXCHANGE_KEY = "exchange";
    private KhNotificationItem notificationItem;

    @BindView(R.id.root_frame)
    FrameLayout root_frame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setContentView(R.layout.activity_redirect_notification);
        ButterKnife.bind(this);
        handleDetailType();
        Utilities.adaptViewForInsertBottomKeepTop(root_frame);
    }

    private void handleDetailType() {
        notificationItem = (KhNotificationItem) getIntent().getSerializableExtra(NOTIFICATION_KEY);
        if (notificationItem == null) {
            return;
        }
        actionType = notificationItem.actionType;
        if (actionType.equals(KhNotificationItem.ActionType.MOVIE_CATEGORY)) {
            root_frame.setPadding(root_frame.getLeft(), Utilities.dpToPx(34.0f), root_frame.getRight(), root_frame.getBottom());
            ActionCategoryFragment fragment = ActionCategoryFragment.newInstance(notificationItem.actionObjectId, notificationItem.getTitle(this), ActionCategoryFragment.CATEGORY);
            addFragment(R.id.root_frame, fragment, false);
        } else if (actionType.equals(KhNotificationItem.ActionType.MOVIE_ACTOR) || actionType.equals(KhNotificationItem.ActionType.MOVIE_DIRECTOR)) {
            MyListFragment fragment = MyListFragment.newInstance(notificationItem.getTitle(this), notificationItem.actionObjectId);
            addFragment(R.id.root_frame, fragment, false);
        } else if (actionType.equals(KhNotificationItem.ActionType.REWARD_DETAIL)) {
            GiftItems g = new GiftItems();
            g.setGiftId(notificationItem.actionObjectId);
            RewardsDetailKHFragment fragment = RewardsDetailKHFragment.newInstance(g);
            addFragment(R.id.root_frame, fragment, false);
        } else if (actionType.equals(KhNotificationItem.ActionType.METFONE_TOPUP)) {
            TopupMetfoneFragment fragment = TopupMetfoneFragment.newInstance();
            addFragment(R.id.root_frame, fragment, false);
        } else if (actionType.equals(KhNotificationItem.ActionType.METFONE_SERVICE)) {
            if (notificationItem.isExchange == 0) {
                WsGetServiceDetailResponse.Response serviceDetail = (WsGetServiceDetailResponse.Response) getIntent().getSerializableExtra(SERVICE_KEY);
                MPBuyServiceDetailFragment fragment = MPBuyServiceDetailFragment.newInstance(serviceDetail);
                addFragment(R.id.root_frame, fragment, false);
            } else if (notificationItem.isExchange == 1) {
                ExchangeItem exchangeItem = (ExchangeItem) getIntent().getSerializableExtra(EXCHANGE_KEY);
                MPBuyServiceDetailFragment fragment = MPBuyServiceDetailFragment.newInstance(exchangeItem);
                addFragment(R.id.root_frame, fragment, false);
            } else {
                finish();
            }

        } else {
            finish();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.module.keeng.network.restpaser;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.SingerInfo;
import com.metfone.selfcare.restful.AbsResultData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RestAllModel extends AbsResultData implements Serializable {

	private static final long serialVersionUID = -43590256707591184L;

	@SerializedName("data")
	private List<AllModel> data;
	@SerializedName("singer_info")
	private SingerInfo singerInfo;

	public List<AllModel> getData() {
		if (data == null)
			data = new ArrayList<>();
		return data;
	}

	public void setData(List<AllModel> data) {
		this.data = data;
	}

	public SingerInfo getSingerInfo() {
		return singerInfo;
	}

	public void setSingerInfo(SingerInfo singerInfo) {
		this.singerInfo = singerInfo;
	}

	@Override
	public String toString() {
		return "RestAllModel [data=" + data + "] error " + getError();
	}

	/*
	 * Kiem tra xem co sai token ko
	 *
	 * @param context
	 *
	 * @return
	 */
	public List<AllModel> getData(Context context) {
		if (isWrongToken()) {
//			AutoLoginAsync.autoLoginManual(context);
		}
		return data;
	}

}

package com.metfone.selfcare.module.selfcare.fragment;

import android.app.DatePickerDialog;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SCAccountDetailChargeFragment extends BaseFragment implements View.OnClickListener {

    private TextView tv7Day;
    private TextView tv30Day;
    private TextView tvWeek;
    private TextView tvMonth;

    private TextView tvSms;
    private TextView tvCall;
    private TextView tvData;
    private TextView tvOther;

    private View layout_sms;
    private View layout_call;
    private View layout_data;
    private View layout_other;

    private EditText edtFrom;
    private EditText edtTo;
    private TextView btnSubmit;


    private SimpleDateFormat sdf = new SimpleDateFormat(TimeHelper.SDF_TIME_MYTEL, Locale.US);

    private long fromDate;
    private long toDate;
    private int postType = SCConstants.POST_TYPE.CALL;

    public static SCAccountDetailChargeFragment newInstance() {
        SCAccountDetailChargeFragment fragment = new SCAccountDetailChargeFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return "SCAccountDetailChargeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_account_charge;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        return view;
    }

    private void initView(View view) {
        sdf = new SimpleDateFormat(TimeHelper.SDF_TIME_MYTEL, Locale.US);

        tv7Day = view.findViewById(R.id.tv7Day);
        tv30Day = view.findViewById(R.id.tv30Day);
        tvWeek = view.findViewById(R.id.tvWeek);
        tvMonth = view.findViewById(R.id.tvMonth);

        tvSms = view.findViewById(R.id.tvSMS);
        tvCall = view.findViewById(R.id.tvCall);
        tvData = view.findViewById(R.id.tvData);
        tvOther = view.findViewById(R.id.tvOther);

        layout_call = view.findViewById(R.id.layout_call);
        layout_sms = view.findViewById(R.id.layout_sms);
        layout_data = view.findViewById(R.id.layout_data);
        layout_other = view.findViewById(R.id.layout_other);

        layout_call.setSelected(true);
        layout_sms.setSelected(false);
        layout_data.setSelected(false);
        layout_other.setSelected(false);

        setTextViewDrawableColor(tvCall, R.color.white);
        setTextViewDrawableColor(tvSms, R.color.sc_text_color_primary);
        setTextViewDrawableColor(tvData, R.color.sc_text_color_primary);
        setTextViewDrawableColor(tvOther, R.color.sc_text_color_primary);

        tvCall.setTextColor(mActivity.getResources().getColor(R.color.white));
        tvSms.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
        tvData.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
        tvOther.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));

        edtFrom = view.findViewById(R.id.edtFrom);
        edtTo = view.findViewById(R.id.edtTo);
        btnSubmit = view.findViewById(R.id.btnSubmit);

        tv7Day.setOnClickListener(this);
        tv30Day.setOnClickListener(this);
        tvWeek.setOnClickListener(this);
        tvMonth.setOnClickListener(this);

        layout_sms.setOnClickListener(this);
        layout_call.setOnClickListener(this);
        layout_data.setOnClickListener(this);
        layout_other.setOnClickListener(this);

        btnSubmit.setOnClickListener(this);



        final Calendar myCalendarFrom = Calendar.getInstance();
        long currentTimeFrom = myCalendarFrom.getTimeInMillis();
        int dayOfMonth = myCalendarFrom.getTime().getDate();
        int hours = Calendar.getInstance().getTime().getHours();
        int minute = Calendar.getInstance().getTime().getMinutes();
        fromDate = currentTimeFrom - dayOfMonth * DateTimeUtils.DAY + (24 * DateTimeUtils.HOUR - hours * DateTimeUtils.HOUR + minute * DateTimeUtils.MINUTE);

        final Calendar myCalendarTo = Calendar.getInstance();
        long currentTimeTo = myCalendarTo.getTimeInMillis();
        toDate = currentTimeTo;

        edtFrom.setText(sdf.format(fromDate));
        edtTo.setText(sdf.format(toDate));

        edtFrom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog dialog = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendarFrom.set(Calendar.YEAR, year);
                        myCalendarFrom.set(Calendar.MONTH, month);
                        myCalendarFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        fromDate = myCalendarFrom.getTime().getTime();
                        edtFrom.setText(sdf.format(myCalendarFrom.getTime()));

                        tv7Day.setSelected(false);
                        tv30Day.setSelected(false);
                        tvWeek.setSelected(false);
                        tvMonth.setSelected(false);

                        tv7Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                        tv30Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                        tvWeek.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                        tvMonth.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                    }
                }, myCalendarFrom.get(Calendar.YEAR),
                        myCalendarFrom.get(Calendar.MONTH),
                        myCalendarFrom.get(Calendar.DAY_OF_MONTH));

                dialog.getDatePicker().setMaxDate(myCalendarTo.getTime().getTime());
                dialog.show();
            }
        });

        edtTo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog dialog = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendarTo.set(Calendar.YEAR, year);
                        myCalendarTo.set(Calendar.MONTH, month);
                        myCalendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        toDate = myCalendarTo.getTime().getTime();
                        edtTo.setText(sdf.format(myCalendarTo.getTime()));

                        tv7Day.setSelected(false);
                        tv30Day.setSelected(false);
                        tvWeek.setSelected(false);
                        tvMonth.setSelected(false);

                        tv7Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                        tv30Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                        tvWeek.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                        tvMonth.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                    }
                }, myCalendarTo.get(Calendar.YEAR),
                        myCalendarTo.get(Calendar.MONTH),
                        myCalendarTo.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTime().getTime());
                dialog.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        int day = Calendar.getInstance().getTime().getDay();
        int hours = Calendar.getInstance().getTime().getHours();
        int minute = Calendar.getInstance().getTime().getMinutes();

        switch (v.getId()) {
            case R.id.tv7Day:
                fromDate = currentTime - 7 * DateTimeUtils.DAY;
                toDate = currentTime;
//                doCharge(fromDate, toDate);

                edtFrom.setText(sdf.format(fromDate));
                edtTo.setText(sdf.format(toDate));

                tv7Day.setSelected(true);
                tv30Day.setSelected(false);
                tvWeek.setSelected(false);
                tvMonth.setSelected(false);

                tv7Day.setTextColor(mActivity.getResources().getColor(R.color.white));
                tv30Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvWeek.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvMonth.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                break;
            case R.id.tv30Day:
                fromDate = currentTime - 30 * DateTimeUtils.DAY;
                toDate = currentTime;
//                doCharge(fromDate, toDate);

                edtFrom.setText(sdf.format(fromDate));
                edtTo.setText(sdf.format(toDate));

                tv7Day.setSelected(false);
                tv30Day.setSelected(true);
                tvWeek.setSelected(false);
                tvMonth.setSelected(false);

                tv7Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tv30Day.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvWeek.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvMonth.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                break;
            case R.id.tvWeek:
                if (day == 0)
                    day = 8;
                fromDate = currentTime - day * DateTimeUtils.DAY + (24 * DateTimeUtils.HOUR - hours * DateTimeUtils.HOUR + minute * DateTimeUtils.MINUTE);
                toDate = currentTime;
//                doCharge(fromDate, toDate);

                edtFrom.setText(sdf.format(fromDate));
                edtTo.setText(sdf.format(toDate));

                tv7Day.setSelected(false);
                tv30Day.setSelected(false);
                tvWeek.setSelected(true);
                tvMonth.setSelected(false);

                tv7Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tv30Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvWeek.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvMonth.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                break;
            case R.id.tvMonth:
                int dayOfMonth = Calendar.getInstance().getTime().getDate();
                fromDate = currentTime - dayOfMonth * DateTimeUtils.DAY + (24 * DateTimeUtils.HOUR - hours * DateTimeUtils.HOUR + minute * DateTimeUtils.MINUTE);
                toDate = currentTime;
//                doCharge(fromDate, toDate);

                edtFrom.setText(sdf.format(fromDate));
                edtTo.setText(sdf.format(toDate));

                tv7Day.setSelected(false);
                tv30Day.setSelected(false);
                tvWeek.setSelected(false);
                tvMonth.setSelected(true);

                tv7Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tv30Day.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvWeek.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvMonth.setTextColor(mActivity.getResources().getColor(R.color.white));
                break;
            case R.id.layout_call:
                postType = SCConstants.POST_TYPE.CALL;

                layout_call.setSelected(true);
                layout_sms.setSelected(false);
                layout_data.setSelected(false);
                layout_other.setSelected(false);

                setTextViewDrawableColor(tvCall, R.color.white);
                setTextViewDrawableColor(tvSms, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvData, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvOther, R.color.sc_text_color_primary);

                tvCall.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvSms.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvData.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvOther.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                break;
            case R.id.layout_sms:
                postType = SCConstants.POST_TYPE.SMS;

                layout_call.setSelected(false);
                layout_sms.setSelected(true);
                layout_data.setSelected(false);
                layout_other.setSelected(false);

                setTextViewDrawableColor(tvCall, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvSms, R.color.white);
                setTextViewDrawableColor(tvData, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvOther, R.color.sc_text_color_primary);

                tvCall.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvSms.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvData.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvOther.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                break;
            case R.id.layout_data:
                postType = SCConstants.POST_TYPE.DATA;

                layout_call.setSelected(false);
                layout_sms.setSelected(false);
                layout_data.setSelected(true);
                layout_other.setSelected(false);

                setTextViewDrawableColor(tvCall, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvSms, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvData, R.color.white);
                setTextViewDrawableColor(tvOther, R.color.sc_text_color_primary);

                tvCall.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvSms.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvData.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvOther.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                break;
            case R.id.layout_other:
                postType = SCConstants.POST_TYPE.OTHER;

                layout_call.setSelected(false);
                layout_sms.setSelected(false);
                layout_data.setSelected(false);
                layout_other.setSelected(true);

                setTextViewDrawableColor(tvCall, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvSms, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvData, R.color.sc_text_color_primary);
                setTextViewDrawableColor(tvOther, R.color.white);

                tvCall.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvSms.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvData.setTextColor(mActivity.getResources().getColor(R.color.sc_text_color_primary));
                tvOther.setTextColor(mActivity.getResources().getColor(R.color.white));
                break;
            case R.id.btnSubmit:
                if (fromDate == 0 || toDate == 0) {
                    ToastUtils.makeText(mActivity, mActivity.getString(R.string.sc_select_date_hint));
                    return;
                }
                if (fromDate > toDate) {
                    ToastUtils.makeText(mActivity, mActivity.getString(R.string.sc_select_date_fail));
                    return;
                }
                doCharge(fromDate, toDate);
                break;
        }
    }

    private void doCharge(long from, long to) {
        if (mActivity != null) {
            Bundle bundle = new Bundle();
            bundle.putLong(SCConstants.KEY_DATA.START_DATE, from == 0 ? System.currentTimeMillis() - Calendar.getInstance().getTime().getHours() * DateTimeUtils.HOUR : from);
            bundle.putLong(SCConstants.KEY_DATA.END_DATE, to == 0 ? System.currentTimeMillis() : to);

            bundle.putLong(SCConstants.KEY_DATA.START_DATE, from);
            bundle.putLong(SCConstants.KEY_DATA.END_DATE, to);
            bundle.putInt(SCConstants.KEY_DATA.POST_TYPE, postType);
//            bundle.putDouble(SCConstants.KEY_DATA.FEE, fee);
//            bundle.putDouble(SCConstants.KEY_DATA.COUNT, count);

            ((TabSelfCareActivity) mActivity).gotoPostageDetail(bundle);
//            ((TabSelfCareActivity) mActivity).gotoPostageOverview(bundle);

//            //Reset
//            fromDate = 0;
//            toDate = 0;
        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(mActivity.getResources().getColor(color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}

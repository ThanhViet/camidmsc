package com.metfone.selfcare.module.tiin.hometiin.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.DateUtilitis;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;

public class NormalTiinAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_HORIZOTAL = 2;
    private final int SECTIONE_EMPTY = 0;
    private HomeTiinModel model;
    private Context mContext;
    private TiinListener.OnHomeTiinItemListener listener;
    private int type = 0;

    public NormalTiinAdapter(HomeTiinModel model, Context mContext, TiinListener.OnHomeTiinItemListener listener, int type) {
        this.model = model;
        this.mContext = mContext;
        this.listener = listener;
        this.type = type;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_HORIZOTAL:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_normal_tiin, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        final TiinModel tiinModel = getItem(position);
        if (holder.getView(R.id.iv_cover) != null) {
            ImageBusiness.setImageNew(tiinModel.getImage(), holder.getView(R.id.iv_cover));
        }
        if (holder.getView(R.id.tv_title) != null) {
            holder.setText(R.id.tv_title, tiinModel.getTitle());
        }
        if (type == HomeTiinAdapter.SECTION_NOW) {
            if (holder.getView(R.id.tv_desc) != null) {
                TextView tvDesc = holder.getView(R.id.tv_desc);
                tvDesc.setVisibility(View.VISIBLE);
                tvDesc.setMaxLines(3);
                tvDesc.setText(tiinModel.getLatestTitle());
            }
            if (holder.getView(R.id.tv_category) != null) {
                holder.setVisible(R.id.tv_category, false);
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setVisible(R.id.button_option, false);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickEvent(tiinModel);
                }
            });
        } else {
            if (holder.getView(R.id.tv_category) != null) {
                TextView tvDuration = holder.getView(R.id.tv_category);
                tvDuration.setTextColor(mContext.getResources().getColor(R.color.v5_text_2));
                tvDuration.setVisibility(View.VISIBLE);
                tvDuration.setText(DateUtilitis.calculateDate(mContext, tiinModel.getDatePub()));
            }
            if (holder.getView(R.id.button_option) != null) {
                holder.setVisible(R.id.button_option, true);
                holder.setOnClickListener(R.id.button_option, new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        listener.onItemClickMore(tiinModel);
                    }
                });
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(tiinModel);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            return SECTIONE_HORIZOTAL;
        }
        return SECTIONE_EMPTY;
    }

    public TiinModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

}

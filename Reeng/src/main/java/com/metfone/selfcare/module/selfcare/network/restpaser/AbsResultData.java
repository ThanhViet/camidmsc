/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.module.selfcare.network.restpaser;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;

import java.io.Serializable;

public class AbsResultData implements Serializable {
	private static final long serialVersionUID = -5702141301177401541L;

	@SerializedName("code")
	private int status;

	@SerializedName("errorCode")
	private int errorCode;

	@SerializedName("message")
	private String message;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void showErrorMessage(Context context) {
		if (context == null)
			return;
			ToastUtils.makeText(context, R.string.error_message_default);
	}
}

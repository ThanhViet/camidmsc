/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2019/9/12
 *
 */

package com.metfone.selfcare.module.search.task;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PhoneNumberHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.helper.encrypt.EncryptUtil;
import com.metfone.selfcare.module.search.listener.SearchContactsListener;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.search.model.CreateThreadChat;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.metfone.selfcare.database.constant.ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT;

public class SearchContactsTask extends AsyncTask<String, Void, ArrayList<Object>> {
    private final String TAG = "SearchContactsTask";
    private WeakReference<ApplicationController> mApplication;
    private SearchContactsListener listener;
    private String keySearch;
    private boolean checkJoined = false;
    private boolean hasCreateNewThread = false;
    private Comparator<String> comparatorName;
    private Comparator<String> comparatorKeySearch;
    private Comparator<Object> comparatorContact;
    private Comparator<Object> comparatorMessage;
    private long startTime;
    private int totalMessages;
    private int totalContacts;

    public SearchContactsTask(ApplicationController application) {
        this.mApplication = new WeakReference<>(application);
    }

    public void setListener(SearchContactsListener listener) {
        this.listener = listener;
    }

    public void setCheckJoined(boolean checkJoined) {
        this.checkJoined = checkJoined;
    }

    public void setComparatorNameUnmark(Comparator<String> comparatorName) {
        this.comparatorName = comparatorName;
    }

    public void setComparatorKeySearchUnmark(Comparator<String> comparatorKeySearch) {
        this.comparatorKeySearch = comparatorKeySearch;
    }

    public void setComparatorContacts(Comparator<Object> comparatorContact) {
        this.comparatorContact = comparatorContact;
    }

    public void setComparatorThreadMessages(Comparator<Object> comparatorMessage) {
        this.comparatorMessage = comparatorMessage;
    }

    public void setHasCreateNewThread(boolean hasCreateNewThread) {
        this.hasCreateNewThread = hasCreateNewThread;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareSearchContacts();
    }

    @Override
    protected void onPostExecute(ArrayList<Object> result) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, "search " + keySearch + " has " + result.size() + "|" + totalMessages
                    + "|" + totalContacts + " results on " + (System.currentTimeMillis() - startTime) + " ms.");
        if (listener != null) listener.onFinishedSearchContacts(keySearch, result);
    }

    private boolean isShowHiddenThread = false;

    @Override
    protected ArrayList<Object> doInBackground(String... params) {
        startTime = System.currentTimeMillis();
        ArrayList<Object> list = new ArrayList<>();
        keySearch = params[0];
        if (Utilities.notNull(mApplication)) {
            MessageBusiness messageBusiness = mApplication.get().getMessageBusiness();
            ArrayList<ThreadMessage> listMessages = new ArrayList<>();
            if (Utilities.notEmpty(messageBusiness.getThreadMessageArrayList())) {
                listMessages.addAll(messageBusiness.getThreadMessageArrayList());
            }
            ContactBusiness contactBusiness = mApplication.get().getContactBusiness();
            ArrayList<PhoneNumber> listContacts = new ArrayList<>();
            if (Utilities.notEmpty(contactBusiness.getListNumberAlls())) {
                listContacts.addAll(contactBusiness.getListNumberAlls());
            }
            totalMessages = listMessages.size();
            totalContacts = listContacts.size();
            if (keySearch == null) keySearch = "";
            else keySearch = keySearch.trim().toLowerCase(Locale.US);
            String encryptPIN = EncryptUtil.encryptSHA256(keySearch);
            String currentPIN = mApplication.get().getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
            isShowHiddenThread = !TextUtils.isEmpty(encryptPIN) && encryptPIN.equals(currentPIN);
            String keySearchTmp = TextHelper.convertUnicodeForSearch(keySearch).replaceAll(Constants.PATTERN.KEY_SEARCH_REPLACE, "");
            Log.d(TAG, "search keySearch: " + keySearch + ", keySearchTmp: " + keySearchTmp);
            if (TextUtils.isEmpty(keySearch) || TextUtils.isEmpty(keySearchTmp)) {
                searchWithoutKey(list, listMessages, listContacts);
            } else {
                String keySearchStartWithPlus = "";
                if (keySearch.startsWith("+")) keySearchStartWithPlus = keySearch.substring(1);
                Log.d(TAG, "keySearchStartWithPlus: " + keySearchStartWithPlus);
                if (TextUtils.isDigitsOnly(keySearch)) {
                    searchWithDigits(list, keySearch, listMessages, listContacts);
                } else if (!TextUtils.isEmpty(keySearchStartWithPlus) && TextUtils.isDigitsOnly(keySearchStartWithPlus)) {
                    searchWithDigits(list, keySearch, listMessages, listContacts);
                } else if (TextUtils.isDigitsOnly(keySearchTmp)) {
                    searchWithDigits(list, keySearchTmp, listMessages, listContacts);
                } else {
                    searchWithText(list, keySearch, listMessages, listContacts);
                }
            }
        }
        return list;
    }

    private void searchWithoutKey(ArrayList<Object> list, ArrayList<ThreadMessage> listMessages, ArrayList<PhoneNumber> listContacts) {
        Log.d(TAG, "searchWithoutKey");
        ArrayList<Object> listResults01 = new ArrayList<>(); //todo messages
        ArrayList<Object> listResults02 = new ArrayList<>(); //todo contacts
        Map<String, Integer> mapMessages = new HashMap<>();
        if (Utilities.notEmpty(listMessages)) {
            for (ThreadMessage item : listMessages) {
                if (item != null) {
                    if (!checkJoined || item.isJoined()) {
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        model.setTitle(item.getThreadName());
                        listResults01.add(model);
                        if (item.getThreadType() == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(item.getSoloNumber()))
                            mapMessages.put(item.getSoloNumber(), 1);
                    }
                }
            }
            listMessages.clear();
        }
        if (Utilities.notEmpty(listContacts)) {
            for (PhoneNumber item : listContacts) {
                if (item != null) {
                    String phoneNumber = item.getJidNumber();
                    if (Utilities.notEmpty(phoneNumber) && !mapMessages.containsKey(phoneNumber)) {
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        model.setTitle(item.getName());
                        listResults02.add(model);
                    }
                }
            }
            listContacts.clear();
        }
        if (Utilities.notEmpty(listResults01)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults01, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults01);
            listResults01.clear();
        }
        if (Utilities.notEmpty(listResults02)) {
            if (comparatorContact != null) {
                try {
                    Collections.sort(listResults02, comparatorContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults02);
            listResults02.clear();
        }
        mapMessages.clear();
    }

    private void searchWithText(ArrayList<Object> list, String keySearch, ArrayList<ThreadMessage> listMessages
            , ArrayList<PhoneNumber> listContacts) {
        //todo keySearch là không phải là chữ
        Log.d(TAG, "searchWithText: " + keySearch);
        ArrayList<Object> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
        ArrayList<Object> listResults02 = new ArrayList<>(); //todo tên bỏ dấu giống hệt keySearch bỏ dấu
        ArrayList<Object> listResults03 = new ArrayList<>(); //todo thread chat có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo thời gian thread chat.
        ArrayList<Object> listResults04 = new ArrayList<>(); //todo danh bạ có: tên bắt đầu bằng keySearch hoặc tên bỏ dấu bắt đầu bằng keySearch bỏ dấu hoặc tên đã bỏ dấu bắt đầu phân tách thành các từ đơn, keySearch đã bỏ dấu phân tách thành các từ đơn, tất cả các từ đơn của kết quả phân tách của keySearch đều là ký tự bắt đầu trong danh sách tất cả các từ đơn của kết qủa phân tách tên. Sắp xếp theo tên abc.
        Map<String, Integer> mapMessages = new HashMap<>();
        String keySearchUnmark = TextHelper.convertUnicodeForSearch(keySearch).toLowerCase(Locale.US).trim();
        List<String> listKeySearch = new ArrayList<>(Arrays.asList(keySearchUnmark.split(Constants.PATTERN.KEY_SEARCH_REPLACE)));
        Log.d(TAG, "listKeySearch: " + listKeySearch);
        int sizeKeySearch = listKeySearch.size();
        if (sizeKeySearch > 1 && comparatorKeySearch != null) {
            Collections.sort(listKeySearch, comparatorKeySearch);
            Log.d(TAG, "listKeySearch: " + listKeySearch);
        }
        if (Utilities.notEmpty(listMessages)) {
            ArrayList<ThreadMessage> listTmp = new ArrayList<>();
            for (ThreadMessage item : listMessages) {
                if (item != null && (!checkJoined || item.isJoined())) {
                    String nameForSearch = TextHelper.convertUnicodeForSearch(item.getThreadName()).toLowerCase(Locale.US).trim();
                    if (nameForSearch.contains(keySearchUnmark)) {
                        listTmp.add(item);
                    } else {
                        boolean check = true;
                        for (String key : listKeySearch) {
                            if (key != null && !nameForSearch.contains(key)) {
                                check = false;
                                break;
                            }
                        }
                        if (check) listTmp.add(item);
                    }
                }
            }
            listMessages.clear();
            // thuc hien tim kiem thread chat
            for (ThreadMessage item : listTmp) {
                String title = item.getThreadName().trim();
                int lengthTitle = title.length();
                String name = title.toLowerCase(Locale.US);
                if (Utilities.notEmpty(name)) {
                    int threadType = item.getThreadType();
                    String phoneNumber = "";
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(item.getSoloNumber()))
                        phoneNumber = item.getSoloNumber();
                    String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                    String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                    if (name.equalsIgnoreCase(keySearch)) {
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(title);
                        int startingIndex = name.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitleSpannable(builder);
                        listResults01.add(model);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(title);
                        int startingIndex = nameUnmark.indexOf(keySearchUnmark);
                        int endingIndex = startingIndex + keySearchUnmark.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitleSpannable(builder);
                        listResults02.add(model);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (name.startsWith(keySearch)) {
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(title);
                        int startingIndex = name.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitleSpannable(builder);
                        listResults03.add(model);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else if (nameUnmark.startsWith(keySearchUnmark)) {
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(title);
                        int startingIndex = nameUnmark.indexOf(keySearchUnmark);
                        int endingIndex = startingIndex + keySearchUnmark.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitleSpannable(builder);
                        listResults03.add(model);
                        if (Utilities.notEmpty(phoneNumber)) mapMessages.put(phoneNumber, 1);
                    } else {
                        //old filter
                        boolean check = false;
                        List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                        int sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            SpannableStringBuilder builder = new SpannableStringBuilder(title);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (Utilities.notEmpty(nameTmp) && nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            int startingIndex = nameUnmark.indexOf(nameTmp);
                                            int endingIndex = startingIndex + tmp.length();
                                            if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                                                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                                            }
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) {
                                ContactProvisional model = new ContactProvisional();
                                model.setContact(item);
                                model.setTitleSpannable(builder);
                                listResults03.add(model);
                                if (Utilities.notEmpty(phoneNumber))
                                    mapMessages.put(phoneNumber, 1);
                            }
                        }
                        if (!check) {
                            //new filter
                            listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                            sizeNameUnmark = listNameUnmark.size();
                            if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                                if (comparatorName != null && sizeNameUnmark > 1)
                                    Collections.sort(listNameUnmark, comparatorName);
                                SpannableStringBuilder builder = new SpannableStringBuilder(title);
                                for (String tmp : listKeySearch) {
                                    if (Utilities.notEmpty(tmp)) {
                                        boolean checkTmp = false;
                                        for (String nameTmp : listNameUnmark) {
                                            if (Utilities.notEmpty(nameTmp) && nameTmp.startsWith(tmp)) {
                                                checkTmp = true;
                                                int startingIndex = nameUnmark.indexOf(nameTmp);
                                                int endingIndex = startingIndex + tmp.length();
                                                if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                                                    builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                                                }
                                                listNameUnmark.remove(nameTmp);
                                                break;
                                            }
                                        }
                                        check = checkTmp;
                                        if (!check) break;
                                    }
                                }
                                if (check) {
                                    ContactProvisional model = new ContactProvisional();
                                    model.setContact(item);
                                    model.setTitleSpannable(builder);
                                    listResults03.add(model);
                                    if (Utilities.notEmpty(phoneNumber))
                                        mapMessages.put(phoneNumber, 1);
                                }
                            }
                        }
                    }
                }
            }
            listTmp.clear();
        }

        if (Utilities.notEmpty(listContacts)) {
            ArrayList<PhoneNumber> listTmp = new ArrayList<>();
            for (PhoneNumber item : listContacts) {
                if (item != null) {
                    String phoneNumber = item.getJidNumber();
                    if (Utilities.notEmpty(phoneNumber) && !mapMessages.containsKey(phoneNumber)) {
                        String nameForSearch = TextHelper.convertUnicodeForSearch(item.getName()).toLowerCase(Locale.US).trim();
                        if (nameForSearch.contains(keySearchUnmark)) {
                            listTmp.add(item);
                        } else {
                            boolean check = true;
                            for (String key : listKeySearch) {
                                if (key != null && !nameForSearch.contains(key)) {
                                    check = false;
                                    break;
                                }
                            }
                            if (check) listTmp.add(item);
                        }
                    }
                }
            }
            listContacts.clear();
            // thuc hien tim kiem danh bạ
            for (PhoneNumber item : listTmp) {
                String title = item.getName().trim();
                int lengthTitle = title.length();
                String name = title.toLowerCase(Locale.US);
                String nameForSearch = TextHelper.convertUnicodeForSearch(title);
                String nameUnmark = nameForSearch.toLowerCase(Locale.US);
                if (name.equalsIgnoreCase(keySearch)) {
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(title);
                    int startingIndex = name.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitleSpannable(builder);
                    listResults01.add(model);
                } else if (nameUnmark.equalsIgnoreCase(keySearchUnmark)) {
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(title);
                    int startingIndex = nameUnmark.indexOf(keySearchUnmark);
                    int endingIndex = startingIndex + keySearchUnmark.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitleSpannable(builder);
                    listResults02.add(model);
                } else if (name.startsWith(keySearch)) {
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(title);
                    int startingIndex = name.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitleSpannable(builder);
                    listResults04.add(model);
                } else if (nameUnmark.startsWith(keySearchUnmark)) {
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(title);
                    int startingIndex = nameUnmark.indexOf(keySearchUnmark);
                    int endingIndex = startingIndex + keySearchUnmark.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitleSpannable(builder);
                    listResults04.add(model);
                } else {
                    //old filter
                    boolean check = false;
                    List<String> listNameUnmark = SearchUtils.getListSplitName(nameForSearch);
                    int sizeNameUnmark = listNameUnmark.size();
                    if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                        if (comparatorName != null && sizeNameUnmark > 1)
                            Collections.sort(listNameUnmark, comparatorName);
                        SpannableStringBuilder builder = new SpannableStringBuilder(title);
                        for (String tmp : listKeySearch) {
                            if (Utilities.notEmpty(tmp)) {
                                boolean checkTmp = false;
                                for (String nameTmp : listNameUnmark) {
                                    if (nameTmp.startsWith(tmp)) {
                                        checkTmp = true;
                                        int startingIndex = nameUnmark.indexOf(nameTmp);
                                        int endingIndex = startingIndex + tmp.length();
                                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                                        }
                                        listNameUnmark.remove(nameTmp);
                                        break;
                                    }
                                }
                                check = checkTmp;
                                if (!check) break;
                            }
                        }
                        if (check) {
                            ContactProvisional model = new ContactProvisional();
                            model.setContact(item);
                            model.setTitleSpannable(builder);
                            listResults04.add(model);
                        }
                    }
                    if (!check) {
                        //new filter
                        listNameUnmark = SearchUtils.getListSplitKey(nameForSearch);
                        sizeNameUnmark = listNameUnmark.size();
                        if (sizeNameUnmark > 1 || (sizeNameUnmark == 1 && !nameUnmark.equalsIgnoreCase(listNameUnmark.get(0)))) {
                            if (comparatorName != null && sizeNameUnmark > 1)
                                Collections.sort(listNameUnmark, comparatorName);
                            SpannableStringBuilder builder = new SpannableStringBuilder(title);
                            for (String tmp : listKeySearch) {
                                if (Utilities.notEmpty(tmp)) {
                                    boolean checkTmp = false;
                                    for (String nameTmp : listNameUnmark) {
                                        if (nameTmp.startsWith(tmp)) {
                                            checkTmp = true;
                                            int startingIndex = nameUnmark.indexOf(nameTmp);
                                            int endingIndex = startingIndex + tmp.length();
                                            if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                                                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                                            }
                                            listNameUnmark.remove(nameTmp);
                                            break;
                                        }
                                    }
                                    check = checkTmp;
                                    if (!check) break;
                                }
                            }
                            if (check) {
                                ContactProvisional model = new ContactProvisional();
                                model.setContact(item);
                                model.setTitleSpannable(builder);
                                listResults04.add(model);
                            }
                        }
                    }
                }
            }
            listTmp.clear();
        }

        if (Utilities.notEmpty(listResults01)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults01, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults01);
            listResults01.clear();
        }
        if (Utilities.notEmpty(listResults02)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults02, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults02);
            listResults02.clear();
        }
        if (Utilities.notEmpty(listResults03)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults03, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults03);
            listResults03.clear();
        }
        if (Utilities.notEmpty(listResults04)) {
            if (comparatorContact != null) {
                try {
                    Collections.sort(listResults04, comparatorContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults04);
            listResults04.clear();
        }
        mapMessages.clear();
    }

    private void searchWithDigits(ArrayList<Object> list, String keySearch
            , ArrayList<ThreadMessage> listMessages, ArrayList<PhoneNumber> listContacts) {
        //todo keySearch là số
        String oldKeySearch = keySearch;
        keySearch = SearchUtils.getKeySearchChat(keySearch);
        String tmpNumber = PhoneNumberHelper.getInstant().getPhoneNumberFromText(mApplication.get(), keySearch);
        if (Utilities.notEmpty(tmpNumber)) {
            keySearch = tmpNumber;
        }
        Log.d(TAG, "searchWithDigits oldKeySearch: " + oldKeySearch + ", keySearch: " + keySearch);
        ArrayList<Object> listResults01 = new ArrayList<>(); //todo tên giống hệt keySearch
        ArrayList<Object> listResults02 = new ArrayList<>(); //todo SĐT giống hệt keySearch
        ArrayList<Object> listResults03 = new ArrayList<>(); //todo tên bắt đầu bằng keySearch
        ArrayList<Object> listResults04 = new ArrayList<>(); //todo SĐT bắt đầu bằng keySearch
        ArrayList<Object> listResults05 = new ArrayList<>(); //todo SĐT chứa bằng keySearch
        ArrayList<Object> listResults06 = new ArrayList<>(); //todo SĐT của 1 thành viên bất kỳ trong nhóm chứa keySearch
        ArrayList<Object> listResults07 = new ArrayList<>(); //todo thread ẩn
        Map<String, Integer> mapMessages = new HashMap<>();
        Map<String, Integer> mapContacts = new HashMap<>();
        if (Utilities.notEmpty(listMessages)) {
            ArrayList<ThreadMessage> listTmp = new ArrayList<>();
            for (ThreadMessage item : listMessages) {
                if (item != null && !checkJoined || item.isJoined()) {
                    String name = item.getThreadName();
                    if (name == null) name = "";
                    else name = name.trim();
                    String phoneNumber = item.getSoloNumber();
                    if (phoneNumber == null) phoneNumber = "";
                    if (name.contains(keySearch) || phoneNumber.contains(keySearch)
                            || item.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                        listTmp.add(item);
                    } else if (isShowHiddenThread && item.getHiddenThread() == 1) {
                        listTmp.add(item);
                    }
                }
            }
            listMessages.clear();
            // thuc hien tim kiem thread chat
            for (ThreadMessage item : listTmp) {
                String name = item.getThreadName();
                if (name == null) name = "";
                else name = name.trim();
                int threadType = item.getThreadType();
                String phoneNumber = item.getSoloNumber();
                if (phoneNumber == null) phoneNumber = "";
                int lengthTitle;
                boolean isAdded = false;
                if (name.equals(keySearch)) {
                    lengthTitle = name.length();
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(name);
                    int startingIndex = name.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitleSpannable(builder);
                    model.setDescription(phoneNumber);
                    listResults01.add(model);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                    isAdded = true;
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !item.isStranger() && phoneNumber.equals(keySearch)) {
                    lengthTitle = phoneNumber.length();
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                    int startingIndex = phoneNumber.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitle(name);
                    model.setDescriptionSpannable(builder);
                    listResults02.add(model);
                    mapMessages.put(phoneNumber, 1);
                    isAdded = true;
                } else if (name.startsWith(keySearch)) {
                    lengthTitle = name.length();
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(name);
                    int startingIndex = name.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitleSpannable(builder);
                    model.setDescription(phoneNumber);
                    listResults03.add(model);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                    isAdded = true;
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !item.isStranger() && phoneNumber.startsWith(keySearch)) {
                    lengthTitle = phoneNumber.length();
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                    int startingIndex = phoneNumber.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitle(name);
                    model.setDescriptionSpannable(builder);
                    listResults04.add(model);
                    mapMessages.put(phoneNumber, 1);
                    isAdded = true;
                } else if (threadType == TYPE_THREAD_PERSON_CHAT && !item.isStranger() && phoneNumber.contains(keySearch)) {
                    lengthTitle = phoneNumber.length();
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                    int startingIndex = phoneNumber.indexOf(keySearch);
                    int endingIndex = startingIndex + keySearch.length();
                    if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                        builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                    }
                    model.setTitle(name);
                    model.setDescriptionSpannable(builder);
                    listResults05.add(model);
                    mapMessages.put(phoneNumber, 1);
                    isAdded = true;
                } else if (threadType == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT) {
                    ArrayList<String> phoneNumbers = item.getPhoneNumbers();
                    for (String phone : phoneNumbers) {
                        if (Utilities.notEmpty(phone) && phone.contains(keySearch)) {
                            ContactProvisional model = new ContactProvisional();
                            model.setContact(item);
                            String descTmp = mApplication.get().getResources().getString(R.string.group_member, phone);
                            lengthTitle = descTmp.length();
                            SpannableStringBuilder builder = new SpannableStringBuilder(descTmp);
                            int startingIndex = descTmp.indexOf(keySearch);
                            int endingIndex = startingIndex + keySearch.length();
                            if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                            }
                            model.setTitle(name);
                            model.setDescriptionSpannable(builder);
                            listResults06.add(model);
                            isAdded = true;
                            break;
                        }
                    }
                }
                if (!isAdded && isShowHiddenThread && item.getHiddenThread() == 1) {
                    ContactProvisional model = new ContactProvisional();
                    model.setContact(item);
                    SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                    model.setTitle(name);
                    model.setDescriptionSpannable(builder);
                    listResults07.add(model);
                    if (threadType == TYPE_THREAD_PERSON_CHAT && Utilities.notEmpty(phoneNumber))
                        mapMessages.put(phoneNumber, 1);
                }
            }
            listTmp.clear();
        }
        if (Utilities.notEmpty(listContacts)) {
            ArrayList<PhoneNumber> listTmp = new ArrayList<>();
            for (PhoneNumber item : listContacts) {
                if (item != null) {
                    String name = item.getName();
                    if (name == null) name = "";
                    else name = name.trim();
                    String phoneNumber = item.getJidNumber();
                    if (phoneNumber == null) phoneNumber = "";
                    if (Utilities.notEmpty(phoneNumber) && !mapMessages.containsKey(phoneNumber)
                            && (name.contains(keySearch) || phoneNumber.contains(keySearch))) {
                        item.setName(name);
                        listTmp.add(item);
                    } else {
                        if (keySearch.startsWith("+") && keySearch.length() > 3) {
                            String rawNumber;
                            if (phoneNumber.startsWith("0")) {// so vn
                                rawNumber = "+84" + phoneNumber.substring(1);
                            } else {
                                rawNumber = item.getRawNumber();
                            }
                            if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                                item.setName(name);
                                listTmp.add(item);
                            }
                        }
                    }
                }
            }
            listContacts.clear();
            // thuc hien tim kiem danh bạ
            for (PhoneNumber item : listTmp) {
                if (item != null) {
                    String phoneNumber = item.getJidNumber();
                    String name = item.getName();
                    if (name == null) name = "";
                    else name = name.trim();
                    int lengthTitle;
                    if (name.equals(keySearch)) {
                        lengthTitle = name.length();
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(name);
                        int startingIndex = name.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitleSpannable(builder);
                        model.setDescription(phoneNumber);
                        listResults01.add(model);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.equals(keySearch)) {
                        lengthTitle = phoneNumber.length();
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                        int startingIndex = phoneNumber.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitle(name);
                        model.setDescriptionSpannable(builder);
                        listResults02.add(model);
                        mapContacts.put(phoneNumber, 1);
                    } else if (name.startsWith(keySearch)) {
                        lengthTitle = name.length();
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(name);
                        int startingIndex = name.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitleSpannable(builder);
                        model.setDescription(phoneNumber);
                        listResults03.add(model);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.startsWith(keySearch)) {
                        lengthTitle = phoneNumber.length();
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                        int startingIndex = phoneNumber.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitle(name);
                        model.setDescriptionSpannable(builder);
                        listResults04.add(model);
                        mapContacts.put(phoneNumber, 1);
                    } else if (phoneNumber.contains(keySearch)) {
                        lengthTitle = phoneNumber.length();
                        ContactProvisional model = new ContactProvisional();
                        model.setContact(item);
                        SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                        int startingIndex = phoneNumber.indexOf(keySearch);
                        int endingIndex = startingIndex + keySearch.length();
                        if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                            builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                        }
                        model.setTitle(name);
                        model.setDescriptionSpannable(builder);
                        listResults05.add(model);
                        mapContacts.put(phoneNumber, 1);
                    } else if (keySearch.startsWith("+") && keySearch.length() > 3) {
                        lengthTitle = phoneNumber.length();
                        String rawNumber;
                        if (phoneNumber.startsWith("0")) {// so vn
                            rawNumber = "+84" + phoneNumber.substring(1);
                        } else {
                            rawNumber = item.getRawNumber();
                        }
                        if (Utilities.notEmpty(rawNumber) && rawNumber.contains(keySearch)) {
                            String tmp = keySearch.substring(3);
                            ContactProvisional model = new ContactProvisional();
                            model.setContact(item);
                            SpannableStringBuilder builder = new SpannableStringBuilder(phoneNumber);
                            int startingIndex = phoneNumber.indexOf(tmp);
                            int endingIndex = startingIndex + tmp.length();
                            if (lengthTitle > 0 && endingIndex <= lengthTitle && startingIndex >= 0 && endingIndex >= 0) {
                                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                            }
                            model.setTitle(name);
                            model.setDescriptionSpannable(builder);
                            listResults05.add(model);
                            mapContacts.put(phoneNumber, 1);
                        }
                    }
                }
            }
            listContacts.clear();
        }
        if (Utilities.notEmpty(listResults01)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults01, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults01);
            listResults01.clear();
        }
        if (Utilities.notEmpty(listResults02)) {
            if (comparatorContact != null) {
                try {
                    Collections.sort(listResults02, comparatorContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults02);
            listResults02.clear();
        }
        if (Utilities.notEmpty(listResults03)) {
            if (comparatorContact != null) {
                try {
                    Collections.sort(listResults03, comparatorContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults03);
            listResults03.clear();
        }
        if (Utilities.notEmpty(listResults04)) {
            if (comparatorContact != null) {
                try {
                    Collections.sort(listResults04, comparatorContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults04);
            listResults04.clear();
        }
        if (Utilities.notEmpty(listResults05)) {
            if (comparatorContact != null) {
                try {
                    Collections.sort(listResults05, comparatorContact);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults05);
            listResults05.clear();
        }
        if (Utilities.notEmpty(listResults06)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults06, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults06);
            listResults06.clear();
        }
        if (Utilities.notEmpty(listResults07)) {
            if (comparatorMessage != null) {
                try {
                    Collections.sort(listResults07, comparatorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            list.addAll(listResults07);
            listResults07.clear();
        }
        if (hasCreateNewThread) {
            if (!mapMessages.containsKey(keySearch) && !mapContacts.containsKey(keySearch)
                    && PhoneNumberHelper.getInstant().isValidPhoneNumber(mApplication.get(), keySearch)) {
                if (list == null) list = new ArrayList<>();
                CreateThreadChat item = new CreateThreadChat();
                item.setName(keySearch);
                item.setKeySearchChat(keySearch);
                list.add(item);
            }
        }
        mapMessages.clear();
        mapContacts.clear();
    }
}
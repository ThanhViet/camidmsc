package com.metfone.selfcare.module.netnews.ChildNews.presenter;


import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;

public interface IChildNewsPresenter extends MvpPresenter {

    void loadData(int cateId, int page, long unixTime);
    void updateLastNews(SharedPref sharedPref, int id);
    int getLastNews(SharedPref sharedPref);
}

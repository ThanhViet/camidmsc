package com.metfone.selfcare.module.home_kh.notification.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.fragment.setting.BaseSettingKhFragment;
import com.metfone.selfcare.module.home_kh.notification.model.KhNotificationItem;
import com.metfone.selfcare.ui.LoadingView;

import butterknife.BindView;
import butterknife.OnClick;

public class FragmentWebViewNotification extends BaseSettingKhFragment {

    @Override
    protected void initView(View view) {

    }

    @Override
    public String getName() {
        return null;
    }

    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtTitle;
    @BindView(R.id.loading_view)
    LoadingView loading_view;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.footer)
    View footer;
    @BindView(R.id.btnNext)
    View btnNext;

    public static FragmentWebViewNotification newInstance(KhNotificationItem item) {
        FragmentWebViewNotification fragment = new FragmentWebViewNotification();
        Bundle args = new Bundle();
        args.putSerializable("Item", item);
        fragment.setArguments(args);
        return fragment;
    }

    private KhNotificationItem notificationItem;


    @OnClick(R.id.icBackToolbar)
    void onBack() {
        popBackStackFragment();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_kh_notification_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("Item")) {
            notificationItem = getArguments().getParcelable("Item");
            if (notificationItem != null) {
                String title = "";
                if (LocaleManager.isKmLanguage(getContext())) {
                    title = notificationItem.titleKh;
                } else {
                    title = notificationItem.titleEn;
                }
                txtTitle.setText(title != null ? title : "");

                webview.getSettings().setJavaScriptEnabled(true);
//                if (notificationItem.getValue(getContext()) != null) {
//                    webview.loadUrl(notificationItem.getValue(getContext()));
//                }

                if (notificationItem.type.equals("landing_page_have_link")) {
                    footer.setVisibility(View.VISIBLE);
                    if (notificationItem.link != null && notificationItem.link.startsWith("http")) {
                        btnNext.setOnClickListener(new OnSingleClickListener() {
                            @Override
                            public void onSingleClick(View view) {
                                try {
                                    Uri uri = Uri.parse(notificationItem.link);
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                } catch (Exception e) {

                                }
                            }
                        });
                    }
                } else {
                    footer.setVisibility(View.GONE);
                }
            }
        }
    }
}

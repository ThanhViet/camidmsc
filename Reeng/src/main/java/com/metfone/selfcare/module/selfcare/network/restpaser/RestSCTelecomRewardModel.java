package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCTelecomRewardModel;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCTelecomRewardModel extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCTelecomRewardModel> data = new ArrayList<>();

    public ArrayList<SCTelecomRewardModel> getData() {
        return data;
    }

    public void setData(ArrayList<SCTelecomRewardModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCTelecomRewardModel [data=" + data + "] errror " + getErrorCode();
    }
}

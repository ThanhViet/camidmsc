package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.image.ImageManager;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.base.BaseAdapterRecyclerView;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.holder.RankHomeHolder;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.RankModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

public class RankAdapter extends BaseAdapterRecyclerView {
    AbsInterface.OnItemListener onClick;
    private RankModel datas;

    RankAdapter(Context context, RankModel itemsList, AbsInterface.OnItemListener listener) {
        super(context, "");
        datas = itemsList;
        onClick = listener;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onBindViewHolder(BaseHolder holder, final int position) {
        if (holder instanceof RankHomeHolder) {
            final AllModel item = getItem(position);
            if (item != null) {
                RankHomeHolder itemHolder = (RankHomeHolder) holder;
                itemHolder.tvName.setText(item.getName());
                itemHolder.tvPosition.setText(String.valueOf(position + 1));
                itemHolder.tvName.setSelected(position == 0);
                itemHolder.btnOption.setSelected(position == 0);
                if (position == 0) {
                    itemHolder.tvPosition.setBackgroundResource(R.drawable.ic_position_01);
                    if (itemHolder.cover != null) {
                        ImageManager.showImageNormalV2(datas.getImage(), itemHolder.cover, ApplicationController.self().getRoundedCornersTransformation());
                        itemHolder.cover.setOnClickListener(v -> {
                            if (mContext instanceof TabKeengActivity) {
                                ((TabKeengActivity) mContext).gotoRankDetail(datas);
                            }
                        });
                    }
                } else if (position == 1) {
                    itemHolder.tvPosition.setBackgroundResource(R.drawable.ic_position_02);
                } else if (position == 2) {
                    itemHolder.tvPosition.setBackgroundResource(R.drawable.ic_position_03);
                } else {
                    itemHolder.tvPosition.setBackgroundResource(R.drawable.ic_position_04);
                }
                if (TextUtils.isEmpty(item.getSinger()))
                    itemHolder.tvSinger.setVisibility(View.GONE);
                else {
                    itemHolder.tvSinger.setVisibility(View.VISIBLE);
                    itemHolder.tvSinger.setText(item.getSinger());
                }
                ImageBusiness.setSong(item.getImage(), itemHolder.image, position, ApplicationController.self().getRound());
                if (itemHolder.mediaView != null)
                    itemHolder.mediaView.setOnClickListener(v -> {
                        if (onClick != null) {
                            onClick.onItemRankClick(datas, position);
                        }
                    });
                if (itemHolder.btnOption != null)
                    itemHolder.btnOption.setOnClickListener(v -> {
//                            if (mContext instanceof MainActivity) {
//                                ((MainActivity) mContext).showPopupMore(item);
//                            }
                        if (onClick != null)
                            onClick.onItemClickOption(item);
                    });
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            if (position == 0)
                return ITEM_RANK_HOME_TOP;
            else
                return ITEM_RANK_HOME;
        }

        return ITEM_EMPTY;
    }

    @Override
    public int getItemCount() {
        if (datas != null) {
            int size = datas.getListSong().size();
            return size <= 5 ? size : 5;
        }
        return 0;
    }

    @Override
    public AllModel getItem(int position) {
        try {
            return datas.getListSong().get(position);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        return null;
    }
}
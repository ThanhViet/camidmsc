package com.metfone.selfcare.module.home_kh.notification.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.LocaleManager;

import java.io.Serializable;

public class KhNotificationItem implements Serializable {
    /*
    {
                    ""type"": ""movie_profile"",
                    ""value"": ""234"",
                    ""readStatus"": 1,
                    ""iconUrl"": ""https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Circle-icons-image.svg/512px-Circle-icons-image.svg.png"",
                    ""time"": ""Sep 05, 2020""
                }
    * */
    public int notifyId;
    public String type;
    public String iconUrl;
    public String time;
    public String link;
    public String titleEn;
    public String titleKh;
    public String valueEn;
    public String valueKh;
    public String actionType;
    public int readStatus;
    public String actionObjectId;
    public String buttonTitle;
    public String featureImage;
    public int isExchange;
    public String videoUrl;

    public String getTitle(Context context) {
        if (LocaleManager.isKmLanguage(context)) {
            if (!StringUtils.isEmpty(titleKh))
                return titleKh;
        }
        return titleEn;
    }

    public String getValue(Context context) {
        if (LocaleManager.isKmLanguage(context)) {
            if (!StringUtils.isEmpty(valueKh))
                return valueKh;
        }
        return valueEn;
    }

    public interface Status {
        public final int READ = 1;
        public final int UN_READ = 0;
    }

    public interface Type {
        public final String NONE = "none";
        public final String ACTION = "action_type";
        public final String LINK = "link";
    }

    public interface ActionType {
        public final String MOVIE_PROFILE = "movie_profile";
        public final String MOVIE_PLAY_DIRECTLY = "movie_play_directly";
        public final String MOVIE_ACTOR = "movie_actor";
        public final String MOVIE_DIRECTOR = "movie_director";
        public final String MOVIE_CATEGORY = "movie_category";
        public final String ESPORT_CHANNEL_PROFILE = "esport_channel_profile";
        public final String ESPORT_PLAY_DIRECTLY = "esport_play_directly";
        public final String ESPORT_TOURNAMENT_PROFILE = "esport_tournament_profile";
        public final String METFONE_SERVICE = "metfone_service";
        public final String METFONE_TOPUP = "metfone_topup";
        public final String REWARD_DETAIL = "reward_detail";
        public final String GIFT_CATALOG = "tet_2022";
    }

    public static int getTypeIcon(String actionType) {
        if (actionType.contains("movie")) {
            return R.drawable.kh_notic_movie;
        }
        if (actionType.contains("esport")) {
            return R.drawable.kh_notic_esport;
        }
        if (actionType.contains("metfone_service")) {
            return R.drawable.kh_notic_metfone;
        }

        if (actionType.contains("reward")) {
            return R.drawable.kh_notic_camid;
        }
        if (actionType.contains("landing")) {
            return R.drawable.kh_notic_camid;
        }
        if (actionType.contains("metfone_topup")) {
            return R.drawable.kh_notic_metfone;
        }
        return R.drawable.kh_notic_camid;
    }
}

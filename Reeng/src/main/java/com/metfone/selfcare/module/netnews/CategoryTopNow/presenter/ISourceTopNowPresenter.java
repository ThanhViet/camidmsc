package com.metfone.selfcare.module.netnews.CategoryTopNow.presenter;

import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;

/**
 * Created by HaiKE on 8/19/17.
 */

public interface ISourceTopNowPresenter extends MvpPresenter {

    void loadData(int cateId, int page);
}

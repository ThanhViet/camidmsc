package com.metfone.selfcare.module.keeng.fragment.category;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.model.Topic;

public class ChildSingerInfoFragment extends BaseFragment {

    private Topic topic;
    private TextView mSingerInfo;

    public static ChildSingerInfoFragment instances(Bundle argument) {
        ChildSingerInfoFragment mInstance = new ChildSingerInfoFragment();
        mInstance.setArguments(argument);
        return mInstance;
    }

    @Override
    public String getName() {
        return ChildSingerInfoFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_singer_info;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            topic = (Topic) getArguments().getSerializable(Constants.KEY_DATA);
        }
        if (topic != null && mSingerInfo != null) {
            mSingerInfo.setText(Html.fromHtml(topic.getSingerInfo()));
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        mSingerInfo = view.findViewById(R.id.tv_singer_info);
        return view;
    }
}

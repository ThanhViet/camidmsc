package com.metfone.selfcare.module.home_kh.api;

public enum GiftPoint {
    ALL("1"),
    Most_Redeem("2"),// popular
    Nearest("3");// ---> Dùng cho nearByYou

    String value;

    GiftPoint(String vl) {
        value = vl;
    }

    public String getValue() {
        return value;
    }
}

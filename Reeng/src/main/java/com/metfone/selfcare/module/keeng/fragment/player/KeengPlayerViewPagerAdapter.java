package com.metfone.selfcare.module.keeng.fragment.player;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.metfone.selfcare.module.keeng.widget.CustomFragmentPagerAdapter;

public class KeengPlayerViewPagerAdapter extends CustomFragmentPagerAdapter {

    public KeengPlayerViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }
}

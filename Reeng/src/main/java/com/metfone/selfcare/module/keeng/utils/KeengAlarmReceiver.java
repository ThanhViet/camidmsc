package com.metfone.selfcare.module.keeng.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.helper.Constants;

import java.util.Calendar;

public class KeengAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String action = intent.getAction();
            if (BuildConfig.DEBUG) {
                String message = intent.getStringExtra(Constants.KEY_DATA);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                int currentMinute = calendar.get(Calendar.MINUTE);
                int currentSecond = calendar.get(Calendar.SECOND);
                Log.e("AlarmReceiver", action + ": " + currentHour + "h " + currentMinute + "m " + currentSecond + "s message: " + message
                        + "\n-----------------------------------------");
            }
            if (!TextUtils.isEmpty(action)) {
                switch (action) {
                    case AlarmUtils.ACTION_ALARM_STOP_MUSIC:
                    case AlarmUtils.ACTION_ALARM_EXIT_APP:
                        try {
                            ApplicationController mApplication = (ApplicationController) context.getApplicationContext();
                            mApplication.getPlayMusicController().closeMusic();
                        } catch (Exception ex) {

                        }
                        break;

                    case AlarmUtils.ACTION_ALARM_CANCEL:
                        break;
                }
            }
        } catch (Exception e) {
            Log.e("AlarmReceiver", e);
        }
    }
}

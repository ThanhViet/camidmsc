package com.metfone.selfcare.module.netnews.EventNews.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.module.netnews.EventNews.adapter.EventAdapter;
import com.metfone.selfcare.module.netnews.EventNews.presenter.EventPresenter;
import com.metfone.selfcare.module.netnews.EventNews.presenter.IEventPresenter;
import com.metfone.selfcare.module.netnews.EventNews.view.IEventView;
import com.metfone.selfcare.module.netnews.activity.NetNewsActivity;
import com.metfone.selfcare.module.newdetails.fragment.BaseFragment;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.EventModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.CustomLoadMoreView;
import com.metfone.selfcare.module.response.EventResponse;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by HaiKE on 8/19/17.
 */

public class EventFragment extends BaseFragment implements IEventView, BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener, AbsInterface.OnEventListener {

    @BindView(R.id.loadingView)
    View loadingView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    int currentPage = 1;
    EventAdapter adapter;
    LinearLayoutManager layoutManager;
    ArrayList<EventModel> datas = new ArrayList<>();
    View notDataView, errorView;
    boolean isRefresh;

    IEventPresenter mPresenter;

    public static EventFragment newInstance() {
        EventFragment fragment = new EventFragment();
        fragment.mPresenter = new EventPresenter();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        if (mPresenter == null) {
            mPresenter = new EventPresenter();
        }
        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        setUp(view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        tvTitle.setText(R.string.title_event);
        layout_refresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        layout_refresh.setOnRefreshListener(this);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getBaseActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new EventAdapter(getBaseActivity(), datas, this);
        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(this);
        adapter.openLoadAnimation(BaseQuickAdapter.ALPHAIN);
//        adapter.setAutoLoadMoreSize(3);
        recyclerView.setAdapter(adapter);
//        recyclerView.setPadding(recyclerView.getPaddingLeft(), getResources().getDimensionPixelOffset(R.dimen.padding_news), recyclerView.getPaddingRight(), recyclerView.getPaddingBottom());

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getBaseActivity(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getBaseActivity().getResources().getDrawable(R.drawable.divider_vertical));
        recyclerView.addItemDecoration(dividerItemDecoration);

        notDataView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_nodata, (ViewGroup) recyclerView.getParent(), false);
        notDataView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onRefresh();
            }
        });
        errorView = getBaseActivity().getLayoutInflater().inflate(R.layout.item_failed, (ViewGroup) recyclerView.getParent(), false);
        errorView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onRefresh();
            }
        });

        isRefresh = true;
        if(datas == null || ( datas.size() == 0))
        {
            loadingView.setVisibility(View.VISIBLE);
            mPresenter.loadData(currentPage);
        }


    }

    @Override
    public void loadDataSuccess(boolean flag) {
        loadingView.setVisibility(View.GONE);
        hideRefresh();
        if(!flag)
        {
            loadingFail();
        }
    }

    @Override
    public void bindData(EventResponse response) {
        if(datas == null)
            datas = new ArrayList<>();

        hideRefresh();
        if(response != null)
        {
            if(response.getData() != null)
            {
                loadingComplete(response.getData());
            }
            else
            {
                loadingFail();
            }
        }
    }

    public void loadingComplete(ArrayList<EventModel> response)
    {
        int mCurrentCounter = response.size();
        if(isRefresh)
        {
            if(mCurrentCounter == 0)
            {
                adapter.setEmptyView(notDataView);
            }
            else
            {
                datas.clear();
                adapter.setNewData(response);
                datas.addAll(response);
            }
        }
        else
        {
            if(mCurrentCounter == 0)
            {
                adapter.loadMoreEnd();
            }
            else
            {
                adapter.addData(response);
                datas.addAll(response);
                adapter.loadMoreComplete();
            }
        }
    }

    public void loadingFail()
    {
        if(isRefresh)
        {
            adapter.setEmptyView(errorView);
        }
        else
        {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        mPresenter.loadData(currentPage);
    }

    @Override
    public void onLoadMoreRequested() {
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                currentPage ++;
                isRefresh = false;
                mPresenter.loadData(currentPage);
            }

        },1000);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btnBack)
    public void onBackClick()
    {
        getBaseActivity().onBackPressed();
    }

    @Override
    public void onItemClick(NewsModel model) {
        model.setReadFromSource(CommonUtils.TYPE_NEWS_DETAIL_FROM_EVENT);
        readNews(model);
    }

    @Override
    public void onItemEventClick(int pos) {
        if (getBaseActivity() == null) return;
        EventModel model = datas.get(pos);
        if (getBaseActivity() instanceof NetNewsActivity) {
            Bundle bundle = new Bundle();
            bundle.putInt(CommonUtils.KEY_CATEGORY_ID, model.getCategoryID());
            bundle.putString(CommonUtils.KEY_CATEGORY_NAME, model.getEventName());
            ((NetNewsActivity) getBaseActivity()).showFragment(CommonUtils.TAB_NEWS_BY_EVENT, bundle,true);
        } else {
            //start NetNewsActivity
            Intent intent = new Intent(getBaseActivity(), NetNewsActivity.class);
            intent.putExtra(CommonUtils.KEY_CATEGORY_ID, model.getCategoryID());
            intent.putExtra(CommonUtils.KEY_CATEGORY_NAME, model.getEventName());
            intent.putExtra(CommonUtils.KEY_TAB, CommonUtils.TAB_NEWS_BY_EVENT);
            getBaseActivity().startActivity(intent);
        }
    }
    @OnClick(R.id.ivSearch)
    public void clickSearch(){
        Utilities.openSearch(getBaseActivity(), Constants.TAB_NEWS_HOME);
    }

    @Override
    public void onItemClickMore(NewsModel model) {
        DialogUtils.showOptionNewsItem(getBaseActivity(), model, new OnClickMoreItemListener() {
            @Override
            public void onClickMoreItem(Object object, int menuId) {
                clickItemOptionNew(object, menuId);
            }
        });
    }
}

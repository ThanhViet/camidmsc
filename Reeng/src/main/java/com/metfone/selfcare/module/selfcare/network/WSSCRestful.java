package com.metfone.selfcare.module.selfcare.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.database.model.ReengAccount;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.httprequest.HttpHelper;
import com.metfone.selfcare.module.keeng.network.restful.AbsRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestAirTimeInfo;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCAccountData;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCBanner;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPackage;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPackageDetail;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCPostageDetail;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCRecommentPackage;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCStore;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCSubContentModel;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCSubModel;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.restful.GsonRequest;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.restful.StringRequest;

import okhttp3.MediaType;

public class WSSCRestful extends AbsRestful {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public final static String BASE_URL_SELF_CARE = "/ReengBackendBiz/selfcare/accounts/";
//    public final static String BASE_URL_SELF_CARE = SCConstants.URL.BASE_URL + "/selfcare/accounts/";

    public final static String GET_SUB = BASE_URL_SELF_CARE + "wsGetSubAccountInfo";
    public final static String GET_HOME_BANNER = BASE_URL_SELF_CARE + "wsGetBanner";
    public final static String GET_ACCOUNT_INFO = BASE_URL_SELF_CARE + "wsGetAccountsDetail";
    public final static String GET_PACKAGE_DETAIL = BASE_URL_SELF_CARE + "wsGetServiceDetail";
    public final static String GET_POSTAGE_DETAIL = BASE_URL_SELF_CARE + "wsGetPostageDetailInfo";
    public final static String GET_RECOMMENT_PACKAGE = BASE_URL_SELF_CARE + "wsGetAllDataAddon";
    public final static String GET_RECOMMENT_SERVICES = BASE_URL_SELF_CARE + "wsGetServices";
    public final static String GET_LIST_PACKAGE = BASE_URL_SELF_CARE + "wsGetServicesByGroup";
    public final static String GET_REGISTER_PACKAGE = BASE_URL_SELF_CARE + "wsGetCurrentUsedServices";
    public final static String GET_REGISTER_SERVICES = BASE_URL_SELF_CARE + "wsGetCurrentUsedServices";
    public final static String GET_STORES = BASE_URL_SELF_CARE + "wsGetNearestStores";
    public final static String RECHARGE = BASE_URL_SELF_CARE + "wsTopUp";
    public final static String REGISTER_PACKAGE = BASE_URL_SELF_CARE + "wsDoActionServiceAddOn";
    public final static String TRANSFER_ISHARE = BASE_URL_SELF_CARE + "wsIshare";
    public final static String ISHARE_CHANGE_PASS = BASE_URL_SELF_CARE + "wsResetPasswordIshare";
    public final static String AIRTIME_CREDIT = BASE_URL_SELF_CARE + "wsDoActionServiceAirTimeCredit";
    public final static String AIRTIME_DATA = BASE_URL_SELF_CARE + "wsDoActionServiceAirTimeData";
    public final static String AIRTIME_AMOUNT = BASE_URL_SELF_CARE + "wsGetInfoAirTime";

    public WSSCRestful(Context context) {
        super(context);
//        HttpsTrustManager.allowAllSSL(); //TODO - 210922
    }

    private int getSubType() {
        ApplicationController applicationController = ApplicationController.self();
        int subType = applicationController.getPref().getInt(SCConstants.PREFERENCE.SC_SUB_TYPE, 1);
        return subType;
    }

    private String getBaseUrl() {
        return UrlConfigHelper.getInstance(ApplicationController.self()).getDomainFile();
    }

    public void getSubList(Response.Listener<RestSCSubContentModel> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + getSubType() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_SUB);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("subType", getSubType());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCSubContentModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCSubContentModel.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_SUB);
    }

    public void getSub(Response.Listener<RestSCSubModel> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + getSubType() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_SUB);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("subType", getSubType());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCSubModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCSubModel.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_SUB);
    }

    public void getHomeBanner(Response.Listener<RestSCBanner> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + SCConstants.NUM_SIZE + "0" + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_HOME_BANNER);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("offset", "0");
        params.addParam("limit", SCConstants.NUM_SIZE);
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCBanner> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCBanner.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_HOME_BANNER);
    }

    public void getAccountInfo(Response.Listener<RestSCAccountData> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_ACCOUNT_INFO);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("subType", getSubType());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCAccountData> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCAccountData.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_ACCOUNT_INFO);
    }

    public void getPackageDetail(String packageData, Response.Listener<RestSCPackageDetail> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + packageData + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_PACKAGE_DETAIL);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("serviceCode", packageData);
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCPackageDetail> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCPackageDetail.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());

        addReq(req, GET_PACKAGE_DETAIL);
    }

    public void getRecommentPackage(Response.Listener<RestSCPackage> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_RECOMMENT_PACKAGE);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCPackage> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCPackage.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_RECOMMENT_PACKAGE);
    }

    public void getRecommentServices(Response.Listener<RestSCRecommentPackage> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_RECOMMENT_SERVICES);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCRecommentPackage> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCRecommentPackage.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_RECOMMENT_SERVICES);
    }

    public void getListPackage(String groupId, Response.Listener<RestSCPackage> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + groupId + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_LIST_PACKAGE);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("subId", (SCUtils.getCurrentAccount() == null) ? "" : SCUtils.getCurrentAccount().getId());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("serviceGroupId", groupId);
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCPackage> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCPackage.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_LIST_PACKAGE);
    }

    public void getRegisterService(Response.Listener<RestSCPackage> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_REGISTER_SERVICES);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCPackage> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCPackage.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_REGISTER_SERVICES);
    }

    public void getRegisterPackage(Response.Listener<RestSCPackage> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_REGISTER_PACKAGE);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCPackage> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCPackage.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_REGISTER_PACKAGE);
    }

    public void recharge(String phone, String serial, Response.Listener<String> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + phone + serial + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + RECHARGE);
        StringRequest req = new StringRequest(Request.Method.POST, params.toString(), response, error);
        req.setParams("msisdn", SCUtils.getPhoneNumber());
        req.setParams("serial", serial);
        req.setParams("desIsdn", phone);
        req.setParams("language", SCUtils.getLanguage());
        req.setParams("timestamp", timeStamp);
        req.setParams("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        req.setParams("revision", Config.REVISION);
        req.setParams("security", security);
        addReq(req, RECHARGE);
    }

    public void registerPackage(String serviceCode, int actionType, Response.Listener<String> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + actionType + serviceCode + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + REGISTER_PACKAGE);
        StringRequest req = new StringRequest(Request.Method.POST, params.toString(), response, error);
        req.setParams("msisdn", SCUtils.getPhoneNumber());
        req.setParams("serviceCode", serviceCode);
        req.setParams("actionType", actionType);
        req.setParams("language", SCUtils.getLanguage());
        req.setParams("timestamp", timeStamp);
        req.setParams("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        req.setParams("revision", Config.REVISION);
        req.setParams("security", security);

        addReq(req, RECHARGE);
    }

    public void getStores(String provinceId, String districtId, String latitude, String longitude, Response.Listener<RestSCStore> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + longitude + latitude + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_STORES);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("latitude", latitude);
        params.addParam("longitude", longitude);
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCStore> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCStore.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_STORES);
    }

    public void getPostageDetail(long from, long to, int postType, int offset, int limit, String sort, Response.Listener<RestSCPostageDetail> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + getSubType() + from + to + postType + offset + limit + sort + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + GET_POSTAGE_DETAIL);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("startDate", from);
        params.addParam("endDate", to);
        params.addParam("postType", postType);
        params.addParam("offset", offset);
        params.addParam("limit", limit);
        params.addParam("sort", sort);
        params.addParam("subType", getSubType());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestSCPostageDetail> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestSCPostageDetail.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, GET_POSTAGE_DETAIL);
    }

    public void transferIshare(String receiveIsdn, String amount, String pass, Response.Listener<String> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + receiveIsdn + pass + amount + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + TRANSFER_ISHARE);
        StringRequest req = new StringRequest(Request.Method.POST, params.toString(), response, error);
        req.setParams("msisdn", SCUtils.getPhoneNumber());
        req.setParams("receiveIsdn", receiveIsdn);
        req.setParams("amount", amount);
        req.setParams("password", pass);
        req.setParams("language", SCUtils.getLanguage());
        req.setParams("timestamp", timeStamp);
        req.setParams("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        req.setParams("revision", Config.REVISION);
        req.setParams("security", security);

        addReq(req, TRANSFER_ISHARE);
    }

    public void resetPasswordIShare(Response.Listener<String> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + ISHARE_CHANGE_PASS);
        StringRequest req = new StringRequest(Request.Method.POST, params.toString(), response, error);
        req.setParams("msisdn", SCUtils.getPhoneNumber());
        req.setParams("language", SCUtils.getLanguage());
        req.setParams("timestamp", timeStamp);
        req.setParams("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        req.setParams("revision", Config.REVISION);
        req.setParams("security", security);

        addReq(req, ISHARE_CHANGE_PASS);
    }

    public void airTimeCredit(String serviceCode, int actionType, Response.Listener<String> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + serviceCode + actionType + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + AIRTIME_CREDIT);
        StringRequest req = new StringRequest(Request.Method.POST, params.toString(), response, error);
        req.setParams(Constants.HTTP.REST_MSISDN, account.getJidNumber());
        req.setParams("serviceCode", serviceCode);
        req.setParams("actionType", actionType);
        req.setParams("language", SCUtils.getLanguage());
        req.setParams("timestamp", timeStamp);
        req.setParams("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        req.setParams("revision", Config.REVISION);
        req.setParams("security", security);
        addReq(req, AIRTIME_CREDIT);
    }

    public void airTimeData(String serviceCode, int actionType, Response.Listener<String> response, Response.ErrorListener error) {

        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        final ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        final String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + serviceCode + actionType + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + AIRTIME_DATA);
        StringRequest req = new StringRequest(Request.Method.POST, params.toString(), response, error);
        req.setParams(Constants.HTTP.REST_MSISDN, account.getJidNumber());
        req.setParams("serviceCode", serviceCode);
        req.setParams("actionType", actionType);
        req.setParams("language", SCUtils.getLanguage());
        req.setParams("timestamp", timeStamp);
        req.setParams("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        req.setParams("revision", Config.REVISION);
        req.setParams("security", security);

        addReq(req, AIRTIME_DATA);
    }

    public void getAirTimeAmount(int typeBalance, Response.Listener<RestAirTimeInfo> response, Response.ErrorListener error) {
        final String timeStamp = System.currentTimeMillis() + "";
        ApplicationController app = ApplicationController.self();
        ReengAccount account = app.getReengAccountBusiness().getCurrentAccount();
        String security = HttpHelper.encryptDataV2(app, account.getJidNumber() + SCUtils.getLanguage() + typeBalance + account.getToken() + timeStamp, account.getToken());

        ResfulString params = new ResfulString(getBaseUrl() + AIRTIME_AMOUNT);
        params.addParam("typebalance", typeBalance);
        params.addParam("msisdn", SCUtils.getPhoneNumber());
        params.addParam("language", SCUtils.getLanguage());
        params.addParam("timestamp", timeStamp);
        params.addParam("clientType", Constants.HTTP.CLIENT_TYPE_STRING);
        params.addParam("revision", Config.REVISION);
        params.addParam("security", security);
        GsonRequest<RestAirTimeInfo> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAirTimeInfo.class, null, response, error);
        req.setHeader("mocha-api", app.getReengAccountBusiness().getMochaApi());
        addReq(req, AIRTIME_AMOUNT);
    }
}

package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.AccountOcsValue;
import com.metfone.selfcare.module.metfoneplus.holder.AccountDetailViewHolder;

import java.util.List;

public class DetailAccountAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ONLY_ITEM = 0;
    private static final int TYPE_FIRST_ITEM = 1;
    private static final int TYPE_MIDDLE_ITEM = 2;
    private static final int TYPE_LAST_ITEM = 3;

    private Context mContext;
    private List<AccountOcsValue> mInfoList;

    public DetailAccountAdapter(Context context, List<AccountOcsValue> infoList) {
        this.mContext = context;
        this.mInfoList = infoList;
    }

    private void setList(List<AccountOcsValue> infoList) {
        this.mInfoList = infoList;
    }

    public void replaceData(List<AccountOcsValue> objectList) {
        setList(objectList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_account, parent, false);
        return new AccountDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AccountDetailViewHolder adHolder = (AccountDetailViewHolder) holder;
        if (holder.getItemViewType() == TYPE_ONLY_ITEM) {
            adHolder.mLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_item_detail_account_only));
        } else if (holder.getItemViewType() == TYPE_FIRST_ITEM) {
            adHolder.mLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_item_detail_account_first));
        } else if (holder.getItemViewType() == TYPE_MIDDLE_ITEM) {
            adHolder.mLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_item_detail_account_middle));
        } else {
            adHolder.mLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_item_detail_account_last));
        }

        AccountOcsValue account = mInfoList.get(position);
        adHolder.mTitle.setText(account.getTitle());

        // Uppercase first character
        String exp = account.getExp();
        String firstCharacterExp = exp.substring(0, 1);
        String otherCharacterExp = exp.substring(1);
        firstCharacterExp = firstCharacterExp.toUpperCase();
        exp = firstCharacterExp + otherCharacterExp;
        adHolder.mExp.setText(exp);

        String content = "";
        if (account.getValue().contains("$")) {
            content = account.getValue().replace("$", "").trim();
            adHolder.mValue.setText(mContext.getString(R.string.m_p_item_detail_account_value, content));
        } else if (account.getValue().contains("MB")) {
            content = account.getValue().replace(" ", "").trim();
            adHolder.mValue.setText(content);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mInfoList.size() == 1) {
            return TYPE_ONLY_ITEM;
        } else if (mInfoList.size() > 1) {
            if (position == 0) {
                return TYPE_FIRST_ITEM;
            } else if (position == mInfoList.size() - 1) {
                return TYPE_LAST_ITEM;
            } else {
                return TYPE_MIDDLE_ITEM;
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        if (mInfoList == null) {
            return 0;
        }
        return mInfoList.size();
    }
}

package com.metfone.selfcare.module.tiin.category.presenter;

import com.metfone.selfcare.module.tiin.base.MvpPresenter;

public interface ICategoryTiinMvpPresenter extends MvpPresenter {
    void getCategory(int id, int page, int num);

    void getListEvent(int id, int page, int num);

    void getMostView(int pid, int cid, int page, int num);

    void getHashTag(int page, String hashTag);
}

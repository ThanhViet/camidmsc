package com.metfone.selfcare.module.metfoneplus.topup.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopUpQrCodeResponse {


    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("object")
    @Expose
    private Object object;
    @SerializedName("userMsg")
    @Expose
    private String userMsg;
    @SerializedName("wsResponse")
    @Expose
    private WsResponse wsResponse;


    @Getter
    @Setter
    public class WsResponse {

        @SerializedName("captchaCode")
        @Expose
        private String captchaCode;
        @SerializedName("timeOut")
        @Expose
        private String timeOut;
    }
}

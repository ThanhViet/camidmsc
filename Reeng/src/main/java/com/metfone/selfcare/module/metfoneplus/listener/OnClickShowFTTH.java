package com.metfone.selfcare.module.metfoneplus.listener;

public interface OnClickShowFTTH {
    void showFTTH();
    void showMobile();
}

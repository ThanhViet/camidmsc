package com.metfone.selfcare.module.home_kh.api;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.tab.model.GiftSearchObject;
import com.metfone.selfcare.network.metfoneplus.BaseResponse;

import java.util.List;

public class WsGetGiftSearchResponse extends BaseResponse<WsGetGiftSearchResponse.Response> {
    public class Response {
        @SerializedName("object")
        public List<GiftSearchObject> object;
    }
}

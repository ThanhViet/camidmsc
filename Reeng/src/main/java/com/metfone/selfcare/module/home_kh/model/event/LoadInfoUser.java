package com.metfone.selfcare.module.home_kh.model.event;

import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;

public class LoadInfoUser {
    public LoadType loadType;
    public AccountRankDTO rankDTO;
    public UserInfo userInfo;

    public enum LoadType {
        NOT_LOGIN, LOADING, USER_INFO
    }

    public LoadInfoUser(LoadType loadType) {
        this.loadType = loadType;
    }

    public LoadInfoUser(AccountRankDTO rankDTO, UserInfo userInfo) {
        this.loadType = LoadType.USER_INFO;
        this.rankDTO = rankDTO;
        this.userInfo = userInfo;
    }
}

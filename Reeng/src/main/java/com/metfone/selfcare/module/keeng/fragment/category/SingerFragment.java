package com.metfone.selfcare.module.keeng.fragment.category;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class SingerFragment extends BaseFragment implements ViewPager.OnPageChangeListener, ChildSingerFragment.SingerInfoUpdateListener {

    private final static int TAB_MUSIC = 0;
    private final static int TAB_VIDEO = 1;
    private final static int TAB_ALBUM = 2;
    private final static int TAB_INFO = 3;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView mCover;
    private TextView tvTitleToolbar, tvCount, tvListenAll;
    private Topic currentTopic;
    private TabKeengActivity mActivity;
    private ViewPagerDetailAdapter adapter;
    private int currentTab = -1;
    private ProgressBar progressBar;
    private View btnBack;

    public static SingerFragment newInstance(Bundle args) {
        SingerFragment mInstance = new SingerFragment();
        mInstance.setArguments(args);
        return mInstance;
    }

    @Override
    public String getName() {
        return "SingerFragment";
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TabKeengActivity) activity;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_singer;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        progressBar = view.findViewById(R.id.progress_bar);
        viewPager = view.findViewById(R.id.viewpager);
        tabLayout = view.findViewById(R.id.tabs);
        mCover = view.findViewById(R.id.imvCover);
        tvCount = view.findViewById(R.id.tv_count);
        tvTitleToolbar = view.findViewById(R.id.tvTitleToolbar);
        tvListenAll = view.findViewById(R.id.tv_listen_all);
        btnBack = view.findViewById(R.id.iv_back);
        return view;
    }

    private void setupCountInfo() {
        if (!isAdded()) return;
        long size;
        switch (currentTab) {
            case TAB_MUSIC:
                size = currentTopic.getTotalSongs();
                if (size < 1) {
                    tvCount.setVisibility(View.GONE);
                    return;
                }
                tvListenAll.setVisibility(View.VISIBLE);
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.music_total_song, size));
                else
                    tvCount.setText(getString(R.string.music_total_songs, size));
                break;
            case TAB_VIDEO:
                tvListenAll.setVisibility(View.GONE);
                size = currentTopic.getTotalVideos();
                if (size < 1) {
                    tvCount.setVisibility(View.GONE);
                    return;
                }
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.total_mv, size));
                else
                    tvCount.setText(getString(R.string.total_mvs, size));
                break;
            case TAB_ALBUM:
                tvListenAll.setVisibility(View.GONE);
                size = currentTopic.getTotalAlbums();
                if (size < 1) {
                    tvCount.setVisibility(View.GONE);
                    return;
                }
                tvCount.setVisibility(View.VISIBLE);
                if (size == 1)
                    tvCount.setText(getString(R.string.music_total_album, size));
                else
                    tvCount.setText(getString(R.string.music_total_albums, size));
                break;
            case TAB_INFO:
                tvListenAll.setVisibility(View.GONE);
                tvCount.setVisibility(View.GONE);
                break;
        }
    }

    private void initViewPager() {
        try {
            progressBar.setVisibility(View.GONE);
            setupViewPager(viewPager);
            setupCountInfo();
            if (currentTopic != null) {
                ImageBusiness.setCover(currentTopic.getCover(), mCover, (int) currentTopic.getSingerId());
                tvTitleToolbar.setText(currentTopic.getName());
            }

            if (tvListenAll != null) {
                tvListenAll.setOnClickListener(view -> {
                    if (currentTab == 0 && adapter != null && mActivity != null) {
                        try {
                            Fragment fragment = adapter.getItem(currentTab);
                            if (fragment instanceof ChildSingerFragment) {
                                List<AllModel> list = ((ChildSingerFragment) fragment).getDatas();
                                if (!list.isEmpty()) {
                                    PlayingList playingList = new PlayingList(list, PlayingList.TYPE_FULL_DATA, MediaLogModel.SRC_SINGER);
                                    mActivity.setMediaPlayingAudioWithState(playingList, 0, Constants.PLAY_MUSIC.REPEAT_All, Constants.PLAY_MUSIC.REPEAT_SUFF_OFF);
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, e);
                        }
                    }
                });
            }
        } catch (IllegalStateException e) {
            Log.e(TAG, e);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    private void loadInfo() {
        progressBar.setVisibility(View.VISIBLE);
        new KeengApi().getInfoSinger(currentTopic.getSingerId(), response -> {
            currentTopic = response.getData();
            if (currentTopic == null) {
                onBackPressed();
            } else {
                initViewPager();
            }
        }, error -> {
            Log.e(TAG, error);
            initViewPager();
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            if (getArguments() != null) {
                currentTopic = (Topic) getArguments().getSerializable(Constants.KEY_DATA);
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
        if (currentTopic == null) {
            onBackPressed();
            return;
        }
        btnBack.setOnClickListener(view -> onBackPressed());
        new Handler().postDelayed(this::loadInfo, 250);
    }

    @Override
    public void onResume() {
        super.onResume();
        setCurrentItemViewPage(currentTab);
    }

    public void setCurrentItemViewPage(int index) {
        if (index >= 0) {
            try {
                viewPager.setCurrentItem(index);
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        int count = 0;
        if (currentTopic != null) {
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable(Constants.KEY_DATA, currentTopic);
            bundle1.putInt("type", Constants.TYPE_SONG);
            adapter.addFragment(ChildSingerFragment.instances(bundle1, this), getString(R.string.song));
            count++;
            Bundle bundle2 = new Bundle();
            bundle2.putSerializable(Constants.KEY_DATA, currentTopic);
            bundle2.putInt("type", Constants.TYPE_VIDEO);
            adapter.addFragment(ChildSingerFragment.instances(bundle2, this), getString(R.string.video_mv));
            count++;
            Bundle bundle3 = new Bundle();
            bundle3.putSerializable(Constants.KEY_DATA, currentTopic);
            bundle3.putInt("type", Constants.TYPE_ALBUM);
            adapter.addFragment(ChildSingerFragment.instances(bundle3, this), getString(R.string.album_keeng));
            if (!TextUtils.isEmpty(currentTopic.getSingerInfo())) {
                Bundle bundle4 = new Bundle();
                bundle4.putSerializable(Constants.KEY_DATA, currentTopic);
                adapter.addFragment(ChildSingerInfoFragment.instances(bundle3), getString(R.string.channel_detail_tab_info));
            }
            count++;
        }
        if (count > 1) {
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setOffscreenPageLimit(count - 1);
        } else {
            tabLayout.setVisibility(View.GONE);
        }
        if (count == 0) {
            adapter.addFragment(EmptyFragment.newInstance(), "");
        }
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        currentTab = position;
        setupCountInfo();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onDetach() {
        if (viewPager != null)
            viewPager.setAdapter(null);
        adapter = null;
        App.getInstance().cancelPendingRequests(KeengApi.GET_INFO_SINGER);
        super.onDetach();
    }

    @Override
    public void onInfoUpdate(Topic singerInfo) {
        currentTopic = singerInfo;
        setupCountInfo();
    }
}

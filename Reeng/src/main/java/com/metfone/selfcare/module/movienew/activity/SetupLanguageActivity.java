package com.metfone.selfcare.module.movienew.activity;

import android.content.Intent;
import android.os.Bundle;

import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.common.utils.DynamicSharePref;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.onboarding.KhOnBoardingActivity;
import com.metfone.selfcare.module.home_kh.service.LoadCinemaService;
import com.metfone.selfcare.module.movie.fragment.MoviePagerFragment;
import com.metfone.selfcare.module.movienew.fragment.SetupLanguageFragment;
import com.metfone.selfcare.util.Utilities;

public class SetupLanguageActivity extends BaseSlidingFragmentActivity implements SetupLanguageFragment.SetupLanguageListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set SystemUI & hide navigation
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);
        setContentView(R.layout.activity_setup_language);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,SetupLanguageFragment.newInstance()).commitAllowingStateLoss();
    }

    @Override
    public void selectLanguage() {
        DynamicSharePref.getInstance().put(Constants.KEY_IS_FIRST_OPEN_APP, true);
        DynamicSharePref.getInstance().put(Constants.KEY_IS_FIRST_TAB_HOME, true);
        LoadCinemaService.start(this);
        Intent intent = new Intent(SetupLanguageActivity.this, KhOnBoardingActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
    }
}

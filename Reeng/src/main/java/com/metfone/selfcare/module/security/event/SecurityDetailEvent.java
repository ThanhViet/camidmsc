/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/28
 */

package com.metfone.selfcare.module.security.event;

public class SecurityDetailEvent {
    private boolean selectAll;
    private boolean deSelectAll;
    private boolean cancelSelect;
    private boolean reloadData;
    private int tabId;

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
    }

    public boolean isCancelSelect() {
        return cancelSelect;
    }

    public void setCancelSelect(boolean cancelSelect) {
        this.cancelSelect = cancelSelect;
    }

    public boolean isReloadData() {
        return reloadData;
    }

    public void setReloadData(boolean reloadData) {
        this.reloadData = reloadData;
    }

    public boolean isDeSelectAll() {
        return deSelectAll;
    }

    public void setDeSelectAll(boolean deSelectAll) {
        this.deSelectAll = deSelectAll;
    }

    public int getTabId() {
        return tabId;
    }

    public void setTabId(int tabId) {
        this.tabId = tabId;
    }
}

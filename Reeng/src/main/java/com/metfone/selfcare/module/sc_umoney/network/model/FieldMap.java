package com.metfone.selfcare.module.sc_umoney.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FieldMap implements Serializable {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("fieldName")
    @Expose
    private String fieldName;
    @SerializedName("fieldID")
    @Expose
    private int fieldID;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getFieldID() {
        return fieldID;
    }

    public void setFieldID(int fieldID) {
        this.fieldID = fieldID;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}
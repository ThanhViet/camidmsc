package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;

/**
 * Created by HaiKE on 7/6/17.
 */

public class HeaderTitleHolder extends BaseHolder {
    public TextView tvTitle;

    public HeaderTitleHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
    }
}

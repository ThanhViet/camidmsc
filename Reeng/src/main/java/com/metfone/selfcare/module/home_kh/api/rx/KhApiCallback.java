package com.metfone.selfcare.module.home_kh.api.rx;

import android.util.Log;

import androidx.annotation.NonNull;

import com.metfone.selfcare.app.dev.ApplicationController;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class KhApiCallback<T> implements Callback<KHBaseResponse<T>> {
    private final String TAG = "KhApiCallback";
    private final int SUCCESS_CODE = 200;

    @Override
    public void onResponse(@NonNull Call<KHBaseResponse<T>> call,
                           @NonNull Response<KHBaseResponse<T>> response) {
        if (response.code() != SUCCESS_CODE) {
            showResponseError(response.code());
            onFailed(String.valueOf(response.code()), null);
            return;
        }
        KHBaseResponse<T> body = response.body();
        if (body != null && !body.isSuccessful()) {
            showResponseError(response.code());
            onFailed(body.getErrorCode(), String.format(Locale.getDefault(),
                    "Đã xảy ra lỗi %s",
                    body.getErrorCode()));
            return;
        }
        onSuccess(body.getData());
    }

    public abstract void onSuccess(T body);

    public abstract void onFailed(String status, String message);

    private void showResponseError(int code) {
        if (code == 401) {
            if (ApplicationController.self().getCurrentActivity() != null) {
                ApplicationController.self().getCurrentActivity().restartApp();
            }
            Log.e(TAG, "onResponse - unauthenticated: " + code);
        } else if (code >= 400 && code < 500) {
            Log.e(TAG, "onResponse - clientError:" + code);
        } else if (code >= 500 && code < 600) {
            Log.e(TAG, "onResponse - serverError:" + code);
        } else {
            Log.e(TAG, "onResponse - RuntimeException - unexpectedError:" + code);
        }
    }
}

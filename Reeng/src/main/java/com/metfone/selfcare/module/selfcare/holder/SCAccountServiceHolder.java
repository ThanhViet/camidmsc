package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.adapter.SCAccountDetailAdapter;
import com.metfone.selfcare.module.selfcare.adapter.SCAccountServiceAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCPackage;

import java.util.ArrayList;

public class SCAccountServiceHolder extends BaseViewHolder {
    private TextView tvTitle;
    private RecyclerView recyclerView;
    private SCAccountServiceAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<SCPackage> data;
    private Context context;
    private AbsInterface.OnPackageListener listener;
    private int type;

    public SCAccountServiceHolder(Context context, View view, AbsInterface.OnPackageListener listener, int type) {
        super(view);
        this.context = context;
        this.listener = listener;
        this.type = type;

        tvTitle = view.findViewById(R.id.tvTitle);
        recyclerView = view.findViewById(R.id.recycler_view);

        adapter = new SCAccountServiceAdapter(context, listener);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);
    }

    public void setData(ArrayList<SCPackage> data) {
        if(type == SCAccountDetailAdapter.TYPE_PACKAGE)
        {
            tvTitle.setText(context.getString(R.string.sc_package_subscribed));
        }
        else
        {
            tvTitle.setText(context.getString(R.string.sc_services_subscribed));
        }

        this.data = data;
        adapter.setItemsList(data);
        adapter.notifyDataSetChanged();
    }
}
package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WsCheckForceUpdateAppResponse {
    @SerializedName("errorCode")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("userMsg")
    @Expose
    String userMsg;

    @SerializedName("wsResponse")
    @Expose
    WsResponse wsResponse;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserMsg() {
        return userMsg;
    }

    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    public WsResponse getWsResponse() {
        return wsResponse;
    }

    public void setWsResponse(WsResponse wsRequest) {
        this.wsResponse = wsRequest;
    }

    public class WsResponse {
        @SerializedName("forceUpgrade")
        boolean forceUpgrade;
        @SerializedName("recommendUpgrade")
        String recommendUpgrade;
        @SerializedName("updateInfo")
        String updateInfo;
        @SerializedName("currVersion")
        String currVersion;
        @SerializedName("newVersion")
        String newVersion;

        public boolean getForceUpgrade() {
            return forceUpgrade;
        }

        public void setForceUpgrade(boolean forceUpgrade) {
            this.forceUpgrade = forceUpgrade;
        }

        public String getRecommendUpgrade() {
            return recommendUpgrade;
        }

        public void setRecommendUpgrade(String recommendUpgrade) {
            this.recommendUpgrade = recommendUpgrade;
        }

        public String getUpdateInfo() {
            return updateInfo;
        }

        public void setUpdateInfo(String updateInfo) {
            this.updateInfo = updateInfo;
        }

        public String getCurrVersion() {
            return currVersion;
        }

        public void setCurrVersion(String currVersion) {
            this.currVersion = currVersion;
        }

        public String getNewVersion() {
            return newVersion;
        }

        public void setNewVersion(String newVersion) {
            this.newVersion = newVersion;
        }

    }
}

package com.metfone.selfcare.module.tiin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.module.newdetails.utils.AppStateProvider;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.view.TextViewWithImages;
import com.metfone.selfcare.module.tiin.network.model.Category;
import com.metfone.selfcare.module.tiin.network.model.CategoryTiinResponse;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TiinUtilities {
    private static final String TAG = TiinUtilities.class.toString();

    @SuppressLint("SimpleDateFormat")
    public static String calculateDate(long timeStr) {
//        Log.e(TAG, "calculateDate: " + timeStr);
        long timeLong;
        try {
            timeLong = timeStr * 1000;
        } catch (Exception e) {
            timeLong = System.currentTimeMillis();
        }
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy | HH:mm");
        return sdfDate.format(new Date(timeLong));
    }

    public static TiinModel parserLinkURLNews(String url) {
        TiinModel item = new TiinModel();
        int type = 0;
        int postId = 0;
        try {
            Pattern regexPattern = Pattern.compile(".+[\\?&]type=(?<type>[\\d]+)&id=(?<id>[\\d]+)",
                    Pattern.DOTALL | Pattern.MULTILINE);
            Matcher matcher = regexPattern.matcher(url);
            if (matcher.find()) {
                type = Integer.parseInt(matcher.group(1));
                postId = Integer.parseInt(matcher.group(2));
            }
            // IF_ELSE - SWITCH Type để mở Activity hoặc Fragment
            switch (type) {
                case 1:
                    item.setId(postId);
                    item.setType(type);
                    break;
                case 2:
                    item.setId(postId);
                    item.setType(type);
                    break;
                case 3:
                    item.setId(postId);
                    item.setType(type);
                    break;
                case 4:
                    item.setId(postId);
                    item.setType(type);
                    break;
                case 5:
                    item.setId(postId);
                    item.setType(type);
                    break;
                case 6:
                    item.setId(postId);
                    item.setType(type);
                    break;
                default:
                    item.setId(0);
                    break;
            }
            // Type = 1 : Bài viết thường

            // Type = 2 : Bài viết dạng ảnh (Slider)

            // Type = 3 : Bài chi tiết video

            // Type = 4 : Bài chi tiết Multimedia

            // Type = 5 : Danh sách bài viết theo chuyên đề (http://tiin.vn/chuyen-muc/phim/chuyen-de/hau-due-mat-troi-ban-viet.html)

            // Type = 6 : Danh sách hồ sơ Sao (http://tiin.vn/chuyen-muc/sao/tin-sao/SelenaGomez.html)
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("LinkText", ex.toString());
        }
        return item;
    }

    public static ArrayList<Category> getListCategoryTiin(Context context) {
        try {
            if (context == null) {
                return null;
            }
            String json = AssetJSONFile(context);
            if (!TextUtils.isEmpty(json)) {
                Gson gson = new Gson();
                CategoryTiinResponse data = gson.fromJson(json, CategoryTiinResponse.class);
                return data.getData();
            }
        } catch (Exception e) {
            Log.e(TAG, "getListCategoryTiin", e);
        }
        return null;
    }

    private static String AssetJSONFile(Context context) throws IOException {
        if (context.getAssets() != null) {
            AssetManager manager = context.getAssets();
            InputStream file = manager.open("data/CategoryTiin.json");
            byte[] formArray = new byte[file.available()];
            file.read(formArray);
            file.close();
            return new String(formArray);
        }
        return "";
    }

    public static boolean checkTrung(ArrayList<TiinModel> temp, TiinModel item) {
        System.out.println("checkTrung---------------------------------------------------------------------------" + item.getId());

        for (int i = 0; i < temp.size(); i++) {
            TiinModel model = temp.get(i);

            System.out.println("checkTrung-------------" + model.getId() + "---" + item.getId() + "---------------" + (model.getId() == item.getId()));

            if (model.getId() == item.getId()) {
                return true;
            }
        }

        System.out.println("checkTrung---------------------------------------------------------------------------");
        System.out.println("");

        return false;
    }

    public static void setText(View view, String str, int typeIcon) {
        if (view == null) return;
        try {
            if (view instanceof TextViewWithImages) {
                TextViewWithImages textView = (TextViewWithImages) view;
                if (typeIcon == 1)//Anh
                {
                    textView.setHaveIcon(true);
                    textView.setText(str + " [img src=ic_type_image/]");
                } else if (typeIcon == 2)//Video
                {
                    textView.setHaveIcon(true);
                    textView.setText(str +" [img src=ic_v5_new_video/]");
                } else if (typeIcon == 4)//Live
                {
                    textView.setHaveIcon(true);
                    textView.setText(str + " [img src=ic_live/]");
                } else {
                    textView.setHaveIcon(false);
                    textView.setText(str);
                }
                textView.setSelected(true);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception", ex);

            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setSelected(true);
                textView.setText(str);
            }
        }
    }

    public static void setTextQuote(View view, String str) {
        if (view == null) return;
        try {
            if (view instanceof TextViewWithImages) {
                TextViewWithImages textView = (TextViewWithImages) view;
                textView.setHaveIcon(true);
                textView.setText("[img src=ic_v5_open_new/] " + str + " [img src=ic_v5_close_new/]");
                textView.setSelected(true);
            }
        } catch (Exception ex) {
//            Log.e(TAG, "Exception", ex);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                textView.setSelected(true);
                textView.setText(str);
            }
        }
    }

    public static void logClickTiin(TiinModel newsModel) {
        new WSOnMedia(ApplicationController.self()).logClickLink(newsModel.getUrl(), Constants.LOG_SOURCE_TYPE.TYPE_TIIN, AppStateProvider.getInstance().getCampId(),new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i(TAG, "callApiLogView response: " + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e(TAG, "VolleyError", volleyError);
            }
        });
    }

    public static TiinModel parserIdfromLink(String url) {
        TiinModel item = new TiinModel();
        int id;
        try {
            Pattern regex = Pattern.compile("http.*tiin.vn.*.html\\?id=(\\d+)");
            Matcher regexMatcher = regex.matcher(url);
            if (regexMatcher.find()) {
                id = Integer.parseInt(regexMatcher.group(1));
                item.setId(id);
            } else {
                item.setId(0);
            }
        } catch (Exception ex) {
            // Syntax error in the regular expression
        }
        return item;
    }

    public static void setWidthHeight(Activity activity, ViewGroup viewGroup) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        ViewGroup.LayoutParams params = viewGroup.getLayoutParams();
        params.width = width;
        params.height = 9 * width / 16;
        viewGroup.setLayoutParams(params);
    }

    public static void setWidthHeightImage(Activity activity, ImageView viewGroup) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        ViewGroup.LayoutParams params = viewGroup.getLayoutParams();
        params.width = width;
        params.height = 9 * width / 16;
        viewGroup.setLayoutParams(params);
    }

    public static void setVisibilityView(View view, boolean flag) {
        if (view != null) {
            if (flag) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    public static void showDialogFail(final BaseSlidingFragmentActivity activity) {
        if (activity == null) {
            return;
        }
        final DialogMessage dialogMessage = new DialogMessage(activity, true);
        dialogMessage.setMessage("Nội dung này không tồn tại!");
        dialogMessage.setGravity(5);
        dialogMessage.setLabelButton("OK");
        dialogMessage.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                activity.onBackPressed();
                dialogMessage.dismiss();
            }
        });
        dialogMessage.setCancelable(false);
        dialogMessage.setCanceledOnTouchOutside(false);
        dialogMessage.show();
    }

    public static void showDialogSource(final BaseSlidingFragmentActivity activity, final String source) {
        DialogConfirm dialogConfirm = new DialogConfirm(activity, false);
        dialogConfirm.setLabel("Link bài gốc");
        dialogConfirm.setMessage(source);
        dialogConfirm.setPositiveLabel("Sao chép link");
        dialogConfirm.setNegativeLabel("Hủy");
        dialogConfirm.setNegativeListener(new NegativeListener() {
            @Override
            public void onNegative(Object result) {
                dialogConfirm.dismiss();
            }
        });
        dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
            @Override
            public void onPositive(Object result) {
                TextHelper.copyToClipboard(activity, source);
                dialogConfirm.dismiss();
            }
        });
        dialogConfirm.show();
    }

    public static boolean checkDate(Context context) {
        long prevTime = SharedPref.newInstance(context).getLong("TIME_CATEGORY", 0l);
        long currentTime = System.currentTimeMillis();

        Calendar prevCalendar = Calendar.getInstance();
        prevCalendar.setTimeInMillis(prevTime);

        int prevDate = prevCalendar.get(Calendar.DAY_OF_YEAR);
        int prevYear = prevCalendar.get(Calendar.YEAR);

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTimeInMillis(currentTime);

        int currentDate = currentCalendar.get(Calendar.DAY_OF_YEAR);
        int currentYear = currentCalendar.get(Calendar.YEAR);

        if (currentYear > prevYear) {
            return true;
        }

        if (currentYear < prevYear) {
            return false;
        }

        if (currentDate > prevDate) {
            return true;
        }
        return false;
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/2
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AccountInfo implements Serializable {
    private static final long serialVersionUID = 211427555766407282L;

    private String desc;
    @SerializedName("charged")
    private String charged;
    @SerializedName("info")
    private ArrayList<DataInfo> data;
    private boolean isViettel;
    private String username;
    private String phoneNumber;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCharged() {
        return charged;
    }

    public void setCharged(String charged) {
        this.charged = charged;
    }

    public ArrayList<DataInfo> getData() {
        return data;
    }

    public void setData(ArrayList<DataInfo> data) {
        this.data = data;
    }

    public boolean isViettel() {
        return isViettel;
    }

    public void setViettel(boolean viettel) {
        isViettel = viettel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "{" +
                "desc='" + desc + '\'' +
                ", charged='" + charged + '\'' +
                ", data=" + data +
                '}';
    }

}

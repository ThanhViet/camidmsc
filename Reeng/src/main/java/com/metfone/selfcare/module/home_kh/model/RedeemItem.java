package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.tab.DateConvert;

import java.io.Serializable;
import java.util.List;

public class RedeemItem implements Serializable {
    @SerializedName("isdn")
    long isdn;
    @SerializedName("giftName")
    String giftName;
    @SerializedName("redeemPoint")
    int redeemPoint;
    @SerializedName("exchangDate")
    String exchangDate;
    @SerializedName("discountRate")
    String discountRate;
    @SerializedName("expireDate")
    String expireDate;
    @SerializedName("imageList")
    List<String> imageList;
    @SerializedName("exchangeCode")
    String exchangeCode;
    @SerializedName("giftQrCode")
    String giftQrCode;
    @SerializedName("giftId")
    String giftId;
    @SerializedName("giftPoint")
    int giftPoint;
    @SerializedName("description")
    String description;
    @SerializedName("quantity")
    int quantity;
    @SerializedName("isHot")
    int isHot;
    @SerializedName("isRecommentGift")
    int isRecommentGift;
    @SerializedName("exchangeGiftHistoryId")
    String exchangeGiftHistoryId;
    @SerializedName("iconUrl")
    String iconUrl;

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public long getIsdn() {
        return isdn;
    }

    public void setIsdn(long isdn) {
        this.isdn = isdn;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getRedeemPoint() {
        return redeemPoint;
    }

    public void setRedeemPoint(int redeemPoint) {
        this.redeemPoint = redeemPoint;
    }

    public String getExchangDate() {
        return exchangDate;
    }

    public void setExchangDate(String exchangDate) {
        this.exchangDate = exchangDate;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public String getGiftQrCode() {
        return giftQrCode;
    }

    public void setGiftQrCode(String giftQrCode) {
        this.giftQrCode = giftQrCode;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public int getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(int giftPoint) {
        this.giftPoint = giftPoint;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public int getIsRecommentGift() {
        return isRecommentGift;
    }

    public void setIsRecommentGift(int isRecommentGift) {
        this.isRecommentGift = isRecommentGift;
    }

    public String getExchangeGiftHistoryId() {
        return exchangeGiftHistoryId;
    }

    public void setExchangeGiftHistoryId(String exchangeGiftHistoryId) {
        this.exchangeGiftHistoryId = exchangeGiftHistoryId;
    }

    public long getExpireDateLong() {
        return DateConvert.convertStringToLong(expireDate, DateConvert.DATE_GIFT_INPUT);
    }
}

package com.metfone.selfcare.module.gameLive.socket.stomp.pathmatcher;

import com.metfone.selfcare.module.gameLive.socket.stomp.dto.StompHeader;
import com.metfone.selfcare.module.gameLive.socket.stomp.dto.StompMessage;

public class SimplePathMatcher implements PathMatcher {

    @Override
    public boolean matches(String path, StompMessage msg) {
        String dest = msg.findHeader(StompHeader.DESTINATION);
        if (dest == null) return false;
        else return path.equals(dest);
    }
}

package com.metfone.selfcare.module.keeng.adapter.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.interfaces.AbsInterface;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;

import java.util.List;

public class AlbumAdapter extends SectionListDataAdapter<AllModel> {

    public AlbumAdapter(Context context, List<AllModel> itemsList, AbsInterface.OnItemListener onClick) {
        super(context, itemsList, onClick);
        setCanViewAll(true);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_child_album_home, null);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final AllModel item = getItem(position);
        if (item != null) {
            holder.setVisible(R.id.image, true);
            holder.setVisible(R.id.tvTitle, true);
            holder.setVisible(R.id.tvContent, true);
            holder.setVisible(R.id.layout_view_all, false);
            holder.setText(R.id.tvTitle, item.getName());
            holder.setText(R.id.tvContent, item.getSinger());
            View view = holder.getView(R.id.image);
            if (view instanceof ImageView)
                ImageBusiness.setAlbum(item.getImage(), ((ImageView) view), position);

            holder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClick != null)
                        onClick.onItemClick(item);
                }
            });
        } else if (isCanViewAll()) {
            holder.setVisible(R.id.image, false);
            holder.setVisible(R.id.tvTitle, false);
            holder.setVisible(R.id.tvContent, false);
            holder.setVisible(R.id.layout_view_all, true);
            holder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClick != null)
                        onClick.onHeaderClick(view, getTag());
                }
            });
        }
    }
}
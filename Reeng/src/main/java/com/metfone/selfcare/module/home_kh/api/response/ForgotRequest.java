package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotRequest {
    @SerializedName("requestId")
    @Expose
    String requestId;

    public String getRequestId() {
        if (requestId == null) return "";
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}


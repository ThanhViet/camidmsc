package com.metfone.selfcare.module.metfoneplus.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.metfone.selfcare.R;


public class NumberPickerBottomSheetDialogFragment extends MPBaseBottomSheetDialogFragment {
    private static final String TAG = NumberPickerBottomSheetDialogFragment.class.getSimpleName();
    private final static String PARAM_TITLE = "param_title";
    private final static String PARAM_ARRAY = "param_array";
    private final static String PARAM_SELECTION_POSITION = "param_selection_position";
    private AppCompatTextView mTitle;
    private NumberPicker mNumberPhonePicker;
    private String[] mValueArr;
    private OnNumberPickerBottomSheetOnClick mOnNumberPickerBottomSheetOnClick;
    private String mValueSelected;
    private int mPositionSelected;

    public NumberPickerBottomSheetDialogFragment() {
        this.mValueArr = new String[]{};
        this.mValueSelected = null;
        this.mPositionSelected = -1;
    }

    public static NumberPickerBottomSheetDialogFragment newInstance(String title, String[] valueArr, int position) {
        NumberPickerBottomSheetDialogFragment number = new NumberPickerBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_TITLE, title);
        bundle.putStringArray(PARAM_ARRAY, valueArr);
        bundle.putInt(PARAM_SELECTION_POSITION, position);
        number.setArguments(bundle);
        return number;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.bottom_sheet_selection;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTitle = view.findViewById(R.id.txt_title);
        mNumberPhonePicker = view.findViewById(R.id.number_picker_phone);

        if (getArguments() != null) {
            mValueArr = getArguments().getStringArray(PARAM_ARRAY);
            //mValueArr =getResources().getStringArray(R.array.add_feedback_metfone);
            mPositionSelected = getArguments().getInt(PARAM_SELECTION_POSITION);
            mTitle.setText(getArguments().getString(PARAM_TITLE, ""));
            setupBottomSheet();
        }

        view.findViewById(R.id.txt_done).setOnClickListener((v) -> {
            if (mOnNumberPickerBottomSheetOnClick != null) {
                if (mValueSelected != null) {
                    mOnNumberPickerBottomSheetOnClick.onDone(mValueSelected, mPositionSelected);
                } else {
                    mOnNumberPickerBottomSheetOnClick.onDone("All", mPositionSelected);
                }

                mOnNumberPickerBottomSheetOnClick.onDismiss();
            }
            this.dismiss();
        });
    }

    private void setupBottomSheet() {
//        mValueArr = mObjectHashMap.keySet().toArray(new String[0]);
        mNumberPhonePicker.setMinValue(0);
        mNumberPhonePicker.setMaxValue(mValueArr.length - 1);
        mNumberPhonePicker.setValue(mPositionSelected);
        mNumberPhonePicker.setDisplayedValues(mValueArr);

        mValueSelected = mValueArr[mPositionSelected];

        //disable soft keyboard
        mNumberPhonePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        //set wrap true or false, try it you will know the difference
        mNumberPhonePicker.setWrapSelectorWheel(false);

        mNumberPhonePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mValueSelected = mValueArr[i1];
                mPositionSelected = i1;
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mOnNumberPickerBottomSheetOnClick != null) {
            mOnNumberPickerBottomSheetOnClick.onDismiss();
        }
        super.onDestroyView();
    }

    public void setOnNumberPickerBottomSheetOnClick(OnNumberPickerBottomSheetOnClick onClick) {
        this.mOnNumberPickerBottomSheetOnClick = onClick;
    }

    public interface OnNumberPickerBottomSheetOnClick {
        void onDone(String valueSelected, int position);
        default void onDismiss() {};
    }
}

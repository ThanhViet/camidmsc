/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/8/15
 *
 */

package com.metfone.selfcare.module.share.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ReengMessageConstant;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.share.holder.ChooseContactShareContentHolder;
import com.metfone.selfcare.module.share.holder.ChooseNonContactShareContentHolder;
import com.metfone.selfcare.module.share.listener.ShareContentListener;

public class ShareContentAdapter extends BaseAdapter<BaseAdapter.ViewHolder, ContactProvisional> {
    private static final int TYPE_THREAD_CHAT = 1;
    private static final int TYPE_CONTACT = 2;
    private static final int TYPE_TITLE_CONTACT = 3;
    private static final int TYPE_CREATE_THREAD = 4;
    private ShareContentListener listener;
    private boolean isLandscape;
    private int typeSharing = 0;
    private ReengMessageConstant.MessageType messageType;
    public ShareContentAdapter(Activity activity, boolean isLandscape, ReengMessageConstant.MessageType messageType) {
        super(activity);
        this.isLandscape = isLandscape;
        this.messageType = messageType;
    }
    public ShareContentAdapter(Activity activity, boolean isLandscape, ReengMessageConstant.MessageType messageType, int typeSharing) {
        super(activity);
        this.isLandscape = isLandscape;
        this.messageType = messageType;
        this.typeSharing = typeSharing;
    }


    public void setListener(ShareContentListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        ContactProvisional item = getItem(position);
        if (item != null) {
            if (item.getContact() instanceof ThreadMessage)
                return TYPE_THREAD_CHAT;
            else if (item.getContact() instanceof PhoneNumber)
                return TYPE_CONTACT;
            else if (item.getContact() instanceof String)
                return TYPE_CREATE_THREAD;
            else if (item.getContact() == null)
                return TYPE_TITLE_CONTACT;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_THREAD_CHAT:
            case TYPE_CONTACT:
//                return new ChooseContactShareContentHolder(layoutInflater.inflate(isLandscape ? R.layout.holder_choose_contact_share_content_land
//                        : R.layout.holder_choose_contact_share_content, parent, false), activity, listener);
                if (typeSharing == 2) {
                    return new ChooseContactShareContentHolder(layoutInflater.inflate(R.layout.holder_choose_contact_share_content, parent, false), activity, listener, typeSharing);
                } else {
                    return new ChooseContactShareContentHolder(layoutInflater.inflate(R.layout.holder_choose_contact_share_content, parent, false), activity, listener, messageType);
                }

            case TYPE_CREATE_THREAD:
//                return new ChooseNonContactShareContentHolder(layoutInflater.inflate(isLandscape ? R.layout.holder_choose_non_contact_share_content_land
//                        : R.layout.holder_choose_non_contact_share_content, parent, false), activity, listener);
                return new ChooseNonContactShareContentHolder(layoutInflater.inflate(R.layout.holder_choose_non_contact_share_content, parent, false), activity, listener);
            case TYPE_TITLE_CONTACT:
//                return new ViewHolder(layoutInflater.inflate(isLandscape ? R.layout.holder_title_contact_share_content_land
//                        : R.layout.holder_title_contact_share_content, parent, false));

                if(messageType == ReengMessageConstant.MessageType.text || messageType == ReengMessageConstant.MessageType.image){
                    return new ViewHolder(layoutInflater.inflate(R.layout.holder_title_forward_content, parent, false));
                } else {
                    return new ViewHolder(layoutInflater.inflate(R.layout.holder_title_contact_share_content, parent, false));
                }
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof ChooseContactShareContentHolder) {
            holder.bindData(getItem(position), position);
        } else if (holder instanceof ChooseNonContactShareContentHolder) {
            holder.bindData(getItem(position), position);
        }
    }

}

package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetRankDefineInfoRequest extends BaseRequest<WsGetRankDefineInfoRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("custId")
        String custId;

        public WsRequest(String isdn, String custId) {
            this.isdn = isdn;
            this.custId = custId;
        }
    }
}

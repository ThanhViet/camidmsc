package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;

import java.util.List;

public class AccountPointRankResponse {
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("listAccountPoint")
    @Expose
    List<KHAccountPoint> listAccountPoint;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<KHAccountPoint> getListPoint() {
        return listAccountPoint;
    }

    public void setListRank(List<KHAccountPoint> listRank) {
        this.listAccountPoint = listRank;
    }
}

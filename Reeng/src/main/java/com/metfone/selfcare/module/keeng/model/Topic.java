/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.metfone.selfcare.module.keeng.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Topic implements Serializable {

    private static final long serialVersionUID = 387041757059728799L;

    @SerializedName("id")
    private long id;
    @SerializedName("id_mocha")
    private String idMocha = "";
    @SerializedName("singer_id")
    private long singerId;
    @SerializedName("name")
    private String name;

    //Cai nay dung cho chu de, ca si
    @SerializedName("song_num")
    private long totalSongs;
    @SerializedName("album_num")
    private long totalAlbums;
    @SerializedName("video_num")
    private long totalVideos;
    @SerializedName("listen_no")
    private long totalViews;
    @SerializedName("singer_info")
    private String singerInfo;

    //Cai nay dung cho the loai
    @SerializedName("num_song")
    private long totalSong2;
    @SerializedName("num_album")
    private long totalAlbums2;
    @SerializedName("num_video")
    private long totalVideos2;

    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image = "";
    private int resource = 0;
    @SerializedName("image310")
    private String imageLarge = "";
    @SerializedName("cover")
    private String cover = "";
    @SerializedName("avatar")
    private String avatar = "";

    private boolean isChoose = false;
    private int type;
    private int source;

    public Topic() {
        super();

    }

    public Topic(long singerId, String name, String cover) {
        super();
        this.name = name;
        this.cover = cover;
        this.singerId = singerId;
    }

    public Topic(String name, String title, int resource) {
        super();
        this.name = name;
        this.title = title;
        this.resource = resource;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImage() {
        if (TextUtils.isEmpty(imageLarge)) {
            return image;
        } else {
            return imageLarge;
        }
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }

    public String getImageSinger() {
        if (!TextUtils.isEmpty(image))
            return image;
        if (!TextUtils.isEmpty(imageLarge))
            return imageLarge;
        if (!TextUtils.isEmpty(avatar))
            return avatar;
        return "";
    }

    public String getCover() {
        if (!TextUtils.isEmpty(cover))
            return cover;
        if (!TextUtils.isEmpty(imageLarge))
            return imageLarge;
        if (!TextUtils.isEmpty(image))
            return image;
        if (!TextUtils.isEmpty(avatar))
            return avatar;
        return "";
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public String getAvatar() {
        if (!TextUtils.isEmpty(avatar))
            return avatar;
        if (!TextUtils.isEmpty(image))
            return image;
        if (!TextUtils.isEmpty(imageLarge))
            return imageLarge;
        return "";
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        if (name == null)
            name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSingerId() {
        return singerId;
    }

    public void setSingerId(long singerId) {
        this.singerId = singerId;
    }

    public String getIdMocha() {
        return idMocha;
    }

    public void setIdMocha(String idMocha) {
        this.idMocha = idMocha;
    }

    public long getTotalSongs() {
        if (totalSong2 > 0)
            return totalSong2;
        return totalSongs;
    }

    public void setTotalSongs(long totalSongs) {
        this.totalSongs = totalSongs;
    }

    public long getTotalAlbums() {
        if (totalAlbums2 > 0)
            return totalAlbums2;
        return totalAlbums;
    }

    public void setTotalAlbums(long totalAlbums) {
        this.totalAlbums = totalAlbums;
    }

    public long getTotalVideos() {
        if (totalVideos2 > 0)
            return totalVideos2;
        return totalVideos;
    }

    public void setTotalVideos(long totalVideos) {
        this.totalVideos = totalVideos;
    }

    public long getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(long totalViews) {
        this.totalViews = totalViews;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public String getSingerInfo() {
        return singerInfo;
    }

    public void setSingerInfo(String singerInfo) {
        this.singerInfo = singerInfo;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", singerId=" + singerId +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", imageLarge='" + imageLarge + '\'' +
                ", cover='" + cover + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}

package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.TotalComplaint;

import java.util.ArrayList;

public class TotalComplaintAdapter extends ArrayAdapter<TotalComplaint> {

    public TotalComplaintAdapter(Context context, ArrayList<TotalComplaint> districtList) {
        super(context, R.layout.item_spinner_total_complaint, districtList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_spinner_total_complaint, parent, false
            );
        }

        TextView tvSelect = convertView.findViewById(R.id.tvSelect);
        TotalComplaint model = getItem(position);
        tvSelect.setText(model.totalComplaint);

        return convertView;
    }
}


package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeResponse {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("result")
    @Expose
    private List<HomeData> homeResult;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<HomeData> getHomeResult() {
        return homeResult;
    }

    public void setHomeResult(List<HomeData> homeResult) {
        this.homeResult = homeResult;
    }
}

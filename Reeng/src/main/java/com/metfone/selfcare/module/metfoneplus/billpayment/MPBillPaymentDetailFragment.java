package com.metfone.selfcare.module.metfoneplus.billpayment;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Observable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.common.util.Strings;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.databinding.FragmentBillpaymentDetailBinding;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.request.BillPaymentTemplateRequest;
import com.metfone.selfcare.network.metfoneplus.response.BillPaymentTemplateResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountPaymentInfoResponse;
import com.metfone.selfcare.util.DecimalDigitsInputFilter;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import retrofit2.Response;

public class MPBillPaymentDetailFragment extends MPBaseBindingFragment<FragmentBillpaymentDetailBinding> {

    public static final String ARGS = "ARGS";
    public static MPBillPaymentDetailFragment newInstance(WsGetAccountPaymentInfoResponse.Response response) {

        Bundle args = new Bundle();
        args.putSerializable(ARGS, response);
        MPBillPaymentDetailFragment fragment = new MPBillPaymentDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public WsGetAccountPaymentInfoResponse.Response data;
    public ObservableField<String> amount = new ObservableField<>("");
    public ObservableBoolean shouldEnablePayButton = new ObservableBoolean(false);
    private MPBillPaymentTemplateAdapter adapter;
    private MetfonePlusClient client;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_billpayment_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.setHandler(this);
        Utilities.adaptViewForInserts(mBinding.viewRoot);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }
        if(getArguments() != null && !getArguments().isEmpty()){
            data = (WsGetAccountPaymentInfoResponse.Response) getArguments().getSerializable(ARGS);
            mBinding.setHandler(this);
        }
        amount.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                shouldEnablePayButton.set(!Strings.isEmptyOrWhitespace(amount.get()));
            }
        });
        client = new MetfonePlusClient();
        adapter = new MPBillPaymentTemplateAdapter();
        mBinding.templateRecyclerView.setAdapter(adapter);
        // set filter for edtInputAmount: enter only two numbers after decimal point
        mBinding.edtInputAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});

        loadTemplate();
    }

    private void loadTemplate(){
        BillPaymentTemplateRequest.Request request = new BillPaymentTemplateRequest.Request();
        client.wsBillPaymentTemplateRequest(request, new MPApiCallback<BillPaymentTemplateResponse>() {
            @Override
            public void onResponse(Response<BillPaymentTemplateResponse> response) {
                if(response != null && response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null){
                    adapter.setTemplateList(response.body().getResult().getWsResponse().paymentHistoryList);
                }
            }

            @Override
            public void onError(Throwable error) {
                ToastUtils.showToast(getContext(), getString(R.string.m_p_notify_error_in_processing));
            }
        });
    }



    public void onPayClicked(){
        data.paymentAmount = amount.get();
        replaceFragmentWithAnimationDefault(MPBillPaymentConfirmFragment.newInstance(data));
    }

    public void onBack() {
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarColor(R.color.m_home_tab_background_2, true);
            ((HomeActivity)mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }else{
            mParentActivity.finish();
            return;
        }
        popBackStackFragment();
    }
}

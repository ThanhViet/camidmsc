package com.metfone.selfcare.module.netnews.HomeNews.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.HomeNewsModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.TiinUtilities;
import com.metfone.selfcare.ui.glide.ImageLoader;

public class VideoHomeNewAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private final int SECTIONE_HORIZOTAL = 2;
    private final int SECTIONE_EMPTY = 0;
    private HomeNewsModel model;
    private Context mContext;
    private AbsInterface.OnHomeNewsItemListener listener;

    public VideoHomeNewAdapter(HomeNewsModel model, Context mContext, AbsInterface.OnHomeNewsItemListener listener) {
        this.model = model;
        this.mContext = mContext;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case SECTIONE_HORIZOTAL:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_tiin_banner_video, parent, false);
                break;
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.holder_tiin_empty, parent, false);
                break;
        }
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        try {
            final NewsModel tiinModel = getItem(position);
            if (holder.getView(R.id.root_item) != null) {
                ViewGroup.LayoutParams layoutParams = holder.getView(R.id.root_item).getLayoutParams();
                layoutParams.width = ApplicationController.self().getWidthPixels();
                layoutParams.height = (int) (layoutParams.width / Constants.RATIO_COVER_MOVIE);
                holder.getView(R.id.root_item).setLayoutParams(layoutParams);
            }
            if (holder.getView(R.id.iv_thumb) != null) {
                ImageLoader.setNewsImage(mContext, tiinModel.getImage169(), (ImageView) holder.getView(R.id.iv_thumb));
            }
            if (holder.getView(R.id.tv_title) != null) {
//                holder.setText(R.id.tv_title, tiinModel.getTitle());
                TiinUtilities.setText(holder.getView(R.id.tv_title), tiinModel.getTitle(),tiinModel.getTypeIcon());
            }
            if (holder.getView(R.id.tv_description) != null) {
                holder.setVisible(R.id.tv_description, false);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickVideo(tiinModel);
                }
            });
        } catch (Exception ex) {

        }
    }

    @Override
    public int getItemCount() {
        return model.getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) != null) {
            return SECTIONE_HORIZOTAL;
        }
        return SECTIONE_EMPTY;
    }

    public NewsModel getItem(int position) {
        try {
            return model.getData().get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }
}

package com.metfone.selfcare.module.movienew.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RateResponse {
    String code;
    String message;
}

package com.metfone.selfcare.module.keeng.holder;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;

public class TopicHolder extends BaseHolder implements OnClickListener {

    public TextView tvTitle, tvContent, tvListened;
    public CircleImageView ivAvatar;
    public ImageView ivAvatar1;
    public ImageView ivBg;

    public TopicHolder(View itemView, BaseListener.OnClickMedia listener) {
        super(itemView, listener);

//        ivBg = (ImageView) itemView.findViewById(R.id.ivBg);
        tvTitle = (TextView) itemView.findViewById(R.id.title);
        tvContent = (TextView) itemView.findViewById(R.id.content);
        tvListened = (TextView) itemView.findViewById(R.id.listen);
        ivAvatar = (CircleImageView) itemView.findViewById(R.id.ivAvatar);
//        ivAvatar1 = (ImageView) itemView.findViewById(R.id.ivAvatar1);
        if (ivAvatar1 != null) {
            ivAvatar1.setOnClickListener(this);
        }

        ivAvatar.setOnClickListener(this);

        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onTopicClick(v, getAdapterPosition());
        }
    }

}

package com.metfone.selfcare.module.tiin.hometiin.view;

import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.response.HomeTiinResponse;

import java.util.List;

public interface HomeTiinMvpView extends MvpView {
    void loadDataSuccess(boolean flag);

    void bindData(List<HomeTiinModel> response);

    void saveDataCache(String data);
}

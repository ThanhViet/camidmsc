package com.metfone.selfcare.module.home_kh.tab.model;

import com.metfone.selfcare.R;

import java.util.ArrayList;
import java.util.List;

public class KhHomeRewardCategoryList implements IHomeModelType {
    public int titleRes = R.string.kh_home_reward;

    public List<KhHomeRewardSearchItem> categoryItems = new ArrayList<>();

    @Override
    public int getItemType() {
        return IHomeModelType.REWARD_CATEGORY;
    }
}

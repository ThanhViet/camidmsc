package com.metfone.selfcare.module.netnews.request;

public class AdvertisingRequest {
    private int type;
    private int vip = 1;
    private int pid;

    public AdvertisingRequest() {
    }

    public AdvertisingRequest(int type, int vip, int pid) {
        this.type = type;
        this.vip = vip;
        this.pid = pid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getVip() {
        return vip;
    }

    public void setVip(int vip) {
        this.vip = vip;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
}

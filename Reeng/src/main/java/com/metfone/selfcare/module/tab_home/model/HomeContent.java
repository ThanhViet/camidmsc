package com.metfone.selfcare.module.tab_home.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.util.Utilities;

import java.io.Serializable;
import java.util.ArrayList;

public class HomeContent implements Serializable {
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_NEWS = "news";
    public static final String TYPE_MOVIE = "film";
    public static final String TYPE_MUSIC = "music";
    public static final String TYPE_BANNER = "banner";
    public static final String TYPE_COMIC = "comic";
    public static final String TYPE_TIIN = "tiin";

    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;

    @SerializedName("flash_hot")
    private ArrayList<Content> listFlashHot;

    @SerializedName("play_hot")
    private ArrayList<Content> listPlayHot;

    @SerializedName("channel_hot")
    private ArrayList<Content> listChannelHot;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Content> getListFlashHot() {
        if (listFlashHot == null) listFlashHot = new ArrayList<>();
        return listFlashHot;
    }

    public ArrayList<Content> getListPlayHot() {
        if (listPlayHot == null) listPlayHot = new ArrayList<>();
        return listPlayHot;
    }

    public ArrayList<Content> getListChannelHot() {
        if (listChannelHot == null) listChannelHot = new ArrayList<>();
        return listChannelHot;
    }

    private void checkEmpty(ArrayList<Content> list, boolean isFlashHot, boolean isPlayHot, boolean isChannelHot) {
        if (!(isVideo() || isNews() || isMovie() || isMusic() || isComic() || isTiin()) && list != null) {
            list.clear();
            return;
        }
        if (Utilities.notEmpty(list)) {
            if (isChannelHot && !isVideo()) {
                list.clear();
            }
            int size = list.size() - 1;
            for (int i = size; i >= 0; i--) {
                if (list.get(i) == null) list.remove(i);
                else {
                    list.get(i).setParentId(id);
                    list.get(i).setParentType(type);
                    list.get(i).setParentName(title);
                    list.get(i).setFlashHot(isFlashHot);
                    list.get(i).setPlayHot(isPlayHot);
                    list.get(i).setChannelHot(isChannelHot);
                    if (isVideo()) {
                        if (isFlashHot) list.get(i).setPosition(LogApi.POS_HOME_SLIDE_VIDEO);
                        else if (isPlayHot) list.get(i).setPosition(LogApi.POS_HOME_VIDEO);
                        else if (isChannelHot) list.get(i).setPosition(LogApi.POS_HOME_CHANNEL);
                    } else if (isMusic()) {
                        if (isFlashHot) list.get(i).setPosition(LogApi.POS_HOME_SLIDE_MUSIC);
                        else if (isPlayHot) list.get(i).setPosition(LogApi.POS_HOME_MUSIC);
                    } else if (isMovie()) {
                        if (isFlashHot) list.get(i).setPosition(LogApi.POS_HOME_SLIDE_MOVIE);
                        else if (isPlayHot) list.get(i).setPosition(LogApi.POS_HOME_MOVIE);
                    } else if (isNews()) {
                        if (isFlashHot) list.get(i).setPosition(LogApi.POS_HOME_SLIDE_NEWS);
                        else if (isPlayHot) list.get(i).setPosition(LogApi.POS_HOME_NEWS);
                    } else if (isTiin()) {
                        if (isFlashHot) list.get(i).setPosition(LogApi.POS_HOME_SLIDE_TIIN);
                        else if (isPlayHot) list.get(i).setPosition(LogApi.POS_HOME_TIIN);
                    } else if (isComic()) {
                        if (isFlashHot) list.get(i).setPosition(LogApi.POS_HOME_SLIDE_COMIC);
                        else if (isPlayHot) list.get(i).setPosition(LogApi.POS_HOME_COMIC);
                    }
                }
            }
        }
    }

    public void checkEmpty() {
        checkEmpty(listFlashHot, true, false, false);
        checkEmpty(listPlayHot, false, true, false);
        checkEmpty(listChannelHot, false, false, true);
    }

    public boolean isEmpty() {
        return Utilities.isEmpty(listFlashHot) && Utilities.isEmpty(listPlayHot) && Utilities.isEmpty(listChannelHot);
    }

    public boolean isVideo() {
        return TYPE_VIDEO.equals(type);
    }

    public boolean isNews() {
        return TYPE_NEWS.equals(type);
    }

    public boolean isMovie() {
        return TYPE_MOVIE.equals(type);
    }

    public boolean isMusic() {
        return TYPE_MUSIC.equals(type);
    }

    public boolean isComic() {
        return TYPE_COMIC.equals(type);
    }

    public boolean isTiin() {
        return TYPE_TIIN.equals(type);
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "HomeContent{" +
                "type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", listFlashHot=" + listFlashHot +
                ", listPlayHot=" + listPlayHot +
                ", listChannelHot=" + listChannelHot +
                '}';
    }
}

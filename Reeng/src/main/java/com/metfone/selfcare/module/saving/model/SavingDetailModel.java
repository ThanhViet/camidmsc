/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/20
 */

package com.metfone.selfcare.module.saving.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SavingDetailModel {
    @SerializedName("total_saving")
    private String totalSaving = "";
    @SerializedName("callout_saving")
    private String callOutSaving = "";
    @SerializedName("smsout_saving")
    private String smsOutSaving = "";
    @SerializedName("saving_details")
    private List<SavingDetails> listDetails;
    @SerializedName("range_date")
    private String rangeDate = "";
    @SerializedName("total_saving_int")
    private long totalSavingInt = 0;
    @SerializedName("smsout_saving_int")
    private long smsOutSavingInt = 0;
    @SerializedName("callout_saving_int")
    private long callOutSavingInt = 0;

    public static SavingStatistics convert2Statistics(SavingDetailModel item) {
        SavingStatistics model = new SavingStatistics();
        if (item != null) {
            model.setTotalSavingInt(item.getTotalSavingInt());
            model.setSmsOutSavingInt(item.getSmsOutSavingInt());
            model.setCallOutSavingInt(item.getCallOutSavingInt());
            model.setTotalSaving(item.getTotalSaving());
            model.setSmsOutSaving(item.getSmsOutSaving());
            model.setCallOutSaving(item.getCallOutSaving());
            model.setRangeDate(item.getRangeDate());
            model.setShowWithAnimation(true);
        }
        return model;
    }

    public long getTotalSavingInt() {
        return totalSavingInt;
    }

    public void setTotalSavingInt(long totalSavingInt) {
        this.totalSavingInt = totalSavingInt;
    }

    public long getSmsOutSavingInt() {
        return smsOutSavingInt;
    }

    public void setSmsOutSavingInt(long smsOutSavingInt) {
        this.smsOutSavingInt = smsOutSavingInt;
    }

    public long getCallOutSavingInt() {
        return callOutSavingInt;
    }

    public void setCallOutSavingInt(long callOutSavingInt) {
        this.callOutSavingInt = callOutSavingInt;
    }

    public List<SavingDetails> getListDetails() {
        if (listDetails == null) listDetails = new ArrayList<>();
        return listDetails;
    }

    public void setListDetails(List<SavingDetails> listDetails) {
        this.listDetails = listDetails;
    }

    public String getCallOutSaving() {
        return callOutSaving;
    }

    public void setCallOutSaving(String callOutSaving) {
        this.callOutSaving = callOutSaving;
    }

    public String getSmsOutSaving() {
        return smsOutSaving;
    }

    public void setSmsOutSaving(String smsOutSaving) {
        this.smsOutSaving = smsOutSaving;
    }

    public String getTotalSaving() {
        return totalSaving;
    }

    public void setTotalSaving(String totalSaving) {
        this.totalSaving = totalSaving;
    }

    public String getRangeDate() {
        return rangeDate;
    }

    public void setRangeDate(String rangeDate) {
        this.rangeDate = rangeDate;
    }

    public static class SavingDetails {
        boolean isFirst;
        @SerializedName("details")
        private List<Details> details;
        @SerializedName("date")
        private String date = "";

        public List<Details> getDetails() {
            if (details == null) details = new ArrayList<>();
            return details;
        }

        public void setDetails(List<Details> details) {
            this.details = details;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public boolean isFirst() {
            return isFirst;
        }

        public void setFirst(boolean first) {
            isFirst = first;
        }
    }

    public static class Details {
        @SerializedName("price")
        private String price = "";
        @SerializedName("time_detail")
        private String timeDetail = "";
        @SerializedName("duration")
        private String duration = "";
        @SerializedName("toUser")
        private String toUser = "";
        @SerializedName("icon")
        private String imageUrl = "";

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getTimeDetail() {
            return timeDetail;
        }

        public void setTimeDetail(String timeDetail) {
            this.timeDetail = timeDetail;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getToUser() {
            return toUser;
        }

        public void setToUser(String toUser) {
            this.toUser = toUser;
        }
    }
}

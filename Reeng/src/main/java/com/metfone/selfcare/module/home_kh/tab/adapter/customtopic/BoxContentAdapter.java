/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter.customtopic;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.tab.TabHomeKhListener;
import com.metfone.selfcare.module.home_kh.tab.adapter.livechannel.LiveChannelHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.rewardcategory.RewardCategoryListHolder;
import com.metfone.selfcare.module.home_kh.tab.adapter.rewardtopic.RewardTopicListHolder;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.metfoneplus.adapter.MPHomeServiceListHolder;
import com.metfone.selfcare.util.Log;

public class BoxContentAdapter extends BaseAdapter<BaseAdapter.ViewHolder, Object> {
    private TabHomeKhListener.OnAdapterClick listener;

    public BoxContentAdapter(Activity act) {
        super(act);
    }

    public void setListener(TabHomeKhListener.OnAdapterClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {

        Object item = getItem(position);
        if (item instanceof IHomeModelType) {
            return ((IHomeModelType) item).getItemType();
        }

        // check type topic or reward
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case IHomeModelType.POPULAR_LIVE_CHANNEL:
                return new LiveChannelHolder(layoutInflater.inflate(R.layout.live_channel_home_kh,parent,false),activity,listener);
            case IHomeModelType.CUSTOM_TOPIC:
                return new MovieHolder(layoutInflater.inflate(R.layout.cinema_movie_item_kh, parent, false), activity, listener);
            case IHomeModelType.REWARD_CATEGORY:
                return new RewardCategoryListHolder(layoutInflater.inflate(R.layout.reward_home_cate_item_kh, parent, false), activity, listener);
            case IHomeModelType.REWARD_TOPIC_1:
            case IHomeModelType.REWARD_TOPIC_2:
                return new RewardTopicListHolder(layoutInflater.inflate(R.layout.holder_reward_topic_kh, parent, false), activity, listener);
            case IHomeModelType.MP_SERVICE:
                return new MPHomeServiceListHolder(layoutInflater.inflate(R.layout.item_service_new, parent, false), activity, listener);

        }
        return new EmptyHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Object item = getItem(position);
        //Log.d(TAG, "onBindViewHolder position:" + position + ", item: " + item);
        holder.bindData(item, position);
    }
}

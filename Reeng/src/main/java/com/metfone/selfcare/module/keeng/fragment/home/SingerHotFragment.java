package com.metfone.selfcare.module.keeng.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.home.SingerHotAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.network.restpaser.RestAllTopic;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.CustomGridLayoutManager;
import com.metfone.selfcare.module.keeng.widget.GridSpacingItemDecoration;

import java.util.ArrayList;

public class SingerHotFragment extends RecyclerFragment<Topic> implements BaseListener.OnLoadMoreListener {
    private SingerHotAdapter adapter;
    private int currentPage = 1;
    private ListenerUtils listenerUtils;
    private TextView tvTitle;
    private View btnBack;

    public SingerHotFragment() {
        super();
        numPerPage = KeengApi.NUMBER_PER_PAGE;
    }

    public static SingerHotFragment newInstance() {
        Bundle args = new Bundle();
        SingerHotFragment fragment = new SingerHotFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_album_hot;
    }

    @Override
    public String getName() {
        return "SingerHotFragment";
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onBackPressed();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearData();
                adapter = new SingerHotAdapter(mActivity, getDatas(), TAG);
                GridLayoutManager layoutManager = new CustomGridLayoutManager(mActivity, 3);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, getResources().getDimensionPixelOffset(R.dimen.padding_normal), true));
                recyclerView.setAdapter(adapter);
                adapter.setOnclickListener(SingerHotFragment.this);
                adapter.setOnCheckEnd(recyclerView, SingerHotFragment.this);
                adapter.setRecyclerView(recyclerView, SingerHotFragment.this);
                doLoadData(true);
            }
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        tvTitle = view.findViewById(R.id.tv_title);
        btnBack = view.findViewById(R.id.iv_back);
        tvTitle.setText(getString(R.string.singer_hot));
        return view;
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        App.getInstance().cancelPendingRequests(KeengApi.GET_SINGER_LIST);
        isLoading = false;
        refreshed();
        loadMored();
        loadingFinish();
        if (adapter != null)
            adapter.setLoaded();
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        new KeengApi().getSingerList(currentPage, numPerPage, new Response.Listener<RestAllTopic>() {
                    @Override
                    public void onResponse(RestAllTopic result) {
                        doAddResult(result.getData());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error);
                        if (errorCount < MAX_ERROR_RETRY) {
                            errorCount++;
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    loadData();
                                }
                            }, Constants.TIME_DELAY_RETRY);
                            return;
                        }
                        doAddResult(null);
                    }
                });
    }

    private void doAddResult(ArrayList<Topic> result) {
        Log.d(TAG, "doAddResult ...............");
        errorCount = 0;
        isLoading = false;
        try {
            adapter.setLoaded();
            checkLoadMoreAbsolute(result);
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                loadingError(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doLoadData(true);
                    }
                });
                return;
            }
            if (getDatas().isEmpty() && result.isEmpty()) {
                loadMored();
                loadingEmpty();
                refreshed();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                setDatas(result);
//                if (mActivity.getFitterSinger() == ITEM_SINGER_A_Z) {
//                    Collections.sort(getDatas(), sortABC);
//                }
                adapter.notifyDataSetChanged();
                currentPage++;
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        currentPage = 1;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onMediaClick(View v, int position) {
        try {
            Topic item = adapter.getItem(position);
            if (item != null && mActivity != null)
                mActivity.gotoSingerDetail(item);
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onDestroy() {
        if (adapter != null) {
            adapter.setOnclickListener(null);
        }
        super.onDestroy();
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }
}

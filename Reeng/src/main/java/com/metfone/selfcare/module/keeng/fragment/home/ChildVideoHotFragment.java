package com.metfone.selfcare.module.keeng.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.adapter.home.MediaHomeHotAdapter;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.keeng.widget.MediaItemAnimator;

import java.util.ArrayList;
import java.util.List;

public class ChildVideoHotFragment extends RecyclerFragment<AllModel> implements BaseListener.OnLoadMoreListener {
    int currentTab = 0;
    //    private TextView btnHot, btnNew, tvTitle;
//    private View btnBack;
    View layoutToolBar;
    private MediaHomeHotAdapter adapter;
    private ListenerUtils listenerUtils;

    public static ChildVideoHotFragment newInstance() {
        Bundle args = new Bundle();
        ChildVideoHotFragment fragment = new ChildVideoHotFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static ChildVideoHotFragment newInstance(Bundle args) {
        ChildVideoHotFragment fragment = new ChildVideoHotFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "ChildVideoHotFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_album_hot;
    }

//    private void selectTab(int position) {
//        if (position == 0) {
//            btnHot.setSelected(true);
//            btnNew.setSelected(false);
//        } else {
//            btnHot.setSelected(false);
//            btnNew.setSelected(true);
//        }
//    }

    @SuppressWarnings("NullableProblems")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        layoutToolBar = view.findViewById(R.id.layout_toolbar);
        layoutToolBar.setVisibility(View.GONE);
//        tvTitle = view.findViewById(R.id.tv_title);
//        btnBack = view.findViewById(R.id.iv_back);
//        btnHot = view.findViewById(R.id.button_hot);
//        btnNew = view.findViewById(R.id.button_new);
//        btnHot.setVisibility(View.VISIBLE);
//        btnNew.setVisibility(View.VISIBLE);
//        tvTitle.setText(getString(R.string.video_mv).toUpperCase());
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        btnBack.setOnClickListener(new OnSingleClickListener() {
//            @Override
//            public void onSingleClick(View view) {
//                onBackPressed();
//            }
//        });
        if (getArguments() != null) {
            currentTab = getArguments().getInt("type");
        }
        new Handler().postDelayed(() -> {
            clearData();
            adapter = new MediaHomeHotAdapter(mActivity, getDatas(), TAG);
            setupGridRecycler(adapter, 2, R.dimen.spacing_small, true);
            recyclerView.setItemAnimator(new MediaItemAnimator());
            adapter.setRecyclerView(recyclerView, ChildVideoHotFragment.this);
//            selectTab(currentTab);
//            btnHot.setOnClickListener(v -> {
//                if (currentTab != 0) {
//                    App.getInstance().cancelPendingRequests(KeengApi.GET_VIDEO_HOT);
//                    App.getInstance().cancelPendingRequests(KeengApi.GET_VIDEO_NEW);
//                    isLoading = false;
//                    currentTab = 0;
//                    selectTab(currentTab);
//                    currentPage = 1;
//                    clearData();
//                    adapter.notifyDataSetChanged();
//                    doLoadData(true);
//                } else {
//                    recyclerScrollToPosition(0);
//                }
//            });
//
//            btnNew.setOnClickListener(v -> {
//                if (currentTab == 0) {
//                    App.getInstance().cancelPendingRequests(KeengApi.GET_VIDEO_HOT);
//                    App.getInstance().cancelPendingRequests(KeengApi.GET_VIDEO_NEW);
//                    isLoading = false;
//                    currentTab = 1;
//                    selectTab(currentTab);
//                    currentPage = 1;
//                    clearData();
//                    adapter.notifyDataSetChanged();
//                    doLoadData(true);
//                } else {
//                    recyclerScrollToPosition(0);
//                }
//            });
            doLoadData(true);
        }, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        App.getInstance().cancelPendingRequests(KeengApi.GET_VIDEO_HOT);
        App.getInstance().cancelPendingRequests(KeengApi.GET_VIDEO_NEW);
        isLoading = false;
        loadMored();
        refreshed();
        loadingFinish();
        if (adapter != null)
            adapter.setLoaded();
        super.onDetach();
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadData();
        }
    }

    private void loadData() {
        if (currentTab == 0) {
            new KeengApi().getVideoHot(currentPage, numPerPage, result -> doAddResult(result.getData()), error -> {
                Log.e(TAG, error);
                if (errorCount < MAX_ERROR_RETRY) {
                    errorCount++;
                    new Handler().postDelayed(this::loadData, Constants.TIME_DELAY_RETRY);
                    return;
                }
                doAddResult(null);
            });
        } else {
            new KeengApi().getVideoNew(currentPage, numPerPage, result -> doAddResult(result.getData()), error -> {
                Log.e(TAG, error);
                if (errorCount < MAX_ERROR_RETRY) {
                    errorCount++;
                    new Handler().postDelayed(this::loadData, Constants.TIME_DELAY_RETRY);
                    return;
                }
                doAddResult(null);
            });
        }
    }

    private void doAddResult(List<AllModel> result) {
        Log.d(TAG, "doAddResult ...............");
        errorCount = 0;
        isLoading = false;
        try {
            adapter.setLoaded();
            checkLoadMoreAbsolute(result);
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                loadingError(v -> doLoadData(true));
                return;
            }
            if (getDatas().size() == 0 && result.size() == 0) {
                loadMored();
                loadingEmpty();
                refreshed();
            } else {
                refreshed();
                loadMored();
                loadingFinish();
                List<AllModel> list = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    if (result.get(i) != null && result.get(i).getType() == Constants.TYPE_VIDEO)
                        list.add(result.get(i));
                }
                setDatas(list);
                adapter.notifyDataSetChanged();
                currentPage++;
            }
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onMediaClick(View v, int position) {
        if (mActivity == null || adapter == null) {
            return;
        }
        AllModel item = adapter.getItem(position);
        if (item != null) {
            item.setSource(MediaLogModel.SRC_VIDEO_LIST);
            mActivity.setMediaToPlayVideo(item);
        }
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }
}

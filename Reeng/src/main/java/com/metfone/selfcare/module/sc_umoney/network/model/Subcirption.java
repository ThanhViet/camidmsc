package com.metfone.selfcare.module.sc_umoney.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subcirption {
    @SerializedName("serviceType")
    @Expose
    private int serviceType;
    @SerializedName("standard")
    @Expose
    private int standard;
    @SerializedName("startDatetime")
    @Expose
    private String startDatetime;
    @SerializedName("actStatus")
    @Expose
    private String actStatus;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("idNo")
    @Expose
    private String idNo;
    @SerializedName("dataPos")
    @Expose
    private int dataPos;
    @SerializedName("responseCode")
    @Expose
    private int responseCode;
    @SerializedName("subId")
    @Expose
    private long subId;
    @SerializedName("productCode")
    @Expose
    private String productCode;
    @SerializedName("userUsing")
    @Expose
    private String userUsing;
    @SerializedName("adjustmentNagative")
    @Expose
    private int adjustmentNagative;
    @SerializedName("custId")
    @Expose
    private long custId;
    @SerializedName("payment")
    @Expose
    private long payment;
    @SerializedName("staOfCycle")
    @Expose
    private int staOfCycle;
    @SerializedName("subType")
    @Expose
    private int subType;
    @SerializedName("actStatusBit")
    @Expose
    private String actStatusBit;
    @SerializedName("issueDate")
    @Expose
    private String issueDate;
    @SerializedName("issuePlace")
    @Expose
    private String issuePlace;

    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int serviceType) {
        this.serviceType = serviceType;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

    public String getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(String startDatetime) {
        this.startDatetime = startDatetime;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public int getDataPos() {
        return dataPos;
    }

    public void setDataPos(int dataPos) {
        this.dataPos = dataPos;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public long getSubId() {
        return subId;
    }

    public void setSubId(long subId) {
        this.subId = subId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUserUsing() {
        return userUsing;
    }

    public void setUserUsing(String userUsing) {
        this.userUsing = userUsing;
    }

    public int getAdjustmentNagative() {
        return adjustmentNagative;
    }

    public void setAdjustmentNagative(int adjustmentNagative) {
        this.adjustmentNagative = adjustmentNagative;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public long getPayment() {
        return payment;
    }

    public void setPayment(long payment) {
        this.payment = payment;
    }

    public int getStaOfCycle() {
        return staOfCycle;
    }

    public void setStaOfCycle(int staOfCycle) {
        this.staOfCycle = staOfCycle;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public String getActStatusBit() {
        return actStatusBit;
    }

    public void setActStatusBit(String actStatusBit) {
        this.actStatusBit = actStatusBit;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssuePlace() {
        return issuePlace;
    }

    public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }
}

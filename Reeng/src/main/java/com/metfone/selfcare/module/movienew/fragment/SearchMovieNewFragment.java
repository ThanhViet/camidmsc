package com.metfone.selfcare.module.movienew.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.NewEditProfileActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.databinding.SearchMovieNewFragmentBinding;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.Movie2;
import com.metfone.selfcare.model.tabMovie.SearchMovieResponse;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.common.CommonSearch;
import com.metfone.selfcare.module.movienew.common.Constant;
import com.metfone.selfcare.module.movienew.holder.HistorySearchViewHolder;
import com.metfone.selfcare.module.movienew.holder.ResultSearchViewHolder;
import com.metfone.selfcare.module.movienew.holder.TopicViewHolder;
import com.metfone.selfcare.module.movienew.model.CategoryModel;
import com.metfone.selfcare.module.movienew.model.Country;
import com.metfone.selfcare.module.movienew.model.CountryModel;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.MaybeModel;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.RetrofitInstance;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static org.linphone.mediastream.MediastreamerAndroidContext.getContext;

public class SearchMovieNewFragment extends BaseSlidingFragmentActivity implements ItemViewClickListener, OnClickContentMovie, View.OnClickListener {

    private final int limit = 3;
    protected ApplicationController app;
    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    UserInfo userInfo = userInfoBusiness.getUser();
    int page = 0;
    private SearchMovieNewFragmentBinding searchFragmentBinding;
    private RetrofitInstance retrofitInstance = new RetrofitInstance();
    private BaseAdapter baseAdapter;
    private BaseAdapter mHistoryAdapter;
    private BaseAdapter mHistoryAdapterHome;
    private BaseAdapter mResultSearchAdapter;
    private ArrayList<Object> topicModels = new ArrayList<>();
    private ArrayList<Movie2> resultData = new ArrayList<>();
    private CommonSearch mCommonSearch;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> mHistoryArraylist = new ArrayList<>();
    private ArrayList<MaybeModel> movieArrayList = new ArrayList<>();
    private ArrayList<SearchMovieResponse.Doc> mMovieArrayList = new ArrayList<>();
    private ArrayList<com.metfone.selfcare.ui.tabvideo.BaseAdapter.ItemObject> items; // danh sách phần từ
    private com.metfone.selfcare.ui.tabvideo.BaseAdapter.ItemObject itemLoadMore; // item load more
    private boolean hasReached = false;
    private String keySearchOld = "";
    private int misdn = 1;
    private int ref_id = 1;
    private ArrayList<Movie> movieArrayListMaybe = new ArrayList<>();
    private BaseAdapter mResultMaybeAdapter;
    private boolean doneSearch = false;
    private boolean isSearching = false;
    private int DELAY_SEARCH = 300;
    private Runnable runnableSearch = new Runnable() {
        @Override
        public void run() {
            search();
        }
    };
    private Handler handlerSearch = new Handler();
    public static SearchMovieNewFragment newInstance() {
        return new SearchMovieNewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchFragmentBinding = DataBindingUtil.setContentView(this, R.layout.search_movie_new_fragment);
        mCommonSearch = new CommonSearch(this);
        app = (ApplicationController) this.getApplication();
        initView();
        initData();
        hideView();
        listenerView();
        hideSoftKeyboard();

    }

    private void listenerView() {
        searchFragmentBinding.tvButtonSkip.setOnClickListener(this::onClick);
        searchFragmentBinding.btnClear.setOnClickListener(this::onClick);
        searchFragmentBinding.edtSearch.setOnClickListener(this::onClick);
        searchFragmentBinding.btnClose.setOnClickListener(this::onClick);
        searchFragmentBinding.edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
        searchFragmentBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String textInput = s.toString().trim();
                if (textInput.isEmpty()) {
                    searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
                    searchFragmentBinding.tvButtonSkip.setTextColor(Color.WHITE);
                    searchFragmentBinding.btnClose.setImageResource(R.drawable.ic_search_new);
                }
//                else {
//                    searchFragmentBinding.tvButtonSkip.setTextColor(getResources().getColor(R.color.tab_search_indicator));
//                    searchFragmentBinding.tvButtonSkip.setText(R.string.text_search_movie);
//                    searchFragmentBinding.btnClose.setImageResource(R.drawable.ic_back_new);
//                    doneSearch = false;
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    searchFragmentBinding.contrasResultSearch.setVisibility(View.GONE);
                    searchFragmentBinding.recyclerHistory.setVisibility(View.GONE);
                } else {
                    searchFragmentBinding.recyclerHistory.setVisibility(View.VISIBLE);
                    handlerSearch.postDelayed(runnableSearch,DELAY_SEARCH);
                }
            }
        });
        layoutManager = new CustomLinearLayoutManager(getContext(), LinearLayout.VERTICAL, false);
        com.metfone.selfcare.adapter.BaseAdapter.setRecyclerViewLoadMore(searchFragmentBinding.recycleResultSearch, new com.metfone.selfcare.adapter.BaseAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!isSearching && !hasReached) {
                    page++;
                    search(keySearchOld);
                }
            }
        });
    }

    private void initData() {
        MovieApi.getInstance().getTopicCountry(new ApiCallbackV2<ArrayList<Country>>() {
            @Override
            public void onSuccess(String msg, ArrayList<Country> result) throws JSONException {
                if (result != null)
                    topicModels.addAll(result);
                baseAdapter.updateAdapter(topicModels);
                checkEmptyTopic();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
        MovieApi.getInstance().getTopicCategory(new ApiCallbackV2<ArrayList<CategoryModel>>() {
            @Override
            public void onSuccess(String msg, ArrayList<CategoryModel> result) throws JSONException {
                if (result != null) {
                    ArrayList<CategoryModel> categoryArr = new ArrayList<>();
                    for (CategoryModel categoryModel : result) {
                        if (!categoryModel.getCategoryid().equals("868462") && !categoryModel.getCategoryid().equals("868459")) {
                            categoryArr.add(categoryModel);
                        }
                    }
                    if (categoryArr != null && categoryArr.size() > 0) {
                        if (topicModels.size() > 0) {
                            topicModels.addAll(0, categoryArr);
                        } else {
                            topicModels.addAll(categoryArr);
                        }
                    }
                }
                baseAdapter.updateAdapter(topicModels);
                //check empty topic
                checkEmptyTopic();
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
        MovieApi.getInstance().getMaybeYouLike(new ApiCallbackV2<ArrayList<HomeData>>() {
            @Override
            public void onSuccess(String msg, ArrayList<HomeData> result) throws JSONException {
                if (result != null && result.size() > 0) {
                    movieArrayListMaybe.clear();
                    for (int i = 0; i < result.size(); i++) {
                        movieArrayListMaybe.add(new Movie(result.get(i)));
                    }

                    if (result.size() > 0) {
                        mResultMaybeAdapter.updateAdapter(movieArrayListMaybe);
                        searchFragmentBinding.contraisMaybe.setVisibility(movieArrayListMaybe.size() > 0 ? View.VISIBLE : View.GONE);
                    } else {
                        searchFragmentBinding.contraisMaybe.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onError(String s) {
                searchFragmentBinding.contraisMaybe.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void checkEmptyTopic() {
        //check empty topic
        if (topicModels.size() == 0) {
            searchFragmentBinding.contrasSearchHome.setVisibility(View.GONE);
        } else {
            searchFragmentBinding.contrasSearchHome.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        changeStatusBar(getResources().getColor(R.color.m_home_tab_background));
        setupUI(searchFragmentBinding.constraintLayout4);
        mHistoryArraylist = mCommonSearch.getSearchHistoryLimit();
        if (mHistoryArraylist.size() > 0) {
            searchFragmentBinding.contrainRecent.setVisibility(View.VISIBLE);
        }
        TopicViewHolder topicHolder = new TopicViewHolder(getWindow().getDecorView().getRootView(), this);
        baseAdapter = new BaseAdapter(topicModels, this, R.layout.item_topic_search, topicHolder);
        FlexboxLayoutManager layoutManagerCast = new FlexboxLayoutManager(this);
        layoutManagerCast.setFlexDirection(FlexDirection.ROW);
        layoutManagerCast.setJustifyContent(JustifyContent.FLEX_START);
        searchFragmentBinding.recyclerViewTopic.setLayoutManager(layoutManagerCast);
        searchFragmentBinding.recyclerViewTopic.setAdapter(baseAdapter);


        ResultSearchViewHolder resultSearchViewHolder = new ResultSearchViewHolder(getWindow().getDecorView().getRootView(), this);
        mResultSearchAdapter = new BaseAdapter(mMovieArrayList, this, R.layout.item_movie_new, resultSearchViewHolder);
        searchFragmentBinding.recycleResultSearch.setLayoutManager(new GridLayoutManager(this, 3));
        searchFragmentBinding.recycleResultSearch.setAdapter(mResultSearchAdapter);

        //Search movie
        HistorySearchViewHolder historySearchViewHolder = new HistorySearchViewHolder(getWindow().getDecorView().getRootView(), this, Constant.RESULT);
        mHistoryAdapter = new BaseAdapter(mHistoryArraylist, this, R.layout.item_history_search, historySearchViewHolder);
        mHistoryAdapter.setItemCount(100);
        searchFragmentBinding.recyclerHistory.setLayoutManager(new LinearLayoutManager(this));
        searchFragmentBinding.recyclerHistory.setAdapter(mHistoryAdapter);

        // history search
        HistorySearchViewHolder historySearchViewHomeHolder = new HistorySearchViewHolder(getWindow().getDecorView().getRootView(), this, Constant.HOME);
        mHistoryAdapterHome = new BaseAdapter(mHistoryArraylist, this, R.layout.item_history_search, historySearchViewHomeHolder);
        searchFragmentBinding.recyclerRecentSearchesHistory.setLayoutManager(new LinearLayoutManager(this));
        mHistoryAdapterHome.setItemCount(100);
        searchFragmentBinding.recyclerRecentSearchesHistory.setAdapter(mHistoryAdapterHome);

        // list maybe
        mResultMaybeAdapter = new BaseAdapter(movieArrayList, this, R.layout.item_movie_new, resultSearchViewHolder);
        searchFragmentBinding.recyclerMaybe.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        searchFragmentBinding.recyclerMaybe.setAdapter(mResultMaybeAdapter);

        // config touch hide keyboard
        setupUI(searchFragmentBinding.contrasSearchHome, this);
    }

    private void hideView() {
//        searchFragmentBinding.recyclerMaybe.setVisibility(movieArrayListMaybe.size() > 0 ? View.VISIBLE : View.GONE);
//        searchFragmentBinding.recyclerRecentSearchesHistory.setVisibility(mHistoryArraylist.size() > 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        hideKeyboard();
        if (list.get(position) instanceof Country) {
            Country countryModel = (Country) list.get(position);
            String categoryName = countryModel.getCategoryName();
            String categoryNameId = countryModel.getCategoryId();
            ActionCategoryFragment actionCategoryFragment = ActionCategoryFragment.newInstance(categoryNameId, categoryName, ActionCategoryFragment.COUNTRY);
            addFragment(R.id.frameCategory, actionCategoryFragment, true);
        }
        if (list.get(position) instanceof CategoryModel) {
            CategoryModel categoryModel = (CategoryModel) list.get(position);
            String categoryName = categoryModel.getCategoryname();
            String categoryNameId = categoryModel.getCategoryid();
            ActionCategoryFragment actionCategoryFragment = ActionCategoryFragment.newInstance(categoryNameId, categoryName, ActionCategoryFragment.CATEGORY);
            addFragment(R.id.frameCategory, actionCategoryFragment, true);
        }
        if (list.get(position) instanceof String) {
            searchFragmentBinding.edtSearch.setText((String) list.get(position));
            searchFragmentBinding.recyclerHistory.setVisibility(View.GONE);
            page = 0;
            hasReached = false;
            search((String) list.get(position));
        }
        if (list.get(position) instanceof Movie) {
            Movie movie = (Movie) list.get(position);
            this.playMovies(movie);
        }
    }

    private void updateKeySearch(String keySearch) {
        mHistoryArraylist.clear();
        mCommonSearch.insertSearchHistory(keySearch);
        mHistoryArraylist.addAll(mCommonSearch.getSearchHistoryLimit());
        mHistoryAdapterHome.updateAdapter(mHistoryArraylist);
        mHistoryAdapter.updateAdapter(mHistoryArraylist);
        if (searchFragmentBinding.contrainRecent.getVisibility() == View.GONE) {
            searchFragmentBinding.contrainRecent.setVisibility(View.VISIBLE);
        }
    }

    private void clearKeySearch() {
        mHistoryArraylist.clear();
        mHistoryArraylist.addAll(mCommonSearch.getSearchHistoryLimit());
        mHistoryAdapterHome.updateAdapter(mHistoryArraylist);
        mHistoryAdapter.updateAdapter(mHistoryArraylist);
        searchFragmentBinding.contrainRecent.setVisibility(View.GONE);
    }

    public void search() {
        doneSearch = true;
        searchFragmentBinding.recyclerHistory.setVisibility(View.INVISIBLE);
        page = 0;
        hasReached = false;
        search(searchFragmentBinding.edtSearch.getText().toString());
        searchFragmentBinding.btnClose.setImageResource(R.drawable.ic_search_new);
       // hideKeyboard();
    }

    public void search(String keySearch) {
        isSearching = true;
        if (keySearch.isEmpty()) return;
        if (searchFragmentBinding.edtSearch.getText().length() > 0) {
            searchFragmentBinding.edtSearch.setSelection(searchFragmentBinding.edtSearch.getText().length());
        }
        searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
        searchFragmentBinding.progress.setVisibility(View.VISIBLE);
        keySearchOld = keySearch;
        StringBuilder resultSearchTxt = new StringBuilder(getString(R.string.txt_about_search_movie));
        mCommonSearch.getSearchApi().searchMoviesCinema(keySearch, SearchUtils.LIMIT_REQUEST_SEARCH_MOVIE
            , 0, new com.metfone.selfcare.module.search.network.ApiCallback<ArrayList<Movie>>() {
                @Override
                public void onSuccess(String msg, ArrayList<Movie> result) throws Exception {
                    searchFragmentBinding.progress.setVisibility(View.GONE);
                    resultSearchTxt.append(" " + result.size() + " ");
                    resultSearchTxt.append(getString(R.string.txt_results_with_the_keyword));
                    resultSearchTxt.append(" \"" + keySearch + "\".");
                    searchFragmentBinding.tvResultSearch.setText(resultSearchTxt.toString());

                    updateResultSearch(result);
                    updateKeySearch(keySearch);
                    if (result.size() < SearchUtils.LIMIT_REQUEST_SEARCH) {
                        hasReached = true;
                    }
                }

                @Override
                public void onError(String s) {
                    searchFragmentBinding.progress.setVisibility(View.GONE);
                    searchFragmentBinding.filmNotFound.setText(R.string.empty_no_data);
                    searchFragmentBinding.filmNotFound.setVisibility(View.VISIBLE);
                    searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
                    searchFragmentBinding.tvResultSearch.setVisibility(View.GONE);
                    searchFragmentBinding.recycleResultSearch.setVisibility(View.GONE);

                    searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
                    searchFragmentBinding.tvButtonSkip.setTextColor(Color.WHITE);
                }

                @Override
                public void onComplete() {
                    isSearching = false;
                }
            }, true);
    }

    private void updateResultSearch(ArrayList<Movie> result) {
        searchFragmentBinding.contrasResultSearch.setVisibility(View.VISIBLE);
        //        if (result != null && result.size() > 0) {
        mResultSearchAdapter.updateAdapter(result);
        searchFragmentBinding.tvButtonSkip.setText(R.string.text_cancel_search_movie);
        searchFragmentBinding.tvButtonSkip.setTextColor(Color.WHITE);
//        } else {
        searchFragmentBinding.filmNotFound.setVisibility(result == null || result.size() == 0 ? View.VISIBLE : View.GONE);
//        }
    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        HomeData homeData = (HomeData) item;
        this.playMovies(new Movie(homeData));

    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        ToastUtils.makeText(this, "onClickMoreMovieItem");
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {
        ToastUtils.makeText(this, "onClickLikeMovieItem");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvButtonSkip:
                hideKeyboard();
                finish();
//                if (searchFragmentBinding.tvButtonSkip.getText().toString().equals(getString(R.string.search))) {
////                    search();
////                } else {
////                    finish();
////                }
                break;
            case R.id.btnClear:
                mCommonSearch.clearHistory();
                clearKeySearch();
                hideKeyboard();
                break;
            case R.id.btnClose:
                if (!doneSearch) {
                    finish();
                }
                break;
        }
    }
    public void setupUI(View view) {

        if (!(view instanceof ConstraintLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(SearchMovieNewFragment.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
                    continue;
                }
                setupUI(innerView);
            }
        }
    }

}

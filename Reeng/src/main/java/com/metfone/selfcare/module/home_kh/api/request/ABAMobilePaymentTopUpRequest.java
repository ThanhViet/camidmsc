package com.metfone.selfcare.module.home_kh.api.request;

import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class ABAMobilePaymentTopUpRequest extends BaseRequest<ABAMobilePaymentTopUpRequest.Request> {
    public static class Request{
        public String isdn;
        public String topupAmount;
        public String cardType = "AbaPay";
    }
}

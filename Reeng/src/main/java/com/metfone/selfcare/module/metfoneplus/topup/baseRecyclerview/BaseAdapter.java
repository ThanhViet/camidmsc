package com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<?> mArrObject;
    private Context mContext;
    private int mLayout;
    private RecyclerView.ViewHolder viewHolder;
    private int mItemCount = 0;


    public BaseAdapter(List<?> mArrObject, Context mContext, int mLayout, RecyclerView.ViewHolder viewHolder) {
        this.mArrObject = mArrObject;
        this.mContext = mContext;
        this.mLayout = mLayout;
        this.viewHolder = viewHolder;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayout, parent, false);
        return new BaseItemViewHolder(view, viewHolder);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((BaseItemViewHolder) holder).onBilViewHolderBase(mContext, viewHolder, mArrObject, position);

    }

    @Override
    public int getItemCount() {
        if (mItemCount == 0) {
            return mArrObject.size();
        }
        if (mItemCount > mArrObject.size()) {
            return mArrObject.size();
        } else {
            return mItemCount;
        }
    }

    public void updateAdapter(List<?> list) {
        mArrObject = list;
        notifyDataSetChanged();
    }

    public void setItemCount(int size) {
        mItemCount = size;
    }
}

package com.metfone.selfcare.module.livestream.network;

import com.metfone.selfcare.module.livestream.network.parse.RestListLiveStreamMessage;
import com.metfone.selfcare.module.livestream.network.parse.RestLiveStreamMessage;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @GET("chat/getMessages")
    Call<RestListLiveStreamMessage> getListMessages(@Query("msisdn") String msisdn, @Query("roomID") String roomId
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);

    @GET("chat/getMessagesLevel2")
    Call<RestListLiveStreamMessage> getListMessagesLevel2(@Query("msisdn") String msisdn, @Query("roomID") String roomId, @Query("id") String id
            , @Query("timestamp") long timestamp, @Query("token") String token, @Query("security") String security);

    @Headers("Content-Type: application/json")
    @POST("chat/postMessage")
    Call<RestLiveStreamMessage> postMessage(@Body RequestBody body
            , @Header("timestamp") long timestamp, @Header("token") String token, @Header("security") String security);
}

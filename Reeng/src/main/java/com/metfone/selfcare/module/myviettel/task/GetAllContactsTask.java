/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/4
 *
 */

package com.metfone.selfcare.module.myviettel.task;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.module.myviettel.listener.GetAllContactsListener;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GetAllContactsTask extends AsyncTask<Void, Void, ArrayList<PhoneNumber>> {
    private final String TAG = "GetAllContactsTask";
    private WeakReference<ApplicationController> mApplication;
    private GetAllContactsListener listener;
    private String phoneNumbers = "";

    public GetAllContactsTask(ApplicationController application) {
        this.mApplication = new WeakReference<>(application);
    }

    public void setListener(GetAllContactsListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        if (listener != null) listener.onPrepareGetAllContacts();
    }

    @Override
    protected void onPostExecute(ArrayList<PhoneNumber> results) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "onPostExecute results: " + results.size() + "\nphoneNumbers: " + phoneNumbers);
        }
        if (listener != null) listener.onFinishedGetAllContacts(results, phoneNumbers);
    }

    @Override
    protected ArrayList<PhoneNumber> doInBackground(Void... params) {
        ArrayList<PhoneNumber> results = new ArrayList<>();
        if (Utilities.notNull(mApplication)) {
            String myNumber = mApplication.get().getReengAccountBusiness().getJidNumber();
            ContactBusiness contactBusiness = mApplication.get().getContactBusiness();
            ArrayList<PhoneNumber> listContacts = new ArrayList<>(contactBusiness.getListNumberAlls());
            if (Utilities.notEmpty(listContacts)) {
                Collections.sort(listContacts, SearchUtils.getComparatorContactForSearch());
                Map<String, String> mapPhoneNumbers = new HashMap<>();
                StringBuilder sb = new StringBuilder();
                int count = 0;
                boolean check = false;
                for (PhoneNumber item : listContacts) {
                    if (item != null && item.getContactId() != null) {
                        String number = item.getJidNumber();
                        if (!TextUtils.isEmpty(number) && !number.equals(myNumber) && !mapPhoneNumbers.containsKey(number)) {
                            results.add(item);
                            mapPhoneNumbers.put(number, number);
                            if (count <= 100) {
                                count++;
                                if (check) {
                                    sb.append(",").append(number);
                                } else {
                                    check = true;
                                    sb.append(number);
                                }
                            }
                        }
                    }
                }
                phoneNumbers = sb.toString();
                listContacts.clear();
                mapPhoneNumbers.clear();
            }
        }
        return results;
    }
}
package com.metfone.selfcare.module.home_kh.tab.adapter.banner;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.home_kh.tab.model.IHomeModelType;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeMovieItem;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;

import butterknife.BindView;

public class KhHomeBannerAdapter extends BaseAdapter<BaseAdapter.ViewHolder, KhHomeMovieItem> {
    private OnClickSliderBanner listener;

    public KhHomeBannerAdapter(Activity act, OnClickSliderBanner listener) {
        super(act);
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {

        return IHomeModelType.BANNER;
    }

    @NonNull
    @Override
    public BaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case IHomeModelType.BANNER:
                return new KhSlideBannerDetailHolder(layoutInflater.inflate(R.layout.home_banner, parent, false), listener);
            default:
                return new EmptyHolder(layoutInflater, parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapter.ViewHolder holder, int position) {
        holder.bindData(getItem(position), position);
    }


    public class KhSlideBannerDetailHolder extends BaseAdapter.ViewHolder {

        @BindView(R.id.card_view)
        View viewRoot;

        @BindView(R.id.img_movie)
        @Nullable
        ImageView ivCover;

        @BindView(R.id.tv_title)
        @Nullable
        TextView tvTitle;

        @BindView(R.id.txt_type)
        @Nullable
        TextView txt_type;

        @BindView(R.id.btn_play)
        ImageView btnPlay;

        private Object data;

        public KhSlideBannerDetailHolder(View view, final OnClickSliderBanner listener) {
            super(view);
            if (viewRoot != null) viewRoot.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null && data != null) {
                        listener.onClickSliderBannerItem(data, getAdapterPosition());
                    }
                }
            });
        }

        @Override
        public void bindData(Object item, int position) {
            data = item;
            KhHomeMovieItem provisional = (KhHomeMovieItem) item;
            if (tvTitle != null) tvTitle.setText(provisional.name);

            String url = (provisional.image_path == null || provisional.image_path.isEmpty()) ? provisional.poster_path : provisional.image_path;
            ImageBusiness.setBannerHome(url, ivCover);
            txt_type.setVisibility(View.GONE);
            btnPlay.setOnClickListener(v -> listener.onClickSliderBannerItem(item, position));
        }

    }

}

package com.metfone.selfcare.module.tab_home.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.text.TextUtils;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.FeedbackActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.MessageBusiness;
import com.metfone.selfcare.business.ReengAccountBusiness;
import com.metfone.selfcare.common.api.LogApi;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.database.model.MoreItemDeepLink;
import com.metfone.selfcare.database.model.ThreadMessage;
import com.metfone.selfcare.helper.AccumulatePointHelper;
import com.metfone.selfcare.helper.ComparatorHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.LuckyWheelHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.model.setting.ConfigTabHomeItem;
import com.metfone.selfcare.module.ModuleActivity;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.myviettel.activity.NonViettelActivity;
import com.metfone.selfcare.module.myviettel.utils.MyViettelUtils;
import com.metfone.selfcare.module.sc_umoney.main_umoney.TabUmoneyActivity;
import com.metfone.selfcare.module.tab_home.event.TabHomeEvent;
import com.metfone.selfcare.module.tab_home.model.HomeContact;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogMessage;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.CALLOUT;
import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.INVITE;
import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.NEAR_FRIEND;
import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.SPOINT;
import static com.metfone.selfcare.helper.DeepLinkHelper.DEEPLINK_INTRO_ANONYMOUS.VQMM;

public class TabHomeUtils {
    private static final int NUM_MAX_SHOW_VIEW_ALL = 10;
    private static final String TAG = "TabHomeUtils";
    private static final int MARGIN_DEFAULT = Utilities.dpToPx(10);

    public static ArrayList<HomeContact> getListQuickContacts() {
        long beginTime = System.currentTimeMillis();
        ArrayList<ThreadMessage> list = new ArrayList<>();
        if (!ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            try {
                MessageBusiness messageBusiness = ApplicationController.self().getMessageBusiness();
                if (Utilities.notEmpty(messageBusiness.getThreadMessageArrayList())) {
                    ArrayList<ThreadMessage> listPins = new ArrayList<>();
                    for (ThreadMessage threadMessage : messageBusiness.getThreadMessageArrayList()) {
                        if (threadMessage.getHiddenThread() != 1) {
                            if (threadMessage.getLastTimePinThread() != 0 &&
                                    (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                                            || threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
                                    && threadMessage.isJoined()) {
                                listPins.add(threadMessage);
                            } else if (threadMessage.isReadyShow(messageBusiness) &&
                                    (threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_PERSON_CHAT
                                            || threadMessage.getThreadType() == ThreadMessageConstant.TYPE_THREAD_GROUP_CHAT)
                                    && threadMessage.isJoined()) {
                                list.add(threadMessage);
                            }
                        }
                    }
                    if (!list.isEmpty()) {
                        Collections.sort(list, ComparatorHelper.getComparatorThreadMessageByLastTime());
                    }
                    if (!listPins.isEmpty()) {
                        Collections.sort(listPins, ComparatorHelper.getComparatorThreadMessageByLastTimePin());
                        for (int i = 0; i < listPins.size(); i++) {
                            if (i == listPins.size() - 1)
                                listPins.get(i).setLastPin(true);
                            else
                                listPins.get(i).setLastPin(false);
                        }
                        list.addAll(0, listPins);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e);
            }
        }
        int size = Math.min(list.size(), NUM_MAX_SHOW_VIEW_ALL);
        ArrayList<HomeContact> contacts = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            contacts.add(new HomeContact(list.get(i)));
        }
//        if (list.size() >= NUM_MAX_SHOW_VIEW_ALL) {
//            HomeContact tmp = new HomeContact();
//            tmp.setViewAll(true);
//            contacts.add(tmp);
//        }
        Log.i(TAG, "[] getListQuickContacts take " + (System.currentTimeMillis() - beginTime) + " ms");
        return contacts;
    }

    public static synchronized ArrayList<ItemMoreHome> getListNotActiveTabs(Context context) {
        ArrayList<ItemMoreHome> list = new ArrayList<>();
        CopyOnWriteArrayList<ConfigTabHomeItem> listNotActive = TabHomeHelper.getInstant(ApplicationController.self()).getListNotActiveItem();
        if (Utilities.notEmpty(listNotActive)) {
            for (ConfigTabHomeItem item : listNotActive) {
                ItemMoreHome itemMoreHome;
                if (item.getHomeTab() == TabHomeHelper.HomeTab.tab_wap) {
                    itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_WAPVIEW
                            , item.getName(), R.drawable.ic_home_more_default, item.getImgMore());
                    itemMoreHome.setDeepLink(item.getId());
                    itemMoreHome.setFuncName(LogApi.FuncName.FUNC_WAP.VALUE);
                } else {
                    itemMoreHome = TabHomeHelper.getFunctionTabHome(context, item);
                }
                if (itemMoreHome != null) list.add(itemMoreHome);
            }
        }
        return list;
    }

    public static synchronized ArrayList<ItemMoreHome> getListFunctions() {
        ArrayList<ItemMoreHome> list = new ArrayList<>();
        ApplicationController mApplication = ApplicationController.self();
        ReengAccountBusiness mAccountBusiness = mApplication.getReengAccountBusiness();
        Resources mRes = mApplication.getResources();
        ContentConfigBusiness configBusiness = mApplication.getConfigBusiness();

        ArrayList<MoreItemDeepLink> listItemDeeplink = configBusiness.getListMoreItemDeepLink();
        if (listItemDeeplink != null && !listItemDeeplink.isEmpty()) {
            for (MoreItemDeepLink itemDeepLink : listItemDeeplink) {
                if (itemDeepLink != null) {
                    ItemMoreHome deeplink = new ItemMoreHome(Constants.HOME.FUNCTION.DEEPLINK
                            , itemDeepLink.getTitle(), R.drawable.ic_home_more_default, itemDeepLink.getIcon());
                    deeplink.setDeepLink(itemDeepLink.getDeepLink());
                    deeplink.setFuncName(LogApi.FuncName.FUNC_DEEPLINK.VALUE);
                    list.add(deeplink);
                }
            }
        }

        if (mApplication.getConfigBusiness().isEnableSpoint()) {
            ItemMoreHome spoint = new ItemMoreHome(Constants.HOME.FUNCTION.SPOINT
                    , mRes.getString(R.string.tab_more_point), R.drawable.ic_func_spoint, null);
            spoint.setFuncName(LogApi.FuncName.FUNC_SPOINT.VALUE);
            list.add(spoint);
        }

        if (mApplication.getConfigBusiness().isEnableSaving() && mApplication.getReengAccountBusiness().isOperatorViettel()) {
            String privilege = mApplication.getPref().getString(Constants.PREFERENCE.PREF_MONEY_SAVING, "");
            if (TextUtils.isEmpty(privilege)) {
                privilege = mRes.getString(R.string.privilege);
            }
            ItemMoreHome saving = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_SAVING_STATISTICS
                    , privilege, R.drawable.ic_more_saving, null);
            saving.setFuncName(LogApi.FuncName.FUNC_PRIVILEGE.VALUE);
            list.add(saving);
        }

        /*if (mAccountBusiness.isAvnoEnable() && !mAccountBusiness.isViettel()) {
            String title;
            if (TextUtils.isEmpty(mAccountBusiness.getAVNONumber())) {
                if (mAccountBusiness.isVietnam())
                    title = mRes.getString(R.string.avno_title);
                else
                    title = mRes.getString(R.string.avno_title_not_vn);
            } else {
                title = mRes.getString(R.string.avno_manager_title);
            }
            ItemMoreHome avno = new ItemMoreHome(Constants.HOME.FUNCTION.AVNO
                    , title, R.drawable.ic_more_avno, null);
            list.add(avno);
        }*/

        if (mApplication.getConfigBusiness().isEnableLuckyWheel()) {
            ItemMoreHome lucky = new ItemMoreHome(Constants.HOME.FUNCTION.LUCKY_WHEEL
                    , mRes.getString(R.string.lucky_wheel_title), R.drawable.ic_func_lucky_wheel_v2, null);
            lucky.setFuncName(LogApi.FuncName.FUNC_LUCKY_WHEEL.VALUE);
            list.add(lucky);
        }

        //callOut
        if (mApplication.getReengAccountBusiness().isEnableCallOut()) {
            ItemMoreHome callOut = new ItemMoreHome(Constants.HOME.FUNCTION.CALLOUT
                    , mRes.getString(R.string.title_mocha_callout), R.drawable.ic_func_call_out, null);
            callOut.setFuncName(LogApi.FuncName.FUNC_CALL_OUT.VALUE);
            list.add(callOut);
        }
        if (mApplication.getConfigBusiness().isEnableTabUMoney() && mAccountBusiness.isLaos()) {
            //todo login vs so Lao va chua log voi location la Lao
            ItemMoreHome itemDataPackage = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_UMONEY, mRes.getString(R.string.umoney), R.drawable.ic_sc_umoney, null);
            list.add(itemDataPackage);
        }

        if (mApplication.getConfigBusiness().isEnableQR()) {
            ItemMoreHome qr = new ItemMoreHome(Constants.HOME.FUNCTION.QR
                    , mRes.getString(R.string.qr_scan_title), R.drawable.ic_func_scan_qr, null);
            qr.setFuncName(LogApi.FuncName.FUNC_QR_CODE.VALUE);
            list.add(qr);
        }

        if (mApplication.getConfigBusiness().isEnableListGame()) {
            ItemMoreHome game = new ItemMoreHome(Constants.HOME.FUNCTION.GAME
                    , mRes.getString(R.string.list_game), R.drawable.ic_func_game, null);
            game.setFuncName(LogApi.FuncName.FUNC_GAME.VALUE);
            list.add(game);
        }

        ItemMoreHome friendNear = new ItemMoreHome(Constants.HOME.FUNCTION.NEAR_FRIEND
                , mRes.getString(R.string.menu_near_you), R.drawable.ic_func_find_friend, null);
        friendNear.setFuncName(LogApi.FuncName.FUNC_FIND_FRIEND.VALUE);
        list.add(friendNear);

//        ItemMoreHome feedBack = new ItemMoreHome(Constants.HOME.FUNCTION.FEED_BACK,
//                mRes.getString(R.string.feedback_title), R.drawable.ic_more_feedback, null);
//        list.add(feedBack);

        ItemMoreHome invite = new ItemMoreHome(Constants.HOME.FUNCTION.INVITE_FRIEND,
                mRes.getString(R.string.invite), R.drawable.ic_func_invite, null);
        invite.setFuncName(LogApi.FuncName.FUNC_INVITE_FRIEND.VALUE);
        list.add(invite);

        if (mApplication.getConfigBusiness().isEnableTabMyViettel() && mAccountBusiness.isVietnam()) {
            ItemMoreHome itemMoreHome = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_MY_VIETTEL
                    , mRes.getString(R.string.tab_my_viettel), R.drawable.ic_my_viettel_v5, null);
            itemMoreHome.setFuncName(LogApi.FuncName.FUNC_MY_VIETTEL.VALUE);
            list.add(itemMoreHome);

        }

        if (mApplication.getConfigBusiness().isEnableDataChallenge() && mAccountBusiness.isVietnam()
                && !mAccountBusiness.isAnonymousLogin()) {
            ItemMoreHome itemDataChallenge = new ItemMoreHome(Constants.HOME.FUNCTION.TAB_DATA_CHALLENGE
                    , mRes.getString(R.string.data_challenge), R.drawable.ic_more_data_challenge, null);
            itemDataChallenge.setFuncName(LogApi.FuncName.FUNC_DATA_CHALLENGE.VALUE);
            list.add(itemDataChallenge);
        }

        if (mAccountBusiness.isVietnam() && mAccountBusiness.isViettel()) {
            String title = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FUNCTION_DATA_PACKAGES_TITLE);
            if (TextUtils.isEmpty(title))
                title = mApplication.getResources().getString(R.string.title_list_data_packages);
            ItemMoreHome itemDataPackage = new ItemMoreHome(Constants.HOME.FUNCTION.LIST_DATA_PACKAGES, title, R.drawable.ic_func_package, null);
            itemDataPackage.setFuncName(LogApi.FuncName.FUNC_DATA_PACKAGE.VALUE);
            list.add(itemDataPackage);
        }

        return list;
    }

    private static void handleRegisterVip(BaseSlidingFragmentActivity activity, String id) {
        ApplicationController mApplication = ApplicationController.self();
        if (!TextUtils.isEmpty(id)) {
            ReportHelper.checkShowConfirmOrRequestFakeMo(mApplication, activity, mApplication.getConfigBusiness().getSubscriptionConfig().getReconfirm(), id, "home_menu");
        }
    }

    private static void confirmRegisterVip(final BaseSlidingFragmentActivity activity) {
        final ApplicationController mApplication = ApplicationController.self();
        Resources mRes = mApplication.getResources();
        String labelOK = mRes.getString(R.string.register);
        String labelCancel = mRes.getString(R.string.cancel);
        String title = mApplication.getConfigBusiness().getSubscriptionConfig().getTitle();
        String msg = mApplication.getConfigBusiness().getSubscriptionConfig().getConfirm();
        PopupHelper.getInstance().showDialogConfirm(activity, title,
                msg, labelOK, labelCancel, (view, entry, menuId) -> {
                    if (menuId == Constants.MENU.POPUP_CONFIRM_REGISTER_VIP) {
                        handleRegisterVip(activity, mApplication.getConfigBusiness().getSubscriptionConfig().getCmd());
                    }
                }, null, Constants.MENU.POPUP_CONFIRM_REGISTER_VIP, false);
    }

    public static void openFeature(BaseSlidingFragmentActivity activity, ItemMoreHome item) {
        if (activity == null || item == null) return;
        ApplicationController mApplication = ApplicationController.self();
        int function = item.getFunction();
        switch (function) {
            case Constants.HOME.FUNCTION.DEEPLINK: {
                DeepLinkHelper.getInstance().openSchemaLink(activity, item.getDeepLink());
            }
            break;

            case Constants.HOME.FUNCTION.CALLOUT: {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, CALLOUT);
                } else
                    NavigateActivityHelper.navigateToAVNOManager(activity);
            }
            break;

            case Constants.HOME.FUNCTION.QR: {
                if (PermissionHelper.allowedPermission(activity, Manifest.permission.CAMERA)) {
                    NavigateActivityHelper.navigateToQRScan(activity);
                } else {
                    PermissionHelper.requestPermissionWithGuide(activity, Manifest.permission.CAMERA, Constants.PERMISSION.PERMISSION_REQUEST_TAKE_PHOTO);
                }
            }
            break;

            case Constants.HOME.FUNCTION.INVITE_FRIEND: {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, INVITE);
                } else
                    NavigateActivityHelper.navigateToInviteFriends(activity, Constants.CHOOSE_CONTACT
                            .TYPE_INVITE_FRIEND);
            }
            break;

            case Constants.HOME.FUNCTION.LUCKY_WHEEL: {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, VQMM);
                } else
                    LuckyWheelHelper.getInstance(mApplication).navigateToLuckyWheel(activity);
            }
            break;

            case Constants.HOME.FUNCTION.SPOINT: {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, SPOINT);
                } else
                    AccumulatePointHelper.getInstance(mApplication).navigateToTotalPoint(activity);
            }
            break;

            case Constants.HOME.FUNCTION.GAME: {
                NavigateActivityHelper.navigateToMoreApps(activity);
            }
            break;

            case Constants.HOME.FUNCTION.NEAR_FRIEND: {
                if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                    DeepLinkHelper.getInstance().openSchemaLink(activity, NEAR_FRIEND);
                } else {
                    if (PermissionHelper.allowedPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) && PermissionHelper.allowedPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                        NavigateActivityHelper.navigateToSettingActivity(activity, Constants.SETTINGS.NEAR_YOU);
                    } else {
                        if (PermissionHelper.declinedPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            PermissionHelper.requestPermissionWithGuide(activity,
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
                        }

                        if (PermissionHelper.declinedPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            PermissionHelper.requestPermissionWithGuide(activity,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
                        }

//                        PermissionHelper.requestPermissionWithGuide(activity,
//                                Manifest.permission.ACCESS_COARSE_LOCATION,
//                                Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
                    }
                }
            }
            break;

            case Constants.HOME.FUNCTION.FEED_BACK: {
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    activity.showDialogLogin();
                } else
                    FeedbackActivity.start(activity);
            }
            break;

            case Constants.HOME.FUNCTION.REGISTER_VIP: {
                confirmRegisterVip(activity);
            }
            break;

            case Constants.HOME.FUNCTION.PERSONAL: {
                NavigateActivityHelper.navigateToSettingActivity(activity, Constants.SETTINGS.SETTING_CONFIG_TAG);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_VIDEO: {
                openTabVideo(activity);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_STRANGER:
                openHomeTab(activity, TabHomeHelper.HomeTab.tab_stranger);
                break;

            case Constants.HOME.FUNCTION.TAB_HOT:
                openHomeTab(activity, TabHomeHelper.HomeTab.tab_hot);
                break;

            case Constants.HOME.FUNCTION.TAB_MOVIE: {
                openTabMovie(activity);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_MUSIC: {
                openTabMusic(activity);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_NEWS: {
                openTabNews(activity);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_SECURITY:
                openHomeTab(activity, TabHomeHelper.HomeTab.tab_security);
                break;

            case Constants.HOME.FUNCTION.TAB_SAVING_STATISTICS: {
                NavigateActivityHelper.navigateToTabSaving(activity);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_WAPVIEW:
                if(item.getDeepLink().equals("1") && item.getTitle().equalsIgnoreCase("Comic")){
                    mApplication.logEventFacebookSDKAndFirebase(activity.getString(R.string.c_comic));
                }else if(item.getDeepLink().equals("2") && item.getTitle().equalsIgnoreCase("Horasas")){
                    mApplication.logEventFacebookSDKAndFirebase(activity.getString(R.string.c_xemboi));
                }
                ModuleActivity.startActivityWap(activity, item.getDeepLink());
                break;

            case Constants.HOME.FUNCTION.LIST_DATA_PACKAGES: {
                String title = mApplication.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.FUNCTION_DATA_PACKAGES_TITLE);
                if (TextUtils.isEmpty(title))
                    title = mApplication.getResources().getString(R.string.title_list_data_packages);
                if (!title.equals(item.getTitle())) {
                    EventBus.getDefault().postSticky(new TabHomeEvent().setUpdateFeature(true));
                }
                NavigateActivityHelper.navigateToListDataPackages(activity);
            }
            break;

            case Constants.HOME.FUNCTION.TAB_TIIN:
                AppProvider.isLoadHome = false;
                SharedPrefs.getInstance().put(ConstantTiin.KEY_CREATE_HOME, false);
                activity.trackingEvent(R.string.click_tab_tiin, R.string.action_tab_tiin, "");
                openHomeTab(activity, TabHomeHelper.HomeTab.tab_tiins);
                break;

            case Constants.HOME.FUNCTION.TAB_MY_VIETTEL:
                openHomeTab(activity, TabHomeHelper.HomeTab.tab_my_viettel);
                break;

            case Constants.HOME.FUNCTION.TAB_SELFCARE:
                openHomeTab(activity, TabHomeHelper.HomeTab.tab_selfcare);
                break;

            case Constants.HOME.FUNCTION.TAB_DATA_CHALLENGE:
                openDataChallenge(activity);
                break;
            case Constants.HOME.FUNCTION.TAB_UMONEY:
                //todo open umoney
                openTabUmoney(activity);
                break;
//            case Constants.HOME.FUNCTION.TAB_ESPORT:
//                openHomeTab(activity, TabHomeHelper.HomeTab.tab_esport);
//                break;
        }
    }

    private static void openHomeTab(BaseSlidingFragmentActivity activity, TabHomeHelper.HomeTab homeTab) {
        openHomeTab(activity, homeTab, null);
    }

    private static void openHomeTab(BaseSlidingFragmentActivity activity, TabHomeHelper.HomeTab homeTab, String idTabWap) {
        if (activity == null || homeTab == null) return;
        ApplicationController application = ApplicationController.self();
        if (TabHomeHelper.isHomeTabEnable(homeTab, application)) {
//            if (TabHomeHelper.getInstant(application).isTabActive(homeTab, idTabWap))
//                DeepLinkHelper.getInstance().navigateToHomeTab(activity, homeTab, idTabWap);
//            else {
//                DeepLinkHelper.getInstance().navigateToModuleActivity(activity, homeTab, idTabWap);
//            }
        } else {
            activity.showToast(R.string.e666_not_support_function);
        }
    }

    public static void openTabVideo(BaseSlidingFragmentActivity activity) {
        openHomeTab(activity, TabHomeHelper.HomeTab.tab_video);
    }

    public static void openTabNews(BaseSlidingFragmentActivity activity) {
        activity.trackingEvent(R.string.click_tab_new, R.string.action_tab_new, "");
        openHomeTab(activity, TabHomeHelper.HomeTab.tab_news);
    }

    public static void openTabMovie(BaseSlidingFragmentActivity activity) {
        openHomeTab(activity, TabHomeHelper.HomeTab.tab_movie);
    }

    public static void openTabMusic(BaseSlidingFragmentActivity activity) {
        openHomeTab(activity, TabHomeHelper.HomeTab.tab_music);
    }

    public static void openMusicKeeng(BaseSlidingFragmentActivity activity, AllModel item) {
        if (activity == null || item == null)
            return;
        switch (item.getType()) {
            case Constants.TYPE_SONG: {
                activity.setMediaToPlaySong(item);
            }
            break;
            case Constants.TYPE_ALBUM: {
                activity.gotoAlbumDetail(item);
            }
            break;
            case Constants.TYPE_VIDEO: {
                activity.setMediaToPlayVideo(item);
            }
            break;
            case Constants.TYPE_PLAYLIST: {
                activity.gotoPlaylistDetail(ConvertHelper.convertToPlaylist(item));
            }
            break;
            default:
                Utilities.processOpenLink(ApplicationController.self(), activity, item.getUrl());
                break;
        }
    }

    public static String getShortName(String username) {
        if (!TextUtils.isEmpty(username)) {
            String[] tmp = username.split(" ");
            for (int i = tmp.length - 1; i >= 0; i--) {
                if (!TextUtils.isEmpty(tmp[i])) return tmp[i];
            }
        }
        return username;
    }

    public static void openTabWap(BaseSlidingFragmentActivity activity, String idTabWap) {
        openHomeTab(activity, TabHomeHelper.HomeTab.tab_wap, idTabWap);
    }

    public static void openDataChallenge(final BaseSlidingFragmentActivity activity) {
        if (activity == null) return;
        ApplicationController application = ApplicationController.self();
        final ReengAccountBusiness reengAccountBusiness = application.getReengAccountBusiness();
        if (reengAccountBusiness.isAnonymousLogin()) {
            activity.showDialogLogin();
        } else {
            if (reengAccountBusiness.isVietnam() && application.getConfigBusiness().isEnableDataChallenge()) {
                final String linkHelp = application.getConfigBusiness().getContentConfigByKey(Constants.PREFERENCE.CONFIG.MY_VIETTEL_DATA_CHALLENGE_RULE);
                boolean isShowDialog = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_SHOW_DIALOG_DATA_CHALLENGE, Boolean.class, false);
                if (isShowDialog) {
                    if (reengAccountBusiness.isViettel()) {
                        MyViettelUtils.registerDataChallenge(activity);
                    } else {
                        Intent intent = new Intent(activity, NonViettelActivity.class);
                        intent.putExtra(Constants.KEY_TITLE, activity.getString(R.string.data_challenge_title));
                        intent.putExtra(Constants.KEY_POSITION, linkHelp);
                        activity.startActivity(intent);
                    }
                } else {
                    DialogMessage dialog = new DialogMessage(activity, true);
                    dialog.setUseHtml(true);
                    dialog.setLabel(activity.getString(R.string.data_challenge_title));
                    dialog.setMessage(activity.getString(R.string.dialog_data_challenge_msg));
                    dialog.setLabelButton(activity.getString(R.string.dialog_data_challenge_label));
                    dialog.setNegativeListener(result -> {
                        SharedPrefs.getInstance().put(Constants.PREFERENCE.PREF_SHOW_DIALOG_DATA_CHALLENGE, true);
                        if (reengAccountBusiness.isViettel()) {
                            MyViettelUtils.registerDataChallenge(activity);
                        } else {
                            Intent intent = new Intent(activity, NonViettelActivity.class);
                            intent.putExtra(Constants.KEY_TITLE, activity.getString(R.string.data_challenge_title));
                            intent.putExtra(Constants.KEY_POSITION, linkHelp);
                            activity.startActivity(intent);
                        }
                    });
                    dialog.show();
                }
            } else {
                activity.showToast(R.string.e666_not_support_function);
            }
        }
    }

    public static void openTabTiin(BaseSlidingFragmentActivity activity) {
        activity.trackingEvent(R.string.click_tab_tiin, R.string.action_tab_tiin, "");
        openHomeTab(activity, TabHomeHelper.HomeTab.tab_tiins);
    }

    public static int getWidthPosterMovie() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 3) / 2.3);
    }

    public static int getWidthMovieWatched() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 1.6);
    }

    public static int getWidthVideo() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 1.6);
    }

    public static int getWidthMusicPlaylist() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 3) / 3.3);
    }

    public static int getWidthKhHomeLiveChannel() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 3) / 1.2);
    }

    public static int getWidthKhHomeTopic() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 4) / 3.2);
    }

    public static int getWidthKhHomeRewardCate() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT) / 5);
    }

    public static int getWidthKhHomeRewardTopic() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 3) / 1.2);
    }

    public static int getWidthChannelVideo() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 5) / 4.8);
    }

    public static int getWidthQuickContact() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 5) / 4.8);
    }

    public static int getWidthFeature() {
        return (int) ((ApplicationController.self().getWidthPixels()) / 4.5);
    }

    public static int getWidthNews() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 1.6);
    }

    public static int getWidthComic() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 1.6);
    }

    public static int getWidthTiin() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT * 2) / 1.6);
    }

    public static int getWidthEventNews() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT) / 3);
    }

    public static int getWidthSpecialTiin() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT) / 1.725);
    }

    public static int getWidthNormalTiin() {
        return (int) ((ApplicationController.self().getWidthPixels() - MARGIN_DEFAULT) / 3);
    }

    public static void openTabUmoney(BaseSlidingFragmentActivity activity) {
        if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setMessage(activity.getResources().getString(R.string.u_Please_login_to_using_Umoney));
            dialogConfirm.setNegativeLabel("Ok");
            dialogConfirm.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    activity.showDialogLogin();
                }
            });
            dialogConfirm.show();

        } else {
            if (!ApplicationController.self().getReengAccountBusiness().isViettel()) {
                DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
                dialogConfirm.setMessage(activity.getResources().getString(R.string.u_Umoney_feature_do_not_support));
                dialogConfirm.setNegativeLabel("Ok");
                dialogConfirm.setNegativeListener(new NegativeListener() {
                    @Override
                    public void onNegative(Object result) {
                    }
                });
                dialogConfirm.show();
            } else {
                activity.startActivity(new Intent(activity, TabUmoneyActivity.class));
            }
        }
    }
}

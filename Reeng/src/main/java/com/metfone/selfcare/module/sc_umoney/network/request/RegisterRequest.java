package com.metfone.selfcare.module.sc_umoney.network.request;

import com.metfone.selfcare.module.sc_umoney.network.model.Info;

import java.io.Serializable;

public class RegisterRequest implements Serializable {
    private int serviceCode;
    private String name;
    private String birthday;
    private int gender;
    private String paperType;
    private String paperNum;
    private String transDesc;

    public int getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(int serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getPaperType() {
        return paperType;
    }

    public void setPaperType(String paperType) {
        this.paperType = paperType;
    }

    public String getPaperNum() {
        return paperNum;
    }

    public void setPaperNum(String paperNum) {
        this.paperNum = paperNum;
    }

    public String getTransDesc() {
        return transDesc;
    }

    public void setTransDesc(Info info) {
        //cusId, subId, cusName, subType, birthday, Gender, serviceType, tier == standard
        if (info != null) {
            try {
                this.transDesc = info.getCustID() + "|" + info.getSubId() + "|" + info.getCustomerName() + "|" + info.getSubType() + "|" + info.getBirthDate() + "|" + gender + "|" + info.getServiceType() + "|" + info.getStandard();
//                this.transDesc= URLEncoder.encode( url, "UTF-8" );
//                this.transDesc = URLDecoder.decode(url, StandardCharsets.UTF_8.toString());
//                this.transDesc = url;
            } catch (Exception e) {
            }
        }
    }
}

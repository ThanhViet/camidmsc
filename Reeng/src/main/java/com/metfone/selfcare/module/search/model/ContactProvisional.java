/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/13
 *
 */

package com.metfone.selfcare.module.search.model;

import android.text.Spannable;

import com.metfone.selfcare.database.model.PhoneNumber;
import com.metfone.selfcare.database.model.ThreadMessage;

import java.util.Random;

public class ContactProvisional {
    public static final int SEND = 1;
    public static final int UNDO = 2;
    public static final int SENT = 3;
    public static final int RETRY = 4;
    public static final int OPEN_CHAT = 5;

    private int state;
    private String title;
    private String description;
    private Spannable titleSpannable;
    private Spannable descriptionSpannable;
    private Object contact;
    private String key;

    public ContactProvisional() {
        key = System.nanoTime() + "" + (new Random()).nextInt(10);
    }

    public Object getContact() {
        return contact;
    }

    public void setContact(Object contact) {
        this.contact = contact;
    }

    public void setContact(ThreadMessage contact) {
        this.contact = contact;
    }

    public void setContact(PhoneNumber contact) {
        this.contact = contact;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Spannable getTitleSpannable() {
        return titleSpannable;
    }

    public void setTitleSpannable(Spannable titleSpannable) {
        this.titleSpannable = titleSpannable;
    }

    public Spannable getDescriptionSpannable() {
        return descriptionSpannable;
    }

    public void setDescriptionSpannable(Spannable descriptionSpannable) {
        this.descriptionSpannable = descriptionSpannable;
    }

    public String getName() {
        if (contact instanceof ThreadMessage) return ((ThreadMessage) contact).getThreadName();
        if (contact instanceof PhoneNumber) return ((PhoneNumber) contact).getName();
        return "";
    }

    public String getPhoneNumber() {
        if (contact instanceof ThreadMessage) return ((ThreadMessage) contact).getSoloNumber();
        if (contact instanceof PhoneNumber) return ((PhoneNumber) contact).getJidNumber();
        return "";
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "ContactProvisional{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", titleSpannable=" + titleSpannable +
                ", descriptionSpannable=" + descriptionSpannable +
                ", contact=" + contact +
                '}';
    }
}

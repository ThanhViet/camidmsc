package com.metfone.selfcare.module.selfcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SCTelecomRewardModel implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("imageUrl")
    @Expose
    private Object imageUrl;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private Object endTime;
    @SerializedName("createdTime")
    @Expose
    private String createdTime;
    @SerializedName("isTelcoResource")
    @Expose
    private Boolean isTelcoResource;
    @SerializedName("telcoResourceType")
    @Expose
    private String telcoResourceType;
    @SerializedName("telcoResourceOcsId")
    @Expose
    private Integer telcoResourceOcsId;
    @SerializedName("telcoResourceAmount")
    @Expose
    private Integer telcoResourceAmount;
    @SerializedName("costs")
    @Expose
    private List<SCTelecomCostModel> costs = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Object imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Object getEndTime() {
        return endTime;
    }

    public void setEndTime(Object endTime) {
        this.endTime = endTime;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Boolean getIsTelcoResource() {
        return isTelcoResource;
    }

    public void setIsTelcoResource(Boolean isTelcoResource) {
        this.isTelcoResource = isTelcoResource;
    }

    public String getTelcoResourceType() {
        return telcoResourceType;
    }

    public void setTelcoResourceType(String telcoResourceType) {
        this.telcoResourceType = telcoResourceType;
    }

    public Integer getTelcoResourceOcsId() {
        return telcoResourceOcsId;
    }

    public void setTelcoResourceOcsId(Integer telcoResourceOcsId) {
        this.telcoResourceOcsId = telcoResourceOcsId;
    }

    public Integer getTelcoResourceAmount() {
        return telcoResourceAmount;
    }

    public void setTelcoResourceAmount(Integer telcoResourceAmount) {
        this.telcoResourceAmount = telcoResourceAmount;
    }

    public List<SCTelecomCostModel> getCosts() {
        return costs;
    }

    public void setCosts(List<SCTelecomCostModel> costs) {
        this.costs = costs;
    }

    @Override
    public String toString() {
        return "SCTelecomRewardModel{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", imageUrl=" + imageUrl +
                ", description=" + description +
                ", startTime='" + startTime + '\'' +
                ", endTime=" + endTime +
                ", createdTime='" + createdTime + '\'' +
                ", isTelcoResource=" + isTelcoResource +
                ", telcoResourceType='" + telcoResourceType + '\'' +
                ", telcoResourceOcsId=" + telcoResourceOcsId +
                ", telcoResourceAmount=" + telcoResourceAmount +
                ", costs=" + costs +
                '}';
    }
}

package com.metfone.selfcare.module.metfoneplus.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.CamIdUserBusiness;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.BaseMPSuccessDialog;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogOtpFragment;
import com.metfone.selfcare.module.metfoneplus.search.model.AvailableNumber;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsDoActionServiceResponse;
import com.metfone.selfcare.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class MPApplyForYouCurrentSimFragment extends MPBaseFragment {
    private static final String NUMBER_KEY = "number";
    private static final String ORDER_TYPE = "order_type";
    private static final String DURATION_KEY = "duration";
    private AvailableNumber availableNumber;
    private int orderType;
    private int duration;
    @BindView(R.id.action_bar_title)
    TextView action_bar_title;
    @BindView(R.id.img_sim)
    TextView img_sim;
    @BindView(R.id.txtTitleCurrentSim)
    TextView txtTitleCurrentSim;
    BaseMPSuccessDialog baseMPSuccessDialog;
    public static CamIdUserBusiness mCamIdUserBusiness = null;
    private static final int ID_CONFIRMATION_DIALOG = 0;
    MPDialogOtpFragment dialogSuccess;
    private boolean isOTPDialogShow = false;
    public MPApplyForYouCurrentSimFragment() {
        // Required empty public constructor
    }
    public static MPApplyForYouCurrentSimFragment newInstance(AvailableNumber phone, int OrderType, int commitDuration) {
        Bundle args = new Bundle();
        MPApplyForYouCurrentSimFragment fragment = new MPApplyForYouCurrentSimFragment();
        args.putSerializable(NUMBER_KEY, phone);
        args.putInt(ORDER_TYPE, OrderType);
        args.putInt(DURATION_KEY, commitDuration);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        if (dialogSuccess != null && !dialogSuccess.isVisible()) {
            isOTPDialogShow = true;
        }
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCamIdUserBusiness = ApplicationController.self().getCamIdUserBusiness();
        if (getArguments() != null) {
            availableNumber = (AvailableNumber) getArguments().getSerializable(NUMBER_KEY);
            orderType = (Integer) getArguments().getInt(ORDER_TYPE);
            duration = (Integer) getArguments().getInt(DURATION_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_apply_for_you_current_sim, container, false);
        Utilities.adaptViewForInserts(view);
        mUnbinder = ButterKnife.bind(this, view);
        action_bar_title.setText(R.string.apply_for_current_sim);
        img_sim.setText(0 + mCamIdUserBusiness.getMetfoneUsernameIsdn().substring(0,2).concat(" ").concat(mCamIdUserBusiness.getMetfoneUsernameIsdn().substring(2)) );
        if (orderType > 0) {
            txtTitleCurrentSim.setText(getString(R.string.your_phone_number));
        } else {
            txtTitleCurrentSim.setText(getString(R.string.m_p_you_current_phone));
        }
        return view;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_apply_for_you_current_sim;
    }

    @OnClick(R.id.buy_services_detail_title_register)
    void onRegisterService() {
        showConfirmDialog();
    }

    @OnClick(R.id.action_bar_back)
    public void onBack() {
        popBackStackFragment();
    }

    private void showConfirmDialog() {
        dialogSuccess = new MPDialogOtpFragment();
      //  dialogSuccess.show(MPApplyForYouCurrentSimFragment.this , ID_CONFIRMATION_DIALOG);
        dialogSuccess.setPhone(availableNumber.getIsdn());
        dialogSuccess.setIsOTPDialogShow(isOTPDialogShow);
        dialogSuccess.addButtonOnClickListener(new MPDialogOtpFragment.ButtonOnClickListener() {
            @Override
            public void onDialogDismiss(String errorMes) {
                dialogSuccess.dismiss();
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), errorMes, false);
                baseMPSuccessDialog.show();
            }
            @Override
            public void onCheckOtpSuccess(String otp) {
                dialogSuccess.dismiss();
                gotoMPApplyingSimFragment(availableNumber, otp, orderType, duration);
            }
        });
        dialogSuccess.show(getChildFragmentManager(), null);
    }


//    @Override
//    public Dialog getDialog(int dialogId) {
//        switch (dialogId) {
//            case ID_CONFIRMATION_DIALOG:
//                return MPDialogOtpFragment.newInstance(null).getDialog(); //Your AlertDialog
//            default:
//                throw new IllegalArgumentException("Unknown dialog id: " + dialogId);
//        }
//    }

    @Override
    public void onPause() {
        if (dialogSuccess != null && !dialogSuccess.isVisible()) {
            isOTPDialogShow = true;
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (isOTPDialogShow) {
            dialogSuccess.setIsOTPDialogShow(isOTPDialogShow);
            dialogSuccess.show(getChildFragmentManager(), null);
            isOTPDialogShow = false;
        }
        super.onResume();
    }
}

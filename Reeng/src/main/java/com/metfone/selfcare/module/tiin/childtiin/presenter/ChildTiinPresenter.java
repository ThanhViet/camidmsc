package com.metfone.selfcare.module.tiin.childtiin.presenter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.childtiin.fragment.ChildTiinFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;
import com.viettel.util.LogDebugHelper;

import org.json.JSONObject;

import java.util.ArrayList;

public class ChildTiinPresenter extends BasePresenter implements IChildTiinMvpPresenter {
    TiinApi mTiinApi;
    String urlCategory="";
    long startTime;
    HttpCallBack httpCallBack = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinFragment)) {
                return;
            }
            Gson gson = new Gson();
            ArrayList<TiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<TiinModel>>() {
            }.getType());

            ((ChildTiinFragment) getMvpView()).bindData(response);
            ((ChildTiinFragment) getMvpView()).loadDataSuccess(true);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
            if (response == null) {
                LogDebugHelper.getInstance().logDebugContent("Tiin category error:" + data + " | " + urlCategory);
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_TIIN_CATEGORY, "Tiin category error:" + data + " | " + urlCategory);
            }
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof ChildTiinFragment)) {
                return;
            }
            ((ChildTiinFragment) getMvpView()).loadDataSuccess(false);
            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_CATEGORY, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            LogDebugHelper.getInstance().logDebugContent("Tiin category error:" + message + " | " + urlCategory);
            ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_TIIN_CATEGORY, "Tiin category error:" + message + " | " + urlCategory);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public ChildTiinPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    @Override
    public void getCategoryNew(int categoryId, int page, int num, long unixTime) {

        TiinRequest tiinRequest = new TiinRequest();
        tiinRequest.setCategoryId(categoryId);
        tiinRequest.setPage(page);
        tiinRequest.setNum(num);
        tiinRequest.setUnixTime(unixTime);
        startTime = System.currentTimeMillis();
        if (categoryId == 0) {
            urlCategory = mTiinApi.getApiUrl(TiinApi.GET_CATEGORY_NEW) + categoryId;
            mTiinApi.getCategoryNew(httpCallBack, tiinRequest);
        } else {
            urlCategory = mTiinApi.getApiUrl(TiinApi.GET_CATEGORY) + categoryId;
            mTiinApi.getCategory(httpCallBack, tiinRequest);
        }
    }
}

package com.metfone.selfcare.module.movie.model;

import java.util.ArrayList;
import java.util.List;

public class HomeCinemaDataCache {

    private static List<PageMovieLoadmore> mListPageMovie = new ArrayList<>();
    private static MoviePagerModel[] moviePagerModels = null;
    private static String trendingTitle = null;
    private static String watchedFriendName = null;
    private static HomeCinemaDataCache homeCinemaDataCache;

    private HomeCinemaDataCache() {
    }

    public static MoviePagerModel[] getMoviePagerModels() {
        return moviePagerModels;
    }

    public static void setMoviePagerModels(MoviePagerModel[] moviePagerModels) {
        HomeCinemaDataCache.moviePagerModels = moviePagerModels;
    }

    public static String getTrendingTitle() {
        return trendingTitle;
    }

    public static void setTrendingTitle(String trendingTitle) {
        HomeCinemaDataCache.trendingTitle = trendingTitle;
    }

    public static String getWatchedFriendName() {
        return watchedFriendName;
    }

    public static void setWatchedFriendName(String watchedFriendName) {
        HomeCinemaDataCache.watchedFriendName = watchedFriendName;
    }

    public static HomeCinemaDataCache getInstance() {
        if (homeCinemaDataCache == null) {
            return new HomeCinemaDataCache();
        }
        return homeCinemaDataCache;
    }

    public static void savePageMovie(List<Object> listTrending, List<Object> listRecentlyAdded, List<Object> listCustomTopic) {
        mListPageMovie.add(new PageMovieLoadmore(listTrending, listRecentlyAdded, listCustomTopic));
    }

    public static List<PageMovieLoadmore> getListPageMovie() {
        return mListPageMovie;
    }

    public static void reset() {
        mListPageMovie.clear();
    }

}

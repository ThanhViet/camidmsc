package com.metfone.selfcare.module.metfoneplus.topup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.util.Utilities;

public class WebViewActivity extends Activity {

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set SystemUI & hide navigation
        Utilities.setSystemUiVisibilityHideNavigation(this, R.color.black);

        setContentView(R.layout.activity_web_view_payment);

        RelativeLayout layoutActionBar = findViewById(R.id.mp_layout_action_bar);
        AppCompatImageView mBackImage = findViewById(R.id.action_bar_back_img);
        WebView paymentWebView = findViewById(R.id.web_view_payment);
        Utilities.adaptViewForInserts(layoutActionBar);
        Utilities.adaptViewForInsertBottom(paymentWebView);

        String url = getIntent().getStringExtra("url");
        paymentWebView.getSettings().setJavaScriptEnabled(true); // enable javascript
        paymentWebView.addJavascriptInterface(new Object() {
            @JavascriptInterface // For API 17+
            public void callbackPayment(String status) {
                callBackPayment(status);
//                Toast.makeText(WebViewActivity.this, "payment status " + status, Toast.LENGTH_SHORT).show();
            }
        }, "Android");

        paymentWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        paymentWebView.loadUrl(url);
        mBackImage.setOnClickListener((view) -> finish());
    }

    private void callBackPayment(String status) {
        if (status.equals("0")) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            Toast.makeText(this, getBaseContext().getString(R.string.m_p_notify_error_in_processing), Toast.LENGTH_SHORT).show();
        }
    }
}
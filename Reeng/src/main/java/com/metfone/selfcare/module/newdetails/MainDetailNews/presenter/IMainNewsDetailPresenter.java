package com.metfone.selfcare.module.newdetails.MainDetailNews.presenter;


import com.metfone.selfcare.module.newdetails.interfaces.MvpPresenter;
import com.metfone.selfcare.module.newdetails.model.NewsModel;

/**
 * Created by huongnd38 on 9/26/18.
 */

public interface IMainNewsDetailPresenter extends MvpPresenter {
    void loadDataRelate(NewsModel model, int page, long unixTime);
    void loadDataRelateFromCategory(NewsModel model, int page, long unixTime);
    void loadDataRelateFromCategoryPosition0(NewsModel model, int page, long unixTime);
    void loadDataRelateFromEvent(NewsModel model, int page, long unixTime);
    void addNewModelSeen(String id);

}


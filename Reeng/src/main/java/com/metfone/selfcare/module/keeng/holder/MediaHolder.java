package com.metfone.selfcare.module.keeng.holder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.TimeHelper;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.base.BaseListener.OnClickMedia;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.util.Utilities;

public class MediaHolder extends BaseHolder implements OnClickListener {
    private TextView tvName, tvSinger, tvListenNo, tvPosition, tvTotalSong, tvDuration;
    public ImageView image;
    private View btnOption, viewSocial, btnDetail;
    protected AllModel item;
    private View viewMedia;
    private boolean hasListenNo = true;
    private boolean hasSinger = true;
    private boolean hasPosition = false;
    private boolean hasOption = false;
    private boolean hasLossless = false;
    private boolean hasLarge = false;
    private boolean hasDocquyen = true;
    private int parrentIndex;

    public MediaHolder(View itemView, OnClickMedia listener) {
        super(itemView, listener);
        image = itemView.findViewById(R.id.image);
        tvName = itemView.findViewById(R.id.title);
        tvSinger = itemView.findViewById(R.id.singer);
        tvListenNo = itemView.findViewById(R.id.listen_no);
        tvPosition = itemView.findViewById(R.id.position);
        btnOption = itemView.findViewById(R.id.button_option);
        btnDetail = itemView.findViewById(R.id.button_detail);
        tvTotalSong = itemView.findViewById(R.id.tv_total_song);
        viewMedia = itemView.findViewById(R.id.layout_media);
        tvDuration = itemView.findViewById(R.id.tvDuration);
//        viewSocial = itemView.findViewById(R.id.layout_social);
//        viewSocial.setVisibility(View.GONE);
        if (viewMedia != null)
            viewMedia.setOnClickListener(this);
        if (btnOption != null)
            btnOption.setOnClickListener(this);
    }

    public boolean isHasLarge() {
        return hasLarge;
    }

    public MediaHolder setHasLarge(boolean hasLarge) {
        this.hasLarge = hasLarge;
        return this;
    }

    public boolean isHasSinger() {
        return hasSinger;
    }

    public MediaHolder setHasSinger(boolean hasSinger) {
        this.hasSinger = hasSinger;
        return this;
    }

    public boolean isHasListenNo() {
        return hasListenNo;
    }

    public MediaHolder setHasListenNo(boolean hasListenNo) {
        this.hasListenNo = hasListenNo;
        return this;
    }

    public boolean isHasPosition() {
        return hasPosition;
    }

    public MediaHolder setHasPosition(boolean hasPosition) {
        this.hasPosition = hasPosition;
        return this;
    }

    //hasLossless
    public boolean isHasOption() {
        return hasOption;
    }

    public MediaHolder setHasOption(boolean hasOption) {
        this.hasOption = hasOption;
        return this;
    }

    public MediaHolder setHasDocquyen(boolean hasDocquyen) {
        this.hasDocquyen = hasDocquyen;
        return this;
    }

    public boolean isHasLossless() {
        return hasLossless;
    }

    public MediaHolder setHasLossless(boolean hasLossless) {
        this.hasLossless = hasLossless;
        return this;
    }

//    public void bind(Context context, AllModel media, OnHomeListener listener, int parrentIndex) {
//        mHomeListener = listener;
//        this.parrentIndex = parrentIndex;
//        bind(context, media);
//    }

    public void bind(Context context, AllModel media) {
        this.item = media;
        if (context == null || item == null) {
            if (viewMedia != null)
                viewMedia.setVisibility(View.GONE);
            return;
        }
        int type = item.getType();
        tvName.setText(item.getName());
        tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        tvSinger.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        if (TextUtils.isEmpty(item.getSinger()))
            tvSinger.setVisibility(View.GONE);
        else {
            tvSinger.setVisibility(View.VISIBLE);
            tvSinger.setText(item.getSinger());
        }
        if (isHasListenNo()) {
            if (type == Constants.TYPE_VIDEO || type == Constants.TYPE_MOVIE) {
                if (item.getListened() < 1) {
                    tvListenNo.setVisibility(View.GONE);
                } else if (item.getListened() == 1) {
                    tvListenNo.setText(context.getString(R.string.m_view_no, "1"));
                    tvListenNo.setVisibility(View.VISIBLE);
                } else {
                    tvListenNo.setText(context.getString(R.string.m_views_no, item.getListenNo()));
                    tvListenNo.setVisibility(View.VISIBLE);
                }
            } else {
                if (item.getListened() < 1) {
                    tvListenNo.setVisibility(View.GONE);
                } else if (item.getListened() == 1) {
                    tvListenNo.setText(context.getString(R.string.m_listen_no, "1"));
                    tvListenNo.setVisibility(View.VISIBLE);
                } else {

                    tvListenNo.setText(context.getString(R.string.m_listens_no, item.getListenNo()));
                    tvListenNo.setVisibility(View.VISIBLE);
                }
            }
        } else {
            tvListenNo.setVisibility(View.GONE);
        }
        if (isHasPosition()) {
            tvPosition.setVisibility(View.VISIBLE);
            tvPosition.setText("" + (getAdapterPosition() + 1));
        } else {
            tvPosition.setVisibility(View.GONE);
        }
        if (isHasOption()) {
            btnOption.setVisibility(View.VISIBLE);
        } else {
            btnOption.setVisibility(View.GONE);
        }
        switch (type) {
            case Constants.TYPE_SONG:
            case Constants.TYPE_SONG_UPLOAD:
                if (isHasSinger()) {
                    if (isHasLarge()) {
                        ImageBusiness.setSong(image, item.getImage310(), getAdapterPosition(), Utilities.dpToPx(19));
                    } else {
                        ImageBusiness.setSong(image, item.getImage(), getAdapterPosition(), Utilities.dpToPx(19));
                    }
                }
                if (isHasLossless() && item.isLossless())
                    tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_lossless), null);
                if (hasDocquyen && item.isDocQuyen())
                    tvSinger.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_docquyen), null, null, null);
                break;
            case Constants.TYPE_ALBUM:
                if (btnDetail != null) {
                    btnDetail.setVisibility(View.VISIBLE);
                }
                if (tvTotalSong != null) {
                    int totalSong = item.getSongList().size();
                    if (totalSong > 0) {
                        tvTotalSong.setVisibility(View.VISIBLE);
                        tvTotalSong.setText(context.getString(R.string.music_total_song, totalSong));
                    }
                }
            case Constants.TYPE_PLAYLIST:
            case Constants.TYPE_SONG_BXH_AM:
            case Constants.TYPE_SONG_BXH_CA:
            case Constants.TYPE_SONG_BXH_VN:
                ImageBusiness.setAlbum(image, item.getImage310(), getAdapterPosition(), Utilities.dpToPx(19));
                break;
            case Constants.TYPE_VIDEO:
                if (tvDuration != null && item.getDuration() > 0) {
                    tvDuration.setVisibility(View.VISIBLE);
                    tvDuration.setText(TimeHelper.getDuraionMediaFile(item.getDuration()));
                }
            case Constants.TYPE_YOUTUBE:
            case Constants.TYPE_VIDEO_UPLOAD:
                if (isHasLarge() && item.getListened() >= Constants.NO_LISTENED) {
                    ImageBusiness.setVideoLarge(item.getImage310(), image, getAdapterPosition());
                } else {
                    ImageBusiness.setVideo(item.getImage(), image, getAdapterPosition());
                }
                break;
            case Constants.TYPE_MOVIE:
//                ImageBusiness.setPoster(image, item.getImage310(), getAdapterPosition());
                break;
            default:
                ImageBusiness.setImage(item.getImage(), image, getAdapterPosition());
                break;
        }
    }

/*
    public void bind(Context context, String domainImage, SearchModel item) {
        if (context == null || item == null) {
            if (viewMedia != null)
                viewMedia.setVisibility(View.GONE);
            return;
        }
        int type = item.getType();
        tvName.setText(item.getFullName());
//        tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        if (TextUtils.isEmpty(item.getFullSinger()))
            tvSinger.setVisibility(View.GONE);
        else {
            tvSinger.setVisibility(View.VISIBLE);
            tvSinger.setText(item.getFullSinger());
        }
        if (isHasListenNo() && item.getListened() > 0) {
            tvListenNo.setVisibility(View.VISIBLE);
            tvListenNo.setText(item.getListenNo());
        } else {
            tvListenNo.setVisibility(View.GONE);
        }
        if (isHasPosition()) {
            tvPosition.setVisibility(View.VISIBLE);
            tvPosition.setText("" + (getAdapterPosition() + 1));
        } else {
            tvPosition.setVisibility(View.GONE);
        }
        if (isHasOption()) {
            btnOption.setVisibility(View.VISIBLE);
        } else {
            btnOption.setVisibility(View.GONE);
        }
        switch (type) {
            case Constants.TYPE_SONG:
                ImageBusiness.setSong(domainImage + item.image, image, getAdapterPosition());
//                if (isHasLossless() && item.isLossless())
//                    tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_lossless), null);
                break;
            case Constants.TYPE_ALBUM:
            case Constants.TYPE_PLAYLIST:
            case Constants.TYPE_SONG_BXH_AM:
            case Constants.TYPE_SONG_BXH_CA:
            case Constants.TYPE_SONG_BXH_VN:
                ImageBusiness.setAlbum(domainImage + item.image, image, getAdapterPosition());
                break;
            case Constants.TYPE_VIDEO:
                ImageBusiness.setVideo(domainImage + item.image, image, getAdapterPosition());
                break;
            default:
                ImageBusiness.setImage(domainImage + item.image, image, getAdapterPosition());
                break;
        }
    }
*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_option:
                if (listener != null)
                    listener.onMediaExpandClick(v, getAdapterPosition());
                return;
            case R.id.layout_media:
                if (listener != null)
                    listener.onMediaClick(v, getAdapterPosition());
                return;
            default:
                break;
        }

    }
}

package com.metfone.selfcare.module.home_kh.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Rewards implements Serializable {
    @SerializedName("giftTypeName")
    @Expose
    private String giftTypeName;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isShortCut")
    @Expose
    private Long isShortCut;
    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("partner")
    @Expose
    private String partner;
    @SerializedName("giftCode")
    @Expose
    private String giftCode;
    @SerializedName("isType")
    @Expose
    private Long isType;
    @SerializedName("telecomGifts")
    @Expose
    private List<TelecomGift> telecomGifts = null;

    public String getGiftTypeName() {
        return giftTypeName;
    }

    public void setGiftTypeName(String giftTypeName) {
        this.giftTypeName = giftTypeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIsShortCut() {
        return isShortCut;
    }

    public void setIsShortCut(Long isShortCut) {
        this.isShortCut = isShortCut;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getGiftCode() {
        return giftCode;
    }

    public void setGiftCode(String giftCode) {
        this.giftCode = giftCode;
    }

    public Long getIsType() {
        return isType;
    }

    public void setIsType(Long isType) {
        this.isType = isType;
    }

    public List<TelecomGift> getTelecomGifts() {
        return telecomGifts;
    }

    public void setTelecomGifts(List<TelecomGift> telecomGifts) {
        this.telecomGifts = telecomGifts;
    }

}

package com.metfone.selfcare.module.metfoneplus.billpayment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.ItemBillpaymentTemplateBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MPBillPaymentTemplateAdapter extends RecyclerView.Adapter<MPBillPaymentTemplateAdapter.ViewHolder> {
    private List<MPBillPaymentTemplate> templateList;

    public MPBillPaymentTemplateAdapter(){
        templateList = new ArrayList<>();
    }
    public void setTemplateList(List<MPBillPaymentTemplate> templateList){
        this.templateList = templateList;
        notifyDataSetChanged();
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_billpayment_template, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.bindData(templateList.get(position));
    }

    @Override
    public int getItemCount() {
        return templateList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }

        public void bindData(MPBillPaymentTemplate template){
            ItemBillpaymentTemplateBinding binding = ItemBillpaymentTemplateBinding.bind(itemView);
            binding.setData(template);
        }
    }
}

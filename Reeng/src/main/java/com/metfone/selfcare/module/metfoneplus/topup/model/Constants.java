package com.metfone.selfcare.module.metfoneplus.topup.model;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import static net.yslibrary.android.keyboardvisibilityevent.util.UIUtil.hideKeyboard;

public class Constants {
    public static final String PHONE = "Phone";
    public static final String AMOUNT = "amount";
    public static final String CALL_PAY = "call_pay";
    public static final String CANCEL = "cancel";
    public static final String CONFIRM = "confirm";

    public static void setupUI(View view, Activity activity) {

        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(activity);
                return false;
            }
        });

//If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }
    }

}

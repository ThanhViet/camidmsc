package com.metfone.selfcare.module.tab_home.model;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.newdetails.model.NewsDetailModel;

import java.util.ArrayList;
import java.util.List;

public class Content {
    @SerializedName("id")
    private String id = "";
    @SerializedName("name")
    private String name;
    @SerializedName("item_type")
    private String itemType;
    @SerializedName("image")
    private String image;
    @SerializedName("url")
    private String url = "";
    @SerializedName("description")
    private String description;
    @SerializedName("list_image")
    private ArrayList<String> listImage;
    @SerializedName("body")
    private List<NewsDetailModel> body;
    @SerializedName("cid")
    private int cid;
    @SerializedName("pid")
    private int pid;
    @SerializedName("timeStamp")
    private int timeStamp;
    @SerializedName("isLive")
    private int isLive;

    @SerializedName("chapter")
    private String chapter;
    @SerializedName("is_subtitle")
    private String isSubtitle;
    @SerializedName("is_narrative")
    private String isNarrative;
    @SerializedName("duration")
    private String duration;
    @SerializedName("total_view")
    private long totalViews = 0;
    @SerializedName("total_chapter")
    private int totalEpisodes = 0;
    @SerializedName("channel_id")
    private long channelId;
    @SerializedName("channel_title")
    private String channelName;
    @SerializedName("channel_avatar")
    private String channelAvatar;
    @SerializedName("published_time")
    private long publishedTime;
    @SerializedName("singer_title")
    private String singerName;

    private String parentId;
    private String parentType;
    private String parentName;
    private boolean isFlashHot;
    private boolean isPlayHot;
    private boolean isChannelHot;
    private int resImage;
    private int position;

    public Content() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getIdLong() {
        try {
            return Long.parseLong(id);
        } catch (Exception e) {//
        }
        return 0L;
    }

    public int getIdInt() {
        try {
            return Integer.parseInt(id);
        } catch (Exception e) {//
        }
        return 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public int getItemTypeInt() {
        try {
            return Integer.parseInt(itemType);
        } catch (Exception e) {//
        }
        return 0;
    }

    public String getImage() {
        if (isPlayHot && getListImage().size() > 0) {
            return getListImage().get(0);
        }
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getListImage() {
        if (listImage == null) listImage = new ArrayList<>();
        return listImage;
//        ArrayList<String> list = new ArrayList<>();
//        if (Utilities.notEmpty(image)) {
//            try {
//                Type type = new TypeToken<ArrayList<String>>() {
//                }.getType();
//                ArrayList<String> listTmp = ApplicationController.self().getGson().fromJson(image, type);
//                for (String item : listTmp) {
//                    if (!TextUtils.isEmpty(item)) list.add(item);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return list;
    }

    public List<NewsDetailModel> getBody() {
        if (body == null) body = new ArrayList<>();
        return body;
    }

    public void setBody(List<NewsDetailModel> body) {
        this.body = body;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(int timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isBoxVideo() {
        return HomeContent.TYPE_VIDEO.equals(parentType);
    }

    public boolean isBoxNews() {
        return HomeContent.TYPE_NEWS.equals(parentType);
    }

    public boolean isBoxMovie() {
        return HomeContent.TYPE_MOVIE.equals(parentType);
    }

    public boolean isBoxMusic() {
        return HomeContent.TYPE_MUSIC.equals(parentType);
    }

    public boolean isBoxBanner() {
        return HomeContent.TYPE_BANNER.equals(parentType);
    }

    public boolean isBoxComic() {
        return HomeContent.TYPE_COMIC.equals(parentType);
    }

    public boolean isBoxTiin() {
        return HomeContent.TYPE_TIIN.equals(parentType);
    }

    public String getParentType() {
        return parentType;
    }

    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public boolean isVideo() {
        return isBoxVideo() && "video".equals(itemType);
    }

    public boolean isChannelVideo() {
        return isBoxVideo() && "channel".equals(itemType);
    }

    public boolean isMovie() {
        return isBoxMovie();
    }

    public boolean isMusicPlaylist() {
        return isBoxMusic() && isPlayHot;
    }

    public boolean isSlideBanner() {
        return isBoxBanner();
    }

    public boolean isFlashHot() {
        return isFlashHot;
    }

    public void setFlashHot(boolean flashHot) {
        isFlashHot = flashHot;
    }

    public boolean isPlayHot() {
        return isPlayHot;
    }

    public void setPlayHot(boolean playHot) {
        isPlayHot = playHot;
    }

    public boolean isChannelHot() {
        return isChannelHot;
    }

    public void setChannelHot(boolean channelHot) {
        isChannelHot = channelHot;
    }

    public int getResImage() {
        return resImage;
    }

    public void setResImage(int resImage) {
        this.resImage = resImage;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public boolean isLive() {
        return isLive == 1;
    }

    public int getDurationMinutes() {
        try {
            int tmp = Integer.parseInt(duration);
            return tmp / 60;
        } catch (Exception e) {
        }
        return 0;
    }

    public int getDurationInt() {
        try {
            return Integer.parseInt(duration);
        } catch (Exception e) {
        }
        return 0;
    }

    public String getDuration() {
        return duration;
    }

    public long getTotalViews() {
        return totalViews;
    }

    public int getTotalEpisodes() {
        return totalEpisodes;
    }

    public boolean isSubtitleFilm() {
        return false;
        //return "1".equals(isSubtitle);
    }

    public void setSubtitle(String isSubtitle) {
        this.isSubtitle = isSubtitle;
    }

    public boolean isNarrativeFilm() {
        return "1".equals(isNarrative);
    }

    public String getChapter() {
        return chapter;
    }

    public int getChapterInt() {
        try {
            return Integer.parseInt(chapter);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return 0;
    }

    public long getChannelId() {
        return channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getChannelAvatar() {
        return channelAvatar;
    }

    public long getPublishedTime() {
        return publishedTime;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Content{" +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", itemType='" + itemType + '\'' +
                ", image='" + image + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", listImage=" + listImage +
                ", parentType='" + parentType + '\'' +
                ", parentName='" + parentName + '\'' +
                ", isFlashHot=" + isFlashHot +
                ", isPlayHot=" + isPlayHot +
                ", isChannelHot=" + isChannelHot +
                ", resImage=" + resImage +
                '}';
    }
}

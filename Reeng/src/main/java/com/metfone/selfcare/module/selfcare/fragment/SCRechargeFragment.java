package com.metfone.selfcare.module.selfcare.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.metfone.selfcare.activity.ChooseContactActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.event.SCQRScanEvent;
import com.metfone.selfcare.module.selfcare.event.SCRechargeEvent;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;
import com.metfone.selfcare.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

public class SCRechargeFragment extends BaseFragment implements View.OnClickListener {

    private TextView btnSubmit;
    private ImageView btnBack;

    private TextInputLayout tilSeries;
    private TextInputLayout tilPhone;
    private EditText edtPhone;
    private EditText edtSeries;

    private String seriesCardScan;

    public static SCRechargeFragment newInstance() {
        SCRechargeFragment fragment = new SCRechargeFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return "SCRechargeFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_recharge;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);
        loadData();

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView(View view) {

        tilPhone = view.findViewById(R.id.tilPhone);
        tilSeries = view.findViewById(R.id.tilSeries);
        edtSeries = tilSeries.getEditText();
        edtPhone = tilPhone.getEditText();

        btnSubmit = view.findViewById(R.id.btnSubmit);
        btnBack = view.findViewById(R.id.btnBack);

        btnSubmit.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        edtSeries.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtSeries.getRight() - edtSeries.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        NavigateActivityHelper.navigateToQRScan(mActivity, Constants.QR_SOURCE.FROM_SC_RECHARGE);
                        return true;
                    }
                }
                return false;
            }
        });

        edtPhone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtPhone.getRight() - edtPhone.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Intent chooseFriend = new Intent(mActivity, ChooseContactActivity.class);
                        chooseFriend.putExtra(Constants.CHOOSE_CONTACT.DATA_TYPE, Constants.CHOOSE_CONTACT.TYPE_SELFCARE_RECHARGE);
                        mActivity.startActivity(chooseFriend, true);
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void loadData() {
        Bundle bundle = getArguments();
        Object data = bundle.getSerializable(Constants.KEY_DATA);
        if (data != null && data instanceof SCQRScanEvent) {
            seriesCardScan = ((SCQRScanEvent) data).getContent();
            if (!TextUtils.isEmpty(seriesCardScan))
                edtSeries.setText(seriesCardScan);
        }

        edtPhone.setText(SCUtils.formatPhoneNumber(SCUtils.getPhoneNumber()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                doSubmit();
                break;

            case R.id.btnBack:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCRechargeEvent event) {
        if (event != null) {
            if (!TextUtils.isEmpty(event.getPhoneNumber()))
                edtPhone.setText(SCUtils.formatPhoneNumber(event.getPhoneNumber()));

            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCQRScanEvent event) {
        if (event != null) {
            if (!TextUtils.isEmpty(event.getContent())) {
                String pinCode = event.getContent();

                if (pinCode.contains("*")) {
                    String[] listCode = pinCode.split("\\*");
                    if (listCode.length > 2)
                        pinCode = listCode[2];

                    pinCode = pinCode.replace("#", "");
                }
                if(pinCode.contains("%23"))
                    pinCode = pinCode.replace("%23", "");

                edtSeries.setText(pinCode);
            }

            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    private void doSubmit() {
        mActivity.hideKeyboard();
        final String phone = SCUtils.formatPhoneNumber856(edtPhone.getText().toString());
        final String card = edtSeries.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            mActivity.showToast(R.string.sc_valid_recharge_phone_input);
            return;
        }
        if (TextUtils.isEmpty(card)) {
            mActivity.showToast(R.string.sc_valid_recharge_card_input);
            return;
        }
        if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(card)) {
            WSSCRestful restful = new WSSCRestful(mActivity);
            restful.recharge(phone, card, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int code = jsonObject.optInt("errorCode", -1);
                        String message = jsonObject.optString("message");
                        if (code == 200) {
                            mActivity.showToast(mActivity.getString(R.string.sc_valid_recharge0));
                            EventBus.getDefault().postSticky(new SCAccountEvent(SCAccountEvent.UPDATE_INFO));
                            mActivity.onBackPressed();
                        } else {
                            mActivity.showToast(message);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception", e);
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            });
        }
    }
}

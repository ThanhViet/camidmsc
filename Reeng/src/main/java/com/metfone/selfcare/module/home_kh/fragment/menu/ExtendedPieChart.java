package com.metfone.selfcare.module.home_kh.fragment.menu;

import android.content.Context;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.PieChart;

public class ExtendedPieChart extends PieChart {
    public ExtendedPieChart(Context context) {
        super(context);
    }

    public ExtendedPieChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendedPieChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setMaxAngle(float maxangle) {
        if (maxangle > 360)
            maxangle = 360f;

        if (maxangle < 10)
            maxangle = 10f;

        this.mMaxAngle = maxangle;
    }
}

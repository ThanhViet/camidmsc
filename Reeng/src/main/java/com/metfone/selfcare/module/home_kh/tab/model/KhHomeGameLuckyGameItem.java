package com.metfone.selfcare.module.home_kh.tab.model;

public class KhHomeGameLuckyGameItem implements IHomeModelType{

    @Override
    public int getItemType() {
        return IHomeModelType.LUCKY_GAME_ITEM;
    }
}

package com.metfone.selfcare.module.search.model;

import com.metfone.selfcare.module.home_kh.model.Rewards;
import com.metfone.selfcare.module.home_kh.tab.model.KhHomeRewardDetailItem;
import com.metfone.selfcare.module.home_kh.tab.model.SearchRewardResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;

public class RewardProvisional extends Provisional {

    private ArrayList<SearchRewardResponse.Result.ItemReward> data;

    public RewardProvisional (ArrayList<SearchRewardResponse.Result.ItemReward> data){
        this.data = data;
    }

    public ArrayList<SearchRewardResponse.Result.ItemReward> getData(){
        return data;
    }

    public void setData(ArrayList<SearchRewardResponse.Result.ItemReward> data){
        this.data = data;
    }
    @Override
    public int getSize() {
        return Utilities.notEmpty(data) ? 1 : 0;
    }
}

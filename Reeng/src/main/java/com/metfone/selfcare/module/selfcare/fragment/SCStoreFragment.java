package com.metfone.selfcare.module.selfcare.fragment;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.metfone.selfcare.adapter.RegionSpinnerAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LocationHelper;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.adapter.SCStoreAdapter;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCProvince;
import com.metfone.selfcare.module.selfcare.model.SCStore;
import com.metfone.selfcare.module.selfcare.network.WSSCRestful;
import com.metfone.selfcare.module.selfcare.network.restpaser.RestSCStore;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SCStoreFragment extends BaseFragment implements AbsInterface.OnStoreListener, OnMapReadyCallback, PermissionHelper.RequestPermissionsResult, ClickListener.IconListener, GoogleMap.OnMyLocationButtonClickListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private ImageView btnBack;
    private Spinner spProvince;
    private Spinner spDistrict;
    private RecyclerView recyclerView;

    private ArrayList<SCProvince> listProvince = new ArrayList<>();

    private RegionSpinnerAdapter districtAdapter;
    private ArrayList<SCProvince> listDistrict = new ArrayList<>();

    private SCStoreAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<SCStore> listStores = new ArrayList<>();

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private Location myLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public static SCStoreFragment newInstance() {

        SCStoreFragment fragment = new SCStoreFragment();
        return fragment;
    }

    @Override
    public String getName() {
        return "SCStoreFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_store;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

//        loadDataProvinces();

        return view;
    }

    private void initView(View view) {
        mLocationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_send_location_map_layout);
        mapFragment.getMapAsync(this);

        btnBack = view.findViewById(R.id.btnBack);
        spProvince = view.findViewById(R.id.spProvince);
        spDistrict = view.findViewById(R.id.spDistrict);
        recyclerView = view.findViewById(R.id.recycler_view);

        listDistrict.clear();
        listDistrict.add(new SCProvince("0", mActivity.getString(R.string.sc_store_township)));
        districtAdapter = new RegionSpinnerAdapter(mActivity, listDistrict);
        spDistrict.setAdapter(districtAdapter);

        listProvince.clear();
        listProvince.add(new SCProvince("0", mActivity.getString(R.string.sc_store_state)));
        RegionSpinnerAdapter provinceAdapter = new RegionSpinnerAdapter(mActivity, listProvince);
        spProvince.setAdapter(provinceAdapter);


        adapter = new SCStoreAdapter(mActivity, this);
        if (recyclerView.getItemDecorationCount() <= 0) {
            layoutManager = new CustomLinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(layoutManager);
        }
        recyclerView.setAdapter(adapter);

//        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                SCProvince province = listProvince.get(position);
//                if(province.getId().equals("0"))
//                {
//                    listDistrict.clear();
//                    listDistrict.add(new SCProvince("0", mActivity.getString(R.string.sc_store_township)));
//                    districtAdapter.setListRegions(listDistrict);
//                    districtAdapter.notifyDataSetChanged();
//                }
//                else
//                {
//                    loadDataDistrict(listProvince.get(position).getId());
//                    loadStores(listProvince.get(position).getId(), null);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String provinceId = ((SCProvince)spProvince.getSelectedItem()).getId();
//                String districtId = ((SCProvince)spDistrict.getSelectedItem()).getId();
//                if(!"0".equals(provinceId) && !"0".equals(districtId))
//                {
//                    loadStores(provinceId, districtId);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });


        initGoogleApiClient();
        if (!hasPermissionLocation()) {
            showDialogRequestPermission();
        }
        else
        {
            checkShowDialogSettingLocation();
        }

        loadStores("", "");
    }

    private void loadStores(String provinceId, String districtId) {
        final WSSCRestful restful = new WSSCRestful(mActivity);
        restful.getStores(provinceId, districtId, myLocation != null ? myLocation.getLatitude() + "" : "", myLocation != null ? myLocation.getLongitude() + "" : "", new Response.Listener<RestSCStore>() {
//            restful.getStores(provinceId, districtId, "", "", new ListenerRest<RestSCStore>() {
            @Override
            public void onResponse(RestSCStore result) {
//                super.onResponse(result);
                if(result != null && result.getData() != null)
                {
//                    result.getData().add(new SCStore("Ha noi", "Abc", "17", "90"));
//                    result.getData().add(new SCStore("Ha noi", "Abc1", "18", "91"));
//                    result.getData().add(new SCStore("Ha noi", "Abc2", "19", "92"));
                    listStores.clear();
                    listStores.addAll(result.getData());
                    adapter.setItemsList(listStores);
                    adapter.notifyDataSetChanged();

                    updateMap();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

//    private void loadDataProvinces() {
//        //Load du lieu quan
//        final WSSCRestful restful = new WSSCRestful(mActivity);
//        restful.getProvince(new Response.Listener<RestSCProvince>() {
//            @Override
//            public void onResponse(RestSCProvince result) {
////                super.onResponse(result);
//                if(result != null && result.getData() != null)
//                {
//                    listProvince.clear();
//                    listProvince.add(new SCProvince("0", mActivity.getString(R.string.sc_store_state)));
//                    listProvince.addAll(result.getData());
//                    provinceAdapter.setListRegions(listProvince);
//                    provinceAdapter.notifyDataSetChanged();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//            }
//        });
//    }
//
//    private void loadDataDistrict(String provinceId)
//    {
//        final WSSCRestful restful = new WSSCRestful(mActivity);
//        restful.getDistrict(provinceId, new Response.Listener<RestSCProvince>() {
//            @Override
//            public void onResponse(RestSCProvince result) {
////                super.onResponse(result);
//                if(result != null && result.getData() != null)
//                {
//                    listDistrict.clear();
//                    listDistrict.add(new SCProvince("0", mActivity.getString(R.string.sc_store_township)));
//                    listDistrict.addAll(result.getData());
//                    districtAdapter.setListRegions(listDistrict);
//                    districtAdapter.notifyDataSetChanged();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//            }
//        });
//    }

    @Override
    public void onStoreClick(SCStore item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(SCConstants.KEY_DATA.STORE_DATA, item);
        ((TabSelfCareActivity)mActivity).gotoStoreDetail(bundle);
    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    @SuppressWarnings({"MissingPermission"})
    private void connectLocationUpdates() {
        if (mGoogleApiClient == null) {
            initGoogleApiClient();
        }
        if (mGoogleApiClient.isConnected()) {
            if (hasPermissionLocation()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } else {
            mGoogleApiClient.connect();
        }
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (hasPermissionLocation()) {
            mMap.setMyLocationEnabled(true);
            moveToLastLocation();
        }
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
               updateMap();
            }
        });
    }

    @Override
    public void onPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constants.PERMISSION.PERMISSION_REQUEST_LOCATION && PermissionHelper.verifyPermissions(grantResults)) {
            checkShowDialogSettingLocation();
        } else {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult");
        if (requestCode == Constants.ACTION.ACTION_SETTING_LOCATION) {
            if (LocationHelper.getInstant(ApplicationController.self()).isNetworkLocationEnabled()) {
                connectLocationUpdates();
//                moveToLastLocation();
//                updateMap();
            } else {

            }
        }
    }

    private void checkShowDialogSettingLocation() {
        if (!LocationHelper.getInstant(ApplicationController.self()).isLocationServiceEnabled()) {
            LocationHelper.getInstant(ApplicationController.self()).showDialogSettingLocationProviders(mActivity, this);
        } else if (!LocationHelper.getInstant(ApplicationController.self()).isNetworkLocationEnabled()) {
            LocationHelper.getInstant(ApplicationController.self()).showDialogSettingHighLocation(mActivity, this);
        }
        else
        {
            connectLocationUpdates();
        }
    }

    private boolean hasPermissionLocation() {
        return !(PermissionHelper.declinedPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                PermissionHelper.declinedPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION));
    }


    private void showDialogRequestPermission() {
        PermissionHelper.setCallBack(this);
//        PermissionHelper.requestPermissionWithGuide(mActivity,
//                Manifest.permission.ACCESS_COARSE_LOCATION,
//                Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);

        if (PermissionHelper.declinedPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            PermissionHelper.requestPermissionWithGuide(mActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
        }

        if (PermissionHelper.declinedPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            PermissionHelper.requestPermissionWithGuide(mActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Constants.PERMISSION.PERMISSION_REQUEST_LOCATION);
        }
    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_OK_GG_PLAY_SERVICE:
                NavigateActivityHelper.navigateToPlayStore(mActivity, Constants.PACKET_NAME.GG_PLAY_SERVICE);
                break;
            case Constants.ACTION.ACTION_SETTING_LOCATION:
                try {
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                            Constants.ACTION.ACTION_SETTING_LOCATION);
                }
                catch (ActivityNotFoundException ex)
                {
                    Log.e(TAG, "Exception" + ex);
                }

                break;
            default:
                break;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if (hasPermissionLocation()) {
            checkShowDialogSettingLocation();
        } else {
            showDialogRequestPermission();
        }
        return false;
    }

    private void moveToLastLocation() {
        String locationProvider = LocationHelper.getInstant(ApplicationController.self()).getBestAvailableLocationProvider(mLocationManager);
        if (!TextUtils.isEmpty(locationProvider)) {// lay vi tri cuoi cung
            try {
                Location lastLocation = mLocationManager.getLastKnownLocation(locationProvider);
                if (lastLocation != null) {
                    myLocation = lastLocation;
                    moveCamera(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 13.0f);
                }
            } catch (SecurityException e) {
                Log.e(TAG, "Exception", e);
            }
        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        }
    }

    private void updateMap()
    {
        if(listStores.size() > 0)
        {
            List<LatLng> locations = new ArrayList<>();
            for (SCStore item: listStores) {
                LatLng latLng = new LatLng(Double.parseDouble(item.getLatitude()), Double.parseDouble(item.getLongitude()));
                locations.add(latLng);
                mMap.addMarker(new MarkerOptions().position(latLng).title(item.getAddr()));
            }

            //LatLngBound will cover all your marker on Google Maps
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(locations.get(0)); //Taking Point A (First LatLng)
            builder.include(locations.get(locations.size() - 1)); //Taking Point B (Second LatLng)
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
            mMap.moveCamera(cu);
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
        }
    }

    private void updateMyLocation(Location location)
    {
        myLocation = location;
        if (myLocation != null) {
            moveCamera(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 13.0f);
        }

        loadStores("", "");
    }

    @Override
    public void onLocationChanged(Location location) {
//        updateMyLocation(location);
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (hasPermissionLocation()) {
            Log.d(TAG, "onConnected: +");
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                updateMyLocation(location);
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(mActivity, 9000);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, "Exception", e);
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onDestroyView() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();

            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.unregisterConnectionFailedListener(this);
        }
        App.getInstance().cancelPendingRequests(WSSCRestful.GET_STORES);
        super.onDestroyView();
    }
}

/*
 * Copyright (c) 2017.
 * www.bigzun.com
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.metfone.selfcare.module.keeng.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.keeng.model.Topic;

import java.io.Serializable;
import java.util.ArrayList;

public class RestAllTopic extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 5742941841792549527L;

    @SerializedName("data")
    private ArrayList<Topic> data;

    public ArrayList<Topic> getData() {
        return data;
    }

    public void setData(ArrayList<Topic> data) {
        this.data = data;
    }

}

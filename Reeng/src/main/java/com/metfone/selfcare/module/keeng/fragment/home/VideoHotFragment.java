package com.metfone.selfcare.module.keeng.fragment.home;

import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.keeng.utils.Log;

public class VideoHotFragment extends BaseFragment implements ViewPager.OnPageChangeListener {
    public static final int TAB_VIDEO_HOT = 0;
    public static final int TAB_VIDEO_NEW = 1;
    int currentTab = 0;
    private TextView tvTitle;
    private View btnBack;
    TabLayout tabLayout;
    ViewPager viewPager;
    private ListenerUtils listenerUtils;
    private ViewPagerDetailAdapter viewPagerAdapter;

    public static VideoHotFragment newInstance() {
        Bundle args = new Bundle();
        VideoHotFragment fragment = new VideoHotFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getName() {
        return "ChildVideoHotFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_album_hot_container;
    }


    @SuppressWarnings("NullableProblems")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) return null;
        tvTitle = view.findViewById(R.id.tv_title);
        btnBack = view.findViewById(R.id.iv_back);
        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.view_pager);
        tvTitle.setText(getString(R.string.video_mv).toUpperCase());
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onBackPressed();
            }
        });

        new Handler().postDelayed(this::setupViewPager, Constants.TIME_DELAY_LOAD_DATA_FRAGMENT);

        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentTab >= 0)
            setCurrentItemViewPage(currentTab);
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        if (viewPager != null)
            viewPager.setAdapter(null);
        viewPagerAdapter = null;
        super.onDetach();
    }

    private void setupViewPager() {
        currentTab = 0;
        viewPagerAdapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        Bundle bundle1 = new Bundle();
        bundle1.putInt("type", TAB_VIDEO_HOT);
        viewPagerAdapter.addFragment(ChildVideoHotFragment.newInstance(bundle1), getString(R.string.tab_hot));

        Bundle bundle2 = new Bundle();
        bundle2.putInt("type", TAB_VIDEO_NEW);
        viewPagerAdapter.addFragment(ChildVideoHotFragment.newInstance(bundle2), getString(R.string.tab_new));

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);
        viewPagerAdapter.notifyDataSetChanged();
        setCurrentItemViewPage(0);
    }

    public void setCurrentItemViewPage(int index) {
        try {
            viewPager.setCurrentItem(index);
            if (tabLayout == null) {
                return;
            }
            TabLayout.Tab tab = tabLayout.getTabAt(index);
            if (tab == null) {
                return;
            }
            tab.select();
        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

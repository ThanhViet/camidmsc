/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.myviettel.adapter.PromotionAdapter;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.PromotionProvisional;

import java.util.ArrayList;

import butterknife.BindView;

public class PromotionHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private PromotionAdapter adapter;
    private ArrayList<Object> datas;

    public PromotionHolder(Activity activity, View view, OnMyViettelListener listener) {
        super(view);
        datas = new ArrayList<>();
        adapter = new PromotionAdapter(activity);
        adapter.setListener(listener);
        adapter.setItems(datas);
        BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
    }

    public void bindData(Object item, int position) {
        if (item instanceof PromotionProvisional) {
            if (datas != null) {
                datas.clear();
                datas.addAll(((PromotionProvisional) item).getList());
            }
            if (adapter != null) adapter.notifyDataSetChanged();
        }
    }

}

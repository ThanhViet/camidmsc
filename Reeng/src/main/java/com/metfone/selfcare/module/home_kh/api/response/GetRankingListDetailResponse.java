package com.metfone.selfcare.module.home_kh.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.home_kh.model.RankDefine;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetRankingListDetailResponse {

    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("result")
    @Expose
    private GetRankingListDetailResult result;


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class GetRankingListDetailResult {
        @SerializedName("wsResponse")
        @Expose
        private List<RankDefine> wsResponse = null;
        @SerializedName("errorCode")
        @Expose
        private String errorCode;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("userMsg")
        @Expose
        private String userMsg;

        public List<RankDefine> getWsResponse() {
            return wsResponse;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }

        public String getUserMsg() {
            return userMsg;
        }


    }

}
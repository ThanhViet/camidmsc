package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class CheckUserGameRequest extends BaseRequest<CheckUserGameRequest.Request> {
    public static class Request {
    }

    public static class WsRequest extends Request {
        @SerializedName("isdn")
        String isdn;
        @SerializedName("programCode")
        String programCode;


        public WsRequest(String isdn, String programCode) {
            this.isdn = isdn;
            this.programCode = programCode;
        }
    }
}


package com.metfone.selfcare.module.movienew.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeCategory {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String categoryName;
    @SerializedName("parentid")
    @Expose
    private Integer parentId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("settop")
    @Expose
    private Integer setTop;
    @SerializedName("total_sub_tab")
    @Expose
    private Integer totalSubTab;
    @SerializedName("eventID")
    @Expose
    private Integer eventID;
    @SerializedName("typeFilmID")
    @Expose
    private Integer typeFilmID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSetTop() {
        return setTop;
    }

    public void setSetTop(Integer setTop) {
        this.setTop = setTop;
    }

    public Integer getTotalSubTab() {
        return totalSubTab;
    }

    public void setTotalSubTab(Integer totalSubTab) {
        this.totalSubTab = totalSubTab;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getTypeFilmID() {
        return typeFilmID;
    }

    public void setTypeFilmID(Integer typeFilmID) {
        this.typeFilmID = typeFilmID;
    }

}

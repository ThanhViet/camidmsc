package com.metfone.selfcare.module.home_kh.fragment.benefits;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.activity.BackupKhActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.GetRankingListDetailResponse;
import com.metfone.selfcare.module.home_kh.api.response.RankDefineResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.benefits.adapter.BenefitPageAdapter;
import com.metfone.selfcare.module.home_kh.model.RankDefine;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;

//0882989999 / 123456
public class BenefitsKHFragment extends RewardBaseFragment implements ViewPager.OnPageChangeListener {

    public static final String TAG = BenefitsKHFragment.class.getSimpleName();
    private static final String KEY_RANK = "KEY_RANK";
    private static final String KEY_RANK_ID = "KEY_RANK_ID";
    private static final String KEY_CURRENT_RANK = "KEY_CURRENT_RANK";
    @BindView(R.id.iconRank)
    AppCompatImageView iconRank;
    @BindView(R.id.nameRank)
    AppCompatTextView nameRank;
    private int currentPositionRank = 0;
    private int currentRankId;
    private int rankId;
    private int currentMinPoint = 0;
    @BindView(R.id.vp)
    ViewPager vp;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtTitleToolbar;
    private List<RankDefine> listRank = new ArrayList<>();
    private Unbinder unbinder;
    private BackupKhActivity mParentActivity;
    private BenefitPageAdapter benefitPageAdapter;

    public static BenefitsKHFragment newInstance(int rankId, int currentRankId) {
        BenefitsKHFragment fragment = new BenefitsKHFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_RANK_ID, rankId);
        args.putInt(KEY_CURRENT_RANK, currentRankId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            rankId = bundle.getInt(KEY_RANK_ID);
            currentRankId = bundle.getInt(KEY_CURRENT_RANK);
        }
        mFragmentManager = getParentFragmentManager();
        new MetfonePlusClient().getRankingListDetail(new MPApiCallback<GetRankingListDetailResponse>() {
            @Override
            public void onResponse(Response<GetRankingListDetailResponse> response) {
                if (response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null) {
                    Log.d(TAG, "onResponse: " + response.body());
                    if (listRank == null) {
                        listRank = new ArrayList<>();
                    }
                    listRank.clear();
                    listRank.addAll(response.body().getResult().getWsResponse());
                    if (benefitPageAdapter != null) {
                        benefitPageAdapter.notifyDataSetChanged();
                    }
                    currentPoint();
                    if (vp != null) {
                        vp.setCurrentItem(findPosition());
                    }
                    updateView(findPosition());
                }
            }

            @Override
            public void onError(Throwable error) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParentActivity = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_benefits_kh;
    }


    @OnClick(R.id.icBackToolbar)
    void onBack() {
        popBackStackFragment();
    }

    private void initData() {
        txtTitleToolbar.setText(ResourceUtils.getString(R.string.benefits));
        Bundle bundle = getArguments();
        benefitPageAdapter = new BenefitPageAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        benefitPageAdapter.setData(listRank);
        vp.setAdapter(benefitPageAdapter);
        int current = getPosition(rankId);
        vp.setCurrentItem(current);
        vp.addOnPageChangeListener(this);
        onPageSelected(current);
        tabLayout.setupWithViewPager(vp);
    }

    private int getPosition(int rankId) {
        for (int i = 0; i < listRank.size(); i++) {
            if (listRank.get(i).getRankId() == rankId) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        updateView(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void currentPoint() {
        if (listRank != null) {
            for (int i = 0; i < listRank.size(); i++) {
                RankDefine rankDefine = listRank.get(i);
                if (rankDefine.getRankId() == currentRankId) {
                    currentMinPoint = rankDefine.getMinPoint();
                    currentPositionRank = i;
                    break;
                }
            }
        }
    }

    private int findPosition() {
        if (listRank != null) {
            for (int i = 0; i < listRank.size(); i++) {
                if (listRank.get(i).getRankId() == rankId) {
                    return i;
                }
            }
        }
        return 0;
    }

    private void updateView(int position) {
        if (listRank != null && listRank.size() > 0 && position < listRank.size()) {
            RankDefine rankShow = listRank.get(position);
            int rankID = rankShow.getRankId();
            if (iconRank != null) {
                if (rankID == KhUserRank.RewardRank.MEMBER.id) {
                    iconRank.setImageResource(R.drawable.ic_rank_member_2);
                } else if (rankID == KhUserRank.RewardRank.SILVER.id) {
                    iconRank.setImageResource(R.drawable.ic_rank_silver_2);
                } else if (rankID == KhUserRank.RewardRank.GOLD.id) {
                    iconRank.setImageResource(R.drawable.ic_rank_gold_2);
                } else if (rankID == KhUserRank.RewardRank.DIAMOND.id) {
                    iconRank.setImageResource(R.drawable.ic_rank_diamond);
                } else if (rankID == KhUserRank.RewardRank.PLATINUM.id) {
                    iconRank.setImageResource(R.drawable.ic_rank_platinum);
                }
            }
            if (nameRank != null) {
                if (currentMinPoint >= rankShow.getMinPoint()) {
                    nameRank.setText(R.string.unlocked_rank);
                } else {
                    nameRank.setText(R.string.not_unlock_rank);
                }
            }
        }
    }
}
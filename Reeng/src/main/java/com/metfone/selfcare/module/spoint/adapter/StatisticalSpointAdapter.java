package com.metfone.selfcare.module.spoint.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.spoint.holder.ItemStatisticalHolder;
import com.metfone.selfcare.module.spoint.network.model.StatisticalItemModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class StatisticalSpointAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private LayoutInflater layoutInflater;
    private List<StatisticalItemModel> data = new ArrayList<>();

    public StatisticalSpointAdapter(Activity activity, List<StatisticalItemModel> data) {
        this.activity = activity;
        this.data = data;
        layoutInflater = LayoutInflater.from(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemStatisticalHolder(layoutInflater.inflate(R.layout.holder_item_statistical,parent,false),activity);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ItemStatisticalHolder){
            ((ItemStatisticalHolder) holder).bindData(data.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if(data == null) return 0;
        return data.size();
    }
}

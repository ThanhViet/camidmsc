/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/25
 *
 */

package com.metfone.selfcare.module.tiin.network.api;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.module.tiin.network.request.TiinRequest;

public class TiinApi extends BaseApi {
    public static final String GET_HOME = "/tiinapi/mocha/homepage";
    public static final String GET_HOME_V2 = "/tiinapi/mocha/homepagev2";
    public static final String GET_CATEGORY_NEW = "/tiinapi/mocha/latestNews";
    public static final String GET_CATEGORY = "/tiinapi/mocha/category";
    public static final String GET_DETAIL_NEW = "/tiinapi/mocha/article";
    public static final String GET_RELATE = "/tiinapi/mocha/related";
    public static final String GET_SIBLINGS = "/tiinapi/mocha/siblings";
    public static final String GET_CATEGORY_EVENT = "/tiinapi/mocha/hotTopic";
    public static final String GET_SEARCH_TIIN = "/tiinapi/mocha/search";
    public static final String GET_LIST_EVENT = "/tiinapi/mocha/hottopicarticles.aspx";
    public static final String GET_MOST_VIEW = "/tiinapi/mocha/mostview";
    public static final String GET_CONFIG_CATEGORY = "/tiinapi/mocha/settings/categories";
    public static final String GET_THEMATIC = "/tiinapi/mocha/thematic";
    private static final String GET_DETAIL_BY_URL = "/tiinapi/mocha/GetDetailByUrl";
    private static final String GET_HASHTAG = "/tiinapi/mocha/HashtagDetailV2";

    private String domain;

    public TiinApi(ApplicationController app) {
        super(app);
        domain = getDomainTiin();
    }

    @Override
    protected Http.Builder get(String url) {
        Http.Builder builder = super.get(url);
        String msisdn = getReengAccountBusiness().getJidNumber();
        if (msisdn != null)
            builder.putParameter("msisdn", msisdn);
        if (getReengAccountBusiness().isAnonymousLogin()) {
            builder.putParameter("loginStatus", "0");
        } else {
            builder.putParameter("loginStatus", "1");
        }
        builder.setTimeOut(60);
        return builder;
    }

    @Override
    protected Http.Builder get(String baseUrl, String url) {
        Http.Builder builder = super.get(baseUrl, url);
        String msisdn = getReengAccountBusiness().getJidNumber();
        if (msisdn != null)
            builder.putParameter("msisdn", msisdn);
        if (getReengAccountBusiness().isAnonymousLogin()) {
            builder.putParameter("loginStatus", "0");
        } else {
            builder.putParameter("loginStatus", "1");
        }
        builder.setTimeOut(60);
        return builder;
    }

    @Override
    protected Http.Builder get(String baseUrl, String url, String tag) {
        Http.Builder builder = super.get(baseUrl, url);
        String msisdn = getReengAccountBusiness().getJidNumber();
        if (msisdn != null)
            builder.putParameter("msisdn", msisdn);
        if (getReengAccountBusiness().isAnonymousLogin()) {
            builder.putParameter("loginStatus", "0");
        } else {
            builder.putParameter("loginStatus", "1");
        }
        builder.setTag(tag);
        builder.setTimeOut(60);
        return builder;
    }
    public String getApiUrl(String apiPath) {
        return domain + apiPath;
    }

    public void getHomeTiin(HttpCallBack httpCallBack) {
        get(domain,GET_HOME_V2)
                .withCallBack(httpCallBack).execute();
    }

    public void getCategoryNew(HttpCallBack httpCallBack, TiinRequest request) {
        get(domain, GET_CATEGORY_NEW)
                .putParameter("pid", request.getPid())
                .putParameter("cid", request.getCid())
                .putParameter("page", request.getPage())
                .putParameter("num", request.getNum())
                .withCallBack(httpCallBack).execute();
    }

    public void getCategory(HttpCallBack httpCallBack, TiinRequest request) {
        get(domain, GET_CATEGORY)
                .putParameter("id", request.getCategoryId())
                .putParameter("num", request.getNum())
                .putParameter("page", request.getPage())
                .putParameter("unixTime", request.getUnixTime())
                .withCallBack(httpCallBack)
                .execute();

    }

    public void getNewDetail(HttpCallBack httpCallBack, TiinRequest request, String tag) {
        get(domain, GET_DETAIL_NEW, tag)
                .putParameter("id", request.getId())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getNewRelate(HttpCallBack httpCallBack, TiinRequest request) {
        get(domain, GET_RELATE)
                .putParameter("id", request.getId())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getNewSibling(HttpCallBack httpCallBack, TiinRequest request) {
        get(domain, GET_SIBLINGS)
                .putParameter("id", request.getCategoryId())
                .putParameter("page", request.getPage())
                .putParameter("num", request.getNum())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getCategoryEvent(HttpCallBack httpCallBack, TiinRequest request) {
        get(domain, GET_CATEGORY_EVENT)
                .putParameter("page", request.getPage())
                .putParameter("num", request.getNum())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getListEvent(HttpCallBack httpCallBack, TiinRequest tiinRequest) {
        get(domain, GET_LIST_EVENT)
                .putParameter("id", tiinRequest.getCategoryId())
                .putParameter("page", tiinRequest.getPage())
                .putParameter("num", tiinRequest.getNum())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getMostView(HttpCallBack httpCallBack, TiinRequest tiinRequest) {
        get(domain, GET_MOST_VIEW)
                .putParameter("pid", tiinRequest.getPid())
                .putParameter("cid", tiinRequest.getCid())
                .putParameter("page", tiinRequest.getPage())
                .putParameter("num", tiinRequest.getNum())
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getConfigCategory(HttpCallBack httpCallBack) {
        get(domain, GET_CONFIG_CATEGORY)
                .withCallBack(httpCallBack)
                .execute();
    }

    public void getNewRelateEvent(HttpCallBack httpCallBack, TiinRequest request) {
        get(domain, GET_THEMATIC)
                .putParameter("id", request.getId())
                .putParameter("page", request.getPage())
                .putParameter("num", request.getNum())
                .withCallBack(httpCallBack)
                .execute();
    }
    public void getDetailByUrl(HttpCallBack httpCallBack, String url){
        get(domain,GET_DETAIL_BY_URL)
                .putParameter("url",url)
                .withCallBack(httpCallBack)
                .execute();
    }
    public void getHashTag(HttpCallBack httpCallBack, String page, String hashtag){
        get(domain,GET_HASHTAG)
                .putParameter("page",page)
                .putParameter("tag",hashtag)
                .withCallBack(httpCallBack)
                .execute();
    }
}

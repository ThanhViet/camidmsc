package com.metfone.selfcare.module.home_kh.fragment.setting;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.home_kh.activity.SettingKhActivity;
import com.metfone.selfcare.module.home_kh.base.BaseDialogKhFragment;
import com.metfone.selfcare.module.home_kh.dialog.KhDialogConfirm;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.v5.utils.Util;
import com.metfone.selfcare.v5.widget.SwitchButton;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

public class HideThreadSettingKhFragment extends BaseSettingKhFragment {

    private static final String TAG = HideThreadSettingKhFragment.class.getSimpleName();

    @BindView(R.id.tbUseFingerprint)
    SwitchButton tbUseFingerprint;
    @BindView(R.id.llUseFingerprintAsPin)
    ConstraintLayout llUseFingerprintAsPin;
    @BindView(R.id.viewChangePIN)
    LinearLayoutCompat viewChangePin;
    @BindView(R.id.tvChangePIN)
    AppCompatTextView tvChangePin;
    @BindView(R.id.tvTitleResetPin)
    AppCompatTextView tvTitleResetPin;
    //    @BindView(R.id.tvDesResetPin)
//    AppCompatTextView tvDesResetPin;
    @BindView(R.id.llResetPin)
    LinearLayoutCompat llResetPin;

    String currentPIN;
    @BindView(R.id.tvUseFingerprint)
    AppCompatTextView tvUseFingerprint;
    @BindView(R.id.tvHelp)
    AppCompatTextView tvHelp;
    @BindView(R.id.icQuestion)
    AppCompatImageView icQuestion;
    @BindView(R.id.icNextOne)
    AppCompatImageView icNextOne;
    @BindView(R.id.icNextTwo)
    AppCompatImageView icNextTwo;


    public static HideThreadSettingKhFragment newInstance() {
        return new HideThreadSettingKhFragment();
    }

    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected void initView(View view) {
        setTitle(R.string.kh_setting_hide_conversations);
        currentPIN = mApplication.getPref().getString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "");
        setViewEnable(!TextUtils.isEmpty(currentPIN));
        checkSupportFingerPrintAuth();
        if (!isSupportFingerPrintAuth) {
            llUseFingerprintAsPin.setVisibility(View.GONE);
        } else {
            llUseFingerprintAsPin.setVisibility(View.VISIBLE);
            boolean isEnableUseFinger = mApplication.getPref().getBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, false);
            tbUseFingerprint.setChecked(isEnableUseFinger);
        }

        // TODO: [START] Cambodia version
//        if (TextUtils.isEmpty(currentPIN))
//            tvHelp.setVisibility(View.VISIBLE);
//        else
//            tvHelp.setVisibility(View.GONE);
        // TODO: [END] Cambodia version
    }


    private void setViewEnable(boolean isEnable) {
        if (isEnable) {
//            tvUseFingerprint.setTextColor(ContextCompat.getColor(mActivity, R.color.text_message_send));
//            tvChangePin.setTextColor(ContextCompat.getColor(mActivity, R.color.text_message_send));
//            tvTitleResetPin.setTextColor(ContextCompat.getColor(mActivity, R.color.text_message_send));
//            tvDesResetPin.setTextColor(ContextCompat.getColor(mActivity, R.color.text_message_send));
            tbUseFingerprint.setEnabled(true);
            llUseFingerprintAsPin.setEnabled(true);
            tvChangePin.setEnabled(true);
            llResetPin.setEnabled(true);
            viewChangePin.setEnabled(true);
        } else {
//            tvUseFingerprint.setTextColor(ContextCompat.getColor(mActivity, R.color.text_disable));
//            tvChangePin.setTextColor(ContextCompat.getColor(mActivity, R.color.text_disable));
//            tvTitleResetPin.setTextColor(ContextCompat.getColor(mActivity, R.color.text_disable));
//            tvDesResetPin.setTextColor(ContextCompat.getColor(mActivity, R.color.text_disable));
            tbUseFingerprint.setEnabled(false);
            llUseFingerprintAsPin.setEnabled(false);
            tvChangePin.setEnabled(false);
            llResetPin.setEnabled(false);
            viewChangePin.setEnabled(false);
//            icQuestion.setEnabled(false);
//            icQuestion.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_disable), android.graphics.PorterDuff.Mode.SRC_IN);
//            icNextOne.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_disable), android.graphics.PorterDuff.Mode.SRC_IN);
//            icNextTwo.setColorFilter(ContextCompat.getColor(getContext(), R.color.text_disable), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkSupportFingerPrintAuth();
    }

    @Override
    public void onPause() {
        dismissFingerprintRegisterDialog();
        super.onPause();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_hide_conversation_setting_kh;
    }

    @Optional
    @OnClick({R.id.tbUseFingerprint, R.id.llUseFingerprintAsPin, R.id.viewChangePIN, R.id.llResetPin, R.id.icQuestion})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tbUseFingerprint:
                processFingerPrintEnable();
                break;
            case R.id.llUseFingerprintAsPin:
                tbUseFingerprint.setChecked(!tbUseFingerprint.isChecked());
                processFingerPrintEnable();
                break;
            case R.id.viewChangePIN:
                if (mActivity instanceof SettingKhActivity) {
                    ((SettingKhActivity) mActivity).navigateToChangePINHideThread();
                }
                break;
            case R.id.llResetPin:
//                DialogConfirm dialogRightClick = new DialogConfirm(mActivity, true);
//                dialogRightClick.setMessage(mActivity.getString(R.string.delete_pin_hidden_thread));
//                dialogRightClick.setLabel(mActivity.getString(R.string.reset_pin));
//                dialogRightClick.setNegativeLabel(mActivity.getString(R.string.cancel));
//                dialogRightClick.setPositiveLabel(mActivity.getString(R.string.reset));
//                dialogRightClick.setPositiveListener(new PositiveListener<Object>() {
//                    @Override
//                    public void onPositive(Object result) {
//                        mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "").apply();
//                        currentPIN = "";
//                        mApplication.getMessageBusiness().deleteAllHiddenThread(mActivity);
//                        setViewEnable(!TextUtils.isEmpty(currentPIN));
//                        mActivity.showToast(R.string.msg_reset_pin_success);
//                    }
//                });
//                dialogRightClick.show();
//                DialogCommon dialogCommon = new DialogCommon(mParentActivity, true);
////                dialogCommon.setLabelAction1(getString(R.string.reset));
                KhDialogConfirm dialogConfirm = KhDialogConfirm.newInstance(
                        getString(R.string.kh_setting_reset_pin_title)
                        , getString(R.string.kh_setting_reset_pin_confirm),
                        com.metfone.selfcare.v5.dialog.DialogConfirm.CONFIRM_TYPE, R.string.cancel, R.string.yes);
                dialogConfirm.setSelectListener(new BaseDialogKhFragment.DialogListener() {
                    @Override
                    public void dialogRightClick(int value) {
                        mApplication.getPref().edit().putString(Constants.PREFERENCE.PREF_PIN_HIDE_THREAD_CHAT, "").apply();
                        currentPIN = "";
                        mApplication.getMessageBusiness().deleteAllHiddenThread(mActivity);
                        setViewEnable(!TextUtils.isEmpty(currentPIN));
                        mActivity.showToast(R.string.msg_reset_pin_success);
                    }

                    @Override
                    public void dialogLeftClick() {

                    }
                });
                dialogConfirm.show(getChildFragmentManager(), "");
                break;
            case R.id.icQuestion: {
                // define y of ic question
                float y = llResetPin.getY() + appBarLayout.getHeight() + icQuestion.getY();
                Util.showDialogExplain(mParentActivity, getString(R.string.reset_pin_des), icQuestion.getX(), y, icQuestion);
            }
        }
    }

    private boolean isSupportFingerPrintAuth = false;
    private boolean isFingerPrintEnrolled = false;

    private DialogConfirm mFingerPrintConfirmDialog = null;


    private void checkSupportFingerPrintAuth() {
        try {
            FingerprintManagerCompat fingerprintManager = FingerprintManagerCompat.from(mActivity);
            isSupportFingerPrintAuth = fingerprintManager.isHardwareDetected();
            isFingerPrintEnrolled = fingerprintManager.hasEnrolledFingerprints();
//            boolean isFingerPrintEnable = mApp.getPref().getBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, false);
//            mApp.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, isFingerPrintEnable && isSupportFingerPrintAuth && isFingerPrintEnrolled).apply();
        } catch (Exception e) {
        }
    }


    private void processFingerPrintEnable() {
        if (!isSupportFingerPrintAuth) return;
        boolean isEnableFingerPrint = tbUseFingerprint.isChecked();
        if (isEnableFingerPrint) {
            if (!isFingerPrintEnrolled) {
                showFingerPrintRegisterDialog();
                tbUseFingerprint.setChecked(false);
                mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, false).apply();
            } else {
                mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, true).apply();
                mActivity.showToast(R.string.setup_successfull);
            }
        } else {
            mApplication.getPref().edit().putBoolean(Constants.PREFERENCE.PREF_PIN_USE_FINGERPRINT, false).apply();
        }
    }

    private void showFingerPrintRegisterDialog() {
        mFingerPrintConfirmDialog = new DialogConfirm(mActivity, true);
        mFingerPrintConfirmDialog.setLabel(getString(R.string.popup_fingerprint_register));
        mFingerPrintConfirmDialog.setMessage(getString(R.string.fingerprint_need_register));
        mFingerPrintConfirmDialog.setNegativeLabel(getString(R.string.cancel));
        mFingerPrintConfirmDialog.setPositiveLabel(getString(R.string.title_message_setting));
        mFingerPrintConfirmDialog.setPositiveListener(result -> {
            // go to fingerprint settings
            try {
                Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                startActivity(intent);
            } catch (Exception e) {
            }
        });
        mFingerPrintConfirmDialog.show();
    }

    private void dismissFingerprintRegisterDialog() {
        try {
            if (mFingerPrintConfirmDialog != null) {
                mFingerPrintConfirmDialog.dismiss();
            }
        } catch (Exception e) {
        }
    }


}

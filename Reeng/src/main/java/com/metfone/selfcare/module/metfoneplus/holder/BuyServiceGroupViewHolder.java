package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.ExchangeItem;
import com.metfone.selfcare.model.camid.ServiceGroup;

public class BuyServiceGroupViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout mLayoutGroup;
    public AppCompatTextView mGroupName;
    public AppCompatTextView mGroupDescription;
    public AppCompatTextView mGroupValid;
    public LinearLayout mLayoutGroupState;
    public LinearLayout mLayoutGroupStateAutoRenew;
    public AppCompatTextView mLayoutGroupStateRegister;
    public ServiceGroup mServiceGroup;
    public ExchangeItem mExchangeItem;

    public BuyServiceGroupViewHolder(@NonNull View itemView) {
        super(itemView);
        mLayoutGroup = itemView.findViewById(R.id.layout_item_buy_service_group);
        mGroupName = itemView.findViewById(R.id.layout_item_buy_service_name);
        mGroupDescription = itemView.findViewById(R.id.layout_item_buy_service_group_description);
        mGroupValid = itemView.findViewById(R.id.layout_item_buy_service_group_valid);
        mLayoutGroupState = itemView.findViewById(R.id.layout_item_buy_service_group_state);
        mLayoutGroupStateAutoRenew = itemView.findViewById(R.id.layout_item_buy_service_group_state_auto_renew);
        mLayoutGroupStateRegister = itemView.findViewById(R.id.layout_item_buy_service_group_state_register);
    }
}

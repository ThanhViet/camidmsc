/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/9/6
 *
 */

package com.metfone.selfcare.module.share.listener;

import com.metfone.selfcare.module.search.model.ContactProvisional;

import java.util.ArrayList;

public interface GetAllContactsListener {
    void onPrepareGetAllContacts();

    void onFinishedGetAllContacts(ArrayList<ContactProvisional> list);
}

package com.metfone.selfcare.module.gameLive.socket;

import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.gameLive.GameLiveActivity;
import com.metfone.selfcare.module.gameLive.model.LiveMessageModel;
import com.metfone.selfcare.module.gameLive.socket.stomp.Stomp;
import com.metfone.selfcare.module.gameLive.socket.stomp.StompClient;
import com.metfone.selfcare.module.gameLive.socket.stomp.dto.StompMessage;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

public class SocketManager {
    private static final String TAG = "SocketManager";

    private SocketManager() {
    }

    public static SocketManager getInstance() {
        return SingletonHelper.INSTANCE;
    }

    private static class SingletonHelper {
        private static final SocketManager INSTANCE = new SocketManager();
    }

    StompClient mWebSocketClient;

    @SuppressLint("CheckResult")
    public void connectWebSocket(final GameLiveActivity context, String url) {
        Log.e(TAG, "connectWebSocket url: " + url);
        mWebSocketClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, url);
        mWebSocketClient.connect();

        mWebSocketClient.lifecycle().subscribe(lifecycleEvent -> {
            switch (lifecycleEvent.getType()) {
                case OPENED:
                    Log.d(TAG, "Stomp connection opened");
                    mWebSocketClient.topic("/topic/gameTH").subscribe(message -> {
                        Log.i(TAG, "Received message: " + message.getPayload());
                        onReceiveMessage(message);
                    });

                    LogDebugHelper.getInstance().logDebugContent("Game Live: Stomp connection opened");
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_MESSAGE, "Game Live: Stomp connection opened");

                    EventBus.getDefault().postSticky(new SocketEvent(SocketEvent.TYPE_OPEN));
                    break;
                case CLOSED:
                    Log.d(TAG, "Stomp connection closed");

                    LogDebugHelper.getInstance().logDebugContent("Game Live: Stomp connection closed");
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_MESSAGE, "Game Live: Stomp connection closed");

                    EventBus.getDefault().postSticky(new SocketEvent(SocketEvent.TYPE_CLOSE));
                    break;
                case ERROR:
                    Log.e(TAG, "Stomp connection error", lifecycleEvent.getException());
                    mWebSocketClient.reconnect();

                    LogDebugHelper.getInstance().logDebugContent("Game Live: Stomp connection error");
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_MESSAGE, "Game Live: Stomp connection error");

                    EventBus.getDefault().postSticky(new SocketEvent(SocketEvent.TYPE_ERROR));
                    break;
            }
        });
    }

    private void onReceiveMessage(StompMessage message) {
        try {
            JSONObject jso = new JSONObject(message.getPayload());
//            int type = jso.optInt("type");
//            int totalNumber = jso.optInt("totalNumber");
//            JSONObject jsonQuestion = jso.optJSONObject("question");
//            long timeServer = jso.optInt("timeServer");

            Gson gson = new Gson();
            LiveMessageModel messageModel = gson.fromJson(jso.toString(), LiveMessageModel.class);

            EventBus.getDefault().postSticky(messageModel);

            if (messageModel.getType() != 6)//Type bang 6 la type cap nhat nguoi ra vao
            {
                LogDebugHelper.getInstance().logDebugContent("Game Live Message: " + message.getPayload());
                ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_GAME_LIVE_MESSAGE, "Game Live Message: " + message.getPayload());
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSONException", e);
        }
    }

    public void sendMessage(String message) {
        if (mWebSocketClient != null)
            mWebSocketClient.send(message);
    }

    public StompClient getWebSocketClient() {
        return mWebSocketClient;
    }

    private void unsubscriber() {
        if (mWebSocketClient != null)
            mWebSocketClient.unsubscribePath("/topic/gameTH");
    }

    public void disconnect() {
        if (mWebSocketClient != null)
            mWebSocketClient.disconnect();
    }

    public boolean isConnected() {
        if (mWebSocketClient != null)
            return mWebSocketClient.isConnected();
        return false;
    }

    public void reconnect() {
        if (mWebSocketClient != null)
            mWebSocketClient.reconnect();
    }
}

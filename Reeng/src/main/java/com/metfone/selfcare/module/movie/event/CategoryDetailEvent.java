/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.event;

import com.metfone.selfcare.model.tabMovie.SubtabInfo;

public class CategoryDetailEvent {
    boolean deleteAll;
    boolean scrollToTop;
    SubtabInfo tabInfo;

    public CategoryDetailEvent(SubtabInfo tabInfo) {
        this.tabInfo = tabInfo;
    }

    public boolean isDeleteAll() {
        return deleteAll;
    }

    public void setDeleteAll(boolean deleteAll) {
        this.deleteAll = deleteAll;
    }

    public boolean isScrollToTop() {
        return scrollToTop;
    }

    public void setScrollToTop(boolean scrollToTop) {
        this.scrollToTop = scrollToTop;
    }

    public SubtabInfo getTabInfo() {
        return tabInfo;
    }

    public void setTabInfo(SubtabInfo tabInfo) {
        this.tabInfo = tabInfo;
    }
}

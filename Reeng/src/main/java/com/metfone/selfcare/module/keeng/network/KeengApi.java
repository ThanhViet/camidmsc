/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/26
 *
 */

package com.metfone.selfcare.module.keeng.network;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.BaseApi;
import com.metfone.selfcare.common.api.http.Http;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeviceHelper;
import com.metfone.selfcare.helper.UrlConfigHelper;
import com.metfone.selfcare.helper.VolleyHelper;
import com.metfone.selfcare.module.keeng.App;
import com.metfone.selfcare.module.keeng.network.restful.PostFormDataRequest;
import com.metfone.selfcare.module.keeng.network.restpaser.RestAllModel;
import com.metfone.selfcare.module.keeng.network.restpaser.RestAllPlaylist;
import com.metfone.selfcare.module.keeng.network.restpaser.RestAllTopic;
import com.metfone.selfcare.module.keeng.network.restpaser.RestModel;
import com.metfone.selfcare.module.keeng.network.restpaser.RestMusicHome;
import com.metfone.selfcare.module.keeng.network.restpaser.RestPlaylist;
import com.metfone.selfcare.module.keeng.network.restpaser.RestRankInfo;
import com.metfone.selfcare.module.keeng.network.restpaser.RestString;
import com.metfone.selfcare.module.keeng.network.restpaser.RestTopicModel;
import com.metfone.selfcare.restful.GsonRequest;
import com.metfone.selfcare.restful.ResfulSearchQueryObj;
import com.metfone.selfcare.restful.ResfulString;
import com.metfone.selfcare.restful.RestTopModel;
import com.metfone.selfcare.restful.StringRequest;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

public class KeengApi extends BaseApi {
    private static final String TAG = "KeengApi";

    public static final String GET_SONG_DETAIL = "/KeengWSRestful/ws/common/getSong";
    public static final String GET_ALBUM_DETAIL = "/KeengWSRestful/ws/common/getAlbumInfo";
    public static final String GET_VIDEO_DETAIL = "/KeengWSRestful/ws/common/getVideo";
    public static final String GET_PLAYLIST_DETAIL = "/KeengWSRestful/ws/common/getPlaylistInfoV1";

    //public static final String GET_CATEGORY_LIST = "/KeengWSRestful/ws/common/getCategoryList";
    //public static final String GET_CATEGORY_DETAIL = "/KeengWSRestful/ws/common/getCategoryDetail";
    //public static final String GET_TOPIC_LIST = "/KeengWSRestful/ws/common/getTopicList";
    public static final String GET_TOPIC_DETAIL = "/KeengWSRestful/ws/common/getTopicDetail";
    public static final String GET_SINGER_LIST = "/KeengWSRestful/ws/common/getSingerList";
    public static final String GET_SINGER_DETAIL = "/KeengWSRestful/ws/common/getSingerDetail";
    public static final String GET_SINGER = "/KeengWSRestful/ws/common/getSingerDetailAll";
    public static final String GET_SONG = "/KeengWSRestful/ws/common/getSong";
    //public static final String GET_ALBUM = "/KeengWSRestful/ws/common/getAlbum";
    public static final String GET_ALBUM_INFO = "/KeengWSRestful/ws/common/getAlbumInfo";
    public static final String GET_PLAYLIST_INFO = "/KeengWSRestful/ws/common/getPlaylistInfoV1";
    public static final String GET_VIDEO = "/KeengWSRestful/ws/common/getVideo";
    public static final String GET_LYRIC = "/KeengWSRestful/ws/common/getLyric";
    public static final String GET_HOT_PLAYLIST = "/KeengWSRestful/ws/common/getListPlaylistHotV1";
    public static final String GET_SEARCH = "/solr/media/select/";
    public static final String GET_VIDEO_HOT = "/KeengWSRestful/ws/common/getListVideoHot";
    public static final String GET_ALBUM_HOT = "/KeengWSRestful/ws/common/getListAlbumHotV1";
    public static final String GET_SONG_HOT = "/KeengWSRestful/ws/common/getListSongHot";
    public static final String GET_TOPIC_HOT = "/KeengWSRestful/ws/common/getListTopicHot";
    public static final String GET_RANK_BY_TYPE = "/KeengWSRestful/ws/common/getRankDetail";
    public static final String GET_INFO_SINGER = "/KeengWSRestful/ws/common/getSinger";
    public static final String GET_INFO_TOPIC = "/KeengWSRestful/ws/common/getTopic";
    public static final String GET_INFO_RANK = "/KeengWSRestful/ws/common/getRankInfo";
    public static final String GET_VIDEO_NEW = "/KeengWSRestful/ws/common/getListVideoNewV4";
    public static final String GET_ALBUM_NEW = "/KeengWSRestful/ws/common/getListAlbumNewV4";
    public static final String GET_SONG_NEW = "/KeengWSRestful/ws/common/getListSongNewV4";
    public static final String GET_TOP_HIT_DETAIL = "/KeengWSRestful/ws/common/getTophitDetail";
    public static final String GET_INFO_TOP_HIT = "/KeengWSRestful/ws/common/getTophit";
    public static final String GET_TOP_HIT = "/KeengWSRestful/ws/common/getListTophit";
    public static final String GET_HOME_V5 = "/KeengWSRestful/ws/common/getHomeV5";

    private static final String COUNTRY_CODE = "country_code";
    private static final String DEVICE_ID = "device_id";
    private static final String VERSION = "version";
    private static final String REVISION = "revision";
    private static final String LANGUAGE_CODE = "language_code";
    private static final String SESSION_TOKEN = "session_token";
    private static final String USER_ID = "user_id";

    public static final int TIME_OUT = 15;
    public static final int MAX_NUMBER_TO_RETRY = 2;
    public static final int NUMBER_PER_PAGE = 30; // ko duoc thay doi
    public static final int NUM_PER_PAGE = 20;
    private final RetryPolicy policy = new DefaultRetryPolicy(TIME_OUT * 1000, MAX_NUMBER_TO_RETRY, 1.0f);

    private String domain;

    public KeengApi() {
        super(ApplicationController.self());
        domain = getDomainKeengMusic();
    }

    @Override
    protected Http.Builder configBuilder(@NonNull Http.Builder builder) {
        builder.putHeader(DEVICE_ID, DeviceHelper.getDeviceId(application));
        builder.putHeader(COUNTRY_CODE, getReengAccountBusiness().getRegionCode());
        builder.putHeader(LANGUAGE_CODE, com.metfone.selfcare.module.keeng.utils.Utilities.getLanguageCode());
        builder.putHeader(VERSION, BuildConfig.VERSION_NAME);
        builder.putHeader(REVISION, String.valueOf(BuildConfig.VERSION_CODE));
        builder.putHeader(SESSION_TOKEN, "");
        builder.putHeader(USER_ID, "0");
        builder.putHeader("msisdn", getReengAccountBusiness().getJidNumber());
        builder.putHeader(DEVICE_ID, getReengAccountBusiness().getMochaApi());
        builder.putHeader(UUID, Utilities.getUuidApp());
        return super.configBuilder(builder);
    }

    public String getApiUrl(String apiPath) {
        return domain + apiPath;
    }

    public Http getSong(long id, String identify, @Nullable final ApiCallbackV2<RestModel> apiCallback) {
        String TAG_REQUEST = "TAG_GET_SONG_DETAIL";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, GET_SONG_DETAIL);
        if (id > 0)
            builder.putParameter("id", String.valueOf(id));
        else if (Utilities.notEmpty(identify))
            builder.putParameter("identify", identify);

        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null)
                    apiCallback.onSuccess("", gson.fromJson(response, RestModel.class));
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http getAlbum(long id, String identify, @Nullable final ApiCallbackV2<RestModel> apiCallback) {
        String TAG_REQUEST = "TAG_GET_ALBUM_DETAIL";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, GET_ALBUM_DETAIL);
        if (id > 0)
            builder.putParameter("id", String.valueOf(id));
        else if (Utilities.notEmpty(identify))
            builder.putParameter("identify", identify);

        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null)
                    apiCallback.onSuccess("", gson.fromJson(response, RestModel.class));
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http getVideo(long id, String identify, @Nullable final ApiCallbackV2<RestModel> apiCallback) {
        String TAG_REQUEST = "TAG_GET_VIDEO_DETAIL";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, GET_VIDEO_DETAIL);
        if (id > 0)
            builder.putParameter("id", String.valueOf(id));
        else if (Utilities.notEmpty(identify))
            builder.putParameter("identify", identify);

        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null)
                    apiCallback.onSuccess("", gson.fromJson(response, RestModel.class));
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    public Http getPlaylist(long id, String identify, @Nullable final ApiCallbackV2<RestPlaylist> apiCallback) {
        String TAG_REQUEST = "TAG_GET_PLAYLIST_DETAIL";
        //Http.cancel(TAG_REQUEST);
        Http.Builder builder = get(domain, GET_PLAYLIST_DETAIL);
        if (id > 0)
            builder.putParameter("id", String.valueOf(id));
        else if (Utilities.notEmpty(identify))
            builder.putParameter("identify", identify);

        builder.putParameter("Platform", Constants.HTTP.PLATFORM);
        builder.putParameter("msisdn", getReengAccountBusiness().getJidNumber());
        builder.withCallBack(new HttpCallBack() {
            @Override
            public void onSuccess(String response) throws Exception {
                if (apiCallback != null)
                    apiCallback.onSuccess("", gson.fromJson(response, RestPlaylist.class));
            }

            @Override
            public void onFailure(String message) {
                if (apiCallback != null)
                    apiCallback.onError(application.getString(R.string.e601_error_but_undefined));
            }

            @Override
            public void onCompleted() {
                if (apiCallback != null) apiCallback.onComplete();
            }
        });
        builder.setTimeOut(TIME_OUT);
        builder.setTag(TAG_REQUEST);
        return builder.execute();
    }

    //todo Volley --------------------------------------------------------
    private <T> void addToRequestQueue(Request<T> req, String tag) {
        com.metfone.selfcare.util.Utilities.addDefaultParamsRequestVolley(req);
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        if (req.getMethod() != Request.Method.POST)
            req.setShouldCache(true);
        req.setRetryPolicy(policy);
        App.getInstance().getRequestQueue().add(req);
    }

    private <T> void addToRequestQueue(Request<T> req) {
        com.metfone.selfcare.util.Utilities.addDefaultParamsRequestVolley(req);
        req.setTag("TAG_REQUEST" + System.currentTimeMillis());
        req.setShouldCache(false);
        req.setRetryPolicy(policy);
        App.getInstance().getRequestQueue().add(req);
    }

    private <T> void initHeader(GsonRequest<T> req) {
        req.setHeader(DEVICE_ID, DeviceHelper.getDeviceId(application));
        req.setHeader(COUNTRY_CODE, getReengAccountBusiness().getRegionCode());
        req.setHeader(LANGUAGE_CODE, com.metfone.selfcare.module.keeng.utils.Utilities.getLanguageCode());
        req.setHeader(SESSION_TOKEN, "");
        req.setHeader(USER_ID, "0");
        req.setHeader(VERSION, BuildConfig.VERSION_NAME);
        req.setHeader(REVISION, String.valueOf(BuildConfig.VERSION_CODE));
        req.setHeader("msisdn", getReengAccountBusiness().getJidNumber());
        req.setHeader(MOCHA_API, getReengAccountBusiness().getMochaApi());
        req.setHeader(UUID, Utilities.getUuidApp());
    }

    private void initHeader(StringRequest req) {
        req.setHeader(DEVICE_ID, DeviceHelper.getDeviceId(application));
        req.setHeader(COUNTRY_CODE, getReengAccountBusiness().getRegionCode());
        req.setHeader(LANGUAGE_CODE, com.metfone.selfcare.module.keeng.utils.Utilities.getLanguageCode());
        req.setHeader(SESSION_TOKEN, "");
        req.setHeader(USER_ID, "0");
        req.setHeader(VERSION, BuildConfig.VERSION_NAME);
        req.setHeader(REVISION, String.valueOf(BuildConfig.VERSION_CODE));
        req.setHeader("msisdn", getReengAccountBusiness().getJidNumber());
        req.setHeader(MOCHA_API, getReengAccountBusiness().getMochaApi());
        req.setHeader(UUID, Utilities.getUuidApp());
    }

    private void initHeader(PostFormDataRequest req) {
        req.setHeader(DEVICE_ID, DeviceHelper.getDeviceId(application));
        req.setHeader(COUNTRY_CODE, getReengAccountBusiness().getRegionCode());
        req.setHeader(LANGUAGE_CODE, com.metfone.selfcare.module.keeng.utils.Utilities.getLanguageCode());
        req.setHeader(SESSION_TOKEN, "");
        req.setHeader(USER_ID, "0");
        req.setHeader(VERSION, BuildConfig.VERSION_NAME);
        req.setHeader(REVISION, String.valueOf(BuildConfig.VERSION_CODE));
        req.setHeader("msisdn", getReengAccountBusiness().getJidNumber());
        req.setHeader(MOCHA_API, getReengAccountBusiness().getMochaApi());
        req.setHeader(UUID, Utilities.getUuidApp());
    }

    private <T> void addReq(GsonRequest<T> req) {
        initHeader(req);
        addToRequestQueue(req);
    }

    private <T> void addReq(GsonRequest<T> req, String tag) {
        initHeader(req);
        addToRequestQueue(req, tag);
    }

    private <T> void addReq(StringRequest req, String tag) {
        initHeader(req);
        addToRequestQueue(req, tag);
    }

    private <T> void addReq(PostFormDataRequest req, String tag) {
        initHeader(req);
        addToRequestQueue(req, tag);
    }

    /**
     * tra ve danh sach chi tiet song,video,album theo chu de
     *
     * @param id
     * @param type
     * @param page
     * @param response
     * @param error
     */
    public void getTopicDetail(long id, int type, int page, int numPerPage
            , Response.Listener<RestAllModel> response, Response.ErrorListener error) {

        ResfulString params = new ResfulString(
                getApiUrl(GET_TOPIC_DETAIL));
        params.addParam("id", id);
        params.addParam("type", type);
        params.addParam("page", page);
        params.addParam("num", numPerPage);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req);
    }

    /**
     * tra ve danh sach ca sy
     *
     * @param page
     * @param response
     * @param error
     */
    public void getSingerList(int page, int num, Response.Listener<RestAllTopic> response, Response.ErrorListener error) {

        ResfulString params = new ResfulString(
                getApiUrl(GET_SINGER_LIST));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllTopic> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllTopic.class, null, response, error);

        addReq(req, GET_SINGER_LIST);
    }

    /**
     * tra ve danh sach chi tiet song,video,album cua ca sy
     *
     * @param id
     * @param type
     * @param page
     * @param response
     * @param error
     */
    public void getSingerDetail(long id, int type, int page, int num
            , Response.Listener<RestAllModel> response, Response.ErrorListener error) {

        ResfulString params = new ResfulString(
                getApiUrl(GET_SINGER_DETAIL));
        params.addParam("id", id);
        params.addParam("type", type);
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");

        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);

        addReq(req);
    }

    public void getSinger(long id, int num, Response.Listener<RestTopicModel> response, Response.ErrorListener error) {

        ResfulString params = new ResfulString(
                getApiUrl(GET_SINGER));
        params.addParam("id", id);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");

        GsonRequest<RestTopicModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestTopicModel.class, null, response, error);

        addReq(req);
    }

    /**
     * tra ve lyric cua 1 bai hat
     *
     * @param id       : bai hat
     * @param response
     * @param error
     */
    public void getLyric(long id, Response.Listener<RestString> response, Response.ErrorListener error) {

        ResfulString params = new ResfulString(
                getApiUrl(GET_LYRIC));
        params.addParam("id", id);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestString> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestString.class, null, response, error);
        addReq(req, GET_LYRIC);
    }

    public void getInfoMediaUrl(String link, Response.Listener<String> response, Response.ErrorListener error) {
        if (!TextUtils.isEmpty(link)) {
            StringRequest req = new StringRequest(link, response, error);
            addReq(req, link);
        }
    }

    public void getHomeNew(Response.Listener<RestMusicHome> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(getApiUrl(GET_HOME_V5));
        params.addParam("Platform", "Mocha");
        GsonRequest<RestMusicHome> req = new GsonRequest<>(Request.Method.GET, params.toString()
                , RestMusicHome.class, null, response, error);
        addReq(req, GET_HOME_V5);
    }

    public void getVideoHot(int page, int num, Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_VIDEO_HOT));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_VIDEO_HOT);
    }

    public void getAlbumHot(int page, int num, Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_ALBUM_HOT));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_ALBUM_HOT);
    }

    public void getSongHot(int page, int num, Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_SONG_HOT));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_SONG_HOT);
    }

    public void getTopicHot(int page, int num, Response.Listener<RestAllTopic> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_TOPIC_HOT));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllTopic> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllTopic.class, null, response, error);
        addReq(req, GET_TOPIC_HOT);
    }

    public GsonRequest<RestAllModel> getRankByType(int type, int typeRank, int page, int num
            , Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_RANK_BY_TYPE));
        params.addParam("item_type", type);
        params.addParam("rank_type", typeRank);
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_RANK_BY_TYPE);
        return req;
    }

    public void getInfoSinger(long id, Response.Listener<RestTopicModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_INFO_SINGER));
        params.addParam("id", id);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestTopicModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestTopicModel.class, null, response, error);
        addReq(req, GET_INFO_SINGER);
    }

    public void getInfoTopic(long id, Response.Listener<RestTopicModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_INFO_TOPIC));
        params.addParam("id", id);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestTopicModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestTopicModel.class, null, response, error);
        addReq(req, GET_INFO_TOPIC);
    }

    public void getInfoRank(int id, Response.Listener<RestRankInfo> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_INFO_RANK));
        params.addParam("rank_type", id);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestRankInfo> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestRankInfo.class, null, response, error);
        addReq(req, GET_INFO_RANK);
    }

    public void getVideoNew(int page, int num, Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_VIDEO_NEW));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_VIDEO_NEW);
    }

    public void getAlbumNew(int page, int num, Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_ALBUM_NEW));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_ALBUM_NEW);
    }

    public void getSongNew(int page, int num, Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_SONG_NEW));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_SONG_NEW);
    }

    public void getTopHitDetail(long id, int type, int page, int numPerPage
            , Response.Listener<RestAllModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_TOP_HIT_DETAIL));
        params.addParam("id", id);
        params.addParam("type", type);
        params.addParam("page", page);
        params.addParam("num", numPerPage);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllModel.class, null, response, error);
        addReq(req, GET_TOP_HIT_DETAIL);
    }

    public void getInfoTopHit(long id, Response.Listener<RestTopicModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_INFO_TOP_HIT));
        params.addParam("id", id);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestTopicModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestTopicModel.class, null, response, error);
        addReq(req, GET_INFO_TOP_HIT);
    }

    public void getTopHit(int page, int num, Response.Listener<RestAllTopic> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_TOP_HIT));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllTopic> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestAllTopic.class, null, response, error);
        addReq(req, GET_TOP_HIT);
    }

    public void getHotPlaylist(int page, int num
            , Response.Listener<RestAllPlaylist> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_HOT_PLAYLIST));
        params.addParam("page", page);
        params.addParam("num", num);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestAllPlaylist> req = new GsonRequest<>(Request.Method.GET,
                params.toString(), RestAllPlaylist.class, null, response, error);
        addReq(req, GET_HOT_PLAYLIST);
    }

    public void getAlbumInfo(long id, String identify, int type
            , Response.Listener<RestModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_ALBUM_INFO));
        if (id > 0)
            params.addParam("id", id);
        else if (!TextUtils.isEmpty(identify))
            params.addParam("identify", identify);
        params.addParam("Platform", "Mocha");
//        params.addParam("type", type);
        GsonRequest<RestModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestModel.class, null, response, error);
        addReq(req, GET_ALBUM_INFO);
    }

    public void getPlaylistInfo(long id, String identify, int type
            , Response.Listener<RestPlaylist> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_PLAYLIST_INFO));
        if (id > 0)
            params.addParam("id", id);
        else if (!TextUtils.isEmpty(identify))
            params.addParam("identify", identify);
        params.addParam("Platform", "Mocha");
//        params.addParam("type", type);
        GsonRequest<RestPlaylist> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestPlaylist.class, null, response, error);
        addReq(req, GET_PLAYLIST_INFO);
    }

    public GsonRequest getVideo(long id, String identify, Response.Listener<RestModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_VIDEO));
        if (id > 0)
            params.addParam("id", id);
        else if (!TextUtils.isEmpty(identify))
            params.addParam("identify", identify);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestModel.class, null, response, error);
        addReq(req, GET_VIDEO);
        return req;
    }

    public GsonRequest getSong(long id, String identify, Response.Listener<RestModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(
                getApiUrl(GET_SONG));
        if (id > 0)
            params.addParam("id", id);
        else if (!TextUtils.isEmpty(identify))
            params.addParam("identify", identify);
        params.addParam("Platform", "Mocha");
        GsonRequest<RestModel> req = new GsonRequest<>(Request.Method.GET, params.toString(), RestModel.class, null, response, error);
        addReq(req, GET_SONG);
        return req;
    }

    //todo WSRestFullService --------------------------------------------------------

    public GsonRequest getTopSong(int num, int page, Response.Listener<RestTopModel> response, Response.ErrorListener error) {
        ResfulString params = new ResfulString(UrlConfigHelper.getInstance(application).
                getUrlConfigOfServiceKeeng(Config.UrlKeengEnum.SERVICE_GET_TOP_SONG));
        params.addParam("num", num);
        params.addParam("page", page);
        Log.d(TAG, "params: " + params);
        GsonRequest<RestTopModel> req = new GsonRequest<>(
                Request.Method.GET, params.toString(), RestTopModel.class, null,
                response, error);
        req.setShouldCache(true);
        VolleyHelper.getInstance(application).addRequestToQueue(req, "TAG_GET_TOP_SONG", true);
        return req;
    }

    public void getSearchSong(String queryKey,
                              Response.Listener<ResfulSearchQueryObj> response,
                              Response.ErrorListener error) {
        String need = UrlConfigHelper.getInstance(application).
                getUrlConfigOfMediaSearchKeeng(Config.UrlKeengEnum.MEDIA2_SEARCH_SUGGESTION);
        ResfulString resfulString = new ResfulString(need, true);
        resfulString.addParam("q", queryKey);
        //TODO add params cho các thị trường khác vn
        resfulString.addParam("wt", "json");
        resfulString.addParam("fl", "id,score,listen_no,type,full_name,full_singer,image");
        resfulString.addParam("indent", true);
        resfulString.addParam("group", true);
        resfulString.addParam("qt", "artistAutoComplete");
        resfulString.addParam("group.field", "type");
        resfulString.addParam("group.limit", 10);
        resfulString.addParam("sort", "score desc,listen_no desc");
        Log.i(TAG, "url getSearchSong: " + resfulString.toString());
        GsonRequest<ResfulSearchQueryObj> gsonRequest = new GsonRequest<>(
                Request.Method.GET, resfulString.toString(), ResfulSearchQueryObj.class,
                null, response, error);
        VolleyHelper.getInstance(application).addRequestToQueue(gsonRequest, "TAG_SEARCH_SONG", false);
    }
}

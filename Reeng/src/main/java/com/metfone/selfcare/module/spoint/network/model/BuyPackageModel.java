package com.metfone.selfcare.module.spoint.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BuyPackageModel implements Serializable {
    @SerializedName("list")
    @Expose
    private List<PackageModel> list;

    public List<PackageModel> getList() {
        if(list == null){
            list = new ArrayList<>();
        }
        return list;
    }

    public void setList(List<PackageModel> list) {
        this.list = list;
    }
}

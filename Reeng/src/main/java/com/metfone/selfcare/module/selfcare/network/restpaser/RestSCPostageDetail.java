package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCPostageDetail;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCPostageDetail extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCPostageDetail> data;

    public ArrayList<SCPostageDetail> getData() {
        return data;
    }

    public void setData(ArrayList<SCPostageDetail> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCPostageDetail [data=" + data + "] errror " + getErrorCode();
    }
}

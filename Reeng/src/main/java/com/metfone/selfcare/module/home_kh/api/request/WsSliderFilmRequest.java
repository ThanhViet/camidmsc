package com.metfone.selfcare.module.home_kh.api.request;


import com.metfone.selfcare.network.metfoneplus.BaseRequest;

import lombok.Data;

@Data
public class WsSliderFilmRequest extends BaseRequest<WsSliderFilmRequest.WsRequest> {
    public static class WsRequest {
        public String language;
    }
}

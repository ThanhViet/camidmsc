package com.metfone.selfcare.module.selfcare.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.adapter.category.ViewPagerDetailAdapter;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.module.selfcare.event.SCAccountEvent;
import com.metfone.selfcare.module.selfcare.model.SCBundle;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SCMyPackageFragment extends BaseFragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView btnBack;
    private TextView tvTitle;
    private ViewPagerDetailAdapter adapter;
    private int type;
    SCRecommentPackageFragment recommentPackageFragment;
    SCRecommentServiceFragment recommentServiceFragment;
    SCRegisterPackageFragment registerPackageFragment;

    public static SCMyPackageFragment newInstance(Bundle bundle) {
        SCMyPackageFragment fragment = new SCMyPackageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public String getName() {
        return "SCMyPackageFragment";
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_my_package;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        initView(view);

        loadData();

        return view;
    }

    private void initView(View view) {
        Bundle bundleData = getArguments();
        SCBundle data = (SCBundle) bundleData.getSerializable(Constants.KEY_DATA);
        type = data.getType();

        btnBack = view.findViewById(R.id.btnBack);
        tvTitle = view.findViewById(R.id.tvTitle);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, new SCBundle(type));

        adapter = new ViewPagerDetailAdapter(getChildFragmentManager());
        registerPackageFragment = SCRegisterPackageFragment.newInstance(bundle);
        if(type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE)
        {
            recommentPackageFragment = SCRecommentPackageFragment.newInstance(bundle);
            adapter.addFragment(recommentPackageFragment, type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE ? mActivity.getString(R.string.sc_packages) : mActivity.getString(R.string.sc_services));
            adapter.addFragment(registerPackageFragment, type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE ? mActivity.getString(R.string.sc_registered) : mActivity.getString(R.string.sc_subscribed));
        }
        else
        {
            recommentServiceFragment = SCRecommentServiceFragment.newInstance(bundle);
            adapter.addFragment(recommentServiceFragment, type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE ? mActivity.getString(R.string.sc_packages) : mActivity.getString(R.string.sc_services));
            adapter.addFragment(registerPackageFragment, type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE ? mActivity.getString(R.string.sc_registered) : mActivity.getString(R.string.sc_subscribed));
        }

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

        if(type == SCConstants.SCREEN_TYPE.TYPE_PACKAGE)
            tvTitle.setText(mActivity.getString(R.string.sc_my_package));
        else if(type == SCConstants.SCREEN_TYPE.TYPE_SERVICES)
            tvTitle.setText(mActivity.getString(R.string.sc_my_services));
    }

    private void loadData()
    {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(final SCAccountEvent event) {
        if (event != null) {
            if (event.getEvent() == SCAccountEvent.UPDATE_INFO) {
                if(recommentPackageFragment != null)
                    recommentPackageFragment.onRefresh();
                if(recommentServiceFragment != null)
                    recommentServiceFragment.onRefresh();
                if(registerPackageFragment != null)
                    registerPackageFragment.onRefresh();
            }
            EventBus.getDefault().removeStickyEvent(event);
        }
    }
}

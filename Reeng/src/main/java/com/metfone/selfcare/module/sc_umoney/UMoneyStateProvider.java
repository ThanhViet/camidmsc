package com.metfone.selfcare.module.sc_umoney;

import com.metfone.selfcare.module.sc_umoney.network.model.FieldMap;
import com.metfone.selfcare.module.sc_umoney.network.model.Info;

import java.util.ArrayList;
import java.util.List;

public class UMoneyStateProvider {
    private static UMoneyStateProvider mInstance = null;
    private List<FieldMap> data;
    private Info info;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public static UMoneyStateProvider getInstance() {
        if (mInstance == null) {
            //prevent multi instance creator in Multi thread.
            synchronized (UMoneyStateProvider.class) {
                if (mInstance == null) {
                    mInstance = new UMoneyStateProvider();
                }
            }
        }
        return mInstance;
    }

    public List<FieldMap> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<FieldMap> data) {
        this.data = data;
    }
}

package com.metfone.selfcare.module.home_kh.fragment;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.util.Log;

public abstract class RewardBaseFragment extends BaseFragment {
    protected boolean menuShowBefore = false;

    protected abstract boolean needShowBottomMenu();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        menuShowBefore = menuIsShowing();
        logActiveReward();
    }

    private boolean menuIsShowing() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() == View.VISIBLE) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(needShowBottomMenu()){
            showBottomMenu();
        }else{
            hideBottomMenu();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        showMenuAgain();
    }

    protected void showMenuAgain(){
        if(menuShowBefore){
            showBottomMenu();
        }else{
            hideBottomMenu();
        }
    }

    protected void hideBottomMenu() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() == View.VISIBLE) {
                bottomNavigationBar.setVisibility(View.GONE);
            }
        }
    }

    protected void showBottomMenu() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            BottomNavigationBar bottomNavigationBar = ((HomeActivity) activity).getNavigationBar();
            if (bottomNavigationBar != null && bottomNavigationBar.getVisibility() != View.VISIBLE) {
                bottomNavigationBar.setVisibility(View.VISIBLE);
            }
        }
    }
    protected int getNavigationBarHeight() {
        Activity activity = getActivity();
        Rect rectangle = new Rect();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        return displayMetrics.heightPixels - (rectangle.top + rectangle.height());
    }

    public void popBackStackFragment() {
        showMenuAgain();
        super.popBackStackFragment();
    }

    private void logActiveReward() {
        if (!FirebaseEventBusiness.activeTab[FirebaseEventBusiness.TabLog.TAB_REWARD]) {
            ApplicationController.self().getFirebaseEventBusiness().logActiveTab(FirebaseEventBusiness.TabLog.TAB_REWARD);
        }
        ApplicationController.self().getFirebaseEventBusiness().logSelectTab("Reward");
    }
}

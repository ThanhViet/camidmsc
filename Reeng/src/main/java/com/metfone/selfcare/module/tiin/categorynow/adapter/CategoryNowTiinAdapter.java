package com.metfone.selfcare.module.tiin.categorynow.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.keeng.widget.DividerItemDecoration;
import com.metfone.selfcare.module.newdetails.view.BaseQuickAdapter;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.tiin.base.event.TiinListener;
import com.metfone.selfcare.module.tiin.network.model.TiinEventModel;

import java.util.List;

public class CategoryNowTiinAdapter extends BaseQuickAdapter<TiinEventModel, BaseViewHolder> {
    private Context mContext;
    private List<TiinEventModel> data;
    private TiinListener.onCategoryNow listener;
    public CategoryNowTiinAdapter(Context context, int id, List<TiinEventModel> datas, TiinListener.onCategoryNow listener) {
        super(id, datas);
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void convert(BaseViewHolder holder, TiinEventModel model) {
        if (holder == null || model == null) {
            return;
        }
        if (holder.getView(R.id.tv_header) != null) {
            holder.setText(R.id.tv_header, model.getThematicName().toUpperCase());
        }
        if (holder.getView(R.id.tv_view_all) != null) {
            holder.setOnClickListener(R.id.tv_view_all, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClickHeader(model.getThematicName(),model.getThematicId());
                }
            });
        }
        if (model.getData().size() > 0) {
            RecyclerView recyclerView = holder.getView(R.id.recycler_view);
            recyclerView.setNestedScrollingEnabled(false);
            if (recyclerView.getItemDecorationCount() <= 0) {
                recyclerView.setLayoutManager(new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                recyclerView.addItemDecoration(new DividerItemDecoration(mContext, R.drawable.divider_default_tiin, true));
            }
            CategoryNowTiinItemAdapter childHomeNewAdapterV5 = new CategoryNowTiinItemAdapter(model, mContext, listener);
            recyclerView.setAdapter(childHomeNewAdapterV5);
        }
    }

}

package com.metfone.selfcare.module.home_kh.api.request;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.network.metfoneplus.BaseRequest;

public class WsGetAllTelecomGiftForFriendRequest extends BaseRequest<WsGetListGiftReceivedRequest.Request> {
    public static class WsRequest extends WsGetListGiftReceivedRequest.Request {
        @SerializedName("language")
        String language;
        public WsRequest(String language) {
            this.language = language;
        }
    }
}

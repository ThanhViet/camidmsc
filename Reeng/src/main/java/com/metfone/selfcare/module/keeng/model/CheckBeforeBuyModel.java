package com.metfone.selfcare.module.keeng.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CheckBeforeBuyModel implements Serializable {
    private static final long serialVersionUID = -4341682650383776135L;
    @SerializedName("sub_type")
    public int subType;
    @SerializedName("item_type")
    public int itemType = 0;
    @SerializedName("item_id")
    public int itemId = 0;
    @SerializedName("is_sv")
    public int is_sv;
    @SerializedName("sub_name")
    public String subName;
    @SerializedName("downloadable")
    public int downloadable = 0;
    @SerializedName("download_price")
    public int downloadPrice = 0;
    @SerializedName("song_list")
    public ArrayList<AllModel> itemList;
    @SerializedName("song_rbt_code")
    public String rbtCode = "";
    @SerializedName("song_rbt_price")
    public int rbtPrice = 0;
    @SerializedName("song_ringtone_url")
    public String ringtoneUrl = "";
    @SerializedName("sub_price_first")
    public String subPriceFirst = "";
    @SerializedName("song_ringtone_mess")
    public String ringtoneMessage = "";
    @SerializedName("user_is_vip")
    private int userIsVip = 0;

    public boolean isDownloadable() {
        if (downloadable == 1 && itemList != null && !itemList.isEmpty())
            return true;
        return false;
    }

    public ArrayList<AllModel> getSongList() {
        return itemList;
    }

    public AllModel getRingToneItem() {
        if (itemList != null && !itemList.isEmpty())
            return itemList.get(0);
        return null;
    }

    public boolean isVip() {
        if (userIsVip == 1)
            return true;
        return false;
    }

    public boolean isNonVip() {
        if (userIsVip == 0)
            return true;
        return false;
    }

    public boolean isRBT() {
        if (!TextUtils.isEmpty(rbtCode))
            return true;
        return false;
    }

    // ----------------song
    public boolean isFreeDownload() {
        if (downloadPrice <= 0)
            return true;
        return false;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public int getDownloadable() {
        return downloadable;
    }

    public void setDownloadable(int downloadable) {
        this.downloadable = downloadable;
    }

    public int getDownloadPrice() {
        return downloadPrice;
    }

    public void setDownloadPrice(int downloadPrice) {
        this.downloadPrice = downloadPrice;
    }

    public ArrayList<AllModel> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<AllModel> itemList) {
        this.itemList = itemList;
    }

    public String getRbtCode() {
        return rbtCode;
    }

    public void setRbtCode(String rbtCode) {
        this.rbtCode = rbtCode;
    }

    public int getRbtPrice() {
        return rbtPrice;
    }

    public void setRbtPrice(int rbtPrice) {
        this.rbtPrice = rbtPrice;
    }

    public String getRingtoneUrl() {
        return ringtoneUrl;
    }

    public void setRingtoneUrl(String ringtoneUrl) {
        this.ringtoneUrl = ringtoneUrl;
    }

    public String getSubPriceFirst() {
        return subPriceFirst;
    }

    public void setSubPriceFirst(String subPriceFirst) {
        this.subPriceFirst = subPriceFirst;
    }

    public String getRingtoneMessage() {
        return ringtoneMessage;
    }

    public void setRingtoneMessage(String ringtoneMessage) {
        this.ringtoneMessage = ringtoneMessage;
    }

    @Override
    public String toString() {
        return "CheckBeforeBuyModel{" +
                "itemType=" + itemType +
                ", itemId=" + itemId +
                ", downloadable=" + downloadable +
                ", downloadPrice=" + downloadPrice +
                ", itemList=" + itemList +
                ", rbtCode='" + rbtCode + '\'' +
                ", rbtPrice=" + rbtPrice +
                ", ringtoneUrl='" + ringtoneUrl + '\'' +
                ", subPriceFirst='" + subPriceFirst + '\'' +
                ", ringtoneMessage='" + ringtoneMessage + '\'' +
                ", userIsVip=" + userIsVip +
                '}';
    }
}
package com.metfone.selfcare.module.metfoneplus.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.AccountOcsValue;
import com.metfone.selfcare.module.metfoneplus.adapter.DetailAccountAdapter;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsGetAccountsOcsDetailResponse;
import com.metfone.selfcare.util.Utilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Response;

public class MPDetailAccountFragment extends MPBaseFragment {
    public static final String TAG = "MPDetailAccountFragment";

    @BindView(R.id.mp_layout_action_bar)
    RelativeLayout mLayoutActionBar;
    @BindView(R.id.action_bar_title)
    AppCompatTextView mActionBarTitle;
    @BindView(R.id.list_detail_history_charge)
    RecyclerView mListDetailCharge;

    private DetailAccountAdapter mDetailAccountAdapter;

    public MPDetailAccountFragment() {

    }

    public static MPDetailAccountFragment newInstance() {
        MPDetailAccountFragment fragment = new MPDetailAccountFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mp_account_detail;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActionBarTitle.setText(getString(R.string.m_p_item_detail_account_screen));
        Utilities.adaptViewForInserts(mLayoutActionBar);

        mDetailAccountAdapter = new DetailAccountAdapter(getContext(), new ArrayList<AccountOcsValue>());
        mListDetailCharge.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mListDetailCharge.setAdapter(mDetailAccountAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getWSGetAccountsOcsDetail();
    }

    @OnClick(R.id.action_bar_back)
    void onBack() {
        popBackStackFragment();
    }

    private void getWSGetAccountsOcsDetail() {
        mParentActivity.showLoadingDialog("", R.string.waiting);
        MetfonePlusClient client = new MetfonePlusClient();
        client.wsGetAccountsOcsDetail(new MPApiCallback<WsGetAccountsOcsDetailResponse>() {
            @Override
            public void onResponse(Response<WsGetAccountsOcsDetailResponse> response) {
                if (response.body() != null
                        && MetfonePlusClient.ERROR_CODE_SUCCESS.equals(response.body().getErrorCode())) {
                    if(response.body().getResult().getWsResponse() != null){
                        List<WsGetAccountsOcsDetailResponse.Response> wsResponse = response.body().getResult().getWsResponse();
                        for (WsGetAccountsOcsDetailResponse.Response res : wsResponse) {
                            // Get value of item have "title" = "ACCOUNTS"
                            if (res.getTitle().equals(getString(R.string.m_p_detail_title_compare_api_account))) {
                                if (res.getAccountOcsValues() == null || res.getAccountOcsValues().size() == 0) {
                                    MPDialogFragment errorDialog = new MPDialogFragment.Builder()
                                        .setTitle(R.string.m_p_dialog_error_title)
                                        .setContent(getString(R.string.m_p_no_data))
                                        .setCancelableOnTouchOutside(true)
                                        .build();
                                    errorDialog.show(getChildFragmentManager(), null);
                                } else {
                                    mDetailAccountAdapter.replaceData(res.getAccountOcsValues());
                                }
                            }
                        }
                    }

                } else {
                    Log.e(TAG, "getWSGetAccountsOcsDetail - onResponse: error");
                }

                mParentActivity.hideLoadingDialog();
            }

            @Override
            public void onError(Throwable error) {
                mParentActivity.hideLoadingDialog();
            }
        });
    }
}

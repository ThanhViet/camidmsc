package com.metfone.selfcare.module.newdetails.interfaces;

import com.metfone.selfcare.module.newdetails.view.MvpView;

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(Exception error);

    void setUserAsLoggedOut();
}

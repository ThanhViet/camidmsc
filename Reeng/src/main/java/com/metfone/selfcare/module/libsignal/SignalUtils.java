package com.metfone.selfcare.module.libsignal;

import com.metfone.selfcare.util.Log;

import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.InvalidKeyException;
import org.whispersystems.libsignal.ecc.Curve;
import org.whispersystems.libsignal.ecc.ECKeyPair;
import org.whispersystems.libsignal.ecc.ECPublicKey;
import org.whispersystems.libsignal.state.PreKeyBundle;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class SignalUtils {
    private static final int REGISTRATION_ID_INDEX = 0;
    private static final int DEVICE_ID_INDEX = 1;
    private static final int PREKEY_ID_INDEX = 2;
    private static final int PREKEY_PUBLIC_INDEX = 3;
    private static final int SIGNED_PREKEY_ID_INDEX = 4;
    private static final int SIGNED_PREKEY_PUBLIC_INDEX = 5;
    private static final int SIGNED_PREKEY_SIGNATURE_INDEX = 6;
    private static final int IDENTITY_KEY_INDEX = 7;

    public static String preKeyBundle2String(PreKeyBundle pPreKeys) throws IOException {
//        StringBuffer buf = new StringBuffer();
//        buf.append(pPreKeys.getRegistrationId()).append("|");
//        buf.append(pPreKeys.getDeviceId()).append("|");
//        buf.append(pPreKeys.getPreKeyId()).append("|");
//        buf.append(Base64.encodeBytes(pPreKeys.getPreKey().serialize())).append("|");
//        buf.append(pPreKeys.getSignedPreKeyId()).append("|");
//        buf.append(Base64.encodeBytes(pPreKeys.getSignedPreKey().serialize())).append("|");
//        buf.append(Base64.encodeBytes(pPreKeys.getSignedPreKeySignature())).append("|");
//        buf.append(Base64.encodeBytes(pPreKeys.getIdentityKey().serialize()));
//
//        return buf.toString();


        ByteArrayOutputStream baos = null;
        DataOutputStream out = null;
        byte[] data = null;
        byte[] serialize = null;
        try {
            baos = new ByteArrayOutputStream();
            out = new DataOutputStream(baos);
            // FORMAT:
            // 4BYTE|4BYTE|4BYTE|4BYTE_LENGTH|DATA|4BYTE|4BYTE_LENGTH|DATA|4BYTE_LENGTH|DATA|4BYTE_LENGTH|DATA

            // RegistrationId
            out.writeInt(pPreKeys.getRegistrationId());

            // DeviceId
            out.writeInt(pPreKeys.getDeviceId());

            // PreKeyId
            out.writeInt(pPreKeys.getPreKeyId());

            // PreKey
            serialize = pPreKeys.getPreKey().serialize();
            out.writeInt(serialize.length);
            out.write(serialize);

            // SignedPreKeyId
            out.writeInt(pPreKeys.getSignedPreKeyId());

            // SignedPreKey
            serialize = pPreKeys.getSignedPreKey().serialize();
            out.writeInt(serialize.length);
            out.write(serialize);

            // SignedPreKeySignature
            serialize = pPreKeys.getSignedPreKeySignature();
            out.writeInt(serialize.length);
            out.write(serialize);

            // IdentityKey
            serialize = pPreKeys.getIdentityKey().serialize();
            out.writeInt(serialize.length);
            out.write(serialize);

            data = baos.toByteArray();
            return Base64.encodeBytes(data);
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        } finally {
            if (baos != null) {
                baos.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }

    public static PreKeyBundle string2PreKeyBundle(String data) throws IOException, InvalidKeyException {
//        try {
//            String arr[] = data.split("\\|");
//            int registrationId = Integer.parseInt(arr[REGISTRATION_ID_INDEX]);
//            int deviceId = Integer.parseInt(arr[DEVICE_ID_INDEX]);
//            int preKeyId = Integer.parseInt(arr[PREKEY_ID_INDEX]);
//            byte[] preKeyPublic_b = Base64.decode(arr[PREKEY_PUBLIC_INDEX]);
//            int signedPreKeyId = Integer.parseInt(arr[SIGNED_PREKEY_ID_INDEX]);
//            byte[] signedPreKeyPublic_s = Base64.decode(arr[SIGNED_PREKEY_PUBLIC_INDEX]);
//            byte[] signedPreKeySignature = Base64.decode(arr[SIGNED_PREKEY_SIGNATURE_INDEX]);
//            byte[] identityKey_s = Base64.decode(arr[IDENTITY_KEY_INDEX]);
//
//            ECPublicKey preKeyPublic = Curve.decodePoint(preKeyPublic_b, 0);
//            ECPublicKey signedPreKeyPublic = Curve.decodePoint(signedPreKeyPublic_s, 0);
//            IdentityKey identityKey = new IdentityKey(identityKey_s, 0);
//
//            return new PreKeyBundle(registrationId, deviceId, preKeyId, preKeyPublic, signedPreKeyId, signedPreKeyPublic,
//                    signedPreKeySignature, identityKey);
//        }
//        catch (InvalidKeyException ex)
//        {
//
//        }
//        catch (IOException ex)
//        {
//
//        }
//        return null;
        DataInputStream in = null;
        try {
            byte[] data2Bytes = Base64.decode(data);
            in = new DataInputStream(new ByteArrayInputStream(data2Bytes));

            // FORMAT:
            // 4BYTE|4BYTE|4BYTE|4BYTE_LENGTH|DATA|4BYTE|4BYTE_LENGTH|DATA|4BYTE_LENGTH|DATA|4BYTE_LENGTH|DATA

            // RegistrationId
            int registrationId = in.readInt();

            // DeviceId
            int deviceId = in.readInt();

            // PreKeyId
            int preKeyId = in.readInt();

            // PreKey
            int dataLength = in.readInt();
            byte[] preKeyPublic_b = new byte[dataLength];
            in.read(preKeyPublic_b, 0, dataLength);

            // SignedPreKeyId
            int signedPreKeyId = in.readInt();

            // SignedPreKey
            dataLength = in.readInt();
            byte[] signedPreKeyPublic_s = new byte[dataLength];
            in.read(signedPreKeyPublic_s, 0, dataLength);

            // SignedPreKeySignature
            dataLength = in.readInt();
            byte[] signedPreKeySignature = new byte[dataLength];
            in.read(signedPreKeySignature, 0, dataLength);

            // IdentityKey
            dataLength = in.readInt();
            byte[] identityKey_s = new byte[dataLength];
            in.read(identityKey_s, 0, dataLength);

            ECPublicKey preKeyPublic = Curve.decodePoint(preKeyPublic_b, 0);
            ECPublicKey signedPreKeyPublic = Curve.decodePoint(signedPreKeyPublic_s, 0);
            IdentityKey identityKey = new IdentityKey(identityKey_s, 0);

            return new PreKeyBundle(registrationId, deviceId, preKeyId, preKeyPublic, signedPreKeyId,
                    signedPreKeyPublic, signedPreKeySignature, identityKey);

        } catch (IOException e) {
            throw e;
        } catch (InvalidKeyException ie) {
            throw ie;
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }


    public static PreKeyBundle getPreKey(int preKeyId, int signedPreKeyId, SignalProtocolStore store) {
        PreKeyBundle preKey = null;
        try {
            IdentityKeyPair identityKeyPair = store.getIdentityKeyPair();
            int registrationId = store.getLocalRegistrationId();

            ECKeyPair preKeyPair = Curve.generateKeyPair();
            ECKeyPair signedPreKeyPair = Curve.generateKeyPair();
            int deviceId = 1;

            byte[] signedPreKeySignature = Curve.calculateSignature(
                    identityKeyPair.getPrivateKey(),
                    signedPreKeyPair.getPublicKey().serialize());

            IdentityKey identityKey = identityKeyPair.getPublicKey();
            ECPublicKey preKeyPublic = preKeyPair.getPublicKey();
            ECPublicKey signedPreKeyPublic = signedPreKeyPair.getPublicKey();

            preKey = new PreKeyBundle(
                    registrationId,
                    deviceId,
                    preKeyId,
                    preKeyPublic,
                    signedPreKeyId,
                    signedPreKeyPublic,
                    signedPreKeySignature,
                    identityKey);

        } catch (InvalidKeyException e) {
            Log.e("E2E", "E2E--" + e.getMessage());
        }

        return preKey;
    }
}

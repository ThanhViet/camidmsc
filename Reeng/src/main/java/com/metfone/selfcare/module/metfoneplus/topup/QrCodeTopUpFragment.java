package com.metfone.selfcare.module.metfoneplus.topup;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.zxing.Result;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.databinding.QrCodeTopUpFragment2Binding;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PermissionHelper;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.metfoneplus.base.MPBaseBindingFragment;
import com.metfone.selfcare.module.metfoneplus.dialog.MPDialogFragment;
import com.metfone.selfcare.module.metfoneplus.topup.dialog.ConfirmationTopUpDialog;
import com.metfone.selfcare.module.metfoneplus.topup.dialog.TopupPayQrCodeBottomSheet;
import com.metfone.selfcare.module.metfoneplus.topup.model.request.TopUpQrCodeRequest;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.BaseResponseMP;
import com.metfone.selfcare.module.metfoneplus.topup.model.response.TopUpQrCodeResponse;
import com.metfone.selfcare.network.camid.ApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.utils.ToastUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;
import static com.metfone.selfcare.module.metfoneplus.topup.model.Constants.CALL_PAY;
import static com.metfone.selfcare.module.metfoneplus.topup.model.Constants.CANCEL;
import static com.metfone.selfcare.module.metfoneplus.topup.model.Constants.CONFIRM;

public class QrCodeTopUpFragment extends MPBaseBindingFragment<QrCodeTopUpFragment2Binding> implements ZXingScannerView.ResultHandler, View.OnClickListener {
    public static final String NUMBER_PHONE_KEY = "phone_key";

    private static final int ZXING_CAMERA_PERMISSION = 1002;
    private static final int ACTION_GOTO_SETTING = 1003;
    private static final String PERMISSION_CAMERA_KEY = "permission_camera_key";
    private ZXingScannerView mScannerView;
    private MetfonePlusClient client = new MetfonePlusClient();
    private TopUpQrCodeRequest mTopUpQrCodeRequest = new TopUpQrCodeRequest();
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private UserInfo currentUser;
    private ConfirmationTopUpDialog mConfirmationTopUpDialog;
    private String mQrCodeSerial;
    private UserInfoBusiness getUserInfoBusiness = new UserInfoBusiness(getContext());
    private String regex = "^\\*199\\*\\d*#";
    private Pattern pattern = Pattern.compile(regex);
    private String syntax = "tel:";
    private String mQrCodeCall = "";
    private SharedPrefs mSharedPrefs;
    private boolean mIsTurnOnCameraTapped = false;
    private String phoneNumber;
    private String userPhoneNumber;

    public static QrCodeTopUpFragment newInstance(String numberPhone) {
        QrCodeTopUpFragment fragment = new QrCodeTopUpFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NUMBER_PHONE_KEY, numberPhone);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static QrCodeTopUpFragment newInstance() {
        Bundle args = new Bundle();
        QrCodeTopUpFragment fragment = new QrCodeTopUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mParentActivity != null && mParentActivity.getWindow() != null) {
            mParentActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
        userPhoneNumber = SharedPrefs.getInstance().get(Constants.PREFERENCE.PREF_WS_ISDN, String.class);
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(NUMBER_PHONE_KEY);
        }else{
            phoneNumber = userPhoneNumber;
        }

        if (phoneNumber != null) {
            phoneNumber = phoneNumber.replace(" ", "");
            if (phoneNumber.charAt(0) == '0') {
                phoneNumber = phoneNumber.substring(1);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.qr_code_top_up_fragment2;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utilities.adaptViewForInserts(mBinding.viewRootlayout);
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.GONE);
        }
        mSharedPrefs = SharedPrefs.getInstance();

        initView();
        initListener();
    }

    private void initRequest(String serial) {
        currentUser = userInfoBusiness.getUser();
        mTopUpQrCodeRequest.setDesIsdn(phoneNumber);
        mTopUpQrCodeRequest.setIsdn(userPhoneNumber);
        mTopUpQrCodeRequest.setSerial(serial);
    }


    private void initView() {
        mScannerView = new ZXingScannerView(getContext());
        handleCameraPermission();
        //--------------------------- EVENT EditText Change-------------------------------
        mBinding.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    mScannerView.resumeCameraPreview(QrCodeTopUpFragment.this);
                    mBinding.editText.setSelection(charSequence.toString().length());
                    mBinding.btnNext.setEnabled(true);
                } else {
                    mBinding.btnNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        com.metfone.selfcare.module.metfoneplus.topup.model.Constants.setupUI(mBinding.viewRootlayout, getActivity());
    }

    private void initListener() {
        mBinding.btnBack.setOnClickListener(this);
        mBinding.btnNext.setOnClickListener(this);
        mBinding.turnOnCamera.setOnClickListener(this);
    }

    private void handleCameraPermission() {
        if (PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.CAMERA)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            openQRScan();
        }
    }

    private void openQRScan() {
        mBinding.frameLayout.post(() -> {
            mBinding.frameLayout.setVisibility(View.VISIBLE);
            mBinding.frameLayoutRequestPermission.setVisibility(View.GONE);
            ViewGroup contentFrame = mBinding.frameLayout;
            contentFrame.addView(mScannerView);
            mScannerView.setAutoFocus(true);
            mScannerView.setAspectTolerance(0.5f);
            if (mScannerView != null) {
                mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();// Start camera on resume
            }
        });
    }

    @Override
    public void handleResult(Result result) {
        mQrCodeSerial = result.getText();
        if (mQrCodeSerial.contains(syntax)) {
            mQrCodeCall = mQrCodeSerial.replace(syntax, "");
            mQrCodeCall = mQrCodeCall.replace("%23", "#");
        }
        Matcher matcher = pattern.matcher(mQrCodeCall.trim());
        boolean match = matcher.matches();
        if (match)
            new TopupPayQrCodeBottomSheet(getActivity(), mQrCodeCall, getActivity(), QrCodeTopUpFragment.this::RunUi).show();
        else {
            Toast.makeText(mApplication, getString(R.string.m_p_top_up_the_card_is_valid), Toast.LENGTH_SHORT).show();
            resumeQRCamera();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PermissionHelper.declinedPermission(mParentActivity, Manifest.permission.CAMERA)) {
            resumeQRCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBack();
                break;
            case R.id.btnNext:
                String ssID = Utilities.formatPhoneNumberCambodia(phoneNumber);
                String serial = String.valueOf(mBinding.editText.getText());
                StringBuilder messPopUp = new StringBuilder(getString(R.string.m_p_top_up_dialog_qr_topup_phone));
                messPopUp.append(" ");
                messPopUp.append(ssID).append(".");
                messPopUp.append("\n");
                messPopUp.append(getString(R.string.m_p_top_up_dialog_qr_topup_serial));
                messPopUp.append(" ");
                messPopUp.append(serial);

                new ConfirmationTopUpDialog(messPopUp.toString(), getActivity(), this::RunUi).show(getChildFragmentManager(), "");
                break;
            case R.id.turnOnCamera:
                mIsTurnOnCameraTapped = true;
                handleCameraPermission();
                break;
        }
    }

    private void RunUi(Object... objects) {
        String result = (String) objects[0];
        if (result.equals(CONFIRM)) {
            initRequest(mBinding.editText.getText().toString().trim());
            requestTrans();
        }
        if (result.equals(CALL_PAY)) {

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(mQrCodeSerial));

            if (ContextCompat.checkSelfPermission(getActivity(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(callIntent);
            } else {
                requestPermissions(new String[]{CALL_PHONE}, 1);
            }
        }
        if (result.equals(CANCEL)) {
            resumeQRCamera();
        }
    }

    private void requestTrans() {
        client.qrCodeTopUpMP(mTopUpQrCodeRequest, new ApiCallback<BaseResponseMP<TopUpQrCodeResponse>>() {
            @Override
            public void onResponse(Response<BaseResponseMP<TopUpQrCodeResponse>> response) {
                // Thanh Cong
                // response != null
                if (response.body() != null && response.body().getData() != null) {
                    String errorCode = response.body().getData().getErrorCode();
                    // errorCode = 0
                    if (errorCode.equals("0")) {
                        ApplicationController.self().getFirebaseEventBusiness().logTopupQRCode(true,mTopUpQrCodeRequest.getSerial());
                        ToastUtils.showToast(getContext(), getString(R.string.m_p_successfully));
                        onBack();
                    } else {
                        // errorCode != 0
                        ApplicationController.self().getFirebaseEventBusiness().logTopupQRCode(false,mTopUpQrCodeRequest.getSerial());
                        ToastUtils.showToast(getContext(), response.body().getData().getMessage() != null ? response.body().getData().getMessage() : getContext().getString(R.string.m_p_notify_error_in_processing));
                    }
                } else {
                    //response = null
                    ToastUtils.showToast(getContext(), getContext().getString(R.string.m_p_notify_error_in_processing));
                }
            }

            @Override
            public void onError(Throwable error) {
                ToastUtils.showToast(getContext(), getContext().getString(R.string.m_p_notify_error_in_processing));
            }
        });
    }

    private void resumeQRCamera() {
        if (mScannerView != null) {
            mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView.startCamera();// Start camera on resume
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openQRScan();
                } else {
                    mBinding.frameLayout.setVisibility(View.GONE);
                    mBinding.frameLayoutRequestPermission.setVisibility(View.VISIBLE);
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        if (mSharedPrefs.get(PERMISSION_CAMERA_KEY, Boolean.class)) {
                            if (mIsTurnOnCameraTapped) {
                                mIsTurnOnCameraTapped = false;
                                showCameraPermissionDialog();
                            }
                        } else {
                            mSharedPrefs.put(PERMISSION_CAMERA_KEY, true);
                        }
                    }
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ACTION_GOTO_SETTING && resultCode == 0
                && ContextCompat.checkSelfPermission(mParentActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            mSharedPrefs.put(PERMISSION_CAMERA_KEY, false);
            openQRScan();
        }
    }

    private void showCameraPermissionDialog() {
        MPDialogFragment dialogSuccess = new MPDialogFragment.Builder()
                .setContent(R.string.m_p_top_up_request_camera_permission_guide)
                .setNameButtonTop(R.string.m_p_top_up_request_camera_permission_open_setting)
                .build();

        dialogSuccess.addButtonOnClickListener(new MPDialogFragment.ButtonOnClickListener() {
            @Override
            public void onTopButtonClick() {
                dialogSuccess.dismiss();
                openSettings();
            }
        });

        dialogSuccess.show(getChildFragmentManager(), null);
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mParentActivity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView = null;
    }

    public void onBack() {
        popBackStackFragment();
        if (mParentActivity instanceof HomeActivity) {
            ((HomeActivity) mParentActivity).setBottomNavigationBarVisibility(View.VISIBLE);
        }
    }
}

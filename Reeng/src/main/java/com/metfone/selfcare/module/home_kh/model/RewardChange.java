package com.metfone.selfcare.module.home_kh.model;

public class RewardChange {
    boolean isDisableButton;

    public RewardChange(boolean isDisableButton) {
        this.isDisableButton = isDisableButton;
    }

    public boolean isDisableButton() {
        return isDisableButton;
    }

    public void setDisableButton(boolean disableButton) {
        isDisableButton = disableButton;
    }
}

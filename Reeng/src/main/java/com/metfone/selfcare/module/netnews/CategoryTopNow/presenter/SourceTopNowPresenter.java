package com.metfone.selfcare.module.netnews.CategoryTopNow.presenter;

import com.google.gson.Gson;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.api.news.NetNewsApi;
import com.metfone.selfcare.module.netnews.CategoryTopNow.fragment.SourceTopNowFragment;
import com.metfone.selfcare.module.netnews.base.BasePresenter;
import com.metfone.selfcare.module.netnews.request.NewsRequest;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.response.NewsResponse;
import com.metfone.selfcare.util.Log;

public class SourceTopNowPresenter extends BasePresenter implements ISourceTopNowPresenter {
    public static final String TAG = SourceTopNowPresenter.class.getSimpleName();

    NetNewsApi mNewsApi;

    public SourceTopNowPresenter() {
        mNewsApi = new NetNewsApi(ApplicationController.self());
    }

    @Override
    public void loadData(int cateId, int page) {
        mNewsApi.getNewsBySource(new NewsRequest(cateId, page, CommonUtils.NUM_SIZE), callback);
    }

    HttpCallBack callback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            if (!isViewAttached() || !(getMvpView() instanceof SourceTopNowFragment)) {
                return;
            }
            Gson gson = new Gson();
            NewsResponse childNewsResponse = gson.fromJson(data, NewsResponse.class);

            ((SourceTopNowFragment) getMvpView()).bindData(childNewsResponse);
            ((SourceTopNowFragment) getMvpView()).loadDataSuccess(true);
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof SourceTopNowFragment)) {
                return;
            }
            ((SourceTopNowFragment) getMvpView()).loadDataSuccess(false);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };
}

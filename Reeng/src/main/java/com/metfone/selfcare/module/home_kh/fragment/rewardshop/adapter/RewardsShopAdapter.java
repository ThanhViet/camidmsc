package com.metfone.selfcare.module.home_kh.fragment.rewardshop.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.util.Utilities;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RewardsShopAdapter extends RecyclerView.Adapter<RewardsShopAdapter.FilterVH> {

    private List<GiftItems> data = new ArrayList<>();
    private IGiftsItemClick iGiftsItemClick;

    public RewardsShopAdapter(List<GiftItems> data, IGiftsItemClick iFilterClick) {
        this.data = data;
        this.iGiftsItemClick = iFilterClick;
    }

    @NonNull
    @Override
    public FilterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reward_shop_kh, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterVH holder, int position) {
        holder.bindData(position);
    }


    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public List<GiftItems> getData() {
        return data;
    }

    public void clearData() {
        data.clear();
        notifyDataSetChanged();
    }

    class FilterVH extends RecyclerView.ViewHolder {
        private LinearLayout layoutParent;
        private AppCompatImageView icon;
        private CircleImageView avatar;

        private AppCompatTextView tvDisCount;
        private AppCompatTextView tvSub;
        private AppCompatTextView tvPoint;

        public FilterVH(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon_reward_shop);
            avatar = itemView.findViewById(R.id.ic_avatar);
            tvDisCount = itemView.findViewById(R.id.tv_discount_reward_shop);
            tvSub = itemView.findViewById(R.id.tv_sub_reward_shop);
            tvPoint = itemView.findViewById(R.id.tv_point_reward_shop);
            layoutParent = itemView.findViewById(R.id.layout_parent);
        }

        public void bindData(int position) {
            GiftItems item = data.get(position);
            Context context = itemView.getContext();
            tvDisCount.setText(String.format(context.getString(R.string.reward_item_discount), item.getDiscountRate()));
            tvSub.setText(item.getGiftName());
            tvPoint.setText(item.getGiftPoint() == 0 ? ResourceUtils.getString(R.string.free) :
                    String.format(context.getString(R.string.reward_item_point), formatPoint(item.getGiftPoint())));

            //The same in layout xml
            int widthOfIconInDp = 167;

            Glide.with(context)
                .asBitmap()
                .load(item.getImageUrl())
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        int widthInPx = Utilities.convertDpToPixel(widthOfIconInDp);
                        int heightInPx = (int) (((float) resource.getHeight() / resource.getWidth()) * widthInPx);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(resource, widthInPx, heightInPx, true);
                        icon.setImageBitmap(scaledBitmap);
                        return true;
                    }
                })
                .into(icon);

            Glide.with(context)
                    .load(item.getIconUrl())
                    .into(avatar);

            layoutParent.setOnClickListener(v -> {
                if (iGiftsItemClick != null) {
                    iGiftsItemClick.itemClick(position);
                }
            });
        }
    }

    public interface IGiftsItemClick {
        void itemClick(int position);
    }

    private String formatPoint(int point) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(point).replaceAll(",", "\\.");
    }
}

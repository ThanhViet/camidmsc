package com.metfone.selfcare.module.tiin.hometiin.presenter;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.api.http.HttpCallBack;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.LogKQIHelper;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.module.tiin.AppProvider;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.base.BasePresenter;
import com.metfone.selfcare.module.tiin.hometiin.fragment.HomeTiinFragment;
import com.metfone.selfcare.module.tiin.network.api.TiinApi;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.util.Log;
import com.viettel.util.LogDebugHelper;

import org.json.JSONObject;

import java.util.ArrayList;

public class HomeTiinPresenter extends BasePresenter implements IHomeTiinMvpPresenter {
    public static final String TAG = HomeTiinPresenter.class.getSimpleName();
    private TiinApi mTiinApi;
    String urlHomeTiin = "";
    long startTime;
    long homeCache, home;
    private HttpCallBack dataCallback = new HttpCallBack() {
        @Override
        public void onSuccess(String data) throws Exception {
            final long endTime = System.currentTimeMillis();
            if (!isViewAttached() || !(getMvpView() instanceof HomeTiinFragment)) {
                return;
            }
            home = data.hashCode();
            if (home != homeCache && home != 0) {
                Gson gson = new Gson();
                ArrayList<HomeTiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<HomeTiinModel>>() {
                }.getType());
                ((HomeTiinFragment) getMvpView()).bindData(response);
                ((HomeTiinFragment) getMvpView()).loadDataSuccess(false);
                ((HomeTiinFragment) getMvpView()).saveDataCache(data);
                AppProvider.isLoadHome = true;
                LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.SUCCESS.getValue());
                if (response == null || response.size() == 0) {
                    LogDebugHelper.getInstance().logDebugContent("Tiin home error:" + data + " | " + urlHomeTiin);
                    ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_TIIN_HOME, "Tiin home error:" + data + " | " + urlHomeTiin);
                }
            }

        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            final long endTime = System.currentTimeMillis();
            Log.d(TAG, "loadData: onFailure - " + message);
            if (!isViewAttached() || !(getMvpView() instanceof HomeTiinFragment)) {
                return;
            }
            ((HomeTiinFragment) getMvpView()).loadDataSuccess(false);

            LogKQIHelper.getInstance(ApplicationController.self()).saveLogKQI(Constants.LogKQI.TIIN_GET_HOME, (endTime - startTime) + "", startTime + "", Constants.LogKQI.StateKQI.ERROR_TIMEOUT.getValue());
            LogDebugHelper.getInstance().logDebugContent("Tiin home error:" + message + " | " + urlHomeTiin);
            ReportHelper.reportError(ApplicationController.self(), ReportHelper.DATA_TYPE_TIIN_HOME, "Tiin home error:" + message + " | " + urlHomeTiin);

        }

        @Override
        public void onCompleted() {
            super.onCompleted();
        }
    };

    public HomeTiinPresenter() {
        mTiinApi = new TiinApi(ApplicationController.self());
    }

    @Override
    public void getHome() {
        startTime = System.currentTimeMillis();
        urlHomeTiin = mTiinApi.getApiUrl(TiinApi.GET_HOME);
        mTiinApi.getHomeTiin(dataCallback);
    }

    @Override
    public void getHomeCache() {
        if ((!isViewAttached() || !(getMvpView() instanceof HomeTiinFragment))) {
            return;
        }
        String data = SharedPrefs.getInstance().get(SharedPrefs.HOME_DATA_CACHE, String.class);
        if (TextUtils.isEmpty(data)) return;
        Gson gson = new Gson();
        homeCache = data.hashCode();
        try {
            ArrayList<HomeTiinModel> response = gson.fromJson(new JSONObject(data).optString("data"), new TypeToken<ArrayList<HomeTiinModel>>() {
            }.getType());
            SharedPrefs.getInstance().put(ConstantTiin.KEY_CREATE_HOME, true);
            ((HomeTiinFragment) getMvpView()).bindData(response);
            ((HomeTiinFragment) getMvpView()).loadDataSuccess(true);
            AppProvider.isLoadHome = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

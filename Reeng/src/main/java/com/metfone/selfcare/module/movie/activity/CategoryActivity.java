/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.module.movie.event.CategoryDetailEvent;
import com.metfone.selfcare.module.movie.event.ShowButtonDeleteEvent;
import com.metfone.selfcare.module.movie.fragment.CategoryDetailFragment;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.tabvideo.BaseActivity;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends BaseActivity implements ViewPager.OnPageChangeListener
        , TabLayout.OnTabSelectedListener, OnInternetChangedListener, ApiCallbackV2<ArrayList<SubtabInfo>> {

    @BindView(R.id.empty_progress)
    ProgressLoading emptyProgress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @BindView(R.id.empty_retry_text1)
    TextView emptyRetryText1;
    @BindView(R.id.empty_retry_text2)
    TextView emptyRetryText2;
    @BindView(R.id.empty_retry_button)
    ImageView emptyRetryButton;
    @BindView(R.id.empty_layout)
    LinearLayout emptyLayout;
    @BindView(R.id.frame_empty)
    LinearLayout frameEmpty;
    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.button_delete)
    ImageView btnDelete;
    @BindView(R.id.line)
    View line;

    private ArrayList<SubtabInfo> data = new ArrayList<>();
    private SubtabInfo tabInfo = null;
    private int currentTab = 0;
    private boolean isSubtab = false;
    private boolean isDataInitiated = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_category);
        ButterKnife.bind(this);
        Serializable serializable = getIntent().getSerializableExtra(Constants.KEY_DATA);
        if (serializable instanceof SubtabInfo) {
            tabInfo = (SubtabInfo) serializable;
        }
        if (tabInfo == null) {
            finish();
            return;
        }
        isSubtab = tabInfo.isSubtab();
        tvTitle.setText(tabInfo.getCategoryName());
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (tabInfo.isMustLoadData()) {
            loadData();
        } else {
            if (tabInfo.getType() == MovieKind.TYPE_LOCAL && MovieKind.CATEGORYID_GET_LIKED.equals(tabInfo.getCategoryId())) {
                SubtabInfo subtab;
                subtab = new SubtabInfo();
                subtab.setType(MovieKind.TYPE_LOCAL);
                subtab.setCategoryId(MovieKind.CATEGORYID_GET_LIKED_ODD);
                subtab.setCategoryName(getString(R.string.movie_odd));
                subtab.setSubtab(isSubtab);
                data.add(subtab);

                subtab = new SubtabInfo();
                subtab.setType(MovieKind.TYPE_LOCAL);
                subtab.setCategoryId(MovieKind.CATEGORYID_GET_LIKED_SERIES);
                subtab.setCategoryName(getString(R.string.movie_series));
                subtab.setSubtab(isSubtab);
                data.add(subtab);
            } else {
                data.add(tabInfo);
            }
            initViewPager();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentTab = position;
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.empty_retry_button)
    public void onEmptyRetryButtonClicked() {
        loadData();
    }

    private void showLoading() {
        if (frameEmpty != null) frameEmpty.setVisibility(View.VISIBLE);
        if (emptyProgress != null) emptyProgress.setVisibility(View.VISIBLE);
        if (emptyText != null) emptyText.setVisibility(View.GONE);
        if (emptyRetryText1 != null) emptyRetryText1.setVisibility(View.GONE);
        if (emptyRetryText2 != null) emptyRetryText2.setVisibility(View.GONE);
        if (emptyRetryButton != null) emptyRetryButton.setVisibility(View.GONE);
    }

    private void loadedEmpty() {
        if (coordinatorLayout != null) coordinatorLayout.setVisibility(View.GONE);
        if (frameEmpty != null) frameEmpty.setVisibility(View.VISIBLE);
        if (emptyText != null) {
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText(R.string.no_data);
        }
        if (emptyProgress != null) emptyProgress.setVisibility(View.GONE);
        if (emptyRetryText1 != null) emptyRetryText1.setVisibility(View.GONE);
        if (emptyRetryText2 != null) emptyRetryText2.setVisibility(View.GONE);
        if (emptyRetryButton != null) emptyRetryButton.setVisibility(View.GONE);
    }

    private void loadedError() {
        if (coordinatorLayout != null) coordinatorLayout.setVisibility(View.GONE);
        if (frameEmpty != null) frameEmpty.setVisibility(View.VISIBLE);
        if (emptyRetryButton != null) emptyRetryButton.setVisibility(View.VISIBLE);
        if (emptyProgress != null) emptyProgress.setVisibility(View.GONE);
        if (emptyText != null) emptyText.setVisibility(View.GONE);
        if (NetworkHelper.isConnectInternet(this)) {
            if (emptyRetryText1 != null) emptyRetryText1.setVisibility(View.VISIBLE);
            if (emptyRetryText2 != null) emptyRetryText2.setVisibility(View.GONE);
        } else {
            if (emptyRetryText1 != null) emptyRetryText1.setVisibility(View.GONE);
            if (emptyRetryText2 != null) emptyRetryText2.setVisibility(View.VISIBLE);
        }
    }

    private void loadedSuccess() {
        if (coordinatorLayout != null) coordinatorLayout.setVisibility(View.VISIBLE);
        if (frameEmpty != null) frameEmpty.setVisibility(View.GONE);
        if (emptyProgress != null) emptyProgress.setVisibility(View.GONE);
        if (emptyText != null) emptyText.setVisibility(View.GONE);
        if (emptyRetryText1 != null) emptyRetryText1.setVisibility(View.GONE);
        if (emptyRetryText2 != null) emptyRetryText2.setVisibility(View.GONE);
        if (emptyRetryButton != null) emptyRetryButton.setVisibility(View.GONE);
    }

    private void initViewPager() {
        isDataInitiated = true;
        if (viewPager != null) {
            FragmentPagerItems.Creator creatorPagerItems = new FragmentPagerItems.Creator(this);
            if (!data.isEmpty()) {
                int size = data.size() - 1;
                for (int i = size; i >= 0; i--) {
                    if (data.get(i) == null || TextUtils.isEmpty(data.get(i).getCategoryName()))
                        data.remove(i);
                }
                Bundle bundle;
                for (int i = 0; i < data.size(); i++) {
                    data.get(i).setSubtab(isSubtab);
                    bundle = new Bundle();
                    bundle.putSerializable(Constants.KEY_DATA, data.get(i));
                    creatorPagerItems.add(FragmentPagerItem.of(data.get(i).getCategoryName(), CategoryDetailFragment.class, bundle));
                }
            }
            FragmentStatePagerItemAdapter adapter = new FragmentStatePagerItemAdapter(getSupportFragmentManager(), creatorPagerItems.create());
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(3);
            viewPager.setCurrentItem(0);
            viewPager.addOnPageChangeListener(this);
            if (tab != null) {
                tab.setupWithViewPager(viewPager);
                tab.addOnTabSelectedListener(this);
            }
            adapter.notifyDataSetChanged();
            int tabSize = data.size();
            if (tabSize < 2) {
                if (appBarLayout != null) appBarLayout.setVisibility(View.GONE);
                if (line != null) line.setVisibility(View.VISIBLE);
            } else {
                if (appBarLayout != null) appBarLayout.setVisibility(View.VISIBLE);
                if (line != null) line.setVisibility(View.GONE);
                if (tab != null) {
                    if (tabSize > 3) {
                        tab.setTabGravity(TabLayout.GRAVITY_CENTER);
                        tab.setTabMode(TabLayout.MODE_SCROLLABLE);
                    } else {
                        tab.setTabGravity(TabLayout.GRAVITY_FILL);
                        tab.setTabMode(TabLayout.MODE_FIXED);
                    }
                }
            }
        }
        if (data.isEmpty()) {
            loadedEmpty();
        } else {
            loadedSuccess();
        }
    }

    private void loadData() {
        showLoading();
        new MovieApi().getListSubtab(tabInfo, this);
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(this) && !isDataInitiated && Utilities.isEmpty(data)) {
            loadData();
        }
    }

    @OnClick(R.id.iv_back)
    public void onBackClicked() {
        finish();
    }

    @OnClick(R.id.button_delete)
    public void onDeleteClicked() {
        if (tabInfo != null) {
            CategoryDetailEvent event = new CategoryDetailEvent(tabInfo);
            event.setDeleteAll(true);
            EventBus.getDefault().post(event);
        }
    }

    @Override
    public void onSuccess(String msg, ArrayList<SubtabInfo> result) throws JSONException {
        if (data == null) data = new ArrayList<>();
        else data.clear();
        if (Utilities.notEmpty(result)) {
            data.addAll(result);
        }
        initViewPager();
    }

    @Override
    public void onError(String s) {
        if (Utilities.isEmpty(data)) {
            loadedError();
        }
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ShowButtonDeleteEvent event) {
        if (event != null) {
            boolean isShow = tabInfo != null && tabInfo.getType() == MovieKind.TYPE_LOCAL
                    && MovieKind.CATEGORYID_GET_WATCHED.equals(tabInfo.getCategoryId());
            if (btnDelete != null)
                btnDelete.setVisibility(event.isShow() && isShow ? View.VISIBLE : View.GONE);
        }
    }
}

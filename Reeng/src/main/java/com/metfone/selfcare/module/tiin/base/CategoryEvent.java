package com.metfone.selfcare.module.tiin.base;

public class CategoryEvent {
    private boolean change;

    public CategoryEvent(boolean change) {
        this.change = change;
    }

    public boolean isChange() {
        return change;
    }

    public void setChange(boolean change) {
        this.change = change;
    }
}

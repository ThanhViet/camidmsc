/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by admin on 2020/4/8
 *
 */

package com.metfone.selfcare.module.share.holder;

import android.app.Activity;
import android.content.res.Resources;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.module.search.model.ContactProvisional;
import com.metfone.selfcare.module.share.listener.ShareContentListener;

import butterknife.BindView;
import butterknife.OnClick;

public class ChooseNonContactShareContentHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.thread_avatar)
    @Nullable
    ImageView ivThreadAvatar;
    @BindView(R.id.contact_avatar_text)
    @Nullable
    TextView tvAvatar;

    @BindView(R.id.button_action)
    TextView btnAction;

    private ShareContentListener listener;
    private ContactProvisional data;
    private int position;
    private ApplicationController mApplication;
    //private int sizeAvatar;
    private Resources resources;

    public ChooseNonContactShareContentHolder(View view, Activity activity, ShareContentListener listener) {
        super(view);
        this.listener = listener;
        mApplication = (ApplicationController) activity.getApplication();
        this.resources = mApplication.getResources();
        //sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.avatar_small_size);
    }

    @Override
    public void bindData(Object item, int position) {
        this.position = position;
        if (item instanceof ContactProvisional) {
            data = (ContactProvisional) item;
            switch (data.getState()) {
                case ContactProvisional.SEND:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(true);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_send));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_send));
                        btnAction.setText(R.string.btn_share_content_state_send);
                    }
                    break;
                case ContactProvisional.SENT:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(false);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_sent));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_sent));
                        btnAction.setText(R.string.btn_share_content_state_sent);
                    }
                    break;
                case ContactProvisional.RETRY:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(true);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_retry));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_retry));
                        btnAction.setText(R.string.btn_share_content_state_retry);
                    }
                    break;
                case ContactProvisional.UNDO:
                case ContactProvisional.OPEN_CHAT:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.VISIBLE);
                        btnAction.setEnabled(true);
                        btnAction.setBackground(resources.getDrawable(R.drawable.button_share_content_state_open_chat));
                        btnAction.setTextColor(resources.getColor(R.color.color_btn_share_content_state_open_chat));
                        btnAction.setText(R.string.btn_share_content_state_open_chat);
                    }
                    break;
                default:
                    if (btnAction != null) {
                        btnAction.setVisibility(View.GONE);
                        btnAction.setEnabled(false);
                    }
                    break;
            }
            if (data.getContact() instanceof String) {
                if (tvTitle != null)
                    tvTitle.setText((String) data.getContact());
//                if (ivThreadAvatar != null) ivThreadAvatar.setVisibility(View.VISIBLE);
//                if (tvAvatar != null) tvAvatar.setVisibility(View.GONE);
//                mApplication.getAvatarBusiness().setUnknownNumberAvatar(ivThreadAvatar, tvAvatar, ((String) data.getContact()), sizeAvatar);
            }
        } else {
            data = null;
        }
    }

    @OnClick(R.id.button_action)
    public void onClickItem() {
        if (listener != null && data != null) {
            listener.onShareToContact(data, position);
        }
    }
}
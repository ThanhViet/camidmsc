package com.metfone.selfcare.module.selfcare;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.module.keeng.base.BaseActivity;
import com.metfone.selfcare.module.keeng.utils.Log;
import com.metfone.selfcare.module.selfcare.fragment.SCAccountDetailFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCHomeFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCMoreFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCMyPackageFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCPackageDetailFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCPackageFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCPostageDetailFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCRechargeFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCStoreDetailFragment;
import com.metfone.selfcare.module.selfcare.fragment.SCStoreFragment;
import com.metfone.selfcare.module.selfcare.fragment.loyalty.SCAritimeFragment;
import com.metfone.selfcare.module.selfcare.fragment.loyalty.SCMyClaimFragment;
import com.metfone.selfcare.module.selfcare.fragment.loyalty.SCMyShareFragment;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.SCBottomSheetCSKHDialog;

import java.io.Serializable;

public class TabSelfCareActivity extends BaseActivity {
    protected final String TAG = getClass().getSimpleName();
    protected Fragment mFragment;
    protected int currentTabId;
    private FloatingActionButton fabSupport;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfcare);

        fabSupport = findViewById(R.id.fabSupport);
        fabSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCBottomSheetCSKHDialog dialogCSKH = new SCBottomSheetCSKHDialog(TabSelfCareActivity.this);
                dialogCSKH.show();
            }
        });

        Intent intent = getIntent();
        int type = intent.getIntExtra(Constants.KEY_TYPE, 0);
        Object object = intent.getSerializableExtra(Constants.KEY_DATA);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_DATA, (Serializable) object);
        goNextTab(type, bundle);
    }

    public void goNextTab(int tabId, Bundle args) {
        switch (tabId) {
            case SCConstants.SELF_CARE.TAB_HOME:
                mFragment = SCHomeFragment.newInstance();
                break;
            case SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL:
                mFragment = SCAccountDetailFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_POSTAGE_DETAIL:
                mFragment = SCPostageDetailFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_RECHARGE:
                mFragment = SCRechargeFragment.newInstance();
                break;
            case SCConstants.SELF_CARE.TAB_PACKAGE_DETAIL:
                mFragment = SCPackageDetailFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_MY_SHARE:
                mFragment = SCMyShareFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_MY_CREDIT:
                mFragment = SCAritimeFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_MY_CLAIM:
                mFragment = SCMyClaimFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_STORES:
                mFragment = SCStoreFragment.newInstance();
                break;
            case SCConstants.SELF_CARE.TAB_STORES_DETAIL:
                mFragment = SCStoreDetailFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_PACKAGE_LIST:
                mFragment = SCPackageFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE:
                mFragment = SCMyPackageFragment.newInstance(args);
                break;
            case SCConstants.SELF_CARE.TAB_MORE:
                mFragment = SCMoreFragment.newInstance();
                break;
            default:
                mFragment = null;
                break;

        }

        if (mFragment != null) {
            currentTabId = tabId;
            try {
                if (!mFragment.isAdded())
                    if (args != null) {
                        if (mFragment.getArguments() == null) {
                            mFragment.setArguments(args);
                        } else {
                            mFragment.getArguments().putAll(args);
                        }
                    }
            } catch (IllegalStateException e) {
//                CrashUtils.logCrash(TAG, e);
            } catch (RuntimeException e) {
//                CrashUtils.logCrash(TAG, e);
            } catch (Exception e) {
//                CrashUtils.logCrash(TAG, e);
            }
            replaceFragment(currentTabId, mFragment);
        }
    }

    public void replaceFragment(final int tabId, final Fragment fragment) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                transactionFragment(tabId, fragment);
            }
        });
    }

    public void transactionFragment(final int tabId, final Fragment fragment) {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_right_to_left_enter, R.anim.activity_right_to_left_exit)
                    .add(R.id.tabContent, fragment, String.valueOf(tabId))
                    .commitAllowingStateLoss();
        } catch (RuntimeException e) {
//            CrashUtils.logCrash(TAG, e);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
    }

    public void goPrevTab() {
        try {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            int entryCount = mFragmentManager.getFragments().size();
            if (entryCount > 0) {
                for (int i = entryCount - 1; i >= 0; i--) {
                    Fragment fragment = mFragmentManager.getFragments().get(i);
                    if (fragment != null) {
                        if (i == 0) {
                            finish();
                            break;
                        } else {
                            onFragmentDetached(fragment.getTag());
                            break;
                        }
                    }
                }
            } else {
                finish();
            }
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
            finish();
        }
    }

    public void onFragmentDetached(String tag) {
        Log.d(TAG, "onFragmentDetached: " + tag);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.activity_left_to_right_enter, R.anim.activity_left_to_right_exit)
                    .remove(fragment)
                    .commitNow();
        }
    }

    @Override
    public void onBackPressed() {
        InputMethodUtils.hideSoftKeyboard(this);
        goPrevTab();
    }

    public void gotoAccountDetail(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL, bundle);
    }

    public void gotoMyPackage(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, bundle);
    }

    public void gotoPostageDetail(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_POSTAGE_DETAIL, bundle);
    }

    public void gotoRecharge() {
        goNextTab(SCConstants.SELF_CARE.TAB_POSTAGE_DETAIL, null);
    }

    public void gotoPackageDetail(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_PACKAGE_DETAIL, bundle);
    }

    public void gotoPackageList(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_PACKAGE_LIST, bundle);
    }

    public void gotoLoyaltyHome(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_LOYALTY_HOME, bundle);
    }

    public void gotoStoreDetail(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_STORES_DETAIL, bundle);
    }

    public void gotoStoreCenter(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_STORES, bundle);
    }

    public void gotoMyShare(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_MY_SHARE, bundle);
    }

    public void gotoMyShareOTP(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_MY_SHARE_OTP, bundle);
    }

    public void gotoMyClaim(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_MY_CLAIM, bundle);
    }

    public void gotoMyCredit(Bundle bundle) {
        goNextTab(SCConstants.SELF_CARE.TAB_MY_CREDIT, bundle);
    }

    public void showFloatingButton() {
        fabSupport.setVisibility(View.VISIBLE);
    }

    public void hideFloatingButton() {
        fabSupport.setVisibility(View.GONE);
    }
}

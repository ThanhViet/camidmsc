package com.metfone.selfcare.module.metfoneplus.topup.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.metfoneplus.topup.model.ServiceGroupTopUpModel;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.ServiceGroupTopUpViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.viewholder.ServiceTopUpViewHolder;

import java.util.List;

public class ServiceGroupTopUpAdapter extends RecyclerView.Adapter<ServiceGroupTopUpViewHolder> {
    private List<ServiceGroupTopUpModel> items;
    private ServiceTopUpViewHolder.EventListener eventListener;
    public ServiceGroupTopUpAdapter(List<ServiceGroupTopUpModel> items, ServiceTopUpViewHolder.EventListener eventListener){
        this.items = items;
        this.eventListener = eventListener;
    }
    @NonNull
    @Override
    public ServiceGroupTopUpViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == LinearLayout.HORIZONTAL){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_service_topup_horizontal,parent,false);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_service_topup_vertical,parent,false);
        }
        return new ServiceGroupTopUpViewHolder(view, eventListener);
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getDisplayDirection();
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceGroupTopUpViewHolder holder, int position) {
        holder.bindView(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

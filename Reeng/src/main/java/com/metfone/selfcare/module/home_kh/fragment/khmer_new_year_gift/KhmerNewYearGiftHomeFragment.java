package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.FragmentKhmerNewYearGiftHomeBinding;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.WsGiftInfoResponse;
import com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift.giftbox.KhmerNewYearGiftBoxFragment;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.util.IFragmentBackPress;

import retrofit2.Response;

public class KhmerNewYearGiftHomeFragment extends BaseFragment implements IFragmentBackPress {

    public static final String TAG = KhmerNewYearGiftHomeFragment.class.getSimpleName();

    private FragmentKhmerNewYearGiftHomeBinding binding;
    private KhHomeClient client;
    private WsGiftInfoResponse.Result infoResponse;

    public ObservableBoolean shouldPlaySound = new ObservableBoolean(true);
    public ObservableBoolean shouldShowError = new ObservableBoolean(false);
    public ObservableField<String> errorMessage = new ObservableField<>();

    private KhmerNewYearGiftActivity activity;

    public static KhmerNewYearGiftHomeFragment newInstance() {
        KhmerNewYearGiftHomeFragment fragment = new KhmerNewYearGiftHomeFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentKhmerNewYearGiftHomeBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        client = new KhHomeClient();
        activity = (KhmerNewYearGiftActivity)getActivity();

        ObjectAnimator animator = ObjectAnimator.ofFloat(binding.imgHand, "translationY", -100f);
        animator.setDuration(800);
        animator.setRepeatCount(Animation.INFINITE);
        animator.start();
    }



    public void openScanScreen(View view){
        view.setEnabled(false);
        soundOff();
        activity.playTabSound();
        client.wsGetGiftInfo(new MPApiCallback<WsGiftInfoResponse>() {
            @Override
            public void onResponse(Response<WsGiftInfoResponse> response) {
                onFinish();
                if(response.body() != null){
                    if(response.body().getResult().getStatus() != 0){
                        showDialogError(response.body().getResult().getMessage());
                        return;
                    }
                    infoResponse = response.body().getResult();
                    if(infoResponse.getCountSpin() > 0){
                        addFragment(R.id.root_frame, KhmerNewYearGiftScanFragment.newInstance(infoResponse), KhmerNewYearGiftScanFragment.TAG);
                    } else {
                        openRequireTopUpScreen();
                    }
                }
            }

            @Override
            public void onError(Throwable error) {
                onFinish();
                activity.connectionError();
            }

            private void onFinish() {
                view.setEnabled(true);
            }
        });

    }

    public void soundOn(){
        activity.playSound();
        shouldPlaySound.set(true);
    }

    private void soundOff(){
        activity.stopSound();
        shouldPlaySound.set(false);
    }

    public void soundClicked(){
        if(shouldPlaySound.get()){
            soundOff();
        }
        else {
            soundOn();
        }
        activity.shouldPlaySound = shouldPlaySound.get();
    }

    public void openGiftBoxScreen(){
        soundOff();
        activity.playTabSound();
        addFragment(R.id.root_frame, KhmerNewYearGiftBoxFragment.newInstance(), KhmerNewYearGiftBoxFragment.TAG);
    }

    public void openRequireTopUpScreen(){
        soundOff();
        activity.playTabSound();
        addFragment(R.id.root_frame, KhmerNewYearGiftRequireTopUpFragment.newInstance(), KhmerNewYearGiftRequireTopUpFragment.TAG);
    }

    public void openTermConditionScreen(){
        soundOff();
        activity.playTabSound();
        addFragment(R.id.root_frame, KhmerNewYearGiftTermCondition.newInstance(), KhmerNewYearGiftTermCondition.TAG);
    }

    @Override
    public String getName() {
        return KhmerNewYearGiftHomeFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return 0;
    }

    private void showDialogError(String message){
        binding.errorButton.setText(R.string.close);
        shouldShowError.set(true);
        errorMessage.set(message);
    }

    public void closePopup() {
        shouldShowError.set(false);
    }

    @Override
    public boolean onFragmentBackPressed() {
        if(shouldShowError.get()){
            shouldShowError.set(false);
            return false;
        }
        return true;
    }
}

package com.metfone.selfcare.module.keeng.adapter.category;

import android.content.Context;

import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.base.BaseAdapterMedia;
import com.metfone.selfcare.module.keeng.base.BaseHolder;
import com.metfone.selfcare.module.keeng.model.AllModel;

import java.util.List;

/**
 * Created by namnh40 on 11/29/2016.
 */

public class ChildSingerAdapter extends BaseAdapterMedia {
    public ChildSingerAdapter(Context context, List<AllModel> datas, String ga_source) {
        super(context, datas, ga_source);
    }

    @Override
    public int getItemViewType(int position) {
        AllModel item = getItem(position);
        if (item == null) {
            if (position == getItemCount() - 1)
                return ITEM_LOAD_MORE;
            else
                return ITEM_EMPTY;
        }
        switch (item.type) {
            case Constants.TYPE_SONG:
                return ITEM_MEDIA_SONG_SINGER;
            case Constants.TYPE_ALBUM:
                return ITEM_MEDIA_ALBUM_HOME_HOT;
            case Constants.TYPE_VIDEO:
                return ITEM_MEDIA_VIDEO_RANK;
            default:
                return ITEM_MEDIA_ALBUM;
        }
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        super.onBindViewHolder(holder, position);
    }
}

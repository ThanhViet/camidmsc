package com.metfone.selfcare.module.movienew.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.metfone.selfcare.R;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.model.ClickLikeMessage;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.Movie2;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movienew.holder.ResultSearchViewHolder;
import com.metfone.selfcare.module.movienew.model.ActionFilmModel;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.util.EnumUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;

public class MyListFragment extends BaseFragment implements ItemViewClickListener {
    private final String TAG = "MyListFragment";
    private static final String TITLE_KEY = "title";
    private static final String ID_KEY = "idDirectorOrCast";

    @BindView(R.id.recyclerMyList)
    RecyclerView recyclerMyList;
    @BindView(R.id.icBack)
    AppCompatImageView icBack;
    @BindView(R.id.titleLayout)
    AppCompatTextView titleLayout;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private Unbinder unbinder;
    private ArrayList<Movie2> movieArrayList = new ArrayList<>();
    private BaseAdapter mResultSearchAdapter;
    private String title = "";
    private String idDirectorOrCast = "";
    private MovieApi movieApi;

    public static MyListFragment newInstance() {
        return new MyListFragment();
    }

    public static MyListFragment newInstance(String title, String id) {
        Bundle args = new Bundle();
        args.putString(TITLE_KEY,title);
        args.putString(ID_KEY,id);
        MyListFragment fragment = new MyListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_list_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        listener();
        loadData();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    private void listener() {
        icBack.setOnClickListener(v -> popBackStackFragment());
    }

    private void initView() {
        ResultSearchViewHolder resultSearchViewHolder = new ResultSearchViewHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        mResultSearchAdapter = new BaseAdapter(movieArrayList, getContext(), R.layout.holder_grid_relate_movie, resultSearchViewHolder);
        recyclerMyList.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerMyList.setAdapter(mResultSearchAdapter);

//        assert getArguments() != null;
        if (getArguments() != null) {
            title = getArguments().getString(TITLE_KEY);
            idDirectorOrCast = getArguments().getString(ID_KEY);
            titleLayout.setText(title);
        }


    }

    private MovieApi getMovieApi() {
        if (movieApi == null) {
            movieApi = new MovieApi();
        }
        return movieApi;
    }

    private void loadData() {
        pbLoading.setVisibility(View.VISIBLE);
        if (title.equals(getString(R.string.my_list))) {
            if (isLogin() == EnumUtils.LoginVia.NOT || ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()){
                //nếu chưa đăng nhập thì không xem được list đã thích
                pbLoading.setVisibility(View.GONE);
            }else {
                //my list liked when  click heart in cinema tab
                pbLoading.setVisibility(View.VISIBLE);
                getMovieApi().getListFilmLiked(new ApiCallbackV2<ArrayList<Movie2>>() {
                    @Override
                    public void onSuccess(String msg, ArrayList<Movie2> result) throws JSONException {
                        if (result != null) {
                            movieArrayList.addAll(result);
                            mResultSearchAdapter.updateAdapter(result);
                        }
                        pbLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String s) {
                        pbLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onComplete() {
                        pbLoading.setVisibility(View.GONE);
                    }
                });
            }
        } else
            MovieApi.getInstance().getListFilmByInfo(idDirectorOrCast, new ApiCallbackV2<ActionFilmModel>() {
                @Override
                public void onSuccess(String msg, ActionFilmModel result) throws JSONException {
                    if (result != null && result.getHomeResult() != null && result.getHomeResult().size() > 0) {
                        mResultSearchAdapter.updateAdapter(result.getHomeResult());
                    }
                    pbLoading.setVisibility(View.GONE);
                }

                @Override
                public void onError(String s) {
                    pbLoading.setVisibility(View.GONE);
                }

                @Override
                public void onComplete() {

                }
            });
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true, priority = 1)
    public void onReceiveData(ClickLikeMessage event) {
        pbLoading.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(event.getIdVideo())) {
            for (Movie2 movie2 : movieArrayList) {
                if (!event.isLike() && movie2.getId().toString().equals(event.getIdVideo())) {
                    movieArrayList.remove(movie2);
                    mResultSearchAdapter.updateAdapter(movieArrayList);
                    break;
                }
            }
        }
        pbLoading.setVisibility(View.GONE);

    }

    @Override
    public void onStop() {
        super.onStop();
        ClickLikeMessage stickyEvent = EventBus.getDefault().getStickyEvent(ClickLikeMessage.class);
        if (stickyEvent != null) {
            EventBus.getDefault().removeStickyEvent(stickyEvent);
        }
    }

    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        if (list.get(position) instanceof Movie) {
            Movie movie = (Movie) list.get(position);
            activity.playMovies(movie);
        } else if (list.get(position) instanceof Movie2) {
            Movie2 movie2 = (Movie2) list.get(position);
            activity.playMovies(new Movie(movie2));
        }
        if (list.get(position) instanceof HomeData) {
            HomeData homeData = (HomeData) list.get(position);
            activity.playMovies(new Movie(homeData));
        }
    }
}

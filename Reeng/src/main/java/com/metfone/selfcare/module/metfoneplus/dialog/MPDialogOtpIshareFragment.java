package com.metfone.selfcare.module.metfoneplus.dialog;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.InputMethodUtils;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.model.camid.Store;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.network.metfoneplus.MetfonePlusClient;
import com.metfone.selfcare.network.metfoneplus.response.WsCheckOtpResponse;
import com.metfone.selfcare.network.metfoneplus.response.WsGetOtpResponse;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.ui.view.CamIdTextView;
import com.metfone.selfcare.ui.view.PinEntryEditText;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.metfone.selfcare.activity.CreateYourAccountActivity.hideKeyboard;

public class MPDialogOtpIshareFragment extends DialogFragment implements View.OnClickListener, ClickListener.IconListener {
    public static final String TAG = MPSupportStoreDialog.class.getSimpleName();
    private static final int TIME_DEFAULT = 60000;
    @BindView(R.id.etOtp)
    PinEntryEditText etOtp;
    @BindView(R.id.ivWrongOTP)
    CamIdTextView ivWrongOTP;
    @BindView(R.id.tvResendOtp)
    CamIdTextView tvResendOtp;
    @BindView(R.id.tvTimeResendOtp)
    CamIdTextView tvTimeResendOtp;
    @BindView(R.id.layout_root_dialog_mp_support_store)
    RelativeLayout layout_root_dialog_mp_support_store;

    private long countDownTimer;
    private Context mContext;
    Unbinder unbinder;
    private Handler mHandler;
    int remain = 0;
    private String phone;
    private String otp;
    private ClickListener.IconListener mClickHandler;
    private BaseSlidingFragmentActivity baseSlidingFragmentActivity;
    private MPDialogOtpIshareFragment.ButtonOnClickListener mButtonOnClickListener;
    BaseMPSuccessDialog baseMPSuccessDialog;
    private static boolean handlerflag = false;
    private boolean isSaveInstanceState = false;
    private boolean checkResend = false;
    private int amount;
    private boolean isOTPDialogShow = false;

    public void setIsOTPDialogShow(boolean isOTPDialogShow) {
        this.isOTPDialogShow = isOTPDialogShow;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }

    public MPDialogOtpIshareFragment() {

    }

    public static MPDialogOtpIshareFragment newInstance(Store store) {
        MPDialogOtpIshareFragment supportStoreDialog = new MPDialogOtpIshareFragment();
        Bundle bundle = new Bundle();
        supportStoreDialog.setArguments(bundle);
        return supportStoreDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFullScreen);
        mHandler = new Handler();
        handlerflag = true;
        if (getArguments() != null) {
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_mp_get_otp, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        etOtp.setSingleCharHint("-");
        etOtp.setHintTextColor(Color.WHITE);
        etOtp.requestFocus();

        layout_root_dialog_mp_support_store.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(layout_root_dialog_mp_support_store.getWindowToken(), 0);
                return true;
            }
        });
        setViewListeners();
        return rootView;
    }

    private void setViewListeners() {
        startCountDown(TIME_DEFAULT);
        if(!isOTPDialogShow){
            generateOtpByServer();
        }
        etOtp.setOnPinEnteredListener(str -> {
            if (str != null && str.length() == 6) {
                otp = etOtp.getText().toString();
                checkOtp();
            }
        });

        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() < 6){
                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
                        etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_pin_otp));
                        ivWrongOTP.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etOtp.setOnKeyListener((v, keyCode, event) -> {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                //this is for backspace
                if (ivWrongOTP.getVisibility() == View.VISIBLE) {
                    etOtp.setPinBackground(ContextCompat.getDrawable(requireContext(), R.drawable.bg_pin_otp));
                    ivWrongOTP.setVisibility(View.GONE);
                }
            }
            return false;
        });
    }


    private void startCountDown(int time) {
        countDownTimer = time;
        tvResendOtp.setEnabled(false);
        String countDownString = getString(R.string.mc_resend_otp);
        tvResendOtp.setText(countDownString);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                countDownTimer = countDownTimer - 1000;
                remain = (int) (countDownTimer / 1000.0);
                if (remain > 0 && mHandler != null) {
                    mHandler.postDelayed(this, 1000);
                } else if (remain == 0) {
                    tvResendOtp.setEnabled(true);
                }
                String countDownString = getString(R.string.mc_resend_otp);
                tvResendOtp.setText(countDownString);
                tvTimeResendOtp.setText(remain + " " + getString(R.string.text_seconds));
            }
        }, 1000);
    }

    public void generateOtpByServer() {
        new MetfonePlusClient().wsGetOTPIshareMetFonePlus(new MPApiCallback<WsGetOtpResponse>() {
            @Override
            public void onResponse(Response<WsGetOtpResponse> response) {
                if (checkResend) {
                    checkResend = false;
                    ivWrongOTP.setVisibility(View.VISIBLE);
                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
                        ivWrongOTP.setText(getString(R.string.mc_resend_success));
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ivWrongOTP.setVisibility(View.GONE);
                            }
                        }, 3000);
                    }
                }

                startSmsUserConsent();
                android.util.Log.d("TAG", "onResponse: " + response.body());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UIUtil.showKeyboard(getContext(),etOtp);
                    }
                }, 200);
            }

            @Override
            public void onError(Throwable error) {
                baseMPSuccessDialog = new BaseMPSuccessDialog(getActivity(), R.drawable.image_error_dialog, getString(R.string.title_sorry), error.getMessage(), false);
                baseMPSuccessDialog.show();
            }
        });
    }
    public void checkOtp() {
        new MetfonePlusClient().wsCheckOTPIshareMetFonePlus(phone, otp, amount, new MPApiCallback<WsCheckOtpResponse>() {
            @Override
            public void onResponse(Response<WsCheckOtpResponse> response) {
                if (response.body() != null && response.body().getResult().getErrorCode().equals("0")) {
                    mButtonOnClickListener.onCheckOtpSuccess(response.body().getResult().getUserMsg(), response.body());
                    //mButtonOnClickListener.onDialogDismiss(response.body().getResult().getUserMsg());
                }  if (response.body() != null && response.body().getResult().getErrorCode().equals("5")) {
                    mButtonOnClickListener.onCheckOtpSuccess(response.body().getResult().getUserMsg(), response.body());
                } else  if (response.body() != null && response.body().getResult().getErrorCode().equals("6")) {
                    mButtonOnClickListener.onCheckOtpSuccess(response.body().getResult().getUserMsg(), response.body());
                } else  if (response.body() != null && response.body().getResult().getErrorCode().equals("8")) {
                    mButtonOnClickListener.onCheckOtpSuccess(response.body().getResult().getUserMsg(), response.body());
                } else  if (response.body() != null && response.body().getResult().getErrorCode().equals("4") || response.body() != null && response.body().getResult().getErrorCode().equals("9") || response.body() != null && response.body().getResult().getErrorCode().equals("10")) {
                    mButtonOnClickListener.onCheckOtpSuccess(response.body().getResult().getUserMsg(), response.body());
                } else {
                    ivWrongOTP.setVisibility(View.VISIBLE);
                    if (ivWrongOTP.getVisibility() == View.VISIBLE){
                        ivWrongOTP.setText(response.body().getResult().getUserMsg());
                    }
                    mButtonOnClickListener.onCheckOtpSuccess("6", response.body());
                }

                handlerflag = false;
            }

            @Override
            public void onError(Throwable error) {
                ivWrongOTP.setVisibility(View.VISIBLE);
                if (ivWrongOTP.getVisibility() == View.VISIBLE){
                    ivWrongOTP.setText(error.getMessage());
                }
            }
        });
    }
    private void startSmsUserConsent() {

        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        Task task = client.startSmsUserConsent(null);

        task.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.btnCancle)
                .setOnClickListener(this);
        view.findViewById(R.id.tvResendOtp)
                .setOnClickListener(this);
//        view.findViewById(R.id.layout_root_dialog_mp_support_store)
//                .setOnClickListener((v) -> {
//                    dismissAllowingStateLoss();
//                });
    }
    public void addButtonOnClickListener(MPDialogOtpIshareFragment.ButtonOnClickListener buttonOnClickListener) {
        this.mButtonOnClickListener = buttonOnClickListener;
    }
    @Override
    public void onPause() {
        super.onPause();
        InputMethodUtils.hideSoftKeyboard(etOtp, mContext);
        dismissAllowingStateLoss();
        mClickHandler = null;
        handlerflag = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancle:
                dismissAllowingStateLoss();
                break;
            case R.id.tvResendOtp:
                checkResend = true;
                ivWrongOTP.setVisibility(View.GONE);
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
        }
    }

    public interface ButtonOnClickListener {
        default void onDialogDismiss(String mesError) {}
        default void onCheckOtpSuccess(String otp, WsCheckOtpResponse wsCheckOtpResponse) {}
    }
    @Override
    public void onResume() {
        etOtp.requestFocus();
        UIUtil.showKeyboard(getContext(),etOtp);
        if (!handlerflag && remain > 0) {
            startCountDown(remain * 1000);
        }
        isSaveInstanceState = false;
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Neu da vao saveinstance tuc la fragment van con song
        if (!isSaveInstanceState) {
            if (mHandler != null) mHandler.removeCallbacksAndMessages(null);
        }
//        if (countDownTimer != null) countDownTimer.cancel();

    }

    @Override
    public void onIconClickListener(View view, Object entry, int menuId) {
        switch (menuId) {
            case Constants.MENU.POPUP_YES:
                startCountDown(TIME_DEFAULT);
                generateOtpByServer();
                break;
            default:
                break;
        }
    }


}

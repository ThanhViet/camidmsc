/*
package com.metfone.selfcare.module.selfcare.fragment.account;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Config;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.helper.workmanager.SettingWorkManager;
import com.metfone.selfcare.module.selfcare.activity.SCAccountActivity;
import com.metfone.selfcare.module.selfcare.fragment.SCBaseFragment;
import com.metfone.selfcare.module.selfcare.model.SCAccount;
import com.metfone.selfcare.module.selfcare.network.SCAccountCallback;
import com.metfone.selfcare.module.selfcare.network.SelfCareAccountApi;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.widget.LoadingViewSC;
import com.metfone.selfcare.network.xmpp.XMPPManager;
import com.metfone.selfcare.ui.EllipsisTextView;
import com.metfone.selfcare.ui.roundview.RoundTextView;
import com.metfone.selfcare.util.Log;

import org.jivesoftware.smack.XMPPException;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.metfone.selfcare.helper.facebook.FacebookHelper.BIRTHDAY_PERMISSION;
import static com.metfone.selfcare.helper.facebook.FacebookHelper.PROFILE_PERMISSION;

*/
/**
 * Created by thanhnt72 on 2/13/2019.
 *//*


public class SCCheckAccountFragment extends SCBaseFragment {

    private static final String SHOW_SKIP = "show_skip";

    @BindView(R.id.tvNoteAccount)
    TextView tvNoteAccount;
    @BindView(R.id.btnInputInfo)
    RoundTextView btnInputInfo;
    @BindView(R.id.btnConnectMyIdExist)
    RoundTextView btnConnectMyIdExist;
    @BindView(R.id.btnConnectViaFb)
    RoundTextView btnConnectViaFb;
    @BindView(R.id.llNewAccount)
    LinearLayout llNewAccount;
    @BindView(R.id.btnLogin)
    RoundTextView btnLogin;
    @BindView(R.id.btnLoginOtherAcc)
    RoundTextView btnLoginOtherAcc;
    @BindView(R.id.llLogin)
    LinearLayout llLogin;
    Unbinder unbinder;

    @BindView(R.id.tilCreateId)
    TextInputLayout tilCreateId;
    @BindView(R.id.tilCreatePass)
    TextInputLayout tilCreatePass;
    @BindView(R.id.tilCreateRePass)
    TextInputLayout tilCreateRePass;

    EditText etCreateUsername;
    EditText etCreatePassword;
    EditText etCreateRePassword;
    @BindView(R.id.loading_view)
    LoadingViewSC loadingView;

    private String userName;
    private boolean showSkip;

    public static SCCheckAccountFragment newInstance(boolean showSkip) {
        SCCheckAccountFragment fragment = new SCCheckAccountFragment();
        Bundle args = new Bundle();
        args.putBoolean(SHOW_SKIP, showSkip);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public String getName() {
        return SCCheckAccountFragment.class.getSimpleName();
    }

    @Override
    public int getResIdView() {
        return R.layout.fragment_sc_check_account;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null) {
            showSkip = getArguments().getBoolean(SHOW_SKIP);
        }

        initEditText();
        setActionBar(view);
//        drawViewDetail();
        getInfoMyID();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) loadingView.getLayoutParams();
        params.height = mApp.getHeightPixels() - mApp.getStatusBarHeight() - mActivity.getResources().getDimensionPixelOffset(R.dimen.action_bar_height);

        loadingView.setBtnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInfoMyID();
            }
        });
        return view;
    }

    private void initEditText() {
        etCreatePassword = tilCreatePass.getEditText();
        etCreateUsername = tilCreateId.getEditText();
        etCreateRePassword = tilCreateRePass.getEditText();

        etCreatePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilCreatePass.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etCreateUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilCreateId.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etCreateRePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilCreateRePass.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getInfoMyID() {
        loadingView.loadBegin();
        llLogin.setVisibility(View.GONE);
        llNewAccount.setVisibility(View.GONE);
//        mActivity.showLoadingSelfCare("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).getMyIdFromMyJid(new SCAccountCallback.SCAccountApiListener() {
            @Override
            public void onSuccess(String response) {
//                mActivity.hideLoadingDialog();

                try {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingView.loadFinish();
                        }
                    });
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("errorCode") == 0) {
                        JSONObject js = jsonObject.optJSONObject("result");
                        if (js != null) {
                            userName = js.optString("userName");
                            mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_USER_NAME, userName).apply();
                        }
                    }
                    drawViewDetail();
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingView.loadError();
                        }
                    });
                }

            }

            @Override
            public void onError(int code, String message) {
                mActivity.hideLoadingDialog();
//                drawViewDetail();
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingView.showRetry();
                    }
                });

            }
        });
    }

    private void drawViewDetail() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(userName)) {
                    llLogin.setVisibility(View.VISIBLE);
                    llNewAccount.setVisibility(View.GONE);
                    tvNoteAccount.setText(String.format(mActivity.getString(R.string.sc_note_account_exist),
                            mApp.getReengAccountBusiness().getJidNumber(), userName, userName));
                } else {
                    llLogin.setVisibility(View.GONE);
                    llNewAccount.setVisibility(View.VISIBLE);
                    //<string name="sc_lear_more_myid">Tìm hiểu về MyID</string>


                    ClickableSpan clickableSpan = new ClickableSpan() {
                        @Override
                        public void updateDrawState(TextPaint ds) {
//                ds.setColor(ds.linkColor);    // you can use custom color
                            ds.setUnderlineText(true);    // this remove the underline
                        }

                        @Override
                        public void onClick(View textView) {
                            DeepLinkHelper.getInstance().openSchemaLink(mActivity, "mytel://survey?ref=" + SCConstants.SC_WEBSITE + "&backConfirm=1");
                        }
                    };


                    String textNormal = mActivity.getString(R.string.sc_note_account_info) + " ";
                    String textToSpan = mActivity.getString(R.string.sc_lear_more_myid);
                    String textToShow = textNormal + textToSpan;
                    SpannableString spannableString = new SpannableString(textToShow);
                    int START_TEXT = textNormal.length();
                    int END_TEXT = textToShow.length();
                    spannableString.setSpan(clickableSpan, START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color
                                    .sc_color_text_normal)),
                            START_TEXT, END_TEXT, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
                    spannableString.setSpan(boldSpan, START_TEXT, END_TEXT, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvNoteAccount.setText(spannableString);
                    tvNoteAccount.setMovementMethod(LinkMovementMethod.getInstance());


                }
            }
        });

    }

    private void setActionBar(View view) {
        LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // action bar
        Toolbar abView = view.findViewById(R.id.tool_bar_fragment);
        abView.removeAllViews();
        abView.addView(mLayoutInflater.inflate(
                R.layout.ab_detail, null), new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        (abView.findViewById(R.id.ab_more_btn)).setVisibility(View.GONE);
        ImageView mImBack = abView.findViewById(R.id.ab_back_btn);
        mImBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        EllipsisTextView mTvwTitle = abView.findViewById(R.id.ab_title);
        mTvwTitle.setText(mActivity.getString(R.string.sc_customer_info));

        if (showSkip) {
            TextView tvSkip = abView.findViewById(R.id.ab_agree_text);
            tvSkip.setText(mActivity.getString(R.string.skip));
            tvSkip.setVisibility(View.VISIBLE);
            tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mApp, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra("home_action", Constants.ACTION.VIEW_CREATE_CHAT);
                    mActivity.startActivity(intent, false);
                    mActivity.clearBackStack();
                    mActivity.finish();
                }
            });
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SelfCareAccountApi.getInstant(mApp).canncelPendingRequest(SelfCareAccountApi.TAG_GET_MYID);
//        unbinder.unbind();
    }

    @OnClick({R.id.btnInputInfo, R.id.btnConnectMyIdExist, R.id.btnConnectViaFb, R.id.btnLogin, R.id.btnLoginOtherAcc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnInputInfo:
                if (isValidInfo())
                    ((SCAccountActivity) mActivity).openEnterInfoFragment(
                            etCreateUsername.getText().toString().trim(),
                            etCreatePassword.getText().toString().trim(), true);
                break;
            case R.id.btnConnectMyIdExist:
                ((SCAccountActivity) mActivity).openLoginMyID(null, true);
                break;
            case R.id.btnConnectViaFb:
                doLoginViaFacebook();
                break;
            case R.id.btnLogin:
                ((SCAccountActivity) mActivity).openLoginMyID(userName, true);
                break;
            case R.id.btnLoginOtherAcc:
                doActiveAccount();
                break;
        }
    }

    private void doActiveAccount() {
        if (NetworkHelper.isConnectInternet(mApp)) {
            if (mApp.getXmppManager().isAuthenticated()) {
                new DeactiveAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                mActivity.showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
            }
        } else {
            mActivity.showToast(getString(R.string.error_internet_disconnect), Toast.LENGTH_SHORT);
        }
    }

    private boolean isValidInfo() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etCreateUsername.getText().toString().trim())) {
            isValid = false;
            tilCreateId.setError(mActivity.getString(R.string.sc_input_text));
        }
        if (TextUtils.isEmpty(etCreatePassword.getText().toString().trim())) {
            isValid = false;
            tilCreatePass.setError(mActivity.getString(R.string.sc_input_text));
        }

        if (TextUtils.isEmpty(etCreateRePassword.getText().toString().trim())) {
            isValid = false;
            tilCreateRePass.setError(mActivity.getString(R.string.sc_input_text));
        }

        if (isValid) {
            String pass = etCreatePassword.getText().toString().trim();
            String repass = etCreateRePassword.getText().toString().trim();
            if (!pass.equals(repass)) {
                isValid = false;
                mActivity.showToast(R.string.sc_pass_doesnt_match);
            } else {
                if (pass.length() < 6) {
                    isValid = false;
                    mActivity.showToast(R.string.sc_valid_pass);
                }
            }
        }
        return isValid;
    }

    private void doLoginViaFacebook() {
        CallbackManager callbackManager = mActivity.getCallbackManager();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("MainActivity", "@@@onSuccess: " + loginResult.getAccessToken().getToken());
                doLogin(loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                Log.i("MainActivity", "@@@onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("MainActivity", "@@@onError: " + error.getMessage());
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList(PROFILE_PERMISSION, BIRTHDAY_PERMISSION));
    }

    private void doLogin(String accessToken) {
        SCAccount scAccount = new SCAccount();
        scAccount.setAccessTokenFacebook(accessToken);
        mActivity.showLoadingSelfCare("", R.string.loading);
        SelfCareAccountApi.getInstant(mApp).registerAccountMyID(scAccount, new SCAccountCallback.SCAccountApiListener() {
            @Override
            public void onSuccess(String response) {
                mActivity.hideLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("errorCode", -1);
                    if (code == 0) {
                        JSONObject jsResult = jsonObject.optJSONObject("result");
                        if (jsResult != null) {
                            String accessToken = jsResult.optString("accessToken");
                            if (!TextUtils.isEmpty(accessToken)) {
                                mApp.getPref().edit().putString(SCConstants.PREFERENCE.SC_KEY_ACCESS_TOKEN, accessToken).apply();
                                SelfCareAccountApi.getInstant(mApp).setAccessToken(accessToken);
                                mActivity.goToHome();
                            } else
                                mActivity.showToast(R.string.e601_error_but_undefined);
                        } else
                            mActivity.showToast(R.string.e601_error_but_undefined);

                    } else if (code == 1) {
                        mActivity.showToast("NRC or Passport đã tồn tại trên hệ thống");
                    } else if (code == 2) {
                        mActivity.showToast("Email đã tồn tại trên hệ thống");
                    } else if (code == 3) {
                        mActivity.showToast("Số điện thoại đã tồn tại trên hệ thống");
                    } else if (code == 4) {
                        mActivity.showToast("Username đã tồn tại trên hệ thống");
                    } else if (code == 5) {
                        mActivity.showToast("Request sai thông tin");
                    } else {
                        mActivity.showToast(R.string.e601_error_but_undefined);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException", e);
                    mActivity.showToast(R.string.e601_error_but_undefined);
                }
            }

            @Override
            public void onError(int code, String message) {
                mActivity.hideLoadingDialog();
                Log.e(TAG, "onError: " + code + " msg: " + message);
                mActivity.showToast(R.string.e601_error_but_undefined);
            }
        });
    }


    private class DeactiveAsyncTask extends AsyncTask<String, String, Boolean> {
        private XMPPManager mXmppManager;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mXmppManager = mApp.getXmppManager();
            mActivity.showLoadingDialog("", R.string.loading);
        }

        @Override
        protected Boolean doInBackground(String... action) {
            try {
                //    mFacebookBusiness.closeSession();
                mXmppManager.removeXmppListener();
                mApp.getReengAccountBusiness().deactivateAccount(mApp);
                return true;
            } catch (XMPPException xe) {
                Log.e(TAG, "XMPPException", xe);
                return false;
            } catch (Exception e) {
                Log.e(TAG, "Exception", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                mApp.cancelNotification(Constants.NOTIFICATION.NOTIFY_MESSAGE);
                mApp.cancelNotification(Constants.NOTIFICATION.NOTIFY_ONMEDIA);
                mApp.cancelNotification(Constants.NOTIFICATION.NOTIFY_OTHER);
                mApp.getXmppManager().destroyXmpp();
                mApp.removeCountNotificationIcon();
                //logout facebook + reinit
                if (!Config.Server.FREE_15_DAYS) {
                    LoginManager.getInstance().logOut();
                }
                // reset music
                mApp.getMusicBusiness().clearSessionAndNotifyMusic();
                clearAndGotoHome();
                // restartApplication();
            } else {
                mActivity.showToast(getString(R.string.e604_error_connect_server), Toast.LENGTH_SHORT);
                // add xmpp listener
                mXmppManager.addXmppListener();
            }
        }
    }

    private void clearAndGotoHome() {
        SettingWorkManager.cancelSettingNotiWork();
        mApp.recreateBusiness();
        Intent i = new Intent(mApp, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mActivity.finish();
        mActivity.startActivity(i);
    }
}
*/

package com.metfone.selfcare.module.metfoneplus.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;

public class BuyServiceCategoryViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout mLayout;
    public AppCompatImageView mImage;
    public AppCompatTextView mTitle;
    public AppCompatTextView mContent;
    public int mPosition;

    public BuyServiceCategoryViewHolder(@NonNull View itemView) {
        super(itemView);
        mLayout = itemView.findViewById(R.id.layout_buy_service_category);
        mImage = itemView.findViewById(R.id.item_buy_service_category_image);
        mTitle = itemView.findViewById(R.id.item_buy_service_category_title);
        mContent = itemView.findViewById(R.id.item_buy_service_category_content);
    }
}

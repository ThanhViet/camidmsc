package com.metfone.selfcare.module.selfcare.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SCPackage implements Serializable {
//    {
//        "id": "HM25",
//            "name": "International Calls Like Local Monthly",
//            "imgUrl": "https://imagemytel.viettelglobal.net/1551407343156.jpeg",
//            "shortDest": null,
//            "description": null,
//            "vasType": null,
//            "language": null,
//            "vasDescriptions": [
//        {
//            "id": 25,
//                "description": null,
//                "shortDesCription": "Let's save money up to 88% of the normal Pay-As-You-Go rate for International calls to 12 destinations. With this promotion, you can enjoy international calling rate as local rate.",
//                "language": "EN"
//        }
//            ],
//        "registerable": true
//    }

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    @SerializedName("shortDest")
    private String shortDes;

    @SerializedName("description")
    private String des;

    @SerializedName("imgUrl")
    private String iconUrl;

    @SerializedName("vasType")
    private String vasType;

    @SerializedName("language")
    private String language;

    @SerializedName("vasDescriptions")
    private ArrayList<SCVasDesciption> vasDescriptions = new ArrayList<>();

    @SerializedName("vasPackageRefs")
    private ArrayList<SCVasPackageRef> vasPackageRefs = new ArrayList<>();

    @SerializedName("registerable")
    private boolean registerable;

    private boolean isRegister = false;

    public boolean isRegister() {
        return isRegister;
    }

    public void setRegister(boolean register) {
        isRegister = register;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return id;
    }

    public void setCode(String code) {
        this.id = code;
    }

    public String getShortDes() {
        if(getVasDescriptions() != null && getVasDescriptions().size() > 0)
        {
            if(!TextUtils.isEmpty(getVasDescriptions().get(0).getShortDesCription()))
                return getVasDescriptions().get(0).getShortDesCription();
            else
                getVasDescriptions().get(0).getDescription();
        }
        else
        {
            if(TextUtils.isEmpty(shortDes))
                return des;
        }
        return shortDes;
    }

    public void setShortDes(String shortDes) {
        this.shortDes = shortDes;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getVasType() {
        return vasType;
    }

    public void setVasType(String vasType) {
        this.vasType = vasType;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ArrayList<SCVasDesciption> getVasDescriptions() {
        return vasDescriptions;
    }

    public void setVasDescriptions(ArrayList<SCVasDesciption> vasDescriptions) {
        this.vasDescriptions = vasDescriptions;
    }

    public boolean isRegisterable() {
        return registerable;
    }

    public void setRegisterable(boolean registerable) {
        this.registerable = registerable;
    }

    public ArrayList<SCVasPackageRef> getVasPackageRefs() {
        return vasPackageRefs;
    }

    @Override
    public String toString() {
        return "SCPackage{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", shortDes='" + shortDes + '\'' +
                ", des='" + des + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", vasType='" + vasType + '\'' +
                ", language='" + language + '\'' +
                ", vasDescriptions=" + vasDescriptions +
                ", registerable=" + registerable +
                ", isRegister=" + isRegister +
                '}';
    }
}

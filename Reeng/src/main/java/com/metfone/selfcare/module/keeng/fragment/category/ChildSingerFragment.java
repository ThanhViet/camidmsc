package com.metfone.selfcare.module.keeng.fragment.category;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.module.keeng.TabKeengActivity;
import com.metfone.selfcare.module.keeng.adapter.category.ChildSingerAdapter;
import com.metfone.selfcare.module.keeng.base.BaseAdapterMedia;
import com.metfone.selfcare.module.keeng.base.BaseListener;
import com.metfone.selfcare.module.keeng.base.RecyclerFragment;
import com.metfone.selfcare.module.keeng.model.AllModel;
import com.metfone.selfcare.module.keeng.model.MediaLogModel;
import com.metfone.selfcare.module.keeng.model.PlayingList;
import com.metfone.selfcare.module.keeng.model.SingerInfo;
import com.metfone.selfcare.module.keeng.model.Topic;
import com.metfone.selfcare.module.keeng.network.KeengApi;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.keeng.utils.Log;

import java.util.List;

public class ChildSingerFragment extends RecyclerFragment<AllModel> implements BaseListener.OnLoadMoreListener {

    private int currentType = Constants.TYPE_SONG;
    private Topic topic;
    private BaseAdapterMedia adapter;
    private TabKeengActivity mActivity;
    private ListenerUtils listenerUtils;
    private SingerInfoUpdateListener listener;

    public static ChildSingerFragment instances(Bundle argument, SingerInfoUpdateListener listener) {
        ChildSingerFragment mInstance = new ChildSingerFragment();
        mInstance.listener = listener;
        mInstance.setArguments(argument);
        return mInstance;
    }

    @Override
    public String getName() {
        return "ChildSingerFragment";
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TabKeengActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            topic = (Topic) getArguments().getSerializable(Constants.KEY_DATA);
            currentType = getArguments().getInt("type");
        }

        adapter = new ChildSingerAdapter(mActivity, getDatas(), TAG);
        setupRecycler(adapter);
        adapter.setRecyclerView(recyclerView, this);
        listenerUtils = ApplicationController.self().getListenerUtils();
        if (listenerUtils != null) {
            listenerUtils.addListener(this);
        }
    }

    @Override
    public void onDestroyView() {
        if (listenerUtils != null) {
            listenerUtils.removerListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDatas().size() == 0) {
            doLoadData(true);
        }
    }

    private void doLoadData(boolean type) {
        if (!isLoading) {
            if (type)
                loadingBegin();
            isLoading = true;
            loadMediaSinger();
        }
    }

    private void loadMediaSinger() {
        if (mActivity == null || topic == null)
            return;
        new KeengApi().getSingerDetail(topic.getSingerId(), currentType, currentPage, numPerPage
                , result -> {
                    loadMediaComplete(result.getData());
                    updateSingerInfo(result.getSingerInfo());
                },
                error -> {
                    Log.e(TAG, error);
                    loadMediaComplete(null);
                });
    }

    private void updateSingerInfo(SingerInfo singerInfo) {
        if (listener != null) {
            topic.setTotalSongs(singerInfo.getNumSong());
            topic.setTotalAlbums(singerInfo.getNumAlbum());
            topic.setTotalVideos(singerInfo.getNumVideo());
            listener.onInfoUpdate(topic);
        }
    }

    protected void loadMediaComplete(List<AllModel> result) {
        isLoading = false;
        try {
            checkLoadMoreAbsolute(result);
            if (result == null) {
                if (isRefresh) {
                    isRefresh = false;
                }
                loadMored();
                adapter.setLoaded();
                loadingError(v -> doLoadData(true));
                return;
            }

            if (getDatas().size() == 0 && result.size() == 0) {
                refreshed();
                loadMored();
                loadingListSingerEmpty(currentType);
            } else {
                refreshed();
                loadMored();
                if (currentType == Constants.TYPE_SONG)
                    ConvertHelper.convertData(result, MediaLogModel.SRC_SINGER);
                setDatas(result);
                adapter.setLoaded();
                adapter.notifyDataSetChanged();
                loadingFinish();
                currentPage++;
            }

        } catch (Exception e) {
            Log.e(TAG, e);
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        canLoadMore = true;
        currentPage = 1;
        doLoadData(false);
    }

    @Override
    public void onLoadMore() {
        if (!isLoading && !isRefresh && canLoadMore) {
            loadMore();
            doLoadData(false);
        }
    }

    @Override
    public void onMediaClick(View v, int position) {
        if (mActivity == null || adapter == null)
            return;
        AllModel item = adapter.getItem(position);
        if (item != null) {
            item.setSource(MediaLogModel.SRC_SINGER);
            switch (item.type) {
                case Constants.TYPE_ALBUM:
                case Constants.TYPE_ALBUM_VIDEO:
                    mActivity.gotoAlbumDetail(item);
                    break;
                case Constants.TYPE_SONG:
                    PlayingList playingList = new PlayingList(getDatas(), PlayingList.TYPE_FULL_DATA, MediaLogModel.SRC_SINGER);
                    mActivity.setMediaPlayingAudio(playingList, position);
                    break;
                case Constants.TYPE_VIDEO:
                    item.setSource(MediaLogModel.SRC_SINGER);
                    mActivity.setMediaToPlayVideo(item);
                    break;
            }
        }
    }

    @Override
    public void onMediaExpandClick(View v, int position) {
        if (mActivity == null || adapter == null)
            return;
        AllModel item = adapter.getItem(position);
        mActivity.showPopupMore(item);
    }

    @Override
    public void onInternetChanged() {
        if (NetworkHelper.isConnectInternet(mActivity) && recyclerView != null && getDatas().size() == 0)
            onRefresh();
    }

    interface SingerInfoUpdateListener {
        void onInfoUpdate(Topic topic);
    }
}

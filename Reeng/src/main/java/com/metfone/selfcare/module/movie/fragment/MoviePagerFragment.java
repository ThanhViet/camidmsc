/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.fragment;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContactBusiness;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.ApiCallbackV3;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.SharedPrefs;
import com.metfone.selfcare.common.utils.listener.ListenerUtils;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.home.TabHomeHelper;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieKind;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tabMovie.SubtabInfo;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.WsSliderFilmResponse;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.event.LoadInfoUser;
import com.metfone.selfcare.module.home_kh.model.event.OpenRewardCamID;
import com.metfone.selfcare.module.home_kh.model.event.RequestRefreshInfo;
import com.metfone.selfcare.module.home_kh.service.LoadCinemaService;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.module.keeng.event.EventHelper;
import com.metfone.selfcare.module.keeng.widget.CustomLinearLayoutManager;
import com.metfone.selfcare.module.movie.adapter.BoxContentAdapter;
import com.metfone.selfcare.module.movie.adapter.FilmTopicAdapter;
import com.metfone.selfcare.module.movie.adapter.MoviePagerAdapter;
import com.metfone.selfcare.module.movie.event.RequestPermissionBright;
import com.metfone.selfcare.module.movie.event.RequestPermissionVolume;
import com.metfone.selfcare.module.movie.event.UpdateWatchedEvent;
import com.metfone.selfcare.module.movie.listener.LoadmoreListener;
import com.metfone.selfcare.module.movie.listener.TabMovieListener;
import com.metfone.selfcare.module.movie.model.HomeCinemaDataCache;
import com.metfone.selfcare.module.movie.model.MoviePagerModel;
import com.metfone.selfcare.module.movienew.dialog.TrailerFullScreenPlayerDialog;
import com.metfone.selfcare.module.movienew.fragment.CategoryFragmentNew;
import com.metfone.selfcare.module.movienew.fragment.MyListFragment;
import com.metfone.selfcare.module.movienew.fragment.SearchMovieNewFragment;
import com.metfone.selfcare.module.movienew.holder.CinemaPlayerHolder;
import com.metfone.selfcare.module.movienew.listener.TrailerPlayerListener;
import com.metfone.selfcare.module.movienew.model.Category;
import com.metfone.selfcare.module.movienew.model.CustomTopic;
import com.metfone.selfcare.module.movienew.model.CustomTopicFilm;
import com.metfone.selfcare.module.movienew.model.HomeData;
import com.metfone.selfcare.module.movienew.model.HomeResult;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.module.movienew.model.evenbus.MessageStopVideo;
import com.metfone.selfcare.module.movienew.model.evenbus.UpdateDataFromService;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.network.metfoneplus.MPApiCallback;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.LoadingView;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.imageview.CircleImageView;
import com.metfone.selfcare.ui.tabvideo.listener.OnInternetChangedListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnTabListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.ImageUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.home.fragment.BaseHomeFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;
import static com.metfone.selfcare.helper.Constants.PREFERENCE.PREF_IS_LOGIN;

public class MoviePagerFragment extends BaseHomeFragment implements OnInternetChangedListener
        , SwipeRefreshLayout.OnRefreshListener, TabMovieListener.OnAdapterClick, OnClickMoreItemListener
        , OnTabListener {

    public static final String LOAD_DATA_SERVICE = "load_data_service";
    public static final String FROM_LOGIN = "from_login";
    public static final String FROM_LOGOUT = "from_logout";
    public static final String FROM_CHANGE_LANGUAGE = "from_change_language";

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.loading_view)
    LoadingView loadingView;
    @BindView(R.id.btnSearch)
    AppCompatImageView btnSearch;
    @BindView(R.id.btn_like)
    AppCompatImageView btn_like;
    @BindView(R.id.btn_category)
    AppCompatImageView btn_category;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.layout_guest)
    LinearLayout layoutGuest;
    @BindView(R.id.layout_user)
    LinearLayout layoutUser;
    @BindView(R.id.img_avatar)
    CircleImageView imgAvatar;
    @BindView(R.id.img_grade)
    AppCompatImageView imgCrown;
    @BindView(R.id.tvName)
    AppCompatTextView tvName;
    @BindView(R.id.tvRankName)
    AppCompatTextView tvRankName;
    @BindView(R.id.layout_rank)
    RelativeLayout layout_rank;
    @BindView(R.id.nscr_home_movie)
    NestedScrollView mScrHomeMovie;
    @BindView(R.id.rll_home_movie)
    RelativeLayout rll_home_movie;
    @BindView(R.id.layout_no_data)
    LinearLayout llNoData;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.viewLoadingAnimation)
    LinearLayout viewLoadingAnimation;
    private boolean checkGetListHome = false;
    private boolean getContinueWatchingList = false;
    private boolean getNotiList = false;
    MoviePagerModel[] emptyMoviePagerModels = new MoviePagerModel[10];
    UserInfo currentUser;
    UserInfoBusiness userInfoBusiness;
    private LinearLayoutManager layoutManager;
    private int position = -1;
    private boolean isLoading;
    private boolean isRefresh = false;
    private boolean needLoadDataFromCache = false;
    private ArrayList<MoviePagerModel> mDataCinema;
    private ArrayList<Object> watchedList;
    private MoviePagerAdapter mMoviePagerAdapter;
    private MovieApi movieApi;
    private ListenerUtils mListenerUtils;
    private String mLastRecentContact;
    private Movie mCurrentMovie;
    private Video mCurrentVideo;
    private String mLabelAvailable;
    private boolean isLoadingHome = false;
    private TrailerFullScreenPlayerDialog trailerFullScreenPlayerDialog;
    private DialogConfirm dialogBright;
    private DialogConfirm dialogVolume;
    private MoviePagerModel[] mMoviePagerModels;
    private boolean isLoadmore;
    private boolean loadingContinueWatching;
    private int mPage = 1;
    private RecyclerView.Adapter mAdapterNeedLoadmore;
    private List<RecyclerView.Adapter> mListAdapterLoadmore = new ArrayList<>();
    private ArrayList<Movie> episodes;
    private ArrayList<Object> listBanner = new ArrayList<>();

    public static MoviePagerFragment newInstance() {
        Bundle args = new Bundle();
        MoviePagerFragment fragment = new MoviePagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    private MPApiCallback<WsSliderFilmResponse> callbackSlider = new MPApiCallback<WsSliderFilmResponse>() {
        @Override
        public void onResponse(retrofit2.Response<WsSliderFilmResponse> response) {
            if (response != null && response.body() != null && response.body().getResult() != null && response.body().getResult().getWsResponse() != null && response.body().getResult().getWsResponse().getHomeCinema() != null) {
                ArrayList<HomeData> copyBanners = new ArrayList<>();
                String id = response.body().getResult().getWsResponse().getHomeCinema();
                /*for (String id : response.body().getResult().getWsResponse().getHomeSlider()) {
                    for (Object ob : listBanner) {
                        HomeData homeData = (HomeData) ob;
                        if (id.equals(String.valueOf(homeData.getId()))) {
                            copyBanners.add(homeData);
                            break;
                        }
                    }
                    if (copyBanners.size() > 0) break;
                }*/
                for (Object ob : listBanner) {
                    HomeData homeData = (HomeData) ob;
                    if (id.equals(String.valueOf(homeData.getId()))) {
                        copyBanners.add(homeData);
                        break;
                    }
                }
                if(copyBanners.size() > 0){
                    MoviePagerModel bannerModel = new MoviePagerModel();
                    bannerModel.setType(MoviePagerModel.TYPE_SLIDER_BANNER);
                    bannerModel.setTitle(copyBanners.get(0).getName());
                    bannerModel.getList().add(copyBanners.get(0));
                    mMoviePagerModels[0] = bannerModel;

                    if (mDataCinema == null) {
                        mDataCinema = new ArrayList<>();
                    } else {
                        mDataCinema.clear();
                    }
                    for (MoviePagerModel moviePagerModel : mMoviePagerModels) {
                        mDataCinema.add(moviePagerModel);
                    }
                    mMoviePagerAdapter.updateData();
                }
            }
        }

        @Override
        public void onError(Throwable error) {

        }
    };

    @Override
    public int getResIdView() {
        return R.layout.fragment_pager_content_v2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            position = bundle.getInt(Constants.TabVideo.POSITION);
            position = bundle.getInt(Constants.KEY_POSITION);
            Serializable serializable = bundle.getSerializable(Constants.KEY_DATA);
        }

        loadingView.setOnClickRetryListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                loadData();
            }
        });


        loadingView.setClickable(true);
        userInfoBusiness = new UserInfoBusiness(getActivity());
        mScrHomeMovie.fullScroll(ScrollView.FOCUS_UP);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.bg_mocha));
        swipeRefreshLayout.setOnRefreshListener(this);
        if (mDataCinema == null) mDataCinema = new ArrayList<>();
        else mDataCinema.clear();
        mMoviePagerAdapter = new MoviePagerAdapter(activity, new LoadmoreListener.ILoadmoreAtPositionListener() {
            @Override
            public void loadmoreAtPosition(int position, RecyclerView.Adapter adapter) {
                if (isLoading) return;
                isLoadmore = true;
                mAdapterNeedLoadmore = adapter;
                if (!mListAdapterLoadmore.contains(mAdapterNeedLoadmore)) {
                    mListAdapterLoadmore.add(mAdapterNeedLoadmore);
                }
                if (mAdapterNeedLoadmore instanceof BoxContentAdapter) {
                    if (((BoxContentAdapter) mAdapterNeedLoadmore).getPageLoaded() < HomeCinemaDataCache.getInstance().getListPageMovie().size()) {
                        loadmoreFromCache();
                    } else {
                        getHomeMovieList(mMoviePagerModels, true);
                    }
                }
            }
        }, new LoadmoreListener.ILoadmoreAtPositionRclvInsideRclv() {
            @Override
            public void loadmoreAtPositionOfRclvInsideRclv(int position, RecyclerView.Adapter adapter) {
                if (isLoading) return;
                isLoadmore = true;
                mAdapterNeedLoadmore = adapter;
                if (mAdapterNeedLoadmore instanceof FilmTopicAdapter) {
                    if (((FilmTopicAdapter) mAdapterNeedLoadmore).getPageLoaded() < HomeCinemaDataCache.getInstance().getListPageMovie().size()) {
                        loadmoreFromCache();
                    } else {
                        getHomeMovieList(mMoviePagerModels, false);
                    }
                }
            }
        });
        mMoviePagerAdapter.setItems(mDataCinema);
        mMoviePagerAdapter.setListener(this);
        mMoviePagerAdapter.setPlayNowListener(new TabMovieListener.IPlayVideoNow() {
            @Override
            public void onPlayNow(Movie movie) {
                logActiveCinema();
                mCurrentMovie = movie;
                if (StringUtils.isEmpty(movie.getIdGroup()) || movie.getIdGroup().equals("0")) {
                    VideoPlayerActivity.playNow(getContext(), mCurrentMovie, mCurrentVideo, "");
                } else {
                    activity.showLoadingDialog("", "");
                    getAllEpisode(movie.getIdGroup());
                }
            }

            @Override
            public void playTrailer(String title, String urlTrailer, long currentTime) {
                if (trailerFullScreenPlayerDialog != null && trailerFullScreenPlayerDialog.isShowing()) {
                    trailerFullScreenPlayerDialog.dismiss();
                }
                CinemaPlayerHolder holder = getCinemaPlayerHolder();
                if (holder != null) {
                    holder.stopVideo();
                }
                trailerFullScreenPlayerDialog = new TrailerFullScreenPlayerDialog(activity);
                trailerFullScreenPlayerDialog.setData(title, urlTrailer, currentTime);
                trailerFullScreenPlayerDialog.setTrailerPlayerListener(new TrailerPlayerListener() {
                    @Override
                    public void dismissTrailerDialog(boolean continuePlay, boolean isEnd, long currentTime) {
                        if (activity != null) {
                            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        }
                        if (holder != null) {
                            holder.updateProgress(currentTime);
                            if (isEnd) {
                                holder.updateProgress(0);
                                holder.startVideo();
                            } else {
                                holder.updateProgress(currentTime);
                                if (continuePlay) {
                                    holder.startVideo();
                                } else {
                                    holder.stopVideo();
                                }
                            }
                        }
                    }
                });
                trailerFullScreenPlayerDialog.show();
            }
        });
        layoutManager = new CustomLinearLayoutManager(activity, LinearLayout.VERTICAL, false);
        BaseAdapter.setupVerticalRecyclerView(activity, recyclerView, layoutManager, mMoviePagerAdapter, false, 3);
        btnSearch.setOnClickListener(v -> {
            logActiveCinema();
            Intent intent = new Intent(getActivity(), SearchMovieNewFragment.class);
            getActivity().startActivity(intent);
        });
        btn_like.setOnClickListener(v -> {
            logActiveCinema();
            MyListFragment myListFragment = MyListFragment.newInstance(getString(R.string.my_list), "");
            addFragment(R.id.frameCategoryFilm, myListFragment, "MyListFragment");
        });
        btn_category.setOnClickListener(v -> {
            logActiveCinema();
            addFragment(R.id.frameCategoryFilm, CategoryFragmentNew.newInstance(), "CategoryFragmentNew");
        });
        Utilities.adaptViewForInserts(header);
        layoutGuest.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
//                logActiveCinema();
                SharedPrefs.getInstance().put(PREF_IS_LOGIN, false);
                NavigateActivityHelper.navigateToRegisterScreenActivity((BaseSlidingFragmentActivity) getActivity(), true);
            }
        });
        imgAvatar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                logActiveCinema();
                NavigateActivityHelper.navigateToSetUpProfile(activity);
            }
        });
        layout_rank.setOnClickListener(v -> {
            logActiveCinema();
            EventBus.getDefault().post(new OpenRewardCamID(TabHomeHelper.HomeTab.tab_cinema));
        });
        tvName.setOnClickListener(v -> {
            logActiveCinema();
            NavigateActivityHelper.navigateToSetUpProfile(activity);
        });
        prepareLoadingAnim(false);
        currentUser = new UserInfoBusiness(getActivity()).getUser();
        handleScroll();
    }

    private void loadmoreFromCache() {
        pbLoading.setVisibility(View.GONE);
        // viewLoadingAnimation.setVisibility(View.VISIBLE);
//        shimmerFrameLayout.startShimmer();
//        shimmerFrameListLayout.startShimmer();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mAdapterNeedLoadmore instanceof BoxContentAdapter) {
                    if (((BoxContentAdapter) mAdapterNeedLoadmore).getBlockName().equals(activity.getString(R.string.cinema_trending))) {
                        ((MoviePagerModel) ((BoxContentAdapter) mAdapterNeedLoadmore).getItem(0)).
                                getList().addAll(((MoviePagerModel) HomeCinemaDataCache.getListPageMovie().
                                get(((BoxContentAdapter) mAdapterNeedLoadmore).getPageLoaded()).getListTrending().get(0)).getList());
                    } else if (((BoxContentAdapter) mAdapterNeedLoadmore).getBlockName().equals(activity.getString(R.string.cinema_recently_added))) {
                        ((MoviePagerModel) ((BoxContentAdapter) mAdapterNeedLoadmore).getItem(0)).
                                getList().addAll(((MoviePagerModel) HomeCinemaDataCache.getListPageMovie().
                                get(((BoxContentAdapter) mAdapterNeedLoadmore).getPageLoaded()).getListRecentlyAdded().get(0)).getList());
                    }
                    ((BoxContentAdapter) mAdapterNeedLoadmore).increasePageLoaded();
                    mAdapterNeedLoadmore.notifyDataSetChanged();
                } else if (mAdapterNeedLoadmore instanceof FilmTopicAdapter) {
                    for (Object customTopic : HomeCinemaDataCache.getListPageMovie().
                            get(((FilmTopicAdapter) mAdapterNeedLoadmore).getPageLoaded()).getListCustomTopic()) {
                        if (((FilmTopicAdapter) mAdapterNeedLoadmore).getBlockName().equals(((CustomTopic) customTopic).getTopicName())) {
                            ((FilmTopicAdapter) mAdapterNeedLoadmore).addListAndNotify(((CustomTopic) customTopic).getListCustomFilm());
                            ((FilmTopicAdapter) mAdapterNeedLoadmore).increasePageLoaded();
                            break;
                        }
                    }
                }
                pbLoading.setVisibility(View.GONE);
//                viewLoadingAnimation.setVisibility(View.GONE);
//                shimmerFrameLayout.clearAnimation();
//                shimmerFrameListLayout.clearAnimation();
                recyclerView.setVisibility(View.VISIBLE);
            }
        }, 1000);

    }

//    private void setAdapterData(RecyclerView.Adapter adapter) {
//        if (adapter instanceof BoxContentAdapter) {
//            if (mAdapterTrendingNow == null && ((BoxContentAdapter) adapter).getBlockName().equals(res.getString(R.string.cinema_trending))) {
//                mAdapterTrendingNow = adapter;
//            } else if (mAdapterRecentlyAdded == null && ((BoxContentAdapter) adapter).getBlockName().equals(res.getString(R.string.cinema_recently_added))) {
//                mAdapterRecentlyAdded = adapter;
//            }
//        }
//    }

    private void getAllEpisode(String filmGroupId) {
        getMovieApi().getListGroupsDetail(filmGroupId,
                new ApiCallbackV3<ArrayList<Movie>>() {
                    @Override
                    public void onSuccess(String lastId, ArrayList<Movie> movies, MovieWatched movieWatched) throws JSONException {
                        if (movies != null && movies.size() > 0) {
                            boolean hasWatched = false;
                            episodes = movies;
                            for (int i = 0; i < movies.size(); i++) {
                                if (movies.get(i).getId().equals(movieWatched.getId())) {
                                    Movie movie = movies.get(i);
                                    movie.setTimeWatched(movieWatched.getTimeSeek());
                                    movies.set(i, movie);
                                    Video lastWatched = Movie.movie2Video(movie);
                                    loadMovieInfoWatched(lastWatched, movieWatched.getTimeSeek());
                                    hasWatched = true;
                                    break;
                                }
                            }
                            if (!hasWatched) {
                                activity.hideLoadingDialog();
                                VideoPlayerActivity.playNow(getContext(), mCurrentMovie, mCurrentVideo, episodes, "");
                            }
                        } else {
                            activity.hideLoadingDialog();
                            ToastUtils.makeText(activity, R.string.video_message_cannot_play_video);
                        }
                    }

                    @Override
                    public void onError(String s) {
                        activity.hideLoadingDialog();
                        VideoPlayerActivity.playNow(getContext(), mCurrentMovie, mCurrentVideo, "");
                    }

                    @Override
                    public void onComplete() {
                    }
                });
//        }
    }

    private void loadMovieInfoWatched(Video mVideo, String timeWatched) {
        new MovieApi().getMovieDetail(mVideo.getId(), false, new ApiCallbackV2<Movie>() {
            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                activity.hideLoadingDialog();
                if (result != null) {
                    Movie.copyLinkFromMovie(mVideo, result);
                    mCurrentVideo = mVideo;
                    mCurrentVideo.setTimeWatched(timeWatched);
                    VideoPlayerActivity.playNow(getContext(), mCurrentMovie, mCurrentVideo, episodes, "");
                }
            }

            @Override
            public void onError(String s) {
                activity.hideLoadingDialog();
                VideoPlayerActivity.playNow(getContext(), mCurrentMovie, mCurrentVideo, episodes, "");
            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void prepareLoadingAnim(boolean isShowRefresh) {
        // Recommend loading data
        List<HomeData> recommendDatas = new ArrayList<>();
        HomeData recommendData = new HomeData();
        recommendDatas.add(recommendData);

        MoviePagerModel recommendModel = new MoviePagerModel();
        recommendModel.setType(MoviePagerModel.TYPE_SLIDER_BANNER_EMPTY);
        recommendModel.setTitle("");
        recommendModel.getList().addAll(recommendDatas);
        recommendModel.setShowRefresh(isShowRefresh);
        emptyMoviePagerModels[0] = recommendModel;

        // Trending loading data
        MoviePagerModel trendingModel = new MoviePagerModel();
        trendingModel.setType(MoviePagerModel.TYPE_BOX_CONTENT);
        trendingModel.setTitle(res.getString(R.string.cinema_trending));

        MoviePagerModel gridItem = new MoviePagerModel();
        gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
        List<HomeData> trendingDatas = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            HomeData trendingData = new HomeData();
            trendingDatas.add(trendingData);
        }
        gridItem.getList().addAll(trendingDatas);
        trendingModel.getList().add(gridItem);

        emptyMoviePagerModels[1] = trendingModel;

        for (MoviePagerModel moviePagerModel : emptyMoviePagerModels) {
            mDataCinema.add(moviePagerModel);
        }
        if (mMoviePagerAdapter != null) mMoviePagerAdapter.updateData();
    }

    @Override
    public TabLayout getTabLayout() {
        return null;
    }

    @Override
    public int getColor() {
        return ContextCompat.getColor(activity, R.color.home_tab_movie);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListenerUtils = app.getListenerUtils();
        if (mListenerUtils != null) mListenerUtils.addListener(this);

        if (activity.getIntent() != null) {
            if (activity.getIntent().getExtras() != null && activity.getIntent().getExtras().getBoolean(LOAD_DATA_SERVICE, false)) {
                if (!LoadCinemaService.isRunning()) {// Tu man hinh splash,Neu service da dung, thi lay tu cache da luu
                    if (HomeCinemaDataCache.getMoviePagerModels() != null) { // Service da dung, kiem tra cache, khong co thi request lai
                        getDataFromCache();
                    } else {
                        loadData();
                    }
                } else {
                    // service dang chay, se lay data trong broadcast
                }
            } else if (activity.getIntent().getBooleanExtra(FROM_LOGIN, false) || activity.getIntent().getBooleanExtra(FROM_LOGOUT, false) || activity.getIntent().getBooleanExtra(FROM_CHANGE_LANGUAGE, false)) {
                // Tu man hinh login hoac logout, lay du lieu tu cache de hien thi, sau do request du lieu moi
                if (HomeCinemaDataCache.getMoviePagerModels() != null) {
                    isRefresh = true; // set bang true de k show place holder
                    getDataFromCache();
                }
                loadData(); // load lai data vi da logout hoac login
            } else {
                if (HomeCinemaDataCache.getMoviePagerModels() == null) { // Neu k co cache thi moi request
                    loadData();
                } else {
                    getDataFromCache();
                }
            }
        } else { // Cac truong hop khac thi lay du lieu cache
            if (HomeCinemaDataCache.getMoviePagerModels() == null) {
                loadData();
            } else {
                getDataFromCache();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && needLoadDataFromCache) {
            getDataFromCache();
        }
    }

    @Override
    public void onDestroyView() {
        if (mListenerUtils != null) mListenerUtils.removerListener(this);
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        if (isLoading) {
            if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
        } else {
            HomeCinemaDataCache.setMoviePagerModels(null);
            if (mDataCinema != null && mDataCinema.size() == 0) {
                prepareLoadingAnim(false);
            }
            isRefresh = true;
            loadData();
            AdsManager.getInstance().reloadAdsNative();
        }
        EventBus.getDefault().post(new RequestRefreshInfo());
    }

    @Override
    public void onInternetChanged() {

    }

    public void hideRefresh() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.destroyDrawingCache();
            swipeRefreshLayout.clearAnimation();
        }
    }

    private void loadData() {
        if (isLoading) return;
        isLoading = true;
        isLoadmore = false;
        mPage = 1;
        if (mListAdapterLoadmore != null && mListAdapterLoadmore.size() > 0) {
            for (RecyclerView.Adapter adapter : mListAdapterLoadmore) {
                if (adapter instanceof BoxContentAdapter) {
                    ((BoxContentAdapter) adapter).setPageLoaded(0);
                }
            }
        }
        HomeCinemaDataCache.reset();
        if (mMoviePagerModels == null) {
            mMoviePagerModels = new MoviePagerModel[10];
        }
        getMoviesForYou(mMoviePagerModels);
        // if user not logged, hide Continue watching (not call api: getContinueWatching)
        if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.NOT) {
            getHomeMovieList(mMoviePagerModels, false);
        } else {
            if (UserInfoBusiness.isLogin() == EnumUtils.LoginVia.MOCHA_OPEN_ID) {
                ContactBusiness contactBusiness = app.getContactBusiness();
                if (!contactBusiness.isInitArrayListReady()) {
                    contactBusiness.initArrayListPhoneNumber();
                }
                if (contactBusiness.getRecentPhoneNumber() != null &&
                        contactBusiness.getRecentPhoneNumber().size() > 0 &&
                        !TextUtils.isEmpty(contactBusiness.getRecentPhoneNumber().get(0).getName())) {
                    mLastRecentContact = contactBusiness.getRecentPhoneNumber().get(0).getJidNumber();
                    mMoviePagerAdapter.setNameWatchedByFriend(contactBusiness.getRecentPhoneNumber().get(0).getName());
                    getWatchedByFriend(mMoviePagerModels, contactBusiness.getRecentPhoneNumber().get(0).getJidNumber());
                }
            }
        }
        getContinueWatchingList(mMoviePagerModels);
    }

    private void getDataFromCache() {
        Log.d("LoadCinemaService", "getDataFromCache");
        needLoadDataFromCache = false;
        mLabelAvailable = HomeCinemaDataCache.getTrendingTitle();
        mMoviePagerModels = HomeCinemaDataCache.getMoviePagerModels();
        if (HomeCinemaDataCache.getWatchedFriendName() != null) {
            mMoviePagerAdapter.setNameWatchedByFriend(HomeCinemaDataCache.getWatchedFriendName());
        }
        mDataCinema.clear();
        for (MoviePagerModel moviePagerModel : mMoviePagerModels) {
            if (moviePagerModel != null) {
                mDataCinema.add(moviePagerModel);
            }
        }
        mMoviePagerAdapter.updateData();
        recyclerView.setVisibility(View.VISIBLE);
        checkGetListHome = true;
        getContinueWatchingList = true;
        getNotiList = true;
        if (loadingView != null) loadingView.showLoadedSuccess();
        goneLoading();
        if(mMoviePagerModels[0] != null && mMoviePagerModels[0].getType() == MoviePagerModel.TYPE_SLIDER_BANNER){
            listBanner.clear();
            listBanner.addAll(mMoviePagerModels[0].getList());
        }
        KhHomeClient.getInstance().getSliderFilm(callbackSlider);
    }

    @Override
    public void onClickTitleBox(Object item, int position) {
        logActiveCinema();
        if (item instanceof HomeData) {
            HomeData homeData = (HomeData) item;
            if (homeData.getId() == null || homeData.getLinkWap() == null) {
                return;
            }
            activity.playMovies(new Movie(homeData));
        } else if (item instanceof CustomTopicFilm) {
            CustomTopicFilm customTopicFilm = (CustomTopicFilm) item;
            activity.playMovies(new Movie(customTopicFilm));
        }
    }

    @Override
    public void onClickRefresh() {
        onRefresh();
    }

    @Override
    public void playFilm(Object item, int position, boolean isPlayNow) {
        if (item instanceof ListLogWatchResponse.Result) {
            getMovieDetail(((ListLogWatchResponse.Result) item).getIdPhim(), true);
        } else if (item instanceof HomeData) {
            HomeData homeData = (HomeData) item;
            activity.playMovies(new Movie(homeData));
        } else if (item instanceof MoviePagerModel) {
            if (((MoviePagerModel) item).getList().get(0) instanceof ListLogWatchResponse.Result) {
                getMovieDetail(((ListLogWatchResponse.Result) ((MoviePagerModel) item).getList().get(0)).getIdPhim(), true);
            }
        }
    }

    @Override
    public void onScrollData() {
        logActiveCinema();
    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        logActiveCinema();
        if (item == null) {
            return;
        }
        if (item instanceof ListLogWatchResponse.Result) {
            ListLogWatchResponse.Result result = (ListLogWatchResponse.Result) item;
            getMovieDetail(result.getIdPhim(), false);
        } else if (item instanceof MoviePagerModel) {
            if (((MoviePagerModel) item).getList().get(0) instanceof ListLogWatchResponse.Result) {
                getMovieDetail(((ListLogWatchResponse.Result) ((MoviePagerModel) item).getList().get(0)).getIdPhim(), false);
            }
        } else {
            HomeData homeData = (HomeData) item;
            try {
                Movie movie = new Movie(homeData);
                activity.playMovies(movie);
            } catch (NullPointerException e) {
                Log.d(TAG, "Data is not load done");
            }
        }
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (item instanceof MoviePagerModel) {
            if (((MoviePagerModel) item).getObject() instanceof MovieWatched) {
                DialogUtils.showOptionMovieWatched(activity, ((MoviePagerModel) item).getObject(), this);
            } else if (((MoviePagerModel) item).getObject() instanceof Movie) {
                DialogUtils.showOptionMovieItem(activity, (Movie) ((MoviePagerModel) item).getObject(), this);
            }
        } else if (item instanceof MovieWatched) {
            DialogUtils.showOptionMovieWatched(activity, item, this);
        } else if (item instanceof Movie) {
            DialogUtils.showOptionMovieItem(activity, (Movie) item, this);
        }
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    @Override
    public void onClickSliderBannerItem(Object item, int position) {
        if (item instanceof Movie) {
            activity.playMovies((Movie) item);
        } else if (item instanceof MoviePagerModel) {
            MoviePagerModel provisional = (MoviePagerModel) item;
            if (provisional.getObject() instanceof Movie) {
                activity.playMovies((Movie) provisional.getObject());
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (UserInfoBusiness.isLogin() != EnumUtils.LoginVia.NOT && !loadingContinueWatching) {
//            ArrayList<MoviePagerModel> moviePagerModels = mMoviePagerAdapter.getItems();
//            MoviePagerModel[] arr = new MoviePagerModel[moviePagerModels.size()];
//            for (int i = 0; i < moviePagerModels.size(); i++) {
//                arr[i] = moviePagerModels.get(i);
//            }
//            getContinueWatchingList(arr);
//        }
        if (mDataCinema != null || mDataCinema.size() > 0) {
            if (recyclerView.getVisibility() == View.VISIBLE && viewLoadingAnimation != null) {
                viewLoadingAnimation.setVisibility(View.GONE);
            }
        }
        mScrHomeMovie.setFocusable(false);
        mScrHomeMovie.requestFocus();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopVideo();

        if (trailerFullScreenPlayerDialog != null) {
            trailerFullScreenPlayerDialog.stopTrailer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(final UpdateWatchedEvent event) {
        Log.i(TAG, "onEvent UpdateWatchedEvent: " + event);
        EventHelper.removeStickyEvent(event);
    }

    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            if (menuId != Constants.MENU.MENU_EXIT && ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                activity.showDialogLogin();
                return;
            }
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Movie) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) throws JSONException {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    } else if (object instanceof MovieWatched) {
                        Movie movie = MovieWatched.convertToMovie((MovieWatched) object);
                        FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia(movie);
                        new WSOnMedia(app).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) throws JSONException {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Movie) {
                        getMovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                            @Override
                            public void onError(String s) {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onSuccess(String msg, String result) throws JSONException {
                                if (activity != null)
                                    activity.showToast(R.string.add_later_success);
                            }
                        });
                    } else if (object instanceof MovieWatched) {
                        getMovieApi().insertWatchLater(MovieWatched.convertToMovie((MovieWatched) object), new ApiCallbackV2<String>() {

                            @Override
                            public void onError(String s) {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onSuccess(String msg, String result) throws JSONException {
                                if (activity != null)
                                    activity.showToast(R.string.add_later_success);
                            }
                        });
                    }
                    break;
                case Constants.MENU.MENU_REMOVE_WATCHED:
                    if (watchedList != null) {
                        for (int i = 0; i < mDataCinema.size(); i++) {
                            if (mDataCinema.get(i).getType() == MoviePagerModel.TYPE_BOX_WATCHED) {
                                mDataCinema.remove(i);
                                break;
                            } else if (i >= 2) {
                                break;
                            }
                        }
                        watchedList.remove(object);
                    }
                    if (Utilities.notEmpty(watchedList)) {
                        if (mDataCinema.size() > 0) {
                            MoviePagerModel model = new MoviePagerModel();
                            model.setType(MoviePagerModel.TYPE_BOX_WATCHED);
                            model.setTitle(res.getString(R.string.film_watched));
                            SubtabInfo subtabInfo = new SubtabInfo();
                            subtabInfo.setType(MovieKind.TYPE_LOCAL);
                            subtabInfo.setCategoryId(MovieKind.CATEGORYID_GET_WATCHED);
                            subtabInfo.setCategoryName(model.getTitle());
                            model.setSubtabInfo(subtabInfo);
                            model.getList().addAll(watchedList);
                            mDataCinema.add(1, model);
                        }
                    } else {
                        watchedList = null;
                    }
                    if (mMoviePagerAdapter != null) mMoviePagerAdapter.updateData();
                    if (Utilities.isEmpty(mDataCinema)) {
                        if (loadingView != null) loadingView.showLoadedEmpty();
                    } else {
                        if (loadingView != null) loadingView.showLoadedSuccess();
                    }
                    if (object instanceof Movie) {
                        getMovieApi().deleteLogWatched(false, (Movie) object, null);
                    } else if (object instanceof MovieWatched) {
                        getMovieApi().deleteLogWatched(false, MovieWatched.convertToMovie((MovieWatched) object), null);
                    }
                    break;
            }
        }
    }

    private void scrollToTop() {
        if (layoutManager != null && recyclerView != null) {
            try {
                recyclerView.stopScroll();
                layoutManager.smoothScrollToPosition(recyclerView, null, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTabReselected(int currentPosition) {
        Log.i(TAG, "onTabReselected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position) {
            scrollToTop();
        }
    }

    @Override
    public void onTabSelected(int currentPosition) {
        Log.i(TAG, "onTabSelected currentPosition: " + currentPosition + ", position: " + position + ", isVisibleToUser: " + isVisibleToUser);
        if (currentPosition == position && isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onRefresh();
                }
            }, 600L);
        }
    }

    @Override
    public void onDisableLoading() {

    }

    private void getMoviesForYou(MoviePagerModel[] moviePagerModels) {
        getMovieApi().getFilmForUser(new ApiCallbackV2<ArrayList<HomeData>>() {
            @Override
            public void onSuccess(String msg, ArrayList<HomeData> result) throws JSONException {
                if (result != null && result.size() > 0) {
                    MoviePagerModel model = new MoviePagerModel();
                    model.setType(MoviePagerModel.TYPE_BOX_CONTENT);
                    model.setTitle(res.getString(R.string.cinema_movie_for_you));
                    SubtabInfo subtabInfo = new SubtabInfo();
                    subtabInfo.setType(result.get(0).getCategories().get(0).getType());
                    subtabInfo.setCategoryId(String.valueOf(result.get(0).getCategories().get(0).getId()));
                    subtabInfo.setCategoryName(result.get(0).getCategories().get(0).getCategoryName());
                    model.setSubtabInfo(subtabInfo);
                    MoviePagerModel gridItem = new MoviePagerModel();
                    gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                    gridItem.getList().addAll(result);
                    model.getList().add(gridItem);
                    moviePagerModels[3] = model;
                    if (!isLoadingHome) {
                        if (mDataCinema == null) {
                            mDataCinema = new ArrayList<>();
                        } else {
                            mDataCinema.clear();
                        }
                        for (MoviePagerModel moviePagerModel : moviePagerModels) {
                            mDataCinema.add(moviePagerModel);
                        }
                        mMoviePagerAdapter.updateData();
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private ArrayList<ListLogWatchResponse.Result> removeDuplicateFilm(ArrayList<ListLogWatchResponse.Result> data) {
        if (data == null) {
            return null;
        }
        ArrayList<ListLogWatchResponse.Result> results = new ArrayList<>();
        HashMap<String, ListLogWatchResponse.Result> hashMap = new HashMap<>();
        for (ListLogWatchResponse.Result item : data) {
            if (item.getIdGroup() == null || item.getIdGroup().toString().equals("0")) {
                results.add(item);
            } else {
                if (hashMap.get(item.getIdGroup().toString()) == null) {
                    results.add(item);
                    hashMap.put(item.getIdGroup().toString(), item);
                }
            }
        }
        return results;
    }

    /**
     * Get info movie: Continue Watching
     *
     * @param moviePagerModels list movie pager model
     */
    private void getContinueWatchingList(MoviePagerModel[] moviePagerModels) {
        Log.d(TAG, "Start getContinueWatchingList");
        loadingContinueWatching = true;
        MovieApi.getInstance().getListLogWatch(new ApiCallbackV2<ListLogWatchResponse>() {
            @Override
            public void onSuccess(String msg, ListLogWatchResponse result) throws JSONException {
                if (result != null) {
                    goneLoading();
                    if (result.getResult().size() > 0) {
                        MoviePagerModel model = new MoviePagerModel();
                        model.setType(MoviePagerModel.TYPE_BOX_WATCHED);
                        model.setTitle(res.getString(R.string.cinema_continue_watching));
                        MoviePagerModel gridItem = new MoviePagerModel();
                        gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                        gridItem.getList().addAll(removeDuplicateFilm(result.getResult()));
                        model.getList().add(gridItem);
                        moviePagerModels[2] = model;
                    }
                }
                onFinish(moviePagerModels);
            }

            @Override
            public void onError(String s) {
                onFinish(moviePagerModels);
            }

            @Override
            public void onComplete() {
                loadingContinueWatching = false;
                getContinueWatchingList = true;
            }
        });
    }

    public void getWatchedByFriend(MoviePagerModel[] moviePagerModels, String phoneNumber) {
        MovieApi.getInstance().getListLogWatchFriend(phoneNumber, new ApiCallbackV2<ListLogWatchResponse>() {
            @Override
            public void onSuccess(String msg, ListLogWatchResponse result) throws JSONException {
                if (result != null) {
                    goneLoading();
                    if (result.getResult().size() > 0) {
                        MoviePagerModel model = new MoviePagerModel();
                        model.setType(MoviePagerModel.TYPE_WATCHED_BY_FRIEND);
                        model.setTitle(res.getString(R.string.cinema_watched_by_friend));
                        model.getList().addAll(result.getResult());
                        moviePagerModels[7] = model;
                    }
                }
            }

            @Override
            public void onError(String s) {
            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void onFinish(MoviePagerModel[] moviePagerModels) {
        isLoadmore = false;
        getHomeMovieList(moviePagerModels, false);
    }

    /**
     * Get list movie:  + Recommend
     * + Trending Now
     * + Movies for you
     * + Recently Added
     * + Available Now
     * + Award-winning Movies
     * + Watched by Your Friends
     */
    private void getHomeMovieList(MoviePagerModel[] moviePagerModels, Boolean checkRefresh) {
        if (!isRefresh) {
            if (pbLoading != null && checkRefresh) {
                pbLoading.setVisibility(View.VISIBLE);
            } else {
                if (viewLoadingAnimation != null) {
                    viewLoadingAnimation.setVisibility(View.VISIBLE);
                }
            }
        }
        if (!isLoadmore) {
            getMovieApi().getCategoryByIds("868462", new ApiCallbackV2<List<Category>>() {
                @Override
                public void onSuccess(String msg, List<Category> result) throws JSONException {
                    if (result != null && result.size() > 0) {
                        mLabelAvailable = result.get(0).getCategoryName();
                    } else {
                        mLabelAvailable = "";
                    }
                }

                @Override
                public void onError(String s) {

                }

                @Override
                public void onComplete() {
                    checkGetListHome = true;
                    goneLoading();
                }
            });
        }
        if (isLoadmore) {
            mPage++;
        }
        isLoadingHome = true;
        getMovieApi().getHomeAppV2(Constants.LIMIT_LOAD_HOME,
                (mPage - 1) * Constants.LIMIT_LOAD_HOME, new ApiCallbackV2<HomeResult>() {
                    @Override
                    public void onSuccess(String msg, HomeResult result) throws JSONException {
                        if(activity != null){
                            if (pbLoading != null)
                            pbLoading.setVisibility(View.GONE);
                        if (viewLoadingAnimation != null) {
                            viewLoadingAnimation.setVisibility(View.GONE);
                        }
//                    shimmerFrameLayout.clearAnimation();
//                    shimmerFrameListLayout.clearAnimation();
                        if (recyclerView != null) {
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                        try {
                            if (recyclerView != null) recyclerView.stopScroll();
                            if (mDataCinema == null) mDataCinema = new ArrayList<>();
                            else if (!isLoadmore) mDataCinema.clear();
                            if (result != null) {
                                Field[] fields = HomeResult.class.getDeclaredFields();
                                for (Field item : fields) {
                                    if (item != null) {
                                        item.setAccessible(true);
                                        String name = item.getName();
                                        List<HomeData> homeDatas = (List<HomeData>) item.get(result);
                                        if (homeDatas != null) {
                                            int size = homeDatas.size();
                                            if (size > 0) {
                                                MoviePagerModel model = new MoviePagerModel();
                                                switch (name) {
                                                    //TODO Hien tai su dung recently add theo yeu cau KH
//                                                    case MovieKind.TYPE_RECOMMEND:
//                                                        model.setType(MoviePagerModel.TYPE_SLIDER_BANNER);
//                                                        model.setTitle(name);
//                                                        model.getList().addAll(homeDatas);
//                                                        moviePagerModels[0] = model;
//                                                        break;
//                                                    case MovieKind.TYPE_TRENDING:
//                                                        model.setType(MoviePagerModel.TYPE_BOX_CONTENT);
//                                                        model.setTitle(res.getString(R.string.cinema_trending));
//                                                        SubtabInfo subtabInfo = new SubtabInfo();
//                                                        if (!Utilities.isEmpty(homeDatas) && !Utilities.isEmpty(homeDatas.get(0).getCategories())) {
//                                                            subtabInfo.setType(homeDatas.get(0).getCategories().get(0).getType());
//                                                            subtabInfo.setCategoryId(String.valueOf(homeDatas.get(0).getCategories().get(0).getId()));
//                                                            subtabInfo.setCategoryName(homeDatas.get(0).getCategories().get(0).getCategoryName());
//                                                        }
//                                                        model.setSubtabInfo(subtabInfo);
//                                                        if (homeDatas.size() > 0) {
//                                                            MoviePagerModel gridItem = new MoviePagerModel();
//                                                            gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
//                                                            gridItem.getList().addAll(homeDatas);
//                                                            model.getList().add(gridItem);
//                                                        }
//                                                        if (homeDatas.size() % 10 != 0 && mAdapterNeedLoadmore != null) {
//                                                            ((BoxContentAdapter) mAdapterNeedLoadmore).setCanLoadmore(false);
//                                                        }
//                                                        moviePagerModels[1] = model;
//                                                        break;
                                                    case MovieKind.TYPE_RECENTLYADDED:
                                                        model.setType(MoviePagerModel.TYPE_BOX_CONTENT);
                                                        if(activity != null) {
                                                            model.setTitle(activity.getString(R.string.cinema_recently_added));
                                                            SubtabInfo subtabInfo = new SubtabInfo();
                                                            if (!Utilities.isEmpty(homeDatas) && !Utilities.isEmpty(homeDatas.get(0).getCategories())) {
                                                                subtabInfo.setType(homeDatas.get(0).getCategories().get(0).getType());
                                                                subtabInfo.setCategoryId(String.valueOf(homeDatas.get(0).getCategories().get(0).getId()));
                                                                subtabInfo.setCategoryName(homeDatas.get(0).getCategories().get(0).getCategoryName());
                                                            }
                                                            model.setSubtabInfo(subtabInfo);
                                                            if (homeDatas.size() > 0) {
                                                                MoviePagerModel gridItem = new MoviePagerModel();
                                                                gridItem.setType(MoviePagerModel.TYPE_BOX_GRID_ITEM);
                                                                gridItem.getList().addAll(homeDatas);
                                                                model.getList().add(gridItem);
                                                            }
                                                            if (homeDatas.size() % 10 != 0 && mAdapterNeedLoadmore != null) {
                                                                ((BoxContentAdapter) mAdapterNeedLoadmore).setCanLoadmore(false);
                                                            }
                                                            moviePagerModels[4] = model;
                                                            //TODO Hien tai su dung recently add theo yeu cau KH
                                                            MoviePagerModel bannerModel = new MoviePagerModel();
                                                            bannerModel.setType(MoviePagerModel.TYPE_SLIDER_BANNER);
                                                            bannerModel.setTitle(name);
                                                            bannerModel.getList().addAll(homeDatas);
                                                            moviePagerModels[0] = bannerModel;
                                                            listBanner.clear();
                                                            listBanner.addAll(homeDatas);
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_AVAILABLE:
                                                        List<HomeData> arrData = homeDatas;
                                                        for (HomeData homeData : arrData) {
                                                            if (StringUtils.isEmpty(homeData.getTrailerUrl())) {
                                                                homeDatas.remove(homeData);
                                                            }
                                                        }
                                                        if (homeDatas.size() > 0) {
                                                            model.setType(MoviePagerModel.TYPE_AVAILABLE_NOW);
                                                            model.setTitle(mLabelAvailable);
                                                            model.getList().add(homeDatas.get(0));
                                                            moviePagerModels[5] = model;
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_CUSTOM_TOPIC:
                                                        model.setType(MoviePagerModel.TYPE_CUSTOM_TOPIC);
                                                        if (homeDatas.size() > 0) {
                                                            MoviePagerModel gridItem = new MoviePagerModel();
                                                            gridItem.setType(MoviePagerModel.TYPE_CUSTOM_TOPIC);
                                                            gridItem.getList().addAll(homeDatas);
                                                            model.getList().add(gridItem);
                                                        }
                                                        moviePagerModels[6] = model;
                                                        break;
                                                }
                                            } else {
                                                switch (name) {
                                                    case MovieKind.TYPE_TRENDING:
                                                        if (moviePagerModels[1] != null) {
                                                            moviePagerModels[1].getList().clear();
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_RECENTLYADDED:
                                                        if (moviePagerModels[4] != null) {
                                                            moviePagerModels[4].getList().clear();
                                                        }
                                                        break;
                                                    case MovieKind.TYPE_CUSTOM_TOPIC:
                                                        if (moviePagerModels[6] != null) {
                                                            ((MoviePagerModel) moviePagerModels[6].getList().get(0)).getList().clear();
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (mDataCinema.size() == 0) {
                                for (MoviePagerModel moviePagerModel : moviePagerModels) {
                                    mDataCinema.add(moviePagerModel);
                                }
                            }


                            if (mMoviePagerAdapter != null && !isLoadmore) {
                                mMoviePagerAdapter.updateData();
                            } else if (mMoviePagerAdapter != null && isLoadmore) {
                                // Fix crash NullPointerException : when swipe list film trending now or recently added -> loadmore -> crash
                                if(moviePagerModels[1].getList().size() > 0 && moviePagerModels[4].getList().size() > 0 && moviePagerModels[6].getList().size() > 0){
                                HomeCinemaDataCache.getInstance().savePageMovie(moviePagerModels[1].getList(),
                                        moviePagerModels[4].getList(), ((MoviePagerModel) moviePagerModels[6].getList().get(0)).getList());
                                if (mAdapterNeedLoadmore instanceof BoxContentAdapter) {
                                    if (((BoxContentAdapter) mAdapterNeedLoadmore).getBlockName().equals(activity.getString(R.string.cinema_trending))) {
                                        ((MoviePagerModel) ((BoxContentAdapter) mAdapterNeedLoadmore).getItem(0)).getList().
                                                addAll(((MoviePagerModel) moviePagerModels[1].getList().get(0)).getList());
                                        ((BoxContentAdapter) mAdapterNeedLoadmore).increasePageLoaded();
                                    } else if (((BoxContentAdapter) mAdapterNeedLoadmore).getBlockName().equals(activity.getString(R.string.cinema_recently_added))) {
                                        ((MoviePagerModel) ((BoxContentAdapter) mAdapterNeedLoadmore).getItem(0)).getList().
                                                addAll(((MoviePagerModel) moviePagerModels[4].getList().get(0)).getList());
                                        ((BoxContentAdapter) mAdapterNeedLoadmore).increasePageLoaded();
                                    }
                                    mAdapterNeedLoadmore.notifyDataSetChanged();
                                } else if (mAdapterNeedLoadmore instanceof FilmTopicAdapter) {
                                    ArrayList<Object> customTopicList = ((MoviePagerModel) moviePagerModels[6].getList().get(0)).getList();
                                    for (Object object : customTopicList) {
                                        if (((FilmTopicAdapter) mAdapterNeedLoadmore).getBlockName().equals(((CustomTopic) object).getTopicName())) {
                                            ((FilmTopicAdapter) mAdapterNeedLoadmore).addListAndNotify(((CustomTopic) object).getListCustomFilm());
                                            ((FilmTopicAdapter) mAdapterNeedLoadmore).increasePageLoaded();
                                            break;
                                        }
                                    }
                                }
                            }

                            }
                            if (Utilities.isEmpty(mDataCinema)) {
                                prepareLoadingAnim(true);
                            } else {
                                if (loadingView != null) loadingView.showLoadedSuccess();
                            }
                            KhHomeClient.getInstance().getSliderFilm(callbackSlider);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                            prepareLoadingAnim(true);
                        }
                        }
                    }

                    @Override
                    public void onError(String s) {
                        if (pbLoading != null)
                            pbLoading.setVisibility(View.GONE);
                        if (viewLoadingAnimation != null) {
                            viewLoadingAnimation.setVisibility(View.GONE);
                        }
//                    shimmerFrameLayout.clearAnimation();
//                    shimmerFrameListLayout.clearAnimation();
                        if (mDataCinema == null) mDataCinema = new ArrayList<>();
                        if (mMoviePagerAdapter != null) mMoviePagerAdapter.updateData();
                        if (Utilities.isEmpty(mDataCinema)) {
                            prepareLoadingAnim(true);
                        } else {
                            if (loadingView != null) loadingView.showLoadedSuccess();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (pbLoading != null)
                            pbLoading.setVisibility(View.GONE);
                        if (viewLoadingAnimation != null) {
                            viewLoadingAnimation.setVisibility(View.GONE);
                        }
//                    shimmerFrameLayout.clearAnimation();
//                    shimmerFrameListLayout.clearAnimation();
                        if (recyclerView != null) {
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                        isLoading = false;
                        isDataInitiated = true;
                        isRefresh = false;
                        isLoadingHome = false;
                        getNotiList = true;
                        goneLoading();
                        hideRefresh();
                        HomeCinemaDataCache.setMoviePagerModels(mMoviePagerModels);
                    }
                });
    }

    public void goneLoading() {
        if (checkGetListHome && getContinueWatchingList && getNotiList) {
            if (viewLoadingAnimation != null) {
                viewLoadingAnimation.setVisibility(View.GONE);
            }
            HomeCinemaDataCache.setMoviePagerModels(mMoviePagerModels);
        }
    }

    /**
     * Get detail file base on film_id
     *
     * @param filmId
     */
    private void getMovieDetail(String filmId, boolean isPlaynow) {
        activity.showLoadingDialog("", "");
        new MovieApi().getMovieDetail(filmId, false, new ApiCallbackV2<Movie>() {
            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                if (result != null) {
                    if (isPlaynow) {
                        mCurrentMovie = result;
                        if (StringUtils.isEmpty(result.getIdGroup()) || result.getIdGroup().equals("0")) {
                            activity.hideLoadingDialog();
                            VideoPlayerActivity.playNow(getContext(), mCurrentMovie, mCurrentVideo, "");
                        } else {
                            getAllEpisode(result.getIdGroup());
                        }
                    } else {
                        activity.hideLoadingDialog();
                        activity.playMovies(result);
                    }
                } else {
                    activity.hideLoadingDialog();
                    ToastUtils.makeText(activity, R.string.video_message_cannot_play_video);
                }
            }

            @Override
            public void onError(String s) {
                activity.hideLoadingDialog();
                ToastUtils.makeText(activity, getString(R.string.e500_internal_server_error));
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventStopVideo(MessageStopVideo message) {
        stopVideo();
    }

    private void stopVideo() {
        if (mDataCinema != null) {
            CinemaPlayerHolder holder = getCinemaPlayerHolder();
            if (holder != null) {
                holder.stopVideo();
            }
        }
    }

    private CinemaPlayerHolder getCinemaPlayerHolder() {
        for (int index = 0; index < mDataCinema.size(); index++) {
            RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(index);
            if (viewHolder instanceof CinemaPlayerHolder) {
                return (CinemaPlayerHolder) viewHolder;
            }
        }
        return null;
    }

    private void handleScroll() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d(TAG, "onScrollStateChanged : " + newState);
                Rect rectPlayer = new Rect();
                int heightScreen = mScrHomeMovie.getMeasuredHeight();
                int heightPlayer = 0;
                if (newState == SCROLL_STATE_DRAGGING || newState == SCROLL_STATE_IDLE) {
                    int first = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                    int last = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                    CinemaPlayerHolder playerHolder = null;
                    for (int i = first; i <= last; i++) {
                        RecyclerView.ViewHolder holder = recyclerView.findViewHolderForAdapterPosition(i);
                        if (holder instanceof CinemaPlayerHolder) {
                            playerHolder = (CinemaPlayerHolder) holder;
                            rectPlayer = playerHolder.getRect();
                            heightPlayer = playerHolder.getHeight();
                            break;
                        }
                    }
                    if (playerHolder != null) {
                        if ((heightScreen - rectPlayer.top > (heightPlayer - 20)) && (rectPlayer.bottom - rectPlayer.top >= heightPlayer)) {
                            if (playerHolder.autoPlay) {
                                playerHolder.startVideo();
                                playerHolder.autoPlay = false;
                            }
                        } else {
                            playerHolder.stopVideo();
                            playerHolder.autoPlay = true;
                        }
                    }
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestPermissionBright(RequestPermissionBright requestPermissionBright) {
        if (dialogBright == null) {
            dialogBright = new DialogConfirm(activity, true);
            dialogBright.setLabel(getString(R.string.permission_allow_floating_view));
            dialogBright.setMessage(getString(R.string.permission_allow_change_brightness));
            dialogBright.setUseHtml(true);
            dialogBright.setCancelable(false);
            dialogBright.setNegativeLabel(getString(R.string.cancel));
            dialogBright.setPositiveLabel(getString(R.string.ok));
            dialogBright.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    if (activity != null && isAdded()) {
                        stopVideo();
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent);
                    }
                }
            });
            dialogBright.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    dialogBright.dismiss();
                }
            });
        }
        if (!dialogBright.isShowing()) {
            dialogBright.show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestPermissionVolume(RequestPermissionVolume requestPermissionVolume) {
        if (dialogVolume == null) {
            dialogVolume = new DialogConfirm(activity, true);
            dialogVolume.setLabel(getString(R.string.permission_allow_floating_view));
            dialogVolume.setMessage(getString(R.string.permission_allow_change_volume));
            dialogVolume.setUseHtml(true);
            dialogVolume.setCancelable(false);
            dialogVolume.setNegativeLabel(getString(R.string.cancel));
            dialogVolume.setPositiveLabel(getString(R.string.ok));
            dialogVolume.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    if (activity != null && isAdded()) {
                        stopVideo();
                        Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                        getContext().startActivity(intent);
                    }
                }
            });
            dialogVolume.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    dialogVolume.dismiss();
                }
            });
        }
        if (!dialogVolume.isShowing()) {
            dialogVolume.show();
        }
    }

    private void drawProfile(AccountRankDTO accountRankDTO, UserInfo userInfo) {
        tvName.setText(userInfo.getFull_name());
        KhUserRank myRank = KhUserRank.getById(accountRankDTO.rankId);
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            Glide.with(this)
                    .load(ImageUtils.convertBase64ToBitmap(userInfo.getAvatar()))
                    .centerCrop()
                    .placeholder(R.drawable.ic_avatar_default)
                    .error(R.drawable.ic_avatar_default)
                    .into(imgAvatar);
        } else {
            imgAvatar.setImageResource(myRank.resAvatar);
        }
        imgCrown.setImageDrawable(ResourceUtils.getDrawable(myRank.resRank));
        tvRankName.setText(getString(myRank.resRankName));
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void loadInfoUser(final LoadInfoUser event) {
        if (event.loadType == LoadInfoUser.LoadType.LOADING) {
            layoutUser.setVisibility(View.GONE);
            layoutGuest.setVisibility(View.GONE);
            llNoData.setVisibility(View.VISIBLE);
        } else if (event.loadType == LoadInfoUser.LoadType.NOT_LOGIN) {
            layoutUser.setVisibility(View.GONE);
            layoutGuest.setVisibility(View.VISIBLE);
            llNoData.setVisibility(View.GONE);
        } else {
            layoutUser.setVisibility(View.VISIBLE);
            layoutGuest.setVisibility(View.GONE);
            llNoData.setVisibility(View.GONE);
            drawProfile(event.rankDTO, event.userInfo);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void updateDataFromService(final UpdateDataFromService updateDataFromService) {
        Log.d("LoadCinemaData", "updateDataFromService");
        getDataFromCache();
        EventBus.getDefault().removeStickyEvent(updateDataFromService);
        LoadCinemaService.stop(getContext());
    }

    private void logActiveCinema() {
//        ((HomeActivity) activity).logActiveTab(FirebaseEventBusiness.TabLog.TAB_CINEMA);
    }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/10/3
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.keeng.utils.ImageBusiness;
import com.metfone.selfcare.module.myviettel.listener.OnMyViettelListener;
import com.metfone.selfcare.module.myviettel.model.DataPackageInfo;
import com.metfone.selfcare.ui.imageview.CircleImageView;

import butterknife.BindView;

public class PromotionDetailHolder extends BaseAdapter.ViewHolder {
    @BindView(R.id.layout_root)
    View viewRoot;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.button_submit)
    TextView btnSubmit;
    @BindView(R.id.iv_cover)
    CircleImageView ivCover;
    @BindView(R.id.tv_short_name)
    TextView tvShortName;

    private DataPackageInfo data;
    private Activity activity;

    public PromotionDetailHolder(Activity activity, View view, final OnMyViettelListener listener) {
        super(view);
        this.activity = activity;
        if (viewRoot != null) {
            viewRoot.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickPromotionInfo(data);
                }
            });
        }
        if (btnSubmit != null)
            btnSubmit.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View view) {
                    if (listener != null) listener.onClickRegisterPromotion(data);
                }
            });
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof DataPackageInfo) {
            data = (DataPackageInfo) item;
            if (tvTitle != null) {
                if (data.getPrice() > 0)
                    tvTitle.setText(Html.fromHtml(activity.getString(R.string.title_promotion_for_you, data.getPackageCode(), TextHelper.formatCurrencyVN(data.getPrice()))));
                else
                    tvTitle.setText(data.getPackageCode());
            }
            if (tvDesc != null) tvDesc.setText(Html.fromHtml(data.getDisplay()));
            if (btnSubmit != null) btnSubmit.setText(data.getLabelReg());
            ImageBusiness.setImagePromotionMyViettel(ivCover, tvShortName, data.getImage(), data.getPackageCode(), data.getId().hashCode());
        }
    }

}

package com.metfone.selfcare.module.selfcare.holder;

import android.content.Context;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.newdetails.view.BaseViewHolder;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCAccountDataInfo;

public class SCHomeControlHolder extends BaseViewHolder {
    View layout_my_account;
    View layout_topup;
    View layout_loyalty;
    View layout_usage_history;
    View layout_services;
    View layout_my_package;
    View layout_store;
    View layout_my_utilities;

    public SCHomeControlHolder(Context context, View view, final AbsInterface.OnSCHomeListener listener) {
        super(view);

        layout_my_account = view.findViewById(R.id.layout_my_account);
        layout_topup = view.findViewById(R.id.layout_topup);
        layout_loyalty = view.findViewById(R.id.layout_loyalty);
        layout_usage_history = view.findViewById(R.id.layout_usage_history);
        layout_services = view.findViewById(R.id.layout_services);
        layout_my_package = view.findViewById(R.id.layout_my_package);
        layout_store = view.findViewById(R.id.layout_store);
        layout_my_utilities = view.findViewById(R.id.layout_my_utilities);

        layout_my_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onAccountClick();
            }
        });
        layout_topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onTopupClick();
            }
        });
        layout_loyalty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onLoyaltyClick();
            }
        });
        layout_usage_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onHistoryClick();
            }
        });
        layout_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onServicesClick();
            }
        });
        layout_my_package.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onPackageListClick();
            }
        });
        layout_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onStoreClick();
            }
        });
        layout_my_utilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null)
                    listener.onUtilitiesClick();
            }
        });
    }

    public void setData(SCAccountDataInfo data) {

    }
}

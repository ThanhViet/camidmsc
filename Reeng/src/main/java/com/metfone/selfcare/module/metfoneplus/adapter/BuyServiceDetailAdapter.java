package com.metfone.selfcare.module.metfoneplus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.model.camid.ServiceGroup;
import com.metfone.selfcare.model.camid.ServicePackage;
import com.metfone.selfcare.module.metfoneplus.holder.BuyServiceDetailViewHolder;

import java.util.Collections;
import java.util.List;

public class BuyServiceDetailAdapter extends RecyclerView.Adapter<BuyServiceDetailViewHolder> {
    private Context mContext;
    private List<ServicePackage> mServicePackageList;
    private List<ServiceGroup> mServiceGroupList;
    private final View.OnClickListener mServiceDetailListener;

    public BuyServiceDetailAdapter(Context context,
                                   List<ServiceGroup> serviceGroupList, List<ServicePackage> servicePackageList,
                                   View.OnClickListener serviceDetailListener) {
        this.mContext = context;
        this.mServiceGroupList = serviceGroupList;
        this.mServicePackageList = servicePackageList;
        this.mServiceDetailListener = serviceDetailListener;
        swapServiceGroup();
    }

    public void replaceServicePackageData(List<ServicePackage> servicePackages) {
        this.mServicePackageList = servicePackages;
        this.mServiceGroupList = null;
        notifyDataSetChanged();
    }

    public void replaceServiceGroupData(List<ServiceGroup> serviceGroupList) {
        this.mServiceGroupList = serviceGroupList;
        this.mServicePackageList = null;
        swapServiceGroup();
        notifyDataSetChanged();
    }
    private void swapServiceGroup(){// Swap Non-auto renew before auto renew
        if (mServiceGroupList != null && mServiceGroupList.size() >= 2) {
            int indexNonAuto = -1, indexAuto = -1;
            for (int i = 0; i < mServiceGroupList.size(); i++) {
                if (mServiceGroupList.get(i).getName().contains("NON-AUTO")) {
                    indexNonAuto = i;
                } else if (mServiceGroupList.get(i).getName().contains("AUTO")) {
                    indexAuto = i;
                }
            }
            if (indexAuto != -1 && indexNonAuto != -1 && indexAuto < indexNonAuto) {
                Collections.swap(mServiceGroupList, indexAuto, indexNonAuto);
            }
        }
    }

    @NonNull
    @Override
    public BuyServiceDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mp_item_buy_services_detail, parent, false);
        return new BuyServiceDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyServiceDetailViewHolder holder, int position) {
        if (mServicePackageList != null) {
            ServicePackage item = mServicePackageList.get(position);
            holder.mDetailName.setText(item.getName());
            holder.mDetailDescription.setText(item.getShortDes());
            holder.mServicePackage = item;
            holder.isAutoRenew = false;

            holder.mLayoutDetail.setSelected(item.isSelected());
            holder.mDetailSelected.setSelected(item.isSelected());
        } else {
            ServiceGroup item = mServiceGroupList.get(position);
            String name = "";
            String description = "";
            if (item.getName().contains("NON-AUTO")) {
                name = mContext.getString(R.string.m_p_buy_service_non_auto_renew);
                description = mContext.getString(R.string.m_p_buy_service_detail_non_auto_renew);
                holder.isAutoRenew = false;
            } else {
                name = mContext.getString(R.string.m_p_buy_service_auto_renew);
                description = mContext.getString(R.string.m_p_buy_service_detail_auto_renew);
                holder.isAutoRenew = true;
            }
            holder.mDetailName.setText(name);
            holder.mDetailDescription.setText(description);

            holder.mServiceGroup = item;

            holder.mLayoutDetail.setSelected(item.isSelected());
            holder.mDetailSelected.setSelected(item.isSelected());
        }

        holder.position = position;
        holder.itemView.setTag(holder);
        holder.itemView.setOnClickListener(mServiceDetailListener);
    }

    @Override
    public int getItemCount() {
        if (mServicePackageList != null) {
            return mServicePackageList.size();
        } else if (mServiceGroupList != null) {
            return mServiceGroupList.size();
        }
        return 0;
    }
}
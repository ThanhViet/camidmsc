/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.movie.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.metfone.selfcare.BuildConfig;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.adapter.EpisodeAdapter;
import com.metfone.selfcare.adapter.QualityAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.ContentConfigBusiness;
import com.metfone.selfcare.business.FirebaseEventBusiness;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.common.FirebaseEventConstant;
import com.metfone.selfcare.common.MovieApi;
import com.metfone.selfcare.common.api.ApiCallbackV2;
import com.metfone.selfcare.common.api.ApiCallbackV3;
import com.metfone.selfcare.common.utils.DeviceUtils;
import com.metfone.selfcare.common.utils.ShareUtils;
import com.metfone.selfcare.common.utils.player.MochaPlayer;
import com.metfone.selfcare.common.utils.player.MochaPlayerUtil;
import com.metfone.selfcare.database.model.ItemContextMenu;
import com.metfone.selfcare.database.model.onmedia.FeedAction;
import com.metfone.selfcare.database.model.onmedia.FeedModelOnMedia;
import com.metfone.selfcare.helper.BackStackHelper;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.PopupHelper;
import com.metfone.selfcare.helper.ads.AdsManager;
import com.metfone.selfcare.helper.ads.AdsUtils;
import com.metfone.selfcare.helper.encrypt.XXTEACrypt;
import com.metfone.selfcare.helper.httprequest.ReportHelper;
import com.metfone.selfcare.listeners.ClickListener;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnClickMoreItemListener;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.ClickLikeMessage;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.model.tabMovie.Actor;
import com.metfone.selfcare.model.tabMovie.Category;
import com.metfone.selfcare.model.tabMovie.Director;
import com.metfone.selfcare.model.tabMovie.Movie;
import com.metfone.selfcare.model.tabMovie.MovieWatched;
import com.metfone.selfcare.model.tabMovie.TrackItem;
import com.metfone.selfcare.model.tab_video.Resolution;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.DateTimeUtils;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseAdapter;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.module.movie.adapter.InfoAdapter;
import com.metfone.selfcare.module.movie.adapter.RelateAdapter;
import com.metfone.selfcare.module.movie.event.RequestPermissionBright;
import com.metfone.selfcare.module.movie.event.RequestPermissionVolume;
import com.metfone.selfcare.module.movie.holder.DirectorActorViewHolder;
import com.metfone.selfcare.module.movie.holder.FilmDetailHolder;
import com.metfone.selfcare.module.movienew.dialog.TrailerFullScreenPlayerDialog;
import com.metfone.selfcare.module.movienew.fragment.MyListFragment;
import com.metfone.selfcare.module.movienew.listener.TrailerPlayerListener;
import com.metfone.selfcare.module.movienew.model.ListLogWatchResponse;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.search.adapter.ViewPagerAdapter;
import com.metfone.selfcare.module.selfcare.widget.EnhancedWrapContentViewPager;
import com.metfone.selfcare.module.share.ShareContentBusiness;
import com.metfone.selfcare.module.share.listener.ShareBusinessListener;
import com.metfone.selfcare.network.camid.response.BaseResponse;
import com.metfone.selfcare.restful.WSOnMedia;
import com.metfone.selfcare.ui.dialog.DialogConfirm;
import com.metfone.selfcare.ui.dialog.NegativeListener;
import com.metfone.selfcare.ui.dialog.PositiveListener;
import com.metfone.selfcare.ui.ratingbar.SimpleRatingBar;
import com.metfone.selfcare.ui.tabvideo.BaseFragment;
import com.metfone.selfcare.ui.tabvideo.listener.FullPlayerListener;
import com.metfone.selfcare.ui.tabvideo.listener.OnVideoChangedDataListener;
import com.metfone.selfcare.ui.tabvideo.playVideo.VideoPlayerActivity;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.QualityVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.ReportVideoDialog;
import com.metfone.selfcare.ui.tabvideo.playVideo.dialog.VideoFullScreenPlayerDialog;
import com.metfone.selfcare.ui.tabvideo.service.VideoService;
import com.metfone.selfcare.ui.view.tab_video.VideoPlaybackControlView;
import com.metfone.selfcare.ui.view.tab_video.VideoPlayerView;
import com.metfone.selfcare.util.DialogUtils;
import com.metfone.selfcare.util.EnumUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.RetrofitInstance;
import com.metfone.selfcare.util.Utilities;
import com.metfone.selfcare.v5.fragment.CommentMovieNewFragment;
import com.metfone.selfcare.v5.fragment.FilmEpisodesFragment;
import com.metfone.selfcare.v5.fragment.FilmRelatedFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.vtm.adslib.AdsCommon;
import com.vtm.adslib.AdsRewardedHelper;
import com.vtm.adslib.template.TemplateView;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.metfone.selfcare.business.UserInfoBusiness.isLogin;

public class FilmDetailFragment extends BaseFragment implements VideoPlaybackControlView.CallBackListener
        , FullPlayerListener.ProviderFullScreen, FullPlayerListener.OnActionFullScreenListener
        , FullPlayerListener.OnFullScreenListener, DialogInterface.OnDismissListener
        , EpisodeAdapter.OnItemEpisodeListener, OnVideoChangedDataListener
        , FilmDetailHolder.OnItemVideoClickedListener, OnClickContentMovie, OnClickMoreItemListener
        , ShareBusinessListener, View.OnClickListener, ItemViewClickListener {
    private static boolean mIsPlaynow;
    private final String TAG = "FilmDetailFragment";
    @BindView(R.id.layout_root)
    View rootView;
    @BindView(R.id.rec_film_info)
    RecyclerView recFilmInfo;
    @BindView(R.id.iv_close)
    AppCompatImageView ivClose;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.appBarLayout)
    LinearLayout appBarLayout;
    @BindView(R.id.viewPager)
    EnhancedWrapContentViewPager viewPager;
    @BindView(R.id.layout_one_tab)
    @Nullable
    View layoutOneTab;
    @BindView(R.id.line_below_tab)
    @Nullable
    View lineBelowTab;
    @BindView(R.id.layout_ads)
    @Nullable
    TemplateView layout_ads;
    //--- Detail New DUONGNK(17-9-2020)
    @BindView(R.id.tvCategory)
    AppCompatTextView tvCategory;
    @BindView(R.id.recyclervViewDirector)
    RecyclerView recyclervViewDirector;
    @BindView(R.id.recyclerViewCast)
    RecyclerView recyclerViewCast;
    @BindView(R.id.tvPublishNew)
    AppCompatTextView tvPublishNew;
    @BindView(R.id.btnPlay)
    LinearLayout btnPlay;
    @BindView(R.id.tvPlay)
    TextView tvPlay;
    @BindView(R.id.ratingBarMovie)
    SimpleRatingBar ratingBarMovie;
    @BindView(R.id.ratingBarMovieRate)
    SimpleRatingBar ratingBarMovieRate;
    BaseAdapter mDirectorAdapter;
    BaseAdapter mActorAdapter;
    @BindView(R.id.llControllerRate)
    LinearLayout llControllerRate;// rating icon
    @BindView(R.id.viewRate)
    FrameLayout viewRate;
    @BindView(R.id.timeTotal)
    AppCompatTextView timeTotal;
    @BindView(R.id.tvReview)
    AppCompatTextView tvReview;
    @BindView(R.id.contrainLayoutDescription)
    ConstraintLayout contrainLayoutDescription;
    @BindView(R.id.ivImage)
    AppCompatImageView ivImage;
    @BindView(R.id.nscrFilmDetail)
    NestedScrollView nscrFilmDetail;
    boolean isPause = false;
    // trailer film
    @BindView(R.id.layoutImageFilm)
    View layoutImageFilm;
    @BindView(R.id.layoutVideoTrailer)
    View layoutVideoTrailer;
    @BindView(R.id.controlViewTrailer)
    RelativeLayout controlViewTrailer;
    @BindView(R.id.layoutPlayerTrailer)
    FrameLayout layoutPlayerTrailer;
    @BindView(R.id.trailer_play)
    ImageView btnPlayTrailer;
    @BindView(R.id.trailer_mute)
    ImageView btnMuteTrailer;
    @BindView(R.id.trailer_full_screen)
    ImageView btnFullTrailer;
    @BindView(R.id.trailer_progress_bar)
    ProgressBar progress_bar;
    private String mLinkWap;// gửi sang fragment comment
    private UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getContext());
    private UserInfo userInfo = userInfoBusiness.getUser();
    private WSOnMedia rest;
    private boolean initLike;
    private DialogConfirm dialogConfirm = null;
    private DialogConfirm dialogVolume = null;
    //-------------------------------------------------
    private RetrofitInstance retrofitInstance = new RetrofitInstance();
    private Unbinder unbinder;
    private boolean isDestroy = false;
    private FilmDetailHolder infoHolder;
    private boolean popOut;
    private boolean isMini;
    private Video mCurrentVideo;
    private Movie currentMovie;
    //    private MovieWatched movieWatched;
    private String playerName;
    private EpisodeAdapter episodeAdapter;
    private RelateAdapter relatedAdapter;
    private InfoAdapter infoAdapter;
    private ArrayList<Object> episodes;
    private ArrayList<Movie> arrEpisodes; // gửi sang từ VideoPlayerActivity
    private ArrayList<Object> relates;
    private ArrayList<Object> infors;
    private boolean playWhenReady = false;
    private MochaPlayer mPlayer;
    private ViewPagerAdapter viewPagerAdapter;
    private boolean isCambodia = false;
    private boolean isShowingDialog = false;
    //    private boolean initLike;
    private Dialog optionsVideoDialog;
    private Dialog reportVideoDialog;
    private Dialog qualityVideoDialog;
    private Dialog speedVideoDialog;
    private boolean isFullScreen = false;
    private boolean playFromService = false;
    private VideoFullScreenPlayerDialog dialogFullScreenPlayer;
    private TrailerFullScreenPlayerDialog trailerFullScreenPlayer;
    private long mLastClickTime = 0;
    private OrientationEventListener orientationEventListener;
    private int lastOrientation;

    //log firebase event play
    boolean isNotLogPlay = true;
    int play_from_type;
    String nameCategory;
    String idCategory;

    private Runnable runnableEnableRotateSensor = () -> {
        if (orientationEventListener != null && orientationEventListener.canDetectOrientation() && (dialogFullScreenPlayer == null || !dialogFullScreenPlayer.isShowing())) {
            orientationEventListener.enable();
        }
    };
    private ShareContentBusiness shareBusiness;
    private Runnable runnableWhenLandscape = () -> {
        onFullScreen();
    };
    private MovieApi movieApi;
    private boolean isPlaying = false;
    private long currentTime = 0;
    private SimpleExoPlayer playerTrailer;
    private VideoPlayerView playerViewTrailer;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(btnPlayTrailer != null){
                btnPlayTrailer.setVisibility(View.GONE);
            }
        }
    };
    private Handler handler = new Handler();
    private Runnable runnableTime = new Runnable() {
        @Override
        public void run() {
            if (progress_bar != null) {
                progress_bar.setProgress((int) ((playerTrailer.getCurrentPosition() * 100) / playerTrailer.getDuration()));
                if(isPlaying){
                    currentTime = playerTrailer.getCurrentPosition();
                }
            }
            handler.postDelayed(this, 1000);
        }
    };

    private Handler handlerLogWatch = new Handler();
    private Runnable runnableLogWatch = new Runnable() {
        @Override
        public void run() {
            if (!mPlayer.getPlayWhenReady()) {
                return;
            }
            if (isNotLogPlay) {
                if (play_from_type == FirebaseEventBusiness.FROM_COUNTRY) {
                    ApplicationController.self().getFirebaseEventBusiness().logPlayFilmCountry(nameCategory);
                } else if (play_from_type == FirebaseEventBusiness.FROM_CATEGORY) {
                    ApplicationController.self().getFirebaseEventBusiness().logPlayFilmCategory(idCategory, nameCategory);
                } else if (play_from_type == FirebaseEventBusiness.FROM_TOPIC) {
                    ApplicationController.self().getFirebaseEventBusiness().logPlayFilmTopic(nameCategory);
                }
                isNotLogPlay = false;
            }
            ApplicationController.self().getFirebaseEventBusiness().logWatchFilm(currentMovie.getId(),currentMovie.getName());
        }
    };

    private Player.EventListener eventListener = new Player.DefaultEventListener() {

        private int currentState;

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            Log.i(TAG, "onPlayerStateChanged playWhenReady: " + playWhenReady + ", playbackState: " + playbackState);
            if (!isFullScreen && currentState != Player.STATE_ENDED && playbackState == Player.STATE_ENDED) {
                handlerEnd();
            }
            currentState = playbackState;
        }
    };

    public static FilmDetailFragment newInstance(Bundle bundle, boolean isPlaynow) {
        FilmDetailFragment fragment = new FilmDetailFragment();
        if (bundle != null) fragment.setArguments(bundle);
        mIsPlaynow = isPlaynow;
        return fragment;
    }

    private static boolean canMini() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(ApplicationController.self().getApplicationContext());
    }

    private void initOrientationListener() {
        if (activity != null) {
            lastOrientation = activity.getRequestedOrientation();
            orientationEventListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {
                int curOrientation;

                @Override
                public void onOrientationChanged(int orientation) {
                    if ((orientation >= 0 && orientation <= 45) || (orientation > 315 && orientation <= 360)) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    } else if (orientation > 45 && orientation <= 135) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    } else if (orientation > 135 && orientation <= 225) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    } else if (orientation > 225 && orientation <= 315) {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    } else {
                        curOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
                    }
                    if (curOrientation != lastOrientation) {
                        lastOrientation = curOrientation;
                        if (rootView != null) {
                            rootView.removeCallbacks(runnableWhenLandscape);
                            if ((curOrientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE || curOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) && mCurrentVideo != null && mCurrentVideo.isVideoLandscape()
                                    && !DeviceUtils.isDeviceLockRotate(activity) && !isShowingDialog
                                    && (mPlayer != null && !mPlayer.isAdDisplayed())) {
                                rootView.postDelayed(runnableWhenLandscape, 300);
                            }
                        }
                    }
                }
            };
            orientationEventListener.disable();
        }
        enableRotateSensor();
    }

    public void enableRotateSensor() {
        if (rootView != null) {
            rootView.removeCallbacks(runnableEnableRotateSensor);
            rootView.postDelayed(runnableEnableRotateSensor, 600);
        }
    }

    public void disableRotateSensor() {
        if (rootView != null)
            rootView.removeCallbacks(runnableEnableRotateSensor);
        if (orientationEventListener != null) orientationEventListener.disable();
    }

    private void playNow() {
        rootView.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (rootView == null
                        || mCurrentVideo == null
                        || Utilities.isEmpty(mCurrentVideo.getId())) return;
                playVideo(mCurrentVideo);
                initOrientationListener();
            }
        }, 1000);
        onFullScreen();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isDestroy = false;
        if (getArguments() != null) {
            try {
                Serializable serializable = getArguments().getSerializable(Constants.TabVideo.VIDEO);
                if (serializable instanceof Video) {
                    mCurrentVideo = (Video) serializable;
                }
                if (mCurrentVideo != null) {
                    currentMovie = Video.video2Movie(mCurrentVideo);
                } else {
                    serializable = getArguments().getSerializable(Constants.TabVideo.MOVIE);
                    if (serializable instanceof Movie)
                        currentMovie = (Movie) serializable;
                    serializable = getArguments().getSerializable(Constants.TabVideo.ARR_EPISODES_MOVIE);
                    if (serializable instanceof ArrayList)
                        arrEpisodes = (ArrayList<Movie>) serializable;
                    if (currentMovie != null) {
                        mCurrentVideo = Movie.movie2Video(currentMovie);
                    }
                }
                playerName = getArguments().getString(Constants.TabVideo.PLAYER_TAG, "");
                playFromService = getArguments().getBoolean(Constants.TabVideo.PLAY_FROM_SERVICE, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(playerName)) playerName = String.valueOf(System.nanoTime());
        mPlayer = MochaPlayerUtil.getPlayer(playerName);
        if (mCurrentVideo == null) mCurrentVideo = new Video();
        ApplicationController.self().getApplicationComponent().providerListenerUtils().removerListener(this);
        ApplicationController.self().getApplicationComponent().providerListenerUtils().addListener(this);

        boolean isAdsShow = FirebaseRemoteConfig.getInstance().getLong(AdsCommon.KEY_FIREBASE.AD_REWARD_SHOW) == 1;
        if (isAdsShow) {
            String adUnit = FirebaseRemoteConfig.getInstance().getString(AdsUtils.KEY_FIREBASE.AD_REWARD);
            AdsManager.getInstance().initAdsReward(adUnit);
        }

        // TODO: 15h50 - 21/09/2021 - ThanhHa - for playing loadMovieInfor5DMax in case you have not logged in
//        isCambodia = ApplicationController.self().getReengAccountBusiness().isCambodia();
        isCambodia = true;

        mLinkWap = currentMovie.getLinkWap();
        getInfoMovie();

        //check data for log firebase event play from country,category,topic
        if (FirebaseEventBusiness.play_from_type != 0) {
            play_from_type = FirebaseEventBusiness.play_from_type;
            nameCategory = FirebaseEventBusiness.nameCategory;
            idCategory = FirebaseEventBusiness.idCategory;
        }
        ApplicationController.self().getFirebaseEventBusiness().logOpenFilmDetail(currentMovie.getId(),currentMovie.getName());
        FirebaseEventBusiness.play_from_type = 0;
        FirebaseEventBusiness.nameCategory = "";
        FirebaseEventBusiness.idCategory = "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        infoHolder = new FilmDetailHolder(activity, view);
        infoHolder.setOnItemVideoClickedListener(this);
        infoHolder.hideSaveVideo();
        mPlayer.addListener(eventListener);
        mPlayer.setVisibleLayoutControl(false);
        Utilities.adaptViewForInsertBottom(rootView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utilities.setSizeFrameVideo(activity, infoHolder.getControllerFrame(), mCurrentVideo.getAspectRatio());
        ivClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                onMiniClicked();
            }
        });
        bindVideo();
        /**
         * url intro video empty sẽ hiển thị ảnh phim và ngược lại sẽ hiển thị play video
         */
        if (TextUtils.isEmpty(currentMovie.getTrailerUrl())) {
            Glide.with(ivImage).load(currentMovie.getImagePath()).into(ivImage);
            layoutVideoTrailer.setVisibility(View.GONE);
            layoutImageFilm.setVisibility(View.VISIBLE);
        } else {
            initTrailer(currentMovie.getTrailerUrl());
            layoutVideoTrailer.setVisibility(View.VISIBLE);
            layoutImageFilm.setVisibility(View.GONE);
        }
        enableController();
        enableListenerPlayer();
        initView();
        listener();
        initViewPagePageBottom();
        if (playFromService) {
            playVideo(mCurrentVideo);
            rootView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (rootView == null
                            || mCurrentVideo == null
                            || Utilities.isEmpty(mCurrentVideo.getId())) return;
                    initOrientationListener();
                }
            }, 1000);
        }
        checkResumeWhenOpenDetail();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPlayer != null) {
            mPlayer.setPaused(false);
            mPlayer.setMini(false);
            mPlayer.setVisibleLayoutControl(false);
            if (mPlayer.isAdDisplayed()) {
                if (mPlayer.getAdPlayer() != null) mPlayer.getAdPlayer().resume();
            } else {
                if (playWhenReady) {
                    mPlayer.setPlayWhenReady(true);
                } else {
                    if (!mPlayer.getPlayWhenReady() && mPlayer.getPlayerView() != null
                            && mPlayer.getPlaybackState() != Player.STATE_IDLE
                            && mPlayer.getPlaybackState() != Player.STATE_BUFFERING)
                        mPlayer.getPlayerView().showController();
                }
            }
        }
        isPause = false;
        enableRotateSensor();
        playWhenReady = false;
        setupUI(rootView);
        setupViewRate(rootView);
        UIUtil.hideKeyboard(getActivity());
    }

    @Override
    public void onPause() {
        disableRotateSensor();
        if (mPlayer != null) {
            mPlayer.setPaused(true);
            mPlayer.setMini(isMini);
            if (!isMini) {
                if (mPlayer.isAdDisplayed()) {
                    if (mPlayer.getAdPlayer() != null) mPlayer.getAdPlayer().pause();
                }
                if (mPlayer.getPlayWhenReady()) playWhenReady = true;
                mPlayer.setPlayWhenReady(false);
            }
        }
        stopTrailer();
        isPause = true;
        if (trailerFullScreenPlayer != null) {
            trailerFullScreenPlayer.stopTrailer();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        isDestroy = true;
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if (initLike != currentMovie.isLike()) {
            ClickLikeMessage clickLikeMessage = new ClickLikeMessage(mCurrentVideo.getId());
            clickLikeMessage.setLike(currentMovie.isLike());
            EventBus.getDefault().postSticky(clickLikeMessage);
        }
        isDestroy = true;
        logWatched();
        ApplicationController.self().getApplicationComponent().providerListenerUtils().removerListener(this);
        infoHolder.setOnItemVideoClickedListener(null);
        disableListenerPlayer();
        if (isMini) {
        } else {
            MochaPlayerUtil.removePlayer(playerName);
        }
        unbinder.unbind();
        dismissAllDialogs();
        super.onDestroyView();
    }

    @Override
    public void handlerLikeVideo(Video currentVideo) {
        infoHolder.bindVideoAction(currentVideo);
        if (currentMovie != null) {
            currentMovie.setLike(currentVideo.isLike());
            currentMovie.setTotalLike((int) currentVideo.getTotalLike());
            application.getApplicationComponent().providerVideoApi().likeOrUnlikeMovie(currentMovie);
        }
    }

    @Override
    public void handlerShareVideo(Video currentVideo) {
        if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            activity.showDialogLogin();
        } else {
            if (shareBusiness != null) {
                shareBusiness.dismissAll();
            }
            shareBusiness = new ShareContentBusiness(activity, currentVideo);
            shareBusiness.setPlayingState(mPlayer != null && mPlayer.getPlayWhenReady());
            shareBusiness.setShareBusinessListener(this);
            shareBusiness.showPopupShareContent();
        }
    }

    @Override
    public void onDismiss() {
        isFullScreen = false;
        onDismiss(null);
        if (mPlayer != null) {
            mPlayer.pause();
            mPlayer.setVisibleLayoutControl(false);
            if(mPlayer.getPlayWhenReady()){
                mPlayer.setPlayWhenReady(false);
            }
        }
        // update progress for list episodes
        for (int i = 0; i < episodes.size(); i++) {
            Video video = (Video) episodes.get(i);
            if (video.getId() == mCurrentVideo.getId()) {
                video.setTimeWatched(String.valueOf(mPlayer.getCurrentPosition()));
                episodes.set(i, video);
                break;
            }
        }
        episodeAdapter.bindData(episodes);
        episodeAdapter.notifyDataSetChanged();
    }

    public void handlerEnd() {
        if (isDestroy) return;
        if (!isPause && Utilities.notEmpty(episodes) && episodes.contains(mCurrentVideo) && !isCambodia) {
            int position = episodes.indexOf(mCurrentVideo);
            if (position + 1 < episodes.size()) {
                Video newVideo = (Video) episodes.get(position + 1);
                switchVideo(newVideo);
                playVideo(mCurrentVideo);
                return;
            }
        }
        if (mPlayer != null) {
            mPlayer.showReplay(false);
            mPlayer.logEnd();
        }
        if (mCurrentVideo != null) mCurrentVideo.setPause(true);
    }

    @Override
    public void onPlayNextVideoForward(Video nextVideo) {
        switchVideo(nextVideo);
    }

    @Override
    public void onPlayEpisode(Video video, int position) {
        // TODO: 4/24/2020 Check Cam
        if (isCambodia) {
            loadMovieInfor5DMax(video);
            return;
        }
        video.setTimeWatched("0"); // đồng bộ xem từ đầu như IOS, đặt text 0, ko phải rỗng
        switchVideo(video);
        playVideo(mCurrentVideo);
    }

    @Override
    public void onChangeSubtitleAudio(Video video) {
        playWhenChangeSubtitleAudio(video);
    }

    @Override
    public Video provideVideoForward() {
        if (mCurrentVideo == null || Utilities.isEmpty(episodes)) return null;
        int position = episodes.indexOf(mCurrentVideo);
        if (position < 0 || position > episodes.size() - 1) return null;
        int positionNext = position + 1;
        Object object = episodes.get(positionNext);
        if (object instanceof Video && !Utilities.equals(object, mCurrentVideo)) {
            return ((Video) object);
        }
        return null;
    }

    @Override
    public Video provideCurrentVideo() {
        return mCurrentVideo;
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDismiss(DialogInterface dialog) {
        if (rootView != null) rootView.setVisibility(View.VISIBLE);
        dismissAllDialogs();
        isFullScreen = false;
        if (activity != null) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            lastOrientation = activity.getRequestedOrientation();
        }
        if (mPlayer != null && infoHolder != null && mCurrentVideo != null) {
            mPlayer.getPlayerView().setRefreshMode((float) mCurrentVideo.getAspectRatio());
            Utilities.setSizeFrameVideo(activity, infoHolder.getControllerFrame(), mCurrentVideo.getAspectRatio());
            mPlayer.addPlayerViewTo(infoHolder.getFrameVideo());
        }
        enableRotateSensor();
    }

    @Override
    public void onPlayerStateChanged(int stage) {
    }

    @Override
    public void onPlayerError(String error) {
    }

    @Override
    public void onVisibilityChange(int visibility) {
    }

    @Override
    public void onFullScreen() {
        stopTrailer();
        dismissAllDialogs();
        disableRotateSensor();
        if (dialogFullScreenPlayer != null && dialogFullScreenPlayer.isShowing())
            dialogFullScreenPlayer.dismiss();
        if (activity == null || activity.isFinishing()) return;
        boolean isLandscape = mCurrentVideo != null && mCurrentVideo.isVideoLandscape();
        //activity.setRequestedOrientation(isLandscape ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        isFullScreen = true;
        dialogFullScreenPlayer = new VideoFullScreenPlayerDialog(activity, currentMovie);
        dialogFullScreenPlayer.setOnActionFullScreenListener(this);
        dialogFullScreenPlayer.setOnFullScreenListener(this);
        dialogFullScreenPlayer.setOnShowListener(dialog -> {
            if (rootView != null) rootView.setVisibility(View.INVISIBLE);
        });
        dialogFullScreenPlayer.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mIsPlaynow) {
                    getActivity().finish();
                }
            }
        });
        dialogFullScreenPlayer.setProviderFullScreen(this);
        dialogFullScreenPlayer.setCurrentVideo(mCurrentVideo);
        dialogFullScreenPlayer.setPlayerName(playerName);
        dialogFullScreenPlayer.setMovies(true);
        dialogFullScreenPlayer.setLandscape(isLandscape);
        if (Utilities.notEmpty(episodes) && episodes.contains(mCurrentVideo)) {
            int currentEpisode = episodes.indexOf(mCurrentVideo);
            dialogFullScreenPlayer.setEpisodes(episodes, true);
            dialogFullScreenPlayer.setCurrentEpisode(currentEpisode);
        }
        dialogFullScreenPlayer.show();
    }

    @Override
    public void onMute(boolean flag) {
    }

    @Override
    public void onTimeChange(long time) {
    }

    @Override
    public void onHaveSeek(boolean flag) {
        if (mPlayer != null) {
            mPlayer.onHaveSeek(flag);
        }
    }

    @Override
    public void onPlayNextVideo() {
        if (isDestroy || isPause) return;
        if (Utilities.notEmpty(episodes) && episodes.contains(mCurrentVideo)) {
            int position = episodes.indexOf(mCurrentVideo);
            int newPosition = position + 1;
            if (newPosition < episodes.size()) {
                Video newVideo = (Video) episodes.get(newPosition);
                // TODO: 4/24/2020 Check Cam
                if (isCambodia) {
                    loadMovieInfor5DMax(newVideo);
                    return;
                }
                switchVideo(newVideo);
                playVideo(mCurrentVideo);
            }
        }
    }

    @Override
    public void onPlayPreviousVideo() {
        if (isDestroy || isPause) return;
        if (Utilities.notEmpty(episodes) && episodes.contains(mCurrentVideo)) {
            int position = episodes.indexOf(mCurrentVideo);
            int newPosition = position - 1;
            if (newPosition < episodes.size()) {
                Video newVideo = (Video) episodes.get(newPosition);
                // TODO: 4/24/2020 Check Cam
                if (isCambodia) {
                    loadMovieInfor5DMax(newVideo);
                    return;
                }
                switchVideo(newVideo);
                playVideo(mCurrentVideo);
            }
        }
    }

    @Override
    public void onPlayPause(boolean state) {
        changeStatusPlayer(state);
        if (!state && mPlayer.getCurrentPosition() > 0) {
            getMovieApi().insertLogWatch(mCurrentVideo.getId(), mPlayer.getCurrentPosition(), new ApiCallbackV2<BaseResponse>() {
                @Override
                public void onSuccess(String msg, BaseResponse result) throws JSONException {
                    if (result != null) {
                        Log.d(TAG, "Insert log watch " + mCurrentVideo.getId() + " : " + result.getMessage());
                    }
                }

                @Override
                public void onError(String s) {
                    Log.d(TAG, "Insert log watch error : " + s);
                }

                @Override
                public void onComplete() {

                }
            });
        }
        if(!state){
            ApplicationController.self().getFirebaseEventBusiness().logStopFilm(mCurrentVideo.getId(),mCurrentVideo.getTitle(), String.valueOf(mPlayer.getCurrentPosition()));
        }
    }

    @Override
    public void onSmallScreen() {
    }

    @Override
    public void onMoreClick() {
        if (mPlayer != null) {
            showSubtitleAudio();
        }
    }

    @Override
    public void onReplay() {
        if (!isFullScreen && mPlayer != null) {
            mPlayer.logStart(mCurrentVideo);
        }
    }

    @Override
    public void onQuality() {
        if (mPlayer != null) {
            handlerQuality(mPlayer.getVideo(), activity);
        }
    }

    @Override
    public void onRequestAds(String position) {
    }

    @Override
    public void onShowAd() {
    }

    @Override
    public void onHideAd() {
    }

    @Override
    public void onHideController() {
    }

    @Override
    public void onShowController() {
    }

    @Override
    public void autoSwitchPlayer() {
    }

    @Override
    public void onClickViewFrame() {
    }

    @Override
    public void onVideoLikeChanged(Video video) {
        if (infoHolder == null) return;
//        if (Utilities.equals(mCurrentVideo, video)) {
//            infoHolder.setLike(mCurrentVideo.isLike());
//            if(!mCurrentVideo.isLike()){
//                unlikeVideo(currentMovie.getId());
//            }else{
//                likeVideo(currentMovie.getId());
//            }
//            mCurrentVideo.setLike(video.isLike());
//            mCurrentVideo.setTotalLike(video.getTotalLike());
//            infoHolder.bindVideoAction(mCurrentVideo);
//            if (currentMovie != null) {
//                currentMovie.setLike(video.isLike());
//                currentMovie.setTotalLike((int) video.getTotalLike());
//                application.getApplicationComponent().providerVideoApi().likeOrUnlikeMovie(currentMovie);
//            }
//        }
        if (currentMovie.isLike()) {
            currentMovie.setLike(false);
            unlikeVideo(currentMovie.getId());
            infoHolder.setLike(false);
        } else {
            currentMovie.setLike(true);
            likeVideo(currentMovie.getId());
            infoHolder.setLike(true);
        }
        int position = episodes.indexOf(video);
        if (position < 0 || position > episodes.size() - 1) return;
        Object object = episodes.get(position);
        if (object instanceof Video) {
            Video mVideo = (Video) object;
            mVideo.setLike(video.isLike());
            mVideo.setTotalLike(video.getTotalLike());
        }
    }

    @Override
    public void onVideoShareChanged(Video video) {
        if (infoHolder == null) return;
        if (Utilities.equals(mCurrentVideo, video)) {
            mCurrentVideo.setTotalShare(video.getTotalShare());
            infoHolder.bindVideoAction(mCurrentVideo);
        }
        int position = episodes.indexOf(video);
        if (position < 0 || position > episodes.size() - 1) return;
        Object object = episodes.get(position);
        if (object instanceof Video) {
            Video mVideo = (Video) object;
            mVideo.setTotalShare(video.getTotalShare());
        }
    }

    @Override
    public void onVideoCommentChanged(Video video) {
        if (infoHolder == null) return;
        if (Utilities.equals(mCurrentVideo, video)) {
            mCurrentVideo.setTotalComment(video.getTotalComment());
            infoHolder.bindVideoAction(mCurrentVideo);
        }
        int position = episodes.indexOf(video);
        if (position < 0 || position > episodes.size() - 1) return;
        Object object = episodes.get(position);
        if (object instanceof Video) {
            Video mVideo = (Video) object;
            mVideo.setTotalComment(video.getTotalComment());
        }
    }

    @Override
    public void onVideoSaveChanged(Video video) {
        if (infoHolder == null) return;
        if (Utilities.equals(mCurrentVideo, video)) {
            mCurrentVideo.setSave(video.isSave());
            infoHolder.bindVideoAction(mCurrentVideo);
        }
        int position = episodes.indexOf(video);
        if (position < 0 || position > episodes.size() - 1) return;
        Object object = episodes.get(position);
        if (object instanceof Video) {
            Video mVideo = (Video) object;
            mVideo.setSave(video.isSave());
        }
    }

    @Override
    public void onVideoWatchLaterChanged(Video video) {
    }

    @Override
    public void onBtnCommentClicked(@NonNull Video video) {
        if (canMini()) {
            openMini(Constants.TabVideo.COMMENT);
        } else {
            autoPauseVideo();
        }
    }

    @Override
    public void onBtnShareClicked(@NonNull Video video) {
        handlerShareVideo(video);
    }

    @Override
    public void onItemEpisodeClicked(@NonNull Video video) {
        if (AdsRewardedHelper.getInstance().canShowAd()) {
            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setLabel(getString(R.string.app_name));
            dialogConfirm.setMessage("Hãy thư giãn một đoạn quảng cáo để tiếp tục xem phim miễn phí bạn nhé!");
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
            dialogConfirm.setPositiveLabel("Xem quảng cáo");
            dialogConfirm.setPositiveListener(result -> {
                AdsManager.getInstance().showAdsReward(new AdsRewardedHelper.AdsRewardListener() {
                    @Override
                    public void onAdClosed() {

                    }

                    @Override
                    public void onAdShow() {

                    }

                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem reward) {
                        playEpisode(video);
                    }
                });
            });
            dialogConfirm.setNegativeListener(result -> {
                if (dialogConfirm != null)
                    dialogConfirm.dismiss();
            });
            dialogConfirm.show();
        } else {
            mCurrentVideo = video;
            playEpisode(video);
            onFullScreen();
        }
    }

    private void playEpisode(Video video) {
//        if (currentVideo != null && Utilities.equals(currentVideo, video)) return;
        // TODO: 4/24/2020 Check Cam
        if (isCambodia) {
            loadMovieInfor5DMax(video);
            return;
        }
        video.setTimeWatched("0"); // đồng bộ xem từ đầu như IOS, đặt text 0, ko phải rỗng
        switchVideo(video);
        playVideo(mCurrentVideo);
    }

    public void onMiniClicked() {
//        if (canMini())
//            openMini("");
//        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            //Hien popup xin quyen
//            DialogConfirm dialogConfirm = new DialogConfirm(activity, true);
//            dialogConfirm.setLabel(getString(R.string.permission_allow_floating_view));
//            dialogConfirm.setMessage(getString(R.string.permission_allow_floating_view_content));
//            dialogConfirm.setUseHtml(true);
//            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
//            dialogConfirm.setPositiveLabel(getString(R.string.ok));
//            dialogConfirm.setPositiveListener(result -> {
//                if (activity != null && isAdded()) {
//                    autoPauseVideo();
//                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
//                    startActivityForResult(intent, 110);
//                }
//            });
//            dialogConfirm.setNegativeListener(result -> {
//                if (activity != null) activity.finish();
//            });
//            dialogConfirm.show();
//        } else
        activity.finish();
    }

    private void initView() {
        episodes = new ArrayList<>();
        episodeAdapter = new EpisodeAdapter(activity);
        episodeAdapter.setOnItemEpisodeListener(this);
        relates = new ArrayList<>();
        relatedAdapter = new RelateAdapter(activity);
        relatedAdapter.setListener(this);
        relatedAdapter.setItems(relates);
        infors = new ArrayList<>();
        infoAdapter = new InfoAdapter(activity);
        recFilmInfo.setLayoutManager(new LinearLayoutManager(activity));
        recFilmInfo.setNestedScrollingEnabled(false);
        recFilmInfo.setAdapter(infoAdapter);
        getAllInfo();
        if (arrEpisodes == null) arrEpisodes = new ArrayList<>();
        if ((StringUtils.isEmpty(currentMovie.getIdGroup()) || currentMovie.getIdGroup().equals("0")) && mIsPlaynow) {
            playNow();
        } else if (arrEpisodes.isEmpty()) {
            getAllEpisode();
        } else {
            updateEpisodesMovie(arrEpisodes);
            playNow();
        }
        getAllRelated();
//        initViewPagePageBottom();

        //------------------------ Detail NEW (DUONGNK - 17/9/2020)

        tvCategory.setText(getCategories());
        tvCategory.setVisibility(getCategories().isEmpty() ? View.GONE : View.VISIBLE);
        String year = mCurrentVideo.getInfo().get("year");
        tvPublishNew.setText(year != null ? year : "2020");
        btnPlay.setOnClickListener(this);
        llControllerRate.setOnClickListener(this::onClick);
        // duongnk set rate phim
        if (!StringUtils.isEmpty(currentMovie.getRate())) {
            ratingBarMovie.setRating(Float.parseFloat(currentMovie.getRate()) == (int) Float.parseFloat(currentMovie.getRate()) ? Float.parseFloat(currentMovie.getRate()) : ((int) Float.parseFloat(currentMovie.getRate()) + 0.5f));
        }
        if (currentMovie.getTotalRated() == 0 || currentMovie.getTotalRated() == 1) {
            tvReview.setText("(" + currentMovie.getTotalRated() + " " + getString(R.string.review) + ")");
        } else {
            tvReview.setText("(" + currentMovie.getTotalRated() + " " + getString(R.string.reviews) + ")");
        }

        ratingBarMovieRate.setRating(0);

        if (!currentMovie.getIdGroup().equals("0")) {
            timeTotal.setText(currentMovie.getTotalEpisodes() + " " + getString(R.string.episodes));
        } else {
            timeTotal.setText(DateTimeUtils.displayFilmTime(String.valueOf(currentMovie.getDurationMinutes())));
        }

        DirectorActorViewHolder holderActor = new DirectorActorViewHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        DirectorActorViewHolder holderDirector = new DirectorActorViewHolder(getActivity().getWindow().getDecorView().getRootView(), this);
        FlexboxLayoutManager layoutManagerCast = new FlexboxLayoutManager(getContext());
        layoutManagerCast.setFlexDirection(FlexDirection.ROW);
        layoutManagerCast.setJustifyContent(JustifyContent.FLEX_START);
        mActorAdapter = new BaseAdapter(getListActor(), getContext(), R.layout.item_director, holderActor);
//        recyclerViewCast.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        recyclerViewCast.setLayoutManager(layoutManagerCast);
        recyclerViewCast.setAdapter(mActorAdapter);

        FlexboxLayoutManager layoutManagerDirector = new FlexboxLayoutManager(getContext());
        layoutManagerDirector.setFlexDirection(FlexDirection.ROW);
        layoutManagerDirector.setJustifyContent(JustifyContent.FLEX_START);
        mDirectorAdapter = new BaseAdapter(getListDirector(), getContext(), R.layout.item_director, holderDirector);
//        recyclervViewDirector.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        recyclervViewDirector.setLayoutManager(layoutManagerDirector);
        recyclervViewDirector.setAdapter(mDirectorAdapter);

        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) mTabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 35, 0);
            tab.requestLayout();
        }


        //--------------------------------------------------------
    }

    private void initViewPagePageBottom() {
        //Handle show two tab (episodes/other videos)
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        lineBelowTab.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.VISIBLE);
        boolean hasPadding;
//        if (currentVideo != null && currentVideo.getFilmGroupIdInt() > 0 && episodes.size() > 1) {
//        if (currentMovie != null && currentMovie.getTotalEpisodes() > 1) {
        if (currentMovie != null && !currentMovie.getIdGroup().equals("0")) {
            FilmEpisodesFragment filmEpisodesFragment = FilmEpisodesFragment.newInstance(mCurrentVideo);
            filmEpisodesFragment.setEpisodeAdapter(episodeAdapter);
            viewPagerAdapter.addFragment(filmEpisodesFragment, getString(R.string.episodes));

//            if (layoutOneTab != null) layoutOneTab.setVisibility(View.GONE);
            hasPadding = true;
        } else {
//            lineBelowTab.setVisibility(View.GONE);
//            mTabLayout.setVisibility(View.GONE);
//            if (layoutOneTab != null) layoutOneTab.setVisibility(View.VISIBLE);
            hasPadding = false;
        }
        initFragmentComment();
//        appBarLayout.setExpanded(true);
        FilmRelatedFragment filmRelatedFragment = FilmRelatedFragment.newInstance(true);
        filmRelatedFragment.setAdapter(relatedAdapter);
        viewPagerAdapter.addFragment(filmRelatedFragment, getString(R.string.more_like));
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(viewPagerAdapter);
//        viewPager.initPageChangeListener();
        mTabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_EPISODES);
                }else if(position == 1){
                    ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_REVIEW);
                }else if(position == 3){
                    ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_MORELIKETHIS);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void listener() {

        // thay đổi viewpager ẩn hiện view commen
        contrainLayoutDescription.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                viewRate.setVisibility(View.GONE);
            }
            return true;
        });
        // rate phim
        ratingBarMovieRate.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                int rate = (int) rating;
                if (infoHolder != null && rate != 0) {
                    infoHolder.rate(true);
                }
                getMovieApi().rateFilm(currentMovie.getId(), rate, new ApiCallbackV2<BaseResponse>() {
                    @Override
                    public void onSuccess(String msg, BaseResponse result) throws JSONException {
                        if (result != null && result.getCode().equals("200")) {
                            Log.d(TAG, "Rate success");
                        } else {
                            Log.d(TAG, "Rate not success");
                        }
                    }

                    @Override
                    public void onError(String s) {
                        Log.d(TAG, "onError : " + s);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
                ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_RATE);
            }
        });
    }

    public void setupUI(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UserInfoBusiness.hideKeyboard(getActivity());
//                    viewRate.setVisibility(View.GONE);
//                    isCheckShowRate = false;
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof EditText) {
//                    viewRate.setVisibility(View.GONE);
//                    isCheckShowRate = false;
                    continue;
                }

//                if (innerView instanceof SimpleRatingBar || innerView instanceof FrameLayout) {
//                    UserInfoBusiness.hideKeyboard(getActivity());
//                    continue;
//                }

                setupUI(innerView);
            }
        }
    }

    public void setupViewRate(View view) {

        if (!(view instanceof RelativeLayout)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    viewRate.setVisibility(View.GONE);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                if (innerView instanceof SimpleRatingBar || innerView instanceof FrameLayout) {
                    continue;
                }

                setupUI(innerView);
            }
        }
    }


    private void checkResumeVideo() {
        MovieApi.getInstance().getListLogWatch(new ApiCallbackV2<ListLogWatchResponse>() {
            @Override
            public void onSuccess(String msg, ListLogWatchResponse result) throws JSONException {
                int length = result.getResult().size();
                for (int i = 0; i < length; i++) {
                    if (mCurrentVideo.getId().equals(result.getResult().get(i).getIdPhim())) {
                        mPlayer.seekTo(Long.parseLong(result.getResult().get(i).getTimeSeek()));
                        break;
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void initFragmentComment() {
        CommentMovieNewFragment commentMovieNewFragment = new CommentMovieNewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("linkWap", mLinkWap);
        bundle.putSerializable("movie", currentMovie);
        commentMovieNewFragment.setArguments(bundle);
        viewPagerAdapter.addFragment(commentMovieNewFragment, getString(R.string.review));
    }

    private void getAllInfo() {
        ArrayList<Map<String, String>> maps = new ArrayList<>();
        Resources resources = getResources();
        String infoStr;
        infoStr = mCurrentVideo.getInfo().get("country");
        if (Utilities.notEmpty(infoStr)) {
            maps.add(getInfo(resources.getString(R.string.movie_info_country), infoStr));
        }
        infoStr = mCurrentVideo.getInfo().get("category");
        if (Utilities.notEmpty(infoStr)) {
            maps.add(getInfo(resources.getString(R.string.movie_info_category), infoStr));
        }
        infoStr = mCurrentVideo.getInfo().get("director");
        if (Utilities.notEmpty(infoStr)) {
            maps.add(getInfo(resources.getString(R.string.movie_info_directors), infoStr));
        }
        infoStr = mCurrentVideo.getInfo().get("year");
        if (Utilities.notEmpty(infoStr)) {
            maps.add(getInfo(resources.getString(R.string.movie_info_year), infoStr));
        }
        infoStr = mCurrentVideo.getInfo().get("publisher");
        if (Utilities.notEmpty(infoStr)) {
            maps.add(getInfo(resources.getString(R.string.movie_info_provided_by), infoStr));
        }
        infoStr = mCurrentVideo.getInfo().get("actor");
        if (Utilities.notEmpty(infoStr)) {
            maps.add(getInfo(resources.getString(R.string.movie_info_actors), infoStr));
        }
//        infoStr = currentVideo.getInfo().get("description");
//        if (Utilities.notEmpty(infoStr)) {
//            maps.add(getInfo(resources.getString(R.string.movie_info_description), infoStr));
//        }
        infors.addAll(maps);
        infoAdapter.bindData(infors);

        bindAds();
    }

    private Map<String, String> getInfo(String key, String value) {
        Map<String, String> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    private void getAllEpisode() {
        activity.showLoadingDialog("","");
        getMovieApi().getListGroupsDetail(String.valueOf(mCurrentVideo.getFilmGroupIdInt()),
                new ApiCallbackV3<ArrayList<Movie>>() {
                    @Override
                    public void onSuccess(String lastId, ArrayList<Movie> movies, MovieWatched movieWatched) throws JSONException {
                        activity.hideLoadingDialog();
                        for (int i = 0; i < movies.size(); i++) {
                            if (movies.get(i).getId().equals(movieWatched.getId())) {
                                Movie movie = movies.get(i);
                                movie.setTimeWatched(movieWatched.getTimeSeek());
                                movies.set(i, movie);
                                Video lastWatched = Movie.movie2Video(movie);
                                mCurrentVideo = lastWatched;
                                infoHolder.setContinueToWatch(lastWatched);
                                loadWatchedMultiEpisote(lastWatched, movieWatched.getTimeSeek());
                                break;
                            }
                        }
                        updateEpisodesMovie(movies);
                        EventBus.getDefault().post(movies);
                    }

                    @Override
                    public void onError(String s) {
                        activity.hideLoadingDialog();
                        if (mIsPlaynow) {
                            playNow();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void updateEpisodesMovie(ArrayList<Movie> movies) {
        ArrayList<Video> videos = new ArrayList<>();
        for (Movie movie : movies) {
            Video video = Movie.movie2Video(movie);
            if (Utilities.isEmpty(video.getChapter())) continue;
            videos.add(video);
        }
        if (Utilities.isEmpty(videos)) {
            return;
        }
        episodes.clear();
        episodes.addAll(videos);
        ArrayList<Object> items = new ArrayList<>();
        items.addAll(videos);
        episodeAdapter.bindData(items);
        if (dialogFullScreenPlayer != null && dialogFullScreenPlayer.isShowing()) {
            if (Utilities.notEmpty(episodes) && episodes.contains(mCurrentVideo)) {
                int currentEpisode = episodes.indexOf(mCurrentVideo);
                dialogFullScreenPlayer.setEpisodes(episodes, true);
                dialogFullScreenPlayer.setCurrentEpisode(currentEpisode);
                dialogFullScreenPlayer.setEpisodesView(true);
            }
        }

    }

    private void getAllRelated() {
        getMovieApi().getRelateFilm(mCurrentVideo.getId(), userInfo.getPhone_number(),
                new ApiCallbackV2<ArrayList<Movie>>() {
                    @Override
                    public void onSuccess(String lastId, ArrayList<Movie> movies) throws JSONException {
                        if (relates != null) {
                            relates.clear();
                            relates.addAll(movies);
                        }
                        if (relatedAdapter != null) relatedAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(String s) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private void playVideo(Video video) {

        isShowingDialog = false;
        if (mPlayer != null && video != null) {
            if (mPlayer.getPlaybackState() == Player.STATE_READY) {
                mPlayer.getPlayerView().setCover(video.getImagePath());
            } else {
                mPlayer.getPlayerView().showCover(video.getImagePath());
            }
            video.setFromSource(Constants.LOG_SOURCE_TYPE.TYPE_KEENG_MOVIES);
            mPlayer.prepareV2(video);
            if (!isPause)
                mPlayer.setPlayWhenReady(true);
        }
        isDataInitiated = true;
        if (dialogFullScreenPlayer != null) {
            dialogFullScreenPlayer.resetInitM3u8();
            dialogFullScreenPlayer.updateUiCurrentVideo();
            dialogFullScreenPlayer.hideOnlyDragView();
        }
        infoHolder.setContinueToWatch(video);
        checkResumeVideo();
    }

    private void playWhenChangeSubtitleAudio(Video video){
        long currentTime = mPlayer.getCurrentPosition();
        mPlayer.prepareV3(video);
        mPlayer.seekTo(currentTime);
    }

//    private void loadMovieInfo5D

    private void loadWatchedMultiEpisote(Video mVideo, String timeWatched) {
        new MovieApi().getMovieDetail(mVideo.getId(), false, new ApiCallbackV2<Movie>() {
            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                if (result != null) {
                    Movie.copyLinkFromMovie(mVideo, result);
                    mCurrentVideo = mVideo;
                    mCurrentVideo.setTimeWatched(timeWatched);
                    if (mIsPlaynow) {
                        playVideo(mCurrentVideo);
                        initOrientationListener();
                        onFullScreen();
                    }
                }
            }

            @Override
            public void onError(String s) {
                ToastUtils.makeText(activity, getString(R.string.e500_internal_server_error));

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void loadMovieInfor5DMax(Video mVideo) {
        new MovieApi().getMovieDetail(mVideo.getId(), false, new ApiCallbackV2<Movie>() {
            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                if (result != null) {
                    Movie.copyLinkFromMovie(mVideo, result);
                    switchVideo(mVideo); // TODO: 4/24/2020 Cập nhật thông tin phim
                    playVideo(mCurrentVideo);
                }
            }

            @Override
            public void onError(String s) {
                ToastUtils.makeText(activity, getString(R.string.e500_internal_server_error));

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void addFrameVideo() {
        if (mPlayer != null) mPlayer.addPlayerViewTo(infoHolder.getFrameVideo());
    }

    private void enableController() {
        if (mPlayer != null) {
            mPlayer.getPlayerView().setEnabled(true);
            mPlayer.getPlayerView().enableFast(true);
            mPlayer.getPlayerView().setUseController(true);
            mPlayer.getPlayerView().getController().setVisibility(View.VISIBLE);
            mPlayer.updatePlaybackState();
        }
    }

    private void disableListenerPlayer() {
        if (mPlayer != null) {
            mPlayer.removeListener(eventListener);
            mPlayer.removeControllerListener(this);
        }
    }

    private void enableListenerPlayer() {
        if (mPlayer != null) {
            mPlayer.removeControllerListener(this);
            mPlayer.addControllerListener(this);
        }
    }

    private void switchVideo(@NonNull Video video) {
        Video temp = mCurrentVideo;
        if (temp != null) {
            temp.setPlaying(false);
            episodeAdapter.notifyItemChanged(Math.min(episodes.indexOf(temp), episodeAdapter.getItemCount() - 1), temp);
        }
        logWatched();
        mCurrentVideo = video;
        currentMovie = Video.video2Movie(video);

        video.setPlaying(true);
        episodeAdapter.notifyItemChanged(Math.min(episodes.indexOf(video), episodeAdapter.getItemCount() - 1), video);
    }

    private void bindVideo() {
        mCurrentVideo.setSave(ApplicationController.self().getApplicationComponent().providerVideoApi().isSave(mCurrentVideo));
        mCurrentVideo.setLike(currentMovie.isLike());
        infoHolder.bindVideoOnlyTitle(mCurrentVideo);
    }

    public void showSubtitleAudio() {
        dialogFullScreenPlayer.showSubtitleAndAudio();
    }

    /**
     * mở màn hình mini
     */
    private void openMini(String action) {
        if (isShowingDialog || !isDataInitiated) return;
        if (activity != null) {
            if (dialogFullScreenPlayer != null) dialogFullScreenPlayer.dismiss();
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                activity.finish();
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            isMini = true;
            VideoService.start(application, mCurrentVideo, playerName, action);

            if (popOut) {
                activity.finish();
                if (!BackStackHelper.getInstance().checkIsLastInStack(application, VideoPlayerActivity.class.getName())) {
                    /*
                     * nếu VideoPlayerActivity không phải là phần tử cuổi cùng
                     * thì sẽ thực hiên ACTION_MAIN
                     */
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /*
                             * về màn hình home của thiết bị
                             */
                            if (application != null) {
                                Intent startMain = new Intent(Intent.ACTION_MAIN);
                                startMain.addCategory(Intent.CATEGORY_HOME);
                                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                application.startActivity(startMain);
                            }
                        }
                    }, 300);
                }
            } else {
                if (BackStackHelper.getInstance().checkIsLastInStack(application, VideoPlayerActivity.class.getName())) {
                    /*
                     * nếu là activity cuối cùng vì mở màn hình home
                     */
                    activity.goToHome();
                } else {
                    /*
                     * nếu không phải là activity cuối cùng thì back
                     */
                    activity.finish();
                }
            }
        }
    }

    private void reportVideo(Video video, BaseSlidingFragmentActivity activity) {
        if (activity == null || activity.isFinishing()) {
            return;
        }
        if (reportVideoDialog != null && reportVideoDialog.isShowing()) {
            reportVideoDialog.dismiss();
        }
        reportVideoDialog = new ReportVideoDialog(activity).setCurrentVideo(video).setTitleDialog(activity.getResources().getString(R.string.title_report_movie));
        if (reportVideoDialog == null) return;
        reportVideoDialog.show();
    }

    public void handlerQuality(final Video video, BaseSlidingFragmentActivity activity) {
        //show quality
//        if (activity == null || activity.isFinishing()) return;
//        if (qualityVideoDialog != null && qualityVideoDialog.isShowing()) {
//            qualityVideoDialog.dismiss();
//        }
//
//        qualityVideoDialog = new QualityVideoDialog(activity)
//                .setCurrentVideo(mPlayer.getVideo())
//                .setOnQualityVideoListener((idx, currentVideo, resolution) -> {
//
//                    if (mPlayer.getVideo() == null
//                            || !Utilities.equals(mPlayer.getVideo(), video)
//                            || mPlayer.getVideo().getIndexQuality() == idx)
//                        return;
//
//                    long position = mPlayer.getCurrentPosition();
//                    long duration = mPlayer.getDuration();
//
//                    mPlayer.getVideo().setIndexQuality(idx);
//                    if (application != null)
//                        application.setConfigResolutionVideo(resolution.getKey());
//                    mPlayer.prepare(resolution.getVideoPath(), currentVideo.getSubTitleUrl());
//                    mPlayer.seekTo(Math.min(position, duration));
//                });
//        qualityVideoDialog.show();

    }

    private void speedVideo(final Video video, BaseSlidingFragmentActivity activity) {
        if (speedVideoDialog != null && speedVideoDialog.isShowing())
            speedVideoDialog.dismiss();

        if (mPlayer != null) {
            speedVideoDialog = DialogUtils.showSpeedDialog(activity, mPlayer.getPlaybackParameters().speed, (object, menuId) -> {
                if (menuId == Constants.MENU.MENU_SETTING && object instanceof Float) {
                    if (mPlayer.getVideo() == null || !Utilities.equals(mPlayer.getVideo(), video))
                        return;
                    PlaybackParameters param = new PlaybackParameters((Float) object);
                    mPlayer.setPlaybackParameters(param);
                }
            });
        }
    }

    private void registerVideo(final BaseSlidingFragmentActivity activity) {
        String labelOK = getResources().getString(R.string.register);
        String labelCancel = getResources().getString(R.string.cancel);
        ContentConfigBusiness contentConfigBusiness = ApplicationController.self().getConfigBusiness();
        String title = contentConfigBusiness.getSubscriptionConfig().getTitle();
        String msg = contentConfigBusiness.getSubscriptionConfig().getConfirm();
        PopupHelper.getInstance().showDialogConfirm(activity,
                title,
                msg,
                labelOK,
                labelCancel,
                new ClickListener.IconListener() {
                    @Override
                    public void onIconClickListener(View view, Object entry, int menuId) {
                        if (menuId != 0) { // 0 là id btnNo
                            String id = application.getConfigBusiness().getSubscriptionConfig().getCmd();
                            ReportHelper.checkShowConfirmOrRequestFakeMo(application, activity, application.getConfigBusiness().getSubscriptionConfig().getReconfirm(), id, "movie_keeng");
                        }
                    }
                }, null,
                Constants.MENU.POPUP_CONFIRM_REGISTER_VIP);
    }

    private ArrayList<ItemContextMenu> getContextMenus(BaseSlidingFragmentActivity activity) {
        ArrayList<ItemContextMenu> itemContextMenus = new ArrayList<>();
        itemContextMenus.add(getContextMenu(activity, R.string.report_video, R.drawable.ic_flag_option, Constants.MENU.MENU_REPORT_VIDEO));
        itemContextMenus.add(getContextMenu(activity, R.string.speed_video, R.drawable.ic_play_speed_option, Constants.MENU.MENU_SPEED_VIDEO));
        if (canMini()) {
            itemContextMenus.add(getContextMenu(activity, R.string.pop_out, R.drawable.ic_v5_pop_out, Constants.MENU.MENU_MINI_VIDEO));
        }
        if (ApplicationController.self() != null
                && !ApplicationController.self().getReengAccountBusiness().isVip()
                && ApplicationController.self().getReengAccountBusiness().isVietnam()
                && !ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
            String title = ApplicationController.self().getConfigBusiness().getSubscriptionConfig().getTitle();
            itemContextMenus.add(getContextMenu(activity, title, R.drawable.ic_v5_video_vip, Constants.MENU.MENU_VIP_VIDEO));
        }
        itemContextMenus.add(getContextMenu(activity, R.string.btn_cancel_option, R.drawable.ic_close_option, Constants.MENU.MENU_EXIT));
        return itemContextMenus;
    }

    private ItemContextMenu getContextMenu(BaseSlidingFragmentActivity activity, @StringRes int mStringRes, @DrawableRes int mDrawableRes, int itemId) {
        return getContextMenu(activity, activity.getString(mStringRes), mDrawableRes, itemId);
    }

    private ItemContextMenu getContextMenu(BaseSlidingFragmentActivity activity, String text, @DrawableRes int mDrawableRes, int itemId) {
        return new ItemContextMenu(text, mDrawableRes, null, itemId);
    }

    /**
     * trạng thái của item video
     *
     * @param isReady true - play, false - pause
     */
    private void changeStatusPlayer(boolean isReady) {
        if (mCurrentVideo != null)
            mCurrentVideo.setPause(!isReady);
    }

    private void autoPauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPaused(true);
            if (mPlayer.getAdPlayer() != null && mPlayer.isAdDisplayed()) {
                mPlayer.getAdPlayer().pause();
            }
            playWhenReady = mPlayer.getPlayWhenReady();
            mPlayer.setPlayWhenReady(false);
        }
    }

    public MovieApi getMovieApi() {
        if (movieApi == null) movieApi = new MovieApi();
        return movieApi;
    }

    @Override
    public void onClickMovieItem(Object item, int position) {
        if (item instanceof Movie) {
            activity.playMovies((Movie) item);
//            activity.finish();
        }
    }

    @Override
    public void onClickMoreMovieItem(Object item, int position) {
        if (item instanceof Movie)
            DialogUtils.showOptionMovieItem(activity, (Movie) item, this);
    }

    @Override
    public void onClickLikeMovieItem(Object item, int position) {

    }

    /**
     * Click Rate,Share
     *
     * @param object
     * @param menuId
     */
    @Override
    public void onClickMoreItem(Object object, int menuId) {
        if (activity != null && !activity.isFinishing() && object != null) {
            switch (menuId) {
                case Constants.MENU.MENU_SHARE_LINK:
                    ShareUtils.openShareMenu(activity, object);
                    break;
                case Constants.MENU.MENU_ADD_FAVORITE:
                    if (object instanceof Movie) {
                        FeedModelOnMedia feed = FeedModelOnMedia.convertMovieToFeedModelOnMedia((Movie) object);
                        new WSOnMedia(application).logActionApp(feed.getFeedContent().getUrl(), "", feed.getFeedContent()
                                , FeedModelOnMedia.ActionLogApp.LIKE, "", feed.getBase64RowId(), ""
                                , FeedModelOnMedia.ActionFrom.mochavideo, new ApiCallbackV2<String>() {

                                    @Override
                                    public void onError(String s) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }

                                    @Override
                                    public void onSuccess(String msg, String result) throws JSONException {
                                        if (activity != null)
                                            activity.showToast(R.string.add_favorite_success);
                                    }
                                });
                    }
                    break;
                case Constants.MENU.MENU_ADD_LATER:
                    if (object instanceof Movie) {
                        getMovieApi().insertWatchLater((Movie) object, new ApiCallbackV2<String>() {

                            @Override
                            public void onError(String s) {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onSuccess(String msg, String result) throws JSONException {
                                if (activity != null)
                                    activity.showToast(R.string.add_later_success);
                            }
                        });
                    }
                    break;
            }
        }
    }

    @Override
    public void onShowShareDialog() {
        if (mPlayer != null) mPlayer.setPlayWhenReady(false);
    }

    @Override
    public void onDismissShareDialog(boolean isPlayingState) {
        if (isPause) playWhenReady = isPlayingState;
        else {
            if (mPlayer != null) mPlayer.setPlayWhenReady(isPlayingState);
        }
    }

    private void dismissAllDialogs() {
        if (optionsVideoDialog != null && optionsVideoDialog.isShowing()) {
            optionsVideoDialog.dismiss();
            optionsVideoDialog = null;
        }
        if (reportVideoDialog != null && reportVideoDialog.isShowing()) {
            reportVideoDialog.dismiss();
            reportVideoDialog = null;
        }
        if (speedVideoDialog != null && speedVideoDialog.isShowing()) {
            speedVideoDialog.dismiss();
            speedVideoDialog = null;
        }
        if (qualityVideoDialog != null && qualityVideoDialog.isShowing()) {
            qualityVideoDialog.dismiss();
            qualityVideoDialog = null;
        }
        if (shareBusiness != null) {
            shareBusiness.dismissAll();
            shareBusiness = null;
        }
    }

    private void bindAds() {
        if (layout_ads != null)
            AdsManager.getInstance().showAdsNative(layout_ads);
    }

    private void logWatched() {
        if (mPlayer != null && currentMovie != null) {
            MovieWatched movieWatched = new MovieWatched(currentMovie);
            movieWatched.setDuration(String.valueOf(mPlayer.getDuration() / 1000));
            movieWatched.setTimeSeek(String.valueOf(mPlayer.getCurrentPosition() / 1000));
            movieWatched.setThroughput(String.valueOf(mPlayer.getThroughput()));
            movieWatched.setTotalTimePlay(String.valueOf(mPlayer.getTotalTimePlay()));
            getMovieApi().insertLogWatched(movieWatched);
            mPlayer.resetEventLogger();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlay:
                playNow();
                handlerLogWatch.removeCallbacks(runnableLogWatch);
                handlerLogWatch.postDelayed(runnableLogWatch,30000);
                ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_PLAY);
                break;
            case R.id.llControllerRate:

                if (isLogin() == EnumUtils.LoginVia.NOT){
                    new UserInfoBusiness(activity).showLoginDialog(activity, R.string.info, R.string.content_popup_login);
                    return;
                }
                if (ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()) {
                    activity.showDialogLogin();
                } else {
                    viewRate.setVisibility(viewRate.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                }
                break;
        }
    }

    private List<Director> getListDirector() {
        return currentMovie.getDirector();
    }

    private List<Actor> getListActor() {
        return currentMovie.getActor();
    }

    private String getCategories() {
        if (currentMovie == null || currentMovie.getCategories() == null || currentMovie.getCategories().size() == 0)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        int leght = currentMovie.getCategories().size();
        for (int i = 0; i < leght; i++) {
            Category category = currentMovie.getCategories().get(i);
            if (i != leght - 1) {
                stringBuilder.append(category.getCategoryname() + " \u2022 ");
            } else {
                stringBuilder.append(category.getCategoryname());
            }
        }
        return stringBuilder.toString();
    }

    // on click dao dien or dien vien
    @Override
    public void onItemViewClickListener(int position, List<?> list) {
        String title = "";
        String idDirectorOrCast = "";
        if (list.get(position) instanceof Director) {
            title = ((Director) list.get(position)).getName();
            idDirectorOrCast = String.valueOf(((Director) list.get(position)).getId());
        }
        if (list.get(position) instanceof Actor) {
            title = ((Actor) list.get(position)).getName();
            idDirectorOrCast = String.valueOf(((Actor) list.get(position)).getId());
        }
        MyListFragment myListFragment = MyListFragment.newInstance(title,idDirectorOrCast);
        addFragment(R.id.frameListByinfo, myListFragment, true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getComments(ArrayList<FeedAction> comments) {
//        tvReview.setText("("+comments.size() + " " + getString(R.string.reviews)+")");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void dismissCommentBottomSheet(Boolean isDismiss) {
        nscrFilmDetail.post(new Runnable() {
            @Override
            public void run() {
                nscrFilmDetail.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestPermissionBright(RequestPermissionBright requestPermissionBright) {
        if (dialogConfirm == null) {
            dialogConfirm = new DialogConfirm(activity, true);
            dialogConfirm.setLabel(getString(R.string.permission_allow_floating_view));
            dialogConfirm.setMessage(getString(R.string.permission_allow_change_brightness));
            dialogConfirm.setUseHtml(true);
            dialogConfirm.setCancelable(false);
            dialogConfirm.setNegativeLabel(getString(R.string.cancel));
            dialogConfirm.setPositiveLabel(getString(R.string.ok));
            dialogConfirm.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    if (activity != null && isAdded()) {
                        autoPauseVideo();
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent);
                    }
                }
            });
            dialogConfirm.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    dialogConfirm.dismiss();
                }
            });
        }
        if (!dialogConfirm.isShowing()) {
            dialogConfirm.show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void requestPermissionVolume(RequestPermissionVolume requestPermissionVolume) {
        if (dialogVolume == null) {
            dialogVolume = new DialogConfirm(activity, true);
            dialogVolume.setLabel(getString(R.string.permission_allow_floating_view));
            dialogVolume.setMessage(getString(R.string.permission_allow_change_volume));
            dialogVolume.setUseHtml(true);
            dialogVolume.setCancelable(false);
            dialogVolume.setNegativeLabel(getString(R.string.cancel));
            dialogVolume.setPositiveLabel(getString(R.string.ok));
            dialogVolume.setPositiveListener(new PositiveListener<Object>() {
                @Override
                public void onPositive(Object result) {
                    if (activity != null && isAdded()) {
                        autoPauseVideo();
                        Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                        getContext().startActivity(intent);
                    }
                }
            });
            dialogVolume.setNegativeListener(new NegativeListener() {
                @Override
                public void onNegative(Object result) {
                    dialogVolume.dismiss();
                }
            });
        }
        if (!dialogVolume.isShowing()) {
            dialogVolume.show();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(btnPlayTrailer != null){
            btnPlayTrailer.removeCallbacks(runnable);
        }
        if(handler != null){
            handler.removeCallbacks(runnableTime);
        }
        if(handlerLogWatch != null){
            handlerLogWatch.removeCallbacks(runnableLogWatch);
        }
        EventBus.getDefault().unregister(this);
    }

    private void getInfoMovie() {
        getMovieApi().getMovieDetail(currentMovie.getId(), new ApiCallbackV2<Movie>() {
            @Override
            public void onSuccess(String msg, Movie result) throws JSONException {
                if (result != null) {
                    if (result.getId().equals(currentMovie.getId())) {
                        if (isLogin() == EnumUtils.LoginVia.NOT || ApplicationController.self().getReengAccountBusiness().isAnonymousLogin()){
                            //nếu chưa đăng nhập thì không làm gì cả
                            infoHolder.rate(false);
                            infoHolder.setLike(false);
                        }else{
                            currentMovie.setLike(result.isLike());
                            infoHolder.rate(result.isIs_rated());
                            infoHolder.setLike(result.isLike());
                            initLike = result.isLike();
                        }
                        infoHolder.bindVideo(Movie.movie2Video(result));
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void likeVideo(String videoId) {
        getMovieApi().likeVideo(videoId, new ApiCallbackV2<BaseResponse>() {
            @Override
            public void onSuccess(String msg, BaseResponse result) throws JSONException {
                if (result != null && result.getCode().equals("200")) {
                    Log.d(TAG, "Like video success");
                }
            }

            @Override
            public void onError(String s) {
                Log.d(TAG, "Like video onError : " + s);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "Like video onComplete");
            }
        });
    }

    private void unlikeVideo(String videoId) {
        getMovieApi().unLikeVideo(videoId, new ApiCallbackV2<BaseResponse>() {
            @Override
            public void onSuccess(String msg, BaseResponse result) throws JSONException {
                if (result != null && result.getCode().equals("200")) {
                    Log.d(TAG, "Unlike video success");
                }
            }

            @Override
            public void onError(String s) {
                Log.d(TAG, "Unlike video onError : " + s);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "Unlike video onComplete");
            }
        });
    }

    private void checkResumeWhenOpenDetail() {
        MovieApi.getInstance().getListLogWatch(new ApiCallbackV2<ListLogWatchResponse>() {
            @Override
            public void onSuccess(String msg, ListLogWatchResponse result) throws JSONException {
                int leght = result.getResult().size();
                for (int i = 0; i < leght; i++) {
                    if (currentMovie.getId().equals(result.getResult().get(i).getIdPhim())) {
                        infoHolder.setContinueToWatch(Movie.movie2Video(currentMovie));
                        break;
                    }
                }
            }

            @Override
            public void onError(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void initTrailer(String url) {

        playerViewTrailer = new VideoPlayerView(activity);
        playerViewTrailer.setUseController(false);
        playerViewTrailer.enableFast(true);
        Picasso.with(activity).load(currentMovie.getImagePath()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                playerViewTrailer.setDefaultArtwork(bitmap);
                playerViewTrailer.setUseArtwork(true);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.d(TAG, "onBitmapFailed");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
        layoutPlayerTrailer.addView(playerViewTrailer);
        String urlOriginal = url;
        url = XXTEACrypt.decryptBase64StringToString(urlOriginal, BuildConfig.KEY_XXTEA_5DMAX_MOVIES);
        url = url == null ? urlOriginal : url;

        if (playerTrailer == null) {
            TrackSelector trackSelectorDef = new DefaultTrackSelector();
            playerTrailer = ExoPlayerFactory.newSimpleInstance(activity, trackSelectorDef);
        }

        String userAgent = Util.getUserAgent(activity, activity.getString(R.string.app_name));
        DefaultDataSourceFactory sourceFactory = new DefaultDataSourceFactory(activity, userAgent);

        playerTrailer.prepare(buildMediaSource(sourceFactory, url));

        playerViewTrailer.setPlayer(playerTrailer);
        playerTrailer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == Player.STATE_BUFFERING || playbackState == Player.STATE_READY) {
                    if (!isPlaying && playerTrailer.getPlayWhenReady()) {
                        playerTrailer.setPlayWhenReady(false);
                        updateButtonPlay(false);
                    }
                } else if (playbackState == Player.STATE_ENDED) {
                    isPlaying = true;
                    updateButtonPlay(true);
                    currentTime = 0;
                    playerTrailer.seekTo(currentTime);
                } else if (playbackState == Player.STATE_IDLE) {
                    isPlaying = false;
                    updateButtonPlay(false);
                }
                Log.d(TAG, "isPlaying : " + isPlaying + ". playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);
            }
        });
        btnPlayTrailer.setVisibility(View.VISIBLE);
        btnPlayTrailer.setOnClickListener(view -> {
            if (playerTrailer != null) {
                btnPlayTrailer.removeCallbacks(runnable);
                boolean oldStatus = isPlaying;
                isPlaying = !isPlaying;
                if (oldStatus) {
                    Log.d(TAG, "Pause");
                    updateButtonPlay(false);
                    currentTime = playerTrailer.getCurrentPosition();
                    playerTrailer.setPlayWhenReady(false);
                    handler.removeCallbacks(runnableTime);
                    ApplicationController.self().getFirebaseEventBusiness().logPauseTrailer(currentMovie.getId(),currentMovie.getName(), String.valueOf(playerTrailer.getDuration()));
                } else {
                    Log.d(TAG, "Play");
                    updateButtonPlay(true);
                    playerTrailer.setPlayWhenReady(true);
                    if (currentTime == playerTrailer.getDuration()) {
                        currentTime = 0;
                    }
                    playerTrailer.seekTo(currentTime);
                    btnPlayTrailer.postDelayed(runnable, 2000);
                    handler.removeCallbacks(runnableTime);
                    handler.postDelayed(runnableTime, 0);
                }
            }
        });
        btnMuteTrailer.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_volume_up_new));
        btnMuteTrailer.setOnClickListener(v -> {
            if (playerTrailer != null) {
                if (playerTrailer.getVolume() == 0) {
                    playerTrailer.setVolume(1);
                    btnMuteTrailer.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_volume_up_new));
                } else {
                    playerTrailer.setVolume(0);
                    btnMuteTrailer.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_volume_off_new));
                }
            }
        });
        controlViewTrailer.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                btnPlayTrailer.removeCallbacks(runnable);
                btnPlayTrailer.setVisibility(View.VISIBLE);
                if (playerTrailer != null && playerTrailer.isPlaying()) {
                    btnPlayTrailer.postDelayed(runnable, 2000);
                }
            }
            return false;
        });
        btnFullTrailer.setOnClickListener(v -> {
            ApplicationController.self().getFirebaseEventBusiness().logButtonFilmClick(FirebaseEventConstant.BUTTON_WATCH_FULLSCREEN_TRAILER);
            if (playerTrailer != null) {
                playerTrailer.setPlayWhenReady(false);
                updateButtonPlay(false);
                currentTime = playerTrailer.getCurrentPosition();
            }
            progress_bar.removeCallbacks(runnableTime);
            trailerFullScreenPlayer = new TrailerFullScreenPlayerDialog(activity);
            trailerFullScreenPlayer.setData(currentMovie.getName(),currentMovie.getTrailerUrl(), currentTime);
            trailerFullScreenPlayer.setTrailerPlayerListener(new TrailerPlayerListener() {
                @Override
                public void dismissTrailerDialog(boolean continuePlay, boolean isEnd, long time) {
                    currentTime = time;
                    if(activity != null){
                        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }
                    if(isEnd){
                        isPlaying = false;
                        updateButtonPlay(false);
                        currentTime = 0;
                        playerTrailer.seekTo(currentTime);
                    }else{
                        progress_bar.setProgress((int) (currentTime*100/playerTrailer.getDuration()));
                        playerTrailer.seekTo(currentTime);
                        playerTrailer.setPlayWhenReady(continuePlay);
                        updateButtonPlay(continuePlay);
                    }
                }
            });
            trailerFullScreenPlayer.show();
        });
        // auto play when open detail
        isPlaying = true;
        playerTrailer.setPlayWhenReady(true);
        updateButtonPlay(true);
        btnPlayTrailer.setVisibility(View.INVISIBLE);
        Log.d(TAG, "Init available now");
    }

    private void updateButtonPlay(boolean play) {
        if (play) {
            if (btnPlayTrailer != null)
                btnPlayTrailer.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_pause_available));
            if (handler != null) {
                handler.removeCallbacks(runnableTime);
                handler.postDelayed(runnableTime, 0);
            }
        } else {
            if (btnPlayTrailer != null) {
                btnPlayTrailer.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_play_available));
                btnPlayTrailer.setVisibility(View.VISIBLE);
            }
            if (handler != null)
                handler.removeCallbacks(runnableTime);
        }
    }

    private MediaSource buildMediaSource(DefaultDataSourceFactory dataSourceFactory, String url) {
        Uri uri = Uri.parse(url);
        @C.ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultDashChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultSsChunkSource.Factory(dataSourceFactory), dataSourceFactory).createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    public void stopTrailer() {
        if (playerTrailer == null) {
            return;
        }
        if (!isPlaying) {
            if(playerTrailer.getPlayWhenReady()){
                playerTrailer.setPlayWhenReady(false);
                updateButtonPlay(false);
                currentTime = playerTrailer.getCurrentPosition();
                progress_bar.removeCallbacks(runnableTime);
                btnPlayTrailer.setVisibility(View.VISIBLE);
            }
            return;
        }
        isPlaying = false;
        playerTrailer.setPlayWhenReady(false);
        updateButtonPlay(false);
        currentTime = playerTrailer.getCurrentPosition();
        progress_bar.removeCallbacks(runnableTime);
        btnPlayTrailer.setVisibility(View.VISIBLE);
    }
}

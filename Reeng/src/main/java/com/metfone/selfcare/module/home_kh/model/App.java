package com.metfone.selfcare.module.home_kh.model;

public class App {
    private String id;
    private String name;
    private String shortDes;
    private Object fullDes;
    private String iconUrl;
    private String iosLink;
    private String androidLink;

    public String getID() { return id; }
    public void setID(String value) { this.id = value; }

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }

    public String getShortDes() { return shortDes; }
    public void setShortDes(String value) { this.shortDes = value; }

    public Object getFullDes() { return fullDes; }
    public void setFullDes(Object value) { this.fullDes = value; }

    public String getIconUrl() { return iconUrl; }
    public void setIconUrl(String value) { this.iconUrl = value; }

    public String getIosLink() { return iosLink; }
    public void setIosLink(String value) { this.iosLink = value; }

    public String getAndroidLink() { return androidLink; }
    public void setAndroidLink(String value) { this.androidLink = value; }
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/6/4
 *
 */

package com.metfone.selfcare.module.search.holder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.module.search.adapter.SearchAllAdapter;
import com.metfone.selfcare.module.search.adapter.SearchDetailAdapter;
import com.metfone.selfcare.module.search.listener.SearchAllListener;
import com.metfone.selfcare.module.search.model.ChatProvisional;
import com.metfone.selfcare.module.search.model.MoviesProvisional;
import com.metfone.selfcare.module.search.model.NewsProvisional;
import com.metfone.selfcare.module.search.model.RewardProvisional;
import com.metfone.selfcare.module.search.model.SearchHistoryProvisional;
import com.metfone.selfcare.module.search.model.TiinProvisional;
import com.metfone.selfcare.module.search.model.VideoProvisional;
import com.metfone.selfcare.module.search.utils.SearchUtils;
import com.metfone.selfcare.util.Utilities;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class BoxHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.line)
    @Nullable
    View line;
    @BindView(R.id.button_delete)
    @Nullable
    View btnDelete;
    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.ll_anonymous)
    @Nullable
    ViewGroup llAnonymous;
    @BindView(R.id.tv_see_all)
    @Nullable
    TextView tvSeeAll;

    private SearchAllListener.OnAdapterClick listener;
    private SearchDetailAdapter adapter;
    private ArrayList<Object> data;
    private int parentType;
    private ApplicationController mApplication;
    private int sizeAvatar;

    @SuppressWarnings("DuplicateBranchesInSwitch")
    public BoxHolder(View view, Activity activity, final SearchAllListener.OnAdapterClick listener, int type) {
        super(view);
        mApplication = (ApplicationController) activity.getApplication();
        this.listener = listener;
        parentType = type;
        sizeAvatar = (int) mApplication.getResources().getDimension(R.dimen.search_size_avatar_contact_more);
        data = new ArrayList<>();
        adapter = new SearchDetailAdapter(activity, data);
        adapter.setParentType(parentType);
        adapter.setListener(listener);
        switch (parentType) {
            case SearchAllAdapter.TYPE_CHAT:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
                break;
            case SearchAllAdapter.TYPE_VIDEO:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
                break;
            case SearchAllAdapter.TYPE_CHANNEL:
                BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, true);
                break;
            case SearchAllAdapter.TYPE_NEWS:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
                break;
            case SearchAllAdapter.TYPE_MOVIES:
                BaseAdapter.setupGridRecycler(activity, recyclerView, null, adapter, 2, R.dimen.v5_spacing_normal, true);
                break;
            case SearchAllAdapter.TYPE_MUSIC:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
                break;
            case SearchAllAdapter.TYPE_TIIN:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
                break;
            case SearchAllAdapter.TYPE_MOVIES_ALL:
//                BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, true);
                int rvMovieSpanCount = 3;
                BaseAdapter.setupGridRecycler(activity, recyclerView, null, adapter, rvMovieSpanCount, R.dimen.v5_spacing_normal, true);
                break;
            case SearchAllAdapter.TYPE_REWARD:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, true);
                break;
            default:
                BaseAdapter.setupVerticalRecycler(activity, recyclerView, null, adapter, false);
                break;
        }
        if (btnDelete != null && listener instanceof SearchAllListener.OnClickBoxHistory) {
            btnDelete.setOnClickListener(v -> ((SearchAllListener.OnClickBoxHistory) listener).onClickDeleteHistorySearch());
        }
        if (llAnonymous != null && listener instanceof SearchAllListener.OnClickBoxThreadChat) {
            llAnonymous.setOnClickListener(v -> ((SearchAllListener.OnClickBoxThreadChat) listener).onClickLogin());
        }
    }

    @SuppressLint("SetTextI18n")
    public void bindData(Object item, int position, String keySearch) {
        if (line != null) line.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
        if (tvSeeAll != null) tvSeeAll.setVisibility(View.GONE);
        if (item instanceof ChatProvisional) {
            if (mApplication.getReengAccountBusiness().isAnonymousLogin()) {
                recyclerView.setVisibility(View.GONE);
                if (llAnonymous != null) {
                    llAnonymous.setVisibility(View.VISIBLE);
                }
            }
            data.clear();
            if (tvTitle != null) tvTitle.setText(R.string.search_tab_chat);
            ChatProvisional model = (ChatProvisional) item;
            final ArrayList<Object> listChat = model.getListChat();
            if (Utilities.notEmpty(listChat)) {
                int size = listChat.size();
                data.addAll(new ArrayList<>(listChat.subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL_CONTACT))));
                boolean isVisible = size > SearchUtils.NUM_SHOW_VIEW_ALL_CONTACT && !mApplication.getReengAccountBusiness().isAnonymousLogin();
                if (isVisible && tvSeeAll != null)
                    tvSeeAll.setVisibility(View.VISIBLE);

//                boolean isVisible = !mApplication.getReengAccountBusiness().isAnonymousLogin();
//                if (isVisible && tvSeeAll != null)
//                    tvSeeAll.setVisibility(View.VISIBLE);
            }
            adapter.setKeySearch(keySearch);
            adapter.notifyDataSetChanged();
        } else if (item instanceof VideoProvisional) {
            data.clear();
            VideoProvisional model = (VideoProvisional) item;
            if (model.getVideos() != null) {
                if (tvTitle != null) tvTitle.setText(R.string.search_tab_video);
                if (Utilities.notEmpty(model.getVideos())) {
                    int size = model.getVideos().size();
                    data.addAll(new ArrayList<>(model.getVideos().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                    if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAll != null) {
                        tvSeeAll.setVisibility(View.VISIBLE);
                    }
                }
            } else if (model.getChannels() != null) {
                if (tvTitle != null) tvTitle.setText(R.string.search_tab_channel);
                if (Utilities.notEmpty(model.getChannels())) {
                    int size = model.getChannels().size();
                    data.addAll(new ArrayList<>(model.getChannels().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                    if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAll != null) {
                        tvSeeAll.setVisibility(View.VISIBLE);
                    }
                }
            }
            adapter.setKeySearch(keySearch);
            adapter.notifyDataSetChanged();
        } else if (item instanceof NewsProvisional) {
            data.clear();
            if (tvTitle != null) tvTitle.setText(R.string.search_tab_news);
            NewsProvisional model = (NewsProvisional) item;
            if (Utilities.notEmpty(model.getData())) {
                int size = model.getData().size();
                data.addAll(new ArrayList<>(model.getData().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAll != null) {
                    tvSeeAll.setVisibility(View.VISIBLE);
                }
            }
            adapter.setKeySearch(keySearch);
            adapter.notifyDataSetChanged();
        } else if (item instanceof MoviesProvisional) {
            data.clear();
            if (tvTitle != null) tvTitle.setText(R.string.search_tab_movies);
            MoviesProvisional model = (MoviesProvisional) item;
            if (Utilities.notEmpty(model.getData())) {
                int size = model.getData().size();
                data.addAll(new ArrayList<>(model.getData().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAll != null) {
                    tvSeeAll.setVisibility(View.VISIBLE);
                }
            }
            adapter.setKeySearch(keySearch);
            adapter.notifyDataSetChanged();
        } else if (item instanceof SearchHistoryProvisional) {
            data.clear();
            if (tvTitle != null) tvTitle.setText(R.string.recentSearches);
            SearchHistoryProvisional model = (SearchHistoryProvisional) item;
            if (Utilities.notEmpty(model.getData())) {
                int size = model.getData().size();
                data.addAll(new ArrayList<>(model.getData().subList(0, Math.min(size, SearchUtils.MAX_HISTORY_SEARCH))));
            }
            adapter.setKeySearch(keySearch);
            adapter.notifyDataSetChanged();
        } else if (item instanceof TiinProvisional) {
            data.clear();
            if (tvTitle != null) tvTitle.setText(R.string.search_tab_tiin);
            TiinProvisional model = (TiinProvisional) item;
            if (Utilities.notEmpty(model.getData())) {
                int size = model.getData().size();
                data.addAll(new ArrayList<>(model.getData().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL))));
                if (size > SearchUtils.NUM_SHOW_VIEW_ALL && tvSeeAll != null) {
                    tvSeeAll.setVisibility(View.VISIBLE);
                }
            }
            adapter.notifyDataSetChanged();
        }else if (item instanceof RewardProvisional) {
            data.clear();
            if (tvTitle != null) tvTitle.setText(R.string.txt_rewards);
            RewardProvisional model = (RewardProvisional) item;
            if (Utilities.notEmpty(model.getData())) {
                int size = model.getData().size();
                data.addAll(new ArrayList<>(model.getData().subList(0, Math.min(size, SearchUtils.NUM_SHOW_VIEW_ALL_REWARD))));
                if (size > SearchUtils.NUM_SHOW_VIEW_ALL_REWARD && tvSeeAll != null) {
                    tvSeeAll.setVisibility(View.VISIBLE);
                }
            }
            adapter.notifyDataSetChanged();
        }

        //hide button view all
        tvTitle.setTextColor(Color.WHITE);
//        tvSeeAll.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.layout_title)
    public void onClickItem() {
        if (listener != null) {
            switch (parentType) {
                case SearchAllAdapter.TYPE_CHAT:
                    if (listener instanceof SearchAllListener.OnClickBoxThreadChat)
                        ((SearchAllListener.OnClickBoxThreadChat) listener).onClickThreadChatMore();
                    break;
                case SearchAllAdapter.TYPE_VIDEO:
                    if (listener instanceof SearchAllListener.OnClickBoxVideo)
                        ((SearchAllListener.OnClickBoxVideo) listener).onClickVideoMore();
                    break;
                case SearchAllAdapter.TYPE_CHANNEL:
                    if (listener instanceof SearchAllListener.OnClickBoxVideo)
                        ((SearchAllListener.OnClickBoxVideo) listener).onClickChannelMore();
                    break;
                case SearchAllAdapter.TYPE_NEWS:
                    if (listener instanceof SearchAllListener.OnClickBoxNews)
                        ((SearchAllListener.OnClickBoxNews) listener).onClickNewsMore();
                    break;
                case SearchAllAdapter.TYPE_MOVIES_ALL:
                case SearchAllAdapter.TYPE_MOVIES:
                    if (listener instanceof SearchAllListener.OnClickBoxMovies)
                        ((SearchAllListener.OnClickBoxMovies) listener).onClickMoviesMore();
                    break;
                case SearchAllAdapter.TYPE_MUSIC:
                    if (listener instanceof SearchAllListener.OnClickBoxMusic)
                        ((SearchAllListener.OnClickBoxMusic) listener).onClickMusicMore();
                    break;
                case SearchAllAdapter.TYPE_TIIN:
                    if (listener instanceof SearchAllListener.OnClickBoxTiin)
                        ((SearchAllListener.OnClickBoxTiin) listener).onClickTiinMore();
                    break;
                case SearchAllAdapter.TYPE_REWARD:
                    if (listener instanceof SearchAllListener.OnClickReward) {
//                        ((SearchAllListener.OnClickReward) listener).onClickReward(null);
                        ((SearchAllListener.OnClickReward) listener).onClickRewardMore();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

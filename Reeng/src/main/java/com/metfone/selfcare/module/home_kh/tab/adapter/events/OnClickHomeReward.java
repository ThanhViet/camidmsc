package com.metfone.selfcare.module.home_kh.tab.adapter.events;

public interface OnClickHomeReward {
    void onClickRewardItem(Object item, int position);
    void onClickRewardCategoryItem(Object item, int position);
}

/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/5
 *
 */

package com.metfone.selfcare.module.myviettel.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DataChallengeProvisional implements Serializable {
    private ArrayList<DataChallenge> data;
    private long totalTransaction;
    private long totalCommission;
    private boolean isSuccess = false;

    public ArrayList<DataChallenge> getData() {
        return data;
    }

    public void setData(ArrayList<DataChallenge> data) {
        this.data = data;
    }

    public long getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(long totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public long getTotalCommission() {
        return totalCommission;
    }

    public void setTotalCommission(long totalCommission) {
        this.totalCommission = totalCommission;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}

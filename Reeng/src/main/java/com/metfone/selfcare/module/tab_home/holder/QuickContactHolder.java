package com.metfone.selfcare.module.tab_home.holder;

import android.app.Activity;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.module.tab_home.adapter.TabHomeDetailAdapter;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;

import java.util.ArrayList;

import butterknife.BindView;

public class QuickContactHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_title)
    @Nullable
    TextView tvTitle;
    @BindView(R.id.tv_view_all)
    @Nullable
    TextView tvViewAll;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private ArrayList<Object> data;
    private TabHomeDetailAdapter adapter;

    public QuickContactHolder(View view, Activity activity, final TabHomeListener.OnAdapterClick listener) {
        super(view);
        if (tvTitle != null) tvTitle.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickTitleBoxContact();
                }
            }
        });
        if (tvViewAll != null) tvViewAll.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                if (listener != null) {
                    listener.onClickTitleBoxContact();
                }
            }
        });
        data = new ArrayList<>();
        adapter = new TabHomeDetailAdapter(activity);
        adapter.setListener(listener);
        BaseAdapter.setupHorizontalRecycler(activity, recyclerView, null, adapter, true);
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof TabHomeModel) {
            data.clear();
            data.addAll(((TabHomeModel) item).getList());
            adapter.setItems(data);
            adapter.notifyDataSetChanged();
        }
    }

}

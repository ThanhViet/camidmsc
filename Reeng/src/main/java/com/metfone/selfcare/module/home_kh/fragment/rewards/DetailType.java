package com.metfone.selfcare.module.home_kh.fragment.rewards;

public enum DetailType {
    DATA("2", "DATA"),
    BALANCE("1030c5a2-8c2f-4dfe-b374-23825a2b60b4", "CHARGE"),
    VALIDITY("DAY", "DAY");
    public final String giftId;
    public final String dataType;

    DetailType(String giftId, String datatype) {
        this.giftId = giftId;
        this.dataType = datatype;
    }
}

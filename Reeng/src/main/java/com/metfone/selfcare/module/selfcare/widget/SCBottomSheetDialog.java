package com.metfone.selfcare.module.selfcare.widget;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.selfcare.TabSelfCareActivity;
import com.metfone.selfcare.module.selfcare.model.SCBundle;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.ui.CusBottomSheetDialog;

import java.io.Serializable;

public class SCBottomSheetDialog implements View.OnClickListener {

    Context context;
    CusBottomSheetDialog bottomSheetDialog;

    private View layout_my_account;
    private View layout_my_utilities;
    private View layout_my_package;
    private View layout_usage_history;
    private View layout_value;
    private View layout_store;
    private View layout_connect;

    public SCBottomSheetDialog(@NonNull Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_sc_bottom_sheet_home, null);
        bottomSheetDialog = new CusBottomSheetDialog(context, R.style.BottomSheetDialogTheme);
        bottomSheetDialog.setContentView(view);
        this.context = context;
        init(view);
    }

    protected void init(View view) {
        layout_my_account = view.findViewById(R.id.layout_my_account);
        layout_my_utilities = view.findViewById(R.id.layout_my_utilities);
        layout_my_package = view.findViewById(R.id.layout_my_package);
        layout_usage_history = view.findViewById(R.id.layout_usage_history);
        layout_value = view.findViewById(R.id.layout_value);
        layout_store = view.findViewById(R.id.layout_store);
        layout_connect = view.findViewById(R.id.layout_connect);

        layout_my_account.setOnClickListener(this);
        layout_my_utilities.setOnClickListener(this);
        layout_my_package.setOnClickListener(this);
        layout_usage_history.setOnClickListener(this);
        layout_value.setOnClickListener(this);
        layout_store.setOnClickListener(this);
        layout_connect.setOnClickListener(this);
    }

    public void dismiss() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.dismiss();
            bottomSheetDialog = null;
        }
        layout_my_account = null;
        layout_my_utilities = null;
        layout_my_package = null;
        layout_usage_history = null;
        layout_value = null;
        layout_store = null;
        layout_connect = null;
    }

    public void show() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_my_account:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL, null);
                dismiss();
                break;

            case R.id.layout_my_utilities:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_PACKAGE_LIST, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_UTILITIES));
                dismiss();
                break;

            case R.id.layout_my_package:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_PACKAGE));
                dismiss();
                break;

            case R.id.layout_usage_history:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_ACCOUNT_DETAIL, new SCBundle(1));
                dismiss();
                break;

            case R.id.layout_value:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_RECOMMENT_PACKAGE, new SCBundle(SCConstants.SCREEN_TYPE.TYPE_SERVICES));
                dismiss();
                break;

            case R.id.layout_store:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_STORES, null);
                dismiss();
                break;

            case R.id.layout_connect:
                doSelfCareDetail(SCConstants.SELF_CARE.TAB_MORE, null);
                dismiss();
                break;
        }
    }

    private void doSelfCareDetail(int type, Object data) {
        if (context != null) {
            Intent intent = new Intent(context, TabSelfCareActivity.class);
            intent.putExtra(Constants.KEY_TYPE, type);
            intent.putExtra(Constants.KEY_DATA, (Serializable) data);
            context.startActivity(intent);
        }
    }
}
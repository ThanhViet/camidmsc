package com.metfone.selfcare.module.livestream.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.model.onmedia.TagMocha;
import com.metfone.selfcare.module.livestream.holder.BaseMessageLiveStreamHolder;
import com.metfone.selfcare.module.livestream.holder.MsgNormalLevel2Holder;
import com.metfone.selfcare.module.livestream.listener.MessageActionListener;
import com.metfone.selfcare.module.livestream.model.LiveStreamMessage;

import java.util.ArrayList;

public class MessageLevel2Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private BaseSlidingFragmentActivity activity;
    private ArrayList<LiveStreamMessage> messages;
    private MessageActionListener listener;
    private TagMocha.OnClickTag onClickTag;

    public MessageLevel2Adapter(BaseSlidingFragmentActivity activity, ArrayList<LiveStreamMessage> messages, MessageActionListener listener, TagMocha.OnClickTag onClickTag) {
        this.activity = activity;
        this.messages = messages;
        this.inflater = LayoutInflater.from(activity);
        this.listener = listener;
        this.onClickTag = onClickTag;
    }

    public void setMessages(ArrayList<LiveStreamMessage> messages) {
        this.messages = messages;
    }

    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getType();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.holder_ls_msg, parent, false);
        BaseMessageLiveStreamHolder viewHolder = new MsgNormalLevel2Holder(view, activity, listener, onClickTag);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BaseMessageLiveStreamHolder)
            ((BaseMessageLiveStreamHolder) holder).setElement(messages.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (messages == null || messages.isEmpty())
            return 0;
        return messages.size();
    }

}

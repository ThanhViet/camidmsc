package com.metfone.selfcare.module.selfcare.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metfone.selfcare.R;
import com.metfone.selfcare.module.keeng.base.RecyclingPagerAdapter;
//import com.metfone.selfcare.module.keeng.utils.CrashUtils;
import com.metfone.selfcare.module.selfcare.event.AbsInterface;
import com.metfone.selfcare.module.selfcare.model.SCSubListModel;
import com.metfone.selfcare.module.selfcare.utils.SCConstants;
import com.metfone.selfcare.module.selfcare.utils.SCUtils;

import java.util.ArrayList;
import java.util.List;

public class SCHomeInfoAdapter extends RecyclingPagerAdapter {
    static final String TAG = "SCHomeInfoAdapter";
    private List<SCSubListModel> datas = new ArrayList<>();
    private LayoutInflater inflater;
    private boolean isInfiniteLoop;
    private AbsInterface.OnSCHomeListener onItemListener;
    private Context context;

    public SCHomeInfoAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isInfiniteLoop = false;
    }

    public void setDatas(List<SCSubListModel> itemsList) {
        this.datas = itemsList;
    }

    private int getPosition(int position) {
        return isInfiniteLoop ? position % datas.size() : position;
    }

    public SCSubListModel getItem(int position) {
        try {
            return datas.get(position);
        } catch (Exception e) {
//            CrashUtils.logCrash(TAG, e);
        }
        return null;
    }

    @Override
    public int getCount() {
        return isInfiniteLoop ? Integer.MAX_VALUE : datas.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_sc_home_info, container, false);
            vh = new ViewHolder();
            vh.tvAccount = (TextView) convertView.findViewById(R.id.tvAccount);
            vh.tvPrepaid = (TextView) convertView.findViewById(R.id.tvPrepaid);
            vh.tvMainData = (TextView) convertView.findViewById(R.id.tvMainData);
            vh.tvPromotionData = (TextView) convertView.findViewById(R.id.tvPromotionData);
            vh.tvData = (TextView) convertView.findViewById(R.id.tvData);
            vh.tvMainBalance = (TextView) convertView.findViewById(R.id.tvMainBalance);
            vh.tvPromotionBalance = (TextView) convertView.findViewById(R.id.tvPromotionBalance);
            vh.tvDataBalance = (TextView) convertView.findViewById(R.id.tvDataBalance);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final SCSubListModel item = getItem(position);
        if (item != null) {

            vh.tvAccount.setText(SCUtils.formatPhoneNumber(item.getIsdn()));

            SCSubListModel.OcsAccount ocsAccount = item.getOcsAccount();
            if(ocsAccount != null)
            {
                if(TextUtils.isEmpty(ocsAccount.getProductCode()))
                    vh.tvPrepaid.setText(item.getSubType());
                else
                    vh.tvPrepaid.setText(item.getSubType() + "/" + ocsAccount.getProductCode());

                vh.tvMainData.setText(SCUtils.numberFormat(ocsAccount.getBalance()));
                vh.tvPromotionData.setText(SCUtils.numberFormat(ocsAccount.getPromotion()));
                vh.tvData.setText(SCUtils.numberFormat(ocsAccount.getData()));

                vh.tvMainBalance.setText(SCConstants.SC_CURRENTCY);
                vh.tvPromotionBalance.setText(SCConstants.SC_CURRENTCY);
                vh.tvDataBalance.setText(SCConstants.SC_DATA_CURRENTCY);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onAccountClick();
                    }
                }
            });
        }

        return convertView;
    }

    public void setOnClickListener(AbsInterface.OnSCHomeListener listener) {
        this.onItemListener = listener;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public SCHomeInfoAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }

    static class ViewHolder {
        public TextView tvAccount;
        public TextView tvPrepaid;
        public TextView tvMainData;
        public TextView tvPromotionData;
        public TextView tvData;
        public TextView tvMainBalance;
        public TextView tvPromotionBalance;
        public TextView tvDataBalance;
    }
}


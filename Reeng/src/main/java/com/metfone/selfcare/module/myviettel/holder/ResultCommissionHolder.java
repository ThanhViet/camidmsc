/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/11/5
 *
 */

package com.metfone.selfcare.module.myviettel.holder;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.TextHelper;

import butterknife.BindView;

public class ResultCommissionHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.tv_transaction)
    @Nullable
    TextView tvTransaction;
    @BindView(R.id.tv_commission)
    @Nullable
    TextView tvCommission;

    public ResultCommissionHolder(View view, final Activity activity) {
        super(view);

    }

    @SuppressLint("SetTextI18n")
    public void bindData(long totalTransaction, long totalCommission) {
        if (tvTransaction != null) {
            tvTransaction.setText(totalTransaction + "");
        }
        if (tvCommission != null) {
            tvCommission.setText(TextHelper.formatCurrencyVN(totalCommission) + " đ");
        }
    }

}
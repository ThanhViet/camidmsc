package com.metfone.selfcare.module.home_kh.api.rx;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KHBaseResponse <T> {
    public static final String SUCCESS = "S200";
    @SerializedName("errorCode")
    @Expose
    private String errorCode;

    @SerializedName("errorMessage")
    private String errorMessage;

    @SerializedName("result")
    @Expose
    private T data;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccessful() {
        return SUCCESS.equals(errorCode);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
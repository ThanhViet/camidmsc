package com.metfone.selfcare.module.home_kh.fragment.khmer_new_year_gift;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.metfone.selfcare.R;
import com.metfone.selfcare.databinding.FragmentKhmerNewYearGiftTermConditionBinding;
import com.metfone.selfcare.module.home_kh.activity.KhmerNewYearGiftActivity;
import com.metfone.selfcare.module.keeng.base.BaseFragment;
import com.metfone.selfcare.util.Utilities;

public class KhmerNewYearGiftTermCondition extends BaseFragment {
    public static final String TAG = KhmerNewYearGiftTermCondition.class.getSimpleName();

    private FragmentKhmerNewYearGiftTermConditionBinding binding;

    public static KhmerNewYearGiftTermCondition newInstance() {
        KhmerNewYearGiftTermCondition fragment = new KhmerNewYearGiftTermCondition();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentKhmerNewYearGiftTermConditionBinding.inflate(getLayoutInflater());
        binding.setHandler(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Utilities.reCalculateDimensionAnchorView(binding.anchorPointForFrame,
                binding.container,
                binding.imageOriginalSize,
                binding.anchorPointForFrame);

        binding.termCondition.loadUrl(getString(R.string.term_condition_metfone_gift));
    }

    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public int getResIdView() {
        return 0;
    }

    public void close(){
        ((KhmerNewYearGiftActivity)getActivity()).playTabSound();
        onBackPressed();
    }
}

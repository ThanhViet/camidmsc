package com.metfone.selfcare.module.home_kh.fragment.confirm;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;

public class DialogGetLinkRedeemKH extends DialogFragment {

    private static final String LINK = "LINK";
    private static final String IMAGE = "IMAGE";
    private static final String CODE = "CODE";
    private AppCompatImageView qrCode;
    private AppCompatTextView tvCode,tvCopy,tvGetLink;
    private FrameLayout container;

    public static DialogGetLinkRedeemKH newInstance(String link,String image, String code) {
        DialogGetLinkRedeemKH dialog = new DialogGetLinkRedeemKH();
        Bundle args = new Bundle();
        args.putString(LINK, link);
        args.putString(IMAGE, image);
        args.putString(CODE, code);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            if (dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(true);
        }
        return inflater.inflate(R.layout.fragment_dialog_get_link_redeem, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        qrCode = view.findViewById(R.id.qrcode);
        tvCode = view.findViewById(R.id.tvCode);
        tvCopy = view.findViewById(R.id.tvCopy);
        tvGetLink = view.findViewById(R.id.tvGetLink);
        container = view.findViewById(R.id.container);
        Bundle bundle = getArguments();
        String link;
        if (bundle != null) {
             link = bundle.getString(LINK, "");
            String image = bundle.getString(IMAGE, "");
            String code = bundle.getString(CODE, "");
            tvCode.setText(code);
            Glide.with(qrCode)
                    .load(image)
                    .into(qrCode);
            tvCopy.setOnClickListener(view1 -> {
                ClipboardManager clipboard = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(code, code);
                clipboard.setPrimaryClip(clip);
                com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), getString(R.string.copied));
            });
            tvGetLink.setOnClickListener(view1 -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                startActivity(browserIntent);
            });
        }
        container.setOnClickListener(view1 -> {
            dismiss();
        });

    }

}
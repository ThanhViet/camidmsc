/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/3/19
 */

package com.metfone.selfcare.module.saving.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.R;
import com.metfone.selfcare.helper.DeepLinkHelper;
import com.metfone.selfcare.module.keeng.base.BaseViewHolder;
import com.metfone.selfcare.module.saving.model.Promotion;
import com.metfone.selfcare.module.saving.model.SavingStatisticsModel;

import java.util.List;

public class PromotionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = PromotionAdapter.class.getSimpleName();
    private final int TYPE_EMPTY = 0;
    private final int TYPE_NORMAL = 1;
    private BaseSlidingFragmentActivity activity;
    private List<Promotion> data;

    public PromotionAdapter(BaseSlidingFragmentActivity activity, List<Promotion> data) {
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getItemViewType(int position) {
        Promotion item = getItem(position);
        if (item != null) {
            return TYPE_NORMAL;
        }
        return TYPE_EMPTY;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_NORMAL) {
            view = LayoutInflater.from(activity).inflate(R.layout.holder_promotion_detail, parent, false);
        } else
            view = LayoutInflater.from(activity).inflate(R.layout.holder_empty, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemHolder, int position) {
        final Promotion item = getItem(position);
        int viewType = getItemViewType(position);
        if (item != null && itemHolder instanceof BaseViewHolder) {
            BaseViewHolder holder = (BaseViewHolder) itemHolder;
            if (viewType == TYPE_NORMAL) {
                holder.setText(R.id.tv_title, item.getDesc());
                ImageView ivIcon = holder.getView(R.id.icon);
                if (ivIcon != null) {
                    Glide.with(activity)
                            .load(item.getImageUrl())
                            .apply(new RequestOptions().placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher))
                            .into(ivIcon);
                }
                if (TextUtils.isEmpty(item.getDeeplinkLabel())) {
                    holder.setVisible(R.id.button_action, false);
                } else {
                    holder.setVisible(R.id.button_action, true);
                    holder.setText(R.id.button_action, item.getDeeplinkLabel());
                    holder.setOnClickListener(R.id.button_action, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (activity != null && item != null && !TextUtils.isEmpty(item.getDeeplinkUrl()))
                                DeepLinkHelper.getInstance().openSchemaLink(activity, item.getDeeplinkUrl());
                        }
                    });
                }
            }
        }
    }

    public Promotion getItem(int position) {
        if (data != null && data.size() > position && position >= 0)
            return data.get(position);
        return null;
    }

    @Override
    public int getItemCount() {
        if (data == null || data.isEmpty())
            return 0;
        return data.size();
    }

}

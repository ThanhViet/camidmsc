/*
package com.metfone.selfcare.module.selfcare.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.fragment.call.CallHistoryFragment;
import com.metfone.selfcare.fragment.contact.HomeContactsFragment;
import com.metfone.selfcare.fragment.contact.HomeGroupsFragment;
import com.metfone.selfcare.fragment.message.ThreadListFragmentRecycleView;
import com.metfone.selfcare.fragment.musickeeng.StrangerMusicFragment;
import com.metfone.selfcare.fragment.onmedia.OnMediaHotFragment;
import com.metfone.selfcare.helper.ListenerHelper;
import com.metfone.selfcare.helper.NetworkHelper;
import com.metfone.selfcare.listeners.LoginStateListener;
import com.metfone.selfcare.ui.ProgressLoading;
import com.metfone.selfcare.ui.roundview.RoundTextView;

*/
/**
 * Created by thanhnt72 on 4/23/2019.
 *//*


public class BaseLoginAnonymousFragment extends Fragment implements LoginStateListener {

    private Context mContext;
    private EmptyViewListener mCallBack;
    private View emptyView;

    protected ProgressLoading mPrbLoading;
    private TextView mTvwNote;
    private ImageView mBtnRetry;
    private TextView mTvwRetry1, mTvwRetry2;

    private View viewLogin;
    private RoundTextView btnLogin;
    private ImageView ivThumb;
    private TextView tvLogin;
    private ApplicationController mApp;

    protected void initViewLogin(LayoutInflater inflater, ViewGroup viewGroup, View parent) {
        if (mApp.getReengAccountBusiness().isAnonymousLogin()) {
            viewLogin = parent.findViewById(R.id.llViewLogin);
            ivThumb = parent.findViewById(R.id.ivThumbLogin);
            tvLogin = parent.findViewById(R.id.tvContentLogin);
            btnLogin = viewLogin.findViewById(R.id.btnLogin);


            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseSlidingFragmentActivity activity = (BaseSlidingFragmentActivity) getActivity();
                    if (activity != null) activity.loginFromAnonymous();
                }
            });
            viewLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            */
/*ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            viewGroup.addView(viewLogin, params);*//*

            ListenerHelper.getInstance().addLoginAnonymousListener(this);

            drawDetailAnonymous();
        }
    }

    private void drawDetailAnonymous() {
        if (mApp.getReengAccountBusiness().isAnonymousLogin()) {
            viewLogin.setVisibility(View.VISIBLE);
            if (mApp.getReengAccountBusiness().needAddNumber())
                btnLogin.setText(mContext.getString(R.string.sc_btn_verify_number));
            else
                btnLogin.setText(mContext.getString(R.string.login));
            drawThumbLogin();

            if(this instanceof StrangerMusicFragment){
                btnLogin.setBackgroundColorRound(ContextCompat.getColor(mContext, R.color.home_tab_strangers));
            } else if (this instanceof OnMediaHotFragment){
                btnLogin.setBackgroundColorRound(ContextCompat.getColor(mContext, R.color.home_tab_onmedia));
            } else {
                btnLogin.setBackgroundColorRound(ContextCompat.getColor(mContext, R.color.home_tab_chat));
            }
        }
    }

    private void drawThumbLogin() {
        if (this instanceof ThreadListFragmentRecycleView) {
            tvLogin.setText(mContext.getString(mApp.getReengAccountBusiness().needAddNumber()
                    ? R.string.sc_login_mocha_inbox : R.string.sc_login_inbox));
//            Glide.with(mApp).load(R.drawable.sc_login_inbox).into(ivThumb);
        } else if (this instanceof CallHistoryFragment) {
            tvLogin.setText(mContext.getString(mApp.getReengAccountBusiness().needAddNumber()
                    ? R.string.sc_login_mocha_call : R.string.sc_login_call));
//            Glide.with(mApp).load(R.drawable.sc_login_call).into(ivThumb);
        } else if (this instanceof HomeContactsFragment) {
            tvLogin.setText(mContext.getString(mApp.getReengAccountBusiness().needAddNumber()
                    ? R.string.sc_login_mocha_contact : R.string.sc_login_contact));
//            Glide.with(mApp).load(R.drawable.sc_login_contact).into(ivThumb);
        } else if (this instanceof HomeGroupsFragment) {
            tvLogin.setText(mContext.getString(mApp.getReengAccountBusiness().needAddNumber()
                    ? R.string.sc_login_mocha_group : R.string.sc_login_group));
//            Glide.with(mApp).load(R.drawable.sc_login_group).into(ivThumb);
        } else if (this instanceof StrangerMusicFragment) {
            tvLogin.setText(mContext.getString(mApp.getReengAccountBusiness().needAddNumber()
                    ? R.string.sc_login_mocha_stranger : R.string.sc_login_stranger));
//            Glide.with(mApp).load(R.drawable.sc_login_inbox).into(ivThumb);
        } else if (this instanceof OnMediaHotFragment) {
            tvLogin.setText(mContext.getString(mApp.getReengAccountBusiness().needAddNumber()
                    ? R.string.sc_login_mocha_onmedia : R.string.sc_login_onmedia));
//            Glide.with(mApp).load(R.drawable.sc_login_onmedia).into(ivThumb);
        }
        Glide.with(mApp).load(mApp.getReengAccountBusiness().needAddNumber()
                ? R.drawable.sc_thumb_verify : R.drawable.sc_thumb_login).into(ivThumb);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mApp = ApplicationController.self();
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        drawDetailAnonymous();
    }

    @Override
    public void onDestroyView() {
        ListenerHelper.getInstance().removeLoginAnonymousListener(this);
        super.onDestroyView();
    }

    */
/*@Override
    public void onLoginSuccess() {
        if (viewLogin != null)
            viewLogin.setVisibility(View.GONE);
    }*//*



    protected void createView(LayoutInflater inflater, RecyclerView recyclerView, EmptyViewListener callBack) {
        this.mCallBack = callBack;
        emptyView = inflater.inflate(R.layout.view_empty, null, false);
        mPrbLoading = (ProgressLoading) emptyView.findViewById(R.id.empty_progress);
        mTvwNote = (TextView) emptyView.findViewById(R.id.empty_text);
        mBtnRetry = (ImageView) emptyView.findViewById(R.id.empty_retry_button);
        mTvwRetry1 = (TextView) emptyView.findViewById(R.id.empty_retry_text1);
        mTvwRetry2 = (TextView) emptyView.findViewById(R.id.empty_retry_text2);
        ViewGroup viewGroup = (ViewGroup) recyclerView.getParent();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(emptyView, params);
        //container.addView(emptyView, 0)
        mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) {
                    showProgressLoading();
                    mCallBack.onRetryClick();
                }
            }
        });
        //hideEmptyView();
        showProgressLoading();
    }

    protected void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    protected void showEmptyNote(String content) {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.VISIBLE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
        mTvwNote.setText(content);
    }

    protected void showEmptyNote() {
        showEmptyNote(mContext.getResources().getString(R.string.list_empty));
    }

    protected void showProgressLoading() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.VISIBLE);
        mTvwNote.setVisibility(View.GONE);
        mBtnRetry.setVisibility(View.GONE);
        mTvwRetry1.setVisibility(View.GONE);
        mTvwRetry2.setVisibility(View.GONE);
    }

    protected boolean isShowProgressLoading() {
        if (emptyView == null || mPrbLoading == null) return false;
        return emptyView.getVisibility() == View.VISIBLE && mPrbLoading.getVisibility() == View.VISIBLE;
    }

    protected void showRetryView() {
        emptyView.setVisibility(View.VISIBLE);
        mPrbLoading.setVisibility(View.GONE);
        mTvwNote.setVisibility(View.GONE);
        mBtnRetry.setVisibility(View.VISIBLE);
        mTvwRetry1.setVisibility(View.VISIBLE);
        mTvwRetry2.setVisibility(View.VISIBLE);
    }

    protected void showEmptyOrRetryView() {
        if (NetworkHelper.isConnectInternet(mContext)) {
            showEmptyNote(mContext.getResources().getString(R.string.list_empty));
        } else {
            showRetryView();
        }
    }

    @Override
    public void onLoginSuccess(String s) {
        if (viewLogin != null)
            viewLogin.setVisibility(View.GONE);
    }

    @Override
    public void onLoginError(String s) {

    }

    public interface EmptyViewListener {
        void onRetryClick();
    }
}
*/

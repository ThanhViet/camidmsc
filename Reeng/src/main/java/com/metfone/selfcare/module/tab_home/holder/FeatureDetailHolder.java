/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2019/7/27
 *
 */

package com.metfone.selfcare.module.tab_home.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.adapter.BaseAdapter;
import com.metfone.selfcare.R;
import com.metfone.selfcare.listeners.OnSingleClickListener;
import com.metfone.selfcare.model.home.ItemMoreHome;
import com.metfone.selfcare.module.tab_home.listener.TabHomeListener;
import com.metfone.selfcare.module.tab_home.utils.ImageBusiness;
import com.metfone.selfcare.module.tab_home.utils.TabHomeUtils;

import butterknife.BindView;

public class FeatureDetailHolder extends BaseAdapter.ViewHolder {

    @BindView(R.id.iv_cover_01)
    ImageView ivCover01;
    @BindView(R.id.tv_title_01)
    TextView tvTitle01;
    @BindView(R.id.layout_line_01)
    View viewFeature01;

    private TabHomeListener.OnAdapterClick listener;

    public FeatureDetailHolder(View view, TabHomeListener.OnAdapterClick listener) {
        super(view);
        ViewGroup.LayoutParams layoutParams = viewFeature01.getLayoutParams();
        layoutParams.width = TabHomeUtils.getWidthFeature();
        viewFeature01.setLayoutParams(layoutParams);
        viewFeature01.requestLayout();
        this.listener = listener;
    }

    @Override
    public void bindData(Object item, int position) {
        if (item instanceof ItemMoreHome) {
            final ItemMoreHome model = (ItemMoreHome) item;
            if (viewFeature01 != null) {
                viewFeature01.setVisibility(View.VISIBLE);
                viewFeature01.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View view) {
                        if (listener != null)
                            listener.onClickFeature(model, position);
                    }
                });
            }
            tvTitle01.setText(model.getTitle());
            ImageBusiness.setFeature(ivCover01, model.getImgUrl(), model.getResId());
        }
    }
}

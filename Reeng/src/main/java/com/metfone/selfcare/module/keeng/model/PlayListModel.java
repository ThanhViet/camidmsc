package com.metfone.selfcare.module.keeng.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.module.keeng.utils.Utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayListModel implements Serializable {
    private static final long serialVersionUID = 2916016752480434166L;

    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name = "";
    @SerializedName("listen_no")
    public long listen_no = 0;
    @SerializedName("type")
    public int type;
    @SerializedName("url")
    public String url;
    @SerializedName("identify")
    private String identify = "";
    @SerializedName("list_media_item")
    private List<AllModel> mediaItemsList;

    @SerializedName("list_item")
    private List<AllModel> mediasList;

    @SerializedName("user")
    private UserInfo mUser;

    @SerializedName("user_id")
    private long userId = 0;

    @SerializedName("is_playlist_video")
    private int isVideoList = 0;

    @SerializedName("total_media")
    private int totalMedia = 0;

    private List<String> listAvatar;
    private String creator;
    private String cover;
    private boolean isFavorite = false;
    private int source = 0;
    private String coverUrl = "";

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getListenNo() {
        return Utilities.formatNumber(listen_no);
    }

    public void setListenNo(long listen_no) {
        this.listen_no = listen_no;
    }

    public long getListened() {
        return listen_no;
    }

    public boolean isVideoList() {
        return isVideoList == 1 || type == Constants.TYPE_PLAYLIST_VIDEO;
    }

    public String getNameUser() {
        String name = "";
        if (mUser != null) {
            name = mUser.getNameUser();
        }
        return name;
    }

    public long getUserId() {
        if (mUser != null)
            return mUser.getId();

        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getSinger() {
        StringBuilder str = new StringBuilder();
        if (mediaItemsList != null) {
            for (int i = 0, size = mediaItemsList.size(); i < size; i++) {
                AllModel media = mediaItemsList.get(i);
                if (media != null && !TextUtils.isEmpty(media.getSinger()) && !str.toString().contains(media.getSinger())) {
                    str.append(" | ").append(media.getSinger());
                }
            }
        } else if (mediasList != null) {
            for (int i = 0, size = mediasList.size(); i < size; i++) {
                AllModel media = mediasList.get(i);
                if (media != null && !TextUtils.isEmpty(media.getSinger()) && !str.toString().contains(media.getSinger())) {
                    str.append(" | ").append(media.getSinger());
                }
            }
        }
        if (str.length() > 3)
            str = new StringBuilder(str.substring(3));
        return str.toString();
    }

    public List<String> getListAvatar() {
        if (listAvatar == null)
            listAvatar = new ArrayList<>();
        else if (listAvatar.size() > 0)
            return listAvatar;

        coverUrl = "";
        if (mediaItemsList != null && !mediaItemsList.isEmpty()) {
            for (int i = 0; i < mediaItemsList.size(); i++) {
                AllModel item = mediaItemsList.get(i);
                if (item != null) {
                    if (TextUtils.isEmpty(coverUrl))
                        coverUrl = item.getImageCover();
                    String image = item.getImage310();
                    if (item.getType() == Constants.TYPE_VIDEO && item.getInfoExtra() != null && !item.getInfoExtra().getListImages().isEmpty()) {
                        image = Utilities.getRandomList(item.getInfoExtra().getListImages());
                    }
                    if (checkUrlImage(listAvatar, image)) {
                        listAvatar.add(image);
                    }
                    if (listAvatar.size() >= 5)
                        break;
                }
            }
        } else if (mediasList != null && !mediasList.isEmpty()) {
            for (int i = 0; i < mediasList.size(); i++) {
                AllModel item = mediasList.get(i);
                if (item != null) {
                    if (TextUtils.isEmpty(coverUrl))
                        coverUrl = item.getImageCover();
                    String image = item.getImage310();
                    if (item.getType() == Constants.TYPE_VIDEO && item.getInfoExtra() != null && !item.getInfoExtra().getListImages().isEmpty()) {
                        image = Utilities.getRandomList(item.getInfoExtra().getListImages());
                    }
                    if (checkUrlImage(listAvatar, image)) {
                        listAvatar.add(image);
                    }
                    if (listAvatar.size() >= 5)
                        break;
                }
            }
        }
        return listAvatar;
    }

    public void setListAvatar(List<String> listAvatar) {
        this.listAvatar = listAvatar;
    }

    public List<String> getListAvatar(boolean isRefresh) {
        if (isRefresh) {
            if (listAvatar == null) listAvatar = new ArrayList<>();
            else listAvatar.clear();
        }
        return getListAvatar();
    }

    private boolean checkUrlImage(List<String> data, String url) {
        if (TextUtils.isEmpty(url))
            return false;

        if (data == null || data.isEmpty())
            return true;

        for (int i = 0; i < data.size(); i++) {
            if (url.equals(data.get(i)))
                return false;
        }
        return true;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        if (name == null)
            name = "";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public UserInfo getUser() {
        return mUser;
    }

    public void setUser(UserInfo mUser) {
        this.mUser = mUser;
    }

    public List<AllModel> getMediaList() {
        if (mediasList != null) return mediasList;
        if (mediaItemsList == null)
            mediaItemsList = new ArrayList<>();
        return mediaItemsList;
    }

    public void setMediaList(List<AllModel> list) {
        if (list == null) {
            this.mediaItemsList = list;
            this.mediasList = list;
        } else {
            if (this.mediasList != null) {
                this.mediasList = list;
                this.mediaItemsList = null;
            } else {
                this.mediaItemsList = list;
            }
        }
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getTotalMedia() {
        if (totalMedia > 0)
            return totalMedia;
        return getMediaList().size();
    }

    public void setTotalMedia(int totalMedia) {
        this.totalMedia = totalMedia;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", mediaItemsList=" + mediaItemsList +
                ", mediasList=" + mediasList +
                '}';
    }
}

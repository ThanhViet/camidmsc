package com.metfone.selfcare.module.newdetails.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FavouriteModel implements Serializable {
    private List<String> data = new ArrayList<>();

    public List<String> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}

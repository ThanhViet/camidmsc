/*
 * Copyright (c) https://bigzun.blogspot.com/
 * Email: bigzun.com@gmail.com
 * Created by namnh40 on 2020/2/4
 *
 */

package com.metfone.selfcare.module.home_kh.tab.adapter.events;

import com.metfone.selfcare.listeners.OnClickContentChannel;
import com.metfone.selfcare.listeners.OnClickContentComic;
import com.metfone.selfcare.listeners.OnClickContentMovie;
import com.metfone.selfcare.listeners.OnClickContentMusic;
import com.metfone.selfcare.listeners.OnClickContentNews;
import com.metfone.selfcare.listeners.OnClickContentTiin;
import com.metfone.selfcare.listeners.OnClickContentVideo;
import com.metfone.selfcare.listeners.OnClickSliderBanner;
import com.metfone.selfcare.module.tab_home.model.TabHomeModel;

public class KhTabHomeListener {

    public interface OnAdapterClick extends OnClickContentVideo, OnClickContentMovie, OnClickContentMusic
            , OnClickContentNews, OnClickContentComic, OnClickContentChannel, OnClickSliderBanner
            , OnClickContentTiin {


        void onClickTitleBox(TabHomeModel item, int position);
    }
}

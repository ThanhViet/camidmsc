package com.metfone.selfcare.module.tiin.activitytiin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.metfone.selfcare.activity.BackupActivity;
import com.metfone.selfcare.activity.BaseSlidingFragmentActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.R;
import com.metfone.selfcare.database.constant.ThreadMessageConstant;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.helper.NavigateActivityHelper;
import com.metfone.selfcare.helper.TextHelper;
import com.metfone.selfcare.model.tab_video.Video;
import com.metfone.selfcare.module.keeng.utils.ConvertHelper;
import com.metfone.selfcare.module.netnews.bottomsheet.BottomSheetAdapter;
import com.metfone.selfcare.module.netnews.bottomsheet.BottomSheetData;
import com.metfone.selfcare.module.newdetails.interfaces.AbsInterface;
import com.metfone.selfcare.module.newdetails.model.BottomSheetModel;
import com.metfone.selfcare.module.newdetails.model.NewsModel;
import com.metfone.selfcare.module.newdetails.utils.CommonUtils;
import com.metfone.selfcare.module.newdetails.utils.SharedPref;
import com.metfone.selfcare.module.newdetails.utils.ToastUtils;
import com.metfone.selfcare.module.tiin.ConstantTiin;
import com.metfone.selfcare.module.tiin.base.BaseFragment;
import com.metfone.selfcare.module.tiin.base.MvpView;
import com.metfone.selfcare.module.tiin.category.fragment.CategoryTiinFragment;
import com.metfone.selfcare.module.tiin.categorynow.fragment.CategoryNowTiinFragment;
import com.metfone.selfcare.module.tiin.configtiin.fragment.ConfigTiinFragment;
import com.metfone.selfcare.module.tiin.detailtiin.maindetailtiin.fragment.MainTiinDetailFragment;
import com.metfone.selfcare.module.tiin.maintiin.fragment.TabTiinFragment;
import com.metfone.selfcare.module.tiin.network.model.HomeTiinModel;
import com.metfone.selfcare.module.tiin.network.model.TiinModel;
import com.metfone.selfcare.ui.glide.ImageLoader;
import com.metfone.selfcare.util.Log;

import java.util.List;
import java.util.Stack;

public class TiinActivity extends BaseSlidingFragmentActivity implements MvpView, AbsInterface.OnClickBottomSheet {
    private final String TAG = BackupActivity.class.getSimpleName();
    public BaseFragment currentFragment = null;
    public BaseFragment originalFragment = null;
    SharedPref mPref;
    ApplicationController mApplication;
    Stack<MainTiinDetailFragment> mNewsDetailFragmentStack = new Stack<>();
    boolean isShowToast;
    //Xu ly bottom sheet
    BottomSheetDialog mBottomSheet;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        mApplication = (ApplicationController) getApplicationContext();
        mPref = new SharedPref(this);

        setUp(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void setUp(Intent intent) {
        int tab = intent.getIntExtra(ConstantTiin.KEY_TAB, -1);
        String title = intent.getStringExtra(ConstantTiin.KEY_TITLE);
        int categoryId = intent.getIntExtra(ConstantTiin.KEY_CATEGORY, -1);
        String hashTag = intent.getStringExtra(ConstantTiin.KEY_HASHTAG);
        HomeTiinModel model = (HomeTiinModel) intent.getSerializableExtra(ConstantTiin.KEY_EVENT_DATA);

        Bundle bundle = new Bundle();
        bundle.putString(ConstantTiin.KEY_TITLE, title);
        bundle.putInt(ConstantTiin.KEY_CATEGORY, categoryId);
        bundle.putString(ConstantTiin.KEY_HASHTAG, hashTag);
        if (model != null) {
            bundle.putSerializable(ConstantTiin.KEY_EVENT_DATA, model);
        }
        switch (tab) {
            case ConstantTiin.TAB_CATEGORY:
                showFragment(ConstantTiin.TAB_CATEGORY, bundle, false);
                break;

            case ConstantTiin.TAB_CONFIG:
                showFragment(ConstantTiin.TAB_CONFIG, bundle, false);
                break;

            case ConstantTiin.TAB_CATEGORY_NOW:
                showFragment(ConstantTiin.TAB_CATEGORY_NOW, bundle, false);
                break;

            default:
                break;
        }

    }

    public void showFragment(int tabId, Bundle bundle, boolean enterAnimation) {
        switch (tabId) {
            case ConstantTiin.TAB_CATEGORY:
                currentFragment = CategoryTiinFragment.newInstance(bundle);
                break;
            case ConstantTiin.TAB_CATEGORY_NOW:
                currentFragment = CategoryNowTiinFragment.newInstance(bundle);
                break;
            case ConstantTiin.TAB_CONFIG:
                currentFragment = ConfigTiinFragment.newInstance();
                break;

        }

        if (currentFragment != null) {
            if (!getSupportFragmentManager().getFragments().contains(currentFragment)) {
                try {
                    if (!currentFragment.isAdded()) {
                        if (currentFragment.getArguments() == null) {
                            currentFragment.setArguments(bundle);
                        } else {
                            currentFragment.getArguments().putAll(bundle);
                        }
                        getSupportFragmentManager()
                                .beginTransaction()
                                .disallowAddToBackStack()
                                .setCustomAnimations(enterAnimation ? R.anim.fragment_slide_left : R.anim.activity_right_to_left_enter, R.anim.fragment_slide_right)
                                .add(R.id.fragment_container, currentFragment)
                                .commitAllowingStateLoss();

                        if (originalFragment == null) {
                            originalFragment = currentFragment;
                        }
                    }
                } catch (IllegalStateException e) {
                    Log.e(TAG, "showFragment", e);
                } catch (RuntimeException e) {
                    Log.e(TAG, "showFragment", e);

                } catch (Exception e) {
                    Log.e(TAG, "showFragment", e);
                }
            }
        }
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void onFragmentDetached(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                    .remove(fragment)
                    .commit();
        }

    }

//    @Override
//    public void onBackPressed() {
//        goPrevTab();
//    }


    @Override
    public void onBackPressed() {
        if(currentFragment  instanceof ConfigTiinFragment) {
            ((ConfigTiinFragment) currentFragment).onClickBack();
            return;
        }
        super.onBackPressed();
    }

    public void goPrevTab() {
        try {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            int entryCount = mFragmentManager.getFragments().size();
            if (entryCount > 0) {
                for (int i = entryCount - 1; i >= 0; i--) {
                    Fragment fragment = mFragmentManager.getFragments().get(i);
                    if (fragment != null) {
                        if (originalFragment != null && fragment == originalFragment) {
                            if (fragment instanceof ConfigTiinFragment) {
                                ((ConfigTiinFragment) fragment).onClickBack();
                            } else {
                                finish();
                            }
                            break;
                        } else {
                            mNewsDetailFragmentStack.remove(fragment);
                            onFragmentDetached(fragment);
                            try {
                                if (i > 0) {
                                    for (int j = i - 1; j >= 0; j--) {
                                        Fragment fragmentPrev = mFragmentManager.getFragments().get(j);

                                        if (fragmentPrev instanceof BaseFragment) {
                                            currentFragment = (BaseFragment) fragmentPrev;
                                        }
                                        break;
                                    }
                                }
                            } catch (Exception e) {
//                                Log.e(TAG, e);
                                Log.e(TAG, "goPrevTab", e);

                            }
//                            break;
                        }
                    }
                }
            } else {
                finish();
            }
        } catch (Exception e) {
            Log.e(TAG, "goPrevTab", e);

        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
////            onBackPressed();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    protected void onDestroy() {
        currentFragment = null;
        originalFragment = null;
        super.onDestroy();
    }

    private void dismissBottomSheetFirst() {
        if (mBottomSheet != null)
            mBottomSheet.dismiss();
    }

    public void showPopupMore(List<BottomSheetModel> datas, final NewsModel item) {
        dismissBottomSheetFirst();
        if (item == null || datas == null || datas.isEmpty())
            return;
        mBottomSheet = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
        TextView tvSinger = sheetView.findViewById(R.id.tvSinger);
        ImageView image = sheetView.findViewById(R.id.image);
        RecyclerView mRecycler = sheetView.findViewById(R.id.recycler);
        ImageView btnFacebook = sheetView.findViewById(R.id.btnFacebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String linkShareFB = CommonUtils.DOMAIN + item.getUrl();
                if (linkShareFB.contains("?alias=app"))
                    linkShareFB = linkShareFB.replace("?alias=app", "");
                CommonUtils.shareFB(TiinActivity.this, linkShareFB, null);
                //AppTracker.sendEvent("V3_MORE_CLICK", "SHARE FACEBOOK", item.getTitle());
            }
        });

        tvTitle.setText(item.getTitle());
        tvSinger.setText(item.getCategory());
        ImageLoader.setImage(this, item.getImage(), image);

        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        BottomSheetAdapter adapter = new BottomSheetAdapter(this, datas, this);
        mRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        mBottomSheet.setContentView(sheetView);
        mBottomSheet.show();
    }

    @Override
    public void onClickSheet(BottomSheetModel item) {
        if (item == null) {
            return;
        }
        switch (item.getType()) {

            case BottomSheetData.COPY_LINK:
                String link = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                if (link.contains("?alias=app"))
                    link = link.replace("?alias=app", "");
                TextHelper.copyToClipboard(this, link);
                ToastUtils.makeText(this, getResources().getString(R.string.copy_to_clipboard));
                //AppTracker.sendEvent("V3_MORE_CLICK", "COPY LINK", item.getTitle());
                break;
            /*case BottomSheetData.READ_ORIGINAL_NEWS:
                try {
                    String linkWebView = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                    if (linkWebView.contains("?alias=app"))
                        linkWebView = linkWebView.replace("?alias=app", "");

                    new FinestWebView.Builder(this).titleDefault("NetNews")
                            .showSwipeRefreshLayout(false)
                            .webViewJavaScriptEnabled(true)
                            .show(linkWebView);
                } catch (Exception ex) {
                    Log.e(TAG, "READ_ORIGINAL_NEWS", ex);

                }
                break;*/
            case BottomSheetData.READ_NEWS:
//                readTiin(item.getNewsModel());

                //AppTracker.sendEvent("V3_READ_NEWS", "MORE RADIO", item.getTitle());
                break;

            case BottomSheetData.SHARE:
                String linkShare = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                if (linkShare.contains("?alias=app"))
                    linkShare = linkShare.replace("?alias=app", "");
                CommonUtils.share(this, linkShare);
                //AppTracker.sendEvent("V3_MORE_CLICK", "SHARE", item.getTitle());
                break;
            case BottomSheetData.SHARE_FACEBOOK:
                String linkShareFB = CommonUtils.DOMAIN + item.getNewsModel().getUrl();
                if (linkShareFB.contains("?alias=app"))
                    linkShareFB = linkShareFB.replace("?alias=app", "");
                CommonUtils.shareFB(this, linkShareFB, null);
                //AppTracker.sendEvent("V3_MORE_CLICK", "SHARE FACEBOOK", item.getTitle());
                break;
        }

        dismissBottomSheetFirst();
    }

    @Override
    public boolean isNetworkConnected() {
        return CommonUtils.isConnected(getApplicationContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        //EventBus.getDefault().register(this);
        //AppTracker.sendEvent("V3_VISIT", "VISIT_APP", "");
    }

    @Override
    protected void onStop() {
        super.onStop();
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void processItemClick(TiinModel model) {
        if (model != null) {
            if (model.getPid() == 94 /*Video*/) {
                playVideo(model);
            } else {
                readTiin(model);

            }
        }
    }

    private void playVideo(TiinModel data) {
        Video videoModel = ConvertHelper.convertTiinToVideoMocha(data);
        ApplicationController.self().getApplicationComponent().providesUtils().openVideoDetail(this, videoModel);
    }

    public void readTiin(TiinModel model) {
        Intent intent = new Intent(this, TiinDetailActivity.class);
        intent.putExtra(ConstantTiin.INTENT_MODULE, model);
        startActivity(intent);
    }

    public boolean isShowToast() {
        return isShowToast;
    }

    public void setShowToast(boolean showToast) {
        isShowToast = showToast;
    }

    private void clearBaseFragmentStack() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            while (!mNewsDetailFragmentStack.isEmpty()) {
                Fragment fragment = mNewsDetailFragmentStack.pop();
                if (fragment != null) {
                    transaction
                            .disallowAddToBackStack()
                            .setCustomAnimations(R.anim.fragment_slide_left, R.anim.fragment_slide_right)
                            .remove(fragment);
                }
            }
            transaction.commit();
            int entryCount = fragmentManager.getFragments().size();
            for (int j = entryCount - 1; j >= 0; j--) {
                Fragment fragmentPrev = fragmentManager.getFragments().get(j);
                if (fragmentPrev instanceof TabTiinFragment) {
                    currentFragment = (BaseFragment) fragmentPrev;
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "clearBaseFragmentStack", e);
        }
    }

    public BaseFragment getCurrentFragment() {
        return currentFragment;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length == 0) {
            Log.i(TAG, "onRequestPermissionsResult - permissions is null or size 0");
            return;
        }

        if (grantResults.length == 0) {
            Log.i(TAG, "onRequestPermissionsResult - grantResults is null or size 0");
            return;
        }

        switch (requestCode) {
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.clear();
        }
    }

    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            int threadId;
            switch (requestCode) {
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_GROUP:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_BROADCAST:
                case Constants.CHOOSE_CONTACT.TYPE_CREATE_CHAT:
                    threadId = data.getIntExtra(Constants.CHOOSE_CONTACT.RESULT_THREAD_ID, -1);
                    NavigateActivityHelper.navigateToChatDetail(this, threadId, ThreadMessageConstant
                            .TYPE_THREAD_GROUP_CHAT);
                    finish();
                    break;
                default:
                    break;
            }
        }
    }
}


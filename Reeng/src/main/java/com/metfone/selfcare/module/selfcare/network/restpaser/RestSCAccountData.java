package com.metfone.selfcare.module.selfcare.network.restpaser;

import com.google.gson.annotations.SerializedName;
import com.metfone.selfcare.module.selfcare.model.SCAccountDataInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class RestSCAccountData extends AbsResultData implements Serializable {

    private static final long serialVersionUID = 8409606354873458054L;

    @SerializedName("result")
    private ArrayList<SCAccountDataInfo> data;

    public ArrayList<SCAccountDataInfo> getData() {
        return data;
    }

    public void setData(ArrayList<SCAccountDataInfo> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestSCAccountData [data=" + data + "] errror " + getErrorCode();
    }
}

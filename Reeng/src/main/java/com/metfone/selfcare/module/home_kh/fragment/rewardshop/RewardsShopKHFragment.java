package com.metfone.selfcare.module.home_kh.fragment.rewardshop;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.StringUtils;
import com.metfone.selfcare.R;
import com.metfone.selfcare.activity.HomeActivity;
import com.metfone.selfcare.app.dev.ApplicationController;
import com.metfone.selfcare.business.UserInfoBusiness;
import com.metfone.selfcare.helper.Constants;
import com.metfone.selfcare.model.account.UserInfo;
import com.metfone.selfcare.module.home_kh.api.KhHomeClient;
import com.metfone.selfcare.module.home_kh.api.response.AccountPointRankResponse;
import com.metfone.selfcare.module.home_kh.api.response.AccountRankInfoResponse;
import com.metfone.selfcare.module.home_kh.api.response.WsGetAllPartnerGiftSearchResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KHBaseResponse;
import com.metfone.selfcare.module.home_kh.api.rx.KhApiCallback;
import com.metfone.selfcare.module.home_kh.fragment.RewardBaseFragment;
import com.metfone.selfcare.module.home_kh.fragment.history.HistoryPointContainerFragment;
import com.metfone.selfcare.module.home_kh.fragment.reward.RewardCamIdKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardsdetail.RewardsDetailKHFragment;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.adapter.RewardsShopAdapter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.ReloadBottomSpace;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.SelectedFilter;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.event.UpdatePoint;
import com.metfone.selfcare.module.home_kh.fragment.rewardshop.model.CategoryFilter;
import com.metfone.selfcare.module.home_kh.model.AccountRankDTO;
import com.metfone.selfcare.module.home_kh.model.GiftItems;
import com.metfone.selfcare.module.home_kh.model.KHAccountPoint;
import com.metfone.selfcare.module.home_kh.model.PartnerGiftSearch;
import com.metfone.selfcare.module.home_kh.model.PointType;
import com.metfone.selfcare.module.home_kh.tab.model.KhUserRank;
import com.metfone.selfcare.module.home_kh.util.ResourceUtils;
import com.metfone.selfcare.util.Log;
import com.metfone.selfcare.util.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;

//0882989999 / 123456
public class RewardsShopKHFragment extends RewardBaseFragment implements Serializable {
    public static final String TAG = RewardsShopKHFragment.class.getSimpleName();
    private Unbinder unbinder;

    private int positionCategory;
    private int positionFilter;
    public static final int CATEGORY = 0;
    public static final int FILTER = 1;
    public static final String KEY_SELECTED_CATEGORY_POSITION = "KEY_SELECTED_CATEGORY_POSITION";
    public static final String KEY_SELECTED_CATEGORY_ID = "KEY_SELECTED_CATEGORY_ID";
    public static final String KEY_SHOW_BOTTOM = "KEY_SHOW_BOTTOM";
    private KhHomeClient homeKhClient;
    private AccountRankDTO accountRankDTO;
    private String categoryName = "All";
    public static String TYPE_ALL = "All";
    private List<GiftItems> listGifts = new ArrayList<>();
    private List<CategoryFilter> listCategory = new ArrayList<>();
    List<PartnerGiftSearch> listPartnerGift = new ArrayList<>();
    private String mSelectedCategoryPositionFromHomeTab = null;
    private String categoryID = null;
    private boolean needShowSpaceBottom = true;

    public static RewardsShopKHFragment newInstance(String data,boolean isId) {
        RewardsShopKHFragment fragment = new RewardsShopKHFragment();
        Bundle args = new Bundle();
        if(!isId){
            args.putString(KEY_SELECTED_CATEGORY_POSITION, data);
            args.putBoolean(KEY_SHOW_BOTTOM,true);
        }else{// from deeplink
            args.putString(KEY_SELECTED_CATEGORY_ID, data);
            args.putBoolean(KEY_SHOW_BOTTOM,false);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.category_value)
    AppCompatTextView filterCategory;

    @BindView(R.id.filter_value)
    AppCompatTextView filterValue;

    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtTitleToolbar;

    @BindView(R.id.tv_point)
    AppCompatTextView tvPoint;

    @BindView(R.id.iconRank)
    AppCompatImageView iconRank;

    @BindView(R.id.tv_name)
    AppCompatTextView tvName;

    @BindView(R.id.rv)
    RecyclerView rvGift;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.space_bottom)
    View space_bottom;

    private RewardsShopAdapter rewardsShopAdapter;

    @Override
    protected boolean needShowBottomMenu() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getParentFragmentManager();
        categoryName = getContext().getString(R.string.all);
        TYPE_ALL = getContext().getString(R.string.all);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mSelectedCategoryPositionFromHomeTab = bundle.getString(KEY_SELECTED_CATEGORY_POSITION);
            categoryID = bundle.getString(KEY_SELECTED_CATEGORY_ID);
            needShowSpaceBottom = bundle.getBoolean(KEY_SHOW_BOTTOM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        if (space_bottom != null && needShowSpaceBottom) {
            if(!fromFilter()){
                ViewGroup.LayoutParams params = space_bottom.getLayoutParams();
                params.height = getNavigationBarHeight();
                space_bottom.setLayoutParams(params);
                space_bottom.setVisibility(View.VISIBLE);
                Log.d(TAG, "Height : " + params.height);
            }
        } else {
            Utilities.adaptViewForInsertBottom(view);
        }
        return view;
    }
    private boolean fromFilter(){
        View v = getActivity().findViewById(R.id.spaceOfFilter);
        if(v != null && v.getHeight() > 0){
            return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvPoint.setText(String.format(getString(R.string.reward_item_point), "0"));

        UserInfoBusiness userInfoBusiness = new UserInfoBusiness(getActivity());
        UserInfo currentUser = userInfoBusiness.getUser();
        if (currentUser != null) {
            String name = currentUser.getFull_name();
            if (name != null) {
                tvName.setText(name);
            }
        }
        initData();
        wsGetAccountPointInfo();

        accountRankDTO = ApplicationController.self().getAccountRankDTO();
        if (accountRankDTO == null) {
            accountRankDTO = new AccountRankDTO();
            accountRankDTO.rankId = 1;
        }
        initRankInfo(accountRankDTO, false);
        if(homeKhClient == null){
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.logApp(Constants.LOG_APP.REWARD_SHOP);
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public int getResIdView() {
        return R.layout.fragment_reward_shop;
    }


    @OnClick(R.id.icBackToolbar)
    void onBack() {
        if(getActivity() instanceof HomeActivity){
            popBackStackFragment();
        }else{
            getActivity().finish();
        }
    }

    private void initData() {
        txtTitleToolbar.setText(ResourceUtils.getString(R.string.rewards_shop));
        initRv();
    }

    private void initRv() {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        rewardsShopAdapter = new RewardsShopAdapter(listGifts, new RewardsShopAdapter.IGiftsItemClick() {
            @Override
            public void itemClick(int position) {
                showDetail(rewardsShopAdapter.getData().get(position));
            }
        });
        LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(activity,
                LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration divider = new DividerItemDecoration(activity, DividerItemDecoration.VERTICAL);
        Drawable mDivider = ContextCompat.getDrawable(activity, R.drawable.reward_divider);
        divider.setDrawable(mDivider);
        rvGift.setLayoutManager(verticalLayoutManager);
        rvGift.addItemDecoration(divider);
        rvGift.setAdapter(rewardsShopAdapter);
    }

    private void showDetail(GiftItems giftItems) {
        replaceFragment(R.id.container_rewards_shop, RewardsDetailKHFragment.newInstance(giftItems), RewardsDetailKHFragment.TAG);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void hideBottomSpace(ReloadBottomSpace showBottomSpace) {
        if (space_bottom != null) {
            space_bottom.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onSelectedFilter(final SelectedFilter item) {
        if (item != null) {
            if (item.getType() == FILTER) {
                categoryID = "";
                positionFilter = item.getPositionSelected();
                filterValue.setText(item.getFilterItems().getName());
                rewardsShopAdapter.clearData();
                getDataCategory();
            } else if (item.getType() == CATEGORY) {
                categoryID = "";
                categoryName = item.getFilterItems().getName();
                positionCategory = item.getPositionSelected();
                filterCategory.setText(item.getFilterItems().getName());
                showDataRv();
            }
            EventBus.getDefault().removeStickyEvent(item);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void eventUpdatePoint(final UpdatePoint item) {
        if (item != null) {
//            getRankAccount(true);
            initRankInfo(accountRankDTO, true);
            wsGetAccountPointInfo();
            EventBus.getDefault().removeStickyEvent(item);
        }
    }

    @OnClick(R.id.layout_filter)
    void showFilter() {
        showFilter(FILTER, positionFilter);
    }

    @OnClick(R.id.layout_category)
    void showCategory() {
        showFilter(CATEGORY, positionCategory);
    }

    private void showFilter(int type, int positionSelected) {
        replaceFragment(R.id.container_rewards_shop,
                FilterShopFragment.newInstance(type,
                        positionSelected,
                        new ArrayList<>(listCategory),
                        null)
                , HistoryPointContainerFragment.TAG);
    }

    @OnClick(R.id.iconRank)
    void showCamIDReward() {
        replaceFragment(R.id.container_rewards_shop, RewardCamIdKHFragment.newInstance(), HistoryPointContainerFragment.TAG);
    }

    private void logH(String m) {
        Log.e("H-RewardShop", m);
    }

    /**
     * get info rank of acc
     */
    private void getRankAccount(boolean resetPoint) {
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountRankInfo(new KhApiCallback<KHBaseResponse<AccountRankInfoResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountRankInfoResponse> body) {
                logH("wsGetAccountRankInfo > onSuccess");
                initRankInfo(body.getData().getAccountRankDTO(), resetPoint);
            }

            @Override
            public void onFailed(String status, String message) {
                logH("wsGetAccountRankInfo > onFailed");
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsGetAccountRankInfo > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountRankInfoResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountRankInfo > onFailure");
            }
        });
    }

    private void initRankInfo(AccountRankDTO account, boolean resetPoint) {
        accountRankDTO = account;
        if (account != null) {
            int rankID = account.rankId;
            KhUserRank.RewardRank myRank = KhUserRank.RewardRank.getById(rankID);
            if (myRank != null) {
                iconRank.setImageDrawable(myRank.drawable);
            }
        }
        if (!resetPoint)
            getDataCategory();
    }


    /**
     * get info list point of acc
     */
    private void wsGetAccountPointInfo() {

        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        homeKhClient.wsGetAccountPointInfo(new KhApiCallback<KHBaseResponse<AccountPointRankResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<AccountPointRankResponse> body) {
                logH("wsGetAccountPointInfo > onSuccess");
                subPoint(body.getData().getListPoint());
            }

            @Override
            public void onFailed(String status, String message) {
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
                logH("wsGetAccountPointInfo > onFailed");
                logH("wsGetAccountPointInfo > " + status + " " + message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<AccountPointRankResponse>>> call, Throwable t) {
                t.printStackTrace();
                logH("wsGetAccountPointInfo > onFailure");
            }
        });
    }

    private void subPoint(List<KHAccountPoint> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        int totalAccumulate = 0;
        int totalAvailability = 0;
        for (KHAccountPoint item : list) {
            if (item.getPointType() == PointType.ACTIVE.id) {
                totalAccumulate = totalAccumulate + item.getPointValue();
            } else if (item.getPointType() == PointType.PAST.id) {
                totalAvailability = totalAvailability + item.getPointValue();
            }
        }
        if (totalAvailability > 1) {
            tvPoint.setText(String.format(getString(R.string.reward_shop_points), formatPoint(totalAvailability)));
        } else {
            tvPoint.setText(String.format(getString(R.string.reward_item_point), formatPoint(totalAvailability)));
        }
    }

    private void getDataCategory() {
        if (accountRankDTO == null) {
            logH("accountRankDTO getDataCategory > null");
            return;
        }
        if (homeKhClient == null) {
            homeKhClient = new KhHomeClient();
        }
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);
        homeKhClient.wsGetAllPartnerGiftSearch(new KhApiCallback<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>>() {
            @Override
            public void onSuccess(KHBaseResponse<WsGetAllPartnerGiftSearchResponse> body) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                logH("wsGetAllPartnerGiftSearch > onSuccess");
                List<PartnerGiftSearch> list = body.getData().getWsResponse().getObject();
                listPartnerGift.clear();

                for (PartnerGiftSearch item : list) {
                    if (item.getListGiftItems() != null && !item.getListGiftItems().isEmpty()) {
                        listPartnerGift.add(item);
                    }
                }
                showDataRv();
            }

            @Override
            public void onFailed(String status, String message) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                logH("wsGetAllPartnerGiftSearch > onFailed");
                logH("wsGetAllPartnerGiftSearch > " + status + " " + message);
                if (getActivity() != null)
                    com.metfone.selfcare.v5.utils.ToastUtils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<KHBaseResponse<KHBaseResponse<WsGetAllPartnerGiftSearchResponse>>> call, Throwable t) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                t.printStackTrace();
                logH("wsGetAllPartnerGiftSearch > onFailure");
            }
            // Ho� ?ang l?y theo position -> gi� tr? ?�ng l� position + 1.
        }, String.valueOf(positionFilter + 1), accountRankDTO.rankId);

    }

    private void showDataRv() {
        listCategory.clear();
        listGifts.clear();
        for (PartnerGiftSearch item : listPartnerGift) {
            List<GiftItems> gifts = item.getListGiftItems();
            if (gifts != null && !gifts.isEmpty()) {
                listCategory.add(new CategoryFilter(item.getGiftTypeName(), item.getIconUrl()));
                if (!StringUtils.isEmpty(categoryID)) {
                    if(categoryID.equals("1")){ // type all from deep link
                        filterCategory.setText(getString(R.string.all));
                        listGifts.addAll(gifts);
                    }
                    else if(categoryID.equals(item.getId())){
                        filterCategory.setText(item.getGiftTypeName());
                        listGifts.addAll(gifts);
                        break;
                    }
                } else if (categoryName.toLowerCase().equals(TYPE_ALL.toLowerCase())
                        || item.getGiftTypeName().toLowerCase().equals(categoryName.toLowerCase())) {
                    listGifts.addAll(gifts);
                }
            }
        }


        if (mSelectedCategoryPositionFromHomeTab != null) {
            int position = Integer.parseInt(mSelectedCategoryPositionFromHomeTab);
            positionCategory = position + 1; // ALL ITEM

            CategoryFilter selected = listCategory.get(position - 1);
            categoryName = selected.getName();
            mSelectedCategoryPositionFromHomeTab = null;
            listGifts.clear();
            filterCategory.setText(categoryName);

            for (PartnerGiftSearch item : listPartnerGift) {
                List<GiftItems> gifts = item.getListGiftItems();
                if (gifts != null && !gifts.isEmpty()) {
                    if (item.getGiftTypeName().toLowerCase().equals(categoryName.toLowerCase())) {
                        listGifts.addAll(gifts);
                    }
                }
            }
        }

        rewardsShopAdapter.notifyDataSetChanged();
    }
}
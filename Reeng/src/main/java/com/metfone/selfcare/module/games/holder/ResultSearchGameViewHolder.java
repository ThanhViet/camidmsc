package com.metfone.selfcare.module.games.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.metfone.selfcare.R;
import com.metfone.selfcare.model.tabGame.GameModel;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.BaseViewHolder;
import com.metfone.selfcare.module.metfoneplus.topup.baseRecyclerview.ItemViewClickListener;
import com.metfone.selfcare.ui.imageview.roundedimageview.RoundedImageView;

import java.util.List;

public class ResultSearchGameViewHolder extends BaseViewHolder {

    private RoundedImageView iv_cover;
    private TextView tvTitleGame;

    public ResultSearchGameViewHolder(@NonNull View itemView, ItemViewClickListener itemViewClickListener) {
        super(itemView, itemViewClickListener);
    }

    @Override
    public void initViewHolder(View v) {
        iv_cover = v.findViewById(R.id.iv_cover);
        tvTitleGame = v.findViewById(R.id.tvTitleGame);
    }

    @Override
    public void onBinViewHolder(List<?> obj, int pos, Context context) {
        if (mData.get(pos) instanceof GameModel) {
            GameModel game = (GameModel) mData.get(pos);
            Glide.with(itemView.getContext())
                    .asBitmap().error(R.drawable.df_image_home_poster)
                    .load(game.getIconURL())
                    .into(iv_cover);
            tvTitleGame.setText(game.getName());
        }
    }

    @Override
    public void onItemViewClick(View v, int pos) {
        if (itemViewClickListener != null) {
            itemViewClickListener.onItemViewClickListener(pos, mData);
        }
    }
}

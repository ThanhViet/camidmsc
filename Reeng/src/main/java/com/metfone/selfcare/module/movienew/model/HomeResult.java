
package com.metfone.selfcare.module.movienew.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeResult {

    @SerializedName("recommend")
    @Expose
    private List<HomeData> recommend = null;
    @SerializedName("trending")
    @Expose
    private List<HomeData> trending = null;
    @SerializedName("continue")
    @Expose
    private List<HomeData> continueWatching = null;
    @SerializedName("movie4you")
    @Expose
    private List<HomeData> movie4you = null;
    @SerializedName("recentlyAdded")
    @Expose
    private List<HomeData> recentlyAdded = null;
    @SerializedName("avaiable")
    @Expose
    private List<HomeData> avaiable = null;
    @SerializedName("custom_topic")
    @Expose
    private List<CustomTopic> customTopic = null;
    @SerializedName("watchedByFriend")
    @Expose
    private List<HomeData> watchedByFriend = null;

    public List<HomeData> getRecommend() {
        return recommend;
    }

    public void setRecommend(List<HomeData> recommend) {
        this.recommend = recommend;
    }

    public List<HomeData> getTrending() {
        return trending;
    }

    public void setTrending(List<HomeData> trending) {
        this.trending = trending;
    }

    public List<HomeData> getContinue() {
        return continueWatching;
    }

    public void setContinue(List<HomeData> _continue) {
        this.continueWatching = _continue;
    }

    public List<HomeData> getMovie4you() {
        return movie4you;
    }

    public void setMovie4you(List<HomeData> movie4you) {
        this.movie4you = movie4you;
    }

    public List<HomeData> getRecentlyAdded() {
        return recentlyAdded;
    }

    public void setRecentlyAdded(List<HomeData> recentlyAdded) {
        this.recentlyAdded = recentlyAdded;
    }

    public List<HomeData> getAvaiable() {
        return avaiable;
    }

    public void setAvaiable(List<HomeData> avaiable) {
        this.avaiable = avaiable;
    }

    public List<CustomTopic> getCustomTopic() {
        return customTopic;
    }

    public void setCustomTopic(List<CustomTopic> customTopic) {
        this.customTopic = customTopic;
    }

    public List<HomeData> getWatchedByFriend() {
        return watchedByFriend;
    }

    public void setWatchedByFriend(List<HomeData> watchedByFriend) {
        this.watchedByFriend = watchedByFriend;
    }

}
